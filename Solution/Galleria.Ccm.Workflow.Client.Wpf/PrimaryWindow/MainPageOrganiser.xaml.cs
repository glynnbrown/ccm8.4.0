﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Ineson 
//    Created.
// V8-28002 : L.Ineson 
//  Removed plan assignment changes
// V8-28290 : L.Ineson
//  Added calls to start and stop data polling on the repository and workpackage screens.
// V8-28559 : D.Pleasance
//  Added xRibbonBackstageTabControl_SelectionChanged
// V8:29162 : I.George
//   Added ribbon_SelectedTabChanged and xRibbonBackstage_IsOpenChanged event handlers.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using Fluent;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment;
using Galleria.Ccm.Workflow.Client.Wpf.PlanRepository;
using Galleria.Ccm.Workflow.Client.Wpf.Reports;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.PrintTemplateSetup;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for MainPageOrganiser.xaml
    /// </summary>
    public sealed partial class MainPageOrganiser : ExtendedRibbonWindow
    {
        #region Fields

        private Boolean _forceScreensReload = false; //flag to indicate that screens should be dropped at next load.
        private PlanRepositoryOrganiser _pgPlanRepository;
        private PlanAssignmentOrganiser _pgPlanAssignment;
        private WorkPackagesOrganiser _pgWorkPackagesOrganiser;
        private ReportsOrganiser _pgReportsOrganiser;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(MainPageOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainPageOrganiser senderControl = (MainPageOrganiser)obj;

            if (e.OldValue != null)
            {
                MainPageViewModel oldModel = (MainPageViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                MainPageViewModel newModel = (MainPageViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;

                //load the current selected screen
                senderControl.OnSelectedScreenChanged();
            }
        }

        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SidePanelContent Property

        public static readonly DependencyProperty SidePanelContentProperty =
            DependencyProperty.Register("SidePanelContent", typeof(Object), typeof(MainPageOrganiser));

        public Object SidePanelContent
        {
            get { return GetValue(SidePanelContentProperty); }
            private set { SetValue(SidePanelContentProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        static MainPageOrganiser()
        {
            //set common help file keys
            ColumnLayoutEditorOrganiser.HelpFileKey = HelpFileKeys.CustomColumnsEditor;
            PrintTemplateSetupWindow.HelpFileKey = HelpFileKeys.PrintTemplateSetup;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MainPageOrganiser()
        {
            InitializeComponent();

            this.ViewModel = new MainPageViewModel();

            App.ViewState.OnShowRibbonTabs += ViewState_OnShowRibbonTabs;
            App.ViewState.OnShowRibbonContextGroups += ViewState_OnShowRibbonContextGroups;
            App.ViewState.OnShowSidePanel += ViewState_OnShowSidePanel;

            Help.SetFilename((DependencyObject)this, App.ViewState.HelpFilePath);

            this.Loaded += MainPageOrganiser_Loaded;
        }

        #endregion

        #region Event Handlers

        #region ThisControl Handlers

        /// <summary>
        /// Carries out intitial first load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPageOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            //Unsubscribe
            this.Loaded -= MainPageOrganiser_Loaded;

            //[SA-19082]Maximise the window after load - doing this in xaml causes problems
            Dispatcher.BeginInvoke((Action)(() => WindowState = System.Windows.WindowState.Maximized), priority: DispatcherPriority.ContextIdle);

            //LocalHelper.SetRibbonBackstageState(this, true);

        }

        private void xRibbonBackstageTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetHelpFileKeys();
        }

        private void ribbon_SelectedTabChanged(object sender, SelectionChangedEventArgs e)
        {
            SetHelpFileKeys();
        }

        private void xRibbonBackstage_IsOpenChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SetHelpFileKeys();
        }


        #endregion

        #region ViewModel Handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == MainPageViewModel.SelectedScreenProperty.Path)
            {
                OnSelectedScreenChanged();
            }
        }

        #endregion

        #region ViewState Handlers

        /// <summary>
        /// Responds to a request to show ribbon tabs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewState_OnShowRibbonTabs(object sender, ViewStateEventArgs<IEnumerable<RibbonTabItem>> e)
        {
            //clear the current tabs from the ribbon
            //don't use tabs.clear() as this does not work
            List<RibbonTabItem> existingTabs = this.ribbon.Tabs.ToList();
            foreach (RibbonTabItem tab in existingTabs)
            {
                this.ribbon.Tabs.Remove(tab);
            }

            //add in the new tabs
            foreach (RibbonTabItem tab in e.UserState)
            {
                this.ribbon.Tabs.Add(tab);
            }

            //select the first tab
            if (this.ribbon.Tabs.Count > 0)
            {
                this.ribbon.Tabs.First().IsSelected = true;
            }
        }

        /// <summary>
        /// Responds to a request to add a ribbon context group
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewState_OnShowRibbonContextGroups(object sender, ViewStateEventArgs<IEnumerable<RibbonContextualTabGroup>> e)
        {
            if (ribbon != null)
            {
                //ribbon.ContextualGroups.Clear();
                //[SA-20089] remove individually as clear does not work correctly.
                List<RibbonContextualTabGroup> existingGroups = ribbon.ContextualGroups.ToList();
                foreach (RibbonContextualTabGroup group in existingGroups)
                {
                    ribbon.ContextualGroups.Remove(group);
                }



                foreach (RibbonContextualTabGroup contextGroup in e.UserState)
                {
                    ribbon.ContextualGroups.Add(contextGroup);
                }
            }
        }

        /// <summary>
        /// Responds to a request to show sidepanel content
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewState_OnShowSidePanel(object sender, ViewStateEventArgs<Object> e)
        {
            Object newContent = null;

            if (e.UserState != null)
            {
                if (e.UserState is UserControl)
                {
                    newContent = e.UserState;
                }
            }
            this.SidePanelContent = newContent;
        }

        #endregion

        /// <summary>
        /// Allow expansion and collapse of side panel on Return key press for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSidePanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                xSidePanel.IsExpanded = !xSidePanel.IsExpanded;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Called whenever the currently selected screen changes.
        /// </summary>
        private void OnSelectedScreenChanged()
        {
            if (this.ViewModel == null) { return; }
            if (this.pagePresenter == null) { return; }

            //show wait cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //clean up
            this.pagePresenter.Content = null;
            this.SidePanelContent = null;
            this.ribbon.Tabs.ToList().ForEach(t => this.ribbon.Tabs.Remove(t));
            this.ribbon.ContextualGroups.ToList().ForEach(g => this.ribbon.ContextualGroups.Remove(g));


            #region dispose of old viewmodels
            if (_forceScreensReload)
            {
                //drop all of the old screens so that everything gets reloaded.
                _forceScreensReload = false;

                foreach (UIScreen screen in Enum.GetValues(typeof(UIScreen)))
                {
                    switch (screen)
                    {
                        case UIScreen.PlanRepository:
                            if (_pgPlanRepository != null)
                            {
                                _pgPlanRepository.Dispose();
                                _pgPlanRepository = null;
                            }
                            break;

                        case UIScreen.WorkPackages:
                            if (_pgWorkPackagesOrganiser != null)
                            {
                                _pgWorkPackagesOrganiser.Dispose();
                                _pgWorkPackagesOrganiser = null;
                            }
                            break;

                        case UIScreen.Reports:
                            if (_pgReportsOrganiser != null)
                            {
                                _pgReportsOrganiser.Dispose();
                                _pgReportsOrganiser = null;
                            }
                            break;


                        default: Debug.Fail("UI screen not getting disposed"); break;
                    }
                }
            }
            else
            {
                //Stop any data polling if we are no longer on that screen.
                if (this.ViewModel.SelectedScreen != UIScreen.PlanRepository
                    && _pgPlanRepository != null
                    && _pgPlanRepository.ViewModel != null)
                {
                    _pgPlanRepository.ViewModel.StopPolling();
                }
                if (this.ViewModel.SelectedScreen != UIScreen.WorkPackages
                    && _pgWorkPackagesOrganiser != null
                    && _pgWorkPackagesOrganiser.ViewModel != null)
                {
                    _pgWorkPackagesOrganiser.ViewModel.StopPolling();
                }
            }
            #endregion

            //load the new selected screen
            switch (this.ViewModel.SelectedScreen)
            {
                case UIScreen.PlanRepository:
                    {
                        if (_pgPlanRepository == null)
                        {
                            _pgPlanRepository = new PlanRepositoryOrganiser();
                        }
                        else
                        {
                            _pgPlanRepository.ViewModel.StartPolling();
                        }

                        this.pagePresenter.Content = _pgPlanRepository;
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanRepository);
                    }
                    break;

                case UIScreen.PlanAssignment:
                    {
                        if (_pgPlanAssignment == null)
                        {
                            _pgPlanAssignment = new PlanAssignmentOrganiser();
                        }
                        this.pagePresenter.Content = _pgPlanAssignment;
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanAssignment);
                    }
                    break;

                case UIScreen.WorkPackages:
                    {
                        if (_pgWorkPackagesOrganiser == null)
                        {
                            _pgWorkPackagesOrganiser = new WorkPackagesOrganiser();
                        }
                        else
                        {
                            _pgWorkPackagesOrganiser.ViewModel.StartPolling();
                        }

                        this.pagePresenter.Content = _pgWorkPackagesOrganiser;
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.Workpackages);
                    }
                    break;

                case UIScreen.Reports:
                    {
                        if (_pgReportsOrganiser == null)
                        {
                            _pgReportsOrganiser = new ReportsOrganiser();
                        }

                        this.pagePresenter.Content = _pgReportsOrganiser;
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.Reports);
                    }
                    break;

                default: Debug.Fail("UIScreen type not recognised"); break;
            }

            //cancel wait cursor on a dispatcher
            Mouse.OverrideCursor = null;
        }

        /// <summary>
        /// Called whenever the tabcontrol ot tabitem changes
        /// </summary>
        private void SetHelpFileKeys()
        {
            if (xRibbonBackstage.IsOpen)
            {
                var tab = ribbonBackstageTabControl;
                if (tab != null)
                {
                    if (tab.SelectedItem == BackstageSetupTab)
                    {
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageSetup);
                    }
                    else if (tab.SelectedItem == BackstageMasterDataSetupTab)
                    {
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageMasterDataSetup);
                    }
                    else if (tab.SelectedItem == BackstageContentSetupTab)
                    {
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageContentSetup);
                    }
                    else if (tab.SelectedItem == BackstageSysAdminTab)
                    {
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageSysAdmin);
                    }
                    else if (tab.SelectedItem == xBackstageDataManagementTab)
                    {
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageDataManagement);
                    }
                    else if (tab.SelectedItem == BackstageHelpTab)
                    {
                        HelpFileKeys.SetHelpFile(this, HelpFileKeys.BackstageHelp);
                    }
                }
            }
            else
            {
                if (this.ViewModel.SelectedScreen == UIScreen.PlanRepository)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanRepository);
                }
                else if (this.ViewModel.SelectedScreen == UIScreen.PlanAssignment)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanAssignment);
                }
                else if (this.ViewModel.SelectedScreen == UIScreen.WorkPackages)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.Workpackages);
                }
                else if (this.ViewModel.SelectedScreen == UIScreen.Reports)
                {
                    HelpFileKeys.SetHelpFile(this, HelpFileKeys.Reports);
                }
            }
        }
        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                IDisposable dis = this.ViewModel;

                this.ViewModel = null;

                if (dis != null)
                {
                    dis.Dispose();
                }

            }

        }

        #endregion

    }
}
