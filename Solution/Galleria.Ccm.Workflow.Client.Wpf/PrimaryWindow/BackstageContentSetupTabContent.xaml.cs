﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27994 : L.Ineson
//  Created
#endregion

#endregion

using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow
{
    public sealed partial class BackstageContentSetupTabContent 
    {
        /// <summary>
        /// Viewmodel dependency property definition
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(BackstageContentSetupTabContent));

        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public BackstageContentSetupTabContent()
        {
            //nb - dont initialize until we are made visible.
            this.IsVisibleChanged += BackstageContentSetupTabContent_IsVisibleChanged;
        }


        /// <summary>
        /// Called when the tab is initially made visible.
        /// </summary>
        private void BackstageContentSetupTabContent_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(this.IsVisible)
            {
                InitializeComponent();
                this.IsVisibleChanged -= BackstageContentSetupTabContent_IsVisibleChanged;
            }
        }
    }
}
