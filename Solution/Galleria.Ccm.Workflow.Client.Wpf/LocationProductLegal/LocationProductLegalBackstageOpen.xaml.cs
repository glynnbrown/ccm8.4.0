﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductLegalBackstageOpen.xaml
    /// </summary>
    public sealed partial class LocationProductLegalBackstageOpen : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationProductLegalViewModel), typeof(LocationProductLegalBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationProductLegalViewModel ViewModel
        {
            get { return (LocationProductLegalViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationProductLegalBackstageOpen senderControl = (LocationProductLegalBackstageOpen)obj;
            senderControl.UpdateItemSource();
        }

        #endregion

        #endregion

        #region Constructor

        public LocationProductLegalBackstageOpen()
        {
            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the grid itemsource based on the available product groups.
        /// </summary>
        private void UpdateItemSource()
        {
            if (this.xAvailableItemsDisplay != null)
            {
                ObservableCollection<ProductGroupViewModel> groupViews = new ObservableCollection<ProductGroupViewModel>();

                if (this.ViewModel != null)
                {
                    foreach (ProductGroup group in this.ViewModel.AvailableProductGroups)
                    {
                        groupViews.Add(new ProductGroupViewModel(group));
                    }
                }

                this.xAvailableItemsDisplay.ItemsSourceExtended = groupViews;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a grid row item double click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableItemsDisplay_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            ProductGroupViewModel item = e.RowItem as ProductGroupViewModel;
            if (item != null && item.ProductGroup != null)
            {
                //Open the category
                this.ViewModel.OpenCommand.Execute(item.ProductGroup.Id);
            }
        }

        #endregion

        private void XAvailableItemsDisplay_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selected = xAvailableItemsDisplay.CurrentItem as ProductGroupViewModel;
                if(selected != null)
                    this.ViewModel.OpenCommand.Execute(selected.ProductGroup.Id);
            }
        }
    }
}
