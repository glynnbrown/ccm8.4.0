﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
// V8-32245 : N.Haywood
//  Changed from location to location info
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.Collections;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance
{
    /// <summary>
    /// ViewModel controller for LocationProductLegalNewRelationship screen
    /// </summary>
    public sealed class LocationProductLegalNewRelationshipViewModel : ViewModelAttachedControlObject<LocationProductLegalNewRelationship>
    {
        #region Fields

        private readonly ReadOnlyCollection<LocationInfo> _masterLocationList;

        private readonly BulkObservableCollection<LocationInfo> _availableLocations = new BulkObservableCollection<LocationInfo>();
        private readonly ReadOnlyBulkObservableCollection<LocationInfo> _availableLocationsRO;
        private readonly ObservableCollection<LocationInfo> _selectedLocations = new ObservableCollection<LocationInfo>();

        private readonly ReadOnlyCollection<ProductInfo> _availableProductsRO;
        private readonly ObservableCollection<ProductInfo> _selectedProducts = new ObservableCollection<ProductInfo>();

        private IEnumerable<LocationProductLegalRowViewModel> _masterLinksCollection;
        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<LocationProductLegalNewRelationshipViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedLocationsProperty = WpfHelper.GetPropertyPath<LocationProductLegalNewRelationshipViewModel>(p => p.SelectedLocations);
        public static readonly PropertyPath AvailableProductsProperty = WpfHelper.GetPropertyPath<LocationProductLegalNewRelationshipViewModel>(p => p.AvailableProducts);
        public static readonly PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<LocationProductLegalNewRelationshipViewModel>(p => p.SelectedProducts);

        //commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalNewRelationshipViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalNewRelationshipViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available locations
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationInfo> AvailableLocations
        {
            get { return _availableLocationsRO; }
        }

        /// <summary>
        /// Returns the editable collection of selected locations
        /// </summary>
        public ObservableCollection<LocationInfo> SelectedLocations
        {
            get { return _selectedLocations; }
        }

        /// <summary>
        /// Returns the collection of available products
        /// </summary>
        public ReadOnlyCollection<ProductInfo> AvailableProducts
        {
            get { return _availableProductsRO; }
        }

        public ObservableCollection<ProductInfo> SelectedProducts
        {
            get { return _selectedProducts; }
        }

        #endregion

        #region Constructor

        public LocationProductLegalNewRelationshipViewModel(
            IEnumerable<LocationInfo> masterLocations,
            IEnumerable<ProductInfo> masterProducts,
            IEnumerable<LocationProductLegalRowViewModel> masterLinksCollection)
        {
            _masterLocationList = new ReadOnlyCollection<LocationInfo>(masterLocations.ToList());
            _availableLocations.AddRange(_masterLocationList);
            _availableLocationsRO = new ReadOnlyBulkObservableCollection<LocationInfo>(_availableLocations);

            _availableProductsRO = new ReadOnlyCollection<ProductInfo>(masterProducts.ToList());

            _masterLinksCollection = masterLinksCollection;
        }

        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _okCommand;

        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            //must have products selected
            if (this.SelectedProducts.Count == 0) { return false; }

            //must have locations selected
            if (this.SelectedLocations.Count == 0) { return false; }

            return true;
        }

        /// <summary>
        /// Confirms the selected items and creates the Legal links
        /// </summary>
        private void OK_Executed()
        {
            //add the links and close
            foreach (ProductInfo product in this.SelectedProducts)
            {
                foreach (LocationInfo location in this.SelectedLocations)
                {
                    LocationProductLegalRowViewModel row = _masterLinksCollection.FirstOrDefault(p => p.ProductInfo.Id == product.Id);
                    if (row != null)
                    {
                        row.EditDictionaryValue(location.Id, true);
                    }
                }
            }

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }

        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the dialog.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        public void OnProductsGrid_SelectedChanged(object sender, SelectionChangedEventArgs e)
        {
            //TODO: SORT OUT! This should not be accessing the attached control grid.

            DataGrid productGrid = (DataGrid)sender;
            ProductInfo selectedProduct = (ProductInfo)productGrid.SelectedItem;

            //Repopulate the available location rows
            _availableLocations.Clear();
            _availableLocations.AddRange(_masterLocationList);

            ObservableCollection<LocationInfo> locationsToDelete = new ObservableCollection<LocationInfo>();

            foreach (LocationInfo location in _availableLocations)
            {
                if (this.SelectedProducts.Count != 0)
                {
                    //If the user has selected more than one product
                    if (this.SelectedProducts.Count > 1)
                    {
                        Int32 matchCount = 0;
                        //Check each product to see if it is already linked to this location
                        foreach (ProductInfo productInfo in this.SelectedProducts)
                        {
                            if (_masterLinksCollection.FirstOrDefault(p => p.ProductInfo.Id == productInfo.Id &&
                                                                     p.LocationLegalLinks.FirstOrDefault(l => l.Key == location.Id).Value == true) != null)
                            {
                                matchCount++;
                            }
                        }

                        //If all the product are linked to this location
                        if (matchCount == this.SelectedProducts.Count)
                        {
                            locationsToDelete.Add(location);
                        }
                    }
                    //If the selected product is linked to this location
                    else if (_masterLinksCollection.FirstOrDefault(p => p.ProductInfo.Id == selectedProduct.Id &&
                                                                     p.LocationLegalLinks.FirstOrDefault(l => l.Key == location.Id).Value == true) != null)
                    {
                        //Add the location to the list to be removed
                        locationsToDelete.Add(location);
                    }
                }
            }

            //Remove the found locations from the available locations
            foreach (LocationInfo deleteLocation in locationsToDelete)
            {
                _availableLocations.Remove(deleteLocation);
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                _masterLinksCollection = null;

                base.IsDisposed = true;
            }
        }


        #endregion
    }
}
