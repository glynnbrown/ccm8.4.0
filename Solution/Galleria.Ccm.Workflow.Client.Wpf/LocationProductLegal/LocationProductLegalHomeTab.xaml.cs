﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductLegalHomeTab.xaml
    /// </summary>
    public partial class LocationProductLegalHomeTab : RibbonTabItem
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationProductLegalViewModel), typeof(LocationProductLegalHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationProductLegalViewModel ViewModel
        {
            get { return (LocationProductLegalViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationProductLegalHomeTab senderControl = (LocationProductLegalHomeTab)obj;

            if (e.OldValue != null)
            {
                LocationProductLegalViewModel oldModel = (LocationProductLegalViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationProductLegalViewModel newModel = (LocationProductLegalViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationProductLegalHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
