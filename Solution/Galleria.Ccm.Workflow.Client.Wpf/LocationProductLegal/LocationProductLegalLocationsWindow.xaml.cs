﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductLegalLocationsWindow.xaml
    /// </summary>
    public sealed partial class LocationProductLegalLocationsWindow : ExtendedRibbonWindow
    {
        #region Fields
        private readonly BulkObservableCollection<Location> _availableLocations = new BulkObservableCollection<Location>();
        #endregion

        #region Properties

        #region AvailableLocationsProperty

        public static readonly DependencyProperty AvailableLocationsProperty =
            DependencyProperty.Register("AvailableLocations", typeof(ReadOnlyBulkObservableCollection<Location>),
            typeof(LocationProductLegalLocationsWindow));

        public ReadOnlyBulkObservableCollection<Location> AvailableLocations
        {
            get { return (ReadOnlyBulkObservableCollection<Location>)GetValue(AvailableLocationsProperty); }
        }

        #endregion

        #region SelectedLocationsProperty

        public static readonly DependencyProperty SelectedLocationsProperty =
            DependencyProperty.Register("SelectedLocations", typeof(ObservableCollection<Location>),
            typeof(LocationProductLegalLocationsWindow));

        public ObservableCollection<Location> SelectedLocations
        {
            get { return (ObservableCollection<Location>)GetValue(SelectedLocationsProperty); }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationProductLegalLocationsWindow(IEnumerable<Location> masterLocationsList,
            IEnumerable<Location> preSelectedLocations)
        {
            SetValue(AvailableLocationsProperty, new ReadOnlyBulkObservableCollection<Location>(_availableLocations));
            SetValue(SelectedLocationsProperty, new ObservableCollection<Location>());

            this.SelectedLocations.CollectionChanged += SelectedLocations_CollectionChanged;

            InitializeComponent();

            _availableLocations.AddRange(masterLocationsList);

            foreach (Location loc in preSelectedLocations)
            {
                this.SelectedLocations.Add(loc);
            }

        }


        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //load the columnsets
            LocationImportMappingList mappingList = LocationImportMappingList.NewLocationImportMappingList();

            //create the datagrid columnsets
            //using (LocationCustomAttributeListViewModel locationCustomAttributeList = new Common.LocationCustomAttributeListViewModel())
            //{
            //    locationCustomAttributeList.FetchAllForEntity();
            //    mappingList = LocationImportMappingList.NewLocationImportMappingList(locationCustomAttributeList.Model);
            //}

            Int32[] initialVisibleMapIds = new Int32[]
            {
                LocationImportMappingList.CodeMapId,
                LocationImportMappingList.NameMapId,
                LocationImportMappingList.Address1MapId,
                LocationImportMappingList.Address2MapId,
                LocationImportMappingList.RegionMapId,
                LocationImportMappingList.CountyMapId,
                LocationImportMappingList.PostalCodeMapId,
                LocationImportMappingList.CityMapId,
                LocationImportMappingList.CountyMapId
            };

            //nb - we are removing the location group code and type name cols
            // as we are only using the location object as the source for this.
            Dictionary<Int32, String> specialCases = new Dictionary<Int32, String>();
            specialCases.Add(LocationImportMappingList.GroupCodeMapId, null);
            //specialCases.Add(LocationImportMappingList.LocationTypeMapId, null);

            //load the unassigned locs list
            DataGridColumnCollection unassignedLocationsColumnSet = new DataGridColumnCollection();
            List<DataGridColumn> unassignedLocationsCols =
                DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList, /*bindingPrefix*/null, initialVisibleMapIds, specialCases);

            foreach (DataGridColumn column in unassignedLocationsCols)
            {
                unassignedLocationsColumnSet.Add(column);
            }


            xUnassignedLocations.ColumnSet = unassignedLocationsColumnSet;


            //load the assigned locs list
            DataGridColumnCollection assignedLocationsColumnSet = new DataGridColumnCollection();

            foreach (DataGridColumn column in DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList, /*bindingPrefix*/null, initialVisibleMapIds, specialCases))
            {
                assignedLocationsColumnSet.Add(column);
            }

            xAssignedLocations.ColumnSet = assignedLocationsColumnSet;
        }

        private void SelectedLocations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    {
                        foreach (Location loc in e.NewItems)
                        {
                            _availableLocations.Remove(loc);
                        }
                    }
                    break;

                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    {
                        foreach (Location loc in e.OldItems)
                        {
                            _availableLocations.Add(loc);
                        }
                    }
                    break;

                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    //TODO
                    break;
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            this.DialogResult = false;
        }


        #region Unassigned Locations Grid

        /// <summary>
        /// Adds the doubleclicked row to the current sublevel stores
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedLocations_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.SelectedLocations.Add((Location)e.RowItem);
        }

        /// <summary>
        /// Removes the passed stores from the current sublevel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedLocations_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            foreach (Location loc in e.Items.Cast<Location>().ToList())
            {
                this.SelectedLocations.Remove(loc);
            }
        }


        private void xUnassignedLocations_ItemsSelected(object sender, ExtendedDataGridItemSelectedEventArgs e)
        {
            xAssignedLocations.SelectedItems.Clear();
        }

        ///// <summary>
        ///// Makes an add request for all selected items
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void UnassignedGridRowContext_AddSelected(object sender, RoutedEventArgs e)
        //{
        //    List<Location> locationsToAdd = xUnassignedLocations.SelectedItems.Cast<Location>().ToList();
        //    this.SelectedLocations.AddItems(locationsToAdd);
        //}
        private void XUnassignedLocations_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                this.SelectedLocations.Add((Location)xUnassignedLocations.CurrentItem);
        }

        #endregion

        #region Assigned Locations Grid

        /// <summary>
        /// Removes the store from the current sublevel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedLocations_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.SelectedLocations.Remove((Location)e.RowItem);
        }

        /// <summary>
        /// Adds the dropped stores to the sublevel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedLocations_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            List<Location> locations = e.Items.Cast<Location>().ToList();
            foreach (Location location in locations)
            {
                this.SelectedLocations.Add(location);
            }

        }

        private void xAssignedLocations_ItemsSelected(object sender, ExtendedDataGridItemSelectedEventArgs e)
        {
            xUnassignedLocations.SelectedItems.Clear();
        }



        ///// <summary>
        ///// Makes a remove request for all selected items
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void xAssignedLocationsRowContext_RemoveSelected(object sender, RoutedEventArgs e)
        //{
        //    List<Location> removeStores = xAssignedLocations.SelectedItems.Cast<Location>().ToList();

        //    foreach (Location loc in removeStores)
        //    {
        //        this.SelectedLocations.Remove(loc);
        //    }

        //}
        private void XAssignedLocations_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.SelectedLocations.Remove((Location)xAssignedLocations.CurrentItem);
            }
        }
        #endregion

        #endregion
    }
}
