﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
// V8-32243 : N.Haywood
//  Made add links only be able to be selected for the locations that show in the grid
//  Added location selection warning
// V8-32245 : N.Haywood
//  Changed to use the generic location selector
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance
{
    public sealed class LocationProductLegalViewModel : ViewModelAttachedControlObject<LocationProductLegalOrganiser>
    {
        #region Fields

        const String _exceptionCategory = "LocationProductLegalMaintenance";

        private readonly ReadOnlyCollection<ProductGroup> _availableProductGroups;
        private ProductGroup _selectedProductGroup;

        private LocationProductLegalListViewModel _masterLocationProductLegalListView;
        private BulkObservableCollection<LocationProductLegalRowViewModel> _availableLegalLinkRows = new BulkObservableCollection<LocationProductLegalRowViewModel>();

        private readonly ReadOnlyCollection<LocationInfo> _masterLocationsList;
        private readonly BulkObservableCollection<LocationInfo> _selectedLocations = new BulkObservableCollection<LocationInfo>();


        private readonly BulkObservableCollection<ProductInfo> _availableProductInfos = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _availableProductInfosRO;

        private Boolean _filterLegalLinks;
        private DataGridColumnCollection _columnSet = new DataGridColumnCollection();

        private Dictionary<Tuple<Int32, Int16>, LocationProductLegal> _LegalLinksDictionary = new Dictionary<Tuple<int, short>, LocationProductLegal>();

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableProductGroupsProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(c => c.AvailableProductGroups);
        public static readonly PropertyPath SelectedProductGroupProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.SelectedProductGroup);
        public static readonly PropertyPath AvailableLocationInfosProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedLocationsProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.SelectedLocations);
        public static readonly PropertyPath AvailableProductInfosProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.AvailableProductInfos);
        public static readonly PropertyPath AvailableLegalLinkRowsProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.AvailableLegalLinkRows);
        public static readonly PropertyPath FilterLegalLinksProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.FilterLegalLinks);
        public static readonly PropertyPath ColumnSetProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.ColumnSet);

        //commands
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(c => c.OpenCommand);
        public static readonly PropertyPath AddLegalLinkCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(v => v.AddLegalLinkCommand);
        public static readonly PropertyPath RemoveLegalLinkCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(v => v.RemoveLegalLinkCommand);
        public static readonly PropertyPath RemoveAllLegalLinksCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(v => v.RemoveAllLegalLinksCommand);
        public static readonly PropertyPath AutoFilterExistingLinksCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(v => v.AutoFilterExistingLinksCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(v => v.CloseCommand);
        public static readonly PropertyPath SetSelectedLocationsCommandProperty = WpfHelper.GetPropertyPath<LocationProductLegalViewModel>(v => v.SetSelectedLocationsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the readonly collection  of product groups available for selection.
        /// </summary>
        public ReadOnlyCollection<ProductGroup> AvailableProductGroups
        {
            get { return _availableProductGroups; }
        }

        /// <summary>
        /// Gets/Sets the selected financial group
        /// </summary>
        public ProductGroup SelectedProductGroup
        {
            get { return _selectedProductGroup; }
            set
            {
                _selectedProductGroup = value;
                OnPropertyChanged(SelectedProductGroupProperty);

                OnSelectedProductGroupChanged(value);
            }
        }

        /// <summary>
        /// Represents all available product infos for the current product selection
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> AvailableProductInfos
        {
            get
            {
                if (_availableProductInfosRO == null)
                {
                    _availableProductInfosRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_availableProductInfos);
                }
                return _availableProductInfosRO;
            }
        }

        /// <summary>
        /// Represents all available location infos for the entity
        /// </summary>
        public ReadOnlyCollection<LocationInfo> AvailableLocations
        {
            get { return _masterLocationsList; }
        }

        /// <summary>
        /// Returns the editable collection of location selected for display.
        /// </summary>
        public BulkObservableCollection<LocationInfo> SelectedLocations
        {
            get { return _selectedLocations; }
        }

        /// <summary>
        /// Represents whether grid should be auto filtered by existing links
        /// </summary>
        public Boolean FilterLegalLinks
        {
            get { return _filterLegalLinks; }
            set
            {
                _filterLegalLinks = value;
                OnPropertyChanged(FilterLegalLinksProperty);
            }
        }

        /// <summary>
        /// Represents the column set of the man data grid
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Represents all available Legal link rows for the current current product group
        /// </summary>
        public BulkObservableCollection<LocationProductLegalRowViewModel> AvailableLegalLinkRows
        {
            get { return _availableLegalLinkRows; }
        }


        #endregion

        #region Constructor

        public LocationProductLegalViewModel()
        {
            //Update permissions flags
            UpdatePermissionFlags();

            //master locations
            _masterLocationsList = new ReadOnlyCollection<LocationInfo>(LocationInfoList.FetchByEntityId(App.ViewState.EntityId));
            _selectedLocations.BulkCollectionChanged += SelectedLocations_BulkCollectionChanged;


            //financial hierarchy
            ProductHierarchy financialHierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);
            _availableProductGroups = new ReadOnlyCollection<ProductGroup>(financialHierarchy.EnumerateAllGroups().ToList());


            //LocationproductLegalListViewModel
            _masterLocationProductLegalListView = new LocationProductLegalListViewModel();
            _masterLocationProductLegalListView.ModelChanged += MasterLocationProductLegalListView_ModelChanged;
            _masterLocationProductLegalListView.FetchForCurrentEntity();



            //Turn AutoFilterLegalLinks on as default
            this.FilterLegalLinks = false;
        }

        #endregion

        #region Commands

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S,
                            DisabledReason = Message.Generic_Save_DisabledReasonInvalidData
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //user must have edit permission 
            if (!_userHasLocationProductLegalEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            #region Check LegalLinks
            if (_availableLegalLinkRows.Count > 0)
            {
                foreach (LocationProductLegalRowViewModel row in _availableLegalLinkRows)
                {
                    if (row.InitialLinks.Count != row.CurrentLinks.Count)
                    {
                        //If counts differ then something must have been added/removed
                        return true;
                    }
                    else
                    {
                        //if equal can still have been changed
                        foreach (Int16 id in row.CurrentLinks)
                        {
                            if (!row.InitialLinks.Contains(id))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            else
            {
                return false;
            }
            #endregion

            return false;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    //Add/Remove items
                    foreach (LocationProductLegalRowViewModel row in AvailableLegalLinkRows)
                    {
                        foreach (KeyValuePair<Int16, Boolean> pair in row.LocationLegalLinks)
                        {
                            LocationProductLegal link;
                            Boolean linkExists = _LegalLinksDictionary.TryGetValue(new Tuple<Int32, Int16>(row.ProductInfo.Id, pair.Key), out link);
                            if (!linkExists)
                            {
                                if (pair.Value == true)
                                {
                                    _masterLocationProductLegalListView.Model.Add(LocationProductLegal.NewLocationProductLegal(pair.Key, row.ProductInfo.Id, App.ViewState.EntityId));
                                }
                            }
                            else
                            {
                                if (pair.Value == false)
                                {
                                    _masterLocationProductLegalListView.Model.Remove(link);
                                }
                            }
                        }
                        row.ResetInitialLinks();
                    }

                    //Save Item
                    _masterLocationProductLegalListView.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exceptionCategory);
                    Exception rootException = ex.GetBaseException();

                    if (this.AttachedControl != null)
                    {
                        //just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            Message.LocationProductLegal_ItemsFriendlyName,
                            OperationType.Save,
                            this.AttachedControl);
                    }

                    base.ShowWaitCursor(true);
                }

                base.ShowWaitCursor(false);
            }

            return continueWithSave;

        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => SaveAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private Boolean SaveAndClose_CanExecute()
        {
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAndCloseCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }


            return true;
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(p => Open_Executed(p), p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        FriendlyDescription = Message.LocationProductLegal_Open_Description
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? id)
        {
            //must not be null
            if (id == null)
            {
                _openCommand.DisabledReason = Message.LocationProductLegal_NoGroupSelectedDisabledReason;
                return false;
            }

            //user must have permission
            if (!_userHasLocationProductLegalFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? id)
        {
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                this.SelectedProductGroup = _availableProductGroups.FirstOrDefault(p => p.Id == id.Value);

                base.ShowWaitCursor(false);

                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                //show the select locations window
                if (this.AttachedControl != null)
                {
                    this.ShowLocationsWindow();
                }
            }
        }

        #endregion

        #region AddLegalLinkCommand

        private RelayCommand _addLegalLinkCommand;
        /// <summary>
        /// Shows the AddLegalLink Window and handles the window result
        /// </summary>
        public RelayCommand AddLegalLinkCommand
        {
            get
            {
                if (_addLegalLinkCommand == null)
                {
                    _addLegalLinkCommand = new RelayCommand(p => AddLegalLink_Executed(), p => AddLegalLink_CanExecute())
                    {
                        FriendlyName = Message.LocationProductLegal_AddLegalLink,
                        FriendlyDescription = Message.LocationProductLegal_AddLegalLink_Description,
                        Icon = ImageResources.LocationProductLegal_AddLegalLink
                    };
                    this.ViewModelCommands.Add(_addLegalLinkCommand);
                }
                return _addLegalLinkCommand;
            }
        }

        private Boolean AddLegalLink_CanExecute()
        {
            return this.AvailableProductInfos.Count > 0;
        }

        private void AddLegalLink_Executed()
        {
            //show the dialog
            LocationProductLegalNewRelationship win = new LocationProductLegalNewRelationship(this.SelectedLocations,
                 _availableProductInfos, this.AvailableLegalLinkRows);

            App.ShowWindow(win, this.AttachedControl, /*IsModal*/true);
        }

        #endregion

        #region RemoveLegalLinkCommand

        private RelayCommand _removeLegalLinkCommand;
        /// <summary>
        /// Unchecks all selected CheckBox cells
        /// </summary>
        public RelayCommand RemoveLegalLinkCommand
        {
            get
            {
                if (_removeLegalLinkCommand == null)
                {
                    _removeLegalLinkCommand = new RelayCommand(p => RemoveLegalLink_Executed(), p => RemoveLegalLink_CanExecute())
                    {
                        FriendlyName = Message.LocationProductLegal_RemoveLegalLink,
                        FriendlyDescription = Message.LocationProductLegal_RemoveLegalLink_Description,
                        Icon = ImageResources.LocationProductLegal_RemoveLegalLink
                    };
                    this.ViewModelCommands.Add(_removeLegalLinkCommand);
                }
                return _removeLegalLinkCommand;
            }
        }

        private Boolean RemoveLegalLink_CanExecute()
        {
            if (this.AttachedControl != null)
            {
                return this.AttachedControl.LegalDataGrid.SelectedCells.Count > 0;
            }
            else
            {
                return false;
            }
        }

        private void RemoveLegalLink_Executed()
        {
            if (this.AttachedControl != null)
            {
                List<DataGridCellInfo> infoList = this.AttachedControl.LegalDataGrid.SelectedCells.ToList();
                for (Int16 i = 0; i < infoList.Count; i++)
                {
                    //Uncheck cells
                    if ((infoList[i].Column.GetCellContent(infoList[i].Item) as CheckBox) != null)
                    {
                        (infoList[i].Column.GetCellContent(infoList[i].Item) as CheckBox).IsChecked = false;
                    }
                }
            }
        }

        #endregion

        #region RemoveAllLegalLinksCommand

        private RelayCommand _removeAllLegalLinksCommand;
        /// <summary>
        /// Unchecks all CheckBox cells
        /// </summary>
        public RelayCommand RemoveAllLegalLinksCommand
        {
            get
            {
                if (_removeAllLegalLinksCommand == null)
                {
                    _removeAllLegalLinksCommand = new RelayCommand(p => RemoveAllLegalLinks_Executed(), p => RemoveAllLegalLinks_CanExecute())
                    {
                        FriendlyName = Message.LocationProductLegal_RemoveAllLegalLinks,
                        FriendlyDescription = Message.LocationProductLegal_RemoveAllLegalLinks_Description,
                        Icon = ImageResources.LocationProductLegal_RemoveAllLegalLinks
                    };
                    this.ViewModelCommands.Add(_removeAllLegalLinksCommand);
                }
                return _removeAllLegalLinksCommand;
            }
        }

        private Boolean RemoveAllLegalLinks_CanExecute()
        {
            return this.AvailableLegalLinkRows.Count > 0;
        }

        private void RemoveAllLegalLinks_Executed()
        {
            ShowWaitCursor(true);
            foreach (LocationProductLegalRowViewModel row in AvailableLegalLinkRows)
            {
                row.ClearLinks();
            }
            ShowWaitCursor(false);
        }

        #endregion

        #region AutoFilterExistingLinksCommand

        private RelayCommand _autoFilterExistingLinksCommand;
        /// <summary>
        /// Toggles between AutoFilter on and AutoFilter off, reloads columns on click to current filter type
        /// </summary>
        public RelayCommand AutoFilterExistingLinksCommand
        {
            get
            {
                if (_autoFilterExistingLinksCommand == null)
                {
                    _autoFilterExistingLinksCommand = new RelayCommand(p => AutoFilterExistingLinks_Executed())
                    {
                        FriendlyName = Message.LocationProductLegal_AutoFilterExistingLinks,
                        FriendlyDescription = Message.LocationProductLegal_AutoFilterExistingLinks_Description,
                        Icon = ImageResources.LocationProductLegal_AutoFilterExistingLinks
                    };
                    this.ViewModelCommands.Add(_autoFilterExistingLinksCommand);
                }
                return _autoFilterExistingLinksCommand;
            }
        }

        private void AutoFilterExistingLinks_Executed()
        {
            //Filter locations/products
            ReloadColumns();
        }

        #endregion

        #region SetSelectedLocationCommand

        private RelayCommand _setSelectedLocationsCommand;

        public RelayCommand SetSelectedLocationsCommand
        {
            get
            {
                if (_setSelectedLocationsCommand == null)
                {
                    _setSelectedLocationsCommand = new RelayCommand(
                        p => SetSelectedLocations_Executed(),
                        p => SetSelectedLocations_CanExecute())
                    {
                        FriendlyName = Message.LocationProductLegal_SetSelectedLocations,
                        FriendlyDescription = Message.LocationProductLegal_SetSelectedLocations_Desc,
                        Icon = ImageResources.LocationProductLegal_SetSelectedLocations
                    };
                    base.ViewModelCommands.Add(_setSelectedLocationsCommand);
                }
                return _setSelectedLocationsCommand;
            }
        }

        private Boolean SetSelectedLocations_CanExecute()
        {
            if (this.AvailableProductInfos.Count == 0)
            {
                this.SetSelectedLocationsCommand.DisabledReason = Message.LocationProductLegal_NoFilterSelected;
                return false;
            }

            return true;
        }

        private void SetSelectedLocations_Executed()
        {
            //Warn the user about change loss
            ModalMessage win = new ModalMessage();
            win.Header = Message.Generic_Warning;
            win.Description = String.Format(Message.LocationProductLegal_SelectLocations_Warning);
            win.MessageIcon = ImageResources.Warning_32;
            win.ButtonCount = 2;
            win.Button1Content = Message.Generic_Ok;
            win.Button2Content = Message.Generic_Cancel;
            win.DefaultButton = ModalMessageButton.Button1;
            win.CancelButton = ModalMessageButton.Button2;
            App.ShowWindow(win, /*isModal*/true);


            if (win.Result == ModalMessageResult.Button1)
            {
                ShowLocationsWindow();
            }
        }

        private void ShowLocationsWindow()
        {
            LocationHierarchyViewModel locationHierarchyViewModel = new LocationHierarchyViewModel();
            locationHierarchyViewModel.FetchForCurrentEntity();
            LocationSelectionWindow win = new LocationSelectionWindow(_masterLocationsList.ToObservableCollection(),
                   this.SelectedLocations.ToObservableCollection(), locationHierarchyViewModel.Model);
            App.ShowWindow(win, this.AttachedControl, true);

            if (win.DialogResult == true)
            {
                this.SelectedLocations.Clear();
                this.SelectedLocations.AddRange(win.ViewModel.TakenLocations.Select(p => p.Location).ToObservableCollection());
            }
        }
        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(), p => Close_CanExecute())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (SaveCommand.CanExecute())
                {
                    continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithItemChangeDialog("Location Product Legal Links", true, SaveCommand);
                }
            }
            return continueExecute;
        }

        /// <summary>
        /// Returns a list of groups that match the given criteria
        /// </summary>
        /// <param name="codeOrNameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<ProductGroup> GetMatchingProductGroups(String codeOrNameCriteria)
        {
            return this._availableProductGroups.Where(
                p => p.Name.ToUpperInvariant().Contains(codeOrNameCriteria) ||
                    p.Code.ToUpperInvariant().Contains(codeOrNameCriteria));
        }

        /// <summary>
        /// Reloads the columns in the datagrid, takes filtering into account
        /// </summary>
        public void ReloadColumns()
        {
            this.ShowWaitCursor(true);

            //Clear existing columns
            _columnSet.Clear();

            #region Product Column
            //Add product column
            DataGridTextColumn productColumn =
                ExtendedDataGrid.CreateReadOnlyTextColumn(
                Message.LocationProductLegal_DataGrid_Products,
                "ProductInfo.FullName", HorizontalAlignment.Left);

            productColumn.Width = new DataGridLength(270);

            //Add column to set
            _columnSet.Add(productColumn);

            #endregion

            #region Location Columns
            const Double locColWidth = 80;


            DataTemplate headerTemplate = this.AttachedControl.Resources["LocationProductLegal_DTLocationColumnHeader"] as DataTemplate;

            if (this.FilterLegalLinks)
            {
                if (this.AvailableLegalLinkRows.Count > 0)
                {
                    //Update the Legal links for the selected locations as 
                    //they are no longer all loaded up front.
                    foreach (var row in this.AvailableLegalLinkRows)
                    {
                        row.PopulateDictionary(this.SelectedLocations.ToList(), _LegalLinksDictionary);
                    }


                    foreach (LocationInfo location in this.SelectedLocations)
                    {
                        Boolean hasLink = this.AvailableLegalLinkRows.Max(p => p.LocationLegalLinks[location.Id]);

                        if (hasLink)
                        {
                            DataGridExtendedCheckBoxColumn col =
                                ExtendedDataGrid.CreateCheckBoxColumn(location.ToString(),
                                String.Format(CultureInfo.InvariantCulture, "LocationLegalLinks[{0}]", location.Id),
                                /*isReadOnly*/false);

                            col.Width = new DataGridLength(locColWidth);
                            col.HeaderTemplate = headerTemplate;

                            _columnSet.Add(col);
                        }
                    }
                }
            }
            else
            {
                //Update the Legal links for the selected locations as
                //they are no longer all loaded up front.
                foreach (var row in this.AvailableLegalLinkRows)
                {
                    row.PopulateDictionary(this.SelectedLocations.ToList(), _LegalLinksDictionary);
                }

                //Load column for every location associated with the current entity
                foreach (LocationInfo location in this.SelectedLocations)
                {
                    DataGridExtendedCheckBoxColumn col =
                            ExtendedDataGrid.CreateCheckBoxColumn(location.ToString(),
                            String.Format(CultureInfo.InvariantCulture, "LocationLegalLinks[{0}]", location.Id),
                        /*isReadOnly*/false);

                    col.Width = new DataGridLength(locColWidth);
                    col.HeaderTemplate = headerTemplate;

                    _columnSet.Add(col);

                }

            }
            #endregion
            this.ShowWaitCursor(false);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected product group
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedProductGroupChanged(ProductGroup newValue)
        {
            _availableProductInfos.Clear();
            if (_availableLegalLinkRows.Count > 0)
            {
                _availableLegalLinkRows.Clear();
            }
            if (newValue != null)
            {
                //Prevent change events occuring as it will exponentially increase the load
                //time as each new row will call the datatable to refresh.
                _availableLegalLinkRows.RaiseListChangedEvents = false;

                //fetch and add products for the selected group          
                //and populate AvailableLegalLinkRows
                foreach (ProductInfo product in ProductInfoList.FetchByMerchandisingGroupId(newValue.Id).OrderBy(p => p.Gtin))
                {
                    _availableProductInfos.Add(product);

                    //Only use the selected location list when loading to cut down on load times.
                    _availableLegalLinkRows.Add(new LocationProductLegalRowViewModel(product,
                      this.SelectedLocations.ToList(), _LegalLinksDictionary));
                }

                //we are now ok to let changed events occur again
                _availableLegalLinkRows.RaiseListChangedEvents = true;

            }


            if (this.AttachedControl != null)
            {
                //Send of a reset to the Legallink rows to update the datatable
                //use a dispatcher so that the screen will switch immediately to the
                //product screen.
                this.AttachedControl.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        _availableLegalLinkRows.Reset();
                    }),
                    priority: System.Windows.Threading.DispatcherPriority.ContextIdle);
            }
        }

        /// <summary>
        /// Responds to changes to the selected locations collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLocations_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //reload the columns collection
            ReloadColumns();
        }

        private void MasterLocationProductLegalListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<LocationProductLegalList> e)
        {
            _LegalLinksDictionary.Clear();
            foreach (LocationProductLegal link in e.NewModel)
            {
                _LegalLinksDictionary.Add(new Tuple<Int32, Int16>(link.ProductId, link.LocationId), link);
            }
        }
        #endregion

        #region Screen Permissions

        private Boolean _userHasLocationProductLegalFetchPerm;
        private Boolean _userHasLocationProductLegalEditPerm;

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //LocationProductLegal permissions
            //nb - we fetch/save the list object so perms are against that.

            //fetch
            _userHasLocationProductLegalFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(LocationProductLegalList));

            //edit
            _userHasLocationProductLegalEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(LocationProductLegalList));
        }

        /// <summary>
        /// Returns true if the current user has the permissions required to simply open this screen.
        /// </summary>
        /// <returns></returns>
        internal static Boolean UserHasRequiredAccessPerms()
        {
            return

                //LocationProductLegalList
                Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(LocationProductLegalList))

                //Location
                && Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(Location));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedLocations.BulkCollectionChanged -= SelectedLocations_BulkCollectionChanged;
                    _masterLocationProductLegalListView.ModelChanged -= MasterLocationProductLegalListView_ModelChanged;

                    //Remove collection values so they get disposed
                    _availableProductInfos.Clear();
                    _LegalLinksDictionary.Clear();
                    _availableLegalLinkRows.Clear();
                    _masterLocationProductLegalListView = null;


                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
