﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
// V8-32245 : N.Haywood
//  Changed from location to location info
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductLegalNewRelationship.xaml
    /// </summary>
    public sealed partial class LocationProductLegalNewRelationship : ExtendedRibbonWindow
    {
        #region Properties

        #region View Model Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationProductLegalNewRelationshipViewModel),
            typeof(LocationProductLegalNewRelationship),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationProductLegalNewRelationshipViewModel ViewModel
        {
            get { return (LocationProductLegalNewRelationshipViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationProductLegalNewRelationship senderControl = (LocationProductLegalNewRelationship)obj;

            if (e.OldValue != null)
            {
                LocationProductLegalNewRelationshipViewModel oldModel = (LocationProductLegalNewRelationshipViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationProductLegalNewRelationshipViewModel newModel = (LocationProductLegalNewRelationshipViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationProductLegalNewRelationship(IEnumerable<LocationInfo> availableLocations,
            IEnumerable<ProductInfo> availableProducts,
            IEnumerable<LocationProductLegalRowViewModel> masterLinksCollection)
        {
            InitializeComponent();

            this.ViewModel = new LocationProductLegalNewRelationshipViewModel(
                availableLocations, availableProducts, masterLinksCollection);
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Calls the viewmodel's event handler for filtering locations based on product selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.ViewModel.OnProductsGrid_SelectedChanged(sender, e);
        }

        #endregion
    }
}
