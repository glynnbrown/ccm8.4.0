﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
// V8-32245 : N.Haywood
//  Changed from location to location info
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance
{
    public class LocationProductLegalRowViewModel : ViewModelObject
    {
        #region Fields
        private ProductInfo _productInfo;
        private Dictionary<Int16, Boolean> _locationLegalLinks;
        private List<Int16> _initialLinks = new List<Int16>();
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductInfoProperty = WpfHelper.GetPropertyPath<LocationProductLegalRowViewModel>(p => p.ProductInfo);
        public static readonly PropertyPath LocationLegalLinksProperty = WpfHelper.GetPropertyPath<LocationProductLegalRowViewModel>(p => p.LocationLegalLinks);
        public static readonly PropertyPath InitialLinksProperty = WpfHelper.GetPropertyPath<LocationProductLegalRowViewModel>(p => p.InitialLinks);
        public static readonly PropertyPath CurrentLinksProperty = WpfHelper.GetPropertyPath<LocationProductLegalRowViewModel>(p => p.CurrentLinks);

        #endregion

        #region Properties

        public ProductInfo ProductInfo
        {
            get { return _productInfo; }
        }

        public Dictionary<Int16, Boolean> LocationLegalLinks
        {
            get
            {
                return _locationLegalLinks;
            }
        }

        public List<Int16> InitialLinks
        {
            get { return _initialLinks; }
        }

        public List<Int16> CurrentLinks
        {
            get { return _locationLegalLinks.Where(p => p.Value == true).Select(p => p.Key).ToList(); }
        }

        #endregion

        #region Constructor

        public LocationProductLegalRowViewModel(ProductInfo productInfo, List<LocationInfo> locationInfoList,
            Dictionary<Tuple<Int32, Int16>, LocationProductLegal> linkDictionary)
        {
            _productInfo = productInfo;
            PopulateDictionary(locationInfoList, linkDictionary);
        }

        #endregion

        #region Methods

        public void PopulateDictionary(List<LocationInfo> locationInfoList,
            Dictionary<Tuple<Int32, Int16>, LocationProductLegal> linkDictionary)
        {
            Dictionary<Int16, Boolean> locationLegalDict = new Dictionary<Int16, Boolean>();
            _initialLinks.Clear();
            foreach (LocationInfo info in locationInfoList)
            {
                //Find if Legal connection exists
                LocationProductLegal output;
                Boolean exists = linkDictionary.TryGetValue(new Tuple<Int32, Int16>(_productInfo.Id, info.Id), out output);
                //Add Legal connection to dictionary
                locationLegalDict.Add(info.Id, exists);
                if (exists)
                {
                    _initialLinks.Add(info.Id);
                }
            }
            _locationLegalLinks = locationLegalDict;
        }

        public void EditDictionaryValue(Int16 key, Boolean value)
        {
            LocationLegalLinks[key] = value;
            OnPropertyChanged(LocationLegalLinksProperty);
        }

        public void ClearLinks()
        {
            while (!LocationLegalLinks.All(p => p.Value == false))
            {
                Int16 key = LocationLegalLinks.FirstOrDefault(p => p.Value != false).Key;
                LocationLegalLinks[key] = false;
            }
            OnPropertyChanged(LocationLegalLinksProperty);
        }

        public void ResetInitialLinks()
        {
            _initialLinks = _locationLegalLinks.Where(p => p.Value == true).Select(p => p.Key).ToList();
            OnPropertyChanged(InitialLinksProperty);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
