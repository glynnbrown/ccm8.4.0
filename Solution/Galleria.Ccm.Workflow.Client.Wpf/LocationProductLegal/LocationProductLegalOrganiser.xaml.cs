﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using System.Text.RegularExpressions;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductLegalMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductLegalOrganiser.xaml
    /// </summary>
    public partial class LocationProductLegalOrganiser : ExtendedRibbonWindow
    {
        #region Constants

        const String AddLegalLinkContextCommandKey = "AddLegalLinksContextCommand";
        const String RemoveLegalLinkContextCommandKey = "RemoveLegalLinksCommand";

        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationProductLegalViewModel), typeof(LocationProductLegalOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationProductLegalViewModel ViewModel
        {
            get { return (LocationProductLegalViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationProductLegalOrganiser senderControl = (LocationProductLegalOrganiser)obj;

            if (e.OldValue != null)
            {
                LocationProductLegalViewModel oldModel = (LocationProductLegalViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(AddLegalLinkContextCommandKey);
                senderControl.Resources.Remove(RemoveLegalLinkContextCommandKey);
            }

            if (e.NewValue != null)
            {
                LocationProductLegalViewModel newModel = (LocationProductLegalViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(AddLegalLinkContextCommandKey, senderControl.AddLegalLinksContextCommand);
                senderControl.Resources.Add(RemoveLegalLinkContextCommandKey, senderControl.RemoveLegalLinksCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationProductLegalOrganiser()
        {
            //show busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = new LocationProductLegalViewModel();
            LocalHelper.SetRibbonBackstageState(this.ViewModel.AttachedControl, true);

            //Add helpfile link
            //HelpFileKeys.SetHelpFile(this, HelpFileKeys.LocationProductLegalMaintenance);todo

            this.Loaded += new RoutedEventHandler(LocationProductLegalOrganiser_Loaded);
        }

        void LocationProductLegalOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationProductLegalOrganiser_Loaded;
            this.ViewModel.ReloadColumns();

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Commands

        #region AddLegalLinksContextCommand

        private RelayCommand _addLegalLinksContextCommand;
        /// <summary>
        /// Add Legal links command to be accessed from the right click context menu on the
        /// data grid, checks all selected CheckBox cells
        /// </summary>
        public RelayCommand AddLegalLinksContextCommand
        {
            get
            {
                if (_addLegalLinksContextCommand == null)
                {
                    _addLegalLinksContextCommand = new RelayCommand(p => AddLegalLinksContext_Executed(), p => AddLegalLinksContext_CanExecute())
                    {
                        FriendlyName = Message.LocationProductLegal_AddLegalLinksContext,
                    };
                }
                return _addLegalLinksContextCommand;
            }
        }

        private Boolean AddLegalLinksContext_CanExecute()
        {
            if (this.LegalDataGrid.SelectedCells.Count > 0)
            {
                foreach (DataGridCellInfo cell in LegalDataGrid.SelectedCells)
                {
                    if ((cell.Column.GetCellContent(cell.Item) as CheckBox) != null)
                    {
                        //if one of the selected cells is a checkbox then return true
                        return true;
                    }
                }
            }
            return false;
        }

        private void AddLegalLinksContext_Executed()
        {
            foreach (DataGridCellInfo cell in LegalDataGrid.SelectedCells)
            {
                DataGridExtendedCheckBoxColumn checkBoxCol = cell.Column as DataGridExtendedCheckBoxColumn;
                if (checkBoxCol == null) continue;

                String colPath = ExtendedDataGrid.GetBindingProperty(checkBoxCol);

                if (!colPath.StartsWith("LocationLegalLinks[")) continue;
                
                Int16 locId;
                if(!Int16.TryParse(colPath.Substring(19, colPath.Length - 20), out locId)) continue;


                LocationProductLegalRowViewModel rowItem = cell.Item as LocationProductLegalRowViewModel;
                if (rowItem == null) continue;

                //set to checked
                rowItem.EditDictionaryValue(locId, true);
            }
        }

        #endregion

        #region RemoveLegalLinksCommand

        private RelayCommand _removeLegalLinksCommand;
        /// <summary>
        /// Unchecks all selected CheckBox cells
        /// </summary>
        public RelayCommand RemoveLegalLinksCommand
        {
            get
            {
                if (_removeLegalLinksCommand == null)
                {
                    _removeLegalLinksCommand = new RelayCommand(p => RemoveLegalLinks_Executed(), p => RemoveLegalLinks_CanExecute())
                    {
                        FriendlyName = Message.LocationProductLegal_DataGrid_Uncheck,
                        FriendlyDescription = Message.LocationProductLegal_RemoveLegalLink_Description
                    };
                }
                return _removeLegalLinksCommand;
            }
        }

        private Boolean RemoveLegalLinks_CanExecute()
        {
            if (this.LegalDataGrid.SelectedCells.Count > 0)
            {
                foreach (DataGridCellInfo cell in LegalDataGrid.SelectedCells)
                {
                    CheckBox cellCheckBox = (cell.Column.GetCellContent(cell.Item) as CheckBox);
                    if (cellCheckBox != null)
                    {
                        if (cellCheckBox.IsChecked == true)
                        {
                            //if one of the selected cells is a checked checkbox cell then return true
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void RemoveLegalLinks_Executed()
        {
            foreach (DataGridCellInfo cell in LegalDataGrid.SelectedCells)
            {
                DataGridExtendedCheckBoxColumn checkBoxCol = cell.Column as DataGridExtendedCheckBoxColumn;
                if (checkBoxCol == null) continue;

                String colPath = ExtendedDataGrid.GetBindingProperty(checkBoxCol);

                if (!colPath.StartsWith("LocationLegalLinks[")) continue;

                Int16 locId;
                if (!Int16.TryParse(colPath.Substring(19, colPath.Length - 20), out locId)) continue;


                LocationProductLegalRowViewModel rowItem = cell.Item as LocationProductLegalRowViewModel;
                if (rowItem == null) continue;

                //set to checked
                rowItem.EditDictionaryValue(locId, false);
            }
        }

        #endregion

        #endregion

        #region Methods

        #endregion

        #region Window close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if needs saving
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
