﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25438 : L.Hodson ~ Created.
// V8-25462 : A.Silva ~ Copied over from Editor.Client.
// V8-25664 : Added event handlers for drag and drop operations and a context menu for side panel.
// V8-26284 : A.Kuszyk
//  Amended to use Common.Wpf selector.
// V8-26822 : L.Ineson
//  Improvements to search
// V8-26941 : A.Silva ~ Implemented Column Manager properly.
// V8-27146 : A.Silva ~ Added Group Validation column to the end of the Column Set collection.
// V8-27058 : A.Probyn ~ Removed reference to MetaTotalPositionCount
// V8-27196 : A.Silva ~ Amended Field Registration for Custom Columns Layout Manager to register only numeric fields.
// V8-27411 : M.Pettit ~ Added AutomationStatus, Plan Status cols, renamed ValidationGroups column
// V8-27621 : A.Silva
//      Amended column totals not showing.
// V8-27411 : M.Pettit ~ Added Planogram Owner (displaying Planogram_UserName property)
// V8-27966 : A.Silva
//      Removed Refresh Planograms from the planogram list context menu.
//      Added cut, copy and paste to the planogram list context menu.
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
// V8-28083  : A.Probyn
//  Added a SortMemberPath to the template columns on creation. Without this, a filter on another column + sort on a template column
//  causes it to go horribly wrong and fall over.
// V8-28250 : L.ineson
//  Moved plan info panel into common
// V8-28093 : L.ineson
//  Made sure that info and name plan columns are always displayed.
// V8-28333 : A.Silva
//      Moved the datagrid context menu from showing only for rows, to showing also over white space.
//      Corrected issue when right clicking would not select the row, but only open the context menu.
// V8-28273 : A.Probyn
//    Corrected so columns are all in visible/frozen options on grid context menu.
#endregion

#region Version History: (CCM 801)
// V8-28701 : J.Pickup
//    UnlockSelected added to xPlanogramsGrid conext menu
#endregion

#region Version History: (CCM 810)
// V8-28242 : M.Shelley
//  Added an editor window for the planogram properties
#endregion

#region Version History: (CCM 810)
// V8-30283 : M.Shelley
//  Only load the grid column set when the window is instanciated, not each time it is loaded.
//  Otherwise the users' column settings get over ridden and any values entered in the column filter 
//  text boxes are not displayed when the user switches back to the tab - even though the filter
//  is still active
#endregion

#region Version History: CCM 8.1.1
// V8-30225 : M.Shelley
//  Added the Planogram UCR to the plan repository grid
// V8-30278 : L.Ineson
//  Amended Plan status column so that it can be sorted & filtered
// V8-30058 : M.Shelley
//  Added a tooltip for the context menu "Properties" and "Delete" items
// V8-30455 : D.Pleasance
//  Amended Plan Status column to use ConverterEnumValueToFriendlyName
#endregion

#region Version History : CCM820
// V8-30527 : A.Kuszyk
//  Ensured planogram grid context menu items only have a tooltip if there is some text to put in it.
//V8-30958 : L.Ineson
//  Updated after column manager changes.
// V8-31266 : A.Probyn
//  Updated column manager to exclude UCR as we force it in this grid and this removes duplicates.
#endregion

#region Version History : CCM830
// V8-31832 : L.Ineson
//  Changes to support calculated columns.
// V8-32191 : A.Probyn
//  Updated to override OnPreviewKeyDown for copy and paste.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Shapes;
using Fluent;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using MenuItem = Fluent.MenuItem;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanRepository
{
    /// <summary>
    /// Interaction logic for PlanRepositoryOrganiser.xaml
    /// </summary>
    public sealed partial class PlanRepositoryOrganiser
    {
        #region Fields

        private readonly List<RibbonTabItem> _ribbonTabList;
        private readonly PlanRepositorySidePanel _sidePanel;
        private GridLength _bottomPanelHeight = new GridLength(420.0);

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanRepositoryViewModel), typeof(PlanRepositoryOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanRepositoryOrganiser)obj;

            // Detach previous viemodel if there was one.
            if (e.OldValue != null)
            {
                PlanRepositoryViewModel oldModel = e.OldValue as PlanRepositoryViewModel;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                PlanRepositoryViewModel newModel = e.NewValue as PlanRepositoryViewModel;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;


            }
            senderControl.UpdateBreadcrumb();
        }


        /// <summary>
        ///     Gets or sets the viewmodel controller for <see cref="PlanRepositoryOrganiser"/>.
        /// </summary>
        public PlanRepositoryViewModel ViewModel
        {
            get { return (PlanRepositoryViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="PlanRepositoryOrganiser"/> class.
        /// </summary>
        public PlanRepositoryOrganiser()
        {
            InitializeComponent();
            ViewModel = new PlanRepositoryViewModel();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanRepository);

            //create the ribbon
            var homeTab = new PlanRepositoryHomeTab();
            BindingOperations.SetBinding(homeTab, PlanRepositoryHomeTab.ViewModelProperty, new Binding(ViewModelProperty.Name) { Source = this });


            _ribbonTabList = new List<RibbonTabItem> { homeTab };

            //create sidepanel
            _sidePanel = new PlanRepositorySidePanel();
            BindingOperations.SetBinding(_sidePanel, PlanRepositorySidePanel.ViewModelProperty, new Binding(ViewModelProperty.Name) { Source = this });

            //pass through command bindings from sidepanel to home tab.
            BindingOperations.SetBinding(homeTab, PlanRepositoryHomeTab.AddToFavouriesCommandProperty,
                new Binding(PlanRepositorySidePanel.AddToFavouriesCommandProperty.Name) { Source = _sidePanel });

            BindingOperations.SetBinding(homeTab, PlanRepositoryHomeTab.RemoveFromFavouriesCommandProperty,
                new Binding(PlanRepositorySidePanel.RemoveFromFavouriesCommandProperty.Name) { Source = _sidePanel });

            Loaded += PlanRepositoryOrganiser_FirstLoaded;
            Loaded += PlanRepositoryOrganiser_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        private void PlanRepositoryOrganiser_FirstLoaded(Object sender, RoutedEventArgs e)
        {
            Loaded -= PlanRepositoryOrganiser_FirstLoaded;

            var factory =
            new PlanogramInfoColumnLayoutFactory(typeof(PlanogramRepositoryRow)
                    , new List<Framework.Model.IModelPropertyInfo>()
                    {
                        PlanogramInfo.UniqueContentReferenceProperty,
                        PlanogramInfo.StatusProperty
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                PlanRepositoryViewModel.ScreenKey, /*PathMask*/"Info.{0}");

            _columnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
            _columnLayoutManager.RegisterDataTemplate<PlanogramRepositoryRow>(PlanogramRepositoryRow.CreateDataTemplate);
            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(xPlanogramGrid);
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Carries out actions whenever this screen loads.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanRepositoryOrganiser_Loaded(Object sender, RoutedEventArgs e)
        {
            //send ribbon request
            App.ViewState.ShowRibbonTabs(_ribbonTabList);

            //send sidepanel request
            App.ViewState.ShowSidePanel(_sidePanel);
        }

        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

           

            //Info column group
            columnSet.Insert(curIdx++, new DataGridExtendedTemplateColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_InformationColumnHeader,
                CellTemplate = Resources["PlanRepositoryInfoColumnDataTemplate"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = "",
                MinWidth = 50,
                IsReadOnly = true,
                SortMemberPath = "LockedToolTip",
                ColumnCellAlignment = HorizontalAlignment.Stretch
            });

            //Planogram Name column
            DataGridExtendedTextColumn nameGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramNameColumnHeader,
                Binding = new Binding("Info.Name"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = "",
                MinWidth = 50,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(curIdx++, nameGridCol);

            // Planogram UCR column
            DataGridExtendedTextColumn ucrGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramUCRColumnHeader,
                Binding = new Binding("Info.UniqueContentReference"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = "",
                MinWidth = 60,
                Width = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(curIdx++, ucrGridCol);

            //Create the header group
            DataGridHeaderGroup statusHeaderGroup = new DataGridHeaderGroup();
            statusHeaderGroup.HeaderName = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_StatusGroupHeader;

            //Plan Status column
            DataGridExtendedTextColumn planStatusCol = new DataGridExtendedTextColumn()
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanStatusColumnHeader,
                Binding = new Binding("Info.Status")
                {
                    Mode = BindingMode.OneWay,
                    Converter = Application.Current.Resources[ResourceKeys.ConverterEnumValueToFriendlyName] as IValueConverter
                },
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                IsReadOnly = true,
                MinWidth = 50,
                ColumnGroupName = statusHeaderGroup.HeaderName,
            };
            columnSet.Insert(curIdx++, planStatusCol);

            //Validation column
            DataGridExtendedTemplateColumn validationStatusCol = new DataGridExtendedTemplateColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_ValidationColumnHeader,
                CellTemplate = Resources["PlanRepositoryValidationColumnDataTemplate"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                MinWidth = 50,
                IsReadOnly = true,
                SortMemberPath = "ValidationProcessingVisibility",
                ColumnGroupName = statusHeaderGroup.HeaderName,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(curIdx++, validationStatusCol);

        }

        /// <summary>
        ///     Handles the change of selection for the current <see cref="PlanogramInfo" />.
        /// </summary>
        private void ViewModel_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanRepositoryViewModel.SelectedPlanogramGroupProperty.Path)
            {
                UpdateBreadcrumb();
            }
        }

        #region XPlanogramGrid handlers


        /// <summary>
        /// Called whenever a planogram is double clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XPlanogramGrid_OnRowItemMouseDoubleClick(Object sender, ExtendedDataGridItemEventArgs e)
        {
            ViewModel.OpenPlanogramCommand.Execute();
        }

        /// <summary>
        /// Called when the enter key is pressed on the grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XPlanogramGrid_OnKeyDown(Object sender, KeyEventArgs e)
        {
            if (ViewModel.SelectedPlanogramRows != null &&
                ViewModel.SelectedPlanogramRows.Count > 0 &&
                 e.Key == Key.Enter)
            {
                ViewModel.OpenPlanogramCommand.Execute();
                e.Handled = true;
            }

            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(xBottomExpander);
            }

            base.OnKeyDown(e);
        }

        private void XPlanogramGrid_OnContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (ViewModel == null) { return; }

            //show the row context menu
            var contextMenu = new Fluent.ContextMenu();

            contextMenu.Items.Add(NewMenuItem(ViewModel.OpenPlanogramCommand));

            // Check if a planogram can be unlocked
            bool canUnlockPlans = ViewModel.UnlockSelectedPlanogramCommand.CanExecute();

            // Check if there are any locked plans selected
            // If so, add the "Unlock" context menu item
            contextMenu.Items.Add(new Separator());
            if (canUnlockPlans)
            {
                contextMenu.Items.Add(NewMenuItem(ViewModel.UnlockSelectedPlanogramCommand));
            }

            // Always add the "Properties" context menu item but if any of the selected plans 
            // are locked disable the menu item (done in the CanExecute method)
            var propMenuitem = NewMenuItem(ViewModel.PlanogramPropertiesCommand);
            if (!String.IsNullOrEmpty(ViewModel.PlanogramPropertiesCommand.DisabledReason))
            {
                propMenuitem.ToolTip = ViewModel.PlanogramPropertiesCommand.DisabledReason;
            }
            contextMenu.Items.Add(propMenuitem);


            contextMenu.Items.Add(NewMenuItem(ViewModel.RenamePlanogramCommand));


            contextMenu.Items.Add(new Separator());
            contextMenu.Items.Add(NewMenuItem(ViewModel.CutPlanogramCommand));
            contextMenu.Items.Add(NewMenuItem(ViewModel.CopyPlanogramCommand));
            contextMenu.Items.Add(NewMenuItem(ViewModel.PastePlanogramCommand));
            contextMenu.Items.Add(new Separator());

            var deleteMenuItem = NewMenuItem(ViewModel.DeletePlanogramCommand);
            if (!String.IsNullOrEmpty(ViewModel.DeletePlanogramCommand.DisabledReason))
            {
                deleteMenuItem.ToolTip = ViewModel.DeletePlanogramCommand.DisabledReason;
            }
            contextMenu.Items.Add(deleteMenuItem);

            contextMenu.Placement = PlacementMode.Mouse;
            contextMenu.IsOpen = true;
        }

        #endregion

        #region Panel handlers

        private void XBottomPanelExpander_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetRow((Expander)sender);
            var row = xBaseGrid.RowDefinitions[index];
            _bottomPanelHeight = row.Height;
            row.Height = GridLength.Auto;
        }

        private void XBottomExpander_OnExpanded(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetRow((Expander)sender);
            var row = xBaseGrid.RowDefinitions[index];
            row.Height = _bottomPanelHeight;
        }

        #endregion

        /// <summary>
        /// Called whenever a breadcrumb is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Breadcrumb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel == null) return;

            TextBlock breadcrumb = (TextBlock)sender;
            PlanogramGroup group = breadcrumb.Tag as PlanogramGroup;
            if (group != null)
            {
                this.ViewModel.SelectedPlanogramGroup = group;
            }
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnPreviewKeyDown(e);

            //If Ctrl+C selected, copy planogram
            if (e.Key == Key.C && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.CopyPlanogramCommand.CanExecute())
                {
                    this.ViewModel.CopyPlanogramCommand.Execute();
                }
            }
            else if (e.Key == Key.X && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.CutPlanogramCommand.CanExecute())
                {
                    this.ViewModel.CutPlanogramCommand.Execute();
                }
            }
            else if (e.Key == Key.V && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.PastePlanogramCommand.CanExecute())
                {
                    this.ViewModel.PastePlanogramCommand.Execute();
                }
            }
        }

        /// <summary>
        /// Expand and collapse Expander on return key press for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xBottomExpander_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (xBottomExpander.IsExpanded)
                {
                    xBottomExpander.IsExpanded = false;
                }
                else
                {
                    xBottomExpander.IsExpanded = true;
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the breadcurb area
        /// </summary>
        private void UpdateBreadcrumb()
        {
            if (this.BreadcrumbStackPanel == null) return;

            //clear out any existing items.
            foreach (TextBlock tb in this.BreadcrumbStackPanel.Children.OfType<TextBlock>())
            {
                tb.MouseLeftButtonDown -= Breadcrumb_MouseLeftButtonDown;
            }
            this.BreadcrumbStackPanel.Children.Clear();

            //load in the path for the selected plan group
            if (this.ViewModel != null && this.ViewModel.SelectedPlanogramGroup != null)
            {
                //add in the icon
                var icon = new System.Windows.Controls.Image()
                {
                    Height = 15,
                    Width = 15,
                    Source = ImageResources.TreeViewItem_OpenFolder,
                    Margin = new Thickness(1, 1, 4, 1),
                    VerticalAlignment = System.Windows.VerticalAlignment.Center
                };
                this.BreadcrumbStackPanel.Children.Add(icon);

                Style itemStyle = (Style)this.Resources["PlanRepository_StyBreadcrumbItem"];
                Style sepStyle = (Style)this.Resources["PlanRepository_StyBreadcrumbSeparator"];

                //load in the breadcrumb parts
                foreach (var group in this.ViewModel.SelectedPlanogramGroup.FetchFullPath())
                {
                    TextBlock breadcrumb = new TextBlock();
                    breadcrumb.Style = itemStyle;
                    breadcrumb.Text = group.Name;
                    breadcrumb.Tag = group;
                    breadcrumb.MouseLeftButtonDown += Breadcrumb_MouseLeftButtonDown;
                    this.BreadcrumbStackPanel.Children.Add(breadcrumb);

                    Path separator = new Path();
                    separator.Style = sepStyle;
                    this.BreadcrumbStackPanel.Children.Add(separator);
                }
            }

        }


        #endregion

        #region Static helper Methods
        
        /// <summary>
        ///     Creates a new instance of <see cref="Fluent.MenuItem"/> from the provided <see cref="IRelayCommand"/> object.
        /// </summary>
        /// <param name="command">The command from which to generate the menu item.</param>
        /// <returns>A new instance of <see cref="Fluent.MenuItem"/>.</returns>
        private static MenuItem NewMenuItem(IRelayCommand command)
        {
            return new MenuItem
            {
                Command = command,
                Header = command.FriendlyName,
                Icon = command.SmallIcon ?? command.Icon,
            };
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (_isDisposed) return;

            Loaded -= PlanRepositoryOrganiser_Loaded;

            ViewModel.Dispose();
            ViewModel = null;

            if (_columnLayoutManager != null)
            {
                _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.Dispose();
            }

            _isDisposed = true;
        }

        #endregion
    }
}
