﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25462: A.Silva ~ Copied over from Editor.Client.

#endregion

#endregion

using System;
using System.Windows;
using Fluent;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanRepository
{
    /// <summary>
    ///     Interaction logic for PlanRepositoryHomeTab.xaml
    /// </summary>
    public partial class PlanRepositoryHomeTab
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(PlanRepositoryViewModel), typeof(PlanRepositoryHomeTab),
            new PropertyMetadata(default(PlanRepositoryViewModel)));

        public PlanRepositoryViewModel ViewModel
        {
            get { return (PlanRepositoryViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region AddToFavouriesCommandProperty

        public static readonly DependencyProperty AddToFavouriesCommandProperty =
            DependencyProperty.Register("AddToFavouritesCommand", typeof(RelayCommand), typeof(PlanRepositoryHomeTab),
            new PropertyMetadata(null));

        public RelayCommand AddToFavouritesCommand
        {
            get { return (RelayCommand)GetValue(AddToFavouriesCommandProperty); }
            set { SetValue(AddToFavouriesCommandProperty, value); }
        }

        #endregion

        #region RemoveFromFavouriesCommandProperty

        public static readonly DependencyProperty RemoveFromFavouriesCommandProperty =
            DependencyProperty.Register("RemoveFromFavouritesCommand", typeof(RelayCommand), typeof(PlanRepositoryHomeTab),
            new PropertyMetadata(null));

        public RelayCommand RemoveFromFavouritesCommand
        {
            get { return (RelayCommand)GetValue(RemoveFromFavouriesCommandProperty); }
            set { SetValue(RemoveFromFavouriesCommandProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanRepositoryHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}