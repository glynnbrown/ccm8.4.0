﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25462 : A.Silva ~ Copied over from Editor.Client.
// V8-26284 : A.Kuszyk
//  Amended to use Common.Wpf selector.
// V8-27966 : A.Silva
//      Added context menu code for the hierarchy selector.

#endregion

#region Version History : CCM820
// V8-30527 : A.Kuszyk
//  Added tooltip to Delete Group menu item in context menu.
// V8-31003 : A.Probyn
//  XSelector_OnNodeContextMenuShowing had been updated to clear the items in the context menu passed in. Duh.
#endregion

#region Version History : CCM830
// V8-32191 : A.Probyn
//  Updated to override OnKeyDown for copy and paste.
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.ViewModel;
using ContextMenu = Fluent.ContextMenu;
using MenuItem = Fluent.MenuItem;
using System.Windows.Input;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanRepository
{
    /// <summary>
    ///     Interaction logic for PlanRepositorySidePanel.xaml
    /// </summary>
    public partial class PlanRepositorySidePanel
    {
        #region Properties

        #region ViewModel Property
        
        /// <summary>
        ///     Viewmodel controller for <see cref="PlanRepositorySidePanel"/>.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof (PlanRepositoryViewModel), typeof (PlanRepositorySidePanel),
            new PropertyMetadata(null/*, OnViewModelPropertyChanged*/));

        /// <summary>
        ///     Gets and sets the viewmodel controller for the <see cref="PlanRepositorySidePanel"/>.
        /// </summary>
        public PlanRepositoryViewModel ViewModel
        {
            get { return (PlanRepositoryViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        /// <summary>
        ///     ViewModel for the Planogram Hierarchy Selector.
        /// </summary>
        private PlanogramHierarchySelectorViewModel SelectorViewModel
        {
            get { return xSelector.ViewModel; }
        }

        #region AddToFavouriesCommandProperty

        public static readonly DependencyProperty AddToFavouriesCommandProperty =
            DependencyProperty.Register("AddToFavouritesCommand", typeof(RelayCommand), typeof(PlanRepositorySidePanel),
            new PropertyMetadata(null));

        public RelayCommand AddToFavouritesCommand
        {
            get { return (RelayCommand)GetValue(AddToFavouriesCommandProperty); }
            set { SetValue(AddToFavouriesCommandProperty, value); }
        }

        #endregion

        #region RemoveFromFavouriesCommandProperty

        public static readonly DependencyProperty RemoveFromFavouriesCommandProperty =
            DependencyProperty.Register("RemoveFromFavouritesCommand", typeof(RelayCommand), typeof(PlanRepositorySidePanel),
            new PropertyMetadata(null));

        public RelayCommand RemoveFromFavouritesCommand
        {
            get { return (RelayCommand)GetValue(RemoveFromFavouriesCommandProperty); }
            set { SetValue(RemoveFromFavouriesCommandProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlanRepositorySidePanel"/> class.
        /// </summary>
        public PlanRepositorySidePanel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void xSelector_Loaded(object sender, RoutedEventArgs e)
        {
            this.xSelector.Loaded -= xSelector_Loaded;
            this.AddToFavouritesCommand = this.xSelector.AddToFavouritesCommand;
            this.RemoveFromFavouritesCommand = this.xSelector.RemoveFromFavouritesCommand;
            this.ViewModel.SelectorViewModel = this.SelectorViewModel;
        }
        
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+C selected, copy planogram
            if (e.Key == Key.C && (Keyboard.Modifiers == ModifierKeys.Control))
            {
                if (this.ViewModel.CopyPlanogramCommand.CanExecute())
                {
                    this.ViewModel.CopyPlanogramCommand.Execute();
                }
            }
            else if (e.Key == Key.X && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.CutPlanogramCommand.CanExecute())
                {
                    this.ViewModel.CutPlanogramCommand.Execute();
                }
            }
            else if (e.Key == Key.V && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.ViewModel.PastePlanogramCommand.CanExecute())
                {
                    this.ViewModel.PastePlanogramCommand.Execute();
                }
            }
        }


        #endregion

        /// <summary>
        ///     Invoked whenever the context menu for the planogram hierarchy selector is needed.
        /// </summary>
        private void XSelector_OnNodeContextMenuShowing(Object sender, PlanogramHierarchyContextEventArgs e)
        {
            var contextMenu = e.ContextMenu;
            
            AddCommandToMenu(contextMenu, ViewModel.NewGroupCommand);
            AddCommandToMenu(contextMenu, ViewModel.EditGroupCommand);
            
            // Always add the delete menu item, because it might not be executable due to permissions.
            MenuItem deleteMenuItem = new MenuItem()
            {
                Command = ViewModel.DeleteGroupCommand,
                Header = ViewModel.DeleteGroupCommand.FriendlyName,
                Icon = ViewModel.DeleteGroupCommand.SmallIcon
            };
            if (!String.IsNullOrEmpty(ViewModel.DeleteGroupCommand.DisabledReason))
            {
                deleteMenuItem.ToolTip = ViewModel.DeleteGroupCommand.DisabledReason;
            }
            contextMenu.Items.Add(deleteMenuItem);
            
            contextMenu.Items.Add(new Separator());
            AddCommandToMenu(contextMenu, ViewModel.CutPlanogramGroupCommand);
            AddCommandToMenu(contextMenu, ViewModel.CopyPlanogramGroupCommand);
            AddCommandToMenu(contextMenu, ViewModel.PastePlanogramGroupCommand);
        }

        #region Methods

        /// <summary>
        /// Adds the given command to the context menu, using its Friendly Name and Small Icon.
        /// </summary>
        private static void AddCommandToMenu(ContextMenu contextMenu, RelayCommand command)
        {
            if (command.CanExecute())
            {
                contextMenu.Items.Add(new MenuItem()
                {
                    Command = command,
                    Header = command.FriendlyName,
                    Icon = command.SmallIcon
                });
            }
        }

        #endregion
    }
}