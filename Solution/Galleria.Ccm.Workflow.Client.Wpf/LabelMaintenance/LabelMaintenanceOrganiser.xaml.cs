﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.LabelMaintenance
{
    /// <summary>
    /// Interaction logic for LabelMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class LabelMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LabelMaintenanceViewModel), typeof(LabelMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public LabelMaintenanceViewModel ViewModel
        {
            get { return (LabelMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LabelMaintenanceOrganiser senderControl = (LabelMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                LabelMaintenanceViewModel oldModel = (LabelMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LabelMaintenanceViewModel newModel = (LabelMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

            }
        }

        #endregion

        #endregion

        #region Constructor

        public LabelMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.LabelMaintenance);

            this.ViewModel = new LabelMaintenanceViewModel();

            this.Loaded += new RoutedEventHandler(LabelMaintenanceOrganiser_Loaded);
        }

        private void LabelMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LabelMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the label content textbox gets double clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LabelContentTextBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.ViewModel.SetFieldCommand.Execute();
        }

        /// <summary>
        /// calles when ever the keys O+Ctrl is pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        #endregion

        #region Window Close

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
