﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-27940 : L.Luong
//	Created

#endregion

#region Version History: (CCM 820)
// V8-30860 : L.Ineson
//  Added ILabelSetupViewModel interface.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;
using FrameworkHelpers = Galleria.Framework.Controls.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Misc;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LabelMaintenance
{
    public sealed class LabelMaintenanceViewModel : ViewModelAttachedControlObject<LabelMaintenanceOrganiser>, ILabelSetupViewModel
    {
        #region Constants

        private const Boolean DoNotAttachToCommandManager = false;
        private const String ExceptionCategory = "LabelMaintenance";
        private const Boolean SaveAsCopy = true;

        #endregion

        #region Fields

        const String _exCategory = "LabelMaintenance";
        private Boolean _userHasLabelCreatePerm;
        private Boolean _userHasLabelFetchPerm;
        private Boolean _userHasLabelEditPerm;
        private Boolean _userHasLabelDeletePerm;

        private readonly LabelInfoListViewModel _labelInfoListView = new LabelInfoListViewModel();
        private readonly ReadOnlyCollection<String> _availableFonts;

        private Label _selectedLabel;

        #endregion

        #region Binding Property paths

        // Properties
        public static readonly PropertyPath SelectedLabelProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.SelectedLabel);
        public static readonly PropertyPath DisplayTextProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.DisplayText);
        public static readonly PropertyPath AvailableLabelsProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.AvailableLabels);

        // Commands
        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.CloseCommand);
        public static PropertyPath SetFieldCommandProperty = WpfHelper.GetPropertyPath<LabelMaintenanceViewModel>(p => p.SetFieldCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Return the collection of available products based on the search criteria
        /// </summary>
        public ReadOnlyBulkObservableCollection<LabelInfo> AvailableLabels
        {
            get { return _labelInfoListView.BindableCollection; }
        }

        /// <summary>
        /// Returns the selected label model.
        /// </summary>
        public Label SelectedLabel
        {
            get { return _selectedLabel; }
            private set
            {
                _selectedLabel = value;

                OnPropertyChanged(SelectedLabelProperty);
                OnSelectedLabelChanged(value, SelectedLabel);
            }
        }

        /// <summary>
        /// Gets/Sets the setting label content.
        /// </summary>
        public String DisplayText
        {
            get
            {
                return
                    (this.SelectedLabel != null) ?
                    ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(this.SelectedLabel.Text, PlanogramFieldHelper.EnumerateAllFields(null).ToList())
                    : String.Empty;
            }
            set
            {
                if (this.SelectedLabel != null)
                {
                    this.SelectedLabel.Text = ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(value, PlanogramFieldHelper.EnumerateAllFields(null).ToList());
                }
            }
        }

        /// <summary>
        /// Returns the collection of available font names.
        /// </summary>
        public ReadOnlyCollection<String> AvailableFonts
        {
            get { return _availableFonts; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LabelMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            //fonts
            List<String> fonts = new List<String>();
            foreach (System.Windows.Media.FontFamily fam in System.Windows.Media.Fonts.SystemFontFamilies)
            {
                fonts.Add(fam.ToString());
            }
            _availableFonts = fonts.OrderBy(f => f).ToList().AsReadOnly();

            _labelInfoListView.FetchAllForEntity();

            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        ///     Gets the New command.
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute(),
                        DoNotAttachToCommandManager)
                    {

                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            if (!_userHasLabelCreatePerm)
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
            else
                return true;

            return false;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                // Create a new item.
                var newLabel = Label.NewLabel(App.ViewState.EntityId, LabelType.Product);

                // Re-initialize as property was changed.
                newLabel.MarkGraphAsInitialized();

                this.SelectedLabel = newLabel;

                // Close the Backstage.
                LocalHelper.SetRibbonBackstageState(AttachedControl, false);
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Object> _openCommand;

        /// <summary>
        ///     Gets the Open command.
        /// </summary>
        public RelayCommand<Object> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {

                    _openCommand = new RelayCommand<Object>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Object itemId)
        {
            if (!_userHasLabelFetchPerm)
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
            else if (itemId == null)
                _openCommand.DisabledReason = String.Empty;
            else
                return true;

            return false;
        }

        private void Open_Executed(Object itemId)
        {
            if (!ContinueWithItemChange()) return;

            base.ShowWaitCursor(true);

            try
            {
                if (itemId != null) this.SelectedLabel = Label.FetchById(itemId);
            }
            catch (DataPortalException e)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(e, ExceptionCategory);
                FrameworkHelpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                base.ShowWaitCursor(true);
            }

            // Close the backstage.
            LocalHelper.SetRibbonBackstageState(AttachedControl, false);

            base.ShowWaitCursor(false);
        }        

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasLabelEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.SelectedLabel == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedLabel.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     Gets the SaveAs command.
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            if (!_userHasLabelCreatePerm)
                SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
            else if (this.SelectedLabel == null)
                SaveAsCommand.DisabledReason = String.Empty;
            else
                return true;

            return false;
        }

        private void SaveAs_Executed()
        {
            SaveCurrentItem(SaveAsCopy);
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        ///     Gets the SaveAndNew command.
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(), 
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            if (SaveCurrentItem()) NewCommand.Execute();
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        ///     Gets the SaveAndClose command.
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            if (SaveCurrentItem() && AttachedControl != null) AttachedControl.Close();
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        ///     Gets the Delete command.
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(),
                        p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            if (!(this.SelectedLabel is Label))
            {
                _deleteCommand.DisabledReason = String.Empty;
            }
            if (!_userHasLabelDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
            }
            else if (this.SelectedLabel == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
            }
            else if (this.SelectedLabel.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
            }
            else
            {
                return true;
            }

            return false;
        }

        private void Delete_Executed()
        {
            // Make sure the item can be deleted (only pure templates can be deleted).
            var itemToDelete = this.SelectedLabel as Label;
            if (itemToDelete == null) return;

            //  If there is an AttachedControl AND the user does NOT confirm the deletion, cancel execution.
            if (AttachedControl != null && !FrameworkHelpers.ConfirmDeleteWithUser(this.SelectedLabel.ToString())) return;

            ShowWaitCursor(true);

            // Mark the item as deleted, so the item does not show on the list anymore.
            itemToDelete.Delete();

            // Create a new item.
            NewCommand.Execute();

            // Commit the deletion.
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException e)
            {
                ShowWaitCursor(false);

                LocalHelper.RecordException(e, ExceptionCategory);
                FrameworkHelpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                ShowWaitCursor(true);
            }

            // Update the available items.
            _labelInfoListView.FetchAllForEntity();

            ShowWaitCursor(false);
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        ///     Gets the Close command.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (AttachedControl != null) AttachedControl.Close();
        }

        #endregion

        #region SetFieldCommand

        private RelayCommand _setFieldCommand;

        /// <summary>
        /// Shows the window to populate the field specified by the param
        /// </summary>
        public RelayCommand SetFieldCommand
        {
            get
            {
                if (_setFieldCommand == null)
                {
                    _setFieldCommand = new RelayCommand(
                        p => SetField_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.LabelMaintenance_SetFieldCommand_Desc
                    };
                    base.ViewModelCommands.Add(_setFieldCommand);
                }
                return _setFieldCommand;

            }
        }

        private void SetField_Executed()
        {
            String fieldValue = this.SelectedLabel.Text;

            FieldSelectorViewModel fieldSelectorView = null;

            if (this.SelectedLabel.Type == LabelType.Fixture)
            {
                fieldSelectorView = new FieldSelectorViewModel(FieldSelectorInputType.Formula, FieldSelectorResolveType.Text,
                        fieldValue, GetPlanFixtureFieldSelectorGroups());
            }
            else 
            {
                fieldSelectorView = new FieldSelectorViewModel(FieldSelectorInputType.Formula, FieldSelectorResolveType.Text,
                        fieldValue, GetPlanogramFieldSelectorGroups());
            }

            fieldSelectorView.ShowDialog(this.AttachedControl);

            if (fieldSelectorView.DialogResult == true)
            {
                this.SelectedLabel.Text = fieldSelectorView.FieldText;
            }
        }

        #endregion

        #endregion

        #region Methods

        public IEnumerable<LabelInfo> GetLabelMatches(String nameCriteria)
        {
            var invariantCriteria = nameCriteria.ToLowerInvariant();
            return AvailableLabels
                .Where(info => info.Name.ToLowerInvariant().Contains(invariantCriteria))
                .OrderBy(info => info.ToString());
        }

        private Boolean SaveCurrentItem(Boolean saveAsCopy = false)
        {
            var itemToSave = this.SelectedLabel as Label;
            if (itemToSave == null) return false;

            var continueWithSave = true;
            var currentId = saveAsCopy ? 0 : itemToSave.Id;
            String newName;
            var isNameAccepted = true;

            if (AttachedControl != null)
            {
                var existingItems = LabelInfoList.FetchByEntityIdIncludingDeleted(App.ViewState.EntityId);

                var dialogTitle = saveAsCopy ? Message.Generic_SaveAs : String.Empty;
                var forceFirstShowDescription = saveAsCopy ? Message.Generic_SaveAs_NameDescription : String.Empty;
                var forceFirstShow = saveAsCopy;
                isNameAccepted = FrameworkHelpers.PromptUserForValue(dialogTitle, forceFirstShowDescription,
                    Message.LabelMaintenance_SaveCurrentItem_DisabledReasonNameIsNotUnique,
                    Label.NameProperty.FriendlyName, 50, forceFirstShow,
                    IsUniqueCheck(currentId, existingItems), itemToSave.Name, out newName);
            }
            else
            {
                // Force a unique value for unit testing.
                newName = FrameworkHelpers.GenerateUniqueString(itemToSave.Name,
                    AvailableLabels.Where(o => saveAsCopy || o.Id != currentId).Select(o => o.Name));
            }

            if (!isNameAccepted) return false; // Cancel saving without an accepted valid name.

            try
            {
                if (!Equals(this.SelectedLabel.Name, newName)) this.SelectedLabel.Name = newName;

                ShowWaitCursor(true);

                if (saveAsCopy)
                {
                    var copy = itemToSave.Copy();
                    copy.Name = newName;
                    this.SelectedLabel = copy.Save();
                }
                else this.SelectedLabel = itemToSave.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                continueWithSave = false; 
                LocalHelper.RecordException(ex, ExceptionCategory);
                var baseException = ex.GetBaseException();

                if (baseException is ConcurrencyException)
                {
                    var isItemReloadRequired = FrameworkHelpers.ShowConcurrencyReloadPrompt(itemToSave.Name);
                    if (isItemReloadRequired)
                    {
                        this.SelectedLabel = Label.FetchById(itemToSave.Id);
                    }
                }
                else
                {
                    FrameworkHelpers.ShowErrorOccurredMessage(itemToSave.Name, OperationType.Save);
                }
                ShowWaitCursor(true);
            }

            _labelInfoListView.FetchAllForEntity();
            ShowWaitCursor(false);

            return continueWithSave;
        }

        /// <summary>
        ///     Shows a warning requesting the user ok to continue if the current item is dirty.
        /// </summary>
        /// <returns><c>True</c> if the action should continue, <c>false</c> otherwise.</returns>
        public Boolean ContinueWithItemChange()
        {
            var businessBase = this.SelectedLabel as BusinessBase;
            return AttachedControl == null || businessBase == null || FrameworkHelpers.ContinueWithItemChange(businessBase, SaveCommand);
        }

        private Predicate<String> IsUniqueCheck(Object currentId, IEnumerable<LabelInfo> existingItems)
        {
            return newName =>
            {
                ShowWaitCursor(true);
                Boolean isDuplicateMatch = existingItems.Any(info => info.Name == newName && !currentId.Equals(info.Id));
                ShowWaitCursor(false);
                return !isDuplicateMatch;
            };
        }

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //create
            _userHasLabelCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(Label));

            //fetch
            _userHasLabelFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(Label));

            //edit
            _userHasLabelEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(Label));

            //delete
            _userHasLabelDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(Label));

        }

        /// <summary>
        /// gets the fixture fields for the selector view
        /// </summary>
        /// <returns></returns>
        private List<FieldSelectorGroup> GetPlanFixtureFieldSelectorGroups()
        {
            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();
            Dictionary<PlanogramItemType, IEnumerable<ObjectFieldInfo>> fields = PlanogramFieldHelper.GetPlanogramObjectFieldInfos();

            foreach (KeyValuePair<PlanogramItemType, IEnumerable<ObjectFieldInfo>> field in fields) 
            {
                if (field.Key.Equals(PlanogramItemType.SubComponent)) 
                {
                    groups.Add(new FieldSelectorGroup(field.Key.ToString(), field.Value));
                }
                else if (field.Key.Equals(PlanogramItemType.Component))
                {
                    groups.Add(new FieldSelectorGroup(field.Key.ToString(), field.Value));
                }
                else if (field.Key.Equals(PlanogramItemType.Fixture))
                {
                    groups.Add(new FieldSelectorGroup(field.Key.ToString(), field.Value));
                }
                else if (field.Key.Equals(PlanogramItemType.Planogram))
                {
                    groups.Add(new FieldSelectorGroup(field.Key.ToString(), field.Value));
                }
            }

            return groups;
        }

        /// <summary>
        /// gets all planogram fields for the selector view
        /// </summary>
        /// <returns></returns>
        private List<FieldSelectorGroup> GetPlanogramFieldSelectorGroups()
        {
            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();
            Dictionary<PlanogramItemType, IEnumerable<ObjectFieldInfo>> fields = PlanogramFieldHelper.GetPlanogramObjectFieldInfos();

            foreach (KeyValuePair<PlanogramItemType, IEnumerable<ObjectFieldInfo>> field in fields)
            {
                groups.Add(new FieldSelectorGroup(field.Key.ToString(), field.Value));
            }

            return groups;
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Responds to a change of selected setting.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnSelectedLabelChanged(Label oldValue, Label newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedLabel_PropertyChanged;
            }
            if (newValue != null)
            {
                newValue.PropertyChanged += SelectedLabel_PropertyChanged;
            }
            OnPropertyChanged(DisplayTextProperty);
            OnPropertyChanged(AvailableLabelsProperty);
        }

        /// <summary>
        /// Responds to property changes on the selected setting and changes it to custom label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLabel_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != Label.TypeProperty.Name)
            {
                OnPropertyChanged(DisplayTextProperty);
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                    _selectedLabel = null;
                    _labelInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
