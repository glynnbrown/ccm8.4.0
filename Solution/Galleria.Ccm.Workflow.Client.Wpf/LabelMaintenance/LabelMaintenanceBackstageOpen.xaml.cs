﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27940 : L.Luong 
//  Created.

#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Reporting.Controls.Wpf.Common;


namespace Galleria.Ccm.Workflow.Client.Wpf.LabelMaintenance
{
    /// <summary>
    /// Interaction logic for LabelMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class LabelMaintenanceBackstageOpen
    {
        #region Fields

        private readonly ObservableCollection<LabelInfo> _searchResults = new ObservableCollection<LabelInfo>();

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(LabelMaintenanceViewModel), typeof(LabelMaintenanceBackstageOpen),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        public LabelMaintenanceViewModel ViewModel
        {
            get { return (LabelMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SearchResults Property

        public static readonly DependencyProperty SearchResultsProperty = DependencyProperty.Register("SearchResults",
            typeof(ReadOnlyObservableCollection<LabelInfo>),
            typeof(LabelMaintenanceBackstageOpen),
            new PropertyMetadata(default(ReadOnlyObservableCollection<LabelInfo>)));

        public ReadOnlyObservableCollection<LabelInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<LabelInfo>)GetValue(SearchResultsProperty); }
            set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #region SearchText Property

        public static readonly DependencyProperty SearchTextProperty = DependencyProperty.Register("SearchText",
            typeof(String), typeof(LabelMaintenanceBackstageOpen), new PropertyMetadata(String.Empty, SearchText_PropertyChanged));

        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public LabelMaintenanceBackstageOpen()
        {
            SearchResults = new ReadOnlyObservableCollection<LabelInfo>(_searchResults);
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked when the collection of available validation templates changes.
        /// </summary>
        private void AvailableLabels_BulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            RefreshAvailableLabels();
        }

        /// <summary>
        ///     Invoked when the search text changes.
        /// </summary>
        private static void SearchText_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as LabelMaintenanceBackstageOpen;
            if (senderControl != null) senderControl.RefreshAvailableLabels();
        }

        /// <summary>
        ///     Invoked when the view model attached to this view changes.
        /// </summary>
        private static void ViewModel_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as LabelMaintenanceBackstageOpen;
            if (senderControl == null) return;

            var oldModel = e.OldValue as LabelMaintenanceViewModel;
            if (oldModel != null)
                oldModel.AvailableLabels.BulkCollectionChanged -=
                    senderControl.AvailableLabels_BulkCollectionChanged;
            var newModel = e.NewValue as LabelMaintenanceViewModel;
            if (newModel != null)
                newModel.AvailableLabels.BulkCollectionChanged +=
                    senderControl.AvailableLabels_BulkCollectionChanged;
            senderControl.RefreshAvailableLabels();
        }

        /// <summary>
        ///     Invoked when the user double clicks on the search list.
        /// </summary>
        private void XSearchResults_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            if (senderControl == null) return;

            var target = senderControl.SelectedItem as LabelInfo;
            if (target == null) return;

            ViewModel.OpenCommand.Execute(target.Id);
        }

        #endregion

        #region Methods

        private void RefreshAvailableLabels()
        {
            _searchResults.Clear();

            if (ViewModel == null) return;

            foreach (var match in ViewModel.GetLabelMatches(SearchText))
            {
                _searchResults.Add(match);
            }
        }

        #endregion

        private void XSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selected = xSearchResults.CurrentItem as LabelInfo;
                if(selected != null)
                    this.ViewModel.OpenCommand.Execute(selected.Id);
            }
        }
    }
}
