﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25438 : L.Hodson 
//  Created
// V8-25787 : N.Foster
//  Added step to start the workflow engine
// V8-25788 : M.Pettit
//  Added step to load the report data models
// V8-26410 : J.Pickup
//      The product date is now rendered in code as opposed to being provided on the background graphic
// V8-277732 : L.Ineson
//  Amended loading order & added license check.
#endregion
#region Version History: (CCM 8.1.0)
// V8-29953 : M.Pettit
//      Updated product date
#endregion
#endregion

using System;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.Startup
{
    /// <summary>
    /// Splash screen code behind.
    /// </summary>
    public sealed partial class LoadingWindow : Window
    {
        #region Fields
        private const Int32 _totalSteps = 3;
        private Int32 _currentStep;
        private String _statusStringFormat;
        private const String _productDate = "2016";
        #endregion

        #region Properties

        #region Status Text Property
        public static readonly DependencyProperty StatusTextProperty =
            DependencyProperty.Register("StatusText", typeof(String), typeof(LoadingWindow));

        public String StatusText
        {
            get { return (String)GetValue(StatusTextProperty); }
            private set { SetValue(StatusTextProperty, value); }
        }
        #endregion

        #region Build Version Property
        public static readonly DependencyProperty BuildVersionProperty =
            DependencyProperty.Register("BuildVersion", typeof(String), typeof(LoadingWindow));

        public String BuildVersion
        {
            get { return (String)GetValue(BuildVersionProperty); }
            private set { SetValue(BuildVersionProperty, value); }
        }
        #endregion

        #region Product Date Property

        public String ProductDate
        {
            get { return _productDate; }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LoadingWindow()
        {
            InitializeComponent();

            // stop this window showing in the taskbar
            this.ShowInTaskbar = false;

            // set the build version
            this.BuildVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            // set the status string format
            _statusStringFormat = Message.LoadingWindow_FormatString;


            // start the loading process
            this.Loaded += new RoutedEventHandler(LoadingWindow_Loaded);

        }

        /// <summary>
        /// Carries out initial loaded actions.
        /// </summary>
        private void LoadingWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LoadingWindow_Loaded;

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    ViewState state = App.ViewState;
                    _currentStep = 1;
                    LoadStep(_currentStep);

                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Controls the load order
        /// </summary>
        /// <param name="stepNo"></param>
        /// <remarks> Only load items necessary to startup here. Otherwise we risk filling 
        /// up the user's memory with objects they might not use.</remarks>
        private void LoadStep(Int32 stepNo)
        {
            switch (stepNo)
            {
                case 1:
                    CheckLicense();
                    break;

                case 2:
                    StartEngine();
                    break;

                case 3:
                    LoadApplication();
                    break;
            }
        }

        /// <summary>
        /// Checks that the app is correctly licensed.
        /// </summary>
        private void CheckLicense()
        {
            // set the status text
            this.StatusText = String.Format(CultureInfo.InvariantCulture, _statusStringFormat, _currentStep, _totalSteps, Message.LoadingWindow_CheckLicense);

            // have to queue the action so the status text gets chance to render
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction(() =>
            {
                Boolean isLicensed = ((Galleria.Ccm.Workflow.Client.Wpf.App)App.Current).CheckLicense();

                if (isLicensed)
                {
                    // move to the next step
                    _currentStep++;
                    LoadStep(_currentStep);
                }
            });
        }

        /// <summary>
        /// Starts the workflow engine
        /// </summary>
        private void StartEngine()
        {
            // set the status text
            this.StatusText = String.Format(CultureInfo.InvariantCulture, _statusStringFormat, _currentStep, _totalSteps, Message.LoadingWindow_Engine);

            // trigger the engine start
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction(() =>
            {
                if (!App.IsUnitTesting && Galleria.Framework.Engine.Engine.IsEnabled)
                    App.Engine.StartEngine();

                // move to the next step 
                _currentStep++;
                LoadStep(_currentStep);
            });
        }


        #region Load Application


        /// <summary>
        /// Performs the load of the application
        /// </summary>
        private void LoadApplication()
        {
            // set the status text
            this.StatusText = String.Format(CultureInfo.InvariantCulture, _statusStringFormat, _currentStep, _totalSteps, Message.LoadingWindow_Application);

            //have to queue the action so the status text gets chance to render
            Galleria.Framework.Controls.Wpf.Helpers.QueueAction(() =>
            {
                MainPageOrganiser mainPageOrganiser = new MainPageOrganiser();
                mainPageOrganiser.Activated += new EventHandler(MainPageOrganiser_Activated);
                mainPageOrganiser.Show();
            });
        }

        /// <summary>
        /// Event handler for the main page organiser becoming activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainPageOrganiser_Activated(object sender, EventArgs e)
        {
            MainPageOrganiser mainPageOrganiser = (MainPageOrganiser)sender;
            mainPageOrganiser.Activated -= MainPageOrganiser_Activated;
            App.Current.MainWindow = mainPageOrganiser;

            ((Galleria.Ccm.Workflow.Client.Wpf.App)App.Current).ShutdownMode = ShutdownMode.OnMainWindowClose;
            this.Close();
        }
        #endregion

        #endregion
    }
}
