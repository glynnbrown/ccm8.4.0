﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25559 : L.Hodson
//  Copied from SA
#endregion
#endregion


using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.Startup
{
    /// <summary>
    /// Viewmodel controller for the DatabaseSetupWindow.
    /// </summary>
    public sealed class DatabaseSetupViewModel : ViewModelAttachedControlObject<DatabaseSetupWindow>
    {
        #region Constants
        const String cMssql = "Mssql"; // default sql dal name
        #endregion

        #region Fields

        #region Connection
        private Connection _currentConnection = Connection.NewConnection(); // The current connection detail
        private UserSystemSetting _currentSettings = null; // The users current settings

        #region Previous connection details

        private ObservableCollection<String> _serverNames = new ObservableCollection<String>(); // Previous server name details
        private ObservableCollection<String> _databaseNames = new ObservableCollection<String>(); // Previous database name details
        private ObservableCollection<String> _databaseFileNames = new ObservableCollection<String>(); // Previous database file details

        #endregion

        #endregion

        private Boolean? _dialogResult;

        private DatabaseSetupStep _currentStep = DatabaseSetupStep.SelectDbType; // The current database setup step
        private DatabaseSetupType _selectedDatabaseType = DatabaseSetupType.Existing; // The selected database type
        private ReadOnlyObservableCollection<DatabaseSetupType> _availableConnectionTypesRO; // List of available connection types
        private String _serverName; // The current server name (sql)
        private String _databaseName; // The current database name (sql)
        private String _newDatabaseName;
        private BackgroundWorker _backgroundWorker;
        private Boolean _isCancelling;
        private Boolean _isSqlServerExpanded = true; // determines existing database selection, sql selected
        private Boolean _isUpgrading = false; // determines if a database is being upgraded
        private DatabaseErrorSetupType _currentDatabaseErrorSetupType; // the current database connection error type
        private EntityInfoList _entityInfoList; // The entity info list
        private EntityInfo _selectedEntity; // the selected business entity
        private String _databaseVersion; // the current database version
        private String _latestDatabaseVersion; // the latest available database version
        private Boolean _isRetryingConnection = false; // determines if retrying connection
        private String _creationFailedDetail; // creation failed message description
        private IDalFactory _dalFactory; // The current dal factory
        private Boolean _existing = false; // true if seleted an existing databse, false otherwise
        private Boolean _isDatabaseAvailable = false; // determines if the database is available
        private Boolean _newConnection = false; // determines if this is a first time connection


        #endregion

        #region Binding Property Paths

        // Properties
        public static readonly PropertyPath ServerNamesProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.ServerNames);
        public static readonly PropertyPath DatabaseNamesProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.DatabaseNames);
        public static readonly PropertyPath DatabaseFileNamesProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.DatabaseFileNames);
        public static readonly PropertyPath IsDatabaseSelectionRememberedProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.IsDatabaseSelectionRemembered);
        public static readonly PropertyPath IsEntitySelectionRememberedProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.IsEntitySelectionRemembered);

        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath WindowTitleProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.WindowTitle);
        public static readonly PropertyPath SelectedDatabaseTypeProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.SelectedDatabaseType);
        public static readonly PropertyPath AvailableConnectionTypesProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.AvailableConnectionTypes);
        public static readonly PropertyPath ServerNameProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.ServerName);
        public static readonly PropertyPath DatabaseNameProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.DatabaseName);
        public static readonly PropertyPath NewDatabaseNameProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.NewDatabaseName);
        public static readonly PropertyPath IsNextCommandVisibleProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.IsNextCommandVisible);
        public static readonly PropertyPath IsCancelCommandVisibleProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.IsCancelCommandVisible);
        public static readonly PropertyPath IsSqlServerExpandedProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.IsSqlServerExpanded);
        public static readonly PropertyPath CurrentDatabaseErrorSetupTypeProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.CurrentDatabaseErrorSetupType);
        public static readonly PropertyPath IsUpgradingProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.IsUpgrading);
        public static readonly PropertyPath MasterEntityListProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.MasterEntityList);
        public static readonly PropertyPath SelectedEntityProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.SelectedEntity);
        public static readonly PropertyPath CreationFailedDetailProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.CreationFailedDetail);
        public static readonly PropertyPath IsRetryingConnectionProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.IsRetryingConnection);
        public static readonly PropertyPath SoftwareVersionProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.SoftwareVersion);
        public static readonly PropertyPath SqlConnectionDetailsProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.SqlConnectionDetails);
        public static readonly PropertyPath DatabaseVersionProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.DatabaseVersion);

        //Comamnds
        public static readonly PropertyPath BackCommandProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.BackCommand);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.NextCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath IsDatabaseAvailableProperty = WpfHelper.GetPropertyPath<DatabaseSetupViewModel>(p => p.IsDatabaseAvailable);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Gets the current window title
        /// </summary>
        public String WindowTitle
        {
            get
            {
                switch (this.CurrentStep)
                {
                    case DatabaseSetupStep.SelectDbType:
                    case DatabaseSetupStep.ExistingDatabase:
                        return Message.DatabaseSetup_Title;
                    case DatabaseSetupStep.SqlEnterDetails:
                        return Message.DatabaseSetup_SqlDatabase_Title;
                    case DatabaseSetupStep.SqlInProgress:
                        return Message.DatabaseSetup_InProgress_Title;
                    case DatabaseSetupStep.CreationFailed:
                        return Message.DatabaseSetup_CreationFailed_Title;
                    case DatabaseSetupStep.AttemptingConnection:
                        return Message.DatabaseSetup_AttemptingConnection_Title;
                    case DatabaseSetupStep.DatabaseIncompatible:
                    case DatabaseSetupStep.ConnectionFailed:
                        return Message.DatabaseSetup_DatabaseError_Title;
                    case DatabaseSetupStep.EntitySelection:
                        return Message.DatabaseSetup_EntitySelection_Title;
                    case DatabaseSetupStep.VerifyInProgress:
                        return Message.DatabaseSetup_VerifyInProgress_Title;
                    case DatabaseSetupStep.VerificationFailed:
                        return Message.DatabaseSetup_VerificationFailed_Title;
                    case DatabaseSetupStep.ErrorOccured:
                        return Message.DatabaseSetup_ErrorOccurred_Title;
                    default: return "";
                }
            }
        }

        /// <summary>
        /// Returns the current step
        /// </summary>
        public DatabaseSetupStep CurrentStep
        {
            get { return _currentStep; }
            set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                OnPropertyChanged(WindowTitleProperty);
                OnPropertyChanged(IsNextCommandVisibleProperty);
                OnPropertyChanged(IsCancelCommandVisibleProperty);

                OnCurrentStepChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of available connection types
        /// </summary>
        public ReadOnlyObservableCollection<DatabaseSetupType> AvailableConnectionTypes
        {
            get
            {
                if (_availableConnectionTypesRO == null)
                {
                    ObservableCollection<DatabaseSetupType> collection = new ObservableCollection<DatabaseSetupType>();
                    foreach (DatabaseSetupType type in Enum.GetValues(typeof(DatabaseSetupType)))
                    {
                        collection.Add(type);
                    }
                    _availableConnectionTypesRO =
                        new ReadOnlyObservableCollection<DatabaseSetupType>(collection);
                }
                return _availableConnectionTypesRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected connection type
        /// </summary>
        public DatabaseSetupType SelectedDatabaseType
        {
            get { return _selectedDatabaseType; }
            set
            {
                _selectedDatabaseType = value;
                OnPropertyChanged(SelectedDatabaseTypeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the sql server name
        /// </summary>
        public String ServerName
        {
            get { return _serverName; }
            set
            {
                _serverName = value;
                OnPropertyChanged(ServerNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the sql database name
        /// </summary>
        public String DatabaseName
        {
            get { return _databaseName; }
            set
            {
                _databaseName = value;
                OnPropertyChanged(DatabaseNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the name of the sql database the user wants to create
        /// </summary>
        public String NewDatabaseName
        {
            get { return _newDatabaseName; }
            set
            {
                _newDatabaseName = value;
                OnPropertyChanged(NewDatabaseNameProperty);
            }
        }


        /// <summary>
        /// Gets/Sets the IsSqlServerExpanded value
        /// </summary>
        public Boolean IsSqlServerExpanded
        {
            get { return _isSqlServerExpanded; }
            set
            {
                _isSqlServerExpanded = value;
                SetDefaultConnection();
                OnPropertyChanged(IsSqlServerExpandedProperty);
            }
        }

        /// <summary>
        /// The Entity List
        /// </summary>
        public EntityInfoList MasterEntityList
        {
            get { return _entityInfoList; }
            set
            {
                _entityInfoList = value;
                OnPropertyChanged(MasterEntityListProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected connection type
        /// </summary>
        public EntityInfo SelectedEntity
        {
            get { return _selectedEntity; }
            set
            {
                _selectedEntity = value;
                OnPropertyChanged(SelectedEntityProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected connection type
        /// </summary>
        public DatabaseErrorSetupType CurrentDatabaseErrorSetupType
        {
            get { return _currentDatabaseErrorSetupType; }
            set
            {
                _currentDatabaseErrorSetupType = value;

                //if ((_currentDatabaseErrorSetupType == DatabaseErrorSetupType.UpgradeRequired) && (IsLocalServerExpanded == true))
                //{
                //    this.IsUpgradeCommandVisible = true;
                //}

                OnPropertyChanged(CurrentDatabaseErrorSetupTypeProperty);
            }
        }

        /// <summary>
        /// Determines if current database is performing an upgrade
        /// </summary>
        public Boolean IsUpgrading
        {
            get { return _isUpgrading; }
            set
            {
                _isUpgrading = value;
                _nextCommand.RaiseCanExecuteChanged();
                _cancelCommand.RaiseCanExecuteChanged();
                OnPropertyChanged(IsUpgradingProperty);
            }
        }

        /// <summary>
        /// The current software version
        /// </summary>
        public String SoftwareVersion
        {
            get
            {
                Version version = new Version(System.Windows.Forms.Application.ProductVersion);
                return "V" + version.Major + "." + version.Minor;
            }
        }

        public String SqlConnectionDetails
        {
            get
            {
                return Message.DatabaseSetup_SqlEnterDetails_ServerName + " : " + _serverName + "\n" +
                       Message.DatabaseSetup_SqlEnterDetails_DatabaseName + " : " + _databaseName;
            }
        }

        /// <summary>
        /// The current database version
        /// </summary>
        public String DatabaseVersion
        {
            get
            {
                return _databaseVersion;
            }
        }

        /// <summary>
        /// Determines if the database connection is retrying
        /// </summary>
        public Boolean IsRetryingConnection
        {
            get { return _isRetryingConnection; }
            set
            {
                _isRetryingConnection = value;
                OnPropertyChanged(IsRetryingConnectionProperty);
            }
        }

        /// <summary>
        /// The creation failed description
        /// </summary>
        public String CreationFailedDetail
        {
            get
            {
                return _creationFailedDetail;
            }
            set
            {
                _creationFailedDetail = value;
                OnPropertyChanged(CreationFailedDetailProperty);
            }
        }

        /// <summary>
        /// Users remember database selection option
        /// </summary>
        public Boolean IsDatabaseSelectionRemembered
        {
            get { return _currentSettings.IsDatabaseSelectionRemembered; }
            set
            {
                _currentSettings.IsDatabaseSelectionRemembered = value;
                OnPropertyChanged(IsDatabaseSelectionRememberedProperty);
            }
        }

        /// <summary>
        /// Users remember entity selection option
        /// </summary>
        public Boolean IsEntitySelectionRemembered
        {
            get { return _currentSettings.IsEntitySelectionRemembered; }
            set
            {
                _currentSettings.IsEntitySelectionRemembered = value;
                OnPropertyChanged(IsEntitySelectionRememberedProperty);
            }
        }

        /// <summary>
        /// Determines if the database is available
        /// </summary>
        public Boolean IsDatabaseAvailable
        {
            get { return _isDatabaseAvailable; }
            set
            {
                _isDatabaseAvailable = value;
                OnPropertyChanged(IsDatabaseAvailableProperty);
            }
        }

        #region Connection

        /// <summary>
        /// The previous servers names detail
        /// </summary>
        public ObservableCollection<String> ServerNames
        {
            get { return _serverNames; }
            set
            {
                _serverNames = value;
                OnPropertyChanged(ServerNamesProperty);
            }
        }

        /// <summary>
        /// The previous database names detail
        /// </summary>
        public ObservableCollection<String> DatabaseNames
        {
            get { return _databaseNames; }
            set
            {
                _databaseNames = value;
                OnPropertyChanged(DatabaseNamesProperty);
            }
        }

        /// <summary>
        /// The previous database file names detail
        /// </summary>
        public ObservableCollection<String> DatabaseFileNames
        {
            get { return _databaseFileNames; }
            set
            {
                _databaseFileNames = value;
                OnPropertyChanged(DatabaseFileNamesProperty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseSetupViewModel() : this(false) { }

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseSetupViewModel(Boolean unitTesting)
        {
            if (!unitTesting)
            {
                // Load current user settings
                _currentSettings = App.GetSystemSettings();

                if (_currentSettings.Connections.Count > 0)
                {
                    // set default connection
                    this.CurrentStep = DatabaseSetupStep.ExistingDatabase;
                    this.SelectedDatabaseType = DatabaseSetupType.Existing;
                    _currentConnection = _currentSettings.Connections.FirstOrDefault(c => c.IsCurrentConnection == true);

                    if (_currentConnection != null)
                    {
                        _serverName = _currentConnection.ServerName;
                        _databaseName = _currentConnection.DatabaseName;
                    }
                    else
                    {
                        _currentConnection = Connection.NewConnection();
                        if (_currentSettings.Connections.Any())
                        {
                            _serverName = _currentSettings.Connections.LastOrDefault().ServerName;
                            _databaseName = _currentSettings.Connections.LastOrDefault().DatabaseName;
                        }
                    }

                    if (_currentSettings.IsDatabaseSelectionRemembered && _currentSettings.IsEntitySelectionRemembered == false)
                    {
                        // Proceed to attempting database connection
                        this.NextCommand.Execute();
                    }

                    // Load all previous connection detail
                    LoadPreviousConnectionDetails();
                }
                else
                {
                    // Set default database setup selection
                    this.CurrentStep = DatabaseSetupStep.SelectDbType;
                    _newConnection = true; // new connection, flag so we can get uom from entity
                }
            }
        }

        #endregion

        #region Commands

        #region BackCommand

        private RelayCommand _backCommand;

        /// <summary>
        /// Returns to the previous step screen
        /// </summary>
        public RelayCommand BackCommand
        {
            get
            {
                if (_backCommand == null)
                {
                    _backCommand = new RelayCommand(
                        p => Back_Executed(),
                        p => Back_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_backCommand);
                }
                return _backCommand;
            }
        }

        private Boolean Back_CanExecute()
        {
            return this.CurrentStep == DatabaseSetupStep.SqlEnterDetails ||
                    this.CurrentStep == DatabaseSetupStep.ExistingDatabase ||
                    this.CurrentStep == DatabaseSetupStep.CreationFailed ||
                    this.CurrentStep == DatabaseSetupStep.VerificationFailed ||
                    this.CurrentStep == DatabaseSetupStep.ErrorOccured ||
                    this.CurrentStep == DatabaseSetupStep.EntitySelection ||
                    this.CurrentStep == DatabaseSetupStep.DatabaseIncompatible;
        }

        private void Back_Executed()
        {
            switch (this.CurrentStep)
            {
                case DatabaseSetupStep.SqlEnterDetails:
                case DatabaseSetupStep.VerificationFailed:
                case DatabaseSetupStep.ErrorOccured:
                case DatabaseSetupStep.ExistingDatabase:
                    _existing = false;
                    this.CurrentStep = DatabaseSetupStep.SelectDbType;
                    break;

                case DatabaseSetupStep.DatabaseIncompatible:
                    if (this.AttachedControl != null)
                    {
                        this._nextCommand.FriendlyName = Message.Generic_Next;
                    }
                    this.CurrentStep = DatabaseSetupStep.ExistingDatabase;
                    break;

                case DatabaseSetupStep.CreationFailed:
                    switch (this.SelectedDatabaseType)
                    {
                        case DatabaseSetupType.SQL:
                            this.CurrentStep = DatabaseSetupStep.SqlEnterDetails;
                            break;
                        default: throw new NotImplementedException();
                    }
                    break;

                case DatabaseSetupStep.EntitySelection:
                    this.SelectedDatabaseType = DatabaseSetupType.Existing;
                    this.CurrentStep = DatabaseSetupStep.ExistingDatabase;
                    _nextCommand.FriendlyName = Message.Generic_Next;
                    break;

                default: throw new NotImplementedException();
            }
        }

        #endregion

        #region NextCommand

        private RelayCommand _nextCommand;

        /// <summary>
        /// Moves to the next step
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.Enter
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }


        private Boolean Next_CanExecute()
        {
            if (this.CurrentStep == DatabaseSetupStep.SqlEnterDetails)
            {
                return !String.IsNullOrEmpty(this.ServerName) && !String.IsNullOrEmpty(this.NewDatabaseName); //SA-14529
            }
            else if (this.CurrentStep == DatabaseSetupStep.ExistingDatabase)
            {
                return !String.IsNullOrEmpty(this.ServerName) && !String.IsNullOrEmpty(this.DatabaseName);
            }
            else if (this.CurrentStep == DatabaseSetupStep.DatabaseIncompatible)
            {
                if (_cancelCommand == null)
                {
                    return false;
                }

                if (_currentDatabaseErrorSetupType == DatabaseErrorSetupType.IncompatibleDatabase)
                {
                    _nextCommand.FriendlyName = Message.Generic_Continue;
                }
                else if (((_currentDatabaseErrorSetupType == DatabaseErrorSetupType.IncompatibleSoftware))
                    || (_isSqlServerExpanded))
                {
                    _nextCommand.FriendlyName = Message.Generic_Quit;
                }
                if (_currentDatabaseErrorSetupType == DatabaseErrorSetupType.UpgradeRequired)
                {
                    return !_isUpgrading;
                }
            }
            else if (this.CurrentStep == DatabaseSetupStep.ConnectionFailed)
            {
                if (_cancelCommand == null)
                {
                    return false;
                }
                if (this.IsDatabaseAvailable)
                {
                    _nextCommand.FriendlyName = Message.Generic_Retry;
                    _cancelCommand.FriendlyName = Message.Generic_Previous;
                }
                else
                {
                    _nextCommand.FriendlyName = Message.Generic_Quit;
                    _cancelCommand.FriendlyName = Message.Generic_Cancel;
                }
            }
            else if (this.CurrentStep == DatabaseSetupStep.EntitySelection)
            {
                _nextCommand.FriendlyName = Message.Generic_Ok;
            }
            else if (this.CurrentStep == DatabaseSetupStep.ErrorOccured)
            {
                _nextCommand.FriendlyName = Message.Generic_Retry;
            }
            else if (this.CurrentStep == DatabaseSetupStep.VerificationFailed)
            {
                _nextCommand.FriendlyName = Message.DatabaseSetup_Restore;
            }

            return true;
        }

        private void Next_Executed()
        {
            switch (this.CurrentStep)
            {
                case DatabaseSetupStep.SelectDbType:
                    switch (this.SelectedDatabaseType)
                    {
                        case DatabaseSetupType.SQL:
                            this.IsSqlServerExpanded = true;
                            this.NewDatabaseName = String.Empty; //SA-14529
                            this.DatabaseName = String.Empty;
                            this.ServerName = String.Empty;
                            this.CurrentStep = DatabaseSetupStep.SqlEnterDetails;
                            break;

                        case DatabaseSetupType.Existing:
                            SetDefaultConnection();
                            this.CurrentStep = DatabaseSetupStep.ExistingDatabase;
                            break;
                    }
                    break;


                case DatabaseSetupStep.SqlEnterDetails:
                    this.DatabaseName = this.NewDatabaseName; //SA-14529
                    this.CurrentStep = DatabaseSetupStep.SqlInProgress;
                    CreateSQLDatabase();
                    break;

                case DatabaseSetupStep.SqlInProgress:
                    this.CurrentStep = DatabaseSetupStep.EntitySelection;
                    break;

                case DatabaseSetupStep.ExistingDatabase:
                    _existing = true;
                    this.CurrentStep = DatabaseSetupStep.AttemptingConnection;
                    AttemptConnection();
                    break;

                case DatabaseSetupStep.DatabaseIncompatible:
                    if (_currentDatabaseErrorSetupType == DatabaseErrorSetupType.IncompatibleDatabase)
                    {
                        // sql database only has minor version ifdference, user can proceed
                        RetryConnection();
                    }
                    else if (this.AttachedControl != null)
                    {
                        this.AttachedControl.Close();
                    }
                    break;

                case DatabaseSetupStep.ConnectionFailed:
                    if (this.IsDatabaseAvailable)
                    {
                        this.IsRetryingConnection = true;
                        RetryConnection();
                    }
                    else
                    {
                        if (this.AttachedControl != null)
                        {
                            this.AttachedControl.Close();
                        }
                    }
                    break;

                case DatabaseSetupStep.EntitySelection:
                    DatabaseAndEntitySelectionComplete();
                    break;

                case DatabaseSetupStep.ErrorOccured:
                    this.CurrentStep = DatabaseSetupStep.AttemptingConnection;
                    AttemptConnection();
                    break;

                case DatabaseSetupStep.VerificationFailed:
                    //if (File.Exists(_onExitBackupFile))
                    //{
                    //    //try { File.Delete(this.FullFileName); }
                    //    //catch { }
                    //    //File.Copy(_onExitBackupFile, this.FullFileName); //copy tested file back to the original

                    //    this.CurrentStep = DatabaseSetupStep.AttemptingConnection;
                    //    AttemptConnection();
                    //}
                    break;
            }
        }


        /// <summary>
        /// Returns true if the next command button should be shown
        /// </summary>
        public Boolean IsNextCommandVisible
        {
            get
            {
                if (this.CurrentStep == DatabaseSetupStep.DatabaseIncompatible)
                {
                    if ((_currentDatabaseErrorSetupType == DatabaseErrorSetupType.IncompatibleSoftware)
                        || (_isSqlServerExpanded))
                    {
                        if (_nextCommand != null)
                        {
                            _nextCommand.RaiseCanExecuteChanged();
                        }
                        return true;
                    }
                }
                else
                if (this.CurrentStep == DatabaseSetupStep.ConnectionFailed)
                {
                    if (_nextCommand != null)
                    {
                        this._nextCommand.RaiseCanExecuteChanged();
                    }
                    return true;
                }
                else if (this._currentStep == DatabaseSetupStep.CreationFailed)
                {
                    this.BackCommand.RaiseCanExecuteChanged();
                }
                else if (this._currentStep == DatabaseSetupStep.EntitySelection)
                {
                    this._nextCommand.RaiseCanExecuteChanged();
                    return true;
                }
                else if (this.CurrentStep == DatabaseSetupStep.VerifyInProgress)
                {
                    this.NextCommand.RaiseCanExecuteChanged();
                    return false;
                }
                else if (this.CurrentStep == DatabaseSetupStep.ErrorOccured)
                {
                    this.NextCommand.RaiseCanExecuteChanged();
                    return true;
                }
                else if (this.CurrentStep == DatabaseSetupStep.VerificationFailed)
                {
                    this.NextCommand.RaiseCanExecuteChanged();
                    return true;
                }

                return ((this.CurrentStep != DatabaseSetupStep.SqlInProgress)
                        && (this.CurrentStep != DatabaseSetupStep.AttemptingConnection)
                        && (this.CurrentStep != DatabaseSetupStep.CreationFailed)
                        );
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the create database operation
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute(), false)
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private Boolean Cancel_CanExecute()
        {
            return !_isCancelling & !_isUpgrading;
        }

        private void Cancel_Executed()
        {
            switch (this.CurrentStep)
            {
                case DatabaseSetupStep.SqlInProgress:
                    //TODO Implement some kind of rollback (currently cancel button is not visible)
                    //disable the button
                    _isCancelling = true;
                    _cancelCommand.RaiseCanExecuteChanged();
                    _backgroundWorker.CancelAsync();
                    break;

                case DatabaseSetupStep.DatabaseIncompatible:
                case DatabaseSetupStep.ConnectionFailed:
                    if (this.AttachedControl != null)
                    {
                        this._nextCommand.FriendlyName = Message.Generic_Next;
                        this._cancelCommand.FriendlyName = Message.Generic_Cancel;
                    }
                    //this.IsUpgradeCommandVisible = false;
                    this.CurrentStep = DatabaseSetupStep.ExistingDatabase;
                    break;

                default:
                    //close the window
                    this.AttachedControl.Close();
                    break;
            }
        }

        public Boolean IsCancelCommandVisible
        {
            get
            {
                return this.CurrentStep != DatabaseSetupStep.SqlInProgress &&
                       this.CurrentStep != DatabaseSetupStep.AttemptingConnection &&
                       this.CurrentStep != DatabaseSetupStep.VerifyInProgress &&
                       this.CurrentStep != DatabaseSetupStep.DatabaseIncompatible;
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of current step
        /// </summary>
        /// <param name="newValue"></param>
        private void OnCurrentStepChanged(DatabaseSetupStep newValue)
        {
            if (newValue == DatabaseSetupStep.EntitySelection)
            {
                if (this.IsEntitySelectionRemembered)
                {
                    Connection existingConnection = GetExistingConnection();
                    Boolean loadEntity = (existingConnection != null) ? existingConnection.IsCurrentConnection : false;
                    if (loadEntity)
                    {
                        //load the entity straight away if it exists else reset
                        this.SelectedEntity = this.MasterEntityList.FirstOrDefault(e => Object.Equals(e.Id, existingConnection.BusinessEntityId));
                        if (this.SelectedEntity != null)
                        {
                            this.NextCommand.Execute();
                        }
                        else
                        {
                            this.IsEntitySelectionRemembered = false;
                        }
                    }
                    else
                    {
                        this.IsEntitySelectionRemembered = false;
                    }

                }
                else if (this.MasterEntityList.Count == 1)
                {
                    this.SelectedEntity = this.MasterEntityList.First();
                    this.NextCommand.Execute();
                }
            }
        }

        #endregion

        #region Methods

        #region Create Database

        #region Create Sql database

        /// <summary>
        /// Starts off the process
        /// </summary>
        private void CreateSQLDatabase()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.DoWork += new DoWorkEventHandler(CreateSQLDatabase_DoWork);
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(CreateDatabase_ProgressChanged);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CreateDatabase_RunWorkerCompleted);
            _backgroundWorker.RunWorkerAsync(new String[] { this.ServerName, this.DatabaseName });
        }
        /// <summary>
        /// Carries out the work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateSQLDatabase_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;
            String[] args = (String[])e.Argument;

            String serverName = args[0];
            String databaseName = args[1];

            // validate
            if (serverName == null)
            {
                throw new Exception("Server name cannot be null");
            }
            if (databaseName == null)
            {
                throw new Exception("Database name cannot be null");
            }

            try
            {
                // create a new config element for the dal
                DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(cMssql);
                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", serverName));
                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", databaseName));
                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("DefaultUser", System.Security.Principal.WindowsIdentity.GetCurrent().Name));

                // now create a new instance of the Mssql dal
                IDalFactory dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalFactoryConfig);

                //[SA-14459]Check DB Version
                using (Galleria.Framework.Dal.Mssql.DalContext context = (Galleria.Framework.Dal.Mssql.DalContext)dalFactory.CreateContext())
                {
                    String[] sqlVersion = context.Connection.ServerVersion.Split('.');
                    if (Convert.ToInt32(sqlVersion[0]) < 10)
                    {
                        throw new Exception(Message.DatabaseSetup_CreationFailed_SQLDescription1);
                    }
                }

                // and call the upgrade process
                dalFactory.Upgrade();

                DalContainer.RegisterFactory(cMssql, dalFactory);
                DalContainer.DalName = cMssql;

                e.Result = true;
            }
            catch (Exception except)
            {
                //report out the exception
                _backgroundWorker.ReportProgress(0, except);
                e.Result = false;
            }
        }

        #endregion

        private void CreateDatabase_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is Exception)
            {
                if (this.AttachedControl != null)
                {
                    Exception except = (Exception)e.UserState;

                    this.CreationFailedDetail = except.Message;
                    this.CurrentStep = DatabaseSetupStep.CreationFailed;
                }
            }
        }
        /// <summary>
        /// Handles the worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateDatabase_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;

            //if (this.CurrentStep == DatabaseSetupStep.LocalInProgress)
            //{
            //worker.DoWork -= CreateLocalDatabase_DoWork;
            //}
            //else
            //{
            worker.DoWork -= CreateSQLDatabase_DoWork;
            //}

            worker.ProgressChanged -= CreateDatabase_ProgressChanged;
            worker.RunWorkerCompleted -= CreateDatabase_RunWorkerCompleted;

            if ((bool)e.Result)
            {
                Galleria.Ccm.Security.DomainPrincipal.BeginAuthentication(AuthenticationComplete);
            }
        }

        #endregion

        #region Attempt Connection

        /// <summary>
        /// Test the connection details
        /// </summary>
        private void AttemptConnection()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(AttemptConnection_DoWork);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AttemptConnection_RunWorkerCompleted);
            _backgroundWorker.RunWorkerAsync();
        }
        /// <summary>
        /// Carries out the work of testing the connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttemptConnection_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;


            DalFactoryConfigElement dalDatabaseFactoryConfig = new DalFactoryConfigElement(cMssql);
            dalDatabaseFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", this.ServerName));
            dalDatabaseFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", this.DatabaseName));

            // now create a new instance of the Mssql dal
            _dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalDatabaseFactoryConfig);

            DalContainer.RegisterFactory(cMssql, _dalFactory);
            DalContainer.DalName = cMssql;

            if (_dalFactory.IsDatabaseAvailable())
            {
                if (ValidDatabaseVersion(e, _dalFactory))
                {
                    e.Result = _currentStep;
                }

                this.IsDatabaseAvailable = true;
            }
            else
            {
                //[16763] Database doesn't exist
                e.Result = null;

                this.IsDatabaseAvailable = false;
            }

        }

        private Boolean ValidDatabaseVersion(DoWorkEventArgs e, IDalFactory dalFactory)
        {
            bool returnVal = true;
            String databaseVersion;
            String latestDatabaseVersion;
            IncompatibilityErrorType incompatibilityErrorType;

            try
            {
                if (dalFactory.UpgradeRequired(out databaseVersion, out latestDatabaseVersion, out incompatibilityErrorType))
                {
                    _databaseVersion = databaseVersion;
                    _latestDatabaseVersion = latestDatabaseVersion;
                    e.Result = DatabaseSetupStep.DatabaseIncompatible;
                    returnVal = false;

                    switch (incompatibilityErrorType)
                    {
                        case IncompatibilityErrorType.None:
                            this.CurrentDatabaseErrorSetupType = DatabaseErrorSetupType.UpgradeRequired;
                            break;
                        case IncompatibilityErrorType.Major:
                            this.CurrentDatabaseErrorSetupType = DatabaseErrorSetupType.IncompatibleSoftware;
                            break;
                        case IncompatibilityErrorType.Minor:
                            this.CurrentDatabaseErrorSetupType = DatabaseErrorSetupType.IncompatibleDatabase;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                returnVal = false;
                Debug.WriteLine(ex.Message);
            }

            return returnVal;
        }

        /// <summary>
        /// Handles the test connection worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AttemptConnection_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= AttemptConnection_DoWork;
            worker.RunWorkerCompleted -= AttemptConnection_RunWorkerCompleted;

            if (e.Result != null)
            {
                if ((DatabaseSetupStep)e.Result == DatabaseSetupStep.DatabaseIncompatible)
                {
                    this.CurrentStep = DatabaseSetupStep.DatabaseIncompatible;
                }
                else
                {
                    Galleria.Ccm.Security.DomainPrincipal.BeginAuthentication(AuthenticationComplete);
                }
            }
            else
            {
                this.CurrentStep = DatabaseSetupStep.ConnectionFailed;
            }
        }

        private void RetryConnection()
        {
            Galleria.Ccm.Security.DomainPrincipal.BeginAuthentication(AuthenticationComplete);
        }

        #endregion

        #region Upgrade Database

        private void UpgradeDatabase()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(UpgradeDatabase_DoWork);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(UpgradeDatabase_RunWorkerCompleted);
            _backgroundWorker.RunWorkerAsync();
        }

        void UpgradeDatabase_DoWork(object sender, DoWorkEventArgs e)
        {
            this.IsUpgrading = true;

            try
            {
                _dalFactory.Upgrade(_latestDatabaseVersion);
            }
            catch (Exception ex)
            {
                //report out the exception
                e.Result = ex;
            }
        }

        void UpgradeDatabase_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= UpgradeDatabase_DoWork;
            worker.RunWorkerCompleted -= UpgradeDatabase_RunWorkerCompleted;

            this.IsUpgrading = false;

            if (e.Result == null)
            {
                //this.IsUpgradeCommandVisible = false;
                _currentDatabaseErrorSetupType = DatabaseErrorSetupType.None;
                Galleria.Ccm.Security.DomainPrincipal.BeginAuthentication(AuthenticationComplete);
            }
            else
            {
                ModalMessage msg = new ModalMessage();
                msg.Description = String.Format(CultureInfo.CurrentCulture, "Unable to upgrade database. Please contact your system administrator. \n\n" + e.Result.ToString());
                msg.Header = Message.DatabaseSetup_UpgradeFailed;
                msg.Button1Content = Message.Generic_Ok;
                msg.ButtonCount = 1;

                msg.Owner = this.AttachedControl;
                msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                msg.ShowDialog();
            }
        }

        #endregion

        #region AuthenticationComplete

        private void AuthenticationComplete(object sender, DomainAuthenticationResultEventArgs e)
        {
            if (e.Exception == null)
            {
                bool isAuthenticated = ApplicationContext.User.Identity.IsAuthenticated;
                if (isAuthenticated)
                {
                    // the user has been successfully authenticated, Retrieve available business entities 
                    MasterEntityList = EntityInfoList.FetchAllEntityInfos();

                    if (MasterEntityList.Count == 0)
                    {
                        //insert a new default entity
                        Entity defaultEntity = Entity.NewEntity();
                        defaultEntity.Name = "Default";
                        defaultEntity = defaultEntity.Save();

                        MasterEntityList = EntityInfoList.FetchAllEntityInfos();
                    }

                    if (!Object.Equals(null, _currentConnection.BusinessEntityId) && !Object.Equals(0, _currentConnection.BusinessEntityId))
                    {
                        SelectedEntity = MasterEntityList.Where(n => n.Id == _currentConnection.BusinessEntityId).FirstOrDefault();
                    }

                    if (SelectedEntity == null)
                    {
                        // set default entity selection
                        SelectedEntity = MasterEntityList.FirstOrDefault();
                    }

                    // set default entity
                    _currentConnection.BusinessEntityId = SelectedEntity.Id;


                    this.CurrentStep = DatabaseSetupStep.EntitySelection;

                }
                else
                {
                    // user is not authorised to use this application
                    ModalMessage msg = new ModalMessage();
                    msg.Description = String.Format(CultureInfo.CurrentCulture, Message.Error_UnauthorisedUser);
                    msg.Header = Message.DatabaseSetup_DatabaseCredentialsFailed;
                    msg.Button1Content = Message.Generic_Ok;
                    msg.ButtonCount = 1;

                    msg.Owner = this.AttachedControl;
                    msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    msg.ShowDialog();

                    if (this.CurrentStep == DatabaseSetupStep.SqlInProgress)
                    {
                        this.CurrentStep = DatabaseSetupStep.CreationFailed;
                    }
                    else if (this.CurrentStep == DatabaseSetupStep.AttemptingConnection)
                    {
                        this.CurrentStep = DatabaseSetupStep.ConnectionFailed;
                    }
                    else if (this.CurrentStep == DatabaseSetupStep.ConnectionFailed)
                    {
                        this.IsRetryingConnection = false;
                    }
                }
            }
            else
            {
                // user is not authorised to use this application
                ModalMessage msg = new ModalMessage();
                msg.Description = String.Format(CultureInfo.CurrentCulture, Message.Error_InvalidCredentials);
                msg.Header = Message.DatabaseSetup_DatabaseCredentialsFailed;
                msg.Button1Content = Message.Generic_Ok;
                msg.ButtonCount = 1;

                msg.Owner = this.AttachedControl;
                msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                msg.ShowDialog();

                if (this.CurrentStep == DatabaseSetupStep.SqlInProgress)
                {
                    this.CurrentStep = DatabaseSetupStep.CreationFailed;
                }
                else if (this.CurrentStep == DatabaseSetupStep.AttemptingConnection)
                {
                    this.CurrentStep = DatabaseSetupStep.ConnectionFailed;
                }
                else if (this.CurrentStep == DatabaseSetupStep.ConnectionFailed)
                {
                    this.IsRetryingConnection = false;
                }
            }
        }

        #endregion

        #region HelperMethods

        /// <summary>
        /// Validates if connection window required
        /// </summary>
        /// <returns>True if connection window required, else false</returns>
        public Boolean DatabaseSetupWindowRequired()
        {
            Boolean returnVal = true;

            if (_currentConnection != null && _currentSettings.IsDatabaseSelectionRemembered && _currentSettings.IsEntitySelectionRemembered)
            {

                DalFactoryConfigElement dalDatabaseFactoryConfig = new DalFactoryConfigElement(cMssql);
                dalDatabaseFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", _currentConnection.ServerName));
                dalDatabaseFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", _currentConnection.DatabaseName));
                IDalFactory dalSqlFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalDatabaseFactoryConfig);
                DalContainer.RegisterFactory(cMssql, dalSqlFactory);
                DalContainer.DalName = cMssql;

                if (dalSqlFactory.IsDatabaseAvailable())
                {
                    DoWorkEventArgs e = new DoWorkEventArgs(null);

                    if (!ValidDatabaseVersion(e, dalSqlFactory))
                    {
                        this.CurrentStep = DatabaseSetupStep.DatabaseIncompatible;
                    }
                    else
                    {
                        returnVal = false;
                    }

                    this.IsDatabaseAvailable = true;
                }
                else
                {
                    //[16763] Database doesn't exist
                    this.CurrentStep = DatabaseSetupStep.ConnectionFailed;
                    returnVal = true;
                    this.IsDatabaseAvailable = false;
                }



                App.ViewState.EntityId = _currentConnection.BusinessEntityId;
            }

            return returnVal;
        }

        /// <summary>
        /// sets default connection details for current type if not set and is available
        /// </summary>
        private void SetDefaultConnection()
        {
            if (this.AttachedControl != null)
            {
                Connection defaultConnection = _currentSettings.Connections.LastOrDefault(c => c.IsCurrentConnection);

                if (defaultConnection == null)
                {
                    defaultConnection = _currentSettings.Connections.LastOrDefault();
                }

                if (defaultConnection != null)
                {
                    this.ServerName = defaultConnection.ServerName;
                    this.DatabaseName = defaultConnection.DatabaseName;
                }

            }
        }

        /// <summary>
        /// Load previous connection options
        /// </summary>
        private void LoadPreviousConnectionDetails()
        {
            _serverNames.Clear();
            _databaseNames.Clear();
            _databaseFileNames.Clear();

            foreach (Connection item in _currentSettings.Connections)
            {
                if (!_serverNames.Contains(item.ServerName)) _serverNames.Add(item.ServerName);
                if (!_databaseNames.Contains(item.DatabaseName)) _databaseNames.Add(item.DatabaseName);
                if (!_databaseFileNames.Contains(item.DatabaseFileName)) _databaseFileNames.Add(item.DatabaseFileName);
            }
        }

        /// <summary>
        /// Remember current selections and proceed to connection complete
        /// </summary>
        private void DatabaseAndEntitySelectionComplete()
        {
            //check if there is an existing connection
            Connection conn = GetExistingConnection();
            if (conn == null)
            {
                //create and add the new connection
                conn = Connection.NewConnection();

                conn.ServerName = _serverName;
                conn.DatabaseName = _databaseName;


                _currentSettings.Connections.Add(conn);
            }

            //update selection values
            conn.BusinessEntityId = this.SelectedEntity.Id;
            conn.IsAutoConnect = (_currentSettings.IsDatabaseSelectionRemembered && _currentSettings.IsEntitySelectionRemembered);
            conn.IsCurrentConnection = true;


            //Make all other connections IsCurrentConnection set to false
            _currentSettings.ResetCurrentConnection(conn);

            if (_newConnection)
            {
                // first time connect set the user settings UOM to the entites UOM
                Entity entity = Entity.FetchById(this.SelectedEntity.Id);
                //_currentSettings.DisplayUnitOfMeasure = entity.SystemSettings.DataSourceUnitOfMeasure;
            }

            // Save settings
            _currentSettings.Save();

            App.ViewState.EntityId = conn.BusinessEntityId;

            // Connection complete, send the completed event
            this.DialogResult = true;
            //if (ConnectionCompleted != null)
            //{
            //    ConnectionCompleted(this, new EventArgs());
            //}
        }

        /// <summary>
        /// Return the existing connection with criteria matching the 
        /// current values
        /// </summary>
        /// <returns></returns>
        private Connection GetExistingConnection()
        {
            Connection conn = null;


            foreach (Connection existingconn in _currentSettings.Connections)
            {
                if (existingconn.ServerName == _serverName &&
                    existingconn.DatabaseName == _databaseName)
                {
                    conn = existingconn;
                    break;
                }
            }

            return conn;
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    _currentConnection = null;
                    _currentSettings = null;
                    _serverNames = null;
                    _databaseNames = null;
                    _databaseFileNames = null;
                    _availableConnectionTypesRO = null;
                    _backgroundWorker = null;
                    _entityInfoList = null;
                    _selectedEntity = null;
                    _dalFactory = null;
                    this.AttachedControl = null;
                }
                this.IsDisposed = true;
            }
        }

        #endregion
    }

    #region Supporting classes

    /// <summary>
    /// Denotes the steps of the database setup window.
    /// </summary>
    public enum DatabaseSetupStep
    {
        SelectDbType,
        SqlEnterDetails,
        SqlInProgress,
        CreationFailed,
        ExistingDatabase,
        AttemptingConnection,
        DatabaseIncompatible,
        ConnectionFailed,
        VerifyInProgress,
        VerificationFailed,
        ErrorOccured,
        EntitySelection
    }

    /// <summary>
    /// Denotes the different setup types.
    /// </summary>
    public enum DatabaseSetupType
    {
        SQL = 0,
        Existing = 1,
    }

    /// <summary>
    /// Denotes the database setup error types.
    /// </summary>
    public enum DatabaseErrorSetupType
    {
        None,
        UpgradeRequired,
        IncompatibleDatabase,
        IncompatibleSoftware
    }

    #endregion
}