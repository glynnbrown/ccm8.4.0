﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25559 : L.Hodson
//  Copied from SA
#endregion
#endregion


using System;
using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.Startup
{
    /// <summary>
    /// Interaction logic for DatabaseSetupWindow.xaml
    /// </summary>
    public partial class DatabaseSetupWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(DatabaseSetupViewModel), typeof(DatabaseSetupWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public DatabaseSetupViewModel ViewModel
        {
            get { return (DatabaseSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            DatabaseSetupWindow senderControl = (DatabaseSetupWindow)obj;

            if (e.OldValue != null)
            {
                DatabaseSetupViewModel oldModel = (DatabaseSetupViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                DatabaseSetupViewModel newModel = (DatabaseSetupViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public DatabaseSetupWindow(DatabaseSetupViewModel databaseSetupViewModel)
        {
            InitializeComponent();

            this.ViewModel = databaseSetupViewModel;
        }

        #endregion

        #region window close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
