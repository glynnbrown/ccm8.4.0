﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27732 : L.Ineson
//  Copied from GFS
#endregion
#endregion

using System;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.Startup
{
    /// <summary>
    /// Interaction logic for CEIP.xaml
    /// </summary>
    public partial class CEIPWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(CEIPViewModel), typeof(CEIPWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the attached viewmodel
        /// </summary>
        public CEIPViewModel ViewModel
        {
            get { return (CEIPViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CEIPWindow senderControl = (CEIPWindow)obj;

            if (e.OldValue != null)
            {
                CEIPViewModel oldModel = (CEIPViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                CEIPViewModel newModel = (CEIPViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CEIPWindow(CEIPViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the retry click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SaveUserSettings();
            this.DialogResult = true;
        }

        #endregion

        #region Window Close

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            IDisposable disposableViewModel = this.ViewModel as IDisposable;
            this.ViewModel = null;

            if (disposableViewModel != null)
            {
                disposableViewModel.Dispose();
            }
        }

        #endregion
    }
}
