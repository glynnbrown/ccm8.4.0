﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-28130 : N.Foster
//  Created
#endregion

#endregion

using System;

namespace Galleria.Ccm.Workflow.Client.Wpf
{
    /// <summary>
    /// Contains constant values available to the entire application.
    /// </summary>
    public static class Constants
    {
    }
}
