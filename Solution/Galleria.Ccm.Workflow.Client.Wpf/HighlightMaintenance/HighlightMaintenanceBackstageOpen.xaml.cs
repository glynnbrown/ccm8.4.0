﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 : J.Pickup
//  Created.

#endregion

#endregion


using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.HighlightMaintenance
{
    /// <summary>
    /// Interaction logic for HighlightMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class HighlightMaintenanceBackstageOpen
    {
        #region Fields

        private readonly ObservableCollection<HighlightInfo> _searchResults = new ObservableCollection<HighlightInfo>();

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(HighlightMaintenanceViewModel), typeof(HighlightMaintenanceBackstageOpen),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        public HighlightMaintenanceViewModel ViewModel
        {
            get { return (HighlightMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SearchResults Property

        public static readonly DependencyProperty SearchResultsProperty = DependencyProperty.Register("SearchResults",
            typeof(ReadOnlyObservableCollection<HighlightInfo>),
            typeof(HighlightMaintenanceBackstageOpen),
            new PropertyMetadata(default(ReadOnlyObservableCollection<HighlightInfo>)));

        public ReadOnlyObservableCollection<HighlightInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<HighlightInfo>)GetValue(SearchResultsProperty); }
            set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #region SearchText Property

        public static readonly DependencyProperty SearchTextProperty = DependencyProperty.Register("SearchText",
            typeof(String), typeof(HighlightMaintenanceBackstageOpen), new PropertyMetadata(String.Empty, SearchText_PropertyChanged));

        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public HighlightMaintenanceBackstageOpen()
        {
            SearchResults = new ReadOnlyObservableCollection<HighlightInfo>(_searchResults);
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Invoked when the user double clicks on the search list.
        /// </summary>
        private void XSearchResults_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            if (senderControl == null) return;

            var target = senderControl.SelectedItem as HighlightInfo;
            if (target == null) return;

            ViewModel.OpenCommand.Execute(target.Id);
        }

        /// <summary>
        ///     Invoked when the search text changes.
        /// </summary>
        private static void SearchText_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as HighlightMaintenanceBackstageOpen;
            if (senderControl != null) senderControl.RefreshAvailableLabels();
        }

        /// <summary>
        ///     Invoked when the collection of available highlights changes.
        /// </summary>
        private void AvailableHighlights_BulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            RefreshAvailableLabels();
        }


        /// <summary>
        ///     Invoked when the view model attached to this view changes.
        /// </summary>
        private static void ViewModel_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as HighlightMaintenanceBackstageOpen;
            if (senderControl == null) return;

            var oldModel = e.OldValue as HighlightMaintenanceViewModel;
            if (oldModel != null)
                oldModel.AvailableHighlights.BulkCollectionChanged -=
                    senderControl.AvailableHighlights_BulkCollectionChanged;
            var newModel = e.NewValue as HighlightMaintenanceViewModel;
            if (newModel != null)
                newModel.AvailableHighlights.BulkCollectionChanged +=
                    senderControl.AvailableHighlights_BulkCollectionChanged;
            senderControl.RefreshAvailableLabels();
        }

        #endregion

        #region Methods

        private void RefreshAvailableLabels()
        {
            _searchResults.Clear();

            if (ViewModel == null) return;

            foreach (var match in ViewModel.GetHighlightMatches(SearchText))
            {
                _searchResults.Add(match);
            }
        }
        #endregion

        private void XSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selected = xSearchResults.CurrentItem as HighlightInfo;
                if(selected != null)
                    this.ViewModel.OpenCommand.Execute(selected.Id);
            }
        }
    }
}
