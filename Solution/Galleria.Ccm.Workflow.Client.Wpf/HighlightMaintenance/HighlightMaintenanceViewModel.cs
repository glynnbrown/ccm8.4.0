﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
// V8-24265 J.Pickup
//  Now reselects labels correctly after actions have occurred.
// V8-25436 : L.Luong
//  Changed to fit new design
// V8-27238 : I.George
// ~ Added IsPlanogramActiveProperty
// V8-27117 : J.Pickup
// ~ SelectedHighlight_ChildChanged handler modified so that the preview value updates the source grouping property.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30258 : A.Probyn
//  Reordered actions in Delete_Executed to get around property set exception.
#endregion
#region Version History: (CCM 8.1.1)
// V8-30270 : I.George
//  Removed IsDirty check in save can execute command
#endregion
#endregion

using System;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Collections.Generic;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Globalization;
using Galleria.Framework.Model;
using System.Collections.Specialized;
using System.Linq;
using System.ComponentModel;
using Csla.Core;
using System.Diagnostics;
using System.Windows.Input;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Planograms.Model;


namespace Galleria.Ccm.Workflow.Client.Wpf.HighlightMaintenance
{
    /// <summary>
    /// Viewmodel controller for the HighlightMaintenance window.
    /// </summary>
    public sealed class HighlightMaintenanceViewModel : ViewModelAttachedControlObject<HighlightMaintenanceOrganiser>
    {
        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedHighlightProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.SelectedHighlight);
        public static readonly PropertyPath CharacteristicGroupsProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.CharacteristicGroups);
        public static readonly PropertyPath CurrentHighlightFiltersProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.CurrentHighlightFilters);

        //Commands 
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.CloseCommand);
        
        public static readonly PropertyPath SetFieldCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.SetFieldCommand);
        public static readonly PropertyPath AddCharacteristicCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.AddCharacteristicCommand);
        public static readonly PropertyPath RemoveCharacteristicCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.RemoveCharacteristicCommand);
        public static readonly PropertyPath SetCharacteristicRuleFieldCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.SetCharacteristicRuleFieldCommand);
        public static readonly PropertyPath SetFilterFieldCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.SetFilterFieldCommand);
        public static readonly PropertyPath RemoveFilterCommandProperty = WpfHelper.GetPropertyPath<HighlightMaintenanceViewModel>(p => p.RemoveFilterCommand);       

        #endregion

        #region Fields

        const String _exCategory = "HighlightMaintenance";
        private const Boolean _doNotAttachToCommandManager = false;
        private const Boolean _saveAsCopy = true;
        private Boolean _userHasHighlightCreatePerm;
        private Boolean _userHasHighlightFetchPerm;
        private Boolean _userHasHighlightEditPerm;
        private Boolean _userHasHighlightDeletePerm;
        

        private readonly HighlightInfoListViewModel _highlightInfoListView = new HighlightInfoListViewModel();
        private Highlight _selectedHighlight;

        private readonly HighlightCharacteristicGroupingCollection _characteristicGroups = new HighlightCharacteristicGroupingCollection();
        private readonly BulkObservableCollection<HighlightFilter> _currentHighlightFilters = new BulkObservableCollection<HighlightFilter>();
        private ReadOnlyBulkObservableCollection<HighlightFilter> _currentHighlightFiltersRO;

        #endregion

        #region Properties

        /// <summary>
        /// Return the collection of available highlihgts based on the search criteria
        /// </summary>
        public ReadOnlyBulkObservableCollection<HighlightInfo> AvailableHighlights
        {
            get { return _highlightInfoListView.BindableCollection; }
        }


        /// <summary>
        /// Returns the currently selected Highlight
        /// </summary>
        public Highlight SelectedHighlight
        {
            get
            {
                return _selectedHighlight;
            }
            set
            {
                _selectedHighlight = value;

                OnPropertyChanged(SelectedHighlightProperty);
            }
        }


        /// <summary>
        /// Returns the collection of characteristics for the current highlight.
        /// </summary>
        public HighlightCharacteristicGroupingCollection CharacteristicGroups
        {
            get { return _characteristicGroups; }
        }


        /// <summary>
        /// Returns the collection of filters for the current highlight.
        /// </summary>
        public ReadOnlyBulkObservableCollection<HighlightFilter> CurrentHighlightFilters
        {
            get
            {
                if (_currentHighlightFiltersRO == null)
                {
                    _currentHighlightFiltersRO = new ReadOnlyBulkObservableCollection<HighlightFilter>(_currentHighlightFilters);
                }
                return _currentHighlightFiltersRO;
            }
        }

        #endregion

        #region Commands

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasHighlightEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.SelectedHighlight == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedHighlight.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

           return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     Gets the SaveAs command.
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            if (!_userHasHighlightCreatePerm)
                SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
            else if (this.SelectedHighlight == null)
                SaveAsCommand.DisabledReason = String.Empty;
            else
                return true;

            //must be valid
            if (!this.SelectedHighlight.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return false;
        }

        private void SaveAs_Executed()
        {
            SaveCurrentItem(_saveAsCopy);
        }

        #endregion

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        ///     Gets the New command.
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute(),
                        _doNotAttachToCommandManager)
                    {

                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            if (!_userHasHighlightCreatePerm)
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
            else
                return true;

            return false;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //Remove Events
                _currentHighlightFilters.BulkCollectionChanged -= CurrentHighlightFilters_BulkCollectionChanged;
                _characteristicGroups.BulkCollectionChanged -= CharacteristicGroups_BulkCollectionChanged;
                _selectedHighlight.PropertyChanged -= SelectedHighlight_PropertyChanged;
                _selectedHighlight.ChildChanged -= SelectedHighlight_ChildChanged;
                
                // Create a new item.
                var newHighlight = Highlight.NewHighlight(App.ViewState.EntityId);

                //Clear out our UI collection properties

                _currentHighlightFilters.Clear();
                _characteristicGroups.Clear();

                // Re-initialize incase property was changed.
                newHighlight.MarkGraphAsInitialized();

                this.SelectedHighlight = newHighlight;

                //ReAdd Events now we have reset collections
                _currentHighlightFilters.BulkCollectionChanged += CurrentHighlightFilters_BulkCollectionChanged;
                _characteristicGroups.BulkCollectionChanged += CharacteristicGroups_BulkCollectionChanged;
                _selectedHighlight.PropertyChanged += SelectedHighlight_PropertyChanged;
                _selectedHighlight.ChildChanged += SelectedHighlight_ChildChanged;

                EnsureBlankRows();

                // Close the Backstage.
                LocalHelper.SetRibbonBackstageState(AttachedControl, false);
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Object> _openCommand;

        /// <summary>
        ///     Gets the Open command.
        /// </summary>
        public RelayCommand<Object> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {

                    _openCommand = new RelayCommand<Object>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Object itemId)
        {
            if (!_userHasHighlightFetchPerm)
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
            else if (itemId == null)
                _openCommand.DisabledReason = String.Empty;
            else
                return true;

            return false;
        }

        private void Open_Executed(Object itemId)
        {
            if (!ContinueWithItemChange()) return;

            base.ShowWaitCursor(true);

            try
            {
                //Attempt to load in the new highlight
                
                //Remove Events
                _currentHighlightFilters.BulkCollectionChanged -= CurrentHighlightFilters_BulkCollectionChanged;
                _characteristicGroups.BulkCollectionChanged -= CharacteristicGroups_BulkCollectionChanged;
                _selectedHighlight.PropertyChanged -= SelectedHighlight_PropertyChanged;
                _selectedHighlight.ChildChanged -= SelectedHighlight_ChildChanged;

                _currentHighlightFilters.Clear();
                _characteristicGroups.Clear();

                //ReAdd Events now we have reset collections
                _currentHighlightFilters.BulkCollectionChanged += CurrentHighlightFilters_BulkCollectionChanged;
                _characteristicGroups.BulkCollectionChanged += CharacteristicGroups_BulkCollectionChanged;
                
                if (itemId != null) this.SelectedHighlight = Highlight.FetchById(itemId);

                _selectedHighlight.PropertyChanged += SelectedHighlight_PropertyChanged;
                _selectedHighlight.ChildChanged += SelectedHighlight_ChildChanged;

                



                _currentHighlightFilters.AddRange(SelectedHighlight.Filters);

                List<HighlightCharacteristicGrouping> listOfCharacteristics = new List<HighlightCharacteristicGrouping>();
                foreach (HighlightCharacteristic currentCharacteristic in SelectedHighlight.Characteristics)
                {
                    listOfCharacteristics.Add(new HighlightCharacteristicGrouping(currentCharacteristic));
                }

                CharacteristicGroups.AddRange(listOfCharacteristics);

                EnsureBlankRows();
            }
            catch (DataPortalException e)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(e, _exCategory);
                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                base.ShowWaitCursor(true);
            }

            // Close the backstage.
            LocalHelper.SetRibbonBackstageState(AttachedControl, false);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        ///     Gets the SaveAndNew command.
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            if (SaveCurrentItem()) NewCommand.Execute();
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        ///     Gets the SaveAndClose command.
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
           if (SaveCurrentItem() && AttachedControl != null) AttachedControl.Close();
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        ///     Gets the Delete command.
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(),
                        p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            if (!(this.SelectedHighlight is Highlight))
            {
                _deleteCommand.DisabledReason = String.Empty;
            }
            if (!_userHasHighlightDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
            }
            else if (this.SelectedHighlight == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
            }
            else if (this.SelectedHighlight.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
            }
            else
            {
                return true;
            }

            return false;
        }

        private void Delete_Executed()
        {
            // Make sure the item can be deleted
            var itemToDelete = this.SelectedHighlight as Highlight;
            if (itemToDelete == null) return;

            ////  If there is an AttachedControl AND the user does NOT confirm the deletion, cancel execution.
            if (AttachedControl != null && !Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedHighlight.ToString())) return;

            ShowWaitCursor(true);

            // Create a new item.
            NewCommand.Execute();

            // Mark the item as deleted, so the item does not show on the list anymore.
            itemToDelete.Delete();            

            // Commit the deletion.
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException e)
            {
                ShowWaitCursor(false);

               LocalHelper.RecordException(e, _exCategory);
                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                ShowWaitCursor(true);
            }

            //// Update the available items.
            _highlightInfoListView.FetchAllForEntity();

            ShowWaitCursor(false);
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        ///     Gets the Close command.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (AttachedControl != null) AttachedControl.Close();
        }

        #endregion

        #region SetFieldCommand

        private RelayCommand _setFieldCommand;

        /// <summary>
        /// Shows the window to populate the field specified by the param
        /// PARM: The field property info.
        /// </summary>
        public RelayCommand SetFieldCommand
        {
            get
            {
                if (_setFieldCommand == null)
                {
                    _setFieldCommand = new RelayCommand(
                        p => SetField_Executed(p),
                        p => SetField_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.HighlightMaintenance_SetField_Desc
                    };
                    base.ViewModelCommands.Add(_setFieldCommand);
                }
                return _setFieldCommand;

            }
        }

        private Boolean SetField_CanExecute(Object args)
        {
            if (this.SelectedHighlight == null)
            {
                return false;
            }

            if (args == null)
            {
                return false;
            }

            return true;
        }

        private void SetField_Executed(Object args)
        {
            Csla.Core.IPropertyInfo fieldPropertyInfo = args as Csla.Core.IPropertyInfo;
            if (fieldPropertyInfo != null)
            {
                if (this.AttachedControl != null)
                {
                    String fieldValue = null;

                    if (fieldPropertyInfo == Highlight.Field1Property)
                    {
                        fieldValue = this.SelectedHighlight.Field1;
                    }
                    else if (fieldPropertyInfo == Highlight.Field2Property)
                    {
                        fieldValue = this.SelectedHighlight.Field2;
                    }

                    FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                        FieldSelectorInputType.Formula, FieldSelectorResolveType.SingleValue,
                        fieldValue, this.GetPlanogramFieldSelectorGroups());

                    viewModel.ShowDialog(this.AttachedControl);

                    if (viewModel.DialogResult == true)
                    {
                        if (fieldPropertyInfo == Highlight.Field1Property)
                        {
                            this.SelectedHighlight.Field1 = viewModel.FieldText;
                        }
                        else if (fieldPropertyInfo == Highlight.Field2Property)
                        {
                            this.SelectedHighlight.Field2 = viewModel.FieldText;
                        }
                    }
                }
            }
        }

        #endregion

        #region AddCharacteristicCommand

        private RelayCommand _addCharacteristicCommand;

        /// <summary>
        /// Adds a new item for characteristic analysis.
        /// </summary>
        public RelayCommand AddCharacteristicCommand
        {
            get
            {
                if (_addCharacteristicCommand == null)
                {
                    _addCharacteristicCommand = new RelayCommand(
                        p => AddCharacteristic_Executed(),
                        p => AddCharacteristic_CanExecute())
                    {
                        FriendlyName = Message.Generic_Add,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addCharacteristicCommand);
                }
                return _addCharacteristicCommand;
            }
        }

        private Boolean AddCharacteristic_CanExecute()
        {
            if (this.SelectedHighlight == null)
            {
                return false;
            }
            return true;
        }

        private void AddCharacteristic_Executed()
        {
            Int32 groupNum = this.SelectedHighlight.Characteristics.Count + 1;

            HighlightCharacteristic newCharacteristic = HighlightCharacteristic.NewHighlightCharacteristic();
            newCharacteristic.Name = String.Format(CultureInfo.CurrentCulture, Message.HighlightMaintenance_NewCharacteristicNameFormat, groupNum);
            this.SelectedHighlight.Characteristics.Add(newCharacteristic);

        }

        #endregion

        #region RemoveCharacteristicCommand

        private RelayCommand _removeCharacteristicCommand;

        /// <summary>
        /// Removes the given aspect
        ///  PARAM: HighlightCharacteristic
        /// </summary>
        public RelayCommand RemoveCharacteristicCommand
        {
            get
            {
                if (_removeCharacteristicCommand == null)
                {
                    _removeCharacteristicCommand = new RelayCommand(
                        p => RemoveCharacteristic_Executed(p),
                        p => RemoveCharacteristic_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeCharacteristicCommand);
                }
                return _removeCharacteristicCommand;
            }
        }

        private Boolean RemoveCharacteristic_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }

            //Cannot remove the last group
            if (this.SelectedHighlight.Characteristics.Count == 1)
            {
                return false;
            }

            return true;
        }

        private void RemoveCharacteristic_Executed(Object args)
        {
            HighlightCharacteristic modelToRemove = null;

            HighlightCharacteristicGrouping view = args as HighlightCharacteristicGrouping;
            if (view != null)
            {
                modelToRemove = view.Model;
            }
            else
            {
                modelToRemove = args as HighlightCharacteristic;
            }

            if (modelToRemove != null)
            {
                modelToRemove.Parent.Characteristics.Remove(modelToRemove);
            }
        }

        #endregion

        #region SetCharacteristicRuleFieldCommand

        private RelayCommand _setCharacteristicRuleFieldCommand;

        /// <summary>
        /// Sets the field value for the given aspect rule
        /// PARAM: HighlightCharacteristicRule
        /// </summary>
        public RelayCommand SetCharacteristicRuleFieldCommand
        {
            get
            {
                if (_setCharacteristicRuleFieldCommand == null)
                {
                    _setCharacteristicRuleFieldCommand = new RelayCommand(
                        p => SetCharacteristicRuleField_Executed(p))
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.HighlightMaintenance_SetField_Desc
                    };
                    base.ViewModelCommands.Add(_setCharacteristicRuleFieldCommand);
                }
                return _setCharacteristicRuleFieldCommand;

            }
        }

        private void SetCharacteristicRuleField_Executed(Object args)
        {
            HighlightCharacteristicRule rule = args as HighlightCharacteristicRule;
            if (rule != null)
            {
                if (this.AttachedControl != null)
                {
                    FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                        FieldSelectorInputType.Formula,
                        FieldSelectorResolveType.SingleValue,
                        rule.Field, this.GetPlanogramFieldSelectorGroups());

                    viewModel.ShowDialog(this.AttachedControl);

                    if (viewModel.DialogResult == true)
                    {
                        rule.Field = viewModel.FieldText;
                    }
                }
            }
        }

        #endregion

        #region RemoveCharacteristicRuleCommand

        private RelayCommand _removeCharacteristicRuleCommand;

        /// <summary>
        /// Removes the given filter.
        /// </summary>
        public RelayCommand RemoveCharacteristicRuleCommand
        {
            get
            {
                if (_removeCharacteristicRuleCommand == null)
                {
                    _removeCharacteristicRuleCommand = new RelayCommand(
                        p => RemoveCharacteristicRule_Executed(p),
                        p => RemoveCharacteristicRule_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeCharacteristicRuleCommand);
                }
                return _removeCharacteristicRuleCommand;
            }
        }

        private Boolean RemoveCharacteristicRule_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }
            return true;
        }

        private void RemoveCharacteristicRule_Executed(Object args)
        {
            HighlightCharacteristicRule rule = args as HighlightCharacteristicRule;
            if (rule != null)
            {
                HighlightCharacteristic parentSetting = rule.Parent;
                if (parentSetting != null)
                {
                    parentSetting.Rules.Remove(rule);
                }
            }

            EnsureBlankRows();
        }

        #endregion

        #region SetFilterFieldCommand

        private RelayCommand _setFilterFieldCommand;

        /// <summary>
        /// Shows the window to populate the group field
        /// </summary>
        public RelayCommand SetFilterFieldCommand
        {
            get
            {
                if (_setFilterFieldCommand == null)
                {
                    _setFilterFieldCommand = new RelayCommand(
                        p => SetFilterField_Executed(p),
                        p => SetFilterField_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        FriendlyDescription = Message.HighlightMaintenance_SetField_Desc
                    };
                    base.ViewModelCommands.Add(_setFilterFieldCommand);
                }
                return _setFilterFieldCommand;

            }
        }

        private Boolean SetFilterField_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }

            return true;
        }

        private void SetFilterField_Executed(Object args)
        {
            HighlightFilter filter = args as HighlightFilter;
            if (filter != null)
            {
                if (this.AttachedControl != null)
                {
                    FieldSelectorViewModel viewModel = new FieldSelectorViewModel(
                        FieldSelectorInputType.Formula,
                        FieldSelectorResolveType.SingleValue,
                        filter.Field, this.GetPlanogramFieldSelectorGroups());

                    viewModel.ShowDialog(this.AttachedControl);

                    if (viewModel.DialogResult == true)
                    {
                        filter.Field = viewModel.FieldText;
                    }
                }
            }
        }

        #endregion

        #region RemoveFilterCommand

        private RelayCommand _removeFilterCommand;

        /// <summary>
        /// Removes the given filter.
        /// </summary>
        public RelayCommand RemoveFilterCommand
        {
            get
            {
                if (_removeFilterCommand == null)
                {
                    _removeFilterCommand = new RelayCommand(
                        p => RemoveFilter_Executed(p),
                        p => RemoveFilter_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeFilterCommand);
                }
                return _removeFilterCommand;
            }
        }

        private Boolean RemoveFilter_CanExecute(Object args)
        {
            if (args == null)
            {
                return false;
            }
            return true;
        }

        private void RemoveFilter_Executed(Object args)
        {
            HighlightFilter filter = args as HighlightFilter;
            if (filter != null)
            {
                if (this.CurrentHighlightFilters.Contains(filter))
                {
                    _currentHighlightFilters.Remove(filter);
                }

                if (this.SelectedHighlight.Filters.Contains(filter))
                {
                    this.SelectedHighlight.Filters.Remove(filter);
                }
            }

            EnsureBlankRows();
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="currentActiveSetting"></param>
        /// <param name="activePlanogram"></param>
        public HighlightMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            _selectedHighlight = Highlight.NewHighlight(App.ViewState.EntityId);
            _highlightInfoListView.FetchAllForEntity();

            _currentHighlightFilters.BulkCollectionChanged += CurrentHighlightFilters_BulkCollectionChanged;
            _characteristicGroups.BulkCollectionChanged += CharacteristicGroups_BulkCollectionChanged;
            _selectedHighlight.PropertyChanged += SelectedHighlight_PropertyChanged;
            _selectedHighlight.ChildChanged += SelectedHighlight_ChildChanged;

            EnsureBlankRows();            
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //create
            _userHasHighlightCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(Highlight));

            //fetch
            _userHasHighlightFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(Highlight));

            //edit
            _userHasHighlightEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(Highlight));

            //delete
            _userHasHighlightDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(Highlight));
        }

        /// <summary>
        /// Makes an attempt to save with incuded checks. Returns successful flag.
        /// </summary>
        private Boolean SaveCurrentItem(Boolean saveAsCopy = false)
        {
            var itemToSave = this.SelectedHighlight as Highlight;
            if (itemToSave == null) return false;

            var continueWithSave = true;
            var currentId = saveAsCopy ? 0 : itemToSave.Id;
            String newName;
            var isNameAccepted = true;

            if (AttachedControl != null)
            {
                var existingItems = HighlightInfoList.FetchByEntityIdIncludingDeleted(App.ViewState.EntityId);

                var dialogTitle = saveAsCopy ? Message.Generic_SaveAs : String.Empty;
                var forceFirstShowDescription = saveAsCopy ? Message.Generic_SaveAs_NameDescription : String.Empty;
                var forceFirstShow = saveAsCopy;
                isNameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(dialogTitle, forceFirstShowDescription,
                    Message.LabelMaintenance_SaveCurrentItem_DisabledReasonNameIsNotUnique,
                    Highlight.NameProperty.FriendlyName, 50, forceFirstShow,
                    IsUniqueCheck(currentId, existingItems), itemToSave.Name, out newName);
            }
            else
            {
                // Force a unique value for unit testing.
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(itemToSave.Name,
                    AvailableHighlights.Where(o => saveAsCopy || o.Id != currentId).Select(o => o.Name));
            }

            if (!isNameAccepted) return false; // Cancel saving without an accepted valid name.

            try
            {
                if (!Equals(this.SelectedHighlight.Name, newName)) this.SelectedHighlight.Name = newName;

                ShowWaitCursor(true);

                if (saveAsCopy)
                {
                    var copy = itemToSave.Copy();
                    copy.Name = newName;
                    this.SelectedHighlight = copy.Save();
                }
                else this.SelectedHighlight = itemToSave.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                continueWithSave = false;
                Galleria.Ccm.Workflow.Client.Wpf.Common.LocalHelper.RecordException(ex, _exCategory);
                var baseException = ex.GetBaseException();

                if (baseException is ConcurrencyException)
                {
                    var isItemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(itemToSave.Name);
                    if (isItemReloadRequired)
                    {
                        this.SelectedHighlight = Highlight.FetchById(itemToSave.Id);
                    }
                }
                else
                {
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToSave.Name, OperationType.Save);
                }
                ShowWaitCursor(true);
            }

            _highlightInfoListView.FetchAllForEntity();
            ShowWaitCursor(false);

            return continueWithSave;
        }

        /// <summary>
        /// Ensures the uniqueness of the provded HighlightInfo
        /// </summary>
        private Predicate<String> IsUniqueCheck(Object currentId, IEnumerable<HighlightInfo> existingItems)
        {
            return newName =>
            {
                ShowWaitCursor(true);
                Boolean isDuplicateMatch = existingItems.Any(info => info.Name == newName && !currentId.Equals(info.Id));
                ShowWaitCursor(false);
                return !isDuplicateMatch;
            };
        }

        /// <summary>
        /// Shows a warning requesting the user ok to continue if the current item is dirty.
        /// </summary>
        /// <returns><c>True</c> if the action should continue, <c>false</c> otherwise.</returns>
        public Boolean ContinueWithItemChange()
        {
            var businessBase = this.SelectedHighlight as BusinessBase;
            return AttachedControl == null || businessBase == null || Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(businessBase, SaveCommand);
        }

        /// <summary>
        /// Gets a list of planogram fields for the field selector.
        /// </summary>
        private List<FieldSelectorGroup> GetPlanogramFieldSelectorGroups()
        {
            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();
            Dictionary<PlanogramItemType, IEnumerable<ObjectFieldInfo>> fields = PlanogramFieldHelper.GetPlanogramObjectFieldInfos();

            foreach (KeyValuePair<PlanogramItemType, IEnumerable<ObjectFieldInfo>> field in fields)
            {
                groups.Add(new FieldSelectorGroup(field.Key.ToString(), field.Value));
            }

            return groups;
        }

        /// <summary>
        /// Adds a blank rows if required.
        /// </summary>
        private void EnsureBlankRows()
        {
            Highlight highlight = this.SelectedHighlight;
            if (highlight != null)
            {
                //add a new blank filter if required.
                if (_currentHighlightFilters.Count == 0 ||
                    !_currentHighlightFilters.Any(c => c.Parent == null))
                {
                    _currentHighlightFilters.Add(HighlightFilter.NewHighlightFilter());
                }

                if (highlight.MethodType == HighlightMethodType.Characteristic)
                {
                    if (highlight.Characteristics.Count == 0)//|| highlight.Characteristics.Any(c => c.Rules.LastOrDefault().Field != String.Empty))
                    {
                        this.AddCharacteristicCommand.Execute();
                    }

                }
            }
        }

        /// <summary>
        /// Gathers the matches for the backstage
        /// </summary>
        public IEnumerable<HighlightInfo> GetHighlightMatches(String nameCriteria)
        {
            var invariantCriteria = nameCriteria.ToLowerInvariant();
            return AvailableHighlights
                .Where(info => info.Name.ToLowerInvariant().Contains(invariantCriteria))
                .OrderBy(info => info.ToString());
        }

        #endregion

        #region Nested classes

        /// <summary>
        /// Collection of HighlightMaintenanceCharacteristicGroupings
        /// </summary>
        public sealed class HighlightCharacteristicGroupingCollection
            : ModelViewCollection<HighlightCharacteristicGrouping, HighlightCharacteristicList, HighlightCharacteristic>
        {
            /// <summary>
            /// Creates a new instance of this type.
            /// </summary>
            public HighlightCharacteristicGroupingCollection()
            {
                this.CreateViewMethod = CreateView;
            }

            /// <summary>
            /// Creates a new view from the given model.
            /// </summary>
            /// <param name="model"></param>
            /// <returns></returns>
            private HighlightCharacteristicGrouping CreateView(HighlightCharacteristic model)
            {
                return new HighlightCharacteristicGrouping(model);
            }
        }


        /// <summary>
        /// Row view for a characteristic grouping.
        /// </summary>
        public sealed class HighlightCharacteristicGrouping : ModelView<HighlightCharacteristic>
        {
            #region Fields

            private readonly BulkObservableCollection<HighlightCharacteristicRule> _rules = new BulkObservableCollection<HighlightCharacteristicRule>();
            private ReadOnlyBulkObservableCollection<HighlightCharacteristicRule> _rulesRO;

            #endregion

            #region Properties

            /// <summary>
            /// Returns the collection of rules for this row.
            /// Includes a blank.
            /// </summary>
            public ReadOnlyBulkObservableCollection<HighlightCharacteristicRule> Rules
            {
                get
                {
                    if (_rulesRO == null)
                    {
                        _rulesRO = new ReadOnlyBulkObservableCollection<HighlightCharacteristicRule>(_rules);
                    }
                    return _rulesRO;
                }
            }

            #endregion

            #region Constructor

            /// <summary>
            /// Creates a new instance of this type.
            /// </summary>
            /// <param name="model"></param>
            public HighlightCharacteristicGrouping(HighlightCharacteristic model)
                : base(model)
            {
                _rules.BulkCollectionChanged += Rules_BulkCollectionChanged;

                model.Rules.BulkCollectionChanged += Characteristic_RulesBulkCollectionChanged;

                Characteristic_RulesBulkCollectionChanged(model,
                    new BulkCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, null));
            }

            #endregion

            #region Event Handlers

            /// <summary>
            /// Called whenever the CurrentCharacteristicRules collection changes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void Rules_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (HighlightCharacteristicRule r in e.ChangedItems)
                        {
                            r.PropertyChanged += HighlightCharacteristicRule_PropertyChanged;
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        foreach (HighlightCharacteristicRule r in e.ChangedItems)
                        {
                            r.PropertyChanged -= HighlightCharacteristicRule_PropertyChanged;
                        }
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        foreach (HighlightCharacteristicRule r in e.ChangedItems)
                        {
                            r.PropertyChanged -= HighlightCharacteristicRule_PropertyChanged;
                        }
                        foreach (HighlightCharacteristicRule r in _rules)
                        {
                            r.PropertyChanged += HighlightCharacteristicRule_PropertyChanged;
                        }
                        break;
                }
            }

            /// <summary>
            /// Called whenever the selected characteristic rules collection changes.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void Characteristic_RulesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (HighlightCharacteristicRule h in e.ChangedItems)
                        {
                            if (!_rules.Contains(h))
                            {
                                _rules.Add(h);
                            }
                        }
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        foreach (HighlightCharacteristicRule h in e.ChangedItems) _rules.Remove(h);
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        _rules.Clear();
                        if (sender != null)
                        {
                            foreach (HighlightCharacteristicRule h in ((HighlightCharacteristic)sender).Rules)
                                _rules.Add(h);
                        }
                        break;
                }

                //ensure there is still a blank row in the collection
                EnsureBlankRow();
            }

            /// <summary>
            /// Called whenever a property changes on a highlight filter.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            private void HighlightCharacteristicRule_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
            {
                HighlightCharacteristicRule rule = (HighlightCharacteristicRule)sender;
                if (rule.Parent == null)
                {
                    if (!String.IsNullOrEmpty(rule.Field))
                    {
                        //the filter was the blank row which is now no longer blank
                        // so add to the main collection.
                        this.Model.Rules.Add(rule);
                    }
                }
            }

            #endregion

            #region Methods

            private void EnsureBlankRow()
            {
                if (_rules.Count == 0 || !_rules.Any(c => c.Parent == null))
                {
                    _rules.Add(HighlightCharacteristicRule.NewHighlightCharacteristicRule());
                }
            }

            #endregion

            #region IDisposable Members

            private Boolean _isDisposed;

            public override void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            private void Dispose(Boolean disposing)
            {
                if (!_isDisposed)
                {
                    if (disposing)
                    {
                        Model.Rules.BulkCollectionChanged -= Characteristic_RulesBulkCollectionChanged;
                        _rules.Clear();
                        _rules.BulkCollectionChanged -= Rules_BulkCollectionChanged;
                    }
                    _isDisposed = true;
                }
            }

            #endregion
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Called whenever the CharacteristicGroups collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CharacteristicGroups_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (HighlightCharacteristicGrouping group in e.ChangedItems)
                        {
                            group.Dispose();
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Reponds to a property change on the selected setting.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedHighlight_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != Highlight.NameProperty.Name)
            {
                if (e.PropertyName == Highlight.MethodTypeProperty.Name)
                {
                    EnsureBlankRows();
                }
            }
        }

        /// <summary>
        /// Event hnadler for the child changed event of SelectedHighlight
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedHighlight_ChildChanged(object sender, ChildChangedEventArgs e)
        {
            //If we are making a change to the HighlightCharacteristicList
            if (e.ChildObject is HighlightCharacteristicList)
            {
                HighlightCharacteristicList list = e.ChildObject as HighlightCharacteristicList;

                //Foreach item that we changed.
                foreach (HighlightCharacteristic highlightCharacteristic in list)
                {
                    //We need to make sure we ADD any charachtertistic groups to the displayed collection that have been added since last update.
                    if (!CharacteristicGroups.Any(c => c.Model.Id == highlightCharacteristic.Id))
                    {
                        HighlightCharacteristicGrouping grping = new HighlightCharacteristicGrouping(highlightCharacteristic);
                        CharacteristicGroups.Add(grping);
                    }

                    //We need to make sure we REMOVE any charactertistic groups to the displayed collection that have been removed since last update.
                    List<HighlightCharacteristic> characteristicsToRemove = new List<HighlightCharacteristic>();

                    foreach (HighlightCharacteristicGrouping existingCharacteristic in CharacteristicGroups)
                    { 
                        var matches = SelectedHighlight.Characteristics.Where(p => p.Id == existingCharacteristic.Model.Id).ToList();

                        if (matches == null || matches.Count == 0)
                        {
                            characteristicsToRemove.Add(existingCharacteristic.Model);
                        }
                    }

                    foreach (HighlightCharacteristic current in characteristicsToRemove)
                    {
                        HighlightCharacteristicGrouping grp = CharacteristicGroups.Where(g => g.Model.Id == current.Id).FirstOrDefault();
                        
                        if (grp != null)
                        {
                            CharacteristicGroups.Remove(grp);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Called whenever the CurrentHighlightFilters collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentHighlightFilters_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged += HighlightFilter_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged -= HighlightFilter_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    foreach (HighlightFilter h in e.ChangedItems)
                    {
                        h.PropertyChanged -= HighlightFilter_PropertyChanged;
                    }
                    foreach (HighlightFilter h in _currentHighlightFilters)
                    {
                        h.PropertyChanged += HighlightFilter_PropertyChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever a property changes on a highlight filter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HighlightFilter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            HighlightFilter filter = (HighlightFilter)sender;
            if (filter.Parent == null)
            {
                if (!String.IsNullOrEmpty(filter.Field))
                {
                    //the filter was the blank row which is now no longer blank
                    // so add to the main collection.
                    this.SelectedHighlight.Filters.Add(filter);
                    _currentHighlightFilters.Add(HighlightFilter.NewHighlightFilter());
                }
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _characteristicGroups.BulkCollectionChanged -= CharacteristicGroups_BulkCollectionChanged;
                    _selectedHighlight.PropertyChanged -= SelectedHighlight_PropertyChanged;
                    _selectedHighlight.ChildChanged -= SelectedHighlight_ChildChanged;

                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}





