﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 : J.Pickup
//  Created.

#endregion

#endregion

using System.Windows;
using Fluent;


namespace Galleria.Ccm.Workflow.Client.Wpf.HighlightMaintenance
{
    /// <summary>
    /// Interaction logic for HighlightMaintenanceHomeTab.xaml
    /// </summary>
    public partial class HighlightMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(HighlightMaintenanceViewModel), typeof(HighlightMaintenanceHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public HighlightMaintenanceViewModel ViewModel
        {
            get { return (HighlightMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public HighlightMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
