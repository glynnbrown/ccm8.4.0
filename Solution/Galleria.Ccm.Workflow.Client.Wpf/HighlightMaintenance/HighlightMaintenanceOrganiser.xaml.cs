﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27941 : J.Pickup
//  Created.

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.HighlightMaintenance
{
    /// <summary>
    /// Interaction logic for HighlightMaintenanceOrganiser.xaml
    /// </summary>
    public partial class HighlightMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(HighlightMaintenanceViewModel), typeof(HighlightMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public HighlightMaintenanceViewModel ViewModel
        {
            get { return (HighlightMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            HighlightMaintenanceOrganiser senderControl = (HighlightMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                HighlightMaintenanceViewModel oldModel = (HighlightMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                HighlightMaintenanceViewModel newModel = (HighlightMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

            }
        }

        #endregion

        #endregion

        #region Constructor

        public HighlightMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.HighlightMaintenance);

            this.ViewModel = new HighlightMaintenanceViewModel();

            this.Loaded += new RoutedEventHandler(HighlightMaintenanceOrganiser_Loaded);
        }

        private void HighlightMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= HighlightMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// calles when ever the keys O+Ctrl is pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        #endregion

        #region Window Close

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
