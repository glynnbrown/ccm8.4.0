﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationClusterMaintenance
{
    /// <summary>
    /// Main page organiser for the location cluster maintenance page
    /// </summary>
    public sealed partial class LocationClusterMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        private bool _isRenaming;
        #endregion

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationClusterMaintenanceViewModel), typeof(LocationClusterMaintenanceOrganiser),
            new PropertyMetadata(null, ViewModel_PropertyChanged));


        private static void ViewModel_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationClusterMaintenanceOrganiser senderControl = (LocationClusterMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                LocationClusterMaintenanceViewModel oldModel = (LocationClusterMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationClusterMaintenanceViewModel newModel = (LocationClusterMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        public LocationClusterMaintenanceViewModel ViewModel
        {
            get { return (LocationClusterMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationClusterMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.LocationClusterMaintenance);

            this.ViewModel = new LocationClusterMaintenanceViewModel();

            this.Loaded += new RoutedEventHandler(LocationClusterMaintenanceOrganiser_Loaded);
        }



        /// <summary>
        /// Carries out intial first load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationClusterMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationClusterMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }


        #endregion

        #region Unassigned Locations Grid

        /// <summary>
        /// Adds the doubleclicked row to the current cluster locations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedLocations_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            List<LocationInfo> selectedLocations = new List<LocationInfo>() { (LocationInfo)e.RowItem };
            this.ViewModel.AddLocationsCommand.Execute(selectedLocations);
        }

        /// <summary>
        /// Removes the passed locations from the current sublevel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedLocations_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            List<LocationInfo> locations = e.Items.Cast<LocationInfo>().ToList();
            this.ViewModel.RemoveLocationsCommand.Execute(locations);
        }

        /// <summary>
        /// Clears the selected items of the assigned grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedLocations_ItemsSelected(object sender, ExtendedDataGridItemSelectedEventArgs e)
        {
            xAssignedLocations.SelectedItems.Clear();
        }

        /// <summary>
        /// Makes an add request for all selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnassignedGridRowContext_AddSelected(object sender, RoutedEventArgs e)
        {
            List<LocationInfo> locationsToAdd = xUnassignedLocations.SelectedItems.Cast<LocationInfo>().ToList();
            this.ViewModel.AddLocationsCommand.Execute(locationsToAdd);
        }

        #endregion

        #region Assigned Locations Grid

        /// <summary>
        /// Removes the location from the current cluster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedLocations_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            List<LocationInfo> selectedLocations = new List<LocationInfo>() { (LocationInfo)e.RowItem };
            this.ViewModel.RemoveLocationsCommand.Execute(selectedLocations);
        }

        /// <summary>
        /// Adds the dropped locations to the cluster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedLocations_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            List<LocationInfo> locations = e.Items.Cast<LocationInfo>().ToList();
            this.ViewModel.AddLocationsCommand.Execute(locations);
        }

        /// <summary>
        /// Makes a remove request for all selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedLocationsRowContext_RemoveSelected(object sender, RoutedEventArgs e)
        {
            List<LocationInfo> removeLocations = xAssignedLocations.SelectedItems.Cast<LocationInfo>().ToList();
            this.ViewModel.RemoveLocationsCommand.Execute(removeLocations);
        }

        #endregion

        #region Clusters list

        /// <summary>
        /// Starts the renaming of a cluster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xClusterList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //get the original textbox source
            System.Windows.Controls.TextBox senderTextBox = ((DependencyObject)e.OriginalSource).FindVisualDescendent<System.Windows.Controls.TextBox>();

            if (senderTextBox != null)
            {
                //enable for renaming
                senderTextBox.IsHitTestVisible = true;
                _isRenaming = true;
            }

        }

        /// <summary>
        /// Disables the sender textbox for editing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtClusterName_LostFocus(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.TextBox senderTextBox = (System.Windows.Controls.TextBox)sender;
            senderTextBox.IsHitTestVisible = false;
            _isRenaming = false;
        }

        /// <summary>
        /// Starts the dragging of the current listbox item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xClusterList_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (!DragDropBehaviour.IsDragging && e.LeftButton == MouseButtonState.Pressed && !_isRenaming)
            {
                //check that the drag source is a listboxitem
                ListBoxItem dragSource = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();

                if (dragSource != null)
                {
                    Cluster dragCluster = (Cluster)dragSource.Content;

                    //[SA-15509] Only start the drag if the cluster is the selected one.
                    if (dragCluster == this.ViewModel.SelectedCluster)
                    {
                        //start the drag
                        DragDropBehaviour dragBehaviour = new DragDropBehaviour(dragCluster);
                        dragBehaviour.DragArea = (FrameworkElement)this.Content;
                        dragBehaviour.BeginDrag();
                    }
                }
            }
        }

        /// <summary>
        /// Handles the drop of data grid rows & also of another cluster item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xClusterList_Drop(object sender, DragEventArgs e)
        {
            //get the hit item
            ListBoxItem hitListBoxItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();

            //drop out if a listbox item was not hit
            if (hitListBoxItem == null) { return; }


            //determine what has been dropped
            if (DragDropBehaviour.IsUnpackedType<ExtendedDataGridRowDropEventArgs>(e.Data))
            {
                //**rows dropped
                ExtendedDataGridRowDropEventArgs rowArgsDropped = DragDropBehaviour.UnpackData<ExtendedDataGridRowDropEventArgs>(e.Data);
                Cluster toCluster = (Cluster)hitListBoxItem.Content;
                List<LocationInfo> locations = rowArgsDropped.Items.Cast<LocationInfo>().ToList();
                this.ViewModel.MoveLocations(locations, this.ViewModel.SelectedCluster, toCluster);
            }

            else if (DragDropBehaviour.IsUnpackedType<Cluster>(e.Data))
            {
                //** cluster dropped            
                Cluster toCluster = (Cluster)hitListBoxItem.Content;
                this.ViewModel.MergeClusters(this.ViewModel.SelectedCluster, toCluster);
            }

        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //load the unassigned locs list
            DataGridColumnCollection unassignedLocationsColumnSet = new DataGridColumnCollection();
            List<DataGridColumn> unassignedLocationsCols = DataObjectViewHelper.GetBoundLocationInfoColumnSet();
            foreach (DataGridColumn col in unassignedLocationsCols)
            {
                unassignedLocationsColumnSet.Add(col);
            }
            //Add datedeleted column
            DataGridDateTimeColumn dtCol = new DataGridDateTimeColumn();
            dtCol.Header = LocationInfo.DateDeletedProperty.Name;
            dtCol.Binding = new Binding(LocationInfo.DateDeletedProperty.Name) { Mode = BindingMode.OneWay };
            dtCol.IsReadOnly = true;
            dtCol.SelectedDateFormat = DatePickerFormat.Short;
            unassignedLocationsColumnSet.Add(dtCol);

            xUnassignedLocations.ColumnSet = unassignedLocationsColumnSet;

            //load the assigned locs list
            DataGridColumnCollection assignedLocationsColumnSet = new DataGridColumnCollection();
            List<DataGridColumn> assignedLocationsCols = DataObjectViewHelper.GetBoundLocationInfoColumnSet();
            foreach (DataGridColumn col in assignedLocationsCols)
            {
                assignedLocationsColumnSet.Add(col);
            }
            //Add datedeleted column
            DataGridDateTimeColumn dtCol2 = new DataGridDateTimeColumn();
            dtCol2.Header = LocationInfo.DateDeletedProperty.Name;
            dtCol2.Binding = new Binding(LocationInfo.DateDeletedProperty.Name) { Mode = BindingMode.OneWay };
            dtCol2.IsReadOnly = true;
            dtCol2.SelectedDateFormat = DatePickerFormat.Short;
            assignedLocationsColumnSet.Add(dtCol2);

            xAssignedLocations.ColumnSet = assignedLocationsColumnSet;
        }


        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        /// <summary>
        /// Section 508 grid functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAssigned = senderControl?.Name == xAssignedLocations.Name;

            if (e.Key == Key.Return)
            {
                if (!isAssigned) this.ViewModel?.AddLocationsCommand.Execute();
                else this.ViewModel?.RemoveLocationsCommand.Execute();
                Keyboard.Focus(isAssigned ? xAssignedLocations : xUnassignedLocations);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAssigned ? xUnassignedLocations : xAssignedLocations);
                e.Handled = true;
            }
        }

        #region Synchronise Grid Columns
        /// <summary>
        /// Synchronise the column visibilities in both grids
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedLocations_ColumnVisibilityChanged(object sender, Galleria.Framework.Model.EventArgs<DataGridColumn> e)
        {
            if (e.ReturnValue != null)
            {
                DataGridColumn changedColumn = (DataGridColumn)e.ReturnValue;
                foreach (DataGridColumn col in xAssignedLocations.Columns)
                {
                    if (col.Header.ToString() == changedColumn.Header.ToString())
                    {
                        xAssignedLocations.Columns[col.DisplayIndex].Visibility = changedColumn.Visibility;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Synchronise the column visibilities in both grids
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedLocations_ColumnVisibilityChanged(object sender, Galleria.Framework.Model.EventArgs<DataGridColumn> e)
        {
            if (e.ReturnValue != null)
            {
                DataGridColumn changedColumn = (DataGridColumn)e.ReturnValue;
                foreach (DataGridColumn col in xUnassignedLocations.Columns)
                {
                    if (col.Header.ToString() == changedColumn.Header.ToString())
                    {
                        xUnassignedLocations.Columns[col.DisplayIndex].Visibility = changedColumn.Visibility;
                        break;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Window close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {

                    IDisposable disposableViewModel = this.ViewModel;

                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                    foreach (RibbonTabItem tab in this.Ribbon.Tabs.ToArray())
                    {
                        this.Ribbon.Tabs.Remove(tab);
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
       
    }
}
