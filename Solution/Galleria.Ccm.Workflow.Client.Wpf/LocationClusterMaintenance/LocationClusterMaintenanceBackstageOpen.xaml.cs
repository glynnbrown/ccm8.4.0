﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationClusterMaintenance
{
    /// <summary>
    /// Interaction logic for ClusterMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class LocationClusterMaintenanceBackstageOpen : UserControl
    {
        #region Fields
        private ObservableCollection<ClusterSchemeInfo> _searchResults = new ObservableCollection<ClusterSchemeInfo>();
        #endregion

        #region Properties

        #region View Model Property
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationClusterMaintenanceViewModel), typeof(LocationClusterMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));
        public LocationClusterMaintenanceViewModel ViewModel
        {
            get { return (LocationClusterMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationClusterMaintenanceBackstageOpen senderControl = (LocationClusterMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                LocationClusterMaintenanceViewModel oldModel = (LocationClusterMaintenanceViewModel)e.OldValue;
                oldModel.MasterClusterSchemeInfoList.BulkCollectionChanged -= senderControl.ViewModel_AvailableLocationClustersBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                LocationClusterMaintenanceViewModel newModel = (LocationClusterMaintenanceViewModel)e.NewValue;
                newModel.MasterClusterSchemeInfoList.BulkCollectionChanged += senderControl.ViewModel_AvailableLocationClustersBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }
        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(LocationClusterMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationClusterMaintenanceBackstageOpen senderControl = (LocationClusterMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<ClusterSchemeInfo>),
            typeof(LocationClusterMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<ClusterSchemeInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<ClusterSchemeInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationClusterMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<ClusterSchemeInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                IEnumerable<ClusterSchemeInfo> results = this.ViewModel.GetMatchingLocationClusters(this.SearchText);

                foreach (ClusterSchemeInfo locInfo in results.OrderBy(l => l.ToString()))
                {
                    _searchResults.Add(locInfo);
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Updates the search results when the available locations list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableLocationClustersBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        ///// <summary>
        ///// Opens the selected item
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    ListBox senderControl = (ListBox)sender;
        //    if (senderControl.SelectedItem != null)
        //    {
        //        if (this.ViewModel != null)
        //        {
        //            ClusterSchemeInfo selectedCluster = (ClusterSchemeInfo)senderControl.SelectedItem;
        //            this.ViewModel.OpenCommand.Execute(selectedCluster.Id);
        //        }
        //    }
        //}

        private void ExtendedDataGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                ClusterSchemeInfo itemToOpen = e.RowItem as ClusterSchemeInfo;
                if (itemToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                }
            }
        }

        private void Datagrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;
                if (this.ViewModel != null)
                {
                    ClusterSchemeInfo itemToOpen = senderControl?.SelectedItem as ClusterSchemeInfo;
                    if (itemToOpen != null)
                    {
                        //send the store to be opened
                        this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                    }
                }
                e.Handled = true;
            }
        }
        #endregion    
    }
}
