﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// CCM-27078 : I.George
//  Added AssignedCategory command and SelectAssignedCategoryCommandProperty
#endregion
#region Version History: (CCM 8.0.3)
// CCM-29216 : D.Pleasance
//	Amended reference FetchByEntityIdIncludingDeleted to FetchByEntityId as data now deleted outright
#endregion

#region Version History: (CCM 8.10)
// V8-29994 : L.Ineson
//  Amended RemoveAllClusters to just call clear.
// V8-30290 : A.Probyn
//  Added defensive code to RefreshUnassignedLocations
#endregion
#region Version History: (CCM 8.1.1)
// CCM:30325 : I.George
// Added friendly description message to SaveAscommand
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationClusterMaintenance
{
    public sealed class LocationClusterMaintenanceViewModel : ViewModelAttachedControlObject<LocationClusterMaintenanceOrganiser>
    {
        #region Fields
        const String _exCategory = "LocationClusterMaintenance"; //Category name for any exceptions raised to gibraltar from here

        private Boolean _userHasClusterCreatePerm;
        private Boolean _userHasClusterFetchPerm;
        private Boolean _userHasClusterEditPerm;
        private Boolean _userHasClusterDeletePerm;

        private readonly LocationInfoListViewModel _masterLocationInfoListView = new LocationInfoListViewModel();
        private readonly ClusterSchemeInfoListViewModel _masterClusterSchemeInfoListView = new ClusterSchemeInfoListViewModel();
        private ClusterScheme _currentScheme;
        private CollectionViewSource _clusterSchemeClusterSource;
        private Cluster _selectedCluster;
        private LocationList _masterLocationList;

        private ObservableCollection<LocationInfo> _selectedAvailableLocations = new ObservableCollection<LocationInfo>();
        private ObservableCollection<LocationInfo> _selectedTakenLocations = new ObservableCollection<LocationInfo>();
        private String _clustersFilterText = String.Empty;

        private ObservableCollection<LocationInfo> _assignedLocations = new ObservableCollection<LocationInfo>();
        private ReadOnlyObservableCollection<LocationInfo> _assignedLocationsRO;

        private ObservableCollection<LocationInfo> _unassignedLocations = new ObservableCollection<LocationInfo>();
        private ReadOnlyObservableCollection<LocationInfo> _unassignedLocationsRO;

        private ProductGroupInfo _assignedCategory;
        #endregion

        #region Binding Property Paths
        public static PropertyPath MasterLocationInfoListProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.MasterLocationInfoList);
        public static PropertyPath MasterClusterSchemeInfoListProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.MasterClusterSchemeInfoList);
        public static PropertyPath SelectedAvailableLocationsProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.SelectedAvailableLocations);
        public static PropertyPath SelectedTakenLocationsProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.SelectedTakenLocations);
        public static PropertyPath CurrentSchemeProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.CurrentScheme);
        public static PropertyPath ClusterSchemeClustersSourceProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.ClusterSchemeClustersSource);
        public static PropertyPath SelectedClusterProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.SelectedCluster);
        public static PropertyPath AssignedLocationsProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.AssignedLocations);
        public static PropertyPath UnassignedLocationsProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.UnassignedLocations);
        public static PropertyPath ClustersFilterTextProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.ClustersFilterText);
        public static PropertyPath IsUnassignedLocationGridVisibleProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.IsUnassignedLocationGridVisible);
        public static PropertyPath AssignedCategoryProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.AssignedCategory);

        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.NewCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.DeleteCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.OpenCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.CloseCommand);
        public static PropertyPath AddClusterCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.AddClusterCommand);
        public static PropertyPath DeleteClusterCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.DeleteClusterCommand);
        public static PropertyPath RemoveAllClustersCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.RemoveAllClustersCommand);
        public static PropertyPath AutoCreateClustersCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.AutoCreateClustersCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.SaveAndCloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.SaveAndNewCommand);
        public static PropertyPath AddLocationsCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.AddLocationsCommand);
        public static PropertyPath RemoveLocationsCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.RemoveLocationsCommand);
        public static PropertyPath RemoveAllLocationsCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.RemoveAllLocationsCommand);
        public static PropertyPath NewImportFromBehaviouralClusteringCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(v => v.NewImportFromBehaviouralClusteringCommand);
        public static PropertyPath SelectAssignedCategoryCommandProperty = WpfHelper.GetPropertyPath<LocationClusterMaintenanceViewModel>(p => p.SelectAssignedCategoryCommand);
 

        #endregion

        #region Properties

        /// <summary>
        /// Returns the master list of location infos
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationInfo> MasterLocationInfoList
        {
            get { return _masterLocationInfoListView.BindingView; }
        }

        /// <summary>
        /// Returns the master list of cluster scheme infos
        /// </summary>
        public ReadOnlyBulkObservableCollection<ClusterSchemeInfo> MasterClusterSchemeInfoList
        {
            get { return _masterClusterSchemeInfoListView.BindingView; }
        }

        /// <summary>
        /// Returns the collection of selected available locations
        /// </summary>
        public ObservableCollection<LocationInfo> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
        }

        /// <summary>
        /// Returns the collection of selected taken locations
        /// </summary>
        public ObservableCollection<LocationInfo> SelectedTakenLocations
        {
            get { return _selectedTakenLocations; }
        }

        /// <summary>
        /// Gets/Sets the current scheme
        /// </summary>
        public ClusterScheme CurrentScheme
        {
            get { return _currentScheme; }
            set
            {
                ClusterScheme oldValue = _currentScheme;
                _currentScheme = value;
                OnPropertyChanged(CurrentSchemeProperty);
                OnCurrentSchemeChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns an ordered view of the clsuter scheme clusters
        /// To access results cast as IEnumerable Cluster
        /// </summary>
        public ICollectionView ClusterSchemeClustersSource
        {
            get
            {
                if (_clusterSchemeClusterSource == null)
                {
                    _clusterSchemeClusterSource = new CollectionViewSource();
                    _clusterSchemeClusterSource.Source = this.CurrentScheme.Clusters;
                    _clusterSchemeClusterSource.View.Filter = this.ClusterSourceFilter;
                    _clusterSchemeClusterSource.SortDescriptions.Add
                    (new SortDescription(ClusterScheme.NameProperty.Name, ListSortDirection.Ascending));
                }
                return _clusterSchemeClusterSource.View;
            }
        }

        /// <summary>
        /// Gets/Sets the selected cluster
        /// </summary>
        public Cluster SelectedCluster
        {
            get { return _selectedCluster; }
            set
            {
                Cluster oldVal = _selectedCluster;
                _selectedCluster = value;
                OnSelectedClusterChanged(oldVal, value);
                OnPropertyChanged(SelectedClusterProperty);
            }
        }

        /// <summary>
        /// Gets the list of locations assigned to the selected cluster
        /// </summary>
        public ReadOnlyObservableCollection<LocationInfo> AssignedLocations
        {
            get
            {
                if (_assignedLocationsRO == null)
                {
                    _assignedLocationsRO = new ReadOnlyObservableCollection<LocationInfo>(_assignedLocations);
                }
                return _assignedLocationsRO;
            }
        }

        /// <summary>
        /// Gets the list of locations not assigned to the selected cluster
        /// </summary>
        public ReadOnlyObservableCollection<LocationInfo> UnassignedLocations
        {
            get
            {
                if (_unassignedLocationsRO == null)
                {
                    _unassignedLocationsRO = new ReadOnlyObservableCollection<LocationInfo>(_unassignedLocations);
                }
                return _unassignedLocationsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the text used to filter the available associatives list
        /// </summary>
        public String ClustersFilterText
        {
            get { return _clustersFilterText; }
            set
            {
                _clustersFilterText = value;
                OnPropertyChanged(ClustersFilterTextProperty);

                //filter the clusters source to match
                if (this._clusterSchemeClusterSource != null)
                {
                    this._clusterSchemeClusterSource.View.Filter = this.ClusterSourceFilter;
                }
            }
        }

        /// <summary>
        /// Gets whether the unassigned location controls should be visible
        /// </summary>
        public Boolean IsUnassignedLocationGridVisible
        {
            get
            {
                return (this.UnassignedLocations.Count > 0);
            }
        }

        public ProductGroupInfo AssignedCategory
        {
            get { return _assignedCategory; }
            set {
                _assignedCategory = value;
                OnPropertyChanged(AssignedCategoryProperty);

                if (this.CurrentScheme != null)
                {
                    this.CurrentScheme.ProductGroupId = (value != null) ? (Int32?)value.Id : null;
                }
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationClusterMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            //location infos
            _masterLocationInfoListView.FetchAllForEntity();

            //cluster scheme infos
            _masterClusterSchemeInfoListView.FetchForCurrentEntity();

            _unassignedLocations.CollectionChanged += UnassignedLocations_CollectionChanged;

            //load a new scheme
            NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;
        /// <summary>
        /// Loads a new scheme
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    this.ViewModelCommands.Add(_newCommand);

                }
                return _newCommand;
            }
        }


        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userHasClusterCreatePerm)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //create a new blank cluster scheme
                ClusterScheme newClusterScheme = ClusterScheme.NewClusterScheme(App.ViewState.EntityId);

                //add an initial cluster
                Cluster initialCluster = Cluster.NewCluster();
                initialCluster.Name = Message.Generic_New;
                newClusterScheme.Clusters.Add(initialCluster);

                //if no other schemes exist in the db make this the default.
                if (this.MasterClusterSchemeInfoList == null || this.MasterClusterSchemeInfoList.Count ==0)
                {
                    newClusterScheme.IsDefault = true;
                }

                newClusterScheme.MarkGraphAsInitialized(); //re-initialize

                //load into the view
                this.CurrentScheme = newClusterScheme;
                Debug.Assert(this.CurrentScheme.IsInitialized, "New item should start initialized");

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current cluster scheme
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasClusterEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.CurrentScheme == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentScheme.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //all locations must be assigned
            if (this.UnassignedLocations.Count > 0)
            {
                this.SaveCommand.DisabledReason = Message.LocationClusterMaintenance_InvalidScheme_UnassignedLocations;
                this.SaveAndNewCommand.DisabledReason = Message.LocationClusterMaintenance_InvalidScheme_UnassignedLocations;
                this.SaveAndCloseCommand.DisabledReason = Message.LocationClusterMaintenance_InvalidScheme_UnassignedLocations;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Calls a save on the current item
        /// </summary>
        /// <returns>true of successful, false if the save was cancelled</returns>
        private Boolean SaveCurrentItem()
        {
            //check the scheme name is unique prompt user if not
            Boolean continueWithSave = true;

            Int32 id = this.CurrentScheme.Id;
            //** check the item unique value
            String newName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                (s) =>
                {
                    Boolean returnValue = true;
                    base.ShowWaitCursor(true);

                    foreach (ClusterSchemeInfo clusInfo in ClusterSchemeInfoList.FetchByEntityId(App.ViewState.EntityId))
                    {
                        if(clusInfo.Id != id && clusInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                        {
                            returnValue = false;
                            break;
                        }
                    }
                   
                    base.ShowWaitCursor(false);
                    return returnValue;
                };

                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(String.Empty, String.Empty,
                    Message.LocationCluster_CodeAlreadyInUse,
                    ClusterScheme.NameProperty.FriendlyName, 50, false, isUniqueCheck, this.CurrentScheme.Name, out newName);
            }
            else
            {
                //force a unique value for unit testing
                //Int32 id = this.CurrentScheme.Id;
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.CurrentScheme.Name,
                    this.MasterClusterSchemeInfoList.Where(p => p.Id != id).Select(p => p.Name));
            }

            if (nameAccepted)
            {
                if (this.CurrentScheme.Name != newName)
                {
                    this.CurrentScheme.Name = newName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                //force all cluster names to be unique
                foreach (Cluster cluster in this.CurrentScheme.Clusters.Reverse())
                {
                    //[SA-15510] Updated code so cluster ignores itself when checking for uniqueness
                    String validName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(cluster.Name, this.CurrentScheme.Clusters.Where(r => r != cluster).Select(p => p.Name));
                    if (cluster.Name != validName)
                    {
                        cluster.Name = validName;
                    }
                }

                try
                {
                    this.CurrentScheme = this.CurrentScheme.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //[SA-17668] set return flag to false as save was not completed.
                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.CurrentScheme.Name);
                        if (itemReloadRequired)
                        {
                            this.CurrentScheme = ClusterScheme.FetchById(this.CurrentScheme.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.CurrentScheme.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //refresh the info list
                _masterClusterSchemeInfoListView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }

            return continueWithSave;
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;
        /// <summary>
        /// Saves the current cluster scheme as a new one
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(p => SaveAs_Executed(), p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    this.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_userHasClusterCreatePerm)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.CurrentScheme == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentScheme.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //all locations must be assigned
            if (this.UnassignedLocations.Count > 0)
            {
                this.SaveAsCommand.DisabledReason = Message.LocationClusterMaintenance_InvalidScheme_UnassignedLocations;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the name to save as
            String copyName;

            Boolean codeAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                (s) =>
                {
                    return !this.MasterClusterSchemeInfoList.Select(p => p.Name).Contains(s);
                };

                codeAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            }
            else
            {
                //force a unique value for unit testing
                copyName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.CurrentScheme.Name, this.MasterClusterSchemeInfoList.Select(p => p.Name));
            }

            if (codeAccepted)
            {
                base.ShowWaitCursor(true);

                //copy the item and save
                ClusterScheme itemCopy = this.CurrentScheme.Copy();
                itemCopy.Name = copyName;
                itemCopy = itemCopy.Save();

                //set the copy as the new selected item
                this.CurrentScheme = itemCopy;

                //update the available infos
                _masterClusterSchemeInfoListView.FetchForCurrentEntity();


                base.ShowWaitCursor(false);
            }
        }
        #endregion

        #region Save & New

        private RelayCommand _saveAndNewCommand;
        /// <summary>
        /// Executes the save command then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(p => SaveAndNew_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_SaveAndNew,
                        FriendlyDescription = Message.Ribbon_SaveAndNew_Tooltip,
                        Icon = ImageResources.SaveAndNew_16
                    };
                    this.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                NewCommand.Execute();
            }
        }
        #endregion

        #region Save & Close

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command show be handled by the view window
        /// to request a close
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(p => SaveAndClose_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Ribbon_SaveAndClose,
                        FriendlyDescription = Message.Ribbon_SaveAndClose_Tooltip,
                        Icon = ImageResources.SaveAndClose_16
                    };
                    this.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //close the attached organiser
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }
        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the current scheme and loads a new one
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand =
                        new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                        {
                            FriendlyName = Message.Ribbon_Delete,
                            FriendlyDescription = Message.Ribbon_Delete_Tooltip,
                            SmallIcon = ImageResources.Delete_16
                        };
                    this.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_userHasClusterDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.CurrentScheme == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.CurrentScheme.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.CurrentScheme.ToString());
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the scheme as deleted
                ClusterScheme schemeToDelete = this.CurrentScheme;
                schemeToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    //commit the delete
                    schemeToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(schemeToDelete.Name, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //update the available items
                _masterClusterSchemeInfoListView.FetchForCurrentEntity();
                _masterLocationInfoListView.FetchAllForEntity();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;
        /// <summary>
        /// Opens the requested scheme
        ///  parameter = cluster scheme id
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p.Value),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        Icon = ImageResources.Open_16
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? clusterSchemeId)
        {
            //user must have get permission
            if (!_userHasClusterFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (clusterSchemeId == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32 clusterSchemeId)
        {
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //fetch the requested item
                    this.CurrentScheme = ClusterScheme.FetchById(clusterSchemeId);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }

                //close the attached organiser backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*IsOpen*/false);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(),
                        p => Close_CanExecute(), attachToCommandManager: false)
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region AddClusterCommand

        private RelayCommand _addClusterCommand;
        /// <summary>
        /// Adds a new cluster to the current scheme
        /// </summary>
        public RelayCommand AddClusterCommand
        {
            get
            {
                if (_addClusterCommand == null)
                {
                    _addClusterCommand =
                        new RelayCommand(p => AddCluster_Executed(), p => AddCluster_CanExecute())
                        {
                            FriendlyName = Message.LocationClusterMaintenance_ClusterAdd,
                            FriendlyDescription = Message.LocationClusterMaintenance_ClusterAdd_Tooltip,
                            Icon = ImageResources.LocationClusterMaintenance_NewCluster
                        };
                    this.ViewModelCommands.Add(_addClusterCommand);
                }
                return _addClusterCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddCluster_CanExecute()
        {
            if (this.CurrentScheme == null)
            {
                return false;
            }

            return true;
        }

        private void AddCluster_Executed()
        {
            //create and add a new cluster
            Cluster newCluster = Cluster.NewCluster();
            this.CurrentScheme.Clusters.Add(newCluster);

            //set as the selected cluster
            this.SelectedCluster = newCluster;
        }

        #endregion

        #region DeleteClusterCommand

        private RelayCommand _deleteClusterCommand;
        /// <summary>
        /// Deletes the current cluster
        /// </summary>
        public RelayCommand DeleteClusterCommand
        {
            get
            {
                if (_deleteClusterCommand == null)
                {
                    _deleteClusterCommand = new RelayCommand(p => DeleteCluster_Executed(), p => DeleteCluster_CanExecute())
                    {
                        FriendlyName = Message.LocationClusterMaintenance_ClusterRemove,
                        FriendlyDescription = Message.LocationClusterMaintenance_ClusterRemove_Tooltip,
                        Icon = ImageResources.LocationClusterMaintenance_DeleteCluster
                    };
                    this.ViewModelCommands.Add(_deleteClusterCommand);
                }
                return _deleteClusterCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean DeleteCluster_CanExecute()
        {
            //must have a cluster selected
            if (this.SelectedCluster == null)
            {
                this.DeleteCommand.DisabledReason = Message.LocationClusterMaintenance_ClusterRemove_Invalid;
                return false;
            }

            return true;
        }

        private void DeleteCluster_Executed()
        {
            if (DeleteCluster_CanExecute())
            {
                this.CurrentScheme.Clusters.Remove(this.SelectedCluster);
                this.SelectedCluster = this.CurrentScheme.Clusters.FirstOrDefault();
            }
        }

        #endregion

        #region RemoveAllClustersCommand

        private RelayCommand _removeAllClustersCommand;
        /// <summary>
        /// Removes all clusters from the current scheme
        /// </summary>
        public RelayCommand RemoveAllClustersCommand
        {
            get
            {
                if (_removeAllClustersCommand == null)
                {
                    _removeAllClustersCommand = new RelayCommand(
                        p => RemoveAllClustersExecuted(),
                        p => RemoveAllClusters_CanExecute())
                    {
                        FriendlyName = Message.LocationClusterMaintenance_ClusterRemoveAll,
                        FriendlyDescription = Message.LocationClusterMaintenance_ClusterRemoveAll_Tooltip,
                        Icon = ImageResources.LocationClusterMaintenance_RemoveAllClusters
                    };
                    this.ViewModelCommands.Add(_removeAllClustersCommand);
                }
                return _removeAllClustersCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RemoveAllClusters_CanExecute()
        {
            //must have a scheme loaded
            if (this.CurrentScheme == null)
            {
                this.RemoveAllClustersCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have clusters
            if (this.CurrentScheme.Clusters.Count == 0)
            {
                this.RemoveAllClustersCommand.DisabledReason = Message.LocationClusterMaintenance_ClusterRemoveAll_Invalid;
                return false;
            }

            return true;
        }

        private void RemoveAllClustersExecuted()
        {
            this.CurrentScheme.Clusters.Clear();
        }

        #endregion

        #region AutoCreateClustersCommand

        private RelayCommand _autoCreateClustersCommand;
        /// <summary>
        /// Auto creates clusters by the given location property.
        /// Paramter = location property String name
        /// </summary>
        public RelayCommand AutoCreateClustersCommand
        {
            get
            {
                if (_autoCreateClustersCommand == null)
                {
                    _autoCreateClustersCommand =
                        new RelayCommand(p => AutoCreateClusters_Executed((String)p),
                        p => AutoCreateClusters_CanExecute((String)p))
                        {
                            FriendlyName = Message.LocationClusterMaintenance_AutocreateClusters,
                            FriendlyDescription = Message.LocationClusterMaintenance_AutocreateClusters_Tooltip,
                            Icon = ImageResources.LocationClusterMaintenance_AutoCreate
                        };
                    this.ViewModelCommands.Add(_autoCreateClustersCommand);
                }
                return _autoCreateClustersCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AutoCreateClusters_CanExecute(String locationPropertyName)
        {
            //must have a scheme loaded
            if (this.CurrentScheme == null)
            {
                this.AutoCreateClustersCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have a location property name
            if (String.IsNullOrEmpty(locationPropertyName))
            {
                this.AutoCreateClustersCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void AutoCreateClusters_Executed(String locationPropertyName)
        {
            ClusterScheme scheme = this.CurrentScheme;

            if (scheme != null)
            {
                base.ShowWaitCursor(true);

                //clear any existing clusters in the cluster scheme
                scheme.Clusters.Clear();

                CollectionViewSource groupedData = new CollectionViewSource();
                groupedData.GroupDescriptions.Add(new PropertyGroupDescription(locationPropertyName));

                if (_masterLocationList == null)
                {
                    _masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);
                }
                groupedData.Source = _masterLocationList;

                //cycle through the groups adding clusters
                foreach (CollectionViewGroup grp in groupedData.View.Groups)
                {
                    //if the rows grouping column data is not null, otherwise ignore
                    if (grp.Name != null)
                    {
                        //create a cluster
                        Cluster newCluster = scheme.Clusters.AddNew();
                        newCluster.Name = grp.Name.ToString();
                        //add the locatons
                        foreach (Location st in grp.Items)
                        {
                            LocationInfo locationInfo = this.MasterLocationInfoList.Where(p => p.Id == st.Id).First();

                            if (locationInfo != null)
                            {
                                newCluster.Locations.Add(locationInfo);
                            }
                        }
                    }
                }

                //reload this list
                this.CurrentScheme = this.CurrentScheme;

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region Add Locations

        private RelayCommand _addLocationsCommand;
        /// <summary>
        /// Adds the current selected available locations to the current cluster
        /// </summary>
        public RelayCommand AddLocationsCommand
        {
            get
            {
                if (_addLocationsCommand == null)
                {
                    _addLocationsCommand = new RelayCommand(p => AddLocations_Executed(), p => AddLocations_CanExecute())
                    {
                        FriendlyName = Message.LocationClusterMaintenance_AddLocations,
                        FriendlyDescription = Message.LocationClusterMaintenance_AddLocations_Tooltip,
                        Icon = ImageResources.LocationClusterMaintenance_Add
                    };
                    this.ViewModelCommands.Add(_addLocationsCommand);
                }
                return _addLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddLocations_CanExecute()
        {
            //must have a cluster selected
            if (this.SelectedCluster == null)
            {
                this.AddLocationsCommand.DisabledReason = Message.LocationClusterMaintenance_LocationOperations_Invalid;
                return false;
            }

            // must have available locations selected
            if (this.SelectedAvailableLocations.Count == 0)
            {
                this.AddLocationsCommand.DisabledReason = Message.LocationClusterMaintenance_AddLocations_Invalid;
                return false;
            }

            return true;
        }

        private void AddLocations_Executed()
        {
            //[TODO] See if add range method can be used instead.
            foreach (LocationInfo st in SelectedAvailableLocations.ToList())
            {
                this.SelectedCluster.Locations.Add(st);
            }
        }

        #endregion

        #region Remove Locations

        private RelayCommand _removeLocationsCommand;
        /// <summary>
        /// Removes the current selected taken locations from the cluster
        /// </summary>
        public RelayCommand RemoveLocationsCommand
        {
            get
            {
                if (_removeLocationsCommand == null)
                {
                    _removeLocationsCommand = new RelayCommand(p => RemoveLocations_Executed(), p => RemoveLocations_CanExecute())
                    {
                        FriendlyName = Message.LocationClusterMaintenance_RemoveLocations,
                        FriendlyDescription = Message.LocationClusterMaintenance_RemoveLocations_Tooltip,
                        Icon = ImageResources.LocationSelectionDeselect,
                        DisabledReason = Message.LocationClusterMaintenance_RemoveLocations_Invalid

                    };
                    this.ViewModelCommands.Add(_removeLocationsCommand);
                }
                return _removeLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RemoveLocations_CanExecute()
        {
            //must have a cluster selected
            if (this.SelectedCluster == null)
            {
                this.RemoveLocationsCommand.DisabledReason = Message.LocationClusterMaintenance_LocationOperations_Invalid;
                return false;
            }

            //must have taken locations selected
            if (this.SelectedTakenLocations.Count == 0)
            {
                this.RemoveLocationsCommand.DisabledReason = Message.LocationClusterMaintenance_RemoveLocations_Invalid;
                return false;
            }

            return true;
        }

        private void RemoveLocations_Executed()
        {
            //[TODO] See if remove range method can be used instead.
            foreach (LocationInfo location in SelectedTakenLocations.ToList())
            {
                //get the ClusterLocation
                ClusterLocation selectedClusterLocation =
                    this.SelectedCluster.Locations.FirstOrDefault(s => s.LocationId == location.Id);

                if (selectedClusterLocation != null)
                {
                    //remove from the target cluster
                    this.SelectedCluster.Locations.Remove(selectedClusterLocation);
                }
            }

        }
        #endregion

        #region Remove all locations

        private RelayCommand _removeAllLocationsCommand;
        /// <summary>
        /// Removes the current selected taken locations from the cluster
        /// </summary>
        public RelayCommand RemoveAllLocationsCommand
        {
            get
            {
                if (_removeAllLocationsCommand == null)
                {
                    _removeAllLocationsCommand =
                        new RelayCommand(p => RemoveAllLocations_Executed(), p => RemoveAllLocations_CanExecute())
                        {
                            FriendlyName = Message.LocationClusterMaintenance_RemoveLocations,
                            FriendlyDescription = Message.LocationClusterMaintenance_RemoveLocations_Tooltip,
                            Icon = ImageResources.LocationClusterMaintenance_Remove
                        };
                    base.ViewModelCommands.Add(_removeAllLocationsCommand);
                }
                return _removeAllLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RemoveAllLocations_CanExecute()
        {
            //must have a cluster selected
            if (this.SelectedCluster == null)
            {
                this.RemoveLocationsCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void RemoveAllLocations_Executed()
        {
            //remove from the target cluster
            this.SelectedCluster.Locations.Clear();
        }

        #endregion

        #region NewImportFromBehaviouralClusteringCommand

        private RelayCommand _newImportFromBehaviouralClusteringCommand;

        /// <summary>
        /// Creates a new cluster scheme based on one created in behavioural clustering
        /// </summary>
        public RelayCommand NewImportFromBehaviouralClusteringCommand
        {
            get
            {
                if (_newImportFromBehaviouralClusteringCommand == null)
                {
                    _newImportFromBehaviouralClusteringCommand =
                        new RelayCommand(p => NewImportFromBehaviouralClustering_Executed(), p => NewImportFromBehaviouralClustering_CanExecute())
                        {
                            FriendlyName = Message.LocationClusterMaintenance_NewImportFromBehaviouralClusteringCommand,
                            Icon = ImageResources.New_32
                        };
                    this.ViewModelCommands.Add(_newImportFromBehaviouralClusteringCommand);
                }
                return _newImportFromBehaviouralClusteringCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean NewImportFromBehaviouralClustering_CanExecute()
        {
            return false;
        }

        private void NewImportFromBehaviouralClustering_Executed()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region SelectAssignedCategoryCommand

        private RelayCommand _selectAssignedCategoryCommand;

        /// <summary>
        /// Shows the modal window to select the assigned category
        /// </summary>
        public RelayCommand SelectAssignedCategoryCommand
        {
            get
            {
                if (_selectAssignedCategoryCommand == null)
                {
                    _selectAssignedCategoryCommand =
                        new RelayCommand(
                            p => SelectAssignedCategory_Executed(),
                            p => SelectAssignedCategory_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ellipsis
                        };
                    this.ViewModelCommands.Add(_selectAssignedCategoryCommand);
                }
                return _selectAssignedCategoryCommand;
            }
        }

        private Boolean SelectAssignedCategory_CanExecute()
        {
            //user must have fetch permission
            if (!Csla.Rules.BusinessRules.HasPermission
                (Csla.Rules.AuthorizationActions.GetObject, typeof(ProductGroup)))
            {
                this.SelectAssignedCategoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }
            return true;
        }

        private void SelectAssignedCategory_Executed()
        {
            if (this.AttachedControl != null)
            {
                MerchGroupSelectionWindow win =
                    new MerchGroupSelectionWindow(null,null,
                        System.Windows.Controls.DataGridSelectionMode.Single);
                App.ShowWindow(win, this.AttachedControl, true);

                if (win.DialogResult == true)
                {
                    if (win.SelectionResult[0] == null)
                    {
                        this.CurrentScheme.ProductGroupId = null;
                        this.AssignedCategory = null;
                    }
                    else
                    {
                        this.AssignedCategory = ProductGroupInfo.NewProductGroupInfo(win.SelectionResult[0]);
                    }
                }
            }
        }


        #endregion


        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the change of the selected scheme
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnCurrentSchemeChanged(ClusterScheme oldValue, ClusterScheme newValue)
        {
            if (oldValue != null)
            {
                oldValue.Clusters.BulkCollectionChanged -= Clusters_BulkCollectionChanged;
            }


            if (newValue != null)
            {
                newValue.Clusters.BulkCollectionChanged += Clusters_BulkCollectionChanged;

                RefreshUnassignedLocations();
                _assignedLocations.Clear();

                //NB: the assigned locations list is fired on sublevel selection

                //set the clusters source
                if (_clusterSchemeClusterSource != null)
                {
                    _clusterSchemeClusterSource.Source = newValue.Clusters;
                    OnPropertyChanged(ClusterSchemeClustersSourceProperty);
                }

                //select the first cluster
                this.SelectedCluster = newValue.Clusters.OrderBy(s => s.Name).FirstOrDefault();
                //Set the AssignedCategory
                if (newValue.ProductGroupId.HasValue)
                {
                    Int32 productGroupId = newValue.ProductGroupId.Value;
                    this.AssignedCategory = ProductGroupInfo.FetchById(productGroupId);
                }
                else
                {
                    this.AssignedCategory = null;
                }

            }
            else
            {
                this.SelectedCluster = null;
                this.AssignedCategory = null;
            }

        }

        /// <summary>
        /// Refreshes the unassigned locations list when the sublevels change
        /// </summary>
        private void Clusters_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            RefreshUnassignedLocations();
        }


        /// <summary>
        /// Attaches to the locations collection changed event for the selected cluster
        /// and updates all required lists
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnSelectedClusterChanged(Cluster oldValue, Cluster newValue)
        {
            if (oldValue != null)
            {
                oldValue.Locations.CollectionChanged -= ClusterLocations_CollectionChanged;
            }

            if (_assignedLocations.Count > 0) { _assignedLocations.Clear(); }
            if (_selectedTakenLocations.Count > 0) { _selectedTakenLocations.Clear(); }

            if (newValue != null)
            {
                newValue.Locations.CollectionChanged += new NotifyCollectionChangedEventHandler(ClusterLocations_CollectionChanged);

                //change the content of the assigned grid based on the selection
                IEnumerable<Int32> takenLocationIds = newValue.Locations.Select<ClusterLocation, Int32>(s => s.LocationId);

                foreach (ClusterLocation clusterLocation in newValue.Locations)
                {
                    LocationInfo locInfo = GetLocationIncludingDeleted(clusterLocation.Code);
                    if (locInfo != null)
                    {
                        _assignedLocations.Add(locInfo);
                    }
                }
            }
            else
            {
                this._assignedLocations.Clear();
                RefreshUnassignedLocations();
            }
        }

        /// <summary>
        /// Responds to changes in the locations collection of the currently selected cluster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClusterLocations_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:

                    //remove the locations from the unassigned items list
                    //and add to the selected list
                    foreach (ClusterLocation selectedStore in e.NewItems)
                    {
                        LocationInfo sInfo = this.UnassignedLocations.FirstOrDefault(s => s.Id == selectedStore.LocationId);
                        if (sInfo != null)
                        {
                            this._unassignedLocations.Remove(sInfo);
                            this._assignedLocations.Add(sInfo);
                            OnPropertyChanged(IsUnassignedLocationGridVisibleProperty);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:

                    //remove from the selected list and add to the unassigned
                    foreach (ClusterLocation selectedStore in e.OldItems)
                    {
                        LocationInfo sInfo = this.AssignedLocations.FirstOrDefault(s => s.Id == selectedStore.LocationId);
                        if (sInfo != null)
                        {
                            this._assignedLocations.Remove(sInfo);
                            //Only move to unasigned list if not a deleted location
                            if (sInfo.DateDeleted == null)
                            {
                                this._unassignedLocations.Add(sInfo);
                            }
                            OnPropertyChanged(IsUnassignedLocationGridVisibleProperty);
                        }
                    }
                    break;

                default:
                    this._assignedLocations.Clear();
                    RefreshUnassignedLocations();
                    break;
            }

        }

        /// <summary>
        /// Responds to changes in the unassigned location collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnassignedLocations_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            OnPropertyChanged(IsUnassignedLocationGridVisibleProperty);
        }

        #endregion

        #region Methods


        /// <summary>
        /// Try to fetch a location using the master list first (which does not contain deleted items)
        /// If it is not found, try to fetch it directly
        /// </summary>
        /// <param name="locationId"></param>
        /// <returns></returns>
        private LocationInfo GetLocationIncludingDeleted(String locationCode)
        {
            LocationInfo locationInfo = _masterLocationInfoListView.Model.FirstOrDefault(l => l.Code == locationCode);
            //try the master list first - this does not contain deleted items
            if (locationInfo != null)
            {
                return locationInfo;
            }
            else
            {
                //item may have been deleted, so is not in the masterlocations list, so fetch by its id
                LocationInfoList infoList = LocationInfoList.FetchByEntityIdLocationCodes(App.ViewState.EntityId, new List<String> { locationCode });
                if (infoList.Count > 0)
                {
                    return infoList[0];
                }
            }
            return null;
        }

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //cluster scheme premissions
            //create
            _userHasClusterCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(ClusterScheme));

            //fetch
            _userHasClusterFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(ClusterScheme));

            //edit
            _userHasClusterEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(ClusterScheme));

            //delete
            _userHasClusterDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(ClusterScheme));
        }

        /// <summary>
        /// Merges the first cluster into the second
        /// </summary>
        /// <param name="mergeCluster">The cluster to be merged</param>
        /// <param name="mergeIntoCluster">The cluster it should be merged into</param>
        public void MergeClusters(Cluster mergeCluster, Cluster mergeIntoCluster)
        {
            Boolean canContinue = true;

            if (mergeCluster != null
                && mergeIntoCluster != null
                && mergeCluster != mergeIntoCluster)
            {
                //get the location list from the merge cluster
                List<LocationInfo> addLocations = new List<LocationInfo>();
                List<LocationInfo> deletedLocations = new List<LocationInfo>();

                foreach (ClusterLocation clusterLoc in mergeCluster.Locations)
                {
                    LocationInfo loc = GetLocationIncludingDeleted(clusterLoc.Code);
                    if (loc.DateDeleted == null)
                    {
                        addLocations.Add(loc);
                    }
                    else
                    {
                        deletedLocations.Add(loc);
                    }
                }

                //if there are any deleed locations warn user these will be removed
                if (deletedLocations.Count > 0)
                {
                    canContinue = false;
                    ModalMessage dialog = new ModalMessage();
                    dialog.MessageIcon = ImageResources.Warning_32;
                    dialog.Title = App.Current.MainWindow.GetType().Assembly.GetName().Name;
                    dialog.Header = Message.LocationClusters_GroupContainsDeletedLocationsHeader;
                    dialog.ButtonCount = 2;
                    dialog.Button1Content = Message.Generic_Continue;
                    dialog.Button2Content = Message.Generic_Cancel;
                    String description = Message.LocationClusters_GroupContainsDeletedLocationsDescription;
                    dialog.Description = String.Format(description, mergeCluster.Name);
                    App.ShowWindow(dialog, true);
                    ModalMessageResult returnAction = dialog.Result;

                    canContinue = (returnAction == ModalMessageResult.Button1);
                }

                if (canContinue)
                {
                    //remove the deleted locations first
                    foreach (LocationInfo deletedStore in deletedLocations)
                    {
                        ClusterLocation cl = mergeCluster.Locations.FirstOrDefault(l => l.LocationId == deletedStore.Id);
                        if (cl != null)
                        {
                            mergeCluster.Locations.Remove(cl);
                        }
                    }

                    //delete the merge sublevel
                    if (DeleteCluster_CanExecute())
                    {
                        this.CurrentScheme.Clusters.Remove(mergeCluster);
                    }

                    //add isolated locations to the target SubLevel
                    foreach (LocationInfo loc in addLocations)
                    {
                        mergeIntoCluster.Locations.Add(loc);
                    }

                    //ensure the unassigned locations list is correctly refreshed
                    RefreshUnassignedLocations();

                    this.SelectedCluster = mergeIntoCluster;
                }

                //IEnumerable<Int16> locationIds = mergeCluster.Locations.Select(p => p.LocationId);
                //List<LocationInfo> locations = this.MasterLocationInfoList.Where(s => locationIds.Contains(s.Id)).ToList();

                //this.SelectedTakenLocations.Clear();
                //this.SelectedAvailableLocations.Clear();

                ////select the clust 
                //this.SelectedCluster = mergeCluster;

                ////request the locations are removed
                //locations.ForEach(s => this.SelectedTakenLocations.Add(s));
                //this.RemoveLocationsCommand.Execute();

                ////delete the merge cluster
                //this.DeleteClusterCommand.Execute();

                ////select the to cluster
                //this.SelectedCluster = mergeIntoCluster;

                ////add the locations to the cluster
                //locations.ForEach(s => this.SelectedAvailableLocations.Add(s));
                //this.AddLocationsCommand.Execute();
            }
        }

        /// <summary>
        /// Moves the locations from one cluster to another
        /// </summary>
        /// <param name="locations"></param>
        /// <param name="fromCluster"></param>
        /// <param name="toCluster"></param>
        public void MoveLocations(IEnumerable<LocationInfo> locationsToMove, Cluster fromCluster, Cluster toCluster)
        {
            if (fromCluster != null
                && toCluster != null
                && toCluster != fromCluster)
            {
                this.SelectedAvailableLocations.Clear();
                this.SelectedTakenLocations.Clear();

                //create the add and remove lists
                List<ClusterLocation> removeClusterLocs = new List<ClusterLocation>();
                List<ClusterLocation> addClusterLocs = new List<ClusterLocation>();

                List<Int16> toClusterTakenIds = toCluster.Locations.Select(l => l.LocationId).ToList();

                List<LocationInfo> deletedLocations = locationsToMove.Where(l => l.DateDeleted != null).ToList();

                Boolean canContinue = true;
                if (deletedLocations.Count > 0)
                {
                    canContinue = false;
                    ModalMessage dialog = new ModalMessage();
                    dialog.MessageIcon = ImageResources.DialogInformation;
                    dialog.Title = App.Current.MainWindow.GetType().Assembly.GetName().Name;
                    dialog.Header = Message.LocationClusters_MoveLocationsHeader;
                    dialog.ButtonCount = 2;
                    dialog.Button1Content = Message.Generic_Continue;
                    dialog.Button2Content = Message.Generic_Cancel;
                    dialog.Description = Message.LocationClusters_MoveLocationsDescription;
                    App.ShowWindow(dialog, true);
                    ModalMessageResult returnAction = dialog.Result;
                    canContinue = (returnAction == ModalMessageResult.Button1);
                }

                if (canContinue)
                {
                    //remove the deleted locations first from the source cluster group
                    foreach (LocationInfo deletedLocation in deletedLocations)
                    {
                        ClusterLocation cl = fromCluster.Locations.FirstOrDefault(l => l.LocationId == deletedLocation.Id);
                        if (cl != null)
                        {
                            fromCluster.Locations.Remove(cl);
                        }
                    }

                    //now move the non-deleted ones
                    foreach (LocationInfo loc in locationsToMove.Where(l => l.DateDeleted == null))
                    {
                        //get the loc to remove
                        Int16 locId = loc.Id;
                        ClusterLocation removeClusterLoc = fromCluster.Locations.FirstOrDefault(l => l.LocationId == locId);
                        if (removeClusterLoc != null)
                        {
                            removeClusterLocs.Add(removeClusterLoc);
                        }

                        //get the loc to add
                        if (!toClusterTakenIds.Contains(loc.Id))
                        {
                            addClusterLocs.Add(ClusterLocation.NewClusterLocation(loc));
                            toClusterTakenIds.Add(loc.Id);
                        }
                    }

                    //remove from the from sublevel
                    fromCluster.Locations.RemoveList(removeClusterLocs);

                    //add to the new sublevel
                    toCluster.Locations.AddRange(addClusterLocs);

                    //select the to cluster
                    this.SelectedCluster = fromCluster;

                    //reset the loc lists
                    RefreshUnassignedLocations();
                }
            }
        }

        /// <summary>
        /// Sets the given scheme as the default
        /// </summary>
        /// <param name="schemeToSet"></param>
        public void SetAsDefaultScheme(ClusterSchemeInfo schemeInfoToSet)
        {
            //get the existing and save as false
            ClusterSchemeInfo existingDefaultSchemeInfo = this.MasterClusterSchemeInfoList.FirstOrDefault(s => s.IsDefault);
            ClusterScheme existingDefaultScheme = (existingDefaultSchemeInfo != null) ? ClusterScheme.FetchById(existingDefaultSchemeInfo.Id) : null;
            if (existingDefaultScheme != null)
            {
                existingDefaultScheme.IsDefault = false;
                existingDefaultScheme.Save();
            }

            //set the new as isdefault = true
            ClusterScheme selectedScheme = ClusterScheme.FetchById(schemeInfoToSet.Id);
            selectedScheme.IsDefault = true;
            selectedScheme.Save();

            //refresh the cluster scheme info list
            _masterClusterSchemeInfoListView.FetchForCurrentEntity();
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.CurrentScheme, this.SaveCommand);
            }
            return continueExecute;
        }

        /// <summary>
        /// Returns a list of cluster schemes that match the given criteria
        /// </summary>
        /// <param name="nameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<ClusterSchemeInfo> GetMatchingLocationClusters(String nameCriteria)
        {
            String invariantCriteria = nameCriteria.ToLowerInvariant();

            return this.MasterClusterSchemeInfoList.Where(
                p => p.Name.ToLowerInvariant().Contains(invariantCriteria));
        }

        /// <summary>
        /// Assesses each item individually, returning true 
        /// if it satisfies filter conditions
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private Boolean ClusterSourceFilter(object item)
        {
            String searchText = this.ClustersFilterText;

            if (!String.IsNullOrEmpty(searchText))
            {
                searchText = searchText.ToUpperInvariant();

                Cluster clusterItem = (Cluster)item;
                String compareName = clusterItem.Name.ToUpperInvariant();
                if (compareName.Contains(searchText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Refreshes the unassigned locations list
        /// </summary>
        private void RefreshUnassignedLocations()
        {
            if (this.CurrentScheme != null)
            {
                //get a list of locations used in the peergroup
                List<Int16> locationsTakenByClusterScheme =
                    this.CurrentScheme.FetchAllocatedLocations().Select(p => p.LocationId).ToList();

                //load the master locations minus those taken into the unassigned grid
                _unassignedLocations.Clear();

                foreach (LocationInfo locInfo in MasterLocationInfoList.Where(s => !locationsTakenByClusterScheme.Contains(s.Id)).ToArray())
                {
                    _unassignedLocations.Add(locInfo);
                }
            }
        }

        #endregion

        #region Disposing

        /// <summary>
        /// Handles an IDisposable dispose call
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    OnCurrentSchemeChanged(this.CurrentScheme, null);

                    _unassignedLocations.CollectionChanged -= UnassignedLocations_CollectionChanged;
                    _masterLocationList = null;

                    this.AttachedControl = null;

                    _masterLocationInfoListView.Dispose();
                    _masterClusterSchemeInfoListView.Dispose();
                }
                this.IsDisposed = true;
            }
        }

        #endregion
    }
}
