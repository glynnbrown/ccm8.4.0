﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationClusterMaintenance
{
    /// <summary>
    /// Interaction logic for LocationClusterMaintenanceBackstageDefaults.xaml
    /// </summary>
    public partial class LocationClusterMaintenanceBackstageDefaults : UserControl
    {
        #region ViewModel Property
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationClusterMaintenanceViewModel), typeof(LocationClusterMaintenanceBackstageDefaults));
        public LocationClusterMaintenanceViewModel ViewModel
        {
            get { return (LocationClusterMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationClusterMaintenanceBackstageDefaults()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the change of an is default flag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IsDefaultCheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox senderControl = (CheckBox)sender;
            ClusterSchemeInfo selectedSchemeInfo = (ClusterSchemeInfo)senderControl.DataContext;

            //call the viewmodel method
            this.ViewModel.SetAsDefaultScheme(selectedSchemeInfo);
        }

        #endregion
    }
}
