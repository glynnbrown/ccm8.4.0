﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationClusterMaintenance
{
    /// <summary>
    /// Interaction logic for LocationClustersMaintenanceHomeTab.xaml
    /// </summary>
    public partial class LocationClusterMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationClusterMaintenanceViewModel), typeof(LocationClusterMaintenanceHomeTab));
        public LocationClusterMaintenanceViewModel ViewModel
        {
            get { return (LocationClusterMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor / Loaded

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationClusterMaintenanceHomeTab()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(LocationClusterMaintenanceHomeTab_Loaded);
        }

        /// <summary>
        /// Carries out intial first load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationClusterMaintenanceHomeTab_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationClusterMaintenanceHomeTab_Loaded;
            LoadAutoCreatePopup();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads all the store properties into a context menu
        /// </summary>
        /// <returns></returns>
        private void LoadAutoCreatePopup()
        {
            RelayCommand autoCreateCommand = (this.DataContext != null) ? ((LocationClusterMaintenanceViewModel)this.DataContext).AutoCreateClustersCommand : null;

            #region Facilities Menu
            //Create Menu Group
            Fluent.MenuItem facilities = new Fluent.MenuItem()
            {
                Header = Message.LocationClusterMaintenance_AutoCreate_Facilities,
                Command = autoCreateCommand,
                CanAddToQuickAccessToolBar = false
            };
            //Add items to group
            //foreach (Csla.Core.IPropertyInfo property in DataObjectViewHelper.GetFacilityGroupPopulatedLocationColumnNames().OrderBy(p => p.FriendlyName))
            //{
            //    facilities.Items.Add(new Fluent.MenuItem()
            //    {
            //        Header = property.FriendlyName,
            //        Command = autoCreateCommand,
            //        CommandParameter = property.Name,
            //        CanAddToQuickAccessToolBar = false
            //    });
            //    facilities.CommandParameter = property.Name;
            //}

            //Add menu group to drop down button
            if (facilities.Items.Count > 0)
            {
                cmdAutoCreateClusters.Items.Add
                (
                    facilities
                );
            }
            #endregion

            #region General Menu
            //Create Menu Group
            Fluent.MenuItem general = new Fluent.MenuItem()
            {
                Header = Message.LocationClusterMaintenance_AutoCreate_General,
                Command = autoCreateCommand,
                CanAddToQuickAccessToolBar = false
            };
            //Add items to group
            //foreach (Csla.Core.IPropertyInfo property in DataObjectViewHelper.GetGeneralGroupPopulatedLocationColumnNames().OrderBy(p => p.FriendlyName))
            //{
            //    general.Items.Add(new Fluent.MenuItem()
            //    {
            //        Header = property.FriendlyName,
            //        Command = autoCreateCommand,
            //        CommandParameter = property.Name,
            //        CanAddToQuickAccessToolBar = false
            //    });
            //    general.CommandParameter = property.Name;
            //}
            //Add menu group to drop down button
            if (general.Items.Count > 0)
            {
                cmdAutoCreateClusters.Items.Add
                (
                    general
                );
            }
            #endregion

            #region Location Menu
            //Create Menu Group
            Fluent.MenuItem location = new Fluent.MenuItem()
            {
                Header = Message.LocationClusterMaintenance_AutoCreate_Location,
                Command = autoCreateCommand,
                CanAddToQuickAccessToolBar = false
            };
            //Add items to group
            foreach (Csla.Core.IPropertyInfo property in DataObjectViewHelper.GetLocationGroupPopulatedLocationColumnNames().OrderBy(p => p.FriendlyName))
            {
                location.Items.Add(new Fluent.MenuItem()
                {
                    Header = property.FriendlyName,
                    Command = autoCreateCommand,
                    CommandParameter = property.Name,
                    CanAddToQuickAccessToolBar = false
                });
                location.CommandParameter = property.Name;
            }

            //Add menu group to drop down button
            if (location.Items.Count > 0)
            {
                cmdAutoCreateClusters.Items.Add
                (
                    location
                );
            }
            #endregion

            #region Marketing Menu
            //Create Menu Group
            Fluent.MenuItem marketing = new Fluent.MenuItem()
            {
                Header = Message.LocationClusterMaintenance_AutoCreate_Marketing,
                Command = autoCreateCommand,
                CanAddToQuickAccessToolBar = false
            };
            //Add items to group
            foreach (Csla.Core.IPropertyInfo property in DataObjectViewHelper.GetMarketingGroupPopulatedLocationColumnNames().OrderBy(p => p.FriendlyName))
            {
                marketing.Items.Add(new Fluent.MenuItem()
                {
                    Header = property.FriendlyName,
                    Command = autoCreateCommand,
                    CommandParameter = property.Name,
                    CanAddToQuickAccessToolBar = false
                });
                marketing.CommandParameter = property.Name;
            }

            //Add menu group to drop down button
            if (marketing.Items.Count > 0)
            {
                cmdAutoCreateClusters.Items.Add
                (
                    marketing
                );
            }
            #endregion

            #region Opening Menu
            //Create Menu Group
            Fluent.MenuItem opening = new Fluent.MenuItem()
            {
                Header = Message.LocationClusterMaintenance_AutoCreate_Opening,
                Command = autoCreateCommand,
                CanAddToQuickAccessToolBar = false
            };
            //Add items to group
            foreach (Csla.Core.IPropertyInfo property in DataObjectViewHelper.GetOpeningGroupPopulatedLocationColumnNames().OrderBy(p => p.FriendlyName))
            {
                opening.Items.Add(new Fluent.MenuItem()
                {
                    Header = property.FriendlyName,
                    Command = autoCreateCommand,
                    CommandParameter = property.Name,
                    CanAddToQuickAccessToolBar = false
                });
                opening.CommandParameter = property.Name;
            }

            //Add menu group to drop down button
            if (opening.Items.Count > 0)
            {
                cmdAutoCreateClusters.Items.Add
                (
                    opening
                );
            }
            #endregion

            #region Location Menu
            //Create Menu Group
            Fluent.MenuItem store = new Fluent.MenuItem()
            {
                Header = Message.LocationClusterMaintenance_AutoCreate_LocationSpecific,
                Command = autoCreateCommand,
                CanAddToQuickAccessToolBar = false
            };
            //Add items to group
            foreach (Csla.Core.IPropertyInfo property in DataObjectViewHelper.GetLocationSpecificGroupPopulatedLocationColumnNames().OrderBy(p => p.FriendlyName))
            {
                store.Items.Add(new Fluent.MenuItem()
                {
                    Header = property.FriendlyName,
                    Command = autoCreateCommand,
                    CommandParameter = property.Name,
                    CanAddToQuickAccessToolBar = false
                });
                store.CommandParameter = property.Name;
            }

            //Add menu group to drop down button
            if (store.Items.Count > 0)
            {
                cmdAutoCreateClusters.Items.Add
                (
                    store
                );
            }
            #endregion

            //bind the button isenabled state to the isenabled state of the first item
            BindingOperations.SetBinding(cmdAutoCreateClusters, Fluent.DropDownButton.IsEnabledProperty, new Binding("Items[0].IsEnabled"));
        }

        #endregion
    }
}
