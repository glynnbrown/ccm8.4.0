﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramImportTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramImportTemplateMaintenanceHomeTab.xaml
    /// </summary>
    public partial class PlanogramImportTemplateMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramImportTemplateMaintenanceViewModel), typeof(PlanogramImportTemplateMaintenanceHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public PlanogramImportTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramImportTemplateMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramImportTemplateMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
