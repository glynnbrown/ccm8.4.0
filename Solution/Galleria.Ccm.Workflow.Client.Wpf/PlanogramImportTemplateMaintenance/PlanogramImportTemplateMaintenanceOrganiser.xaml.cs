﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : D.Pleasance
//  Created
// V8-28219 : N.Haywood
//  changed PlanogramImportFileType.Spaceman to SpacemanV9
#endregion
#region Version History: (CCM 8.0.1)
// V8-28622 : D.Pleasance
//  Added PlanogramImportTemplatePerformanceMetrics
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramImportTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramImportTemplateMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class PlanogramImportTemplateMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        const String _clearMappingCommandKey = "ClearMappingCommand";
        const String _clearAllMappingsCommandKey = "ClearAllMappingsCommand";
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramImportTemplateMaintenanceViewModel), typeof(PlanogramImportTemplateMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public PlanogramImportTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramImportTemplateMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramImportTemplateMaintenanceOrganiser senderControl = (PlanogramImportTemplateMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                PlanogramImportTemplateMaintenanceViewModel oldModel = (PlanogramImportTemplateMaintenanceViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
                senderControl.Resources.Remove(_clearMappingCommandKey);
                senderControl.Resources.Remove(_clearAllMappingsCommandKey);
            }

            if (e.NewValue != null)
            {
                PlanogramImportTemplateMaintenanceViewModel newModel = (PlanogramImportTemplateMaintenanceViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
                senderControl.Resources.Add(_clearMappingCommandKey, newModel.ClearMappingCommand);
                senderControl.Resources.Add(_clearAllMappingsCommandKey, newModel.ClearAllMappingsCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramImportTemplateMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramImportTemplateMaintenance);

            this.ViewModel = new PlanogramImportTemplateMaintenanceViewModel(Model.PlanogramImportFileType.SpacemanV9);

            this.Loaded += new RoutedEventHandler(PlanogramImportTemplateMaintenanceOrganiser_Loaded);
        }

        private void PlanogramImportTemplateMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramImportTemplateMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// calles when ever the keys O+Ctrl is pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        private void OnPerformanceMetricDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            var command = ViewModel.ViewMetricCommand;
            if (command.CanExecute()) command.Execute();
        }

        #endregion

        #region Window Close

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion

        private void XDataGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var command = ViewModel.ViewMetricCommand;
                if (command.CanExecute()) command.Execute();
            }
        }
    }
}