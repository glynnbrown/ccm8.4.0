﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramImportTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramImportTemplateMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class PlanogramImportTemplateMaintenanceBackstageOpen
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(PlanogramImportTemplateMaintenanceViewModel), typeof(PlanogramImportTemplateMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        public PlanogramImportTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramImportTemplateMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramImportTemplateMaintenanceBackstageOpen()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void XSearchResults_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                PlanogramImportTemplateInfo importTemplateToOpen = (PlanogramImportTemplateInfo)senderControl.SelectedItem;

                if (importTemplateToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(importTemplateToOpen.Id);
                }
            }
        }

        #endregion

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selected = searchResultsListBox.SelectedItem as PlanogramImportTemplateInfo;
                if(selected != null)
                    this.ViewModel.OpenCommand.Execute(selected.Id);
            }
        }
    }
}