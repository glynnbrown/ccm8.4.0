﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26891 : L.Ineson
//	Created
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Diagnostics.CodeAnalysis;
using Fluent;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Controls;
using Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using System.Diagnostics;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Data;
using System.Reflection;

namespace Galleria.Ccm.Workflow.Client.Wpf.BlockingMaintenance
{
    /// <summary>
    /// Interaction logic for BlockingMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class BlockingMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Fields

        public static String RemoveProductCommandKey = "RemoveProductCommand";

        private GridLength _structurePanelWidth = new GridLength(175D);
        private GridLength _propertiesPanelWidth = new GridLength(240.0);

        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(BlockingMaintenanceViewModel), typeof(BlockingMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public BlockingMaintenanceViewModel ViewModel
        {
            get { return (BlockingMaintenanceViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingMaintenanceOrganiser senderControl = (BlockingMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                BlockingMaintenanceViewModel oldModel = (BlockingMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.ZoomToFitCommand.Executed -= senderControl.ViewModel_ZoomToFitCommandExecuted;
                oldModel.ZoomToFitHeightCommand.Executed -= senderControl.ViewModel_ZoomToFitHeightCommandExecuted;
                oldModel.ZoomInCommand.Executed -= senderControl.ViewModel_ZoomInCommandExecuted;
                oldModel.ZoomOutCommand.Executed -= senderControl.ViewModel_ZoomOutCommandExecuted;
            }

            if (e.NewValue != null)
            {
                BlockingMaintenanceViewModel newModel = (BlockingMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.ZoomToFitCommand.Executed += senderControl.ViewModel_ZoomToFitCommandExecuted;
                newModel.ZoomToFitHeightCommand.Executed += senderControl.ViewModel_ZoomToFitHeightCommandExecuted;
                newModel.ZoomInCommand.Executed += senderControl.ViewModel_ZoomInCommandExecuted;
                newModel.ZoomOutCommand.Executed += senderControl.ViewModel_ZoomOutCommandExecuted;
            }
        }

       

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BlockingMaintenanceOrganiser()
        {
            this.AddHandler(Divider.MouseRightButtonDownEvent, (MouseButtonEventHandler)OnDividerRightClicked);

            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.BlockingMaintenance);

            this.ViewModel = new BlockingMaintenanceViewModel();


            this.Loaded += BlockingMaintenanceOrganiser_Loaded;
        }

        private void BlockingMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= BlockingMaintenanceOrganiser_Loaded;

            this.blockingEditor.LabelFormat = "{0}" + this.ViewModel.DisplayUnits.Length.Abbreviation;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        #region Viewmodel

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if(e.PropertyName == BlockingMaintenanceViewModel.DisplayUnitsProperty.Path)
            {
                this.blockingEditor.LabelFormat = "{0}" + this.ViewModel.DisplayUnits.Length.Abbreviation;
            }
        }

        private void ViewModel_ZoomToFitCommandExecuted(object sender, EventArgs e)
        {
            this.blockingEditor.ZoomMode = BlockingZoomMode.FitAll;
        }

        private void ViewModel_ZoomToFitHeightCommandExecuted(object sender, EventArgs e)
        {
            this.blockingEditor.ZoomMode = BlockingZoomMode.FitToHeight;
        }

        private void ViewModel_ZoomInCommandExecuted(object sender, EventArgs e)
        {
            this.blockingEditor.ZoomIn();
        }

        private void ViewModel_ZoomOutCommandExecuted(object sender, EventArgs e)
        {
            this.blockingEditor.ZoomOut();
        }

        #endregion

        #region Panels

        private void StructurePanel_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xContentLayout.ColumnDefinitions[index];
            _structurePanelWidth = column.Width; // Store the width of the the panel BEFORE it was collapsed.
            column.Width = GridLength.Auto; // Collapse the panel.
        }

        private void StructurePanel_OnExpanded(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xContentLayout.ColumnDefinitions[index];
            column.Width = _structurePanelWidth; // Restore the panel to the size it had before being collapsed.
        }

        private void PropertiesPanel_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xContentLayout.ColumnDefinitions[index];
            _propertiesPanelWidth = column.Width; // Store the width of the the panel BEFORE it was collapsed.
            column.Width = GridLength.Auto; // Collapse the panel.
        }

        private void PropertiesPanel_OnExpanded(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetColumn((Expander)sender);
            var column = xContentLayout.ColumnDefinitions[index];
            column.Width = _propertiesPanelWidth; // Restore the panel to the size it had before being collapsed.
        }

        #endregion


        /// <summary>
        /// Called whenever a divider is right clicked.
        /// </summary>
        private void OnDividerRightClicked(Object sender, MouseEventArgs e)
        {
            Divider div= ((DependencyObject)e.OriginalSource).FindVisualAncestor<Divider>();
            if (div == null) return;

            ShowDividerContextMenu(div.Context as BlockingDivider);
        }

        /// <summary>
        /// Called when the user right clicks on a divider grid row.
        /// </summary>
        private void DividerGrid_RowItemMouseRightClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            //make sure the row is selected
            DividerGrid.SelectedItem = e.RowItem;

            ShowDividerContextMenu(e.RowItem as BlockingDivider);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows the divider context menu.
        /// </summary>
        /// <param name="div"></param>
        private void ShowDividerContextMenu(BlockingDivider div)
        {
            Debug.Assert(div != null);
            if (div == null) return;

            //Show the context menu
            Fluent.ContextMenu cm = new Fluent.ContextMenu();

            Fluent.MenuItem item = new Fluent.MenuItem();
            item.Command = this.ViewModel.RemoveDividerCommand;
            item.CommandParameter = div;
            item.Header = this.ViewModel.RemoveDividerCommand.FriendlyName;
            item.Icon = this.ViewModel.RemoveDividerCommand.SmallIcon;
            cm.Items.Add(item);

            if (div.Type == PlanogramBlockingDividerType.Vertical)
            {
                cm.Items.Add(new Separator());

                item = new Fluent.MenuItem();
                item.IsCheckable = true;
                item.Header = Message.BlockingMaintenance_DividerSnap;

                BindingOperations.SetBinding(item, Fluent.MenuItem.IsCheckedProperty,
                    new Binding(BlockingDivider.IsSnappedProperty.Name){Source=div, Mode=BindingMode.TwoWay});
                
                cm.Items.Add(item);
            }


            cm.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            cm.IsOpen = true;
        }

        #endregion

        #region Window Close

        /// <summary>
        /// On the application being closed, checked if changes may require saving
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }

            }

        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion

        

    }
}
