﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26891 : L.Ineson
//	Created
#endregion

#endregion

using System.Windows;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.BlockingMaintenance
{
    /// <summary>
    /// Interaction logic for BlockingMaintenanceHomeTab.xaml
    /// </summary>
    public partial class BlockingMaintenanceHomeTab : RibbonTabItem
    {

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(BlockingMaintenanceViewModel), typeof(BlockingMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public BlockingMaintenanceViewModel ViewModel
        {
            get { return (BlockingMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingMaintenanceHomeTab senderControl = (BlockingMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                BlockingMaintenanceViewModel oldModel = (BlockingMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                BlockingMaintenanceViewModel newModel = (BlockingMaintenanceViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BlockingMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion

    }
}
