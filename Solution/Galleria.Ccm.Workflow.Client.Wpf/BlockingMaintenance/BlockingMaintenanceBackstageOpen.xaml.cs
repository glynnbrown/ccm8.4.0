﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26891 : L.Ineson
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using System.IO;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Text.RegularExpressions;

namespace Galleria.Ccm.Workflow.Client.Wpf.BlockingMaintenance
{
    /// <summary>
    /// Interaction logic for BlockingMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class BlockingMaintenanceBackstageOpen : UserControl
    {

        #region Fields
        private ObservableCollection<BlockingInfo> _searchResults = new ObservableCollection<BlockingInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(BlockingMaintenanceViewModel), typeof(BlockingMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public BlockingMaintenanceViewModel ViewModel
        {
            get { return (BlockingMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingMaintenanceBackstageOpen senderControl = (BlockingMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                BlockingMaintenanceViewModel oldModel = (BlockingMaintenanceViewModel)e.OldValue;
                oldModel.AvailableItems.BulkCollectionChanged -= senderControl.ViewModel_AvailableBlockingsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                BlockingMaintenanceViewModel newModel = (BlockingMaintenanceViewModel)e.NewValue;
                newModel.AvailableItems.BulkCollectionChanged += senderControl.ViewModel_AvailableBlockingsBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(BlockingMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BlockingMaintenanceBackstageOpen senderControl = (BlockingMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<BlockingInfo>),
            typeof(BlockingMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<BlockingInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<BlockingInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public BlockingMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<BlockingInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                String searchPattern = LocalHelper.GetRexexKeywordPattern(this.SearchText);

                var results =
                    this.ViewModel.AvailableItems
                    .Where(w => Regex.IsMatch(w.Name, searchPattern, RegexOptions.IgnoreCase))
                    .OrderBy(l => l.ToString());

                foreach (var info in results.OrderBy(l => l.ToString()))
                {
                    _searchResults.Add(info);
                }
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Updates the search results when the available locations list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableBlockingsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// Opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                BlockingInfo storeToOpen = (BlockingInfo)senderControl.SelectedItem;

                if (storeToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                }
            }
        }

        #endregion
    }
}
