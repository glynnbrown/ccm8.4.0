﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-26891 : L.Ineson
//	Created
// V8-27419 : L.Luong
//  Added method to change name if duplicate name has been entered
// V8-27411 : M.Pettit
//  Set Package.FetchById() to be a readonly fetch, to avoid placing a lock on the package
// V8-28283 : L.Ineson
//  Saving no longer accepts a name of just spaces.
// CCM-27599 : J.Pickup
//  Added Entity to FetchByPlanogramSelectorViewModel Constructor
#endregion

#region Version History: (CCM 8.0.1)
// V8-28936 : M.Shelley
//  Modified ".." to localised message label
#endregion

#region Version History : CCM 802
// V8-28993 : A.Kuszyk
//  Removed obselete blocking information.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.BlockingEditor;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Planograms.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Helpers;


namespace Galleria.Ccm.Workflow.Client.Wpf.BlockingMaintenance
{
    /// <summary>
    /// Viewmodel controller for BlockingMaintenanceWindow
    /// </summary>
    public sealed class BlockingMaintenanceViewModel : ViewModelAttachedControlObject<BlockingMaintenanceOrganiser>
    {
        #region Fields

        const String _exCategory = "BlockingMaintenance";//Category name for any exceptions raised to gibraltar from here

        private ModelPermission<Blocking> _itemPermissions = new ModelPermission<Blocking>();
        private ModelPermission<ProductHierarchy> _productHierarchyPermissions = new ModelPermission<ProductHierarchy>();
        private ModelPermission<PlanogramInfo> _planogramPermissions = new ModelPermission<PlanogramInfo>();
        private ModelPermission<Product> _productPermission = new ModelPermission<Product>();

        private readonly ReadOnlyCollection<ObjectFieldInfo> _availableFields;

        private readonly BlockingInfoListViewModel _masterInfoView = new BlockingInfoListViewModel();
        private Blocking _currentItem;

        private ProductGroupInfo _assignedCategory;

        private Planogram _watermarkPlanogram;

        private BlockingLocation _selectedBlockLocation;
        private BlockingGroup _selectedBlockGroup;
        private BlockingToolType _toolType;

        private DisplayUnitOfMeasureCollection _displayUnits;

        #endregion

        #region Binding Property Paths

        //Properties
        public static PropertyPath AvailableItemsProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.AvailableItems);
        public static PropertyPath CurrentItemProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.CurrentItem);
        public static PropertyPath AssignedCategoryProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.AssignedCategory);
        public static PropertyPath WatermarkPlanogramProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.WatermarkPlanogram);
        public static PropertyPath SelectedBlockLocationProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.SelectedBlockLocation);
        public static PropertyPath SelectedBlockGroupProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.SelectedBlockGroup);
        public static PropertyPath ToolTypeProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.ToolType);
        public static PropertyPath DisplayUnitsProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.DisplayUnits);

        //Commands
        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.CloseCommand);

        public static PropertyPath SetAssignedCategoryCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.SetAssignedCategoryCommand);
        public static PropertyPath SetWatermarkPlanogramCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.SetWatermarkPlanogramCommand);
        public static PropertyPath ZoomToFitCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.ZoomToFitCommand);
        public static PropertyPath ZoomToFitHeightCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.ZoomToFitHeightCommand);
        public static PropertyPath ZoomInCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.ZoomInCommand);
        public static PropertyPath ZoomOutCommandProperty = WpfHelper.GetPropertyPath<BlockingMaintenanceViewModel>(p => p.ZoomOutCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available performance selections
        /// </summary>
        public ReadOnlyBulkObservableCollection<BlockingInfo> AvailableItems
        {
            get { return _masterInfoView.BindableCollection; }
        }

        /// <summary>
        /// Returns the current performance selection.
        /// </summary>
        public Blocking CurrentItem
        {
            get { return _currentItem; }
            private set
            {
                Blocking oldValue = _currentItem;

                _currentItem = value;
                OnPropertyChanged(CurrentItemProperty);

                OnCurrentItemChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the merchandising group to which this assortment is assigned
        /// </summary>
        public ProductGroupInfo AssignedCategory
        {
            get { return _assignedCategory; }
            set
            {
                _assignedCategory = value;

                Int32? newId = (value != null) ? (Int32?)value.Id : null;

                if (this.CurrentItem != null
                    && this.CurrentItem.ProductGroupId != newId
                    && newId.HasValue)
                {
                    this.CurrentItem.ProductGroupId = newId.Value;
                }

                OnPropertyChanged(AssignedCategoryProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the planogram to use as a watermark
        /// </summary>
        public Planogram WatermarkPlanogram
        {
            get { return _watermarkPlanogram; }
            set
            {
                if (value != null)
                {
                    //work out the new block dimensions
                    Single blockingHeight, blockingWidth;
                    value.GetBlockingAreaSize(out blockingHeight, out blockingWidth);

                    try
                    {
                        this.CurrentItem.Height = blockingHeight;
                        this.CurrentItem.Width = blockingWidth;
                    }
                    catch (Csla.PropertyLoadException)
                    { }
                }


                _watermarkPlanogram = value;

                Int32? newId = (value != null) ? (Int32?)value.Id : null;

                if (this.CurrentItem != null
                    && this.CurrentItem.PlanogramId != newId
                    && newId.HasValue)
                {
                    this.CurrentItem.PlanogramId = newId.Value;
                }

                OnPropertyChanged(WatermarkPlanogramProperty);

                OnWatermarkPlanogramChanged(value);
            }
        }

        /// <summary>
        /// Gets/Sets the selected block location.
        /// </summary>
        public BlockingLocation SelectedBlockLocation
        {
            get { return _selectedBlockLocation; }
            set
            {
                _selectedBlockLocation = value;
                OnPropertyChanged(SelectedBlockLocationProperty);

                this.SelectedBlockGroup =
                    (value != null) ? value.GetBlockingGroup() : null;
            }
        }

        /// <summary>
        /// Resturns the currently selected blocking group.
        /// </summary>
        public BlockingGroup SelectedBlockGroup
        {
            get { return _selectedBlockGroup; }
            set
            {
                if (_selectedBlockGroup != value)
                {
                    BlockingGroup oldValue = _selectedBlockGroup;

                    _selectedBlockGroup = value;
                    OnPropertyChanged(SelectedBlockGroupProperty);

                    OnSelectedBlockGroupChanged(oldValue, value);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the active mouse tool type
        /// </summary>
        public BlockingToolType ToolType
        {
            get { return _toolType; }
            set
            {
                _toolType = value;
                OnPropertyChanged(ToolTypeProperty);
            }
        }

        /// <summary>
        /// Returns the display uom collection to use.
        /// </summary>
        public DisplayUnitOfMeasureCollection DisplayUnits
        {
            get { return _displayUnits; }
            private set
            {
                _displayUnits = value;
                OnPropertyChanged(DisplayUnitsProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public BlockingMaintenanceViewModel()
        {
            _displayUnits = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.CurrentEntityView.Model);

            _masterInfoView.FetchForCurrentEntity();
            _availableFields = new ReadOnlyCollection<ObjectFieldInfo>(Product.EnumerateDisplayableFieldInfos().ToList());

            //load a new item.
            NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Loads a new store
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(
                            p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //create a new item
                Blocking newItem = Blocking.NewBlocking(App.ViewState.EntityId);
                newItem.Height = 160;
                newItem.Width = 250;
                newItem.MarkGraphAsInitialized();

                //close the backstage
                this.AttachedControl.SetRibbonBackstageState(false);
                this.CurrentItem = newItem;
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand _openCommand;
        /// <summary>
        /// Loads the requested store
        /// parameter = the id of the store to open
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Object id)
        {
            //user must have get permission
            if (!_itemPermissions.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (id == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Object id)
        {
            Int32? itemId = id as Int32?;
            if (itemId.HasValue)
            {
                //check if current item requires save first
                if (ContinueWithItemChange())
                {
                    base.ShowWaitCursor(true);

                    try
                    {
                        //fetch the requested item
                        this.CurrentItem = Blocking.FetchById(itemId.Value);
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        LocalHelper.RecordException(ex);
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                        base.ShowWaitCursor(true);
                    }


                    //close the backstage
                    LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                    base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_itemPermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.CurrentItem == null)
            {
                this.SaveCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                this.SaveAndNewCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                this.SaveAndCloseCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                return false;
            }

            //must be valid
            if (!this.CurrentItem.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            Int32 itemId = this.CurrentItem.Id;

            //** check the item unique value
            String newName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                _masterInfoView.FetchForCurrentEntity();

                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(
                    (s) =>
                    {
                        // V8-28283 -  return false if empty.
                        if (String.IsNullOrEmpty(s.Trim())) return false;

                        return !this.AvailableItems.Any(p =>
                        p.Id != itemId && p.Name.ToLowerInvariant() == s.ToLowerInvariant());

                    },
                    this.CurrentItem.Name, out newName);
            }
            else
            {
                //force a unique value for unit testing
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.CurrentItem.Name,
                    this.AvailableItems.Where(p => p.Id != itemId).Select(p => p.Name));
            }

            //set the code
            if (nameAccepted)
            {
                if (this.CurrentItem.Name != newName)
                {
                    this.CurrentItem.Name = newName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.CurrentItem = this.CurrentItem.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //[SA-17668] set return flag to false as save was not completed.
                    continueWithSave = false;

                    LocalHelper.RecordException(ex);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.CurrentItem.Name);
                        if (itemReloadRequired)
                        {
                            this.CurrentItem = Blocking.FetchById(this.CurrentItem.Id);
                        }
                    }
                    else if (rootException is Galleria.Framework.Dal.DuplicateException)
                    {
                       App.ShowWindow(
                           new ModalMessage()
                           {
                               Header = Message.BlockingMaintenance_DuplicateDeletedError_Header,
                               Description = Message.BlockingMaintenance_DuplicateDeletedError_Description,
                               MessageIcon = ImageResources.Dialog_Error,
                               Button1Content = Message.Generic_Ok,
                               ButtonCount = 1

                           }, this.AttachedControl, true);
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.CurrentItem.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //refresh the info list
                _masterInfoView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }

            return continueWithSave;

        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(p),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //must be able to execute save
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAsCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed(Object args)
        {
            //** confirm the name to save as
            String copyName;
            Boolean nameAccepted = true;

            if (this.AttachedControl != null)
            {
                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptForSaveAsName((s) =>
                {
                    // V8-28283 -  return false if empty.
                    if (String.IsNullOrEmpty(s.Trim())) return false;

                    return !this.AvailableItems.Any(p => p.Name.ToLowerInvariant() == s.ToLowerInvariant());

                }, String.Empty, out copyName);
            }
            else
            {
                //use the name that was passed through.
                copyName = args as String;
            }

            if (nameAccepted)
            {
                base.ShowWaitCursor(true);

                //copy the item.
                Blocking itemCopy = this.CurrentItem.Copy();
                itemCopy.Name = copyName;
                this.CurrentItem = itemCopy;

                base.ShowWaitCursor(false);

                //save it.
                SaveCurrentItem();
            }
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// Parameter = store to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_itemPermissions.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.CurrentItem == null)
            {
                _deleteCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                return false;
            }

            //must not be new
            if (this.CurrentItem.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.CurrentItem.Name.ToString());
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                var itemToDelete = this.CurrentItem;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //update the available items
                _masterInfoView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region SetAssignedCategoryCommand

        private RelayCommand _setAssignedCategoryCommand;

        /// <summary>
        /// Sets the blocking productgroupid
        /// </summary>
        public RelayCommand SetAssignedCategoryCommand
        {
            get
            {
                if (_setAssignedCategoryCommand == null)
                {
                    _setAssignedCategoryCommand =
                        new RelayCommand(
                            p => SetAssignedCategory_Executed(),
                            p => SetAssignedCategory_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ellipsis,
                        };
                    this.ViewModelCommands.Add(_setAssignedCategoryCommand);
                }
                return _setAssignedCategoryCommand;
            }
        }

        private Boolean SetAssignedCategory_CanExecute()
        {
            //must have an item selected
            if (this.CurrentItem == null)
            {
                this.SetAssignedCategoryCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                return false;
            }

            //user must have product group fetch permission
            if (!_productHierarchyPermissions.CanFetch)
            {
                this.SetAssignedCategoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SetAssignedCategory_Executed()
        {
            if (this.AttachedControl != null)
            {
                MerchGroupSelectionWindow merchandisingHierarchyWindow = new MerchGroupSelectionWindow();
                merchandisingHierarchyWindow.SelectionMode = DataGridSelectionMode.Single;

                App.ShowWindow(merchandisingHierarchyWindow, this.AttachedControl, true);

                if (merchandisingHierarchyWindow.DialogResult == true)
                {
                    this.AssignedCategory = ProductGroupInfo.NewProductGroupInfo(merchandisingHierarchyWindow.SelectionResult[0]);
                }
            }
        }

        #endregion

        #region SetWatermarkPlanogramCommand

        private RelayCommand _setWatermarkPlanogramCommand;

        /// <summary>
        /// Allows the user to select a plan to use as a watermark.
        /// </summary>
        public RelayCommand SetWatermarkPlanogramCommand
        {
            get
            {
                if (_setWatermarkPlanogramCommand == null)
                {
                    _setWatermarkPlanogramCommand =
                        new RelayCommand(
                            p => SetWatermarkPlanogram_Executed(),
                            p => SetWatermarkPlanogram_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ellipsis,
                        };
                    base.ViewModelCommands.Add(_setWatermarkPlanogramCommand);
                }
                return _setWatermarkPlanogramCommand;
            }
        }

        private Boolean SetWatermarkPlanogram_CanExecute()
        {
            //must have an item selected
            if (this.CurrentItem == null)
            {
                this.SetAssignedCategoryCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                return false;
            }

            //user must have planogram fetch permission
            if (!_planogramPermissions.CanFetch)
            {
                this.SetAssignedCategoryCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SetWatermarkPlanogram_Executed()
        {
            PlanogramSelectorViewModel winView = new PlanogramSelectorViewModel(App.ViewState.CustomColumnLayoutFolderPath, App.ViewState.EntityId);
            winView.ShowDialog(this.AttachedControl);

            if (winView.DialogResult == true)
            {
                base.ShowWaitCursor(true);

                var newValue = winView.SelectedPlanogramInfo;

                //set the watermark
                Planogram plan = null;
                try
                {
                    plan = Package.FetchById(newValue.Id).Planograms.FirstOrDefault();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    Exception rootEx = ex.GetBaseException();
                    LocalHelper.RecordException(rootEx);

                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                        newValue.Name, OperationType.Open);
                }

                if (plan != null)
                {
                    this.WatermarkPlanogram = plan;
                }

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region RemoveDividerCommand

        private RelayCommand _removeDividerCommand;

        /// <summary>
        /// Removes the given divider
        /// </summary>
        public RelayCommand RemoveDividerCommand
        {
            get
            {
                if (_removeDividerCommand == null)
                {
                    _removeDividerCommand = new RelayCommand(
                        p => RemoveDivider_Executed(p),
                        p => RemoveDivider_CanExecute(p))
                    {
                        FriendlyName = Message.BlockingMaintenance_DeleteDividerTool,
                        SmallIcon = ImageResources.BlockingMaintenance_DeleteDividerTool32
                    };
                    base.ViewModelCommands.Add(_removeDividerCommand);
                }
                return _removeDividerCommand;
            }
        }

        private Boolean RemoveDivider_CanExecute(Object args)
        {
            if (this.CurrentItem == null || args == null)
            {
                this.RemoveDividerCommand.DisabledReason = Message.Generic_NoItemSelected;
                return false;
            }

            return true;
        }

        private void RemoveDivider_Executed(Object args)
        {
            BlockingDivider div = args as BlockingDivider;
            if (args == null) return;


            this.CurrentItem.Dividers.Remove(div);
        }

        #endregion

        #region ZoomToFit

        private RelayCommand _zoomToFitCommand;

        /// <summary>
        /// Zooms to fit the entire blocking area
        /// </summary>
        public RelayCommand ZoomToFitCommand
        {
            get
            {
                if (_zoomToFitCommand == null)
                {
                    _zoomToFitCommand = new RelayCommand(
                        p => ZoomToFit_Executed(),
                        p => ZoomToFit_CanExecute())
                    {
                        FriendlyName = Message.BlockingMaintenance_ZoomToFit,
                        FriendlyDescription = Message.BlockingMaintenance_ZoomToFit_Desc,
                        SmallIcon = ImageResources.BlockingMaintenance_ZoomToFit16
                    };
                    base.ViewModelCommands.Add(_zoomToFitCommand);
                }
                return _zoomToFitCommand;
            }
        }

        private Boolean ZoomToFit_CanExecute()
        {
            if (this.CurrentItem == null)
            {
                this.ZoomToFitCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                return false;
            }

            return true;
        }

        private void ZoomToFit_Executed()
        {
            //Handled in code behind.
        }

        #endregion

        #region ZoomToFitHeight

        private RelayCommand _zoomToFitHeightCommand;

        /// <summary>
        /// Fits the blocking area by height
        /// </summary>
        public RelayCommand ZoomToFitHeightCommand
        {
            get
            {
                if (_zoomToFitHeightCommand == null)
                {
                    _zoomToFitHeightCommand = new RelayCommand(
                        p => ZoomToFitHeight_Executed(),
                        p => ZoomToFitHeight_CanExecute())
                    {
                        FriendlyName = Message.BlockingMaintenance_ZoomToFitHeight,
                        FriendlyDescription = Message.BlockingMaintenance_ZoomToFitHeight_Desc,
                        Icon = ImageResources.BlockingMaintenance_ZoomToFitHeight32,
                        SmallIcon = ImageResources.BlockingMaintenance_ZoomToFitHeight16
                    };
                    base.ViewModelCommands.Add(_zoomToFitHeightCommand);
                }
                return _zoomToFitHeightCommand;
            }
        }

        private Boolean ZoomToFitHeight_CanExecute()
        {
            if (this.CurrentItem == null)
            {
                this.ZoomToFitHeightCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                return false;
            }

            return true;
        }

        private void ZoomToFitHeight_Executed()
        {
            //Handled in code behind.
        }

        #endregion

        #region ZoomIn
        private RelayCommand _zoomInCommand;

        /// <summary>
        /// Zooms in on the blocking area.
        /// </summary>
        public RelayCommand ZoomInCommand
        {
            get
            {
                if (_zoomInCommand == null)
                {
                    _zoomInCommand = new RelayCommand(
                        p => ZoomIn_Executed(),
                        p => ZoomIn_CanExecute())
                    {
                        FriendlyName = Message.BlockingMaintenance_ZoomIn,
                        FriendlyDescription = Message.BlockingMaintenance_ZoomIn_Desc,
                        SmallIcon = ImageResources.BlockingMaintenance_ZoomIn16,
                    };
                    base.ViewModelCommands.Add(_zoomInCommand);
                }
                return _zoomInCommand;
            }
        }

        private Boolean ZoomIn_CanExecute()
        {
            if (this.CurrentItem == null)
            {
                this.ZoomInCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                return false;
            }

            return true;
        }

        private void ZoomIn_Executed()
        {
        }

        #endregion

        #region ZoomOut

        private RelayCommand _zoomOutCommand;

        /// <summary>
        /// Zooms out the blocking area.
        /// </summary>
        public RelayCommand ZoomOutCommand
        {
            get
            {
                if (_zoomOutCommand == null)
                {
                    _zoomOutCommand = new RelayCommand(
                        p => ZoomOut_Executed(),
                        p => ZoomOut_CanExecute())
                    {
                        FriendlyName = Message.BlockingMaintenance_ZoomOut,
                        FriendlyDescription = Message.BlockingMaintenance_ZoomOut_Desc,
                        SmallIcon = ImageResources.BlockingMaintenance_ZoomOut16,
                    };
                    base.ViewModelCommands.Add(_zoomOutCommand);
                }
                return _zoomOutCommand;
            }
        }

        private Boolean ZoomOut_CanExecute()
        {
            if (this.CurrentItem == null)
            {
                this.ZoomOutCommand.DisabledReason = Message.BlockingMaintenance_NoCurrentItem;
                return false;
            }

            return true;
        }

        private void ZoomOut_Executed()
        {
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current performance selection property value changes.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnCurrentItemChanged(Blocking oldValue, Blocking newValue)
        {
            if (oldValue != null)
            {
                oldValue.Groups.BulkCollectionChanged -= CurrentItem_GroupsBulkCollectionChanged;
                oldValue.ChildChanged -= CurrentItem_ChildChanged;
            }

            if (newValue != null)
            {
                //get the assigned category info and watermark
                try
                {
                    this.AssignedCategory = ProductGroupInfoList.FetchByProductGroupIds(
                        new List<Int32> { newValue.ProductGroupId }).FirstOrDefault();

                    this.WatermarkPlanogram =
                        (newValue.PlanogramId.HasValue) ?
                        Package.FetchById(newValue.PlanogramId.Value).Planograms.FirstOrDefault()
                        : null;
                }
                catch (DataPortalException ex)
                {
                    LocalHelper.RecordException(ex.GetBaseException());
                }

                this.SelectedBlockLocation = newValue.Locations.FirstOrDefault();

                newValue.Groups.BulkCollectionChanged += CurrentItem_GroupsBulkCollectionChanged;
                newValue.ChildChanged += CurrentItem_ChildChanged;
            }
            else
            {
                this.AssignedCategory = null;
                this.WatermarkPlanogram = null;
                this.SelectedBlockLocation = null;
            }
        }

        /// <summary>
        /// Called whenever the dividers collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentItem_GroupsBulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            if (this.SelectedBlockGroup == null
                && this.CurrentItem.Groups.Count > 0)
            {
                this.SelectedBlockGroup = this.CurrentItem.Groups.FirstOrDefault();
            }
        }

        /// <summary>
        /// Called whenever the selected block group changes
        /// </summary>
        /// <param name="group"></param>
        private void OnSelectedBlockGroupChanged(BlockingGroup oldValue, BlockingGroup newValue)
        {
            if (oldValue != null)
            {
                oldValue.ChildChanged -= SelectedBlockGroup_ChildChanged;
            }

            if (newValue != null)
            {
                //select the first location.
                if (this.SelectedBlockLocation == null
                   || !this.CurrentItem.Locations.Contains(this.SelectedBlockLocation)
                    || this.SelectedBlockLocation.BlockingGroupId != newValue.Id)
                {
                    this.SelectedBlockLocation =
                        this.CurrentItem.Locations.FirstOrDefault(l => l.BlockingGroupId == newValue.Id);
                }

                newValue.ChildChanged += SelectedBlockGroup_ChildChanged;
            }
            else
            {
                this.SelectedBlockLocation = null;
            }
        }

        private void CurrentItem_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs != null
                && e.ChildObject is BlockingGroup)
            {
                if (e.PropertyChangedArgs.PropertyName == BlockingGroup.NameProperty.Name)
                {
                    IEnumerable<BlockingGroup> groups = this.CurrentItem.Groups;
                    if (groups.Select(p => p.Name.ToLowerInvariant().Trim()).Distinct().Count() < groups.Count())
                    {
                        if (groups.Where(p => p.Name.ToLowerInvariant().Trim() == SelectedBlockGroup.Name.ToLowerInvariant().Trim()).Count() > 1)
                        {
                            Int32 countDuplication = 0;
                            String newName = SelectedBlockGroup_NameDuplication(SelectedBlockGroup.Name.Trim(), countDuplication);
                            SelectedBlockGroup.Name = newName;
                        }
                    }
                }
            }
        }

        private String SelectedBlockGroup_NameDuplication(String groupName, Int32 count)
        {
            String name = String.Format("{0} ({1})", groupName, count);
            IEnumerable<BlockingGroup> groups = this.CurrentItem.Groups;
            if (groups.Any(p => p.Name.ToLowerInvariant().Trim() == name.ToLowerInvariant().Trim()))
            {
                count++;
                return SelectedBlockGroup_NameDuplication(groupName, count);
            }
            else
            {
                return name;
            }
        }

        /// <summary>
        /// Called whenever a child object of the selected blocking group changes
        /// </summary>
        private void SelectedBlockGroup_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            // Nothing to do here - we used to update the dynamic/static product lists.
        }

        /// <summary>
        /// Called whenever the watermark planogram changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnWatermarkPlanogramChanged(Planogram newValue)
        {
            if (newValue != null)
            {
                this.DisplayUnits = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(newValue);
            }
            else
            {
                this.DisplayUnits = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.CurrentEntityView.Model);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            return CommonHelper.GetWindowService().ContinueWithItemChange(this.CurrentItem, this.SaveCommand);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                this.CurrentItem = null;

                _masterInfoView.Dispose();


                base.IsDisposed = true;
            }
        }

        #endregion

    }

}
