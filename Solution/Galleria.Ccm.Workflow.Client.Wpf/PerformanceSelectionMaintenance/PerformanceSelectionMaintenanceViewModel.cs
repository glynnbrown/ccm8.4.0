﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
// CCM-26953 : I.George
// Added MetricRowView
#endregion
#region Version History: (CCM 8.1.1)
// CCM:30325 : I.George
// Added friendly description to SaveAs and Delete commands
#endregion
#region Version History: (CCM 8.2)
// V8-31041 : L.Ineson
//  Stopped timeline groups being cleared when doing save as.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.GFSModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance
{
    /// <summary>
    /// Viewmodel controller for PerformanceSelectionMaintenanceWindow
    /// </summary>
    public sealed class PerformanceSelectionMaintenanceViewModel : ViewModelAttachedControlObject<PerformanceSelectionMaintenanceWindow>
    {
        #region Fields

        private GFSModel.Entity _gfsEntity;

        private ModelPermission<PerformanceSelection> _performanceSelectionPermissions = new ModelPermission<PerformanceSelection>();

        private readonly PerformanceSelectionInfoListViewModel _masterPerformanceSelectionsView = new PerformanceSelectionInfoListViewModel();
        private PerformanceSelection _currentItem;

        private readonly GFSPerformanceSourceListViewModel _gfsPerformanceSourceListView = new GFSPerformanceSourceListViewModel();
        private GFSModel.PerformanceSource _selectedPerformanceSource;

        private readonly GFSTimelineGroupListViewModel _gfsTimelineGroupListView = new GFSTimelineGroupListViewModel();
        private readonly BulkObservableCollection<TimelineGroupView> _timelineGroups = new BulkObservableCollection<TimelineGroupView>();
        private ReadOnlyBulkObservableCollection<TimelineGroupView> _timelineGroupsRO;

        private String _warningText;

        private BulkObservableCollection<MetricRowView> _availbleMetricRows = new BulkObservableCollection<MetricRowView>();
        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableItemsProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.AvailableItems);
        public static readonly PropertyPath CurrentItemProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.CurrentItem);
        public static readonly PropertyPath PerformanceSourcesProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.PerformanceSources);
        public static readonly PropertyPath SelectedPerformanceSourceProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.SelectedPerformanceSource);
        public static readonly PropertyPath TimelineGroupsProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.TimelineGroups);
        public static readonly PropertyPath SelectedTimelineGroupsCountProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.SelectedTimelineGroupsCount);
        public static readonly PropertyPath WarningTextProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.WarningText);
        public static readonly PropertyPath AvailableMetricRowsProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.AvailableMetricRows);
        
        //Commands
        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<PerformanceSelectionMaintenanceViewModel>(v => v.CloseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available performance selections
        /// </summary>
        public ReadOnlyBulkObservableCollection<PerformanceSelectionInfo> AvailableItems
        {
            get { return _masterPerformanceSelectionsView.BindingView; }
        }

        /// <summary>
        /// Returns the current performance selection.
        /// </summary>
        public PerformanceSelection CurrentItem
        {
            get { return _currentItem; }
            private set
            {
                PerformanceSelection oldValue = _currentItem;
                _currentItem = value;
                OnPropertyChanged(CurrentItemProperty);
                OnCurrentItemChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the collection of gfs performance sources. If the selected source is not available
        /// then this will also be included.
        /// </summary>
        public ReadOnlyBulkObservableCollection<GFSModel.PerformanceSource> PerformanceSources
        {
            get { return _gfsPerformanceSourceListView.BindingView; }
        }

        /// <summary>
        /// Gets/Sets the selected performance source.
        /// </summary>
        public GFSModel.PerformanceSource SelectedPerformanceSource
        {
            get { return _selectedPerformanceSource; }
            set
            {
                var oldValue = _selectedPerformanceSource;

                _selectedPerformanceSource = value;
                OnPropertyChanged(SelectedPerformanceSourceProperty);
                OnSelectedPerformanceSourceChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns a collection of available metric rows
        /// </summary>
        public BulkObservableCollection<MetricRowView> AvailableMetricRows
        {
            get { return _availbleMetricRows; }
        }

      
        /// <summary>
        /// Returns the collection of timeline groups.
        /// </summary>
        public ReadOnlyBulkObservableCollection<TimelineGroupView> TimelineGroups
        {
            get
            {
                if (_timelineGroupsRO == null)
                {
                    _timelineGroupsRO = new ReadOnlyBulkObservableCollection<TimelineGroupView>(_timelineGroups);
                }
                return _timelineGroupsRO;
            }
        }

        /// <summary>
        /// Returns a count of the number of selected timeline groups.
        /// </summary>
        public Int32 SelectedTimelineGroupsCount
        {
            get { return TimelineGroups.Count(t => t.IsSelected); }
        }

        /// <summary>
        /// Gets a warning to be shown when an existing performance selection is loaded.
        /// This may be that the performance source is 
        /// </summary>
        public String WarningText
        {
            get { return _warningText; }
            private set
            {
                _warningText = value;
                OnPropertyChanged(WarningTextProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PerformanceSelectionMaintenanceViewModel()
        {
            _timelineGroups.BulkCollectionChanged += TimelineGroups_BulkCollectionChanged;
           

            _gfsPerformanceSourceListView.ModelChanged += GFSPerformanceSourceListView_ModelChanged;
            _gfsTimelineGroupListView.ModelChanged += GFSTimelineGroupListView_ModelChanged;
            

            _masterPerformanceSelectionsView.FetchForCurrentEntity();

            _gfsEntity = GFSModel.Entity.FetchByCcmEntity(App.ViewState.CurrentEntityView.Model);
            _gfsPerformanceSourceListView.FetchAll(_gfsEntity);

            //load a new item.
            NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Loads a new store
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(
                            p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_performanceSelectionPermissions.CanCreate)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //create a new item
                PerformanceSelection newItem = PerformanceSelection.NewPerformanceSelection(App.ViewState.EntityId);

                this.CurrentItem = newItem;

                newItem.MarkGraphAsInitialized();

                //close the backstage
                this.AttachedControl.SetRibbonBackstageState(false);
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand _openCommand;
        /// <summary>
        /// Loads the requested store
        /// parameter = the id of the store to open
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Object id)
        {
            //user must have get permission
            if (!_performanceSelectionPermissions.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (id == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Object id)
        {
            Int32? itemId = id as Int32?;
            if (itemId.HasValue)
            {
                //check if current item requires save first
                if (ContinueWithItemChange())
                {
                    base.ShowWaitCursor(true);

                    try
                    {
                        //fetch the requested item
                        this.CurrentItem = PerformanceSelection.FetchById(itemId.Value);
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        LocalHelper.RecordException(ex);
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                        base.ShowWaitCursor(true);
                    }


                    //close the backstage
                    LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                    base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_performanceSelectionPermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.CurrentItem == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentItem.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            if (this.SelectedPerformanceSource == null)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem(/*updateTimelineGroups*/true);
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem(Boolean updateTimelineGroups)
        {
            //first update the selected groups
            if (updateTimelineGroups)
            {
                UpdatePerformanceSelectionTimelineGroups();
            }

            Boolean continueWithSave = true;

            Int32 itemId = this.CurrentItem.Id;

            //** check the item unique value
            String newName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                _masterPerformanceSelectionsView.FetchForCurrentEntity();

                Predicate<String> isUniqueCheck = (s) =>
                    {
                        Boolean returnValue = true;
                        base.ShowWaitCursor(true);

                        foreach (PerformanceSelectionInfo performanceSelectionInfo in PerformanceSelectionInfoList.FetchByEntityIdIncludingDeleted(App.ViewState.EntityId))
                        {
                            if (performanceSelectionInfo.Id != itemId && performanceSelectionInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                            {
                                returnValue = false;
                                break;
                            }
                        }
                        base.ShowWaitCursor(false);

                        return returnValue;
                    };

                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(String.Empty, String.Empty, Message.PerformanceSelectionMaintenance_NameAlreadyInUse, PerformanceSelection.NameProperty.FriendlyName,  /*forceFirstShow*/false, isUniqueCheck, this.CurrentItem.Name, out newName);
            }
            else
            {
                //force a unique value for unit testing
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.CurrentItem.Name,
                    this.AvailableItems.Where(p => p.Id != itemId).Select(p => p.Name));
            }

            //set the code
            if (nameAccepted)
            {
                if (this.CurrentItem.Name != newName)
                {
                    this.CurrentItem.Name = newName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.CurrentItem = this.CurrentItem.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //[SA-17668] set return flag to false as save was not completed.
                    continueWithSave = false;

                    LocalHelper.RecordException(ex);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.CurrentItem.Name);
                        if (itemReloadRequired)
                        {
                            this.CurrentItem = PerformanceSelection.FetchById(this.CurrentItem.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.CurrentItem.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //refresh the info list
                _masterPerformanceSelectionsView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }
            return continueWithSave;

        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(p),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_performanceSelectionPermissions.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //must be able to execute save
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAsCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed(Object args)
        {
            //** confirm the name to save as
            String copyName;
            Boolean nameAccepted = true;

            if (this.AttachedControl != null)
            {
                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptForSaveAsName((s) =>
                {
                    return !this.AvailableItems.Any(p => p.Name.ToLowerInvariant() == s.ToLowerInvariant());

                }, String.Empty, out copyName);
            }
            else
            {
                //use the name that was passed through.
                copyName = args as String;
            }

            if (nameAccepted)
            {
                base.ShowWaitCursor(true);

                //copy the item.
                PerformanceSelection itemCopy = this.CurrentItem.Copy();
                itemCopy.Name = copyName;
                this.CurrentItem = itemCopy;

                base.ShowWaitCursor(false);

                //save it.
                SaveCurrentItem(/*updateTimelineGroups*/false);
            }
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem(/*updateTimelineGroups*/true);

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem(/*updateTimelineGroups*/true);

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// Parameter = store to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_performanceSelectionPermissions.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.CurrentItem == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.CurrentItem.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.CurrentItem.ToString());
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                var itemToDelete = this.CurrentItem;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //update the available items
                _masterPerformanceSelectionsView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the GFSPerformanceSourceListView model changes
        /// </summary>
        private void GFSPerformanceSourceListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<GFSModel.PerformanceSourceList> e)
        {
            UpdateSelectedPerformanceSource();
        }

        /// <summary>
        /// Called whenever the GFSTimelineGroupListView model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GFSTimelineGroupListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<GFSModel.TimelineGroupList> e)
        {
            UpdateTimelineGroups();
        }
        
        /// <summary>
        /// Called whenever the current performance selection property value changes.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnCurrentItemChanged(PerformanceSelection oldValue, PerformanceSelection newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= CurrentPerformanceSelection_PropertyChanged;
            }

            if (newValue != null)
            {
                newValue.PropertyChanged += CurrentPerformanceSelection_PropertyChanged;
            }

            UpdateSelectedPerformanceSource();
        }

        /// <summary>
        /// Called whenever a property changes on the current performance selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentPerformanceSelection_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PerformanceSelection.SelectionTypeProperty.Name)
            {
                //TODO: invert selection?
            }
        }

        /// <summary>
        /// Called whenever the value of the SelectedPerformanceSource property changes.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnSelectedPerformanceSourceChanged(GFSModel.PerformanceSource oldValue, GFSModel.PerformanceSource newValue)
        {
            _gfsTimelineGroupListView.Model = null;
         
            if (newValue != null)
            {
                //update the id on the current item.
                PerformanceSelection currentSelection = this.CurrentItem;
                if (currentSelection != null
                    && currentSelection.GFSPerformanceSourceId != newValue.Id)
                {
                    //clear off the old groups
                    if (currentSelection.TimelineGroups.Count > 0) currentSelection.TimelineGroups.Clear();

                    //set the new id.
                    currentSelection.GFSPerformanceSourceId = newValue.Id;

                    UpdateMetricRows();
                   
                }
                //fetch the new timeline groups
                _gfsTimelineGroupListView.BeginFetchByPerformanceSource(_gfsEntity, newValue);
            }

        }

        /// <summary>
        /// Called whenever the timeline groups collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimelineGroups_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (TimelineGroupView groupView in e.ChangedItems)
                    {
                        groupView.PropertyChanged += TimelineGroupView_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (TimelineGroupView groupView in e.ChangedItems)
                    {
                        groupView.PropertyChanged -= TimelineGroupView_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (TimelineGroupView groupView in e.ChangedItems)
                        {
                            groupView.PropertyChanged -= TimelineGroupView_PropertyChanged;
                        }
                    }
                    foreach (TimelineGroupView groupView in _timelineGroups)
                    {
                        groupView.PropertyChanged += TimelineGroupView_PropertyChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever a property changes on a timeline group view.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimelineGroupView_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TimelineGroupView.IsSelectedProperty.Path)
            {
                //notify that the selection count has changed.
                OnPropertyChanged(SelectedTimelineGroupsCountProperty);

                //mark the current item as dirty so that we know the group selection has changed.
                if (this.CurrentItem != null && !this.CurrentItem.IsDirty)
                {
                    this.CurrentItem.MarkGraphAsDirty();
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null )
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.CurrentItem, this.SaveCommand);
            }
            else if (this.CurrentItem != null && this.CurrentItem.IsDirty && !this.CurrentItem.IsInitialized)
            {
                //we are unit testing so just fire off a property change if the dialog would be shown.
                 OnPropertyChanged("ContinueWithItemChange");
            }
            return continueExecute;
        }

        /// <summary>
        /// Refreshes the Performance Sources collection
        /// </summary>
        private void UpdateSelectedPerformanceSource()
        {
            //select the first available performance source
            if (this.CurrentItem != null
                && this.CurrentItem.GFSPerformanceSourceId != 0)
            {
                var source = this.PerformanceSources.FirstOrDefault(p => p.Id == this.CurrentItem.GFSPerformanceSourceId);

                if (source == null)
                {
                    //the source is no longer available so set
                    // the warning and select the first that is.
                    this.WarningText = Message.PerformanceSelectionMaintenance_WarningSourceNotAvailable;

                    this.SelectedPerformanceSource = this.PerformanceSources.FirstOrDefault();
                   
                }
                else
                {
                    this.WarningText = null;
                    this.SelectedPerformanceSource = source;
                    //update the metric rows
                    UpdateMetricRows();
                }
            }
            else
            {
                this.SelectedPerformanceSource = this.PerformanceSources.FirstOrDefault();
            }
        }

        /// <summary>
        /// Updates the timeline groups collection.
        /// </summary>
        private void UpdateTimelineGroups()
        {
            if (this.CurrentItem == null) return;
            if (_timelineGroups.Count > 0) _timelineGroups.Clear();

            if (this.SelectedPerformanceSource != null
                && _gfsTimelineGroupListView.Model != null)
            {

                PerformanceSelectionTimeType timeType = this.CurrentItem.TimeType;

                List<TimelineGroupView> groups = new List<TimelineGroupView>();

                foreach (var gfsGroup in _gfsTimelineGroupListView.Model)
                {
                    groups.Add(new TimelineGroupView(gfsGroup));
                }

                if (groups.Count > 0)
                {
                    timeType = groups[0].TimelineFrequency;
                }


                if (this.CurrentItem != null)
                {
                    List<Int64> unavailableGroups = new List<Int64>();

                    foreach (Int64 selectedCode in
                        this.CurrentItem.TimelineGroups.Select(t => t.Code))
                    {
                        TimelineGroupView group = groups.FirstOrDefault(g => g.Code == selectedCode);
                        if (group == null)
                        {
                            unavailableGroups.Add(selectedCode);
                        }
                        else
                        {
                            group.IsSelected = true;
                        }
                    }

                    if (unavailableGroups.Count > 0  && this.WarningText == null)
                    {
                        this.WarningText = Message.PerformanceSelectionMaintenance_UnavailableTimelineGroups;
                    }
                }
                _timelineGroups.AddRange(groups.OrderBy(g => g.Code));


                if (this.CurrentItem.TimeType != timeType)
                {
                    this.CurrentItem.TimeType = timeType;
                }
                if (this._currentItem != null)
                {
                    _currentItem.MarkGraphAsInitialized();
                }
            }

            OnPropertyChanged(SelectedTimelineGroupsCountProperty);
        }

        /// <summary>
        /// Updates the selected timeline groups
        /// on the current performance selection
        /// </summary>
        private void UpdatePerformanceSelectionTimelineGroups()
        {
            List<Int64> groupsToSelect =
                this.TimelineGroups.Where(t => t.IsSelected).Select(t => t.Code).ToList();

            PerformanceSelection item = this.CurrentItem;
            
            if(item.TimelineGroups.Count > 0) item.TimelineGroups.Clear();

            foreach (Int64 code in groupsToSelect)
            {
                item.TimelineGroups.Add(code);
            }
        }

        /// <summary>
        /// Update the metric Row with data from the performance source
        /// </summary>
        private void UpdateMetricRows()
        {
            List<PerformanceSelectionMetric> toAdd = new List<PerformanceSelectionMetric>();

            _availbleMetricRows.Clear();
            // loads the performance selection name from the performance source
            foreach (PerformanceSourceMetric metric in _selectedPerformanceSource.PerformanceSourceMetrics)
            {
                PerformanceSelectionMetric performanceSelectionMetric = _currentItem.Metrics.FirstOrDefault(p => p.GFSPerformanceSourceMetricId == metric.Id);
                if (performanceSelectionMetric == null)
                {
                    performanceSelectionMetric = PerformanceSelectionMetric.NewPerformanceSelectionMetric();
                    performanceSelectionMetric.AggregationType = metric.AggregationType;
                    performanceSelectionMetric.GFSPerformanceSourceMetricId = metric.Id;
                    toAdd.Add(performanceSelectionMetric);
                }
                _availbleMetricRows.Add(new MetricRowView(metric, performanceSelectionMetric));
            }

            _currentItem.Metrics.AddRange(toAdd);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _timelineGroups.Clear();
                _timelineGroups.BulkCollectionChanged -= TimelineGroups_BulkCollectionChanged;

                _gfsPerformanceSourceListView.ModelChanged -= GFSPerformanceSourceListView_ModelChanged;
                _gfsTimelineGroupListView.ModelChanged -= GFSTimelineGroupListView_ModelChanged;

                _masterPerformanceSelectionsView.Dispose();
                _gfsPerformanceSourceListView.Dispose();
                _gfsTimelineGroupListView.Dispose();

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    public sealed class TimelineGroupView : ModelView<GFSModel.TimelineGroup>
    {
        #region Fields
        private Boolean _isSelected;
        private PerformanceSelectionTimeType _timelineFrequency;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsSelectedProperty = WpfHelper.GetPropertyPath<TimelineGroupView>(p => p.IsSelected);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether 
        /// this timeline group is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnPropertyChanged(IsSelectedProperty);
            }
        }

        /// <summary>
        /// Returns the timeline code.
        /// </summary>
        public Int64 Code
        {
            get { return Model.Code; }
        }

        public String Name
        {
            get {return Model.Name;}
        }

        public PerformanceSelectionTimeType TimelineFrequency
        {
            get { return _timelineFrequency; }
        }


        #endregion

        #region Constructor

        public TimelineGroupView(GFSModel.TimelineGroup model)
            : base(model)
        {
            ProcessCode();
        }

        #endregion

        private void ProcessCode()
        {
            String codeString = Code.ToString();

            switch (codeString.Length)
            {
                case 4:
                    _timelineFrequency = PerformanceSelectionTimeType.Year;
                    break;
                case 6:
                    _timelineFrequency = PerformanceSelectionTimeType.Quarter;
                    break;
                case 8:
                    _timelineFrequency = PerformanceSelectionTimeType.Month;
                    break;
                case 10:
                    _timelineFrequency = PerformanceSelectionTimeType.Week;
                    break;
                case 12:
                    _timelineFrequency = PerformanceSelectionTimeType.Day;
                    break;
                default:
                    _timelineFrequency = PerformanceSelectionTimeType.Week;
                    break;
            }

        }

        public override void Dispose()
        {

        }
    }

    public sealed class MetricRowView : Galleria.Framework.ViewModel.ViewModelObject
    {
        #region FieldNames
        PerformanceSelectionMetric _performanceSelectionMetric;
        GFSModel.PerformanceSourceMetric _performanceSourceMetric;
        #endregion

        #region Property Paths
        public static readonly PropertyPath AggregationTypeProperty = WpfHelper.GetPropertyPath<MetricRowView>(p => p.AggregationType);
        public static readonly PropertyPath IsCalculatedMetricProperty = WpfHelper.GetPropertyPath<MetricRowView>(p => p.IsCalculatedMetric);
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="model"></param>
        public MetricRowView(GFSModel.PerformanceSourceMetric metric, PerformanceSelectionMetric selection) 
        {
            _performanceSourceMetric = metric;
            _performanceSelectionMetric = selection;
        }

        #endregion

        #region Properties
        /// <summary>
        /// returns the metric name
        /// </summary>
        public String MetricName
        {
            get { return _performanceSourceMetric.MetricName; }
        }

        /// <summary>
        /// Gets/Sets the aggregation type
        /// </summary>
        public AggregationType AggregationType
        {
            get { return _performanceSelectionMetric.AggregationType; }
            set
            {
                _performanceSelectionMetric.AggregationType = value;
                OnPropertyChanged(AggregationTypeProperty);
            }
        }

        public Boolean IsCalculatedMetric
        {
            get { return _performanceSourceMetric.IsCalculatedMetric; }
        }

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (this._performanceSelectionMetric != null)
                    {

                    }
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }
}
