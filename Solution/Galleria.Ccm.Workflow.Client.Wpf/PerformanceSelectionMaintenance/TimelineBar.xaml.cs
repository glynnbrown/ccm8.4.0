﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Collections;
using System.Globalization;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance
{
    /// <summary>
    /// Visual representation of a collection of timelines
    /// </summary>
    public sealed partial class TimelineBar : UserControl, IDisposable
    {
        #region Events
        /// <summary>
        /// Event handler that is used for indicating the selection has changed
        /// </summary>
        public event EventHandler<TimelineDateSelectionEventArgs> DragSelectionChanged;
        /// <summary>
        /// Event handler that is used for indicating that a selection of dates has been made / requested
        /// to be committed
        /// </summary>
        public event EventHandler<TimelineDateSelectionEventArgs> DatesSelected;

        /// <summary>
        /// Event handler that is used for indicating whether a drag action is a select or de-select
        /// </summary>
        public event EventHandler<TimelineSelectingEventArgs> SetDragType;
        #endregion

        #region Fields

        private TimelineGrouping _currentGrouping = TimelineGrouping.Unknown; //the period represented by the bar
        private List<TimelineGroupView> _timelines; //the timelines held by the bar
        private ObservableCollection<TimelineSlot> _slots = new ObservableCollection<TimelineSlot>(); //the controls visualising timelines

        #endregion

        #region Constructor

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="timelines">The timelines that are to be represented on the bar</param>
        public TimelineBar(List<TimelineGroupView> timelines)
        {
            InitializeComponent();
            _slots.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(Slots_CollectionChanged);

            if (_timelines != null)
            {
                if (_timelines is IDisposable) ((IDisposable)_timelines).Dispose();
            }

            _timelines = timelines;

            // pull the timeline frequency and name
            if (_timelines.Count > 0)
            {
                _currentGrouping = _timelines[0].TimelineFrequency;
                txtYear.Text = _timelines[0].Year.ToString(CultureInfo.InvariantCulture);

                // Setup the grid and then fill it with the timelines
                SetupGrid();
                FillGrid();
            }
            else
            {
                // No timelines, nothing to display, raise an exception
                //App.ViewState.ErrorService.ShowError(ErrorMessageHelper.GetPerformanceSourceHasNoTimelinesMessage(performanceSourceName));
            }
        }

        #endregion

        #region Methods

        #region Highlights

        /// <summary>
        /// Removes all highlights within the timeslotbar
        /// </summary>
        public void RemoveSlotHighlights()
        {
            foreach (TimelineSlot slot in _slots)
            {
                slot.IsHighlighted = false;
            }
        }

        /// <summary>
        /// Add's slot highlights to all the appropriate Timeline Slot's within the timelineslotbar
        /// </summary>
        /// <param name="start">start date of the highlights</param>
        /// <param name="end">end date for the highlights</param>
        public void AddSlotHighlights(Int64 start, Int64 end)
        {
            foreach (TimelineSlot slot in _slots)
            {
                if (slot.TimelineStartDate >= start && slot.TimelineStartDate <= end)
                {
                    slot.IsHighlighted = true;
                }
                else
                {
                    slot.IsHighlighted = false;
                }
            }
        }

        #endregion

        #region Construction of the dynamic timeline slot array / annotations of the slots

        /// <summary>
        /// Creates timeline slots to fill the grid - if a timeline has not been passed for a given
        /// date range, then the slot will be left empty - this is by design and visually shows the start
        /// point and end point of a performance source
        /// </summary>
        private void FillGrid()
        {
            //remove an y existing slots
            if (_slots.Count > 0)
            {
                _slots.RemoveAllItems();
            }

            foreach (var timeline in _timelines)
            {
                int counter = 0;
                TimelineSlot tls = new TimelineSlot(timeline);


                // to understand what column to place the data in we must understand the correlation of the start
                // date to the grouping.
                switch (_currentGrouping)
                {
                    //case TimelineGrouping.Week:
                    //    // divide by 7 to get the number of weeks passed since the date started
                    //    Grid.SetColumn(tls, (int)Math.Floor((double)(timeline.StartDate.DayOfYear / 7)));
                    //    break;
                    //case TimelineGrouping.Period:
                    //    // divide the day count by 28 to get a whole number - that should be slot.
                    //    Grid.SetColumn(tls, (int)Math.Floor((double)(timeline.StartDate.DayOfYear / 28)));
                    //    break;
                    case TimelineGrouping.Month:
                        // have to deduct 1 to get from 1-12 to 0-11 since grid bases at 0
                        Grid.SetColumn(tls, (timeline.Month - 1));
                        break;
                    case TimelineGrouping.Quarter:
                        // have to deduct 1 to get from 1-12 to 0-11 since grid bases at 0
                        Grid.SetColumn(tls, (timeline.Month - 1));
                        // protect against this being at the end of the grid, take the min of 3 slots or
                        // 13 - months so worst case is 13 - 12 = 1, colspan = 1.
                        Grid.SetColumnSpan(tls, Math.Min(3, 13 - timeline.Month));
                        break;
                    case TimelineGrouping.Year:
                        Grid.SetColumn(tls, 0);
                        Grid.SetColumnSpan(tls, 12);
                        break;
                    //case TimelineGrouping.Other:
                    //    // unknown breakdown, we will do 1 column per value for now.
                    //    Grid.SetColumn(tls, counter);
                    //    // increase the column counter
                    //    counter++;
                    //    break;
                }
                Grid.SetRow(tls, 0);
                // add the slot to the grid.
                grdSlots.Children.Add(tls);
                //add the local reference
                _slots.Add(tls);
            }
        }

        /// <summary>
        /// Sets the timelineslot grid up based on the timeline grouping that is being dealt with.
        /// The slot count for the timeline slots and the annotations differ depending on the frequency
        /// </summary>
        private void SetupGrid()
        {
            // organise the grid based on the grouping, which indicates how many grid columns we have.
            int slotCount = 0;
            int annotateCount = 0;
            switch (_currentGrouping)
            {
                case TimelineGrouping.Week:
                    // could be 52 or 53 if we have a long year - so check which is larger, timeline count or 52
                    slotCount = Math.Max(52, _timelines.Count);
                    annotateCount = 12;
                    break;
                //case TimelineGrouping.Period:
                //    slotCount = 13;
                //    annotateCount = 13;
                //    break;
                case TimelineGrouping.Month:
                    slotCount = 12;
                    annotateCount = 12;
                    break;
                case TimelineGrouping.Quarter:
                    slotCount = 12;
                    annotateCount = 12;
                    break;
                case TimelineGrouping.Year:
                    slotCount = 1;
                    annotateCount = 12;
                    break;
                //case TimelineGrouping.Other:
                //    // unknown breakdown, we will do 1 column per value for now.
                //    slotCount = _timelines.Count;
                //    annotateCount = _timelines.Count;
                //    break;
            }

            // create the actual grid columns
            CreateColumns(grdSlots, slotCount + 1);
            CreateColumns(grdAnnotations, annotateCount);

            // fill in the names of the slots
            AnnotateGridBar(annotateCount);

        }

        /// <summary>
        /// set up the bottom textural reference for the timeslot bar
        /// </summary>
        /// <param name="colCount">number of annotate columns in the grid</param>
        private void AnnotateGridBar(int colCount)
        {
            for (int i = 0; i < colCount; i++)
            {
                TextBlock txt = new TextBlock();
                // TODO : Need to use financial sequnce number if they have provided it? needs decision
                switch (_currentGrouping)
                {
                    case TimelineGrouping.Week:
                    case TimelineGrouping.Quarter:
                    case TimelineGrouping.Month:
                    case TimelineGrouping.Year:
                        txt.Text = CultureInfo.CurrentUICulture.DateTimeFormat.AbbreviatedMonthNames[i].ToString();
                        break;
                    //case TimelineGrouping.Period:
                    //    // annotate as Period 1 to 13
                    //    txt.Text = "Period " + (i + 1).ToString(CultureInfo.InvariantCulture);
                    //    break;
                    //case TimelineGrouping.Other:
                    //    // can not annotate it
                    //    // TODO : decide whether there is anything we can do in this situation
                    //    txt.Text = String.Empty;
                    //    break;
                }
                txt.HorizontalAlignment = HorizontalAlignment.Center;
                txt.Foreground = (Brush)App.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ThemeColour_BrushTextHeader];
                Grid.SetColumn(txt, i);
                Grid.SetRow(txt, 1);
                grdAnnotations.Children.Add(txt);
            }
        }

        /// <summary>
        /// Adds a set number of columns to the specified grid - the columns are added with a width unit of 1*
        /// so that it defaults to a uniform grid style layout on the horizontal plane.
        /// </summary>
        /// <param name="grid">grid to have columns added to</param>
        /// <param name="colCount">number of columns to be added</param>
        private static void CreateColumns(Grid grid, int colCount)
        {
            for (int i = 0; i < colCount; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(1, GridUnitType.Star) });
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the slots collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Slots_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (TimelineSlot tls in e.NewItems)
                    {
                        tls.PreviewMouseMove += new MouseEventHandler(TimelineSlot_PreviewMouseMove);
                        tls.DragOver += new DragEventHandler(TimelineSlot_DragOver);
                        tls.Drop += new DragEventHandler(TimelineSlot_Drop);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (TimelineSlot tls in e.OldItems)
                    {
                        tls.PreviewMouseMove -= TimelineSlot_PreviewMouseMove;
                        tls.DragOver -= TimelineSlot_DragOver;
                        tls.Drop -= TimelineSlot_Drop;
                        tls.Dispose();
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    throw new NotSupportedException("Use remove all instead so handlers are properly unsubscribed");
            }
        }

        /// <summary>
        /// Drag over event for when a timeslot is highlighted , raise the event to the parent so it can be
        /// distributed to the other timelineslot bars
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimelineSlot_DragOver(object sender, DragEventArgs e)
        {
            TimelineSlot startSlot = DragDropBehaviour.UnpackData<TimelineSlot>(e.Data);
            TimelineSlot endSlot = (TimelineSlot)sender;

            // check that the item being dragged over is infact a timeline slot
            if (startSlot != null)
            {
                // raise event to parent that item is being hovered over to show highlight
                DragSelectionChanged(this, new TimelineDateSelectionEventArgs(startSlot.TimelineStartDate, endSlot.TimelineStartDate));
            }
        }

        /// <summary>
        /// Drop event which commits a selection of timelines, event is sent to the parent so all 
        /// the timeline bars can update based on the start / end dates
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimelineSlot_Drop(object sender, DragEventArgs e)
        {
            //unpack the timelines
            TimelineSlot startSlot = DragDropBehaviour.UnpackData<TimelineSlot>(e.Data);
            TimelineSlot endSlot = (TimelineSlot)sender;

            // check the dropped object is a timeline slot
            if (startSlot != null)
            {
                // check that we have not drag / dropped on ourself, if we have not then send the event.
                if (startSlot.TimelineStartDate != endSlot.TimelineStartDate)
                {
                    DatesSelected(this, new TimelineDateSelectionEventArgs(startSlot.TimelineStartDate, endSlot.TimelineStartDate));
                }
                else
                {
                    // drop is on same object, consider this the equivalent of a mouse click and 
                    // changed the state of the IsSelected for the current timeline slot
                    startSlot.IsSelected = !startSlot.IsSelected;
                }
            }
        }

        /// <summary>
        /// Mouse preview - set's up the drag behaviour for the timeline slot.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimelineSlot_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && DragDropBehaviour.IsDragging != true)
            {
                DragDropBehaviour drag = new DragDropBehaviour((TimelineSlot)sender);
                drag.DragArea = (FrameworkElement)this.Parent;
                drag.DragAdorner = null;//dont show adorner

                // raise an event so the parent performance source knows whether we are selecting, or 
                // deselecting.
                bool isSelecting = !((TimelineSlot)sender).IsSelected;
                SetDragType(this, new TimelineSelectingEventArgs(isSelecting));


                //start dragging
                drag.BeginDrag();
            }
        }



        #endregion

        #region IDisposable

        private bool _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    _slots.RemoveAllItems();
                    _slots.CollectionChanged -= Slots_CollectionChanged;
                }
                _isDisposed = true;
            }

        }

        #endregion
    }
}
