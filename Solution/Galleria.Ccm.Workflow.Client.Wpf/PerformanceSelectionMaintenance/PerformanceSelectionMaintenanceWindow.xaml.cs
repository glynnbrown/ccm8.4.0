﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Diagnostics.CodeAnalysis;
using Fluent;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance
{
    /// <summary>
    /// Interaction logic for PerformanceSelectionMaintenanceWindow.xaml
    /// </summary>
    public sealed partial class PerformanceSelectionMaintenanceWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(PerformanceSelectionMaintenanceViewModel), typeof(PerformanceSelectionMaintenanceWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public PerformanceSelectionMaintenanceViewModel ViewModel
        {
            get { return (PerformanceSelectionMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PerformanceSelectionMaintenanceWindow senderControl = (PerformanceSelectionMaintenanceWindow)obj;

            if (e.OldValue != null)
            {
                PerformanceSelectionMaintenanceViewModel oldModel = (PerformanceSelectionMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                PerformanceSelectionMaintenanceViewModel newModel = (PerformanceSelectionMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PerformanceSelectionMaintenanceWindow()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PerformanceSelectionMaintenance);

            this.ViewModel = new PerformanceSelectionMaintenanceViewModel();


            this.Loaded += PerformanceSelectionMaintenanceWindow_Loaded;
        }

        private void PerformanceSelectionMaintenanceWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PerformanceSelectionMaintenanceWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close

        /// <summary>
        /// On the application being closed, checked if changes may require saving
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }
            }

        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }

  
}
