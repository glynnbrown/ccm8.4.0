﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance
{
    /// <summary>
    /// Controls the display fo timeline slots for the performance source
    /// </summary>
    public sealed partial class PerformanceSourceBar : UserControl
    {
        #region Fields
        private ObservableCollection<TimelineBar> _bars = new ObservableCollection<TimelineBar>(); //list referencing bar controls
        private Boolean _isSelecting = true; // bool to indicating if the user is selecting timelines
        #endregion

        #region Properties

        #region TimelineGroupsProperty

        public static readonly DependencyProperty TimelineGroupsProperty =
            DependencyProperty.Register("TimelineGroups", typeof(BulkObservableCollection<TimelineGroupView>), typeof(PerformanceSourceBar),
            new PropertyMetadata(null, OnTimelineGroupsChanged));

        private static void OnTimelineGroupsChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PerformanceSourceBar senderControl = (PerformanceSourceBar)obj;

            if (e.OldValue != null)
            {
                var oldValue = (BulkObservableCollection<TimelineGroupView>)e.OldValue;
                oldValue.BulkCollectionChanged -= senderControl.TimelineGroups_BulkCollectionChanged;
               
            }

            if (e.NewValue != null)
            {
                var newValue = (BulkObservableCollection<TimelineGroupView>)e.NewValue;
                newValue.BulkCollectionChanged += senderControl.TimelineGroups_BulkCollectionChanged;
            }

            senderControl.SetupForPerformanceSource();
        }


        public BulkObservableCollection<TimelineGroupView> TimelineGroups
        {
            get { return (BulkObservableCollection<TimelineGroupView>)GetValue(TimelineGroupsProperty); }
            set { SetValue(TimelineGroupsProperty, value); }
        }

        private void TimelineGroups_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
           SetupForPerformanceSource();
        }

        #endregion


        #region BarCount Property

        public static readonly DependencyProperty BarCountProperty =
            DependencyProperty.Register("BarCount", typeof(int), typeof(PerformanceSourceBar));

        /// <summary>
        /// Returns a count of the number of bars displayed in this perf source
        /// </summary>
        public int BarCount
        {
            get { return (int)GetValue(BarCountProperty); }
            private set { SetValue(BarCountProperty, value); }
        }


        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PerformanceSourceBar()
        {
            InitializeComponent();
            _bars.CollectionChanged += new NotifyCollectionChangedEventHandler(Bars_CollectionChanged);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Recreates the timeline bars for the performance source
        /// </summary>
        private void SetupForPerformanceSource()
        {
            //clear out the stack & bars references
            stkYearBars.Children.Clear();
            if (_bars.Count > 0)
            {
                _bars.RemoveAllItems();
            }

            if (this.TimelineGroups != null  && this.TimelineGroups.Count > 0)
            {
                // need to find out what sort of performance source this is, frequency wise.
                // need to know how many years we are dealing with
                Int32 startYear = this.TimelineGroups.Min(t=> t.Year);
                Int32 endYear = this.TimelineGroups.Max(t => t.Year);

                // load in the appropriate number of timelineslot bars
                for (int i = startYear; i <= endYear; i++)
                {
                    // get the list of timelines for this year
                    List<TimelineGroupView> slots = this.TimelineGroups.Where(s => s.Year == i).ToList();

                    // add a timeline bar to represent the year
                    TimelineBar bar = new TimelineBar(slots);
                    _bars.Add(bar);

                    // add it into the stackpanel handling the display of the years
                    stkYearBars.Children.Add(bar);
                }

            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the bars collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Bars_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (TimelineBar bar in e.NewItems)
                    {
                        // hook up the drag events
                        bar.DragSelectionChanged += new EventHandler<TimelineDateSelectionEventArgs>(TimelineBar_DragSelectionChanged);
                        bar.DatesSelected += new EventHandler<TimelineDateSelectionEventArgs>(TimelineBar_DatesSelected);
                        bar.SetDragType += new EventHandler<TimelineSelectingEventArgs>(TimelineBar_SetDragType);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (TimelineBar bar in e.OldItems)
                    {
                        bar.DragSelectionChanged -= TimelineBar_DragSelectionChanged;
                        bar.DatesSelected -= TimelineBar_DatesSelected;
                        bar.SetDragType -= TimelineBar_SetDragType;
                        bar.Dispose();
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    throw new NotSupportedException("Use remove all items instead");

            }

            this.BarCount = _bars.Count;
        }

        /// <summary>
        /// Allows the children to set the dragging selection / deselection
        /// </summary>
        /// <param name="isSelecting"></param>
        private void TimelineBar_SetDragType(object sender, TimelineSelectingEventArgs e)
        {
            _isSelecting = e.IsSelecting;
        }

        /// <summary>
        /// Drop event for the timeline slot drag-drop handler
        /// </summary>
        /// <param name="sender">Timeline Bar Object - as the sender / source</param>
        /// <param name="e">Drag Event Args</param>
        private void stkYearBars_Drop(object sender, DragEventArgs e)
        {
            // if a drop occurs on the stkYearBars then the object was dropped outside
            // of a slot and is considered a cancelling of the drop.
            // Just need to tell the bars to remove all the highlights that have been displayed
            foreach (TimelineBar bar in _bars)
            {
                bar.RemoveSlotHighlights();
            }
        }

        /// <summary>
        /// Event handler for when a bar notifies us that the drag has completed on a timeslot which
        /// indicates there is a defined start date and end date of the selection
        /// A drag selection could of been right-to-left or left-to-right so we have to rearrange
        /// the dates here to make life easier on all the calls.
        /// </summary>
        /// <param name="startDate">start date of the drag</param>
        /// <param name="endDate">end date of the drag</param>
        private void TimelineBar_DatesSelected(object sender, TimelineDateSelectionEventArgs e)
        {
            //TimelineSelectionViewModel.SelectDates(e.StartDate, e.EndDate, this.PerformanceSource);
        }

        /// <summary>
        /// Event handler for the drag selection change - which indicates a start date and end date
        /// of a potential selection, so the appropriate bars need to be highlighted.
        /// This could also un-highlight bars so we inform all of the timelines of the change, and
        /// let them decide.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        private void TimelineBar_DragSelectionChanged(object sender, TimelineDateSelectionEventArgs e)
        {
            // the selection has changed but the drop has not occured, the highlight must include
            // all the values between the two dates.
            foreach (TimelineBar bar in _bars)
            {
                // send in the dates the right way round regardless so each doesn't have to deal with the
                // drag from right to left.
                if (e.StartDate < e.EndDate)
                {
                    bar.AddSlotHighlights(e.StartDate, e.EndDate);
                }
                else
                {
                    bar.AddSlotHighlights(e.EndDate, e.StartDate);
                }
            }
        }
        #endregion
    }
}
