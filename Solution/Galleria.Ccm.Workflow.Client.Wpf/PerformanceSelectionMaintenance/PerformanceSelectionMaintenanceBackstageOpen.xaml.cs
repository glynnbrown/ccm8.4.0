﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance
{
    /// <summary>
    /// Interaction logic for PerformanceSelectionMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class PerformanceSelectionMaintenanceBackstageOpen : UserControl
    {
        #region Fields
        private readonly ObservableCollection<PerformanceSelectionInfo> _searchResults = new ObservableCollection<PerformanceSelectionInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(PerformanceSelectionMaintenanceViewModel), typeof(PerformanceSelectionMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public PerformanceSelectionMaintenanceViewModel ViewModel
        {
            get { return (PerformanceSelectionMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PerformanceSelectionMaintenanceBackstageOpen senderControl = (PerformanceSelectionMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                PerformanceSelectionMaintenanceViewModel oldModel = (PerformanceSelectionMaintenanceViewModel)e.OldValue;
                oldModel.AvailableItems.BulkCollectionChanged -= senderControl.ViewModel_AvailablePerformanceSelectionsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                PerformanceSelectionMaintenanceViewModel newModel = (PerformanceSelectionMaintenanceViewModel)e.NewValue;
                newModel.AvailableItems.BulkCollectionChanged += senderControl.ViewModel_AvailablePerformanceSelectionsBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(PerformanceSelectionMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PerformanceSelectionMaintenanceBackstageOpen senderControl = (PerformanceSelectionMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<PerformanceSelectionInfo>),
            typeof(PerformanceSelectionMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<PerformanceSelectionInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<PerformanceSelectionInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PerformanceSelectionMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<PerformanceSelectionInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                String searchPattern = LocalHelper.GetRexexKeywordPattern(this.SearchText);

                var results =
                    this.ViewModel.AvailableItems
                    .Where(w => Regex.IsMatch(w.Name, searchPattern, RegexOptions.IgnoreCase))
                    .OrderBy(l => l.ToString());

                foreach (var info in results.OrderBy(l => l.ToString()))
                {
                    _searchResults.Add(info);
                }
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Updates the search results when the available locations list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailablePerformanceSelectionsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// Opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                var storeToOpen = senderControl.SelectedItem as PerformanceSelectionInfo;

                if (storeToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                }
            }
        }

        #endregion

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                ListBox senderControl = (ListBox)sender;
                ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
                if (clickedItem != null)
                {
                    var storeToOpen = senderControl.SelectedItem as PerformanceSelectionInfo;

                    if (storeToOpen != null)
                    {
                        //send the store to be opened
                        this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                    }
                }
            }
    }
    }
}
