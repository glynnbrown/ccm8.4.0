﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion

#endregion

using System.Windows;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance
{
    /// <summary>
    /// Interaction logic for PerformanceSelectionMaintenanceHomeTab.xaml
    /// </summary>
    public partial class PerformanceSelectionMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(PerformanceSelectionMaintenanceViewModel), typeof(PerformanceSelectionMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public PerformanceSelectionMaintenanceViewModel ViewModel
        {
            get { return (PerformanceSelectionMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PerformanceSelectionMaintenanceHomeTab senderControl = (PerformanceSelectionMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                PerformanceSelectionMaintenanceViewModel oldModel = (PerformanceSelectionMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                PerformanceSelectionMaintenanceViewModel newModel = (PerformanceSelectionMaintenanceViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PerformanceSelectionMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
