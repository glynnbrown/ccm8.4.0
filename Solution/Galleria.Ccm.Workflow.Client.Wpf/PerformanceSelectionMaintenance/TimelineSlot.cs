﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Copied from ISO v2.
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Diagnostics.CodeAnalysis;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.PerformanceSelectionMaintenance
{
    /// <summary>
    /// Visualisation wrapper for a scenario timeline
    /// </summary>
    public class TimelineSlot : System.Windows.Controls.Button, IDisposable
    {
        #region Properties

        #region Timeline Property
        public static readonly DependencyProperty TimelineProperty =
            DependencyProperty.Register("Timeline", typeof(TimelineGroupView), typeof(TimelineSlot));
        /// <summary>
        /// Gets/Sets the timeline data context
        /// </summary>
        public TimelineGroupView Timeline
        {
            get { return (TimelineGroupView)GetValue(TimelineProperty); }
            private set { SetValue(TimelineProperty, value); }
        }
        #endregion

        #region IsHighlighted property
        public static readonly DependencyProperty IsHighlightedProperty =
            DependencyProperty.Register("IsHighlighted", typeof(bool), typeof(TimelineSlot));
        /// <summary>
        /// Gets/Sets the highlighted status of the slot
        /// </summary>
        public Boolean IsHighlighted
        {
            get { return (Boolean)GetValue(IsHighlightedProperty); }
            set { SetValue(IsHighlightedProperty, value); }
        }
        #endregion

        #region IsSelected property
        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(TimelineSlot),
            new PropertyMetadata(false, IsSelected_PropertyChanged));

        private static void IsSelected_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            TimelineSlot senderControl = (TimelineSlot)obj;
            senderControl.Timeline.IsSelected = (bool)e.NewValue;
        }
        /// <summary>
        /// Gets/Sets the selected status of the slot
        /// </summary>
        public Boolean IsSelected
        {
            get { return (Boolean)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        #endregion

        /// <summary>
        /// The timeline slot start date time (read only)
        /// </summary>
        public Int64 TimelineStartDate
        {
            get { return this.Timeline.Code; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// static constructor - style key override
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static TimelineSlot()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(TimelineSlot), new FrameworkPropertyMetadata(typeof(TimelineSlot)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="timelineContext"></param>
        public TimelineSlot(TimelineGroupView timelineContext)
        {
            this.Timeline = timelineContext;
            this.IsSelected = timelineContext.IsSelected;

            ////set the slot tooltip
            //ScreenTip tip = new ScreenTip();
            //tip.Text = String.Format(CultureInfo.CurrentUICulture, Message.TimelineSelection_GridTooltip,
            //    this.Timeline.Parent.Name, this.Timeline.Parent.TimelineFrequencyName,
            //    this.Timeline.StartDate.ToLocalTime().ToShortDateString());
            //this.ToolTip = tip;

            timelineContext.PropertyChanged += new PropertyChangedEventHandler(TimelineContext_PropertyChanged);
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Synchs the source is selected with this
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimelineContext_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TimelineGroupView.IsSelectedProperty.Path)
            {
                this.IsSelected = this.Timeline.IsSelected;
            }
        }

        #endregion

        #region IDisposable Members

        private bool _isDisposed;

        /// <summary>
        /// Disposes of this object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    this.Timeline.PropertyChanged -= TimelineContext_PropertyChanged;
                }

                _isDisposed = true;
            }

        }
        #endregion
    }
}
