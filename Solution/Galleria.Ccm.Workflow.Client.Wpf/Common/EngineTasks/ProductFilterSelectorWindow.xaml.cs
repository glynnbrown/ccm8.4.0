﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-27426 : L.Ineson
//	Created
#endregion

#region Version History : CCM830
// V8-31559 : A.Kuszyk
//  Amended to allow single filter selection.
// V8-31798 : A.Kuszyk
//  Fixed issues with selector opening, loading and closing.
// V8-31824 : A.Kuszyk
//  Added support for colours.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Engine;
using System.ComponentModel;
using Galleria.Framework.Helpers;
using System.Collections.ObjectModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    /// <summary>
    /// Interaction logic for ProductFilterSelectorWindow.xaml
    /// </summary>
    public partial class ProductFilterSelectorWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductFilterSelectorViewModel), typeof(ProductFilterSelectorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductFilterSelectorWindow senderControl = (ProductFilterSelectorWindow)obj;

            if (e.OldValue != null)
            {
                ProductFilterSelectorViewModel oldModel = (ProductFilterSelectorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ProductFilterSelectorViewModel newModel = (ProductFilterSelectorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        /// Returns the viewmodel controller for this screen
        /// </summary>
        public ProductFilterSelectorViewModel ViewModel
        {
            get { return (ProductFilterSelectorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public ProductFilterSelectorWindow(ProductFilterSelectorViewModel viewmodel)
        {
            InitializeComponent();

            this.ViewModel = viewmodel;
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }

    public sealed class ProductFilterSelectorViewModel : ViewModelAttachedControlObject<ProductFilterSelectorWindow>
    {
        #region Fields

        private Boolean? _dialogResult;
        private ObservableCollection<ProductFilterValue> _filters = new ObservableCollection<ProductFilterValue>();
        private readonly ReadOnlyCollection<ObjectFieldInfo> _availableFields;
        #endregion

        #region Binding Property Paths

        //Properties
        public static PropertyPath FiltersProperty = WpfHelper.GetPropertyPath<ProductFilterSelectorViewModel>(p => p.Filters);
        public static PropertyPath AllowMultipleProperty = WpfHelper.GetPropertyPath<ProductFilterSelectorViewModel>(p => p.AllowMultiple);

        //Commands
        public static PropertyPath SelectFieldCommandProperty = WpfHelper.GetPropertyPath<ProductFilterSelectorViewModel>(p => p.SelectFieldCommand);
        public static PropertyPath AddFilterCommandProperty = WpfHelper.GetPropertyPath<ProductFilterSelectorViewModel>(p => p.AddFilterCommand);
        public static PropertyPath RemoveFilterCommandProperty = WpfHelper.GetPropertyPath<ProductFilterSelectorViewModel>(p => p.RemoveFilterCommand);
        public static PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<ProductFilterSelectorViewModel>(p => p.OKCommand);
        public static PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ProductFilterSelectorViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the dialog result 
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the collection of set up filters
        /// </summary>
        public ObservableCollection<ProductFilterValue> Filters
        {
            get { return _filters; }
        }

        /// <summary>
        /// Returns the list of fields that may be selected
        /// </summary>
        public ReadOnlyCollection<ObjectFieldInfo> AvailableFields
        {
            get { return _availableFields; }
        }

        /// <summary>
        /// Indicates if multiple filters are allowed or not.
        /// </summary>
        public Boolean AllowMultiple { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ProductFilterSelectorViewModel(
            IList<ObjectFieldInfo> availableFields, 
            Boolean allowMultiple = true, 
            IEnumerable<ProductFilterValue> existingFilters = null)
        {
            _availableFields = new ReadOnlyCollection<ObjectFieldInfo>(availableFields);
            AllowMultiple = allowMultiple;

            if (existingFilters != null && existingFilters.Any())
            {
                foreach (ProductFilterValue existingFilter in existingFilters)
                {
                    Filters.Add(existingFilter);
                }
            }
            Initialize();
        }

        public ProductFilterSelectorViewModel(
            IList<ObjectFieldInfo> availableFields,
            Boolean allowMultiple,
            IEnumerable<ITaskParameterValue> existingFilters)
        {
            _availableFields = new ReadOnlyCollection<ObjectFieldInfo>(availableFields);
            AllowMultiple = allowMultiple;

            if (existingFilters != null && existingFilters.Any())
            {
                foreach (ITaskParameterValue val in existingFilters)
                {
                    // Get the field from the first value.
                    ObjectFieldInfo field = ObjectFieldInfo.ExtractFieldsFromText(val.Value1 as String, availableFields).FirstOrDefault();
                    if (field == null) continue;

                    Object filterValue;
                    TaskParameterFilterOperandType operandType;
                    try
                    {
                        // Get the operand from the second value.
                        Int32 operandTypeAsInt = (Int32)val.Value2;
                        if (!Enum.TryParse<TaskParameterFilterOperandType>(operandTypeAsInt.ToString(), out operandType)) continue;

                        // Get the filter criteria from the third value.
                        filterValue = val.Value3;
                        if (field.PropertyType.IsEnum)
                        {
                            Int32 filterValueAsInt = (Int32)filterValue;
                            filterValue = new EnumConverter(field.PropertyType).ConvertFrom(filterValueAsInt.ToString()) ?? val.Value3;
                        }
                    }
                    catch (InvalidCastException)
                    {
                        continue;
                    }

                    Filters.Add(new ProductFilterValue()
                    {
                        Field = field,
                        OperandType = operandType,
                        Value = filterValue
                    });
                }
            }
            Initialize();
        }

        private void Initialize()
        {
            if (!Filters.Any())
            {
                this.Filters.Add(new ProductFilterValue());
            }
        }


        #endregion

        #region Commands

        #region SelectFieldCommand

        private RelayCommand _selectFieldCommand;

        public RelayCommand SelectFieldCommand
        {
            get
            {
                if (_selectFieldCommand == null)
                {
                    _selectFieldCommand = new RelayCommand(
                        p => SelectFieldCommand_Executed(p),
                        p => SelectFieldCommand_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Ellipsis,

                    };
                    base.ViewModelCommands.Add(_selectFieldCommand);
                }
                return _selectFieldCommand;
            }
        }

        private Boolean SelectFieldCommand_CanExecute(Object args)
        {
            if (args == null) return false;

            return true;
        }

        private void SelectFieldCommand_Executed(Object args)
        {
            ProductFilterValue row = args as ProductFilterValue;

            //create the columnset
            DataGridColumnCollection columnSet = new DataGridColumnCollection();
            columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Message.ProductFilterSelector_SelectField_GroupName, "GroupName", HorizontalAlignment.Left));
            columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Message.ProductFilterSelector_SelectField_Field, "PropertyFriendlyName", HorizontalAlignment.Left));
            columnSet[0].Width = DataGridLength.SizeToCells;

            //Show sequence field selection window
            GenericSingleItemSelectorViewModel viewModel = new GenericSingleItemSelectorViewModel(Message.ProductFilterSelector_SelectField_Title)
            {
                ItemSource = _availableFields,
                SelectionMode = DataGridSelectionMode.Single,
                ColumnSet = columnSet,
            };

            CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(viewModel);

            if (viewModel.DialogResult == true)
            {
                //Cast value
                ObjectFieldInfo selectedField = viewModel.SelectedItems.Cast<ObjectFieldInfo>().FirstOrDefault();

                if (selectedField != null)
                {
                    Int32 rowIndex = this.Filters.IndexOf(row);

                    ProductFilterValue newRow = new ProductFilterValue() { Field = selectedField };

                    if(rowIndex < Filters.Count - 1 && rowIndex >= 0)
                    {
                        Filters.Insert(rowIndex, newRow);
                    }
                    else
                    {
                        Filters.Add(newRow);
                    }
                    
                    this.Filters.Remove(row);
                }
            }
        }

        #endregion

        #region AddFilterCommand

        private RelayCommand _addFilterCommand;

        /// <summary>
        /// Adds a new field to the selected blocking group.
        /// </summary>
        public RelayCommand AddFilterCommand
        {
            get
            {
                if (_addFilterCommand == null)
                {
                    _addFilterCommand = new RelayCommand(
                        p => AddFilterCommand_Executed())
                    {
                        //FriendlyName = Message.BlockingMaintenance_AddFilter,
                        SmallIcon = ImageResources.Add_16,
                    };
                    base.ViewModelCommands.Add(_addFilterCommand);
                }
                return _addFilterCommand;
            }
        }

        private void AddFilterCommand_Executed()
        {
            ProductFilterValue newRow = new ProductFilterValue();

            this.SelectFieldCommand.Execute(newRow);

            if (newRow.Field != null)
            {
                this.Filters.Add(newRow);
            }
        }

        #endregion

        #region RemoveFilterCommand

        private RelayCommand _removeFilterCommand;

        /// <summary>
        /// Removes a new field to the selected blocking group.
        /// </summary>
        public RelayCommand RemoveFilterCommand
        {
            get
            {
                if (_removeFilterCommand == null)
                {
                    _removeFilterCommand = new RelayCommand(
                        p => RemoveFilterCommand_Executed(p))
                    {
                        //FriendlyName = Message.BlockingMaintenance_RemoveFilter,
                        SmallIcon = ImageResources.Delete_16,
                    };
                    base.ViewModelCommands.Remove(_removeFilterCommand);
                }
                return _removeFilterCommand;
            }
        }

        private void RemoveFilterCommand_Executed(Object args)
        {
            ProductFilterValue row = args as ProductFilterValue;
            if (row == null) return;

            this.Filters.Remove(row);
        }

        #endregion

        #region OK

        private RelayCommand _okCommand;

        /// <summary>
        /// Commits the current formula.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        InputGestureKey = Key.Enter
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            if (!Filters.Any() || Filters.Any(f => f.Field == null))
            {
                OKCommand.DisabledReason = Message.ProductFilterSelectorWindow_OkDisabledReason; 
                return false;
            }
            return true;
        }

        private void OK_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the change and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    public sealed class ProductFilterValue : INotifyPropertyChanged
    {
        #region Fields
        private ObjectFieldInfo _field;
        private TaskParameterFilterOperandType _operandType;
        private Object _value;
        private Dictionary<TaskParameterFilterOperandType, String> _availableOperandTypes;
        #endregion

        #region Binding Property Path

        public static PropertyPath FieldProperty = WpfHelper.GetPropertyPath<ProductFilterValue>(p => p.Field);
        public static PropertyPath FieldDisplayNameProperty = WpfHelper.GetPropertyPath<ProductFilterValue>(p => p.FieldDisplayName);
        public static PropertyPath OperandTypeProperty = WpfHelper.GetPropertyPath<ProductFilterValue>(p => p.OperandType);
        public static PropertyPath ValueProperty = WpfHelper.GetPropertyPath<ProductFilterValue>(p => p.Value);
        public static PropertyPath AvailableOperandsProperty = WpfHelper.GetPropertyPath<ProductFilterValue>(p => p.AvailableOperands);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the field to be filtered by
        /// </summary>
        public ObjectFieldInfo Field
        {
            get { return _field; }
            set
            {
                _field = value;

                OnPropertyChanged(FieldProperty);
                OnPropertyChanged(FieldDisplayNameProperty);
                UpdateAvailableOperands();
            }
        }

        /// <summary>
        /// Returns the friendly name to display
        /// for this field.
        /// </summary>
        public String FieldDisplayName
        {
            get
            {
                return (Field != null) ?
                    Field.PropertyFriendlyName : String.Empty;
            }
        }

        /// <summary>
        /// Gets/Sets the operand type to use when 
        /// processing the value
        /// </summary>
        public TaskParameterFilterOperandType OperandType
        {
            get { return _operandType; }
            set
            {
                _operandType = value;
                OnPropertyChanged(OperandTypeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the value to use when filtering.
        /// </summary>
        public Object Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged(ValueProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available operands
        /// </summary>
        public Dictionary<TaskParameterFilterOperandType, String> AvailableOperands
        {
            get
            {
                return _availableOperandTypes;
            }
            private set
            {
                _availableOperandTypes = value;
                OnPropertyChanged(AvailableOperandsProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ProductFilterValue()
        { }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a task parameter value from the values
        /// of this filter.
        /// </summary>
        /// <returns></returns>
        public ITaskParameterValue CreateTaskParameterValue()
        {
            return new TaskParameterValue(this.Field.FieldPlaceholder, this.OperandType, this.Value);
        }

        /// <summary>
        /// Updates the available operands collection.
        /// </summary>
        public void UpdateAvailableOperands()
        {
            var availableOperands = new Dictionary<TaskParameterFilterOperandType, String>();

            foreach (TaskParameterFilterOperandType operand in GetAvailableTypes(this.Field))
            {
                availableOperands.Add(operand, TaskParameterFilterOperandTypeHelper.FriendlyNames[operand]);
            }

            this.AvailableOperands = availableOperands;

            //update the model selected operand type if it is no longer valid.
            if (!availableOperands.Keys.Contains(this.OperandType))
            {
                this.OperandType = TaskParameterFilterOperandType.Equals;
            }
        }

        private static TaskParameterFilterOperandType[] GetAvailableTypes(ObjectFieldInfo field)
        {
            if (field == null)
            {
                return Enum.GetValues(typeof(TaskParameterFilterOperandType))
                    .Cast<TaskParameterFilterOperandType>().ToArray();
            }


            var basicPropertyType = CommonHelper.GetBasicPropertyType(field.PropertyType);

            switch (basicPropertyType)
            {
                default:
                
                case CommonHelper.PropertyType.String:
                    return new TaskParameterFilterOperandType[]
                    {
                        TaskParameterFilterOperandType.Equals,
                         TaskParameterFilterOperandType.Contains,
                         TaskParameterFilterOperandType.NotEquals,
                         TaskParameterFilterOperandType.NotContains,
                    };

                case CommonHelper.PropertyType.Boolean:
                    return new TaskParameterFilterOperandType[]
                    {
                        TaskParameterFilterOperandType.Equals,
                         TaskParameterFilterOperandType.NotEquals,
                    };

                case CommonHelper.PropertyType.Decimal:
                case CommonHelper.PropertyType.Integer:
                    if (field.PropertyName.Equals(PlanogramProduct.FillColourProperty.Name))
                    {
                        return new TaskParameterFilterOperandType[]
                        {
                            TaskParameterFilterOperandType.Equals,
                            TaskParameterFilterOperandType.NotEquals,
                        };
                    }
                    else
                    {
                        return new TaskParameterFilterOperandType[]
                        {
                            TaskParameterFilterOperandType.Equals,
                            TaskParameterFilterOperandType.NotEquals,
                            TaskParameterFilterOperandType.GreaterThan,
                            TaskParameterFilterOperandType.GreaterThanOrEqualTo,
                            TaskParameterFilterOperandType.LessThan,
                            TaskParameterFilterOperandType.LessThanOrEqualTo
                        };
                    }

                case CommonHelper.PropertyType.DateTime:
                    return new TaskParameterFilterOperandType[]
                    {
                        TaskParameterFilterOperandType.Equals,
                         TaskParameterFilterOperandType.NotEquals,
                         TaskParameterFilterOperandType.GreaterThan,
                         TaskParameterFilterOperandType.GreaterThanOrEqualTo,
                         TaskParameterFilterOperandType.LessThan,
                         TaskParameterFilterOperandType.LessThanOrEqualTo
                    };

                case CommonHelper.PropertyType.Enum:
                    return new TaskParameterFilterOperandType[]
                    {
                        TaskParameterFilterOperandType.Equals,
                         TaskParameterFilterOperandType.NotEquals,
                    };
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }

    public sealed class ProductFilterTemplateSelector : DataTemplateSelector
    {
        public DataTemplate FlagTemplate { get; set; }
        public DataTemplate DateTemplate { get; set; }
        public DataTemplate NumericTemplate { get; set; }
        public DataTemplate TextTemplate { get; set; }
        public DataTemplate EnumTemplate { get; set; }
        public DataTemplate ColourTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            ProductFilterValue value = item as ProductFilterValue;
            if (value != null && value.Field != null)
            {
                CommonHelper.PropertyType propertyType =  CommonHelper.GetBasicPropertyType(value.Field.PropertyType);

                switch (propertyType)
                {
                    case CommonHelper.PropertyType.String:
                        return this.TextTemplate;

                    case CommonHelper.PropertyType.DateTime:
                        return this.DateTemplate;

                    case CommonHelper.PropertyType.Boolean:
                        return this.FlagTemplate;

                    case CommonHelper.PropertyType.Integer:
                    case CommonHelper.PropertyType.Decimal:
                        if (value.Field.PropertyName.Equals(PlanogramProduct.FillColourProperty.Name))
                        {
                            return ColourTemplate;
                        }
                        else
                        {
                            return this.NumericTemplate;
                        }

                    case CommonHelper.PropertyType.Enum:
                        return this.EnumTemplate;
                }
            }

            return this.TextTemplate;
        }
    }

}
