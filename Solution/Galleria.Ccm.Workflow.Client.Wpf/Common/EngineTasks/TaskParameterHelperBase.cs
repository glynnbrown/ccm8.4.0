﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 820)
// V8-30774 : A.Kuszyk
//  Created.
// V8-30907 : A.Kuszyk
//  Updated CreateNewParameterColumn to use read only flag.
// V8-3177 : L.Ineson
//  Made sure that sort member path gets set when generating the parameter column.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
// V8-32133 : A.Probyn
//  Added new Virtual AlternateDisplayValue property
// V8-32357 : M.Brumby
//  [PCR01561] Ability Customise the names of Workflow Tasks
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using System.ComponentModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public abstract class TaskParameterHelperBase : ITaskParameterHelper, INotifyPropertyChanged
    {
        #region Fields
        
        /// <summary>
        /// The parent workflow to which <see cref="WorkflowTaskParameter"/> belongs.
        /// </summary>
        private Galleria.Ccm.Model.Workflow _workflow = null;

        /// <summary>
        /// A reference to the <see cref="WorkflowTaskParameter"/>, which contains the values set in the Workflow maintenance screen.
        /// </summary>
        protected WorkflowTaskParameter WorkflowTaskParameter;

        /// <summary>
        /// A reference to the <see cref="WorkpackagePlanogramParameter"/>, which contains the values set in the Workpackage Wizard, if
        /// the user is on that screen.
        /// </summary>
        protected WorkpackagePlanogramParameter WorkpackagePlanogramParameter;

        /// <summary>
        /// The Ids of the source planograms in the <see cref="Workpackage"/> referred to by <see cref="WorkpackagePlanogramParameter"/>.
        /// </summary>
        private List<Int32> _workpackageSourcePlanogramIds = null;

        
        #endregion

        #region Properties
        /// <summary>
        /// Returns true if this is a multi value parameter type. Defaults to false.
        /// </summary>
        public virtual Boolean IsMultiValue
        {
            get { return false; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup. Defaults to false.
        /// </summary>
        public virtual Boolean IsContentLink
        {
            get { return false; }
        }

        /// <summary>
        /// Returns true if the parameter is allowed to be null. Defaults to false.
        /// </summary>
        public virtual Boolean IsNullAllowed
        {
            get { return false; }
        } 

        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected abstract String DataTemplateName { get; }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected abstract String DataTemplateColumnName { get; }

        /// <summary>
        /// The parent workflow to which <see cref="WorkflowTaskParameter"/> belongs.
        /// </summary>
        protected Galleria.Ccm.Model.Workflow ParentWorkflow
        {
            get
            {
                if (_workflow == null)
                {
                    if (this.WorkflowTaskParameter == null ||
                        this.WorkflowTaskParameter.Parent == null ||
                        this.WorkflowTaskParameter.Parent.Parent == null)
                    {
                        _workflow = Galleria.Ccm.Model.Workflow.NewWorkflow(App.ViewState.EntityId);
                    }
                    else
                    {
                        _workflow = this.WorkflowTaskParameter.Parent.Parent;
                    }
                }
                return _workflow;
            }
        }

        public virtual Object AlternateDisplayValue
        {
            get { return null; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the Ids of the source planograms in the <see cref="Workpackage"/> referred to 
        /// by <see cref="WorkpackagePlanogramParameter"/>.
        /// </summary>
        protected IEnumerable<Int32> GetWorkpackageSourcePlanogramIds()
        {
            if (_workpackageSourcePlanogramIds == null)
            {
                if (this.WorkpackagePlanogramParameter == null ||
                    WorkpackagePlanogramParameter.Parent == null ||
                    WorkpackagePlanogramParameter.Parent.Parent == null)
                {
                    _workpackageSourcePlanogramIds = new List<Int32>();
                }
                else
                {
                    _workpackageSourcePlanogramIds = this.WorkpackagePlanogramParameter.Parent.Parent.Planograms
                        .Select(p => p.SourcePlanogramId)
                        .Where(id => id.HasValue)
                        .Select(id => id.Value)
                        .Distinct()
                        .ToList();
                }
            }
            return _workpackageSourcePlanogramIds;
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="isNullAllowed"></param>
        /// <returns></returns>
        public String Validate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            // Check this task helper for errors (value errors that the helper knows about).
            String helperError = OnValidate(values, isNullAllowed);

            // If we don't have a reference to the workflow task parameter, we can't perform any
            // other checks, just return the error we've got.
            if (WorkflowTaskParameter == null) return helperError;

            // Check for errors with the workflow task.
            String taskError;
            if (WorkpackagePlanogramParameter != null)
            {
                taskError = WorkpackagePlanogramParameter.IsValid(WorkflowTaskParameter);
            }
            else
            {
                taskError = WorkflowTaskParameter.IsValid();
            }

            // Return both error values in a friendly combination.
            if(String.IsNullOrEmpty(helperError) && String.IsNullOrEmpty(taskError)) return null;
            if(String.IsNullOrEmpty(helperError)) return taskError;
            if(String.IsNullOrEmpty(taskError)) return helperError;
            return String.Format("{0}; {1}", helperError, taskError);
        }

        /// <summary>
        /// Called on derived classes when <see cref="Validate"/> is called on this instance.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="isNullAllowed"></param>
        /// <returns></returns>
        protected abstract String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed);

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        /// <param name="currentValues"></param>
        /// <returns></returns>
        public abstract List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode);

        /// <summary>
        /// Creates a new parameter column for the parameter type.
        /// </summary>
        /// <param name="header"></param>
        /// <param name="headerGroupName"></param>
        /// <param name="parameterPath"></param>
        /// <param name="isReadOnly"></param>
        /// <param name="parameterDetails"></param>
        /// <returns></returns>
        public virtual DataGridColumn CreateNewParameterColumn(
            String header, String headerGroupName, String parameterPath, Boolean isReadOnly, EngineTaskParameterInfo parameterDetails, Object headerGroupToolTip)
        {
            DataGridBoundTemplateColumn col = new DataGridBoundTemplateColumn();
            col.Binding = new Binding(parameterPath) { Mode = BindingMode.OneWay };
            if(!this.IsMultiValue) col.SortMemberPath = parameterPath + ".Value";

            col.Header = header;
            col.HeaderGroupNames.Add(new DataGridHeaderGroup() { HeaderName = headerGroupName, HeaderToolTip = headerGroupToolTip });
            col.IsReadOnly = isReadOnly;

            col.CellTemplate = Application.Current.Resources[DataTemplateColumnName] as DataTemplate;
            ExtendedDataGrid.SetColumnCellAlignment(col, System.Windows.HorizontalAlignment.Stretch);

            return col;
        }

        /// <summary>
        /// Returns the workflow maintenance data template for this parameter type
        /// </summary>
        /// <returns></returns>
        public virtual DataTemplate GetDataTemplate()
        {
            return Application.Current.Resources[DataTemplateName] as DataTemplate;
        }

        /// <summary>
        /// Does any task specific work i.e. useful for tasks with parameters that are dependant on other parameters. 
        /// </summary>
        public virtual void Initialize(WorkflowTaskParameter workflowTaskParameter, WorkpackagePlanogramParameter workpackagePlanogramParameter)
        {
            WorkflowTaskParameter = workflowTaskParameter;
            WorkpackagePlanogramParameter = workpackagePlanogramParameter;
        }

        /// <summary>
        /// Performs any actions required when the value of the parameter changes
        /// (for example - clears child prameter values etc)
        /// </summary>
        public virtual void OnValueChanged()
        {
            // Do nothing
        }



        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Raises the PropertyChanged event
        /// </summary>
        /// <param name="propertyName"></param>
        protected void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
