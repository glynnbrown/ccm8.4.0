﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM811
// V8-29633 : J.Pickup
//  Created.
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM 820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class ClusterNameTaskParameterHelper : TaskParameterHelperBase
    {
        #region Fields 
        private List<String> _clusterNames;
        #endregion

        #region Properties
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ClusterNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_ClusterNameColumn"; }
        }

        /// <summary>
        /// The names of the clusters available to select based on the previously defined cluster scheme (Seperate parameter).
        /// </summary>
        private List<String> ClusterNames
        {
            get
            {
                return _clusterNames;
            }
            set
            {
                _clusterNames = value;
            }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Returns true if the parameter is allowed to be null
        /// </summary>
        public override bool IsNullAllowed
        {
            get { return true; }
        } 
        #endregion

        #region Methods
        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            String error = null;
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }
            else
            {
                if (!DoesClusterExistOnClusterScheme(Convert.ToString(val)) && val != String.Empty)
                {
                    error = Message.ClusterNameTaskParameterHelper_Validate_ClusterDoesntExistOnClusterScheme;
                }
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            this.UpdateClusterNames();

            var win = new GenericSingleItemSelectorWindow();
            win.ItemSource = ClusterNames;
            win.SelectionMode = DataGridSelectionMode.Single;
            WindowHelper.ShowWindow(win, parentWindow, true);

            if (win.DialogResult == true)
            {
                if (win.SelectedItems != null)
                {
                    return new List<ITaskParameterValue> 
                    { 
                       new  TaskParameterValue(win.SelectedItems.Cast<String>().First(), null, null)
                    };
                }
            }

            return currentValues.ToList();
        }

        /// <summary>
        /// Does work to Initialize the current object.
        /// </summary>
        public override void Initialize(WorkflowTaskParameter workflowTaskParameter, WorkpackagePlanogramParameter workpackagePlanogramParameter)
        {
            base.Initialize(workflowTaskParameter, workpackagePlanogramParameter);
            UpdateClusterNames();
        }

        /// <summary>
        /// Load the list of cluster names from the selected cluster scheme parameter from which this paramerter is dependant.
        /// </summary>
        /// <returns>Returns the selected cluster scheme</returns>
        private ClusterScheme GetSelectedClusterScheme()
        {
            WorkflowTaskParameter clusterSchemeNameTaskParameter = WorkflowTaskParameter.Parent.Parameters.FirstOrDefault(p => p.ParameterDetails.ParameterType == TaskParameterType.ClusterSchemeNameSingle);

            String clusterSchemeName = String.Empty;

            //Workflow task-specific code
            if (clusterSchemeNameTaskParameter != null)
            {
                clusterSchemeName = Convert.ToString(clusterSchemeNameTaskParameter.Value);
            }

            //Workpackage wizard-specific code
            if (WorkpackagePlanogramParameter != null)
            {
                if (WorkpackagePlanogramParameter.Parent != null)
                {
                    WorkpackagePlanogramParameter workpackagePlanogramParameter = WorkpackagePlanogramParameter.Parent.Parameters.FirstOrDefault(p => p.ParameterDetails.ParameterType == TaskParameterType.ClusterSchemeNameSingle);

                    if (workpackagePlanogramParameter != null)
                    {
                        clusterSchemeName = Convert.ToString(workpackagePlanogramParameter.Value);
                    }
                }
            }

            if (!String.IsNullOrEmpty(clusterSchemeName))
            {
                ClusterSchemeInfo clusterSchemeInfo = base.ParentWorkflow.TaskParameterDataCache.GetClusterSchemeInfos().FirstOrDefault(cs => cs.Name == clusterSchemeName);

                if (clusterSchemeInfo != null)
                {
                    Int32? clusterSchemeId = clusterSchemeInfo.Id;

                    if (clusterSchemeId != null)
                    {
                        return ClusterScheme.FetchById(clusterSchemeId.Value);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Updates the avilable cluster names from the selected cluster scheme.
        /// </summary>
        private void UpdateClusterNames()
        {
            ClusterScheme clusterScheme = GetSelectedClusterScheme();

            if (clusterScheme != null)
            {
                ClusterNames = clusterScheme.Clusters.Select(c => c.Name).ToList();
            }
        }

        /// <summary>
        /// Checks whether the supplied cluster exists on the selected cluster scheme.
        /// </summary>
        /// <param name="clusterName"></param>
        /// <returns></returns>
        private Boolean DoesClusterExistOnClusterScheme(String clusterName)
        {
            ClusterScheme clusterScheme = GetSelectedClusterScheme();

            if (clusterScheme != null)
            {
                return (clusterScheme.Clusters.FirstOrDefault(c => c.Name == clusterName) != null);
            }

            return false;
        } 
        #endregion
    }
}