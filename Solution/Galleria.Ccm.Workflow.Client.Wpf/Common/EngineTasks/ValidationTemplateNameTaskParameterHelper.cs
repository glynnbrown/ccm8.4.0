﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27269 : A.Kuszyk
//  Created.
// V8-27426 : L.Ineson
//  Changed to use ITaskParameterValue

#endregion

#region Version History : CCM 810

// V8-29746 : A.Probyn
//  Updated IsContentLink to be true

#endregion

#region Version History : (CCM8.1.1)

// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods

#endregion

#region Version History: CCM820

// V8-30255 : A.Silva
//  Changed IsNullAllowed to true, as it can be left blank by the user.
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion

#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Windows.Controls;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class ValidationTemplateNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override Boolean IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName { get { return "TaskTemplates_ValidationTemplateNameColumn"; } }

        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName { get { return "TaskTemplates_ValidationTemplateNameTemplate"; } }

        /// <summary>
        ///     Returns <c>true</c> if the parameter is allowed to be null.
        /// </summary>
        public override Boolean IsNullAllowed
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            ITaskParameterValue value = values.FirstOrDefault();
            Object val = value != null ? value.Value1 : null;
            String error = null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }
            else
            {
                ValidationTemplateInfoList infos = base.ParentWorkflow.TaskParameterDataCache.GetValidationTemplateInfos();

                if (infos.All(info => info.Name != (String)val))
                {
                    error = Message.EngineTasks_Blocking_InvalidError;
                }
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            var win = new GenericSingleItemSelectorWindow();
            win.ItemSource = base.ParentWorkflow.TaskParameterDataCache.GetValidationTemplateInfos();
            win.SelectionMode = DataGridSelectionMode.Single;
            WindowHelper.ShowWindow(win, parentWindow, true);

            if (win.DialogResult == true)
            {
                if (win.SelectedItems != null)
                {
                    return new List<ITaskParameterValue> 
                    { 
                        new TaskParameterValue(win.SelectedItems.Cast<ValidationTemplateInfo>().First().Name)
                    };
                }
            }

            return currentValues.ToList();
        }
    }
}
