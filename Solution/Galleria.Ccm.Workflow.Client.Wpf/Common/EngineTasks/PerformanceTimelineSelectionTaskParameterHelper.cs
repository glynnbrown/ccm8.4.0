﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM830
// V8-31830 : A.Probyn
//  Created
// V8-32086 : N.Haywood
//  Added category code
// V8-32166 : A.Probyn
//  Added extra defensive code.
//  Fixed code in SelectValues so that the parent performance selection parameter can still be found when there are
//  other tasks with the parameters of the same paramter Id.
#endregion
#region Version History : CCM832
// CCM-18917 : G.Richards
//  Workflow parameter selection takes a long time to complete. It will now only select the performance selection criteria once its changed rather than every time.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.UpdateProductPerformance.v1;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class PerformanceTimelineSelectionTaskParameterHelper : TaskParameterHelperBase
    {
        #region Fields

        private Object _alternateDisplayValue = null;

        static WorkpackagePlanogramParameter _workpackagePlanogramParameterCache;
        static IEnumerable<WorkpackagePlanogramParameterValue> _workpackagePlanogramParameterValues;

        #endregion

        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_PerformanceTimelineSelectionTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_PerformanceTimelineSelectionColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            // V8-28585:
            // No product codes is a valid selection.
            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            // Fetch the entity details
            Entity entity = base.ParentWorkflow.TaskParameterDataCache.GetEntity();

            if (entity.GFSId.HasValue && !(String.IsNullOrEmpty(entity.SystemSettings.FoundationServicesEndpoint)))
            {
                try
                {
                    PerformanceSelectionInfoList workflowSelectedPerformanceSelections = base.ParentWorkflow.TaskParameterDataCache.GetPerformanceSelectionInfos();

                    Object relatedParameterValue = null;
                    //If we are at workpackage planogram selection parameter
                    if (base.WorkpackagePlanogramParameter != null)
                    {
                        //Find other performance selection parameter from workflow
                        Int32 myParentId = WorkpackagePlanogramParameter.ParameterDetails.ParentId.Value;
                        WorkpackagePlanogramParameter relatedParameter = base.WorkpackagePlanogramParameter.Parent.Parameters.Where(p => p.TaskDetails.TaskType == base.WorkpackagePlanogramParameter.TaskDetails.TaskType && p.ParameterDetails.Id == myParentId).FirstOrDefault();
                        if (relatedParameter != null)
                        {
                            relatedParameterValue = relatedParameter.Value;
                        }
                    }
                    else
                    {
                        //Fallback on workflow task parameter
                        Int32 myParentId = WorkflowTaskParameter.ParameterDetails.ParentId.Value;
                        WorkflowTaskParameter relatedParameter = base.WorkflowTaskParameter.Parent.Parameters.Where(p => p.TaskDetails.TaskType == base.WorkflowTaskParameter.TaskDetails.TaskType && p.ParameterDetails.Id == myParentId).FirstOrDefault();
                        if (relatedParameter != null)
                        {
                            relatedParameterValue = relatedParameter.Value;
                        }
                    }

                    //try and find performance select from related parameter
                    PerformanceSelectionInfo performanceSelectionInfo = workflowSelectedPerformanceSelections.FirstOrDefault(p => p.Name.Equals(relatedParameterValue));
                    GFSModel.TimelineGroupList timelineGroups = null; 
                    GFSModel.PerformanceSource performanceSource = null;
                    if (performanceSelectionInfo != null)
                    {
                        timelineGroups = base.ParentWorkflow.TaskParameterDataCache.GetPerformanceSourceTimelineGroupsByPerformanceSelection(performanceSelectionInfo);
                        performanceSource = base.ParentWorkflow.TaskParameterDataCache.GetPerformanceSourceById(performanceSelectionInfo.GFSPerformanceSourceId); 
                    }

                    if (performanceSelectionInfo != null && timelineGroups != null && performanceSource != null)
                    {

                        PerformanceSelection perfSelection = base.ParentWorkflow.TaskParameterDataCache.GetPerformanceSelectionById(performanceSelectionInfo.Id);

                        //Show new performance selection window
                        var viewmodel = new PerformanceTimelineSelectionViewModel(perfSelection, performanceSource, timelineGroups, currentValues.Select(v => (Int64)v.Value1));
                        viewmodel.ShowDialog(parentWindow);

                        if (viewmodel.DialogResult == true)
                        {
                            IEnumerable<TimelineGroupView> timelinesToReturn;
                            if (perfSelection.SelectionType == PerformanceSelectionType.Fixed)
                            {
                                //Return inverted selections - to form the exclusions
                                timelinesToReturn = viewmodel.TimelineGroups.Where(p => !p.IsSelected);
                            }
                            else
                            {
                                //Return selections - to form the exclusions
                                timelinesToReturn = viewmodel.TimelineGroups.Where(p => p.IsSelected);
                            }

                            //Return values
                            return timelinesToReturn.Select(p => new TaskParameterValue(p.Code)).Cast<ITaskParameterValue>().ToList();
                        }
                    }
                    else
                    {
                        //Inform user
                        ModalMessage message = new ModalMessage();
                        message.Title = Message.PerformanceTimelineSelectionTaskParameterHelper_SelectPerformanceSelectionTimelines_Title;
                        message.Header = Message.PerformanceTimelineSelectionTaskParameterHelper_SelectPerformanceSelectionTimelines_MissingPerformanceSelection_Header;
                        message.Description = Message.PerformanceTimelineSelectionTaskParameterHelper_SelectPerformanceSelectionTimelines_MissingPerformanceSelection_Description;
                        message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                        message.ButtonCount = 1;
                        message.Button1Content = Message.Generic_Ok;
                        message.DefaultButton = ModalMessageButton.Button1;
                        message.MessageIcon = ImageResources.DialogError;
                        WindowHelper.ShowWindow(message, true);
                    }
                }
                catch (DataPortalException ex)
                {
                    LocalHelper.RecordException(ex);
                    Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                }
            }
            else
            {
                //Inform user
                ModalMessage message = new ModalMessage();
                message.Title = Message.PerformanceTimelineSelectionTaskParameterHelper_SelectPerformanceSelection_Connection_Title;
                message.Header = Message.PerformanceTimelineSelectionTaskParameterHelper_SelectPerformanceSelection_MissingConnection_Header;
                message.Description = Message.PerformanceTimelineSelectionTaskParameterHelper_SelectPerformanceSelection_MissingConnection_Description;
                message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                message.ButtonCount = 1;
                message.Button1Content = Message.Generic_Ok;
                message.DefaultButton = ModalMessageButton.Button1;
                message.MessageIcon = ImageResources.DialogError;
                WindowHelper.ShowWindow(message, true);
            }

            return currentValues.ToList();
        }

        /// <summary>
        /// Method to handle the changing of the timeline codes selection
        /// </summary>
        public override void OnValueChanged()
        {
            base.OnValueChanged();
            
            if (WorkpackagePlanogramParameter != null)
            {
                //Find linked performance for this planogram
                WorkpackagePlanogramParameter newValuePerformanceSelectionParameter = base.WorkpackagePlanogramParameter.Parent.Parameters.FirstOrDefault(p => p.ParameterDetails.ParameterType == TaskParameterType.PerformanceSelectionNameSingle);

                if (newValuePerformanceSelectionParameter?.Value != null)
                {
                    //Do we have a planogram parameter cached? or has the selection changed since the previously selected performance criteria 
                    if (_workpackagePlanogramParameterCache == null || !newValuePerformanceSelectionParameter.Value.Equals(_workpackagePlanogramParameterCache.Value))
                    {
                        //Setup a new planogram parameter cache
                        _workpackagePlanogramParameterCache = newValuePerformanceSelectionParameter;
                        _workpackagePlanogramParameterValues = null;
                    }

                    //Enumerate through all other planograms in the workpackage
                    foreach (WorkpackagePlanogram planogram in base.WorkpackagePlanogramParameter.Parent.Parent.Planograms.Where(p => p != base.WorkpackagePlanogramParameter.Parent).ToList())
                    {
                        //Find performance selection
                        WorkpackagePlanogramParameter performanceSelectionParameter = planogram.Parameters.FirstOrDefault(p => p.ParameterDetails.ParameterType == TaskParameterType.PerformanceSelectionNameSingle);
                        if (performanceSelectionParameter != null)
                        {
                            //If they are assigned to the same performance selection
                            if (performanceSelectionParameter.Value != null && performanceSelectionParameter.Value.Equals(newValuePerformanceSelectionParameter.Value))
                            {
                                //Find timeline selection parameter
                                WorkpackagePlanogramParameter timelineSelectionParameter = planogram.Parameters.FirstOrDefault(p => p.ParameterDetails.ParameterType == TaskParameterType.PerformanceTimelinesSelection);
                                if (timelineSelectionParameter != null)
                                {
                                    //Clear existing values
                                    if (timelineSelectionParameter.Values.Any()) timelineSelectionParameter.Values.Clear();

                                    //Create a new planogram parameter cache to be used until the criteria changes
                                    if (_workpackagePlanogramParameterValues == null)
                                    {
                                        _workpackagePlanogramParameterValues = base.WorkpackagePlanogramParameter.Values.Select(p => WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(p.Value1));
                                    }

                                    //Add items silently without triggering OnValueChanged again
                                    timelineSelectionParameter.AddValues(_workpackagePlanogramParameterValues, false);
                                }
                            }
                        }
                    }
                }
            }

            OnPropertyChanged("AlternateDisplayValue");
        }

        public override Object AlternateDisplayValue
        {
            get { return GenerateAlternateDisplayValue(); }
        }

        public Object GenerateAlternateDisplayValue()
        {
            if (base.WorkpackagePlanogramParameter == null && base.WorkflowTaskParameter == null) return 0;

            //Get performance selection parameter name for this planogram
            String performanceSelectionName = GetPerformanceSelectionName();
            if (String.IsNullOrEmpty(performanceSelectionName)) return 0;


            //Find corresponding performance selection info
            PerformanceSelectionInfo linkedInfo = base.ParentWorkflow.TaskParameterDataCache.GetPerformanceSelectionInfos()
                .FirstOrDefault(p => p.Name.Equals(performanceSelectionName));

            return linkedInfo == null ? 0 : GetSelectedTimelinesCount(linkedInfo, base.ParentWorkflow.TaskParameterDataCache);
        }

        private String GetPerformanceSelectionName()
        {
            if (base.WorkpackagePlanogramParameter != null)
            {
                WorkpackagePlanogramParameter performanceSelectionParameter = base.WorkpackagePlanogramParameter.Parent.Parameters
                    .FirstOrDefault(p => p.ParameterDetails.ParameterType == TaskParameterType.PerformanceSelectionNameSingle);
                if (performanceSelectionParameter == null || performanceSelectionParameter.Value == null) return String.Empty;
                return (String)performanceSelectionParameter.Value;
            }
            else
            {
                WorkflowTaskParameter workflowTaskParameter = base.WorkflowTaskParameter.Parent.Parameters
                    .FirstOrDefault(p => p.ParameterDetails.ParameterType == TaskParameterType.PerformanceSelectionNameSingle);
                if (workflowTaskParameter == null || workflowTaskParameter.Value == null) return String.Empty;
                return (String)workflowTaskParameter.Value;
            }
        }

        public static Int32 GetSelectedTimelinesCount(PerformanceSelectionInfo perfSelectionInfo, Model.Workflow.DataCache cache)
        {
            //Get all available timeline groups
            GFSModel.TimelineGroupList allTimelineGroups = cache.GetPerformanceSourceTimelineGroupsByPerformanceSelection(perfSelectionInfo);
            if (allTimelineGroups == null) return 0;


            PerformanceSelection perfSelection = cache.GetPerformanceSelectionById(perfSelectionInfo.Id);
            IEnumerable<Int64> allTimelineCodes = allTimelineGroups.Select(p => p.Code);

            if (perfSelectionInfo.SelectionType == PerformanceSelectionType.Fixed)
            {
                return perfSelection.TimelineGroups.Count(g => allTimelineCodes.Contains(g.Code));
            }
            else
            {
                //Calculate the timelines included from the dynamic performance selection weeks
                GFSModel.Entity gfsEntity = cache.GetGfsModelEntity();
                if (gfsEntity == null) return 0;

                GFSModel.PerformanceSource performanceSource = cache.GetPerformanceSourceById(perfSelectionInfo.GFSPerformanceSourceId);


                IEnumerable<Int64> specifiedDynamicTimelineCodes = perfSelection.GetTimelineCodesDynamic(gfsEntity, performanceSource);

                //Form the exclusions based on entire timeline groups, 
                //minus these date period inclusions & add extra exclusions that there may be
                IEnumerable<Int64> perfectionSelectionTimelineExclusions = perfSelection.TimelineGroups.Select(p => p.Code);

                //Return selections - to form the exclusions
                //Specified dynamic timelines EXCEPT extra performance selection exclusions, removed from all available timelines 
                return specifiedDynamicTimelineCodes.Except(perfectionSelectionTimelineExclusions).Count();
            }
        }


    }
}