﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31560 : D.Pleasance
//  Created.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Data;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class DecreaseProductMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_DecreaseProductMultipleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_DecreaseProductMultipleColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            List<Tuple<String, ProductDecreaseType, Single>> decreaseProducts = currentValues.Select(item =>
                        new Tuple<String, ProductDecreaseType, Single>(item.Value1.ToString(), (ProductDecreaseType)item.Value2, (Single)item.Value3)).
                    ToList();

            DecreaseProductSelectorViewModel viewmodel = new DecreaseProductSelectorViewModel(decreaseProducts);
            viewmodel.ShowDialog(parentWindow);

            if (viewmodel.DialogResult == true)
            {
                return viewmodel.DecreaseProductRows.Select(p => new TaskParameterValue(p.ProductGtin, p.ProductDecreaseType, p.DecreaseValue)).Cast<ITaskParameterValue>().ToList();
            }

            return currentValues.ToList();
        }
    }
}