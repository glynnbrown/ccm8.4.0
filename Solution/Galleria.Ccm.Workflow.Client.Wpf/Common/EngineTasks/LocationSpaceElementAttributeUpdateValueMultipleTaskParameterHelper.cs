﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    class LocationSpaceElementAttributeUpdateValueMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_LocationSpaceElementAttributeUpdateValueMultipleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_LocationSpaceElementAttributeUpdateValueMultipleColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            // Any value is allowed, including a selection of zero items.
            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            // First, get a list of the current values. We need these in a format that the attribute selector
            // can use, so if we have any, we cast them back into their original data types and package them
            // up as a list of tuples.
            List<Tuple<String, Object, Object>> currentValuesAsTuples = null;
            if (currentValues.Any())
            {
                currentValuesAsTuples = new List<Tuple<String, Object, Object>>();
                foreach (ITaskParameterValue value in currentValues)
                {
                    String value1;
                    try
                    {
                        value1 = (String)value.Value1;
                    }
                    catch
                    {
                        continue;
                    }
                    if (String.IsNullOrEmpty(value1)) continue;
                    currentValuesAsTuples.Add(new Tuple<String, Object, Object>(value1, null, null));
                }
            }

            // Create and display the selector window.
            LocationSpaceElementAttributeSelectorViewModel viewModel = new LocationSpaceElementAttributeSelectorViewModel(currentSelection: currentValuesAsTuples);
            //viewModel.AvailableAttributes.AddRange()
            CommonHelper.GetWindowService().ShowDialog<LocationSpaceElementAttributeSelectorWindow>(viewModel);

            // Return selection as appropriate.
            if (viewModel.DialogResult == true)
            {
                return viewModel.SelectedAttributes.
                    // shelf attribute parameters consist of its property name
                    Select(a=>new TaskParameterValue(a.PropertyName)).
                    Cast<ITaskParameterValue>().
                    ToList();
            }

            return currentValues.ToList();
        }
    }
}