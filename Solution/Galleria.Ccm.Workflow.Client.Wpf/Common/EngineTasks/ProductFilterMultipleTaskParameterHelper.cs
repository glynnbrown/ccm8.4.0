﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27426 : L.Ineson 
//  Created.
// V8-27426 : L.Ineson
//  Changed to use ITaskParameterValue
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM 820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : CCM830
// V8-31559 : A.Kuszyk
//  Re-factored to use updated window.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    /// <summary>
    /// Helper class used to allow the user to input values for a 
    /// ProductFilterMultiple engine task parameter type.
    /// </summary>
    public sealed class ProductFilterMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ProductFilterMultipleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_ProductFilterMultipleColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            String error = null;

            //nb - ignore the isNullAllowed flag for this type.
            // If no filters are provided then the engine will select all products.

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            var viewmodel = new ProductFilterSelectorViewModel(
                PlanogramProduct.EnumerateDisplayableFieldInfos(includeMetadata:true,includeCustom:true,includePerformanceData:true).ToList(),
                allowMultiple: true,
                existingFilters: currentValues);
            viewmodel.ShowDialog(parentWindow);

            if (viewmodel.DialogResult == true)
            {
                List<ITaskParameterValue> parameterValues = new List<ITaskParameterValue>();
                foreach (ProductFilterValue filter in viewmodel.Filters)
                {
                    parameterValues.Add(filter.CreateTaskParameterValue());
                }
                return parameterValues;
            }

            return currentValues.ToList();
        }
    }
}
