﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830
// V8-31944 : A.Silva
//  Created
// V8-32086 : N.Haywood
//  Added category code
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class PlanogramComparisonTemplateNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override String DataTemplateName { get { return "TaskTemplates_PlanogramComparisonTemplateNameTemplate"; } }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override String DataTemplateColumnName { get { return "TaskTemplates_PlanogramComparisonTemplateNameColumn"; } }

        /// <summary>
        /// Returns true if the parameter is allowed to be null. Defaults to false.
        /// </summary>
        public override Boolean IsNullAllowed { get { return true; } }

        /// <summary>
        /// Called on derived classes when <see cref="TaskParameterHelperBase.Validate"/> is called on this instance.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="isNullAllowed"></param>
        /// <returns></returns>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            ITaskParameterValue value = values.FirstOrDefault();
            Object val = value != null ? value.Value1 : null;
            String error = null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }
            else
            {
                PlanogramComparisonTemplateInfoList infos = ParentWorkflow.TaskParameterDataCache.GetPlanogramComparisonTemplateInfos();

                if (infos.All(info => info.Name != (String)val))
                {
                    error = Message.EngineTasks_PlanogramComparisonTemplate_InvalidError;
                }
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        /// <param name="currentValues"></param>
        /// <returns></returns>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            var win = new GenericSingleItemSelectorWindow
                      {
                          ItemSource = ParentWorkflow.TaskParameterDataCache.GetPlanogramComparisonTemplateInfos(),
                          SelectionMode = DataGridSelectionMode.Single
                      };
            WindowHelper.ShowWindow(win, parentWindow, true);

            if (win.DialogResult != true) return currentValues.ToList();
            return win.SelectedItems == null
                       ? currentValues.ToList()
                       : new List<ITaskParameterValue> {new TaskParameterValue(win.SelectedItems.Cast<PlanogramComparisonTemplateInfo>().First().Name)};
        }
    }
}
