﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 803
// V8-29643 : D.Pleasance
//	Created.
#endregion

#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    class PlanogramAttributeMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_PlanogramAttributeMultipleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_PlanogramAttributeMultipleColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            // Any value is allowed, including a selection of zero items.
            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            // First, get a list of the current values. We need these in a format that the attribute selector
            // can use, so if we have any, we cast them back into their original data types and package them
            // up as a list of tuples.
            List<Tuple<PlanogramAttributeSourceType, String, Object>> currentValuesAsTuples = null;
            if (currentValues.Any())
            {
                currentValuesAsTuples = new List<Tuple<PlanogramAttributeSourceType, String, Object>>();
                foreach (ITaskParameterValue value in currentValues)
                {
                    PlanogramAttributeSourceType value1;
                    String value2;
                    Object value3;
                    try
                    {
                        value1 = (PlanogramAttributeSourceType)value.Value1;
                        value2 = (String)value.Value2;
                        value3 = value.Value3;
                    }
                    catch
                    {
                        continue;
                    }
                    if (String.IsNullOrEmpty(value2)) continue;
                    currentValuesAsTuples.Add(new Tuple<PlanogramAttributeSourceType, String, Object>(value1, value2, value3));
                }
            }

            // Create and display the selector window.
            PlanogramAttributeSelectorViewModel viewModel = 
                new PlanogramAttributeSelectorViewModel(currentSelection: currentValuesAsTuples);
            CommonHelper.GetWindowService().ShowDialog<PlanogramAttributeSelectorWindow>(viewModel);
            
            // Return selection as appropriate.
            if (viewModel.DialogResult == true)
            {
                return viewModel.SelectedAttributes.
                    // planogram attribute parameters consist of the attribute source, its property name and value to apply
                    Select(a=>new TaskParameterValue(a.Source, a.PropertyName, a.PropertyValue)).
                    Cast<ITaskParameterValue>().
                    ToList();
            }

            return currentValues.ToList();
        }
    }
}