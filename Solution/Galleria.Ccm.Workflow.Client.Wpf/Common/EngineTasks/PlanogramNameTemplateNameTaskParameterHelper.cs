﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM830
// V8-31831 : A.Probyn
//  Created
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Windows.Controls;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class PlanogramNameTemplateNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_PlanogramNameTemplateNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_PlanogramNameTemplateNameColumn"; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Returns true if the parameter is allowed to be null
        /// </summary>
        public override bool IsNullAllowed
        {
            get
            {
                if (WorkflowTaskParameter == null) return false;
                if (WorkpackagePlanogramParameter != null) return WorkpackagePlanogramParameter.IsNullAllowed(WorkflowTaskParameter);
                return WorkflowTaskParameter.IsNullAllowed();
            }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }
            else
            {
                var planogramNameTemplates = base.ParentWorkflow.TaskParameterDataCache.GetPlanogramNameTemplateInfos();
                if (!planogramNameTemplates.Any(template => template.Name == (String)val))
                {
                    error = Message.EngineTasks_PlanogramNameTemplate_InvalidError;
                }

            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            var win = new PlanogramNameTemplateSelectionWindow();
            WindowHelper.ShowWindow(win, parentWindow, true);

            if (win.SelectionResult != null)
            {
                return new List<ITaskParameterValue>
                { 
                    new TaskParameterValue(win.SelectionResult.Name)
                };
            }

            return currentValues.ToList();
        }
    }
}
