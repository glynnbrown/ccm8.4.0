﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-29078 : A.Kuszyk
//  Created.
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Csla;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Services;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using System.Windows.Controls;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class CategoryCodeTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_CategoryCodeTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_CategoryCodeColumn"; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            ITaskParameterValue taskParameter = values.FirstOrDefault();
            String error = null;

            if (taskParameter == null)
            {
                // This is fine, because the task can handle null values.
            }
            else
            {
                ProductGroupInfo productGroupInfo = null;
                try
                {
                    productGroupInfo = ProductGroupInfoList
                        .FetchByProductGroupCodes(new String[] { Convert.ToString(taskParameter.Value2) })
                        .FirstOrDefault();
                }
                catch (DataPortalException ex)
                {
                    if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw ex;
                } 
                finally
                {
                    if (productGroupInfo == null)
                    {
                        error = Message.EngineTasks_CategoryCode_InvalidError; 
                    }
                }
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            MerchGroupSelectionWindow selectionWindow = new MerchGroupSelectionWindow();
            ServiceContainer.GetService<IWindowService>().ShowDialog<MerchGroupSelectionWindow>(selectionWindow);

            if (selectionWindow.DialogResult == true)
            {
                if (selectionWindow.SelectionResult != null && selectionWindow.SelectionResult.Any())
                {
                    ProductGroup selectedGroup = selectionWindow.SelectionResult.First();
                    return new List<ITaskParameterValue> { new TaskParameterValue(selectedGroup.Name,selectedGroup.Code) };
                }
            }

            return currentValues.ToList();
        }
    }
}
