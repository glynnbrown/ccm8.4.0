﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25460 : L.Ineson 
//  Created.
// V8-27426 : L.Ineson
//  Changed to use ITaskParameterValue
// V8-28585 : A.Kuszyk
//  Amended validation to allow no product selections.
#endregion
#region Version History: CCM811
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History: CCM820
// V8-30728 : A.Kuszyk
//   Added IsDisabled and DisabledReason members.
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : CCM830
// V8-31561 : A.Kuszyk
//  Added facility to manually order products for Add Products using Lists tasks.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.UpdateProductColourFromHighlight.v1;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class ProductCodeMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ProductCodeMultipleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_ProductCodeMultipleColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            // V8-28585:
            // No product codes is a valid selection.
            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            // V8-31561
            // The product selection should be manually orderable for the Add Products using List tasks.
            Boolean isOrderedList =
                WorkflowTaskParameter.TaskDetails.TaskType.Equals("Galleria.Ccm.Engine.Tasks.AddProductManual.v1.Task") ||
                WorkflowTaskParameter.TaskDetails.TaskType.Equals("Galleria.Ccm.Engine.Tasks.InsertProduct.v1.Task");

            var viewmodel = new ProductSelectorViewModel(currentValues.Select(v => v.Value1 as String), categoryCode, isOrderable: isOrderedList);
            viewmodel.ShowDialog(parentWindow);

            if (viewmodel.DialogResult == true)
            {
                return viewmodel.AssignedProducts.Select(p => new TaskParameterValue(p.Gtin)).Cast<ITaskParameterValue>().ToList();
            }

            return currentValues.ToList();
        }
    }
}
