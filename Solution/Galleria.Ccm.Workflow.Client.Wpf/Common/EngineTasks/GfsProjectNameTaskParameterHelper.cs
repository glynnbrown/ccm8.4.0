﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
//  V8-27783 : M.Brumby
//      Created.
// V8-28284  : A.Probyn
//      Added defensive code to SelectValues to deal with no gfs connection or error connecting to GFS.
#endregion
#region Version History: CCM811
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History: CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
// V8-30511 : J.Pickup
//  Now does validation and can only select projects that match plan assignment category or plan category.
// V8-31582 : N.Foster
//  Fixed issue where planogram id converted to Int16 instead of Int32
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
// CCM-18566 : M.Brumby
//  Selecting value now catches more errors
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class GfsProjectNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_GfsProjectNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_GfsProjectNameColumn"; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            using (new CodePerformanceMetric())
            {
                Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
                String error = null;

                if (val == null)
                {
                    if (!isNullAllowed)
                    {
                        error = Message.EngineTasks_NoValueError;
                    }
                }
                else
                {
                    // Fetch the planogram for this workpackage or null if we are workflow
                    PlanogramInfo myPlanogram = this.DeterminePlanogramForWorkpackage();

                    if (myPlanogram != null)
                    {
                        // Work out whether to use category code from plan or plan assignment
                        String resolvedCategoryCode = ResolveCategoryCode(myPlanogram);

                        Boolean acceptable = false;

                        if (!String.IsNullOrEmpty(resolvedCategoryCode))
                        {
                            // Check that category for selected project exists in the avilable projects for the category.
                            GFSModel.Entity gfsEntity = base.ParentWorkflow.TaskParameterDataCache.GetGfsModelEntity();

                            // Otherwise continue and attempt to load projects for the given category
                            List<GFSModel.Project> projectsToDisplay = GetAndMergeProjectsForCategoryAndProjectsWithNoCatgegoryAssigned(gfsEntity, resolvedCategoryCode);

                            acceptable = projectsToDisplay.Any(fr => fr.Name.Equals(values.First().Value1));
                        }
                        else
                        {
                            acceptable = true;
                        }

                        if (!acceptable)
                        {
                            error = Message.GfsProjectNameTaskParameterHelper_InvalidProjectForPlanCategory;
                        }
                    }
                }
                return error;
            }
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            using (new CodePerformanceMetric())
            {
                var win = new GenericSingleItemSelectorWindow();
                win.SelectionMode = DataGridSelectionMode.Single;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // Fetch the entity details
                Entity entity = base.ParentWorkflow.TaskParameterDataCache.GetEntity();

                if (entity.GFSId.HasValue && !(String.IsNullOrEmpty(entity.SystemSettings.FoundationServicesEndpoint)))
                {
                    try
                    {
                        // Fetch the gfs entity details
                        GFSModel.Entity gfsEntity = base.ParentWorkflow.TaskParameterDataCache.GetGfsModelEntity();

                        // Fetch the planogram for this workpackage or null if we are workflow
                        PlanogramInfo myPlanogram = this.DeterminePlanogramForWorkpackage();

                        // Set the windows item source according to circumstance
                        DetermineAndSetItemSelectorWindowSource(win, myPlanogram, gfsEntity);

                        System.Windows.Forms.Cursor.Current = null;
                        WindowHelper.ShowWindow(win, parentWindow, true);

                        if (win.DialogResult == true)
                        {
                            if (win.SelectedItems != null)
                            {
                                return new List<ITaskParameterValue> {
                        new TaskParameterValue(win.SelectedItems.Cast<GFSModel.Project>().First().Name) };
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LocalHelper.RecordException(ex);
                        Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                    }
                }
                else
                {
                    //Inform user
                    ModalMessage message = new ModalMessage();
                    message.Title = Message.GFSProjectNameTaskParameterHelper_SelectGfsProject_Connection_Title;
                    message.Header = Message.GFSProjectNameTaskParameterHelper_SelectGfsProject_MissingConnection_Header;
                    message.Description = Message.GFSProjectNameTaskParameterHelper_SelectGfsProject_MissingConnection_Description;
                    message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    message.ButtonCount = 1;
                    message.Button1Content = Message.Generic_Ok;
                    message.DefaultButton = ModalMessageButton.Button1;
                    message.MessageIcon = ImageResources.DialogError;
                    WindowHelper.ShowWindow(message, true);
                }

                return currentValues.ToList();
            }
        }

        #region Helper Methods

        /// <summary>
        /// Gets the planogram for the current workpackage parameter.
        /// </summary>
        /// <returns></returns>
        private PlanogramInfo DeterminePlanogramForWorkpackage()
        {
            if (this.WorkpackagePlanogramParameter != null
                        && this.WorkpackagePlanogramParameter.Parent != null
                        && this.WorkpackagePlanogramParameter.Parent.SourcePlanogramId != null)
            {
                WorkpackagePlanogram workpackagePlanogram = this.WorkpackagePlanogramParameter.Parent;
                PlanogramInfo myPlanogram =
                    base.ParentWorkflow.TaskParameterDataCache.GetPlanogramInfo(
                        Convert.ToInt32(workpackagePlanogram.SourcePlanogramId),
                        this.GetWorkpackageSourcePlanogramIds());

                return myPlanogram;
            }

            else return null;
        }

        /// <summary>
        /// Loads the correct category code based on the planogram which we should use to search proects for.
        /// </summary>
        /// <param name="planogramInfo"></param>
        /// <returns></returns>
        private String ResolveCategoryCode(PlanogramInfo planogramInfo)
        {
            LocationPlanAssignmentList assignmentList = base.ParentWorkflow.TaskParameterDataCache.GetLocationPlanAssignments(planogramInfo.Id);
            if (assignmentList.Any())
            {
                ProductGroupInfo productGroup = base.ParentWorkflow.TaskParameterDataCache.GetProductGroupInfo(assignmentList.First().ProductGroupId);
                if (productGroup != null)
                {
                    return productGroup.Code;
                }
            }

            return planogramInfo.CategoryCode;
        }

        /// <summary>
        /// Sets the itemsource of the Itemselector. 
        /// </summary>
        /// <param name="win"></param>
        /// <param name="myPlanogram"></param>
        private void DetermineAndSetItemSelectorWindowSource(GenericSingleItemSelectorWindow win, PlanogramInfo myPlanogram, GFSModel.Entity gfsEntity)
        {
            if (myPlanogram != null)
            {
                // Work out whether to use category code from plan or plan assignment
                String resolvedCategoryCode = ResolveCategoryCode(myPlanogram);

                // If loaded planogram has no category code then load all projects
                if (String.IsNullOrEmpty(resolvedCategoryCode))
                {
                    win.ItemSource = base.ParentWorkflow.TaskParameterDataCache.GetProjectInfos();
                }
                else
                {
                    // Otherwise continue and attempt to load projects for the given category
                    List<GFSModel.Project> projectsToDisplay = GetAndMergeProjectsForCategoryAndProjectsWithNoCatgegoryAssigned(gfsEntity, resolvedCategoryCode);

                    if (projectsToDisplay != null && projectsToDisplay.Any())
                    {
                        win.ItemSource = projectsToDisplay;
                    }
                }
            }
            else
            {
                // We are on workflow tab so should be allowed to select whatever as no planogram associated yet. 
                win.ItemSource = base.ParentWorkflow.TaskParameterDataCache.GetProjectInfos();
            }
        }

        /// <summary>
        /// Gets and merges (Distinctly) the projects for a category including projects that have no category assigned whatsoever. 
        /// </summary>
        private List<GFSModel.Project> GetAndMergeProjectsForCategoryAndProjectsWithNoCatgegoryAssigned(GFSModel.Entity gfsEntity, String resolvedCategoryCode)
        {
            // Specific category
            if (gfsEntity == null) return new List<GFSModel.Project>();

            // Specific category
            GFSModel.ProjectList categorySpecificResults = base.ParentWorkflow.TaskParameterDataCache.GetProjectInfoByProductGroupCode(resolvedCategoryCode);

            // No categories assigned
            GFSModel.ProjectList allUnassignedForEntity = base.ParentWorkflow.TaskParameterDataCache.GetProjectInfosWithNoProductGroupAssigned();

            // Figure out which ones are distinct and merge
            IEnumerable<Int32> takenIds = categorySpecificResults.Select(pr => pr.Id);

            List<GFSModel.Project> mergedProjects = allUnassignedForEntity.Where(ufc => !takenIds.Contains(ufc.Id)).ToList();
            mergedProjects.AddRange(categorySpecificResults);

            return mergedProjects;
        }

        #endregion
    }
}
