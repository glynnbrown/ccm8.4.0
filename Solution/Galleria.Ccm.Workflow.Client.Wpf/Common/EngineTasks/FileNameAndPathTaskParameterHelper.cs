﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24779 : D.Pleasance
//  Created
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM 820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
// V8-30725 : M.Brumby
//  Changed file filter to be the same as the wizard file filter
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Engine;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class FileNameAndPathTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_FileNameAndPathTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_FileNameAndPathColumn"; }
        }

        protected override string OnValidate(IEnumerable<Engine.ITaskParameterValue> values, bool isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }

            return error;
        }

        public override List<Engine.ITaskParameterValue> SelectValues(IEnumerable<Engine.ITaskParameterValue> currentValues, System.Windows.Window parentWindow, String categoryCode)
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.Filter = Message.WorkpackageWizard_SelectFiles_Filter;
            dlg.Multiselect = false;
            dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return new List<ITaskParameterValue> {
                        new TaskParameterValue(dlg.FileName) };

            }

            return currentValues.ToList();
        }
    }
}
