﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson 
//  Created.
// V8-27426 : L.Ineson
//  Changed to use ITaskParameterValue
#endregion
#region Version History : (CCM811)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize() method to allow the WorkpackagePlanogramParameter to be passed in
//  Add OnValueChanged() method
#endregion
#region Version History : CCM 820
// V8-30774 : A.Kuszyk
//  Removed single parameter Initialize overload.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
// V8-32133 : A.Probyn
//  Added new AlternateDisplayValue property definition to interface.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public interface ITaskParameterHelper
    {
        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        Boolean IsMultiValue { get; }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        Boolean IsContentLink { get; }

        /// <summary>
        /// Returns true if the parameter is allowed to be null
        /// </summary>
        Boolean IsNullAllowed { get; }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="isNullAllowed"></param>
        /// <returns></returns>
        String Validate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed);

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        /// <param name="currentValues"></param>
        /// <returns></returns>
        List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode);

        /// <summary>
        /// Creates a new parameter column for the parameter type.
        /// </summary>
        /// <param name="header"></param>
        /// <param name="headerGroupName"></param>
        /// <param name="parameterPath"></param>
        /// <param name="isReadOnly"></param>
        /// <param name="parameterDetails"></param>
        /// <param name="headerGroupToolTip">tool tip for the header group if required</param>
        /// <returns></returns>
        DataGridColumn CreateNewParameterColumn(
            String header, String headerGroupName, String parameterPath, 
            Boolean isReadOnly, EngineTaskParameterInfo parameterDetails, Object headerGroupToolTip);

        /// <summary>
        /// Returns the workflow maintenance data template for this parameter type
        /// </summary>
        /// <returns></returns>
        DataTemplate GetDataTemplate();

        /// <summary>
        /// Does any task specific work i.e. useful for tasks with parameters that are dependant on other parameters. 
        /// </summary>
        void Initialize(WorkflowTaskParameter workflowTaskParameter, WorkpackagePlanogramParameter workpackagePlanogramParameter);

        /// <summary>
        /// Performs any actions required when the value of the parameter changes
        /// (for example - clears child prameter values etc)
        /// </summary>
        void OnValueChanged();

        /// <summary>
        /// Returns an alternative dispaly value for the helper, if required. 
        /// /// </summary>
        Object AlternateDisplayValue { get; }
    }
}
