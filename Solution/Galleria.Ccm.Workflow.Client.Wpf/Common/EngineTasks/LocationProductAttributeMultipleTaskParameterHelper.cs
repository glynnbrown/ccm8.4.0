﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 820
// V8-30733 : L.Luong
//	Created.
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class LocationProductAttributeMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_LocationProductAttributeMultipleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_LocationProductAttributeMultipleColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            // Any value is allowed, including a selection of zero items.
            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            // Create and display the selector window.
            LocationProductAttributeSelectorViewModel viewModel =
                new LocationProductAttributeSelectorViewModel(currentValues.Select(c => c.Value1.ToString()));
            CommonHelper.GetWindowService().ShowDialog<LocationProductAttributeSelectorWindow>(viewModel);

            // Return selection as appropriate.
            if (viewModel.DialogResult == true)
            {
                return viewModel.SelectedAttributes.
                    // Product attribute parameters consist of the attribute source and its property name.
                    Select(a => new TaskParameterValue(a.PropertyName)).
                    Cast<ITaskParameterValue>().
                    ToList();
            }

            return currentValues.ToList();
        }
    }
}
