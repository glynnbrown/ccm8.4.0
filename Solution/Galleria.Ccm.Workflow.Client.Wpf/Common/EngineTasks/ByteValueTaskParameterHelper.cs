﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson 
//  Created.
// V8-27426 : L.Ineson
//  Changed to use ITaskParameterValue
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
// V8-32357 : M.Brumby
//  [PCR01561] Ability Customise the names of Workflow Tasks
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class ByteValueTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ByteTemplate"; }
        }

        /// <summary>
        /// Returns an empty string, because we override CreateNewParameterColumn in this class.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return String.Empty; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            if (!isNullAllowed && val == null)
            {
                error = Message.EngineTasks_NoValueError;
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            //do nothing - invalid.
            throw new ArgumentOutOfRangeException();
        }

        /// <summary>
        /// Creates a new parameter column for the parameter type.
        /// </summary>
        public override DataGridColumn CreateNewParameterColumn(
            String header, String headerGroupName, String parameterPath,
           Boolean isReadOnly, EngineTaskParameterInfo parameterDetails, Object headerGroupToolTip)
        {
            String bindingPath = parameterPath + ".Value";

            DataGridExtendedTextColumn col = new DataGridExtendedTextColumn();
            col.Header = header;
            col.HeaderGroupNames.Add(new DataGridHeaderGroup() { HeaderName = headerGroupName, HeaderToolTip = headerGroupToolTip });

            Binding b = new Binding(bindingPath);
            b.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;

            if (isReadOnly)
            {
                col.IsReadOnly = true;
                b.Mode = BindingMode.OneWay;
            }

            col.Binding = b;

            return col;
        }
    }
}
