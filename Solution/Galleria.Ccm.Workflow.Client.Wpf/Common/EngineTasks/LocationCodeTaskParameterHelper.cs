﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-29078 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM 810
// V8-30026 : M.Brumby
//  Ensure location code is part of the location code selected result as well as location name
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM 820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Csla;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Services;
using Galleria.Ccm.Common.Wpf.Services;
using System.Windows;
using System.Collections.ObjectModel;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using System.Windows.Controls;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class LocationCodeTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_LocationCodeTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_LocationCodeColumn"; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            ITaskParameterValue taskParameter = values.FirstOrDefault();
            String error = null;

            if (taskParameter == null)
            {
                // This is fine, because the task can handle null values.
            }
            else
            {
                LocationInfo locationInfo = null;
                try
                {
                    locationInfo = base.ParentWorkflow.TaskParameterDataCache.GetLocationInfos()
                        .FirstOrDefault(l => l.Code.Equals(Convert.ToString(taskParameter.Value2)));
                }
                catch (DataPortalException ex) 
                {
                    if (!(ex.GetBaseException() is DtoDoesNotExistException)) throw ex;
                } 
                finally
                {
                    if (locationInfo == null)
                    {
                        error = Message.EngineTasks_LocationCode_InvalidError; 
                    }
                }
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            LocationSelectionViewModel viewModel = new LocationSelectionViewModel(
                base.ParentWorkflow.TaskParameterDataCache.GetLocationInfos(),
                new ObservableCollection<LocationInfo>(),
                base.ParentWorkflow.TaskParameterDataCache.GetLocationHierarchy(),
                System.Windows.Controls.DataGridSelectionMode.Single);

            ServiceContainer.GetService<IWindowService>().ShowDialog<LocationSelectionWindow>(viewModel);

            if (viewModel.DialogResult == true)
            {
                if (viewModel.SelectedAvailableLocations != null && viewModel.SelectedAvailableLocations.Any())
                {
                    LocationInfoRowViewModel selectedLocation = viewModel.SelectedAvailableLocations.First();
                    return new List<ITaskParameterValue> 
                    { 
                        new TaskParameterValue(String.Format("{0} {1}",  selectedLocation.Location.Code, selectedLocation.Location.Name), selectedLocation.Location.Code) 
                    };
                }
            }

            return currentValues.ToList();
        }
    }
}
