﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27949 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM811
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
//  Added specific validation against parent parameter for both workflow and workpackage -type parameters
// V8-30566 : M.Pettit
//	Altered IsNullAllowed to ensure that the correct parameter value is checked when there are 2 tasks in the workpackage with child/parent parameters
#endregion
#region Version History: CCM820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
// V8-30771 : A.Kuszyk
//  Re-factored IsNullAllowed logic into Recalc assortment task.
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
// V8-30836 : J.Pickup
//  Select values now uses InventoryProfileAttributeSelectorWindow and also uses IWindowService
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class InventoryProfileNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_InventoryProfileNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_InventoryProfileNameColumn"; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Returns true if the parameter is allowed to be null
        /// </summary>
        public override bool IsNullAllowed
        {
            get
            {
                if (WorkflowTaskParameter == null) return false;
                if (WorkpackagePlanogramParameter != null) return WorkpackagePlanogramParameter.IsNullAllowed(WorkflowTaskParameter);
                return WorkflowTaskParameter.IsNullAllowed();
            }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }
            else
            {
                var inventoryProfiles = base.ParentWorkflow.TaskParameterDataCache.GetInventoryProfileInfos();

                if (!inventoryProfiles.Any(m => m.Name.Equals(Convert.ToString(val), StringComparison.InvariantCultureIgnoreCase)))
                {
                    error = Message.EngineTasks_InventoryProfile_InvalidError;
                }
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            InventoryProfileAttributeSelectorViewModel viewModel = new InventoryProfileAttributeSelectorViewModel(
                base.ParentWorkflow.TaskParameterDataCache.GetInventoryProfileInfos());
            ServiceContainer.GetService<IWindowService>().ShowDialog<InventoryProfileAttributeSelectorWindow>(viewModel);

            if(viewModel.DialogResult == true)
            {
                if (viewModel.SelectedInventoryProfile != null)
                {
                    return new List<ITaskParameterValue> 
                    {
                        new TaskParameterValue(viewModel.SelectedInventoryProfile.Name)
                    };
                }
            }

            return currentValues.ToList();
        }
    }
}
