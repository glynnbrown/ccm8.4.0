﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29633 : D.Pleasance
//  Created.
#endregion
#region Version History: CCM810
// V8-29879 : D.Pleasance
//  Corrected incorrect validation error description.
#endregion 
#region Version History: CCM811
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed. Selected items now updates dependant properties when required.
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History: CCM820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class ClusterSchemeNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ClusterSchemeNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_ClusterSchemeNameColumn"; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Returns true if the parameter is allowed to be null
        /// </summary>
        public override bool IsNullAllowed
        {
            get
            {
                //if a workflow task parameter helper instance
                if (WorkflowTaskParameter != null && WorkpackagePlanogramParameter == null) return true;
                return false;
            }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }
            else
            {
                ClusterSchemeInfoList clusterScehemes = base.ParentWorkflow.TaskParameterDataCache.GetClusterSchemeInfos();

                if (!clusterScehemes.Any(scheme => scheme.Name == (String)val))
                {
                    error = Message.EngineTasks_ClusterScheme_InvalidError;
                }
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            String originalValue = Convert.ToString(this.WorkflowTaskParameter.Value);

            var win = new GenericSingleItemSelectorWindow();
            win.ItemSource = base.ParentWorkflow.TaskParameterDataCache.GetClusterSchemeInfos();
            win.SelectionMode = DataGridSelectionMode.Single;
            WindowHelper.ShowWindow(win,parentWindow, true);

            //Reset dependant properties
            if (win.SelectedItems != null && win.SelectedItems.Cast<ClusterSchemeInfo>().First().Name != originalValue)
            {
                Int32 myId = WorkflowTaskParameter.Parent.Parameters.IndexOf(WorkflowTaskParameter);
                List<WorkflowTaskParameter> relatedParameters = WorkflowTaskParameter.Parent.Parameters.Where(p => p.ParameterDetails.ParentId == myId).ToList();
                if (relatedParameters != null)
                {
                    foreach (WorkflowTaskParameter param in relatedParameters)
                    {
                        param.Value = param.ParameterDetails.DefaultValue;
                    }
                }
            }

            if (win.DialogResult == true)
            {
                if (win.SelectedItems != null)
                {
                    return new List<ITaskParameterValue> 
                    { 
                       new  TaskParameterValue(win.SelectedItems.Cast<ClusterSchemeInfo>().First().Name, null, null)
                    };
                }
            }

            return currentValues.ToList();
        }

        /// <summary>
        /// Performs any actions required when the value of the parameter changes
        /// (for example - clears child prameter values etc)
        /// </summary>
        public override void OnValueChanged()
        {
            //Clear out any child parameters from the same task
            if (WorkpackagePlanogramParameter != null)
            {
                IEnumerable<WorkpackagePlanogramParameter> childParameters = WorkpackagePlanogramParameter.GetChildParameters();
				foreach (var dependantParameter in childParameters)
                {
                    dependantParameter.Value = dependantParameter.ParameterDetails.DefaultValue;
                }
            }
        }
    }
}