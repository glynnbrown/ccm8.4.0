﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 801
// V8-27494 : A.Kuszyk
//	Created.
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    class ProductAttributeMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ProductAttributeMultipleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_ProductAttributeMultipleColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            // Any value is allowed, including a selection of zero items.
            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            // First, get a list of the current values. We need these in a format that the attribute selector
            // can use, so if we have any, we cast them back into their original data types and package them
            // up as a list of tuples.
            List<Tuple<ProductAttributeSourceType, String>> currentValuesAsTuples = null;
            if (currentValues.Any())
            {
                currentValuesAsTuples = new List<Tuple<ProductAttributeSourceType, String>>();
                foreach (ITaskParameterValue value in currentValues)
                {
                    ProductAttributeSourceType value1;
                    String value2;
                    try
                    {
                        value1 = (ProductAttributeSourceType)value.Value1;
                        value2 = (String)value.Value2;
                    }
                    catch
                    {
                        continue;
                    }
                    if (String.IsNullOrEmpty(value2)) continue;
                    currentValuesAsTuples.Add(new Tuple<ProductAttributeSourceType, String>(value1, value2));
                }
            }

            // Create and display the selector window.
            ProductAttributeSelectorViewModel viewModel = 
                new ProductAttributeSelectorViewModel(includeCustomFields: true, includePerformanceFields:false, currentSelection: currentValuesAsTuples);
            CommonHelper.GetWindowService().ShowDialog<ProductAttributeSelectorWindow>(viewModel);
            
            // Return selection as appropriate.
            if (viewModel.DialogResult == true)
            {
                return viewModel.SelectedAttributes.
                    // Product attribute parameters consist of the attribute source and its property name.
                    Select(a=>new TaskParameterValue(a.Source, a.PropertyName)).
                    Cast<ITaskParameterValue>().
                    ToList();
            }

            return currentValues.ToList();
        }
    }
}
