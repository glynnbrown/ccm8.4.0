﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: v8.0

// V8-27562 : A.Silva
//      Created.

#endregion

#region Version History : CCM 801
// V8-28835 : A.Kuszyk
//  Added additional isNullAllowed check to Validate().
#endregion

#region Version History : CCM 810 
// V8-29746 : A.Probyn
//  Updated IsContentLink to be true
#endregion

#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion

#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class RenumberingStrategyNameSingleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_RenumberingStrategyNameSingleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_RenumberingStrategyNameSingleColumn"; }
        }

        /// <summary>
        ///     Shows the value selector for the parameter type.
        /// </summary>
        /// <param name="currentValues"></param>
        /// <param name="parentWindow"></param>
        /// <returns></returns>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues,
            Window parentWindow, String categoryCode)
        {
            var window = new GenericSingleItemSelectorWindow
            {
              ItemSource  = base.ParentWorkflow.TaskParameterDataCache.GetRenumberingStrategyInfos(),
              SelectionMode = DataGridSelectionMode.Single
            };

            WindowHelper.ShowWindow(window, parentWindow, true);

            return window.DialogResult == true && window.SelectedItems != null
                ? new List<ITaskParameterValue>
                {
                    new TaskParameterValue(window.SelectedItems.Cast<RenumberingStrategyInfo>().First().Name)
                }
                : currentValues.ToList();
        }

        /// <summary>
        ///     Validates the given values and returns a friendly error message as required.
        /// </summary>
        /// <param name="values"></param>
        /// <param name="isNullAllowed"></param>
        /// <returns></returns>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            var nameParameter = (values.Any() ? values.First().Value1 : null) as String;

            if (!isNullAllowed && String.IsNullOrEmpty(nameParameter))
            {
                return Message.EngineTasks_NoValueError;
            }


            if (!isNullAllowed)
            {
                var infos = base.ParentWorkflow.TaskParameterDataCache.GetRenumberingStrategyInfos();
                if (infos.All(info => info.Name != nameParameter))
                {
                    return "Please enter a valid Renumbering Strategy name.";
                }
            }

            return null;
        }
    }
}