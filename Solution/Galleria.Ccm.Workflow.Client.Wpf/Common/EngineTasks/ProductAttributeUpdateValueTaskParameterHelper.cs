﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM830

// V8-32359 : A.Silva
//  Created.
// V8-32818 : A.Silva
//  Amended OnValidate, so that empty parameters are allowed.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    internal class ProductAttributeUpdateValueMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        ///     The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override String DataTemplateName { get { return "TaskTemplates_ProductAttributeUpdateValueMultipleTemplate"; } }

        /// <summary>
        ///     The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override String DataTemplateColumnName { get { return "TaskTemplates_ProductAttributeUpdateValueMultipleColumn"; } }

        /// <summary>
        ///     Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue { get { return true; } }

        /// <summary>
        ///     Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            //  Happy without values.
            return null;
        }

        /// <summary>
        ///     Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues,
                                                               Window parentWindow,
                                                               String categoryCode)
        {
            // First, get a list of the current values. We need these in a format that the attribute selector
            // can use, so if we have any, we cast them back into their original data types and package them
            // up as a list of tuples.
            List<Tuple<String, Object>> currentSelection = null;
            currentValues = currentValues as ICollection<ITaskParameterValue> ?? currentValues.ToList();

            if (currentValues.Any())
            {
                currentSelection = new List<Tuple<String, Object>>();
                foreach (ITaskParameterValue value in currentValues)
                {
                    String value1;
                    Object value2;
                    try
                    {
                        value1 = (String)value.Value1;
                        value2 = value.Value2;
                    }
                    catch
                    {
                        continue;
                    }
                    currentSelection.Add(new Tuple<String, Object>(value1, value2));
                }
            }

            // Create and display the selector window.
            var viewModel = new ProductAttributeUpdateSelectorViewModel(currentSelection);
            CommonHelper.GetWindowService().ShowDialog<ProductAttributeUpdateSelectorWindow>(viewModel);

            // Return selection as appropriate.
            if (viewModel.DialogResult == true)
            {
                return viewModel.SelectedAttributes.
                    // Product attribute update value parameters consist of the attribute update value object.
                                 Select(updateValue => new TaskParameterValue(updateValue.PropertyName, updateValue.NewValue)).
                                 Cast<ITaskParameterValue>().
                                 ToList();
            }

            return currentValues.ToList();
        }
    }
}
