﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : D.Pleasance
//  Created.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29483 : D.Pleasance
//  Amended to allow null planogram import template, not required for pog files
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
// V8-31092 : M.Brumby
//  Filter templates by file type
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.IO;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class PlanogramImportTemplateNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_PlanogramImportTemplateNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_PlanogramImportTemplateNameColumn"; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            // Do nothing - a null planogram import template selection is allowed.
            if (val == null || String.IsNullOrEmpty(Convert.ToString(val))) return error;

            
            IEnumerable<PlanogramImportTemplateInfo> templates = base.ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplateInfos();

            IEnumerable<PlanogramImportTemplateInfo> matchingNames =
                        templates.Where(p=> p.Name == Convert.ToString(val));

            if (!matchingNames.Any())
            {
                error = Message.EngineTasks_PlanogramImportTemplateName_InvalidError;
            }

            if (WorkflowTaskParameter != null)
            {
                Int32 myId = WorkflowTaskParameter.ParameterDetails.ParentId.Value;//WorkflowTaskParameter.Parent.Parameters.IndexOf(WorkflowTaskParameter);
                WorkflowTaskParameter relatedParameter = WorkflowTaskParameter.Parent.Parameters.Where(p => p.ParameterDetails.Id == myId).FirstOrDefault();

                if (relatedParameter != null && relatedParameter.Value != null &&
                        !relatedParameter.Value.ToString().EndsWith("POG", StringComparison.OrdinalIgnoreCase))
                {
                    switch (PlanogramImportFileTypeHelper.GetTypeFromExtension(Path.GetExtension(relatedParameter.Value.ToString())))
                    {
                        case PlanogramImportFileType.Apollo:
                            if (matchingNames.Any(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType != PlanogramImportFileType.Apollo))
                            {
                                error = Message.EngineTasks_PlanogramImportTemplateName_Apollo;
                            }
                            break;
                        case PlanogramImportFileType.ProSpace:
                            if (matchingNames.Any(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType != PlanogramImportFileType.ProSpace))
                            {
                                error = Message.EngineTasks_PlanogramImportTemplateName_ProSpace;
                            }
                            break;
                        case PlanogramImportFileType.SpacemanV9:
                            if (matchingNames.Any(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType != PlanogramImportFileType.SpacemanV9))
                            {
                                error = Message.EngineTasks_PlanogramImportTemplateName_Spaceman;
                            }
                            break;
                        default:
                            break;

                    }
                }

                if (WorkpackagePlanogramParameter != null)
                {
                    WorkpackagePlanogramParameter relatedPlanogramParameter = WorkpackagePlanogramParameter.Parent.Parameters.Where(p => p.WorkflowTaskParameterId == relatedParameter.Id).FirstOrDefault();

                    if (relatedPlanogramParameter != null && relatedPlanogramParameter.Value != null &&
                        !relatedPlanogramParameter.Value.ToString().EndsWith("POG", StringComparison.OrdinalIgnoreCase))
                    {
                        switch (PlanogramImportFileTypeHelper.GetTypeFromExtension(Path.GetExtension(relatedPlanogramParameter.Value.ToString())))
                        {
                            case PlanogramImportFileType.Apollo:
                                if (matchingNames.Any(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType != PlanogramImportFileType.Apollo))
                                {
                                    error = Message.EngineTasks_PlanogramImportTemplateName_Apollo;
                                }
                                break;
                            case PlanogramImportFileType.ProSpace:
                                if (matchingNames.Any(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType != PlanogramImportFileType.ProSpace))
                                {
                                    error = Message.EngineTasks_PlanogramImportTemplateName_ProSpace;
                                }
                                break;
                            case PlanogramImportFileType.SpacemanV9:
                                if (matchingNames.Any(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType != PlanogramImportFileType.SpacemanV9))
                                {
                                    error = Message.EngineTasks_PlanogramImportTemplateName_Spaceman;
                                }
                                break;
                            default:
                                break;

                        }
                    }
                }
            }
            


            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {

            IEnumerable<PlanogramImportTemplateInfo> templates = base.ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplateInfos();
            if (WorkflowTaskParameter != null)
            {
                Int32 myId = WorkflowTaskParameter.ParameterDetails.ParentId.Value;//WorkflowTaskParameter.Parent.Parameters.IndexOf(WorkflowTaskParameter);
                WorkflowTaskParameter relatedParameter = WorkflowTaskParameter.Parent.Parameters.Where(p => p.ParameterDetails.Id == myId).FirstOrDefault();

                if (relatedParameter != null && relatedParameter.Value != null)
                {
                    if (relatedParameter.Value.ToString().EndsWith("POG", StringComparison.OrdinalIgnoreCase))
                    {
                        templates = new List<PlanogramImportTemplateInfo>();
                    }
                    else
                    {
                        switch (PlanogramImportFileTypeHelper.GetTypeFromExtension(Path.GetExtension(relatedParameter.Value.ToString())))
                        {
                            case PlanogramImportFileType.Apollo:
                                templates = templates.Where(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType == PlanogramImportFileType.Apollo).ToList();
                                break;
                            case PlanogramImportFileType.ProSpace:
                                templates = templates.Where(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType == PlanogramImportFileType.ProSpace).ToList();
                                break;
                            case PlanogramImportFileType.SpacemanV9:
                                templates = templates.Where(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType == PlanogramImportFileType.SpacemanV9).ToList();
                                break;
                            default:
                                templates = new List<PlanogramImportTemplateInfo>();
                                break;

                        }
                    }
                }

                if (WorkpackagePlanogramParameter != null)
                {
                    
                    WorkpackagePlanogramParameter relatedPlanogramParameter = WorkpackagePlanogramParameter.Parent.Parameters.Where(p => p.WorkflowTaskParameterId == relatedParameter.Id).FirstOrDefault();

                    if (relatedPlanogramParameter != null && relatedPlanogramParameter.Value != null)
                    {
                        if (relatedPlanogramParameter.Value.ToString().EndsWith("POG", StringComparison.OrdinalIgnoreCase))
                        {
                            templates = new List<PlanogramImportTemplateInfo>();
                        }
                        else
                        {
                            switch (PlanogramImportFileTypeHelper.GetTypeFromExtension(Path.GetExtension(relatedPlanogramParameter.Value.ToString())))
                            {
                                case PlanogramImportFileType.Apollo:
                                    templates = templates.Where(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType == PlanogramImportFileType.Apollo).ToList();
                                    break;
                                case PlanogramImportFileType.ProSpace:
                                    templates = templates.Where(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType == PlanogramImportFileType.ProSpace).ToList();
                                    break;
                                case PlanogramImportFileType.SpacemanV9:
                                    templates = templates.Where(t => ParentWorkflow.TaskParameterDataCache.GetPlanogramImportTemplate(t.Id).FileType == PlanogramImportFileType.SpacemanV9).ToList();
                                    break;
                                default:
                                    templates = new List<PlanogramImportTemplateInfo>();
                                    break;

                            }
                        }
                    }
                }
            }

            var win = new GenericSingleItemSelectorWindow();
            win.ItemSource = templates;
            win.SelectionMode = DataGridSelectionMode.Single;
            WindowHelper.ShowWindow(win,parentWindow, true);

            if (win.DialogResult == true)
            {
                if (win.SelectedItems != null)
                {
                    return new List<ITaskParameterValue>
                    { 
                      new TaskParameterValue( win.SelectedItems.Cast<PlanogramImportTemplateInfo>().First().Name)
                    };
                }
            }

            return currentValues.ToList();
        }
    }
}
