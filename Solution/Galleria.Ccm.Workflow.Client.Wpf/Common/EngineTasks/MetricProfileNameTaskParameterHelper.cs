﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27949 : A.Kuszyk
//  Created.
#endregion
#region Version History: CCM811
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
//  Added specific validation against parent parameter for both workflow and workpackage -type parameters
// V8-30566 : M.Pettit
//	Altered IsNullAllowed to ensure that the correct parameter value is checked when there are 2 tasks in the workpackage with child/parent parameters
#endregion
#region Version History: CCM 820
// V8-30596 : N.Foster
//  Resolved issue where duplicate tasks in same workflow resulted in exception
// V8-30771 : A.Kuszyk
//  Re-factored IsNullAllowed logic into Recalc assortment task.
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
// V8-31095 : A.Probyn
//  Updated to use EngineTasks_NoValueError like other helpers.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Engine.Tasks.RecalculateAssortmentInventory.v1;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class MetricProfileNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_MetricProfileNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_MetricProfileNameColumn"; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Returns true if the parameter is allowed to be null
        /// </summary>
        public override bool IsNullAllowed
        {
            get 
            {
                if (WorkflowTaskParameter == null) return false;
                if (WorkpackagePlanogramParameter != null) return WorkpackagePlanogramParameter.IsNullAllowed(WorkflowTaskParameter);
                return WorkflowTaskParameter.IsNullAllowed();
            }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            //if the value is not set, check the parent task as it may require a value
            if (val == null || String.IsNullOrEmpty(Convert.ToString(val)))
            {
                //For the RecalculateAssortmentInventory task, parameter value depends on a 
                //parent parameter, Calculation Options
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
                return error;
            }

            MetricProfileInfoList metricProfiles = base.ParentWorkflow.TaskParameterDataCache.GetMetricProfileInfos();

            if (!metricProfiles.Any(m=>m.Name.Equals(Convert.ToString(val),StringComparison.InvariantCultureIgnoreCase)))
            {
                error = Message.EngineTasks_MetricProfile_InvalidError;
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            var win = new GenericSingleItemSelectorWindow();
            win.ItemSource = base.ParentWorkflow.TaskParameterDataCache.GetMetricProfileInfos();
            win.SelectionMode = DataGridSelectionMode.Single;
            WindowHelper.ShowWindow(win, parentWindow, true);

            if (win.DialogResult == true)
            {
                if (win.SelectedItems != null)
                {
                    return new List<ITaskParameterValue> {
                        new TaskParameterValue(win.SelectedItems.Cast<MetricProfileInfo>().First().Name) };
                }
            }

            return currentValues.ToList();
        }
    }
}
