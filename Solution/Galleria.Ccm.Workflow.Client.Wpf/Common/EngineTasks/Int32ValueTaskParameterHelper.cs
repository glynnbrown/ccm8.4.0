﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Created.
// V8-30907 : A.Kuszyk
//  Changed to use data template.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Engine;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class Int32ValueTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_Int32Template"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard screen.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_Int32Column"; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            if (!isNullAllowed && val == null)
            {
                error = Message.EngineTasks_NoValueError;
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            //do nothing - invalid.
            throw new ArgumentOutOfRangeException();
        }
    }
}
