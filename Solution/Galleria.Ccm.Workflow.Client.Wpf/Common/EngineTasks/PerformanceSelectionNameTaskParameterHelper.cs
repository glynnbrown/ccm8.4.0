﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26769 : L.Ineson 
//  Created.
// V8-27426 : L.Ineson
//  Changed to use ITaskParameterValue
#endregion
#region Version History: (CCM 8.0.3)
// V8-29491 : D.Pleasance
//  Amended so that a null performance selection is valid, this is handled in the task itself. This change has come about due 
//  to the default workflows that are to be shipped with V8 will have no performance selection.
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-31830 : A.Probyn
//  Updated to set performance selection timeline paramter upon being set.
// V8-32086 : N.Haywood
//  Added category code
// V8-32133 : A.Probyn
//  Updated so dynamic performance selections have all other timelines outside of this dynamic value excluded too.
// V8-32662 : A.Silva
//  Amended OnValueChanged() to avoid a NullReference issue when no Linked Performance Selection Parameter Values could be fetched for a WorkflowTaskParameter.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class PerformanceSelectionNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_PerformanceSelectionNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_PerformanceSelectionNameColumn"; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            // Do nothing - a null performance selection is allowed.
            if (val == null || String.IsNullOrEmpty(Convert.ToString(val))) return error;

            IEnumerable<PerformanceSelectionInfo> matchingNames =
                base.ParentWorkflow.TaskParameterDataCache.GetPerformanceSelectionInfos()
                .Where(p => p.Name == Convert.ToString(val));

            if (!matchingNames.Any())
            {
                error = Message.EngineTasks_PerformanceSelection_InvalidError;
            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            //first check if a GFS connection is required and available
            if (
                (WorkpackagePlanogramParameter != null
                    && WorkpackagePlanogramParameter.EnumerateRelatedParameters(WorkflowTaskParameter)
                        .Any(t => t.ParameterDetails.ParameterType == TaskParameterType.PerformanceTimelinesSelection))
                ||
                (WorkflowTaskParameter != null
                    && WorkflowTaskParameter.EnumerateRelatedParameters()
                        .Any(p => p.ParameterDetails.ParameterType == TaskParameterType.PerformanceTimelinesSelection))
            )
            {
                CommonHelper.GetWindowService().ShowWaitCursor();

                //check the connection is available
                GFSModel.Entity gfsEntity = ParentWorkflow.TaskParameterDataCache.GetGfsModelEntity();

                CommonHelper.GetWindowService().HideWaitCursor();

                if (gfsEntity == null)
                {
                    //show a message and return
                    CommonHelper.GetWindowService().ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Error,
                        Message.EngineTasks_PerformanceSelection_NoGFSConnectionError,
                        Message.EngineTasks_PerformanceSelection_NoGFSConnectionError_Desc);
                    return currentValues.ToList();
                }
            }


            var win = new GenericSingleItemSelectorWindow();
            win.ItemSource = base.ParentWorkflow.TaskParameterDataCache.GetPerformanceSelectionInfos();
            win.SelectionMode = DataGridSelectionMode.Single;
            if (parentWindow != null) win.Owner = parentWindow;
            CommonHelper.GetWindowService().ShowDialog<GenericSingleItemSelectorWindow>(win);

            if (win.DialogResult == true)
            {
                if (win.SelectedItems != null)
                {
                    return new List<ITaskParameterValue>
                    { 
                      new TaskParameterValue( win.SelectedItems.Cast<PerformanceSelectionInfo>().First().Name)
                    };
                }
            }
            return currentValues.ToList();
        }


        public override void OnValueChanged()
        {
            base.OnValueChanged();

            //Reset dependant properties to be this performance selections timeline exclusions
            if (WorkpackagePlanogramParameter != null)
            {
                foreach (WorkpackagePlanogramParameter param in WorkpackagePlanogramParameter.EnumerateRelatedParameters(WorkflowTaskParameter))
                {
                    if (param.ParameterDetails.ParameterType == TaskParameterType.PerformanceTimelinesSelection)
                    {
                        List<ITaskParameterValue> newParameterValues = GetLinkedPerformanceSelectionParameterValues(WorkpackagePlanogramParameter.Value);

                        param.Values.Clear();
                        if (newParameterValues != null)
                            param.AddValues(newParameterValues.Select(p => WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(p)), true);
                    }
                    else
                    {
                        param.Value = param.ParameterDetails.DefaultValue;
                    }
                }
            }
            else if (WorkflowTaskParameter != null)
            {
                foreach (WorkflowTaskParameter param in WorkflowTaskParameter.EnumerateRelatedParameters())
                {
                    if (param.ParameterDetails.ParameterType == TaskParameterType.PerformanceTimelinesSelection)
                    {
                        List<ITaskParameterValue> newParameterValues = GetLinkedPerformanceSelectionParameterValues(WorkflowTaskParameter.Value);

                        param.Values.Clear();
                        if (newParameterValues != null)
                            param.AddValues(newParameterValues.Select(WorkflowTaskParameterValue.NewWorkflowTaskParameterValue), true);
                    }
                    else
                    {
                        param.Value = param.ParameterDetails.DefaultValue;
                    }
                }
            }
        }

        private List<ITaskParameterValue> GetLinkedPerformanceSelectionParameterValues(Object linkedPerformanceSelectionValue)
        {
            List<ITaskParameterValue> newParameterValues;
            Model.Workflow.DataCache cache = ParentWorkflow.TaskParameterDataCache;

            PerformanceSelectionInfo performanceSelectionInfo;
            if (!cache.TryGetPerformanceSelectionInfo(Convert.ToString(linkedPerformanceSelectionValue), out performanceSelectionInfo)) return null;

            GFSModel.TimelineGroupList allTimelineGroups = cache.GetPerformanceSourceTimelineGroupsByPerformanceSelection(performanceSelectionInfo);
            GFSModel.PerformanceSource performanceSource = cache.GetPerformanceSourceById(performanceSelectionInfo.GFSPerformanceSourceId);
            PerformanceSelection perfSelection = cache.GetPerformanceSelectionById(performanceSelectionInfo.Id);

            if (perfSelection.SelectionType == PerformanceSelectionType.Fixed)
            {
                //Return inverted selections - to form the exclusions
                newParameterValues =
                    allTimelineGroups.Select(p => p.Code)
                                     .Except(perfSelection.TimelineGroups.Select(p => p.Code))
                                     .Select(p => new TaskParameterValue(p))
                                     .Cast<ITaskParameterValue>()
                                     .ToList();
            }
            else
            {
                //Calculate the timelines included from the dynamic performance selection weeks
                GFSModel.Entity gfsEntity = cache.GetGfsModelEntity();
                if (gfsEntity != null)
                {
                    IEnumerable<Int64> specifiedDynamicTimelineCodes = perfSelection.GetTimelineCodesDynamic(gfsEntity, performanceSource);

                    //Form the exclusions based on entire timeline groups, 
                    //minus these date period inclusions & add extra exclusions that there may be
                    IEnumerable<Int64> perfectionSelectionTimelineExclusions = perfSelection.TimelineGroups.Select(p => p.Code);

                    //Return selections - to form the exclusions
                    //Specified dynamic timelines EXCEPT extra performance selection exclusions, removed from all available timelines 
                    IEnumerable<Int64> dynamicTimelineCodes = specifiedDynamicTimelineCodes as IList<Int64> ?? specifiedDynamicTimelineCodes.ToList();
                    IEnumerable<Int64> excludedTimelines = dynamicTimelineCodes.Except(dynamicTimelineCodes.Except(perfectionSelectionTimelineExclusions));
                    newParameterValues = excludedTimelines.Select(p => new TaskParameterValue(p)).Cast<ITaskParameterValue>().ToList();
                }
                else
                {
                    newParameterValues = new List<ITaskParameterValue>();
                }
            }

            return newParameterValues;

        }
    }
}
