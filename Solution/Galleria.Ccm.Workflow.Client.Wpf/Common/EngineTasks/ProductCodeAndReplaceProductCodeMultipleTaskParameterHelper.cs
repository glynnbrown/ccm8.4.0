﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.3)
// V8-29723 : D.Pleasance
//  Created.
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Data;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class ProductCodeAndReplaceProductCodeMultipleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ProductCodeAndReplaceProductCodeMultipleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_ProductCodeAndReplaceProductCodeMultipleColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            List<Tuple<String, String>> replaceGtins = currentValues.Select(item =>
                        new Tuple<String, String>(item.Value1.ToString(), item.Value2.ToString())).
                    ToList();
            
            var viewmodel = new ReplaceProductSelectorViewModel(replaceGtins);
            viewmodel.ShowDialog(parentWindow);

            if (viewmodel.DialogResult == true)
            {
                return viewmodel.ReplaceProductRows.Select(p => new TaskParameterValue(p.ProductGtin, p.ReplacementProductGtin)).Cast<ITaskParameterValue>().ToList();
            }

            return currentValues.ToList();
        }
    }
}