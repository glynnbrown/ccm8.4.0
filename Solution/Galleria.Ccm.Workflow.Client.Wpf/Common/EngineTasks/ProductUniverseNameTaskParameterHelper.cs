﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26950 : A.Kuszyk
//  Created.
// V8-27426 : L.Ineson
//  Changed to use ITaskParameterValue
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM 820
// V8-30730 : A.Kuszyk
//   Fixed issues with Validate method.
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
// V8-30840 : A.Kuszyk
//  Updated to use Product Universe selection window.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Windows.Controls;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Data;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class ProductUniverseNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ProductUniverseNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_ProductUniverseNameColumn"; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String error = null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }
            else
            {
                var universes = base.ParentWorkflow.TaskParameterDataCache.GetProductUniverseInfos();
                if (!universes.Any(univ => univ.Name == (String)val))
                {
                    error = Message.EngineTasks_ProductUniverse_InvalidError;
                }

            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            var win = new ProductUniverseSelectionWindow();
            WindowHelper.ShowWindow(win,parentWindow, true);

            if (win.SelectedProductUniverse != null)
            {
                return new List<ITaskParameterValue>
                { 
                    new TaskParameterValue(win.SelectedProductUniverse.Name)
                };
            }

            return currentValues.ToList();
        }
    }
}
