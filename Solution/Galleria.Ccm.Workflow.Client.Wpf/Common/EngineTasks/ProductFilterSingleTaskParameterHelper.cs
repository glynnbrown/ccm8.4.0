﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History : CCM 830
// V8-31559 : A.Kuszyk
//  Created.
// V8-32086 : N.Haywood
//  Added category code
// V8-31824 : A.Kuszyk
//  Changed to use ProductFilterSingle data template and added ProductFilterValueConverter.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Engine;
using System.Windows;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using System.ComponentModel;
using System.Windows.Data;
using System.Reflection;
using System.Collections;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class ProductFilterSingleTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_ProductFilterSingleTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_ProductFilterSingleColumn"; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            String error = null;

            //nb - ignore the isNullAllowed flag for this type.
            // If no filters are provided then the engine will select all products.

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            ProductFilterSelectorViewModel viewmodel = new ProductFilterSelectorViewModel(
                PlanogramProduct.EnumerateDisplayableFieldInfos(includeMetadata: true, includeCustom: true, includePerformanceData: true).ToList(), 
                allowMultiple: false, 
                existingFilters: currentValues);
            viewmodel.ShowDialog(parentWindow);

            if (viewmodel.DialogResult == true)
            {
                List<ITaskParameterValue> parameterValues = new List<ITaskParameterValue>();
                foreach (ProductFilterValue filter in viewmodel.Filters)
                {
                    parameterValues.Add(filter.CreateTaskParameterValue());
                }
                return parameterValues;
            }

            return currentValues.ToList();
        }
    }

    /// <summary>
    /// A converter for use with the ProductFilterSingle data templates.
    /// </summary>
    public class ProductFilterValueConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            ITaskParameterValue taskParameterValue = value as ITaskParameterValue;
            if (taskParameterValue == null) return null;

            // Get field text.
            String fieldText = GetFieldText(taskParameterValue);

            // Get the operand text.
            TaskParameterFilterOperandType operand;
            if(!Enum.TryParse<TaskParameterFilterOperandType>(taskParameterValue.Value2.ToString(), out operand)) return null;
            String operandText = TaskParameterFilterOperandTypeHelper.ShortFriendlyNames[operand];

            // Get the value text.
            Boolean isColour = fieldText.Equals(PlanogramProduct.FillColourProperty.FriendlyName);
            String valueText = GetValueText(taskParameterValue, isColour);

            return String.Format("{0} {1} {2}", fieldText, operandText, valueText);
        }

        private String GetFieldText(ITaskParameterValue taskParameterValue)
        {
            ObjectFieldInfo matchingField = PlanogramProduct.EnumerateDisplayableFieldInfos(true, true, true)
                .FirstOrDefault(o => o.FieldPlaceholder.Equals(taskParameterValue.Value1));

            if (matchingField == null)
            {
                return System.Convert.ToString(taskParameterValue.Value1);
            }

            return matchingField.PropertyFriendlyName;
        }

        private static String GetValueText(ITaskParameterValue taskParameterValue, Boolean isColour)
        {
            String valueText;
            if (taskParameterValue.Value3 == null)
            {
                valueText = String.Empty;
            }
            else if (isColour)
            {
                try
                {
                    Int32 colour = System.Convert.ToInt32(taskParameterValue.Value3);
                    valueText = ColorPickerGallery.GetSelectedColorName(ColorHelper.IntToColor(colour));
                }
                catch (FormatException)
                {
                    valueText = System.Convert.ToString(taskParameterValue.Value3);
                }
                catch (InvalidCastException)
                {
                    valueText = System.Convert.ToString(taskParameterValue.Value3);
                }
            }
            else if (taskParameterValue.Value3 is DateTime)
            {
                valueText = ((DateTime)taskParameterValue.Value3).ToShortDateString();
            }
            else if (taskParameterValue.Value3.GetType().IsEnum)
            {
                Type enumType = taskParameterValue.Value3.GetType();
                String enumText = System.Convert.ToString(taskParameterValue.Value3);
                Object enumValue;
                try
                {
                    enumValue = Enum.Parse(enumType, enumText, ignoreCase: true);
                }
                catch (ArgumentException)
                {
                    enumValue = null;
                }
                catch (OverflowException)
                {
                    enumValue = null;
                }
                Type enumHelperType = Assembly.GetAssembly(enumType).GetType(String.Format("{0}Helper", enumType.FullName), throwOnError: false);
                if (enumHelperType == null)
                {
                    valueText = enumText;
                }
                else
                {
                    FieldInfo friendlyNamesField = enumHelperType.GetField("FriendlyNames", BindingFlags.Static | BindingFlags.Public);
                    if (friendlyNamesField == null)
                    {
                        valueText = enumText;
                    }
                    else
                    {
                        IDictionary friendlyNamesDictionary = friendlyNamesField.GetValue(null) as IDictionary;
                        if (friendlyNamesDictionary == null)
                        {
                            valueText = enumText;
                        }
                        else
                        {
                            if (friendlyNamesDictionary.Contains(enumValue))
                            {
                                String friendlyName = friendlyNamesDictionary[enumValue] as String;
                                if (friendlyName == null)
                                {
                                    valueText = enumText;
                                }
                                else
                                {
                                    valueText = friendlyName;
                                }
                            }
                            else
                            {
                                valueText = enumText;
                            }
                        }
                    }
                }
            }
            else
            {
                valueText = System.Convert.ToString(taskParameterValue.Value3);
            }
            return valueText;
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new Object();
        }
    }
}
