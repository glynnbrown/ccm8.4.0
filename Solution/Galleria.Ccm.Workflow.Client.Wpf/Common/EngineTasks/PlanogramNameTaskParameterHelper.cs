﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 802
// V8-29003 : A.Kuszyk
//  Created.
#endregion
#region Version History: ?
// V8-29398 : M.Pettit
//  PlanogramName task parameter is multivalue; both name and id are stored
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
// V8-31174 : A.Kuszyk
//  Added call to tasks is null allowed method.
#endregion
#region Version History : CCM830
// V8-31584 : A.Kuszyk
//  Amended to use cached parameter data.
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Services;
using Galleria.Ccm.Common.Wpf.Services;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class PlanogramNameTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_PlanogramNameTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_PlanogramNameColumn"; }
        }

        /// <summary>
        /// Returns true if this is a multi value parameter type
        /// Value 1 is the planogram name, value2 is the planogram_id
        /// </summary>
        public override Boolean IsMultiValue
        {
            get { return true; }
        }

        /// <summary>
        /// Returns true if the parameter item requires a content lookup
        /// </summary>
        public override bool IsContentLink
        {
            get { return true; }
        }

        /// <summary>
        /// Returns true if the parameter is allowed to be null
        /// </summary>
        public override bool IsNullAllowed
        {
            get
            {
                if (WorkflowTaskParameter == null) return false;
                if (WorkpackagePlanogramParameter != null) return WorkpackagePlanogramParameter.IsNullAllowed(WorkflowTaskParameter);
                return WorkflowTaskParameter.IsNullAllowed();
            }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            ITaskParameterValue val = (values.Any()) ? values.FirstOrDefault() : null;
            String error = null;

            if (val == null)
            {
                if (!isNullAllowed)
                {
                    error = Message.EngineTasks_NoValueError;
                }
            }
            else
            {
                PlanogramInfo matchingNames = base.ParentWorkflow.TaskParameterDataCache.GetPlanogramInfo(
                    Convert.ToInt32(val.Value2), GetWorkpackageSourcePlanogramIds());

                if (matchingNames == null)
                {
                    error = Message.EngineTasks_Planogram_InvalidError;
                }

            }

            return error;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            PlanogramSelectorViewModel viewModel = new PlanogramSelectorViewModel(
                App.ViewState.CustomColumnLayoutFolderPath, App.ViewState.EntityId);
            
            ServiceContainer.GetService<IWindowService>().ShowDialog<PlanogramSelectorWindow>(viewModel);

            if (viewModel.DialogResult == true)
            {
                if (viewModel.SelectedPlanogramInfo!= null)
                {
                    return new List<ITaskParameterValue> {
                        new TaskParameterValue(viewModel.SelectedPlanogramInfo.Name, viewModel.SelectedPlanogramInfo.Id) };
                }
            }

            return currentValues.ToList();
        }
    }
}
