﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-28066 : A.Kuszyk
//  Created.
// V8-28233 : A.Kuszyk 
//  Added column template for percentages.
// V8-28279 : A.Kuszyk
//  Remove percentage string validation, because value is now a Single.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29491 : D.Pleasance
//  Amended Validate method so that 0 is a valid option.
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  Introduced Initialize and IsNullAllowed
// V8-30486 : M.Pettit
//  Added overloaded Initialize, OnValueChanged methods
#endregion
#region Version History : CCM 820
// V8-30774 : A.Kuszyk
//  Re-factored to use base class.
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using System.Windows;
using Galleria.Ccm.Engine;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public sealed class PercentageTaskParameterHelper : TaskParameterHelperBase
    {
        /// <summary>
        /// The name of the data template to use for the workflow maintenance screen control.
        /// </summary>
        protected override string DataTemplateName
        {
            get { return "TaskTemplates_PercentageTemplate"; }
        }

        /// <summary>
        /// The name of the data template to use for the workpackage wizard BAG control.
        /// </summary>
        protected override string DataTemplateColumnName
        {
            get { return "TaskTemplates_PercentageColumn"; }
        }

        /// <summary>
        /// Validates the given values and returns a friendly error message as required.
        /// </summary>
        protected override String OnValidate(IEnumerable<ITaskParameterValue> values, Boolean isNullAllowed)
        {
            Object val = (values.Any()) ? values.FirstOrDefault().Value1 : null;
            String percentageString = Convert.ToString(val);

            // Check for a null or empty string
            if (!isNullAllowed && String.IsNullOrEmpty(percentageString))
            {
                return Message.EngineTasks_NoValueError;
            }
            if (percentageString == null) return null;

            // Try converting to single.
            Single percentageAsSingle;
            if(!Single.TryParse(percentageString, out percentageAsSingle))
            {
                return Message.EngineTasks_PercentageValueWasNotAValidNumber;
            }

            // Finally, check that percentage value is between 0 and 1.
            if (percentageAsSingle.LessThan(0,4) || percentageAsSingle.GreaterThan(1,4))
            {
                return Message.EngineTasks_PercentageMustBeBetween0And100;
            }

            return null;
        }

        /// <summary>
        /// Shows the value selector for the parameter type.
        /// </summary>
        public override List<ITaskParameterValue> SelectValues(IEnumerable<ITaskParameterValue> currentValues, Window parentWindow, String categoryCode)
        {
            //do nothing - invalid.
            throw new ArgumentOutOfRangeException();
        }
    }
}
