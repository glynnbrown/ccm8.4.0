﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25460 : L.Ineson 
//  Created.
// V8-26950 : A.Kuszyk
//  Added ConsumerDecisionTree and Blocking parameter types.
// V8-27269 : A.Kuszyk
//  Added ValidationTemplate paramter.
// V8-27426 : L.Ineson
//  Added ProductFilterMultiple helper
// V8-27562 : A.Silva
//      Added RenumberingStrategyNameSingle
// V8-24779 : D.Pleasance
//      Added FileNameAndPath
// V8-27949 : A.Kuszyk
//  Added MetricProfileName and InventoryProfileName.
// V8-27783 : M.Brumby
//  Added GfsProjectName
// V8-28066 : A.Kuszyk
//  Added Percentage.
#endregion
#region Version History: CCM801
// V8-27494 : A.Kuszyk
//  Added Product Attribute Multiple.
#endregion
#region Version History: CCM802
// V8-29003 : A.Kuszyk
//  Added PlanogramBlockingName and PlanogramSequenceName.
// V8-29078 : A.Kuszyk
//  Added CategoryCodeSingle and LocationCodeSingle.
#endregion
#region Version History: CCM803
// V8-29633 : D.Pleasance
//  Added ClusterSchemeName.
// V8-29643 : D.Pleasance
//  Added PlanogramAttributeMultiple
// V8-29723 : D.Pleasance
//  Added ProductCodeAndReplaceProductCodeMultiple
#endregion
#region Version History: CCM811
// V8-30482 : A.Probyn
//  Added defensive code to overloaded GetHelper where the overloaded method with 1 parameter being called would instantly
//  of thrown a null exception. Musn't have been tested.
// V8-30482 : J.Pickup
//  Made changes to now get the helper and initialize it, and to also get it without initializing. 
// V8-30486 : M.Pettit
//  Added missing SequenceNameSingle, MinorAssortmentNameSingle parameter types in GetHelper() base method
#endregion
#region Version History: CCM820
// V8-30728 : A.Kuszyk
//   Added HighlightName option.
// V8-30733 : L.Luong
//   Added LocationProductAttributeMultiple option
// V8-30761 : A.Kuszyk
//  Added Int32.
#endregion
#region Version History: CCM830
// V8-31560 : D.Pleasance
//   Added DecreaseProductMultiple.
// V8-31559 : A.Kuszyk
//  Added ProductFilterSingle.
// V8-31830 : A.Probyn
//  Added PerformanceTimelinesSelection
// V8-31831 : A.Probyn
//  Added PlanogramNameTemplateNameSingle
// V8-31944 : A.Silva
//  Added PlanogramComparisonTemplateNameSingle.
// V8-32359 : A.Silva
//  Added ProductAttributeUpdateValueMultiple to the TaskParameterHelper factory.
//  Refactored GetHelper code to remove some duplication.

#endregion
#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion
#endregion

using System.Diagnostics;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks
{
    public static class TaskParameterHelper
    {
        /// <summary>
        /// Gets the helper without initializing the helper. 
        /// </summary>
        /// <param name="parameterType"></param>
        /// <returns>ITaskParameterHelper instance without initiazing it.</returns>
        public static ITaskParameterHelper GetHelper(TaskParameterType parameterType)
        {
            return GetHelper(null, parameterType, null);
        }

        /// <summary>
        /// Gets the helper and initializes it.
        /// </summary>
        /// <remarks>This is the preffered overload</remarks>
        /// <param name="parameter"></param>
        /// <returns>Initialized ITaskParameterHelper.</returns>
        public static ITaskParameterHelper GetHelper(WorkflowTaskParameter parameter)
        {
            return GetHelper(parameter, parameter.ParameterDetails.ParameterType, null);
        }

        /// <summary>
        /// Gets the helper and initializes it.
        /// </summary>
        /// <remarks>This is the preffered overload</remarks>
        /// <param name="parameter"></param>
        /// <param name="workpackageParameter"></param>
        /// <returns>Initialized ITaskParameterHelper.</returns>
        public static ITaskParameterHelper GetHelper(WorkflowTaskParameter parameter, WorkpackagePlanogramParameter workpackageParameter)
        {
            return GetHelper(parameter, parameter.ParameterDetails.ParameterType, workpackageParameter);
        }

        /// <summary>
        /// Gets the helper and initializes it.
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="parameterType"></param>
        /// <param name="workpackageParameter"></param>
        /// <returns>>Initialized ITaskParameterHelper.</returns>
        private static ITaskParameterHelper GetHelper(WorkflowTaskParameter parameter, TaskParameterType parameterType, WorkpackagePlanogramParameter workpackageParameter)
        {
            ITaskParameterHelper parameterHelper;
            switch (parameterType)
            {
                default:
                    Debug.Fail("TODO");
                    parameterHelper = new StringValueTaskParameterHelper();
                    break;
                case TaskParameterType.String:
                case TaskParameterType.Unknown:
                    parameterHelper = new StringValueTaskParameterHelper();
                    break;
                case TaskParameterType.Boolean:
                    parameterHelper = new BooleanValueTaskParameterHelper();
                    break;
                case TaskParameterType.Byte:
                    parameterHelper = new ByteValueTaskParameterHelper();
                    break;
                case TaskParameterType.Int32:
                    parameterHelper = new Int32ValueTaskParameterHelper();
                    break;
                case TaskParameterType.Enum:
                    parameterHelper = new EnumValueTaskParameterHelper();
                    break;
                case TaskParameterType.ProductCodeSingle:
                    parameterHelper = new ProductCodeTaskParameterHelper();
                    break;
                case TaskParameterType.ProductCodeMultiple:
                    parameterHelper = new ProductCodeMultipleTaskParameterHelper();
                    break;
                case TaskParameterType.AssortmentNameSingle:
                    parameterHelper = new AssortmentNameTaskParameterHelper();
                    break;
                case TaskParameterType.PerformanceSelectionNameSingle:
                    parameterHelper = new PerformanceSelectionNameTaskParameterHelper();
                    break;
                case TaskParameterType.ProductUniverseNameSingle:
                    parameterHelper = new ProductUniverseNameTaskParameterHelper();
                    break;
                case TaskParameterType.ConsumerDecisionTreeNameSingle:
                    parameterHelper = new ConsumerDecisionTreeNameTaskParameterHelper();
                    break;
                case TaskParameterType.BlockingNameSingle:
                    parameterHelper = new BlockingNameTaskParameterHelper();
                    break;
                case TaskParameterType.ValidationTemplateSingle:
                    parameterHelper = new ValidationTemplateNameTaskParameterHelper();
                    break;
                case TaskParameterType.ProductFilterMultiple:
                    parameterHelper = new ProductFilterMultipleTaskParameterHelper();
                    break;
                case TaskParameterType.RenumberingStrategyNameSingle:
                    parameterHelper = new RenumberingStrategyNameSingleTaskParameterHelper();
                    break;
                case TaskParameterType.FileNameAndPath:
                    parameterHelper = new FileNameAndPathTaskParameterHelper();
                    break;
                case TaskParameterType.MetricProfileNameSingle:
                    parameterHelper = new MetricProfileNameTaskParameterHelper();
                    break;
                case TaskParameterType.InventoryProfileNameSingle:
                    parameterHelper = new InventoryProfileNameTaskParameterHelper();
                    break;
                case TaskParameterType.SequenceNameSingle:
                    parameterHelper = new StringValueTaskParameterHelper();
                    break;
                case TaskParameterType.MinorAssortmentNameSingle:
                    parameterHelper = new StringValueTaskParameterHelper();
                    break;
                case TaskParameterType.PlanogramImportTemplateNameSingle:
                    parameterHelper = new PlanogramImportTemplateNameTaskParameterHelper();
                    break;
                case TaskParameterType.GfsProjectName:
                    parameterHelper = new GfsProjectNameTaskParameterHelper();
                    break;
                case TaskParameterType.Percentage:
                    parameterHelper = new PercentageTaskParameterHelper();
                    break;
                case TaskParameterType.ProductAttributeMultiple:
                    parameterHelper = new ProductAttributeMultipleTaskParameterHelper();
                    break;
                case TaskParameterType.PlanogramBlockingNameSingle:
                case TaskParameterType.PlanogramSequenceNameSingle:
                    parameterHelper = new PlanogramNameTaskParameterHelper();
                    break;
                case TaskParameterType.CategoryCodeSingle:
                    parameterHelper = new CategoryCodeTaskParameterHelper();
                    break;
                case TaskParameterType.LocationCodeSingle:
                    parameterHelper = new LocationCodeTaskParameterHelper();
                    break;
                case TaskParameterType.ClusterSchemeNameSingle:
                    parameterHelper = new ClusterSchemeNameTaskParameterHelper();
                    break;
                case TaskParameterType.ClusterNameSingle:
                    parameterHelper = new ClusterNameTaskParameterHelper();
                    break;
                case TaskParameterType.PlanogramAttributeMultiple:
                    parameterHelper = new PlanogramAttributeMultipleTaskParameterHelper();
                    break;
                case TaskParameterType.ProductCodeAndReplaceProductCodeMultiple:
                    parameterHelper = new ProductCodeAndReplaceProductCodeMultipleTaskParameterHelper();
                    break;
                case TaskParameterType.HighlightNameSingle:
                    parameterHelper = new HighlightNameTaskParameterHelper();
                    break;
                case TaskParameterType.LocationProductAttributeMultiple:
                    parameterHelper = new LocationProductAttributeMultipleTaskParameterHelper();
                    break;
                case TaskParameterType.DecreaseProductMultiple:
                    parameterHelper = new DecreaseProductMultipleTaskParameterHelper();
                    break;
                case TaskParameterType.ProductFilterSingle:
                    parameterHelper = new ProductFilterSingleTaskParameterHelper();
                    break;
                case TaskParameterType.PerformanceTimelinesSelection:
                    parameterHelper = new PerformanceTimelineSelectionTaskParameterHelper();
                    break;
                case TaskParameterType.PlanogramNameTemplateNameSingle:
                    parameterHelper = new PlanogramNameTemplateNameTaskParameterHelper();
                    break;
                case TaskParameterType.PlanogramComparisonTemplateNameSingle:
                    parameterHelper = new PlanogramComparisonTemplateNameTaskParameterHelper();
                    break;
                case TaskParameterType.ProductAttributeUpdateValueMultiple:
                    parameterHelper = new ProductAttributeUpdateValueMultipleTaskParameterHelper();
                    break;
                case TaskParameterType.LocationSpaceElementAttributeUpdateValueMultiple:
                    parameterHelper = new LocationSpaceElementAttributeUpdateValueMultipleTaskParameterHelper();
                    break;
            }

            parameterHelper.Initialize(parameter, workpackageParameter);
            return parameterHelper;
        }
    }
}