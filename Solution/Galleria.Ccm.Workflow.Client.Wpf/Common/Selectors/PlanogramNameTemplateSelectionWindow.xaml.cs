﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 802)
// V8-29026 : D.Pleasance
//      Created 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// Interaction logic for PlanogramNameTemplateSelectionWindow.xaml
    /// </summary>
    public partial class PlanogramNameTemplateSelectionWindow : ExtendedRibbonWindow
    {
        #region Fields

        private readonly PlanogramNameTemplateInfoListViewModel _planogramNameTemplateInfoListViewModel = new PlanogramNameTemplateInfoListViewModel();
        private PlanogramNameTemplateInfo _selectionResult;

        #endregion

        #region Properties

        public PlanogramNameTemplateInfo SelectionResult
        {
            get { return _selectionResult; }
            set
            {
                _selectionResult = value;
            }
        }

        public ReadOnlyBulkObservableCollection<PlanogramNameTemplateInfo> AvailablePlanogramNameTemplates
        {
            get { return _planogramNameTemplateInfoListViewModel.BindableCollection; }
        }

        #endregion

        public PlanogramNameTemplateSelectionWindow()
        {
            _planogramNameTemplateInfoListViewModel.FetchAllForEntity();

            InitializeComponent();
        }

        #region EventHandlers

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            //Set selected planogram name template
            this.SelectionResult = this.availablePlanogramNameTemplates.SelectedItem as PlanogramNameTemplateInfo;
            this.Close();
        }

        private void availablePlanogramNameTemplates_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            //Set selected planogram name template
            this.SelectionResult = this.availablePlanogramNameTemplates.SelectedItem as PlanogramNameTemplateInfo;
            this.Close();
        }

        /// <summary>
        /// Close button event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Allows selection when the Return key is pressed for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void availablePlanogramNameTemplates_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && availablePlanogramNameTemplates.SelectedItem != null)
            {
                e.Handled = true;
                //Set selected planogram name template
                this.SelectionResult = this.availablePlanogramNameTemplates.SelectedItem as PlanogramNameTemplateInfo;
                this.Close();
            }
            else
            {
                return;
            }
        }

        #endregion
    }
}