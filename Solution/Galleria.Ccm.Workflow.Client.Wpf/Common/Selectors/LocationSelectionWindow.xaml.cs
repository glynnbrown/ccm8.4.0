﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors
{
    /// <summary>
    /// Interaction logic for LocationSelectionWindow.xaml
    /// </summary>
    public partial class LocationSelectionWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationSelectionViewModel), typeof(LocationSelectionWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationSelectionViewModel ViewModel
        {
            get { return (LocationSelectionViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSelectionWindow senderControl = (LocationSelectionWindow)obj;

            if (e.OldValue != null)
            {
                LocationSelectionViewModel oldModel = (LocationSelectionViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationSelectionViewModel newModel = (LocationSelectionViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        [Obsolete("Use selector in common instead")]
        public LocationSelectionWindow(ObservableCollection<LocationInfo> availableLocations, ObservableCollection<LocationInfo> takenLocations, LocationHierarchy locationHierarchy)
        {
            InitializeComponent();

            //Create view model
            this.ViewModel = new LocationSelectionViewModel(availableLocations, takenLocations, locationHierarchy);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Performs a RemoveLocationsCommand for rows dropped on the available locations grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.xSelectedLocationsGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveLocationsCommand.Execute();
                }
            }
        }

        /// <summary>
        /// Performs an AddLocationsCommand for rows dropped selected locations grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSelectedLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.xAvailableLocationsGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.AddLocationsCommand.Execute();
                }
            }
        }

        #endregion

        #region Methods

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            //Close window
            this.DialogResult = false;
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            //Close window
            this.DialogResult = true;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #endregion

        private void LocationGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAssigned = senderControl?.Name == xSelectedLocationsGrid.Name;

            if (e.Key == Key.Return)
            {
                if (!isAssigned) this.ViewModel?.AddLocationsCommand.Execute();
                else this.ViewModel?.RemoveLocationsCommand.Execute();
                Keyboard.Focus(isAssigned ? xSelectedLocationsGrid : xAvailableLocationsGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAssigned ? xAvailableLocationsGrid : xSelectedLocationsGrid);
                e.Handled = true;
            }
        }
    }
}
