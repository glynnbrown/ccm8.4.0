﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM 800)
// GFS-25454 : J.Pickup
//      Created copied over from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// Interaction logic for ConsumerDecisionTreeWindow.xaml
    /// </summary>
    public partial class ConsumerDecisionTreeWindow : ExtendedRibbonWindow
    {
        #region Fields

        private ConsumerDecisionTreeInfo _selectionResult;
        private ObservableCollection<ConsumerDecisionTreeInfo> _availableConsumerDecisionTrees;

        #endregion

        #region Properties

        public ConsumerDecisionTreeInfo SelectionResult
        {
            get { return _selectionResult; }
            set
            {
                _selectionResult = value;
            }
        }

        public ObservableCollection<ConsumerDecisionTreeInfo> AvailableConsumerDecisionTrees
        {
            get { return _availableConsumerDecisionTrees; }
            set
            {
                _availableConsumerDecisionTrees = value;
            }
        }

        #endregion

        public ConsumerDecisionTreeWindow()
        {
            ConsumerDecisionTreeInfoList _masterList = ConsumerDecisionTreeInfoList.FetchByEntityId(App.ViewState.EntityId);
            _availableConsumerDecisionTrees = _masterList.ToObservableCollection<ConsumerDecisionTreeInfo>();

            InitializeComponent();
        }

        #region EventHandlers

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<ConsumerDecisionTreeInfo> selectedItems =
                        availableConsumerDecisionTrees.SelectedItems.Cast<ConsumerDecisionTreeInfo>();

            this.SelectionResult = selectedItems.FirstOrDefault();

            this.DialogResult = true;
        }

        #endregion

        #region window close

        /// <summary>
        /// Cancel view model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //set the result (will close the window)
            this.DialogResult = false;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }


        #endregion
    }
}