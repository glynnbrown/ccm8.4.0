﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Controls.Wpf;
using System.Globalization;
using Galleria.Ccm.Model;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.ComponentModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors
{
    public class LocationSelectionViewModel : ViewModelAttachedControlObject<LocationSelectionWindow>
    {
        #region Fields

        private ObservableCollection<LocationInfoRowViewModel> _availableLocations = new ObservableCollection<LocationInfoRowViewModel>();
        private ObservableCollection<LocationInfoRowViewModel> _selectedAvailableLocations = new ObservableCollection<LocationInfoRowViewModel>();
        private ObservableCollection<LocationInfoRowViewModel> _takenLocations = new ObservableCollection<LocationInfoRowViewModel>();
        private ObservableCollection<LocationInfoRowViewModel> _selectedTakenLocations = new ObservableCollection<LocationInfoRowViewModel>();
        private DataGridColumnCollection _availableLocationColumnSet = new DataGridColumnCollection();
        private DataGridColumnCollection _takenLocationColumnSet = new DataGridColumnCollection();

        #endregion

        #region Property Paths

        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<LocationSelectionViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedAvailableLocationsProperty = WpfHelper.GetPropertyPath<LocationSelectionViewModel>(p => p.SelectedAvailableLocations);
        public static readonly PropertyPath TakenLocationsProperty = WpfHelper.GetPropertyPath<LocationSelectionViewModel>(p => p.TakenLocations);
        public static readonly PropertyPath SelectedTakenLocationsProperty = WpfHelper.GetPropertyPath<LocationSelectionViewModel>(p => p.SelectedTakenLocations);
        public static readonly PropertyPath AddLocationsCommandProperty = WpfHelper.GetPropertyPath<LocationSelectionViewModel>(p => p.AddLocationsCommand);
        public static readonly PropertyPath RemoveLocationsCommandProperty = WpfHelper.GetPropertyPath<LocationSelectionViewModel>(p => p.RemoveLocationsCommand);
        public static readonly PropertyPath AvailableLocationColumnSetProperty = WpfHelper.GetPropertyPath<LocationSelectionViewModel>(p => p.AvailableLocationColumnSet);
        public static readonly PropertyPath TakenLocationColumnSetProperty = WpfHelper.GetPropertyPath<LocationSelectionViewModel>(p => p.TakenLocationColumnSet);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/sets the list of locations
        /// </summary>
        public ObservableCollection<LocationInfoRowViewModel> AvailableLocations
        {
            get { return _availableLocations; }
        }

        /// <summary>
        /// Gets/sets the selected available locations
        /// </summary>
        public ObservableCollection<LocationInfoRowViewModel> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
        }

        /// <summary>
        /// Gets/sets the taken locations
        /// </summary>
        public ObservableCollection<LocationInfoRowViewModel> TakenLocations
        {
            get { return _takenLocations; }
        }

        /// <summary>
        /// Gets/sets the selected taken locations
        /// </summary>
        public ObservableCollection<LocationInfoRowViewModel> SelectedTakenLocations
        {
            get { return _selectedTakenLocations; }
        }

        /// <summary>
        /// Returns the columnset to use to display the available locations
        /// </summary>
        public DataGridColumnCollection AvailableLocationColumnSet
        {
            get { return _availableLocationColumnSet; }
        }

        /// <summary>
        /// Returns the columnset to use to display the taken locations
        /// </summary>
        public DataGridColumnCollection TakenLocationColumnSet
        {
            get { return _takenLocationColumnSet; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public LocationSelectionViewModel(ObservableCollection<LocationInfo> availableLocations, ObservableCollection<LocationInfo> takenLocations, LocationHierarchy locationHierarchy)
        {
            //Build column set
            BuildLocationColumnSet(locationHierarchy);

            //Get all location hierarchy groups
            IEnumerable<LocationGroup> locationGroups = locationHierarchy.FetchAllGroups();

            //Set available locations
            foreach (LocationInfo location in availableLocations)
            {
                //Find group
                LocationGroup locationGroup = locationGroups.FirstOrDefault(p =>
                    p.AssignedLocations.Select(l => l.LocationId).Contains(location.Id));
                if (locationGroup != null)
                {
                    //Get parent path & assigned group
                    List<LocationGroup> parentPathGroups = locationGroup.GetParentPath();
                    parentPathGroups.Add(locationGroup);

                    //Add to collection
                    this.AvailableLocations.Add(new LocationInfoRowViewModel(location, parentPathGroups.Where(p => !p.IsRoot).Select(p => p.ToString()).ToList()));
                }
            }


            //Subscribe to the collection changed event for the selected locations
            this.TakenLocations.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(SelectedTakenLocations_CollectionChanged);

            //Add taken locations to the correct collection for pre existing selection
            foreach (LocationInfo location in takenLocations)
            {
                //Find group
                LocationGroup locationGroup = locationGroups.FirstOrDefault(p =>
                    p.AssignedLocations.Select(l => l.LocationId).Contains(location.Id));

                if (locationGroup != null)
                {
                    //Get parent path & assigned group
                    List<LocationGroup> parentPathGroups = locationGroup.GetParentPath();
                    parentPathGroups.Add(locationGroup);

                    //Add to collection
                    this.TakenLocations.Add(new LocationInfoRowViewModel(location, parentPathGroups.Where(p => !p.IsRoot).Select(p => p.ToString()).ToList()));
                }
            }
        }

        /// <summary>
        /// Event handler for the selected taken locations collection changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedTakenLocations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case (NotifyCollectionChangedAction.Add):
                    foreach (LocationInfoRowViewModel locationRow in e.NewItems)
                    {
                        //Find corresponding
                        LocationInfoRowViewModel rowToRemove = this.AvailableLocations.FirstOrDefault(p => p.Location.Id == locationRow.Location.Id);

                        if (rowToRemove != null)
                        {
                            this.AvailableLocations.Remove(rowToRemove);
                        }
                    }
                    break;
                case (NotifyCollectionChangedAction.Remove):
                    foreach (LocationInfoRowViewModel locationRow in e.OldItems)
                    {
                        this.AvailableLocations.Add(locationRow);
                    }
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to build the location column set based on the location heirarcy
        /// </summary>
        private void BuildLocationColumnSet(LocationHierarchy locationHierarchy)
        {
            //Clear existing columns if any exist
            if (this.AvailableLocationColumnSet.Count > 0 || this.TakenLocationColumnSet.Count > 0)
            {
                this.AvailableLocationColumnSet.Clear();
                this.TakenLocationColumnSet.Clear();
            }

            //Get levels
            List<LocationLevel> levels = locationHierarchy.FetchAllLevels().Where(p => !p.IsRoot).ToList();

            //Add Name & Code
            this.AvailableLocationColumnSet.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.CodeProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.CodeProperty.Name), HorizontalAlignment.Left));
            this.TakenLocationColumnSet.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.CodeProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.CodeProperty.Name), HorizontalAlignment.Left));
            this.AvailableLocationColumnSet.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.NameProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.NameProperty.Name), HorizontalAlignment.Left));
            this.TakenLocationColumnSet.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.NameProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.NameProperty.Name), HorizontalAlignment.Left));
            //Add level columns
            if (levels.Count >= 1)
            {
                this.AvailableLocationColumnSet.Add(
                    ExtendedDataGrid.CreateReadOnlyTextColumn
                    (levels[0].Name, String.Format(CultureInfo.InvariantCulture, "{0}[{1}]", LocationInfoRowViewModel.LocationGroupLevelValuesProperty.Path, 0), HorizontalAlignment.Left));
                this.TakenLocationColumnSet.Add(
                    ExtendedDataGrid.CreateReadOnlyTextColumn
                    (levels[0].Name, String.Format(CultureInfo.InvariantCulture, "{0}[{1}]", LocationInfoRowViewModel.LocationGroupLevelValuesProperty.Path, 0), HorizontalAlignment.Left));
            }

            if (levels.Count >= 2)
            {
                this.AvailableLocationColumnSet.Add(
                    ExtendedDataGrid.CreateReadOnlyTextColumn
                    (levels[1].Name, String.Format(CultureInfo.InvariantCulture, "{0}[{1}]", LocationInfoRowViewModel.LocationGroupLevelValuesProperty.Path, 1), HorizontalAlignment.Left));
                this.TakenLocationColumnSet.Add(
                    ExtendedDataGrid.CreateReadOnlyTextColumn
                    (levels[1].Name, String.Format(CultureInfo.InvariantCulture, "{0}[{1}]", LocationInfoRowViewModel.LocationGroupLevelValuesProperty.Path, 1), HorizontalAlignment.Left));
            }

            //Add other columns
            AddDefaultLocationColumns(AvailableLocationColumnSet);
            AddDefaultLocationColumns(TakenLocationColumnSet);
        }

        //Method to add default location columns
        private void AddDefaultLocationColumns(DataGridColumnCollection columnCollection)
        {
            columnCollection.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.Address1Property.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.Address1Property.Name), HorizontalAlignment.Left));
            columnCollection.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.Address2Property.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.Address2Property.Name), HorizontalAlignment.Left));
            columnCollection.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.CityProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.CityProperty.Name), HorizontalAlignment.Left));
            columnCollection.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.CountyProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.CountyProperty.Name), HorizontalAlignment.Left));
            columnCollection.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.PostalCodeProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.PostalCodeProperty.Name), HorizontalAlignment.Left));
            columnCollection.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn
                (Location.CountryProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.CountryProperty.Name), HorizontalAlignment.Left));
            columnCollection.Add(
                    ExtendedDataGrid.CreateReadOnlyTextColumn
                    (Location.RegionProperty.FriendlyName, String.Format(CultureInfo.InvariantCulture, "{0}.{1}", LocationInfoRowViewModel.LocationProperty.Path, Location.RegionProperty.Name), HorizontalAlignment.Left));
        }

        #endregion

        #region Commands

        #region Add Locations

        private RelayCommand _addLocationsCommand;
        /// <summary>
        /// Adds the current selected available locations to the taken list
        /// </summary>
        public RelayCommand AddLocationsCommand
        {
            get
            {
                if (_addLocationsCommand == null)
                {
                    _addLocationsCommand = new RelayCommand(p => AddLocations_Executed(), p => AddLocations_CanExecute())
                    {
                        FriendlyName = Message.LocationSelection_AddLocations,
                        FriendlyDescription = Message.LocationSelection_AddLocations_Tooltip,
                        Icon = ImageResources.MasterDataManagement_LocationSetup
                    };
                    this.ViewModelCommands.Add(_addLocationsCommand);
                }
                return _addLocationsCommand;
            }
        }

        private bool AddLocations_CanExecute()
        {
            return (this.SelectedAvailableLocations.Count > 0) ? true : false;
        }

        private void AddLocations_Executed()
        {
            if (AddLocations_CanExecute())
            {
                foreach (LocationInfoRowViewModel location in this.SelectedAvailableLocations.ToList())
                {
                    this.TakenLocations.Add(location);
                }
            }
        }
        #endregion

        #region Remove Locations

        private RelayCommand _removeLocationsCommand;
        /// <summary>
        /// Removes the current selected taken locations from the master list
        /// </summary>
        public RelayCommand RemoveLocationsCommand
        {
            get
            {
                if (_removeLocationsCommand == null)
                {
                    _removeLocationsCommand = new RelayCommand(p => RemoveLocations_Executed(), p => RemoveLocations_CanExecute())
                    {
                        FriendlyName = Message.LocationSelection_RemoveLocations,
                        FriendlyDescription = Message.LocationSelection_RemoveLocations_Tooltip,
                        Icon = ImageResources.Delete_32
                    };
                    this.ViewModelCommands.Add(_removeLocationsCommand);
                }
                return _removeLocationsCommand;
            }
        }

        private bool RemoveLocations_CanExecute()
        {
            return (this.SelectedTakenLocations.Count > 0) ? true : false;
        }

        private void RemoveLocations_Executed()
        {
            if (RemoveLocations_CanExecute())
            {
                foreach (LocationInfoRowViewModel location in this.SelectedTakenLocations.ToList())
                {
                    this.TakenLocations.Remove(location);
                }
            }
        }
        #endregion

        #endregion

        #region Event Handlers

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.SelectedTakenLocations.CollectionChanged -= SelectedTakenLocations_CollectionChanged;
                    this._selectedTakenLocations.Clear();
                    this._selectedAvailableLocations.Clear();
                    this._availableLocations.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class LocationInfoRowViewModel : INotifyPropertyChanged
    {
        #region Fields

        private LocationInfo _location;
        private List<String> _locationGroupLevelValues = new List<String>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath LocationProperty = WpfHelper.GetPropertyPath<LocationInfoRowViewModel>(p => p.Location);
        public static readonly PropertyPath LocationGroupLevelValuesProperty = WpfHelper.GetPropertyPath<LocationInfoRowViewModel>(p => p.LocationGroupLevelValues);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Location
        /// </summary>
        public LocationInfo Location
        {
            get { return _location; }
        }

        /// <summary>
        /// Gets the group names for each level
        /// </summary>
        public List<String> LocationGroupLevelValues
        {
            get { return _locationGroupLevelValues; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LocationInfoRowViewModel(LocationInfo location, List<String> locationGroupLevelValues)
        {
            //Set the properties
            _location = location;
            _locationGroupLevelValues = locationGroupLevelValues;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
        }


        #endregion
    }
}
