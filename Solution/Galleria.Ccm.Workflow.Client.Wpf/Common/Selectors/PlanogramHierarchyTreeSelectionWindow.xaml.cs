﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25460 : L.Ineson
//	Created
// V8-26284 : A.Kuszyk 
//  Amended to use Common.Wpf selector.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// Interaction logic for PlanogramHierarchyTreeSelectionWindow.xaml
    /// </summary>
    public sealed partial class PlanogramHierarchyTreeSelectionWindow : ExtendedRibbonWindow
    {
        private PlanogramGroup _selectedGroup;

        #region Properties

        public PlanogramGroup SelectedGroup
        {
            get { return _selectedGroup; }
            private set { _selectedGroup = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramHierarchyTreeSelectionWindow()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedGroupView = XTreeview.SelectedGroup;
            if(selectedGroupView != null)
            {
                SelectedGroup = selectedGroupView.PlanogramGroup;
            }
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #endregion
    }
}
