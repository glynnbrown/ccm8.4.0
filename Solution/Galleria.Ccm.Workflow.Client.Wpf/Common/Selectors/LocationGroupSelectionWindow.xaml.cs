﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors
{
    /// <summary>
    /// Interaction logic for LocationGroupSelectionWindow.xaml
    /// </summary>
    public sealed partial class LocationGroupSelectionWindow : ExtendedRibbonWindow
    {
        #region Fields
        private LocationHierarchy _locationHierarchy;
        private IEnumerable<Int32> _excludeIds;
        #endregion

        #region Properties

        #region SelectionModeProperty

        public static readonly DependencyProperty SelectionModeProperty =
           DependencyProperty.Register("SelectionMode", typeof(DataGridSelectionMode), typeof(LocationGroupSelectionWindow),
           new PropertyMetadata(DataGridSelectionMode.Extended));

        /// <summary>
        /// Gets/Sets the selection type
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return (DataGridSelectionMode)GetValue(SelectionModeProperty); }
            set { SetValue(SelectionModeProperty, value); }
        }

        #endregion

        #region SelectionResult property

        private List<LocationGroup> _selectionResult;
        /// <summary>
        /// Returns a readonly collection of the group/groups selected.
        /// </summary>
        public List<LocationGroup> SelectionResult
        {
            get { return _selectionResult; }
            private set { _selectionResult = value; }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationGroupSelectionWindow() : this(null, null, DataGridSelectionMode.Extended) { }

        public LocationGroupSelectionWindow(LocationHierarchy locationHierarchy, IEnumerable<Int32> excludeIds) :
            this(locationHierarchy, excludeIds, DataGridSelectionMode.Extended) { }

        public LocationGroupSelectionWindow(IEnumerable<Int32> excludeIds, DataGridSelectionMode selectionMode)
            : this(null, excludeIds, selectionMode) { }

        public LocationGroupSelectionWindow(LocationHierarchy locationHierarchy, IEnumerable<Int32> excludeIds, DataGridSelectionMode selectionMode)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            _locationHierarchy = locationHierarchy;
            if (_locationHierarchy == null)
            {
                _locationHierarchy = LocationHierarchy.FetchByEntityId(App.ViewState.EntityId);
            }

            _excludeIds = excludeIds;

            this.SelectionMode = selectionMode;

            if (App.Current != null)
            {
                InitializeComponent();
            }

            this.Loaded += new RoutedEventHandler(LocationGroupSelectionWindow_Loaded);
        }

        private void LocationGroupSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationGroupSelectionWindow_Loaded;

            FocusPrefilter();

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            ReloadColumns();

            //get all groups in the hierarchy
            LocationGroupViewModel rootUnitView = new LocationGroupViewModel(_locationHierarchy.RootGroup);

            List<LocationGroupViewModel> locGroupViews = rootUnitView.GetAllChildUnits().ToList();

            //remove any exclusions
            if (_excludeIds != null)
            {
                locGroupViews.RemoveAll(g => _excludeIds.Contains(g.LocationGroup.Id));
            }

            flattenedGrid.ItemsSourceExtended = locGroupViews;
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            OnSelectionComplete();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void flattenedGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.SelectionMode == DataGridSelectionMode.Single)
            {
                if (flattenedGrid.SelectedItems.Count > 0)
                {
                    OnSelectionComplete();
                }
            }
        }

        /// <summary>
        /// When enter is pressed return selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flattenedGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    if (flattenedGrid.SelectedItems.Count > 0)
                    {
                        e.Handled = true;
                        OnSelectionComplete();
                    }
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Carries out prefiltering when raised by the datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flattenedGrid_PrefilterItem(object sender, ExtendedDataGridFilterEventArgs e)
        {
            String prefilter = flattenedGrid.PrefilterText;
            if (!String.IsNullOrEmpty(prefilter))
            {
                LocationGroupViewModel row = e.Item as LocationGroupViewModel;
                if (row != null)
                {
                    if (row.LocationGroup == null)
                    {
                        e.Accepted = false;
                    }
                    else
                    {
                        String productGroupDesc = row.LocationGroup.ToString().ToLowerInvariant();
                        if (!productGroupDesc.Contains(prefilter.ToLowerInvariant()))
                        {
                            e.Accepted = false;
                        }
                    }
                }
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// Reloads level grid columns
        /// </summary>
        private void ReloadColumns()
        {
            if (flattenedGrid != null)
            {
                //clear out the existing columns
                if (flattenedGrid.Columns.Count > 0)
                {
                    flattenedGrid.Columns.Clear();
                }

                //add in the new columns
                List<DataGridColumn> columnSet = DataObjectViewHelper.GetLocationGroupColumnSet(_locationHierarchy, /*includeRootCol*/true);
                if (columnSet.Count > 0)
                {
                    columnSet.ForEach(c => flattenedGrid.Columns.Add(c));
                }

            }
        }


        private void OnSelectionComplete()
        {
            this.SelectionResult = flattenedGrid.SelectedItems.Cast<LocationGroupViewModel>().Select(s => s.LocationGroup).ToList();
            this.DialogResult = true;
        }

        /// <summary>
        /// Places focus into the prefilter search textbox
        /// </summary>
        private void FocusPrefilter()
        {
            if (this.flattenedGrid != null
                && this.flattenedGrid.Template != null)
            {
                Border prefilterBar = this.flattenedGrid.Template.FindName("PART_PrefilterBar", flattenedGrid) as Border;
                if (prefilterBar != null
                    && prefilterBar.Child != null)
                {
                    TextBox prefilterBox = prefilterBar.Child.FindVisualDescendent<TextBox>();
                    if (prefilterBox != null)
                    {
                        prefilterBox.Focus();
                    }
                }
            }
        }


        #endregion

    }
}
