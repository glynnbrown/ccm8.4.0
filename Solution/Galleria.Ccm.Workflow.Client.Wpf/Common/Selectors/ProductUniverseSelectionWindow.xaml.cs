﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// Interaction logic for ProductUniverseSelectionWindow.xaml
    /// </summary>
    public partial class ProductUniverseSelectionWindow : ExtendedRibbonWindow
    {
        #region Fields

        private ProductUniverseInfoListViewModel _productUniverseInfoView = new ProductUniverseInfoListViewModel();
        private ObservableCollection<ProductUniverseInfo> _searchResults = new ObservableCollection<ProductUniverseInfo>();
        private Int32? _currentProductGroupId;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the available product universes
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductUniverseInfo> AvailableProductUniverses
        {
            get
            {
                return this._productUniverseInfoView.BindingView;
            }
        }

        /// <summary>
        /// Returns the current product group id
        /// </summary>
        public Int32? CurrentProductGroupId
        {
            get
            {
                return _currentProductGroupId;
            }
        }

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<ProductUniverseInfo>),
            typeof(ProductUniverseSelectionWindow),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<ProductUniverseInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<ProductUniverseInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #region SelectedProductUniverseProperty

        public static readonly DependencyProperty SelectedProductUniverseProperty =
            DependencyProperty.Register("SelectedProductUniverse", typeof(ProductUniverseInfo),
            typeof(ProductUniverseSelectionWindow),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the selected product universe
        /// </summary>
        public ProductUniverseInfo SelectedProductUniverse
        {
            get { return (ProductUniverseInfo)GetValue(SelectedProductUniverseProperty); }
            private set { SetValue(SelectedProductUniverseProperty, value); }
        }

        #endregion

        #region FilterAvailableProperty

        public static readonly DependencyProperty FilterAvailableProperty =
            DependencyProperty.Register("FilterAvailable", typeof(Boolean), typeof(ProductUniverseSelectionWindow),
            new PropertyMetadata(true, OnFilterAvailablePropertyChanged));

        /// <summary>
        /// Gets/Sets whether to filter the universes by product group or not
        /// </summary>
        public Boolean FilterAvailable
        {
            get { return (Boolean)GetValue(FilterAvailableProperty); }
            set { SetValue(FilterAvailableProperty, value); }
        }

        private static void OnFilterAvailablePropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            //Create selection window
            ProductUniverseSelectionWindow senderControl = (ProductUniverseSelectionWindow)obj;

            //Clear filters
            senderControl.resultsGrid.ClearFilterValues();

            //Update search results
            senderControl.UpdateSearchResults();
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="cdtView"></param>
        public ProductUniverseSelectionWindow() : this(null, null) { }
        public ProductUniverseSelectionWindow(ProductUniverseInfoListViewModel productUniverseView, Int32? currentProductGroupId)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //Set current product group id
            _currentProductGroupId = currentProductGroupId;

            //If cdt is passed in
            if (productUniverseView != null)
            {
                _productUniverseInfoView = productUniverseView;
            }

            //Load if empty
            if (_productUniverseInfoView.Model == null)
            {
                _productUniverseInfoView.FetchAllForEntity();
            }

            //Update search results for first time.
            UpdateSearchResults();

            //Update search results
            this.SearchResults = new ReadOnlyObservableCollection<ProductUniverseInfo>(_searchResults);

            InitializeComponent();

            //Attach to the loaded event handler
            this.Loaded += new RoutedEventHandler(ProductUniverseSelectionWindow_Loaded);
        }

        /// <summary>
        /// Loaded event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductUniverseSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProductUniverseSelectionWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            //Clear search results
            _searchResults.Clear();

            //Get matching cdts
            IEnumerable<ProductUniverseInfo> results = this.GetMatchingProductUniverses();

            foreach (ProductUniverseInfo productUniverseInfo in results.OrderBy(l => l.ToString()))
            {
                _searchResults.Add(productUniverseInfo);
            }
        }

        /// <summary>
        /// Returns a list of product universes that match the given criteria
        /// </summary>
        /// <param name="codeOrNameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<ProductUniverseInfo> GetMatchingProductUniverses()
        {
            if (this._currentProductGroupId.HasValue && this.FilterAvailable)
            {
                return this.AvailableProductUniverses.Where(
                    p => p.ProductGroupId.Equals(this._currentProductGroupId.Value));
            }
            else
            {
                return this.AvailableProductUniverses;
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Select and close button event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectAndCloseButton_Click(object sender, RoutedEventArgs e)
        {
            //Set selected product universe
            this.SelectedProductUniverse = this.resultsGrid.SelectedItem as ProductUniverseInfo;

            this.Close();
        }

        /// <summary>
        /// Close button event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ResultsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            //Set selected product universe
            this.SelectedProductUniverse = this.resultsGrid.SelectedItem as ProductUniverseInfo;

            this.Close();
        }

        /// <summary>
        /// Replicates mouse double-click event for the return key for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resultsGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                //Set selected product universe
                this.SelectedProductUniverse = this.resultsGrid.SelectedItem as ProductUniverseInfo;
                e.Handled = true;
                this.Close();
            }
            else
            {
                return;
            }
        }

        #endregion
    }
}
