﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// A control similar to the side panel in outlook
    /// </summary>
    [TemplatePart(Name = "PART_ResizeThumb", Type = typeof(Thumb))]
    public class OutlookSidePanel : ContentControl
    {
        #region Fields

        private Thumb _partResizeThumb;
        private Double _lastExpandedWidth;

        #endregion

        #region Properties

        #region NavigationButtons Property

        public static readonly DependencyProperty NavigationButtonsProperty = 
            DependencyProperty.Register("NavigationButtons", typeof(IList), typeof(OutlookSidePanel),
            new PropertyMetadata(null));

        public IList NavigationButtons
        {
            get { return (IList)GetValue(NavigationButtonsProperty); }
        }

        #endregion

        #region IsExpandedProperty

        public static readonly DependencyProperty IsExpandedProperty =
            DependencyProperty.Register("IsExpanded", typeof(Boolean), typeof(OutlookSidePanel),
            new UIPropertyMetadata(true, OnIsExpandedPropertyChanged));

        private static void OnIsExpandedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            OutlookSidePanel senderControl = (OutlookSidePanel)obj;
            senderControl.OnIsExpandedChanged((Boolean)e.NewValue);
        }

        protected virtual void OnIsExpandedChanged(Boolean newValue)
        {
            if (newValue)
            {
                this.Width = _lastExpandedWidth;
            }
            else
            {
                _lastExpandedWidth = this.Width;
                this.Width = this.MinWidth;
            }
        }

        public Boolean IsExpanded
        {
            get { return (Boolean)GetValue(IsExpandedProperty); }
            set { SetValue(IsExpandedProperty, value); }
        }

        #endregion

        #endregion

        #region Constructors

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static OutlookSidePanel()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
               typeof(OutlookSidePanel), new FrameworkPropertyMetadata(typeof(OutlookSidePanel)));
        }

        public OutlookSidePanel()
        {
            SetValue(NavigationButtonsProperty, new List<Object>());
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();


            _partResizeThumb = GetTemplateChild("PART_ResizeThumb") as Thumb;
            if (_partResizeThumb != null)
            {
                _partResizeThumb.DragDelta += new DragDeltaEventHandler(PartResizeThumb_DragDelta);
            }
        }

        /// <summary>
        /// Called whenever the resize thumb is dragged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PartResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            Double horizDelta = e.HorizontalChange;
            if (horizDelta != 0)
            {
                Double newWidth = Math.Max(this.MinWidth, this.Width + horizDelta);

                if (newWidth <= this.MinWidth)
                {
                    this.IsExpanded = false;
                }
                else if (newWidth > this.MinWidth)
                {
                    this.IsExpanded = true;
                    this.Width = newWidth;
                }

            }
        }

        #endregion
    }
}
