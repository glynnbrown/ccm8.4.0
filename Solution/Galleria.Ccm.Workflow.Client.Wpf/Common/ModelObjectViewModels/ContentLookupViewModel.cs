﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 :J.Pickup
//	Created
// CCM-28492 :J.Pickup
//	Enforcing distinct on Planogram's name.
#endregion
#region Version History: (CCM 810)
// CCM-29986 : A.Probyn
//  Updated to use new FetchNonDebugByCategoryCode instead of FetchByCategoryCode
#endregion
#region Version History: (CCM 830)
// V8-31832 : L.Ineson
//  Added support for calculated columns.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.ViewModel;
using System.Collections.ObjectModel;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Runtime.CompilerServices;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.ModelObjectViewModels
{
    /// <summary>
    /// Represents a view of a ContentLookupViewModel for the UI
    /// </summary>
    public sealed class ContentLookupRowViewModel : ViewModelObject
    {
        #region Fields

        private PlanogramInfoListViewModel _availablePlanogramInfos = new PlanogramInfoListViewModel();
        private BulkObservableCollection<ContentLookupRow> _contentLookupRows = new BulkObservableCollection<ContentLookupRow>();
        private ContentLookupList _contentLookupsForEntity;

        #endregion

        #region Properties

        public BulkObservableCollection<ContentLookupRow> ContentLookupRows
        {
            get { return _contentLookupRows; }
        }

        private ContentLookupList ContentLookupsForEntity
        {
            get { return _contentLookupsForEntity; }
            set { _contentLookupsForEntity = value; }
        }

        public ReadOnlyBulkObservableCollection<PlanogramInfo> AvailablePlanogramInfos
        {
            get
            {
                return _availablePlanogramInfos.BindableCollection;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ContentLookupRowViewModel()
        {

        }

        #endregion

        #region Methods

        public void UpdateContentLookupRows(String code)
        {
            if (_contentLookupRows != null && _contentLookupRows.Count > 0)
            {
                _contentLookupRows.Clear();
            }

            _availablePlanogramInfos.FetchNonDebugByCategoryCode(code);
           _contentLookupsForEntity = ContentLookupList.FetchByEntityId(App.ViewState.EntityId);

            ContentLookup currentContentLookup;
            List<ContentLookupRow> rows = new List<ContentLookupRow>();

            //Make sure we enforce Distinct on id.
            foreach (PlanogramInfo currentInfo in this.AvailablePlanogramInfos)
            {
                //Try and locate content lookup once
                currentContentLookup = _contentLookupsForEntity.Where(c => c.PlanogramId.Equals(currentInfo.Id)).FirstOrDefault();
                if (currentContentLookup != null)
                {
                    ContentLookupRow newRow = new ContentLookupRow(currentInfo, currentContentLookup);
                    rows.Add(newRow);
                }
                else
                {
                    //When a content lookup record isn't associated to the planogram then create row without assigning.
                    ContentLookupRow newRow = new ContentLookupRow(currentInfo);
                    rows.Add(newRow);
                }
            }

            ContentLookupRows.AddRange(rows);
        }

        public void UpdateContentLookupRowsForUnassociated()
        {
            if (_contentLookupRows != null && _contentLookupRows.Count > 0)
            {
                _contentLookupRows.Clear();
            }

            _availablePlanogramInfos.FetchByNullCategoryCode();

            _contentLookupsForEntity = ContentLookupList.FetchByEntityId(App.ViewState.EntityId);

            ContentLookup currentContentLookup;

            List<ContentLookupRow> rows = new List<ContentLookupRow>();

            foreach (PlanogramInfo currentInfo in this.AvailablePlanogramInfos)
            {
                currentContentLookup = _contentLookupsForEntity.Where(c => c.PlanogramId.Equals(currentInfo.Id)).FirstOrDefault();
                if (currentContentLookup != null)
                {
                    ContentLookupRow newRow = new ContentLookupRow(currentInfo, currentContentLookup);
                    rows.Add(newRow);
                }
                else
                {
                    //When a content lookup record isn't associated to the planogram then create row without assigning.
                    ContentLookupRow newRow = new ContentLookupRow(currentInfo);
                    rows.Add(newRow);
                }
            }

            ContentLookupRows.AddRange(rows);
        }

        public void SaveContentLookupItems()
        {
            try
            {
                foreach (ContentLookupRow currentRowToSave in ContentLookupRows)
                {
                    if (currentRowToSave.FriendlyContentLookup != null && currentRowToSave.FriendlyContentLookup.IsDirty)
                    {
                        //Double check that the planogram name has been assigned and is correct.
                        if (currentRowToSave.ContentLookup.PlanogramId == null || currentRowToSave.ContentLookup.PlanogramId != currentRowToSave.PlanogramInfo.Id)
                        {
                            currentRowToSave.ContentLookup.PlanogramName = currentRowToSave.PlanogramInfo.Name;
                            currentRowToSave.ContentLookup.PlanogramId = currentRowToSave.PlanogramInfo.Id;
                        }

                        //If Is a new ContentLookup then won't be a child so call save directly on the child.
                        if (!currentRowToSave.ContentLookup.IsChild)
                        {
                            currentRowToSave.ContentLookup = currentRowToSave.ContentLookup.Save();
                        }
                    }
                }

                //we will call save on the list as some are children.
                if (ContentLookupsForEntity.IsValid)
                {
                    _contentLookupsForEntity = _contentLookupsForEntity.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Boolean IsDirty()
        {
            if (_contentLookupRows != null && _contentLookupRows.Count > 0)
            {
                if (_contentLookupRows.Where(c => c.FriendlyContentLookup != null && c.FriendlyContentLookup.IsDirty == true).FirstOrDefault() != null)
                {
                    //if ((_contentLookupRows.Where(c => c.FriendlyContentLookup.IsDirty == true).FirstOrDefault()) != null)
                    //{
                        return true;
                    //}
                }
                
            }
            return false;
            
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    #region Nested Classes

    /// <summary>
    /// Represents a row view of a ContentLookup for the UI (ContentLookup and it's associated planogram).
    /// </summary>
    public sealed class ContentLookupRow : INotifyPropertyChanged
    {
        #region Fields

        PlanogramInfo planogramInfo;
        ContentLookup contentLookup;
        String usersFriendlyName;
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Property Paths

        //Properties
        public static readonly PropertyPath ContentLookupRowsProperty = WpfHelper.GetPropertyPath<ContentLookupRow>(p => p.ContentLookup);
        public static readonly PropertyPath PlanogramInfoProperty = WpfHelper.GetPropertyPath<ContentLookupRow>(p => p.PlanogramInfo);
        public static readonly PropertyPath UsersFriendlyNameProperty = WpfHelper.GetPropertyPath<ContentLookupRow>(p => p.UsersFriendlyName);
        public static readonly PropertyPath FriendlyContentLookupProperty = WpfHelper.GetPropertyPath<ContentLookupRow>(p => p.FriendlyContentLookup);
        

        #endregion

        #region Properties

        public PlanogramInfo PlanogramInfo
        {
            get { return planogramInfo; }
            set
            {
                planogramInfo = value;
                OnPropertyChanged(PlanogramInfoProperty);
            }
        }

        public ContentLookup ContentLookup
        {
            get 
            {
                if (contentLookup == null)
                {
                    CreateNewLookup();
                    OnPropertyChanged(FriendlyContentLookupProperty);
                }
                return contentLookup; 
            }
            set
            {
                contentLookup = value;
                OnPropertyChanged(ContentLookupRowsProperty);
                OnPropertyChanged(UsersFriendlyNameProperty);
            }
        }

        public String UsersFriendlyName
        {
            get { return usersFriendlyName; }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                    _calculatedValueResolver = new CalculatedValueResolver(GetCalculatedValue);

                return _calculatedValueResolver;
            }
        }

        #endregion

        #region Constructors

        public ContentLookupRow(PlanogramInfo sourcePlanogram, ContentLookup sourceContentLookup)
        {
            PlanogramInfo = sourcePlanogram;
            ContentLookup = sourceContentLookup;

            User relatedUser = User.FetchById(contentLookup.UserId);
            usersFriendlyName = relatedUser.FirstName + " " + relatedUser.LastName;

        }

        public ContentLookupRow(PlanogramInfo sourcePlanogram)
        {
            PlanogramInfo = sourcePlanogram;
        }

        #endregion

        #region Methods

        public void CreateNewLookup()
        {
            if (contentLookup == null)
            {
                //Creates a new lookup
                contentLookup = ContentLookup.NewContentLookup(App.ViewState.EntityId);

                //Sets the friendly name (only gets set on creation).
                User relatedUser = User.FetchById(contentLookup.UserId);
                usersFriendlyName = relatedUser.FirstName + " " + relatedUser.LastName;

                //Notify of property changes.
                OnPropertyChanged(ContentLookupRowsProperty);
                OnPropertyChanged(UsersFriendlyNameProperty);
            }
        }

        private object GetCalculatedValue(String expressionText)
        {
            ObjectFieldExpression expression = new ObjectFieldExpression(expressionText);
            expression.AddDynamicFieldParameters(PlanogramInfo.EnumerateDisplayableFieldInfos());

            expression.ResolveDynamicParameter += OnFieldExpressionResolveParameter;

            //evaluate
            Object returnValue = expression.Evaluate();

            expression.ResolveDynamicParameter -= OnFieldExpressionResolveParameter;

            return returnValue;
        }

        /// <summary>
        /// Called when resolving a calculated parameter.
        /// </summary>
        private void OnFieldExpressionResolveParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            e.Result = e.Parameter.FieldInfo.GetValue(this.PlanogramInfo);
        }

        #endregion

        #region HelperProperties

        //A Friendly version for the UI that wont create the lookup if it doesn't actually exist.
        public ContentLookup FriendlyContentLookup
        {
            get { return contentLookup; }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(PropertyPath Property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(Property.Path));
            }
        }

        #endregion

    }

    #endregion
  
}