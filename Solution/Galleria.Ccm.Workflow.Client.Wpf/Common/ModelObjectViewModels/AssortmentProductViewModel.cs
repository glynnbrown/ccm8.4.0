﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn
//  Created 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections.Specialized;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Collections;
using System.Diagnostics;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// Represents a view of a Assortment Product for the ui
    /// </summary>
    public sealed class AssortmentProductView : ViewModelObject
    {
        #region Fields

        private AssortmentProduct _assortmentProduct;
        private Product _product;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AssortmentProductProperty = WpfHelper.GetPropertyPath<AssortmentProductView>(p => p.AssortmentProduct);
        public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<AssortmentProductView>(p => p.Product);
        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<AssortmentProductView>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<AssortmentProductView>(p => p.ProductName);
        
        #endregion

        #region Properties

        /// <summary>
        /// Expose the gtin
        /// </summary>
        public String ProductGtin
        {
            get { return _assortmentProduct.Gtin; }
        }

        /// <summary>
        /// Expose the name
        /// </summary>
        public String ProductName
        {
            get { return _assortmentProduct.Name; }
        }

        /// <summary>
        /// Returns the the source product for this view
        /// </summary>
        public Product Product
        {
            get { return _product; }
        }

        /// <summary>
        /// Returns the assortment product in question
        /// </summary>
        public AssortmentProduct AssortmentProduct
        {
            get { return _assortmentProduct;}
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentProductView(AssortmentProduct assortmentProduct, Product sourceProduct)
        {
            _assortmentProduct = assortmentProduct;
            _product = sourceProduct;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _product = null;
                    _assortmentProduct = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
