﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)
//// V8-25453 : A.Kuszyk
////  Created (copied from SA).
//#endregion
//#endregion

//using System;
//using System.Linq;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Windows;
//using System.Windows.Controls;
//using Galleria.Framework.Collections;
//using Galleria.Framework.Helpers;
//using Galleria.Framework.ViewModel;
//using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
//using Galleria.Ccm.Model;
//using Galleria.Ccm.Workflow.Client.Wpf.Common.ModelObjectViewModels;

//namespace Galleria.Ccm.Workflow.Client.Wpf.Common
//{
//    public enum ProductSearchType
//    {
//        Criteria,
//        ProductUniverse,
//    }
//    public static class ProductSearchTypeHelper
//    {
//        public static readonly Dictionary<ProductSearchType, String> FriendlyNames =
//            new Dictionary<ProductSearchType, String>()
//            {
//                {ProductSearchType.Criteria, Message.Enum_ProductSearchType_Criteria },
//                {ProductSearchType.ProductUniverse, Message.Enum_ProductSearchType_ProductUniverse },
//            };
//    }

//    //public sealed class ProductSelectionViewModel : ViewModelAttachedControlObject<ProductSelectionWindow>
//    //{
//    //    #region Fields

//    //    private ProductSearchType _searchType = ProductSearchType.Criteria;
//    //    private DataGridSelectionMode _selectionMode = DataGridSelectionMode.Extended;
//    //    private ProductInfoListViewModel _productInfoListView = new ProductInfoListViewModel();

//    //    private readonly BulkObservableCollection<ProductInfo> _searchResults = new BulkObservableCollection<ProductInfo>();
//    //    private ReadOnlyBulkObservableCollection<ProductInfo> _searchResultsRO;

//    //    private ObservableCollection<Int32> _excludedProductIds = new ObservableCollection<Int32>();
//    //    private ObservableCollection<ProductInfo> _selectedProducts = new ObservableCollection<ProductInfo>();

//    //    private Boolean _isSearching;
//    //    private Boolean _isSearchQueued;
//    //    private String _searchCriteria;
//    //    private Boolean _containsRecords;

//    //    //private ProductUniverseInfo _selectedProductUniverse;

//    //    #endregion

//    //    #region Binding Property Paths

//    //    //properties
//    //    public static readonly PropertyPath SearchTypeProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.SearchType);
//    //    public static readonly PropertyPath SelectionModeProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.SelectionMode);
//    //    public static readonly PropertyPath IsSearchingProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.IsSearching);
//    //    public static readonly PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.SearchResults);
//    //    public static readonly PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.SelectedProducts);
//    //    public static readonly PropertyPath SearchCriteriaProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.SearchCriteria);
//    //    //public static readonly PropertyPath SelectedProductUniverseProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.SelectedProductUniverse);
//    //    public static readonly PropertyPath ContainsRecordsProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.ContainsRecords);

//    //    //commands
//    //    //public static readonly PropertyPath SelectMerchGroupCommandProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.SelectMerchGroupCommand);
//    //    //public static readonly PropertyPath SelectProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ProductSelectionViewModel>(p => p.SelectProductUniverseCommand);

//    //    #endregion

//    //    #region Properties

//    //    /// <summary>
//    //    /// Gets/Sets the selection type
//    //    /// </summary>
//    //    public ProductSearchType SearchType
//    //    {
//    //        get { return _searchType; }
//    //        set
//    //        {
//    //            _searchType = value;
//    //            OnPropertyChanged(SearchTypeProperty);
//    //            PerformSearch();
//    //        }
//    //    }

//    //    /// <summary>
//    //    /// Returns the selection mode for this instance
//    //    /// </summary>
//    //    public DataGridSelectionMode SelectionMode
//    //    {
//    //        get { return _selectionMode; }
//    //    }

//    //    /// <summary>
//    //    /// Returns true if a search is currently being performed
//    //    /// </summary>
//    //    public Boolean IsSearching
//    //    {
//    //        get { return _isSearching; }
//    //        private set
//    //        {
//    //            _isSearching = value;
//    //            OnPropertyChanged(IsSearchingProperty);
//    //        }
//    //    }

//    //    /// <summary>
//    //    /// Returns the readonly collection of search results
//    //    /// </summary>
//    //    public ReadOnlyBulkObservableCollection<ProductInfo> SearchResults
//    //    {
//    //        get
//    //        {
//    //            //return _productInfoListView.BindingView;

//    //            if (_searchResultsRO == null)
//    //            {
//    //                _searchResultsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_searchResults);
//    //            }
//    //            return _searchResultsRO;
//    //        }
//    //    }

//    //    /// <summary>
//    //    /// Returns the editable collection of selected products
//    //    /// </summary>
//    //    public ObservableCollection<ProductInfo> SelectedProducts
//    //    {
//    //        get { return _selectedProducts; }
//    //    }

//    //    /// <summary>
//    //    /// Gets/Sets the search criteria value.
//    //    /// </summary>
//    //    public String SearchCriteria
//    //    {
//    //        get { return _searchCriteria; }
//    //        set
//    //        {
//    //            _searchCriteria = value;
//    //            OnPropertyChanged(SearchCriteriaProperty);
//    //            PerformSearch();
//    //        }
//    //    }

//    //    ///// <summary>
//    //    ///// Gets/Sets the selected product universe search criteria
//    //    ///// </summary>
//    //    //public ProductUniverseInfo SelectedProductUniverse
//    //    //{
//    //    //    get { return _selectedProductUniverse; }
//    //    //    set
//    //    //    {
//    //    //        _selectedProductUniverse = value;
//    //    //        OnPropertyChanged(SelectedProductUniverseProperty);
//    //    //        PerformSearch();
//    //    //    }
//    //    //}

//    //    /// <summary>
//    //    /// Returns true if the search results grid contains records
//    //    /// </summary>
//    //    public Boolean ContainsRecords
//    //    {
//    //        get { return _containsRecords; }
//    //        private set
//    //        {
//    //            _containsRecords = value;
//    //            OnPropertyChanged(ContainsRecordsProperty);
//    //        }
//    //    }

//    //    /// <summary>
//    //    /// Returns a collection of product ids to be excluded when displaying results.
//    //    /// </summary>
//    //    public ObservableCollection<Int32> ExcludedProductIds
//    //    {
//    //        get { return _excludedProductIds; }
//    //    }

//    //    #endregion

//    //    #region Constructor


//    //    /// <summary>
//    //    /// Constructor
//    //    /// </summary>
//    //    /// <param name="selectionMode"></param>
//    //    public ProductSelectionViewModel(DataGridSelectionMode selectionMode)
//    //    {
//    //        _selectionMode = selectionMode;
//    //        _productInfoListView.ModelChanged += ProductInfoListView_ModelChanged;
//    //    }

//    //    #endregion

//    //    #region Commands

//    //    #region SelectProductUniverse

//    //    //private RelayCommand _selectProductUniverseCommand;

//    //    //public RelayCommand SelectProductUniverseCommand
//    //    //{
//    //    //    get
//    //    //    {
//    //    //        if (_selectProductUniverseCommand == null)
//    //    //        {
//    //    //            _selectProductUniverseCommand = new RelayCommand(
//    //    //                p => SelectProductUniverse_Executed())
//    //    //            {
//    //    //                FriendlyName = "..."
//    //    //            };
//    //    //        }
//    //    //        return _selectProductUniverseCommand;
//    //    //    }
//    //    //}

//    //    //private void SelectProductUniverse_Executed()
//    //    //{
//    //    //    ProductUniverseSelectionWindow productUniverseWindow = new ProductUniverseSelectionWindow();
//    //    //    productUniverseWindow.SelectionMode = DataGridSelectionMode.Single;

//    //    //    App.ShowWindow(productUniverseWindow, true);

//    //    //    if (productUniverseWindow.SelectionResult != null)
//    //    //    {
//    //    //        this.SelectedProductUniverse = productUniverseWindow.SelectionResult[0];
//    //    //    }
//    //    //}

//    //    #endregion

//    //    #endregion

//    //    #region Event Handlers

//    //    /// <summary>
//    //    /// Responds to a change to the product info list model
//    //    /// </summary>
//    //    /// <param name="sender"></param>
//    //    /// <param name="e"></param>
//    //    private void ProductInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
//    //    {
//    //        //mark as finished searching
//    //        this.IsSearching = false;

//    //        //clear out any existing results
//    //        if (_searchResults.Count > 0) _searchResults.Clear();

//    //        //If the productInfoListView doesn't contain any results the "No Records" message should be displayed
//    //        this.ContainsRecords = (_productInfoListView.Model.Count > 0);

//    //        //add in results minus any exclusions.
//    //        List<Int32> excludeIds = this.ExcludedProductIds.ToList();
//    //        if (excludeIds.Count > 0)
//    //        {
//    //            _searchResults.AddRange(_productInfoListView.Model.Where(p => !excludeIds.Contains(p.Id)));
//    //        }
//    //        else
//    //        {
//    //            _searchResults.AddRange(_productInfoListView.Model);
//    //        }


//    //        //check if another search is queued
//    //        if (_isSearchQueued)
//    //        {
//    //            PerformSearch();
//    //        }
//    //    }

//    //    #endregion

//    //    #region Methods

//    //    /// <summary>
//    //    /// Performs the search operation
//    //    /// </summary>
//    //    private void PerformSearch()
//    //    {
//    //        Int32 entityId = App.ViewState.EntityId;

//    //        if (!this.IsSearching)
//    //        {
//    //            _isSearchQueued = false;

//    //            this.IsSearching = true;

//    //            //search asyncronously
//    //            switch (this.SearchType)
//    //            {
//    //                case ProductSearchType.Criteria:
//    //                    _productInfoListView.BeginFetchByEntityIdSearchCriteria(entityId, this.SearchCriteria);
//    //                    break;

//    //                //case ProductSearchType.ProductUniverse:
//    //                //    _productInfoListView.BeginFetchByProductUniverseId((this.SelectedProductUniverse != null) ? this.SelectedProductUniverse.Id : 0);
//    //                //    break;

//    //                default: throw new NotImplementedException();
//    //            }
//    //        }
//    //        else
//    //        {
//    //            _isSearchQueued = true;
//    //        }

//    //    }

//    //    #endregion

//    //    #region IDisposable Members

//    //    protected override void Dispose(bool disposing)
//    //    {
//    //        if (!base.IsDisposed)
//    //        {
//    //            if (disposing)
//    //            {
//    //                //dispose of the product info list
//    //                _productInfoListView.ModelChanged -= ProductInfoListView_ModelChanged;
//    //                _productInfoListView.Dispose();
//    //                _productInfoListView = null;

//    //            }
//    //            base.IsDisposed = true;
//    //        }
//    //    }

//    //    #endregion
//    //}
//}
