﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn
//  Created 
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections.Specialized;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Collections;
using System.Diagnostics;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// Represents a view of a Assortment Location for the ui
    /// </summary>
    public sealed class AssortmentLocationView : ViewModelObject
    {
        #region Fields

        private AssortmentLocation _assortmentLocation;
        private Location _location;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AssortmentLocationProperty = WpfHelper.GetPropertyPath<AssortmentLocationView>(p => p.AssortmentLocation);
        public static readonly PropertyPath LocationProperty = WpfHelper.GetPropertyPath<AssortmentLocationView>(p => p.Location);
        public static readonly PropertyPath LocationCodeProperty = WpfHelper.GetPropertyPath<AssortmentLocationView>(p => p.LocationCode);
        public static readonly PropertyPath LocationNameProperty = WpfHelper.GetPropertyPath<AssortmentLocationView>(p => p.LocationName);

        #endregion

        #region Properties

        /// <summary>
        /// Expose the location id
        /// </summary>
        public Int16 LocationId
        {
            get { return _assortmentLocation.LocationId; }
        }

        /// <summary>
        /// Expose the code
        /// </summary>
        public String LocationCode
        {
            get { return _assortmentLocation.Code; }
        }

        /// <summary>
        /// Expose the name
        /// </summary>
        public String LocationName
        {
            get { return _assortmentLocation.Name; }
        }

        /// <summary>
        /// Returns the the source location for this view
        /// </summary>
        public Location Location
        {
            get { return _location; }
        }

        /// <summary>
        /// Returns the assortment location in question
        /// </summary>
        public AssortmentLocation AssortmentLocation
        {
            get { return _assortmentLocation; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentLocationView(AssortmentLocation assortmentLocation, Location sourceLocation)
        {
            _assortmentLocation = assortmentLocation;
            _location = sourceLocation;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _location = null;
                    _assortmentLocation = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
