﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Controls;
using System.Globalization;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    public class NotNullValidationRule : ValidationRule
    {
        #region Fields
        private String _errorText = Message.NotNullValidationRule_DefaultTooltip;
        #endregion

        #region Properties

        public String ErrorText
        {
            get { return _errorText; }
            set { _errorText = value; }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public NotNullValidationRule() : base(ValidationStep.RawProposedValue, true) { }

        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            //assume success
            ValidationResult result = ValidationResult.ValidResult;

            if (value is String)
            {


                String valString = value as String;
                if (String.IsNullOrEmpty(valString))
                {
                    result = new ValidationResult(false, _errorText);
                }
            }
            else
            {
                if (value == null)
                {
                    result = new ValidationResult(false, _errorText);
                }
            }


            return result;
        }
        #endregion
    }
}
