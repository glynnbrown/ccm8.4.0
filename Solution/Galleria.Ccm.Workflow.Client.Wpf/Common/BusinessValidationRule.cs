﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Data;
using System.Globalization;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// Enum to indicate different dialog screens
    /// </summary>
    public enum DialogScreen
    {
        DataWeightingMaintenance,
        MetricMaintenance,
        StoreMaintenance,
        ClusterSchemeMaintenance,
        ReviewSettings,
        Options,
        SaveAs,
        ReviewFinder,
        MerchHierarchyMaintenance,
        FixtureMaintenance
    }

    public class BusinessValidationRule : ValidationRule
    {
        #region Constructros
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public BusinessValidationRule() : base(ValidationStep.CommittedValue, true) { }
        #endregion

        #region Fields
        //private UIScreen? _sourceScreen; // stores the source screen
        private DialogScreen? _sourceDialog; // stores the source dialog
        #endregion

        #region Properties

        ///// <summary>
        ///// The source screen
        ///// </summary>
        //public UIScreen? SourceScreen
        //{
        //    get { return _sourceScreen; }
        //    set { _sourceScreen = value; }
        //}

        /// <summary>
        /// The source dialog
        /// </summary>
        public DialogScreen? SourceDialog
        {
            get { return _sourceDialog; }
            set { _sourceDialog = value; }
        }


        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            // use the binding expression to locate the binding source
            // and property, then using the IDataErrorInfo interface
            // query the source to determine if any business rules
            // have been broken
            BindingExpression expression = value as BindingExpression;
            if (expression != null)
            {
                //get the propertyPath and resolve down to the dataItem
                Object dataItem = expression.DataItem;
                String[] pathParts = expression.ParentBinding.Path.Path.Split('.');
                if (pathParts.Count() > 1)
                {
                    //the path was multi part so get the data item as the path minus the last part.
                    String reconstructedPath = expression.ParentBinding.Path.Path.Replace("." + pathParts.Last(), "");
                    dataItem = Galleria.Framework.Controls.Wpf.Helpers.GetItemPropertyValue(dataItem, new System.Windows.PropertyPath(reconstructedPath));
                }


                IDataErrorInfo sourceItem = dataItem as IDataErrorInfo;
                if (sourceItem != null)
                {
                    String sourceProperty = pathParts.Last();
                    String errorContent = sourceItem[sourceProperty];
                    if (!String.IsNullOrEmpty(errorContent))
                    {
                        //get the language resource identifier
                        //Type sourceType = expression.DataItem.GetType();

                        //PropertyInfo friendlyNameField = sourceType.BaseType.GetProperty("FriendlyName", (BindingFlags.Static | BindingFlags.Public | BindingFlags.GetProperty));
                        //object objectVal = null;
                        //if (friendlyNameField != null)
                        //{
                        //    objectVal = friendlyNameField.GetValue(expression.DataItem, null);
                        //}
                        //String propertyMessageHeader;
                        //if (objectVal == null)
                        //{
                        //    propertyMessageHeader = sourceType.Name;
                        //}
                        //else
                        //{
                        //    propertyMessageHeader = (String)objectVal;
                        //}

                        // generate our validation result
                        validationResult = new ValidationResult(false, errorContent);
                    }
                }
            }

            // and return the validation result
            return validationResult;
        }
        #endregion
    }

    /// <summary>
    /// Validation rule for use when binding to model objects (percentage value)
    /// </summary>
    public class PercentageBusinessValidationRule : ValidationRule
    {
        #region Constructros
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public PercentageBusinessValidationRule() : base(ValidationStep.CommittedValue, true) { }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            // use the binding expression to locate the binding source
            // and property, then using the IDataErrorInfo interface
            // query the source to determine if any business rules
            // have been broken
            BindingExpression expression = value as BindingExpression;
            if (expression != null)
            {
                //get the propertyPath and resolve down to the dataItem
                Object dataItem = expression.DataItem;
                if (dataItem != null)
                {
                    String[] pathParts = expression.ParentBinding.Path.Path.Split('.');
                    if (pathParts.Count() > 1)
                    {
                        //the path was multi part so get the data item as the path minus the last part.
                        String reconstructedPath = expression.ParentBinding.Path.Path.Replace("." + pathParts.Last(), "");
                        dataItem = Galleria.Framework.Controls.Wpf.Helpers.GetItemPropertyValue(dataItem, new System.Windows.PropertyPath(reconstructedPath));
                    }

                    IDataErrorInfo sourceItem = dataItem as IDataErrorInfo;
                    if (sourceItem != null)
                    {
                        String sourceProperty = pathParts.Last();
                        String errorContent = sourceItem[sourceProperty];
                        if (!String.IsNullOrEmpty(errorContent))
                        {
                            //get the language resource identifier
                            Type sourceType = expression.DataItem.GetType();

                            PropertyInfo friendlyNameField = sourceType.BaseType.GetProperty("FriendlyName", (BindingFlags.Static | BindingFlags.Public | BindingFlags.GetProperty));
                            object objectVal = null;
                            if (friendlyNameField != null)
                            {
                                objectVal = friendlyNameField.GetValue(expression.DataItem, null);
                            }
                            string propertyMessageHeader;
                            if (objectVal == null)
                            {
                                propertyMessageHeader = sourceType.Name;
                            }
                            else
                            {
                                propertyMessageHeader = (String)objectVal;
                            }

                            //split the errorContent message
                            String[] splitErrorContent = errorContent.Split(' ');
                            String boundaryValue = splitErrorContent.Last();
                            Double parsedBoundaryValue;
                            Double.TryParse(boundaryValue, out parsedBoundaryValue);

                            //recreate the errorContent message
                            // use all parts of the message apart from the actual boundaryValue, which will be replaced
                            errorContent = String.Empty;
                            for (int i = 0; i < splitErrorContent.Count() - 1; i++)
                            {
                                errorContent += splitErrorContent[i] + ' ';
                            }
                            // append adjusted boundary value
                            errorContent += (parsedBoundaryValue * 100).ToString();

                            // generate our validation result
                            validationResult = new ValidationResult(false, errorContent);
                        }
                    }
                }
            }

            // and return the validation result
            return validationResult;
        }
        #endregion
    }
}
