﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// V8-24264 : A.Probyn
//  Changed CreateReadOnlyColumnSetFromMappingList over to use Galleria.Ccm.Common.Wpf.Helpers.CommonHelper.CreateReadOnlyColumn
#endregion
#region Version History: (CCM 8.02)
// V8-25805 : L.Ineson
//  Can no longer sort on the root hierarchy columns.
#endregion

#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Imports;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    /// <summary>
    /// Holds standard datagrid views for data objects
    /// </summary>
    public static class DataObjectViewHelper
    {
        #region General

        /// <summary>
        /// Creates a list of readonly columns from the given mapping list.
        /// </summary>
        /// <param name="mappingList">the mapping list</param>
        /// <param name="visibleColumnMapIds">The map ids to start visible or null for all</param>
        /// <returns></returns>
        public static List<DataGridColumn> CreateReadOnlyColumnSetFromMappingList(
            IModelObjectColumnHelper mappingList,
            String bindingPrefix = null,
            Int32[] visibleColumnMapIds = null,
            Dictionary<Int32, String> specialCaseBindings = null)
        {
            List<DataGridColumn> columnSet = new List<DataGridColumn>();

            foreach (ImportMapping map in mappingList)
            {
                String header = map.PropertyName;
                String bindingPath = null;

                //check if this is a special case
                if (specialCaseBindings != null &&
                    specialCaseBindings.Keys.Contains(map.PropertyIdentifier))
                {
                    bindingPath = specialCaseBindings[map.PropertyIdentifier];
                }
                else
                {
                    bindingPath = mappingList.GetBindingPath(map.PropertyIdentifier, bindingPrefix);
                }


                if (!String.IsNullOrEmpty(bindingPath))
                {
                    DataGridColumn col = Galleria.Ccm.Common.Wpf.Helpers.CommonHelper.CreateReadOnlyColumn(bindingPath,
                        map.PropertyType,
                        header,
                        map.PropertyDisplayType,
                        App.ViewState.DisplayUnits,
                        mappingList.GetColumnGroupName(map.PropertyIdentifier),
                        true);

                    columnSet.Add(col);
                }
            }

            return columnSet;
        }

        #endregion

        #region Locations

        /// <summary>
        /// Returns a readonly columnset of all location info object properties
        /// </summary>
        /// <param name="includeNonPopulated">if true all columns will be returned
        /// otherwise only those that are populated in the db</param>
        /// <returns></returns>
        public static List<DataGridColumn> GetBoundLocationInfoColumnSet()
        {
            List<DataGridColumn> columnList = new List<DataGridColumn>();

            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.CodeProperty.FriendlyName, Location.CodeProperty.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.NameProperty.FriendlyName, Location.NameProperty.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.Address1Property.FriendlyName, Location.Address1Property.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.Address2Property.FriendlyName, Location.Address2Property.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.RegionProperty.FriendlyName, Location.RegionProperty.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.CountyProperty.FriendlyName, Location.CountyProperty.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.PostalCodeProperty.FriendlyName, Location.PostalCodeProperty.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.TVRegionProperty.FriendlyName, Location.TVRegionProperty.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.CityProperty.FriendlyName, Location.CityProperty.Name, HorizontalAlignment.Left));
            columnList.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(Location.CountryProperty.FriendlyName, Location.CountryProperty.Name, HorizontalAlignment.Left));

            return columnList;
        }

        #endregion

        #region Location Group

        /// <summary>
        /// Returns the flattened readonly columnset for displaying location groups in a grid.
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <returns></returns>
        public static List<DataGridColumn> GetLocationGroupColumnSet(LocationHierarchy hierarchy, Boolean includeRootColumn)
        {
            List<DataGridColumn> columnSet = new List<DataGridColumn>();

            if (hierarchy != null)
            {
                System.Windows.Style elementStyle = new System.Windows.Style(typeof(TextBlock));
                elementStyle.BasedOn = DataGridExtendedTextColumn.DefaultElementStyle;
                elementStyle.Setters.Add(new Setter(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis));


                foreach (LocationLevel level in hierarchy.EnumerateAllLevels())
                {
                    String searchKey = level.Name;

                    if (!level.IsRoot)
                    {
                        //Create the label col
                        String bindingPath = String.Format("{0}[{1}].{2}",
                            LocationGroupViewModel.LevelPathValuesProperty.Path, searchKey, LocationGroup.GroupLabelPropertyName);

                        DataGridExtendedTextColumn col = new DataGridExtendedTextColumn()
                        {
                            IsReadOnly = true,
                            Width = 150,
                            Binding = new Binding(bindingPath) { Mode = BindingMode.OneWay },
                            ElementStyle = elementStyle,
                        };

                        //bind the column header to the level name
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(LocationLevel.NameProperty.Name) { Source = level });

                        //add the column to the set
                        columnSet.Add(col);

                    }
                    else if (includeRootColumn)
                    {
                        String bindingPath = String.Format("{0}[{1}].{2}",
                            LocationGroupViewModel.LevelPathValuesProperty.Path, searchKey, LocationGroup.GroupLabelPropertyName);

                        DataGridExtendedTextColumn rootCol = new DataGridExtendedTextColumn()
                        {
                            Header = null,
                            Width = 40,
                            IsReadOnly = true,
                            Binding = new Binding(bindingPath) { Mode = BindingMode.OneWay },
                            ElementStyle = elementStyle,
                            CanUserSort = false
                        };
                        rootCol.SetValue(ExtendedDataGrid.CanUserFilterProperty, false);
                        columnSet.Add(rootCol);
                    }
                }
            }

            return columnSet;
        }

        #endregion

        #region Product Group

        /// <summary>
        /// Returns the flattened readonly columnset for displaying product groups in a grid.
        /// </summary>
        /// <param name="hierarchy"></param>
        /// <returns></returns>
        public static List<DataGridColumn> GetProductGroupColumnSet(ProductHierarchy hierarchy, Boolean includeRootColumn)
        {
            List<DataGridColumn> columnSet = new List<DataGridColumn>();

            if (hierarchy != null)
            {
                System.Windows.Style elementStyle = new System.Windows.Style(typeof(TextBlock));
                elementStyle.BasedOn = DataGridExtendedTextColumn.DefaultElementStyle;
                elementStyle.Setters.Add(new Setter(TextBlock.TextTrimmingProperty, TextTrimming.CharacterEllipsis));


                foreach (ProductLevel level in hierarchy.EnumerateAllLevels())
                {
                    String searchKey = level.Name;

                    if (!level.IsRoot)
                    {
                        //Create the label col
                        String bindingPath = String.Format("{0}[{1}].{2}",
                            ProductGroupViewModel.LevelPathValuesProperty.Path, searchKey, ProductGroup.GroupLabelPropertyName);

                        DataGridExtendedTextColumn col = new DataGridExtendedTextColumn()
                        {
                            IsReadOnly = true,
                            Width = 150,
                            Binding = new Binding(bindingPath) { Mode = BindingMode.OneWay },
                            ElementStyle = elementStyle
                        };


                        //bind the column header to the level name
                        BindingOperations.SetBinding(col, DataGridColumn.HeaderProperty,
                            new Binding(ProductLevel.NameProperty.Name) { Source = level });

                        //add the column to the set
                        columnSet.Add(col);

                    }
                    else if (includeRootColumn)
                    {
                        String bindingPath = String.Format("{0}[{1}].{2}",
                            ProductGroupViewModel.LevelPathValuesProperty.Path, searchKey, ProductGroup.GroupLabelPropertyName);

                        DataGridExtendedTextColumn rootCol = new DataGridExtendedTextColumn()
                        {
                            Header = null,
                            Width = 40,
                            IsReadOnly = true,
                            Binding = new Binding(bindingPath) { Mode = BindingMode.OneWay },
                            ElementStyle = elementStyle,
                            CanUserSort = false
                        };
                        rootCol.SetValue(ExtendedDataGrid.CanUserFilterProperty, false);
                        columnSet.Add(rootCol);

                    }

                }
            }

            return columnSet;
        }

        #endregion

        #region AutoCreate Cluster Helpers

        /// <summary>
        /// Returns the list of properties populated in the database that form the location menu group
        /// </summary>
        /// <returns></returns>
        public static List<Csla.Core.IPropertyInfo> GetLocationGroupPopulatedLocationColumnNames()
        {
            List<Csla.Core.IPropertyInfo> populatedLocationColumns = new List<Csla.Core.IPropertyInfo>();

            LocationAttribute storeAttribute = LocationAttribute.FetchLocationAttributes(App.ViewState.EntityId);

            // Add location attributes that can be split to auto create cluster data
            if (storeAttribute.HasRegion) populatedLocationColumns.Add(Location.RegionProperty);
            if (storeAttribute.HasCounty) populatedLocationColumns.Add(Location.CountyProperty);
            if (storeAttribute.HasAddress1) populatedLocationColumns.Add(Location.Address1Property);
            if (storeAttribute.HasAddress2) populatedLocationColumns.Add(Location.Address2Property);
            if (storeAttribute.HasCity) populatedLocationColumns.Add(Location.CityProperty);
            if (storeAttribute.HasCountry) populatedLocationColumns.Add(Location.CountryProperty);


            return populatedLocationColumns;
        }

        /// <summary>
        /// Returns the list of properties populated in the database that form the general menu group
        /// </summary>
        /// <returns></returns>
        public static List<Csla.Core.IPropertyInfo> GetGeneralGroupPopulatedLocationColumnNames()
        {
            List<Csla.Core.IPropertyInfo> populatedStoreColumns = new List<Csla.Core.IPropertyInfo>();

            LocationAttribute storeAttribute = LocationAttribute.FetchLocationAttributes(App.ViewState.EntityId);

            // Add location attributes that can be split to auto create cluster data
            if (storeAttribute.HasManagerName) populatedStoreColumns.Add(Location.ManagerNameProperty);
            if (storeAttribute.HasRegionalManagerName) populatedStoreColumns.Add(Location.RegionalManagerNameProperty);
            if (storeAttribute.HasDivisionalManagerName) populatedStoreColumns.Add(Location.DivisionalManagerNameProperty);
            if (storeAttribute.HasDistributionCentre) populatedStoreColumns.Add(Location.DistributionCentreProperty);

            return populatedStoreColumns;
        }

        /// <summary>
        /// Returns the list of properties populated in the database that form the marketing menu group
        /// </summary>
        /// <returns></returns>
        public static List<Csla.Core.IPropertyInfo> GetMarketingGroupPopulatedLocationColumnNames()
        {
            List<Csla.Core.IPropertyInfo> populatedStoreColumns = new List<Csla.Core.IPropertyInfo>();

            LocationAttribute storeAttribute = LocationAttribute.FetchLocationAttributes(App.ViewState.EntityId);

            // Add location attributes that can be split to auto create cluster data
            if (storeAttribute.HasTVRegion) populatedStoreColumns.Add(Location.TVRegionProperty);
            if (storeAttribute.HasAdvertisingZone) populatedStoreColumns.Add(Location.AdvertisingZoneProperty);

            return populatedStoreColumns;
        }

        /// <summary>
        /// Returns the list of properties populated in the database that form the facility menu group
        /// </summary>
        /// <returns></returns>
        public static List<Csla.Core.IPropertyInfo> GetFacilityGroupPopulatedLocationColumnNames()
        {
            List<Csla.Core.IPropertyInfo> populatedStoreColumns = new List<Csla.Core.IPropertyInfo>();

            LocationAttribute storeAttribute = LocationAttribute.FetchLocationAttributes(App.ViewState.EntityId);

            // Add location attributes that can be split to auto create cluster data
            if (storeAttribute.HasPetrolForecourt) populatedStoreColumns.Add(Location.HasPetrolForecourtProperty);
            if (storeAttribute.HasPetrolForecourtType) populatedStoreColumns.Add(Location.PetrolForecourtTypeProperty);
            if (storeAttribute.HasRestaurant) populatedStoreColumns.Add(Location.RestaurantProperty);
            if (storeAttribute.HasNewsCube) populatedStoreColumns.Add(Location.HasNewsCubeProperty);
            if (storeAttribute.HasAtmMachines) populatedStoreColumns.Add(Location.HasAtmMachinesProperty);
            if (storeAttribute.HasCustomerWC) populatedStoreColumns.Add(Location.HasCustomerWCProperty);
            if (storeAttribute.HasBabyChanging) populatedStoreColumns.Add(Location.HasBabyChangingProperty);
            if (storeAttribute.HasInLocationBakery) populatedStoreColumns.Add(Location.HasInStoreBakeryProperty);
            if (storeAttribute.HasHotFoodToGo) populatedStoreColumns.Add(Location.HasHotFoodToGoProperty);
            if (storeAttribute.HasRotisserie) populatedStoreColumns.Add(Location.HasRotisserieProperty);
            if (storeAttribute.HasFishmonger) populatedStoreColumns.Add(Location.HasFishmongerProperty);
            if (storeAttribute.HasButcher) populatedStoreColumns.Add(Location.HasButcherProperty);
            if (storeAttribute.HasPizza) populatedStoreColumns.Add(Location.HasPizzaProperty);
            if (storeAttribute.HasDeli) populatedStoreColumns.Add(Location.HasDeliProperty);
            if (storeAttribute.HasSaladBar) populatedStoreColumns.Add(Location.HasSaladBarProperty);
            if (storeAttribute.HasOrganic) populatedStoreColumns.Add(Location.HasOrganicProperty);
            if (storeAttribute.HasGrocery) populatedStoreColumns.Add(Location.HasGroceryProperty);
            if (storeAttribute.HasMobilePhones) populatedStoreColumns.Add(Location.HasMobilePhonesProperty);
            if (storeAttribute.HasDryCleaning) populatedStoreColumns.Add(Location.HasDryCleaningProperty);
            if (storeAttribute.HasHomeShoppingAvailable) populatedStoreColumns.Add(Location.HasHomeShoppingAvailableProperty);
            if (storeAttribute.HasOptician) populatedStoreColumns.Add(Location.HasOpticianProperty);
            if (storeAttribute.HasPharmacy) populatedStoreColumns.Add(Location.HasPharmacyProperty);
            if (storeAttribute.HasTravel) populatedStoreColumns.Add(Location.HasTravelProperty);
            if (storeAttribute.HasPhotoDepartment) populatedStoreColumns.Add(Location.HasPhotoDepartmentProperty);
            if (storeAttribute.HasCarServiceArea) populatedStoreColumns.Add(Location.HasCarServiceAreaProperty);
            if (storeAttribute.HasGardenCentre) populatedStoreColumns.Add(Location.HasGardenCentreProperty);
            if (storeAttribute.HasClinic) populatedStoreColumns.Add(Location.HasClinicProperty);
            if (storeAttribute.HasAlcohol) populatedStoreColumns.Add(Location.HasAlcoholProperty);
            if (storeAttribute.HasFashion) populatedStoreColumns.Add(Location.HasFashionProperty);
            if (storeAttribute.HasCafe) populatedStoreColumns.Add(Location.HasCafeProperty);
            if (storeAttribute.HasRecycling) populatedStoreColumns.Add(Location.HasRecyclingProperty);
            if (storeAttribute.HasPhotocopier) populatedStoreColumns.Add(Location.HasPhotocopierProperty);
            if (storeAttribute.HasLottery) populatedStoreColumns.Add(Location.HasLotteryProperty);
            if (storeAttribute.HasPostOffice) populatedStoreColumns.Add(Location.HasPostOfficeProperty);
            if (storeAttribute.HasMovieRental) populatedStoreColumns.Add(Location.HasMovieRentalProperty);

            return populatedStoreColumns;
        }

        /// <summary>
        /// Returns the list of properties populated in the database that form the location specific menu group
        /// </summary>
        /// <returns></returns>
        public static List<Csla.Core.IPropertyInfo> GetLocationSpecificGroupPopulatedLocationColumnNames()
        {
            List<Csla.Core.IPropertyInfo> populatedStoreColumns = new List<Csla.Core.IPropertyInfo>();

            LocationAttribute storeAttribute = LocationAttribute.FetchLocationAttributes(App.ViewState.EntityId);

            // Add location attributes that can be split to auto create cluster data
            if (storeAttribute.HasMezzFitted) populatedStoreColumns.Add(Location.IsMezzFittedProperty);
            if (storeAttribute.HasSizeGrossArea) populatedStoreColumns.Add(Location.SizeGrossFloorAreaProperty);
            if (storeAttribute.HasSizeNetSalesArea) populatedStoreColumns.Add(Location.SizeNetSalesAreaProperty);
            if (storeAttribute.HasSizeMezzSalesArea) populatedStoreColumns.Add(Location.SizeMezzSalesAreaProperty);
            if (storeAttribute.HasNoOfCheckouts) populatedStoreColumns.Add(Location.NoOfCheckoutsProperty);

            return populatedStoreColumns;
        }

        /// <summary>
        /// Returns the list of properties populated in the database that form the Opening Times menu group
        /// </summary>
        /// <returns></returns>
        public static List<Csla.Core.IPropertyInfo> GetOpeningGroupPopulatedLocationColumnNames()
        {
            List<Csla.Core.IPropertyInfo> populatedStoreColumns = new List<Csla.Core.IPropertyInfo>();

            LocationAttribute storeAttribute = LocationAttribute.FetchLocationAttributes(App.ViewState.EntityId);

            // Add location attributes that can be split to auto create cluster data
            if (storeAttribute.HasOpenMonday) populatedStoreColumns.Add(Location.IsOpenMondayProperty);
            if (storeAttribute.HasOpenTuesday) populatedStoreColumns.Add(Location.IsOpenTuesdayProperty);
            if (storeAttribute.HasOpenWednesday) populatedStoreColumns.Add(Location.IsOpenWednesdayProperty);
            if (storeAttribute.HasOpenThursday) populatedStoreColumns.Add(Location.IsOpenThursdayProperty);
            if (storeAttribute.HasOpenFriday) populatedStoreColumns.Add(Location.IsOpenFridayProperty);
            if (storeAttribute.HasOpenSaturday) populatedStoreColumns.Add(Location.IsOpenSaturdayProperty);
            if (storeAttribute.HasOpenSunday) populatedStoreColumns.Add(Location.IsOpenSundayProperty);
            if (storeAttribute.Has24Hours) populatedStoreColumns.Add(Location.Is24HoursProperty);

            return populatedStoreColumns;
        }

        #endregion
    }
}
