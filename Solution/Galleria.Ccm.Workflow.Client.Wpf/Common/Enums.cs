﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
// CCM-25448 : N.Haywood
//  Added location space
// CCM-26124 : I.George
// Added the Enums for WasteValueType and ForceValueType
// CCM-26560 : A.Probyn
//  Added ListSortDirectionFriendlyNames
// V8-26911 : L.Luong
//  Added Enum for SummariseType and EventLogRecordTopCount Dictionary
// V8-27709 : L.Luong
//  Added UILanguageCodeHelper
#endregion

#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//	Added Match Type
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.ComponentModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{
    #region UIScreen

    /// <summary>
    /// Enum to indicate different nav tabs of the ui
    /// </summary>
    public enum UIScreen
    {
        PlanRepository,
        PlanAssignment,
        WorkPackages,
        Reports,
    }

    #endregion

    #region ViewTotalType

    /// <summary>
    /// enum to indicate the type of total the user wants
    /// </summary>
    public enum ViewTotalType
    {
        Sum,
        Average,
        Count,
        StandardDeviation
    }


    #region LocationSpaceGroupByType

    /// <summary>
    /// Location Space Group By Types
    /// </summary>
    public enum LocationSpaceGroupByType
    {
        None = 1,
        Hierarchy = 2
    }

    #endregion

    #region LocationSpaceViewType

    /// <summary>
    /// Location Space View Type enum
    /// </summary>
    public enum LocationSpaceViewType
    {
        Grid,
        Slider
    }

    #endregion

    #endregion

    #region InventoryRuleWasteHurdleType

    /// <summary>
    /// Used by the InventoryRules and InventoryObjectives screens
    /// </summary>
    public enum InventoryRuleWasteHurdleType
    {
        Units = 0,
        CasePacks = 1
    }
    public static class InventoryRuleWasteHurdleTypeHelper
    {
        public static readonly Dictionary<InventoryRuleWasteHurdleType, String> FriendlyNames =
            new Dictionary<InventoryRuleWasteHurdleType, String>()
            {
                {InventoryRuleWasteHurdleType.Units, Message.Enum_InventoryRuleWasteHurdleType_Units},
                {InventoryRuleWasteHurdleType.CasePacks, Message.Enum_InventoryRuleWasteHurdleType_CasePacks},
            };
    }

    #endregion

    #region InventoryRuleForceType

    /// <summary>
    /// Used by the InventoryRules and InventoryObjectives screens
    /// </summary>
    public enum InventoryRuleForceType
    {
        Units = 0,
        Facings = 1
    }
    public static class InventoryRuleForceTypeHelper
    {
        public static readonly Dictionary<InventoryRuleForceType, String> FriendlyNames =
            new Dictionary<InventoryRuleForceType, String>()
            {
                {InventoryRuleForceType.Units, Message.Enum_InventoryRuleForceType_Units},
                {InventoryRuleForceType.Facings, Message.Enum_InventoryRuleForceType_Facings},
            };
    }

    #endregion

    #region MatchType

    public enum MatchType
    {
        ExactMatch,
        ClosestBelow,
        ClosestAbove,
        NoMatch
    }

    public static class MatchTypeHelper
    {
        public static readonly Dictionary<MatchType, String> FriendlyNames =
            new Dictionary<MatchType, String>()
            {
                {MatchType.ExactMatch, Message.Enum_MatchType_ExactMatch},
                {MatchType.ClosestBelow, Message.Enum_MatchType_ClosestBelow},
                {MatchType.ClosestAbove, Message.Enum_MatchType_ClosestAbove},
                {MatchType.NoMatch, Message.Enum_MatchType_NoMatch},
            };
    }

    #endregion

    #region SummriseType

    //Event Logs group Grid by time Enum
    public enum SummariseType
    {
        None,
        Seconds3,
        Seconds5,
        Seconds10,
        Seconds60
    }

    #endregion

    /// <summary>
    /// Provides a method of retrieving the language toString overrides for enums
    /// </summary>
    public static class EnumHelpers
    {
        #region ViewTotalType

        private static Dictionary<ViewTotalType, String> _viewTotalTypeFriendlyNames;
        public static Dictionary<ViewTotalType, String> ViewTotalTypeFriendlyNames
        {
            get
            {
                if (_viewTotalTypeFriendlyNames == null)
                {
                    _viewTotalTypeFriendlyNames =
                        new Dictionary<ViewTotalType, String>(
                            new Dictionary<ViewTotalType, String>()
                    {
                       {ViewTotalType.Sum,  Message.Enum_ViewTotalType_Sum},
                       {ViewTotalType.Average,  Message.Enum_ViewTotalType_Average},
                       {ViewTotalType.Count, Message.Enum_ViewTotalType_Count},
                       {ViewTotalType.StandardDeviation, Message.Enum_ViewTotalType_StandardDeviation}
                    });
                }
                return _viewTotalTypeFriendlyNames;
            }
        }

        #endregion

        #region LocationSpaceGroupByType

        /// <summary>
        /// Location Space Group By Type Friendly Name Helper
        /// </summary>
        public static class LocationSpaceGroupByTypeHelper
        {
            public static readonly Dictionary<LocationSpaceGroupByType, String> LocationSpaceGroupByTypeFriendlyNames =
            new Dictionary<LocationSpaceGroupByType, String>()
            {
                {LocationSpaceGroupByType.None, Message.LocationSpace_GroupBy_None},
                {LocationSpaceGroupByType.Hierarchy, Message.LocationSpace_GroupBy_Hierarchy}
            };
        }

        #endregion

        #region ListSortDirection

        private static Dictionary<ListSortDirection, String> _listSortDirectionFriendlyNames;
        public static Dictionary<ListSortDirection, String> ListSortDirectionFriendlyNames
        {
            get
            {
                if (_listSortDirectionFriendlyNames == null)
                {
                    _listSortDirectionFriendlyNames =
                        new Dictionary<ListSortDirection, String>(
                            new Dictionary<ListSortDirection, String>()
                    {
                       {ListSortDirection.Ascending,  Message.Enum_ListSortDirection_Ascending},
                       {ListSortDirection.Descending,  Message.Enum_ListSortDirection_Descending}
                    });
                }
                return _listSortDirectionFriendlyNames;
            }
        }
        

        #endregion

        #region EventLogRecordTopCount

        public static readonly Dictionary<Int16, String> EventLogRecordTopCountFriendlyNames =
        new Dictionary<Int16, String>()
        {
            {100, Message.EventLog_RecordTopCount_100Rec},
            {500, Message.EventLog_RecordTopCount_500Rec},
            {1000, Message.EventLog_RecordTopCount_1000Rec},
            {2000, Message.EventLog_RecordTopCount_2000Rec}
        };

        #endregion

    }

    #region UILanguageHelper

    public static class UILanguageCodeHelper
    {
        public static readonly Dictionary<String, String> UILanguageCodeFriendlyNames =
            new Dictionary<String, String>()
            {
                {"en-gb", new CultureInfo("en-US").NativeName}
            };
    }
    #endregion
}
