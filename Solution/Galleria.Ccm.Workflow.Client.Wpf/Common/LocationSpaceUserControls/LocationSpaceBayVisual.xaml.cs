﻿using Galleria.Ccm.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.LocationSpaceUserControls
{
    /// <summary>
    /// Interaction logic for LocationSpaceBayVisual.xaml
    /// </summary>
    public partial class LocationSpaceBayVisual : UserControl, IDisposable
    {
        /// <summary>
        /// Gets/Sets the fixture to be displayed.
        /// </summary>
        public LocationSpaceBay LocationSpaceBay
        {
            get { return (LocationSpaceBay)GetValue(LocationSpaceBayProperty); }
            private set { SetValue(LocationSpaceBayProperty, value); }
        }

        public static readonly DependencyProperty LocationSpaceBayProperty =
        DependencyProperty.Register(nameof(LocationSpaceBay), typeof(LocationSpaceBay), typeof(LocationSpaceBayVisual));

        #region Constructor
        public LocationSpaceBayVisual(LocationSpaceBay locationSpaceBay)
        {
            InitializeComponent();
            LocationSpaceBay = locationSpaceBay;
        }

        #endregion

        #region IsSelectedProperty

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(Boolean), typeof(LocationSpaceBayVisual),
            new PropertyMetadata(false, OnIsSelectedPropertyChanged));

        private static void OnIsSelectedPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceBayVisual senderControl = (LocationSpaceBayVisual)obj;

            if ((Boolean)e.NewValue)
            {
                senderControl.xBorder.Background = App.Current.Resources[Galleria.Framework.Controls.Wpf.ResourceKeys.Main_BrushSelected] as Brush;
                senderControl.OnSelected();
            }
            else
            {
                senderControl.xBorder.Background = Brushes.Transparent;
                senderControl.OnDeselected();
            }
        }

        /// <summary>
        /// Gets/Sets whether this is selected.
        /// </summary>
        public Boolean IsSelected
        {
            get { return (Boolean)GetValue(IsSelectedProperty); }
            set
            {
                SetValue(IsSelectedProperty, value);
            }
        }

        #endregion

        #region Selected

        public static readonly RoutedEvent SelectedEvent =
            EventManager.RegisterRoutedEvent("Selected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(LocationSpaceBayVisual));

        public event RoutedEventHandler Selected
        {
            add { AddHandler(SelectedEvent, value); }
            remove { RemoveHandler(SelectedEvent, value); }
        }

        private void OnSelected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(LocationSpaceBayVisual.SelectedEvent));
        }

        #endregion

        #region De-selected

        public static readonly RoutedEvent DeselectedEvent =
            EventManager.RegisterRoutedEvent("Deselected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(LocationSpaceBayVisual));

        public event RoutedEventHandler Deselected
        {
            add { AddHandler(DeselectedEvent, value); }
            remove { RemoveHandler(DeselectedEvent, value); }
        }

        private void OnDeselected()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(LocationSpaceBayVisual.DeselectedEvent));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the user clicks on this control
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                this.IsSelected = !this.IsSelected;
            }
            else
            {
                // Check if this is already set and if so, force a change event
                if (this.IsSelected)
                {
                    OnSelected();
                }
                this.IsSelected = true;
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;
        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.LocationSpaceBay = null;
                _isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }
        #endregion

    }
}
