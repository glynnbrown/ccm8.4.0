﻿using Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common.LocationSpaceUserControls
{
    /// <summary>
    /// Interaction logic for AdvancedLocationSpacePropertiesPanel.xaml
    /// </summary>
    public partial class AdvancedLocationSpacePropertiesPanel : UserControl
    {

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationSpaceMaintenanceViewModel), typeof(AdvancedLocationSpacePropertiesPanel));

        /// <summary>
        /// Returns the viewmodel controller for this panel
        /// </summary>
        public LocationSpaceMaintenanceViewModel ViewModel
        {
            get { return (LocationSpaceMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public AdvancedLocationSpacePropertiesPanel()
        {

            InitializeComponent();


        }

        #endregion
    }
}
