﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)

// V8-25438 : L.Hodson ~ Created.
// V8-25444 : N.Haywood
//  Added CreateReadOnlyColumnSetFromMappingList
// CCM-25449 : N.Haywood
//  Added IsCollectionNullorEmpty
// CCM-25448 : N.Haywood
//  Added ToObservableCollection
// V8-25453 : A.Kuszyk
//  Added ContinueWithActionPrompt.
//V8-25788 : M.Pettit
//  Added FileHelper
// V8-25460 : L.Ineson
//  Added OpenPlanograms in editor.
//  Tidied up.
// V8-25454 : J.Pickup
//  Added AddItems<T> extension method and RemoveAllItems<T>
// V8-25455 : J.Pickup
//  Added static method FindVisualAncestor
// V8-26217 : A.Kuszyk
//  Added GetBitmapImage.
// V8-26244 : A.Kuszyk
//  Added FillPatternBrushes.
// V8-27193 : A.Silva   ~ Corrected OpenPlanogramsInEditor() so that each argument is passed in quotes, to avoid issues with spaces in the names.
// V8-27114 : A.Silva   ~ Amended OpenPlanogramsInEditor() so that it gets the Editor Client path from App.ViewState, warning if it has not been set.
// V8-27114 : J.Pickup
//  OpenPlanogramsInEditor() now shows appropriate error message and has been localised. 
// V8-28127 : A.Probyn
//  Added defensive code to OpenPlanogramsInEditor

#endregion

#region Version History: (CCM 8.2.0)

// V8-30940 : M.Shelley
//  Added a optional boolean parameter "isWarning" to the ContinueWithActionPrompt method to allow the dialog 
//  icon to either be an information icon or a warning icon (default - this was the previous behaviour)
// CCM-19023 : J.Mendes
//  Code added to initialize and update the real image provider. This is needed to make the export of a planograms with images to work as per the Editor.
//  The code added is similar to Editor but we kept separated because there may be different needs. 
//  So for all future changes to this initializer we should consider the Editor initializer as well.      

#endregion

#endregion

using Fluent;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Framework.Controls.Wpf.HelperClasses;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Galleria.Ccm.Workflow.Client.Wpf.Common
{

    /// <summary>
    /// Static class providing various helper methods
    /// </summary>
    public static class LocalHelper
    {
        #region General

        /// <summary>
        /// Gets the ribbon belonging to the parent window of the control and sets its backstage is open state.
        /// TODO: Old, to be removed.
        /// </summary>
        /// <param name="sender">the control to find the parent window of.</param>
        /// <param name="isBackstageOpen">the new backstage isopen state </param>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void SetRibbonBackstageState(DependencyObject sender, bool isBackstageOpen)
        {
            //get the parent window
            RibbonWindow parentWindow;
            if (sender is RibbonWindow) { parentWindow = (RibbonWindow)sender; }
            else { parentWindow = sender.FindVisualAncestor<RibbonWindow>(); }

            CommonHelper.SetRibbonBackstageState(parentWindow, isBackstageOpen);
        }

        /// <summary>
        /// Returns the Rexex pattern to use when finding matches for 
        /// input keywords.
        /// </summary>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public static String GetRexexKeywordPattern(String searchText)
        {
            String pattern = searchText;
            if (!String.IsNullOrEmpty(pattern))
            {
                StringBuilder sb = new StringBuilder();
                sb.Append('^');

                foreach (String keyword in pattern.Split(' '))
                {
                    if (keyword != "*" && keyword != "+")
                    {
                        String word = keyword.Replace("*", String.Empty);
                        word = word.Replace("+", String.Empty);

                        sb.AppendFormat(CultureInfo.InvariantCulture, "(?=.*?{0})", word);
                    }
                }

                sb.Append(".*$");


                pattern = sb.ToString();
            }
            return pattern;
        }

        /// <summary>
        /// Returns true if there are no binding validation errors
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static Boolean AreBindingsValid(DependencyObject obj)
        {
            if (obj != null)
            {
                return !System.Windows.Controls.Validation.GetHasError(obj) &&
                    LogicalTreeHelper.GetChildren(obj).OfType<DependencyObject>().All(child => AreBindingsValid(child));
            }
            else
            {
                //unit testing so just return true
                return true;
            }
        }

        /// <summary>
        /// TODO - Remove.
        /// </summary>
        /// <param name="mappingList"></param>
        /// <param name="bindingPrefix"></param>
        /// <param name="visibleColumnMapIds"></param>
        /// <param name="specialCaseBindings"></param>
        /// <returns></returns>
        [Obsolete("Use method in DataObjectViewHelper class")]
        public static List<DataGridColumn> CreateReadOnlyColumnSetFromMappingList(
          IModelObjectColumnHelper mappingList,
          String bindingPrefix = null,
          Int32[] visibleColumnMapIds = null,
          Dictionary<Int32, String> specialCaseBindings = null)
        {
            return DataObjectViewHelper.CreateReadOnlyColumnSetFromMappingList(mappingList, bindingPrefix, visibleColumnMapIds, specialCaseBindings);
        }

        #endregion

        #region Extensions

        /// <summary>
        /// Converts the given ienumerable to an observable collection of the specified type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerableList"></param>
        /// <returns></returns>
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> enumerableList)
        {
            if (enumerableList != null)
            {
                //create an emtpy observable collection object  
                var observableCollection = new ObservableCollection<T>();

                //loop through all the records and add to observable collection object  
                foreach (var item in enumerableList)
                {
                    observableCollection.Add(item);
                }

                //return the populated observable collection  
                return observableCollection;

            }

            return null;
        }

        #endregion

        #region Messages / Prompts

        /// <summary>
        /// Records that the given exception has occurred
        /// </summary>
        /// <param name="ex"></param>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public static void RecordException(Exception ex, String exceptionCategory = "Handled")
        {
            CommonHelper.RecordException(ex, exceptionCategory);
        }

        /// <summary>
        /// Shows a warning requesting whether the user would like to continue with the action
        /// </summary>
        /// <param name="title"></param>
        /// <param name="actionDescription"></param>
        /// <returns></returns>
        public static Boolean ContinueWithActionPrompt(String title, String actionHeader, String actionDescription, Boolean isWarning = true)
        {
            ModalMessageResult itemChangedAction = ModalMessageResult.Button1;

            ModalMessage dialog = new ModalMessage();
            dialog.Title = title;
            dialog.MessageIcon = (isWarning ? ImageResources.Dialog_Warning : ImageResources.DialogInformation);
            dialog.Header = actionHeader;
            dialog.Description = actionDescription;
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            dialog.ButtonCount = 2;
            dialog.Button1Content = Message.Generic_Yes;
            dialog.Button2Content = Message.Generic_No;

            //Show dialog
            App.ShowWindow(dialog, true);

            //Get result
            itemChangedAction = dialog.Result;

            if (itemChangedAction == ModalMessageResult.Button1)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Displays a message to the user informing them that a duplicate exception occurred
        /// </summary>
        /// <param name="owningWindow">The attached control that is the parent of this message</param>
        /// <param name="duplicateException">The exception thrown</param>
        public static void ShowDuplicateExceptionMessage(Window owningWindow, DuplicateException duplicateException)
        {
            ModalMessage dialog = new ModalMessage();
            dialog.Title = Application.Current.MainWindow.GetType().Assembly.GetName().Name;
            dialog.Header = Message.Generic_SaveFailed_DuplicateItemHeader;
            String description = Message.Generic_SaveFailed_DuplicateItemDescription;
            String exceptionMessage = duplicateException.Description != null ? duplicateException.Description : duplicateException.Message;
            dialog.Description = String.Format(description, exceptionMessage);
            dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            dialog.Owner = owningWindow;
            dialog.ButtonCount = 1;
            dialog.Button1Content = Message.Generic_Ok;
            dialog.DefaultButton = ModalMessageButton.Button1;
            dialog.MessageIcon = ImageResources.DialogWarning;
            dialog.ShowDialog();
        }


        
        #endregion

        #region Diagram

        /// <summary>
        /// Clears all nodes that are decendents of the given node
        /// </summary>
        /// <param name="node">The parent node to clear beneath</param>
        public static void ClearDescendantVisuals(Diagram diagram, FrameworkElement node)
        {
            if (diagram != null && node != null)
            {
                List<FrameworkElement> childList =
                    diagram.Links.Where(l => l.StartItem == node)
                    .Select(l => l.EndItem).ToList();

                foreach (FrameworkElement child in childList)
                {
                    // clear all the children recursively
                    ClearDescendantVisuals(diagram, child);
                    //now it is safe to remove 
                    diagram.Items.Remove(child);
                }

            }
        }

        #endregion

        #region Planograms

        /// <summary>
        ///     Opens the planogram editor with the selected planograms.
        /// </summary>
        /// <param name="selectedPlanograms">List of selected <see cref="PlanogramInfo"/> items to be opened.</param>
        /// <returns></returns>
        public static Process OpenPlanogramsInEditor(IEnumerable<PlanogramInfo> selectedPlanograms)
        {
            var editorClientPath = App.ViewState.EditorClientPath;
            if (!String.IsNullOrEmpty(editorClientPath)) return OpenPlanogramsInEditor(editorClientPath, selectedPlanograms);

            MessageBox.Show(Message.LocalHelper_EditorClientNotInstalled);
            return null;
        }

        /// <summary>
        ///  Opens the editor with the selected planograms ready to be edited.
        /// </summary>
        /// <param name="exePath">Path to the editor's exe.</param>
        /// <param name="selectedPlanograms">Collection of planogram infos to open from the repository.</param>
        /// <returns></returns>
        private static Process OpenPlanogramsInEditor(String exePath, IEnumerable<PlanogramInfo> selectedPlanograms)
        {
            var planogramInfos = selectedPlanograms.Where(p => p != null).Select(o => string.Format(@"""{0}|{1}""", o.Id, o.Name)).ToList();

            var process = new Process
            {
                StartInfo =
                {
                    FileName =
                        exePath,
                    Arguments = String.Join(" ", planogramInfos)
                }
            };
            try
            {
                process.Start();
            }
            catch (Exception)
            {
                MessageBox.Show("Editor executable not found.");
            }

            return process;
        }

        #endregion

        #region Real Image Handling

        private static IRealImageProvider _currentImageProvider;

        /// <summary>
        ///     Determines which real image provider to register based on the current user settings.
        /// </summary>
        public static void RegisterRealImageProvider()
        {

            //If it is register already do not Registered again
            if (!PlanogramImagesHelper.IsPlanogramImageRendererRegistered() || PlanogramImagesHelper.RealImageProvider != null) { return; }

            IRealImageProviderSettings settings = GetImageSettings();
            if (settings == null) return;

            //  We have settings, either reload the current provider or create a new one, depending on the source setting.
            switch (settings.ProductImageSource)
            {
                case RealImageProviderType.Gfs:
                    if (_currentImageProvider is RealImageProviderForGfs)
                    {
                        _currentImageProvider.Reload(settings);
                    }
                    else
                    {
                        if (_currentImageProvider != null) _currentImageProvider.Dispose();
                        _currentImageProvider = new RealImageProviderForGfs(App.ViewState.EntityId, settings);
                    }
                    break;
                case RealImageProviderType.Repository:
                // Commented out so that it is not used for now, use the folder provider for now.
                //if (_currentImageProvider is RealImageProviderForRepository)
                //    _currentImageProvider.Reload(settings);
                //else
                //    if (_currentImageProvider != null) _currentImageProvider.Dispose();
                //    _currentImageProvider = new RealImageProviderForRepository(ViewState.EntityId, settings);
                //break;
                case RealImageProviderType.Folder:
                    if (_currentImageProvider is RealImageProviderForFolder)
                    {
                        _currentImageProvider.Reload(settings);
                    }
                    else
                    {
                        if (_currentImageProvider != null) _currentImageProvider.Dispose();
                        _currentImageProvider = new RealImageProviderForFolder(settings);
                    }
                    break;
            }

            PlanogramImagesHelper.RegisterRealImageProvider(_currentImageProvider);
        }

        /// <summary>
        ///     Gets the current Image Settings to be used.
        /// </summary>
        /// <returns></returns>
        /// <remarks>TODO move into the view state class.</remarks>
        private static IRealImageProviderSettings GetImageSettings()
        {
            IRealImageProviderSettings imageSettings = App.ViewState.Settings.Model;
            if (imageSettings == null) return null;

            //if the local setting is not pointing at the repository one then
            // just return local
            if (imageSettings.ProductImageSource != RealImageProviderType.Repository)
                return imageSettings;

            //  Get the system settings if we need to use the repository ones and there is a connection.
            Entity entity = Entity.FetchById(App.ViewState.EntityId);
            if (entity != null) imageSettings = entity.SystemSettings;

            return imageSettings;
        }

        /// <summary>
        ///     Update the GFS real image provider when the GFS settings changes.
        /// </summary>
        public static void UpdateGFSRegisterRealImageProvider()
        {
            IRealImageProviderSettings settings = GetImageSettings();
            if (settings == null) return;

            if (_currentImageProvider != null) _currentImageProvider.Dispose();
            _currentImageProvider = new RealImageProviderForGfs(App.ViewState.EntityId, settings);

            PlanogramImagesHelper.RegisterRealImageProvider(_currentImageProvider);

        }
        #endregion

        #region Fill Pattern Brushes

        public static Dictionary<PlanogramProductFillPatternType, Brush> ProductFillPatternBrushes
        {
            get
            {
                return new Dictionary<PlanogramProductFillPatternType, Brush>()
                {
                    {PlanogramProductFillPatternType.Solid, 
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Solid, 5)},

                     {PlanogramProductFillPatternType.Border,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Border, 5)},

                     {PlanogramProductFillPatternType.Crosshatch,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Crosshatch, 5)},

                      {PlanogramProductFillPatternType.DiagonalDown,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.DiagonalDown, 5)},

                        {PlanogramProductFillPatternType.DiagonalUp,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.DiagonalUp, 5)},

                        {PlanogramProductFillPatternType.Dotted,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Dotted, 5)},

                        {PlanogramProductFillPatternType.Horizontal,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Horizontal, 5)},

                        {PlanogramProductFillPatternType.Vertical,
                        WpfMaterialHelper.CreateAbsolutePatternBrush(Brushes.Black, Brushes.White, ModelMaterial3D.FillPatternType.Vertical, 5)},
                };
            }
        }

        #endregion
    }
}
