﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-25454 : J.Pickup 
//  Created (Copied over from GFS)
// V8-26491 : A.Kuszyk
//  Localised AddRegionCommand friendly name.
#endregion
#region Version History: (CCM 8.3.0)
// CCM-18480 : D.Pleasance
//  Amended location grids to be of type AssortmentLocationView so that all location attributes can be selected.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    public sealed class AssortmentSetupAddRegionsViewModel : ViewModelAttachedControlObject<AssortmentSetupAddRegionsWindow>
    {
        #region Fields

        private Assortment _currentAssortment;
        private Dictionary<Int16, AssortmentLocationView> _locationLookup = new Dictionary<Int16, AssortmentLocationView>();

        private BulkObservableCollection<RegionRow> _regionRows = new BulkObservableCollection<RegionRow>();
        private ReadOnlyBulkObservableCollection<RegionRow> _regionRowsRO;

        private RegionRow _selectedRegionRow;

        private BulkObservableCollection<AssortmentLocationView> _availableLocations = new BulkObservableCollection<AssortmentLocationView>();
        private ReadOnlyBulkObservableCollection<AssortmentLocationView> _availableLocationsRO;

        private ObservableCollection<AssortmentLocationView> _selectedAvailableLocations = new ObservableCollection<AssortmentLocationView>();
        private ObservableCollection<AssortmentLocationView> _selectedRegionLocations = new ObservableCollection<AssortmentLocationView>();

        private List<RegionRow> _deletedRegionRows = new List<RegionRow>();
        #endregion

        #region Binding properties

        //properties
        public static readonly PropertyPath CurrentAssortmentProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.CurrentAssortment);
        public static readonly PropertyPath RegionRowsProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.RegionRows);
        public static readonly PropertyPath SelectedRegionRowProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.SelectedRegionRow);
        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedAvailableLocationsProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.SelectedAvailableLocations);
        public static readonly PropertyPath SelectedRegionLocationsProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.SelectedRegionLocations);

        //commands
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath AddRegionCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.AddRegionCommand);
        public static readonly PropertyPath RemoveSelectedRegionCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.RemoveSelectedRegionCommand);
        public static readonly PropertyPath AddSelectedLocationsCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.AddSelectedLocationsCommand);
        public static readonly PropertyPath AddAllLocationsCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.AddAllLocationsCommand);
        public static readonly PropertyPath RemoveSelectedLocationsCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.RemoveSelectedLocationsCommand);
        public static readonly PropertyPath RemoveAllLocationsCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionsViewModel>(p => p.RemoveAllLocationsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment
        /// </summary>
        public Assortment CurrentAssortment
        {
            get { return _currentAssortment; }
            set
            {
                _currentAssortment = value;
                OnPropertyChanged(CurrentAssortmentProperty);
            }
        }

        /// <summary>
        /// Returns the collection of family rows.
        /// </summary>
        public ReadOnlyBulkObservableCollection<RegionRow> RegionRows
        {
            get
            {
                if (_regionRowsRO == null)
                {
                    _regionRowsRO = new ReadOnlyBulkObservableCollection<RegionRow>(_regionRows);
                }
                return _regionRowsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected region row
        /// </summary>
        public RegionRow SelectedRegionRow
        {
            get { return _selectedRegionRow; }
            set
            {
                _selectedRegionRow = value;
                OnPropertyChanged(SelectedRegionRowProperty);

                OnSelectedRegionRowChanged(value);
            }
        }

        /// <summary>
        /// Returns the readonly collection of locations still available for adding to the region
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentLocationView> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyBulkObservableCollection<AssortmentLocationView>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected available locations
        /// </summary>
        public ObservableCollection<AssortmentLocationView> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
        }

        /// <summary>
        /// Returns the collection of selected region locations
        /// </summary>
        public ObservableCollection<AssortmentLocationView> SelectedRegionLocations
        {
            get { return _selectedRegionLocations; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="scenario">The given scenario</param>
        public AssortmentSetupAddRegionsViewModel(Assortment assortment)
        {
            Dictionary<Int16, Location> locations = LocationList.FetchByEntityId(App.ViewState.EntityId).ToDictionary(p => p.Id);
            
            foreach(AssortmentLocation assortmentLocation in assortment.Locations)
            {
                if (locations.ContainsKey(assortmentLocation.LocationId))
                {
                    _locationLookup.Add(assortmentLocation.LocationId, new AssortmentLocationView(assortmentLocation, locations[assortmentLocation.LocationId]));
                }
            }

            _regionRows.BulkCollectionChanged += RegionRows_BulkCollectionChanged;
            _selectedAvailableLocations.CollectionChanged += SelectedAvailableLocations_CollectionChanged;
            _selectedRegionLocations.CollectionChanged += SelectedRegionLocations_CollectionChanged;

            _currentAssortment = assortment;

            ReloadRegionRows();

            //select the first available row.
            if (this.RegionRows.Count > 0)
            {
                this.SelectedRegionRow = this.RegionRows.First();
            }
        }

        #endregion

        #region Commands

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies any changes made in the current item to the parent scenario
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //at least one row must be dirty
            if (!this.RegionRows.Any(f => f.IsDirty))
            {
                //if no rows are dirty, check if any rows have been deleted
                if (this._deletedRegionRows.Count == 0)
                {
                    return false;
                }
                return true;
            }

            //all rows must be valid
            if (!this.RegionRows.All(r => r.Region.IsValid))
            {
                return false;
            }

            //all regions must have a unique name
            if (this.RegionRows.Select(r => r.Region.Name).Distinct().Count() != this.RegionRows.Count)
            {
                return false;
            }

            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //delete all removed rows
            foreach (RegionRow deletedRow in _deletedRegionRows)
            {
                deletedRow.DeleteExistingRegion();
                deletedRow.Dispose();
            }
            _deletedRegionRows.Clear();

            //cycle through all rows applying changes made
            foreach (RegionRow row in this.RegionRows)
            {
                row.CommitChanges(_currentAssortment);
            }

            //recreate all rows
            ReloadRegionRows();

            base.ShowWaitCursor(false);

        }

        #endregion

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndClose_Executed(),
                        p => ApplyAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        private Boolean ApplyAndClose_CanExecute()
        {
            //all rows must be valid
            if (!this.RegionRows.All(r => r.Region.IsValid))
            {
                return false;
            }

            //all regions must have a unique name
            if (this.RegionRows.Select(r => r.Region.Name).Distinct().Count() != this.RegionRows.Count)
            {
                return false;
            }

            return true;
        }

        private void ApplyAndClose_Executed()
        {
            ApplyCurrentItem();

            //close the attached window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region AddRegionCommand

        private RelayCommand _addRegionCommand;

        /// <summary>
        /// Adds a new region to the list
        /// </summary>
        public RelayCommand AddRegionCommand
        {
            get
            {
                if (_addRegionCommand == null)
                {
                    _addRegionCommand = new RelayCommand(
                        p => AddRegion_Executed())
                    {
                        FriendlyName = Message.AssortmentSetup_AddRegion,
                        SmallIcon = ImageResources.Add_16,
                    };
                    base.ViewModelCommands.Add(_addRegionCommand);
                }
                return _addRegionCommand;
            }
        }

        private void AddRegion_Executed()
        {
            RegionRow newRow = new RegionRow();

            _regionRows.Add(newRow);

            this.SelectedRegionRow = newRow;
        }

        #endregion

        #region RemoveSelectedRegionCommand

        private RelayCommand _removeSelectedRegionCommand;

        /// <summary>
        /// Removes the currently selected region row
        /// </summary>
        public RelayCommand RemoveSelectedRegionCommand
        {
            get
            {
                if (_removeSelectedRegionCommand == null)
                {
                    _removeSelectedRegionCommand = new RelayCommand(
                        p => RemoveSelectedRegion_Executed(),
                        p => RemoveSelectedRegion_CanExecute())
                    {
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeSelectedRegionCommand);
                }
                return _removeSelectedRegionCommand;
            }
        }

        private Boolean RemoveSelectedRegion_CanExecute()
        {
            if (this.SelectedRegionRow == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedRegion_Executed()
        {
            RegionRow rowToRemove = this.SelectedRegionRow;

            //warn the user if the region to be deleted has any associated products
            Boolean continueWithRemove = true;
            if (rowToRemove.Region.Products.Count > 0)
            {
                //warn the user
                ModalMessage msg = new ModalMessage();
                msg.Header = rowToRemove.Region.Name;
                msg.Description = Message.AssortmentSetup_Region_DeleteHasRulesWarning_Desc;
                msg.ButtonCount = 2;
                msg.Button1Content = Message.Generic_Ok;
                msg.Button2Content = Message.Generic_Cancel;
                msg.CancelButton = ModalMessageButton.Button2;
                msg.DefaultButton = ModalMessageButton.Button1;
                msg.MessageIcon = ImageResources.Warning_32;

                App.ShowWindow(msg, this.AttachedControl, /*isModal*/true);

                if (msg.Result != ModalMessageResult.Button1)
                {
                    continueWithRemove = false;
                }
            }

            if (continueWithRemove)
            {
                //select the next available row
                if (this.RegionRows.Count > 1)
                {
                    this.RegionRows.First(r => r != rowToRemove);
                }

                //remove the row from the main collection and add to to the deleted list
                _regionRows.Remove(rowToRemove);
                _deletedRegionRows.Add(rowToRemove);

                if (AttachedControl != null)
                {
                    AttachedControl.xTakenLocationsGrid.ClearGrid(true);
                }
            }
        }

        #endregion

        #region AddSelectedLocationsCommand

        private RelayCommand _addSelectedLocationsCommand;

        /// <summary>
        /// Adds any selected available locations to taken list
        /// </summary>
        public RelayCommand AddSelectedLocationsCommand
        {
            get
            {
                if (_addSelectedLocationsCommand == null)
                {
                    _addSelectedLocationsCommand = new RelayCommand(
                        p => AddSelectedLocations_Executed(),
                        p => AddSelectedLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddToAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_AddSelected
                    };
                    base.ViewModelCommands.Add(_addSelectedLocationsCommand);
                }
                return _addSelectedLocationsCommand;
            }
        }

        private Boolean AddSelectedLocations_CanExecute()
        {
            //must have a region
            if (this.SelectedRegionRow == null)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentSetup_NoRegionSelected;
                return false;
            }

            //must have selected locations available
            if (this.SelectedAvailableLocations.Count == 0)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentSetup_NoLocationsSelected;
                return false;
            }

            return true;
        }

        private void AddSelectedLocations_Executed()
        {
            List<AssortmentLocationView> addList = this.SelectedAvailableLocations.ToList();

            //clear off the selection first to make loading quicker.
            this.SelectedAvailableLocations.Clear();

            //remove from the available
            _availableLocations.RemoveRange(addList);

            //add the items
            this.SelectedRegionRow.RegionLocations.AddRange(addList);
        }

        #endregion

        #region AddAllLocationsCommand

        private RelayCommand _addAllLocationsCommand;

        /// <summary>
        /// Adds all available locations to the taken list
        /// </summary>
        public RelayCommand AddAllLocationsCommand
        {
            get
            {
                if (_addAllLocationsCommand == null)
                {
                    _addAllLocationsCommand = new RelayCommand(
                        p => AddAllLocations_Executed(),
                        p => AddAllLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddAllToAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_AddAll
                    };
                    base.ViewModelCommands.Add(_addAllLocationsCommand);

                }
                return _addAllLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddAllLocations_CanExecute()
        {
            //must have a region
            if (this.SelectedRegionRow == null)
            {
                this.AddAllLocationsCommand.DisabledReason = Message.AssortmentSetup_NoRegionSelected;
                return false;
            }

            //must have locations available
            if (this.AvailableLocations.Count == 0)
            {
                this.AddAllLocationsCommand.DisabledReason = Message.AssortmentSetup_NoLocationsToAdd;
                return false;
            }

            return true;
        }

        private void AddAllLocations_Executed()
        {
            List<AssortmentLocationView> addList;
            if (AttachedControl != null)
            {
                //accessing the grid directly here is naughty but don't have a better way atm.
                addList = AttachedControl.xAvailableLocationsGrid.Items.Cast<AssortmentLocationView>().ToList();
            }
            else
            {
                addList = AvailableLocations.ToList();
            }


            //clear off any selection first.
            if (SelectedAvailableLocations.Count > 0)
            {
                SelectedAvailableLocations.Clear();
            }

            //remove from the available
            _availableLocations.RemoveRange(addList);

            //add all items
            SelectedRegionRow.RegionLocations.AddRange(addList);

        }

        #endregion

        #region RemoveSelectedLocationsCommand

        private RelayCommand _removeSelectedLocationsCommand;

        /// <summary>
        /// Removes the selected locations from the taken list
        /// </summary>
        public RelayCommand RemoveSelectedLocationsCommand
        {
            get
            {
                if (_removeSelectedLocationsCommand == null)
                {
                    _removeSelectedLocationsCommand = new RelayCommand(
                        p => RemoveSelectedLocations_Executed(),
                        p => RemoveSelectedLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveFromAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_RemoveSelected,
                        DisabledReason = Message.AssortmentSetup_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_removeSelectedLocationsCommand);
                }
                return _removeSelectedLocationsCommand;
            }
        }

        private Boolean RemoveSelectedLocations_CanExecute()
        {
            if (this.SelectedRegionRow == null)
            {
                return false;
            }

            if (this.SelectedRegionLocations.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedLocations_Executed()
        {
            List<AssortmentLocationView> removeList = this.SelectedRegionLocations.ToList();

            //clear off the selection first to make loading quicker.
            this.SelectedRegionLocations.Clear();

            //remove the items
            this.SelectedRegionRow.RegionLocations.RemoveRange(removeList);

            //add to the available
            _availableLocations.AddRange(removeList);
        }

        #endregion

        #region RemoveAllLocationsCommand

        private RelayCommand _removeAllLocationsCommand;

        /// <summary>
        /// Removes all locations from the taken list
        /// </summary>
        public RelayCommand RemoveAllLocationsCommand
        {
            get
            {
                if (_removeAllLocationsCommand == null)
                {
                    _removeAllLocationsCommand = new RelayCommand(
                        p => RemoveAllLocations_Executed(),
                        p => RemoveAllLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveAllFromAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_RemoveAll,
                        DisabledReason = Message.AssortmentSetup_NoLocationsToRemove
                    };
                    base.ViewModelCommands.Add(_removeAllLocationsCommand);
                }
                return _removeAllLocationsCommand;
            }
        }

        private Boolean RemoveAllLocations_CanExecute()
        {
            //must have an selected
            if (this.SelectedRegionRow == null)
            {
                RemoveAllLocationsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have locations
            if (this.SelectedRegionRow.RegionLocations.Count == 0)
            {
                RemoveAllLocationsCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void RemoveAllLocations_Executed()
        {
            //clear off any selection first.
            if (SelectedRegionLocations.Count > 0)
            {
                SelectedRegionLocations.Clear();
            }

            //get the list of locs to be removed
            List<AssortmentLocationView> removeList;
            if (AttachedControl != null)
            {
                //accessing the grid directly here is naughty but don't have a better way atm.
                removeList = AttachedControl.xTakenLocationsGrid.Items.Cast<AssortmentLocationView>().ToList();
            }
            else
            {
                removeList = SelectedRegionRow.RegionLocations.ToList();
            }


            //remove all products
            SelectedRegionRow.RegionLocations.RemoveRange(removeList);

            //add to the available
            _availableLocations.AddRange(removeList);

        }


        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected family
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedRegionRowChanged(RegionRow newValue)
        {
            ReloadAvailableLocations();
        }

        /// <summary>
        /// Responds to changes to the family rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegionRows_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove ||
                e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
            {
                foreach (RegionRow row in e.ChangedItems)
                {
                    row.Dispose();
                }
            }
        }

        /// <summary>
        /// Responds to selected item changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedAvailableLocations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                this.SelectedRegionLocations.Clear();
            }
        }

        /// <summary>
        /// Responds to selected item changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedRegionLocations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                this.SelectedAvailableLocations.Clear();
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.RegionRows.Any(p => p.IsDirty))
                {
                    ChildItemChangeResult result = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        this.ApplyCommand.Execute();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }

                }
            }
            return continueExecute;

        }

        /// <summary>
        /// Recreates the family rows collection
        /// </summary>
        private void ReloadRegionRows()
        {
            if (_regionRows.Count > 0)
            {
                _regionRows.Clear();
            }

            List<RegionRow> newRows = new List<RegionRow>();
            
            foreach (var region in _currentAssortment.Regions)
            {
                List<AssortmentLocationView> regionLocations = new List<AssortmentLocationView>();
                foreach (Int16 locationId in region.Locations.Select(l => l.LocationId))
                {
                    AssortmentLocationView location = null;
                    if (_locationLookup.TryGetValue(locationId, out location))
                    {
                        regionLocations.Add(location);
                    }
                }

                newRows.Add(new RegionRow(region, regionLocations));
            }

            _regionRows.AddRange(newRows);
        }

        /// <summary>
        /// Reloads the list of available products
        /// </summary>
        private void ReloadAvailableLocations()
        {
            if (_availableLocations.Count > 0)
            {
                _availableLocations.Clear();
            }

            if (_currentAssortment.Locations != null)
            {
                List<AssortmentLocationView> assortLocations = _locationLookup.Values.ToList();
                
                foreach (RegionRow regionRow in this.RegionRows)
                {
                    foreach (AssortmentLocationView location in regionRow.RegionLocations)
                    {
                        assortLocations.Remove(location);
                    }
                }

                _availableLocations.AddRange(assortLocations);
            }            
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                _regionRows.BulkCollectionChanged -= RegionRows_BulkCollectionChanged;
                _selectedAvailableLocations.CollectionChanged -= SelectedAvailableLocations_CollectionChanged;
                _selectedRegionLocations.CollectionChanged -= SelectedRegionLocations_CollectionChanged;
                _locationLookup = null;

                if (disposing)
                {
                    _regionRows.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Represents a row on the region screen
    /// </summary>
    public sealed class RegionRow : ViewModelObject
    {
        #region Fields

        private AssortmentRegion _existingRegion;
        private AssortmentRegion _region;
        private Boolean _isDirty;

        private BulkObservableCollection<AssortmentLocationView> _regionLocations = new BulkObservableCollection<AssortmentLocationView>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath RegionProperty = WpfHelper.GetPropertyPath<RegionRow>(p => p.Region);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<RegionRow>(p => p.IsDirty);
        public static readonly PropertyPath RegionLocationsProperty = WpfHelper.GetPropertyPath<RegionRow>(p => p.RegionLocations);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the region represented by this row.
        /// </summary>
        public AssortmentRegion Region
        {
            get { return _region; }
        }


        /// <summary>
        /// Returns the collection of products allocated to the family.
        /// </summary>
        public BulkObservableCollection<AssortmentLocationView> RegionLocations
        {
            get { return _regionLocations; }
        }

        /// <summary>
        /// Returns true if this row is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        #endregion

        #region Constructor

        public RegionRow()
        {
            _region = AssortmentRegion.NewAssortmentRegion();
            _isDirty = true;

            _region.PropertyChanged += Region_PropertyChanged;
            _regionLocations.BulkCollectionChanged += RegionLocations_BulkCollectionChanged;
        }


        public RegionRow(AssortmentRegion existingRegion, List<AssortmentLocationView> regionLocations)
        {
            _existingRegion = existingRegion;
            _region = existingRegion.Clone();
            
            _regionLocations.AddRange(regionLocations);
            
            _isDirty = false;

            _region.PropertyChanged += Region_PropertyChanged;
            _regionLocations.BulkCollectionChanged += RegionLocations_BulkCollectionChanged;
        }

        #endregion

        #region Event Handlers

        private void Region_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            this.IsDirty = true;
        }


        private void RegionLocations_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            this.IsDirty = true;
        }



        #endregion

        #region Methods

        public void CommitChanges(Assortment assort)
        {
            if (this.IsDirty)
            {
                AssortmentRegion commitRegion = null;

                if (_existingRegion == null)
                {
                    commitRegion = _region;

                    //just add the rule
                    assort.Regions.Add(_region);
                }
                else
                {
                    commitRegion = _existingRegion;

                    //copy values
                    _existingRegion.Name = _region.Name;
                }

                //clear existing locs and add all new
                commitRegion.Locations.Clear();

                List<AssortmentRegionLocation> addLocations = new List<AssortmentRegionLocation>();
                foreach (AssortmentLocationView loc in this.RegionLocations)
                {
                    addLocations.Add(AssortmentRegionLocation.NewAssortmentRegionLocation(loc.LocationId));
                }
                commitRegion.Locations.AddRange(addLocations);
                
                this.IsDirty = false;
            }
        }

        public void DeleteExistingRegion()
        {
            if (_existingRegion != null)
            {
                _existingRegion.ParentAssortment.Regions.Remove(_existingRegion);
            }
            this.IsDirty = false;
        }

        #endregion

        #region IDisposable
        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _region.PropertyChanged -= Region_PropertyChanged;
                _regionLocations.BulkCollectionChanged -= RegionLocations_BulkCollectionChanged;                
                
                if (disposing)
                {
                }

                base.IsDisposed = true;
            }
        }
        #endregion
    }
}