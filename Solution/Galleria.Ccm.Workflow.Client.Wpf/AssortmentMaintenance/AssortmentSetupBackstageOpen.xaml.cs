﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupBackstageOpen.xaml
    /// </summary>
    public partial class AssortmentSetupBackstageOpen : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupViewModel), typeof(AssortmentSetupBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentSetupViewModel ViewModel
        {
            get { return (AssortmentSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupBackstageOpen senderControl = (AssortmentSetupBackstageOpen)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupViewModel oldModel = (AssortmentSetupViewModel)e.OldValue;
            }

            if (e.NewValue != null)
            {
                AssortmentSetupViewModel newModel = (AssortmentSetupViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentSetupBackstageOpen()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void ExtendedDataGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                AssortmentInfo itemToOpen = e.RowItem as AssortmentInfo;
                if (itemToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                }
            }
        }

        private void Datagrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;
                if (this.ViewModel != null)
                {
                    AssortmentInfo itemToOpen = senderControl?.CurrentItem as AssortmentInfo;
                    if (itemToOpen != null)
                    {
                        //send the store to be opened
                        this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                    }
                }
                e.Handled = true;
            }
        }

        #endregion        
    }
}