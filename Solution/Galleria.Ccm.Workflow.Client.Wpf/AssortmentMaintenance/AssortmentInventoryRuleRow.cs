﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32718 : A.Probyn
//  Updated so full product attributes can be displayed.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.Globalization;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance
{
    /// <summary>
    /// Represents a row on the assortment inventory rules screen
    /// </summary>
    public sealed class AssortmentInventoryRuleRow : ViewModelObject
    {
        #region Fields
        private Assortment _assortment;
        private AssortmentProduct _assortmentProduct;
        private Product _product;
        private AssortmentInventoryRule _existingRule;
        private AssortmentInventoryRule _rule;
        private Boolean _isDirty;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.AssortmentProduct);
        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.ProductName);
        public static readonly PropertyPath RuleProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.Rule);
        public static readonly PropertyPath RuleSummaryProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.RuleSummary);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleRow>(p => p.IsDirty);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the assortment product this row relates to
        /// </summary>
        public AssortmentProduct AssortmentProduct
        {
            get { return _assortmentProduct; }
        }

        /// <summary>
        /// Gets the product this row relates to
        /// </summary>
        public Product Product
        {
            get { return _product; }
        }

        /// <summary>
        /// Gets the editing rule for this row.
        /// </summary>
        public AssortmentInventoryRule Rule
        {
            get { return _rule; }
        }

        /// <summary>
        /// Gets the Gtin of the product this row relates to
        /// </summary>
        public String ProductGtin
        {
            get { return _assortmentProduct.Gtin; }
        }

        /// <summary>
        /// Gets the Name of the product this row relates to
        /// </summary>
        public String ProductName
        {
            get { return _assortmentProduct.Name; }
        }

        /// <summary>
        /// Gets a summary of the current rule
        /// </summary>
        public String RuleSummary
        {
            get
            {
                if (this.IsNullRule)
                {
                    return String.Empty;
                }
                else
                {
                    String value = String.Empty;

                    if (_rule.CasePack != 0) 
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0} {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_CasePacks, _rule.CasePack);
                    }

                    if (_rule.DaysOfSupply != 0) 
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_Dos, _rule.DaysOfSupply);
                    }

                    if (_rule.ShelfLife != 0) 
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_ShelfLife, _rule.ShelfLife.ToString("P0", CultureInfo.CurrentCulture));
                    }

                    if (_rule.ReplenishmentDays != 0) 
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_Delivery, _rule.ReplenishmentDays);
                    }

                    if (_rule.WasteHurdleUnits != 0) 
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_WasteHurdleUnits, _rule.WasteHurdleUnits);
                    }
                    if (_rule.WasteHurdleCasePack != 0) 
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                            "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_WasteHurdleCasePacks, _rule.WasteHurdleCasePack);
                    }

                    if (_rule.MinUnits != 0) 
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                           "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_MinUnits, _rule.MinUnits);
                    }
                    if (_rule.MinFacings != 0)
                    {
                        value = String.Format(CultureInfo.CurrentCulture,
                           "{0}, {1}={2}", value, Message.AssortmentInventoryRules_RuleSummary_MinFacings, _rule.MinFacings);
                    }


                    return value.TrimStart(", ".ToCharArray());
                }
            }
        }

        /// <summary>
        /// Returns true if the rule has no values.
        /// </summary>
        public Boolean IsNullRule
        {
            get
            {
                if (_rule == null) { return true; }
                if (_rule.CasePack != 0) { return false; }
                if (_rule.DaysOfSupply != 0) { return false; }
                if (_rule.ShelfLife != 0) { return false; }
                if (_rule.ReplenishmentDays != 0) { return false; }
                if (_rule.WasteHurdleUnits != 0) { return false; }
                if (_rule.WasteHurdleCasePack != 0) { return false; }
                if (_rule.MinUnits != 0) { return false; }
                if (_rule.MinFacings != 0) { return false; }

                return true;
            }
        }

        /// <summary>
        /// Returns true if the row is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        #endregion

        #region Constructor

        public AssortmentInventoryRuleRow(Assortment assortment, AssortmentProduct assortmentproduct, Product sourceProduct)
        {
            _assortmentProduct = assortmentproduct;
            _product = sourceProduct;
            _assortment = assortment;

            //check if a rule exists on the range
            _existingRule = assortment.InventoryRules.FirstOrDefault(p => p.ProductId.Equals(assortmentproduct.ProductId));

            //set the rule to actually edit
            if (_existingRule != null)
            {
                _rule = _existingRule.Clone();
            }
            else
            {
                //create a new null rule
                _rule = AssortmentInventoryRule.NewAssortmentInventoryRule(assortmentproduct);
                ClearRule();
                //nb - dont mark dirty as this is just a blank rule
            }
            _rule.PropertyChanged += Rule_PropertyChanged;
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes to the current rule
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Rule_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(RuleSummaryProperty);

            this.IsDirty = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Commits any changes made back to the given node
        /// </summary>
        /// <param name="applyToRange"></param>
        public void CommitChanges()
        {
            if (this.IsDirty)
            {
                Assortment applyToAssortment = _assortment;
                AssortmentInventoryRule existingRule = _existingRule;
                AssortmentInventoryRule editRule = _rule;

                //if this had an existing rule but is now null - remove it
                if (this.IsNullRule && existingRule != null)
                {
                    applyToAssortment.InventoryRules.Remove(existingRule);
                }
                else if (!this.IsNullRule && existingRule == null)
                {
                    //just add the rule
                    applyToAssortment.InventoryRules.Add(editRule);
                }
                else if (!this.IsNullRule && existingRule != null)
                {
                    //copy values
                    _existingRule.CopyValues(editRule);
                }

                this.IsDirty = false;
            }
        }

        /// <summary>
        /// Clears off all of the rule values
        /// </summary>
        public void ClearRule()
        {
            _rule.CasePack = 0;
            _rule.DaysOfSupply = 0;
            _rule.ShelfLife = 0;
            _rule.ReplenishmentDays = 0;
            _rule.WasteHurdleUnits = 0;
            _rule.WasteHurdleCasePack = 0;
            _rule.WasteHurdleUnits = 0;
            _rule.MinUnits = 0;
            _rule.MinFacings = 0;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _rule.PropertyChanged -= Rule_PropertyChanged;

                if (disposing)
                {
                    _assortment = null;
                    _existingRule = null;
                    _rule = null;
                    _assortmentProduct = null;
                }
            }

        }

        #endregion
    }
}

