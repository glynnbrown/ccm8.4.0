﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-25454 : J.Pickup 
//  Created (Copied over from GFS)
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.ComponentModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// ViewModel controller for RegionProductWindow
    /// </summary>
    public sealed class AssortmentSetupAddRegionalProductsViewModel : ViewModelAttachedControlObject<AssortmentSetupAddRegionalProductsWindow>
    {
        #region Fields

        private Assortment _currentAssortment;
        private Dictionary<Int32, Product> _fullProductLookup;
        private AssortmentProduct _selectedPrimaryProduct;

        private BulkObservableCollection<RegionProductRow> _regionProductRows = new BulkObservableCollection<RegionProductRow>();
        private ReadOnlyBulkObservableCollection<RegionProductRow> _regionProductRowsRO;

        private RegionProductRow _selectedRegionProductRow;

        #endregion

        #region Binding properties

        //properties
        public static readonly PropertyPath SelectedPrimaryProductProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.SelectedPrimaryProduct);
        public static readonly PropertyPath RegionProductRowsProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.RegionProductRows);
        public static readonly PropertyPath SelectedRegionProductRowProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.SelectedRegionProductRow);

        //commands
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SelectPrimaryProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.SelectPrimaryProductCommand);
        public static readonly PropertyPath EditRegionsCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.EditRegionsCommand);
        public static readonly PropertyPath SelectRegionalProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.SelectRegionalProductCommand);
        public static readonly PropertyPath RemoveRegionalProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddRegionalProductsViewModel>(p => p.RemoveRegionalProductCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The currently selected AssortmentProduct (primary product)
        /// </summary>
        public AssortmentProduct SelectedPrimaryProduct
        {
            get { return _selectedPrimaryProduct; }
            set
            {
                _selectedPrimaryProduct = value;
                OnPropertyChanged(SelectedPrimaryProductProperty);

                OnSelectedPrimaryProductChanged(value);
            }
        }

        /// <summary>
        /// Returns the readonly collection of regional products
        /// </summary>
        public ReadOnlyBulkObservableCollection<RegionProductRow> RegionProductRows
        {
            get
            {
                if (_regionProductRowsRO == null)
                {
                    _regionProductRowsRO = new ReadOnlyBulkObservableCollection<RegionProductRow>(_regionProductRows);
                }
                return _regionProductRowsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the current selected regional product row
        /// </summary>
        public RegionProductRow SelectedRegionProductRow
        {
            get { return _selectedRegionProductRow; }
            set
            {
                _selectedRegionProductRow = value;
                OnPropertyChanged(SelectedRegionProductRowProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="scenario">The given scenario</param>
        public AssortmentSetupAddRegionalProductsViewModel(Assortment assortment)
        {
            _currentAssortment = assortment;

            //Fetch master products once for display purposes
            _fullProductLookup = ProductList.FetchByProductIds(_currentAssortment.Products.Select(p => p.ProductId)).ToDictionary(p => p.Id);
            
        }

        #endregion

        #region Commands

        #region Apply

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies any changes made in the current item to the parent scenario
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //must have primary product selected
            if (this.SelectedPrimaryProduct == null)
            {
                return false;
            }

            //must be dirty
            if (!this.RegionProductRows.Any(p => p.IsDirty))
            {
                return false;
            }


            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        /// <summary>
        /// Applies changes made to the current item.
        /// </summary>
        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //cycle through all rows applying changes made
            foreach (RegionProductRow row in this.RegionProductRows)
            {
                row.CommitChanges();
            }

            //Assign as primary product if contain the primary product, else reset as normal product
            AssortmentProduct primaryProduct = this.SelectedPrimaryProduct;
            Boolean containsRegionalSwap = false;
            if (primaryProduct != null)
            {
                //create a row per region
                foreach (AssortmentRegion region in _currentAssortment.Regions)
                {
                    if (region.Products.Any(p => p.PrimaryProductId == primaryProduct.ProductId))
                    {
                        containsRegionalSwap = true;
                        break;
                    }
                }

                if (containsRegionalSwap)
                {
                    this.SelectedPrimaryProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.Regional;
                    this.SelectedPrimaryProduct.IsPrimaryRegionalProduct = true;
                }
                else
                {
                    this.SelectedPrimaryProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.None;
                    this.SelectedPrimaryProduct.IsPrimaryRegionalProduct = false;
                }
            }

            //take a snap of the currently selected item
            AssortmentRegion selectedRegion = (this.SelectedRegionProductRow != null) ? this.SelectedRegionProductRow.Region : null;

            //recreate all rows
            ReloadRegionRows();

            //reselect
            if (selectedRegion != null)
            {
                foreach (RegionProductRow row in this.RegionProductRows)
                {
                    if (row.Region == selectedRegion)
                    {
                        this.SelectedRegionProductRow = row;
                        break;
                    }
                }
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyAndClose

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndClose_Executed(),
                        p => ApplyAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        private Boolean ApplyAndClose_CanExecute()
        {
            //must have primary product selected
            if (this.SelectedPrimaryProduct == null)
            {
                return false;
            }

            //must be dirty
            if (!this.RegionProductRows.Any(p => p.IsDirty))
            {
                return false;
            }

            return true;
        }

        private void ApplyAndClose_Executed()
        {
            ApplyCurrentItem();

            //close the attached window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region EditRegions

        private RelayCommand _editRegionsCommand;

        /// <summary>
        /// Launches the edit regions window
        /// </summary>
        public RelayCommand EditRegionsCommand
        {
            get
            {
                if (_editRegionsCommand == null)
                {
                    _editRegionsCommand = new RelayCommand(
                        p => EditRegions_Executed())
                    {
                        FriendlyName = Message.AssortmentSetup_EditRegions,
                        SmallIcon = ImageResources.AssortmentSetup_RegionalProducts
                    };
                    base.ViewModelCommands.Add(_editRegionsCommand);
                }
                return _editRegionsCommand;
            }
        }

        private void EditRegions_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentSetupAddRegionsWindow win = new AssortmentSetupAddRegionsWindow(new AssortmentSetupAddRegionsViewModel(this._currentAssortment));
                App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

                if (this.SelectedPrimaryProduct != null)
                {
                    //check if a row has been added or removed
                    List<RegionProductRow> existingRows = this.RegionProductRows.ToList();
                    List<RegionProductRow> newRows = new List<RegionProductRow>();

                    Assortment scen = _currentAssortment;
                    //iterate through all regions in the scenario
                    foreach (AssortmentRegion scenRegion in scen.Regions)
                    {
                        RegionProductRow row = existingRows.FirstOrDefault(r => r.Region == scenRegion);
                        if (row == null)
                        {
                            //add a row for the region - there wont be a rule yet so don't bother looking.
                            newRows.Add(new RegionProductRow(scenRegion, SelectedPrimaryProduct));
                        }
                        else
                        {
                            existingRows.Remove(row);
                        }
                    }

                    //remove old rows
                    _regionProductRows.RemoveRange(existingRows);
                    _regionProductRows.AddRange(newRows);
                }
            }
        }

        #endregion

        #region SelectPrimaryProduct

        private RelayCommand _selectPrimaryProductCommand;

        /// <summary>
        /// Allows selection of available products to be the regional primary product
        /// </summary>
        public RelayCommand SelectPrimaryProductCommand
        {
            get
            {
                if (_selectPrimaryProductCommand == null)
                {
                    _selectPrimaryProductCommand =
                        new RelayCommand(p => SelectPrimaryProduct_Executed())
                        {
                            FriendlyName = Message.Assortment_AddLinkedLocationHierarchyCommand,
                            FriendlyDescription = Message.AssortmentSetup_RegionalProductSetup_GetRegionalProduct_Tooltip
                        };
                    base.ViewModelCommands.Add(_selectPrimaryProductCommand);
                }
                return _selectPrimaryProductCommand;
            }
        }

        private void SelectPrimaryProduct_Executed()
        {
            if (ContinueWithItemChange())
            {
                if (this.AttachedControl != null)
                {
                    // Get list of products that can be the primary regional product
                    List<AssortmentProductView> availableAssortmentProducts = new List<AssortmentProductView>();
                    foreach (AssortmentProduct assortmentProduct in this._currentAssortment.Products.Where(
                                    p => p.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.None
                                    || p.IsPrimaryRegionalProduct))
                    {
                        Product sourceProduct = null;
                        _fullProductLookup.TryGetValue(assortmentProduct.ProductId, out sourceProduct);
                        availableAssortmentProducts.Add(new AssortmentProductView(assortmentProduct, sourceProduct));
                    }                   

                    AssortmentRegionalPrimaryProductSelectionWindow assortmentRegionalPrimaryProductSelectionWindow = new AssortmentRegionalPrimaryProductSelectionWindow(availableAssortmentProducts);
                    CommonHelper.GetWindowService().ShowDialog<AssortmentRegionalPrimaryProductSelectionWindow>(assortmentRegionalPrimaryProductSelectionWindow);

                    if (assortmentRegionalPrimaryProductSelectionWindow.DialogResult == true)
                    {
                        this.SelectedPrimaryProduct = assortmentRegionalPrimaryProductSelectionWindow.SelectionResult.AssortmentProduct;
                    }
                }
            }
        }

        #endregion

        #region SelectRegionalProduct

        private RelayCommand _selectRegionalProductCommand;

        /// <summary>
        /// Allows selection of available products that can be a regional swap
        /// </summary>
        public RelayCommand SelectRegionalProductCommand
        {
            get
            {
                if (_selectRegionalProductCommand == null)
                {
                    _selectRegionalProductCommand = new RelayCommand(
                        p => SelectRegionalProduct_Executed(p),
                        p => SelectRegionalProduct_CanExecute(p))
                    {
                        FriendlyName = Message.Assortment_AddLinkedLocationHierarchyCommand
                    };
                    base.ViewModelCommands.Add(_selectRegionalProductCommand);
                }
                return _selectRegionalProductCommand;
            }
        }

        private Boolean SelectRegionalProduct_CanExecute(Object arg)
        {
            RegionProductRow row = arg as RegionProductRow;

            //must have a region row selected
            if (row == null)
            {
                return false;
            }

            return true;
        }

        private void SelectRegionalProduct_Executed(Object arg)
        {
            RegionProductRow row = arg as RegionProductRow;
            if (row != null)
            {

                if (this.AttachedControl != null)
                {
                    // Load the available assortment products that can be regional swaps
                    List<AssortmentProductView> availableAssortmentProducts = new List<AssortmentProductView>();

                    foreach (AssortmentProduct assortmentProduct in this._currentAssortment.Products)
                    {
                        Product sourceProduct = null;
                        _fullProductLookup.TryGetValue(assortmentProduct.ProductId, out sourceProduct);

                        if (assortmentProduct.ProductId == this.SelectedPrimaryProduct.ProductId)
                        {
                            availableAssortmentProducts.Add(new AssortmentProductView(assortmentProduct, sourceProduct));
                        }
                        else
                        {
                            if (assortmentProduct.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.None)
                            {
                                //check that the product hasnt already been assigned a region
                                Boolean alreadyAssigned = false;

                                foreach (RegionProductRow regionProductRow in RegionProductRows)
                                {
                                    if (regionProductRow.SelectedProduct != null && regionProductRow.SelectedProduct.ProductId == assortmentProduct.ProductId)
                                    {
                                        alreadyAssigned = true;
                                    }

                                }

                                if (!alreadyAssigned) { availableAssortmentProducts.Add(new AssortmentProductView(assortmentProduct, sourceProduct)); }
                            }
                        }
                    }

                    AssortmentRegionalProductSelectionWindow assortmentRegionalProductSelectionWindow = new AssortmentRegionalProductSelectionWindow(availableAssortmentProducts);
                    CommonHelper.GetWindowService().ShowDialog<AssortmentRegionalProductSelectionWindow>(assortmentRegionalProductSelectionWindow);

                    if (assortmentRegionalProductSelectionWindow.DialogResult == true)
                    {
                        this.SelectedRegionProductRow.SelectedProduct = assortmentRegionalProductSelectionWindow.SelectionResult.AssortmentProduct;
                    }
                }
            }
        }

        #endregion

        #region RemoveRegionalProduct

        private RelayCommand _removeRegionalProductCommand;

        /// <summary>
        /// Allows selection of available products that can be a regional swap
        /// </summary>
        public RelayCommand RemoveRegionalProductCommand
        {
            get
            {
                if (_removeRegionalProductCommand == null)
                {
                    _removeRegionalProductCommand = new RelayCommand(
                        p => RemoveRegionalProduct_Executed(p),
                        p => RemoveRegionalProduct_CanExecute(p))
                    {
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeRegionalProductCommand);
                }
                return _removeRegionalProductCommand;
            }
        }

        private Boolean RemoveRegionalProduct_CanExecute(Object arg)
        {
            RegionProductRow row = arg as RegionProductRow;

            //must have a region row selected
            if (row == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveRegionalProduct_Executed(Object arg)
        {
            RegionProductRow row = arg as RegionProductRow;
            if (row != null)
            {
                if (this.AttachedControl != null)
                {
                    this.SelectedRegionProductRow.SelectedProduct = null;
                }
            }
        }

        #endregion

        #endregion

        #region EventHandlers

        /// <summary>
        /// Responds to a change of selected primary product
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedPrimaryProductChanged(AssortmentProduct newValue)
        {
            ReloadRegionRows();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.RegionProductRows.Any(r => r.IsDirty))
                {
                    ChildItemChangeResult result = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        this.ApplyCommand.Execute();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //Do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }
                }
            }
            return continueExecute;
        }

        /// <summary>
        /// Reloads the region rows collection
        /// </summary>
        private void ReloadRegionRows()
        {
            _regionProductRows.Clear();

            AssortmentProduct primaryProduct = this.SelectedPrimaryProduct;
            if (primaryProduct != null)
            {
                //create a row per region
                foreach (AssortmentRegion region in _currentAssortment.Regions)
                {
                    _regionProductRows.Add(new RegionProductRow(region, primaryProduct));
                }
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (_regionProductRows != null)
                    {
                        _regionProductRows.Clear();
                    }

                    _selectedPrimaryProduct = null;
                    _fullProductLookup.Clear();
                    _fullProductLookup = null;
                    _currentAssortment = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Represents a row on the region product screen
    /// </summary>
    public sealed class RegionProductRow : INotifyPropertyChanged
    {
        #region Fields
        private AssortmentRegion _region;
        private AssortmentRegionProduct _regionProduct;
        private AssortmentProduct _selectedProduct;
        private Boolean _isDirty;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath RegionProperty = WpfHelper.GetPropertyPath<RegionProductRow>(p => p.Region);
        public static readonly PropertyPath SelectedProductProperty = WpfHelper.GetPropertyPath<RegionProductRow>(p => p.SelectedProduct);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<RegionProductRow>(p => p.IsDirty);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the region this row represents
        /// </summary>
        public AssortmentRegion Region
        {
            get { return _region; }
        }

        /// <summary>
        /// Returns the selected product for this row.
        /// </summary>
        public AssortmentProduct SelectedProduct
        {
            get { return _selectedProduct; }
            set
            {
                _selectedProduct = value;
                OnPropertyChanged(SelectedProductProperty);

                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Returns true if this row is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        #endregion

        #region Constructor

        public RegionProductRow(AssortmentRegion region, AssortmentProduct primaryProduct)
        {
            _region = region;

            //check if a region product exists
            _regionProduct = region.Products.Where(p => p.PrimaryProductId == primaryProduct.ProductId).FirstOrDefault();

            if (_regionProduct != null)
            {
                //set the selected product
                if (_regionProduct.RegionalProductId != null)
                {
                    Assortment parentAssortment = _region.ParentAssortment;
                    if (parentAssortment.Products != null)
                    {
                        _selectedProduct = parentAssortment.Products.Where(p => p.ProductId == _regionProduct.RegionalProductId).FirstOrDefault();
                    }
                }
            }
            else
            {
                _regionProduct = AssortmentRegionProduct.NewAssortmentRegionalProduct(_region, primaryProduct);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Commits changes made to the row
        /// </summary>
        public void CommitChanges()
        {
            if (this.IsDirty)
            {
                Boolean isExistingRegion = _regionProduct.ParentAssortmentRegion != null;

                if (this.SelectedProduct != null)
                {
                    if (_regionProduct.RegionalProductId != this.SelectedProduct.ProductId)
                    {
                        //reset previous
                        Assortment parentAssortment = _region.ParentAssortment;
                        if (parentAssortment.Products != null)
                        {
                            AssortmentProduct currentregionalProduct = parentAssortment.Products.Where(p => p.ProductId == _regionProduct.RegionalProductId).FirstOrDefault();
                            if (currentregionalProduct != null)
                            {
                                //re assign regional product
                                currentregionalProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.None;
                            }
                        }
                    }

                    _regionProduct.RegionalProductId = this.SelectedProduct.ProductId;
                    // Set the assortment product to be a regional swap product
                    this.SelectedProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.Regional;
                }
                else
                {
                    Assortment parentAssortment = _region.ParentAssortment;
                    if (parentAssortment.Products != null)
                    {
                        AssortmentProduct currentregionalProduct = parentAssortment.Products.Where(p => p.ProductId == _regionProduct.RegionalProductId).FirstOrDefault();
                        if (currentregionalProduct != null)
                        {
                            //re assign regional product
                            currentregionalProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.None;
                        }
                    }

                    _regionProduct.RegionalProductId = null;
                }

                if (isExistingRegion && this.SelectedProduct == null)
                {
                    //remove the row
                    _region.Products.Remove(_regionProduct);
                }
                else if (!isExistingRegion && this.SelectedProduct != null)
                {
                    //add the row
                    _region.Products.Add(_regionProduct);
                }

                this.IsDirty = false;
            }
        }

        #endregion

        #region INotifyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}