﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
#endregion
#endregion

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Collections.Generic;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupProductRule.xaml
    /// </summary>
    public partial class AssortmentSetupProductRuleWindow : ExtendedRibbonWindow
    {
        const String _removeRuleCommandKey = "ProductsRulesRemoveRuleCommand";

        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupProductRuleViewModel),
            typeof(AssortmentSetupProductRuleWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentSetupProductRuleViewModel ViewModel
        {
            get { return (AssortmentSetupProductRuleViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupProductRuleWindow senderControl =
                (AssortmentSetupProductRuleWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupProductRuleViewModel oldModel =
                    (AssortmentSetupProductRuleViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(_removeRuleCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentSetupProductRuleViewModel newModel =
                    (AssortmentSetupProductRuleViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(_removeRuleCommandKey, newModel.RemoveRuleCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentSetupProductRuleWindow(Assortment selectedAssortment)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = new AssortmentSetupProductRuleViewModel(selectedAssortment);

            this.Loaded += new RoutedEventHandler(AssortmentSetupProductRuleWindow_Loaded);
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentSetupProductRuleWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(AssortmentSetupProductRuleWindow_Loaded);

            var factory =
            new PlanogramAssortmentProductColumnLayoutFactory(typeof(AssortmentSetupProductRuleRow),
                    null,
                    new List<Framework.Model.IModelPropertyInfo>() 
                    { 
                        Product.GtinProperty,
                        Product.NameProperty
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentProducts, /*PathMask*/"SourceProduct.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(xProductDataGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event handlers
        
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach(DataGridColumn column in BuildAssortmentProductColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }

            //add the delete row col
            columnSet.Insert(curIdx, new DataGridExtendedTemplateColumn
            {
                Header = String.Empty,
                CellTemplate = Resources["ProductRulesWindow_DTRemoveProductRuleColumn"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = false,
                CanUserHide = false,
                CanUserFreeze = false,
                ColumnGroupName = "",
                Width = 40,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            });            
        }
        
        #endregion

        #region Methods

        private ObservableCollection<DataGridColumn> BuildAssortmentProductColumnList()
        {
            //load the columnset
            ObservableCollection<DataGridColumn> columnList = new ObservableCollection<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    AssortmentProduct.GtinProperty.FriendlyName,
                    AssortmentSetupProductRuleRow.ProductGTINProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 80;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    AssortmentProduct.NameProperty.FriendlyName,
                    AssortmentSetupProductRuleRow.ProductNameProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            DataGridTextColumn col2 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Message.AssortmentSetupProductRules_ColumnRule,
                    AssortmentSetupProductRuleRow.RuleSummaryProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col2.MinWidth = 50;
            col2.Width = 185;
            columnList.Add(col2);

            return columnList;
        }

        #endregion

        #region Window Close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!this.ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   if (_columnLayoutManager != null)
                   {
                       _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                       _columnLayoutManager.Dispose();
                   }

                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        /// <summary>
        /// Method to force the EditValue and EditMaximum value textboxes to refresh if
        /// and invalid value was entered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExtendedTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.RefreshSelectedRows();
            }
        }

        /// <summary>
        /// This moves focus to the Product Edit controls when return key is pressed for 508 compliance   
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductRule_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;
                FocusNavigationDirection direction = FocusNavigationDirection.Down;
                TraversalRequest tRequest = new TraversalRequest(direction);
                senderControl?.MoveFocus(tRequest);
                e.Handled = true;
            }
        }
    }
}
