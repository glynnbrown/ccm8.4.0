﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupAddLocationsManuallyWindow.xaml
    /// </summary>
    public sealed partial class AssortmentSetupAddLocationsManuallyWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupAddLocationsManuallyViewModel),
            typeof(AssortmentSetupAddLocationsManuallyWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets the window viewmodel context
        /// </summary>
        public AssortmentSetupAddLocationsManuallyViewModel ViewModel
        {
            get { return (AssortmentSetupAddLocationsManuallyViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupAddLocationsManuallyWindow senderControl = (AssortmentSetupAddLocationsManuallyWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupAddLocationsManuallyViewModel oldModel = (AssortmentSetupAddLocationsManuallyViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                AssortmentSetupAddLocationsManuallyViewModel newModel = (AssortmentSetupAddLocationsManuallyViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assortmentModel"></param>
        public AssortmentSetupAddLocationsManuallyWindow(Assortment assortmentModel, LocationList masterLocationList)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();


            this.ViewModel = new AssortmentSetupAddLocationsManuallyViewModel(assortmentModel, masterLocationList);
            this.Loaded += AssortmentSetupAddLocationsManuallyWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentSetupAddLocationsManuallyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentSetupAddLocationsManuallyWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Int32[] initialVisibleMapIds = new Int32[]
            {
                LocationImportMappingList.CodeMapId,
                LocationImportMappingList.NameMapId,
                LocationImportMappingList.Address1MapId,
                LocationImportMappingList.Address2MapId,
                LocationImportMappingList.RegionMapId,
                LocationImportMappingList.CountyMapId,
                LocationImportMappingList.PostalCodeMapId,
                LocationImportMappingList.CityMapId,
                LocationImportMappingList.CountyMapId
            };

            //nb - we are removing the location group code and type name cols
            // as we are only using the location object as the source for this.
            Dictionary<Int32, String> specialCases = new Dictionary<Int32, String>();
            specialCases.Add(LocationImportMappingList.GroupCodeMapId, null);
        }


        private void AllLocationsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.AddLocationsCommand.Execute();
        }

        private void AllLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.AddedLocationsGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveLocationCommand.Execute();
                }
            }
        }

        private void AddedLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.AllLocationsGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.AddLocationsCommand.Execute();
                }
            }
        }

        private void LocationGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAddedLocation = senderControl?.Name == AddedLocationsGrid.Name;

            if (e.Key == Key.Return)
            {
                if (isAddedLocation) this.ViewModel.RemoveLocationCommand.Execute();
                else this.ViewModel.AddLocationsCommand.Execute();
                Keyboard.Focus(isAddedLocation ? AddedLocationsGrid : AllLocationsGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAddedLocation ? AllLocationsGrid : AddedLocationsGrid);
                e.Handled = true;
            }
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
     
    }
}
