﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2015

//#region Version History: (V8 8.3.0)
//// V8-31950 : A.Probyn
////  Created (not complete, and not released in 8.3.0). This will be addressed in a future version.
//#endregion
//#endregion

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Windows;
//using Galleria.Framework.Collections;
//using Galleria.Framework.Controls.Wpf;
//using Galleria.Framework.Helpers;
//using Galleria.Framework.ViewModel;
//using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
//using Galleria.Ccm.Model;
//using System.Globalization;
//using System.ComponentModel;
//using System.Collections.ObjectModel;
//using Galleria.Framework.Planograms.Model;

//namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance
//{
//    /// <summary>
//    /// ViewModel controller for copy rules window
//    /// </summary>
//    public sealed class AssortmentSetupCopyRulesViewModel : ViewModelAttachedControlObject<AssortmentSetupCopyRulesWindow>
//    {
//        #region Fields

//        private Assortment _currentAssortment;
//        private AssortmentRulesRow _currentAssortmentRow;

//        private ObservableCollection<AssortmentRulesRow> _availableAssortmentRows = new ObservableCollection<AssortmentRulesRow>();

//        private ObservableCollection<AssortmentRulesRow> _selectedDestinationAssortmentRows = new ObservableCollection<AssortmentRulesRow>();
//        private BulkObservableCollection<AssortmentRulesRow> _availableDestinationAssortmentRows = new BulkObservableCollection<AssortmentRulesRow>();
//        private ReadOnlyBulkObservableCollection<AssortmentRulesRow> _availableDestinationAssortmentRowsRO;
        
//        private Boolean _isProductRulesSelected = false;
//        private Boolean _isFamilyRulesSelected = false;
//        private Boolean _isInventoryRulesSelected = false;

//        #endregion

//        #region Binding Property Paths

//        //Properties
//        public static readonly PropertyPath CurrentAssortmentRowProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.CurrentAssortmentRow);
//        public static readonly PropertyPath AvailableDestinationAssortmentRowsProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.AvailableDestinationAssortmentRows);
//        public static readonly PropertyPath SelectedDestinationAssortmentRowsProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.SelectedDestinationAssortmentRows);
//        public static readonly PropertyPath IsProductRulesSelectedProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.IsProductRulesSelected);
//        public static readonly PropertyPath IsFamilyRulesSelectedProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.IsFamilyRulesSelected);
//        public static readonly PropertyPath IsInventoryRulesSelectedProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.IsInventoryRulesSelected);

//        //Commands
//        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.OkCommand);
//        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.CancelCommand);
//        public static readonly PropertyPath RemoveRuleCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.RemoveRuleCommand);
//        public static readonly PropertyPath SelectAllCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.SelectAllCommand);
//        public static readonly PropertyPath ClearAllCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupCopyRulesViewModel>(p => p.ClearAllCommand);

//        #endregion

//        #region Properties

//        /// <summary>
//        /// Gets/Sets the selected range model
//        /// </summary>
//        public AssortmentRulesRow CurrentAssortmentRow
//        {
//            get { return _currentAssortmentRow; }
//            set
//            {
//                _currentAssortmentRow = value;
//                OnPropertyChanged(CurrentAssortmentRowProperty);

//                if (_currentAssortmentRow != null)
//                {
//                    this.IsProductRulesSelected = _currentAssortmentRow.HasProductRules;
//                    this.IsFamilyRulesSelected = _currentAssortmentRow.HasFamilyRules;
//                    this.IsInventoryRulesSelected = _currentAssortmentRow.HasInventoryRules;

//                    //Reset current range nodes, remove selected
//                    _availableDestinationAssortmentRows.Clear();
//                    _availableDestinationAssortmentRows.AddRange(_availableAssortmentRows);
//                    _availableDestinationAssortmentRows.Remove(_currentAssortmentRow);
//                }
//            }
//        }

//        /// <summary>
//        /// Returns the collection of current model ranges
//        /// </summary>
//        public ReadOnlyBulkObservableCollection<AssortmentRulesRow> AvailableDestinationAssortmentRows
//        {
//            get
//            {
//                if (_availableDestinationAssortmentRowsRO == null)
//                {
//                    _availableDestinationAssortmentRowsRO = new ReadOnlyBulkObservableCollection<AssortmentRulesRow>(_availableDestinationAssortmentRows);
//                }
//                return _availableDestinationAssortmentRowsRO;
//            }
//        }

//        /// <summary>
//        /// Gets/Sets the selected current model ranges
//        /// </summary>
//        public ObservableCollection<AssortmentRulesRow> SelectedDestinationAssortmentRows
//        {
//            get { return _selectedDestinationAssortmentRows; }
//            set
//            {
//                _selectedDestinationAssortmentRows = value;
//                OnPropertyChanged(SelectedDestinationAssortmentRowsProperty);
//            }
//        }

//        /// <summary>
//        /// Gets/Sets If the product rules are to be copied
//        /// </summary>
//        public Boolean IsProductRulesSelected
//        {
//            get { return _isProductRulesSelected; }
//            set
//            {
//                _isProductRulesSelected = value;
//                OnPropertyChanged(IsProductRulesSelectedProperty);
//            }
//        }

//        /// <summary>
//        /// Gets/Sets If the family rules are to be copied
//        /// </summary>
//        public Boolean IsFamilyRulesSelected
//        {
//            get { return _isFamilyRulesSelected; }
//            set
//            {
//                _isFamilyRulesSelected = value;
//                OnPropertyChanged(IsFamilyRulesSelectedProperty);
//            }
//        }

//        /// <summary>
//        /// Gets/Sets If the inventory rules are to be copied
//        /// </summary>
//        public Boolean IsInventoryRulesSelected
//        {
//            get { return _isInventoryRulesSelected; }
//            set
//            {
//                _isInventoryRulesSelected = value;
//                OnPropertyChanged(IsInventoryRulesSelectedProperty);
//            }
//        }

//        #endregion

//        #region Constructor

//        /// <summary>
//        /// Testing Constructor
//        /// </summary>
//        /// <param name="currentScenario"></param>
//        public AssortmentSetupCopyRulesViewModel(Assortment currentAssortment)
//        {
//            _currentAssortment = currentAssortment;

//            ReloadAvailableAssortmentRules();

//            this.CurrentAssortmentRow = new AssortmentRulesRow(currentAssortment);;
//        }

//        #endregion

//        #region Commands

//        #region Ok

//        private RelayCommand _okCommand;

//        /// <summary>
//        /// Applies the changes to the current item
//        /// </summary>
//        public RelayCommand OkCommand
//        {
//            get
//            {
//                if (_okCommand == null)
//                {
//                    _okCommand = new RelayCommand(
//                        p => Ok_Executed(),
//                        p => Ok_CanExecute())
//                    {
//                        FriendlyName = Message.Generic_Ok, 
//                        FriendlyDescription = Message.Generic_Apply_Tooltip,
//                        Icon = ImageResources.Apply_32,
//                        SmallIcon = ImageResources.Apply_16
//                    };
//                    base.ViewModelCommands.Add(_okCommand);
//                }
//                return _okCommand;
//            }
//        }

//        private Boolean Ok_CanExecute()
//        {
//            _okCommand.DisabledReason = null;

//            if (this.CurrentAssortmentRow != null && !this.CurrentAssortmentRow.HasRules)
//            {
//                return false;
//            }

//            if (!this.IsProductRulesSelected && !this.IsFamilyRulesSelected && !this.IsInventoryRulesSelected)
//            {
//                return false;
//            }

//            if (this.AvailableDestinationAssortmentRows.Where(p => p.IsSelected).Count() == 0)
//            {
//                return false;
//            }            

//            return true;
//        }

//        private void Ok_Executed()
//        {
//            //ModalMessage overwriteWarning = new ModalMessage();
//            //overwriteWarning.Header = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupCopyRules_ApplyHeader, this.CurrentAssortmentRow.Node.FullName);
//            //overwriteWarning.Description = Message.AssortmentSetupCopyRules_ApplyDescription;
//            //overwriteWarning.MessageIcon = ImageResources.Warning_32;
//            //overwriteWarning.ButtonCount = 2;
//            //overwriteWarning.Button1Content = Message.Generic_Ok;
//            //overwriteWarning.Button2Content = Message.Generic_Cancel;
//            //overwriteWarning.DefaultButton = ModalMessageButton.Button1;
//            //overwriteWarning.CancelButton = ModalMessageButton.Button2;
//            //App.ShowWindow(overwriteWarning, AttachedControl, true);

//            //Boolean continueWithApplyingRules = (overwriteWarning.Result == ModalMessageResult.Button1);

//            //if (continueWithApplyingRules)
//            //{
//            //    ApplyRules();

//            //    if (this.AttachedControl != null)
//            //    {
//            //        this.AttachedControl.Close();
//            //    }
//            //}
//        }

//        /// <summary>
//        /// Applies changes made to the current item.
//        /// </summary>
//        private void ApplyRules()
//        {
//            base.ShowWaitCursor(true);

//            //foreach (AssortmentRulesRow rules in _AvailableDestinationAssortmentRows.Where(p => p.IsSelected))
//            //{
//            //    if (this.IsProductRulesSelected)
//            //    {
//            //        rules.Node.ProductRules.ApplyRules(this.CurrentAssortmentRow.Node.ProductRules);
//            //    }
//            //    if (this.IsFamilyRulesSelected)
//            //    {
//            //        rules.Node.FamilyRules.AppyRules(this.CurrentAssortmentRow.Node.FamilyRules);
//            //    }
//            //    if (this.IsInventoryRulesSelected)
//            //    {
//            //        //Apply rules with the CDT node refresh supressed, as we update them all once after this for loop.
//            //        rules.Node.InventoryRules.AppyRules(this.CurrentAssortmentRow.Node.InventoryRules, true);
//            //    }
//            //}

//            ////refresh CDT as apply rules has the CDT node refresh supressed
//            //if (this.IsInventoryRulesSelected)
//            //{
//            //    if (this._currentScenario != null && this._currentScenario.ConsumerDecisionTree != null)
//            //    {
//            //        List<ScenarioConsumerDecisionTreeNode> cdtNodes = this._currentScenario.ConsumerDecisionTree.FetchAllNodes().ToList();
//            //        //Update all related ScenarioProductUniverseProducts
//            //        cdtNodes.ForEach(n => n.UpdateRuleIndicator(false, false, false, false, true, false));

//            //    }
//            //}
            
//            base.ShowWaitCursor(false);
//        }

//        #endregion
        
//        #region Cancel

//        private RelayCommand _cancelCommand;

//        /// <summary>
//        /// Cancels changes and closes the window
//        /// </summary>
//        public RelayCommand CancelCommand
//        {
//            get
//            {
//                if (_cancelCommand == null)
//                {
//                    _cancelCommand = new RelayCommand(
//                        p => Cancel_Executed())
//                    {
//                        FriendlyName = Message.Generic_Cancel,
//                    };
//                    base.ViewModelCommands.Add(_cancelCommand);
//                }
//                return _cancelCommand;
//            }
//        }

//        private void Cancel_Executed()
//        {
//            if (this.AttachedControl != null)
//            {
//                this.AttachedControl.Close();
//            }
//        }

//        #endregion

//        #region RemoveRule

//        private RelayCommand _removeRuleCommand;

//        /// <summary>
//        /// Removes the given rule
//        /// </summary>
//        public RelayCommand RemoveRuleCommand
//        {
//            get
//            {
//                if (_removeRuleCommand == null)
//                {
//                    _removeRuleCommand = new RelayCommand(
//                        p => RemoveRule_Executed(p),
//                        p => RemoveRule_CanExecute(p))
//                    {
//                        FriendlyName = Message.Generic_Remove,
//                        SmallIcon = ImageResources.Delete_16
//                    };
//                    base.ViewModelCommands.Add(_removeRuleCommand);

//                }
//                return _removeRuleCommand;
//            }
//        }

//        private Boolean RemoveRule_CanExecute(Object arg)
//        {
//            AssortmentRulesRow row = arg as AssortmentRulesRow;

//            if (row == null)
//            {
//                return false;
//            }

//            if (!row.HasRules)
//            {
//                return false;
//            }

//            return true;
//        }

//        private void RemoveRule_Executed(Object arg)
//        {
//            //ModalMessage removeWarning = new ModalMessage();
//            //removeWarning.Header = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetupCopyRules_RemoveHeader, this.CurrentAssortmentRow.Node.FullName);
//            //removeWarning.Description = Message.AssortmentSetupCopyRules_RemoveDescription;
//            //removeWarning.MessageIcon = ImageResources.Warning_32;
//            //removeWarning.ButtonCount = 2;
//            //removeWarning.Button1Content = Message.Generic_Ok;
//            //removeWarning.Button2Content = Message.Generic_Cancel;
//            //removeWarning.DefaultButton = ModalMessageButton.Button1;
//            //removeWarning.CancelButton = ModalMessageButton.Button2;
//            //App.ShowWindow(removeWarning, AttachedControl, true);

//            //Boolean continueWithApplyingRules = (removeWarning.Result == ModalMessageResult.Button1);

//            //if (continueWithApplyingRules)
//            //{
//            //    AssortmentRulesRow row = arg as AssortmentRulesRow;
//            //    //row.Node.ProductRules.RemoveList(row.Node.ProductRules.ToList());
//            //    //row.Node.FamilyRules.RemoveList(row.Node.FamilyRules.ToList());
//            //    //row.Node.InventoryRules.RemoveList(row.Node.InventoryRules.ToList());

//            //    ReloadAvailableAssortmentRules();
//            //    //this.CurrentAssortmentRow = _scenarioModelNodeRows.Where(p => p.Node.ScenarioModelNodeId.Equals(row.Node.ScenarioModelNodeId)).FirstOrDefault();
//            //}
//        }

//        #endregion

//        #region SelectAll

//        private RelayCommand _selectAllCommand;

//        /// <summary>
//        /// Selected all of the nodes for the given rule
//        /// </summary>
//        public RelayCommand SelectAllCommand
//        {
//            get
//            {
//                if (_selectAllCommand == null)
//                {
//                    _selectAllCommand = new RelayCommand(
//                        p => SelectAll_Executed(),
//                        p => SelectAll_CanExecute())
//                    {
//                        FriendlyName = Message.AssortmentSetupCopyRules_SelectAll,
//                        FriendlyDescription = Message.AssortmentSetupCopyRules_SelectAllDescription,
//                        SmallIcon = ImageResources.AssortmentSetupCopyRules_SelectAll
//                    };
//                    base.ViewModelCommands.Add(_selectAllCommand);

//                }
//                return _selectAllCommand;
//            }
//        }

//        private Boolean SelectAll_CanExecute()
//        {
//            return this.AvailableDestinationAssortmentRows.Count > 0;
//        }

//        private void SelectAll_Executed()
//        {
//            foreach (AssortmentRulesRow row in this.AvailableDestinationAssortmentRows)
//            {
//                row.IsSelected = true;
//            }
//        }

//        #endregion

//        #region ClearAll

//        private RelayCommand _clearAllCommand;

//        /// <summary>
//        /// Cleared all of the nodes for the given rule
//        /// </summary>
//        public RelayCommand ClearAllCommand
//        {
//            get
//            {
//                if (_clearAllCommand == null)
//                {
//                    _clearAllCommand = new RelayCommand(
//                        p => ClearAll_Executed(),
//                        p => ClearAll_CanExecute())
//                    {
//                        FriendlyName = Message.AssortmentSetupCopyRules_ClearAll,
//                        FriendlyDescription = Message.AssortmentSetupCopyRules_ClearAllDescription,
//                        SmallIcon = ImageResources.AssortmentSetupCopyRules_ClearAll
//                    };
//                    base.ViewModelCommands.Add(_clearAllCommand);

//                }
//                return _clearAllCommand;
//            }
//        }

//        private Boolean ClearAll_CanExecute()
//        {
//            return this.AvailableDestinationAssortmentRows.Count > 0;
//        }

//        private void ClearAll_Executed()
//        {
//            foreach (AssortmentRulesRow row in this.AvailableDestinationAssortmentRows)
//            {
//                row.IsSelected = false;
//            }
//        }

//        #endregion

//        #endregion

//        #region Methods
        
//        /// <summary>
//        /// Recreates the available assortments
//        /// </summary>
//        private void ReloadAvailableAssortmentRules()
//        {
            
//        }

//        #endregion
        
//        #region IDisposable

//        protected override void Dispose(Boolean disposing)
//        {
//            if (!base.IsDisposed)
//            {
//                if (disposing)
//                {
//                    _currentAssortment = null;
//                    _currentAssortmentRow = null;
//                    _selectedDestinationAssortmentRows.Clear();
//                    _selectedDestinationAssortmentRows = null;
//                    _availableDestinationAssortmentRows.Clear();
//                    _availableDestinationAssortmentRows = null;
//                }

//                base.IsDisposed = true;
//            }
//        }

//        #endregion
//    }

//    /// <summary>
//    /// Represents a row on the copy rules screen
//    /// </summary>
//    public sealed class AssortmentRulesRow : ViewModelObject
//    {
//        #region Fields
//        private Assortment _assortment;
//        private Boolean _isSelected;

//        //Product rule totals
//        private Int16 _productRule_ForceCount = 0;
//        private Int16 _productRule_PreserveCount = 0;
//        private Int16 _productRule_MinimumHurdleCount = 0;
//        private Int16 _productRule_CoreCount = 0;

//        //Family rule totals
//        private Int16 _familyRule_MinProductCount = 0;
//        private Int16 _familyRule_MaxProductCount = 0;
//        private Int16 _familyRule_DependencyList = 0;
//        private Int16 _familyRule_Delist = 0;

//        #endregion

//        #region Binding Property Paths

//        public static readonly PropertyPath AssortmentProperty = WpfHelper.GetPropertyPath<AssortmentRulesRow>(p => p.Assortment);
//        public static readonly PropertyPath IsSelectedProperty = WpfHelper.GetPropertyPath<AssortmentRulesRow>(p => p.IsSelected);

//        #endregion

//        #region Properties

//        /// <summary>
//        /// Gets the assortment this row relates to
//        /// </summary>
//        public Assortment Assortment
//        {
//            get { return _assortment; }
//        }


//        /// <summary>
//        /// Returns true if product rules exist.
//        /// </summary>
//        public Boolean HasProductRules
//        {
//            get
//            {
//                return
//                    (_productRule_ForceCount > 0 ||
//                    _productRule_PreserveCount > 0 ||
//                    _productRule_MinimumHurdleCount > 0 ||
//                    _productRule_CoreCount > 0);
//            }
//        }

//        public String ProductRuleOverview
//        {
//            get
//            {
//                if (this.HasProductRules)
//                {
//                    // {0} Force, {1} Preserve, {2} Min Hurdle, {3} Core : Product rules
//                    return String.Format(Message.AssortmentSetupCopyRules_ProductRulesOverview, 
//                                        _productRule_ForceCount, _productRule_PreserveCount, 
//                                        _productRule_MinimumHurdleCount, _productRule_CoreCount);
//                }
//                else
//                {
//                    return String.Empty;
//                }
//            }
//        }

//        /// <summary>
//        /// Returns true if family rules exist.
//        /// </summary>
//        public Boolean HasFamilyRules
//        {
//            get
//            {
//                return
//                    (_familyRule_MinProductCount > 0 ||
//                    _familyRule_MaxProductCount > 0 ||
//                    _familyRule_DependencyList > 0 ||
//                    _familyRule_Delist > 0);
//            }
//        }

//        public String FamilyRuleOverview
//        {
//            get
//            {
//                if (this.HasFamilyRules)
//                {
//                    // {0} Min Product Count, {1} Max Product Count, {2} Dependency List, {3} Delist : Family rules
//                    return String.Format(Message.AssortmentSetupCopyRules_FamilyRulesOverview,
//                                        _familyRule_MinProductCount, _familyRule_MaxProductCount,
//                                        _familyRule_DependencyList, _familyRule_Delist);
//                }
//                else
//                {
//                    return String.Empty;
//                }
//            }
//        }

//        /// <summary>
//        /// Returns true if inventory rules exist.
//        /// </summary>
//        public Boolean HasInventoryRules
//        {
//            get
//            {
//                return
//                    (_assortment.InventoryRules.Count > 0);
//            }
//        }

//        public String InventoryRuleOverview
//        {
//            get
//            {
//                if (this.HasInventoryRules)
//                {
//                    // {0} Inventory override rules
//                    return String.Format(Message.AssortmentSetupCopyRules_InventoryRulesOverview,
//                                        _assortment.InventoryRules.Count);
//                }
//                else
//                {
//                    return String.Empty;
//                }
//            }
//        }

//        /// <summary>
//        /// Returns true if the model node contains any rule.
//        /// </summary>
//        public Boolean HasRules
//        {
//            get
//            {
//                return (this.HasProductRules || this.HasFamilyRules || this.HasInventoryRules);
//            }
//        }


//        /// <summary>
//        /// Returns true if the row is dirty
//        /// </summary>
//        public Boolean IsSelected
//        {
//            get { return _isSelected; }
//            set
//            {
//                _isSelected = value;
//                OnPropertyChanged(IsSelectedProperty);
//            }
//        }

//        #endregion

//        #region Constructor

//        public AssortmentRulesRow(Assortment node)
//        {
//            _assortment = node;

//            // Get Assortment Products Product Rule detail
//            foreach (AssortmentProduct productWithProductRule in _assortment.Products.Where(p => p.RuleType != PlanogramAssortmentProductRuleType.None))
//            {
//                switch (productWithProductRule.RuleType)
//                {
//                    case PlanogramAssortmentProductRuleType.Force:
//                        _productRule_ForceCount++;
//                        break;
//                    case PlanogramAssortmentProductRuleType.Preserve:
//                        _productRule_PreserveCount++;
//                        break;
//                    case PlanogramAssortmentProductRuleType.MinimumHurdle:
//                        _productRule_MinimumHurdleCount++;
//                        break;
//                    case PlanogramAssortmentProductRuleType.Core:
//                        _productRule_CoreCount++;
//                        break;
//                    default:
//                        break;

//                }
//            }

//            // Get Assortment Products Family Rule detail
//            foreach (AssortmentProduct productWithFamilyRule in _assortment.Products.Where(p => p.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None))
//            {
//                switch (productWithFamilyRule.FamilyRuleType)
//                {
//                    case PlanogramAssortmentProductFamilyRuleType.MinimumProductCount:
//                        _familyRule_MinProductCount++;
//                        break;
//                    case PlanogramAssortmentProductFamilyRuleType.MaximumProductCount:
//                        _familyRule_MaxProductCount++;
//                        break;
//                    case PlanogramAssortmentProductFamilyRuleType.DependencyList:
//                        _familyRule_DependencyList++;
//                        break;
//                    //case PlanogramAssortmentProductFamilyRuleType.Delist:
//                    //    _familyRule_Delist++;
//                    //    break;
//                    default:
//                        break;
//                }
//            }
//        }

//        #endregion

//        #region IDisposable Members

//        protected override void Dispose(Boolean disposing)
//        {
//            if (!base.IsDisposed)
//            {
//                if (disposing)
//                {
//                    _assortment = null;
//                }
//            }
//        }

//        #endregion
//    }
//}