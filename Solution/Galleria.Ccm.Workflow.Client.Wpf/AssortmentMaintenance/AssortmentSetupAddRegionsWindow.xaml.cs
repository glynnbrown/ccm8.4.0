﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#region Version History: (CCM 8.3.0)
// CCM-18480 : D.Pleasance
//  Amended location grids to be of type AssortmentLocationView so that all location attributes can be selected.
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Collections.Generic;
using Galleria.Ccm.Common.Wpf;
using System.Windows.Controls;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for RegionWindow.xaml
    /// </summary>
    public sealed partial class AssortmentSetupAddRegionsWindow : ExtendedRibbonWindow
    {
        const String RemoveSelectedRegionCommandKey = "RegionRemoveSelectedRegionCommand";

        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupAddRegionsViewModel),
            typeof(AssortmentSetupAddRegionsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentSetupAddRegionsViewModel ViewModel
        {
            get { return (AssortmentSetupAddRegionsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupAddRegionsWindow senderControl =
                (AssortmentSetupAddRegionsWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupAddRegionsViewModel oldModel = (AssortmentSetupAddRegionsViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(RemoveSelectedRegionCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentSetupAddRegionsViewModel newModel = (AssortmentSetupAddRegionsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(RemoveSelectedRegionCommandKey, newModel.RemoveSelectedRegionCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentSetupAddRegionsWindow(AssortmentSetupAddRegionsViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;

            this.Loaded += RegionWindow_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(RegionWindow_Loaded);

            var factory =
            new AssortmentLocationCustomColumnLayoutFactory(typeof(AssortmentLocationView),
                    new List<Framework.Model.IModelPropertyInfo>()
                    {
                        Location.CodeProperty,
                        Location.NameProperty
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentLocations, /*pathmask*/ "Location.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(this.xAvailableLocationsGrid);
            _columnLayoutManager.AttachDataGrid(this.xTakenLocationsGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentLocationColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }
        }

        private void XRegionsRowGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;
                FocusNavigationDirection direction = FocusNavigationDirection.Down;
                TraversalRequest tRequest = new TraversalRequest(direction);
                senderControl?.MoveFocus(tRequest);
                e.Handled = true;
            }
        }

        private void ProductGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAssigned = senderControl?.Name == xTakenLocationsGrid.Name;

            if (e.Key == Key.Return)
            {
                if (isAssigned) this.ViewModel?.RemoveSelectedLocationsCommand.Execute();
                else this.ViewModel?.AddSelectedLocationsCommand.Execute();
                Keyboard.Focus(isAssigned ? xTakenLocationsGrid : xAvailableLocationsGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAssigned ? xAvailableLocationsGrid : xTakenLocationsGrid);
                e.Handled = true;
            }
        }

        #endregion

        #region Methods

        private List<DataGridColumn> BuildAssortmentLocationColumnList()
        {
            //load the columnset
            List<DataGridColumn> columnList = new List<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Location.CodeProperty.FriendlyName,
                    AssortmentLocationView.LocationCodeProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 50;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    Location.NameProperty.FriendlyName,
                    AssortmentLocationView.LocationNameProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            return columnList;
        }

        #endregion

        #region Window Close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!this.ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
      
    }
}