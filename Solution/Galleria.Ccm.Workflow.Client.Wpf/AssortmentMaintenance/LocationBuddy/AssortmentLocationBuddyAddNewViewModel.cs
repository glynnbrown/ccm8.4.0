﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.Globalization;

using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Csla.Core;

using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Collections;
using System.Collections.Specialized;
using System.Windows.Controls;
using System.ComponentModel;
using System.Windows.Input;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    public sealed class AssortmentLocationBuddyAddNewViewModel : ViewModelAttachedControlObject<AssortmentLocationBuddyAddNewWindow>
    {
        #region Fields
        private Assortment _currentAssortment;
        private BulkObservableCollection<AssortmentLocation> _availableTargetLocations = new BulkObservableCollection<AssortmentLocation>();
        private ReadOnlyBulkObservableCollection<AssortmentLocation> _availableTargetLocationsRO;
        private AssortmentLocation _selectedTargetLocation;
        private BulkObservableCollection<LocationInfo> _availableSourceLocations = new BulkObservableCollection<LocationInfo>();
        private ReadOnlyBulkObservableCollection<LocationInfo> _availableSourceLocationsRO;
        private LocationInfo _selectedSourceLocation;
        private PlanogramAssortmentLocationBuddyTreatmentType _selectedTreatmentType;
        private BulkObservableCollection<AssortmentLocationBuddyRow> _assortmentLocationBuddyRows = new BulkObservableCollection<AssortmentLocationBuddyRow>();
        private AssortmentLocationBuddy _currentTargetLocationBuddy;          //If the target has an existing buddy, it is recorded in this variable to save searching for it again later
        private IEnumerable<LocationInfo> _orderedList;
        public const Single MaxBuddyUtilisationPercentage = 100;
        private Boolean _isDirty;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedTargetLocationProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.SelectedTargetLocation);
        public static readonly PropertyPath AvailableSourceLocationsProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.AvailableSourceLocations);
        public static readonly PropertyPath SelectedSourceLocationProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.SelectedSourceLocation);
        public static readonly PropertyPath SelectedTreatmentTypeProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.SelectedTreatmentType);
        public static readonly PropertyPath AssortmentLocationBuddyRowsProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.AssortmentLocationBuddyRows);

        //Commands
        public static readonly PropertyPath AddCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.AddCommand);
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath RemoveSourceLocationCommandProperty = WpfHelper.GetPropertyPath<AssortmentLocationBuddyAddNewViewModel>(p => p.RemoveSourceLocationCommand);


        #endregion

        #region Properties

        /// <summary>
        /// Returns a readonly collection of available locations (for target selection)
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentLocation> AvailableLocations
        {
            get
            {
                if (_availableTargetLocationsRO == null)
                {
                    _availableTargetLocationsRO = new ReadOnlyBulkObservableCollection<AssortmentLocation>(_availableTargetLocations);
                }
                return _availableTargetLocationsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected target location
        /// </summary>
        public AssortmentLocation SelectedTargetLocation
        {
            get { return _selectedTargetLocation; }
            set
            {
                OnSelectedTargetLocationChanged(value);
                OnPropertyChanged(SelectedTargetLocationProperty);
            }
        }

        /// <summary>
        /// Returns a readonly collection of available locations (for source selection)
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationInfo> AvailableSourceLocations
        {
            get
            {
                if (_availableSourceLocationsRO == null)
                {
                    _availableSourceLocationsRO = new ReadOnlyBulkObservableCollection<LocationInfo>(_availableSourceLocations);
                }
                return _availableSourceLocationsRO;
            }
        }

        /// <summary>
        /// Retruns the selected source location (in ComboBox)
        /// </summary>
        public LocationInfo SelectedSourceLocation
        {
            get { return _selectedSourceLocation; }
            set
            {
                _selectedSourceLocation = value;
                OnPropertyChanged(SelectedSourceLocationProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected treatment type
        /// </summary>
        public PlanogramAssortmentLocationBuddyTreatmentType SelectedTreatmentType
        {
            get { return _selectedTreatmentType; }
            set
            {
                _selectedTreatmentType = value;
                OnPropertyChanged(SelectedTreatmentTypeProperty);
            }
        }

        public BulkObservableCollection<AssortmentLocationBuddyRow> AssortmentLocationBuddyRows
        {
            get { return _assortmentLocationBuddyRows; }
        }

        /// <summary>
        /// Checks whether the object is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor; creates the view model
        /// </summary>
        public AssortmentLocationBuddyAddNewViewModel(Assortment currentAssortment, AssortmentLocation selectedLocation)
        {
            _assortmentLocationBuddyRows.BulkCollectionChanged += AssortmentLocationBuddyRows_BulkCollectionChanged;
            _currentAssortment = currentAssortment;

            //initialise location lists
            _orderedList = LocationInfoList.FetchByEntityId(App.ViewState.EntityId).OrderBy(f => f.Code);
            _availableTargetLocations.AddRange(_currentAssortment.Locations);
            _availableSourceLocations.AddRange(_orderedList);

            //select the first items in lists
            if (selectedLocation != null)
            {
                SelectedTargetLocation = selectedLocation;
            }
            else
            {
                SelectedTargetLocation = this.AvailableLocations.FirstOrDefault();
            }
            SelectedSourceLocation = this.AvailableSourceLocations.FirstOrDefault();

            _selectedTreatmentType = PlanogramAssortmentLocationBuddyTreatmentType.Avg;
        }

        #endregion

        #region Methods

        void LoadExistingBuddy(AssortmentLocationBuddy TargetToLoad)
        {
            //initialise lists
            List<Object> _locationIdsToLookup = new List<Object>();                 //used to store ids of locations to find in the LocationList
            List<LocationInfo> _foundLocations = new List<LocationInfo>();  //used to store locations after they've been found by their ID

            SelectedTreatmentType = TargetToLoad.TreatmentType;

            //construct list of locations to lookup
            if (TargetToLoad.S1LocationId != null)
            {
                _locationIdsToLookup.Add(TargetToLoad.S1LocationId);
            }
            if (TargetToLoad.S2LocationId != null)
            {
                _locationIdsToLookup.Add(TargetToLoad.S2LocationId);
            }
            if (TargetToLoad.S3LocationId != null)
            {
                _locationIdsToLookup.Add(TargetToLoad.S3LocationId);
            }
            if (TargetToLoad.S4LocationId != null)
            {
                _locationIdsToLookup.Add(TargetToLoad.S4LocationId);
            }
            if (TargetToLoad.S5LocationId != null)
            {
                _locationIdsToLookup.Add(TargetToLoad.S5LocationId);
            }

            //Initialise foundLocations indexes for later use
            for (Int32 index = 0; index < _locationIdsToLookup.Count; index++)
            {
                _foundLocations.Add(null);
            }

            //lookup AssortmentLocations by linked Ids
            //for every location
            foreach (LocationInfo location in _availableSourceLocations)
            {
                //compare to all Ids that need finding
                for (Int32 index = 0; index < _locationIdsToLookup.Count; index++)
                {
                    if (location.Id.Equals(_locationIdsToLookup[index]))
                    {
                        //if found, add to foundLocations list, matching order of locationIDs (important for matching correct utilisation to location)
                        _foundLocations[index] = location;
                        //Each location can only have one buddy, so LocationIDsToLookup are all unique. If one locations matches, none other will.
                        break;
                    }
                }
                //Check if all locations have been matched; if so, exit loop.
                Boolean allLocationIDsFound = !(_foundLocations.Contains(null));
                if (allLocationIDsFound)
                {
                    break;
                }
            }

            //dictionary to convert index to utilisation percentage
            Dictionary<Int32, Single?> convertIndexToPercentage = new Dictionary<Int32, Single?>();
            convertIndexToPercentage.Add(0, TargetToLoad.S1Percentage);
            convertIndexToPercentage.Add(1, TargetToLoad.S2Percentage);
            convertIndexToPercentage.Add(2, TargetToLoad.S3Percentage);
            convertIndexToPercentage.Add(3, TargetToLoad.S4Percentage);
            convertIndexToPercentage.Add(4, TargetToLoad.S5Percentage);

            //create buddyrows using foundLocation and their utilisation percentage
            for (Int32 index = 0; index < _foundLocations.Count; index++)
            {
                AssortmentLocationBuddyRows.Add(new AssortmentLocationBuddyRow(_foundLocations[index], (Single)(convertIndexToPercentage[index] * MaxBuddyUtilisationPercentage)));
            }

        }

        private void OnSelectedTargetLocationChanged(AssortmentLocation value)
        {
            //Change selected target location to new location
            _selectedTargetLocation = value;

            //reset buddyRows
            _assortmentLocationBuddyRows.Clear();

            //check if a LocationBuddy already exists for selected target location
            _currentTargetLocationBuddy = AssortmentLocationBuddy.NewAssortmentLocationBuddy();
            foreach (AssortmentLocationBuddy LocationBuddy in _currentAssortment.LocationBuddies)
            {
                if (LocationBuddy.LocationId.Equals(_selectedTargetLocation.LocationId))
                {
                    //save existing target location's LocationBuddy
                    _currentTargetLocationBuddy = LocationBuddy;
                    //load the existing buddy into the form
                    LoadExistingBuddy(_currentTargetLocationBuddy);
                    break;
                }
            }
            SelectedSourceLocation = AvailableSourceLocations.FirstOrDefault();
        }

        #endregion

        #region Commands

        #region Add

        private RelayCommand _addCommand;

        /// <summary>
        /// Discards changes and closes the window
        /// </summary>
        public RelayCommand AddCommand
        {
            get
            {
                //Lazy load
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand
                        (
                            p => Add_Executed(),
                            p => Add_CanExecute()
                        )
                    {
                        FriendlyName = Message.Generic_Add,
                    };

                    base.ViewModelCommands.Add(_addCommand);
                }
                return _addCommand;
            }

        }

        private void Add_Executed()
        {
            _assortmentLocationBuddyRows.Add(new AssortmentLocationBuddyRow(_selectedSourceLocation, 100));
            _availableSourceLocations.Remove(_selectedSourceLocation);
            SelectedSourceLocation = AvailableSourceLocations.FirstOrDefault();
            this._isDirty = true;
        }

        private Boolean Add_CanExecute()
        {
            if (_assortmentLocationBuddyRows.Count == 5)
            {
                this.AddCommand.DisabledReason = Message.AssortmentLocationBuddy_AddNew_MaxBuddiesAdded;
                return false;
            }
            if (_selectedTargetLocation == null)
            {
                this.AddCommand.DisabledReason = Message.AssortmentLocationBuddy_NoTargetsAvailable;
                return false;
            }
            if (_selectedSourceLocation == null)
            {
                this.AddCommand.DisabledReason = Message.AssortmentLocationBuddy_NoSourcesAvailable;
                return false;
            }
            return true;
        }


        #endregion

        #region OK

        private RelayCommand _okCommand;
        /// <summary>
        /// Accepts changes and closes the window
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                //Lazy load
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand
                        (
                            p => OK_Executed(),
                            p => OK_CanExecute()
                        )
                    {
                        FriendlyName = Message.Generic_Ok,
                    };

                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }

        }

        private Boolean OK_CanExecute()
        {
            //Check at least 1 row exists
            if (_selectedTargetLocation == null)
            {
                this.AddCommand.DisabledReason = Message.AssortmentLocationBuddy_NoTargetsAvailable;
                return false;
            }
            if (_assortmentLocationBuddyRows.Count == 0)
            {
                this.OKCommand.DisabledReason = Message.AssortmentLocationBuddy_NoBuddiesAdded;
                return false;
            }
            else
            {
                //checks row utilisation
                foreach (AssortmentLocationBuddyRow buddyRow in _assortmentLocationBuddyRows)
                {
                    if (buddyRow.Utilisation > 1000 || buddyRow.Utilisation < 1)
                    {
                        this.OKCommand.DisabledReason = String.Format(CultureInfo.CurrentCulture, Message.AssortmentLocationBuddy_BadPercentage, buddyRow.Location.Name);
                        return false;
                    }
                }
            }
            return true;
        }

        private void OK_Executed()
        {
            AssortmentLocationBuddy newAssortmentLocationBuddy = AssortmentLocationBuddy.NewAssortmentLocationBuddy();
            newAssortmentLocationBuddy.LocationId = _selectedTargetLocation.LocationId;
            newAssortmentLocationBuddy.TreatmentType = _selectedTreatmentType;
            newAssortmentLocationBuddy.S1LocationId = _assortmentLocationBuddyRows[0].Location.Id;
            newAssortmentLocationBuddy.S1Percentage = _assortmentLocationBuddyRows[0].Utilisation / MaxBuddyUtilisationPercentage;

            if (_assortmentLocationBuddyRows.Count > 1)
            {
                newAssortmentLocationBuddy.S2LocationId = _assortmentLocationBuddyRows[1].Location.Id;
                newAssortmentLocationBuddy.S2Percentage = _assortmentLocationBuddyRows[1].Utilisation / MaxBuddyUtilisationPercentage;
                if (_assortmentLocationBuddyRows.Count > 2)
                {
                    newAssortmentLocationBuddy.S3LocationId = _assortmentLocationBuddyRows[2].Location.Id;
                    newAssortmentLocationBuddy.S3Percentage = _assortmentLocationBuddyRows[2].Utilisation / MaxBuddyUtilisationPercentage;
                    if (_assortmentLocationBuddyRows.Count > 3)
                    {
                        newAssortmentLocationBuddy.S4LocationId = _assortmentLocationBuddyRows[3].Location.Id;
                        newAssortmentLocationBuddy.S4Percentage = _assortmentLocationBuddyRows[3].Utilisation / MaxBuddyUtilisationPercentage;
                        if (_assortmentLocationBuddyRows.Count > 4)
                        {
                            newAssortmentLocationBuddy.S5LocationId = _assortmentLocationBuddyRows[4].Location.Id;
                            newAssortmentLocationBuddy.S5Percentage = _assortmentLocationBuddyRows[4].Utilisation / MaxBuddyUtilisationPercentage;
                        }
                    }
                }
            }

            if (_currentTargetLocationBuddy != null)
            {
                _currentAssortment.LocationBuddies.Remove(_currentTargetLocationBuddy);
            }

            _currentAssortment.LocationBuddies.Add(newAssortmentLocationBuddy);
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Discards changes and closes the window
        /// </summary>.
        public RelayCommand CancelCommand
        {
            get
            {
                //Lazy load
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand
                        (
                            p => Cancel_Executed()
                        )
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };

                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }

        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region RemoveSourceLocation

        private RelayCommand<AssortmentLocationBuddyRow> _removeSourceLocationCommand;

        /// <summary>
        /// Removes source location row
        /// </summary>.
        public RelayCommand<AssortmentLocationBuddyRow> RemoveSourceLocationCommand
        {
            get
            {
                //Lazy load
                if (_removeSourceLocationCommand == null)
                {
                    _removeSourceLocationCommand = new RelayCommand<AssortmentLocationBuddyRow>
                        (
                            p => RemoveSourceLocation_Executed(p),
                            p => RemoveSourceLocation_CanExecute(p)
                        )
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16
                    };

                    base.ViewModelCommands.Add(_removeSourceLocationCommand);
                }
                return _removeSourceLocationCommand;
            }

        }

        private void RemoveSourceLocation_Executed(AssortmentLocationBuddyRow toRemove)
        {
            //Remove location buddy row
            _assortmentLocationBuddyRows.Remove(toRemove);
            SelectedSourceLocation = _availableSourceLocations.FirstOrDefault();
        }

        private Boolean RemoveSourceLocation_CanExecute(AssortmentLocationBuddyRow value)
        {
            if (value != null)
            {
                return true;
            }
            return false;
        }

        #endregion

        #endregion

        #region Event Handlers

        private void AssortmentLocationBuddyRows_BulkCollectionChanged(Object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //reset source locations
            _availableSourceLocations.Clear();
            _availableSourceLocations.AddRange(_orderedList);

            //remove target location from possible source locations
            if (_selectedTargetLocation != null)
            {
                LocationInfo targetLocation = _availableSourceLocations.FirstOrDefault(p => p.Id.Equals(_selectedTargetLocation.LocationId));
                if (targetLocation != null)
                {
                    _availableSourceLocations.Remove(targetLocation);
                }
            }

            foreach (AssortmentLocationBuddyRow buddyRow in _assortmentLocationBuddyRows)
            {
                LocationInfo targetLocation = _availableSourceLocations.FirstOrDefault(p => p.Id.Equals(buddyRow.Location.Id));
                if (targetLocation != null)
                {
                    _availableSourceLocations.Remove(targetLocation);
                }
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _assortmentLocationBuddyRows.BulkCollectionChanged -= AssortmentLocationBuddyRows_BulkCollectionChanged;
                    _assortmentLocationBuddyRows.Clear();
                    _availableSourceLocations.Clear();
                    _availableSourceLocations = null;
                    _availableTargetLocations.Clear();
                    _availableTargetLocations = null;
                    _orderedList = null;
                    _selectedSourceLocation = null;
                    _selectedTargetLocation = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
