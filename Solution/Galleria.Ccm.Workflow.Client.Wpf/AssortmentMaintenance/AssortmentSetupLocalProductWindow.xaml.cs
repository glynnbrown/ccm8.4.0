﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion

#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;
using System.Collections.Generic;
using System.Windows.Controls;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupLocalProductWindow.xaml
    /// </summary>
    public partial class AssortmentSetupLocalProductWindow : ExtendedRibbonWindow
    {
        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupLocalProductViewModel),
                typeof(AssortmentSetupLocalProductWindow),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentSetupLocalProductViewModel ViewModel
        {
            get { return (AssortmentSetupLocalProductViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupLocalProductWindow senderControl = (AssortmentSetupLocalProductWindow) obj;

            if (e.OldValue != null)
            {
                AssortmentSetupLocalProductViewModel oldModel = (AssortmentSetupLocalProductViewModel) e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                AssortmentSetupLocalProductViewModel newModel = (AssortmentSetupLocalProductViewModel) e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentSetupLocalProductWindow(AssortmentSetupLocalProductViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(AssortmentSetupLocalProductWindow_Loaded);
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentSetupLocalProductWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(AssortmentSetupLocalProductWindow_Loaded);

            var factory =
                new AssortmentLocationCustomColumnLayoutFactory(typeof(AssortmentLocationView),
                    new List<Framework.Model.IModelPropertyInfo>()
                    {
                        Location.CodeProperty,
                        Location.NameProperty
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentLocations, /*pathmask*/ "Location.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(this.assignedGrid);
            _columnLayoutManager.AttachDataGrid(this.unassignedGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action) (() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event handlers

        private void ColumnLayoutManager_ColumnSetChanging(object sender,
            ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentLocationColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        private void AssignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.unassignedGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.AddSelectedLocationsCommand.Execute();
                }
            }
        }

        private void UnassignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.assignedGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveSelectedLocationsCommand.Execute();
                }
            }
        }

        private void UnassignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.AddSelectedLocationsCommand.Execute();
            }
        }

        private void AssignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.RemoveSelectedLocationsCommand.Execute();
            }
        }

        #endregion

        #region Methods

        private List<DataGridColumn> BuildAssortmentLocationColumnList()
        {
            //load the columnset
            List<DataGridColumn> columnList = new List<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                Location.CodeProperty.FriendlyName,
                AssortmentLocationView.LocationCodeProperty.Path,
                System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 50;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                Location.NameProperty.FriendlyName,
                AssortmentLocationView.LocationNameProperty.Path,
                System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            return columnList;
        }

        #endregion

        #region OnClose

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action) (() =>
                {
                    if (_columnLayoutManager != null)
                    {
                        _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                        _columnLayoutManager.Dispose();
                    }

                    if (this.ViewModel != null)
                    {
                        IDisposable dis = this.ViewModel;

                        this.ViewModel = null;

                        if (dis != null)
                        {
                            dis.Dispose();
                        }

                    }

                }), priority: DispatcherPriority.Background);
        }

        #endregion

        private void LocationGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAssigned = senderControl?.Name == assignedGrid.Name;

            if (e.Key == Key.Return)
            {
                if (isAssigned) this.ViewModel?.RemoveSelectedLocationsCommand.Execute();
                else this.ViewModel?.AddSelectedLocationsCommand.Execute();
                Keyboard.Focus(isAssigned ? assignedGrid : unassignedGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAssigned ? unassignedGrid : assignedGrid);
                e.Handled = true;
            }
        }
    }
}
