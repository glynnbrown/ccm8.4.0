﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011
#region Version History: (CCM 8.0)
// V8-25454 : J.Pickup 
//  Created (Copied over from GFS)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.IO;
using Galleria.Ccm.Security;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    public class AssortmentSetupAttachFileViewModel
        : ViewModelAttachedControlObject<AssortmentSetupAttachFileWindow>
    {
        #region Fields
        private Assortment _selectedAssortment;
        private BulkObservableCollection<AssortmentFile> _assignedAssortmentFiles = new BulkObservableCollection<AssortmentFile>();
        private ReadOnlyBulkObservableCollection<AssortmentFile> _assignedAssortmentFilesRO;
        private AssortmentFile _selectedAssortmentFile;

        #endregion

        #region Binding properties
        public static readonly PropertyPath SelectedAssortmentProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupAttachFileViewModel>(p => p.SelectedAssortment);
        public static readonly PropertyPath AssignedAssortmentFilesProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupAttachFileViewModel>(p => p.AssignedAssortmentFiles);

        public static readonly PropertyPath SelectedAssortmentFileProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupAttachFileViewModel>(p => p.SelectedAssortmentFile);
        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment
        /// </summary>
        public Assortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            set
            {
                _selectedAssortment = value;
                OnPropertyChanged(SelectedAssortmentProperty);
            }
        }

        /// <summary>
        /// The list fo files that we will add to the assortment
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentFile> AssignedAssortmentFiles
        {
            get
            {
                if (_assignedAssortmentFilesRO == null)
                {
                    _assignedAssortmentFilesRO = new ReadOnlyBulkObservableCollection<AssortmentFile>(_assignedAssortmentFiles);
                }
                return _assignedAssortmentFilesRO;
            }
        }

        /// <summary>
        /// The category strategy linked to this category tactic
        /// </summary>
        public AssortmentFile SelectedAssortmentFile
        {
            get { return _selectedAssortmentFile; }
            set
            {
                _selectedAssortmentFile = value;
                OnPropertyChanged(SelectedAssortmentFileProperty);
            }
        }

        #endregion

        #region Constructor

        public AssortmentSetupAttachFileViewModel(Assortment assortment)
        {
            //Load assortment selected in previous screen
            this.SelectedAssortment = assortment;

            //Load the list fo files already attached to the assortment
            _assignedAssortmentFiles.AddRange(this.SelectedAssortment.Files);

        }

        #endregion

        #region Commands

        #region AddLinkedFileCommand

        private RelayCommand _addLinkedFileCommand;

        public RelayCommand AddLinkedFileCommand
        {
            get
            {
                if (_addLinkedFileCommand == null)
                {
                    _addLinkedFileCommand =
                        new RelayCommand(p => AddLinkedFile_Executed(), p => AddLinkedFile_CanExecute())
                        {
                            FriendlyName = Message.Generic_Browse,
                            Icon = ImageResources.Add_32
                        };
                    this.ViewModelCommands.Add(_addLinkedFileCommand);
                }
                return _addLinkedFileCommand;
            }
        }

        private Boolean AddLinkedFile_CanExecute()
        {
            //user must have edit permission
            if (!Csla.Rules.BusinessRules.HasPermission
                (Csla.Rules.AuthorizationActions.CreateObject, typeof(Model.AssortmentFile)))
            {
                this.AddLinkedFileCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            return true;
        }

        private void AddLinkedFile_Executed()
        {
            if (this.AttachedControl != null)
            {
                System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
                dlg.Filter = Message.AssortmentSetup_AttachFile_Filter;
                dlg.Multiselect = false;
                dlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    String fileToAdd = dlg.FileName;

                    //Check file size
                    System.IO.FileInfo fileToAddInfo = new System.IO.FileInfo(fileToAdd);
                    if (fileToAddInfo.Length > Int32.MaxValue)
                    {
                        #region Show message that file is too large
                        //File is too large to store SizeInBytes property to inform user and cancel
                        ModalMessage msg = new ModalMessage();
                        msg.Title = Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                        msg.Header = Message.AssortmentSetup_CannotAddFile_Header;
                        msg.Description = String.Format(Message.CategoryManagement_CannotAddFile_Message, fileToAdd);
                        msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                        msg.Owner = this.AttachedControl;
                        msg.ButtonCount = 1;
                        msg.Button1Content = Message.Generic_Ok;
                        msg.DefaultButton = ModalMessageButton.Button1;
                        msg.MessageIcon = ImageResources.Warning_32;
                        msg.ShowDialog();
                        #endregion
                        return;
                    }

                    // Check if file already exists
                    AssortmentFile assignedFile = _assignedAssortmentFiles
                        .FirstOrDefault(f => (f.FileName == fileToAddInfo.Name)
                            && (f.SourceFilePath == Path.GetDirectoryName(fileToAddInfo.FullName)));
                    if (assignedFile != null)
                    {
                        #region Show message that file already exists
                        ModalMessage msg = new ModalMessage();
                        msg.Title = Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                        msg.Header = Message.AssortmentSetup_CannotAddFile_Header;
                        msg.Description = String.Format(Message.AssortmentSetup_FileTooLarge_Message, fileToAddInfo.Name);
                        msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                        msg.Owner = this.AttachedControl;
                        msg.ButtonCount = 1;
                        msg.Button1Content = Message.Generic_Ok;
                        msg.DefaultButton = ModalMessageButton.Button1;
                        msg.MessageIcon = ImageResources.Warning_32;
                        msg.ShowDialog();
                        #endregion
                        return;
                    }

                    //Create our object model
                    Model.File file = Model.File.NewFile(App.ViewState.EntityId, fileToAdd, DomainPrincipal.CurrentUserId);
                    //Save the file object and return its new object
                    file = file.Save();
                    //Create an assortment file object
                    AssortmentFile assortmentFile = AssortmentFile.NewAssortmentFile(file);
                    //Add to our list of files
                    _assignedAssortmentFiles.Add(assortmentFile);
                    OnPropertyChanged(AssignedAssortmentFilesProperty);
                }
            }
            //close the backstage
            LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
        }

        #endregion

        #region RemoveLinkedFileCommand

        private RelayCommand<AssortmentFile> _removeLinkedFileCommand;

        /// <summary>
        /// Removes the given file from the list.
        /// </summary>
        public RelayCommand<AssortmentFile> RemoveLinkedFileCommand
        {
            get
            {
                if (_removeLinkedFileCommand == null)
                {
                    _removeLinkedFileCommand = new RelayCommand<AssortmentFile>(
                        p => RemoveLinkedFile_Executed(p),
                        p => RemoveLinkedFile_CanExecute(p))
                    {
                        FriendlyName = Message.AssortmentSetup_AttachFile_RemoveLinkedFileCommand,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeLinkedFileCommand);
                }
                return _removeLinkedFileCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RemoveLinkedFile_CanExecute(AssortmentFile fileToRemove)
        {
            //user must have delete permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(AssortmentFile)))
            {
                _removeLinkedFileCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (fileToRemove == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveLinkedFile_Executed(AssortmentFile fileToRemove)
        {
            _assignedAssortmentFiles.Remove(fileToRemove);
            OnPropertyChanged(AssignedAssortmentFilesProperty);
        }

        #endregion

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;

        /// <summary>
        /// Apply the changes to the correct AssortmentProduct object in the assortment
        /// and close the window
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndClose_Executed(),
                        p => ApplyAndClose_CanExecute(), false)
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        [DebuggerStepThrough]
        private bool ApplyAndClose_CanExecute()
        {
            return true;
        }

        private void ApplyAndClose_Executed()
        {
            //Copy the attached files in the temprary _assignedAssortmentFiles object into our 
            //current Assortment

            //Remove any deleted items
            List<AssortmentFile> filesToDelete = new List<AssortmentFile>();
            foreach (AssortmentFile af in this.SelectedAssortment.Files)
            {
                AssortmentFile assignedFile = _assignedAssortmentFiles.FirstOrDefault(f => f.FileId == af.FileId);
                if (assignedFile == null)
                {
                    //The file has been removed so delete it from SelectedAssortment
                    filesToDelete.Add(af);
                }
            }
            this.SelectedAssortment.Files.RemoveList(filesToDelete);

            //Add any new items
            foreach (AssortmentFile nf in _assignedAssortmentFiles)
            {
                AssortmentFile newFile = this.SelectedAssortment.Files.FirstOrDefault(f => f.FileId == nf.FileId);
                if (newFile == null)
                {
                    //The file is new so add to selectedAssortment
                    this.SelectedAssortment.Files.Add(nf);
                }
            }

            //close the window
            this.AttachedControl.Close();
        }
        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the create database operation
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute(), false)
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Cancel_CanExecute()
        {
            return true;
        }

        private void Cancel_Executed()
        {
            //close the window
            this.AttachedControl.Close();
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _assignedAssortmentFiles.Clear();
                    _selectedAssortmentFile = null;
                    _selectedAssortment = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
