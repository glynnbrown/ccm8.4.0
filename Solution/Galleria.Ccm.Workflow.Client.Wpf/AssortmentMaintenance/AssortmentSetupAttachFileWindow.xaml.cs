﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.IO;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupAttachFileWindow.xaml
    /// </summary>
    public partial class AssortmentSetupAttachFileWindow : ExtendedRibbonWindow
    {
        #region Constants
        const String RemoveLinkedFileCommandKey = "RemoveLinkedFileCommand";
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupAttachFileViewModel),
            typeof(AssortmentSetupAttachFileWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentSetupAttachFileViewModel ViewModel
        {
            get { return (AssortmentSetupAttachFileViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupAttachFileWindow senderControl =
                (AssortmentSetupAttachFileWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupAttachFileViewModel oldModel =
                    (AssortmentSetupAttachFileViewModel)e.OldValue;
                senderControl.Resources.Remove(RemoveLinkedFileCommandKey);
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                AssortmentSetupAttachFileViewModel newModel =
                    (AssortmentSetupAttachFileViewModel)e.NewValue;
                senderControl.Resources.Add(RemoveLinkedFileCommandKey, newModel.RemoveLinkedFileCommand);
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentSetupAttachFileWindow(AssortmentSetupAttachFileViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(AssortmentSetupAttachFileWindow_Loaded);
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentSetupAttachFileWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(AssortmentSetupAttachFileWindow_Loaded);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Event handler for the view model property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == AssortmentSetupAttachFileViewModel.SelectedAssortmentFileProperty.Path)
            {
                if (this.ViewModel != null)
                {
                    if (this.ViewModel.SelectedAssortmentFile != null)
                    {
                        //Load Image in preview window
                        Ccm.Model.File imageFile = Ccm.Model.File.GetById(this.ViewModel.SelectedAssortmentFile.FileId);

                        BitmapImage bmpImage = BitmapHelper.GetBitmapImage(imageFile.Data);
                        xPreviewImage.Source = bmpImage;
                    }
                    else
                    {
                        xPreviewImage.Source = null;
                    }
                }
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        #endregion

        #region OnClose

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        #endregion
    }


    /// <summary>
    /// Bitmap Helper class
    /// </summary>
    public class BitmapHelper
    {
        /// <summary>
        /// Converts a Blob to a BitmapImage
        /// </summary>
        /// <param name="Blob"></param>
        /// <returns></returns>
        public static BitmapImage GetBitmapImage(Byte[] Blob)
        {
            using (MemoryStream dataStream = new MemoryStream(Blob))
            {
                return GetBitmapImage(dataStream);
            }
        }

        /// <summary>
        /// Gets a bitmap image from a memory stream
        /// </summary>
        /// <param name="dataStream">memory stream</param>
        /// <returns>BitmapImage</returns>
        public static BitmapImage GetBitmapImage(MemoryStream dataStream)
        {
            BitmapImage img = new BitmapImage();
            dataStream.Position = 0;
            img.BeginInit();
            img.StreamSource = new MemoryStream(dataStream.ToArray());
            img.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            img.CacheOption = BitmapCacheOption.Default;
            img.EndInit();
            img.Freeze();
            return img;
        }

        /// <summary>
        /// Creates a Bitmap image from a blob with a particular size and pixel format
        /// </summary>
        /// <param name="dataStream">Original Stream</param>
        /// <param name="size">Height and Width of the image</param>
        /// <param name="format">Pixel format of the image</param>
        /// <returns></returns>
        public static BitmapSource CreateBitmap(Byte[] Blob, Double width, Double height, Boolean maintainAspectRatio, Boolean reSizeAgainstLongerDimention)
        {
            Size size = new Size(width, height);
            BitmapSource image = GetBitmapImage(new MemoryStream(Blob));
            if (image.PixelHeight < height && image.PixelWidth < width)
            {
                return image;
            }
            else
            {
                return ReSize(image, size, maintainAspectRatio, reSizeAgainstLongerDimention);
            }
        }

        /// <summary>
        /// Creates a Bitmap image from a stream with a particular size and pixel format
        /// </summary>
        /// <param name="dataStream">Original Stream</param>
        /// <param name="size">Height and Width of the image</param>
        /// <param name="format">Pixel format of the image</param>
        /// <returns></returns>
        public static BitmapSource CreateBitmap(MemoryStream dataStream, Size size, PixelFormat format, Boolean maintainAspectRatio)
        {
            BitmapSource image = GetBitmapImage(dataStream);
            image = ReSize(image, size, maintainAspectRatio);
            return FormatBitmap(image, format);
        }

        /// <summary>
        /// Creates a Byte array from a Stream
        /// </summary>
        /// <param name="dataStream">Stream to read</param>
        /// <param name="CloseStream">Closes the Stream once finished if True.</param>
        /// <returns>Array of Bytes</returns>
        public static Byte[] CreateBlob(Stream dataStream, Boolean CloseStream)
        {
            Byte[] blob = new Byte[dataStream.Length];
            //Ensure stream is at the start
            dataStream.Position = 0;

            //Stream read does not necessarily read all the bytes asked for so
            //a loop is required to ensure all bytes are read.
            for (Int32 pos = 0; pos < dataStream.Length; )
            {
                Int32 len = dataStream.Read(blob, pos, (Int32)dataStream.Length - pos);
                if (len == 0)
                {
                    break;
                }
                pos += len;

            }

            if (CloseStream) dataStream.Close();
            return blob;
        }
        public static Byte[] CreateBlob(BitmapSource img)
        {
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(img));
            using (MemoryStream stream = new MemoryStream())
            {
                png.Save(stream);
                return CreateBlob(stream, true);
            }
        }

        public static Byte[] CreatePNGBlob(BitmapSource img)
        {
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(img));
            using (MemoryStream dataStream = new MemoryStream())
            {
                png.Save(dataStream);

                Byte[] blob = new Byte[dataStream.Length];
                //Ensure stream is at the start
                dataStream.Position = 0;

                //Stream read does not necessarily read all the bytes asked for so
                //a loop is required to ensure all bytes are read.
                for (Int32 pos = 0; pos < dataStream.Length; )
                {
                    Int32 len = dataStream.Read(blob, pos, (Int32)dataStream.Length - pos);
                    if (len == 0)
                    {
                        break;
                    }
                    pos += len;

                }

                return blob;
            }
        }

        /// <summary>
        /// Redraws an image to a new size.
        /// </summary>
        /// <param name="originalImage">the image you wish resizing</param>
        /// <param name="size">height and widht to resize to</param>
        /// <param name="maintainAspectRatio">maintains the aspect ratio of the image</param>
        /// <returns>The resized image</returns>
        public static BitmapSource ReSize(BitmapSource originalImage, Size size, Boolean maintainAspectRatio, Boolean reSizeAgainstLongerDimention = true)
        {
            Single nPercent = 0;
            Single nPercentW = 0;
            Single nPercentH = 0;

            if (size.Width == 0) nPercentW = 1;
            else nPercentW = ((Single)size.Width / (Single)originalImage.PixelWidth);

            if (size.Height == 0) nPercentH = 1;
            else nPercentH = ((Single)size.Height / (Single)originalImage.PixelHeight);

            if (reSizeAgainstLongerDimention)
            {
                if (nPercentH < nPercentW) nPercent = nPercentH;
                else nPercent = nPercentW;
            }
            else
            {
                if (nPercentH < nPercentW) nPercent = nPercentW;
                else nPercent = nPercentH;
            }

            TransformedBitmap resizedImage;
            if (maintainAspectRatio)
            {
                resizedImage = new TransformedBitmap(originalImage, new ScaleTransform(nPercent, nPercent));
            }
            else
            {
                resizedImage = new TransformedBitmap(originalImage, new ScaleTransform(nPercentW, nPercentH));
            }
            resizedImage.Freeze();
            return resizedImage;
        }
        private static BitmapSource FormatBitmap(BitmapSource originalImage, PixelFormat format)
        {
            FormatConvertedBitmap img = new FormatConvertedBitmap(originalImage, format, originalImage.Palette, 0.0f);
            //img.Freeze();
            return img;
        }
    }
}
