﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// CCM-18489 : M.Pettit
//  Add selected assortment products to grid
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.ComponentModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentProductBuddyAdvancedAddWindow.xaml
    /// </summary>
    public partial class AssortmentProductBuddyAdvancedAddWindow : ExtendedRibbonWindow
    {
        #region constants

        const String _removeProductBuddyCommandKey = "RemoveProductBuddyCommand";

        #endregion

        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentProductBuddyAdvancedAddViewModel), typeof(AssortmentProductBuddyAdvancedAddWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public AssortmentProductBuddyAdvancedAddViewModel ViewModel
        {
            get { return (AssortmentProductBuddyAdvancedAddViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentProductBuddyAdvancedAddWindow senderControl = (AssortmentProductBuddyAdvancedAddWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentProductBuddyAdvancedAddViewModel oldModel = (AssortmentProductBuddyAdvancedAddViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(_removeProductBuddyCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentProductBuddyAdvancedAddViewModel newModel = (AssortmentProductBuddyAdvancedAddViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(_removeProductBuddyCommandKey, newModel.RemoveProductBuddyCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentProductBuddyAdvancedAddWindow(Assortment currentAssortment, List<AssortmentProduct> selectedProducts)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;
            
            InitializeComponent();
            this.ViewModel = new AssortmentProductBuddyAdvancedAddViewModel(currentAssortment);
            this.ViewModel.AddTargetProductsFromList(selectedProducts);
            this.Loaded += AssortmentProductBuddyAdvancedAddWindow_Loaded;
        }

        private void AssortmentProductBuddyAdvancedAddWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentProductBuddyAdvancedAddWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnPreviewKeyDown(e);

            //if trying to paste
            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.V)
            {
                if (Clipboard.ContainsText())
                {
                    this.ViewModel.AddBuddiesFromClipboard(Clipboard.GetText());
                }
            }
        }

        private void DataGridView_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                var senderControl = sender as ExtendedDataGrid;
                FocusNavigationDirection direction = FocusNavigationDirection.Down;
                TraversalRequest tRequest = new TraversalRequest(direction);
                senderControl?.MoveFocus(tRequest);
                e.Handled = true;
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Method to override the on closed method
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion

      
    }
}
