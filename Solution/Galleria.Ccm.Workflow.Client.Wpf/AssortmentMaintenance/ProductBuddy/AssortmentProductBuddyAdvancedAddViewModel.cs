﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
// V8-32718 : A.Probyn
//  Updated so full product attributes can be displayed for target product selection
// V8-32812 : A.Heathcote
//  Added MultiSelect Functionality
// CCM-18310 : A.Heathcote
//  Made changes to Select Target Products to add the Treatment type and Treatment Percentage selection window.
// CCM-14021 : A.Heathcote 
//  Removed the _changePercentage field as it was no longer in use.
// CCM-18458 : M.Brumby
//  Don't allow the selection of assortment products as a target product if it is already a target product
// CCM-14012 : A.Heathcote
//  Made changes to SelectTargetProducts to make it use the new constructor and use products from outside the category.
// CCM-18489 : M.Pettit
//  Add selected assortment products to grid
// CCM-18470 : G.Richards
//  Multi-select on combo box options changes all selected rows
// CCM-18458 : A.Heathcote
//  Added the excluded list to satisfy the edited constructor.
// CCM-18560 : D.Pleasance
//  Extended grid to allow multiple updates for editable grid columns.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// ViewModel controller class for ProductBuddyAdvancedAddWindow
    /// </summary>
    public sealed class AssortmentProductBuddyAdvancedAddViewModel : ViewModelAttachedControlObject<AssortmentProductBuddyAdvancedAddWindow>
    {
        #region Fields

        private Assortment _currentAssortment;
        private Dictionary<String, ProductInfo> _availableSourceProductLookup = new Dictionary<String, ProductInfo>();
        private Dictionary<Int32, Product> _availableTargetProductLookup = new Dictionary<Int32, Product>();
        private Boolean _isRemoveExistingBuddiesSelected = false;
        private BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow> _productBuddyRows = new BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow>();
        private BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow> _productBuddyRowsSelected = new BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow>();
        private Int32 _productGroupId;
        private Boolean _isUpdatingMultiInput;
        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath IsRemoveExistingBuddiesSelectedProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.IsRemoveExistingBuddiesSelected);
        public static readonly PropertyPath ProductBuddyRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.ProductBuddyRows);
        public static readonly PropertyPath ProductBuddyRowsSelectedProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.ProductBuddyRowsSelected);
       
        //commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath RemoveProductBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyAdvancedAddViewModel>(p => p.RemoveProductBuddyCommand);
        #endregion

        #region Properties
        
        /// <summary>
        /// Gets/Sets flag to indicate if the existing buddies should be cleared.
        /// </summary>
        public Boolean IsRemoveExistingBuddiesSelected
        {
            get { return _isRemoveExistingBuddiesSelected; }
            set
            {
                _isRemoveExistingBuddiesSelected = value;
                OnPropertyChanged(IsRemoveExistingBuddiesSelectedProperty);
            }
        }

        /// <summary>
        /// Returns the collection of product buddy rows to be added.
        /// </summary>
        public BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow> ProductBuddyRows
        {
            get { return _productBuddyRows; }
        }

        /// <summary>
        /// Returns the collection of product buddy rows selected.
        /// </summary>
        public BulkObservableCollection<AssortmentProductBuddyAdvancedAddRow> ProductBuddyRowsSelected
        {
            get { return _productBuddyRowsSelected; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public AssortmentProductBuddyAdvancedAddViewModel(Assortment currentAssortment)
        {
            _currentAssortment = currentAssortment;
            _productGroupId = _currentAssortment.ProductGroupId;
            //Initialise product lists
            _availableSourceProductLookup = ProductInfoList.FetchByEntityId(App.ViewState.EntityId).ToDictionary(p => p.Gtin);
            _availableTargetProductLookup = ProductList.FetchByProductIds(_currentAssortment.Products.Select(p => p.ProductId)).ToDictionary(p => p.Id);

            this.ProductBuddyRows.BulkCollectionChanged += ProductBuddyRows_BulkCollectionChanged;

            //add an initial blank row
            _productBuddyRows.Add(new AssortmentProductBuddyAdvancedAddRow(
                _currentAssortment, 
                _availableSourceProductLookup, 
                _availableTargetProductLookup,
                (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
        }

        #endregion

        #region Commands

        #region OK

        private RelayCommand _okCommand;

        /// <summary>
        /// Adds the product buddies requested.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean OK_CanExecute()
        {
            //must have at least 1 row
            if (this.ProductBuddyRows.Count(p => !p.IsNewPlaceholderRow) == 0)
            {
                this.OKCommand.DisabledReason = Message.AssortmentProductBuddyAdvancedAdd_OK_DisabledNoRows;
                return false;
            }

            //all buddies must be valid
            if (!this.ProductBuddyRows.All(p => (p.IsNewPlaceholderRow) ? true : p.IsValid))
            {
                this.OKCommand.DisabledReason = Message.AssortmentProductBuddyAdvancedAdd_OK_DisabledInvalidRows;
                return false;
            }

            return true;
        }

        private void OK_Executed()
        {
            //clear existing buddies if required
            if (this.IsRemoveExistingBuddiesSelected)
            {
                this._currentAssortment.ProductBuddies.Clear();
            }

            //add the new buddies
            foreach (AssortmentProductBuddyAdvancedAddRow row in this.ProductBuddyRows)
            {
                if (row.TargetProduct != null)
                {
                    //check if a buddy already exists for the target
                    // if yes then remove it first
                    Object targetId = row.TargetProduct.ProductId;
                    AssortmentProductBuddy existingBuddy = this._currentAssortment.ProductBuddies.FirstOrDefault(p => targetId.Equals(p.ProductId));
                    if (existingBuddy != null)
                    {
                        this._currentAssortment.ProductBuddies.Remove(existingBuddy);
                    }

                    //add the new buddy
                    this._currentAssortment.ProductBuddies.Add(AssortmentProductBuddyAdvancedAddRow.CreateProductBuddy(row));
                }
            }


            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion
        
        #region RemoveProductBuddy

        private RelayCommand _removeProductBuddyCommand;

        /// <summary>
        /// Removes the given buddy
        /// </summary>
        public RelayCommand RemoveProductBuddyCommand
        {
            get
            {
                if (_removeProductBuddyCommand == null)
                {
                    _removeProductBuddyCommand = new RelayCommand(
                        p => RemoveProductBuddy_Executed(p),
                        p => RemoveProductBuddy_CanExecute(p))
                    {
                        //SmallIcon = ImageResources.ProductBuddyAdvancedAdd_RemoveProductBuddy
                        SmallIcon = ImageResources.AssortmentProductBuddyWizard_RemoveSourceProduct
                    };
                    base.ViewModelCommands.Add(_removeProductBuddyCommand);
                }
                return _removeProductBuddyCommand;
            }
        }

        private Boolean RemoveProductBuddy_CanExecute(Object arg)
        {
            AssortmentProductBuddyAdvancedAddRow buddyToRemove = arg as AssortmentProductBuddyAdvancedAddRow;

            if (buddyToRemove == null)
            {
                return false;
            }

            //must not be the new placeholder row.
            if (buddyToRemove.IsNewPlaceholderRow)
            {
                return false;
            }

            return true;
        }

        private void RemoveProductBuddy_Executed(Object arg)
        {
            AssortmentProductBuddyAdvancedAddRow buddyToRemove = arg as AssortmentProductBuddyAdvancedAddRow;
            if (buddyToRemove != null)
            {
                this.ProductBuddyRows.Remove(buddyToRemove);
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the product buddy rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductBuddyRows_BulkCollectionChanged(Object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (AssortmentProductBuddyAdvancedAddRow row in e.ChangedItems)
                    {
                        row.PropertyChanged += ProductBuddyRow_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (AssortmentProductBuddyAdvancedAddRow row in e.ChangedItems)
                    {
                        row.Dispose();
                        row.PropertyChanged -= ProductBuddyRow_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (AssortmentProductBuddyAdvancedAddRow row in e.ChangedItems)
                        {
                            row.Dispose();
                            row.PropertyChanged -= ProductBuddyRow_PropertyChanged;
                        }
                    }
                    break;
            }            
        }

        /// <summary>
        /// Responds to a property change on a product buddy row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductBuddyRow_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TargetProductProperty.Path)
            {
                //add a new blank row if required
                AssortmentProductBuddyAdvancedAddRow blankRow = this.ProductBuddyRows.FirstOrDefault(r => r.TargetProduct == null);
                if (blankRow == null)
                {
                    this.ProductBuddyRows.Add(new AssortmentProductBuddyAdvancedAddRow(_currentAssortment, _availableSourceProductLookup, _availableTargetProductLookup,(Action
                        <AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
                }
            }

            if (!_isUpdatingMultiInput)
            {
                if (_productBuddyRowsSelected.Count > 1)
                {
                    if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.SourceTypeProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TreatmentTypeProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.ProductAttributeTypeProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TreatmentTypePercentageProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source1CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source1PercentageCellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source2CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source2PercentageCellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source3CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source3PercentageCellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source4CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source4PercentageCellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source5CellValueProperty.Path ||
                        e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source5PercentageCellValueProperty.Path)
                    {
                        _isUpdatingMultiInput = true;
                        AssortmentProductBuddyAdvancedAddRow productBuddyRow = (AssortmentProductBuddyAdvancedAddRow)sender;
                        foreach (AssortmentProductBuddyAdvancedAddRow selectedProductBuddyRow in _productBuddyRowsSelected)
                        {
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.SourceTypeProperty.Path)
                            {
                                selectedProductBuddyRow.SourceTypeCellValue = productBuddyRow.SourceTypeCellValue;
                            }

                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TreatmentTypeProperty.Path)
                            {
                                Boolean applyValue = true;

                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    if(productBuddyRow.TreatmentType ==  PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg ||
                                        productBuddyRow.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                                    {
                                        applyValue = false;
                                    }
                                }
                                if (applyValue)
                                {
                                    selectedProductBuddyRow.TreatmentTypeCellValue = productBuddyRow.TreatmentTypeCellValue;
                                }
                            }

                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.ProductAttributeTypeProperty.Path)
                            {
                                selectedProductBuddyRow.ProductAttributeTypeCellValue = productBuddyRow.ProductAttributeTypeCellValue;
                            }

                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.TreatmentTypePercentageProperty.Path)
                            {
                                if (selectedProductBuddyRow.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg ||
                                    selectedProductBuddyRow.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                                {
                                    selectedProductBuddyRow.TreatmentTypePercentage = productBuddyRow.TreatmentTypePercentage;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source1CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source1CellValue = productBuddyRow.Source1CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source1PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source1CellValue != null)
                                {
                                    selectedProductBuddyRow.Source1PercentageCellValue = productBuddyRow.Source1PercentageCellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source2CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source2CellValue = productBuddyRow.Source2CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source2PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source2CellValue != null)
                                {
                                    selectedProductBuddyRow.Source2PercentageCellValue = productBuddyRow.Source2PercentageCellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source3CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source3CellValue = productBuddyRow.Source3CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source3PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source3CellValue != null)
                                {
                                    selectedProductBuddyRow.Source3PercentageCellValue = productBuddyRow.Source3PercentageCellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source4CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source4CellValue = productBuddyRow.Source4CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source4PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source3CellValue != null)
                                {
                                    selectedProductBuddyRow.Source4PercentageCellValue = productBuddyRow.Source4PercentageCellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source5CellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                                {
                                    selectedProductBuddyRow.Source5CellValue = productBuddyRow.Source5CellValue;
                                }
                            }
                            if (e.PropertyName == AssortmentProductBuddyAdvancedAddRow.Source5PercentageCellValueProperty.Path)
                            {
                                if (selectedProductBuddyRow.Source4CellValue != null)
                                {
                                    selectedProductBuddyRow.Source5PercentageCellValue = productBuddyRow.Source5PercentageCellValue;
                                }
                            }
                        }
                        _isUpdatingMultiInput = false;
                    }
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add passed products as target products
        /// </summary>
        /// <param name="selectedAssortmentProducts"></param>
        public void AddTargetProductsFromList(List<AssortmentProduct> selectedAssortmentProducts)
        {
            if (!selectedAssortmentProducts.Any()) return;

            //remove the placeholder row
            RemovePlaceholderRow();

            foreach (AssortmentProduct asstProduct in selectedAssortmentProducts)
            {
                AssortmentProductBuddyAdvancedAddRow row = new AssortmentProductBuddyAdvancedAddRow(
                    _currentAssortment, _availableSourceProductLookup, _availableTargetProductLookup, 
                        (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts);
                row.SourceType = PlanogramAssortmentProductBuddySourceType.Attribute;
                row.SourceTypeCellValue = row.SourceType.ToString();
                row.TargetProduct = asstProduct;
                row.TargetProductCellValue = asstProduct.ToString();
                row.TargetProductError = null;
                this.ProductBuddyRows.Add(row);
            }

            //add the placeholder row back in
            this.ProductBuddyRows.Add(
                new AssortmentProductBuddyAdvancedAddRow(
                    _currentAssortment, _availableSourceProductLookup, _availableTargetProductLookup, 
                    (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
        }

        /// <summary>
        /// Removes the placeholder row rom the buddy rows list
        /// </summary>
        private void RemovePlaceholderRow()
        {
            foreach (AssortmentProductBuddyAdvancedAddRow buddy in ProductBuddyRows.ToList())
            {
                //remove the placeholder row then re-add it it to ensure that it is the bottom row in the grid
                if (!buddy.IsNewPlaceholderRow) continue;
                else
                {
                    ProductBuddyRows.Remove(buddy);
                }
            }
        }

        #region Select Target Products

        public void SelectTargetProducts(AssortmentProductBuddyAdvancedAddRow row)
        {            
            List<String> excludedList = new List<string>();
            foreach (AssortmentProductBuddyAdvancedAddRow advancedRow in ProductBuddyRows)
            {
                if (advancedRow.TargetProduct == null) continue;
                excludedList.Add(advancedRow.TargetProduct.Gtin);
            }
            List<AssortmentProductView> availableProducts = new List<AssortmentProductView>();
            foreach (AssortmentProduct assortProduct in this._currentAssortment.Products)
            {
                //If the product is not already a target product then add it to the possible list
                if (!this.ProductBuddyRows.Any(p => Equals(p.TargetProduct, assortProduct)))
                {
                Product fullProduct = null;
                row._sourceTargetAssortmentProductLookup.TryGetValue(assortProduct.ProductId, out fullProduct);
                availableProducts.Add(new AssortmentProductView(assortProduct, fullProduct));
            }                       
            }                       
            List<Product> availableProductsAsTypeProduct = new List<Product>();
            foreach (AssortmentProductView assortmentProduct in availableProducts)
            {
                availableProductsAsTypeProduct.Add(assortmentProduct.Product);
            }
            ProductSelectorViewModel viewModel = new ProductSelectorViewModel(true, _productGroupId, excludedList);
            CommonHelper.GetWindowService().ShowDialog<ProductSelectorWindow>(viewModel);
            ProductBuddyAdvancedAddManualProductBuddyWindow manualWin = null;
            //addapt to foreach (will have to create a new row for each)
            if (viewModel.AssignedProducts == null) return;
            if (viewModel.DialogResult == true && viewModel.AssignedProducts.Any())
            {
                //creates the first buddy using the first selected product
                if (viewModel.ManualBuddyButtonPressed)
                {
                    //Show new manual buddy window
                    manualWin = new ProductBuddyAdvancedAddManualProductBuddyWindow();
                    manualWin.SelectedProductText = String.Format("{0} {1}", viewModel.AssignedProducts.Count, Message.ProductBuddyWizard_MultipleBuddySourceText);
                    CommonHelper.GetWindowService().ShowDialog<ProductBuddyAdvancedAddManualProductBuddyWindow>(manualWin);
                   
                    //if selected make the buddy manual with a source product of itself
                    row.SourceType = PlanogramAssortmentProductBuddySourceType.Manual;
                    row.SourceTypeCellValue = row.SourceType.ToString();
                   
                    row.Source1 = ProductInfo.NewProductInfo(viewModel.AssignedProducts[0]);
                    row.Source1CellValue = row.Source1.ToString();
                   
                    row.TreatmentType = manualWin.SelectedTreatmentType;
                    row.TreatmentTypeCellValue = row.TreatmentType.ToString();
                   
                    row.Source1Percentage = manualWin.SelectedTreatmentTypePercentage;
                    row.Source1PercentageCellValue = row.Source1Percentage.ToString();
                }
                else
                {
                    //otherwise treat as attribute not as manual 
                    row.SourceType = PlanogramAssortmentProductBuddySourceType.Attribute;
                    row.SourceTypeCellValue = row.SourceType.ToString();
                }
                //update the cell value & error silently.
                row.TargetProductCellValue = viewModel.AssignedProducts[0].ToString();               
                row.TargetProductError = null;
    
                //this is no longer the placeholder.
                row.IsNewPlaceholderRow = false;

                row.TargetProduct = AssortmentProduct.NewAssortmentProduct(viewModel.AssignedProducts[0]);
            }

            if(viewModel.AssignedProducts.Count > 1)
            {
                //since the first buddy was created above, we now create the rest of the selected products as buddies 
                Int32 index = 1;
                foreach(Product product in viewModel.AssignedProducts)
                {
                    if(product != viewModel.AssignedProducts[index]) continue;
                    else
                    {
                        AssortmentProductBuddyAdvancedAddRow newRow = new AssortmentProductBuddyAdvancedAddRow(this._currentAssortment,
                            this._availableSourceProductLookup, this._availableTargetProductLookup, 
                            (Action<AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts);
                        if (viewModel.ManualBuddyButtonPressed)
                        {
                            //The rest of the manual buddies are created in the same way as the first 
                            newRow.SourceType = PlanogramAssortmentProductBuddySourceType.Manual;
                            newRow.Source1 = ProductInfo.NewProductInfo(viewModel.AssignedProducts[index]);
                            newRow.SourceTypeCellValue = newRow.SourceType.ToString();

                            newRow.Source1CellValue = newRow.Source1.ToString();
                            newRow.Source1Percentage = manualWin.SelectedTreatmentTypePercentage;
                            newRow.Source1PercentageCellValue = newRow.Source1Percentage.ToString();

                            newRow.TreatmentType = manualWin.SelectedTreatmentType;
                            newRow.TreatmentTypeCellValue = newRow.TreatmentType.ToString();
                        }
                        else
                        {
                            newRow.SourceType = PlanogramAssortmentProductBuddySourceType.Attribute;                            
                            newRow.SourceTypeCellValue = newRow.SourceType.ToString();
                        }
                        newRow.TargetProductCellValue = viewModel.AssignedProducts[index].ToString();
                        newRow.TargetProductError = null;

                        //this is no longer the placeholder.
                        newRow.IsNewPlaceholderRow = false;
                        newRow.TargetProduct = AssortmentProduct.NewAssortmentProduct(viewModel.AssignedProducts[index]);
                        
                        this.ProductBuddyRows.Add(newRow);
                        index++;
                    }
              }

            foreach (AssortmentProductBuddyAdvancedAddRow buddies in ProductBuddyRows.ToList())
            {
                //remove the placeholder row then re-add it it to ensure that it is the bottom row in the grid
                if (!buddies.IsNewPlaceholderRow) continue;
                else
                {
                    ProductBuddyRows.Remove(buddies);
                }
            }               
                this.ProductBuddyRows.Add(new AssortmentProductBuddyAdvancedAddRow(_currentAssortment, _availableSourceProductLookup, _availableTargetProductLookup, (Action
                    <AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
          }               
        }

        #endregion

        #region Add Buddies From Clipboard

        /// <summary>
        /// Creates buddy rows from the clipboard string
        /// </summary>
        /// <param name="clipboardText"></param>
        public void AddBuddiesFromClipboard(String clipboardText)
        {
            //split into buddy rows
            String[] rows = clipboardText.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (rows.Length != 0)
            {
                //work out the delimiter from the first row
                Boolean isExcelDelim = false;
                String[] row1Split = rows[0].Split("\t".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (row1Split.Length >= 3)
                {
                    isExcelDelim = true;
                }

                //check if this is a comma delim
                Boolean isCommaDelim = false;
                if (!isExcelDelim)
                {
                    row1Split = rows[0].Split(',');
                    if (row1Split.Length >= 3)
                    {
                        isCommaDelim = true;
                    }
                }


                if (isExcelDelim || isCommaDelim)
                {
                    //remove the last blank row
                    this.ProductBuddyRows.Remove(this.ProductBuddyRows.Last());

                    foreach (String row in rows)
                    {
                        //split the row
                        String[] rowSplit;
                        if (isExcelDelim)
                        {
                            rowSplit = row.Split("\t".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        }
                        else
                        {
                            rowSplit = row.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        }

                        //create
                        AssortmentProductBuddyAdvancedAddRow buddyRow = new AssortmentProductBuddyAdvancedAddRow(_currentAssortment, _availableSourceProductLookup, _availableTargetProductLookup, (Action
                        <AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts);
                        buddyRow.TargetProductCellValue = rowSplit[0];

                        if (rowSplit.Length >= 2)
                        {
                            buddyRow.SourceTypeCellValue = rowSplit[1];
                        }

                        if (rowSplit.Length >= 3)
                        {
                            buddyRow.TreatmentTypeCellValue = rowSplit[2];
                        }

                        if (buddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Attribute)
                        {
                            if (rowSplit.Length >= 4)
                            {
                                buddyRow.ProductAttributeTypeCellValue = rowSplit[3];
                            }
                        }
                        else if (buddyRow.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                        {
                            if (rowSplit.Length >= 4)
                            {
                                buddyRow.Source1CellValue = rowSplit[3];
                            }
                            if (rowSplit.Length >= 5)
                            {
                                buddyRow.Source1PercentageCellValue = rowSplit[4];
                            }

                            if (rowSplit.Length >= 6)
                            {
                                buddyRow.Source2CellValue = rowSplit[5];
                            }
                            if (rowSplit.Length >= 7)
                            {
                                buddyRow.Source2PercentageCellValue = rowSplit[6];
                            }

                            if (rowSplit.Length >= 8)
                            {
                                buddyRow.Source3CellValue = rowSplit[7];
                            }
                            if (rowSplit.Length >= 9)
                            {
                                buddyRow.Source3PercentageCellValue = rowSplit[8];
                            }

                            if (rowSplit.Length >= 10)
                            {
                                buddyRow.Source4CellValue = rowSplit[9];
                            }
                            if (rowSplit.Length >= 11)
                            {
                                buddyRow.Source4PercentageCellValue = rowSplit[10];
                            }

                            if (rowSplit.Length >= 11)
                            {
                                buddyRow.Source5CellValue = rowSplit[10];
                            }
                            if (rowSplit.Length >= 12)
                            {
                                buddyRow.Source5PercentageCellValue = rowSplit[11];
                            }
                        }
                        //add the buddy row.
                        this.ProductBuddyRows.Add(buddyRow);
                    }
                    //add the blank row back in
                    this.ProductBuddyRows.Add(new AssortmentProductBuddyAdvancedAddRow(_currentAssortment, _availableSourceProductLookup, _availableTargetProductLookup,(Action
                        <AssortmentProductBuddyAdvancedAddRow>)SelectTargetProducts));
                }
            }

        }

        #endregion

        #endregion

        #region IDisposable
        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.ProductBuddyRows.Clear();//clear rows so they get disposed.
                    this.ProductBuddyRows.BulkCollectionChanged -= ProductBuddyRows_BulkCollectionChanged;
                    _availableSourceProductLookup.Clear();
                    _availableSourceProductLookup = null;
                    _currentAssortment = null;
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }
}
