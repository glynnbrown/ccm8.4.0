﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32812 : A.Heathcote
//	Columns are now generated here instead of in the XAML
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.ComponentModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentProductBuddyWindow.xaml
    /// </summary>
    public sealed partial class AssortmentProductBuddyWindow : ExtendedRibbonWindow
    {
        const String _removeBuddyCommandKey = "ProductBuddies_RemoveBuddyCommand";
        const String _removeSourceCommandKey = "ProductBuddies_RemoveSourceProductCommand";
        private ColumnLayoutManager _columnLayoutManager;
        private DataGridExtendedTemplateColumn _deleteRowColumn;

        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentProductBuddyViewModel), typeof(AssortmentProductBuddyWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the viewmodel controller for this screen
        /// </summary>
        public AssortmentProductBuddyViewModel ViewModel
        {
            get { return (AssortmentProductBuddyViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentProductBuddyWindow senderControl = (AssortmentProductBuddyWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentProductBuddyViewModel oldModel = (AssortmentProductBuddyViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(_removeBuddyCommandKey);
                senderControl.Resources.Remove(_removeSourceCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentProductBuddyViewModel newModel = (AssortmentProductBuddyViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(_removeBuddyCommandKey, newModel.RemoveBuddyCommand);
                senderControl.Resources.Add(_removeSourceCommandKey, newModel.RemoveSourceProductCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentProductBuddyWindow(Assortment currentAssortment)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add link to SA.chm
            Help.SetFilename((DependencyObject)this, App.ViewState.HelpFilePath);
            Help.SetKeyword((DependencyObject)this, "34");

            this.ViewModel = new AssortmentProductBuddyViewModel(currentAssortment);

            this.Loaded += AssortmentProductBuddyWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentProductBuddyWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentProductBuddyWindow_Loaded;
            UpdateColumns();
            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handler

        private void ColumnLayoutManager_ColumnsChanging(Object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;
            Int32 currentIndex = 0;
            List<DataGridColumn> columnsToAdd = new List<DataGridColumn>(){
            new DataGridExtendedTextColumn
            {
                Header = Message.AssortmentProductBuddy_Col_Product,
                IsReadOnly = true,
                Binding = new Binding(AssortmentMaintenance.AssortmentSetup.AssortmentProductBuddyRow.TargetProductProperty.Path) {Mode = BindingMode.OneWay },
                SortMemberPath = "TargetProduct.Gtin",
                Width = 220
            },
            new DataGridExtendedTextColumn
            {
                Header = Message.AssortmentProductBuddy_Col_Summary,
                IsReadOnly = true,
                Binding = new Binding(AssortmentMaintenance.AssortmentSetup.AssortmentProductBuddyRow.SummaryProperty.Path) {Mode = BindingMode.OneWay },
            }
        };
            foreach (DataGridColumn column in columnsToAdd)
            {
                columnSet.Insert(currentIndex, column);
                currentIndex++;
            }

            _deleteRowColumn = new DataGridExtendedTemplateColumn
            {
                Header = null,
                Width = 40,
                CanUserFilter = false,
                CanUserResize = false,
                CellTemplate = Resources["AssortmentProductBuddyWindow_DeleteButtonTemplate"] as DataTemplate
            };
            columnSet.Insert(currentIndex, _deleteRowColumn);
            currentIndex++;
        }

        #endregion

        #region Methods

        private void UpdateColumns()
        {
            #region Hardcoded columns
            //DataGridColumnCollection columnSet = new DataGridColumnCollection();
            //DataGridExtendedTextColumn newColumn = null;

            //newColumn = new DataGridExtendedTextColumn
            //{
            //    Header = Message.AssortmentProductBuddy_Col_Product,
            //    IsReadOnly = true,
            //    Binding = new Binding(AssortmentMaintenance.AssortmentSetup.AssortmentProductBuddyRow.TargetProductProperty.Path)
            //        { Mode = BindingMode.OneWay },
            //    SortMemberPath = "TargetProduct.Gtin",
            //    Width = 150
            //};
            //columnSet.Add(newColumn);

            //newColumn = new DataGridExtendedTextColumn
            //{
            //    Header = Message.AssortmentProductBuddy_Col_Summary,
            //    IsReadOnly = true,
            //    Binding = new Binding(AssortmentMaintenance.AssortmentSetup.AssortmentProductBuddyRow.SummaryProperty.Path)
            //    { Mode = BindingMode.OneWay },
            //};
            //columnSet.Add(newColumn);

            //_deleteRowColumn = new DataGridExtendedTemplateColumn
            //{
            //    Header = null,
            //    Width = 40,
            //    CanUserFilter = false,
            //    CanUserResize = false,
            //    CellTemplate = Resources["AssortmentProductBuddyWindow_DeleteButtonTemplate"] as DataTemplate
            //};
            //columnSet.Add(_deleteRowColumn);
            //this.ProductBuddyGrid.ColumnSet = columnSet;
            #endregion

            var factory = new AssortmentProductBuddyColumnLayoutFactory(typeof(AssortmentProduct));

            DisplayUnitOfMeasureCollection displayUnits = 
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId);

            if (this.ViewModel == null) return;

            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                displayUnits, 
                AssortmentProductBuddyViewModel.ScreenKey);

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnsChanging;
            _columnLayoutManager.AttachDataGrid(this.ProductBuddyGrid);
        }

        #endregion

        #region Window close

        /// <summary>
        /// Responds to the window cross being clicked
        /// </summary>
        /// <param name="e"></param>
        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!this.ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }

        /// <summary>
        /// Method to override the on closed method
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion

        private void ProductBuddyGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;
                FocusNavigationDirection direction = FocusNavigationDirection.Down;
                TraversalRequest tRequest = new TraversalRequest(direction);
                senderControl?.MoveFocus(tRequest);
                e.Handled = true;
            }
        }
    }
}
