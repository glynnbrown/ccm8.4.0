﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentAssortmentProductBuddyWizardWindow.xaml
    /// </summary>
    public sealed partial class AssortmentProductBuddyWizardWindow : ExtendedRibbonWindow
    {
        #region Constants

        const String RemoveSourceProductCommandKey = "RemoveSourceProductCommand";

        #endregion

        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentProductBuddyWizardViewModel), typeof(AssortmentProductBuddyWizardWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets the viewmodel controller for this window
        /// </summary>
        public AssortmentProductBuddyWizardViewModel ViewModel
        {
            get { return (AssortmentProductBuddyWizardViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentProductBuddyWizardWindow senderControl = (AssortmentProductBuddyWizardWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentProductBuddyWizardViewModel oldModel = (AssortmentProductBuddyWizardViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                //remove any resource commands
                senderControl.Resources.Remove(RemoveSourceProductCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentProductBuddyWizardViewModel newModel = (AssortmentProductBuddyWizardViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                //add RemoveSourceProductCommand as a resource
                senderControl.Resources.Add(RemoveSourceProductCommandKey, newModel.RemoveSourceProductCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="selectedRanges"></param>
        /// <param name="selectedProduct"></param>
        public AssortmentProductBuddyWizardWindow(Assortment assortment, AssortmentProduct selectedProduct)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new AssortmentProductBuddyWizardViewModel(assortment, selectedProduct);

            this.Loaded += AssortmentProductBuddyWizardWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial first load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentProductBuddyWizardWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentProductBuddyWizardWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Forces the toggle button to be uncheckable
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToggleButton_ForceUncheckable(object sender, MouseButtonEventArgs e)
        {
            if (((ToggleButton)sender).IsChecked == true)
            {
                //if the original source was not an ordinary button
                if (((DependencyObject)e.OriginalSource).FindVisualAncestor<Button>() == null)
                {
                    e.Handled = true;
                }
            }
        }

        private void ToggleButton_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {           
            if (e.Key == Key.Return)
            {
                
                var buttonType = e.OriginalSource.GetType();

                if (buttonType == typeof(Button)) return;

                    var senderControl = sender as ToggleButton;

                    if (senderControl != null)
                        {
                            senderControl.IsChecked = !senderControl.IsChecked;
                            e.Handled = true;
                        }
            }
        }
        #endregion

        #region Window close

        /// <summary>
        /// Method to override the on closed method
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
     
    }
}
