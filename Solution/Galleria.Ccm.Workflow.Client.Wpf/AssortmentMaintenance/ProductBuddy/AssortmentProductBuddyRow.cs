﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
// CCM-14014 : M.Pettit
//  Added reference to linked product model for viewing attributes on the grid
// CCM-14012 : A.Heathcote
//  Added to the constructor to allow the Target Product to be from outside the assortment.
// CCM-18489 : A.Heathcote
//  Changed the error in IDataErrorInfo Members for percentages to allow 1000% instead of 100%
// CCM-14014 : M.Pettit
//  Constructor now fetches the product model in case any optional columns have been added
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Model;
using System.Collections.Generic;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Represents a row in the product buddy window
    /// </summary>
    public sealed class AssortmentProductBuddyRow : ViewModelObject
    {
        #region Fields

        private Boolean _isDirty;

        private Assortment _assortment;
        private Dictionary<Int32, ProductInfo> _availableProducts;
        private AssortmentProductBuddy _existingProductBuddy;
        private AssortmentProductBuddy _productBuddy;

        private AssortmentProduct _targetProduct;
        private Product _product;

        private ObservableCollection<ProductBuddySourceRow> _sourceRows = new ObservableCollection<ProductBuddySourceRow>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyRow>(p => p.IsDirty);
        public static readonly PropertyPath TargetProductProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyRow>(p => p.TargetProduct);
        public static readonly PropertyPath SummaryProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyRow>(p => p.Summary);
        public static readonly PropertyPath SourceRowsProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyRow>(p => p.SourceRows);

        #endregion

        #region Properties

        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        /// <summary>
        /// Gets the target product for this row.
        /// </summary>
        public AssortmentProduct TargetProduct
        {
            get { return _targetProduct; }
        }

        /// <summary>
        /// Gets/Sets the linked Master Product for the assortment product reference, if set
        /// </summary>
        public Product Product
        {
            get { return _product; }
        }

        /// <summary>
        /// Gets a summary of the product buddy
        /// </summary>
        public String Summary
        {
            get
            {
                String value;

                switch (this.Buddy.SourceType)
                {
                    case PlanogramAssortmentProductBuddySourceType.Attribute:

                        value = String.Format(
                            CultureInfo.CurrentCulture,
                            "{0}: {1}, {2}",
                            PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddySourceType.Attribute],
                            PlanogramAssortmentProductBuddyProductAttributeTypeHelper.FriendlyNames[this.Buddy.ProductAttributeType],
                            PlanogramAssortmentProductBuddyTreatmentTypeHelper.FriendlyNames[this.Buddy.TreatmentType]);

                        if (this.Buddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || this.Buddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                        {
                            value = String.Format(CultureInfo.CurrentCulture, "{0} : {1}%", value, this.Buddy.TreatmentTypePercentage * 100);
                        }

                        break;

                    case PlanogramAssortmentProductBuddySourceType.Manual:

                        value = PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames[PlanogramAssortmentProductBuddySourceType.Manual];

                        if (this.SourceRows.Count > 0)
                        {
                            value =
                                String.Format(
                            CultureInfo.CurrentCulture,
                            "{0}: {1}",
                            value,
                                this.SourceRows[0].Product.Gtin);
                        }
                        if (this.SourceRows.Count > 1)
                        {
                            value = String.Format(
                            CultureInfo.CurrentCulture,
                            "{0} +{1} {2}",
                            value,
                            this.SourceRows.Count - 1,
                            Message.AssortmentProductBuddy_Summary_Others);
                        }

                        break;

                    default:
                        value = PlanogramAssortmentProductBuddySourceTypeHelper.FriendlyNames[this.Buddy.SourceType];
                        break;
                }

                return value;
            }
        }

        /// <summary>
        /// Returns the editing product buddy
        /// </summary>
        public AssortmentProductBuddy Buddy
        {
            get { return _productBuddy; }
        }

        /// <summary>
        /// Returns the collection of source rows
        /// </summary>
        public ObservableCollection<ProductBuddySourceRow> SourceRows
        {
            get { return _sourceRows; }
        }

        #endregion

        #region Constructor

        public AssortmentProductBuddyRow(AssortmentProductBuddy buddy, Dictionary<Int32, ProductInfo> availableProducts)
        {
            Debug.Assert(buddy.Parent != null, "Parameter should be an existing buddy");

            _existingProductBuddy = buddy;
            _productBuddy = _existingProductBuddy.Clone();
            _assortment = buddy.Parent;
            _availableProducts = availableProducts;

            //fetch the source product model object as it may be needed for custom columns
            try
            {
                _product = Product.FetchById(buddy.ProductId);
            }
            catch
            {
                //do nothing - the product may have been deleted
            }

            //get the target product
            if (_assortment != null)
            {
                _targetProduct = _assortment.Products.FirstOrDefault(p => p.ProductId.Equals(buddy.ProductId));
                if (_targetProduct == null)
                {
                    List<ProductInfo> repositoryProducts = new List<ProductInfo>();
                    repositoryProducts.AddRange(ProductInfoList.FetchByEntityId(App.ViewState.EntityId).Where(p => p.Id.Equals(buddy.ProductId)));
                    _targetProduct = AssortmentProduct.NewAssortmentProduct(repositoryProducts.FirstOrDefault());
                }
            }

            ResetSourceRows();

            _isDirty = false;
            _productBuddy.PropertyChanged += ProductBuddy_PropertyChanged;
            _sourceRows.CollectionChanged += SourceRows_CollectionChanged;
        }

        public AssortmentProductBuddyRow(Assortment assortment, Product product, AssortmentProduct targetProduct, Dictionary<Int32, ProductInfo> availableProducts)
        {
            _targetProduct = targetProduct;
            _product = product;
            _assortment = assortment;
            _availableProducts = availableProducts;

            //check for an existing buddy
            Int32 targetProductId = targetProduct.ProductId;
            _existingProductBuddy = assortment.ProductBuddies.FirstOrDefault(p => p.ProductId.Equals(targetProductId));

            if (_existingProductBuddy == null)
            {
                _productBuddy = AssortmentProductBuddy.NewAssortmentProductBuddy();
                _productBuddy.ProductId = targetProductId;
                _isDirty = true;
            }
            else
            {
                _productBuddy = _existingProductBuddy.Clone();
                _isDirty = false;
            }

            ResetSourceRows();

            _productBuddy.PropertyChanged += ProductBuddy_PropertyChanged;
            _sourceRows.CollectionChanged += SourceRows_CollectionChanged;
        }



        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes on the product buddy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductBuddy_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(SummaryProperty);
            this.IsDirty = true;
        }

        /// <summary>
        /// Responds to changes to the source rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourceRows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //resync all buddies
            _productBuddy.S1ProductId = null;
            _productBuddy.S1Percentage = null;
            _productBuddy.S2ProductId = null;
            _productBuddy.S2Percentage = null;
            _productBuddy.S3ProductId = null;
            _productBuddy.S3Percentage = null;
            _productBuddy.S4ProductId = null;
            _productBuddy.S4Percentage = null;
            _productBuddy.S5ProductId = null;
            _productBuddy.S5Percentage = null;


            if (this.SourceRows.Count > 0)
            {
                _productBuddy.S1ProductId = this.SourceRows[0].Product.Id;
                _productBuddy.S1Percentage = this.SourceRows[0].Percentage;
            }

            if (this.SourceRows.Count > 1)
            {
                _productBuddy.S2ProductId = this.SourceRows[1].Product.Id;
                _productBuddy.S2Percentage = this.SourceRows[1].Percentage;
            }

            if (this.SourceRows.Count > 2)
            {
                _productBuddy.S3ProductId = this.SourceRows[2].Product.Id;
                _productBuddy.S3Percentage = this.SourceRows[2].Percentage;
            }

            if (this.SourceRows.Count > 3)
            {
                _productBuddy.S4ProductId = this.SourceRows[3].Product.Id;
                _productBuddy.S4Percentage = this.SourceRows[3].Percentage;
            }

            if (this.SourceRows.Count > 4)
            {
                _productBuddy.S5ProductId = this.SourceRows[4].Product.Id;
                _productBuddy.S5Percentage = this.SourceRows[4].Percentage;
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Resets the source rows collection
        /// </summary>
        private void ResetSourceRows()
        {
            _sourceRows.Clear();

            AssortmentProductBuddy buddy = _productBuddy;

            if (_assortment != null)
            {
                //source 1
                if (buddy.S1ProductId != null)
                {
                    ProductInfo source1 = buddy.S1ProductId.HasValue && _availableProducts.ContainsKey(buddy.S1ProductId.Value) ? _availableProducts[buddy.S1ProductId.Value] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source1, buddy.S1Percentage));
                }

                //source 2
                if (buddy.S2ProductId != null)
                {
                    ProductInfo source2 = buddy.S2ProductId.HasValue && _availableProducts.ContainsKey(buddy.S2ProductId.Value) ? _availableProducts[buddy.S2ProductId.Value] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source2, buddy.S2Percentage));
                }

                //source 3
                if (buddy.S3ProductId != null)
                {
                    ProductInfo source3 = buddy.S3ProductId.HasValue && _availableProducts.ContainsKey(buddy.S3ProductId.Value) ? _availableProducts[buddy.S3ProductId.Value] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source3, buddy.S3Percentage));
                }

                //source 4
                if (buddy.S4ProductId != null)
                {
                    ProductInfo source4 = buddy.S4ProductId.HasValue && _availableProducts.ContainsKey(buddy.S4ProductId.Value) ? _availableProducts[buddy.S4ProductId.Value] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source4, buddy.S4Percentage));
                }

                //source 5
                if (buddy.S5ProductId != null)
                {
                    ProductInfo source5 = buddy.S5ProductId.HasValue && _availableProducts.ContainsKey(buddy.S5ProductId.Value) ? _availableProducts[buddy.S5ProductId.Value] : null;
                    _sourceRows.Add(new ProductBuddySourceRow(source5, buddy.S5Percentage));
                }
            }
        }

        /// <summary>
        /// Commits changes back to the source buddy
        /// </summary>
        public void CommitChanges()
        {
            if (_isDirty)
            {
                Assortment applyToAssortment = _assortment;
                AssortmentProductBuddy existingBuddy = _existingProductBuddy;
                AssortmentProductBuddy editBuddy = _productBuddy;

                #region ensure all source percentages are the same as the source rows
                foreach (ProductBuddySourceRow sourceRow in _sourceRows)
                {
                    if (editBuddy.S1ProductId != null && editBuddy.S1ProductId.Equals(sourceRow.Product.Id))
                    {
                        editBuddy.S1Percentage = sourceRow.Percentage;
                    }
                    if (editBuddy.S2ProductId != null && editBuddy.S2ProductId.Equals(sourceRow.Product.Id))
                    {
                        editBuddy.S2Percentage = sourceRow.Percentage;
                    }
                    if (editBuddy.S3ProductId != null && editBuddy.S3ProductId.Equals(sourceRow.Product.Id))
                    {
                        editBuddy.S3Percentage = sourceRow.Percentage;
                    }
                    if (editBuddy.S4ProductId != null && editBuddy.S4ProductId.Equals(sourceRow.Product.Id))
                    {
                        editBuddy.S4Percentage = sourceRow.Percentage;
                    }
                    if (editBuddy.S5ProductId != null && editBuddy.S5ProductId.Equals(sourceRow.Product.Id))
                    {
                        editBuddy.S5Percentage = sourceRow.Percentage;
                    }
                }
                #endregion

                if (existingBuddy == null)
                {
                    //just add the edit buddy
                    applyToAssortment.ProductBuddies.Add(editBuddy);

                }
                else
                {
                    //copy the values
                    existingBuddy.SourceType = editBuddy.SourceType;
                    existingBuddy.TreatmentType = editBuddy.TreatmentType;
                    existingBuddy.TreatmentTypePercentage = (editBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg
                        || editBuddy.TreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                        ? editBuddy.TreatmentTypePercentage
                        : 1;
                    existingBuddy.ProductAttributeType = editBuddy.ProductAttributeType;
                    existingBuddy.S1ProductId = editBuddy.S1ProductId;
                    existingBuddy.S2ProductId = editBuddy.S2ProductId;
                    existingBuddy.S3ProductId = editBuddy.S3ProductId;
                    existingBuddy.S4ProductId = editBuddy.S4ProductId;
                    existingBuddy.S5ProductId = editBuddy.S5ProductId;
                    existingBuddy.S1Percentage = editBuddy.S1Percentage;
                    existingBuddy.S2Percentage = editBuddy.S2Percentage;
                    existingBuddy.S3Percentage = editBuddy.S3Percentage;
                    existingBuddy.S4Percentage = editBuddy.S4Percentage;
                    existingBuddy.S5Percentage = editBuddy.S5Percentage;
                }

                _isDirty = false;
            }
        }

        /// <summary>
        /// Deletes the existing source buddy.
        /// </summary>
        public void DeleteExistingBuddy()
        {
            if (_existingProductBuddy != null)
            {
                _existingProductBuddy.Parent.ProductBuddies.Remove(_existingProductBuddy);
                _existingProductBuddy = null;
                _isDirty = false;
            }
        }

        public void OnSourceRowChanged()
        {
            _isDirty = true;
        }

        public Boolean IsValid()
        {
            if (!_sourceRows.All(r => r.IsValid()) || !this.Buddy.IsValid)
            {
                return false;
            }
            return true;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    _productBuddy.PropertyChanged -= ProductBuddy_PropertyChanged;
                }

                IsDisposed = true;
            }
        }

        #endregion
    }

    public sealed class ProductBuddySourceRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields
        private ProductInfo _product;
        private Single? _percentage;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<ProductBuddySourceRow>(p => p.Product);
        public static readonly PropertyPath PercentageProperty = WpfHelper.GetPropertyPath<ProductBuddySourceRow>(p => p.Percentage);

        #endregion

        #region Properties

        public ProductInfo Product
        {
            get { return _product; }
        }

        public Single? Percentage
        {
            get { return _percentage; }
            set
            {
                _percentage = value;
                OnPropertyChanged(PercentageProperty);
            }

        }

        #endregion

        #region Constructor

        public ProductBuddySourceRow(ProductInfo product, Single? percentage)
        {
            _product = product;
            _percentage = percentage;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns true if this row is valid
        /// </summary>
        public Boolean IsValid()
        {
            return (String.IsNullOrEmpty(this["Percentage"]));
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public String Error
        {
            get { return String.Empty; }
        }

        public String this[String columnName]
        {
            get
            {
                if (columnName == PercentageProperty.Path)
                {
                    //Changed to allow 1000% as 100% was not enough and previously in other screens 1000% was the maximum allowed.
                    if (this.Percentage == null || !(this.Percentage > 0 && this.Percentage <= 10))
                    {
                        return Message.AssortmentProductBuddyWizard_InvalidSourcePercentage;
                    }
                }

                return String.Empty;
            }
        }

        #endregion
    }
}
