﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
// V8-31950 : A.Probyn
//  Updated for multi select products.
//  Updated to remove App.ShowWindow on assortment product selector
// V8-32396 : A.Probyn
//  Updated to allow selection from outside the assortment for buddies
// V8-32718 : A.Probyn
//  Updated so full product attributes can be displayed for target product selection
// V8-32812 : A.Heathcote
//  Updated to allow multiple product selection
// CCM-14012 : A.Heathcote
//  Updated SelectMultipleTargetProduct_Executed() to allow use of the new Product Selector constructor.
// CCM-18490 : M.Pettit
//  AddProducts command now passes the assortment's category as an inital category to the selector window
// CCM18467 : A.Heathcote
//  Added to SelectSpecificTarget Command to make sure the _useSpecificProduct boolean is changed appropriatly
// CCM-18489 : A.Heathcote
//  Changed the error in IDataErrorInfo Members for percentages to allow 1000% instead of 100%
// CCM-18458 : A.Heathcote
//  Added the excluded list to satisfy the edited constructor. (the list being passed is empty)
// CCM-18578 : L.Ineson
//  Fixed null ref error with using current product.
#endregion

#endregion

using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Used to define steps in the Product Buudy
    /// wizard and display relevant fields.
    /// </summary>
    public enum AssortmentProductBuddyWizardStep
    {
        SelectProduct,
        SelectSource,
        SelectBuddies,          //Manual buddy selection
        SelectAttribute         //Use Product Attribute to select buddies
    }

    /// <summary>
    /// ViewModel controller class for AssortmentProductBuddyWizardWindow
    /// </summary>
    public sealed class AssortmentProductBuddyWizardViewModel : ViewModelAttachedControlObject<AssortmentProductBuddyWizardWindow>, IDataErrorInfo
    {
        #region Fields

        private Assortment _currentAssortment;
        private Dictionary<Int32, Product> _sourceAssortmentProductLookup = new Dictionary<Int32, Product>();
        private AssortmentProductBuddyWizardStep _currentStep = AssortmentProductBuddyWizardStep.SelectProduct;
        private Int16 _currentStepNumber = 1;
        private Boolean _usePreselectedProduct;
        private Boolean _useSpecificProduct;
        private Boolean _useMultipleProduct;
        private AssortmentProduct _preselectedProduct;
        private AssortmentProduct _specificProduct;
        private String _multipleSelectionText;
        private List<AssortmentProduct> _multipleProduct = new List<AssortmentProduct>();
        private PlanogramAssortmentProductBuddySourceType _selectedSourceType;
        private PlanogramAssortmentProductBuddyTreatmentType _selectedTreatmentType;
        private Single _selectedTreamentTypePercentage;
        private ReadOnlyCollection<PlanogramAssortmentProductBuddyProductAttributeType> _availableProductAttributesRO;
        private PlanogramAssortmentProductBuddyProductAttributeType _selectedProductAttribute;
        private BulkObservableCollection<AssortmentProductBuddyWizardSourceRow> _selectedSourceProducts = new BulkObservableCollection<AssortmentProductBuddyWizardSourceRow>();
        private Boolean _useAttribute = false;
        private Boolean _useManualSelect = false;
        private List<AssortmentProductView> sourceList = new List<AssortmentProductView>();
        private List<Product> productList = new List<Product>();
        private Int32 _productGroupId;
        #endregion

        #region Binding Property Paths

        //Property Paths
        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath CurrentStepNumberProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.CurrentStepNumber);
        public static readonly PropertyPath UseAttributeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.UseAttribute);
        public static readonly PropertyPath UseManualSelectProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.UseManualSelect);
        public static readonly PropertyPath UsePreselectedProductProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.UsePreselectedProduct);
        public static readonly PropertyPath UseSpecificProductProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.UseSpecificProduct);
        public static readonly PropertyPath UseMultipleProductProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.UseMultipleProduct);
        public static readonly PropertyPath PreselectedProductProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.PreselectedProduct);
        public static readonly PropertyPath SpecificProductProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.SpecificProduct);
        public static readonly PropertyPath SelectedTreatmentTypeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.SelectedTreatmentType);
        public static readonly PropertyPath SelectedTreatmentTypePercentageProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.SelectedTreatmentTypePercentage);
        public static readonly PropertyPath AvailableProductAttributesProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.AvailableProductAttributes);
        public static readonly PropertyPath SelectedProductAttributeProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.SelectedProductAttribute);
        public static readonly PropertyPath SelectedSourceProductsProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.SelectedSourceProducts);
        public static readonly PropertyPath MultipleSelectionTextProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.MultipleSelectionText);
        
        //Command Paths
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.PreviousCommand);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.NextCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath SelectTargetProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.SelectTargetProductCommand);
        public static readonly PropertyPath SelectMultipleProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.SelectMultipleTargetProductCommand);
        public static readonly PropertyPath AddSourceProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.AddSourceProductCommand);
        public static readonly PropertyPath RemoveSourceProductCommandProperty = WpfHelper.GetPropertyPath<AssortmentProductBuddyWizardViewModel>(p => p.RemoveSourceProductCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the current step
        /// </summary>
        public AssortmentProductBuddyWizardStep CurrentStep
        {
            get { return _currentStep; }
            set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);

                //Update the next command name
                if (value == AssortmentProductBuddyWizardStep.SelectBuddies || value == AssortmentProductBuddyWizardStep.SelectAttribute)
                {
                    this.NextCommand.FriendlyName = Message.Generic_Finish;
                }
                else
                {
                    this.NextCommand.FriendlyName = Message.Generic_Next;
                }
            }
        }

        /// <summary>
        /// Gets the current step number
        /// </summary>
        public Int32 CurrentStepNumber
        {
            get
            {
                //This number provides the 'Step (x)' number. The 'of (y)' part of this text block is held at Messages.AssortmentProductBuddyWizard_NumberOfStages.
                return _currentStepNumber;
            }
        }

        /// <summary>
        /// Gets/sets the value of UseAttribute
        /// </summary>
        public Boolean UseAttribute
        {
            get { return _useAttribute; }
            set
            {
                _useAttribute = value;
                if (value)
                {
                    this.UseManualSelect = false;
                    _selectedSourceType = PlanogramAssortmentProductBuddySourceType.Attribute;
                }                
                OnPropertyChanged(UseAttributeProperty);
            }
        }

        /// <summary>
        /// Get/sets the value of UseManualSelect
        /// </summary>
        public Boolean UseManualSelect
        {
            get { return _useManualSelect; }
            set
            {
                _useManualSelect = value;
                if (value)
                {
                    this.UseAttribute = false;
                    _selectedSourceType = PlanogramAssortmentProductBuddySourceType.Manual;
                }
               
                OnPropertyChanged(UseManualSelectProperty);
            }
        }

        /// <summary>
        /// Gets/Sets if the currently selected product should be the one to have the product buddy.
        /// </summary>
        public Boolean UsePreselectedProduct
        {
            get { return _usePreselectedProduct; }
            set
            {
                _usePreselectedProduct = value;
                if (_usePreselectedProduct)
                {
                    _useSpecificProduct = false;
                    _useMultipleProduct = false;
                    UseManualSelect = false;
                    UseAttribute = true;
                }
                OnPropertyChanged(UseSpecificProductProperty);
                OnPropertyChanged(UsePreselectedProductProperty);
                OnPropertyChanged(UseMultipleProductProperty);
            }
        }

        public Boolean UseSpecificProduct
        {
            get { return _useSpecificProduct; }
            set { _useSpecificProduct = value;
            if (_useSpecificProduct)
            {
                _usePreselectedProduct = false;
                _useMultipleProduct = false;
            }
            OnPropertyChanged(UseSpecificProductProperty);
            OnPropertyChanged(UsePreselectedProductProperty);
            OnPropertyChanged(UseMultipleProductProperty);
            }
        }

        public Boolean UseMultipleProduct
        {
            get { return _useMultipleProduct; }
            set
            {
                _useMultipleProduct = value;
                if (_useMultipleProduct)
                {
                    _usePreselectedProduct = false;
                    _useSpecificProduct = false;
                }
                OnPropertyChanged(UseSpecificProductProperty);
                OnPropertyChanged(UsePreselectedProductProperty);
                OnPropertyChanged(UseMultipleProductProperty);
            }
        }
        /// <summary>
        /// Returns the currently selected product
        /// </summary>
        public AssortmentProduct PreselectedProduct
        {
            get { return _preselectedProduct; }
        }

        /// <summary>
        /// Gets/Sets the specific product to use
        /// </summary>
        public AssortmentProduct SpecificProduct
        {
            get { return _specificProduct; }
            set
            {
                _specificProduct = value;
                OnPropertyChanged(SpecificProductProperty);
            }
        }

        public String MultipleSelectionText
        {
            get { return _multipleSelectionText; }
            set
            {
                _multipleSelectionText = value;
                OnPropertyChanged(MultipleSelectionTextProperty);
            }
        }
        public List<AssortmentProduct> MultipleProduct
        {
            get { return _multipleProduct;}
            set 
            {
                _multipleProduct = value;                
            }
        }

        /// <summary>
        /// Gets/Sets the selected treatment type
        /// </summary>
        public PlanogramAssortmentProductBuddyTreatmentType SelectedTreatmentType
        {
            get { return _selectedTreatmentType; }
            set
            {
                _selectedTreatmentType = value;
                OnPropertyChanged(SelectedTreatmentTypeProperty);
            }
        }

        /// <summary>
        /// Gets/sets the selected treatment type percentage
        /// </summary>
        public Single SelectedTreatmentTypePercentage
        {
            get { return _selectedTreamentTypePercentage; }
            set
            {
                _selectedTreamentTypePercentage = value;
                OnPropertyChanged(SelectedTreatmentTypePercentageProperty);
            }
        }

        /// <summary>
        /// Returns a readonly collection of available 
        /// product attributes
        /// </summary>
        public ReadOnlyCollection<PlanogramAssortmentProductBuddyProductAttributeType> AvailableProductAttributes
        {
            get { return _availableProductAttributesRO; }
        }

        /// <summary>
        /// Gets/Sets the selected product attribute for attribute source type
        /// </summary>
        public PlanogramAssortmentProductBuddyProductAttributeType SelectedProductAttribute
        {
            get { return _selectedProductAttribute; }
            set
            {
                _selectedProductAttribute = value;
                OnPropertyChanged(SelectedProductAttributeProperty);
            }
        }

        /// <summary>
        /// Returns the collection of selected source products
        /// </summary>
        public BulkObservableCollection<AssortmentProductBuddyWizardSourceRow> SelectedSourceProducts
        {
            get { return _selectedSourceProducts; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentProductBuddyWizardViewModel(Assortment currentAssortment, AssortmentProduct preselectedProduct)
        {
            _currentAssortment = currentAssortment;
            _preselectedProduct = preselectedProduct;
            _productGroupId = currentAssortment.ProductGroupId;
            
            _usePreselectedProduct = (_preselectedProduct != null);
            if (_preselectedProduct == null) _useSpecificProduct = true;
            _selectedTreatmentType = PlanogramAssortmentProductBuddyTreatmentType.Sum;

            //Initialise product lists
            _sourceAssortmentProductLookup = ProductList.FetchByProductIds(currentAssortment.Products.Select(p => p.ProductId)).ToDictionary(p => p.Id);

            _availableProductAttributesRO = PlanogramAssortmentProductBuddyProductAttributeTypeHelper.FriendlyNames.OrderBy(f => f.Value).Select(f => f.Key).ToList().AsReadOnly();
            _selectedProductAttribute = _availableProductAttributesRO.First();
        }

        #endregion

        #region Commands

        #region PreviousCommand

        private RelayCommand _previousCommand;

        /// <summary>
        /// Moves back a screen
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        private Boolean Previous_CanExecute()
        {
            if (this.CurrentStep == AssortmentProductBuddyWizardStep.SelectProduct)
            {
                return false;
            }
            return true;
        }

        private void Previous_Executed()
        {
            _currentStepNumber--;
            OnPropertyChanged(CurrentStepNumberProperty);
            switch (this.CurrentStep)
            {
                case AssortmentProductBuddyWizardStep.SelectProduct:
                    //do nothing
                    break;

                case AssortmentProductBuddyWizardStep.SelectSource:
                    this.CurrentStep = AssortmentProductBuddyWizardStep.SelectProduct;
                    break;

                case AssortmentProductBuddyWizardStep.SelectBuddies:
                    if (this.UseMultipleProduct)
                    {
                        _currentStepNumber--;
                        OnPropertyChanged(CurrentStepNumberProperty);
                        this.UseAttribute = true;
                        this.CurrentStep = AssortmentProductBuddyWizardStep.SelectProduct;
                        break;
                    }
                    this.CurrentStep = AssortmentProductBuddyWizardStep.SelectSource;
                    break;

                case AssortmentProductBuddyWizardStep.SelectAttribute:
                    this.CurrentStep = AssortmentProductBuddyWizardStep.SelectSource;
                    break;

                default: throw new NotImplementedException();
            }
        }

        #endregion

        #region NextCommand

        private RelayCommand _nextCommand;

        /// <summary>
        /// Moves to the next stage of the wizard
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        private Boolean Next_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case AssortmentProductBuddyWizardStep.SelectProduct:
                    if (this.UseSpecificProduct && (this.SpecificProduct == null))
                    {
                        //no specific product selected
                        this.NextCommand.DisabledReason = Message.AssortmentProductBuddyWizard_Next_DisabledNoTargetProduct;
                        return false;
                    }
                    else if (this.UseMultipleProduct && (this.MultipleProduct.Count() == 0))
                    {
                        this.NextCommand.DisabledReason = Message.AssortmentProductBuddyWizard_Next_DisabledNoTargetProduct;
                        return false;
                    }
                    break;

                case AssortmentProductBuddyWizardStep.SelectSource:
                    if (!_useAttribute && !_useManualSelect)
                    {
                        //no specific product selected
                        this.NextCommand.DisabledReason = Message.AssortmentProductBuddyWizard_Next_DisabledNoSourceSelected;
                        return false;
                    }
                    break;

                case AssortmentProductBuddyWizardStep.SelectAttribute:
                    if ((this.SelectedTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg ||
                        this.SelectedTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg) &&
                        !(this.SelectedTreatmentTypePercentage > 0 && this.SelectedTreatmentTypePercentage <= 1))
                    {
                        return false;
                    }
                    break;

                case AssortmentProductBuddyWizardStep.SelectBuddies:
                    //Must have source products if the source type is manual
                    if (!this.SelectedSourceProducts.Any() && !this.UseMultipleProduct)
                    {
                        this.NextCommand.DisabledReason = Message.AssortmentProductBuddyWizard_Next_DisabledNoSourceProduct;
                        return false;
                    }
                    //source rows must all be valid
                    if (!this.UseMultipleProduct && !this.SelectedSourceProducts.All(r => r.IsValid()))
                    {
                        this.NextCommand.DisabledReason = String.Empty;
                        return false;
                    }
                    if (this.UseMultipleProduct && (this._selectedTreamentTypePercentage == 0 || this._selectedTreamentTypePercentage > 10.0))
                    {
                        this.NextCommand.DisabledReason = Message.ProductBuddyWizard_InvalidPercentage;
                        return false;
                    }
                    break;

                default: throw new NotImplementedException();
            }

            return true;
        }

        private void Next_Executed()
        {
            _currentStepNumber++;
            OnPropertyChanged(CurrentStepNumberProperty);
            switch (this.CurrentStep)
            {
                case AssortmentProductBuddyWizardStep.SelectProduct:
                    if (this.UseMultipleProduct)
                    {
                        _currentStepNumber++;
                        OnPropertyChanged(CurrentStepNumberProperty);
                        this.UseManualSelect = true;
                        this.SelectedTreatmentTypePercentage = 1;
                        this.CurrentStep = AssortmentProductBuddyWizardStep.SelectBuddies;
                        break;
                    }
                    this.CurrentStep = AssortmentProductBuddyWizardStep.SelectSource;
                    this.UseAttribute = true;
                    break;
                    
                case AssortmentProductBuddyWizardStep.SelectSource:
                    if (_useAttribute)
                    {
                        this.CurrentStep = AssortmentProductBuddyWizardStep.SelectAttribute;
                    }
                    else if (_useManualSelect)
                    {
                        this.CurrentStep = AssortmentProductBuddyWizardStep.SelectBuddies;
                    }
                    break;

                case AssortmentProductBuddyWizardStep.SelectAttribute:
                    //close the window
                    WizardComplete();
                    break;

                case AssortmentProductBuddyWizardStep.SelectBuddies:
                    //close the window
                    WizardComplete();
                    break;
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the wizard
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = false;
                this.AttachedControl.Close();
            }
        }
        #endregion

        #region SelectTargetProductCommand

        private RelayCommand _selectTargetProductCommand;

        /// <summary>
        /// Shows a dialog to select a target product
        /// </summary>
        public RelayCommand SelectTargetProductCommand
        {
            get
            {
                if (_selectTargetProductCommand == null)
                {
                    _selectTargetProductCommand = new RelayCommand(
                        p => SelectTargetProduct_Executed())
                    {
                        FriendlyName = ".."
                    };
                    base.ViewModelCommands.Add(_selectTargetProductCommand);
                }
                return _selectTargetProductCommand;
            }
        }

        private void SelectTargetProduct_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentProductView sourceProduct = null;

                if (this.AttachedControl != null)
                {
                    List<Int32> excludeProductIds = new List<Int32>();
                    excludeProductIds.AddRange(this.SelectedSourceProducts.Select(r => r.Product.Id));

                    List<AssortmentProductView> availableProducts = new List<AssortmentProductView>();
                    foreach (AssortmentProduct assortProduct in this._currentAssortment.Products.Where(p => !excludeProductIds.Contains(p.ProductId)))
                    { 
                        Product fullProduct = null;
                        _sourceAssortmentProductLookup.TryGetValue(assortProduct.ProductId, out fullProduct);
                        availableProducts.Add(new AssortmentProductView(assortProduct, fullProduct));
                    }

                    AssortmentProductSelectionWindow win = new AssortmentProductSelectionWindow(availableProducts);
                    CommonHelper.GetWindowService().ShowDialog<AssortmentProductSelectionWindow>(win);

                    if (win.DialogResult == true &&
                        win.SelectionResult != null)
                    {
                        sourceProduct = win.SelectionResult.FirstOrDefault();
                    }
                }

                if (sourceProduct != null)
                {
                    this.SpecificProduct = sourceProduct.AssortmentProduct;

                    //switch the selected option to use the product just selected.
                    this.UsePreselectedProduct = false;
                    this.UseSpecificProduct = true;
                }
            }
        }

        #endregion

        #region SelectMultipleTargetProductCommand
        private RelayCommand _selectMultipleTargetProductCommand;

        public RelayCommand SelectMultipleTargetProductCommand
        {
            get
            {
                if (_selectMultipleTargetProductCommand == null)
                {
                    _selectMultipleTargetProductCommand = new RelayCommand(
                        p => SelectMultipleTargetProduct_Executed())
                        {
                            FriendlyName = ".."
                        };
                    base.ViewModelCommands.Add(_selectMultipleTargetProductCommand);
                }
                return _selectMultipleTargetProductCommand;
            }
        }

        private void SelectMultipleTargetProduct_Executed()
        {
            if (this.AttachedControl != null)
            {

                AssortmentProductView sourceProduct = null;
                List<Int32> excludeProductIds = new List<Int32>();
                excludeProductIds.AddRange(this.SelectedSourceProducts.Select(r => r.Product.Id));

                if (MultipleProduct.Count != 0)
                {
                    MultipleProduct.Clear();
                    productList.Clear();
                }
                List<String> gtins = new List<String>();
                ProductSelectorViewModel viewModel = new ProductSelectorViewModel(false, _productGroupId, gtins);
                ProductSelectorWindow win = new ProductSelectorWindow(viewModel);
                CommonHelper.GetWindowService().ShowDialog<ProductSelectorWindow>(win);

                if (win.DialogResult == true &&
                    win.ViewModel.AssignedProducts != null)
                {                        
                    foreach(Product result in win.ViewModel.AssignedProducts)
                    {
                        productList.Add(result);
                        sourceList.Add(new AssortmentProductView(AssortmentProduct.NewAssortmentProduct(result), result));
                    }
                }            

                if (sourceList.Count != 0)
                {
                    if (MultipleProduct.Count != 0) MultipleProduct.Clear();
                    sourceProduct = sourceList.FirstOrDefault();
                    this.MultipleSelectionText = productList.Count.ToString() + Message.ProductBuddyWizard_MultipleBuddySourceText;
                    foreach (AssortmentProductView product in sourceList)
                    {
                       this.MultipleProduct.Add(product.AssortmentProduct);                        
                    }
                    //switch the selected option to use the product just selected.
                    this.UsePreselectedProduct = false;
                    this.UseMultipleProduct = true;
                }
            }
        }
   
        
        #endregion

        #region AddSourceProductCommand

        private static RelayCommand _addSourceProductCommand;

        /// <summary>
        /// Shows the selection dialog to add a source product
        /// </summary>
        public RelayCommand AddSourceProductCommand
        {
            get
            {
                if (_addSourceProductCommand == null)
                {
                    _addSourceProductCommand = new RelayCommand(
                        p => AddSourceProduct_Executed(),
                        p => AddSourceProduct_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddyWizard_AddSourceProduct,
                        SmallIcon = ImageResources.AssortmentProductBuddyWizard_AddSourceProduct
                    };
                    base.ViewModelCommands.Add(_addSourceProductCommand);
                }
                return _addSourceProductCommand;
            }
        }

        private Boolean AddSourceProduct_CanExecute()
        {
            //max 5 allowed
            if (this.SelectedSourceProducts.Count >= 5)
            {
                this.AddSourceProductCommand.DisabledReason = Message.AssortmentProductBuddyWizard_AddSourceProduct_DisabledMax;
                return false;
            }

            return true;
        }

        private void AddSourceProduct_Executed()
        {
            if (this.AttachedControl != null)
            {
                if (this.AttachedControl != null)
                {
                    List<String> excludeProducts = new List<String>();
                    excludeProducts.AddRange(this.SelectedSourceProducts.Select(r => r.Product.Gtin));

                    ProductSelectorHelper productSelectorHelper = new ProductSelectorHelper();
                    productSelectorHelper.IsMultiSelectEnabled = true;
                    productSelectorHelper.ExcludedProducts.AddRange(excludeProducts);

                    var winModel = new ProductSelectorViewModel(productSelectorHelper, _currentAssortment.ProductGroupId);
                    CommonHelper.GetWindowService().ShowDialog<ProductSelectorWindow>(winModel);

                    if (winModel.DialogResult == true)
                    {
                        IEnumerable<Product> sourceProducts = winModel.AssignedProducts.ToList();
                        if (sourceProducts != null)
                        {
                            if (this.SelectedSourceProducts.Count < 5)
                            {
                                Int32 numberToAdd = 5 - SelectedSourceProducts.Count;
                                foreach (Product sourceProduct in sourceProducts.Take(numberToAdd))
                                {
                                    this.SelectedSourceProducts.Add(new AssortmentProductBuddyWizardSourceRow(ProductInfo.NewProductInfo(sourceProduct)));
                                }
                            }
                        }
                    }
                }
            }
        }


        private void ProductSelectionWindow_AddSourceProductSelectionRequested(object sender, Framework.Model.EventArgs<IEnumerable<ProductInfo>> e)
        {
            IEnumerable<ProductInfo> sourceProducts = e.ReturnValue.Except(this.SelectedSourceProducts.Select(p => p.Product)).ToList();

            
        }
         
        
        #endregion

        #region RemoveSourceProductCommand

        private RelayCommand _removeSourceProductCommand;

        /// <summary>
        /// Removes the given source product
        /// </summary>
        public RelayCommand RemoveSourceProductCommand
        {
            get
            {
                if (_removeSourceProductCommand == null)
                {
                    _removeSourceProductCommand = new RelayCommand(
                        p => RemoveSourceProduct_Executed(p),
                        p => RemoveSourceProduct_CanExecute(p))
                    {
                        SmallIcon = ImageResources.AssortmentProductBuddyWizard_RemoveSourceProduct
                    };
                    base.ViewModelCommands.Add(_removeSourceProductCommand);
                }
                return _removeSourceProductCommand;
            }
        }

        private Boolean RemoveSourceProduct_CanExecute(Object arg)
        {
            AssortmentProductBuddyWizardSourceRow removeProd = arg as AssortmentProductBuddyWizardSourceRow;

            if (removeProd == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveSourceProduct_Executed(Object arg)
        {
            AssortmentProductBuddyWizardSourceRow removeProd = arg as AssortmentProductBuddyWizardSourceRow;
            if (removeProd != null)
            {
                this.SelectedSourceProducts.Remove(removeProd);
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        #endregion

        #region Methods

        /// <summary>
        /// Creates a product buddy from the held values
        /// </summary>
        private AssortmentProductBuddy CreateProductBuddy()
        {
            AssortmentProductBuddy buddy = null;

            AssortmentProduct product =
                (this.UsePreselectedProduct) ? this.PreselectedProduct : this.SpecificProduct;

            if (product != null)
            {
                buddy = AssortmentProductBuddy.NewAssortmentProductBuddy(product);

                buddy.SourceType = _selectedSourceType;
                buddy.TreatmentType = this.SelectedTreatmentType;
                buddy.TreatmentTypePercentage = (this.SelectedTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg
                    || this.SelectedTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg)
                    ? this.SelectedTreatmentTypePercentage
                    : 1;
                
                buddy.ProductAttributeType = this.SelectedProductAttribute;

                if (buddy.SourceType == PlanogramAssortmentProductBuddySourceType.Manual)
                {
                    Int32 sourceCount = this.SelectedSourceProducts.Count;

                    if (sourceCount >= 1)
                    {
                        buddy.S1ProductId = this.SelectedSourceProducts[0].Product.Id;
                        buddy.S1Percentage = this.SelectedSourceProducts[0].Percentage;
                    }

                    if (sourceCount >= 2)
                    {
                        buddy.S2ProductId = this.SelectedSourceProducts[1].Product.Id;
                        buddy.S2Percentage = this.SelectedSourceProducts[1].Percentage;
                    }

                    if (sourceCount >= 3)
                    {
                        buddy.S3ProductId = this.SelectedSourceProducts[2].Product.Id;
                        buddy.S3Percentage = this.SelectedSourceProducts[2].Percentage;
                    }

                    if (sourceCount >= 4)
                    {
                        buddy.S4ProductId = this.SelectedSourceProducts[3].Product.Id;
                        buddy.S4Percentage = this.SelectedSourceProducts[3].Percentage;
                    }

                    if (sourceCount == 5)
                    {
                        buddy.S5ProductId = this.SelectedSourceProducts[4].Product.Id;
                        buddy.S5Percentage = this.SelectedSourceProducts[4].Percentage;
                    }
                }
            }

            return buddy;
        }

        /// <summary>
        /// Called upon successful completion of the wizard
        /// </summary>
        private void WizardComplete()
        {
            if (this.UseMultipleProduct)
            {

               foreach (AssortmentProduct selected in MultipleProduct)
                {
                    this.SpecificProduct = selected;
                    ProductInfo matchingProduct = 
                        ProductInfo.NewProductInfo(productList.FirstOrDefault(p => p.Id == selected.ProductId));
                    if (matchingProduct == null) continue;                   
                    AssortmentProductBuddyWizardSourceRow row = new AssortmentProductBuddyWizardSourceRow(matchingProduct);
                    this.SelectedSourceProducts.Add(row);
                    this.SelectedSourceProducts[0].Percentage = this.SelectedTreatmentTypePercentage;

                    //remove the existing buddy if we have one.
                    AssortmentProductBuddy existingBuddy =
                        _currentAssortment.ProductBuddies.FirstOrDefault(p => String.Compare(selected.Gtin, p.ProductGtin, /*ignoreCase*/true) == 0);
                    if (existingBuddy != null) _currentAssortment.ProductBuddies.Remove(existingBuddy);

                    _currentAssortment.ProductBuddies.Add(CreateProductBuddy());
                    this.SelectedSourceProducts.Clear();
                }
            }
            else
            {
                String targetGTIN = null;
                if (this.UseSpecificProduct && this.SpecificProduct != null)
                {
                    targetGTIN = this.SpecificProduct.Gtin;
                }
                else if (_preselectedProduct != null)
                {
                    targetGTIN = _preselectedProduct.Gtin;
                }

                //remove the existing buddy if we have one.
                AssortmentProductBuddy existingBuddy =
                    _currentAssortment.ProductBuddies.FirstOrDefault(p => String.Compare(targetGTIN, p.ProductGtin, /*ignoreCase*/true) == 0);
                if (existingBuddy != null) _currentAssortment.ProductBuddies.Remove(existingBuddy);

                _currentAssortment.ProductBuddies.Add(CreateProductBuddy());
            }

            //close the window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = true;
                this.AttachedControl.Close();
            }
        }

        #endregion


        #region IDataErrorInfo Members

        public String Error
        {
            get { return String.Empty; }
        }

        public String this[String columnName]
        {
            get
            {
                //treatment type validation
                if (columnName == SelectedTreatmentTypePercentageProperty.Path)
                {
                    //Changed SelectedTreatmentTypePercentage to allow 1000% as 100% was not enough and 1000% was allowed previously in other screens.
                    if ((SelectedTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.BottomPercentAvg || SelectedTreatmentType == PlanogramAssortmentProductBuddyTreatmentType.TopPercentAvg || UseMultipleProduct)
                        && !(SelectedTreatmentTypePercentage > 0 && SelectedTreatmentTypePercentage <= 10))
                    {
                        return Message.AssortmentLocationBuddyReview_EditTreatmentTypePercentagee_NotPercentage;
                    }
                }

                return String.Empty;
            }
        }

        #endregion

        #region IDisposable
        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //dispose of the add source product command as for some
                    //bizarre reason it memory leaks - even when it has no code in it.
                    _addSourceProductCommand = null;
                    _sourceAssortmentProductLookup.Clear();
                    _sourceAssortmentProductLookup = null;
                    _currentAssortment = null;
                    _preselectedProduct = null;
                    _selectedSourceProducts.Clear();
                    _selectedSourceProducts = null;
                    _specificProduct = null;
                    _multipleProduct.Clear();
                    productList.Clear();
                    sourceList.Clear();
                }
                base.IsDisposed = true;
            }
        }
        #endregion
    }

}

