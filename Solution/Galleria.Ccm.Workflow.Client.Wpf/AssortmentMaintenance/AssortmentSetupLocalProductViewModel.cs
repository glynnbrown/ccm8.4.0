﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011
#region Version History: (CCM 8.0)
// V8-25454 : J.Pickup 
//  Created (Copied over from GFS)
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
//  V8-32718 : A.Probyn
//  Updated to have full fat locations available
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    public class AssortmentSetupLocalProductViewModel
        : ViewModelAttachedControlObject<AssortmentSetupLocalProductWindow>
    {
        #region Fields

        private Boolean _isEditing = false; //Is the model object being edited?
        private Boolean _isDirty = false;  //Keeps track of whether any changes made to the current region

        private Assortment _selectedAssortment;
        private Dictionary<Int32, Product> _fullProductLookup;
        private Dictionary<Int16, Location> _fullLocationLookup;
        private AssortmentProduct _selectedLocalAssortmentProduct; //The product in the assortment the user has selected

        //The list of current local products for the assortment
        private BulkObservableCollection<AssortmentProduct> _currentAssortmentLocalProducts = new BulkObservableCollection<AssortmentProduct>();
        private ReadOnlyBulkObservableCollection<AssortmentProduct> _currentAssortmentLocalProductsRO;

        //The list of available locations for the local product to choose from (= assortment locations - ones already added)
        private BulkObservableCollection<AssortmentLocationView> _availableLocations = new BulkObservableCollection<AssortmentLocationView>();
        private ReadOnlyBulkObservableCollection<AssortmentLocationView> _availableLocationsRO;
        //The currently selected location(s) in the available locations list
        private ObservableCollection<AssortmentLocationView> _selectedAvailableLocations = new ObservableCollection<AssortmentLocationView>();

        //The list of taken assortment locations already part of the local products
        private BulkObservableCollection<AssortmentLocationView> _takenLocations = new BulkObservableCollection<AssortmentLocationView>();
        private ReadOnlyBulkObservableCollection<AssortmentLocationView> _takenLocationsRO;
        //The currently selected location(s) in the taken locations list
        private ObservableCollection<AssortmentLocationView> _selectedTakenLocations = new ObservableCollection<AssortmentLocationView>();

        private BulkObservableCollection<AssortmentLocalProduct> _assignedAssortmentLocalProducts = new BulkObservableCollection<AssortmentLocalProduct>();

        private String _selectedLocalProductDescription = String.Empty;

        #endregion

        #region Binding properties

        public static readonly PropertyPath SelectedAssortmentProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.SelectedAssortment);
        public static readonly PropertyPath SelectedLocalAssortmentProductProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.SelectedLocalAssortmentProduct);
        public static readonly PropertyPath SelectedLocalProductDescriptionProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.SelectedLocalProductDescription);
        public static readonly PropertyPath AvailableAssortmentLocalProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.AvailableAssortmentLocalProducts);
        public static readonly PropertyPath CurrentAssortmentLocalProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.CurrentAssortmentLocalProducts);
        public static readonly PropertyPath AvailableLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedAvailableLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.SelectedAvailableLocations);
        public static readonly PropertyPath TakenLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.TakenLocations);
        public static readonly PropertyPath SelectedTakenLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.SelectedTakenLocations);

        //commands
        public static readonly PropertyPath ApplyCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath GetLocalProductCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupLocalProductViewModel>(p => p.GetLocalProductCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment
        /// </summary>
        public Assortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            set
            {
                _selectedAssortment = value;
                OnPropertyChanged(SelectedAssortmentProperty);
                OnPropertyChanged(AvailableAssortmentLocalProductsProperty);
            }
        }

        /// <summary>
        /// Property for the products assigned to the selected Assortment
        /// </summary>
        public ObservableCollection<AssortmentProduct> AvailableAssortmentLocalProducts
        {
            get { return _selectedAssortment.Products.ToObservableCollection(); }
        }

        /// <summary>
        /// Gets/Sets the product in the assortment the user has selected
        /// </summary>
        public AssortmentProduct SelectedLocalAssortmentProduct
        {
            get { return _selectedLocalAssortmentProduct; }
            set
            {
                AssortmentProduct oldModel = _selectedLocalAssortmentProduct;
                _selectedLocalAssortmentProduct = value;
                OnPropertyChanged(SelectedLocalAssortmentProductProperty);
                OnPropertyChanged(SelectedLocalProductDescriptionProperty);
                OnCurrentAssortmentLocalProductChanged(oldModel, value);
            }
        }

        /// <summary>
        /// The selected local product description
        /// </summary>
        public String SelectedLocalProductDescription
        {
            get
            {
                if (this.SelectedLocalAssortmentProduct != null)
                {
                    return String.Format(Message.AssortmentSetup_LocalProduct_LocalProductDescription, SelectedLocalAssortmentProduct.Gtin, SelectedLocalAssortmentProduct.Name);
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns the readonly collection of local products applied within the assortment
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentProduct> CurrentAssortmentLocalProducts
        {
            get
            {
                if (_currentAssortmentLocalProductsRO == null)
                {
                    _currentAssortmentLocalProductsRO = new ReadOnlyBulkObservableCollection<AssortmentProduct>(_currentAssortmentLocalProducts);
                }
                return _currentAssortmentLocalProductsRO;
            }
        }

        /// <summary>
        /// Returns the readonly collection of locations still available for adding to the region
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentLocationView> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyBulkObservableCollection<AssortmentLocationView>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }


        /// <summary>
        /// Returns the readonly collection of locations already added to the region
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentLocationView> TakenLocations
        {
            get
            {
                if (_takenLocationsRO == null)
                {
                    _takenLocationsRO = new ReadOnlyBulkObservableCollection<AssortmentLocationView>(_takenLocations);
                }
                return _takenLocationsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected available locations
        /// </summary>
        public ObservableCollection<AssortmentLocationView> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
        }

        /// <summary>
        /// Returns the collection of selected available locations
        /// </summary>
        public ObservableCollection<AssortmentLocationView> SelectedTakenLocations
        {
            get { return _selectedTakenLocations; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="assortment"></param>
        /// <param name="selectedProduct"></param>
        public AssortmentSetupLocalProductViewModel(Assortment assortment, Int32? assortmentProductId)
        {
            //Load assortment selected in previous screen
            this.SelectedAssortment = assortment;

            //Fetch master products once for display purposes
            _fullProductLookup = ProductList.FetchByProductIds(this.SelectedAssortment.Products.Select(p => p.ProductId)).ToDictionary(p => p.Id);
            _fullLocationLookup = LocationList.FetchByEntityId(this.SelectedAssortment.EntityId).ToDictionary(p => p.Id);
            
            // Load assortments current local products
            LoadCurrentLocalProducts();
        }

        #endregion

        #region Commands

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies any changes made in the current item to the parent asssortment
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Apply_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            if (!this._isDirty)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Apply_Executed()
        {
            if (this.SelectedLocalAssortmentProduct != null)
            {
                if (this.TakenLocations.Count > 0)
                {
                    this.SelectedLocalAssortmentProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.Local;
                }
                else
                {
                    this.SelectedLocalAssortmentProduct.ProductLocalizationType = PlanogramAssortmentProductLocalizationType.None;
                }
            }

            LoadCurrentLocalProducts();
        }

        #endregion

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(p => ApplyAndClose_Executed(), p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        [DebuggerStepThrough]
        private void ApplyAndClose_Executed()
        {
            this.Apply_Executed();

            //close the attached window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region GetLocalProductCommand

        private RelayCommand _getLocalProductCommand;

        /// <summary>
        /// Allows selection of available assortment products to be the local product
        /// </summary>
        public RelayCommand GetLocalProductCommand
        {
            get
            {
                if (_getLocalProductCommand == null)
                {
                    _getLocalProductCommand = new RelayCommand(
                        p => GetLocalProduct_Executed())
                    {
                        FriendlyName = Message.Assortment_AddLinkedLocationHierarchyCommand,
                        FriendlyDescription = Message.AssortmentSetup_LocalProductSetup_GetLocalProduct_Tooltip
                    };
                    base.ViewModelCommands.Add(_getLocalProductCommand);
                }
                return _getLocalProductCommand;
            }
        }

        private void GetLocalProduct_Executed()
        {
            if (this.AttachedControl != null)
            {
                // Get list of products that can be the local products
                List<AssortmentProductView> availableAssortmentProducts = new List<AssortmentProductView>();
                foreach (AssortmentProduct assortmentProduct in this.SelectedAssortment.Products.Where(
                                p => p.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Normal &&
                                    p.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.None ||
                                    p.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.Local))
                {
                    Product sourceProduct = null;
                    _fullProductLookup.TryGetValue(assortmentProduct.ProductId, out sourceProduct);
                    availableAssortmentProducts.Add(new AssortmentProductView(assortmentProduct, sourceProduct));                    
                }

                //Show local product selection window
                AssortmentLocalProductSelectionWindow assortmentLocalProductSelectionWindow = new AssortmentLocalProductSelectionWindow(availableAssortmentProducts);
                Galleria.Ccm.Common.Wpf.Helpers.CommonHelper.GetWindowService().ShowDialog<AssortmentLocalProductSelectionWindow>(assortmentLocalProductSelectionWindow);

                //If result returned
                if (assortmentLocalProductSelectionWindow.DialogResult == true)
                {
                    //Set selected product
                    this.SelectedLocalAssortmentProduct = assortmentLocalProductSelectionWindow.SelectionResult.AssortmentProduct;
                }
            }
        }

        #endregion

        #region AddSelectedLocationsCommand

        private RelayCommand _addSelectedLocationsCommand;

        /// <summary>
        /// Adds any selected available locations for the local product
        /// </summary>
        public RelayCommand AddSelectedLocationsCommand
        {
            get
            {
                if (_addSelectedLocationsCommand == null)
                {
                    _addSelectedLocationsCommand = new RelayCommand(
                        p => AddSelectedLocations_Executed(),
                        p => AddSelectedLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddToAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_AddSelected,
                        DisabledReason = Message.AssortmentSetup_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_addSelectedLocationsCommand);
                }
                return _addSelectedLocationsCommand;
            }
        }

        private Boolean AddSelectedLocations_CanExecute()
        {
            //must have a region
            if (this.SelectedLocalAssortmentProduct == null)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentSetup_RegionalProductSetup_NoLocalProductSelected;
                return false;
            }

            //must have locations available
            if (this.AvailableLocations.Count == 0)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentLocations;
                return false;
            }

            //must have selected locations available
            if (this.SelectedAvailableLocations.Count == 0)
            {
                this.AddSelectedLocationsCommand.DisabledReason = Message.AssortmentSetup_NoLocationsSelected;
                return false;
            }

            return true;
        }

        private void AddSelectedLocations_Executed()
        {
            //Add to the taken Locations list so that grid updated
            _takenLocations.AddRange(_selectedAvailableLocations);

            //Add to model region's locations list
            List<AssortmentLocalProduct> newAssortmentLocalProducts = new List<AssortmentLocalProduct>();
            foreach (AssortmentLocationView loc in this.SelectedAvailableLocations)
            {
                AssortmentLocalProduct newAssortmentLocalProduct =
                    AssortmentLocalProduct.NewAssortmentLocalProduct(loc.LocationId, this.SelectedLocalAssortmentProduct.ProductId);
                newAssortmentLocalProducts.Add(newAssortmentLocalProduct);
            }
            _assignedAssortmentLocalProducts.AddRange(newAssortmentLocalProducts);
            this.SelectedAssortment.LocalProducts.AddList(newAssortmentLocalProducts);
            _selectedAvailableLocations.Clear();
        }

        #endregion

        #region AddAllLocationsCommand

        private RelayCommand _addAllLocationsCommand;

        /// <summary>
        /// Adds all available locations for the local product
        /// </summary>
        public RelayCommand AddAllLocationsCommand
        {
            get
            {
                if (_addAllLocationsCommand == null)
                {
                    _addAllLocationsCommand = new RelayCommand(
                        p => AddAllLocations_Executed(),
                        p => AddAllLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddAllToAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_AddAll,
                        DisabledReason = Message.AssortmentSetup_NoLocationsToAdd
                    };
                    base.ViewModelCommands.Add(_addAllLocationsCommand);

                }
                return _addAllLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool AddAllLocations_CanExecute()
        {
            //must have a local assortment product
            if (this.SelectedLocalAssortmentProduct == null)
            {
                this.AddAllLocationsCommand.DisabledReason = Message.AssortmentSetup_RegionalProductSetup_NoLocalProductSelected;
                return false;
            }

            //must have locations available
            if (this.AvailableLocations.Count == 0)
            {
                this.AddAllLocationsCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentLocations;
                return false;
            }

            return true;
        }

        private void AddAllLocations_Executed()
        {
            //Add to the taken Locations list so that grid updated
            _takenLocations.AddRange(_availableLocations);

            //Add to model region's locations list
            List<AssortmentLocalProduct> newAssortmentLocalProducts = new List<AssortmentLocalProduct>();
            foreach (AssortmentLocationView loc in this.AvailableLocations)
            {
                AssortmentLocalProduct newAssortmentLocalProduct =
                    AssortmentLocalProduct.NewAssortmentLocalProduct(loc.LocationId, this.SelectedLocalAssortmentProduct.ProductId);
                newAssortmentLocalProducts.Add(newAssortmentLocalProduct);
            }
            _assignedAssortmentLocalProducts.AddRange(newAssortmentLocalProducts);
            this.SelectedAssortment.LocalProducts.AddList(newAssortmentLocalProducts);
            _selectedAvailableLocations.Clear();
        }

        #endregion

        #region RemoveSelectedLocationsCommand

        private RelayCommand _removeSelectedLocationsCommand;

        /// <summary>
        /// Removes the selected locations for the selected local product
        /// </summary>
        public RelayCommand RemoveSelectedLocationsCommand
        {
            get
            {
                if (_removeSelectedLocationsCommand == null)
                {
                    _removeSelectedLocationsCommand = new RelayCommand(
                        p => RemoveSelectedLocations_Executed(),
                        p => RemoveSelectedLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveFromAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_RemoveSelected,
                        DisabledReason = Message.AssortmentSetup_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_removeSelectedLocationsCommand);
                }
                return _removeSelectedLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RemoveSelectedLocations_CanExecute()
        {
            //must have an assortment selected
            if (this.SelectedLocalAssortmentProduct == null)
            {
                this.RemoveSelectedLocationsCommand.DisabledReason = Message.AssortmentSetup_RegionalProductSetup_NoLocalProductSelected;
                return false;
            }

            if (_selectedTakenLocations.Count == 0)
            {
                this.RemoveSelectedLocationsCommand.DisabledReason = Message.AssortmentSetup_NoLocationsSelected;
                return false;
            }
            else
            {
                this.RemoveSelectedLocationsCommand.DisabledReason = String.Empty;
            }

            return true;
        }

        private void RemoveSelectedLocations_Executed()
        {
            List<Int16> locationIds = this.SelectedTakenLocations.Select(l => l.LocationId).ToList();

            //Remove from taken Locations list so that grid updated
            _takenLocations.RemoveRange(this.SelectedTakenLocations.ToList());

            //Remove from model region's locations list
            IEnumerable<AssortmentLocalProduct> removedLocations = this._assignedAssortmentLocalProducts
                .Where(p => locationIds.Contains(p.LocationId) && p.ProductId == this.SelectedLocalAssortmentProduct.ProductId).ToList();

            this._assignedAssortmentLocalProducts.RemoveRange(removedLocations);
            this.SelectedAssortment.LocalProducts.RemoveList(removedLocations);

            this.SelectedTakenLocations.Clear();
        }

        #endregion

        #region RemoveAllLocationsCommand

        private RelayCommand _removeAllLocationsCommand;

        /// <summary>
        /// Removes all locations from the current local product
        /// </summary>
        public RelayCommand RemoveAllLocationsCommand
        {
            get
            {
                if (_removeAllLocationsCommand == null)
                {
                    _removeAllLocationsCommand = new RelayCommand(
                        p => RemoveAllLocations_Executed(),
                        p => RemoveAllLocations_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveAllFromAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_RemoveAll,
                        DisabledReason = Message.AssortmentSetup_NoLocationsToRemove
                    };
                    base.ViewModelCommands.Add(_removeAllLocationsCommand);
                }
                return _removeAllLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RemoveAllLocations_CanExecute()
        {
            //must have a local assortment product selected
            if (this.SelectedLocalAssortmentProduct == null)
            {
                this.RemoveAllLocationsCommand.DisabledReason = Message.AssortmentSetup_RegionalProductSetup_NoLocalProductSelected;
                return false;
            }

            //must have locations
            if (this._assignedAssortmentLocalProducts.Count == 0)
            {
                this.RemoveAllLocationsCommand.DisabledReason = Message.AssortmentSetup_NoLocationsToRemove;
                return false;
            }

            return true;
        }

        private void RemoveAllLocations_Executed()
        {
            //Remove from taken Locations list so that grid updated
            _takenLocations.Clear();

            //Remove from model region's locations list
            this.SelectedAssortment.LocalProducts.RemoveList(this._assignedAssortmentLocalProducts);
            this._assignedAssortmentLocalProducts.Clear();

            this.SelectedTakenLocations.Clear();
        }


        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Prompt user to continue opening new or existing after changes made
        /// </summary>
        /// <returns></returns>
        public Boolean ContinueWithItemChange()
        {
            if (_isEditing)
            {
                if (_isDirty)
                {
                    //ask user if they want to cancel, save or discard
                    ModalMessageResult messageResult = ModalMessageResult.Button3;
                    ModalMessage win = new ModalMessage();
                    win.Title = System.Windows.Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                    win.Header = Message.Generic_ApplyChangesBeforeContinuingWarning;
                    win.Description = Message.AssortmentSetup_RegionalLocalProductsApplyDiscard;
                    win.MessageIcon = ImageResources.Dialog_Information;
                    win.ButtonCount = 3;
                    win.Button1Content = Message.Generic_Apply;
                    win.Button2Content = Message.Generic_DontApply;
                    win.Button3Content = Message.Generic_Cancel;
                    win.DefaultButton = ModalMessageButton.Button1;
                    win.CancelButton = ModalMessageButton.Button3;
                    App.ShowWindow(win, this.AttachedControl, true);

                    messageResult = win.Result;

                    if (messageResult == ModalMessageResult.Button1)
                    {
                        //save changes
                        this.Apply_Executed();
                        return true;
                    }
                    else if (messageResult == ModalMessageResult.Button2)
                    {
                        //discard any changes
                        return true;
                    }
                    else if (messageResult == ModalMessageResult.Button3)
                    {
                        //user has cancelled 
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Loads the current local products within the selected assortment
        /// </summary>
        private void LoadCurrentLocalProducts()
        {
            this._currentAssortmentLocalProducts.Clear();

            this._currentAssortmentLocalProducts.AddRange(
                this._selectedAssortment.Products.Where(
                    p => p.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.Local));
        }

        /// <summary>
        /// Load local product available \ assigned locations
        /// </summary>
        private void LoadAvailableAndTakenLocations()
        {
            if (this.SelectedLocalAssortmentProduct != null)
            {
                _availableLocations.Clear();
                _takenLocations.Clear();

                List<AssortmentLocalProduct> localProducts = this.SelectedAssortment.LocalProducts.Where(p => p.ProductId == this.SelectedLocalAssortmentProduct.ProductId).ToList();

                foreach (AssortmentLocation location in this.SelectedAssortment.Locations)
                {
                    AssortmentLocalProduct localProduct = localProducts.Where(p => p.LocationId == location.LocationId).FirstOrDefault();

                    if (localProduct != null)
                    {
                        _takenLocations.Add(new AssortmentLocationView(location, _fullLocationLookup.ContainsKey(location.LocationId) ? _fullLocationLookup[location.LocationId] : null));
                        _assignedAssortmentLocalProducts.Add(localProduct);
                        _isDirty = false; 
                    }
                    else
                    {
                        _availableLocations.Add(new AssortmentLocationView(location, _fullLocationLookup.ContainsKey(location.LocationId) ? _fullLocationLookup[location.LocationId] : null));
                    }
                }
            }
        }

        /// <summary>
        /// Returns a list of local products that match the given criteria
        /// </summary>
        /// <param name="NameCriteria"></param>
        /// <returns>A list of regions matching the criteria</returns>
        public IEnumerable<AssortmentProduct> GetMatchingLocalProducts(String NameCriteria)
        {
            return this.CurrentAssortmentLocalProducts.Where(
                p => p.Name.ToUpperInvariant().Contains(NameCriteria));
        }

        #endregion

        #region Event Handlers

        private void OnCurrentAssortmentLocalProductChanged(AssortmentProduct oldModel, AssortmentProduct newModel)
        {
            if (oldModel != null)
            {
                this._assignedAssortmentLocalProducts.BulkCollectionChanged -= new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(AssignedAssortmentLocalProducts_BulkCollectionChanged);
            }

            if (newModel != null)
            {
                this._assignedAssortmentLocalProducts.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(AssignedAssortmentLocalProducts_BulkCollectionChanged);
            }

            LoadAvailableAndTakenLocations();

            _isDirty = false;
        }

        /// <summary>
        /// Respond to assigned local product locations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssignedAssortmentLocalProducts_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (AssortmentLocalProduct item in e.ChangedItems)
                    {
                        //remove from the available list
                        Int32 itemId = item.LocationId;
                        AssortmentLocationView availableLoc = _availableLocations.FirstOrDefault(p => p.LocationId == itemId);
                        _availableLocations.Remove(availableLoc);
                    }
                    _isDirty = true;
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (AssortmentLocalProduct item in e.ChangedItems)
                    {
                        //add back to the available list
                        Int32 itemId = item.LocationId;
                        AssortmentLocation availableLoc = this.SelectedAssortment.Locations.FirstOrDefault(p => p.LocationId == itemId);
                        if (availableLoc != null)
                        {
                            _availableLocations.Add(new AssortmentLocationView(availableLoc, _fullLocationLookup.ContainsKey(availableLoc.LocationId) ? _fullLocationLookup[availableLoc.LocationId] : null));
                        }
                    }
                    _isDirty = true;
                    break;

                case NotifyCollectionChangedAction.Reset:
                    IEnumerable<AssortmentLocationView> availableLocs = this.SelectedAssortment.Locations.Select(p => new AssortmentLocationView(p, _fullLocationLookup.ContainsKey(p.LocationId) ? _fullLocationLookup[p.LocationId] : null));
                    if (availableLocs != null)
                    {
                        _availableLocations.Clear();
                        _availableLocations.AddRange(availableLocs);
                    }
                    _isDirty = true;
                    break;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.SelectedLocalAssortmentProduct = null;
                    _selectedAssortment = null;

                    _fullProductLookup.Clear();
                    _fullProductLookup = null;
                    _fullLocationLookup.Clear();
                    _fullLocationLookup = null;
                    _currentAssortmentLocalProducts.Clear();
                    _availableLocations.Clear();
                    _selectedAvailableLocations.Clear();
                    _takenLocations.Clear();
                    _selectedTakenLocations.Clear();
                    _assignedAssortmentLocalProducts.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}