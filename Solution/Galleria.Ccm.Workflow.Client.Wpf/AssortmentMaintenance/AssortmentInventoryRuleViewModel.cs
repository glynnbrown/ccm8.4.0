﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31551 : A.Probyn
//  Created
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32718 : A.Probyn
//  Updated so full product attributes can be displayed.
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using System.Globalization;
using System.Windows.Input;
using System.Diagnostics;
using System.Collections.Specialized;
using System.ComponentModel;


namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance
{
    /// <summary>
    /// ViewModel controller for the assortment inventory rules screen
    /// </summary>
    public sealed class AssortmentInventoryRuleViewModel : ViewModelAttachedControlObject<AssortmentInventoryRuleWindow>, IDataErrorInfo
    {
        #region Fields

        private Assortment _currentAssortment;
        private Dictionary<Int32, Product> _fullProductLookup;
        private BulkObservableCollection<AssortmentInventoryRuleRow> _productRows = new BulkObservableCollection<AssortmentInventoryRuleRow>();
        private ReadOnlyBulkObservableCollection<AssortmentInventoryRuleRow> _productRowsRO;

        private ObservableCollection<AssortmentInventoryRuleRow> _selectedProductRows = new ObservableCollection<AssortmentInventoryRuleRow>();

        private Single? _editCasePack;
        private Single? _editDaysOfSupply;
        private Single? _editShelfLife;
        private Single? _editReplenishmentDays;
        private AssortmentInventoryRuleForceType? _editForceType;
        private Int32? _editForceValue;
        private AssortmentInventoryRuleWasteHurdleType? _editWasteHurdleType;
        private Single? _editWasteHurdleValue;


        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath CurrentAssortmentProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.CurrentAssortment);
        public static readonly PropertyPath ProductRowsProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.ProductRows);
        public static readonly PropertyPath SelectedProductRowsProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.SelectedProductRows);
        public static readonly PropertyPath EditCasePackProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.EditCasePack);
        public static readonly PropertyPath EditDaysOfSupplyProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.EditDaysOfSupply);
        public static readonly PropertyPath EditShelfLifeProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.EditShelfLife);
        public static readonly PropertyPath EditReplenishmentDaysProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.EditReplenishmentDays);
        public static readonly PropertyPath EditForceTypeProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.EditForceType);
        public static readonly PropertyPath EditForceValueProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.EditForceValue);
        public static readonly PropertyPath EditWasteHurdleTypeProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.EditWasteHurdleType);
        public static readonly PropertyPath EditWasteHurdleValueProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.EditWasteHurdleValue);

        //commands
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath RemoveRuleCommandProperty = WpfHelper.GetPropertyPath<AssortmentInventoryRuleViewModel>(p => p.RemoveRuleCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the assortment for which the rule will be created
        /// </summary>
        public Assortment CurrentAssortment
        {
            get { return _currentAssortment; }
            set
            {
                _currentAssortment = value;
                OnPropertyChanged(CurrentAssortmentProperty);
                OnCurrentAssortmentChanged(_currentAssortment);
            }
        }

        /// <summary>
        /// Returns the collection product rows
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentInventoryRuleRow> ProductRows
        {
            get
            {
                if (_productRowsRO == null)
                {
                    _productRowsRO = new ReadOnlyBulkObservableCollection<AssortmentInventoryRuleRow>(_productRows);
                }
                return _productRowsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected product rows
        /// </summary>
        public ObservableCollection<AssortmentInventoryRuleRow> SelectedProductRows
        {
            get { return _selectedProductRows; }
        }

        public Single? EditCasePack
        {
            get { return _editCasePack; }
            set
            {
                _editCasePack = value;

                if (value.HasValue)
                {
                    foreach (AssortmentInventoryRuleRow row in this.SelectedProductRows)
                    {
                        row.Rule.CasePack = value.Value;
                    }
                }

                OnPropertyChanged(EditCasePackProperty);
            }
        }

        public Single? EditDaysOfSupply
        {
            get { return _editDaysOfSupply; }
            set
            {
                _editDaysOfSupply = value;

                if (value.HasValue)
                {
                    foreach (AssortmentInventoryRuleRow row in this.SelectedProductRows)
                    {
                        row.Rule.DaysOfSupply = value.Value;
                    }
                }

                OnPropertyChanged(EditDaysOfSupplyProperty);
            }
        }

        public Single? EditShelfLife
        {
            get { return _editShelfLife; }
            set
            {
                _editShelfLife = value;

                if (value.HasValue)
                {
                    foreach (AssortmentInventoryRuleRow row in this.SelectedProductRows)
                    {
                        row.Rule.ShelfLife = value.Value;
                    }
                }

                OnPropertyChanged(EditShelfLifeProperty);
            }
        }

        public Single? EditReplenishmentDays
        {
            get { return _editReplenishmentDays; }
            set
            {
                _editReplenishmentDays = value;

                if (value.HasValue)
                {
                    foreach (AssortmentInventoryRuleRow row in this.SelectedProductRows)
                    {
                        row.Rule.ReplenishmentDays = value.Value;
                    }
                }

                OnPropertyChanged(EditReplenishmentDaysProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected force type
        /// </summary>
        public AssortmentInventoryRuleForceType? EditForceType
        {
            get { return _editForceType; }
            set
            {
                _editForceType = value;

                OnPropertyChanged(EditForceTypeProperty);

                if (value.HasValue)
                {
                    OnEditForceTypeChanged(value);
                }
            }
        }

        /// <summary>
        /// Helper property, 
        /// Gets/Sets the force value based on 
        /// the selected force type
        /// </summary>
        public Int32? EditForceValue
        {
            get { return _editForceValue; }
            set
            {
                _editForceValue = value;

                if (value.HasValue)
                {
                    foreach (AssortmentInventoryRuleRow row in this.SelectedProductRows)
                    {
                        if (this.EditForceType == AssortmentInventoryRuleForceType.Facings)
                        {
                            row.Rule.MinFacings = value.Value;
                        }
                        else
                        {
                            row.Rule.MinUnits = value.Value;
                        }
                    }
                }

                OnPropertyChanged(EditForceValueProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the rule waste hurdle value 
        /// based on the selected waste hurdle type
        /// </summary>
        public Single? EditWasteHurdleValue
        {
            get
            {
                return _editWasteHurdleValue;
            }
            set
            {
                _editWasteHurdleValue = value;

                if (value.HasValue)
                {
                    foreach (AssortmentInventoryRuleRow row in this.SelectedProductRows)
                    {
                        if (this.EditWasteHurdleType == AssortmentInventoryRuleWasteHurdleType.CasePacks)
                        {
                            row.Rule.WasteHurdleCasePack = value.Value;
                        }
                        else
                        {
                            row.Rule.WasteHurdleUnits = value.Value;
                        }
                    }
                }

                OnPropertyChanged(EditWasteHurdleValueProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected waste hurdle type.
        /// </summary>
        public AssortmentInventoryRuleWasteHurdleType? EditWasteHurdleType
        {
            get { return _editWasteHurdleType; }
            set
            {
                _editWasteHurdleType = value;
                OnPropertyChanged(EditWasteHurdleTypeProperty);

                if (value.HasValue)
                {
                    OnEditWasteHurdleTypeChanged(value);
                }
            }
        }

        #endregion

        #region Constructor
        
        /// <summary>
        /// Testing Constructor
        /// </summary>
        public AssortmentInventoryRuleViewModel(Assortment currentAssortment)
        {
            _productRows.BulkCollectionChanged += ProductRows_BulkCollectionChanged;
            _selectedProductRows.CollectionChanged += SelectedProductRows_CollectionChanged;

            //Fetch master products once for display purposes
            _fullProductLookup = ProductList.FetchByProductIds(currentAssortment.Products.Select(p => p.ProductId)).ToDictionary(p => p.Id);

            //Loading method handles object load
            this.CurrentAssortment = currentAssortment;
        }

        #endregion

        #region Commands

        #region Apply

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies the changes to the current item
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        private Boolean Apply_CanExecute()
        {
            //must have a range selected
            if (this.CurrentAssortment == null)
            {
                return false;
            }

            //must be dirty
            if (!this.ProductRows.Any(p => p.IsDirty))
            {
                return false;
            }

            //must all be valid
            if (!this.ProductRows.All(p => p.Rule.IsValid))
            {
                return false;
            }


            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        /// <summary>
        /// Applies changes made to the current item.
        /// </summary>
        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //cycle through all product rows applying changes made
            foreach (AssortmentInventoryRuleRow row in this.ProductRows)
            {
                row.CommitChanges();
            }

            //take a snap of the currently selected ids
            List<Int32> selectedProductIds = this.SelectedProductRows.Select(r => r.AssortmentProduct.Id).ToList();

            //recreate all product rows
            ReloadProductRows();


            //reselect
            foreach (AssortmentInventoryRuleRow row in this.ProductRows)
            {
                if (selectedProductIds.Contains(row.AssortmentProduct.Id))
                {
                    this.SelectedProductRows.Add(row);
                }
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyAndClose

        private RelayCommand _applyAndCloseCommand;

        /// <summary>
        /// Applies changes to the current item and closes the window
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndClose_Executed(),
                        p => ApplyAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        private Boolean ApplyAndClose_CanExecute()
        {
            //must have a range selected
            if (this.CurrentAssortment == null)
            {
                return false;
            }

            //must all be valid
            if (!this.ProductRows.All(p => p.Rule.IsValid))
            {
                return false;
            }

            return true;
        }

        private void ApplyAndClose_Executed()
        {
            ApplyCurrentItem();

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region RemoveRule

        private RelayCommand _removeRuleCommand;

        /// <summary>
        /// Nulls off the current product rule
        /// </summary>
        public RelayCommand RemoveRuleCommand
        {
            get
            {
                if (_removeRuleCommand == null)
                {
                    _removeRuleCommand = new RelayCommand(
                        p => RemoveRule_Executed(p),
                        p => RemoveRule_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeRuleCommand);

                }
                return _removeRuleCommand;
            }
        }

        private Boolean RemoveRule_CanExecute(Object arg)
        {
            AssortmentInventoryRuleRow row = arg as AssortmentInventoryRuleRow;

            //must have a valid row
            if (row == null)
            {
                return false;
            }

            //must not be a null row
            if (row.IsNullRule)
            {
                return false;
            }

            return true;
        }

        private void RemoveRule_Executed(Object arg)
        {
            AssortmentInventoryRuleRow row = arg as AssortmentInventoryRuleRow;
            if (row != null)
            {
                row.ClearRule();

                //if the row is selected then refresh
                if (this.SelectedProductRows.Contains(row))
                {
                    List<AssortmentInventoryRuleRow> selectedRows = this.SelectedProductRows.ToList();
                    this.SelectedProductRows.Clear();
                    selectedRows.ForEach(r => this.SelectedProductRows.Add(r));
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected range
        /// </summary>
        /// <param name="newValue"></param>
        private void OnCurrentAssortmentChanged(Assortment newValue)
        {
            this.SelectedProductRows.Clear();

            ReloadProductRows();

            //select the first available row
            if (this.ProductRows.Count > 0)
            {
                if (this.SelectedProductRows.Count > 0)
                {
                    this.SelectedProductRows.Clear();
                }
                this.SelectedProductRows.Add(this.ProductRows.OrderBy(p => p.AssortmentProduct.Gtin).First());
            }
        }

        /// <summary>
        /// Responds to changes to the product rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductRows_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove ||
                e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Reset)
            {
                foreach (AssortmentInventoryRuleRow row in e.ChangedItems)
                {
                    row.Dispose();
                }
            }
        }

        /// <summary>
        /// Responds to changes to the selected product rows collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedProductRows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //reset all vals to null
            _editCasePack = null;
            _editDaysOfSupply = null;
            _editShelfLife = null;
            _editReplenishmentDays = null;
            _editForceType = null;
            _editForceValue = null;
            _editWasteHurdleValue = null;

            //reset the edit values
            if (this.SelectedProductRows.Count > 0)
            {
                AssortmentInventoryRule firstRule = this.SelectedProductRows.First().Rule;

                //edit case pack
                Single firstCasePackValue = firstRule.CasePack;
                if (this.SelectedProductRows.All(r => r.Rule.CasePack == firstCasePackValue))
                {
                    _editCasePack = firstCasePackValue;
                }

                //days of supply
                Single firstdosValue = firstRule.DaysOfSupply;
                if (this.SelectedProductRows.All(r => r.Rule.DaysOfSupply == firstdosValue))
                {
                    _editDaysOfSupply = firstdosValue;
                }

                //shelf life
                Single firstShelfLife = firstRule.ShelfLife;
                if (this.SelectedProductRows.All(r => r.Rule.ShelfLife == firstShelfLife))
                {
                    _editShelfLife = firstShelfLife;
                }

                //replen days
                Single firstReplenDays = firstRule.ReplenishmentDays;
                if (this.SelectedProductRows.All(r => r.Rule.ReplenishmentDays == firstReplenDays))
                {
                    _editReplenishmentDays = firstReplenDays;
                }

                //edit force type
                AssortmentInventoryRuleForceType firstForceType = (firstRule.MinFacings != 0) ? AssortmentInventoryRuleForceType.Facings : AssortmentInventoryRuleForceType.Units;
                if (this.SelectedProductRows.All(
                    r => ((r.Rule.MinFacings != 0) ? AssortmentInventoryRuleForceType.Facings : AssortmentInventoryRuleForceType.Units) == firstForceType))
                {
                    _editForceType = firstForceType;
                }


                //force value ?
                Int32 firstForceValue = (firstRule.MinFacings != 0) ? firstRule.MinFacings : firstRule.MinUnits;
                if (this.SelectedProductRows.All(
                    r => ((r.Rule.MinFacings != 0) ? r.Rule.MinFacings : r.Rule.MinUnits) == firstForceValue))
                {
                    _editForceValue = firstForceValue;
                }


                //edit force type
                AssortmentInventoryRuleWasteHurdleType firstWasteHurdleType = (firstRule.WasteHurdleCasePack != 0) ? AssortmentInventoryRuleWasteHurdleType.CasePacks : AssortmentInventoryRuleWasteHurdleType.Units;
                if (this.SelectedProductRows.All(
                    r => ((r.Rule.WasteHurdleCasePack != 0) ? AssortmentInventoryRuleWasteHurdleType.CasePacks : AssortmentInventoryRuleWasteHurdleType.Units) == firstWasteHurdleType))
                {
                    _editWasteHurdleType = firstWasteHurdleType;
                }

                //waste hurdle value
                Single firstWasteHurdleValue = (firstRule.WasteHurdleCasePack != 0) ? firstRule.WasteHurdleCasePack : firstRule.WasteHurdleUnits;
                if (this.SelectedProductRows.All(
                    r => ((r.Rule.WasteHurdleCasePack != 0) ? r.Rule.WasteHurdleCasePack : r.Rule.WasteHurdleUnits) == firstWasteHurdleValue))
                {
                    _editWasteHurdleValue = firstWasteHurdleValue;
                }


            }


            //fire off all property changed events
            OnPropertyChanged(EditCasePackProperty);
            OnPropertyChanged(EditDaysOfSupplyProperty);
            OnPropertyChanged(EditShelfLifeProperty);
            OnPropertyChanged(EditReplenishmentDaysProperty);
            OnPropertyChanged(EditForceTypeProperty);
            OnPropertyChanged(EditForceValueProperty);
            OnPropertyChanged(EditWasteHurdleValueProperty);

        }

        /// <summary>
        /// Responds to a change of selected force type.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnEditForceTypeChanged(AssortmentInventoryRuleForceType? newValue)
        {
            foreach (AssortmentInventoryRuleRow row in this.SelectedProductRows)
            {

                //swap the values if we need to
                if (newValue == AssortmentInventoryRuleForceType.Facings)
                {
                    if (row.Rule.MinFacings == 0 && row.Rule.MinUnits != 0)
                    {
                        row.Rule.MinFacings = row.Rule.MinUnits;
                        row.Rule.MinUnits = 0;
                    }
                }
                else
                {
                    if (row.Rule.MinUnits == 0 && row.Rule.MinFacings != 0)
                    {
                        row.Rule.MinUnits = row.Rule.MinFacings;
                        row.Rule.MinFacings = 0;
                    }
                }


            }
        }

        /// <summary>
        /// Responds to a change of selected waste hurdle type
        /// </summary>
        /// <param name="newValue"></param>
        private void OnEditWasteHurdleTypeChanged(AssortmentInventoryRuleWasteHurdleType? newValue)
        {
            foreach (AssortmentInventoryRuleRow row in this.SelectedProductRows)
            {
                //swap the values if we need to
                if (newValue == AssortmentInventoryRuleWasteHurdleType.CasePacks)
                {
                    if (row.Rule.WasteHurdleCasePack == 0 && row.Rule.WasteHurdleUnits != 0)
                    {
                        row.Rule.WasteHurdleCasePack = row.Rule.WasteHurdleUnits;
                        row.Rule.WasteHurdleUnits = 0;
                    }
                }
                else
                {
                    if (row.Rule.WasteHurdleUnits == 0 && row.Rule.WasteHurdleCasePack != 0)
                    {
                        row.Rule.WasteHurdleUnits = row.Rule.WasteHurdleCasePack;
                        row.Rule.WasteHurdleCasePack = 0;
                    }
                }
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.ProductRows.Any(p => p.IsDirty))
                {
                    ChildItemChangeResult result = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        this.ApplyCommand.Execute();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }

                }
            }
            return continueExecute;
        }

        /// <summary>
        /// Recreates the product rows collection.
        /// </summary>
        private void ReloadProductRows()
        {
            //clear all existing product rows
            if (_productRows.Count > 0)
            {
                _productRows.Clear();
            }
            if (this.SelectedProductRows.Count > 0)
            {
                this.SelectedProductRows.Clear();
            }

            //reload for the selected assortment
            List<AssortmentInventoryRuleRow> rows = new List<AssortmentInventoryRuleRow>();
            foreach (AssortmentProduct asstProduct in _currentAssortment.Products)
            {
                Product sourceProduct = null;
                _fullProductLookup.TryGetValue(asstProduct.ProductId, out sourceProduct);

                rows.Add(new AssortmentInventoryRuleRow(_currentAssortment, asstProduct, sourceProduct));
            }
            _productRows.AddRange(rows);
        }

        #endregion

        #region IDisposable members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _productRows.Clear();
                    _productRows.BulkCollectionChanged -= ProductRows_BulkCollectionChanged;
                    _fullProductLookup.Clear();
                    _fullProductLookup = null;
                    _selectedProductRows.CollectionChanged -= SelectedProductRows_CollectionChanged;
                    _selectedProductRows.Clear();
                    _selectedProductRows = null;

                }

                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                if (_editShelfLife > 1 || _editShelfLife < 0)
                {
                    return Message.AssortmentInventoryRules_ShelfLifeError;
                }
                //no errors in check columns
                return String.Empty;
            }
        }

        #endregion
    }

}

