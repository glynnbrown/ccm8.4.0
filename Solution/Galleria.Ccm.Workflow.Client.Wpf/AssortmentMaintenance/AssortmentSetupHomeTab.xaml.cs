﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#endregion
using System.Linq;
using System.Windows;
using Fluent;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupHomeTab.xaml
    /// </summary>
    public sealed partial class AssortmentSetupHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupViewModel), typeof(AssortmentSetupHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentSetupViewModel ViewModel
        {
            get { return (AssortmentSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentSetupHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}