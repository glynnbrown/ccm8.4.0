﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
// V8-27259 : A.Kuszyk
//  Amended command binding of remove button.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupFamilyRulesWindow.xaml
    /// </summary>
    public partial class AssortmentSetupFamilyRulesWindow : ExtendedRibbonWindow
    {
        public static readonly String RemoveCommandKey = "FamilyRulesRemoveRuleCommand";

        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel",
            typeof(AssortmentSetupFamilyRulesViewModel),
            typeof(AssortmentSetupFamilyRulesWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public AssortmentSetupFamilyRulesViewModel ViewModel
        {
            get { return (AssortmentSetupFamilyRulesViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupFamilyRulesWindow senderControl = (AssortmentSetupFamilyRulesWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupFamilyRulesViewModel oldModel = (AssortmentSetupFamilyRulesViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(RemoveCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentSetupFamilyRulesViewModel newModel = (AssortmentSetupFamilyRulesViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(RemoveCommandKey, newModel.RemoveCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentSetupFamilyRulesWindow(AssortmentSetupFamilyRulesViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(AssortmentSetupFamilyRules_Loaded);
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentSetupFamilyRules_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(AssortmentSetupFamilyRules_Loaded);

            var factory =
            new PlanogramAssortmentProductColumnLayoutFactory(typeof(AssortmentProductView),
                    null,
                    new List<Framework.Model.IModelPropertyInfo>() 
                    { 
                        Product.GtinProperty,
                        Product.NameProperty
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentProducts, /*PathMask*/"Product.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(xUnassignedGrid);
            _columnLayoutManager.AttachDataGrid(xAssignedGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event handlers
        
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentProductColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }
        }
        
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
        }

        private void AssignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.xUnassignedGrid)
            {
                if (this.ViewModel != null && this.ViewModel.AddSelectedProductsCommand.CanExecute())
                {
                    this.ViewModel.AddSelectedProductsCommand.Execute();
                }
            }
        }

        private void UnassignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.xAssignedGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveSelectedProductsCommand.Execute();
                }
            }
        }

        private void UnassignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.AddSelectedProductsCommand.Execute();
            }
        }

        private void AssignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.RemoveSelectedProductsCommand.Execute();
            }
        }

        /// <summary>
        /// moves focus to Family Rule grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XFamilyDataGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ExtendedDataGrid;
                FocusNavigationDirection direction = FocusNavigationDirection.Down;
                TraversalRequest tRequest = new TraversalRequest(direction);
                senderControl?.MoveFocus(tRequest);
                e.Handled = true;
            }
        }

        private void ProductGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAssigned = senderControl?.Name == xAssignedGrid.Name;

            if (e.Key == Key.Return)
            {
                if (isAssigned) this.ViewModel?.RemoveSelectedProductsCommand.Execute();
                else this.ViewModel?.AddSelectedProductsCommand.Execute();
                Keyboard.Focus(isAssigned ? xAssignedGrid : xUnassignedGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAssigned ? xUnassignedGrid : xAssignedGrid);
                e.Handled = true;
            }
        }
        #endregion

        #region Methods

        private ObservableCollection<DataGridColumn> BuildAssortmentProductColumnList()
        {
            //load the columnset
            ObservableCollection<DataGridColumn> columnList = new ObservableCollection<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    AssortmentProduct.GtinProperty.FriendlyName,
                    AssortmentProductView.ProductGtinProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
                    col0.MinWidth = 50;
                    col0.Width = 80;
                    columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    AssortmentProduct.NameProperty.FriendlyName,
                    AssortmentProductView.ProductNameProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            return columnList;
        }

        #endregion

        #region OnClose

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (_columnLayoutManager != null)
                {
                    _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                    _columnLayoutManager.Dispose();
                }

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
     
    }
}