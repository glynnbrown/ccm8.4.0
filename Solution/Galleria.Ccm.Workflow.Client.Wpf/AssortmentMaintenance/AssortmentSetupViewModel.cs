﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25454 : J.Pickup
//      Created (Print Preview switched off for now).
// CCM-26303 : J.Pickup
//      fixed bug with save as. Should now save with db constraints applied.
// CCM-26286 : J.Pickup 
//      Added additional save & new, save and close ribbon buttons
// CCM-26140 : I.George
//     Added FetchByEntityIncludingDeleted to the save Command
// V8-26705 : A.Kuszyk
//      Changed ProductSelectionWindow to Common.Wpf version.
// V8-26765 : A.Kuszyk
//      Changed the use of AssortmentProduct enums to their Planogram equivalents.
// V8-27065 : L.Luong
//     Modified NewFromConsumerTree to display the products on the gird
// V8-27064 : L.Luong
//     Modified FromProductUniverse to display the products on the gird
// V8-27150 : L.Ineson
//  Swapped add products command to use new selector.
// V8-24264 : A.Probyn
//  Added missing command properties for location related commands.
#endregion
#region Version History: (CCM 8.0.3)
// CCM-29134 : D.Pleasance
//  Amended reference to FetchByEntityId from FetchByEntityIdIncludingDeleted since we now deleted outright
#endregion
#region Version History: (CCM 8.1.0)
// V8-30252 : N.Haywood
//  Removed limit on backstage open load
#endregion
#region Version History: (CCM 8.1.1)
// V8-30325 : I.George
//  Added friendly description to RemoveSelectedProductCommand and added shortcut key to saveCommand
// V8-30270 : I.George
//  Removed IsDirty check in save can execute command
#endregion
#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Added Buddy related commands
// V8-31551 : A.Probyn
//  Added inventory rules related commands
// V8-31854 : A.Probyn 
//  Added extra code to help with disable reason bindings
// V8-32086 : N.Haywood
//  Added category code auto selection
// CCM-18489 : M.Pettit
//  Add any selected assortment products to AdvancedAdd window grid
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using System.ComponentModel;
using System.Collections.Specialized;
using Galleria.Framework.Model;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    public sealed class AssortmentSetupViewModel : ViewModelAttachedControlObject<AssortmentSetupOrganiser>
    {
        #region Constants

        private const String _gibraltarCategory = "Assortment";
        private const Double _lastLocationsGridNormalViewDefaultWidth = 215;
        private const Int32 MaxRetryCount = 3;
        private const Int32 MaxBackstageItemsFetch = 1001;

        #endregion

        #region Fields

        private Assortment _selectedAssortment; // The current selected assortment

        private readonly AssortmentInfoListViewModel _masterAssortmentInfoListView = new AssortmentInfoListViewModel(); // the master list of current assortments
        private BulkObservableCollection<AssortmentInfo> _availableAssortments = new BulkObservableCollection<AssortmentInfo>(); // the list of available assortments
        private ReadOnlyBulkObservableCollection<AssortmentInfo> _availableAssortmentsRO;

        private readonly BulkObservableCollection<AssortmentProductRow> _assortmentProductRows = new BulkObservableCollection<AssortmentProductRow>();
        private readonly ReadOnlyBulkObservableCollection<AssortmentProductRow> _assortmentProductRowsRO;
        private ObservableCollection<AssortmentProductRow> _selectedProducts = new ObservableCollection<AssortmentProductRow>();


        private readonly LocationInfoListViewModel _masterLocationInfoListView = new LocationInfoListViewModel();
        private LocationList _masterLocationList; //used by the manual location select window.

        private LocationInfoList _allLocationsInfoListIncludingDeleted;

        private ObservableCollection<LocationInfo> _currentLocations = new ObservableCollection<LocationInfo>();
        private ObservableCollection<LocationInfo> _selectedCurrentLocations = new ObservableCollection<LocationInfo>();

        private LocationBarState _locationPanelState = LocationBarState.Off;
        private Double _lastLocationsGridNormalViewWidth = _lastLocationsGridNormalViewDefaultWidth;
        private ProductGroupInfo _selectedMerchandisingGroup;
        private ConsumerDecisionTreeInfo _selectedConsumerDecisionTree;
        private String _filterItemsKey;
        private String _filterItemsCountMessage;

        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath SelectedAssortmentProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.SelectedAssortment);
        public static readonly PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.SelectedProducts);
        public static readonly PropertyPath AvailableAssortmentsProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.AvailableAssortmentInfos);
        public static readonly PropertyPath SelectedMerchandisingGroupProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.SelectedMerchandisingGroup);
        public static readonly PropertyPath SelectedConsumerDecisonTreeProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.SelectedConsumerDecisonTree);
        public static readonly PropertyPath IsReadOnlyProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.IsReadOnly);
        public static readonly PropertyPath LocationBarStateProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.LocationBarState);

        public static readonly PropertyPath CurrentLocationsProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(c => c.CurrentLocations);
        public static readonly PropertyPath SelectedCurrentLocationsProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(c => c.SelectedCurrentLocations);
        public static readonly PropertyPath FilterItemsKeyProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.FilterItemsKey);
        public static readonly PropertyPath FilterItemsCountMessageProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.FilterItemsCountMessage);

        //commands
        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath NewFromProductUniverseCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.NewFromProductUniverseCommand);
        public static readonly PropertyPath NewFromConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.NewFromConsumerDecisionTreeCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(P => P.DeleteCommand);
        public static readonly PropertyPath PrintPreviewCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.PrintPreviewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath AddLocationsManuallyCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.AddLocationsManuallyCommand);
        public static readonly PropertyPath RemoveLocationsCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.RemoveLocationsCommand);
        public static readonly PropertyPath LocationBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.LocationBuddyCommand);
        public static readonly PropertyPath LocationBuddyReviewCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.LocationBuddyReviewCommand);
        public static readonly PropertyPath LocationBuddyAddNewCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.LocationBuddyAddNewCommand);
        public static readonly PropertyPath ProductBuddyShowCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.ProductBuddyShowCommand);
        public static readonly PropertyPath ProductBuddyCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.ProductBuddyCommand);
        public static readonly PropertyPath ProductBuddyAddNewWizardCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.ProductBuddyAddNewWizardCommand);
        public static readonly PropertyPath ProductBuddyAddNewAdvancedCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupViewModel>(p => p.ProductBuddyAddNewAdvancedCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current Selected Assortment
        /// </summary>
        public Assortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            private set
            {
                Assortment oldValue = _selectedAssortment;

                _selectedAssortment = value;
                UpdateLocationsGrid();
                OnPropertyChanged(SelectedAssortmentProperty);
                OnPropertyChanged(IsReadOnlyProperty);

                OnSelectedAssortmentChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the row views for the current assortment products
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentProductRow> AssortmentProductRows
        {
            get { return _assortmentProductRowsRO; }
        }

        /// <summary>
        /// Returns the collection of selected products
        /// </summary>
        public ObservableCollection<AssortmentProductRow> SelectedProducts
        {
            get { return _selectedProducts; }
        }

        /// <summary>
        /// Returns the collection of available assortments
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentInfo> AvailableAssortmentInfos
        {
            get
            {
                if (_availableAssortmentsRO == null)
                {
                    _availableAssortmentsRO =
                        new ReadOnlyBulkObservableCollection<AssortmentInfo>(_availableAssortments);

                }
                return _availableAssortmentsRO;
            }
        }

        /// <summary>
        /// Returns the merchandising group to which this assortment is assigned
        /// </summary>
        public ProductGroupInfo SelectedMerchandisingGroup
        {
            get { return _selectedMerchandisingGroup; }
            set
            {
                _selectedMerchandisingGroup = value;

                Int32? newId = (value != null) ? (Int32?)value.Id : null;

                if (this.SelectedAssortment != null
                    && this.SelectedAssortment.ProductGroupId != newId
                    && newId.HasValue)
                {
                    this.SelectedAssortment.ProductGroupId = newId.Value;
                }

                OnPropertyChanged(SelectedMerchandisingGroupProperty);
            }
        }

        /// <summary>
        /// Returns the consumer decision tree to which this assortment is assigned
        /// </summary>
        public ConsumerDecisionTreeInfo SelectedConsumerDecisonTree
        {
            get { return _selectedConsumerDecisionTree; }
            set
            {
                _selectedConsumerDecisionTree = value;

                Int32? newId = (value != null) ? (Int32?)value.Id : null;

                if (this.SelectedAssortment != null
                    && this.SelectedAssortment.ConsumerDecisionTreeId != newId)
                {
                    this.SelectedAssortment.ConsumerDecisionTreeId = newId;
                }
                OnSelectedConsumerDecsionTreeChanged();

                OnPropertyChanged(SelectedConsumerDecisonTreeProperty);
            }
        }

        /// <summary>
        /// Determines Read Only criteria based upon state of model
        /// </summary>
        public Boolean IsReadOnly
        {
            get
            {
                if (this.SelectedAssortment == null || this.SelectedAssortment.IsNew)
                {
                    return !_userHasAssortmentCreatePerm;
                }
                else
                {
                    return !_userHasAssortmentEditPerm;
                }
            }
        }

        /// <summary>
        /// Gets / Sets the location bar viewable state
        /// </summary>
        public LocationBarState LocationBarState
        {
            get { return _locationPanelState; }
            set
            {
                //Save current normal view width if in Normal view
                if (_locationPanelState == AssortmentSetup.LocationBarState.Normal)
                {
                    if (this.AttachedControl != null)
                    {
                        GridLength gl = this.AttachedControl.xLocationsColumn.Width;
                        //Only save value if user hasn't made it too small to be useful
                        if (gl.Value > 50)
                        {
                            _lastLocationsGridNormalViewWidth = gl.Value;
                        }
                    }
                }

                _locationPanelState = value;

                if (this.AttachedControl != null)
                {
                    switch (_locationPanelState)
                    {
                        case AssortmentSetup.LocationBarState.Normal:
                            this.AttachedControl.xLocationsColumn.MinWidth = 40;
                            this.AttachedControl.xLocationsColumn.Width = new GridLength(_lastLocationsGridNormalViewWidth);
                            break;

                        case AssortmentSetup.LocationBarState.Minimised:
                            this.AttachedControl.xLocationsColumn.MinWidth = 0;
                            this.AttachedControl.xLocationsColumn.Width = new GridLength(26);
                            break;

                        default:
                            this.AttachedControl.xLocationsColumn.MinWidth = 0;
                            this.AttachedControl.xLocationsColumn.Width = new GridLength(0);
                            break;
                    }
                }
                OnPropertyChanged(LocationBarStateProperty);
            }
        }

        /// <summary>
        /// The list of current locations that matches the filter/group criteria
        /// </summary>
        public ObservableCollection<LocationInfo> CurrentLocations
        {
            get { return _currentLocations; }
            set
            {
                _currentLocations = value;
                OnPropertyChanged(CurrentLocationsProperty);
            }
        }

        /// <summary>
        /// The list of selected items in the CurrentLocations grid
        /// </summary>
        public ObservableCollection<LocationInfo> SelectedCurrentLocations
        {
            get { return _selectedCurrentLocations; }
        }

        /// <summary>
        /// Gets/Sets the fetch criteria for backstage item fetch
        /// </summary>
        public String FilterItemsKey
        {
            get { return _filterItemsKey; }
            set
            {
                _filterItemsKey = value;
                OnPropertyChanged(FilterItemsKeyProperty);

                UpdateAvailableAssortmentItems();
            }
        }

        /// <summary>
        /// Gets/Sets the filtered grid items count message
        /// </summary>
        public String FilterItemsCountMessage
        {
            get { return _filterItemsCountMessage; }
            private set
            {
                _filterItemsCountMessage = value;
                OnPropertyChanged(FilterItemsCountMessageProperty);
            }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentSetupViewModel()
        {
            //intialize collections
            _assortmentProductRowsRO = new ReadOnlyBulkObservableCollection<AssortmentProductRow>(_assortmentProductRows);

            //update permissions
            UpdatePermissionFlags();

            //assortment info list view
            _masterAssortmentInfoListView.ModelChanged += MasterAssortmentInfoListView_ModelChanged;
            _masterAssortmentInfoListView.FetchForCurrentEntity();

            //loc info list
            _masterLocationInfoListView.ModelChanged += LocationInfoListView_ModelChanged;
            _masterLocationInfoListView.FetchAllForEntity();

            //loc info List including Deleted items
            _allLocationsInfoListIncludingDeleted = LocationInfoList.FetchByEntityIdIncludingDeleted(App.ViewState.EntityId);

            //create a new Assortment
            this.NewCommand.Execute();

        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new item
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.AssortmentSetup_NewCommand_Tooltip,
                        InputGestureKey = Key.N,
                        InputGestureModifiers = ModifierKeys.Control,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userHasAssortmentCreatePerm)
            {
                this.NewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                this.SelectedAssortment = Assortment.NewAssortment(App.ViewState.EntityId);
                this.SelectedAssortment.MarkGraphAsInitialized();

                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }
        }

        #endregion

        #region NewFromProductUniverseCommand

        private RelayCommand _newFromProductUniverseCommand;

        /// <summary>
        /// import assortment products command (from Universe)
        /// </summary>
        public RelayCommand NewFromProductUniverseCommand
        {
            get
            {
                if (_newFromProductUniverseCommand == null)
                {
                    _newFromProductUniverseCommand = new RelayCommand(
                        p => NewFromProductUniverse_Executed(),
                        p => NewFromProductUniverse_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_ImportFromUniverse,
                        Icon = ImageResources.AssortmentSetup_AddFromProductUniverse
                    };
                    base.ViewModelCommands.Add(_newFromProductUniverseCommand);
                }
                return _newFromProductUniverseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean NewFromProductUniverse_CanExecute()
        {
            //user must have create permission
            if (!_userHasAssortmentCreatePerm)
            {
                this.NewFromProductUniverseCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void NewFromProductUniverse_Executed()
        {
            if (ContinueWithItemChange())
            {
                if (this.AttachedControl != null)
                {
                    ProductUniverseSelectionWindow productUniverseWindow = new ProductUniverseSelectionWindow();
                    App.ShowWindow(productUniverseWindow, this.AttachedControl, true);

                    if (productUniverseWindow.SelectedProductUniverse != null)
                    {
                        Assortment newAssortment = Assortment.NewAssortment(App.ViewState.EntityId);

                        ProductUniverse productUniverse = ProductUniverse.FetchById(productUniverseWindow.SelectedProductUniverse.Id);
                        foreach (ProductUniverseProduct productUniverseProduct in productUniverse.Products)
                        {
                            try
                            {
                                Product product = Product.FetchById(productUniverseProduct.ProductId);
                                AssortmentProduct assortmentProduct = AssortmentProduct.NewAssortmentProduct(product);
                                newAssortment.Products.Add(assortmentProduct);
                            }
                            catch(Exception ex)
                            {
                                //just log and carry on
                                CommonHelper.RecordException(ex);
                            }
                        }

                        newAssortment.ProductGroupId = productUniverse.ProductGroupId ?? default(Int32);
                        this.SelectedAssortment = newAssortment;
                    }
                }

                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }

        }

        #endregion

        #region NewFromConsumerDecisionTreeCommand

        private RelayCommand _newFromConsumerDecisionTreeCommand;

        /// <summary>
        /// import assortment products command (from Consumer Decision Tree)
        /// </summary>
        public RelayCommand NewFromConsumerDecisionTreeCommand
        {
            get
            {
                if (_newFromConsumerDecisionTreeCommand == null)
                {
                    _newFromConsumerDecisionTreeCommand = new RelayCommand(
                        p => NewFromConsumerDecisionTree_Executed(),
                        p => NewFromConsumerDecisionTree_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_ImportFromConsumerDecisionTree,
                        Icon = ImageResources.AssortmentSetup_AddFromCDT
                    };
                    base.ViewModelCommands.Add(_newFromConsumerDecisionTreeCommand);
                }
                return _newFromConsumerDecisionTreeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean NewFromConsumerDecisionTree_CanExecute()
        {
            //user must have create permission
            if (!_userHasAssortmentCreatePerm)
            {
                this.NewFromConsumerDecisionTreeCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void NewFromConsumerDecisionTree_Executed()
        {
            if (ContinueWithItemChange())
            {
                if (this.AttachedControl != null)
                {
                    ConsumerDecisionTreeWindow consumerDecisionTreeWindow = new ConsumerDecisionTreeWindow();
                    App.ShowWindow(consumerDecisionTreeWindow, this.AttachedControl, true);

                    if (consumerDecisionTreeWindow.SelectionResult != null)
                    {
                        Assortment newAssortment = Assortment.NewAssortment(App.ViewState.EntityId);

                        ConsumerDecisionTree consumerDecisionTree = ConsumerDecisionTree.FetchById(consumerDecisionTreeWindow.SelectionResult.Id);
                        foreach (ConsumerDecisionTreeNode consumerDecisionTreeNode in consumerDecisionTree.GetAllNodes())
                        {
                            foreach (ConsumerDecisionTreeNodeProduct consumerDecisionTreeNodeProduct in consumerDecisionTreeNode.Products)
                            {
                                Product product = Product.FetchById(consumerDecisionTreeNodeProduct.ProductId);
                                AssortmentProduct assortmentProduct = AssortmentProduct.NewAssortmentProduct(product);
                                newAssortment.Products.Add(assortmentProduct);
                            }
                        }
                        newAssortment.ProductGroupId = consumerDecisionTree.ProductGroupId ?? default(Int32);
                        newAssortment.ConsumerDecisionTreeId = consumerDecisionTree.Id;
                        this.SelectedAssortment = newAssortment;
                    }
                }

                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Opens assortment for the given id
        /// param: Int32 the content id to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        DisabledReason = Message.Assortment_OpenCommand_DisabledReason
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? contentId)
        {
            //id must not be null
            if (!contentId.HasValue)
            {
                return false;
            }

            //user must have fetch permission
            if (!_userHasAssortmentFetchPerm)
            {
                this.OpenCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? assortmentId)
        {
            if (this.ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //fetch the item
                    this.SelectedAssortment = Assortment.GetById(assortmentId.Value);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _gibraltarCategory);
                    if (this.AttachedControl != null)
                    {
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            String.Empty, OperationType.Open,
                            this.AttachedControl);
                    }

                    base.ShowWaitCursor(true);
                }

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region Save

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.SelectedAssortment == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment.ProductGroupId == null)
            {
                this.SaveCommand.DisabledReason = Message.Assortment_SaveDisabled_NoCategory;
                return false;
            }

            //must be valid
            if (!this.SelectedAssortment.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            if (SaveCurrentItem())
            {
                attemptSave();
            }
        }

        /// <summary>
        /// Calls a save on the current item
        /// </summary>
        /// <returns>true of successful, false if the save was cancelled</returns>
        private Boolean SaveCurrentItem(Boolean markAsNewVersion = false)
        {
            String newName;
            Boolean nameAccepted = true;
            Int32 id = this.SelectedAssortment.Id;

            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck = (s) =>
                    {
                        Boolean returnValue = true;
                        base.ShowWaitCursor(true);
                        foreach (AssortmentInfo AssoInfo in AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId))
                        {
                            if (AssoInfo.Id != id && AssoInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                            {
                                returnValue = false;
                                break;
                            }
                        }
                        base.ShowWaitCursor(false);
                        return returnValue;
                    };
                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(String.Empty, String.Empty,
                    Message.Assortment_CodeAlreadyInUse,
                    Assortment.NameProperty.FriendlyName, 50, false, isUniqueCheck, this.SelectedAssortment.Name, out newName);
            }

            else
            {
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedAssortment.Name,
                   this.AvailableAssortmentInfos.Where(p => p.Id != id).Select(p => p.Name));
            }

            if (nameAccepted)
            {
                if (this.SelectedAssortment.Name != newName)
                {
                    SelectedAssortment.Name = newName;
                }
            }

            return nameAccepted;
        }

        private void attemptSave()
        {
            try
            {
                //Save_CanExecute Current Item
                this.SelectedAssortment = this.SelectedAssortment.Save();

            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex, _gibraltarCategory);
                Exception rootException = ex.GetBaseException();

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedAssortment.Name);
                    if (itemReloadRequired)
                    {
                        this.SelectedAssortment = Assortment.GetById(this.SelectedAssortment.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.

                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                    this.SelectedAssortment.Name, OperationType.Save);
                }

            }
            //Update backstage list
            _masterAssortmentInfoListView.FetchForCurrentEntity();

        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a completely new one
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureKey = Key.F12,
                        InputGestureModifiers = ModifierKeys.None
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //may not be null
            if (this.SelectedAssortment == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have create permission
            if (!_userHasAssortmentCreatePerm)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            if (this.SelectedAssortment.ProductGroupId == null)
            {
                this.SaveAsCommand.DisabledReason = Message.Assortment_SaveDisabled_NoCategory;
                return false;
            }

            //must be valid
            if (!this.SelectedAssortment.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //save as should only be used for previously saved items
            if (this.SelectedAssortment.IsNew)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_SaveAs_DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the name to save as
            String copyName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                (s) =>
                {
                    return !this.AvailableAssortmentInfos.Select(p => p.Name).Contains(s);
                };

                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            }
            else
            {
                //force a unique value for unit testing
                copyName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedAssortment.Name, this.AvailableAssortmentInfos.Select(p => p.Name));
            }

            if (nameAccepted)
            {
                base.ShowWaitCursor(true);

                //copy the item and save
                Assortment itemCopy = this.SelectedAssortment.Copy();
                itemCopy.Name = copyName;
                itemCopy.UniqueContentReference = Guid.NewGuid();

                itemCopy = itemCopy.Save();

                //set the copy as the new selected item
                this.SelectedAssortment = itemCopy;

                //update the available infos
                UpdateAvailableAssortmentItems();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Deletes the item as a new version
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => DeleteVersion_Executed(),
                        p => DeleteVersion_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean DeleteVersion_CanExecute()
        {
            //must not be null
            if (this.SelectedAssortment == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have delete permission
            if (!_userHasAssortmentDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be new
            if (this.SelectedAssortment.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void DeleteVersion_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(
                    this.SelectedAssortment.ToString());
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the scheme as deleted
                Assortment itemToDelete = this.SelectedAssortment;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    //commit the delete
                    Assortment.DeleteById(itemToDelete.Id);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _gibraltarCategory);
                    if (this.AttachedControl != null)
                    {
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            itemToDelete.Name, OperationType.Delete,
                            this.AttachedControl);
                    }

                    base.ShowWaitCursor(true);
                }

                UpdateAvailableAssortmentItems();

                base.ShowWaitCursor(false);
            }

        }

        #endregion

        #region SaveAndNew

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Saves the current item and creates a new one
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16,
                        DisabledReason = SaveCommand.DisabledReason
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                attemptSave();
                NewCommand.Execute();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAndClose

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16,
                        DisabledReason = SaveCommand.DisabledReason
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean itemSaved = SaveCurrentItem();

            base.ShowWaitCursor(false);

            //if save completed
            if (itemSaved)
            {
                attemptSave();

                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }

        }

        #endregion

        #region AddLinkedProductHierarchyCommand

        private RelayCommand _addLinkedProductHierarchyCommand;

        /// <summary>
        /// Add related hierarchy product
        /// </summary>
        public RelayCommand AddLinkedProductHierarchyCommand
        {
            get
            {
                if (_addLinkedProductHierarchyCommand == null)
                {
                    _addLinkedProductHierarchyCommand =
                        new RelayCommand(p => LinkedProductHierarchy_Executed(), p => LinkedProductHierarchy_CanExecute())
                        {
                            FriendlyName = Message.Assortment_AddLinkedLocationHierarchyCommand
                        };
                    this.ViewModelCommands.Add(_addLinkedProductHierarchyCommand);
                }
                return _addLinkedProductHierarchyCommand;
            }
        }

        private Boolean LinkedProductHierarchy_CanExecute()
        {
            //user must have fetch permission
            if (!Csla.Rules.BusinessRules.HasPermission
                (Csla.Rules.AuthorizationActions.GetObject, typeof(ProductHierarchy)))
            {
                this.AddLinkedProductHierarchyCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            if (this.SelectedAssortment != null)
            {
                if (this.SelectedAssortment.IsNew)
                {
                    //user must have create permission
                    if (!_userHasAssortmentCreatePerm)
                    {
                        this.AddLinkedProductHierarchyCommand.DisabledReason = Message.Generic_NoCreatePermission;
                        return false;
                    }
                }
                else
                {
                    //user must have edit permission
                    if (!_userHasAssortmentEditPerm)
                    {
                        this.AddLinkedProductHierarchyCommand.DisabledReason = Message.Generic_NoEditPermission;
                        return false;
                    }
                }
            }

            return true;
        }

        private void LinkedProductHierarchy_Executed()
        {
            if (this.AttachedControl != null)
            {
                MerchGroupSelectionWindow merchandisingHierarchyWindow = new MerchGroupSelectionWindow();
                merchandisingHierarchyWindow.SelectionMode = DataGridSelectionMode.Single;

                App.ShowWindow(merchandisingHierarchyWindow, this.AttachedControl, true);

                if (merchandisingHierarchyWindow.DialogResult == true)
                {
                    this.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(merchandisingHierarchyWindow.SelectionResult[0]);
                }
            }
        }

        #endregion

        #region AddLinkedConsumerDecisionTreeCommand

        private RelayCommand _addLinkedConsumerDecisionTreeCommand;

        /// <summary>
        /// Add related hierarchy product
        /// </summary>
        public RelayCommand AddLinkedConsumerDecisionTreeCommand
        {
            get
            {
                if (_addLinkedConsumerDecisionTreeCommand == null)
                {
                    _addLinkedConsumerDecisionTreeCommand =
                        new RelayCommand(p => LinkedConsumerDecisionTree_Executed(), p => LinkedConsumerDecisionTree_CanExecute())
                        {
                            FriendlyName = Message.Assortment_AddLinkedLocationHierarchyCommand
                        };
                    this.ViewModelCommands.Add(_addLinkedConsumerDecisionTreeCommand);
                }
                return _addLinkedConsumerDecisionTreeCommand;
            }
        }

        private Boolean LinkedConsumerDecisionTree_CanExecute()
        {
            //user must have fetch permission
            if (!Csla.Rules.BusinessRules.HasPermission
                (Csla.Rules.AuthorizationActions.GetObject, typeof(ConsumerDecisionTreeInfo)))
            {
                this.AddLinkedConsumerDecisionTreeCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            if (this.SelectedAssortment != null)
            {
                if (this.SelectedAssortment.IsNew)
                {
                    //user must have create permission
                    if (!_userHasAssortmentCreatePerm)
                    {
                        this.AddLinkedConsumerDecisionTreeCommand.DisabledReason = Message.Generic_NoCreatePermission;
                        return false;
                    }
                }
                else
                {
                    //user must have edit permission
                    if (!_userHasAssortmentEditPerm)
                    {
                        this.AddLinkedConsumerDecisionTreeCommand.DisabledReason = Message.Generic_NoEditPermission;
                        return false;
                    }
                }
            }

            return true;
        }

        private void LinkedConsumerDecisionTree_Executed()
        {
            if (this.AttachedControl != null)
            {
                ConsumerDecisionTreeWindow consumerDecisionTreeWindow = new ConsumerDecisionTreeWindow();
                App.ShowWindow(consumerDecisionTreeWindow, this.AttachedControl, true);

                if (consumerDecisionTreeWindow.DialogResult == true)
                {
                    this.SelectedConsumerDecisonTree = consumerDecisionTreeWindow.SelectionResult;
                }
            }
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        /// <summary>
        /// Removes the selected products from the current assortment
        /// </summary>
        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_RemoveSelectedProducts,
                        FriendlyDescription = Message.AssortmentSetup_RemoveSelectedProducts,
                        Icon = ImageResources.AssortmentSetup_RemoveSelectedProducts,
                        DisabledReason = Message.AssortmentSetup_RemoveSelectedProducts_DisabledReason
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RemoveSelectedProducts_CanExecute()
        {
            return (this.SelectedProducts.Count > 0);
        }

        private void RemoveSelectedProducts_Executed()
        {
            this.SelectedAssortment.Products.RemoveList(this.SelectedProducts.Select(p => p.AssortmentProduct).ToList());
            this.SelectedProducts.Clear();
        }

        #endregion

        #region AddProduct

        private RelayCommand _addProductCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand AddProductCommand
        {
            get
            {
                if (_addProductCommand == null)
                {
                    _addProductCommand = new RelayCommand(
                        p => AddProduct_Executed(),
                        p => AddProduct_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_AddProduct,
                        FriendlyDescription = Message.AssortmentSetup_AddProduct_Tooltip,
                        Icon = ImageResources.AssortmentSetup_AddProducts
                    };
                    base.ViewModelCommands.Add(_addProductCommand);
                }
                return _addProductCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddProduct_CanExecute()
        {
            //user must have fetch permission
            if (!_userHasProductInfoFetchPerm)
            {
                this.AddProductCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //must have an assortment selected
            if (this.SelectedAssortment == null)
            {
                this.AddProductCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }


            if (this.SelectedAssortment.IsNew)
            {
                //user must have create permission
                if (!_userHasAssortmentCreatePerm)
                {
                    this.AddProductCommand.DisabledReason = Message.Generic_NoCreatePermission;
                    return false;
                }
            }
            else if (!_userHasAssortmentEditPerm)
            {
                //user must have edit permission
                this.AddProductCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;

            }


            return true;
        }

        private void AddProduct_Executed()
        {
            base.ShowWaitCursor(true);

            //Only continue if current items are saved/don't need saving
            if (this.AttachedControl != null)
            {
                List<Int32> takenProductIds =
                    (this.SelectedAssortment.Products != null) ?
                    this.SelectedAssortment.Products.Select(p => p.ProductId).ToList() : new List<Int32>();

                ProductSelectorViewModel winModel = new ProductSelectorViewModel(takenProductIds, this._selectedAssortment.ProductGroupId);
                winModel.ShowDialog(this.AttachedControl);

                if (winModel.DialogResult == true)
                {
                    List<Product> assignedProducts = winModel.AssignedProducts.ToList();

                    //remove any old products
                    List<AssortmentProduct> productsToRemove =
                        this.SelectedAssortment.Products.Where(p => !assignedProducts.Any(a => a.Id == p.ProductId)).ToList();
                    this.SelectedAssortment.Products.RemoveList(productsToRemove);

                    //add any new products
                    List<AssortmentProduct> productsToAdd = new List<AssortmentProduct>();
                    foreach (Product product in assignedProducts)
                    {
                        if (!takenProductIds.Contains(product.Id))
                        {
                            productsToAdd.Add(AssortmentProduct.NewAssortmentProduct(product));
                        }
                    }
                    this.SelectedAssortment.Products.AddList(productsToAdd);
                }

            }
            else
            {
                //for testing purposes
                this.SelectedAssortment.Products.AddList(ProductInfoList.FetchByEntityId(App.ViewState.EntityId));
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddLocationsByCodeCommand

        private RelayCommand _addLocationsByCodeCommand;

        /// <summary>
        /// Add the selected location(s)
        /// </summary>
        public RelayCommand AddLocationsByCodeCommand
        {
            get
            {
                if (_addLocationsByCodeCommand == null)
                {
                    _addLocationsByCodeCommand =
                        new RelayCommand(p => AddLocationsByCode_Executed(), p => AddLocationsByCode_CanExecute())
                        {
                            Icon = ImageResources.Add_32,
                            SmallIcon = ImageResources.AssortmentSetup_AddLocations,
                            FriendlyName = Message.AssortmentSetup_AddLocationsCodes,
                            FriendlyDescription = Message.AssortmentSetup_AddLocationsCodes_Description
                        };
                    this.ViewModelCommands.Add(_addLocationsByCodeCommand);
                }
                return _addLocationsByCodeCommand;
            }
        }

        private Boolean AddLocationsByCode_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.AddLocationsByCodeCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.AddLocationsByCodeCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            return true;
        }

        private void AddLocationsByCode_Executed()
        {
            //Build the view model
            if (this.AttachedControl != null)
            {
                if (CheckCanEditLocations())
                {
                    //Remove any deleted locations from the current assortment before continuing
                    foreach (LocationInfo location in this.CurrentLocations)
                    {
                        if (location.DateDeleted != null)
                        {
                            AssortmentLocation asstLocation = this.SelectedAssortment.Locations.
                                FirstOrDefault(l => l.LocationId == location.Id);
                            if (asstLocation != null)
                            {
                                this.SelectedAssortment.Locations.Remove(asstLocation);
                            }
                        }
                    }

                    AssortmentSetupAddLocationsByCodeViewModel addLocationsViewModel =
                        new AssortmentSetupAddLocationsByCodeViewModel(
                    this.SelectedAssortment);

                    //Open the Window
                    AssortmentSetupAddLocationsByCodeWindow addLocationsWindow =
                        new AssortmentSetupAddLocationsByCodeWindow(addLocationsViewModel);
                    addLocationsWindow.Owner = this.AttachedControl;
                    App.ShowWindow(addLocationsWindow, this.AttachedControl, /*isModal*/true);

                    UpdateLocationsGrid();
                }
            }
        }

        #endregion

        #region AddLocationsManuallyCommand

        private RelayCommand _addLocationsManuallyCommand;

        /// <summary>
        /// Add the selected location(s)
        /// </summary>
        public RelayCommand AddLocationsManuallyCommand
        {
            get
            {
                if (_addLocationsManuallyCommand == null)
                {
                    _addLocationsManuallyCommand =
                        new RelayCommand(p => AddLocationsManually_Executed(), p => AddLocationsManually_CanExecute())
                        {
                            Icon = ImageResources.Add_32,
                            SmallIcon = ImageResources.AssortmentSetup_AddLocations,
                            FriendlyName = Message.AssortmentSetup_AddLocationsManual,
                            FriendlyDescription = Message.AssortmentSetup_AddLocationsManual_Description
                        };
                    this.ViewModelCommands.Add(_addLocationsManuallyCommand);
                }
                return _addLocationsManuallyCommand;
            }
        }

        private Boolean AddLocationsManually_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.AddLocationsManuallyCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.AddLocationsManuallyCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            return true;
        }

        private void AddLocationsManually_Executed()
        {
            if (this.AttachedControl != null)
            {
                if (CheckCanEditLocations())
                {
                    //load the loc list if it does not already exist
                    if (_masterLocationList == null)
                    {
                        base.ShowWaitCursor(true);

                        try
                        {
                            _masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);
                        }
                        catch (DataPortalException ex)
                        {
                            base.ShowWaitCursor(false);

                            LocalHelper.RecordException(ex, _gibraltarCategory);
                            if (this.AttachedControl != null)
                            {
                                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                    String.Empty, OperationType.Open,
                                    this.AttachedControl);
                            }

                            base.ShowWaitCursor(true);
                        }
                        base.ShowWaitCursor(false);
                    }

                    //Open the Window
                    AssortmentSetupAddLocationsManuallyWindow addLocationsWindow =
                        new AssortmentSetupAddLocationsManuallyWindow(this.SelectedAssortment, _masterLocationList);

                    App.ShowWindow(addLocationsWindow, this.AttachedControl, /*isModal*/true);

                    UpdateLocationsGrid();
                }
            }
        }

        #endregion

        #region RemoveLocationsCommand

        private RelayCommand _removeLocationsCommand;

        /// <summary>
        /// Remove the selected location(s)
        /// </summary>
        public RelayCommand RemoveLocationsCommand
        {
            get
            {
                if (_removeLocationsCommand == null)
                {
                    _removeLocationsCommand =
                        new RelayCommand(p => RemoveLocations_Executed(), p => RemoveLocations_CanExecute())
                        {
                            Icon = ImageResources.Delete_32,
                            SmallIcon = ImageResources.Delete_16,
                            FriendlyName = Message.AssortmentSetup_RemoveLocationsButton_Caption,
                            FriendlyDescription = Message.AssortmentSetup_RemoveLocationsButton_Description
                        };
                    this.ViewModelCommands.Add(_removeLocationsCommand);
                }
                return _removeLocationsCommand;
            }
        }

        private Boolean RemoveLocations_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.RemoveLocationsCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.RemoveLocationsCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            if (this.SelectedAssortment.Locations != null && this.SelectedAssortment.Locations.Count == 0)
            {
                this.RemoveLocationsCommand.DisabledReason = Message.Assortment_SaveDisabled_NoLocation;
                return false;
            }

            if (this.SelectedCurrentLocations.Count == 0)
            {
                this.RemoveLocationsCommand.DisabledReason = Message.ProjectContentManagement_Disabled_NoSelectedLocations;
                return false;
            }

            return true;
        }

        private void RemoveLocations_Executed()
        {
            if (this.AttachedControl != null)
            {
                foreach (LocationInfo location in this.SelectedCurrentLocations)
                {
                    //Replace with code that will be used when the command is executed
                    AssortmentLocation asstLocation = this.SelectedAssortment.Locations.FirstOrDefault(l => l.LocationId == location.Id);
                    if (asstLocation != null)
                    {
                        this.SelectedAssortment.Locations.Remove(asstLocation);
                    }
                }

                //Update the locations grid
                UpdateLocationsGrid();
            }
        }

        #endregion

        #region ShowProductRulesCommand

        private RelayCommand _showProductRulesCommand;

        /// <summary>
        /// Show the Product Rules dialog
        /// </summary>
        public RelayCommand ShowProductRulesCommand
        {
            get
            {
                if (_showProductRulesCommand == null)
                {
                    _showProductRulesCommand =
                        new RelayCommand(p => ShowProductRules_Executed(), p => ShowProductRules_CanExecute())
                        {
                            Icon = ImageResources.AssortmentSetup_ProductRules,
                            FriendlyName = Message.AssortmentSetup_ProductRulesButton_Caption,
                            FriendlyDescription = Message.AssortmentSetup_ProductRulesButton_Description
                        };
                    this.ViewModelCommands.Add(_showProductRulesCommand);
                }
                return _showProductRulesCommand;
            }
        }

        private Boolean ShowProductRules_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.ShowProductRulesCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.ShowProductRulesCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            if (this.SelectedAssortment.Products != null && this.SelectedAssortment.Products.Count == 0)
            {
                this.ShowProductRulesCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void ShowProductRules_Executed()
        {
            if (this.AttachedControl != null)
            {
                //Build the view model
                //AssortmentProduct selectedProduct = null;
                //if (_selectedProducts.Count == 1)
                //{
                //    selectedProduct = _selectedProducts[0];
                //}
                //AssortmentSetupProductRulesViewModel productRulesViewModel =
                //    new AssortmentSetupProductRulesViewModel(this.SelectedAssortment, selectedProduct);

                //AssortmentSetupProductRulesWindow productRulesWindow =
                //    new AssortmentSetupProductRulesWindow(productRulesViewModel);
                //productRulesWindow.Owner = this.AttachedControl;
                //App.ShowWindow(productRulesWindow, /*isModal*/true);

                //Open the Window
                AssortmentSetupProductRuleWindow productRuleWindow =
                    new AssortmentSetupProductRuleWindow(this.SelectedAssortment);
                productRuleWindow.Owner = this.AttachedControl;
                App.ShowWindow(productRuleWindow, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #region ShowFamilyRulesCommand

        private RelayCommand _showFamilyRulesCommand;

        /// <summary>
        /// Add related Location 
        /// </summary>
        public RelayCommand ShowFamilyRulesCommand
        {
            get
            {
                if (_showFamilyRulesCommand == null)
                {
                    _showFamilyRulesCommand =
                        new RelayCommand(p => ShowFamilyRules_Executed(), p => ShowFamilyRules_CanExecute())
                        {
                            Icon = ImageResources.AssortmentSetup_FamilyRules,
                            FriendlyName = Message.AssortmentSetup_FamilyRulesButton_Caption,
                            FriendlyDescription = Message.AssortmentSetup_FamilyRulesButton_Description
                        };
                    this.ViewModelCommands.Add(_showFamilyRulesCommand);
                }
                return _showFamilyRulesCommand;
            }
        }

        private Boolean ShowFamilyRules_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.ShowFamilyRulesCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.ShowFamilyRulesCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            if (this.SelectedAssortment.Products != null && this.SelectedAssortment.Products.Count == 0)
            {
                this.ShowFamilyRulesCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void ShowFamilyRules_Executed()
        {
            AssortmentProduct selectedProduct = null;
            if (_selectedProducts.Count == 1)
            {
                if (_selectedProducts[0].AssortmentProduct.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None)
                {
                    selectedProduct = _selectedProducts[0].AssortmentProduct;
                }
            }

            //Show the assortment family rules window.
            CommonHelper.GetWindowService().ShowDialog<AssortmentSetupFamilyRulesWindow>(
                new AssortmentSetupFamilyRulesViewModel(this.SelectedAssortment, selectedProduct));
        }

        #endregion

        #region RegionSetupCommand

        private RelayCommand _regionSetupCommand;

        /// <summary>
        /// Add related Location 
        /// </summary>
        public RelayCommand RegionSetupCommand
        {
            get
            {
                if (_regionSetupCommand == null)
                {
                    _regionSetupCommand =
                        new RelayCommand(
                            p => RegionSetup_Executed(),
                            p => RegionSetup_CanExecute())
                        {
                            Icon = ImageResources.AssortmentSetup_RegionalProducts,
                            FriendlyName = Message.AssortmentSetup_RegionSetupButton_Caption,
                            FriendlyDescription = Message.AssortmentSetup_RegionSetupButton_Description
                        };
                    this.ViewModelCommands.Add(_regionSetupCommand);
                }
                return _regionSetupCommand;
            }
        }

        private Boolean RegionSetup_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.RegionSetupCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.RegionSetupCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            if (this.SelectedAssortment.Locations.Count == 0)
            {
                this.RegionSetupCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentLocations;
                return false;
            }

            return true;
        }

        private void RegionSetup_Executed()
        {
            if (this.AttachedControl != null)
            {
                //Build the view model
                AssortmentSetupAddRegionsViewModel addRegionsViewModel =
                    new AssortmentSetupAddRegionsViewModel(this.SelectedAssortment);

                //Open the Window
                AssortmentSetupAddRegionsWindow addRegionsWindow =
                    new AssortmentSetupAddRegionsWindow(addRegionsViewModel);
                App.ShowWindow(addRegionsWindow, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #region RegionalProductSetupCommand

        private RelayCommand _regionalProductSetupCommand;

        /// <summary>
        /// Add related Location 
        /// </summary>
        public RelayCommand RegionalProductSetupCommand
        {
            get
            {
                if (_regionalProductSetupCommand == null)
                {
                    _regionalProductSetupCommand =
                        new RelayCommand(
                            p => RegionalProductSetup_Executed(),
                            p => RegionalProductSetup_CanExecute())
                        {
                            Icon = ImageResources.AssortmentSetup_RegionalProducts,
                            FriendlyName = Message.AssortmentSetup_RegionalProductSetupButton_Caption,
                            FriendlyDescription = Message.AssortmentSetup_RegionalProductSetupButton_Description
                        };
                    this.ViewModelCommands.Add(_regionalProductSetupCommand);
                }
                return _regionalProductSetupCommand;
            }
        }

        private Boolean RegionalProductSetup_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this._regionalProductSetupCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.RegionalProductSetupCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            if (this.SelectedAssortment.Products != null && this.SelectedAssortment.Products.Count == 0)
            {
                this.RegionalProductSetupCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void RegionalProductSetup_Executed()
        {
            if (this.AttachedControl != null)
            {
                //Build the view model
                AssortmentSetupAddRegionalProductsViewModel addRegionalProductsViewModel =
                    new AssortmentSetupAddRegionalProductsViewModel(this.SelectedAssortment);

                //Open the Window
                AssortmentSetupAddRegionalProductsWindow addRegionalProductsWindow =
                    new AssortmentSetupAddRegionalProductsWindow(addRegionalProductsViewModel);
                App.ShowWindow(addRegionalProductsWindow, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #region ShowLocalProductsCommand

        private RelayCommand _showLocalProductsCommand;

        /// <summary>
        /// Show the Local Products dialog
        /// </summary>
        public RelayCommand ShowLocalProductsCommand
        {
            get
            {
                if (_showLocalProductsCommand == null)
                {
                    _showLocalProductsCommand =
                        new RelayCommand(p => ShowLocalProducts_Executed(), p => ShowLocalProducts_CanExecute())
                        {
                            Icon = ImageResources.AssortmentSetup_LocalProducts,
                            FriendlyName = Message.AssortmentSetup_LocalProductsButton_Caption,
                            FriendlyDescription = Message.AssortmentSetup_LocalProductsButton_Description
                        };
                    this.ViewModelCommands.Add(_showLocalProductsCommand);
                }
                return _showLocalProductsCommand;
            }
        }

        private Boolean ShowLocalProducts_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.ShowLocalProductsCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.ShowLocalProductsCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            if (this.SelectedAssortment.Products != null && this.SelectedAssortment.Products.Count == 0)
            {
                this.ShowLocalProductsCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void ShowLocalProducts_Executed()
        {
            Int32? selectedProductId = null;

            //Build the view model
            AssortmentProduct selectedProduct = null;
            if (_selectedProducts.Count == 1)
            {
                if (_selectedProducts[0].AssortmentProduct.ProductLocalizationType == PlanogramAssortmentProductLocalizationType.Local)
                {
                    selectedProduct = _selectedProducts[0].AssortmentProduct;
                    selectedProductId = selectedProduct.ProductId;
                }
            }

            //show the local products window.
            CommonHelper.GetWindowService().ShowDialog<AssortmentSetupLocalProductWindow>(
               new AssortmentSetupLocalProductViewModel(this.SelectedAssortment, selectedProductId));
        }

        #endregion

        #region ShowInventoryRulesCommand

        private RelayCommand _showInventoryRulesCommand;

        /// <summary>
        /// Add related Location 
        /// </summary>
        public RelayCommand ShowInventoryRulesCommand
        {
            get
            {
                if (_showInventoryRulesCommand == null)
                {
                    _showInventoryRulesCommand =
                        new RelayCommand(p => ShowInventoryRules_Executed(), p => ShowInventoryRules_CanExecute())
                        {
                            Icon = ImageResources.AssortmentSetup_InventoryRules,
                            FriendlyName = Message.AssortmentSetup_InventoryRules,
                            FriendlyDescription = Message.AssortmentSetup_InventoryRules_Description
                        };
                    this.ViewModelCommands.Add(_showInventoryRulesCommand);
                }
                return _showInventoryRulesCommand;
            }
        }

        private Boolean ShowInventoryRules_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.ShowInventoryRulesCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.ShowInventoryRulesCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            if (this.SelectedAssortment.Products != null && this.SelectedAssortment.Products.Count == 0)
            {
                this.ShowInventoryRulesCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        private void ShowInventoryRules_Executed()
        {
            if (this.AttachedControl != null)
            {
                //Open the Window
                AssortmentInventoryRuleWindow assortmentSetupInventoryRulesWindow =
                    new AssortmentInventoryRuleWindow(this.SelectedAssortment);
                App.ShowWindow(assortmentSetupInventoryRulesWindow, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #region ProductBuddyCommands

        private RelayCommand _productBuddyCommand;

        /// <summary>
        /// Show the product buddy screen
        /// </summary>
        public RelayCommand ProductBuddyCommand
        {
            get
            {
                if (_productBuddyCommand == null)
                {
                    _productBuddyCommand = new RelayCommand(
                            p => ProductBuddyShow_Executed(),
                            p => ProductBuddy_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddy,
                        FriendlyDescription = Message.AssortmentProductBuddy_Desc,
                        Icon = ImageResources.AssortmentSetup_ProductBuddys,
                        DisabledReason = Message.AssortmentSetup_NoAssortmentProducts
                    };
                    this.ViewModelCommands.Add(_productBuddyCommand);
                }
                return _productBuddyCommand;
            }
        }

        private Boolean ProductBuddy_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                this.ProductBuddyCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentProducts;
                return false;
            }

            if (this.SelectedAssortment.Products.Count <= 0)
            {
                this.ProductBuddyCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentProducts;
                return false;
            }

            return true;
        }

        #region ProductBuddyShowCommand

        private RelayCommand _productBuddyShowCommand;

        /// <summary>
        /// Launches a window showing existing product buddies
        /// </summary>
        public RelayCommand ProductBuddyShowCommand
        {
            get
            {
                if (_productBuddyShowCommand == null)
                {
                    _productBuddyShowCommand = new RelayCommand(
                        p => ProductBuddyShow_Executed(),
                        p => ProductBuddyShow_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddy_Show,
                        FriendlyDescription = Message.AssortmentProductBuddy_Show_Desc,
                        Icon = ImageResources.AssortmentSetup_ProductBuddys,
                        DisabledReason = Message.AssortmentSetup_NoAssortmentProducts
                    };
                    base.ViewModelCommands.Add(_productBuddyShowCommand);
                }
                return _productBuddyShowCommand;
            }
        }

        private Boolean ProductBuddyShow_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                return false;
            }

            if (this.SelectedAssortment.Products.Count <= 0)
            {
                return false;
            }

            return true;
        }

        private void ProductBuddyShow_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentProductBuddyWindow win = new AssortmentProductBuddyWindow(this.SelectedAssortment);
                App.ShowWindow(win, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #region ProductBuddyAddNewWizardCommand

        private RelayCommand _productBuddyAddNewWizardCommand;

        /// <summary>
        /// Launches the add new product buddy wizard
        /// </summary>
        public RelayCommand ProductBuddyAddNewWizardCommand
        {
            get
            {
                if (_productBuddyAddNewWizardCommand == null)
                {
                    _productBuddyAddNewWizardCommand = new RelayCommand(
                        p => ProductBuddyAddNewWizard_Executed(),
                        p => ProductBuddyAddNewWizard_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddyWizard,
                        FriendlyDescription = Message.AssortmentProductBuddyWizard_Desc,
                        Icon = ImageResources.AssortmentSetup_ProductBuddys_Wizard,
                        DisabledReason = Message.AssortmentSetup_NoAssortmentProducts
                    };
                    base.ViewModelCommands.Add(_productBuddyAddNewWizardCommand);
                }
                return _productBuddyAddNewWizardCommand;
            }
        }

        private Boolean ProductBuddyAddNewWizard_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                return false;
            }

            if (this.SelectedAssortment.Products.Count <= 0)
            {
                return false;
            }

            return true;
        }

        private void ProductBuddyAddNewWizard_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentProduct selectedProduct = null;

                if (this.SelectedProducts.Count > 0)
                {
                    selectedProduct = this.SelectedProducts.Select(s=> s.AssortmentProduct).FirstOrDefault();
                }

                AssortmentProductBuddyWizardWindow win = new AssortmentProductBuddyWizardWindow(this.SelectedAssortment, selectedProduct);
                App.ShowWindow(win, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #region ProductBuddyAddNewAdvancedCommand

        private RelayCommand _productBuddyAddNewAdvancedCommand;

        /// <summary>
        /// Launches the add new product buddy advanced window.
        /// </summary>
        public RelayCommand ProductBuddyAddNewAdvancedCommand
        {
            get
            {
                if (_productBuddyAddNewAdvancedCommand == null)
                {
                    _productBuddyAddNewAdvancedCommand = new RelayCommand(
                        p => ProductBuddyAddNewAdvanced_Executed(),
                        p => ProductBuddyAddNewAdvanced_CanExecute())
                    {
                        FriendlyName = Message.AssortmentProductBuddyAdvancedAdd,
                        FriendlyDescription = Message.AssortmentProductBuddyAdvancedAdd_Desc,
                        Icon = ImageResources.AssortmentSetup_ProductBuddys_Advanced,
                        DisabledReason = Message.AssortmentSetup_NoAssortmentProducts
                    };
                    base.ViewModelCommands.Add(_productBuddyAddNewAdvancedCommand);
                }
                return _productBuddyAddNewAdvancedCommand;
            }
        }

        private Boolean ProductBuddyAddNewAdvanced_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                return false;
            }

            if (this.SelectedAssortment.Products.Count <= 0)
            {
                return false;
            }

            return true;
        }

        private void ProductBuddyAddNewAdvanced_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentProductBuddyAdvancedAddWindow win =
                    new AssortmentProductBuddyAdvancedAddWindow(this.SelectedAssortment, this.SelectedProducts.Select(s=> s.AssortmentProduct).ToList());
                App.ShowWindow(win, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #endregion

        #region LocationBuddyCommand

        private RelayCommand _locationBuddyCommand;

        /// <summary>
        /// Show the location buddy screen
        /// </summary>
        public RelayCommand LocationBuddyCommand
        {
            get
            {
                if (_locationBuddyCommand == null)
                {
                    _locationBuddyCommand =
                        new RelayCommand(
                            p => LocationBuddyReview_Executed(),
                            p => LocationBuddy_CanExecute())
                        {
                            FriendlyName = Message.AssortmentLocationBuddy,
                            FriendlyDescription = Message.AssortmentLocationBuddy_Desc,
                            Icon = ImageResources.AssortmentSetup_LocationBuddys_Review,
                            DisabledReason = Message.AssortmentSetup_NoAssortmentLocations
                        };
                    this.ViewModelCommands.Add(_locationBuddyCommand);
                }
                return _locationBuddyCommand;
            }
        }

        private Boolean LocationBuddy_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                this.LocationBuddyCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentLocations;
                return false;
            }

            if (this.SelectedAssortment.Locations.Count <= 0)
            {
                this.LocationBuddyCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentLocations;
                return false;
            }

            return true;
        }

        #region LocationBuddyReviewCommand

        private RelayCommand _locationBuddyReviewCommand;

        /// <summary>
        /// Launches a window Reviewing existing Location buddies
        /// </summary>
        public RelayCommand LocationBuddyReviewCommand
        {
            get
            {
                if (_locationBuddyReviewCommand == null)
                {
                    _locationBuddyReviewCommand = new RelayCommand(
                        p => LocationBuddyReview_Executed(),
                        p => LocationBuddyReview_CanExecute()
                        )
                    {
                        FriendlyName = Message.AssortmentLocationBuddy_Review,
                        FriendlyDescription = Message.AssortmentLocationBuddy_Review_Desc,
                        Icon = ImageResources.AssortmentSetup_LocationBuddys_Review,
                        DisabledReason = Message.AssortmentSetup_NoAssortmentLocations
                    };
                    base.ViewModelCommands.Add(_locationBuddyReviewCommand);
                }
                return _locationBuddyReviewCommand;
            }
        }


        private Boolean LocationBuddyReview_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                return false;
            }

            if (this.SelectedAssortment.Locations.Count <= 0)
            {
                return false;
            }

            return true;
        }

        private void LocationBuddyReview_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentLocationBuddyReviewWindow win = new AssortmentLocationBuddyReviewWindow(this.SelectedAssortment);
                App.ShowWindow(win, /*isModal*/true);
            }
        }

        #endregion

        #region LocationBuddyAddNewCommand

        private RelayCommand _locationBuddyAddNewCommand;

        /// <summary>
        /// Launches the add new Location buddy 
        /// </summary>
        public RelayCommand LocationBuddyAddNewCommand
        {
            get
            {
                if (_locationBuddyAddNewCommand == null)
                {
                    _locationBuddyAddNewCommand = new RelayCommand(
                        p => LocationBuddyAddNew_Executed(),
                        p => LocationBuddyAddNew_CanExecute())
                    {
                        FriendlyName = Message.AssortmentLocationBuddy_AddNew,
                        FriendlyDescription = Message.AssortmentLocationBuddy_AddNew_Desc,
                        Icon = ImageResources.AssortmentSetup_LocationBuddys_AddNew,
                        DisabledReason = Message.AssortmentSetup_NoAssortmentLocations
                    };
                    base.ViewModelCommands.Add(_locationBuddyAddNewCommand);
                }
                return _locationBuddyAddNewCommand;
            }
        }

        private Boolean LocationBuddyAddNew_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                return false;
            }

            if (this.SelectedAssortment.Locations.Count <= 0)
            {
                return false;
            }

            return true;
        }

        private void LocationBuddyAddNew_Executed()
        {
            if (this.AttachedControl != null)
            {
                AssortmentLocationBuddyAddNewWindow win = new AssortmentLocationBuddyAddNewWindow(this.SelectedAssortment);
                App.ShowWindow(win, /*isModal*/true);
            }
        }

        #endregion

        #endregion

        #region ShowAttachFileCommand

        private RelayCommand _showAttachFileCommand;

        /// <summary>
        /// Show the Local Products dialog
        /// </summary>
        public RelayCommand ShowAttachFileCommand
        {
            get
            {
                if (_showAttachFileCommand == null)
                {
                    _showAttachFileCommand =
                        new RelayCommand(p => ShowAttachFile_Executed(), p => ShowAttachFile_CanExecute())
                        {
                            Icon = ImageResources.AssortmentSetup_AttachFile,
                            FriendlyName = Message.AssortmentSetup_AttachFileButton_Caption,
                            FriendlyDescription = Message.AssortmentSetup_AttachFileButton_Description
                        };
                    this.ViewModelCommands.Add(_showAttachFileCommand);
                }
                return _showAttachFileCommand;
            }
        }

        private Boolean ShowAttachFile_CanExecute()
        {
            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.ShowAttachFileCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedAssortment == null)
            {
                this.ShowAttachFileCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            return true;
        }

        private void ShowAttachFile_Executed()
        {
            if (this.AttachedControl != null)
            {
                //Build the view model
                AssortmentSetupAttachFileViewModel attachFileViewModel =
                    new AssortmentSetupAttachFileViewModel(this.SelectedAssortment);

                //Open the Window
                AssortmentSetupAttachFileWindow attachFileWindow =
                    new AssortmentSetupAttachFileWindow(attachFileViewModel);
                attachFileWindow.Owner = this.AttachedControl;
                App.ShowWindow(attachFileWindow, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #region UnrangeSelectedProductsCommand

        private RelayCommand _unrangeSelectedProductsCommand;

        /// <summary>
        /// Sets the IsRanged flag to false for the selected products
        /// </summary>
        public RelayCommand UnrangeSelectedProductsCommand
        {
            get
            {
                if (_unrangeSelectedProductsCommand == null)
                {
                    _unrangeSelectedProductsCommand = new RelayCommand(
                        p => UnrangeSelectedProducts_Executed(),
                        p => UnrangeSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_UnrangeSelectedProducts,
                        DisabledReason = Message.AssortmentSetup_UnrangeSelectedProducts_DisabledReason
                    };
                    base.ViewModelCommands.Add(_unrangeSelectedProductsCommand);
                }
                return _unrangeSelectedProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool UnrangeSelectedProducts_CanExecute()
        {
            return (this.SelectedProducts.Count > 0);
        }

        private void UnrangeSelectedProducts_Executed()
        {
            foreach (var selectedProduct in this.SelectedProducts.ToList())
            {
                selectedProduct.AssortmentProduct.IsRanged = false;
            }
        }

        #endregion

        #region RangeSelectedProductsCommand

        private RelayCommand _rangeSelectedProductsCommand;

        /// <summary>
        /// Sets the IsRanged flag to false for the selected products
        /// </summary>
        public RelayCommand RangeSelectedProductsCommand
        {
            get
            {
                if (_rangeSelectedProductsCommand == null)
                {
                    _rangeSelectedProductsCommand = new RelayCommand(
                        p => RangeSelectedProducts_Executed(),
                        p => RangeSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_RangeSelectedProducts,
                        DisabledReason = Message.AssortmentSetup_RangeSelectedProducts_DisabledReason
                    };
                    base.ViewModelCommands.Add(_rangeSelectedProductsCommand);
                }
                return _rangeSelectedProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RangeSelectedProducts_CanExecute()
        {
            return (this.SelectedProducts.Count > 0);
        }

        private void RangeSelectedProducts_Executed()
        {
            foreach (var selectedProduct in this.SelectedProducts.ToList())
            {
                selectedProduct.AssortmentProduct.IsRanged = true;
            }
        }

        #endregion

        #region UnrangeAllProductsCommand

        private RelayCommand _unrangeAllProductsCommand;

        /// <summary>
        /// Sets the IsRanged flag to false for the selected products
        /// </summary>
        public RelayCommand UnrangeAllProductsCommand
        {
            get
            {
                if (_unrangeAllProductsCommand == null)
                {
                    _unrangeAllProductsCommand = new RelayCommand(
                        p => UnrangeAllProducts_Executed(),
                        p => UnrangeAllProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_UnrangeAllProducts,
                        DisabledReason = Message.AssortmentSetup_UnrangeAllProducts_DisabledReason
                    };
                    base.ViewModelCommands.Add(_unrangeAllProductsCommand);
                }
                return _unrangeAllProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool UnrangeAllProducts_CanExecute()
        {
            if (this.SelectedAssortment != null)
            {
                return (this.SelectedAssortment.Products != null && this.SelectedAssortment.Products.Count > 0);
            }
            else
                return false;
        }

        private void UnrangeAllProducts_Executed()
        {
            foreach (AssortmentProduct selectedProduct in this.SelectedAssortment.Products.ToList())
            {
                selectedProduct.IsRanged = false;
            }
        }

        #endregion

        #region RangeAllProductsCommand

        private RelayCommand _rangeAllProductsCommand;

        /// <summary>
        /// Sets the IsRanged flag to false for the selected products
        /// </summary>
        public RelayCommand RangeAllProductsCommand
        {
            get
            {
                if (_rangeAllProductsCommand == null)
                {
                    _rangeAllProductsCommand = new RelayCommand(
                        p => RangeAllProducts_Executed(),
                        p => RangeAllProducts_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_RangeAllProducts,
                        DisabledReason = Message.AssortmentSetup_RangeAllProducts_DisabledReason
                    };
                    base.ViewModelCommands.Add(_rangeAllProductsCommand);
                }
                return _rangeAllProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RangeAllProducts_CanExecute()
        {
            if (this.SelectedAssortment != null)
            {
                return (this.SelectedAssortment.Products.Count > 0);
            }
            else return false;
        }

        private void RangeAllProducts_Executed()
        {
            foreach (AssortmentProduct selectedProduct in this.SelectedAssortment.Products.ToList())
            {
                selectedProduct.IsRanged = true;
            }
        }

        #endregion

        #region PrintPreviewCommand

        private RelayCommand _printPreviewComamnd;

        /// <summary>
        /// Shows a print preview using the current print option
        /// </summary>
        public RelayCommand PrintPreviewCommand
        {
            get
            {
                if (_printPreviewComamnd == null)
                {
                    _printPreviewComamnd = new RelayCommand(
                        p => PrintPreview_Executed(p), p => PrintPreview_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_PrintPreview,
                        FriendlyDescription = Message.Generic_PrintPreview_Desc,
                        Icon = ImageResources.PrintPreview_32
                    };
                    base.ViewModelCommands.Add(_printPreviewComamnd);
                }
                return _printPreviewComamnd;
            }
        }

        [DebuggerStepThrough]
        private Boolean PrintPreview_CanExecute(Object arg)
        {
            //a planogram has been selected
            if (this.SelectedAssortment == null)
            {
                this.PrintPreviewCommand.DisabledReason = String.Empty;
                return false;
            }

            if (!this.SaveCommand.CanExecute())
            {
                this.PrintPreviewCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void PrintPreview_Executed(Object arg)
        {
            throw new NotImplementedException();

            //String printOptionName = arg as String;

            ////if no name provided then get default
            //if (String.IsNullOrEmpty(printOptionName))
            //{
            //    base.ShowWaitCursor(true);

            //    try
            //    {

            //        PrintOptionInfo defaultInfo =
            //        PrintOptionInfo.FetchDefaultByEntityIdContentType(App.ViewState.EntityId, PrintOptionContentType.Assortment);
            //        if (defaultInfo != null)
            //        {
            //            printOptionName = defaultInfo.Name;
            //        }
            //    }
            //    catch (DataPortalException) { }

            //    base.ShowWaitCursor(false);
            //}

            //if (!String.IsNullOrEmpty(printOptionName))
            //{
            //    Boolean continueWithPreview = true;

            //    //warn if the item needs saving
            //    if (this.SelectedAssortment.IsDirty)
            //    {
            //        continueWithPreview = false;

            //        ModalMessage msg = new ModalMessage()
            //        {
            //            Header = Message.PrintPreview_ItemNeedsSaving_Header,
            //            Description = Message.PrintPreview_ItemNeedsSaving_Desc,
            //            MessageIcon = ImageResources.Dialog_Warning,
            //            ButtonCount = 2,
            //            Button1Content = Message.Generic_Save,
            //            Button2Content = Message.Generic_Cancel
            //        };
            //        App.ShowWindow(msg, this.AttachedControl, true);

            //        if (msg.Result == ModalMessageResult.Button1)
            //        {
            //            continueWithPreview = SaveCurrentItem();
            //        }
            //    }


            //    if (continueWithPreview)
            //    {
            //        Helpers.ViewPrintPreview(this.SelectedAssortment.UniqueContentReference, printOptionName, this.AttachedControl);
            //    }
            //}
        }

        #endregion

        #endregion

        #region Methods


        /// <summary>
        /// Updates the available content items collection
        /// 
        /// </summary>
        /// <param name="isShowLatestVersionsOnlyChanged">
        /// True if update request came from ShowLatestVersionOnly; false otherwise; default false
        /// </param>
        private void UpdateAvailableAssortmentItems(Boolean isShowLatestVersionsOnlyChanged = false)
        {
            base.ShowWaitCursor(true);

            //make note of the original content count
            Int32 originalContentCount = _availableAssortments.Count;

            //update the available content collection
            if (originalContentCount > 0) { _availableAssortments.Clear(); }

            AssortmentInfoList backstageAssortmentInfos;

            //if (this.FilterItemsKey != null && this.FilterItemsKey != String.Empty)
            //{
            //    backstageAssortmentInfos = AssortmentInfoList.FetchByEntityIdAssortmentSearchCriteria(App.ViewState.EntityId, this.FilterItemsKey);
            //}
            //else
            //{
            backstageAssortmentInfos = AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId);
            //}


            //if fetched items
            //if (backstageAssortmentInfos != null && backstageAssortmentInfos.Count() < MaxBackstageItemsFetch)
            //{
            _availableAssortments.AddRange(backstageAssortmentInfos);
            this.FilterItemsCountMessage = String.Format(Message.Assortment_FilterItemsCountMessage, backstageAssortmentInfos.Count);
            //}
            //else
            //{
            //    this.FilterItemsCountMessage = String.Format(Message.Assortment_FilterItemsCountMaxExceededMessage);
            //}

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedAssortment, this.SaveCommand);
            }
            return continueExecute;
        }

        private void UpdateLocationsGrid()
        {
            this.CurrentLocations.Clear();

            if (this.SelectedAssortment != null)
            {
                BulkObservableCollection<LocationInfo> newGridItems =
                    new BulkObservableCollection<LocationInfo>();

                if (this.SelectedAssortment.Locations != null)
                {
                    foreach (AssortmentLocation al in this.SelectedAssortment.Locations)
                    {
                        LocationInfo locationInfo = _allLocationsInfoListIncludingDeleted
                            .FirstOrDefault(l => l.Id == al.LocationId);
                        if (locationInfo != null)
                        {
                            newGridItems.Add(locationInfo);
                        }
                    }
                }
                foreach (var item in newGridItems)
                {
                    this.CurrentLocations.Add(item);
                }
            }
        }

        /// <summary>
        /// If locations for the assorment contain deleted items, need to check if the user wants to continue
        /// as it will automatically remove the deleted locations when the editor launches
        /// </summary>
        /// <returns></returns>
        private Boolean CheckCanEditLocations()
        {
            if (this.CurrentLocations.Any(l => l.DateDeleted != null))
            {
                ModalMessageResult messageResult = ModalMessageResult.Button2;

                ModalMessage win = new ModalMessage();
                win.Header = Message.AssortmentSetup_EditLocationsWhereDeletedExistsHeader;
                win.Description = Message.AssortmentSetup_EditLocationsWhereDeletedExistsDescription;
                win.MessageIcon = ImageResources.Dialog_Warning;
                win.ButtonCount = 2;
                win.Button1Content = Message.Generic_Yes;
                win.Button2Content = Message.Generic_Cancel;
                win.DefaultButton = ModalMessageButton.Button1;
                win.CancelButton = ModalMessageButton.Button2;
                App.ShowWindow(win, this.AttachedControl, true);

                messageResult = win.Result;
                return (messageResult != ModalMessageResult.Button2);
            }
            return true;
        }

        /// <summary>
        /// Adds in row views for the given assortment products.
        /// </summary>
        /// <param name="assortmentProducts"></param>
        private void AddAssortmentProductRows(IEnumerable<AssortmentProduct> assortmentProducts)
        {
            ProductList masterProducts = null;
            try
            {
                masterProducts = ProductList.FetchByEntityIdProductGtins(
                    App.ViewState.EntityId,
                    assortmentProducts.Select(p => p.Gtin).ToList());
            }
            catch (Exception ex)
            {
                CommonHelper.RecordException(ex);
            }

            List<AssortmentProductRow> rowsToAdd = new List<AssortmentProductRow>();
            foreach (AssortmentProduct ap in assortmentProducts)
            {
                Product masterProduct =
                   (masterProducts != null) ? masterProducts.FirstOrDefault(p => String.Compare(ap.Gtin, p.Gtin, true) == 0)
                   : null;

                rowsToAdd.Add(new AssortmentProductRow(ap, masterProduct));
            }
            _assortmentProductRows.AddRange(rowsToAdd);
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Called whenever the selected cdt changes.
        /// </summary>
        private void OnSelectedConsumerDecsionTreeChanged()
        {
            if (this.SelectedConsumerDecisonTree != null)
            {
                this.SelectedAssortment.ConsumerDecisionTreeId = this.SelectedConsumerDecisonTree.Id;
            }
        }


        /// <summary>
        /// Responds to a change of the master assortment info list
        /// </summary>
        private void MasterAssortmentInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<AssortmentInfoList> e)
        {
            OnMasterAssortmentInfoListViewModelChanged(e.NewModel);
        }
        private void OnMasterAssortmentInfoListViewModelChanged(AssortmentInfoList newModel)
        {
            //create the available assortment viewmodels
            if (_availableAssortments.Any()) _availableAssortments.Clear();

            if (newModel != null)
            {
                UpdateAvailableAssortmentItems();
            }
        }


        /// <summary>
        /// Responds to changes to the master loc list view
        /// </summary>
        private void LocationInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<LocationInfoList> e)
        {
            //reset the master location list
            //don't bother reloading it - it will be done if it is needed.
            _masterLocationList = null;
        }

        /// <summary>
        /// Responds to a change of selected assormtent
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnSelectedAssortmentChanged(Assortment oldValue, Assortment newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedAssortment_PropertyChanged;
                oldValue.Products.BulkCollectionChanged -= CurrentAssortment_ProductsBulkCollectionChanged;
            }


            if (_assortmentProductRows.Any()) _assortmentProductRows.Clear();
            if (_selectedProducts.Any()) _selectedProducts.Clear();

            ProductGroupInfo newSelectedProductGroup = null;

            if (newValue != null)
            {
                newValue.PropertyChanged += SelectedAssortment_PropertyChanged;
                newValue.Products.BulkCollectionChanged += CurrentAssortment_ProductsBulkCollectionChanged;

                //selected productgroup.
                newSelectedProductGroup = ProductGroupInfo.FetchById(newValue.ProductGroupId);

                //set the selected cdt
                ConsumerDecisionTreeInfo selectedCdt = null;
                if (newValue.ConsumerDecisionTreeId.HasValue)
                {
                    try
                    {
                        selectedCdt = ConsumerDecisionTreeInfoList.FetchByIds(
                            new List<Int32> { newValue.ConsumerDecisionTreeId.Value }).FirstOrDefault();
                    }
                    catch (Exception ex)
                    {
                        CommonHelper.RecordException(ex);
                    }
                }
                this.SelectedConsumerDecisonTree = selectedCdt;


                //set the assortment product rows
                AddAssortmentProductRows(newValue.Products);
            }

            this.SelectedMerchandisingGroup = newSelectedProductGroup;
        }

        /// <summary>
        /// Called whenever the current assortment products collection changes.
        /// </summary>
        private void CurrentAssortment_ProductsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    AddAssortmentProductRows(e.ChangedItems.Cast<AssortmentProduct>());
                    break;

                case NotifyCollectionChangedAction.Remove:
                    _assortmentProductRows.RemoveRange(_assortmentProductRows.Where(a =>
                    e.ChangedItems.Cast<AssortmentProduct>().Contains(a.AssortmentProduct)).ToArray());
                    break;

                case NotifyCollectionChangedAction.Reset:
                    {
                        //remove old products
                        _assortmentProductRows.RemoveRange(
                            _assortmentProductRows.Where(r => !this.SelectedAssortment.Products.Contains(r.AssortmentProduct)));

                        //add new ones.
                        AddAssortmentProductRows(this.SelectedAssortment.Products.Where(ap => !_assortmentProductRows.Any(r => r.AssortmentProduct == ap)).ToArray());
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to property changes on the selected assortment
        /// </summary>
        private void SelectedAssortment_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == Assortment.ProductGroupIdProperty.Name)
            {
                OnPropertyChanged(SelectedMerchandisingGroupProperty);
            }
            else if (e.PropertyName == Assortment.ConsumerDecisionTreeIdProperty.Name)
            {
                OnPropertyChanged(SelectedConsumerDecisonTreeProperty);
            }
        }

        #endregion

        #region Screen Permissions

        private Boolean _userHasAssortmentCreatePerm;
        private Boolean _userHasAssortmentFetchPerm;
        private Boolean _userHasAssortmentEditPerm;
        private Boolean _userHasAssortmentDeletePerm;
        private Boolean _userHasProductInfoFetchPerm;

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //Assortment premissions
            //create
            _userHasAssortmentCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(Assortment));

            //fetch
            _userHasAssortmentFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(Assortment));

            //edit
            _userHasAssortmentEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(Assortment));

            //delete
            _userHasAssortmentDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(Assortment));

            //ProductInfo
            _userHasProductInfoFetchPerm = Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(ProductInfoList));
        }

        /// <summary>
        /// Returns true if the user has sufficient permissions to launch this screen.
        /// </summary>
        /// <returns></returns>
        internal static Boolean UserHasRequiredAccessPerms()
        {
            return

                //assortment fetch perm
                Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(Assortment));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterAssortmentInfoListView.ModelChanged -= MasterAssortmentInfoListView_ModelChanged;
                    _masterLocationInfoListView.ModelChanged -= LocationInfoListView_ModelChanged;
                    this.SelectedAssortment = null;
                    _selectedProducts.Clear();
                    _availableAssortments.Clear();

                    _masterLocationInfoListView.Dispose();
                    //_consumerDecisionTreeInfoListView.Dispose();
                    _masterAssortmentInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    public enum LocationBarState
    {
        Normal = 0,
        Minimised = 1,
        Off = 2,
    }

    /// <summary>
    /// Binding validation rule to ensure assortment product rank is unique:
    /// </summary>
    public sealed class AssortmentProductRankRule : ValidationRule
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public AssortmentProductRankRule() : base(ValidationStep.CommittedValue, true) { }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            BindingExpression expression = value as BindingExpression;
            if (expression != null)
            {
                AssortmentProduct sourceItem = expression.DataItem as AssortmentProduct;
                if (sourceItem != null)
                {
                    AssortmentProductList assortProductList = (AssortmentProductList)sourceItem.Parent;

                    //validate non-zero, set rank values for uniquness
                    IEnumerable<AssortmentProduct> rankedProducts = assortProductList.Where(a => a.Rank > 0);

                    // if user entered at least one rank value -> validate uniqueness
                    if (rankedProducts.Any())
                    {
                        foreach (AssortmentProduct assortmentProduct in rankedProducts.ToList())
                        {
                            if (assortmentProduct != sourceItem)
                            {
                                if (assortmentProduct.Rank == sourceItem.Rank)
                                {
                                    validationResult = new ValidationResult(false, Message.Assortment_Product_RankValidation);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            // and return the validation result
            return validationResult;
        }
        #endregion

    }


}