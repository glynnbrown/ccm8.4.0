﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#endregion

using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupAddLocationsByCodeWindow.xaml
    /// </summary>
    public partial class AssortmentSetupAddLocationsByCodeWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupAddLocationsByCodeViewModel),
            typeof(AssortmentSetupAddLocationsByCodeWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentSetupAddLocationsByCodeViewModel ViewModel
        {
            get { return (AssortmentSetupAddLocationsByCodeViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupAddLocationsByCodeWindow senderControl =
                (AssortmentSetupAddLocationsByCodeWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupAddLocationsByCodeViewModel oldModel =
                    (AssortmentSetupAddLocationsByCodeViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                AssortmentSetupAddLocationsByCodeViewModel newModel =
                    (AssortmentSetupAddLocationsByCodeViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentSetupAddLocationsByCodeWindow(AssortmentSetupAddLocationsByCodeViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(AssortmentSetupAddLocationsByCodeWindow_Loaded);
        }

        /// <summary>
        /// Event handler for the on loaded event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssortmentSetupAddLocationsByCodeWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(AssortmentSetupAddLocationsByCodeWindow_Loaded);
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Event handler for the view model property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if ((e.PropertyName == AssortmentSetupAddLocationsByCodeViewModel.PastedLocationsProperty.Path) ||
                (e.PropertyName == AssortmentSetupAddLocationsByCodeViewModel.SelectedLocationsProperty.Path))
            {
                //Ensure the context menu items enabled states are updated if items are put into/removed from grid
                this.ViewModel.RemoveAllInvalidLocationsCommand.RaiseCanExecuteChanged();
                this.ViewModel.RemoveLocationCommand.RaiseCanExecuteChanged();
                this.ViewModel.PasteLocationsIntoGridCommand.RaiseCanExecuteChanged();
            }

        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+V selected, execute paste code
            if (e.Key == Key.V && Keyboard.Modifiers == ModifierKeys.Control)
            {
                this.ViewModel.PasteLocationsIntoGridCommand.Execute();
            }
        }

        #endregion

        #region OnClose

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        #endregion
    }
}
