﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-25454 : J.Pickup 
//  Created (Copied over from GFS)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    public class AssortmentSetupAddLocationsByCodeViewModel
        : ViewModelAttachedControlObject<AssortmentSetupAddLocationsByCodeWindow>
    {
        #region Fields

        private Assortment _selectedAssortment;
        private Boolean _isParsingList = false;
        private LocationInfoListViewModel _masterLocationInfoListView;
        private BulkObservableCollection<PastedLocation> _pastedLocations = new BulkObservableCollection<PastedLocation>();
        private PastedLocation _selectedLocation;
        private ObservableCollection<PastedLocation> _selectedLocations = new ObservableCollection<PastedLocation>();

        #endregion

        #region Binding properties

        public static readonly PropertyPath SelectedAssortmentProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsByCodeViewModel>(p => p.SelectedAssortment);

        public static readonly PropertyPath PastedLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsByCodeViewModel>(p => p.PastedLocations);
        public static readonly PropertyPath SelectedLocationProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsByCodeViewModel>(p => p.SelectedLocation);
        public static readonly PropertyPath SelectedLocationsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsByCodeViewModel>(p => p.SelectedLocations);

        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment
        /// </summary>
        public Assortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            set
            {
                _selectedAssortment = value;
                OnPropertyChanged(SelectedAssortmentProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the current selected location
        /// </summary>
        public PastedLocation SelectedLocation
        {
            get { return _selectedLocation; }
            set
            {
                _selectedLocation = value;
                OnPropertyChanged(SelectedLocationProperty);
            }
        }

        /// <summary>
        /// Returns the collection of locations pasted in by the user
        /// </summary>
        public BulkObservableCollection<PastedLocation> PastedLocations
        {
            get { return _pastedLocations; }
            set
            {
                _pastedLocations = value;
                OnPropertyChanged(PastedLocationsProperty);
            }
        }

        /// <summary>
        /// Returns the editable collection of selected locations
        /// </summary>
        public ObservableCollection<PastedLocation> SelectedLocations
        {
            get { return _selectedLocations; }
        }

        #endregion

        #region Constructor

        public AssortmentSetupAddLocationsByCodeViewModel(Assortment assortment)
        {
            //Get latest list of location info objects
            _masterLocationInfoListView = new LocationInfoListViewModel();
            _masterLocationInfoListView.FetchAllForEntity();

            //Load assortment selected in previous screen
            this.SelectedAssortment = assortment;
        }

        #endregion

        #region Commands

        #region PasteLocationsIntoGridCommand

        private RelayCommand<String> _pasteLocationsIntoGridCommand;

        /// <summary>
        /// Paste data from clipboard into grid command
        /// </summary>
        public RelayCommand<String> PasteLocationsIntoGridCommand
        {
            get
            {
                if (_pasteLocationsIntoGridCommand == null)
                {
                    _pasteLocationsIntoGridCommand = new RelayCommand<String>(
                        p => PasteLocationsIntoGrid_Executed(p),
                        p => PasteLocationsIntoGrid_CanExecute(), false)
                    {
                        FriendlyName = Message.ProjectContentManagement_PasteLocationsCommand
                    };
                    base.ViewModelCommands.Add(_pasteLocationsIntoGridCommand);
                }
                return _pasteLocationsIntoGridCommand;
            }
        }

        [DebuggerStepThrough]
        private bool PasteLocationsIntoGrid_CanExecute()
        {
            //Ensure clipboard contains some data
            if (!Clipboard.ContainsData(DataFormats.Text))
            {
                return false;
            }

            return true;
        }

        private void PasteLocationsIntoGrid_Executed(String locationText)
        {
            Boolean dataIsInvalid = false;
            String clipboardData = String.Empty;

            if (String.IsNullOrEmpty(locationText))
            {
                if (Clipboard.ContainsData(DataFormats.Text))
                {
                    try
                    {
                        clipboardData = Clipboard.GetData(DataFormats.Text) as String;
                        if (String.IsNullOrWhiteSpace(clipboardData))
                        {
                            //clipboard data is text but contains no codes
                            dataIsInvalid = true;
                        }
                    }
                    catch
                    {
                        //clipboard data is text but is not valid for some reason
                        dataIsInvalid = true;
                    }

                }
                else
                {
                    //clipboard data is not text
                    dataIsInvalid = true;
                }

                if (dataIsInvalid)
                {
                    //Inform user   
                    ModalMessage dialog = new ModalMessage();
                    dialog.Title = Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                    dialog.Header = Message.AssortmentSetup_PasteLocationsMessageTitle;
                    dialog.Description = Message.AssortmentSetup_PasteLocations_DataNotValid;
                    dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    dialog.Owner = this.AttachedControl;
                    dialog.ButtonCount = 1;
                    dialog.Button1Content = Message.Generic_Ok;
                    dialog.DefaultButton = ModalMessageButton.Button1;
                    dialog.MessageIcon = ImageResources.Dialog_Information;
                    dialog.ShowDialog();

                    return;
                }
            }
            else
            {
                //Testing purposes only
                clipboardData = locationText;
            }

            //All is ok, so continue

            //parse data
            List<String> locationCodes = new List<String>();

            //Get the codes pasted in
            String[] pastedCodes =
                clipboardData.Split(new String[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            //Add to a new collection
            ObservableCollection<PastedLocation> pastedRawData = new ObservableCollection<PastedLocation>();
            foreach (String pastedCode in pastedCodes.Where(s => (!String.IsNullOrWhiteSpace(s))))
            {
                PastedLocation newLocation = new PastedLocation(pastedCode);
                pastedRawData.Add(newLocation);
            }

            PastedLocations.CollectionChanged += PastedLocations_CollectionChanged;

            PastedLocations.AddRange(pastedRawData);

            OnPropertyChanged(PastedLocationsProperty);
        }

        #endregion

        #region RemoveLocationCommand

        private RelayCommand _removeLocationCommand;

        /// <summary>
        /// Removes the selected locations from the grid
        /// </summary>
        public RelayCommand RemoveLocationCommand
        {
            get
            {
                if (_removeLocationCommand == null)
                {
                    _removeLocationCommand = new RelayCommand(
                        p => RemoveLocation_Executed(),
                        p => RemoveLocation_CanExecute())
                    {
                        Icon = ImageResources.Delete_16
                    };
                    this.ViewModelCommands.Add(_removeLocationCommand);
                }
                return _removeLocationCommand;
            }
        }

        private Boolean RemoveLocation_CanExecute()
        {
            if (this.PastedLocations.Count == 0)
            {
                return false;
            };
            if (this.SelectedLocations.Count == 0)
            {
                return false;
            };
            if (_isParsingList)
            {
                return false;
            }

            return true;
        }

        private void RemoveLocation_Executed()
        {
            this._pastedLocations.RemoveRange(this.SelectedLocations);

            OnPropertyChanged(PastedLocationsProperty);
        }

        #endregion

        #region RemoveAllInvalidLocationsCommand

        private RelayCommand _removeAllInvalidLocationsCommand;

        /// <summary>
        /// Cancels the create database operation
        /// </summary>
        public RelayCommand RemoveAllInvalidLocationsCommand
        {
            get
            {
                if (_removeAllInvalidLocationsCommand == null)
                {
                    _removeAllInvalidLocationsCommand = new RelayCommand(
                        p => RemoveAllInvalidLocations_Executed(),
                        p => RemoveAllInvalidLocations_CanExecute(), false)
                    {
                        FriendlyName = Message.ProjectContentManagement_AddLocations_RemoveAllInvalidLocations,
                        Icon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeAllInvalidLocationsCommand);
                }
                return _removeAllInvalidLocationsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RemoveAllInvalidLocations_CanExecute()
        {
            if (this.PastedLocations.Count == 0)
            {
                return false;
            };
            if (_isParsingList)
            {
                return false;
            }

            return true;
        }

        private void RemoveAllInvalidLocations_Executed()
        {
            //Build list of items to remove
            List<PastedLocation> itemsToRemove = new List<PastedLocation>();
            foreach (PastedLocation location in this.PastedLocations)
            {
                if (location.Status != PastedItemSearchStatusType.LocationFound)
                {
                    itemsToRemove.Add(location);
                }
            }

            //remove them
            this.PastedLocations.RemoveRange(itemsToRemove);
            //Update UI
            OnPropertyChanged(PastedLocationsProperty);
        }

        #endregion

        #region AddLocationsToAssortmentCommand

        private RelayCommand _addLocationsToAssortmentCommand;

        /// <summary>
        /// adds the specified locations to the Assortment
        /// </summary>
        public RelayCommand AddLocationsToAssortmentCommand
        {
            get
            {
                if (_addLocationsToAssortmentCommand == null)
                {
                    _addLocationsToAssortmentCommand = new RelayCommand(
                        p => AddLocationsToAssortment_Executed(),
                        p => AddLocationsToAssortment_CanExecute())
                    {
                        FriendlyName = Message.ProjectContentManagement_AddMatchedLocations
                    };
                    base.ViewModelCommands.Add(_addLocationsToAssortmentCommand);
                }
                return _addLocationsToAssortmentCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddLocationsToAssortment_CanExecute()
        {
            if (!this.PastedLocations.Any(p => p.Status == PastedItemSearchStatusType.LocationFound))
            {
                return false;
            };

            if (_isParsingList)
            {
                return false;
            }

            return true;
        }

        private void AddLocationsToAssortment_Executed()
        {
            List<Int16> existingLocationIds = this.SelectedAssortment.Locations.Select(l => l.LocationId).ToList();

            //Add the valid locations to the project content
            foreach (PastedLocation location in
                this.PastedLocations
                    .Where(p => (p.Status == PastedItemSearchStatusType.LocationFound) &&
                                (!existingLocationIds.Contains(p.MatchedLocation.Id))))
            {
                AssortmentLocation loc = AssortmentLocation.NewAssortmentLocation(location.MatchedLocation);
                this.SelectedAssortment.Locations.Add(loc);
            }

            //Close the form
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the create database operation
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute(), false)
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Cancel_CanExecute()
        {
            return true;
        }

        private void Cancel_Executed()
        {
            //close the window
            this.AttachedControl.Close();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Executes parsing if collection changes when new items are pasted in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PastedLocations_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    ParsePastedData();
                    break;

                case NotifyCollectionChangedAction.Move:
                case NotifyCollectionChangedAction.Remove:
                case NotifyCollectionChangedAction.Replace:
                    break;


                case NotifyCollectionChangedAction.Reset:
                    ParsePastedData();
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Looks up the pasted data and matches it to existing location codes
        /// </summary>
        private void ParsePastedData()
        {
            //prevent items being removed while parsing
            _isParsingList = true;

            //get the list of locations already in the assortment
            List<Int16> existingAssortmentLocations =
                this.SelectedAssortment.Locations.Select(l => l.LocationId).ToList();

            //Build a list of locations we have already found
            List<LocationInfo> existingLocationInfos = new List<LocationInfo>();

            //parse the list to find existing locations
            foreach (PastedLocation pastedItem in this.PastedLocations)
            {
                LocationInfo foundLocationInfo =
                    this._masterLocationInfoListView.Model
                        .FirstOrDefault(l => l.Code.ToUpper() == pastedItem.PastedData.ToUpper());
                if (foundLocationInfo != null)
                {
                    //See if one already existing in the assortment locations list
                    if (existingAssortmentLocations.Contains(foundLocationInfo.Id))
                    {
                        //raise as already existing
                        pastedItem.Status = PastedItemSearchStatusType.AlreadyExistsInAssortment;
                        pastedItem.MatchedLocation = null;
                    }
                    //See if one already exists in this list
                    else if (existingLocationInfos.Contains(foundLocationInfo))
                    {
                        //raise as a duplicate
                        pastedItem.Status = PastedItemSearchStatusType.DuplicateLocation;
                        pastedItem.MatchedLocation = null;
                    }
                    else
                    {
                        //it is a new location so add it 
                        pastedItem.Status = PastedItemSearchStatusType.LocationFound;
                        pastedItem.MatchedLocation = foundLocationInfo;
                        existingLocationInfos.Add(foundLocationInfo);
                    }
                }
                else
                {
                    pastedItem.Status = PastedItemSearchStatusType.CodeNotFound;
                }
            }

            OnPropertyChanged(PastedLocationsProperty);

            PastedLocations.CollectionChanged -= PastedLocations_CollectionChanged;

            _isParsingList = false;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    PastedLocations.CollectionChanged -= PastedLocations_CollectionChanged;

                    _selectedLocations.Clear();
                    _pastedLocations.Clear();

                    _selectedLocations = null;
                    _pastedLocations = null;
                    _selectedAssortment = null;

                    _masterLocationInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Represents an item in the PastedLocations grid
    /// </summary>
    public class PastedLocation
    {
        #region Fields

        private String _pastedData = String.Empty;
        private PastedItemSearchStatusType _status = PastedItemSearchStatusType.Searching;
        private String _statusFriendlyName = String.Empty;
        private String _matchedLocationReference = String.Empty;
        private LocationInfo _matchedLocation = null;

        #endregion

        #region Properties

        public PastedItemSearchStatusType Status
        {
            get { return _status; }
            set
            {
                _status = value;
                this.StatusFriendlyName = PastedItemSearchStatusTypeHelper.FriendlyNames[_status];
            }
        }

        public String StatusFriendlyName
        {
            get { return _statusFriendlyName; }
            set { _statusFriendlyName = value; }
        }

        public String PastedData
        {
            get { return _pastedData; }
            set { _pastedData = value; }
        }

        public String MatchedLocationReference
        {
            get { return _matchedLocationReference; }
            set
            {
                _matchedLocationReference = value;
            }
        }

        public LocationInfo MatchedLocation
        {
            get { return _matchedLocation; }
            set
            {
                _matchedLocation = value;
                if (_matchedLocation != null)
                {
                    this.MatchedLocationReference =
                        String.Format("{0} : {1}", _matchedLocation.Code, _matchedLocation.Name);
                }
                else
                {
                    this.MatchedLocationReference = String.Empty;
                }
            }
        }

        #endregion

        #region Constructor

        public PastedLocation(String pastedData)
        {
            _pastedData = pastedData;
            this.Status = PastedItemSearchStatusType.Searching;
        }

        #endregion
    }

    public enum PastedItemSearchStatusType
    {
        Searching = 0,
        CodeNotFound = 1,
        LocationFound = 2,
        DuplicateLocation = 3,
        AlreadyExistsInAssortment = 4
    }

    public static class PastedItemSearchStatusTypeHelper
    {
        public static readonly Dictionary<PastedItemSearchStatusType, String> FriendlyNames =
            new Dictionary<PastedItemSearchStatusType, String>()
            {
                {PastedItemSearchStatusType.Searching, Message.Enum_PastedItemSearchStatusType_Searching},
                {PastedItemSearchStatusType.CodeNotFound, Message.Enum_PastedItemSearchStatusType_CodeNotFound},
                {PastedItemSearchStatusType.LocationFound, Message.Enum_PastedItemSearchStatusType_LocationFound},
                {PastedItemSearchStatusType.DuplicateLocation, Message.Enum_PastedItemSearchStatusType_DuplicateLocation},
                {PastedItemSearchStatusType.AlreadyExistsInAssortment, Message.Enum_PastedItemSearchStatusType_AlreadyExistsInAssortment}
            };
    }
}
