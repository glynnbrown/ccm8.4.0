﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-31950 : A.Probyn
//  Added SelectionMode
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Windows.Controls;
using System.ComponentModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentProductSelectionWindow.xaml
    /// </summary>
    public partial class AssortmentProductSelectionWindow : ExtendedRibbonWindow
    {
        #region Fields

        private IEnumerable<AssortmentProductView> _selectionResult;
        private IEnumerable<AssortmentProductView> _availableAssortmentProducts;
        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        public IEnumerable<AssortmentProductView> SelectionResult
        {
            get { return _selectionResult; }
            private set
            {
                _selectionResult = value;
            }
        }
        
        #region SelectionMode Property

        /// <summary>
        ///     <see cref="DependencyProperty" /> for the <see cref="SelectionMode" /> property.
        /// </summary>
        public static readonly DependencyProperty SelectionModeProperty = DependencyProperty.Register("SelectionMode",
            typeof(DataGridSelectionMode), typeof(AssortmentProductSelectionWindow),
            new PropertyMetadata(DataGridSelectionMode.Single));

        /// <summary>
        ///     Gets or set the <see cref="DataGridSelectionMode" /> for <see cref="xSelectionGrid" />.
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return (DataGridSelectionMode)GetValue(SelectionModeProperty); }
            set { SetValue(SelectionModeProperty, value); }
        }

        #endregion
        
        #endregion

        #region Constructor

        public AssortmentProductSelectionWindow(IEnumerable<AssortmentProductView> availableAssortmentProducts)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.Loaded += AssortmentProductSelectionWindow_Loaded;

            this._availableAssortmentProducts = availableAssortmentProducts;
        }

        private void AssortmentProductSelectionWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentProductSelectionWindow_Loaded;

            var factory =
            new PlanogramAssortmentProductColumnLayoutFactory(typeof(AssortmentProductView),
                    null,
                    new List<Framework.Model.IModelPropertyInfo>() 
                    { 
                        Product.GtinProperty,
                        Product.NameProperty
                    });

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                factory,
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                CCMClient.ColumnScreenKeyAssortmentProducts, /*PathMask*/"Product.{0}");

            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(assortmentProductGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //set data
            assortmentProductGrid.ItemsSourceExtended = _availableAssortmentProducts.ToList();
        }
        
        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix on addition columns:
            Int32 curIdx = 0;

            //Add default product columns
            foreach (DataGridColumn column in BuildAssortmentProductColumnList())
            {
                columnSet.Insert(curIdx, column);
                curIdx++;
            }
        }
        
        private void assortmentProductGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.DialogResult = true;
        }

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            //set the result (will close the window)
            this.DialogResult = true;
        }

        /// <summary>
        /// When enter is pressed return selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void assortmentProductGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Enter:
                    if (assortmentProductGrid.SelectedItems.Count > 0)
                    {
                        e.Handled = true;
                        this.DialogResult = true;
                    }
                    break;
                default:
                    break;
            }
        }

        #endregion

        #region Methods
        
        private ObservableCollection<DataGridColumn> BuildAssortmentProductColumnList()
        {
            //load the columnset
            ObservableCollection<DataGridColumn> columnList = new ObservableCollection<DataGridColumn>();

            DataGridTextColumn col0 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    AssortmentProduct.GtinProperty.FriendlyName,
                    AssortmentProductView.ProductGtinProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col0.MinWidth = 50;
            col0.Width = 80;
            columnList.Add(col0);

            DataGridTextColumn col1 = ExtendedDataGrid.CreateReadOnlyTextColumn(
                    AssortmentProduct.NameProperty.FriendlyName,
                    AssortmentProductView.ProductNameProperty.Path,
                    System.Windows.HorizontalAlignment.Left);
            col1.Width = 250;
            col1.MinWidth = 50;
            columnList.Add(col1);

            return columnList;
        }

        #endregion

        #region window close

        /// <summary>
        /// Cancel view model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            //set the result (will close the window)
            this.DialogResult = false;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (this.DialogResult == true)
            {
                this.SelectionResult = assortmentProductGrid.SelectedItems.Cast<AssortmentProductView>();
            }
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   if (_columnLayoutManager != null)
                   {
                       _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                       _columnLayoutManager.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}