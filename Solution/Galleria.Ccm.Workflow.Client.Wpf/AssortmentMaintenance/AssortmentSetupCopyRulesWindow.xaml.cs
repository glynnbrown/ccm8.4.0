﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (V8 8.3.0)
// V8-31950 : A.Probyn
//  Created (not complete, and not released in 8.3.0). This will be addressed in a future version.
#endregion
#endregion

using System.Windows;
using Galleria.Framework.Controls.Wpf;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using System.Windows.Input;
using System;
using System.Linq;
using System.ComponentModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance
{
    /// <summary>
    /// Interaction logic for AssortmentSetupAssortmentSetupCopyRulesWindow.xaml
    /// </summary>
    public sealed partial class AssortmentSetupCopyRulesWindow : ExtendedRibbonWindow
    {
    }
}
//        const String RemoveRulesCommandKey = "CopyRules_RemoveRulesCommand";

//        #region Properties

//        #region ViewModel property

//        public static readonly DependencyProperty ViewModelProperty =
//            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupCopyRulesViewModel), typeof(AssortmentSetupCopyRulesWindow),
//            new PropertyMetadata(null, OnViewModelPropertyChanged));

//        public AssortmentSetupCopyRulesViewModel ViewModel
//        {
//            get { return (AssortmentSetupCopyRulesViewModel)GetValue(ViewModelProperty); }
//            private set { SetValue(ViewModelProperty, value); }
//        }

//        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
//        {
//            AssortmentSetupCopyRulesWindow senderControl = (AssortmentSetupCopyRulesWindow)obj;

//            if (e.OldValue != null)
//            {
//                AssortmentSetupCopyRulesViewModel oldModel = (AssortmentSetupCopyRulesViewModel)e.OldValue;
//                oldModel.AttachedControl = null;

//                senderControl.Resources.Remove(RemoveRulesCommandKey);
//            }

//            if (e.NewValue != null)
//            {
//                AssortmentSetupCopyRulesViewModel newModel = (AssortmentSetupCopyRulesViewModel)e.NewValue;
//                newModel.AttachedControl = senderControl;

//                senderControl.Resources.Add(RemoveRulesCommandKey, newModel.RemoveRuleCommand);
//            }
//        }

//        #endregion

//        #endregion

//        #region Constructor

//        public AssortmentSetupCopyRulesWindow(AssortmentSetupCopyRulesViewModel viewModel)
//        {
//            Mouse.OverrideCursor = Cursors.Wait;

//            InitializeComponent();

//            //Add link to CCM.chm
//            //Help.SetFilename((DependencyObject)this, App.ViewState.HelpFilePath);
//            //Help.SetKeyword((DependencyObject)this, "48");

//            this.ViewModel = viewModel; ;

//            this.Loaded += AssortmentSetupCopyRulesWindow_Loaded;
//        }

//        private void AssortmentSetupCopyRulesWindow_Loaded(object sender, RoutedEventArgs e)
//        {
//            this.Loaded -= AssortmentSetupCopyRulesWindow_Loaded;

//            //cancel the busy cursor
//            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
//        }

//        #endregion

//        #region Window Close

//        protected override void OnClosed(System.EventArgs e)
//        {
//            base.OnClosed(e);

//            Dispatcher.BeginInvoke(
//               (Action)(() =>
//               {
//                   IDisposable disposableViewModel = this.ViewModel;
//                   this.ViewModel = null;

//                   if (disposableViewModel != null)
//                   {
//                       disposableViewModel.Dispose();
//                   }

//               }), priority: System.Windows.Threading.DispatcherPriority.Background);
//        }


//        #endregion
//    }
//}
