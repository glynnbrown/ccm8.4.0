﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Collections.Generic;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupOrganiser.xaml
    /// </summary>
    public sealed partial class AssortmentSetupOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        private ColumnLayoutManager _columnLayoutManager;
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupViewModel), typeof(AssortmentSetupOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentSetupViewModel ViewModel
        {
            get { return (AssortmentSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupOrganiser senderControl = (AssortmentSetupOrganiser)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupViewModel oldModel = (AssortmentSetupViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                AssortmentSetupViewModel newModel = (AssortmentSetupViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }



        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentSetupOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.AssortmentMaintenance);

            this.ViewModel = new AssortmentSetupViewModel();

            this.Loaded += AssortmentSetupOrganiser_Loaded;
        }



        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions on initial load.
        /// </summary>
        private void AssortmentSetupOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentSetupOrganiser_Loaded;

            //hide the location bar.
            this.ViewModel.LocationBarState = LocationBarState.Minimised;

            //create the grid column factory.
            var factory =new AssortmentProductCustomColumnLayoutFactory(typeof(AssortmentProductRow), null);

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(factory, App.ViewState.DisplayUnits, "Assortment");
            _columnLayoutManager.AttachDataGrid(xAssignedProducts);


            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel controller
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == AssortmentSetupViewModel.SelectedAssortmentProperty.Path)
            {
                //[GFS-21899] When the selected assortment changes clear all filters from the products grid.
                if (this.xAssignedProducts != null)
                {
                    this.xAssignedProducts.ClearFilterValues();
                    this.xAssignedProducts.GroupByDescriptions.Clear();
                }
            }
        }

        /// <summary>
        /// Called on key down.
        /// </summary>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.backstageOpenTab.IsSelected = true;
            }
        }


        /// <summary>
        /// Event fired when the user finishes dragging the locations bar grid splitter
        /// </summary>
        private void xLocationsSplitter_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                //If in normal view mode and user has made window as small as possible, change to minimised mode
                if (this.ViewModel.LocationBarState == LocationBarState.Normal)
                {
                    GridLength gl = xLocationsColumn.Width;
                    if (gl.Value == xLocationsColumn.MinWidth)
                    {
                        this.ViewModel.LocationBarState = LocationBarState.Minimised;
                    }
                }
            }
        }

        /// <summary>
        /// Event fired when the user clicks the minimised locations bar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xChangeLocationsToNormalView_Click(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.LocationBarState = LocationBarState.Normal;
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion

        private void xLocationsPanelCollapseButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.LocationBarState = LocationBarState.Minimised;
            }
        }

        private void xLocationsPanelExpandButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.LocationBarState = LocationBarState.Normal;
            }
        }
    }
}