﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-25454 : J.Pickup 
//  Created (Copied over from GFS)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Diagnostics;
using System.Collections.ObjectModel;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    public sealed class AssortmentSetupAddLocationsManuallyViewModel
        : ViewModelAttachedControlObject<AssortmentSetupAddLocationsManuallyWindow>
    {
        #region Fields

        private Assortment _selectedAssortment;
        private LocationList _masterLocationList;

        private BulkObservableCollection<Location> _availableLocations = new BulkObservableCollection<Location>();
        private ReadOnlyBulkObservableCollection<Location> _availableLocationsRO;
        private ObservableCollection<Location> _selectedAvailableLocations = new ObservableCollection<Location>();

        private BulkObservableCollection<Location> _addedLocations = new BulkObservableCollection<Location>();
        private ReadOnlyBulkObservableCollection<Location> _addedLocationsRO;
        private ObservableCollection<Location> _selectedLocationsToRemove = new ObservableCollection<Location>();

        #endregion

        #region Binding properties

        //properties
        public static readonly PropertyPath SelectedAssortmentProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.SelectedAssortment);
        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedAvailableLocationsProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.SelectedAvailableLocations);
        public static readonly PropertyPath AddedLocationsProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.AddedLocations);
        public static readonly PropertyPath SelectedLocationsToRemoveProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.SelectedLocationsToRemove);

        //commands
        public static readonly PropertyPath AddLocationsCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.AddLocationsCommand);
        public static readonly PropertyPath RemoveLocationCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.RemoveLocationCommand);
        public static readonly PropertyPath AddLocationsToAssortmentCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.AddLocationsToAssortmentCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<AssortmentSetupAddLocationsManuallyViewModel>(p => p.CancelCommand);


        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment object
        /// </summary>
        public Assortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            private set
            {
                _selectedAssortment = value;
                OnPropertyChanged(SelectedAssortmentProperty);

                OnSelectedAssortmentChanged();
            }
        }

        /// <summary>
        /// Gets the list of all locations
        /// </summary>
        public ReadOnlyBulkObservableCollection<Location> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyBulkObservableCollection<Location>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected locations
        /// </summary>
        public ObservableCollection<Location> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
        }

        /// <summary>
        /// Gets the list of locations to add to the project content
        /// </summary>
        public ReadOnlyBulkObservableCollection<Location> AddedLocations
        {
            get
            {
                if (_addedLocationsRO == null)
                {
                    _addedLocationsRO = new ReadOnlyBulkObservableCollection<Location>(_addedLocations);
                }
                return _addedLocationsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected locations
        /// </summary>
        public ObservableCollection<Location> SelectedLocationsToRemove
        {
            get { return _selectedLocationsToRemove; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assortment"></param>
        public AssortmentSetupAddLocationsManuallyViewModel(Assortment assortment, LocationList masterLocationList)
        {
            //locations
            _masterLocationList = masterLocationList;
            if (_masterLocationList == null)
            {
                _masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);
            }

            //assortment
            this.SelectedAssortment = assortment;
        }

        #endregion

        #region Commands

        #region AddLocationsCommand

        private RelayCommand _addLocationsCommand;

        /// <summary>
        /// adds the specified locations to the content
        /// </summary>
        public RelayCommand AddLocationsCommand
        {
            get
            {
                if (_addLocationsCommand == null)
                {
                    _addLocationsCommand = new RelayCommand(
                        p => AddLocations_Executed(),
                        p => AddLocations_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_AddLocationsButton_Caption,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addLocationsCommand);
                }
                return _addLocationsCommand;
            }
        }

        private Boolean AddLocations_CanExecute()
        {
            if (this.SelectedAvailableLocations.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddLocations_Executed()
        {
            base.ShowWaitCursor(true);

            List<Location> addList = this.SelectedAvailableLocations.ToList();

            //clear for speed
            this.SelectedAvailableLocations.Clear();

            //remove from the available
            _availableLocations.RemoveRange(addList);

            //add the the assigned
            _addedLocations.AddRange(addList);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region RemoveLocationCommand

        private RelayCommand _removeLocationCommand;

        /// <summary>
        /// Removes a location from list to add to project
        /// </summary>
        public RelayCommand RemoveLocationCommand
        {
            get
            {
                if (_removeLocationCommand == null)
                {
                    _removeLocationCommand = new RelayCommand(
                        p => RemoveLocation_Executed(),
                        p => RemoveLocation_CanExecute())
                    {
                        FriendlyName = Message.ProjectContentManagement_AddLocations_RemoveSelection,
                        SmallIcon = ImageResources.Delete_16
                    };
                    this.ViewModelCommands.Add(_removeLocationCommand);
                }
                return _removeLocationCommand;
            }
        }

        private Boolean RemoveLocation_CanExecute()
        {
            if (this.SelectedLocationsToRemove.Count == 0)
            {
                return false;
            }


            return true;
        }

        private void RemoveLocation_Executed()
        {
            base.ShowWaitCursor(true);

            List<Location> removeList = this.SelectedLocationsToRemove.ToList();

            //clear out the selection for speed
            this.SelectedLocationsToRemove.Clear();

            //remove from assigned
            _addedLocations.RemoveRange(removeList);

            //add back to the available
            _availableLocations.AddRange(removeList);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddLocationsToAssortmentCommand

        private RelayCommand _addLocationsToAssortmentCommand;

        /// <summary>
        /// adds the specified locations to the project content
        /// </summary>
        public RelayCommand AddLocationsToAssortmentCommand
        {
            get
            {
                if (_addLocationsToAssortmentCommand == null)
                {
                    _addLocationsToAssortmentCommand = new RelayCommand(
                        p => AddLocationsToAssortment_Executed(),
                        p => AddLocationsToAssortment_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_AddLocationsButton_Caption
                    };
                    base.ViewModelCommands.Add(_addLocationsToAssortmentCommand);
                }
                return _addLocationsToAssortmentCommand;
            }
        }

        private Boolean AddLocationsToAssortment_CanExecute()
        {
            //must have locations assigned
            if (this.AddedLocations.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddLocationsToAssortment_Executed()
        {
            base.ShowWaitCursor(true);

            List<Int16> currentLocationIds = this.SelectedAssortment.Locations.Select(l => l.LocationId).ToList();
            List<Int16> revisedLocationIds = this.AddedLocations.Select(l => l.Id).ToList();

            //Remove any project content locations not in the added list anymore
            List<AssortmentLocation> assortmentLocationsToRemove = new List<AssortmentLocation>();
            foreach (AssortmentLocation al in this.SelectedAssortment.Locations)
            {
                if (!revisedLocationIds.Contains(al.LocationId))
                {
                    assortmentLocationsToRemove.Add(al);
                }
            }
            this.SelectedAssortment.Locations.RemoveList(assortmentLocationsToRemove);


            //Add in the new ones that are not already in the list
            foreach (Location locationInfo in this.AddedLocations)
            {
                if (!currentLocationIds.Contains(locationInfo.Id))
                {
                    AssortmentLocation al = AssortmentLocation.NewAssortmentLocation(locationInfo);
                    this.SelectedAssortment.Locations.Add(al);
                }
            }

            base.ShowWaitCursor(false);

            //close the window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the create database operation
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute(), false)
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Cancel_CanExecute()
        {
            return true;
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Event handlers

        /// <summary>
        /// Responds to a change of selected assortment
        /// </summary>
        private void OnSelectedAssortmentChanged()
        {
            ResetLocationsLists();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Resets the added locaiotns list to hold the ones already in the current project content's 
        /// locations list - all newly added items not saved will be removed
        /// </summary>
        private void ResetLocationsLists()
        {
            if (_selectedAvailableLocations.Count > 0) { _selectedAvailableLocations.Clear(); }
            if (_availableLocations.Count > 0) { _availableLocations.Clear(); }
            if (_selectedLocationsToRemove.Count > 0) { _selectedLocationsToRemove.Clear(); }
            if (_addedLocations.Count > 0) { _addedLocations.Clear(); }


            List<Int16> currentLocationIds = this.SelectedAssortment.Locations.Select(l => l.LocationId).ToList();

            //cycle through locations adding to the correct list
            foreach (Location loc in _masterLocationList)
            {
                if (currentLocationIds.Contains(loc.Id))
                {
                    _addedLocations.Add(loc);
                }
                else
                {
                    _availableLocations.Add(loc);
                }
            }

        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterLocationList = null;
                    _addedLocations.Clear();
                    _availableLocations.Clear();
                    _selectedAvailableLocations.Clear();
                    _selectedLocationsToRemove.Clear();
                    _selectedAssortment = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
