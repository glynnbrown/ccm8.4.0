﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011
#region Version History: (CCM 8.0)
// V8-25454 : J.Pickup 
//  Created (Copied over from GFS)
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
#endregion
#region Version History: CCM830
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
// V8-32718 : A.Probyn ~ Updated so full product attributes can be displayed.
#endregion
#endregion

using System;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Represents a row on the product rules screen
    /// </summary>
    public sealed class AssortmentSetupProductRuleRow : ViewModelObject
    {
        #region Fields
        private Assortment _assortment;
        private AssortmentProduct _assortmentProduct;
        private Product _sourceProduct;
        private AssortmentProduct _existingProduct;
        private Boolean _isDirty;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AssortmentProductProperty = WpfHelper.GetPropertyPath<AssortmentSetupProductRuleRow>(p => p.AssortmentProduct);
        public static readonly PropertyPath ProductGTINProperty = WpfHelper.GetPropertyPath<AssortmentSetupProductRuleRow>(p => p.ProductGTIN);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<AssortmentSetupProductRuleRow>(p => p.ProductName);
        public static readonly PropertyPath RuleSummaryProperty = WpfHelper.GetPropertyPath<AssortmentSetupProductRuleRow>(p => p.RuleSummary);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentSetupProductRuleRow>(p => p.IsDirty);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the product this row relates to
        /// </summary>
        public Assortment SelectedAssortment
        {
            get { return _assortment; }
        }

        /// <summary>
        /// Gets the product this row relates to
        /// </summary>
        public AssortmentProduct AssortmentProduct
        {
            get { return _assortmentProduct; }
        }

        /// <summary>
        /// Gets the source product this row relates to
        /// </summary>
        public Product SourceProduct
        {
            get { return _sourceProduct; }
        }

        /// <summary>
        /// Gets the Gtin of the product this row relates to
        /// </summary>
        public String ProductGTIN
        {
            get { return _assortmentProduct.Gtin; }
        }

        /// <summary>
        /// Gets the Name of the product this row relates to
        /// </summary>
        public String ProductName
        {
            get { return _assortmentProduct.Name; }
        }

        /// <summary>
        /// Gets a summary of the current rule
        /// </summary>
        public String RuleSummary
        {
            get
            {
                if (this.IsNullRule)
                {
                    return String.Empty;
                }
                else
                {
                    return _assortmentProduct.RuleSummary;
                }
            }
        }

        /// <summary>
        /// Returns true if the rule has no values.
        /// </summary>
        public Boolean IsNullRule
        {
            get
            {
                if (_assortmentProduct == null) { return true; }
                return (_assortmentProduct.RuleType == PlanogramAssortmentProductRuleType.None && _assortmentProduct.HasMaximum == false);
            }
        }

        /// <summary>
        /// Returns true if the row is dirty
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        public Boolean HasMaximum
        {
            get { return _assortmentProduct.HasMaximum; }
        }
        #endregion

        #region Constructor

        public AssortmentSetupProductRuleRow(Assortment assortment, AssortmentProduct assortmentProduct, Product sourceProduct)
        {
            _assortment = assortment;
            _existingProduct = assortmentProduct;
            _sourceProduct = sourceProduct;

            //Take copy for custom begin edit
            _assortmentProduct = assortmentProduct.Clone();
            _assortmentProduct.PropertyChanged += Product_PropertyChanged;

            _isDirty = false;
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes to the current rule
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Product_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            OnPropertyChanged(RuleSummaryProperty);
            this.IsDirty = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Commits any changes made back to the given node
        /// </summary>
        /// <param name="applyToRange"></param>
        public void CommitChanges()
        {
            if (this.IsDirty)
            {
                AssortmentProduct existingRule = _existingProduct;
                AssortmentProduct editRule = _assortmentProduct;

                //copy values
                existingRule.ExactListFacings = editRule.ExactListFacings;
                existingRule.ExactListUnits = editRule.ExactListUnits;
                existingRule.PreserveListFacings = editRule.PreserveListFacings;
                existingRule.PreserveListUnits = editRule.PreserveListUnits;
                existingRule.MinListFacings = editRule.MinListFacings;
                existingRule.MinListUnits = editRule.MinListUnits;
                existingRule.MaxListFacings = editRule.MaxListFacings;
                existingRule.MaxListUnits = editRule.MaxListUnits;
                existingRule.ProductTreatmentType = editRule.ProductTreatmentType;

                this.IsDirty = false;
            }
        }

        /// <summary>
        /// Clears off this rule
        /// </summary>
        public void ClearRule()
        {
            _assortmentProduct.ClearRule();
            this.IsDirty = true;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _assortmentProduct.PropertyChanged -= Product_PropertyChanged;

                if (disposing)
                {
                    _assortment = null;
                    _existingProduct = null;
                    _assortmentProduct = null;
                }
            }

        }

        #endregion
    }
}
