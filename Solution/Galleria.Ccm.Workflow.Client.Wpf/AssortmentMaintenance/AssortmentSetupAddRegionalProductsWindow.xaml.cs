﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//      Created
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using System.ComponentModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    /// <summary>
    /// Interaction logic for AssortmentSetupAddRegionalProductsWindow.xaml
    /// </summary>
    public partial class AssortmentSetupAddRegionalProductsWindow : ExtendedRibbonWindow
    {
        const String _removeRuleCommandKey = "RegionProductRemoveRegionalProductCommand";
        const String _selectRegionProductCommandKey = "RegionProductSelectRegionProductCommand";

        #region Properties

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentSetupAddRegionalProductsViewModel), typeof(AssortmentSetupAddRegionalProductsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public AssortmentSetupAddRegionalProductsViewModel ViewModel
        {
            get { return (AssortmentSetupAddRegionalProductsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentSetupAddRegionalProductsWindow senderControl = (AssortmentSetupAddRegionalProductsWindow)obj;

            if (e.OldValue != null)
            {
                AssortmentSetupAddRegionalProductsViewModel oldModel = (AssortmentSetupAddRegionalProductsViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(_selectRegionProductCommandKey);
                senderControl.Resources.Remove(_removeRuleCommandKey);
            }

            if (e.NewValue != null)
            {
                AssortmentSetupAddRegionalProductsViewModel newModel = (AssortmentSetupAddRegionalProductsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(_selectRegionProductCommandKey, newModel.SelectRegionalProductCommand);
                senderControl.Resources.Add(_removeRuleCommandKey, newModel.RemoveRegionalProductCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public AssortmentSetupAddRegionalProductsWindow(AssortmentSetupAddRegionalProductsViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;
            this.Loaded += RegionProductWindow_Loaded;
        }

        /// <summary>
        /// Carries out initial first load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RegionProductWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= RegionProductWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close


        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            if (!this.ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }

            base.OnCrossCloseRequested(e);
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {
                   IDisposable disposableViewModel = this.ViewModel;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}