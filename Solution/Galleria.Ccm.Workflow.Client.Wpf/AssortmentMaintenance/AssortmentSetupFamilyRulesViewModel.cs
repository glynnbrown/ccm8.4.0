﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-26491 : A.Kuszyk
//  Localised AddSelectedProductsCommand Disabled Reason.
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32396 : A.Probyn
//  Updated for delist family rule
// V8-32718 : A.Probyn
//  Updated so full product attributes can be displayed.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMaintenance.AssortmentSetup
{
    public class AssortmentSetupFamilyRulesViewModel
        : ViewModelAttachedControlObject<AssortmentSetupFamilyRulesWindow>, IDataErrorInfo
    {
        #region Fields

        // The selected assortment
        private Assortment _selectedAssortment;

        private Dictionary<Int32, Product> _fullProductLookup;

        //The list of current family rules for the assortment
        private BulkObservableCollection<AssortmentFamilyRuleRowViewModel> _currentAssortmentFamilyRules = new BulkObservableCollection<AssortmentFamilyRuleRowViewModel>();
        private ReadOnlyBulkObservableCollection<AssortmentFamilyRuleRowViewModel> _currentAssortmentFamilyRulesRO;

        //The current Assortment Region being edited
        private AssortmentFamilyRuleRowViewModel _currentAssortmentFamilyRule;

        //The list of available products for the family rule to choose from 
        private BulkObservableCollection<AssortmentProductView> _availableProducts = new BulkObservableCollection<AssortmentProductView>();
        private ReadOnlyBulkObservableCollection<AssortmentProductView> _availableProductsRO;
        //The currently selected product(s) in the available products list
        private ObservableCollection<AssortmentProductView> _selectedAvailableProducts = new ObservableCollection<AssortmentProductView>();

        //The list of taken assortment products already part of the family
        private BulkObservableCollection<AssortmentProductView> _assignedProducts = new BulkObservableCollection<AssortmentProductView>();
        private ReadOnlyBulkObservableCollection<AssortmentProductView> _assignedProductsRO;
        //The currently selected product(s) in the taken products list
        private ObservableCollection<AssortmentProductView> _selectedAssignedProducts = new ObservableCollection<AssortmentProductView>();

        private String _selectedFamilyRuleName = String.Empty; // the family rule name
        private PlanogramAssortmentProductFamilyRuleType _selectedFamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None; // the family rule type
        private Byte? _selectedFamilyRuleValue = null; // the family rule value

        private List<AssortmentFamilyRuleRowViewModel> _deletedFamilyRuleRows = new List<AssortmentFamilyRuleRowViewModel>();

        #endregion

        #region Binding properties

        public static readonly PropertyPath SelectedAssortmentProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.SelectedAssortment);
        public static readonly PropertyPath CurrentAssortmentFamilyRulesProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.CurrentAssortmentFamilyRules);
        public static readonly PropertyPath CurrentAssortmentFamilyRuleProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.CurrentAssortmentFamilyRule);
        public static readonly PropertyPath AvailableProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.AvailableProducts);
        public static readonly PropertyPath SelectedAvailableProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.SelectedAvailableProducts);
        public static readonly PropertyPath TakenProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.AssignedProducts);
        public static readonly PropertyPath SelectedTakenProductsProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.SelectedAssignedProducts);
        public static readonly PropertyPath SelectedFamilyRuleNameProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.SelectedFamilyRuleName);
        public static readonly PropertyPath SelectedFamilyRuleTypeProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.SelectedFamilyRuleType);
        public static readonly PropertyPath SelectedFamilyRuleValueProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.SelectedFamilyRuleValue);
        public static readonly PropertyPath ValidFamilyRuleValueProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.ValidFamilyRuleValue);

        //commands
        public static readonly PropertyPath AddFamilyRuleCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.AddFamilyRuleCommand);
        public static readonly PropertyPath RemoveCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.RemoveCommand);
        public static readonly PropertyPath CancelCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath ApplyCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.ApplyCommand);
        public static readonly PropertyPath ApplyAndCloseCommandProperty =
            WpfHelper.GetPropertyPath<AssortmentSetupFamilyRulesViewModel>(p => p.ApplyAndCloseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Property for the selected Assortment
        /// </summary>
        public Assortment SelectedAssortment
        {
            get { return _selectedAssortment; }
            set
            {
                _selectedAssortment = value;
                OnPropertyChanged(SelectedAssortmentProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the family rule name
        /// </summary>
        public String SelectedFamilyRuleName
        {
            get { return _selectedFamilyRuleName; }
            set
            {
                _selectedFamilyRuleName = value;
                if (this.CurrentAssortmentFamilyRule != null)
                {
                    this.CurrentAssortmentFamilyRule.FamilyRuleName = _selectedFamilyRuleName;
                }
                OnPropertyChanged(SelectedFamilyRuleNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the family rule Type
        /// </summary>
        public PlanogramAssortmentProductFamilyRuleType SelectedFamilyRuleType
        {
            get { return _selectedFamilyRuleType; }
            set
            {
                _selectedFamilyRuleType = value;
                if (this.CurrentAssortmentFamilyRule != null)
                {
                    this.CurrentAssortmentFamilyRule.FamilyRuleType = _selectedFamilyRuleType;
                }
                OnPropertyChanged(SelectedFamilyRuleTypeProperty);
                OnPropertyChanged(SelectedFamilyRuleValueProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the family rule value
        /// </summary>
        public Byte? SelectedFamilyRuleValue
        {
            get { return _selectedFamilyRuleValue; }
            set
            {
                _selectedFamilyRuleValue = value;
                if (this.CurrentAssortmentFamilyRule != null)
                {
                    this.CurrentAssortmentFamilyRule.FamilyRuleValue = _selectedFamilyRuleValue;
                }
                OnPropertyChanged(SelectedFamilyRuleValueProperty);
            }
        }

        /// <summary>
        /// Returns the readonly collection of familys applied within the assortment
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentFamilyRuleRowViewModel> CurrentAssortmentFamilyRules
        {
            get
            {
                if (_currentAssortmentFamilyRulesRO == null)
                {
                    _currentAssortmentFamilyRulesRO = new ReadOnlyBulkObservableCollection<AssortmentFamilyRuleRowViewModel>(_currentAssortmentFamilyRules);
                }
                return _currentAssortmentFamilyRulesRO;
            }
        }

        /// <summary>
        /// The currently selected family rule
        /// </summary>
        public AssortmentFamilyRuleRowViewModel CurrentAssortmentFamilyRule
        {
            get { return _currentAssortmentFamilyRule; }
            set
            {
                base.ShowWaitCursor(true);

                _currentAssortmentFamilyRule = value;
                LoadAvailableAndAssignedProducts();
                _selectedFamilyRuleType = _currentAssortmentFamilyRule == null ? PlanogramAssortmentProductFamilyRuleType.None : _currentAssortmentFamilyRule.FamilyRuleType;
                _selectedFamilyRuleName = _currentAssortmentFamilyRule == null ? String.Empty : _currentAssortmentFamilyRule.FamilyRuleName;
                _selectedFamilyRuleValue = _currentAssortmentFamilyRule == null ? null : _currentAssortmentFamilyRule.FamilyRuleValue;
                OnPropertyChanged(CurrentAssortmentFamilyRuleProperty);
                OnPropertyChanged(SelectedFamilyRuleTypeProperty);
                OnPropertyChanged(SelectedFamilyRuleValueProperty);

                base.ShowWaitCursor(false);
            }
        }

        /// <summary>
        /// Returns the readonly collection of products still available for the family
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentProductView> AvailableProducts
        {
            get
            {
                if (_availableProductsRO == null)
                {
                    _availableProductsRO = new ReadOnlyBulkObservableCollection<AssortmentProductView>(_availableProducts);
                }
                return _availableProductsRO;
            }
        }


        /// <summary>
        /// Returns the readonly collection of products already added to the family
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentProductView> AssignedProducts
        {
            get
            {
                if (_assignedProductsRO == null)
                {
                    _assignedProductsRO = new ReadOnlyBulkObservableCollection<AssortmentProductView>(_assignedProducts);
                }
                return _assignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected available products
        /// </summary>
        public ObservableCollection<AssortmentProductView> SelectedAvailableProducts
        {
            get { return _selectedAvailableProducts; }
        }

        /// <summary>
        /// Returns the collection of selected available products
        /// </summary>
        public ObservableCollection<AssortmentProductView> SelectedAssignedProducts
        {
            get { return _selectedAssignedProducts; }
        }

        /// <summary>
        /// Determines if the current family rule value is valid
        /// </summary>
        private Boolean ValidFamilyRuleValue
        {
            get
            {
                if (SelectedFamilyRuleType == PlanogramAssortmentProductFamilyRuleType.MinimumProductCount ||
                        SelectedFamilyRuleType == PlanogramAssortmentProductFamilyRuleType.MaximumProductCount)
                {
                    if (SelectedFamilyRuleValue == null || SelectedFamilyRuleValue <= 0 || SelectedFamilyRuleValue > this._assignedProducts.Count)
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="assortment">The assortment product</param>
        /// <param name="assortmentProduct">The assortment product containing family rule</param>
        public AssortmentSetupFamilyRulesViewModel(Assortment assortment, AssortmentProduct assortmentProduct)
        {
            //Load assortment selected in previous screen
            this.SelectedAssortment = assortment;

            //Fetch master products once for display purposes
            _fullProductLookup = ProductList.FetchByProductIds(_selectedAssortment.Products.Select(p => p.ProductId)).ToDictionary(p => p.Id);

            // Load assortments current family products
            LoadCurrentFamilyRules();

        }

        #endregion

        #region Commands

        #region AddFamilyRuleCommand

        private RelayCommand _addFamilyRuleCommand;

        /// <summary>
        /// Adds a new region to the list
        /// </summary>
        public RelayCommand AddFamilyRuleCommand
        {
            get
            {
                if (_addFamilyRuleCommand == null)
                {
                    _addFamilyRuleCommand = new RelayCommand(
                        p => AddRegion_Executed())
                    {
                        FriendlyName = Message.AssortmentSetup_FamilyRules_AddFamilyRule,
                        SmallIcon = ImageResources.Add_16,
                    };
                    base.ViewModelCommands.Add(_addFamilyRuleCommand);
                }
                return _addFamilyRuleCommand;
            }
        }

        private void AddRegion_Executed()
        {
            AssortmentFamilyRuleRowViewModel newFamily = new AssortmentFamilyRuleRowViewModel(String.Empty, PlanogramAssortmentProductFamilyRuleType.None, null, new List<AssortmentProductView>());
            _currentAssortmentFamilyRules.Add(newFamily);
            this.CurrentAssortmentFamilyRule = newFamily;
        }

        #endregion

        #region RemoveCommand

        private RelayCommand _removeCommand;

        /// <summary>
        /// Removes the current item
        /// </summary>
        public RelayCommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                {
                    _removeCommand = new RelayCommand(
                        p => Remove_Executed(p),
                        p => Remove_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        FriendlyDescription = Message.Generic_Remove_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeCommand);
                }
                return _removeCommand;
            }
        }

        private Boolean Remove_CanExecute(Object arg)
        {
            AssortmentFamilyRuleRowViewModel row = arg as AssortmentFamilyRuleRowViewModel;

            if (row == null)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            if (row.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Remove_Executed(Object arg)
        {
            AssortmentFamilyRuleRowViewModel row = arg as AssortmentFamilyRuleRowViewModel;
            if (row != null)
            {
                row.ClearRule();
                _currentAssortmentFamilyRules.Remove(row);
                _deletedFamilyRuleRows.Add(row);
            }
        }

        #endregion

        #region ApplyCommand

        private RelayCommand _applyCommand;

        /// <summary>
        /// Applies any changes made in the current item to the parent asssortment
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => Apply_Executed(),
                        p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_Apply,
                        FriendlyDescription = Message.Generic_Apply_Tooltip,
                        Icon = ImageResources.Apply_32,
                        SmallIcon = ImageResources.Apply_16
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        //[DebuggerStepThrough]
        private Boolean Apply_CanExecute()
        {
            if (this.SelectedAssortment == null)
            {
                ApplyCommand.DisabledReason = String.Empty;
                return false;
            }

            //all rows must be valid
            if (!this.CurrentAssortmentFamilyRules.All(r => r.IsValid))
            {
                return false;
            }

            //all regions must have a unique name
            if (this.CurrentAssortmentFamilyRules.Select(r => r.FamilyRuleName).Distinct().Count() != this.CurrentAssortmentFamilyRules.Count)
            {
                return false;
            }

            //at least one row must be dirty
            if (!this.CurrentAssortmentFamilyRules.Any(f => f.IsDirty))
            {
                //if no rows are dirty, check if any rows have been deleted
                if (this._deletedFamilyRuleRows.Count == 0)
                {
                    return false;
                }
                return true;
            }

            return true;
        }

        private void Apply_Executed()
        {
            ApplyCurrentItem();
        }

        private void ApplyCurrentItem()
        {
            base.ShowWaitCursor(true);

            //delete all removed rows
            foreach (AssortmentFamilyRuleRowViewModel deletedRow in _deletedFamilyRuleRows)
            {
                deletedRow.DeleteFamilyRule();
                deletedRow.Dispose();
            }
            _deletedFamilyRuleRows.Clear();

            //cycle through all rows applying changes made
            foreach (AssortmentFamilyRuleRowViewModel row in this.CurrentAssortmentFamilyRules)
            {
                row.CommitChanges();
            }

            //recreate all rows
            LoadCurrentFamilyRules();
            LoadAvailableAndAssignedProducts();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(p => ApplyAndClose_Executed(), p => Apply_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        FriendlyDescription = Message.Generic_ApplyAndClose_Tooltip,
                        SmallIcon = ImageResources.ApplyAndClose_16
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        [DebuggerStepThrough]
        private void ApplyAndClose_Executed()
        {
            this.Apply_Executed();

            //close the attached window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels changes and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        /// <summary>
        /// Adds any selected available products for the family
        /// </summary>
        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddToAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_AddSelected,
                        DisabledReason = Message.AssortmentSetup_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            if (_currentAssortmentFamilyRule == null)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_FamilyRules_NoFamilyRuleSelected;
                return false;
            }

            //must have a region
            if (_currentAssortmentFamilyRule.FamilyRuleName.Length == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_FamilyRules_FamilyRuleNameValidation;
                return false;
            }

            if (_currentAssortmentFamilyRule.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_FamilyRules_FamilyRuleTypeValidation;
                return false;
            }

            //must have locations available
            if (this.AvailableProducts.Count == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentLocations;
                return false;
            }

            //must have selected locations available
            if (this.SelectedAvailableProducts.Count == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_NoLocationsSelected;
                return false;
            }

            return true;
        }

        private void AddSelectedProducts_Executed()
        {
            //Add to the assigned Products list so that grid updated
            _assignedProducts.AddRange(_selectedAvailableProducts);
            this._currentAssortmentFamilyRule.AssignedProducts.AddRange(_selectedAvailableProducts);
            _availableProducts.RemoveRange(_selectedAvailableProducts);
            _selectedAvailableProducts.Clear();
            OnPropertyChanged(SelectedFamilyRuleValueProperty);
        }

        #endregion

        #region AddAllProductsCommand

        private RelayCommand _addAllProductsCommand;

        /// <summary>
        /// Adds all available products for the family
        /// </summary>
        public RelayCommand AddAllProductsCommand
        {
            get
            {
                if (_addAllProductsCommand == null)
                {
                    _addAllProductsCommand = new RelayCommand(
                        p => AddAllProducts_Executed(),
                        p => AddAllProducts_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_AddAllToAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_AddAll,
                        DisabledReason = Message.AssortmentSetup_NoLocationsToAdd
                    };
                    base.ViewModelCommands.Add(_addAllProductsCommand);

                }
                return _addAllProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool AddAllProducts_CanExecute()
        {
            if (_currentAssortmentFamilyRule == null)
            {
                this.AddSelectedProductsCommand.DisabledReason = "No family Rule selected";
                return false;
            }

            //must have a region
            if (_currentAssortmentFamilyRule.FamilyRuleName.Length == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_FamilyRules_FamilyRuleNameValidation;
                return false;
            }

            if (_currentAssortmentFamilyRule.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_FamilyRules_FamilyRuleTypeValidation;
                return false;
            }

            //must have locations available
            if (this.AvailableProducts.Count == 0)
            {
                this.AddAllProductsCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentLocations;
                return false;
            }

            return true;
        }

        private void AddAllProducts_Executed()
        {
            //Add to the taken Products list so that grid updated
            _assignedProducts.AddRange(_availableProducts);
            _currentAssortmentFamilyRule.AssignedProducts.AddRange(_availableProducts);
            _availableProducts.Clear();
            _selectedAvailableProducts.Clear();
            OnPropertyChanged(SelectedFamilyRuleValueProperty);
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        /// <summary>
        /// Removes the selected products for the family rule
        /// </summary>
        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveFromAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_RemoveSelected,
                        DisabledReason = Message.AssortmentSetup_NoLocationsSelected
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RemoveSelectedProducts_CanExecute()
        {
            if (_currentAssortmentFamilyRule == null)
            {
                this.AddSelectedProductsCommand.DisabledReason = "No family Rule selected";
                return false;
            }

            //must have a region
            if (_currentAssortmentFamilyRule.FamilyRuleName.Length == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_FamilyRules_FamilyRuleNameValidation;
                return false;
            }

            if (_selectedAssignedProducts.Count == 0)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_NoLocationsSelected;
                return false;
            }
            else
            {
                this.RemoveSelectedProductsCommand.DisabledReason = String.Empty;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            //Remove from taken Locations list so that grid updated
            _availableProducts.AddRange(this.SelectedAssignedProducts);
            _currentAssortmentFamilyRule.AssignedProducts.RemoveRange(this.SelectedAssignedProducts);
            _assignedProducts.RemoveRange(this.SelectedAssignedProducts);
            this.SelectedAssignedProducts.Clear();
            OnPropertyChanged(SelectedFamilyRuleValueProperty);
        }

        #endregion

        #region RemoveAllProductsCommand

        private RelayCommand _removeAllProductsCommand;

        /// <summary>
        /// Removes all products from the current family
        /// </summary>
        public RelayCommand RemoveAllProductsCommand
        {
            get
            {
                if (_removeAllProductsCommand == null)
                {
                    _removeAllProductsCommand = new RelayCommand(
                        p => RemoveAllProducts_Executed(),
                        p => RemoveAllProducts_CanExecute())
                    {
                        FriendlyDescription = Message.Generic_RemoveAllFromAssigned,
                        SmallIcon = ImageResources.AssortmentSetup_RemoveAll,
                        DisabledReason = Message.AssortmentSetup_NoLocationsToRemove
                    };
                    base.ViewModelCommands.Add(_removeAllProductsCommand);
                }
                return _removeAllProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RemoveAllProducts_CanExecute()
        {
            if (_currentAssortmentFamilyRule == null)
            {
                this.AddSelectedProductsCommand.DisabledReason = "No family Rule selected";
                return false;
            }

            //must have a region
            if (_currentAssortmentFamilyRule.FamilyRuleName.Length == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.AssortmentSetup_FamilyRules_FamilyRuleNameValidation;
                return false;
            }

            //must have products
            if (this._assignedProducts.Count == 0)
            {
                this.RemoveAllProductsCommand.DisabledReason = Message.AssortmentSetup_NoLocationsToRemove;
                return false;
            }

            return true;
        }

        private void RemoveAllProducts_Executed()
        {
            _availableProducts.AddRange(_assignedProducts);
            _assignedProducts.Clear();
            _currentAssortmentFamilyRule.AssignedProducts.Clear();
            this.SelectedAssignedProducts.Clear();
            OnPropertyChanged(SelectedFamilyRuleValueProperty);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.CurrentAssortmentFamilyRules.Any(p => p.IsDirty))
                {
                    ChildItemChangeResult result = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithChildItemChangeDialog(this.ApplyCommand.CanExecute());

                    if (result == ChildItemChangeResult.Apply)
                    {
                        this.ApplyCommand.Execute();
                    }
                    else if (result == ChildItemChangeResult.DoNotApply)
                    {
                        //do nothing
                    }
                    else
                    {
                        continueExecute = false;
                    }

                }
            }
            return continueExecute;

        }

        /// <summary>
        /// Loads the current family products within the selected assortment
        /// </summary>
        private void LoadCurrentFamilyRules()
        {
            this._currentAssortmentFamilyRules.Clear();

            List<String> currentFamilyRule = new List<string>();

            foreach (AssortmentProduct product in this._selectedAssortment.Products.Where(p => p.FamilyRuleType != PlanogramAssortmentProductFamilyRuleType.None).ToList())
            {
                if (!currentFamilyRule.Contains(product.FamilyRuleName))
                {
                    List<AssortmentProductView> ruleAssortmentProductviews = new List<AssortmentProductView>();
                    foreach(AssortmentProduct assortmentProduct in this._selectedAssortment.Products.Where(p => p.FamilyRuleName == product.FamilyRuleName))
                    {
                        Product sourceProduct = null;
                        _fullProductLookup.TryGetValue(assortmentProduct.ProductId, out sourceProduct);

                        ruleAssortmentProductviews.Add(new AssortmentProductView(assortmentProduct, sourceProduct));
                    }

                    AssortmentFamilyRuleRowViewModel assortmentFamilyRuleRowViewModel = new AssortmentFamilyRuleRowViewModel(product.FamilyRuleName, 
                        product.FamilyRuleType, 
                        product.FamilyRuleValue,
                        ruleAssortmentProductviews);
                    _currentAssortmentFamilyRules.Add(assortmentFamilyRuleRowViewModel);
                    currentFamilyRule.Add(product.FamilyRuleName);
                }
            }
        }

        /// <summary>
        /// Load family product available \ assigned family products
        /// </summary>
        private void LoadAvailableAndAssignedProducts()
        {
            _availableProducts.Clear();
            _assignedProducts.Clear();

            foreach (AssortmentProduct availableProduct in this.SelectedAssortment.Products.Where(
                                                        p => p.FamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None).ToList())
            {
                Product sourceProduct = null;
                _fullProductLookup.TryGetValue(availableProduct.ProductId, out sourceProduct);

                _availableProducts.Add(new AssortmentProductView(availableProduct, sourceProduct));
            }

            if (_currentAssortmentFamilyRule != null)
            {
                _assignedProducts.AddRange(this._currentAssortmentFamilyRule.AssignedProducts);
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.SelectedFamilyRuleName = String.Empty;
                    _selectedAssortment = null;
                    _fullProductLookup.Clear();
                    _fullProductLookup = null;
                    _currentAssortmentFamilyRules.Clear();
                    _availableProducts.Clear();
                    _selectedAvailableProducts.Clear();
                    _assignedProducts.Clear();
                    _selectedAssignedProducts.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo Members

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                String result = null;

                //Family Rule Name
                if (columnName == SelectedFamilyRuleNameProperty.Path)
                {
                    if (String.IsNullOrWhiteSpace(SelectedFamilyRuleName))
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetup_FamilyRules_FamilyRuleNameValidation);
                    }
                }

                //Family Rule Tpe
                if (columnName == SelectedFamilyRuleTypeProperty.Path)
                {
                    if (SelectedFamilyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetup_FamilyRules_FamilyRuleTypeValidation);
                    }
                }

                //Family Rule Value
                if (columnName == SelectedFamilyRuleValueProperty.Path)
                {
                    if (!ValidFamilyRuleValue)
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.AssortmentSetup_FamilyRules_FamilyRuleValueValidation);
                    }
                }

                return result; // return result
            }
        }

        #endregion
    }

    /// <summary>
    /// Family Rule Row Wrapper Object
    /// </summary>
    public class AssortmentFamilyRuleRowViewModel : ViewModelObject
    {
        #region Field Names

        private String _familyRuleName;
        private PlanogramAssortmentProductFamilyRuleType _familyRuleType;
        private Byte? _familyRuleValue;
        private BulkObservableCollection<AssortmentProductView> _assignedProducts = new BulkObservableCollection<AssortmentProductView>();
        private List<AssortmentProductView> _originalProducts = new List<AssortmentProductView>();
        private Boolean _isDirty;

        #endregion

        #region Property Paths

        public static readonly PropertyPath FamilyRuleNameProperty = WpfHelper.GetPropertyPath<AssortmentFamilyRuleRowViewModel>(p => p.FamilyRuleName);
        public static readonly PropertyPath FamilyRuleTypeProperty = WpfHelper.GetPropertyPath<AssortmentFamilyRuleRowViewModel>(p => p.FamilyRuleType);
        public static readonly PropertyPath FamilyRuleValueProperty = WpfHelper.GetPropertyPath<AssortmentFamilyRuleRowViewModel>(p => p.FamilyRuleValue);
        public static readonly PropertyPath IsDirtyProperty = WpfHelper.GetPropertyPath<AssortmentFamilyRuleRowViewModel>(p => p.IsDirty);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the Family Rule Name
        /// </summary>
        public String FamilyRuleName
        {
            get { return _familyRuleName; }
            set
            {
                _familyRuleName = value;
                _isDirty = true;
            }
        }

        /// <summary>
        /// Gets the family Rule Type
        /// </summary>
        public PlanogramAssortmentProductFamilyRuleType FamilyRuleType
        {
            get { return _familyRuleType; }
            set
            {
                _familyRuleType = value;
                _isDirty = true;
            }
        }

        /// <summary>
        /// Gets the family Rule Value
        /// </summary>
        public Byte? FamilyRuleValue
        {
            get { return _familyRuleValue; }
            set
            {
                _familyRuleValue = value;
                _isDirty = true;
            }
        }

        public String FamilyRuleDetail
        {
            get
            {
                String value = PlanogramAssortmentProductFamilyRuleTypeHelper.FriendlyNames[_familyRuleType];

                if (_familyRuleType != PlanogramAssortmentProductFamilyRuleType.DependencyList &&
                    _familyRuleType != PlanogramAssortmentProductFamilyRuleType.Delist)
                {
                    value = String.Format("{0} {1}", value, _familyRuleValue);
                }
                return value;
            }
        }

        /// <summary>
        /// Returns the collection family rows
        /// </summary>
        public BulkObservableCollection<AssortmentProductView> AssignedProducts
        {
            get { return _assignedProducts; }
        }

        /// <summary>
        /// Returns true if this row is dirty.
        /// </summary>
        public Boolean IsDirty
        {
            get { return _isDirty; }
            private set
            {
                _isDirty = value;
                OnPropertyChanged(IsDirtyProperty);
            }
        }

        /// <summary>
        /// Determines if the current family rule value is valid
        /// </summary>
        public Boolean IsValid
        {
            get
            {
                if (this._familyRuleName.Length == 0)
                {
                    return false;
                }

                if (_assignedProducts.Count == 0)
                {
                    return false;
                }

                if (_familyRuleType == PlanogramAssortmentProductFamilyRuleType.MinimumProductCount ||
                        _familyRuleType == PlanogramAssortmentProductFamilyRuleType.MaximumProductCount)
                {
                    if (_familyRuleValue == null || _familyRuleValue <= 0 || _familyRuleValue > this._assignedProducts.Count)
                    {
                        return false;
                    }
                }
                else if (_familyRuleType == PlanogramAssortmentProductFamilyRuleType.None)
                {
                    return false;
                }

                return true;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="locationSpace"></param>
        public AssortmentFamilyRuleRowViewModel(String familyRuleName, PlanogramAssortmentProductFamilyRuleType familyRuleType, Byte? familyRuleValue,
                                                List<AssortmentProductView> assignedProducts)
        {
            //Load properties
            this._familyRuleName = familyRuleName;
            this._familyRuleType = familyRuleType;
            this._familyRuleValue = familyRuleValue;
            this._originalProducts = assignedProducts;
            this._assignedProducts.AddRange(assignedProducts);
            this._assignedProducts.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(_assignedProducts_BulkCollectionChanged);

            _isDirty = false;
        }

        void _assignedProducts_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            _isDirty = true;
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this._familyRuleName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Commits any changes made back to the given node
        /// </summary>
        /// <param name="applyToRange"></param>
        public void CommitChanges()
        {
            if (this.IsDirty)
            {
                foreach (AssortmentProductView assortmentProductView in _assignedProducts)
                {
                    if (_originalProducts.Contains(assortmentProductView))
                    {
                        _originalProducts.Remove(assortmentProductView);
                    }

                    assortmentProductView.AssortmentProduct.FamilyRuleName = this._familyRuleName;
                    assortmentProductView.AssortmentProduct.FamilyRuleType = this._familyRuleType;
                    assortmentProductView.AssortmentProduct.FamilyRuleValue = this._familyRuleValue;
                }

                foreach (AssortmentProductView assortmentProductView in _originalProducts)
                {
                    assortmentProductView.AssortmentProduct.FamilyRuleName = String.Empty;
                    assortmentProductView.AssortmentProduct.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None;
                    assortmentProductView.AssortmentProduct.FamilyRuleValue = null;
                }

                this.IsDirty = false;
            }
        }

        /// <summary>
        /// Nulls off this rule
        /// </summary>
        public void ClearRule()
        {
            this.FamilyRuleName = String.Empty;
            this.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None;
            this.FamilyRuleValue = null;
            this.IsDirty = true;
        }

        public void DeleteFamilyRule()
        {
            foreach (AssortmentProductView assortmentProductView in _originalProducts)
            {
                assortmentProductView.AssortmentProduct.FamilyRuleName = String.Empty;
                assortmentProductView.AssortmentProduct.FamilyRuleType = PlanogramAssortmentProductFamilyRuleType.None;
                assortmentProductView.AssortmentProduct.FamilyRuleValue = null;
            }
            this.IsDirty = false;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this._assignedProducts.BulkCollectionChanged -= _assignedProducts_BulkCollectionChanged;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}