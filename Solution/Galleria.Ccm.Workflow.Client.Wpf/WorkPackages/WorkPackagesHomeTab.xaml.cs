﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson ~ Created.
#endregion

#endregion


using Fluent;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages
{
    /// <summary>
    /// Interaction logic for WorkPackagesHomeTab.xaml
    /// </summary>
    public sealed partial class WorkPackagesHomeTab : RibbonTabItem
    {

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(WorkPackagesViewModel), typeof(WorkPackagesHomeTab),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets the viewmodel controller for this window.
        /// </summary>
        public WorkPackagesViewModel ViewModel
        {
            get { return (WorkPackagesViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion


        #region Constructor

        public WorkPackagesHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
