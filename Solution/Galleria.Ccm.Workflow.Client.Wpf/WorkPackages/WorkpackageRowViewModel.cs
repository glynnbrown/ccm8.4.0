﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-27411 : M.Pettit
//  Created.
// V8-28293/V8-28247 : A.Kuszyk
//  Updated estimated time remaining to reduce time as progress is made and give estimates in minutes/hours/days.
#endregion
#region Version History: CCM803
// V8-29590 : A.Probyn
//  Extended workpackages tooltip for planogram status.
#endregion
#region Version History: CCM810
// V8-29590 : A.Probyn
//  Updated tooltip layouts
// V8-30177 : N.Foster
//  Correctly calculate time remaining
#endregion
#region Version History: CCM810
// V8-30880 : L.Luong
//  Pending and Queued now are joined as Queued
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages
{
    public sealed class WorkpackageRow : INotifyPropertyChanged
    {
        #region Fields

        private WorkpackageInfo _info;

        private ImageSource _statusIcon = null;
        private String _statusText = String.Empty;
        private String _statusEstimatedTimeLeft = String.Empty;
        private String _statusToolTip = String.Empty;
        private String _planogramErrorStatusToolTip = String.Empty;
        private WorkpackageRowStatusType _workpackageStatusType;

        #endregion

        #region Property Paths

        public static readonly PropertyPath InfoProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.Info);
        public static readonly PropertyPath WorkpackageStatusTypeProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.WorkpackageStatusType);
        public static readonly PropertyPath StatusIconProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.StatusIcon);
        public static readonly PropertyPath StatusTextProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.StatusText);
        public static readonly PropertyPath StatusToolTipProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.StatusToolTip);
        public static readonly PropertyPath StatusEstimatedTimeLeftProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.StatusEstimatedTimeLeft);
        public static readonly PropertyPath QueuedPlanCountProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.QueuedPlanCount);
        public static readonly PropertyPath CompletedPlanCountProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.CompletedPlanCount);
        public static readonly PropertyPath PlanogramErrorStatusToolTipProperty = WpfHelper.GetPropertyPath<WorkpackageRow>(o => o.PlanogramErrorStatusToolTip);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the actual info held by this row.
        /// </summary>
        public WorkpackageInfo Info
        {
            get { return _info; }
            private set
            {
                _info = value;
                OnPropertyChanged(InfoProperty);
            }
        }

        public WorkpackageRowStatusType WorkpackageStatusType
        {
            get { return _workpackageStatusType; }
        }

        public ImageSource StatusIcon
        {
            get { return _statusIcon; }
        }

        public String StatusText
        {
            get { return _statusText; }
        }

        public String StatusEstimatedTimeLeft
        {
            get { return _statusEstimatedTimeLeft; }
        }

        public String StatusToolTip
        {
            get { return _statusToolTip; }
        }

        public String PlanogramErrorStatusToolTip
        {
            get { return _planogramErrorStatusToolTip; }
        }

        public Int32 QueuedPlanCount
        {
            get
            {
                if (this.Info == null) return 0;
                return this.Info.PlanogramQueuedCount + this.Info.PlanogramPendingCount;
            }
        }

        public Int32 CompletedPlanCount
        {
            get
            {
                if (this.Info == null) return 0;
                return this.Info.PlanogramCompleteCount + this.Info.PlanogramFailedCount;


                //if (this.Info.PlanogramCount == 0) return 0;
                //if (this.Info.ProgressMax == 0)
                //{
                //    if (this.Info.ProcessingStatus == ProcessingStatus.Complete
                //        || this.Info.ProcessingStatus == ProcessingStatus.Failed)
                //    {
                //        return this.Info.PlanogramCount;
                //    }
                //    return 0;
                //}
                //if (this.Info.ProgressCurrent == 0) return 0;

                //Int32 progressPerPlan = this.Info.ProgressMax / this.Info.PlanogramCount;
                //if (progressPerPlan == 0) return 0;

                //Int32 plansComplete = this.Info.ProgressCurrent / progressPerPlan;
                //return plansComplete;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkpackageRow(WorkpackageInfo info)
        {
            _info = info;

            //Set the automation status icon, text and tooltip
            SetStatusProperties(_info);
        }

        #endregion

        #region Methods

        public void UpdateInfo(WorkpackageInfo newInfo)
        {
            Debug.Assert(newInfo.Id == Info.Id, "Different workpackage id!");
            this.Info = newInfo;
            SetStatusProperties(_info);
            
            OnPropertyChanged(StatusIconProperty);
            OnPropertyChanged(StatusTextProperty);
            OnPropertyChanged(StatusToolTipProperty);
            OnPropertyChanged(StatusEstimatedTimeLeftProperty);
            OnPropertyChanged(PlanogramErrorStatusToolTipProperty);
            OnPropertyChanged(QueuedPlanCountProperty);
            OnPropertyChanged(CompletedPlanCountProperty);
        }

        private void SetStatusProperties(WorkpackageInfo info)
        {
            WorkpackageRowStatusType statusType = WorkpackageRowStatusType.Incomplete;
            ImageSource statusIcon = null;
            String statusText = String.Empty;
            String statusToolTip = String.Empty;
            String estimatedTimeRemaining = String.Empty;

            switch (info.ProcessingStatus)
            {
                #region Pending
                case WorkpackageProcessingStatusType.Pending:
                    statusType = WorkpackageRowStatusType.Incomplete;
                    statusIcon = null;
                    statusText = Message.Workpackages_StatusPending_Text;
                    statusToolTip = Message.Workpackages_StatusPending_ToolTip;
                    estimatedTimeRemaining = String.Empty;
                    break;
                #endregion

                #region Queued
                case WorkpackageProcessingStatusType.Queued:
                    statusType = WorkpackageRowStatusType.Incomplete;
                    statusIcon = null;
                    statusText = Message.Workpackages_StatusPending_Text; //WorkpackageProcessingStatusTypeHelper.FriendlyNames[info.ProcessingStatus];
                    statusToolTip = Message.Workpackages_StatusPending_ToolTip; //Message.Workpackages_StatusQueued_ToolTip;
                    estimatedTimeRemaining = String.Empty;
                    break;
                #endregion

                #region Processing
                case WorkpackageProcessingStatusType.Processing:
                    statusType = WorkpackageRowStatusType.Incomplete;
                    statusIcon = null;
                    statusText = WorkpackageProcessingStatusTypeHelper.FriendlyNames[info.ProcessingStatus];

                    //Tooltip Line 1:
                    String minsRemaining = CalculateProcessMinutesRemaining(info, out estimatedTimeRemaining);
                    String msgLine1 = String.Format(Message.Workpackages_StatusProcessing_ToolTipLine1, minsRemaining);

                    //Tooltip Line 2:
                    String msgLine2 = String.Empty; 
                    if (info.ProgressDateStarted != null)
                    {
                        //This job was started on {0}.
                        msgLine2 = String.Format(
                                    Message.Workpackages_StatusProcessing_ToolTipLine2,
                                    ((DateTime)info.ProgressDateStarted).ToString("F"));
                    }
                    else
                    {
                        //This job's start date has not yet been set.
                        msgLine2 = Message.Workpackages_StatusProcessingToolTipLine2UnknownDate;
                    }

                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine(msgLine1);
                    builder.Append(msgLine2);

                    statusToolTip = builder.ToString();
                    break;
                #endregion

                #region Complete
                case WorkpackageProcessingStatusType.Complete:
                    statusType = WorkpackageRowStatusType.Complete;
                    statusIcon = WorkpackageRowStatusTypeHelper.FriendlyIcons[statusType];
                     statusText = WorkpackageRowStatusTypeHelper.FriendlyNames[statusType];
                    estimatedTimeRemaining = String.Empty;
                    statusToolTip = GetPlanogramStatusString(info);
                    break;
                #endregion

                #region Complete with some Failed
                case WorkpackageProcessingStatusType.CompleteWithSomeFailed:
                    statusType = WorkpackageRowStatusType.CompleteWithFailures;
                    statusIcon = WorkpackageRowStatusTypeHelper.FriendlyIcons[statusType];
                     statusText = WorkpackageRowStatusTypeHelper.FriendlyNames[statusType];
                    estimatedTimeRemaining = String.Empty;
                    statusToolTip = GetPlanogramStatusString(info);
                    break;
                #endregion

                #region Complete with all warnings
                case WorkpackageProcessingStatusType.CompleteWithAllWarnings:
                    statusType = WorkpackageRowStatusType.CompleteWithAllWarnings;
                    statusIcon = WorkpackageRowStatusTypeHelper.FriendlyIcons[statusType];
                     statusText = WorkpackageRowStatusTypeHelper.FriendlyNames[statusType];
                    estimatedTimeRemaining = String.Empty;
                    statusToolTip = GetPlanogramStatusString(info);
                    break;
                #endregion

                #region Complete with some warnings
                case WorkpackageProcessingStatusType.CompleteWithSomeWarnings:
                    statusType = WorkpackageRowStatusType.CompleteWithSomeWarnings;
                    statusIcon = WorkpackageRowStatusTypeHelper.FriendlyIcons[statusType];
                     statusText = WorkpackageRowStatusTypeHelper.FriendlyNames[statusType];
                    estimatedTimeRemaining = String.Empty;
                    statusToolTip = GetPlanogramStatusString(info);
                    break;
                #endregion

                #region Failed
                case WorkpackageProcessingStatusType.Failed:
                    statusType = WorkpackageRowStatusType.Failed;
                    statusIcon = WorkpackageRowStatusTypeHelper.FriendlyIcons[statusType];
                    statusText = WorkpackageRowStatusTypeHelper.FriendlyNames[statusType];
                    estimatedTimeRemaining = String.Empty;
                    statusToolTip = GetPlanogramStatusString(info);
                    break;
                #endregion

            }
           

            _workpackageStatusType = statusType;
            _statusIcon = statusIcon;
            _statusText = statusText;
            _statusToolTip = statusText;
            _statusEstimatedTimeLeft = estimatedTimeRemaining;

        }

        //private static WorkpackageRowStatusType GetWorkpackageStatusType(WorkpackageInfo info)
        //{
        //    //If no failures or warnings
        //    if (info.PlanogramFailedCount == 0 && info.PlanogramWarningCount == 0)
        //    {
        //        return WorkpackageRowStatusType.Complete;
        //    }
        //    //If all plans failed
        //    else if (info.PlanogramFailedCount > 0 && info.PlanogramFailedCount == info.PlanogramCount)
        //    {
        //        return WorkpackageRowStatusType.Failed;
        //    }
        //    //If some plans, but not all
        //    else if (info.PlanogramFailedCount > 0 && info.PlanogramFailedCount < info.PlanogramCount)
        //    {
        //        return WorkpackageRowStatusType.CompleteWithFailures;
        //    }
        //    else
        //    {
        //        //Default to complete with warnings
        //        return WorkpackageRowStatusType.CompleteWithSomeWarnings;
        //    }
        //}

        /// <summary>
        /// Static method to construct the error status string
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        private static String GetPlanogramStatusString(WorkpackageInfo info)
        {
            StringBuilder builder = new StringBuilder();
            if (info.PlanogramFailedCount == 0 && info.PlanogramWarningCount == 0)
            {
                builder.AppendLine(String.Format(Message.Workpackages_StatusError_TooltipAllComplete, info.PlanogramCount));
            }
            else if (info.PlanogramWarningCount > 0 && info.PlanogramWarningCount == info.PlanogramCount)
            {
                builder.AppendLine(String.Format(Message.Workpackages_StatusError_TooltipAllWarnings, info.PlanogramCount));
            }
            else if (info.PlanogramFailedCount > 0 && info.PlanogramFailedCount == info.PlanogramCount)
            {
                builder.AppendLine(String.Format(Message.Workpackages_StatusError_TooltipAllFailed, info.PlanogramCount));
            }
            else
            {
                if (info.PlanogramCompleteCount > 0)
                {
                    builder.AppendLine(String.Format(Message.Workpackages_StatusError_TooltipComplete, info.PlanogramCompleteCount));
                }
                if (info.PlanogramWarningCount > 0)
                {
                    builder.AppendLine(String.Format(Message.Workpackages_StatusError_TooltipWarnings, info.PlanogramWarningCount));
                }
                if (info.PlanogramFailedCount > 0)
                {
                    builder.AppendLine(String.Format(Message.Workpackages_StatusError_TooltipFailed, info.PlanogramFailedCount));
                }
            }

            return builder.ToString().TrimEnd('\r', '\n');
        }

        private static String CalculateProcessMinutesRemaining(WorkpackageInfo info, out String estimatedTimeRemaining)
        {
            estimatedTimeRemaining = String.Empty; 
            
            try
            {
                if (info.ProgressDateStarted != null && info.ProgressDateLastUpdated != null &&
                info.ProgressMax > 0)
                {
                    //We can only calculate the remaining time if some progress has already been made
                    if (info.ProgressCurrent > 0 && (DateTime)info.ProgressDateLastUpdated > (DateTime)info.ProgressDateStarted)
                    {
                        //have to use utcnow so that a stuck job's processing time increases
                        Double minutesSoFar = (DateTime.UtcNow).Subtract((DateTime)info.ProgressDateStarted).TotalMinutes;
                        Double minsRemaining = minutesSoFar * (((Double)info.ProgressMax / (Double)info.ProgressCurrent) -1);

                        if (minsRemaining < 1)
                        {
                            // return "less than 1 minute"
                            estimatedTimeRemaining = "< 1m";
                            return Message.Workpackages_Status_ProcessingLessThanOneMinute;
                        }
                        else if (minsRemaining < 1.5 && minsRemaining > 1)
                        {
                            // return "1 minute"
                            estimatedTimeRemaining = "1m";
                            return String.Format("1 {0}", Message.Workpackages_Status_ProcessingMinute);
                        }
                        else if (minsRemaining >= 60 && minsRemaining < 1440)
                        {
                            // return number of hours to 1 d.p.
                            var numberOfHours = Math.Round(minsRemaining / 60f, 1);
                            estimatedTimeRemaining = String.Format("{0}h", numberOfHours);
                            return String.Format("{0} {1}", numberOfHours, Message.Workpackage_Status_ProcessingHours);
                        }
                        else if (minsRemaining >= 1440)
                        {
                            // return number of days to 1 d.p.
                            var numberOfDays = Math.Round(minsRemaining / 1440f, 1);
                            estimatedTimeRemaining = String.Format("{0}d", numberOfDays);
                            return String.Format("{0} {1}", numberOfDays, Message.Workpackage_Status_ProcessingDays);
                        }
                        else
                        {
                            // return "XX minutes"
                            estimatedTimeRemaining = String.Format("{0}m", minsRemaining.ToString("F0"));
                            return String.Format("{0} {1}", minsRemaining.ToString("F0"), Message.Workpackages_Status_ProcessingMinutes);
                        }
                    }
                }
            }
            catch
            {
                //don't cry over spilt milk
            }
            // return amibiguous "a few minutes"
            return Message.Workpackages_Status_ProcessingMinutesUnknown;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));

                if (property.Path == "Info")
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(StatusIconProperty.Path));
                    PropertyChanged(this, new PropertyChangedEventArgs(StatusTextProperty.Path));
                    PropertyChanged(this, new PropertyChangedEventArgs(StatusToolTipProperty.Path));
                    PropertyChanged(this, new PropertyChangedEventArgs(StatusEstimatedTimeLeftProperty.Path));
                }
            }
        }

        #endregion
    }

    /// <summary>
    /// UI Workpackage row status enum
    /// </summary>
    public enum WorkpackageRowStatusType
    { 
        Complete,
        CompleteWithSomeWarnings,
        CompleteWithAllWarnings,
        CompleteWithFailures,
        Failed,
        Incomplete
    }

    public static class WorkpackageRowStatusTypeHelper
    {
        public static readonly Dictionary<WorkpackageRowStatusType, String> FriendlyNames =
            new Dictionary<WorkpackageRowStatusType, String>()
            {
                {WorkpackageRowStatusType.Complete, Message.Enum_WorkpackageRowStatusType_Complete},
                
                {WorkpackageRowStatusType.CompleteWithSomeWarnings, 
                    WorkpackageProcessingStatusTypeHelper.FriendlyNames[WorkpackageProcessingStatusType.CompleteWithSomeWarnings]},

                {WorkpackageRowStatusType.CompleteWithAllWarnings, 
                    WorkpackageProcessingStatusTypeHelper.FriendlyNames[WorkpackageProcessingStatusType.CompleteWithAllWarnings]},
                
                {WorkpackageRowStatusType.CompleteWithFailures, 
                     WorkpackageProcessingStatusTypeHelper.FriendlyNames[WorkpackageProcessingStatusType.CompleteWithSomeFailed]},
                
                {WorkpackageRowStatusType.Failed, Message.Enum_WorkpackageRowStatusType_Failed},
                {WorkpackageRowStatusType.Incomplete, String.Empty},
            };

        public static readonly Dictionary<WorkpackageRowStatusType, ImageSource> FriendlyIcons =
            new Dictionary<WorkpackageRowStatusType, ImageSource>()
            {
                {WorkpackageRowStatusType.Complete, Ccm.Common.Wpf.Resources.ImageResources.WorkpackageStatusComplete_16},
                {WorkpackageRowStatusType.CompleteWithAllWarnings, Ccm.Common.Wpf.Resources.ImageResources.WorkpackageStatusComplete_16}, 
                {WorkpackageRowStatusType.CompleteWithSomeWarnings, Ccm.Common.Wpf.Resources.ImageResources.WorkpackageStatusComplete_16},
                {WorkpackageRowStatusType.CompleteWithFailures, Ccm.Common.Wpf.Resources.ImageResources.WorkpackageStatusCompleteWithErrorsWarnings_16},
                {WorkpackageRowStatusType.Failed, Ccm.Common.Wpf.Resources.ImageResources.WorkpackageStatusFailed_16},
                {WorkpackageRowStatusType.Incomplete, null},
            };
    }
}
