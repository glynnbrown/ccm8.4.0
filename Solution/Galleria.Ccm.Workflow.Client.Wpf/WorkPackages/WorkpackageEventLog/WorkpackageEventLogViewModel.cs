﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 810)
// V8-29590 : A.Probyn ~ Created.
// V8-29861 : A.Probyn
//  ~ Turned all filters on by detail apart from Detailed
//  ~ Applied performance improvements for large numbers of planograms
#endregion

#region Version History: (CCM 8.2.0)
// V8-30432 : M.Shelley
//  Add a right-click context menu to pre-filter the workpackage event data by planogram name.
#endregion

#region Version History: (CCM 8.3.0)
// V8-32105 : L.Ineson
// Added event unsubscribes to dispose and added defensive code to async handler.
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using System.ComponentModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Collections;
using System.Collections.ObjectModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageEventLog
{
    public enum WorkpackageEventLogViewType
    { 
        Planograms,
        Workpackages
    }

    /// <summary>
    /// Viewmodel controller for the workpackage event log window.
    /// </summary>
    public sealed class WorkpackageEventLogViewModel : ViewModelAttachedControlObject<WorkpackageEventLogWindow>
    {
        #region Fields

        private Boolean? _dialogResult;
        private Workpackage _selectedWorkpackage;
        private Boolean _isErrorVisible = true;
        private Boolean _isWarningVisible = true; //Off on load by default
        private Boolean _isInformationVisible = true; //Off on load by default
        private Boolean _isDetailedVisible = false; //Off on load by default
        private Boolean _isFetchingPlanogramEventLogRows = false;
        private Boolean _isFetchingWorkpackageEventLogRows = false;
        private WorkpackageEventLogViewType _selectedViewType = WorkpackageEventLogViewType.Planograms; //default to planograms

        private Boolean _isWorkpackageFilterOutOfDate = true; //true on load
        private Boolean _isPlanogramFilterOutOfDate = true; //true on load

        private String _initialPlanFilter;  // Initial plan filter name

        //Planogram related
        private PlanogramEventLogInfoListViewModel _availablePlanogramEventLogsView = new PlanogramEventLogInfoListViewModel();
        private BulkObservableCollection<WorkpackageEventLogPlanogramItem> _availablePlanogramEventLogs = new BulkObservableCollection<WorkpackageEventLogPlanogramItem>();
        private ReadOnlyBulkObservableCollection<WorkpackageEventLogPlanogramItem> _availablePlanogramEventLogsRO;
        private ObservableCollection<WorkpackageEventLogPlanogramItem> _filteredPlanogramEventLogs = new ObservableCollection<WorkpackageEventLogPlanogramItem>();
        private WorkpackageEventLogPlanogramItem _selectedPlanogramEventLog = null;

        //Workpackage related
        private EventLogListViewModel _availableWorkpackageEventLogView = new EventLogListViewModel();
        private BulkObservableCollection<WorkpackageEventLogItem> _availableWorkpackageEventLogs = new BulkObservableCollection<WorkpackageEventLogItem>();
        private ReadOnlyBulkObservableCollection<WorkpackageEventLogItem> _availableWorkpackageEventLogsRO;
        private ObservableCollection<WorkpackageEventLogItem> _filteredWorkpackageEventLogs = new ObservableCollection<WorkpackageEventLogItem>();
        private WorkpackageEventLogItem _selectedWorkpackageEventLog = null;

        #endregion

        #region Property Path

        public static readonly PropertyPath SelectedWorkpackageProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.SelectedWorkpackage);
        public static readonly PropertyPath SelectedPlanogramEventLogProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.SelectedPlanogramEventLog);
        public static readonly PropertyPath FilteredPlanogramEventLogsProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.FilteredPlanogramEventLogs);
        public static readonly PropertyPath SelectedWorkpackageEventLogProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.SelectedWorkpackageEventLog);
        public static readonly PropertyPath FilteredWorkpackageEventLogsProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.FilteredWorkpackageEventLogs);
        public static readonly PropertyPath IsErrorVisibleProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.IsErrorVisible);
        public static readonly PropertyPath IsWarningVisibleProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.IsWarningVisible);
        public static readonly PropertyPath IsInformationVisibleProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.IsInformationVisible);
        public static readonly PropertyPath IsDetailedVisibleProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.IsDetailedVisible);
        public static readonly PropertyPath SelectedViewTypeProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.SelectedViewType);
        public static readonly PropertyPath IsFetchingPlanogramEventLogRowsProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.IsFetchingPlanogramEventLogRows);
        public static readonly PropertyPath IsFetchingWorkpackageEventLogRowsProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.IsFetchingWorkpackageEventLogRows);

        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogViewModel>(p => p.CloseCommand);
        
        #endregion

        #region Properties

        /// <summary>
        /// Returns thge selected workpackage
        /// </summary>
        public Workpackage SelectedWorkpackage
        {
            get { return _selectedWorkpackage; }
        }

        /// <summary>
        /// Get/Set the selected view type
        /// </summary>
        public WorkpackageEventLogViewType SelectedViewType
        {
            get { return _selectedViewType; }
            set
            {
                _selectedViewType = value;
                UpdateFilteredEventLogs();
                OnPropertyChanged(SelectedViewTypeProperty);
            }
        }

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Gets the list of all the selected workpackages available planogram event logs
        /// </summary>
        public ReadOnlyBulkObservableCollection<WorkpackageEventLogPlanogramItem> AvailablePlanogramEventLogs
        {
            get
            {
                if (_availablePlanogramEventLogsRO == null)
                {
                    _availablePlanogramEventLogsRO = new ReadOnlyBulkObservableCollection<WorkpackageEventLogPlanogramItem>(_availablePlanogramEventLogs);
                }
                return _availablePlanogramEventLogsRO;
            }
        }

        /// <summary>
        /// The filtered list of event logs that is displayed in the UI.
        /// </summary>
        public ObservableCollection<WorkpackageEventLogPlanogramItem> FilteredPlanogramEventLogs
        {
            get { return _filteredPlanogramEventLogs; }
            private set
            {
                _filteredPlanogramEventLogs = value;
                OnPropertyChanged(FilteredPlanogramEventLogsProperty);
            }
        }

        /// <summary>
        /// Gets/sets the selected planogram event log
        /// </summary>
        public WorkpackageEventLogPlanogramItem SelectedPlanogramEventLog
        {
            get { return _selectedPlanogramEventLog; }
            set
            {
                _selectedPlanogramEventLog = value;
                OnPropertyChanged(SelectedPlanogramEventLogProperty);
            }
        }

        /// <summary>
        /// Gets the list of all the selected workpackage event logs
        /// </summary>
        public ReadOnlyBulkObservableCollection<WorkpackageEventLogItem> AvailableWorkpackageEventLogs
        {
            get
            {
                if (_availableWorkpackageEventLogsRO == null)
                {
                    _availableWorkpackageEventLogsRO = new ReadOnlyBulkObservableCollection<WorkpackageEventLogItem>(_availableWorkpackageEventLogs);
                }
                return _availableWorkpackageEventLogsRO;
            }
        }

        /// <summary>
        /// The filtered list of event logs that is displayed in the UI.
        /// </summary>
        public ObservableCollection<WorkpackageEventLogItem> FilteredWorkpackageEventLogs
        {
            get { return _filteredWorkpackageEventLogs; }
            private set
            {
                _filteredWorkpackageEventLogs = value;
                OnPropertyChanged(FilteredWorkpackageEventLogsProperty);
            }
        }

        /// <summary>
        /// Gets/sets the selected workpackage event log
        /// </summary>
        public WorkpackageEventLogItem SelectedWorkpackageEventLog
        {
            get { return _selectedWorkpackageEventLog; }
            set
            {
                _selectedWorkpackageEventLog = value;
                OnPropertyChanged(SelectedWorkpackageEventLogProperty);
            }
        }

        /// <summary>
        /// Indicates if Error event logs are visible.
        /// </summary>
        public Boolean IsErrorVisible
        {
            get
            {
                return _isErrorVisible;
            }
            set
            {
                _isErrorVisible = value;
                SetOutOfDateFilterFlag(true);
                UpdateFilteredEventLogs();
                OnPropertyChanged(IsErrorVisibleProperty);
            }
        }

        /// <summary>
        /// Indicates if Warning event logs are visible.
        /// </summary>
        public Boolean IsWarningVisible
        {
            get
            {
                return _isWarningVisible;
            }
            set
            {
                _isWarningVisible = value;
                SetOutOfDateFilterFlag(true);
                UpdateFilteredEventLogs();
                OnPropertyChanged(IsWarningVisibleProperty);
            }
        }

        /// <summary>
        /// Indicates if Information event logs are visible.
        /// </summary>
        public Boolean IsInformationVisible
        {
            get
            {
                return _isInformationVisible;
            }
            set
            {
                _isInformationVisible = value;
                SetOutOfDateFilterFlag(true);
                UpdateFilteredEventLogs();
                OnPropertyChanged(IsInformationVisibleProperty);
            }
        }

        /// <summary>
        /// Indicates if Detailed (formally named debug) event logs are visible.
        /// </summary>
        public Boolean IsDetailedVisible
        {
            get
            {
                return _isDetailedVisible;
            }
            set
            {
                _isDetailedVisible = value;
                SetOutOfDateFilterFlag(true);
                UpdateFilteredEventLogs();
                OnPropertyChanged(IsDetailedVisibleProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether we are fetching the planogram evenet log rows
        /// </summary>
        public Boolean IsFetchingPlanogramEventLogRows
        {
            get { return _isFetchingPlanogramEventLogRows; }
            set
            {
                _isFetchingPlanogramEventLogRows = value;
                OnPropertyChanged(IsFetchingPlanogramEventLogRowsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether we are fetching the workpackage evenet log rows
        /// </summary>
        public Boolean IsFetchingWorkpackageEventLogRows
        {
            get { return _isFetchingWorkpackageEventLogRows; }
            set
            {
                _isFetchingWorkpackageEventLogRows = value;
                OnPropertyChanged(IsFetchingWorkpackageEventLogRowsProperty);
            }
        }

        public String PlanNameFilterText
        {
            get { return _initialPlanFilter; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkpackageEventLogViewModel(Workpackage workpackage, String selectedPlanName=null)
        {
            //Store workpackage
            _selectedWorkpackage = workpackage;

            //Attach to events for async load
            _availablePlanogramEventLogsView.ModelChanged += AvailablePlanogramEventLogsView_ModelChanged;
            _availableWorkpackageEventLogView.ModelChanged += AvailableWorkpackageEventLogsView_ModelChanged;
            
            //Fetch all related workpackage event logs
            _availableWorkpackageEventLogView.FetchByWorkpackageIdAsync(workpackage.Id);
            this.IsFetchingWorkpackageEventLogRows = true;

            //Fetch all related planogram event logs
            _availablePlanogramEventLogsView.FetchByWorkpackageIdAsync(workpackage.Id);
            this.IsFetchingPlanogramEventLogRows = true;

            _initialPlanFilter = selectedPlanName;
        }

        #endregion

        #region Commands
        
        #region Close Command

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the current window.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => CloseCommand_Execute())
                    {
                        FriendlyName = Message.Generic_Close
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void CloseCommand_Execute()
        {
            this.DialogResult = true;
        }

        #endregion


        #endregion

        #region Methods

        /// <summary>
        /// General update filtered event logs
        /// </summary>
        private void UpdateFilteredEventLogs()
        {
            if (this.SelectedViewType == WorkpackageEventLogViewType.Workpackages && _isWorkpackageFilterOutOfDate)
            {
                UpdateWorkpackageFilteredEventLogs();
            }
            else if(this.SelectedViewType == WorkpackageEventLogViewType.Planograms && _isPlanogramFilterOutOfDate)
            {
                UpdatePlanogramFilteredEventLogs();
            }
        }

        /// <summary>
        /// Set the out of date flags
        /// </summary>
        /// <param name="value"></param>
        private void SetOutOfDateFilterFlag(Boolean value)
        {
            _isWorkpackageFilterOutOfDate = true;
            _isPlanogramFilterOutOfDate = true;
        }

        /// <summary>
        /// Re-filters the FilteredEventLogs based on the visibility of the log types.
        /// </summary>
        private void UpdateWorkpackageFilteredEventLogs()
        {
            base.ShowWaitCursor(true);

            //Update based on current view type
            if (this.SelectedViewType == WorkpackageEventLogViewType.Workpackages)
            {
                this.IsFetchingWorkpackageEventLogRows = true;

                this.FilteredWorkpackageEventLogs.Clear();
                foreach (var eventLog in this.AvailableWorkpackageEventLogs)
                {
                    switch (eventLog.WorkpackageEventLog.EntryType)
                    {
                        case System.Diagnostics.EventLogEntryType.Error:
                            if (IsErrorVisible) this.FilteredWorkpackageEventLogs.Add(eventLog);
                            break;
                        case System.Diagnostics.EventLogEntryType.Warning:
                            if (IsWarningVisible) this.FilteredWorkpackageEventLogs.Add(eventLog);
                            break;
                        case System.Diagnostics.EventLogEntryType.Information:
                            if (IsInformationVisible) this.FilteredWorkpackageEventLogs.Add(eventLog);
                            break;
                        case System.Diagnostics.EventLogEntryType.FailureAudit:
                            if (IsDetailedVisible) this.FilteredWorkpackageEventLogs.Add(eventLog);
                            break;
                        case System.Diagnostics.EventLogEntryType.SuccessAudit:
                            if (IsDetailedVisible) this.FilteredWorkpackageEventLogs.Add(eventLog);
                            break;
                        default:
                            continue;
                    }
                }

                this.SelectedWorkpackageEventLog = this.FilteredWorkpackageEventLogs.FirstOrDefault();

                //Set fetching to false
                this.IsFetchingWorkpackageEventLogRows = false;

                //Reset out of date flag
                _isWorkpackageFilterOutOfDate = false;
            }

            base.ShowWaitCursor(false);

        }

        /// <summary>
        /// Re-filters the FilteredEventLogs based on the visibility of the log types.
        /// </summary>
        private void UpdatePlanogramFilteredEventLogs()
        {
            base.ShowWaitCursor(true);

            if (this.SelectedViewType == WorkpackageEventLogViewType.Planograms)
            {
                this.IsFetchingPlanogramEventLogRows = true;

                this.FilteredPlanogramEventLogs.Clear();
                foreach (var eventLog in this.AvailablePlanogramEventLogs)
                {
                    switch (eventLog.PlanogramEventLogInfo.EntryType)
                    {
                        case PlanogramEventLogEntryType.Error:
                            if (IsErrorVisible) FilteredPlanogramEventLogs.Add(eventLog);
                            break;
                        case PlanogramEventLogEntryType.Warning:
                            if (IsWarningVisible) FilteredPlanogramEventLogs.Add(eventLog);
                            break;
                        case PlanogramEventLogEntryType.Information:
                            if (IsInformationVisible) FilteredPlanogramEventLogs.Add(eventLog);
                            break;
                        case PlanogramEventLogEntryType.Debug:
                            if (IsDetailedVisible) FilteredPlanogramEventLogs.Add(eventLog);
                            break;
                        default:
                            continue;
                    }
                }

                this.SelectedPlanogramEventLog = this.FilteredPlanogramEventLogs.FirstOrDefault();

                //Set fetching to false
                this.IsFetchingPlanogramEventLogRows = false;

                //Reset out of date flag
                _isPlanogramFilterOutOfDate = false;
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Model changed event handler for the available planogram event logs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailablePlanogramEventLogsView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramEventLogInfoList> e)
        {
            if (IsDisposed) return;

            if (e.NewModel != null)
            {
                //Create row wrappers
                foreach (PlanogramEventLogInfo eventLog in e.NewModel)
                { 
                    _availablePlanogramEventLogs.Add(new WorkpackageEventLogPlanogramItem(eventLog));
                }

                //Update filtered records
                UpdatePlanogramFilteredEventLogs();
            }

            this.IsFetchingPlanogramEventLogRows = false;
        }

        /// <summary>
        /// Model changed event handler for the available workpackage event logs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableWorkpackageEventLogsView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<EventLogList> e)
        {
            if (e.NewModel != null)
            {
                //Create row wrappers
                foreach (EventLog eventLog in e.NewModel)
                { 
                    _availableWorkpackageEventLogs.Add(new WorkpackageEventLogItem(eventLog));
                }

                //Update filtered records
                UpdateWorkpackageFilteredEventLogs();
            }

            this.IsFetchingWorkpackageEventLogRows = false;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    _availablePlanogramEventLogsView.ModelChanged -= AvailablePlanogramEventLogsView_ModelChanged;
                    _availableWorkpackageEventLogView.ModelChanged -= AvailableWorkpackageEventLogsView_ModelChanged;

                    _availableWorkpackageEventLogs.Clear();
                    _availableWorkpackageEventLogs = null;
                    _availablePlanogramEventLogs.Clear();
                    _availablePlanogramEventLogs = null;
                    _availablePlanogramEventLogsView.Dispose();
                    _availableWorkpackageEventLogView = null;
                    _filteredPlanogramEventLogs.Clear();
                    _filteredPlanogramEventLogs = null;
                    _filteredWorkpackageEventLogs.Clear();
                    _filteredWorkpackageEventLogs = null;
                    _selectedPlanogramEventLog = null;
                    _selectedWorkpackageEventLog = null;
                }

                IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Event log planogram item
    /// </summary>
    public class WorkpackageEventLogItem : INotifyPropertyChanged
    {
        #region Fields

        private EventLog _workpackageEventLog;
        private String _entryType;

        #endregion

        #region Property Paths

        public static readonly PropertyPath EventNameProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogItem>(p => p.EventName);
        public static readonly PropertyPath EntryTypeProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogItem>(p => p.EntryType);
        public static readonly PropertyPath DescriptionProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogItem>(p => p.Description);
        public static readonly PropertyPath DateTimeProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogItem>(p => p.DateTime);
        public static readonly PropertyPath ContentProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogItem>(p => p.Content);
        
        #endregion

        #region Properties

        /// <summary>
        /// Exposes the workpackage event log
        /// </summary>
        public EventLog WorkpackageEventLog
        {
            get { return _workpackageEventLog; }
        }

        /// <summary>
        /// Returns the event log event name
        /// </summary>
        public String EventName
        {
            get { return _workpackageEventLog.EventLogName; }
        }

        /// <summary>
        /// The friendly name of the model's EntryType
        /// </summary>
        public String EntryType
        {
            get
            {
                if (_entryType == null)
                {
                    _entryType = EventLogEntryTypeHelper.FriendlyNames[_workpackageEventLog.EntryType];
                }
                return _entryType;
            }
        }

        /// <summary>
        /// The content of the event log
        /// </summary>
        public String Description
        {
            get { return WorkpackageEventLog.Description; }
        }

        /// <summary>
        /// The date and time upon which the event log was recorded.
        /// </summary>
        public DateTime DateTime
        {
            get { return WorkpackageEventLog.DateTime; }
        }

        /// <summary>
        /// The event log content.
        /// </summary>
        public String Content
        {
            get { return WorkpackageEventLog.Content; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="workpackage"></param>
        /// <param name="planogramEventLog"></param>
        public WorkpackageEventLogItem(EventLog workpackageEventLog)
        {
            _workpackageEventLog = workpackageEventLog;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }

    /// <summary>
    /// Event log planogram item
    /// </summary>
    public class WorkpackageEventLogPlanogramItem : INotifyPropertyChanged
    {
        #region Fields

        private PlanogramEventLogInfo _planogramEventLogInfo;
        private String _affectedType;
        private String _entryType;
        private String _eventType;

        #endregion

        #region Property Paths

        public static readonly PropertyPath PlanogramNameProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.PlanogramName);
        public static readonly PropertyPath ExecutionOrderProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.ExecutionOrder);
        public static readonly PropertyPath PlanogramEventLogInfoProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.PlanogramEventLogInfo);
        public static readonly PropertyPath AffectedTypeProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.AffectedType);
        public static readonly PropertyPath EntryTypeProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.EntryType);
        public static readonly PropertyPath EventTypeProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.EventType);
        public static readonly PropertyPath ContentProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.Content);
        public static readonly PropertyPath DescriptionProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.Description);
        public static readonly PropertyPath WorkpackageSourceProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.WorkpackageSource);
        public static readonly PropertyPath TaskSourceProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.TaskSource);
        public static readonly PropertyPath DateTimeProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.DateTime);
        public static readonly PropertyPath EventIdProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.EventId);
        public static readonly PropertyPath ScoreProperty = WpfHelper.GetPropertyPath<WorkpackageEventLogPlanogramItem>(p => p.Score);

        #endregion

        #region Properties
        
        /// <summary>
        /// Exposes the planogram event log
        /// </summary>
        public PlanogramEventLogInfo PlanogramEventLogInfo
        {
            get { return _planogramEventLogInfo; }
        }

        /// <summary>
        /// The Execution Order the event log represents
        /// </summary>
        public Int64 ExecutionOrder
        {
            get { return this.PlanogramEventLogInfo.ExecutionOrder; }
        }

        /// <summary>
        /// The planogram name the event log represents
        /// </summary>
        public String PlanogramName
        {
            get { return this.PlanogramEventLogInfo.PlanogramName; }
        }

        /// <summary>
        /// The friendly name of the model's AffectedType
        /// </summary>
        public String AffectedType
        {
            get
            {
                if (_affectedType == null)
                {
                    if (this.PlanogramEventLogInfo.AffectedType.HasValue)
                    {
                        _affectedType = PlanogramEventLogAffectedTypeHelper.FriendlyNames[this.PlanogramEventLogInfo.AffectedType.Value];
                    }
                    else
                    {
                        _affectedType = String.Empty;
                    }
                }
                return _affectedType;
            }
        }

        /// <summary>
        /// The friendly name of the model's EntryType
        /// </summary>
        public String EntryType
        {
            get
            {
                if (_entryType == null)
                {
                    _entryType = PlanogramEventLogEntryTypeHelper.FriendlyNames[this.PlanogramEventLogInfo.EntryType];
                }
                return _entryType;
            }
        }

        /// <summary>
        /// The friendly name of the model's EventType
        /// </summary>
        public String EventType
        {
            get
            {
                if (_eventType == null)
                {
                    _eventType = PlanogramEventLogEventTypeHelper.FriendlyNames[this.PlanogramEventLogInfo.EventType];
                }
                return _eventType;
            }
        }

        /// <summary>
        /// The content of the event log
        /// </summary>
        public String Content
        {
            get { return this.PlanogramEventLogInfo.Content; }
        }

        /// <summary>
        /// The description of the event log.
        /// </summary>
        public String Description
        {
            get { return this.PlanogramEventLogInfo.Description; }
        }

        /// <summary>
        /// The workpackage source of the event log.
        /// </summary>
        public String WorkpackageSource
        {
            get { return this.PlanogramEventLogInfo.WorkpackageSource; }
        }

        /// <summary>
        /// The task source of the event log.
        /// </summary>
        public String TaskSource
        {
            get { return this.PlanogramEventLogInfo.TaskSource; }
        }

        /// <summary>
        /// The date and time upon which the event log was recorded.
        /// </summary>
        public DateTime DateTime
        {
            get { return this.PlanogramEventLogInfo.DateTime; }
        }

        /// <summary>
        /// The EventId of the event log, relating to values stored in Galleria.Ccm.Logging.EventLogEvent
        /// </summary>
        public Int32 EventId
        {
            get { return this.PlanogramEventLogInfo.EventId; }
        }

        /// <summary>
        /// The Score of the event log, relating to values stored in Galleria.Ccm.Logging.EventLogEvent
        /// </summary>
        public Byte Score
        {
            get { return this.PlanogramEventLogInfo.Score; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="workpackage"></param>
        /// <param name="planogramEventLog"></param>
        public WorkpackageEventLogPlanogramItem(PlanogramEventLogInfo planogramEventLogInfo)
        {
            _planogramEventLogInfo = planogramEventLogInfo;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
