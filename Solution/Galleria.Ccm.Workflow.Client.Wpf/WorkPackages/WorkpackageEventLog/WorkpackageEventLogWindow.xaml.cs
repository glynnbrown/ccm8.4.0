﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//   Created (
#endregion

#region Version History: (CCM 8.2.0)
// V8-30432 : M.Shelley
//  Add a right-click context menu to pre-filter the workpackage event data by planogram name.
#endregion

#region Version History: (CCM 8.2.0)
//    CCM-18493 : L.Bailey
//      Decrease Units and Facings by Product fail
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Windows.Controls;
using Microsoft.Win32;
using System.Collections.Generic;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.IO;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageEventLog
{
    /// <summary>
    /// Interaction logic for EntitySetupBackstageOpen.xaml
    /// </summary>
    public partial class WorkpackageEventLogWindow : ExtendedRibbonWindow
    {

        #region Fields
        private ContextMenu _columnHeaderContextMenu; //the column header menu
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(WorkpackageEventLogViewModel), typeof(WorkpackageEventLogWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public WorkpackageEventLogViewModel ViewModel
        {
            get { return (WorkpackageEventLogViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WorkpackageEventLogWindow senderControl = (WorkpackageEventLogWindow)obj;

            if (e.OldValue != null)
            {
                WorkpackageEventLogViewModel oldModel = (WorkpackageEventLogViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                WorkpackageEventLogViewModel newModel = (WorkpackageEventLogViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Export to Excel Command

        private static RoutedCommand _exportGridToExcelCommand = new RoutedCommand();
        /// <summary>
        /// Returns the command to export this grid to excel
        /// </summary>
        public static RoutedCommand ExportGridToExcel
        {
            get { return _exportGridToExcelCommand; }
        }
        private static void ExportGridToExcelCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ExportDataGridToExcel((WorkpackageEventLogWindow)sender);
        }
        private static Boolean ExportGridToExcelCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            return true;
        }

        #endregion

        #region Constructor

        // Load form with filters set
        public WorkpackageEventLogWindow(WorkpackageEventLogViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Set help file key
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.WorkpackagePlanogramEventLog);

            this.ViewModel = viewModel;

            this.Loaded += new RoutedEventHandler(EventLogWindow_Loaded);

            //register the export to excel command
            CommandManager.RegisterClassCommandBinding(typeof(WorkpackageEventLogWindow),
                new CommandBinding(_exportGridToExcelCommand,
                    ExportGridToExcelCommand_Executed));

            this.xPlanogramEventLogGrid.ColumnContextMenuOpening += ColumnContextMenuOpened;

        }

        // Set up loaded event after the form is loaded
        private void EventLogWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= EventLogWindow_Loaded;

            // V8-30432 - Fetch the planogram name column and apply any filter if the user has right-clicked
            // on a planogram row in the workpackage event log window

            var planNamePath = WorkpackageEventLogPlanogramItem.PlanogramNameProperty.Path;
            var planNameColumn = xPlanogramEventLogGrid.Columns.Where(x => x.SortMemberPath == planNamePath).FirstOrDefault();
            ExtendedDataGrid.SetColumnFilterValue(planNameColumn, this.ViewModel.PlanNameFilterText);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Methods
        /// <summary>
        /// Exports the given grid to excel.
        /// </summary>
        public static void ExportDataGridToExcel(WorkpackageEventLogWindow window)
        {
            string[] stringSeparators = new string[] { "\r\n" };

            ExtendedDataGrid grid = window.xPlanogramEventLogGrid as ExtendedDataGrid;
            if (grid == null) return;

            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Excel Workbook|*.xlsx|Excel 97-2003|*.xls";

            if (saveDialog.ShowDialog() == true)
            {
                String fileName = saveDialog.FileName;

                //create a new workbook to hold the export
                Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
                Aspose.Cells.Worksheet workSheet = workbook.Worksheets[0];

                List<DataGridColumn> colList = new List<DataGridColumn>();
                foreach (DataGridColumn column in grid.Columns.OrderBy(c => c.DisplayIndex))
                {
                    //Content column is always exported as it is permanently visible
                    //in the bottom pane of the control
                    if (column.Header != Message.WorkpackageEventLog_ColumnHeader_Content)
                    {
                        if (column.Visibility != Visibility.Visible) continue;
                    }
                    if (column is DataGridTemplateColumn && column.ClipboardContentBinding == null) continue;
                    colList.Add(column);
                }


                Int32 curCol = 0;
                Int32 curRow = 0;

                //copy grid column headers
                foreach (DataGridColumn column in colList)
                {
                    workSheet.Cells[0, curCol].Value = column.Header;
                    curCol++;
                }
                curRow++;

                //is the content column included - should always be available now
                DataGridColumn contentCol = colList.FirstOrDefault(c => c.Header == Message.WorkpackageEventLog_ColumnHeader_Content);
                if (contentCol != null)
                {
                    //We have a content column included so we need to add a 
                    //row for each content item - these are CR delimited
                    Int32 contentColIndex = colList.IndexOf(contentCol);

                    foreach (var row in grid.Items)
                    {
                        String contentValue = contentCol.OnCopyingCellClipboardContent(row).ToString();

                        //split content into array of strings
                        String[] contentRows = contentValue.Split(stringSeparators, StringSplitOptions.None);

                        //loop through array, creating row for each content item
                        foreach (String contentRow in contentRows)
                        {
                            curCol = 0;

                            foreach (DataGridColumn column in colList)
                            {
                                if (colList.IndexOf(column) == contentColIndex)
                                {
                                    //use the content array value for the cell
                                    workSheet.Cells[curRow, curCol].Value = contentRow;
                                }
                                else
                                {
                                    workSheet.Cells[curRow, curCol].Value = column.OnCopyingCellClipboardContent(row);
                                }
                                curCol++;
                            }
                            curRow++;
                        }
                    }
                }
                else
                {
                    //just export all rows
                    foreach (var row in grid.Items)
                    {
                        curCol = 0;
                        foreach (DataGridColumn column in colList)
                        {
                            workSheet.Cells[curRow, curCol].Value = column.OnCopyingCellClipboardContent(row);
                            curCol++;
                        }

                        curRow++;
                    }
                }
           
                try
                {
                    //save the file.
                    workbook.Save(fileName);
                }
                catch (IOException)
                {
                    //The file is in use
                    ModalMessage dlg = new ModalMessage();
                    dlg.Header = Galleria.Framework.Controls.Wpf.Resources.Language.Generic_FileIsOpenMessageHeader;
                    dlg.Description = String.Format(Galleria.Framework.Controls.Wpf.Resources.Language.Generic_FileIsOpenMessageDescription, fileName);
                    dlg.MessageIcon = ImageResources.Warning_32;
                    dlg.ButtonCount = 1;
                    dlg.Button1Content = Galleria.Framework.Controls.Wpf.Resources.Language.Generic_Ok;
                    dlg.Owner = window;

                    dlg.ShowDialog();
                }

            }

        }

        #endregion


        #region Events

        private void ColumnContextMenuOpened(object sender, ExtendedDataGridContextMenuOpeningEventArgs e)
        {
            _columnHeaderContextMenu = e.ContextMenu as ContextMenu;
            if (_columnHeaderContextMenu == null) return;

            MenuItem exportOption = new MenuItem();
            exportOption.Header = Galleria.Framework.Controls.Wpf.Resources.Language.ExtendedDataGrid_ExportToExcel;
            exportOption.Command = _exportGridToExcelCommand; // ExtendedDataGrid.ExportGridToExcel;
            exportOption.CommandTarget = this;
            _columnHeaderContextMenu.Items.Insert(0, exportOption);

            //show the context menu
            _columnHeaderContextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
            _columnHeaderContextMenu.IsOpen = true;
            _columnHeaderContextMenu.StaysOpen = true;
        }

        /// <summary>
        /// The following Key events allow to switch between the tabs when the space bar is pressed
        /// This functionality existed using directional keys but only when the tab was in focus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xPlanogramsTab_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xWorkpackageTab);
            }
            else
            {
                return;
            }
        }
        private void xWorkpackageTab_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                Keyboard.Focus(xPlanogramsTab);
            }
            else
            {
                return;
            }
        }

        /// <summary>
        /// This allows keyboard functionality to the filter menu, the return key expands/collapses
        /// the dropdown menu, and the space-bar checks/unchecks the highlighted item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void filterDropDown_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                filterDropDown.IsDropDownOpen = !filterDropDown.IsDropDownOpen;

                //if the dropdown is open with items it selects the first one, necessary to allow
                //The directional keys to select and item
                if (filterDropDown.IsDropDownOpen && filterDropDown.HasItems)
                {
                    Keyboard.Focus((MenuItem)filterDropDown.Items[0]);
                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                foreach (MenuItem item in filterDropDown.Items)
                {
                    if (item.IsHighlighted)
                    {
                        item.IsChecked = !item.IsChecked;
                    }
                }
            }
        }

        #endregion

        #region Window close

        //on close form event
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        //After form closed
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }
        #endregion
    }
}

