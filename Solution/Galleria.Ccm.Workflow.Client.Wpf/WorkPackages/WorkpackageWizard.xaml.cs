﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Framework.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Collections.Specialized;
using Galleria.Ccm.Model;
using System.Linq;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages
{
    /// <summary>
    /// Interaction logic for WorkpackageWizard.xaml
    /// </summary>
    public sealed partial class WorkpackageWizard : ExtendedRibbonWindow
    {
        #region Constants
        const String WorkpackageDetailsStepTemplateName = "WorkpackageWizard_WorkpackageDetailsStep";
        const String SelectWorkflowStepTemplateName = "WorkpackageWizard_SelectWorkflowStep";
        const String SelectPlanogramsStepTemplateName = "WorkpackageWizard_SelectPlanogramsStep";
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(WorkpackageWizardViewModel), typeof(WorkpackageWizard),
            new PropertyMetadata(null, OnViewModelPropertyChanged));


        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WorkpackageWizard senderControl = (WorkpackageWizard)obj;

            if (e.OldValue != null)
            {
                WorkpackageWizardViewModel oldModel = (WorkpackageWizardViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                WorkpackageWizardViewModel newModel = (WorkpackageWizardViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }

            senderControl.SetContentPresenterTemplate();
        }

       
        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public WorkpackageWizardViewModel ViewModel
        {
            get { return (WorkpackageWizardViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public WorkpackageWizard(WorkpackageWizardViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;

            this.Loaded += WorkpackageWizard_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkpackageWizard_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= WorkpackageWizard_Loaded;
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }), priority: DispatcherPriority.Background);
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == WorkpackageWizardViewModel.CurrentWizardStepProperty.Path)
            {
                SetContentPresenterTemplate();
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Updates the template used by the step content presenter
        /// </summary>
        private void SetContentPresenterTemplate()
        {
            if (this.StepContentPresenter != null)
            {
                if (this.ViewModel != null)
                {
                    switch (this.ViewModel.CurrentWizardStep)
                    {
                        case WorkpackageWizardStep.WorkpackageDetail:
                            this.StepContentPresenter.ContentTemplate = this.FindResource(WorkpackageDetailsStepTemplateName) as DataTemplate;
                            break;

                        case WorkpackageWizardStep.SelectWorkflow:
                            this.StepContentPresenter.ContentTemplate = this.FindResource(SelectWorkflowStepTemplateName) as DataTemplate;
                            break;

                        case WorkpackageWizardStep.SelectPlanograms:
                            this.StepContentPresenter.ContentTemplate = this.FindResource(SelectPlanogramsStepTemplateName) as DataTemplate;
                            break;

                        default:
                            this.StepContentPresenter.ContentTemplate = null;
                            break;
                    }
                }
                else
                {
                    this.StepContentPresenter.ContentTemplate = null;
                }

            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    IDisposable disposableViewModel = (IDisposable)this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }

        }


        #endregion
    }

}
