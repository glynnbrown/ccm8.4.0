﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
// V8-26367 : A.Silva ~ Added handlers for the ContextMenuShowing events for the extended data grids.
// V8-27160 : L.Ineson ~ Moved column manager for planogram grid here and removed workpackages one completely.
// V8-27186 : A.Silva ~ Wrapped planogram list items in PlanogramRepositoryRows.
// V8-27411 : M.Pettit ~ Added AutomationStatus, Est. Time Left, Plan Status cols, renamed ValidationGroups column
// V8-27411 : M.Pettit ~ Added Planogram Owner (displaying Planogram_UserName property)
// V8-28040 : A.Probyn
//  ~ Added code to call ApplyFiltersIntoGrid after column set is applied to the grid.
//  ~ Corrected so that columns are added to ColumnSet not Columns in order for the CustomColumnLayout to work.
// V8-28083  : A.Probyn
//  Added a SortMemberPath to the template columns on creation. Without this, a filter on another column + sort on a template column
//  causes it to go horribly wrong and fall over.
// V8-28315 : D.Pleasance 
//  Added ability to filter workpackages by automation status
// V8-28457 : D.Pleasance
//  Amended PageCount calculation to round up so data is not lost within a page that isnt available for selection.
// V8-28273 : A.Probyn
//    Corrected so columns are all in visible/frozen options on grid context menu.
#endregion

#region Version History: (CCM 8.0.1)
// V8-28507 : D.Pleasance
//  Reworked paging functionality
#endregion

#region Version History: (CCM 8.1.0)
// V8-30201 : D.Pleasance 
//  Amended ProcessingStatus column width
// V8-30160 : M.Brumby
//  Prevented "Cannot change ObservableCollection during a CollectionChanged event" error some users were getting (including me)
#endregion

#region Version History: (CCM 8.1.1)
// V8-30377 : I.George 
//  Double clicking a plan now opens the plan instead of the task explorer
// V8-30278 : L.Ineson
//  Amended Plan status column so that it can be sorted & filtered
// V8-30599 : A.Probyn
//  Added missing ClipboardContentBinding to the validation and automation status planogram columns.
#endregion

#region Version History: (CCM 8.2.0)
// V8-30432 : M.Shelley
//  Add a right-click context menu to pre-filter the workpackage event data by planogram name.
//  Add a small change such that when a user selects multiple planogram rows, the event log
//  window opens unfiltered.
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion

#region Version History: CCM830
//V8-31832 : L.Ineson
//  Added support for calculated columns
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using Fluent;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Misc;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Framework.Planograms.Model;
using System.Windows.Input;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages
{
    /// <summary>
    ///     Interaction logic for WorkPackagesOrganiser.xaml
    /// </summary>
    public sealed partial class WorkPackagesOrganiser : IDisposable
    {
        #region Fields

        private readonly List<RibbonTabItem> _ribbonTabList;
        private readonly WorkPackagesSidePanel _sidePanel;
        private GridLength _bottomPanelHeight = new GridLength(420.0);
        private ColumnLayoutManager _planogramColumnLayoutManager; //The column layout manager to use for this instance's planogram list.

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (WorkPackagesViewModel), typeof (WorkPackagesOrganiser),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     Viewmodel controller
        /// </summary>
        public WorkPackagesViewModel ViewModel
        {
            get { return (WorkPackagesViewModel) GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (WorkPackagesOrganiser) obj;

            if (e.OldValue != null)
            {
                var oldModel = (WorkPackagesViewModel) e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                var newModel = (WorkPackagesViewModel) e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }

            senderControl.ReloadTaskNodes();           
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Creates a new instance of this type.
        /// </summary>
        public WorkPackagesOrganiser()
        {
            InitializeComponent();
            ViewModel = new WorkPackagesViewModel();

            //Add link to helpfile
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.Workpackages);

            //create the ribbon
            var homeTab = new WorkPackagesHomeTab();
            BindingOperations.SetBinding(homeTab, WorkPackagesHomeTab.ViewModelProperty,
                new Binding(ViewModelProperty.Name) {Source = this});


            _ribbonTabList = new List<RibbonTabItem> {homeTab};

            //create sidepanel
            _sidePanel = new WorkPackagesSidePanel();
            BindingOperations.SetBinding(_sidePanel, WorkPackagesSidePanel.ViewModelProperty,
                new Binding(ViewModelProperty.Name) {Source = this});

            //create planogram grid column manager
            _planogramColumnLayoutManager = new ColumnLayoutManager(
                new PlanogramInfoColumnLayoutFactory(typeof(WorkpackageRow)),
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.CurrentEntityView.Model),
                "WorkPackagePlanograms.Workflow", "Info.{0}");
            _planogramColumnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
            _planogramColumnLayoutManager.RegisterDataTemplate<PlanogramRepositoryRow>(PlanogramRepositoryRow.CreateDataTemplate);
            _planogramColumnLayoutManager.ColumnSetChanging += PlanogramColumnLayoutManager_ColumnSetChanging;
            _planogramColumnLayoutManager.AttachDataGrid(this.PlanogramGrid);


            Loaded += WorkPackagesOrganiser_Loaded;
        }



        /// <summary>
        ///     Carries out actions whenever this screen loads.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkPackagesOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            //send ribbon request
            App.ViewState.ShowRibbonTabs(_ribbonTabList);

            //send sidepanel request
            App.ViewState.ShowSidePanel(_sidePanel);

        }

        #endregion

        #region Event Handlers

        private void PlanogramColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix with additional columns:
            Int32 curIdx = 0;

            //Info column group
            columnSet.Insert(curIdx++, new DataGridExtendedTemplateColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_InformationColumnHeader,
                CellTemplate = Resources["PlanRepositoryInfoColumnDataTemplate"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                MinWidth = 50,
                IsReadOnly = true,
                SortMemberPath = "LockedToolTip",
                ColumnGroupName = "",
                ColumnCellAlignment = HorizontalAlignment.Stretch
            });

            //Planogram UserName column
            //var userNameColumn = _planogramColumnLayoutManager.CurrentColumnSet
            //    .FirstOrDefault(c => c.SortMemberPath == "Info.UserName");
            //DataGridExtendedTextColumn userNameGridCol = new DataGridExtendedTextColumn
            //{
            //    Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramUserNameColumnHeader,
            //    Binding = new Binding(userNameColumn.SortMemberPath),
            //    CanUserFilter = true,
            //    CanUserSort = true,
            //    CanUserReorder = false,
            //    CanUserResize = true,
            //    CanUserHide = false,
            //    MinWidth = 50,
            //    IsReadOnly = true,
            //    ColumnCellAlignment = HorizontalAlignment.Stretch
            //};
            //columnSet.Insert(curIdx++,userNameGridCol);

            //Planogram Name column
            DataGridExtendedTextColumn nameGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramNameColumnHeader,
                Binding = new Binding("Info.Name"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                MinWidth = 50,
                IsReadOnly = true,
                ColumnGroupName = "",
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(curIdx++, nameGridCol);

            //Create the header group
            DataGridHeaderGroup statusHeaderGroup = new DataGridHeaderGroup();
            statusHeaderGroup.HeaderName = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_StatusGroupHeader;

            //Automation Status column
            DataGridExtendedTemplateColumn autoStatusCol = new DataGridExtendedTemplateColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_AutomationStatusColumnHeader,
                CellTemplate = Resources["PlanRepositoryAutomationStatusColumnDataTemplate"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                Width = 170,
                IsReadOnly = true,
                SortMemberPath = "Info." + PlanogramInfo.AutomationProcessingStatusProperty.Name,
                ColumnGroupName = statusHeaderGroup.HeaderName,
                ColumnCellAlignment = HorizontalAlignment.Stretch,
                ClipboardContentBinding = new Binding("Info." + PlanogramInfo.AutomationProcessingStatusProperty.Name)
                {
                    Mode = BindingMode.OneWay,
                    Converter = App.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ConverterDictionaryKeyToValue] as IValueConverter,
                    ConverterParameter = ProcessingStatusTypeHelper.FriendlyNames
                }
            };
            autoStatusCol.HeaderGroupNames.Add(statusHeaderGroup);
            columnSet.Insert(curIdx++, autoStatusCol);

            //Automation Est. Time Left column
            DataGridExtendedTextColumn timeLeftGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.Workpackages_EstTimeLeft,
                Binding = new Binding("AutomationStatusEstimatedTimeLeft"),
                CanUserFilter = false,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                MinWidth = 50,
                IsReadOnly = true,
                SortMemberPath = "AutomationStatusEstimatedTimeLeft",
                ColumnGroupName = statusHeaderGroup.HeaderName,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            timeLeftGridCol.HeaderGroupNames.Add(statusHeaderGroup);
            columnSet.Insert(curIdx++, timeLeftGridCol);

            //Plan Status column
            DataGridExtendedTextColumn planStatusCol = new DataGridExtendedTextColumn()
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanStatusColumnHeader,
                Binding = new Binding("Info.Status")
                {
                    Mode = BindingMode.OneWay,
                    ConverterParameter = PlanogramStatusTypeHelper.FriendlyNames,
                    Converter = Application.Current.Resources[Framework.Controls.Wpf.ResourceKeys.ConverterDictionaryKeyToValue] as IValueConverter
                },
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                IsReadOnly = true,
                MinWidth = 50,
                ColumnGroupName = statusHeaderGroup.HeaderName,
            };
            planStatusCol.HeaderGroupNames.Add(statusHeaderGroup);
            columnSet.Insert(curIdx++, planStatusCol);

            //Validation column
            DataGridExtendedTemplateColumn validationStatusCol = new DataGridExtendedTemplateColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_ValidationColumnHeader,
                CellTemplate = Resources["PlanRepositoryValidationColumnDataTemplate"] as DataTemplate,
                CanUserFilter = false,
                CanUserSort = false,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                MinWidth = 50,
                IsReadOnly = true,
                SortMemberPath = "ValidationProcessingVisibility",
                ColumnGroupName = statusHeaderGroup.HeaderName,
                ColumnCellAlignment = HorizontalAlignment.Stretch,
                ClipboardContentBinding = new Binding("ValidationTooltip")
            };
            validationStatusCol.HeaderGroupNames.Add(statusHeaderGroup);
            columnSet.Insert(curIdx++, validationStatusCol);
        }

        /// <summary>
        ///     Called whenever a property changes on the viewmodel
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == WorkPackagesViewModel.CurrentWorkpackageTasksProperty.Path)
            {
                ReloadTaskNodes();
            }            
            else if (e.PropertyName == WorkPackagesViewModel.SelectedWorkpackageRowProperty.Path)
            {
                if(this.ViewModel.SelectedWorkpackageRow != null)
                    this.WorkpackageGrid.ScrollIntoView(this.ViewModel.SelectedWorkpackageRow);
            }
            else if (e.PropertyName == WorkPackagesViewModel.SelectedPlanogramInfoProperty.Path)
            {                
                //V8-30160: Update the info panel when the the info property changes. The binding to the panel is
                //actually to the SelectedPlanogramRow property so an explicit update is required here.
                //The usual on property change update is not used so that the following error does not occur:
                //  "Cannot change ObservableCollection during a CollectionChanged event".
                //Oddly does did not happen for all users!
                
                xPlanogramInfoPanel.GetBindingExpression(PlanogramInfoPanel.ViewModelProperty).UpdateTarget();
            }
        }


        /// <summary>
        ///     Called whenever the user collapses the plan info row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanInfoPanelExpander_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            _bottomPanelHeight = PlanInfoRow.Height;
            PlanInfoRow.Height = GridLength.Auto;
        }

        /// <summary>
        ///     Called whenever the user expands the plan info row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanInfoPanelExpander_OnExpanded(Object sender, RoutedEventArgs e)
        {
            PlanInfoRow.Height = _bottomPanelHeight;
        }


        /// <summary>
        ///     Called whenever the user double clicks on a row on the plan list grid.
        /// </summary>
        private void PlanogramGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (ViewModel != null)
            {
                if (e.RowItem != null)
                {
                    ViewModel.OpenPlanCommand.Execute();
                }
            }
        }

        /// <summary>
        /// Add a right click contex menu to the planogram grid to allow the user to open the log window
        /// with the planogram name filter set to the name of the currently selected planogram
        /// </summary>
        /// <param name="sender">Event sender object</param>
        /// <param name="e">Content menu event arguments</param>
        private void PlanogramGrid_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            if (ViewModel == null) { return; }

            //show the row context menu
            var contextMenu = new Fluent.ContextMenu();

            String selectedPlanName = null;

            // Check if the user has selected more than 1 planogram - if so, open the Event Log window unfiltered
            if (this.ViewModel.SelectedPlanogramRows.Count > 1)
            {
                // Don't do anything as the selected plan is already null and we want the event log window
                // to open unfiltered in the same way as when the user clicks the "Event Log" button
            }
            else if (this.ViewModel.SelectedPlanogramRow != null && this.ViewModel.SelectedPlanogramRow.Info != null)
            {
                selectedPlanName = this.ViewModel.SelectedPlanogramRow.Info.Name;
            }

            var viewPlanFilterItem = new Fluent.MenuItem()
            {
                Command = ViewModel.EventLogCommand,
                CommandParameter = selectedPlanName,
                Header = ViewModel.EventLogCommand.FriendlyName,
                Icon = ViewModel.EventLogCommand.SmallIcon ?? ViewModel.EventLogCommand.Icon,
            };

            contextMenu.Items.Add(viewPlanFilterItem);

            contextMenu.Placement = PlacementMode.Mouse;
            contextMenu.IsOpen = true;
        }

        private void xTasksDiagram_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.xTasksDiagram.IsVisible)
            {
                this.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        if(this.xTasksDiagram != null) this.xTasksDiagram.ArrangeDiagram();
                    }), priority: System.Windows.Threading.DispatcherPriority.Background);
            }
 
        }

        /// <summary>
        /// Opens the selected planogram to copy the double mouse click functionality
        /// also gives focus to the planogram info expander when the Space key is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanogramGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (ViewModel != null)
                {
                    if (PlanogramGrid.SelectedItem != null)
                    {
                        ViewModel.OpenPlanCommand.Execute();
                        e.Handled = true;
                    }
                }
            }
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(PlanInfoPanelExpander);
            }
            else
            {
                return;
            }
        }
        /// <summary>
        /// Expands or collapses Expander when Enter key pressed, also give focus to 
        /// the TasksPanel Expander when the Space bar is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanInfoPanelExpander_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                PlanInfoPanelExpander.IsExpanded = !PlanInfoPanelExpander.IsExpanded ? true : false;
            }

            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(TasksPanel);
            }
            else
            {
                return;
            }
        }
        /// <summary>
        /// Return key expands or collapses TasksPanel Expander, also gives focus to Worpackage grid
        /// when Spacebar pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TasksPanel_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {               
                TasksPanel.IsExpanded = !TasksPanel.IsExpanded ? true : false;
            }

            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(WorkpackageGrid);
            }
            else
            {
                return;
            }
        }
        /// <summary>
        /// Space key gives focus to the PlanogramGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkpackageGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                Keyboard.Focus(PlanogramGrid);
                e.Handled = true;
            }
            else
            {
                return;
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Reloads the tasks diagram
        /// </summary>
        private void ReloadTaskNodes()
        {
            var diagram = xTasksDiagram;

            if (diagram != null)
            {
                diagram.Items.Clear();

                if (ViewModel != null
                    && ViewModel.CurrentWorkpackageTasks != null
                    && ViewModel.CurrentWorkpackageTasks.Any())
                {
                    diagram.IsAutoArrangeOn = false;

                    WorkpackagesTaskNode lastNode = null;
                    foreach (var task in ViewModel.CurrentWorkpackageTasks.OrderBy(t => t.SequenceId))
                    {
                        var taskNode = new WorkpackagesTaskNode(task);
                        diagram.Items.Add(taskNode);

                        taskNode.UpdateLayout();

                        if (lastNode != null)
                        {
                            diagram.Links.Add(new DiagramItemLink(lastNode, taskNode));
                        }

                        lastNode = taskNode;
                    }

                    diagram.IsAutoArrangeOn = true;
                }
            }
        }

        #endregion
        
        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                Loaded -= WorkPackagesOrganiser_Loaded;
                _planogramColumnLayoutManager.ColumnSetChanging -= PlanogramColumnLayoutManager_ColumnSetChanging;
                _planogramColumnLayoutManager.Dispose();

                IDisposable disposableViewModel = ViewModel;
                ViewModel = null;
                if (disposableViewModel != null)
                {
                    disposableViewModel.Dispose();
                }

                _isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }


        #endregion

    }

    #region Supporting Classes

    /// <summary>
    ///     Diagram node for depicting a workpackages task
    /// </summary>
    public sealed class WorkpackagesTaskNode : ContentControl
    {
        #region Properties

        #region Context Property

        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register("Context", typeof (WorkflowTaskInfo), typeof (WorkpackagesTaskNode),
                new PropertyMetadata(null, OnContextPropertyChanged));

        public WorkflowTaskInfo Context
        {
            get { return (WorkflowTaskInfo) GetValue(ContextProperty); }
            private set { SetValue(ContextProperty, value); }
        }

        private static void OnContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((WorkpackagesTaskNode) obj).Content = e.NewValue;
        }

        #endregion

        public Int32 ShapeNo
        {
            get { return 0; }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline",
            Justification = "Style key override")]
        static WorkpackagesTaskNode()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof (WorkpackagesTaskNode), new FrameworkPropertyMetadata(typeof (WorkpackagesTaskNode)));
        }

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="level"></param>
        public WorkpackagesTaskNode(WorkflowTaskInfo task)
        {
            Context = task;
        }

        #endregion
    }

    #endregion
}
