﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Hodson ~ Created.
#endregion

#endregion

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages
{
    /// <summary>
    /// Interaction logic for WorkPackagesSidePanel.xaml
    /// </summary>
    public sealed partial class WorkPackagesSidePanel : UserControl
    {

        #region Properties

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(WorkPackagesViewModel), typeof(WorkPackagesSidePanel),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WorkPackagesSidePanel senderControl = (WorkPackagesSidePanel)obj;

            if (e.OldValue != null)
            {
                WorkPackagesViewModel oldModel = (WorkPackagesViewModel)e.OldValue;
                //oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                WorkPackagesViewModel newModel = (WorkPackagesViewModel)e.NewValue;
                //newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public WorkPackagesViewModel ViewModel
        {
            get { return (WorkPackagesViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        //#region SelectedGroupView Property

        //public static readonly DependencyProperty SelectedGroupViewProperty =
        //    DependencyProperty.Register("SelectedGroupView", typeof(PlanogramGroupSelectorView), typeof(WorkPackagesSidePanel),
        //    new PropertyMetadata(null, OnSelectedGroupViewPropertyChanged));

        //private static void OnSelectedGroupViewPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        //{
        //    WorkPackagesSidePanel senderControl = (WorkPackagesSidePanel)obj;

        //    if (senderControl.ViewModel != null)
        //    {
        //        PlanogramGroupSelectorView newValue = e.NewValue as PlanogramGroupSelectorView;

        //        if (newValue != null)
        //        {
        //            if (newValue.IsRecentWorkGroup)
        //            {
        //                if (!senderControl.ViewModel.IsRecentWorkSelected)
        //                {
        //                    senderControl.ViewModel.IsRecentWorkSelected = true;
        //                }
        //            }
        //            else
        //            {
        //                if (senderControl.ViewModel.SelectedGroup != newValue.PlanogramGroup)
        //                {
        //                    senderControl.ViewModel.SelectedGroup = newValue.PlanogramGroup;
        //                }
        //            }

        //        }
        //        else
        //        {
        //            senderControl.ViewModel.SelectedGroup = null;
        //        }

        //    }
        //}

        //public PlanogramGroupSelectorView SelectedGroupView
        //{
        //    get { return (PlanogramGroupSelectorView)GetValue(SelectedGroupViewProperty); }
        //    set { SetValue(SelectedGroupViewProperty, value); }
        //}

        //#endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkPackagesSidePanel()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        ///// <summary>
        ///// Called whenever a property changes on the viewmodel.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == WorkPackagesViewModel.SelectedGroupProperty.Path)
        //    {
        //        if (this.ViewModel.IsRecentWorkSelected)
        //        {
        //            this.Selector.SelectRecentWork();
        //        }
        //        else if (this.ViewModel.SelectedGroup != null)
        //        {
        //            this.Selector.SetSelectedGroup(this.ViewModel.SelectedGroup);
        //        }
        //        else
        //        {
        //            this.Selector.SelectedGroup = null;
        //        }
        //    }
        //}

        #endregion

    }
}
