﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#endregion


using System;
using System.Linq;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.ComponentModel;
using Galleria.Framework.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Csla.Rules;
using Csla;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages
{
    /// <summary>
    /// Viewmodel controller for the workpackage wizard
    /// </summary>
    public sealed class WorkpackageWizardViewModel : ViewModelAttachedControlObject<WorkpackageWizard>
    {
        #region Fields

        private WorkpackageWizardStep _currentWizardStep = WorkpackageWizardStep.WorkpackageDetail;
        private IWorkpackageWizardStepViewModel _currentStepViewModel;
        private readonly WorkpackageWizardData _workpackageData;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath CurrentWizardStepProperty = WpfHelper.GetPropertyPath<WorkpackageWizardViewModel>(p => p.CurrentWizardStep);
        public static readonly PropertyPath CurrentStepViewModelProperty = WpfHelper.GetPropertyPath<WorkpackageWizardViewModel>(p => p.CurrentStepViewModel);
        public static readonly PropertyPath WorkpackageDataProperty = WpfHelper.GetPropertyPath<WorkpackageWizardViewModel>(p => p.WorkpackageData);

        //Commands
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<WorkpackageWizardViewModel>(p => p.NextCommand);
        public static readonly PropertyPath FinishCommandProperty = WpfHelper.GetPropertyPath<WorkpackageWizardViewModel>(p => p.FinishCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<WorkpackageWizardViewModel>(p => p.PreviousCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<WorkpackageWizardViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current step of the wizard
        /// </summary>
        public WorkpackageWizardStep CurrentWizardStep
        {
            get { return _currentWizardStep; }
            private set
            {
                _currentWizardStep = value;
                OnPropertyChanged(CurrentWizardStepProperty);

                OnCurrentWizardStepChanged(value);
            }
        }

        /// <summary>
        /// Returns the viewmodel for the current wizard step
        /// </summary>
        public IWorkpackageWizardStepViewModel CurrentStepViewModel
        {
            get { return _currentStepViewModel; }
            private set
            {
                IWorkpackageWizardStepViewModel oldValue = _currentStepViewModel;

                _currentStepViewModel = value;
                OnPropertyChanged(CurrentStepViewModelProperty);

                //dispose of the old value
                if (oldValue != null)
                {
                    oldValue.Dispose();
                }
            }
        }

        /// <summary>
        /// Returns a wrapper holding all of the data required
        /// to create the workpackage
        /// </summary>
        public WorkpackageWizardData WorkpackageData
        {
            get { return _workpackageData; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public WorkpackageWizardViewModel()
        {
            _workpackageData = new WorkpackageWizardData(Workpackage.NewWorkpackage(1));
            OnCurrentWizardStepChanged(this.CurrentWizardStep);
        }

        #endregion

        #region Commands

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// Advances the wizard to the next step.
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                        {
                            FriendlyName = Message.Generic_Next,
                        };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        private Boolean Next_CanExecute()
        {
            //must not be on the last step
            if (this.CurrentWizardStep == WorkpackageWizardStep.SelectPlanograms)
            {
                return false;
            }

            //the current step must be valid
            if (!this.CurrentStepViewModel.IsValidAndComplete)
            {
                return false;
            }

            return true;
        }

        private void Next_Executed()
        {
            switch (this.CurrentWizardStep)
            {
                case WorkpackageWizardStep.WorkpackageDetail:
                    this.CurrentWizardStep = WorkpackageWizardStep.SelectWorkflow;
                    break;

                case WorkpackageWizardStep.SelectWorkflow:
                    this.CurrentWizardStep = WorkpackageWizardStep.SelectPlanograms;
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        #endregion

        #region Finish

        private RelayCommand _finishCommand;

        /// <summary>
        /// Advances the wizard to the finish step.
        /// </summary>
        public RelayCommand FinishCommand
        {
            get
            {
                if (_finishCommand == null)
                {
                    _finishCommand = new RelayCommand(
                        p => Finish_Executed(),
                        p => Finish_CanExecute())
                    {
                        FriendlyName = Message.Generic_Finish,
                    };
                    base.ViewModelCommands.Add(_finishCommand);
                }
                return _finishCommand;
            }
        }

        private Boolean Finish_CanExecute()
        {
            //must be on the last step
            if (this.CurrentWizardStep != WorkpackageWizardStep.SelectPlanograms)
            {
                return false;
            }

            return true;
        }

        private void Finish_Executed()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Previous

        private RelayCommand _previousCommand;

        /// <summary>
        /// Advances the wizard to the previous step.
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous,
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        private Boolean Previous_CanExecute()
        {
            //must not be on the first step.
            if (this.CurrentWizardStep == WorkpackageWizardStep.WorkpackageDetail)
            {
                return false;
            }

            return true;
        }

        private void Previous_Executed()
        {
            switch (this.CurrentWizardStep)
            {
                case WorkpackageWizardStep.SelectWorkflow:
                    this.CurrentWizardStep = WorkpackageWizardStep.WorkpackageDetail;
                    break;

                case WorkpackageWizardStep.SelectPlanograms:
                    this.CurrentWizardStep = WorkpackageWizardStep.SelectWorkflow;
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Advances the wizard to the cancel step.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current wizard step changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnCurrentWizardStepChanged(WorkpackageWizardStep newValue)
        {
            base.ShowWaitCursor(true);

            //load the correct viewmodel
            switch (newValue)
            {
                case WorkpackageWizardStep.WorkpackageDetail:
                    this.CurrentStepViewModel = new WorkpackageWizardWorkpackageDetailsViewModel(this.WorkpackageData);
                    break;

                case WorkpackageWizardStep.SelectWorkflow:
                    this.CurrentStepViewModel = new WorkpackageWizardSelectWorkflowViewModel(this.WorkpackageData);
                    break;

                case WorkpackageWizardStep.SelectPlanograms:
                    this.CurrentStepViewModel = null;
                    break;

                case WorkpackageWizardStep.Summary:
                    this.CurrentStepViewModel = null;
                    break;
                   
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Methods

        

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.CurrentStepViewModel = null;
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    

    #region Supporting Classes

    /// <summary>
    /// Denotes the different steps that the 
    /// workpackage wizard may go through.
    /// </summary>
    public enum WorkpackageWizardStep
    {
        WorkpackageDetail = 0,
        SelectWorkflow = 1,
        SelectPlanograms = 2,
        Summary = 3
    }

    /// <summary>
    /// Wrapper containing all data required to create the workpackage.
    /// </summary>
    public sealed class WorkpackageWizardData : INotifyPropertyChanged
    {
        #region Fields
        private readonly Workpackage _workpackage;
        private WorkflowInfo _workflow;
        private String _planogramGroupName;
        private readonly BulkObservableCollection<Int32> _assignedPlanogamIds = new BulkObservableCollection<Int32>();
        #endregion

        #region Binding Property Paths

        //public static readonly PropertyPath WorkpackageNameProperty = WpfHelper.GetPropertyPath<WorkpackageWizardData>(p => p.WorkpackageName);
        public static readonly PropertyPath WorkflowProperty = WpfHelper.GetPropertyPath<WorkpackageWizardData>(p => p.Workflow);
        public static readonly PropertyPath PlanogramGroupNameProperty = WpfHelper.GetPropertyPath<WorkpackageWizardData>(p => p.PlanogramGroupName);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the workpackage model itself
        /// </summary>
        public Workpackage Workpackage
        {
            get { return _workpackage; }
        }

        /// <summary>
        /// Gets the name of the planogram
        /// group to which the workpackage is assigned.
        /// </summary>
        public String PlanogramGroupName
        {
            get { return _planogramGroupName; }
            private set
            {
                _planogramGroupName = value;
                OnPropertyChanged(PlanogramGroupNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the assigned workflow.
        /// </summary>
        public WorkflowInfo Workflow
        {
            get { return _workflow; }
            set
            {
                _workflow = value;
                OnPropertyChanged(WorkflowProperty);
            }
        }

        /// <summary>
        /// Returns the collection of selected planograms.
        /// </summary>
        public BulkObservableCollection<Int32> AssignedPlanogramIds
        {
            get { return _assignedPlanogamIds; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="package"></param>
        public WorkpackageWizardData(Workpackage package)
        {
            _workpackage = package;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Sets the planogramgroup to which the workpackage will be assigned
        /// </summary>
        /// <param name="group"></param>
        public void SetPlanogramGroup(PlanogramGroup group)
        {
            SetPlanogramGroup(group.Id, group.Name);
        }
        public void SetPlanogramGroup(Int32 groupId, String groupName)
        {
            _workpackage.PlanogramGroupId = groupId;
            this.PlanogramGroupName = groupName;
        }

        #endregion

        #region INofityPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }

    /// <summary>
    /// Defines the expected members of a WorkpackageWizard step viewmodel
    /// </summary>
    public interface IWorkpackageWizardStepViewModel : IDisposable
    {
        Boolean IsValidAndComplete { get; }
    }

    /// <summary>
    /// Viewmodel controlling the Workpackage details step of the workpackage wizard.
    /// </summary>
    public sealed class WorkpackageWizardWorkpackageDetailsViewModel : ViewModelObject, IWorkpackageWizardStepViewModel
    {
        #region Fields
        private readonly WorkpackageWizardData _workpackageData;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath WorkpackageNameProperty = WpfHelper.GetPropertyPath<WorkpackageWizardWorkpackageDetailsViewModel>(p => p.WorkpackageName);
        public static readonly PropertyPath PlanogramGroupNameProperty = WpfHelper.GetPropertyPath<WorkpackageWizardWorkpackageDetailsViewModel>(p => p.PlanogramGroupName);

        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this stage of the wizard is
        /// valid and complete.
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get
            {
                if (String.IsNullOrEmpty(this.WorkpackageName)) return false;
                if (String.IsNullOrEmpty(this.PlanogramGroupName)) return false;

                return true;
            }
        }

        /// <summary>
        /// Gets/Sets the name of the workpackage
        /// </summary>
        public String WorkpackageName
        {
            get { return _workpackageData.Workpackage.Name; }
            set
            {
                _workpackageData.Workpackage.Name = value;
                OnPropertyChanged(WorkpackageNameProperty);
            }
        }

        /// <summary>
        /// Returns the name of the currently assigned planogram group.
        /// </summary>
        public String PlanogramGroupName
        {
            get { return _workpackageData.PlanogramGroupName; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public WorkpackageWizardWorkpackageDetailsViewModel(WorkpackageWizardData data)
        {
            _workpackageData = data;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Viewmodel controlling the Select Workflow step of the workpackage wizard
    /// </summary>
    public sealed class WorkpackageWizardSelectWorkflowViewModel : ViewModelObject, IWorkpackageWizardStepViewModel
    {
        #region Fields
        private readonly WorkpackageWizardData _workpackageData;
        private readonly WorkflowInfoListViewModel _masterWorkflowInfoList = new WorkflowInfoListViewModel();
        private WorkflowInfo _selectedWorkflow;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath AvailableWorkflowsAsyncProperty = WpfHelper.GetPropertyPath<WorkpackageWizardSelectWorkflowViewModel>(p => p.AvailableWorkflowsAsync);
        public static readonly PropertyPath SelectedWorkflowProperty = WpfHelper.GetPropertyPath<WorkpackageWizardSelectWorkflowViewModel>(p => p.SelectedWorkflow);
        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this stage of the wizard is
        /// valid and complete.
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get 
            {
                return true;
               // return (this.SelectedWorkflow != null);
            }
        }

        /// <summary>
        /// Gets the collection of available workflows aysncronously
        /// </summary>
        public WorkflowInfoList AvailableWorkflowsAsync
        {
            get
            {
                if (_masterWorkflowInfoList.Model == null)
                {
                    _masterWorkflowInfoList.FetchForCurrentEntityAsync();
                }
                return _masterWorkflowInfoList.Model;
            }
        }

        /// <summary>
        /// Gets/Sets the selected workflow
        /// </summary>
        public WorkflowInfo SelectedWorkflow
        {
            get { return _selectedWorkflow; }
            set
            {
                _selectedWorkflow = value;
                OnPropertyChanged(SelectedWorkflowProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public WorkpackageWizardSelectWorkflowViewModel(WorkpackageWizardData data)
        {
            _masterWorkflowInfoList.ModelChanged += MasterWorkflowInfoList_ModelChanged;

            _workpackageData = data;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the master workflowlist changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MasterWorkflowInfoList_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<WorkflowInfoList> e)
        {
            OnPropertyChanged(AvailableWorkflowsAsyncProperty);
        }

        #endregion

        #region Methods

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterWorkflowInfoList.ModelChanged -= MasterWorkflowInfoList_ModelChanged;
                    _masterWorkflowInfoList.Dispose();
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Viewmodel controlling the Select Planograms step of the workpackage wizard.
    /// </summary>
    public sealed class WorkpackageWizardSelectPlansViewModel : ViewModelObject, IWorkpackageWizardStepViewModel
    {
        #region Fields
        private WorkpackageWizardData _workpackageData;

        private Boolean _isCreateNew;


        private String _planogramNameFilter;
        private PlanogramGroup _planogramGroupFilter;


        private PlanogramInfoListViewModel _planInfoListView = new PlanogramInfoListViewModel();

        private readonly BulkObservableCollection<PlanogramInfo> _planogramSearchResults = new BulkObservableCollection<PlanogramInfo>();
        private ReadOnlyBulkObservableCollection<PlanogramInfo> _planogramSearchResultsRO;

        private readonly BulkObservableCollection<PlanogramInfo> _assignedPlanograms = new BulkObservableCollection<PlanogramInfo>();
        private ReadOnlyBulkObservableCollection<PlanogramInfo> _assignedPlanogramsRO;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsCreateNewProperty = WpfHelper.GetPropertyPath<WorkpackageWizardSelectPlansViewModel>(p => p.IsCreateNew);
        public static readonly PropertyPath PlanogramNameFilterProperty = WpfHelper.GetPropertyPath<WorkpackageWizardSelectPlansViewModel>(p => p.PlanogramNameFilter);
        public static readonly PropertyPath PlanogramGroupFilterProperty = WpfHelper.GetPropertyPath<WorkpackageWizardSelectPlansViewModel>(p => p.PlanogramGroupFilter);

        //Commands

        #endregion

        #region Properties

        public Boolean IsValidAndComplete
        {
            get 
            {
                if (!IsCreateNew && AssignedPlanograms.Count == 0)
                {
                    return false;
                }

                return true; 
            }

        }

        /// <summary>
        /// Gest/Sets whether new plans are to be created.
        /// </summary>
        public Boolean IsCreateNew
        {
            get { return _isCreateNew; }
            set
            {
                _isCreateNew = value;
                OnPropertyChanged(IsCreateNewProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the plan name to search for.
        /// </summary>
        public String PlanogramNameFilter
        {
            get { return _planogramNameFilter; }
            set
            {
                _planogramNameFilter = value;
                OnPropertyChanged(PlanogramNameFilterProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the planogram group to filer
        /// </summary>
        public PlanogramGroup PlanogramGroupFilter
        {
            get { return _planogramGroupFilter; }
            set
            {
                _planogramGroupFilter = value;
                OnPropertyChanged(PlanogramGroupFilterProperty);
            }
        }

        /// <summary>
        /// Returns the list of planograms which may be selected.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramInfo> PlanogramSearchResults
        {
            get
            {
                if (_planogramSearchResultsRO == null)
                {
                    _planogramSearchResultsRO = new ReadOnlyBulkObservableCollection<PlanogramInfo>(_planogramSearchResults);
                }
                return _planogramSearchResultsRO;
            }
        }

        /// <summary>
        /// Returns the list of allocated planograms.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramInfo> AssignedPlanograms
        {
            get
            {
                if (_assignedPlanogramsRO == null)
                {
                    _assignedPlanogramsRO = new ReadOnlyBulkObservableCollection<PlanogramInfo>(_assignedPlanograms);
                }
                return _assignedPlanogramsRO;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public WorkpackageWizardSelectPlansViewModel(WorkpackageWizardData data)
        {
            _workpackageData = data;
            _planInfoListView.ModelChanged += PlanInfoListView_ModelChanged;

            //fetch all plans for the given planogram group id.
            _planInfoListView.FetchByPlanogramGroupIdAsync(data.Workpackage.PlanogramGroupId);
        }

        #endregion

        #region Commands

        #region UpdateSearchResultsCommand

        private RelayCommand _updateSearchResultsCommand;

        /// <summary>
        /// Updates the search results
        /// </summary>
        public RelayCommand UpdateSearchResultsCommand
        {
            get
            {
                if (_updateSearchResultsCommand == null)
                {
                    _updateSearchResultsCommand = new RelayCommand(
                        p => UpdateSearchResultsCommand_Executed())
                        {
                            FriendlyName = "Search"
                        };
                }
                return _updateSearchResultsCommand;
            }
        }

        private void UpdateSearchResultsCommand_Executed()
        {
            _planInfoListView.FetchBySearchCriteriaAsync(this.PlanogramNameFilter,
                (this.PlanogramGroupFilter != null) ? (Int32?)this.PlanogramGroupFilter.Id : null);
        }

        #endregion


        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the plan info list model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramInfoList> e)
        {
            if (e.NewModel != null)
            {
                UpdateAvailableAndAssignedPlanograms();
            }
        }

        #endregion

        #region Methods

        private void UpdateAvailableAndAssignedPlanograms()
        {
            //List<PlanogramInfo> availableInfos = null;
            //List<PlanogramInfo> assignedInfos= null;

            //if (_planInfoListView.Model != null)
            //{

            //    WorkpackageWizardData data = _workpackageData;
            //    PlanogramInfoList masterList = _planInfoListView.Model;

            //    availableInfos = new List<PlanogramInfo>(masterList.Count - data.AssignedPlanogramIds.Count);
            //    assignedInfos = new List<PlanogramInfo>(data.AssignedPlanogramIds.Count);

            //    foreach (PlanogramInfo info in masterList)
            //    {
            //        if (data.AssignedPlanogramIds.Contains(info.Id))
            //        {
            //            assignedInfos.Add(info);
            //        }
            //        else
            //        {
            //            availableInfos.Add(info);
            //        }
            //    }

                
            //}

            //_availablePlanograms.Clear();
            //_availablePlanograms.AddRange(availableInfos);

            //_assignedPlanograms.Clear();
            //_assignedPlanograms.AddRange(assignedInfos);
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _planInfoListView.ModelChanged -= PlanInfoListView_ModelChanged;
                    _planInfoListView.Dispose();
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    #endregion
}
