﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// CCM-29026 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.1.1)
// V8-30357 : M.Brumby
//  Added store space columns
#endregion
#region Version History : CCM820
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion
#endregion

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System.Windows.Input;
using Image = System.Windows.Controls.Image;
using System.Windows.Shapes;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using System;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Windows.Data;
using Galleria.Ccm.Common.Wpf.ViewModel;


namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SelectStoreSpecificPlanogramsStep.xaml
    /// </summary>
    public sealed partial class SelectStoreSpecificPlanogramsStep : UserControl
    {
        #region Fields
        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties

        #region ViewModel Property

        private SelectStoreSpecificPlanogramsStepViewModel _viewModel; //unloaded reference to the viewmodel.

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectStoreSpecificPlanogramsStepViewModel), typeof(SelectStoreSpecificPlanogramsStep),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SelectStoreSpecificPlanogramsStep senderControl = (SelectStoreSpecificPlanogramsStep)obj;


            if (e.OldValue != null)
            {
                SelectStoreSpecificPlanogramsStepViewModel oldModel = (SelectStoreSpecificPlanogramsStepViewModel)e.OldValue;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;

            }

            if (e.NewValue != null)
            {
                SelectStoreSpecificPlanogramsStepViewModel newModel = (SelectStoreSpecificPlanogramsStepViewModel)e.NewValue;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;

            }
        }

        public SelectStoreSpecificPlanogramsStepViewModel ViewModel
        {
            get { return (SelectStoreSpecificPlanogramsStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public SelectStoreSpecificPlanogramsStep(IWizardStepViewModel viewModel, WizardWindow wizardWindow)
        {
            InitializeComponent();
            _viewModel = viewModel as SelectStoreSpecificPlanogramsStepViewModel;
            _viewModel.WizardWindow = wizardWindow;

            this.Loaded += new RoutedEventHandler(SelectStoreSpecificPlanogramsStep_Loaded);
            this.Unloaded += new RoutedEventHandler(SelectStoreSpecificPlanogramsStep_Unloaded);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions when this step is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectStoreSpecificPlanogramsStep_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = _viewModel;

            ResetColumnManager();


            // Stop showing the busy cursor as loading the screen has finished.
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; })
                , DispatcherPriority.Background, null);
        }

        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            if (this.ViewModel != null && !this.ViewModel.IsSelectFromStores)
            {
                //Planogram Name column
                DataGridExtendedTextColumn nameGridCol = new DataGridExtendedTextColumn
                {
                    Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramNameColumnHeader,
                    Binding = new Binding("Name"),
                    CanUserFilter = true,
                    CanUserSort = true,
                    CanUserReorder = false,
                    CanUserResize = true,
                    CanUserHide = false,
                    MinWidth = 50,
                    IsReadOnly = true,
                    ColumnCellAlignment = HorizontalAlignment.Stretch
                };
                columnSet.Insert(0, nameGridCol);
            }
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.ViewModel == null) return;

            if (e.PropertyName == SelectStoreSpecificPlanogramsStepViewModel.IsSelectFromStoresProperty.Path)
            {
                ResetColumnManager();
            }
        }


        /// <summary>
        /// Carries out actions when this step is unloaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectStoreSpecificPlanogramsStep_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = null;
        }

        private void AvailableGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.AddSelectedCommand.Execute();
            }
        }

        private void AssignedGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.RemoveSelectedCommand.Execute();
            }
        }
        /// <summary>
        /// The following KeyDown Events execute the Add and Remove selected commands
        /// to imitate the mouse double click events for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (AvailableGrid.SelectedItem != null && e.Key == Key.Return)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.AddSelectedCommand.Execute();
                    Keyboard.Focus(AvailableGrid);
                }
                e.Handled = true;
            }
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(AssignedGrid);
            }
            else
            {
                return;
            }
        }
        private void AssignedGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (AssignedGrid.SelectedItem != null && e.Key == Key.Return)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveSelectedCommand.Execute();
                    Keyboard.Focus(AssignedGrid);
                }
                e.Handled = true;
            }
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(AvailableGrid);
            }
            else
            {
                return;
            }
        }

        #endregion

        #region Method

        private void ResetColumnManager()
        {
            if (_columnLayoutManager != null)
            {
                _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.Dispose();
                _columnLayoutManager = null;
            }


            var uom = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId);
            if (this.ViewModel.IsSelectFromStores)
            {
                _columnLayoutManager = new ColumnLayoutManager(new LocationAndLocationSpaceColumnLayoutFactory(), uom,
                    SelectStoreSpecificPlanogramsStepViewModel.ScreenKeyPlanogram);
            }
            else
            {
                _columnLayoutManager = new ColumnLayoutManager(new PlanogramInfoColumnLayoutFactory(), uom,
                    SelectStoreSpecificPlanogramsStepViewModel.ScreenKeyPlanogram);
            }
            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(this.AvailableGrid);
        }


        #endregion

        #region IDisposable

        private Boolean _isDisposed;
        private WizardWindow wizardWindow;

        public void Dispose()
        {
            if (_isDisposed) return;

            Loaded -= SelectStoreSpecificPlanogramsStep_Loaded;

            ViewModel.Dispose();
            ViewModel = null;

            if (_columnLayoutManager != null)
            {
                _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.Dispose();
                _columnLayoutManager = null;
            }

            _isDisposed = true;
        }

        #endregion
    }
}