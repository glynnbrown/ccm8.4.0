﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SelectUniqueStoreFixturingStep.xaml
    /// </summary>
    public partial class SelectUniqueStoreFixturingStep : UserControl
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectUniqueStoreFixturingStepViewModel), typeof(SelectUniqueStoreFixturingStep),
            new PropertyMetadata(null));

        public SelectUniqueStoreFixturingStepViewModel ViewModel
        {
            get { return (SelectUniqueStoreFixturingStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stepViewModel"></param>
        public SelectUniqueStoreFixturingStep(IWizardStepViewModel stepViewModel)
        {
            InitializeComponent();
            this.ViewModel = stepViewModel as SelectUniqueStoreFixturingStepViewModel;
        }

        #endregion
    }
}
