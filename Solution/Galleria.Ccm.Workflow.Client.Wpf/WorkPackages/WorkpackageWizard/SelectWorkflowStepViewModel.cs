﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#region Version History: (CCM 8.11)
// V8-30301 : L.Ineson
//  Initial selected workflow is now first after list has been ordered.
#endregion

#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion
#endregion


using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{

    /// <summary>
    /// Viewmodel controlling the Select Workflow step of the workpackage wizard
    /// </summary>
    public sealed class SelectWorkflowStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Fields
        private readonly WorkpackageWizardData _workpackageData;
        private readonly WorkflowInfoListViewModel _masterWorkflowInfoList = new WorkflowInfoListViewModel();
        private WorkflowInfo _selectedWorkflow;
        private readonly WorkflowTaskInfoListViewModel _selectedWorkflowTasksView = new WorkflowTaskInfoListViewModel();
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath AvailableWorkflowsAsyncProperty = WpfHelper.GetPropertyPath<SelectWorkflowStepViewModel>(p => p.AvailableWorkflowsAsync);
        public static readonly PropertyPath SelectedWorkflowProperty = WpfHelper.GetPropertyPath<SelectWorkflowStepViewModel>(p => p.SelectedWorkflow);
        public static readonly PropertyPath SelectedWorkflowTasksProperty = WpfHelper.GetPropertyPath<SelectWorkflowStepViewModel>(p => p.SelectedWorkflowTasks);
        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this stage of the wizard is
        /// valid and complete.
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get
            {
                return (this.SelectedWorkflow != null);
            }
        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizard_SelectWorkflowStep_StepDisabledReason; }
        }

        /// <summary>
        /// Gets the collection of available workflows aysncronously
        /// </summary>
        public WorkflowInfoList AvailableWorkflowsAsync
        {
            get
            {
                if (_masterWorkflowInfoList.Model == null)
                {
                    _masterWorkflowInfoList.FetchForCurrentEntityAsync();
                }
                return _masterWorkflowInfoList.Model;
            }
        }

        /// <summary>
        /// Gets/Sets the selected workflow
        /// </summary>
        public WorkflowInfo SelectedWorkflow
        {
            get { return _selectedWorkflow; }
            set
            {
                _selectedWorkflow = value;
                OnPropertyChanged(SelectedWorkflowProperty);

                OnSelectedWorkflowInfoChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of task views for the selected workflow.
        /// </summary>
        public WorkflowTaskInfoList SelectedWorkflowTasks
        {
            get { return _selectedWorkflowTasksView.Model; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SelectWorkflowStepViewModel(WorkpackageWizardData data)
        {
            _masterWorkflowInfoList.ModelChanged += MasterWorkflowInfoList_ModelChanged;
            _workpackageData = data;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the master workflowlist changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MasterWorkflowInfoList_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<WorkflowInfoList> e)
        {
            OnPropertyChanged(AvailableWorkflowsAsyncProperty);

            if (e.NewModel != null)
            {
                //try to select the assignd workflow.
                this.SelectedWorkflow =
                    (_workpackageData.Workflow != null) ?
                    this.SelectedWorkflow = e.NewModel.OrderBy(w=> w.Name).FirstOrDefault(w => w.Id == _workpackageData.Workflow.Id)
                    : e.NewModel.OrderBy(w => w.Name).FirstOrDefault();
            }
            else
            {
                this.SelectedWorkflow = null;
            }
        }

        /// <summary>
        /// Called whenever the value of the SelectedWorkflowInfo property changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedWorkflowInfoChanged(WorkflowInfo newValue)
        {
            _selectedWorkflowTasksView.Model = null;

            if(newValue != null)
            {
                _selectedWorkflowTasksView.FetchByWorkflowId(newValue.Id);

            }
            OnPropertyChanged(SelectedWorkflowTasksProperty);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out any final actions to complete this step.
        /// </summary>
        public void CompleteStep()
        {
            Debug.Assert(IsValidAndComplete, "Proceeding with invalid step.");
                
            //update the workpackage data
            _workpackageData.Workflow = Model.Workflow.FetchById(this.SelectedWorkflow.Id);
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return Message.WorkpackageWizard_SelectWorkflowStep_Header; }
        }

        public String StepDescription
        {
            get { return Message.WorkpackageWizard_SelectWorkflowStep_Desc; }
        }

 
        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterWorkflowInfoList.ModelChanged -= MasterWorkflowInfoList_ModelChanged;
                    _masterWorkflowInfoList.Dispose();
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
