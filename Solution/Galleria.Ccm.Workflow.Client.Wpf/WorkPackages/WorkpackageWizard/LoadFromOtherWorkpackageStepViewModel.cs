﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion
#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion

#endregion

using System;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controller for the load from other workpackages wizard step.
    /// </summary>
    public sealed class LoadFromOtherWorkpackageStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Fields

        private WorkpackageWizardData _data;
        private readonly WorkpackageInfoListViewModel _availableWorkpackages = new WorkpackageInfoListViewModel();
        private WorkpackageInfo _selectedWorkpackage;

        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath AvailableWorkpackagesProperty = WpfHelper.GetPropertyPath<LoadFromOtherWorkpackageStepViewModel>(p => p.AvailableWorkpackages);
        public static readonly PropertyPath SelectedWorkpackageProperty = WpfHelper.GetPropertyPath<LoadFromOtherWorkpackageStepViewModel>(p => p.SelectedWorkpackage);

        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this step is valid
        /// and ready to proceed
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get { return this.SelectedWorkpackage != null; }
        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizardLoadFromOtherWorkpackageStep_StepDisabledReason; }
        }

        /// <summary>
        /// Returns the collection of available workpackages
        /// </summary>
        public ReadOnlyBulkObservableCollection<WorkpackageInfo> AvailableWorkpackages
        {
            get { return _availableWorkpackages.BindableCollection; }
        }

        /// <summary>
        /// Gets/Sets the selected workpackage
        /// </summary>
        public WorkpackageInfo SelectedWorkpackage
        {
            get { return _selectedWorkpackage; }
            set
            {
                _selectedWorkpackage = value;
                OnPropertyChanged(SelectedWorkpackageProperty);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public LoadFromOtherWorkpackageStepViewModel(WorkpackageWizardData data)
        {
            _data = data;
            _availableWorkpackages.FetchForCurrentEntityAsync();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to finalize 
        /// the selections made in this step
        /// </summary>
        public void CompleteStep()
        {
            Workpackage wk = Workpackage.FetchById(this.SelectedWorkpackage.Id);
            _data.CopyDetails(wk);
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return Message.WorkpackageWizardLoadFromOtherStep_StepHeader; }
        }

        public String StepDescription
        {
            get { return Message.WorkpackageWizardLoadFromOtherStep_StepDescription; }
        }


        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _availableWorkpackages.Dispose();
                }

                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
