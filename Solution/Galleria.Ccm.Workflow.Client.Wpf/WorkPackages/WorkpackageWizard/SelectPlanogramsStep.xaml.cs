﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
// CCM-27095 : I.George
// changed the code to use hierachy selector for the group search. User can now add favourites

#endregion
#region Version History : CCM820
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using Image = System.Windows.Controls.Image;


namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SelectPlanogramsStep.xaml
    /// </summary>
    public sealed partial class SelectPlanogramsStep : UserControl
    {
        #region Fields

        private ColumnLayoutManager _columnLayoutManager;

        #endregion

        #region Properties
        
        #region ViewModel Property

        private SelectPlanogramsStepViewModel _viewModel; //unloaded reference to the viewmodel.

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectPlanogramsStepViewModel), typeof(SelectPlanogramsStep),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SelectPlanogramsStep senderControl = (SelectPlanogramsStep)obj;
            

            if (e.OldValue != null)
            {
                SelectPlanogramsStepViewModel oldModel = (SelectPlanogramsStepViewModel)e.OldValue;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;

            }
            
            if (e.NewValue != null)
            {
                SelectPlanogramsStepViewModel newModel = (SelectPlanogramsStepViewModel)e.NewValue;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
               
            }
            senderControl.UpdateBreadcrumb();
           
        }

        public SelectPlanogramsStepViewModel ViewModel
        {
            get { return (SelectPlanogramsStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        public PlanogramHierarchySelectorViewModel SelectorViewModel
        {
            get { return xSelector.ViewModel; }
        }
        #endregion

        #region AddToFavouriesCommandProperty

        public static readonly DependencyProperty AddToFavouriesCommandProperty =
            DependencyProperty.Register("AddToFavouritesCommand", typeof(RelayCommand), typeof(SelectPlanogramsStep),
            new PropertyMetadata(null));

        public RelayCommand AddToFavouritesCommand
        {
            get { return (RelayCommand)GetValue(AddToFavouriesCommandProperty); }
            set { SetValue(AddToFavouriesCommandProperty, value); }
        }

        #endregion

        #region RemoveFromFavouriesCommandProperty

        public static readonly DependencyProperty RemoveFromFavouriesCommandProperty =
            DependencyProperty.Register("RemoveFromFavouritesCommand", typeof(RelayCommand), typeof(SelectPlanogramsStep),
            new PropertyMetadata(null));

        public RelayCommand RemoveFromFavouritesCommand
        {
            get { return (RelayCommand)GetValue(RemoveFromFavouriesCommandProperty); }
            set { SetValue(RemoveFromFavouriesCommandProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public SelectPlanogramsStep(IWizardStepViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel as SelectPlanogramsStepViewModel;

            this.Loaded += new RoutedEventHandler(SelectPlanogramsStep_Loaded);
            this.Unloaded += new RoutedEventHandler(SelectPlanogramsStep_Unloaded);
        }

        
        #endregion


        #region Event Handlers

        /// <summary>
        /// Carries out actions when this step is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectPlanogramsStep_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = _viewModel;

            if (_columnLayoutManager == null)
            {
                //create the column manager
                _columnLayoutManager = new ColumnLayoutManager(
                    new PlanogramInfoColumnLayoutFactory(),
                    DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                    SelectPlanogramsStepViewModel.ScreenKey, "Model.{0}");
                _columnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
                _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.AttachDataGrid(PlanogramsGrid);
            }


            // Stop showing the busy cursor as loading the screen has finished.
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; })
                , DispatcherPriority.Background, null);
        }

        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix additional columns:
            Int32 colIdx =0;


            // Adds DataGridExtendedCheckBoxColumn
            DataGridExtendedCheckBoxColumn checkBoxColumn = new DataGridExtendedCheckBoxColumn();
            checkBoxColumn.Header = Message.WorkpackageWizardSelectPlansStep_IsSelected;
            checkBoxColumn.CanUserFilter = false;
            checkBoxColumn.CanUserSort = false;
            checkBoxColumn.ColumnCellAlignment = HorizontalAlignment.Stretch;
            columnSet.Insert(colIdx++, checkBoxColumn);

            // sets the binding on the DataGridExtendedCheckBoxColumn
            Binding binding = new Binding();
            binding.Path = new PropertyPath("IsSelected");
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.Mode = BindingMode.TwoWay;
            checkBoxColumn.Binding = binding;


            //Planogram Name column
            DataGridExtendedTextColumn nameGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramNameColumnHeader,
                Binding = new Binding("Model.Name"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = false,
                MinWidth = 50,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, nameGridCol);
        }


        /// <summary>
        /// Carries out actions when this step is unloaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectPlanogramsStep_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = null;
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == SelectPlanogramsStepViewModel.PlanogramGroupSearchCriteriaProperty.Path)
            {
                UpdateBreadcrumb();
            }
         }

        /// <summary>
        /// Called when the user clicks the assign selected menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssignSelectedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (SelectPlanogramsStepRowView row in this.PlanogramsGrid.SelectedItems)
            {
                row.IsSelected = true;
            }
        }

        /// <summary>
        /// Called when the user clicks the unassign selected menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnassignSelectedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (SelectPlanogramsStepRowView row in this.PlanogramsGrid.SelectedItems)
            {
                row.IsSelected = false;
            }
        }

        /// <summary>
        /// called when the screen gets loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSelector_Loaded(object sender, RoutedEventArgs e)
        {
                this.xSelector.Loaded -= xSelector_Loaded;
                this.AddToFavouritesCommand = this.xSelector.AddToFavouritesCommand;
                this.RemoveFromFavouritesCommand = this.xSelector.RemoveFromFavouritesCommand;
        }

        /// <summary>
        /// Called whenever a breadcrumb is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Breadcrumb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null) return;

            TextBlock breadcrumb = (TextBlock)sender;
            PlanogramGroup group = breadcrumb.Tag as PlanogramGroup;
            if (group != null)
            {
                ViewModel.PlanogramGroupSearchCriteria = group;
            }
        }

        #endregion

        #region Method
      
        /// <summary>
        /// Updates the breadcrumb path
        /// </summary>
        private void UpdateBreadcrumb()
        {
            if (BreadcrumbStackPanel == null) return;

            //clear out any existing items.
            foreach (TextBlock tb in BreadcrumbStackPanel.Children.OfType<TextBlock>())
            {
                tb.MouseLeftButtonDown -= Breadcrumb_MouseLeftButtonDown;
            }
            BreadcrumbStackPanel.Children.Clear();

            //load in the path for the selected plan group
            if (ViewModel != null && ViewModel.PlanogramGroupSearchCriteria != null) 
            {
              
                //add in the icon
                var icon = new Image()
                {
                    Height = 15,
                    Width = 15,
                    Source = ImageResources.TreeViewItem_OpenFolder,
                    Margin = new Thickness(1, 1, 4, 1),
                    VerticalAlignment = VerticalAlignment.Center
                };
                BreadcrumbStackPanel.Children.Add(icon);

                Style itemStyle = (Style)Resources["PlanRepository_StyBreadcrumbItem"];
                Style sepStyle = (Style)Resources["PlanRepository_StyBreadcrumbSeparator"];

                //load in the breadcrumb parts
                foreach (var group in ViewModel.PlanogramGroupSearchCriteria.FetchFullPath())
                {
                    TextBlock breadcrumb = new TextBlock();
                    breadcrumb.Style = itemStyle;
                    breadcrumb.Text = group.Name;
                    breadcrumb.Tag = group;
                    breadcrumb.MouseLeftButtonDown += Breadcrumb_MouseLeftButtonDown;
                    BreadcrumbStackPanel.Children.Add(breadcrumb);

                    Path separator = new Path();
                    separator.Style = sepStyle;
                    BreadcrumbStackPanel.Children.Add(separator);
                }
           

            }

        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (_isDisposed) return;

            Loaded -= SelectPlanogramsStep_Loaded;

            ViewModel.Dispose();
            ViewModel = null;

            if (_columnLayoutManager != null)
            {
                _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.Dispose();
                _columnLayoutManager = null;
            }

            _isDisposed = true;
        }

        #endregion
    }
}
