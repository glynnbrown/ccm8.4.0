﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
// V8-26950 : Updated CreateColumnSet() to add columns in SequenceId order.
// V8-28299 : A.Kuszyk
//  Added dictionary of parameter indices to CreateColumnSet to account for task re-ordering.
#endregion

#region Version History: (CCM 8.1.0)
// V8-30105 : M.Pettit
//  When a user multi-selects parmaters across columns and updates, only edited column should be updated.
#endregion

#region Version History: (CCM 8.1.1)
// V8-30155 : L.Ineson
//  ContentLookupList now gets fetched for all plan ids together for speed.
// V8-30429  : J.Pickup
//  Introduced WorkflowTaskParameter
// V8-30486 : M.Pettit
//  Enforce a valdiation of all parameters once loading is complete as some parameters rely on parent/child parameter values
// V8-30602 : A.Probyn
//  Updated loading so that if the wizards data IsLoadingExistingWorkpackage flag is true, we no 
//  longer prepopulate the parameters and lose any existing setup for a workpackage.
#endregion

#region Version History: (CCM 8.20)
// V8-30511 : J.Pickup
//  Added some logic to ensure the window nulls of any default parameters that fail validation upon window load (workpackage vs workflow valdiation).
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion

#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] Ability Customise the names of Workflow Tasks
#endregion

#region Version History: CCM832
// CCM-18930 : G.Richards
// Release the bulk collection changed event to prevent potential memory leaks.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controller for the set task parameters step of the workpackage wizard
    /// </summary>
    public sealed class SetTaskParametersStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Fields
        private readonly WorkpackageWizardData _wizardData;
        private readonly ReadOnlyCollection<SetTaskParametersColumnDefinition> _columnSet;
        private readonly BulkObservableCollection<WorkpackageWizardCellInfo> _selectedCells = new BulkObservableCollection<WorkpackageWizardCellInfo>();

        private Object _multiInputValue;
        private Boolean _isMultiInputEnabled;
        private TaskParameterType? _multiInputType;
        private Boolean _isUpdatingMultiInput;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath ColumnSetProperty = WpfHelper.GetPropertyPath<SetTaskParametersStepViewModel>(p => p.ColumnSet);
        public static readonly PropertyPath PlanogramRowsProperty = WpfHelper.GetPropertyPath<SetTaskParametersStepViewModel>(p => p.PlanogramRows);
        public static readonly PropertyPath SelectedCellsProperty = WpfHelper.GetPropertyPath<SetTaskParametersStepViewModel>(p => p.SelectedCells);
        public static readonly PropertyPath MultiInputValueProperty = WpfHelper.GetPropertyPath<SetTaskParametersStepViewModel>(p => p.MultiInputValue);
        public static readonly PropertyPath IsMultiInputEnabledProperty = WpfHelper.GetPropertyPath<SetTaskParametersStepViewModel>(p => p.IsMultiInputEnabled);
        public static readonly PropertyPath MultiInputTypeProperty = WpfHelper.GetPropertyPath<SetTaskParametersStepViewModel>(p => p.MultiInputType);

        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this step is valid
        /// and ready to proceed
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get
            {
                foreach (WorkpackageWizardPlanogram row in this.PlanogramRows)
                {
                    foreach (WorkpackageWizardParameter param in row.Parameters)
                    {
                        if (!String.IsNullOrEmpty(param.Error))
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizardSetTaskParametersStep_StepDisabledReason; }
        }

        /// <summary>
        /// Returns the columns definitions to load.
        /// </summary>
        public ReadOnlyCollection<SetTaskParametersColumnDefinition> ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Returns the bag row views.
        /// </summary>
        public IEnumerable<WorkpackageWizardPlanogram> PlanogramRows
        {
            get { return _wizardData.WorkpackagePlans; }
        }

        /// <summary>
        /// Returns the editable collection of selected parameter cells.
        /// </summary>
        public BulkObservableCollection<WorkpackageWizardCellInfo> SelectedCells
        {
            get { return _selectedCells; }
        }

        /// <summary>
        /// Gets/Sets the value accross the selected cells
        /// </summary>
        public Object MultiInputValue
        {
            get { return _multiInputValue; }
            set
            {
                _multiInputValue = value;

                if (!_isUpdatingMultiInput)
                {
                    _isUpdatingMultiInput = true;
                    foreach (var cell in this.SelectedCells)
                    {
                        cell.SetCellValue(value);
                    }
                    _isUpdatingMultiInput = false;
                }
                OnPropertyChanged(MultiInputValueProperty);
            }
        }

        /// <summary>
        /// Returns true if multi cell input is enabled
        /// </summary>
        public Boolean IsMultiInputEnabled
        {
            get { return _isMultiInputEnabled; }
            private set
            {
                _isMultiInputEnabled = value;
                OnPropertyChanged(IsMultiInputEnabledProperty);
            }
        }

        /// <summary>
        /// Returns the parameter type for the multi cell input.
        /// </summary>
        public TaskParameterType? MultiInputType
        {
            get { return _multiInputType; }
            set
            {
                _multiInputType = value;
                OnPropertyChanged(MultiInputTypeProperty);
            }
        }

        /// <summary>
        /// Returns the enum source for the multiinput
        /// </summary>
        public IEnumerable MultiInputItemsSource
        {
            get
            {
                if (IsMultiInputEnabled && MultiInputType == TaskParameterType.Enum)
                {
                    return this.SelectedCells[0].Parameter.EnumSource;
                }
                return null;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SetTaskParametersStepViewModel(WorkpackageWizardData data)
        {
            _selectedCells.BulkCollectionChanged += SelectedParameters_BulkCollectionChanged;

            _wizardData = data;
            _columnSet = CreateColumnSet(data);

            //fetch content lookups for the given planogram rows.
            List<Int32> lookupPlanIds =
                this.PlanogramRows.Select(p => p.GetContentLookupPlanogramId())
                    .Where(i => i.HasValue).Select(i => i.Value).Distinct().ToList();

            ContentLookupList lookupList = null;
            if (lookupPlanIds.Any())
            {
                try
                {
                    lookupList = ContentLookupList.FetchByPlanogramIds(lookupPlanIds);
                }
                catch (DataPortalException ex)
                {
                    //just record it and carry on.
                    CommonHelper.RecordException(ex);
                }
            }

            //hook into parameter value changes
            foreach (WorkpackageWizardPlanogram row in this.PlanogramRows)
            {
                //Get the lookup for the row.
                Int32? lookupPlanId = row.GetContentLookupPlanogramId();

                ContentLookup lookup =
                    (lookupList != null && lookupPlanId.HasValue) ?
                    lookupList.FirstOrDefault(r => r.PlanogramId == lookupPlanId.Value) : null;

                //Prepopulate rows only if we're not loading an existing workpackage
                if (!data.IsLoadingExistingWorkpackage)
                {
                    row.PrePopulateParameterValues(lookup);
                }

                //Hook into the parameter changes
                foreach (WorkpackageWizardParameter parameter in row.Parameters)
                {   
                    //enforce validation again now that all parameters are loaded.
                    //Note: Since parameters are loaded and validated individually, there can be a case
                    //where a child parameter needs to be validated against its parent but its parent was not 
                    //loaded yet - validating here negates this situation
                    parameter.Validate();

                    //Hook into the parameter changes
                    parameter.PropertyChanged += Parameter_PropertyChanged;
                }
            }

            //When we load this screen if any values are invalid for the given planogram then we should snuff them out.
            //This issue occurs when validation taken depends on a planogram being available to become determinate.
            foreach (WorkpackageWizardPlanogram row in this.PlanogramRows)
            {
                foreach (WorkpackageWizardParameter param in row.Parameters)
                {
                    if (!String.IsNullOrEmpty(param.Error))
                    {
                        param.Value = null;
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the property changed event is fired on a parameter.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Parameter_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            WorkpackageWizardParameter senderParam = (WorkpackageWizardParameter)sender;

            if (!_isUpdatingMultiInput)
            {
                if (e.PropertyName == "Value")
                {
                    if (this.SelectedCells.Count > 0 &&
                        this.SelectedCells.Select(c => c.Parameter).Contains(senderParam))
                    {
                        _isUpdatingMultiInput = true;
                        foreach (var cell in this.SelectedCells)
                        {
                            if (cell.Parameter != null)
                            {
                                //only update parameters in a single column i.e. with the same workflow parameter
                                if (cell.Parameter.Model.WorkflowTaskParameterId == senderParam.Model.WorkflowTaskParameterId)
                                {
                                    cell.Parameter.CopyValue(senderParam);
                                }
                            }
                        }
                        _isUpdatingMultiInput = false;
                    }
                }
            }
        }

        /// <summary>
        /// Called whenever the items held by the SelectedParameters collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedParameters_BulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateMultiInputProperties();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to finalize 
        /// the selections made in this step
        /// </summary>
        public void CompleteStep()
        {

        }

        /// <summary>
        /// Updates the values of the MultiInput related properties
        /// </summary>
        private void UpdateMultiInputProperties()
        {
            _isUpdatingMultiInput = true;

            if (this.SelectedCells.Count == 0)
            {
                this.IsMultiInputEnabled = false;
                this.MultiInputValue = null;
                this.MultiInputType = null;
            }
            else
            {
                if (this.SelectedCells.All(c => c.Parameter != null))
                {
                    //all are parameter type cells.
                    if (this.SelectedCells.Count > 1)
                    {
                        var firstParamType = this.SelectedCells[0].Parameter.ParameterType;
                        Object firstValue = this.SelectedCells[0].Parameter.Value;

                        //check if the parameters are all of the same type.
                        if (this.SelectedCells.All(c => c.Parameter.ParameterType == firstParamType))
                        {
                            //check all values are the same
                            if (this.SelectedCells.All(c => c.Parameter.Value != firstValue))
                            {
                                this.MultiInputValue = firstValue;
                                this.MultiInputType = firstParamType;
                                this.IsMultiInputEnabled = true;
                            }
                            else
                            {
                                //mixed values - can edit
                                this.MultiInputValue = null;
                                this.IsMultiInputEnabled = true;
                            }
                        }
                        else
                        {
                            //mixed types - cannot edit
                            this.IsMultiInputEnabled = false;
                            this.MultiInputType = null;
                        }
                    }
                    else
                    {
                        //only 1 cell selected
                        var parameter = this.SelectedCells[0].Parameter;
                        this.IsMultiInputEnabled = true;
                        this.MultiInputValue = parameter.Value;
                        this.MultiInputType = parameter.ParameterType;
                    }
                }
                else
                {
                    this.IsMultiInputEnabled = false;
                    this.MultiInputValue = null;
                    this.MultiInputType = null;
                }
            }


            _isUpdatingMultiInput = false;
        }


        /// <summary>
        /// Loads the column set based on the workpackage wizard data.
        /// </summary>
        private static ReadOnlyCollection<SetTaskParametersColumnDefinition> CreateColumnSet(WorkpackageWizardData data)
        {
            List<SetTaskParametersColumnDefinition> columns = new List<SetTaskParametersColumnDefinition>();
            if (!data.WorkpackagePlans.Any()) return columns.AsReadOnly();

            //add the source columns
            var sourceNameCol = new SetTaskParametersColumnDefinition();
            sourceNameCol.Header = "Source Name";
            sourceNameCol.ColumnType = SetTaskParametersColumnType.Text;
            sourceNameCol.IsReadOnly = true;
            sourceNameCol.Path = WorkpackageWizardPlanogram.SourceNameProperty.Path;
            columns.Add(sourceNameCol);

            // First, we need to build a dictionary of the indexes the parameters will
            // hold in the collection they are bound to (the workpackage wizard parameters). 
            // This might not be the order they appear in the sorted list of tasks,
            // because the tasks may have been re-ordered since the workpackage was saved.
            Dictionary<Object, Int32> indexesByParameterId = new Dictionary<Object, Int32>();
            Int32 parameterIndex = 0;
            foreach (WorkpackageWizardParameter workpackageWizardParameter in data.WorkpackagePlans.First().Parameters)
            {
                indexesByParameterId.Add(workpackageWizardParameter.Model.WorkflowTaskParameterId, parameterIndex);
                parameterIndex++;
            }

            // Now, we can add the columns.
            foreach (WorkflowTask task in data.Workflow.Tasks.OrderBy(t => t.SequenceId))
            {
                foreach (WorkflowTaskParameter workflowTaskParameter in task.Parameters)
                {
                    if (!workflowTaskParameter.IsHidden)
                    {
                        Int32 paramIdx;
                        if (indexesByParameterId.TryGetValue(workflowTaskParameter.Id, out paramIdx))
                        {
                            var paramCol = new SetTaskParametersColumnDefinition();
                            paramCol.ColumnType = SetTaskParametersColumnType.Parameter;

                            paramCol.HeaderGroupingName = String.Format("{0}: {1}", task.SequenceId + 1, task.VisibleName);
                            paramCol.HeaderGroupingToolTip = task.VisibleDescription;

                            paramCol.Header = workflowTaskParameter.ParameterDetails.Name;

                            // Take the parameter index from the dictionary we built.
                            // I.e. the index that the workpackage wizard parameter that corresponds to this
                            // workflow task parameter appears.
                            paramCol.Path = String.Format(CultureInfo.InvariantCulture, "Parameters[{0}]", paramIdx);
                            paramCol.IsReadOnly = workflowTaskParameter.IsReadOnly;

                            paramCol.ParameterDetails = workflowTaskParameter.ParameterDetails;
                            paramCol.Parameter = workflowTaskParameter;
                            paramCol.WorkflowParameterId = workflowTaskParameter.Id;


                            columns.Add(paramCol);
                        }
                    }
                }
            }



            return columns.AsReadOnly();
        }


        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return Message.WorkpackageWizardSetTaskParametersStep_StepHeader; }
        }

        public String StepDescription
        {
            get { return Message.WorkpackageWizardSetTaskParametersStep_StepDescription; }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //Release bulk collection changed event
                    _selectedCells.BulkCollectionChanged -= SelectedParameters_BulkCollectionChanged;
                    _selectedCells.Clear();
                    
                    //unhook into parameter value changes
                    foreach (WorkpackageWizardPlanogram row in this.PlanogramRows)
                    {
                        foreach (WorkpackageWizardParameter parameter in row.Parameters)
                        {
                            parameter.PropertyChanged -= Parameter_PropertyChanged;
                        }
                    }
                }

                base.IsDisposed = true;
            }
        }

        #endregion


    }

    public enum SetTaskParametersColumnType
    {
        Parameter,
        Text,
        //CheckBox,
        //ComboBox,
        //Numeric,
        //ProductCode,
        //ProductCodeMultiple,
        //PlanogramStatus,
    }

    public sealed class SetTaskParametersColumnDefinition
    {

        public String Header { get; set; }
        public String HeaderGroupingName { get; set; }
        public String HeaderGroupingToolTip { get; set; }
        public String Path { get; set; }
        public SetTaskParametersColumnType ColumnType { get; set; }
        public Boolean IsReadOnly { get; set; }

        public Int32? WorkflowParameterId { get; set; }
        public EngineTaskParameterInfo ParameterDetails { get; set; }
        public WorkflowTaskParameter Parameter { get; set; }
        

        //public IEnumerable ItemSource { get; set; }
        //public String SelectedValuePath { get; set; }
        //public String DisplayMemberPath { get; set; }
    }


    public sealed class WorkpackageWizardCellInfo
    {

        public WorkpackageWizardPlanogram Row { get; private set; }
        public String PropertyName { get; private set; }
        public WorkpackageWizardParameter Parameter { get; private set; }

        public WorkpackageWizardCellInfo(WorkpackageWizardPlanogram row, String property)
        {
            Row = row;
            PropertyName = property;

            Parameter = GetParameter(this);
        }

        public Object GetCellValue()
        {
            if (Parameter != null)
            {
                return Parameter.Value;
            }
            else
            {
                return Galleria.Framework.Controls.Wpf.Helpers.GetItemPropertyValue(Row, PropertyName);
            }
        }

        public void SetCellValue(Object value)
        {
            if (Parameter != null)
            {
                Parameter.Value = value;
            }
            //else if (PropertyName == WorkpackageWizardPlanogram.OutputStatusProperty.Path)
            //{
            //    Row.OutputStatus = value;
            //}
            else
            {
                Debug.Fail("Could not set");
            }
        }

        /// <summary>
        /// Returns the parameter for the selected cell
        /// </summary>
        /// <returns></returns>
        private static WorkpackageWizardParameter GetParameter(WorkpackageWizardCellInfo cellInfo)
        {
            if (cellInfo != null && cellInfo.Row != null)
            {
                if (cellInfo.PropertyName.StartsWith("Parameters"))
                {
                    Int32 startIndexParam = cellInfo.PropertyName.IndexOf('[');
                    Int32 endIndexParam = cellInfo.PropertyName.IndexOf(']');

                    Int32 paramIndexKey =
                        Convert.ToInt32(cellInfo.PropertyName.Substring(startIndexParam + 1, endIndexParam - startIndexParam - 1));


                    return cellInfo.Row.Parameters[paramIndexKey];

                }

            }
            return null;
        }
    }
}
