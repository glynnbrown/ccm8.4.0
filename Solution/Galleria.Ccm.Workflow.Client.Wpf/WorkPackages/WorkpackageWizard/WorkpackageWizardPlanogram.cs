﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
// V8-26705 : A.Kuszyk ~ Changed ProductSelectionViewModel to Common.Wpf version.
// V8-26950 : A.Kuszyk ~ Added PerformContentLookup().
// V9-27041 : A.Kuszyk ~ Added error handling to PerformContentLookup method.
// V8-27228 : A.Kuszyk ~ Amended PerformContentLookup to only retrieve a value if no value already exists.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29608 : D.Pleasance 
//  Added constructor overload to provide parent planogram info. This is required when attempting content lookups as this should be taken from original plan.
// V8-29609 : D.Pleasance 
//  Further changes to constructor overload to include location details, this is so that this data is passed so that it isnt required to be looked up again.
#endregion
#region Version History: (CCM 8.1.0)
// V8-29746 : A.Probyn
//  Updated to no longer use ContentLookup FetchByPlanogramName and now use the new FetchByPlanogramId
// V8-30026 : M.Brumby
//  Ensure location code is part of the location code selected result as well as location name
// V8-30076 : M.Pettit
//  Old Parameter removal now carried out before GenerateTaskParameters is executed
//  Try..Catch added around Parameter addition as failsafe due to UI timing issues
#endregion
#region Version History: (CCM 8.1.1)
// V8-30155 : L.Ineson
//  The lookup record now gets passed into the PrePopulateParameterValues method.
// V8-30417 : M.Brumby
//  Added functionality for auto assigning location parameters
// V8-30429  : J.Pickup
//  TaskParameterHelper.GetHelper paramter change.
// V8-30486 : M.Pettit
//  Child WorkpackageWizardParameters now have knowledge of their parent WorkpackageWizardPlanogram (this instance)
#endregion
#region Version History : (CCM 8.3.0)
// V8-32086 : N.Haywood
//  Added category code
// CCM-18393 : M.Brumby
//  PCR01601 - Improvements to CCM publishing - Changes for auto assigning category code parameters
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks;
using Galleria.Framework.Helpers;
using System.Collections.Generic;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Wrapper containing data required to create a workpackage planogram.
    /// </summary>
    public sealed class WorkpackageWizardPlanogram : ModelView<WorkpackagePlanogram>, IDataErrorInfo
    {
        #region Nested Classes

        public class WorkpackageWizardParameterCollection :
            ModelViewCollection<WorkpackageWizardParameter, WorkpackagePlanogramParameterList, WorkpackagePlanogramParameter>
        {
            public WorkpackageWizardParameterCollection(WorkpackagePlanogramParameterList list, CreateViewDelegate createViewMethod)
                : base(createViewMethod)
            {
                Attach(list);
            }
        }

        #endregion

        #region Fields

        private WorkpackageWizardData _parent;
        private readonly WorkpackageWizardParameterCollection _taskParameters;
        private String _outputStatus;
        private PlanogramInfo _parentPlanogram = null;
        private String _locationName = String.Empty;
        private String _locationCode = String.Empty;
        private String _categoryCode = String.Empty;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath SourceNameProperty = WpfHelper.GetPropertyPath<WorkpackageWizardPlanogram>(p => p.SourceName);
        public static readonly PropertyPath OutputStatusProperty = WpfHelper.GetPropertyPath<WorkpackageWizardPlanogram>(p => p.OutputStatus);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the name of the source.
        /// </summary>
        public String SourceName
        {
            get { return Model.Name; }
        }

        /// <summary>
        /// Gets/Sets the status that the plan will be published as 
        /// on approval.
        /// </summary>
        public String OutputStatus
        {
            get { return _outputStatus; }
            set
            {
                _outputStatus = value;
                OnPropertyChanged(OutputStatusProperty);
            }
        }

        /// <summary>
        /// Returns the collection of parameters.
        /// </summary>
        public WorkpackageWizardParameterCollection Parameters
        {
            get { return _taskParameters; }
        }

        public String CategoryCode
        {
            get { return _categoryCode; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="info"></param>
        public WorkpackageWizardPlanogram(WorkpackagePlanogram model, WorkpackageWizardData parent)
            : base(model)
        {
            _parent = parent;
            _taskParameters = new WorkpackageWizardParameterCollection(model.Parameters, CreateParameterView);
            _outputStatus = "Live";

            //try to find the product group for this plan
            foreach(PlanogramInfo planInfo in parent.AssignedPlanograms)
            {
                if (model.Name == planInfo.Name)
                {
                    _categoryCode = planInfo.CategoryCode;
                }
            }
        }

        public WorkpackageWizardPlanogram(WorkpackagePlanogram model, WorkpackageWizardData parent, PlanogramInfo parentPlanogram, String locationName, String locationCode)
            : base(model)
        {
            _parent = parent;
            _taskParameters = new WorkpackageWizardParameterCollection(model.Parameters, CreateParameterView);
            _outputStatus = "Live";
            _parentPlanogram = parentPlanogram;
            _locationName = locationName;
            _locationCode = locationCode;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Populates Task Parameters with values fetched from the Planogram's Conent Lookup.
        /// </summary>
        public void PrePopulateParameterValues(ContentLookup contentLookup)
        {
            Debug.Assert(contentLookup == null ||
                contentLookup.PlanogramId == GetContentLookupPlanogramId(),
                "The given content lookup does not match the planogram id.");

           
            //now prepopulate the parameters
            foreach (WorkpackageWizardParameter parameter in _taskParameters)
            {
                if (parameter.ParameterType == Engine.TaskParameterType.FileNameAndPath)
                {
                    if (Model.PlanogramSourceFileName.Length > 0) // may not be populated if opening from existing
                    {
                        parameter.SetValue(Model.PlanogramSourceFileName);
                    }
                }
                if (parameter.ParameterType == Engine.TaskParameterType.LocationCodeSingle)
                {
                    if (_locationName.Length > 0 && _locationCode.Length > 0)
                    {
                        parameter.SetValue(String.Format("{0} {1}", _locationCode, _locationName), _locationCode);
                    }
                }
                else if (contentLookup != null)
                {
                    if (parameter.Value == null)
                    {
                        parameter.SetValuesFromContentLookup(contentLookup);
                    }
                }
            }
        }

        /// <summary>
        /// Creates the view for the given parameter model.
        /// </summary>
        private WorkpackageWizardParameter CreateParameterView(WorkpackagePlanogramParameter model)
        {
            WorkflowTaskParameter taskParam =
                 _parent.Workflow.Tasks.SelectMany(t => t.Parameters).First(p => p.Id == model.WorkflowTaskParameterId);

            return new WorkpackageWizardParameter(model, taskParam, this);
        }

        /// <summary>
        /// Generates the task parameters array.
        /// </summary>
        /// <param name="workflow"></param>
        public void GenerateTaskParameters(Model.Workflow workflow, IEnumerable<LocationInfo> locations = null, ProductGroupInfo productgroup = null)
        {
            foreach (WorkflowTask task in workflow.Tasks.OrderBy(t => t.SequenceId))
            {
                foreach (WorkflowTaskParameter workflowParam in task.Parameters)
                {
                    WorkpackagePlanogramParameter taskParam = null;
                    if (_taskParameters != null)
                    {
                        taskParam = Model.Parameters.FirstOrDefault(t => t.WorkflowTaskParameterId == workflowParam.Id);
                    }

                    if (taskParam == null)
                    {
                        //V8-30076 - timing issue can cause an error here when replacing one workflow with another
                        //this is a failsafe check to prevent the issue throwing an exception to the user - needs sorting in 8.2
                        try
                        {
                            //If we have a single location code coming in then assign it
                            //NOTE: Auto Assigning values is supposed to override any default value set for the task.
                            if (workflowParam.ParameterDetails.ParameterType == Engine.TaskParameterType.LocationCodeSingle 
                                && locations != null && locations.Count() == 1)
                            {
                                LocationInfo location = locations.First();
                                WorkflowTaskParameterValue value = WorkflowTaskParameterValue.NewWorkflowTaskParameterValue();
                                value.Value1 = String.Format("{0} {1}", location.Code, location.Name);
                                value.Value2 = location.Code;
                                workflowParam.Values.Add(value);
                            }
                            if (workflowParam.ParameterDetails.ParameterType == Engine.TaskParameterType.CategoryCodeSingle
                                && productgroup != null)
                            {
                                WorkflowTaskParameterValue value = WorkflowTaskParameterValue.NewWorkflowTaskParameterValue();
                                value.Value1 = String.Format("{0} {1}", productgroup.Code, productgroup.Name);
                                value.Value2 = productgroup.Code;
                                workflowParam.Values.Add(value);
                            }
                            taskParam = Model.Parameters.Add(workflowParam);                           
                        }
                        catch
                        { }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the planogram id to use when fetching the 
        /// content lookup for this planogram.
        /// </summary>
        /// <returns></returns>
        public Int32? GetContentLookupPlanogramId()
        {
            //return null if we don't actually need to fetch a content lookup for this.
            if (!_taskParameters.Any(p => TaskParameterHelper.GetHelper(p.ParameterType).IsContentLink))
                return null;

            //otherwise return based on the source/ parent planogram.
            if (this.Model.SourcePlanogramId.HasValue)
            {
                return (_parentPlanogram == null ? this.Model.SourcePlanogramId.Value : _parentPlanogram.Id);
            }
            else if (_parentPlanogram != null)
            {
                return _parentPlanogram.Id;
            }

            return null;
        }

        #endregion

        #region IDataErrorInfo

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                return null;
            }
        }

        #endregion

        #region IDisposable

        public override void Dispose()
        {
            _taskParameters.Dispose();
        }

        #endregion
    }
}
