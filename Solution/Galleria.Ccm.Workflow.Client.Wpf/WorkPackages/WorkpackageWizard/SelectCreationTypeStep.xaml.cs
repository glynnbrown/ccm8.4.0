﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SelectCreationTypeStep.xaml
    /// </summary>
    public sealed partial class SelectCreationTypeStep : UserControl, IDisposable
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectCreationTypeStepViewModel), typeof(SelectCreationTypeStep),
            new PropertyMetadata(null));

        public SelectCreationTypeStepViewModel ViewModel
        {
            get { return (SelectCreationTypeStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stepViewModel"></param>
        public SelectCreationTypeStep(IWizardStepViewModel stepViewModel)
        {
            InitializeComponent();
            this.ViewModel = stepViewModel as SelectCreationTypeStepViewModel;
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                this.ViewModel = null;

                _isDisposed = true;
            }
        }

        #endregion
    }
}
