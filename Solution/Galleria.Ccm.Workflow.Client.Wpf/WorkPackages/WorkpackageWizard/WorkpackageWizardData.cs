﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson
//  Created.
// V8-26705 : A.Kuszyk
//  Changed ProductSelectionViewModel to Common.Wpf version.
// V8-24779 : D.Pleasance
//  Added new from file implementation.
// V8-28046 : N.Foster
//  Added ModifyExisting workpackage type
// V8-28299 : A.Kuszyk
//  Amended LoadFromExisting to re-generate task parameters.
#endregion
#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Added StoreSpecific
#endregion
#region Version History: (CCM 8.0.3)
// V8-29608 : D.Pleasance 
//  Reworked the holding of _workpackagePlans. Also amended to provide parent planogram info. 
//  This is required when attempting content lookups as this should be taken from original plan.
// V8-29609 : D.Pleasance 
//  Further changes to StoreSpecific and creation of WorkpackageWizardPlanogram wrapper so that location details are also passed so that it isnt required to be looked up again.
#endregion
#region Version History: (CCM 810)
// V8-29858 : M.Pettit
//  CopyDetails now forces lazy-loaded properties to be pre-loaded before the copy commences
// V8-30076 : M.Pettit
//  Clear all old parameters if the workflow has changed
// V8-30122 : L.Ineson
//  Stopped naming template resolve crashing out when there is no loc space
// available.
#endregion
#region Version History: CCM811
// V8-30417 : M.Brumby
//  Added functionality for auto assigning location parameters
// V8-30602 : A.Probyn
//  Added new IsLoadingExistingWorkpackage
#endregion
#region Version History: CCM820

// V8-31023 : A.Silva
//  Modified GenerateWorkpackagePlans to add a workpackage per planogram in each of the psa files selected.

#endregion
#region Version History: (CCM 8.3.0)
// V8-31831 : A.Probyn
//  Reworked for formulae.
// V8-32300 : A.Probyn
//  Refactored to use central ValidatePlanogramNameUnique method.
// CCM-18393 : M.Brumby
//  PCR01601 - Improvements to CCM publishing - Changes for auto assigning category code
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramNameTemplateMaintenance;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Framework.Planograms.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Wrapper containing all data required to create the workpackage.
    /// </summary>
    public sealed class WorkpackageWizardData : INotifyPropertyChanged
    {
        #region Fields
        private readonly Workpackage _workpackage;
        private WorkpackageType _workpackageType = WorkpackageType.NewFromExisting;

        private Model.Workflow _workflow;
        private readonly BulkObservableCollection<LocationInfo> _assignedStores = new BulkObservableCollection<LocationInfo>();
        private readonly BulkObservableCollection<PlanogramInfo> _assignedPlanogams = new BulkObservableCollection<PlanogramInfo>();
        private readonly BulkObservableCollection<String> _assignedFiles = new BulkObservableCollection<String>();
        private LocationPlanAssignmentList _assignedLocationPlanAssignmentList;
        private ProductGroupInfo _assignedProductGroup;
        private PlanogramNameTemplate _assignedPlanogramNameTemplate;
                
        private readonly BulkObservableCollection<WorkpackageWizardPlanogram> _workpackagePlans = new BulkObservableCollection<WorkpackageWizardPlanogram>();
        private ReadOnlyBulkObservableCollection<WorkpackageWizardPlanogram> _workpackagePlansRO;

        private Boolean _runNow = true;
        private Boolean _autoAssignLocationParameters = false;
        private Boolean _autoAssignCategoryParameters = false;        
        private Boolean _isLoadingExistingWorkpackage = false;
        #endregion

        #region Binding Property Paths

        //public static readonly PropertyPath WorkpackageTypeProperty = WpfHelper.GetPropertyPath<WorkpackageWizardData>(p => p.CreationType);
        public static readonly PropertyPath WorkflowProperty = WpfHelper.GetPropertyPath<WorkpackageWizardData>(p => p.Workflow);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the workpackage model itself
        /// </summary>
        public Workpackage Workpackage
        {
            get { return _workpackage; }
        }

        /// <summary>
        /// Gets/Sets the assigned workflow.
        /// </summary>
        public Model.Workflow Workflow
        {
            get { return _workflow; }
            set
            {
                _workflow = value;
                
                if (value != null)
                {
                    this.Workpackage.WorkflowId = value.Id;
                }

                OnPropertyChanged(WorkflowProperty);
                OnWorkflowChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of selected stores.
        /// </summary>
        public BulkObservableCollection<LocationInfo> AssignedStores
        {
            get { return _assignedStores; }
        }

        /// <summary>
        /// Returns the collection of selected planograms.
        /// </summary>
        public BulkObservableCollection<PlanogramInfo> AssignedPlanograms
        {
            get { return _assignedPlanogams; }
        }

        /// <summary>
        /// Returns the collection of selected Location Plan Assignments.
        /// </summary>
        public LocationPlanAssignmentList AssignedLocationPlanAssignmentList
        {
            get { return _assignedLocationPlanAssignmentList; }
            set
            {
                _assignedLocationPlanAssignmentList = value;
            }
        }

        public ProductGroupInfo AssignedProductGroup
        {
            get { return _assignedProductGroup; }
            set
            {
                _assignedProductGroup = value;
            }
        }

        /// <summary>
        /// Returns the collection of selected Planogram Name Template.
        /// </summary>
        public PlanogramNameTemplate AssignedPlanogramNameTemplate
        {
            get { return _assignedPlanogramNameTemplate; }
            set
            {
                _assignedPlanogramNameTemplate = value;
            }
        }
        
        /// <summary>
        /// Returns the collection of selected planograms.
        /// </summary>
        public BulkObservableCollection<String> AssignedFiles
        {
            get { return _assignedFiles; }
        }

        /// <summary>
        /// Returns the collection of workpackage plans
        /// </summary>
        public ReadOnlyBulkObservableCollection<WorkpackageWizardPlanogram> WorkpackagePlans
        {
            get 
            {
                if (_workpackagePlansRO == null)
                {
                    _workpackagePlansRO = new ReadOnlyBulkObservableCollection<WorkpackageWizardPlanogram>(_workpackagePlans);
                }
                return _workpackagePlansRO;
            }
        }

        /// <summary>
        /// Gets/Sets whether the workpackage is to be executed immediately.
        /// </summary>
        public Boolean RunNow
        {
            get { return _runNow; }
            set { _runNow = value; }
        }

        /// <summary>
        /// True if we want to auto assign any Assigned Locations to
        /// any Location parameters.
        /// </summary>        
        public Boolean AutoAssignLocationParameters
        {
            get { return _autoAssignLocationParameters; }
            set
            {
                _autoAssignLocationParameters = value;
            }
        }

        /// <summary>
        /// True if we want to auto assign any Assigned Locations to
        /// any Location parameters.
        /// </summary>        
        public Boolean AutoAssignCategoryParameters
        {
            get { return _autoAssignCategoryParameters; }
            set
            {
                _autoAssignCategoryParameters = value;
            }
        }

        /// <summary>
        /// Is loading existing workpackage
        /// </summary>
        public Boolean IsLoadingExistingWorkpackage
        {
            get { return _isLoadingExistingWorkpackage; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="package"></param>
        public WorkpackageWizardData(Workpackage package, Boolean isLoadingExistingWorkpackage)
        {
            _workpackage = package;
            _isLoadingExistingWorkpackage = isLoadingExistingWorkpackage;

            //get the workflow if one is already assigned,
            if (package.WorkflowId != 0)
            {
                _workflow = Model.Workflow.FetchById(package.WorkflowId);
            }
            if (package.Planograms.Count > 0)
            {
                // get the assigned planograms
                this.AssignedPlanograms.AddRange(
                    PlanogramInfoList.FetchByIds(package.Planograms.Where(p=> p.SourcePlanogramId.HasValue).Select(p=> p.SourcePlanogramId.Value)));

                foreach (WorkpackagePlanogram workpackagePlanogram in _workpackage.Planograms)
                {
                    _workpackagePlans.Add(new WorkpackageWizardPlanogram(workpackagePlanogram, this));
                }
            }
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates the wizard data ready to create a new workpackage.
        /// </summary>
        /// <returns></returns>
        public static WorkpackageWizardData CreateNewWorkpackage()
        {
            WorkpackageWizardData data = new WorkpackageWizardData(Workpackage.NewWorkpackage(App.ViewState.EntityId), false);
            data.Workpackage.Name = null;
            return data;
        }

        /// <summary>
        /// Loads the wizard data for an existing workpackage
        /// </summary>
        /// <param name="workpackageId"></param>
        /// <returns></returns>
        public static WorkpackageWizardData LoadFromExisting(Int32 workpackageId)
        {
            WorkpackageWizardData data = null;

            try
            {
                data = new WorkpackageWizardData(Workpackage.FetchById(workpackageId), true);
                // We need to re-generate the task parameters here, in case the parameters for the tasks
                // have changed since the workpackage was saved.
                data.GenerateTaskParameters();
            }
            catch (DataPortalException) { }

            return data;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the selected workflow is changed.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnWorkflowChanged(Model.Workflow newValue)
        {
            if (newValue != null)
            {
                this.Workpackage.WorkflowId = newValue.Id;

                //V8-30076 - the workflow has changed so clear all old parameters
                foreach (WorkpackagePlanogram plan in this.Workpackage.Planograms)
                {
                    plan.Parameters.Clear();
                }
                foreach (WorkpackageWizardPlanogram plan in this.WorkpackagePlans)
                {
                    plan.Parameters.Clear();
                }
                
                
               //regenerate workpackage parameters
                GenerateTaskParameters();
            }
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Generates the workpackage plans for the current data
        /// </summary>
        public void GenerateWorkpackagePlans()
        {
            List<WorkpackagePlanogram> plans = new List<WorkpackagePlanogram>();
            _workpackagePlans.Clear();

            switch (this.Workpackage.WorkpackageType)
            {
                case WorkpackageType.NewFromExisting:
                    {
                        // build a lookup indexed by source planogram id
                        Dictionary<Int32, WorkpackagePlanogram> workpackagePlanograms = new Dictionary<Int32, WorkpackagePlanogram>();
                        foreach (WorkpackagePlanogram workpackagePlanogram in _workpackage.Planograms)
                        {
                            if (workpackagePlanogram.SourcePlanogramId != null)
                            {
                                workpackagePlanograms.Add((Int32)workpackagePlanogram.SourcePlanogramId, workpackagePlanogram);
                            }
                        }

                        // now add new planograms to the workpackage
                        plans = new List<WorkpackagePlanogram>(AssignedPlanograms.Count);
                        foreach (PlanogramInfo info in AssignedPlanograms)
                        {
                            // get the existing planogram if it exists
                            // else create a new one
                            WorkpackagePlanogram packagePlan = null;
                            if (!workpackagePlanograms.TryGetValue(info.Id, out packagePlan))
                            {
                                packagePlan = _workpackage.Planograms.Add(info);
                            }

                            // add to the list of workpackage planograms
                            plans.Add(packagePlan);
                            _workpackagePlans.Add(new WorkpackageWizardPlanogram(packagePlan, this));
                        }

                        // remove any old workpackage planograms
                        _workpackage.Planograms.RemoveList(_workpackage.Planograms.Except(plans).ToList());
                    }
                    break;

                case WorkpackageType.LoadFromWorkpackage:
                    foreach (WorkpackagePlanogram workpackagePlanogram in _workpackage.Planograms)
                    {
                        _workpackagePlans.Add(new WorkpackageWizardPlanogram(workpackagePlanogram, this));
                    }
                    break;

                case WorkpackageType.NewFromFile:
                    {
                        _workpackage.Planograms.Clear();
                        
                        // now add new planograms to the workpackage
                        foreach (String planFile in AssignedFiles)
                        {
                            // Split any ProSpace plans before adding them to the workpackage.
                            String extension = Path.GetExtension(planFile);
                            if (extension != null &&
                                extension.Equals(".psa"))
                            {
                                List<String> planNames = FileHelper.GetPsaPlanogramsFromFile(planFile);

                                IEnumerable<WorkpackagePlanogram> workpackagePlanograms = planNames.Select(planName => _workpackage.Planograms.Add(planName));
                                foreach (WorkpackagePlanogram packagePlan in workpackagePlanograms)
                                {
                                    packagePlan.PlanogramSourceFileName = planFile;
                                    _workpackagePlans.Add(new WorkpackageWizardPlanogram(packagePlan, this));
                                }
                            }
                            else
                            {
                                // get the existing planogram if it exists
                                // else create a new one
                                WorkpackagePlanogram packagePlan = _workpackage.Planograms.Add(Path.GetFileNameWithoutExtension(planFile));
                                packagePlan.PlanogramSourceFileName = planFile;
                            
                                _workpackagePlans.Add(new WorkpackageWizardPlanogram(packagePlan, this));
                                
                            }
                        }
                    }
                    break;

                case WorkpackageType.ModifyExisting:
                    {
                        // build a lookup indexed by source planogram id
                        Dictionary<Int32, WorkpackagePlanogram> workpackagePlanograms = new Dictionary<Int32, WorkpackagePlanogram>();
                        foreach (WorkpackagePlanogram workpackagePlanogram in _workpackage.Planograms)
                        {
                            if (workpackagePlanogram.SourcePlanogramId != null)
                            {
                                workpackagePlanograms.Add((Int32)workpackagePlanogram.SourcePlanogramId, workpackagePlanogram);
                            }
                        }

                        // now add new planograms to the workpackage
                        plans = new List<WorkpackagePlanogram>(AssignedPlanograms.Count);
                        foreach (PlanogramInfo info in AssignedPlanograms)
                        {
                            // get the existing planogram if it exists
                            // else create a new one
                            WorkpackagePlanogram packagePlan = null;
                            if (!workpackagePlanograms.TryGetValue(info.Id, out packagePlan))
                            {
                                packagePlan = _workpackage.Planograms.Add(info);
                            }

                            // ensure that the destination plan id is
                            // set to match the source planogram id which
                            // tells the engine not to create a new output planogram
                            packagePlan.DestinationPlanogramId = (Int32)packagePlan.SourcePlanogramId;

                            // add to the list of workpackage planograms
                            plans.Add(packagePlan);
                            _workpackagePlans.Add(new WorkpackageWizardPlanogram(packagePlan, this));
                        }

                        // remove any old workpackage planograms
                        _workpackage.Planograms.RemoveList(_workpackage.Planograms.Except(plans).ToList());
                    }
                    break;
                    
                case WorkpackageType.StoreSpecific:
                    {
                        _workpackage.Planograms.Clear(); // ATM we always clear plan list, to be reviewed if we have store reference held against workpackage planogram.

                        #region Obtain lookup detail
                        List<Int16> storeIds = new List<Int16>();
                        List<Int32> planogramIds = new List<Int32>();
                        List<FieldSelectorGroup> availableFieldGroups = PlanogramNameTemplateMaintenanceViewModel.PlanogramNameFieldSelectorGroups();
                        List<ObjectFieldInfo> formulaFields = ObjectFieldInfo.ExtractFieldsFromText(this.AssignedPlanogramNameTemplate.BuilderFormula, availableFieldGroups.SelectMany(f => f.Fields).Distinct());

                        //Create object field expression
                        ObjectFieldExpression formulaFieldExpression = new ObjectFieldExpression(this.AssignedPlanogramNameTemplate.BuilderFormula);
                        
                        if (AssignedStores.Count > 0)
                        {
                            storeIds = AssignedStores.Select(p => p.Id).ToList();
                            planogramIds = this.AssignedLocationPlanAssignmentList.Where(p => storeIds.Contains(p.LocationId)).Select(p => p.PlanogramId).Distinct().ToList();
                        }
                        else
                        {
                            planogramIds = AssignedPlanograms.Select(p => p.Id).ToList();
                            storeIds = this.AssignedLocationPlanAssignmentList.Where(p => planogramIds.Contains(p.PlanogramId)).Select(p => p.LocationId).Distinct().ToList();
                        }

                        Dictionary<Int16, LocationSpaceProductGroupInfo> locationSpaceLookup;
                        Dictionary<Int16, Location> locationLookup;
                        Dictionary<Int32, Planogram> planogramLookup;
                        GetPlanogramNameTemplateLookupData(storeIds, planogramIds, formulaFields, out locationSpaceLookup, out locationLookup, out planogramLookup);
                        #endregion

                        #region Create store plans and resolve plan names
                        List<String> planogramNames = new List<String>();
                        
                        if (AssignedStores.Count > 0)
                        {
                            Dictionary<Int16, LocationPlanAssignment> locationPlanAssignmentLookup = this.AssignedLocationPlanAssignmentList.Where(p => storeIds.Contains(p.LocationId)).ToLookupDictionary(p => p.LocationId);
                            Dictionary<Int32, PlanogramInfo> planogramInfoLookup = PlanogramInfoList.FetchByIds(locationPlanAssignmentLookup.Values.Select(p => p.PlanogramId).Distinct()).ToLookupDictionary(p => p.Id);

                            foreach (LocationInfo location in AssignedStores)
                            {
                                LocationPlanAssignment locationPlanAssignment = locationPlanAssignmentLookup[location.Id];
                                String planogramNameTemplateAttribute = ResolvePlanogramNameTemplate(formulaFieldExpression, formulaFields, location.Id, locationPlanAssignment.PlanogramId, locationSpaceLookup, locationLookup, planogramLookup);
                                planogramNameTemplateAttribute = PlanogramNameTemplate.ValidatePlanogramNameUnique(planogramNameTemplateAttribute, planogramNames);
                                _workpackage.Planograms.Add(planogramInfoLookup[locationPlanAssignment.PlanogramId], planogramNameTemplateAttribute);
                                _workpackagePlans.Add(new WorkpackageWizardPlanogram(_workpackage.Planograms.Last(), this, planogramInfoLookup[locationPlanAssignment.PlanogramId], location.Name, location.Code));
                            }
                        }
                        else
                        {
                            foreach (PlanogramInfo planogram in AssignedPlanograms)
                            {
                                foreach (LocationPlanAssignment locationPlanAssignment in this.AssignedLocationPlanAssignmentList.Where(p => p.PlanogramId == planogram.Id))
                                {
                                    String planogramNameTemplateAttribute = ResolvePlanogramNameTemplate(formulaFieldExpression, formulaFields, locationPlanAssignment.LocationId, planogram.Id, locationSpaceLookup, locationLookup, planogramLookup);
                                    planogramNameTemplateAttribute = PlanogramNameTemplate.ValidatePlanogramNameUnique(planogramNameTemplateAttribute, planogramNames);
                                    _workpackage.Planograms.Add(planogram, planogramNameTemplateAttribute);

                                    Location location = null;
                                    if (!locationLookup.TryGetValue(locationPlanAssignment.LocationId, out location))
                                    {
                                        try
                                        {
                                            location = Location.FetchById(locationPlanAssignment.LocationId);
                                            locationLookup[locationPlanAssignment.LocationId] = location;
                                        }
                                        catch (DataPortalException) { }
                                        if (location == null) continue;
                                    }

            
                                    _workpackagePlans.Add(new WorkpackageWizardPlanogram(
                                        _workpackage.Planograms.Last(), 
                                        this, planogram,
                                        location.Name,
                                        location.Code));
                                }
                            }
                        }
                        #endregion
                    }
                    break;

                default:
                    throw new NotImplementedException();

            }
        }
                
        /// <summary>
        /// Gets the lookup data for resolving the planogram name template
        /// </summary>
        /// <param name="storeIds">The store ids filter</param>
        /// <param name="planogramIds">The planogram ids filter</param>
        /// <param name="formulaFields">The planogram name template lookup fields</param>
        /// <param name="locationSpaceLookup">The location space lookup dictionary</param>
        /// <param name="locationLookup">The location lookup dictionary</param>
        /// <param name="planogramLookup">The planogram lookup dictionary</param>
        private void GetPlanogramNameTemplateLookupData(List<Int16> storeIds, List<Int32> planogramIds, List<ObjectFieldInfo> formulaFields, out Dictionary<Int16, LocationSpaceProductGroupInfo> locationSpaceLookup, 
                                                            out Dictionary<Int16, Location> locationLookup, out Dictionary<Int32, Planogram> planogramLookup)
        {
            Boolean getLocationSpaceLookupData = false;
            Boolean getLocationLookupData = false;
            Boolean getPlanogramLookupData = false;

            foreach (ObjectFieldInfo objectFieldInfo in formulaFields)
            {
                if (objectFieldInfo.OwnerType == typeof(LocationSpaceProductGroup))
                {
                    getLocationSpaceLookupData = true;
                }
                if (objectFieldInfo.OwnerType == typeof(Location))
                {
                    getLocationLookupData = true;
                }
                if (objectFieldInfo.OwnerType == typeof(Planogram))
                {
                    getPlanogramLookupData = true;
                }
            }

            locationSpaceLookup = new Dictionary<Int16, LocationSpaceProductGroupInfo>();
            locationLookup = new Dictionary<Int16, Location>();
            planogramLookup = new Dictionary<Int32, Planogram>();

            if (getLocationSpaceLookupData)
            {
                LocationSpaceProductGroupInfoList locationSpaceProductGroupInfoList = LocationSpaceProductGroupInfoList.FetchByProductGroupId(this.AssignedProductGroup.Id); 
                locationSpaceLookup = locationSpaceProductGroupInfoList.Where(p => storeIds.Contains(p.LocationId)).ToDictionary(p => p.LocationId);                
            }

            if (getLocationLookupData)
            {
                foreach (Int16 locationId in storeIds)
                {
                    locationLookup.Add(locationId, Location.FetchById(locationId));
                }
            }

            if (getPlanogramLookupData)
            {
                foreach (Int32 planogramId in planogramIds)
                {
                    Package package = Package.FetchById(planogramId);
                    planogramLookup.Add(planogramId, package.Planograms[0]);
                }
            }
        }

        /// <summary>
        /// Resolves the planogram name template from the specified lookup data
        /// </summary>
        /// <param name="formulaFields">The planogram name template lookup fields to resolve</param>
        /// <param name="locationId">The location to lookup</param>
        /// <param name="planogramId">The planogram to lookup</param>
        /// <param name="locationSpaceLookup">The location space lookup dictionary</param>
        /// <param name="locationLookup">The location lookup dictionary</param>
        /// <param name="planogramLookup">The planogram lookup dictionary</param>
        /// <returns></returns>
        private String ResolvePlanogramNameTemplate(ObjectFieldExpression expression, List<ObjectFieldInfo> formulaFields, Int16 locationId, Int32 planogramId, Dictionary<Int16, LocationSpaceProductGroupInfo> locationSpaceLookup, Dictionary<Int16, Location> locationLookup, Dictionary<Int32, Planogram> planogramLookup)
        {
            //Clear expression parameters
            expression.Parameters.Clear();

            String planogramName = this.AssignedPlanogramNameTemplate.BuilderFormula;

            foreach (ObjectFieldInfo objectFieldInfo in formulaFields)
            {
                object sourceValue = null;
                String value = String.Empty;

                if (objectFieldInfo.OwnerType == typeof(LocationSpaceProductGroup))
                {
                    LocationSpaceProductGroupInfo group;
                    if (locationSpaceLookup.TryGetValue(locationId, out group))
                    {
                        sourceValue = objectFieldInfo.GetValue(group);
                    }
                }
                else if (objectFieldInfo.OwnerType == typeof(Location))
                {
                    Location location;
                    if (locationLookup.TryGetValue(locationId, out location))
                    {
                        sourceValue = objectFieldInfo.GetValue(location);
                    }
                }
                else if (objectFieldInfo.OwnerType == typeof(Planogram))
                {
                    Planogram planogram;
                    if (planogramLookup.TryGetValue(planogramId, out planogram))
                    {
                        sourceValue = objectFieldInfo.GetValue(planogram);
                    }
                }
                //addd value as a paramter
                expression.AddParameter(objectFieldInfo.FieldPlaceholder, sourceValue);
            }
            
            //evaluate the formula against the source planogram
            Object returnValue = expression.Evaluate();

            if (returnValue != null)
            {
                planogramName = returnValue.ToString();
            }

            return planogramName;
        }


        /// <summary>
        /// Generates the task parameters for the current workflow.
        /// </summary>
        public void GenerateTaskParameters()
        {
            foreach (WorkpackageWizardPlanogram plan in this.WorkpackagePlans)
            {
                IEnumerable<LocationInfo> locations = null;
                ProductGroupInfo productGroup = null;

                if (this.AutoAssignLocationParameters)
                {
                    locations = this.AssignedStores;
                }
                if (this.AutoAssignCategoryParameters)
                {
                    productGroup = this.AssignedProductGroup;
                }

                plan.GenerateTaskParameters(this.Workflow, locations, productGroup);
                
            }
        }

        /// <summary>
        /// Saves the workpackage based on the allocated details
        /// </summary>
        public Workpackage SaveWorkpackage()
        {
            //Save the initial package details
            Workpackage package = this.Workpackage;

            return package.Save();
        }

        /// <summary>
        /// Copies in details from another workpackage
        /// </summary>
        /// <param name="other"></param>
        public void CopyDetails(Workpackage other)
        {
            this.Workflow = Model.Workflow.FetchById(other.WorkflowId);

            _assignedPlanogams.Clear();
            _assignedPlanogams.AddRange(
                PlanogramInfoList.FetchByIds(other.Planograms.Where(s => s.SourcePlanogramId.HasValue).Select(p => p.SourcePlanogramId.Value)));

            this.Workpackage.Planograms.Clear();

            //force all async lazy-loaded children to load first
            other.LoadChildren();

            //Now perform the copy of each planogram
            foreach (var wp in other.Planograms)
            {
                this.Workpackage.Planograms.Add(wp.Copy());
            }
        }

        #endregion

        #region INofityPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
    
}
