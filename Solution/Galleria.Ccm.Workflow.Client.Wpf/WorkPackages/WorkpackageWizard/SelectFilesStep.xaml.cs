﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SelectFilesStep.xaml
    /// </summary>
    public sealed partial class SelectFilesStep : UserControl
    {
        #region Fields

        #endregion

        #region Properties

        #region ViewModel Property

        private SelectFilesStepViewModel _viewModel; //unloaded reference to the viewmodel.

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectFilesStepViewModel), typeof(SelectFilesStep),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SelectFilesStep senderControl = (SelectFilesStep)obj;


            if (e.OldValue != null)
            {
                SelectFilesStepViewModel oldModel = (SelectFilesStepViewModel)e.OldValue;
            }

            if (e.NewValue != null)
            {
                SelectFilesStepViewModel newModel = (SelectFilesStepViewModel)e.NewValue;                
            }
        }

        public SelectFilesStepViewModel ViewModel
        {
            get { return (SelectFilesStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion
        
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public SelectFilesStep(IWizardStepViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel as SelectFilesStepViewModel;

            this.Loaded += new RoutedEventHandler(SelectFilesStep_Loaded);
            this.Unloaded += new RoutedEventHandler(SelectFilesStep_Unloaded);
        }

        #endregion


        #region Event Handlers

        /// <summary>
        /// Carries out actions when this step is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectFilesStep_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = _viewModel;
            
            // Stop showing the busy cursor as loading the screen has finished.
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; })
                , DispatcherPriority.Background, null);
        }
        
        /// <summary>
        /// Carries out actions when this step is unloaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectFilesStep_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = null;
        }

        /// <summary>
        /// Called when the user right clicks on grid row.
        /// </summary>
        private void PlanogramsGrid_RowItemMouseRightClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            //Show a quick context menu to allow multi check/uncheck of all highlighted rows
            ContextMenu cm = new ContextMenu();

            //Select highlighted rows:
            if (this.PlanogramsGrid.SelectedItems.OfType<SelectFilesStepRowView>().Any(r => !r.IsSelected))
            {
                MenuItem select = new MenuItem();
                select.Header = Message.WorkpackageWizard_SelectFilesStep_SelectHighlightedRows;
                select.Click +=
                (s, arg) =>
                {
                    foreach (var row in this.PlanogramsGrid.SelectedItems.OfType<SelectFilesStepRowView>())
                    {
                        row.IsSelected = true;
                    }
                };
                cm.Items.Add(select);
            }

            //Uncheck highlighted rows.
            if (this.PlanogramsGrid.SelectedItems.OfType<SelectFilesStepRowView>().Any(r => r.IsSelected))
            {
                MenuItem unselect = new MenuItem();
                unselect.Header = Message.WorkpackageWizard_SelectFilesStep_UnselectHighlightedRows;
                unselect.Click +=
                (s, arg) =>
                {
                    foreach (var row in this.PlanogramsGrid.SelectedItems.OfType<SelectFilesStepRowView>())
                    {
                        row.IsSelected = false;
                    }
                };
                cm.Items.Add(unselect);
            }

            cm.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
            cm.IsOpen = true;
        }

        #endregion

        #region Method

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (_isDisposed) return;

            Loaded -= SelectFilesStep_Loaded;

            ViewModel.Dispose();
            ViewModel = null;
            
            _isDisposed = true;
        }

        #endregion

        
    }
}
