﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
// V8-28046 : N.Foster
//  Added ModifyExisting workpackage type
#endregion
#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Added StoreSpecific \ SelectPlanogramNameTemplate
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for WorkpackageWizard.xaml
    /// </summary>
    public sealed partial class WizardWindow : ExtendedRibbonWindow
    {
        #region Constants
        const String WorkpackageDetailsStepTemplateName = "WorkpackageWizard_WorkpackageDetailsStep";
        const String SelectWorkflowStepTemplateName = "WorkpackageWizard_SelectWorkflowStep";
        const String SelectPlanogramsStepTemplateName = "WorkpackageWizard_SelectPlanogramsStep";
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(WizardViewModel), typeof(WizardWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));


        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WizardWindow senderControl = (WizardWindow)obj;

            if (e.OldValue != null)
            {
                WizardViewModel oldModel = (WizardViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                WizardViewModel newModel = (WizardViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }

            senderControl.SetContentPresenterTemplate();
        }


        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public WizardViewModel ViewModel
        {
            get { return (WizardViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public WizardWindow(WizardViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.WorkpackageWizard);


            this.ViewModel = viewModel;

            this.Loaded += WorkpackageWizard_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkpackageWizard_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= WorkpackageWizard_Loaded;
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }), priority: DispatcherPriority.Background);
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == WizardViewModel.CurrentStepViewModelProperty.Path)
            {
                SetContentPresenterTemplate();
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Updates the template used by the step content presenter
        /// </summary>
        private void SetContentPresenterTemplate()
        {
            if (this.StepContentPresenter != null)
            {
                IDisposable oldContent = this.StepContentPresenter.Content as IDisposable;

                FrameworkElement newContent = null;

                if (this.ViewModel != null)
                {
                    IWizardStepViewModel stepController = this.ViewModel.CurrentStepViewModel;

                    switch (this.ViewModel.CurrentWizardStep)
                    {
                        case WizardStep.SelectWorkpackageType:
                            newContent = new SelectCreationTypeStep(stepController);
                            break;

                        case WizardStep.SelectPlanograms:
                            newContent = new SelectPlanogramsStep(stepController);
                            break;

                        case WizardStep.SelectFiles:
                            newContent = new SelectFilesStep(stepController);
                            break;

                        case WizardStep.SelectStoreSpecific:
                            newContent = new SelectStoreSpecificPlanogramsStep(stepController, this);
                            break;

                        case WizardStep.SelectPlanogramNameTemplate:
                            newContent = new SelectPlanogramNameTemplateStep(stepController);
                            break;

                        case WizardStep.SelectStores:
                            newContent = new SelectStoresStep(stepController);
                            break;

                        case WizardStep.SelectUniqueStoreFixturing:
                            newContent = new SelectUniqueStoreFixturingStep(stepController);
                            break;

                        case WizardStep.LoadFromOtherWorkpackage:
                            newContent = new LoadFromOtherWorkpackageStep(stepController);
                            break;

                        case WizardStep.SelectWorkflow:
                            newContent = new SelectWorkflowStep(stepController);
                            break;

                        case WizardStep.SetTaskParameters:
                            newContent = new SetTaskParametersStep(stepController);
                            break;

                        case WizardStep.Summary:
                            newContent = new SummaryStep(stepController);
                            break;
                    }
                }

                this.StepContentPresenter.Content = newContent;

                //clear off the old content.
                if (oldContent != null)
                {
                    oldContent.Dispose();
                }
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    IDisposable disposableViewModel = (IDisposable)this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }

        }


        #endregion
    }
}
