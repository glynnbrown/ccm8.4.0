﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)

// V8-29026 : D.Pleasance ~ Created.

#endregion

#region Version History: (CCM 8.1.1)

// V8-30357 : M.Brumby
//  Added store space columns

#endregion

#region Version History: (CCM 8.20)

// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property

#endregion

#region Version History : CCM830

// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll commands, ammended Icons for AddSelected and RemoveSelected commands.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Windows.Controls;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controlling the Select Store specific Planograms step of the workpackage wizard.
    /// </summary>
    public sealed class SelectStoreSpecificPlanogramsStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Constants
        internal const String ScreenKeyPlanogram = "SelectStoreSpacificPlanogramsStep_Planogram";
        internal const String ScreenKeyLocation = "SelectStoreSpacificPlanogramsStep_Location";
        #endregion

        #region Fields
        
        private WorkpackageWizardData _workpackageData;
        private ProductGroupInfo _selectedProductGroup = null;
        private Boolean _isSelectFromStores = false;

        private readonly BulkObservableCollection<object> _availableRows = new BulkObservableCollection<object>();
        private ReadOnlyBulkObservableCollection<object> _availableRowsRO;

        private readonly BulkObservableCollection<object> _assignedRows = new BulkObservableCollection<object>();
        private ReadOnlyBulkObservableCollection<object> _assignedRowsRO;

        private ObservableCollection<object> _selectedAvailableRows = new ObservableCollection<object>();
        private ObservableCollection<object> _selectedAssignedRows = new ObservableCollection<object>();

        private ProductHierarchyViewModel _productHierarchyView = new ProductHierarchyViewModel();
        private LocationList _masterLocationList;
        private LocationPlanAssignmentList _productGroupPlanAssignments = null;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedProductGroupProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.SelectedProductGroup);
        public static readonly PropertyPath IsSelectFromStoresProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.IsSelectFromStores);
        public static readonly PropertyPath AvailableRowsProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.AvailableRows);
        public static readonly PropertyPath AssignedRowsProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.AssignedRows);
        public static readonly PropertyPath SelectedAvailableRowsProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.SelectedAvailableRows);
        public static readonly PropertyPath SelectedAssignedRowsProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.SelectedAssignedRows);
        
        //Commands
        public static readonly PropertyPath SelectProductGroupCommandProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.SelectProductGroupCommand);
        public static readonly PropertyPath AddSelectedCommandProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.AddSelectedCommand);
        public static readonly PropertyPath RemoveSelectedCommandProperty = WpfHelper.GetPropertyPath<SelectStoreSpecificPlanogramsStepViewModel>(p => p.RemoveSelectedCommand);

        #endregion

        #region Properties

        public bool IsValidAndComplete
        {
            get
            {
                if (_assignedRows.Count == 0)
                {
                    return false;
                }

                return true;
            }
        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizard_SelectStoreSpecific_StepDisableReason; }
        }

        /// <summary>
        /// Gets/Sets the currently selected group
        /// </summary>
        public ProductGroupInfo SelectedProductGroup
        {
            get { return _selectedProductGroup; }
            set
            {
                _selectedProductGroup = value;
                OnPropertyChanged(SelectedProductGroupProperty);
                OnSelectedProductGroupChanged();
            }
        }
        
        /// <summary>
        /// Gets/Sets IsSelectFromStores
        /// </summary>
        public Boolean IsSelectFromStores
        {
            get { return _isSelectFromStores; }
            set
            {
                _isSelectFromStores = value;
                _availableRows.Clear();
                _assignedRows.Clear();
                OnPropertyChanged(IsSelectFromStoresProperty);
                UpdateAvailableSelections();
            }
        }

        /// <summary>
        /// Returns the available rows
        /// </summary>
        public ReadOnlyBulkObservableCollection<object> AvailableRows
        {
            get
            {
                if (_availableRowsRO == null)
                {
                    _availableRowsRO = new ReadOnlyBulkObservableCollection<object>(_availableRows);
                }
                return _availableRowsRO;
            }
        }

        /// <summary>
        /// Returns the collection of assigned rows for the IsSelectFromStores filter type.
        /// </summary>
        public ReadOnlyBulkObservableCollection<object> AssignedRows
        {
            get
            {
                if (_assignedRowsRO == null)
                {
                    _assignedRowsRO = new ReadOnlyBulkObservableCollection<object>(_assignedRows);
                }
                return _assignedRowsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected available rows
        /// </summary>
        public ObservableCollection<object> SelectedAvailableRows
        {
            get { return _selectedAvailableRows; }
        }

        /// <summary>
        /// Returns the collection of selected assigned rows
        /// </summary>
        public ObservableCollection<object> SelectedAssignedRows
        {
            get { return _selectedAssignedRows; }
        }

        #endregion

        #region Commands

        #region SelectProductGroupCommand

        private RelayCommand _selectProductGroupCommand;

        /// <summary>
        /// Shows the select Product group dialog
        /// </summary>
        public RelayCommand SelectProductGroupCommand
        {
            get
            {
                if (_selectProductGroupCommand == null)
                {
                    _selectProductGroupCommand = new RelayCommand(
                        p => SelectProductGroup_Executed(),
                        p => SelectProductGroup_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                }
                return _selectProductGroupCommand;
            }
        }

        private Boolean SelectProductGroup_CanExecute()
        {
            //user must have get permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(ProductGroup)))
            {
                this.SelectProductGroupCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SelectProductGroup_Executed()
        {
            ProductHierarchy hierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);
            
            MerchGroupSelectionWindow dialog =
                new MerchGroupSelectionWindow(hierarchy, null, DataGridSelectionMode.Single);
            App.ShowWindow(dialog, this.WizardWindow, true);

            if (dialog.DialogResult == true)
            {
                ProductGroup selectedGroup = dialog.SelectionResult.FirstOrDefault();
                if (selectedGroup != null)
                {
                    this.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(selectedGroup);
                }
            }
        }

        #endregion

        #region AddSelectedCommand

        private RelayCommand _addSelectedCommand;

        /// <summary>
        /// Adds any selected available rows to the assigned list
        /// </summary>
        public RelayCommand AddSelectedCommand
        {
            get
            {
                return _addSelectedCommand ??
                       (_addSelectedCommand =
                        new RelayCommand(p => AddSelected_Executed(), p => AddSelected_CanExecute())
                        {
                            FriendlyDescription = Message.WorkpackageWizard_SelectStoreSpecific_AddSelected,
                            SmallIcon = ImageResources.Add_16,
                        });
            }
        }

        private Boolean AddSelected_CanExecute()
        {
            //must have rows selected
            if (this.SelectedAvailableRows.Count == 0)
            {
                this.AddSelectedCommand.DisabledReason = Message.WorkpackageWizard_SelectStoreSpecific_AddSelected_DisabledReason;
                return false;
            }

            return true;
        }

        private void AddSelected_Executed()
        {
            AddRows(this.SelectedAvailableRows.ToList());
        }

        #endregion

        #region RemoveSelectedCommand

        private RelayCommand _removeSelectedCommand;

        /// <summary>
        /// Removes the selected rows from the assigned list
        /// </summary>
        public RelayCommand RemoveSelectedCommand
        {
            get
            {
                return _removeSelectedCommand ??
                       (_removeSelectedCommand =
                        new RelayCommand(p => RemoveSelected_Executed(), p => RemoveSelected_CanExecute())
                        {
                            FriendlyDescription = Message.WorkpackageWizard_SelectStoreSpecific_RemoveSelected,
                            SmallIcon = ImageResources.Delete_16,
                        });
            }
        }

        private Boolean RemoveSelected_CanExecute()
        {
            ////must have assigned rows
            if (this.SelectedAssignedRows.Count == 0)
            {
                this.RemoveSelectedCommand.DisabledReason = Message.WorkpackageWizard_SelectStoreSpecific_RemoveSelected_DisabledReason;
                return false;
            }

            return true;
        }

        private void RemoveSelected_Executed()
        {
            RemoveRows(this.SelectedAssignedRows.ToList());
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SelectStoreSpecificPlanogramsStepViewModel(WorkpackageWizardData data)
        {
            _workpackageData = data;

            _masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);
            _productHierarchyView.FetchMerchandisingHierarchyForCurrentEntity();

            if (_workpackageData.AssignedProductGroup != null)
            {
                this.SelectedProductGroup = _workpackageData.AssignedProductGroup;
                this.IsSelectFromStores = _workpackageData.AssignedStores.Any();

                if (_workpackageData.AssignedStores.Any())
                {
                    List<Int16> storeIds = _workpackageData.AssignedStores.Select(p => p.Id).ToList();
                    _assignedRows.AddRange(
                        LocationAndLocationSpaceRowViewModel.GetRowViewModels(App.ViewState.EntityId, _selectedProductGroup.Id, _masterLocationList)
                        .Where(p => storeIds.Contains(p.Location.Id)));
                    //_assignedRows.AddRange(_masterLocationList.Where(p => storeIds.Contains(p.Id)));
                }
                else
                {
                    List<Int32> planogramIds = _workpackageData.AssignedPlanograms.Select(p => p.Id).ToList();
                    _assignedRows.AddRange(_availableRows.Cast<PlanogramInfo>().Where(p => planogramIds.Contains(p.Id)));
                }
                _availableRows.RemoveRange(_assignedRows);
            }
            else
            {
                this.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(_productHierarchyView.Model.RootGroup);
            }
        }

        #endregion

        #region Event Handlers

        

        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to complete this step.
        /// </summary>
        public void CompleteStep()
        {
            _workpackageData.AssignedStores.Clear();
            _workpackageData.AssignedPlanograms.Clear();

            if (IsSelectFromStores)
            {
                _workpackageData.AssignedStores.AddRange(LocationInfoList.FetchByEntityIdLocationCodes(App.ViewState.EntityId, _assignedRows.Cast<LocationAndLocationSpaceRowViewModel>().Select(p => p.Location.Code)));
            }
            else
            {
                _workpackageData.AssignedPlanograms.AddRange(_assignedRows.Cast<PlanogramInfo>().ToList());
            }

            _workpackageData.AssignedProductGroup = _selectedProductGroup;
            _workpackageData.AssignedLocationPlanAssignmentList = _productGroupPlanAssignments;
        }

        private void OnSelectedProductGroupChanged()
        {
            _availableRows.Clear();
            _assignedRows.Clear();

            // Fetch product group plan assignments
            _productGroupPlanAssignments = LocationPlanAssignmentList.FetchByEntityIdProductGroupId(App.ViewState.EntityId, _selectedProductGroup.Id);
            UpdateAvailableSelections();
        }

        private void UpdateAvailableSelections()
        {
            if (_productGroupPlanAssignments != null)
            {
                if (IsSelectFromStores)
                {
                    List<Int16> storeIds = _productGroupPlanAssignments.Select(p => p.LocationId).ToList();
                    _availableRows.AddRange(
                        LocationAndLocationSpaceRowViewModel.GetRowViewModels(App.ViewState.EntityId, _selectedProductGroup.Id, _masterLocationList)
                        .Where(p => storeIds.Contains(p.Location.Id)));                    
                }
                else
                {
                    _availableRows.AddRange(PlanogramInfoList.FetchByIds(_productGroupPlanAssignments.Select(p => p.PlanogramId).Distinct()));
                }
            }
        }

        private void AddRows(List<object> rows)
        {
            _availableRows.RemoveRange(rows);
            _assignedRows.AddRange(rows);            
            this.SelectedAvailableRows.Clear();
        }

        private void RemoveRows(List<object> rows)
        {
            _availableRows.AddRange(rows);                
            _assignedRows.RemoveRange(rows);
            this.SelectedAssignedRows.Clear();
        }
        
        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return Message.WorkpackageWizardSelectStoreSpecificPlanogramsStep_StepHeader; }
        }

        public String StepDescription
        {
            get { return Message.WorkpackageWizardSelectStoreSpecificPlanogramsStep_StepDescription; }
        }

        public WizardWindow WizardWindow { get; internal set; }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedProductGroup = null;
                    _availableRows.Clear();
                    _assignedRows.Clear();        
                    _selectedAvailableRows = null;
                    _selectedAssignedRows = null;
                    _productHierarchyView = null;
                    _masterLocationList = null;
                    _productGroupPlanAssignments = null;
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }  
}