﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25460 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Linq;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Data;
using Galleria.Ccm.Model;
using System.Text.RegularExpressions;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SelectWorkflowStep.xaml
    /// </summary>
    public partial class SelectWorkflowStep : UserControl, IDisposable
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectWorkflowStepViewModel), typeof(SelectWorkflowStep),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SelectWorkflowStep senderControl = (SelectWorkflowStep)obj;

            if (e.OldValue != null)
            {
                SelectWorkflowStepViewModel oldModel = (SelectWorkflowStepViewModel)e.OldValue;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;

            }

            if (e.NewValue != null)
            {
                SelectWorkflowStepViewModel newModel = (SelectWorkflowStepViewModel)e.NewValue;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }

            senderControl.ReloadTaskNodes();
        }

        /// <summary>
        /// Returns the viewmodel context
        /// </summary>
        public SelectWorkflowStepViewModel ViewModel
        {
            get { return (SelectWorkflowStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SearchCriteria Property

        public static readonly DependencyProperty SearchCriteriaProperty =
            DependencyProperty.Register("SearchCriteria", typeof(String), typeof(SelectWorkflowStep),
            new PropertyMetadata(String.Empty, OnSearchCriteriaPropertyChanged));

        private static void OnSearchCriteriaPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SelectWorkflowStep senderControl = (SelectWorkflowStep)obj;

            senderControl._searchPattern = LocalHelper.GetRexexKeywordPattern(e.NewValue as String);


            if (senderControl.AvailableWorkflowsSource != null)
            {

                senderControl.AvailableWorkflowsSource.View.Refresh();
            }
        }

        public String SearchCriteria
        {
            get { return (String)GetValue(SearchCriteriaProperty); }
            set { SetValue(SearchCriteriaProperty, value); }
        }

        private String _searchPattern = String.Empty;

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public SelectWorkflowStep(IWizardStepViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel as SelectWorkflowStepViewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == SelectWorkflowStepViewModel.SelectedWorkflowTasksProperty.Path)
            {
                ReloadTaskNodes();
            }
        }

        /// <summary>
        /// Called whenever the engine tasks source has an item to process for
        /// filtering.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableWorkflowsSource_Filter(object sender, FilterEventArgs e)
        {
            WorkflowInfo info = e.Item as WorkflowInfo;
            if (info != null)
            {
                e.Accepted = Regex.IsMatch(info.Name, _searchPattern, RegexOptions.IgnoreCase);
            }
            else
            {
                e.Accepted = false;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reloads the tasks diagram
        /// </summary>
        private void ReloadTaskNodes()
        {
            var diagram = this.xTasksDiagram;

            if (diagram != null)
            {
                diagram.Items.Clear();

                if (this.ViewModel != null
                    && this.ViewModel.SelectedWorkflowTasks != null
                    && this.ViewModel.SelectedWorkflowTasks.Count > 0)
                {
                    diagram.IsAutoArrangeOn = false;

                    WorkpackagesTaskNode lastNode = null;
                    foreach (var task in this.ViewModel.SelectedWorkflowTasks.OrderBy(t => t.SequenceId))
                    {
                        WorkpackagesTaskNode taskNode = new WorkpackagesTaskNode(task);
                        diagram.Items.Add(taskNode);

                        taskNode.UpdateLayout();

                        if (lastNode != null)
                        {
                            diagram.Links.Add(new DiagramItemLink(lastNode, taskNode));
                        }

                        lastNode = taskNode;
                    }

                    diagram.IsAutoArrangeOn = true;
                }
            }
        }


        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                this.ViewModel = null;
                layoutPanel.Children.Clear();

                _isDisposed = true;
            }
        }

        #endregion

        
    }

}
