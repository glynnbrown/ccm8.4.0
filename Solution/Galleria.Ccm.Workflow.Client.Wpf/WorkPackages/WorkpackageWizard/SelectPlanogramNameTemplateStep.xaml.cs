﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// CCM-29026 : D.Pleasance
//  Created
#endregion
#region Version History: (CCM 8.3.0)
// V8-31831 : A.Probyn
//  Reworked for formulae.
#endregion
#endregion

using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using System.Windows.Input;
using Image = System.Windows.Controls.Image;
using System.Windows.Shapes;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using System;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Windows.Data;


namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SelectPlanogramNameTemplateStep.xaml
    /// </summary>
    public sealed partial class SelectPlanogramNameTemplateStep : UserControl
    {
        #region Fields

        #endregion

        #region Properties
        
        #region ViewModel Property

        private SelectPlanogramNameTemplateStepViewModel _viewModel; //unloaded reference to the viewmodel.

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectPlanogramNameTemplateStepViewModel), typeof(SelectPlanogramNameTemplateStep),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public SelectPlanogramNameTemplateStepViewModel ViewModel
        {
            get { return (SelectPlanogramNameTemplateStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            SelectPlanogramNameTemplateStep senderControl = (SelectPlanogramNameTemplateStep)obj;

            if (e.OldValue != null)
            {
                SelectPlanogramNameTemplateStepViewModel oldModel = (SelectPlanogramNameTemplateStepViewModel)e.OldValue;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.SelectedGroupFields.BulkCollectionChanged -= senderControl.ViewModel_ItemTypeFieldsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                SelectPlanogramNameTemplateStepViewModel newModel = (SelectPlanogramNameTemplateStepViewModel)e.NewValue;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.SelectedGroupFields.BulkCollectionChanged += senderControl.ViewModel_ItemTypeFieldsBulkCollectionChanged;
            }
        }

        #endregion
        
        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="viewModel"></param>
        public SelectPlanogramNameTemplateStep(IWizardStepViewModel viewModel)
        {
            InitializeComponent();
            _viewModel = viewModel as SelectPlanogramNameTemplateStepViewModel;

            this.Loaded += new RoutedEventHandler(SelectPlanogramNameTemplateStep_Loaded);
            this.Unloaded += new RoutedEventHandler(SelectPlanogramNameTemplateStep_Unloaded);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions when this step is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectPlanogramNameTemplateStep_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = _viewModel;

            //initialize the caret to the end
            this.ViewModel.CaretIndex = this.ViewModel.FieldText.Length;

            // Stop showing the busy cursor as loading the screen has finished.
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; })
                , DispatcherPriority.Background, null);
        }

        /// <summary>
        /// Carries out actions when this step is unloaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectPlanogramNameTemplateStep_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = null;
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == SelectPlanogramNameTemplateStepViewModel.CaretIndexProperty.Path)
            {
                if (this.xFormulaTextBox != null)
                {
                    this.xFormulaTextBox.CaretIndex = this.ViewModel.CaretIndex;
                }
            }
        }

        /// <summary>
        /// Called whenever the item type fields collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_ItemTypeFieldsBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //Reapply the prefilter
            String val = this.xFieldGridFilter.Text;
            if (!String.IsNullOrEmpty(val))
            {
                this.xFieldGridFilter.Text = null;
                this.xFieldGridFilter.Text = val;
            }
        }

        /// <summary>
        /// Called whenever the user double clicks on the field grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FieldGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (e.RowItem != null)
            {
                this.ViewModel.AddFieldToText((ObjectFieldInfo)e.RowItem);
            }
        }

        /// <summary>
        /// Called whenever the user double clicks on the operators list box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OperatorsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem item = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
            if (item != null)
            {
                this.ViewModel.AddOperator((ObjectFieldExpressionOperator)item.Content);
            }
        }

        private void xFormulaTextBox_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.CaretIndex = this.xFormulaTextBox.CaretIndex;
            }
        }

        private void xFormulaTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.CaretIndex = this.xFormulaTextBox.CaretIndex;
            }
        }

        /// <summary>
        /// Same functionality as the mouse double-click event to add selected field to the formula
        /// The space bar allows focus to move to the operator selection list for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FieldGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && FieldGrid.SelectedItem != null)
            {
                this.ViewModel.AddFieldToText((ObjectFieldInfo)FieldGrid.SelectedItem);
                e.Handled = true;
            }
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(OperatorsListBox);
            }
            else
            {
                return;
            }
        }
        /// <summary>
        /// Same functionality as mouse double-click event to add selected operator to the formula
        /// The space bar switches focus back to the field selection grid for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OperatorsListBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && OperatorsListBox.SelectedItem != null)
            {
                ListBoxItem item = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
                if (item != null)
                {
                    this.ViewModel.AddOperator((ObjectFieldExpressionOperator)item.Content);
                }
            }
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(FieldGrid);
            }
            else
            {
                return;
            }
        }
        #endregion

        #region Method

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (_isDisposed) return;

            Loaded -= SelectPlanogramNameTemplateStep_Loaded;

            ViewModel.Dispose();
            ViewModel = null;

            _isDisposed = true;
        }

        #endregion
    }
}