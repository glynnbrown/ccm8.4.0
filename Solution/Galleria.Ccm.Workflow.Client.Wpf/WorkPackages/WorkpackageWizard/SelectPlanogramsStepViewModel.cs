﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
// CCM-27095 : I.George
// changed the code to use hierachy selector for the group search. User can now add favourites
// CCM-27599 : J.Pickup
//  Added Entity to FetchBySearchCriteria

#endregion
#region Version History: (CCM 8.0.1)
// V8-28626 : D.Pleasance
//  Amended PlanInfoListView_ModelChanged so that _availablePlanogramRows is only updated if not ShowAllAssigned.
#endregion
#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion
#region Version History: (CCM 8.3.0)
// V8-32017 : N.Haywood
//  Added code to remember/select the previously selected planogram group
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using System.Runtime.CompilerServices;


namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controlling the Select Planograms step of the workpackage wizard.
    /// </summary>
    public sealed class SelectPlanogramsStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Constants
        internal const CustomColumnLayoutType ColumnLayoutType = CustomColumnLayoutType.Planogram;
        internal const String ScreenKey = "SelectPlanogramsStep";
        #endregion

        #region Fields
        private WorkpackageWizardData _workpackageData;
        private PlanogramInfoListViewModel _planInfoListView = new PlanogramInfoListViewModel();

        private PlanogramHierarchyViewModel _planHierarchyView = new PlanogramHierarchyViewModel();

        private readonly BulkObservableCollection<SelectPlanogramsStepRowView> _availablePlanogramRows = new BulkObservableCollection<SelectPlanogramsStepRowView>();
        private ReadOnlyBulkObservableCollection<SelectPlanogramsStepRowView> _availablePlanogramRowsRO;
        private Boolean _isPlanogramSearchResultsBusy;

        private readonly BulkObservableCollection<PlanogramInfo> _assignedPlanograms = new BulkObservableCollection<PlanogramInfo>();
        private ReadOnlyBulkObservableCollection<PlanogramInfo> _assignedPlanogramsRO;

        private readonly UserPlanogramGroupListViewModel _userFavouritePlanGroupsView = new UserPlanogramGroupListViewModel();

        private Boolean _showAllAssigned;
        private PlanogramGroup _planogramGroupSearchCriteria;
        private Boolean _isShowBreadCrumb;
        private String _planogramSearchCriteria;
        private String _currentEntityName;
        

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsPlanogramSearchResultsBusyProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(p => p.IsPlanogramSearchResultsBusy);
        public static readonly PropertyPath PlanogramRowsProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(p => p.PlanogramRows);
        public static readonly PropertyPath ShowAllAssignedProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(p => p.ShowAllAssigned);
        public static readonly PropertyPath PlanogramGroupSearchCriteriaProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(p => p.PlanogramGroupSearchCriteria);
        public static readonly PropertyPath AssignedPlanogramsProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(p => p.AssignedPlanograms);
        public static readonly PropertyPath PlanogramHierarchyViewProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(p => p.PlanogramHierarchyView);
        public static readonly PropertyPath UserFavouriteGroupsViewProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(o => o.UserFavouriteGroupsView);
        public static readonly PropertyPath PlanogramSearchCriteriaProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(o => o.PlanogramSearchCriteria);
        public static readonly PropertyPath RefreshPlanogramsCommandProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(o => o.RefreshPlanogramsCommand);
        public static readonly PropertyPath IsShowBreadCrumbProperty = WpfHelper.GetPropertyPath<SelectPlanogramsStepViewModel>(o => o.IsShowBreadCrumb);


        #endregion

        #region Properties

        /// <summary>
        /// Returns true if details required by
        /// this step are valid and complete.
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get
            {
                if (_assignedPlanograms.Count == 0)
                {
                    return false;
                }

                return true;
            }

        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizard_SelectPlanogramsStep_StepDisabledReason; }
        }

        public Boolean IsShowBreadCrumb
             {
            get { return _isShowBreadCrumb; }
          
        }
      
        /// <summary>
        /// Returns the list of planograms which may be selected.
        /// </summary>
        public ReadOnlyBulkObservableCollection<SelectPlanogramsStepRowView> PlanogramRows
        {
            get
            {
                if (_availablePlanogramRowsRO == null)
                {
                    _availablePlanogramRowsRO = new ReadOnlyBulkObservableCollection<SelectPlanogramsStepRowView>(_availablePlanogramRows);
                }
                return _availablePlanogramRowsRO;
            }
        }

        /// <summary>
        /// Returns true if the PlanogramSearchResults property is loading
        /// </summary>
        public Boolean IsPlanogramSearchResultsBusy
        {
            get { return _isPlanogramSearchResultsBusy; }
            private set
            {
                _isPlanogramSearchResultsBusy = value;
                OnPropertyChanged(IsPlanogramSearchResultsBusyProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the search results 
        /// should show assigned plans.
        /// </summary>
        public Boolean ShowAllAssigned
        {
            get { return _showAllAssigned; }
            set
            {
                _showAllAssigned = value;
                OnPropertyChanged(ShowAllAssignedProperty);
                 _isShowBreadCrumb = !value;
                OnPropertyChanged(IsShowBreadCrumbProperty);
               UpdateAvailablePlanogramInfos();
                
            }
        }

        /// <summary>
        /// Gets/Sets the planogram group to filter search results by.
        /// </summary>
        public PlanogramGroup PlanogramGroupSearchCriteria
        {
            get { return _planogramGroupSearchCriteria; }
            set
            {
                 _planogramGroupSearchCriteria = value;
                  OnPropertyChanged(PlanogramGroupSearchCriteriaProperty);
                  OnPlanogramGroupSearchCriteriaChanged();
          
            }
        }

        /// <summary>
        /// Returns the master planogram hierarchy view.
        /// </summary>
        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return _planHierarchyView; }
        }

        /// <summary>
        /// Returns the view of the current user favourite groups.
        /// </summary>
        public UserPlanogramGroupListViewModel UserFavouriteGroupsView
        {
            get { return _userFavouritePlanGroupsView; }
        }

        /// <summary>
        /// Called whenever the planogram 
        /// </summary>
        private void OnPlanogramSearchCriteriaChanged()
        {
            UpdateAvailablePlanogramInfos();
        }

        /// <summary>
        /// Gets/Sets the search criteria to use when fetching planograms.
        /// </summary>
        public String PlanogramSearchCriteria
        {
            get { return _planogramSearchCriteria; }
            set
            {
                _planogramSearchCriteria = value;
                OnPropertyChanged(PlanogramSearchCriteriaProperty);
                if (!ShowAllAssigned)
                {
                    UpdateAvailablePlanogramInfos();
                }

            }
        }

        /// <summary>
        /// Returns the collection of planograms which have been assigned
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramInfo> AssignedPlanograms
        {
            get
            {
                if (_assignedPlanogramsRO == null)
                {
                    _assignedPlanogramsRO = new ReadOnlyBulkObservableCollection<PlanogramInfo>(_assignedPlanograms);
                }
                return _assignedPlanogramsRO;
            }
        }

        #endregion

        #region Commands

        #region RefreshPlanogramsCommand

        private RelayCommand _refreshPlanogramsCommand;

        /// <summary>
        /// Refreshes the current available planograms
        /// </summary>
        public RelayCommand RefreshPlanogramsCommand
        {
            get
            {
                if (_refreshPlanogramsCommand == null)
                {
                    _refreshPlanogramsCommand = new RelayCommand(
                        p => RefreshPlanograms_Executed(),
                        p => RefreshPlanograms_CanExecute())
                    {
                        FriendlyName = Message.PlanRepository_Refresh,
                        SmallIcon = ImageResources.PlanRepository_Refresh16,
                        InputGestureKey = Key.F5,
                    };

                }
                return _refreshPlanogramsCommand;
            }
        }

        private Boolean RefreshPlanograms_CanExecute()
        {
            if (PlanogramGroupSearchCriteria == null)
            {
                return false;
            }
            if (ShowAllAssigned == true)
            {
                return false;
            }

            return true;
        }

        private void RefreshPlanograms_Executed()
        {
            UpdateAvailablePlanogramInfos();
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SelectPlanogramsStepViewModel(WorkpackageWizardData data)
        {
            _availablePlanogramRows.BulkCollectionChanged += AvailablePlanogram_BulkCollectionChanged;
            _workpackageData = data;
            _planInfoListView.ModelChanged += PlanInfoListView_ModelChanged;

            //load in any infos for assigned plans that we have
            _assignedPlanograms.AddRange(_workpackageData.AssignedPlanograms);

            // connects to the repository to fetch entity details
            OnRepositoryConnectionChanged();

            PlanogramGroupViewModel lastPlanogramGroupViewModel = null;
            if (App.ViewState.SessionPlanogramGroupSelection != null)
            {
                lastPlanogramGroupViewModel = this._planHierarchyView.
                    EnumerateAllGroupViews().
                    FirstOrDefault(p =>
                        p.PlanogramGroup.Id == App.ViewState.SessionPlanogramGroupSelection.PlanogramGroupId);
            }
            if (lastPlanogramGroupViewModel == null)
            {
                this.PlanogramGroupSearchCriteria = _planHierarchyView.Model.RootGroup;
            }
            else
            {
                this.PlanogramGroupSearchCriteria = lastPlanogramGroupViewModel.PlanogramGroup;
            }

            // if plans are already assigned then show them immediately.
            this.ShowAllAssigned = (data.AssignedPlanograms.Count > 0);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the plan info list model changes
        /// </summary>
        /// <param namenu="sender"></param>
        /// <param name="e"></param>
        private void PlanInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramInfoList> e)
        {
            if (!ShowAllAssigned)
            {
                if (_availablePlanogramRows.Count > 0) _availablePlanogramRows.Clear();

                if (e.NewModel != null)
                {
                    List<SelectPlanogramsStepRowView> resultRows = new List<SelectPlanogramsStepRowView>(e.NewModel.Count);

                    Int32[] assignedIds = _assignedPlanograms.Select(p => p.Id).ToArray();
                    foreach (PlanogramInfo info in _planInfoListView.Model)
                    {

                        resultRows.Add(new SelectPlanogramsStepRowView(info, assignedIds.Contains(info.Id)));
                    }
                    _availablePlanogramRows.AddRange(resultRows);
                }
            }
            this.IsPlanogramSearchResultsBusy = false;
        }

        /// <summary>
        /// Called whenever the items held by the planogram search results collection change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailablePlanogram_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (SelectPlanogramsStepRowView row in e.ChangedItems)
                    {
                        row.IsSelectedChanged += RowView_IsSelectedChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (SelectPlanogramsStepRowView row in e.ChangedItems)
                    {
                        row.IsSelectedChanged -= RowView_IsSelectedChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (SelectPlanogramsStepRowView row in e.ChangedItems)
                        {
                            row.IsSelectedChanged -= RowView_IsSelectedChanged;
                        }
                    }
                    foreach (SelectPlanogramsStepRowView row in PlanogramRows)
                    {
                        row.IsSelectedChanged += RowView_IsSelectedChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the IsSelected value of a row changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RowView_IsSelectedChanged(object sender, EventArgs e)
        {
            SelectPlanogramsStepRowView row = (SelectPlanogramsStepRowView)sender;

            if (row.IsSelected)
            {
                if (!_assignedPlanograms.Any(a => a.Id == row.Model.Id))
               {
                    _assignedPlanograms.Add(row.Model);
               }
            }
            else
            {
             _assignedPlanograms.Remove(_assignedPlanograms.FirstOrDefault(p => p.Id == row.Model.Id));
             
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to complete this step.
        /// </summary>
        public void CompleteStep()
        {
            _workpackageData.AssignedPlanograms.Clear();
            _workpackageData.AssignedPlanograms.AddRange(_assignedPlanograms);
        }


        /// <summary>
        /// Called whenever the value of the SelectedPlanogramGroup changes
        /// </summary>
        private void OnPlanogramGroupSearchCriteriaChanged()
        {
            UpdateAvailablePlanogramInfos();
        }

        /// <summary>
        /// Updates the collection of available planograms.
        /// </summary>
        private void UpdateAvailablePlanogramInfos()
        {
            if (_availablePlanogramRows.Count > 0) _availablePlanogramRows.Clear();

            PlanogramGroup selectedGroup = this.PlanogramGroupSearchCriteria;

            Int32? planGroupId = null;
            if (selectedGroup != null && ShowAllAssigned != true)
            {
                if (String.IsNullOrEmpty(PlanogramSearchCriteria)
                    || selectedGroup.Id != _planHierarchyView.Model.RootGroup.Id)
                {
                    planGroupId = selectedGroup.Id;
                }

                //start an async search.
                _planInfoListView.FetchBySearchCriteriaAsync(PlanogramSearchCriteria, planGroupId, App.ViewState.EntityId);
                this.IsPlanogramSearchResultsBusy = true;
            }

           else if (this.ShowAllAssigned)
            {
                List<SelectPlanogramsStepRowView> resultRows = new List<SelectPlanogramsStepRowView>(_assignedPlanograms.Count);
                foreach (PlanogramInfo info in _assignedPlanograms)
                {
                    resultRows.Add(new SelectPlanogramsStepRowView(info, true));
                }
                _availablePlanogramRows.AddRange(resultRows);
            }
            else
            {
                _planInfoListView.Model = null;
            }

           
        }

        /// <summary>
        /// Method to connect to the repository and fetch planogram details
        /// </summary>
        private void OnRepositoryConnectionChanged()
        {
            _planHierarchyView.FetchForCurrentEntity();
            _userFavouritePlanGroupsView.FetchForCurrentUserAndEntity();

            this.PlanogramGroupSearchCriteria = _planHierarchyView.Model.RootGroup;
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return "Select Planograms"; }
        }

        public String StepDescription
        {
            get { return "Specify which planograms are to be affected by the workpackage."; }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    // Store the selected group's Id in the ViewState for next time.
                    App.ViewState.SessionPlanogramGroupSelection =
                        new SessionPlanogramGroupSelection(this.PlanogramGroupSearchCriteria);

                    _availablePlanogramRows.Clear();

                    _availablePlanogramRows.BulkCollectionChanged -= AvailablePlanogram_BulkCollectionChanged;

                    _planInfoListView.ModelChanged -= PlanInfoListView_ModelChanged;
                    _planInfoListView.Dispose();

                    OnRepositoryConnectionChanged();
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    #region Supporting Classes

    public sealed class SelectPlanogramsStepRowView
    {
        #region Fields
        private readonly PlanogramInfo _plan;
        private Boolean _isSelected;
        private CalculatedValueResolver _calculatedValueResolver;
        #endregion

        #region Event

        public event EventHandler IsSelectedChanged;

        private void RaiseIsSelectedChanged()
        {
            if (IsSelectedChanged != null)
            {
                IsSelectedChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Properties

        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                RaiseIsSelectedChanged();
            }
        }

        public PlanogramInfo Model
        {
            get { return _plan; }
        }


        public String Name
        {
            get { return _plan.Name; }
        }

        /// <summary>
        /// Returns the resolver used for binding to calculated values.
        /// </summary>
        public CalculatedValueResolver CalculatedValueResolver
        {
            get
            {
                if (_calculatedValueResolver == null)
                    _calculatedValueResolver = new CalculatedValueResolver(GetCalculatedValue);

                return _calculatedValueResolver;
            }
        }
        
        #endregion

        #region Constructor

        public SelectPlanogramsStepRowView(PlanogramInfo planInfo, Boolean isSelected)
        {
            _plan = planInfo;
            _isSelected = isSelected;
        }

        #endregion

        #region Methods

        private object GetCalculatedValue(String expressionText)
        {
            ObjectFieldExpression expression = new ObjectFieldExpression(expressionText);
            expression.AddDynamicFieldParameters(PlanogramInfo.EnumerateDisplayableFieldInfos());

            expression.ResolveDynamicParameter += OnFieldExpressionResolveParameter;

            //evaluate
            Object returnValue = expression.Evaluate();

            expression.ResolveDynamicParameter -= OnFieldExpressionResolveParameter;

            return returnValue;
        }

        /// <summary>
        /// Called when resolving a calculated parameter.
        /// </summary>
        private void OnFieldExpressionResolveParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
        {
            e.Result = e.Parameter.FieldInfo.GetValue(this.Model);
        }

        #endregion
    }

    #endregion


}