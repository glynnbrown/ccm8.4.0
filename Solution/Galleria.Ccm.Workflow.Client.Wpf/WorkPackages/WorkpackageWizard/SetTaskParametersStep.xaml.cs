﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  TaskParameterHelper.GetHelper paramter change.
#endregion
#region Version History : (CCM830)
// V8-32357 : M.Brumby
//  [PCR01561] Ability Customise the names of Workflow Tasks
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SetTaskParametersStep.xaml
    /// </summary>
    public partial class SetTaskParametersStep : UserControl,IDisposable
    {
        #region Fields
        private readonly Dictionary<DataGridColumn, SetTaskParametersColumnDefinition> _colToDef = new Dictionary<DataGridColumn, SetTaskParametersColumnDefinition>();
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SetTaskParametersStepViewModel), typeof(SetTaskParametersStep),
            new PropertyMetadata(null));

        public SetTaskParametersStepViewModel ViewModel
        {
            get { return (SetTaskParametersStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stepViewModel"></param>
        public SetTaskParametersStep(IWizardStepViewModel stepViewModel)
        {
            InitializeComponent();
            this.ViewModel = stepViewModel as SetTaskParametersStepViewModel;

            LoadColumns();
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the bag grid selected cells collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BAG_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                if (e.RemovedCells != null)
                {
                    foreach (var cell in e.RemovedCells)
                    {
                        if (cell.Column == null) continue;

                        WorkpackageWizardPlanogram row = cell.Item as WorkpackageWizardPlanogram;
                        String propertyName = ExtendedDataGrid.GetBindingProperty(cell.Column);

                        var cellInfo = this.ViewModel.SelectedCells.FirstOrDefault(
                            c => c.Row == row && c.PropertyName == propertyName);

                        this.ViewModel.SelectedCells.Remove(cellInfo);
                    }
                }

                if (e.AddedCells != null)
                {
                    foreach (var cell in e.AddedCells)
                    {
                        if (!(cell.Item is WorkpackageWizardPlanogram)) continue;
                        if (cell.Column == null) continue;


                        WorkpackageWizardCellInfo cellInfo =
                            new WorkpackageWizardCellInfo((WorkpackageWizardPlanogram)cell.Item,
                                ExtendedDataGrid.GetBindingProperty(cell.Column));

                        this.ViewModel.SelectedCells.Add(cellInfo);

                    }
                }

            }

            //if (this.MultiInputPresenter != null)
            //{
            //    //Force a target update so that the template gets reselected.
            //    var dataTemplateSelector = this.MultiInputPresenter.ContentTemplateSelector;
            //    this.MultiInputPresenter.ContentTemplateSelector = null;
            //    this.MultiInputPresenter.ContentTemplateSelector = dataTemplateSelector;
            //}
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the column definitions.
        /// </summary>
        private void LoadColumns()
        {
            if (_isDisposed) return;

            _colToDef.Clear();

            foreach (var colDef in this.ViewModel.ColumnSet)
            {
                switch (colDef.ColumnType)
                {
                    case SetTaskParametersColumnType.Parameter:
                        {
                            var parameterTypeHelper = TaskParameterHelper.GetHelper(colDef.Parameter);
                            DataGridColumn col =
                                parameterTypeHelper.CreateNewParameterColumn(
                                colDef.Header, colDef.HeaderGroupingName, colDef.Path, 
                                colDef.IsReadOnly, colDef.ParameterDetails,
                                colDef.HeaderGroupingToolTip);


                            this.BAG.Columns.Add(col);
                            _colToDef.Add(col, colDef);
                        }
                        break;

                    #region Default / Text
                    default:
                    case SetTaskParametersColumnType.Text:
                        {
                            DataGridExtendedTextColumn col = new DataGridExtendedTextColumn();
                            col.Header = colDef.Header;
                            col.HeaderGroupNames.Add(new DataGridHeaderGroup() { HeaderName = colDef.HeaderGroupingName });

                            if (colDef.IsReadOnly)
                            {
                                col.Binding = new Binding(colDef.Path) { Mode = BindingMode.OneWay };
                                col.IsReadOnly = true;
                            }
                            else
                            {
                                col.Binding = new Binding(colDef.Path)
                                {
                                    Mode = BindingMode.TwoWay,
                                    UpdateSourceTrigger = UpdateSourceTrigger.LostFocus,
                                    ValidatesOnDataErrors = true
                                };
                            }

                            this.BAG.Columns.Add(col);
                            _colToDef.Add(col, colDef);
                        }
                        break;
                    #endregion

                }
            }

        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                BAG.SelectedCellsChanged -= BAG_SelectedCellsChanged;

                

                this.ViewModel = null;

                var columns = BAG.Columns.ToList();
                BAG.Columns.Clear();

                foreach (var c in columns)
                {
                    BindingOperations.ClearAllBindings(c);
                }

                _isDisposed = true;
            }
        }

        #endregion
    }


}
