﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
// V8-26705 : A.Kuszyk ~ Changed ProductSelectionViewModel to Common.Wpf version.
// V8-26950 : A.Kuszyk ~ Added SetValuesFromContentLookup().
#endregion

#region Version History : CCM 802
// V8-29002 : A.Kuszyk
//  Added content lookup support for Planogram sources.
#endregion

#region Version History : CCM 803
// V8-29609 : D.Pleasance
//  Added method overload for SetValue to pass 2 parameters
#endregion

#region Version History : CCM 810
// V8-29746 : A.Probyn
//  Updated to not lookup planogram id for blocking and sequence, as we now have the ids available.
//  Updated so that renumbering strategy and validation template values are looked up from the content lookup.
// V8-29077 : N.Haywood
//  Added SelectValues_CanExecute
// V8-30025 : N.Haywood
//  Added ClearValuesCommand
#endregion

#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  TaskParameterHelper.GetHelper paramter change.
// V8-30486 : M.Pettit
//  Child WorkpackageWizardParameters now have knowledge of their parent WorkpackageWizardPlanogram (this instance)
// V8-30566 : M.Pettit
//	Updated Model_PropertyChanged as child parameters of tasks other than the edited paraamters's task were being re-validated
//	Altered SelectValues_Executed to only return child parameters that are in the same task as the parent and have the correct ParentId
#endregion

#region Version History : CCM 820
// V8-30728 : A.Kuszyk
//   Added taskhelper IsDisabled to SelectValues_CanExecute.
// V8-30907 : A.Kuszyk
//  Added IsRequired properties.
#endregion
#region Version History : CCM 830
// V8-31830 : A.Probyn
//  Added OnValueChanged to SelectValues_Executed method so that any child paramter objects can be altered upon
//  the user selecting new values, in line with the Model_OnPropertyChanged.
//  Updated SelectValues_Executed so that collection count changes flag a change has occured too
// V8-31831 : A.Probyn
//  Extended so PlanogramNameTemplateNameSingle looks for in content lookup.
// V8-32086 : N.Haywood
//  Added category code
// V8-32133 : A.Probyn
//  Added new AlternateDisplayValue to return the helper value (if setup).
// V8-32589 : L.Ineson
//  Stopped IsRequired firing while copying values.
#endregion
#region Version History: CCM832
// CCM-18930 : G.Richards
// Added a call to the wizard windows wait cursor method so we can show progress for long parameter selection tasks.
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks;
using Galleria.Framework.ViewModel;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Input;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Abstract wrapper for WorkpackagePlanogramParameter
    /// </summary>
    public sealed class WorkpackageWizardParameter : ModelView<WorkpackagePlanogramParameter>, IDataErrorInfo
    {
        #region Fields
        private WorkflowTaskParameter _workflowParameter;
        private ITaskParameterHelper _taskTypeHelper;
        private WorkpackageWizardPlanogram _parent;
        private Boolean _suppressChildChanged;

        #endregion

        #region Properties

        /// <summary>
        /// Indiicates if the UI control for this parameter should respond to user input (i.e. not IsReadOnly).
        /// </summary>
        public Boolean IsHitTestVisible { get { return !IsReadOnly; } }

        /// <summary>
        /// Indicates if this parameter value is required.
        /// </summary>
        public Boolean IsRequired
        {
            get { return String.IsNullOrEmpty(Model.IsRequired(_workflowParameter)); }
        }

        /// <summary>
        /// The reason that <see cref="IsRequired"/> might be false.
        /// </summary>
        public String IsRequiredTooltip
        {
            get { return Model.IsRequired(_workflowParameter); }
        }

        /// <summary>
        /// Gets the parameter type.
        /// </summary>
        public TaskParameterType ParameterType
        {
            get { return _workflowParameter.ParameterDetails.ParameterType; }
        }

        /// <summary>
        /// Returns true if this parameter should be readonly.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return _workflowParameter.IsReadOnly; }
        }

        /// <summary>
        /// Itemsource for enum type parameters
        /// </summary>
        public EngineTaskParameterEnumValueInfoList EnumSource
        {
            get { return _workflowParameter.ParameterDetails.EnumValues; }
        }

        /// <summary>
        /// Returns the values collection
        /// </summary>
        public WorkpackagePlanogramParameterValueList Values
        {
            get { return Model.Values; }
        }

        /// <summary>
        /// Gets/Sets the first value.
        /// For use by single value parameters.
        /// </summary>
        public Object Value
        {
            get { return Model.Value; }
            set
            {
                Model.Value = value;
                //Validate();

                //OnPropertyChanged("Value");

            }
        }

        /// <summary>
        /// Returns true if this is multi input.
        /// </summary>
        public Boolean IsMultiInputType
        {
            get { return _taskTypeHelper.IsMultiValue; }
        }

        /// <summary>
        /// Returns an alternative display value if setup
        /// </summary>
        public Object AlternateDisplayValue
        {
            get { return _taskTypeHelper.AlternateDisplayValue; }
        }

        #endregion

        #region Constructor

        public WorkpackageWizardParameter(
            WorkpackagePlanogramParameter model, WorkflowTaskParameter workflowParameter, WorkpackageWizardPlanogram parent)
            : base(model)
        {
            _workflowParameter = workflowParameter;
            _taskTypeHelper = TaskParameterHelper.GetHelper(workflowParameter, model);
            _parent = parent;
            Validate();

            this.Model.PropertyChanged += Model_PropertyChanged;

            this.Model.Parent.ChildChanged += Parent_ChildChanged;
        }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="model"></param>
        /// <param name="workflowParameter"></param>
        public WorkpackageWizardParameter(
            WorkpackagePlanogramParameter model, WorkflowTaskParameter workflowParameter)
            : base(model)
        {
            _workflowParameter = workflowParameter;
            _taskTypeHelper = TaskParameterHelper.GetHelper(workflowParameter, model);
            Validate();

            this.Model.PropertyChanged += Model_PropertyChanged;

            this.Model.Parent.ChildChanged += Parent_ChildChanged;
        }

        #endregion

        #region SelectValuesCommand

        private RelayCommand _setValueCommand;

        /// <summary>
        /// Sets the value based on the parameter type.
        /// </summary>
        public RelayCommand SelectValuesCommand
        {
            get
            {
                if (_setValueCommand == null)
                {
                    _setValueCommand = new RelayCommand(
                        p => SelectValues_Executed(), p => SelectValues_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        SmallIcon = ImageResources.WorkpackageWizardSetTaskParametersStep_ShowValueSelector
                    };
                }
                return _setValueCommand;
            }
        }

        public Boolean SelectValues_CanExecute()
        {
            if (Model != null)
            {
                String disabledReason = Model.IsRequired(_workflowParameter);
                if (!String.IsNullOrEmpty(disabledReason))
                {
                    SelectValuesCommand.DisabledReason = disabledReason;
                    return false;
                }
            }
            return !this._workflowParameter.IsReadOnly;
        }

        private void SelectValues_Executed()
        {
            WorkpackagePlanogramParameterValueList originalValues = this.Values.Copy();

            Boolean valueOneStartedNull = originalValues.FirstOrDefault() == null || originalValues.FirstOrDefault().Value1 == null;
            Boolean valueTwoStartedNull = originalValues.FirstOrDefault() == null || originalValues.FirstOrDefault().Value2 == null;
            Boolean valueThreeStartedNull = originalValues.FirstOrDefault() == null || originalValues.FirstOrDefault().Value3 == null;

            Window parentWindow = Galleria.Framework.Controls.Wpf.Helpers.GetWindow<WizardWindow>();
            
            List<ITaskParameterValue> newValues = _taskTypeHelper.SelectValues(this.Values, parentWindow, this._parent.CategoryCode);

            ((WizardWindow)parentWindow).ViewModel.ShowWaitCursor(true);
            
            this.Values.Clear();
            foreach (ITaskParameterValue value in newValues)
            {
                this.Values.Add(WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(value));
            }

            Validate();
            OnPropertyChanged("Value");
            
            Boolean hasChangeOccurred = false;

            hasChangeOccurred = originalValues.FirstOrDefault() != null 
                && originalValues.FirstOrDefault().Value1 != null 
                && this.Values.FirstOrDefault() != null 
                && this.Values.FirstOrDefault().Value1 != null 
                && !originalValues.FirstOrDefault().Value1.Equals(this.Values.FirstOrDefault().Value1);

            if (originalValues.FirstOrDefault() != null 
                && originalValues.FirstOrDefault().Value2 != null 
                && this.Values.FirstOrDefault() != null 
                && this.Values.FirstOrDefault().Value2 != null 
                && !originalValues.FirstOrDefault().Value2.Equals(this.Values.FirstOrDefault().Value2))
            {
                hasChangeOccurred = true;
            }

            if (originalValues.FirstOrDefault() != null 
                && originalValues.FirstOrDefault().Value3 != null 
                && this.Values.FirstOrDefault() != null 
                && this.Values.FirstOrDefault().Value3 != null 
                && !originalValues.FirstOrDefault().Value3.Equals(this.Values.FirstOrDefault().Value3))
            {
                hasChangeOccurred = true;
            }

            if (valueOneStartedNull && this.Values.FirstOrDefault() != null && this.Values.FirstOrDefault().Value1 != null)
            {
                hasChangeOccurred = true;
            }

            if (valueTwoStartedNull && this.Values.FirstOrDefault() != null && this.Values.FirstOrDefault().Value2 != null)
            {
                hasChangeOccurred = true;
            }

            if (valueThreeStartedNull && this.Values.FirstOrDefault() != null && this.Values.FirstOrDefault().Value3 != null)
            {
                hasChangeOccurred = true;
            }

            //V8-31830 - check if values count has changed
            if (originalValues.Count != this.Values.Count)
            {
                hasChangeOccurred = true;
            }
            
            if (hasChangeOccurred)
            {
                IEnumerable<WorkpackagePlanogramParameter> childParameters = this.Model.GetChildParameters();
				foreach (var dependantParameter in childParameters)
				{
					dependantParameter.Value = dependantParameter.ParameterDetails.DefaultValue;
					_workflowParameter.Value = this.Model.Value;
				}

                //Perform any helper-specific tasks when the value changes (i.e. clear any child parameters)
                _taskTypeHelper.OnValueChanged();

                OnPropertyChanged("AlternateDisplayValue");
            }

            ((WizardWindow)parentWindow).ViewModel.ShowWaitCursor(false);
        }

        #endregion

        #region ClearValuesCommand

        private RelayCommand _clearValueCommand;

        /// <summary>
        /// Sets the value based on the parameter type.
        /// </summary>
        public RelayCommand ClearValuesCommand
        {
            get
            {
                if (_clearValueCommand == null)
                {
                    _clearValueCommand = new RelayCommand(
                        p => ClearValues_Executed())
                    {
                        FriendlyName = Message.WorkpackageWizard_ClearSelection,
                        SmallIcon = ImageResources.Delete_16
                    };
                }
                return _clearValueCommand;
            }
        }


        private void ClearValues_Executed()
        {
            this.Values.Clear();

            Validate();
            OnPropertyChanged("Value");
            OnPropertyChanged("AlternateDisplayValue");
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets values to those stored in the given Content Lookup, if appropriate values are found.
        /// </summary>
        /// <param name="contentLookup">The Content Lookup to take values from.</param>
        public void SetValuesFromContentLookup(ContentLookup contentLookup)
        {
            WorkpackagePlanogramParameterValue valueToAdd=null;
            switch(ParameterType)
            {
                case TaskParameterType.AssortmentNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.AssortmentName));
                    break;
                case TaskParameterType.BlockingNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.BlockingName));
                    break;
                case TaskParameterType.ClusterNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.ClusterName));
                    break;
                case TaskParameterType.ClusterSchemeNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.ClusterSchemeName));
                    break;
                case TaskParameterType.ConsumerDecisionTreeNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.ConsumerDecisionTreeName));
                    break;
                case TaskParameterType.InventoryProfileNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.InventoryProfileName));
                    break;
                case TaskParameterType.MetricProfileNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.MetricProfileName));
                    break;
                case TaskParameterType.MinorAssortmentNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.AssortmentMinorRevisionName));
                    break;
                case TaskParameterType.PerformanceSelectionNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.PerformanceSelectionName));
                    break;
                case TaskParameterType.ProductUniverseNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.ProductUniverseName));
                    break;
                case TaskParameterType.SequenceNameSingle:
                    valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue((contentLookup.SequenceName));
                    break;
                case TaskParameterType.PlanogramBlockingNameSingle:
                    {
                        //If a blocking is setup, use the linked blocking id (planogram id)
                        if (contentLookup.BlockingId.HasValue)
                        {
                            valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(
                                    contentLookup.BlockingName, contentLookup.BlockingId);
                        }
                        break; 
                    }
                case TaskParameterType.PlanogramSequenceNameSingle:
                    {
                        //If a sequence is setup, use the linked sequence id (planogram id)
                        if (contentLookup.SequenceId.HasValue)
                        {
                            valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(
                                    contentLookup.SequenceName, contentLookup.SequenceId);
                        }
                        break;
                    }
                case TaskParameterType.ValidationTemplateSingle:
                    {
                        //If a validation template is setup, use the linked template id
                        if (contentLookup.ValidationTemplateId.HasValue)
                        {
                            valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(
                                    contentLookup.ValidationTemplateName, contentLookup.ValidationTemplateId);
                        }
                        break;
                    }
                case TaskParameterType.RenumberingStrategyNameSingle:
                    {
                        //If a renumbering strategy is setup, use the linked strategy id
                        if (contentLookup.RenumberingStrategyId.HasValue)
                        {
                            valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(
                                    contentLookup.RenumberingStrategyName, contentLookup.RenumberingStrategyId);
                        }
                        break;
                    }
                case TaskParameterType.PlanogramNameTemplateNameSingle:
                    {
                        //If a planogram name template is setup, use the linked template id
                        if (contentLookup.PlanogramNameTemplateId.HasValue)
                        {
                            valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(
                                    contentLookup.PlanogramNameTemplateName, contentLookup.PlanogramNameTemplateId);
                        }
                        break;
                    }
                default:
                    break;
            };

            if (valueToAdd != null)
            {
                Values.Clear();
                Values.Add(valueToAdd);
            }

            Validate();
            OnPropertyChanged("Value");
            if (_taskTypeHelper != null) _taskTypeHelper.OnValueChanged();
        }

        public void SetValue(String parameterValue)
        {
            WorkpackagePlanogramParameterValue valueToAdd = null;

            valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue();
            valueToAdd.Value1 = parameterValue;

            Values.Clear();
            Values.Add(valueToAdd);

            Validate();
            OnPropertyChanged("Value");
        }

        public void SetValue(String parameterValue1, String parameterValue2)
        {
            WorkpackagePlanogramParameterValue valueToAdd = null;

            valueToAdd = WorkpackagePlanogramParameterValue.NewWorkpackagePlanogramParameterValue(parameterValue1, parameterValue2);
            
            Values.Clear();
            Values.Add(valueToAdd);

            Validate();
            OnPropertyChanged("Value");
        }

        public void Validate()
        {
            this.Error = _taskTypeHelper.Validate(this.Values, _taskTypeHelper.IsNullAllowed);
        }

        public void CopyValue(WorkpackageWizardParameter otherParameter)
        {
            if (otherParameter != this && !this.IsReadOnly
                && otherParameter.ParameterType == this.ParameterType)
            {
                if (IsMultiInputType)
                {
                    //supress the child changed handler otherwise
                    // this takes ages..
                    _suppressChildChanged = true;

                    if(this.Values.Any())this.Values.Clear();
                    foreach (var value in otherParameter.Values)
                    {
                        this.Values.Add(value.Copy());
                    }
                    Validate();
                    OnPropertyChanged("Value"); 
                    OnPropertyChanged("AlternateDisplayValue");

                    _suppressChildChanged = false;
                    OnPropertyChanged("IsRequired");
                    OnPropertyChanged("IsRequiredTooltip");
                }
                else
                {
                    this.Value = otherParameter.Value;
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when a workflow task parameter in the parent task changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Parent_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            if(_suppressChildChanged) return;

            if (e.ChildObject is WorkpackagePlanogramParameterValueList &&
                e.CollectionChangedArgs != null &&
                e.CollectionChangedArgs.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                OnPropertyChanged("IsRequired");
                OnPropertyChanged("IsRequiredTooltip");
            }
        }

        /// <summary>
        /// Forces the views value property changed event when the models changes externally. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                //Get the relevant helper for this task parameter
                if (_taskTypeHelper == null)
                {
                    _taskTypeHelper = TaskParameterHelper.GetHelper(_workflowParameter, this.Model);
                }

                //Validate the current parameter's value
                Validate();
                OnPropertyChanged("Value");

                //Perform any helper-specific tasks when the value changes (i.e. clear any child parameters)
                _taskTypeHelper.OnValueChanged();

                OnPropertyChanged("AlternateDisplayValue");

				//find all of its child parameters
                IEnumerable<WorkpackagePlanogramParameter> childParameters = this.Model.GetChildParameters();
				foreach (WorkpackagePlanogramParameter childParameter in childParameters)
                {
                    //Now find the corresponding  workpackage wizard parameter for each child
					WorkpackageWizardParameter wizardParameter = 
						this._parent.Parameters.FirstOrDefault(p=>
							p.Model.Id == childParameter.Id &&
							p.Model.ParameterDetails.Name == childParameter.ParameterDetails.Name &&
							p.Model.ParameterDetails.ParameterType == childParameter.ParameterDetails.ParameterType);
                    if (wizardParameter != null)
                    {
                        //and re-evaulate it against the new parent value
						wizardParameter.Validate();
                    }
                }
            }
            else if (e.PropertyName == "Values")
            {
                OnPropertyChanged("AlternateDisplayValue");
            }
        }

		void OldModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e.PropertyName == "Value")
			{
				WorkpackagePlanogramParameter matchingWorkpackagePlanogramParameter =
					this.Model.Parent.Parameters.First(p => p.ParameterDetails.Name == this.Model.ParameterDetails.Name);

				//Get the relevant helper for this task parameter
				if (_taskTypeHelper == null)
				{
					_taskTypeHelper = TaskParameterHelper.GetHelper(_workflowParameter, this.Model);
				}

				//Validate the current parameter's value
				Validate();
                OnPropertyChanged("Value");

				//Clear any child parameters if set
                _taskTypeHelper.OnValueChanged();

                OnPropertyChanged("AlternateDisplayValue");

				//Validate any child parameters
				if (matchingWorkpackagePlanogramParameter != null)
				{
					//Get the id of the workpackage planogram parameter
					Int32 id = matchingWorkpackagePlanogramParameter.Parent.
						Parameters.IndexOf(matchingWorkpackagePlanogramParameter);


					//find all of its child parameters
					List<WorkpackagePlanogramParameter> dependantWorkpackagePlanogramParameters =
						matchingWorkpackagePlanogramParameter.Parent.Parameters.
							Where(p => p.ParameterDetails.ParentId == id).ToList();

					foreach (WorkpackagePlanogramParameter childWorkpackagePlanogramParameter in dependantWorkpackagePlanogramParameters)
					{
						WorkpackageWizardParameter wizardParameter = this._parent.Parameters.FirstOrDefault(p => p.Model.Id == childWorkpackagePlanogramParameter.Id);
						if (wizardParameter != null)
						{
							wizardParameter.Validate();
						}
					}
				}
            }
            else if (e.PropertyName == "Values")
            {
                OnPropertyChanged("AlternateDisplayValue");
            }
		}
        
        #endregion

        #region IDataErrorInfo Members

        private String _error;

        public String Error
        {
            get { return _error; }
            private set
            {
                _error = value;
            }
        }

        public String this[String columnName]
        {
            get
            {
                return this.Error;
            }
        }

        #endregion

        #region IDisposable
        private Boolean _isDisposed;
        public override void Dispose()
        {
            if (!_isDisposed)
            {
                this.Model.PropertyChanged -= Model_PropertyChanged;
                this.Model.Parent.ChildChanged -= Parent_ChildChanged;
                _isDisposed = true;
            }

        }

        #endregion
    }

}
