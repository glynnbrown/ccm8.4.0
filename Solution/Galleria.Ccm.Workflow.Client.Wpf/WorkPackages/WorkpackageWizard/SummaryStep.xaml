﻿<!--Header Information-->
<!--Copyright © Galleria RTS Ltd 2014.-->
<!-- Version History : (8.0)
    CCM-25460 : L.Ineson ~ Created
-->
<!-- Version History : (8.03)
    V8-29641 : L.Ineson ~ Have hidden run now run later options.
-->
<!-- Version History : (8.10)
V8-29811 : A.Probyn
    Extended for PlanogramLocationTypeProperty
-->

<UserControl x:Class="Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard.SummaryStep"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:g="clr-namespace:Galleria.Framework.Controls.Wpf;assembly=Galleria.Framework.Controls.Wpf"
             xmlns:root="clr-namespace:Galleria.Ccm.Workflow.Client.Wpf"
             xmlns:common="clr-namespace:Galleria.Ccm.Workflow.Client.Wpf.Common"
             xmlns:lg="clr-namespace:Galleria.Ccm.Workflow.Client.Wpf.Resources.Language"
             xmlns:local="clr-namespace:Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard"
             x:Name="stepControl">

    <Grid DataContext="{Binding Path=ViewModel, ElementName=stepControl}" 
          Margin="30">
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="*"/>
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>


        <TextBlock x:Name="lblWorkPackageName" Grid.Row="0" Grid.Column="0" Margin="6"
            Text="{x:Static lg:Message.WorkpackageSummaryStep_WorkpackageName}"/>

        <g:ExtendedTextBox Grid.Row="0" Grid.Column="1" Margin="10,6,6,6" VerticalAlignment="Center"
                           Width="250" MaxLength="100"
                           Text="{Binding Path={x:Static local:SummaryStepViewModel.WorkpackageNameProperty}, 
                            Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}"
                           AutomationProperties.LabeledBy="{Binding ElementName=lblWorkPackageName}"/>


        <TextBlock Grid.Row="1" Grid.Column="0" Margin="6" VerticalAlignment="Center"
            Text="{x:Static lg:Message.WorkpackageSummaryStep_WorkflowName}"/>

        <TextBlock Grid.Row="1" Grid.Column="1" Margin="10,6,6,6" VerticalAlignment="Center"
            Text="{Binding Path={x:Static local:SummaryStepViewModel.WorkflowNameProperty}}"/>


        <TextBlock Grid.Row="2" Grid.Column="0" Margin="6" VerticalAlignment="Center"
            Text="{x:Static lg:Message.WorkpackageSummaryStep_PlanogramCount}"/>

        <TextBlock Grid.Row="2" Grid.Column="1" Margin="10,6,6,6" VerticalAlignment="Center"
            Text="{Binding Path={x:Static local:SummaryStepViewModel.PlanogramCountProperty}}"/>

        <TextBlock Grid.Row="3" Grid.Column="0" Margin="6"
            Text="{x:Static lg:Message.WorkpackageSummaryStep_FileLocationTitle}">
            <TextBlock.Visibility>
                <MultiBinding Converter="{StaticResource {x:Static g:ResourceKeys.ConverterMultiConditionToVisibility}}"
                        ConverterParameter="or">
                    <Binding Path="{x:Static local:SummaryStepViewModel.WorkpackageTypeProperty}"
                            Converter="{StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}"
                            ConverterParameter="equals:NewFromExisting"/>
                    <Binding Path="{x:Static local:SummaryStepViewModel.WorkpackageTypeProperty}"
                            Converter="{StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}"
                            ConverterParameter="equals:StoreSpecific"/>
                </MultiBinding>
            </TextBlock.Visibility>
        </TextBlock>

        <RadioButton Grid.Row="4" Grid.Column="1" Margin="6"
                  IsThreeState="False" GroupName="LocationType"
                  IsChecked="{Binding Path={x:Static local:SummaryStepViewModel.PlanogramLocationTypeProperty}, 
                                    ConverterParameter=equals:InNewSubFolder, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged,
                                    Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}}"
                                           HorizontalAlignment="Left">

            <RadioButton.Visibility>
                <MultiBinding Converter="{StaticResource {x:Static g:ResourceKeys.ConverterMultiConditionToVisibility}}"
                        ConverterParameter="or">
                    <Binding Path="{x:Static local:SummaryStepViewModel.WorkpackageTypeProperty}"
                            Converter="{StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}"
                            ConverterParameter="equals:NewFromExisting"/>
                    <Binding Path="{x:Static local:SummaryStepViewModel.WorkpackageTypeProperty}"
                            Converter="{StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}"
                            ConverterParameter="equals:StoreSpecific"/>
                </MultiBinding>
            </RadioButton.Visibility>
            <TextBlock FlowDirection="LeftToRight" Text="{x:Static lg:Message.WorkpackageSummaryStep_InNewSubFolderTitle}" />
        </RadioButton>


        <RadioButton Grid.Row="5" Grid.Column="1" Margin="6"
                  IsThreeState="False" GroupName="LocationType"
                  IsChecked="{Binding Path={x:Static local:SummaryStepViewModel.PlanogramLocationTypeProperty}, 
                                    ConverterParameter=equals:InSourceFolder, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged,
                                    Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}}"
                                           HorizontalAlignment="Left">
            <RadioButton.Visibility>
                <MultiBinding Converter="{StaticResource {x:Static g:ResourceKeys.ConverterMultiConditionToVisibility}}"
                        ConverterParameter="or">
                    <Binding Path="{x:Static local:SummaryStepViewModel.WorkpackageTypeProperty}"
                            Converter="{StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}"
                            ConverterParameter="equals:NewFromExisting"/>
                    <Binding Path="{x:Static local:SummaryStepViewModel.WorkpackageTypeProperty}"
                            Converter="{StaticResource {x:Static g:ResourceKeys.ConverterConditionToBool}}"
                            ConverterParameter="equals:StoreSpecific"/>
                </MultiBinding>
            </RadioButton.Visibility>
            <TextBlock FlowDirection="LeftToRight" Text="{x:Static lg:Message.WorkpackageSummaryStep_InSourceFolderTitle}" />
        </RadioButton>
        
        <!--<StackPanel  Grid.Row="3" Grid.ColumnSpan="2" Orientation="Horizontal" Margin="20,30,0,0">
            
            <RadioButton Content="Run Now"
                         IsChecked="{Binding Path={x:Static local:SummaryStepViewModel.RunNowProperty}, Mode=OneWay, UpdateSourceTrigger=PropertyChanged}"
                         GroupName="RunNowRadioGroup"/>

            <RadioButton Content="Run Later" Margin="20,0,0,0"
                         GroupName="RunNowRadioGroup"/>

        </StackPanel>-->
        

    </Grid>
</UserControl>
