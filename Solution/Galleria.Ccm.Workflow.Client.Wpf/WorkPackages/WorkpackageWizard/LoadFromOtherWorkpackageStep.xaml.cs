﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#endregion

using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for LoadFromOtherWorkpackageStep.xaml
    /// </summary>
    public sealed partial class LoadFromOtherWorkpackageStep : UserControl
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LoadFromOtherWorkpackageStepViewModel), typeof(LoadFromOtherWorkpackageStep),
            new PropertyMetadata(null));

        public LoadFromOtherWorkpackageStepViewModel ViewModel
        {
            get { return (LoadFromOtherWorkpackageStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SearchCriteria Property

        public static readonly DependencyProperty SearchCriteriaProperty =
            DependencyProperty.Register("SearchCriteria", typeof(String), typeof(LoadFromOtherWorkpackageStep),
            new PropertyMetadata(String.Empty, OnSearchCriteriaPropertyChanged));

        private static void OnSearchCriteriaPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LoadFromOtherWorkpackageStep senderControl = (LoadFromOtherWorkpackageStep)obj;

            senderControl._searchPattern = LocalHelper.GetRexexKeywordPattern(e.NewValue as String);


            if (senderControl.AvailableWorkpackagesSource != null)
            {

                senderControl.AvailableWorkpackagesSource.View.Refresh();
            }
        }

        public String SearchCriteria
        {
            get { return (String)GetValue(SearchCriteriaProperty); }
            set { SetValue(SearchCriteriaProperty, value); }
        }

        private String _searchPattern = String.Empty;

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stepViewModel"></param>
        public LoadFromOtherWorkpackageStep(IWizardStepViewModel stepViewModel)
        {
            InitializeComponent();
            this.ViewModel = stepViewModel as LoadFromOtherWorkpackageStepViewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the engine tasks source has an item to process for
        /// filtering.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableWorkpackagesSource_Filter(object sender, FilterEventArgs e)
        {
            WorkpackageInfo info = e.Item as WorkpackageInfo;
            if (info != null)
            {
                e.Accepted = Regex.IsMatch(info.Name, _searchPattern, RegexOptions.IgnoreCase);
            }
            else
            {
                e.Accepted = false;
            }
        }

        #endregion

        #region IDisposable Members

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(Boolean disposing)
        {
            if (!_isDisposed)
            {
                this.ViewModel = null;

                _isDisposed = true;
            }
        }

        #endregion

    }
}
