﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson
//  Created.
// V8-28046 : N.Foster
//  Added ModifyExisting workpackage type
#endregion
#region Version History: (CCM 8.0.2)
// V8-29026 : D.Pleasance
//  Added StoreSpecific \ SelectPlanogramNameTemplate
#endregion
#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Updated to use new StepDisabledReason property
#endregion
#region Version History: CCM832
// CCM-18930 : G.Richards
// Added a wait cursor method so we can show or hide the wait cursor triggered from the wizard parameter selection change.
#endregion
#endregion

using System;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controller for the workpackage wizard
    /// </summary>
    public sealed class WizardViewModel : ViewModelAttachedControlObject<WizardWindow>
    {
        #region Fields

        private Boolean? _dialogResult;
        private Int32? _createdPackageId;
        private WizardStep _currentWizardStep = WizardStep.SelectWorkpackageType;
        private IWizardStepViewModel _currentStepViewModel;
        private readonly WorkpackageWizardData _workpackageData;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath CurrentWizardStepProperty = WpfHelper.GetPropertyPath<WizardViewModel>(p => p.CurrentWizardStep);
        public static readonly PropertyPath CurrentStepViewModelProperty = WpfHelper.GetPropertyPath<WizardViewModel>(p => p.CurrentStepViewModel);
        public static readonly PropertyPath WorkpackageDataProperty = WpfHelper.GetPropertyPath<WizardViewModel>(p => p.WorkpackageData);

        //Commands
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<WizardViewModel>(p => p.NextCommand);
        public static readonly PropertyPath FinishCommandProperty = WpfHelper.GetPropertyPath<WizardViewModel>(p => p.FinishCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<WizardViewModel>(p => p.PreviousCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<WizardViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the created package id.
        /// </summary>
        public Int32? CreatedPackageId
        {
            get { return _createdPackageId; }
            private set { _createdPackageId = value; }
        }

        /// <summary>
        /// Returns the current step of the wizard
        /// </summary>
        public WizardStep CurrentWizardStep
        {
            get { return _currentWizardStep; }
            private set
            {
                _currentWizardStep = value;
                OnPropertyChanged(CurrentWizardStepProperty);

                OnCurrentWizardStepChanged(value);
            }
        }

        /// <summary>
        /// Returns the viewmodel for the current wizard step
        /// </summary>
        public IWizardStepViewModel CurrentStepViewModel
        {
            get { return _currentStepViewModel; }
            private set
            {
                IWizardStepViewModel oldValue = _currentStepViewModel;

                _currentStepViewModel = value;
                OnPropertyChanged(CurrentStepViewModelProperty);

                //dispose of the old value
                if (oldValue != null)
                {
                    oldValue.Dispose();
                }
            }
        }

        /// <summary>
        /// Returns a wrapper holding all of the data required
        /// to create the workpackage
        /// </summary>
        public WorkpackageWizardData WorkpackageData
        {
            get { return _workpackageData; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public WizardViewModel()
        {
            _workpackageData = WorkpackageWizardData.CreateNewWorkpackage();
            OnCurrentWizardStepChanged(this.CurrentWizardStep);
        }

        /// <summary>
        ///Creates a new instance of this type, loading the existing workpackage.
        /// </summary>
        /// <param name="existingWorkpackageInfo"></param>
        public WizardViewModel(WorkpackageInfo existingWorkpackageInfo)
        {
            _workpackageData = WorkpackageWizardData.LoadFromExisting(existingWorkpackageInfo.Id);

            //if we failed to load the workpackage then show an error and close.
            if (_workpackageData == null)
            {
                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                    existingWorkpackageInfo.Name, OperationType.Open);

                this.DialogResult = false;
            }
            else
            {
                // set the current step to the parameters screen
                this.CurrentWizardStep = WizardStep.SetTaskParameters;
            }
        }

        #endregion

        #region Commands

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// Advances the wizard to the next step.
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        private Boolean Next_CanExecute()
        {
            //must not be on the last step
            if (this.CurrentWizardStep == WizardStep.Summary)
            {
                this.NextCommand.DisabledReason = null;
                return false;
            }

            //the current step must be valid
            if (!this.CurrentStepViewModel.IsValidAndComplete)
            {
                this.NextCommand.DisabledReason = this.CurrentStepViewModel.StepDisabledReason;
                return false;
            }

            return true;
        }

        private void Next_Executed()
        {
            base.ShowWaitCursor(true);

            //Complete the previous step.
            this.CurrentStepViewModel.CompleteStep();

            WizardStep nextWizardStep = WizardStep.Summary;

            //select the next one.
            switch (this.CurrentWizardStep)
            {
                case WizardStep.SelectWorkpackageType:
                    {
                        switch (this.WorkpackageData.Workpackage.WorkpackageType)
                        {
                            case WorkpackageType.NewFromExisting:
                            case WorkpackageType.ModifyExisting:
                                nextWizardStep = WizardStep.SelectPlanograms;
                                break;

                            case WorkpackageType.StoreSpecific:
                                nextWizardStep = WizardStep.SelectStoreSpecific;
                                break;

                            //case WorkpackageType.UniqueStoreSpecific:
                            //    nextWizardStep = WizardStep.SelectUniqueStoreFixturing;
                            //    break;

                            case WorkpackageType.LoadFromWorkpackage:
                                nextWizardStep = WizardStep.LoadFromOtherWorkpackage;
                                break;

                            case WorkpackageType.NewFromFile:
                                nextWizardStep = WizardStep.SelectFiles;
                                break;

                            default: throw new NotImplementedException();
                        }
                    }
                    break;

                case WizardStep.SelectPlanograms:
                case WizardStep.SelectFiles:
                case WizardStep.SelectStores:
                case WizardStep.SelectUniqueStoreFixturing:
                case WizardStep.LoadFromOtherWorkpackage:
                case WizardStep.SelectPlanogramNameTemplate:
                    {
                        this.WorkpackageData.GenerateWorkpackagePlans();

                        nextWizardStep = WizardStep.SelectWorkflow;
                    }
                    break;

                case WizardStep.SelectStoreSpecific:
                    nextWizardStep = WizardStep.SelectPlanogramNameTemplate;
                    break;

                case WizardStep.SelectWorkflow:
                    {
                        this.WorkpackageData.GenerateTaskParameters();
                        nextWizardStep = WizardStep.SetTaskParameters;
                    }
                    break;

                case WizardStep.SetTaskParameters:
                    nextWizardStep = WizardStep.Summary;
                    break;

                case WizardStep.Summary:
                    //nothing to load as this is the last step
                    break;

                default:
                    throw new NotImplementedException();
            }

            this.CurrentWizardStep = nextWizardStep;

            //force a requery.
            this.NextCommand.RaiseCanExecuteChanged();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Finish

        private RelayCommand _finishCommand;

        /// <summary>
        /// Advances the wizard to the finish step.
        /// </summary>
        public RelayCommand FinishCommand
        {
            get
            {
                if (_finishCommand == null)
                {
                    _finishCommand = new RelayCommand(
                        p => Finish_Executed(),
                        p => Finish_CanExecute())
                    {
                        FriendlyName = Message.Generic_Finish,
                    };
                    base.ViewModelCommands.Add(_finishCommand);
                }
                return _finishCommand;
            }
        }

        private Boolean Finish_CanExecute()
        {
            //must be on the last step
            if (this.CurrentWizardStep != WizardStep.Summary)
            {
                return false;
            }

            //must be complete
            if (!this.CurrentStepViewModel.IsValidAndComplete)
            {
                return false;
            }

            return true;
        }

        private void Finish_Executed()
        {
            base.ShowWaitCursor(true);

            //complete the step
            this.CurrentStepViewModel.CompleteStep();

            //save the workpackage
            Boolean success = true;
            try
            {
               Workpackage package = this.WorkpackageData.SaveWorkpackage();
               _createdPackageId = package.Id;

               //execute immediately if requested.
               if (this.WorkpackageData.RunNow)
               {
                   package.Execute();
               }
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);

                Exception rootEx = ex.GetBaseException();
                LocalHelper.RecordException(rootEx);

                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                    this.WorkpackageData.Workpackage.Name, OperationType.Save);

                success = false;
            }

            base.ShowWaitCursor(false);

            //close this window
            if (success)
            {
                this.DialogResult = true;
            }
        }

        #endregion

        #region Previous

        private RelayCommand _previousCommand;

        /// <summary>
        /// Advances the wizard to the previous step.
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous,
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        private Boolean Previous_CanExecute()
        {
            //must not be on the first step.
            if (this.CurrentWizardStep == WizardStep.SelectWorkpackageType)
            {
                return false;
            }

            return true;
        }

        private void Previous_Executed()
        {
            WizardStep prevStep = WizardStep.SelectWorkpackageType;

            switch (this.CurrentWizardStep)
            {
                case WizardStep.SelectWorkpackageType:
                    //nothing to do as this is the first step
                    break;

                case WizardStep.LoadFromOtherWorkpackage:
                case WizardStep.SelectPlanograms:
                case WizardStep.SelectFiles:
                case WizardStep.SelectStoreSpecific:
                case WizardStep.SelectStores:
                case WizardStep.SelectUniqueStoreFixturing:
                    prevStep = WizardStep.SelectWorkpackageType;
                    break;

                case WizardStep.SelectPlanogramNameTemplate:
                    prevStep = WizardStep.SelectStoreSpecific;
                    break;

                case WizardStep.SelectWorkflow:
                    {
                        switch (this.WorkpackageData.Workpackage.WorkpackageType)
                        {
                            case WorkpackageType.NewFromExisting:
                            case WorkpackageType.ModifyExisting:
                                prevStep = WizardStep.SelectPlanograms;
                                break;

                            case WorkpackageType.StoreSpecific:
                                prevStep = WizardStep.SelectPlanogramNameTemplate;
                                break;

                            case WorkpackageType.NewFromFile:
                                prevStep = WizardStep.SelectFiles;
                                break;

                            //case WorkpackageType.UniqueStoreSpecific:
                            //    prevStep = WizardStep.SelectUniqueStoreFixturing;
                            //    break;

                            case WorkpackageType.LoadFromWorkpackage:
                                prevStep = WizardStep.LoadFromOtherWorkpackage;
                                break;

                            default: throw new NotImplementedException();
                        }
                    }
                    break;

                case WizardStep.SetTaskParameters:
                    prevStep = WizardStep.SelectWorkflow;
                    break;

                case WizardStep.Summary:
                    prevStep = WizardStep.SetTaskParameters;
                    break;

                default:
                    throw new NotImplementedException();
            }

            this.CurrentWizardStep = prevStep;
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Advances the wizard to the cancel step.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods
        public new void ShowWaitCursor(bool show)
        {
            base.ShowWaitCursor(show);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current wizard step changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnCurrentWizardStepChanged(WizardStep newValue)
        {
            base.ShowWaitCursor(true);

            //load the correct viewmodel
            switch (newValue)
            {
                case WizardStep.SelectWorkpackageType:
                    this.CurrentStepViewModel = new SelectCreationTypeStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.SelectPlanograms:
                    this.CurrentStepViewModel = new SelectPlanogramsStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.SelectFiles:
                    this.CurrentStepViewModel = new SelectFilesStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.SelectStoreSpecific:
                    this.CurrentStepViewModel = new SelectStoreSpecificPlanogramsStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.SelectPlanogramNameTemplate:
                    this.CurrentStepViewModel = new SelectPlanogramNameTemplateStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.LoadFromOtherWorkpackage:
                    this.CurrentStepViewModel = new LoadFromOtherWorkpackageStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.SelectStores:
                    this.CurrentStepViewModel = new SelectStoresStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.SelectUniqueStoreFixturing:
                    this.CurrentStepViewModel = new SelectUniqueStoreFixturingStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.SelectWorkflow:
                    this.CurrentStepViewModel = new SelectWorkflowStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.SetTaskParameters:
                    this.CurrentStepViewModel = new SetTaskParametersStepViewModel(this.WorkpackageData);
                    break;

                case WizardStep.Summary:
                    this.CurrentStepViewModel = new SummaryStepViewModel(this.WorkpackageData);
                    break;

                default: throw new NotImplementedException();

            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.CurrentStepViewModel = null;
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }

    #region Suppporting Classes

    /// <summary>
    /// Denotes the different steps that the 
    /// workpackage wizard may go through.
    /// </summary>
    public enum WizardStep
    {
        SelectWorkpackageType = 0,
        SelectPlanograms = 1,
        SelectStores = 2,
        SelectFiles = 3,
        SelectStoreSpecific = 4,
        SelectPlanogramNameTemplate = 5,
        SelectUniqueStoreFixturing = 6,
        LoadFromOtherWorkpackage = 7,
        SelectWorkflow = 8,
        SetTaskParameters = 9,
        Summary = 10
    }

    #endregion

}
