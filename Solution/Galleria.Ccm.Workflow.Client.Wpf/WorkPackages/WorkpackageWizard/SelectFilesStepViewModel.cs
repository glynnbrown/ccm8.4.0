﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-24779 : D.Pleasance ~ Created.
// V8-28414 : D.Pleasance
//  Amended to remember initial file directory when selecting files
#endregion
#region Version History: (CCM 8.0.3)
// V8-29483 : D.Pleasance
//  Amended to allow selection of pog files.
#endregion
#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion
#region Version History: (CCM 8.30)
// V8-32804 : L.Ineson
//  Corrected subfolder search so that it goes deeper than just 1 level.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Input;
using System.IO;
using System.Globalization;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controlling the Select files step of the workpackage wizard.
    /// </summary>
    public sealed class SelectFilesStepViewModel : ViewModelObject, IWizardStepViewModel, IDataErrorInfo
    {
        #region Constants
        internal const String ScreenKey = "SelectFilesStep";
        const String cDirectoryFileExtension = "*.pog|*.pln|*.psa|*.xmz";
        #endregion

        #region Fields

        private WorkpackageWizardData _workpackageData;

        private readonly BulkObservableCollection<SelectFilesStepRowView> _availableFiles = new BulkObservableCollection<SelectFilesStepRowView>();
        private ReadOnlyBulkObservableCollection<SelectFilesStepRowView> _availableFilesRO;

        private readonly BulkObservableCollection<SelectFilesStepRowView> _assignedFiles = new BulkObservableCollection<SelectFilesStepRowView>();
        private ReadOnlyBulkObservableCollection<SelectFilesStepRowView> _assignedFilesRO;

        private String _selectedDirectory;
        private Boolean _isSelectedDirectoryValid = true;
        private Boolean _selectFolderOption = true;
        private Boolean _includeSubfolders = true;
        private String _initialFileDirectory = String.Empty;

        #endregion

        #region Binding Property Paths

        //Properties       
        public static readonly PropertyPath SelectFolderOptionProperty = WpfHelper.GetPropertyPath<SelectFilesStepViewModel>(p => p.SelectFolderOption);
        public static readonly PropertyPath IncludeSubfoldersProperty = WpfHelper.GetPropertyPath<SelectFilesStepViewModel>(p => p.IncludeSubfolders);
        public static readonly PropertyPath AvailableFilesProperty = WpfHelper.GetPropertyPath<SelectFilesStepViewModel>(p => p.AvailableFiles);
        public static readonly PropertyPath AssignedFilesProperty = WpfHelper.GetPropertyPath<SelectFilesStepViewModel>(p => p.AssignedFiles);
        public static readonly PropertyPath SelectedDirectoryProperty = WpfHelper.GetPropertyPath<SelectFilesStepViewModel>(p => p.SelectedDirectory);
        public static readonly PropertyPath IsSelectedDirectoryValidProperty = WpfHelper.GetPropertyPath<SelectFilesStepViewModel>(p => p.IsSelectedDirectoryValid);

        //Commands
        public static readonly PropertyPath SelectDirectoryCommandProperty = WpfHelper.GetPropertyPath<SelectFilesStepViewModel>(p => p.SelectDirectoryCommand);
        public static readonly PropertyPath SelectFilesCommandProperty = WpfHelper.GetPropertyPath<SelectFilesStepViewModel>(p => p.SelectFilesCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets whether folder option selected.
        /// </summary>
        public Boolean SelectFolderOption
        {
            get { return _selectFolderOption; }
            set
            {
                _selectFolderOption = value;
                OnPropertyChanged(SelectFolderOptionProperty);                
            }
        }

        public Boolean IsSelectedDirectoryValid
        {
            get { return _isSelectedDirectoryValid; }
            private set
            {
                _isSelectedDirectoryValid = value;
                OnPropertyChanged(IsSelectedDirectoryValidProperty);
            }
        }

        /// <summary>
        /// Gets \ sets the selected folder.
        /// </summary>
        public String SelectedDirectory
        {
            get { return _selectedDirectory; }
            set
            {
                _selectedDirectory = value;
                OnPropertyChanged(SelectedDirectoryProperty);

                this.IsSelectedDirectoryValid = true;

                if (_availableFiles.Count > 0) _availableFiles.Clear();
                if (_assignedFiles.Count > 0) _assignedFiles.Clear();
                LoadPlansWithinDirectory(value);
            }
        }

        /// <summary>
        /// Gets
        /// </summary>
        public Int32 SelectedFilesCount
        {
            get { return _assignedFiles.Where(p=> p.IsSelected).Count(); }
        }

        /// <summary>
        /// Gets/Sets whether to include sub folders in directory search.
        /// </summary>
        public Boolean IncludeSubfolders
        {
            get { return _includeSubfolders; }
            set
            {
                _includeSubfolders = value;
                OnPropertyChanged(IncludeSubfoldersProperty);                
            }
        }

        /// <summary>
        /// Returns the list of planograms which may be selected.
        /// </summary>
        public ReadOnlyBulkObservableCollection<SelectFilesStepRowView> AvailableFiles
        {
            get
            {
                if (_availableFilesRO == null)
                {
                    _availableFilesRO = new ReadOnlyBulkObservableCollection<SelectFilesStepRowView>(_availableFiles);
                }
                return _availableFilesRO;
            }
        }

        /// <summary>
        /// The assigned plan files
        /// </summary>
        public ReadOnlyBulkObservableCollection<SelectFilesStepRowView> AssignedFiles
        {
            get
            {
                if (_assignedFilesRO == null)
                {
                    _assignedFilesRO = new ReadOnlyBulkObservableCollection<SelectFilesStepRowView>(_assignedFiles);
                }
                return _assignedFilesRO;
            }
        }

        #endregion

        #region Commands

        #region SelectDirectoryCommand

        private RelayCommand _selectDirectoryCommand;

        /// <summary>
        /// Select plan directory and loads plan files
        /// </summary>
        public RelayCommand SelectDirectoryCommand
        {
            get
            {
                if (_selectDirectoryCommand == null)
                {
                    _selectDirectoryCommand = new RelayCommand(
                        p => SelectDirectory_Executed(),
                        p => SelectDirectory_CanExecute())
                    {
                        FriendlyName = Message.WorkpackageWizard_SelectDirectory,
                        SmallIcon = ImageResources.Open_16
                    };

                }
                return _selectDirectoryCommand;
            }
        }

        private Boolean SelectDirectory_CanExecute()
        {
            return _selectFolderOption;
        }

        private void SelectDirectory_Executed()
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowser.ShowNewFolderButton = false;

            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.SelectedDirectory = folderBrowser.SelectedPath;
            }
        }

        public void LoadPlansWithinDirectory(String directory)
        {
            CommonHelper.GetWindowService().ShowWaitCursor();
            try
            {
                LoadDirectoryFiles(directory, _includeSubfolders);
            }
            catch (Exception)
            {
                this.IsSelectedDirectoryValid = false;

                //ModalMessage warning = new ModalMessage();
                //warning.Header = Message.WorkpackageWizardSelectFilesStep_LoadPlan;
                //warning.Description = String.Format(CultureInfo.CurrentCulture, Message.WorkpackageWizardSelectFilesStep_LoadPlan_Description, directory, ex.Message);
                //warning.ButtonCount = 1;
                //warning.Button1Content = Message.Generic_Ok;
                //warning.MessageIcon = ImageResources.Dialog_Error;
                //App.ShowWindow(warning, /*isModal*/true);
            }
            finally
            {
                CommonHelper.GetWindowService().HideWaitCursor();
            }
        }

        /// <summary>
        /// Loads all plans from the given directory and subfolders.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="includeSubfolders"></param>
        private void LoadDirectoryFiles(String dir, Boolean includeSubfolders)
        {
            List<SelectFilesStepRowView> resultRows = new List<SelectFilesStepRowView>();

            string[] fileFilters = cDirectoryFileExtension.Split('|');

            foreach (String filter in fileFilters)
            {
                foreach (String planFile in Directory.GetFiles(dir, filter))
                {
                    resultRows.Add(new SelectFilesStepRowView(planFile, true));
                }
            }

            _availableFiles.AddRange(resultRows);
            _assignedFiles.AddRange(resultRows);


            if (includeSubfolders)
            {
                //recurse
                foreach (String d in Directory.GetDirectories(dir))
                {
                    LoadDirectoryFiles(d, includeSubfolders);
                }
            }
        }

        #endregion

        #region SelectFilesCommand

        private RelayCommand _selectFilesCommand;

        /// <summary>
        /// Select plans to load
        /// </summary>
        public RelayCommand SelectFilesCommand
        {
            get
            {
                if (_selectFilesCommand == null)
                {
                    _selectFilesCommand = new RelayCommand(
                        p => SelectFiles_Executed(),
                        p => SelectFiles_CanExecute())
                    {
                        FriendlyName = Message.WorkpackageWizard_SelectFiles,
                        SmallIcon = ImageResources.Add_16
                    };

                }
                return _selectFilesCommand;
            }
        }

        private Boolean SelectFiles_CanExecute()
        {
            return !_selectFolderOption;
        }

        private void SelectFiles_Executed()
        {
            System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.Filter = Message.WorkpackageWizard_SelectFiles_Filter;
            dlg.Multiselect = true;
            dlg.InitialDirectory = _initialFileDirectory;

            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                List<SelectFilesStepRowView> resultRows = new List<SelectFilesStepRowView>();
                List<String> planFiles = _availableFiles.Select(p => p.PlanFile).ToList();

                foreach (String planFile in dlg.FileNames)
                {
                    if (!planFiles.Contains(planFile))
                    {
                        resultRows.Add(new SelectFilesStepRowView(planFile, true));
                    }
                }

                _initialFileDirectory = Path.GetDirectoryName(dlg.FileNames[0]);

                _availableFiles.AddRange(resultRows);
                _assignedFiles.AddRange(resultRows);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SelectFilesStepViewModel(WorkpackageWizardData data)
        {
            _availableFiles.BulkCollectionChanged += AvailableFiles_BulkCollectionChanged;
            _workpackageData = data;
            
            if (_workpackageData.AssignedFiles.Count > 0)
            {
                List<SelectFilesStepRowView> resultRows = new List<SelectFilesStepRowView>();

                foreach (String planFile in _workpackageData.AssignedFiles)
                {
                    resultRows.Add(new SelectFilesStepRowView(planFile, true));
                }

                _availableFiles.AddRange(resultRows);
                _assignedFiles.AddRange(resultRows);
            }

            _initialFileDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the items held by the planogram search results collection change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableFiles_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (SelectFilesStepRowView row in e.ChangedItems)
                    {
                        row.IsSelectedChanged += RowView_IsSelectedChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (SelectFilesStepRowView row in e.ChangedItems)
                    {
                        row.IsSelectedChanged -= RowView_IsSelectedChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (SelectFilesStepRowView row in e.ChangedItems)
                        {
                            row.IsSelectedChanged -= RowView_IsSelectedChanged;
                        }
                    }
                    foreach (SelectFilesStepRowView row in AvailableFiles)
                    {
                        row.IsSelectedChanged += RowView_IsSelectedChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the IsSelected value of a row changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RowView_IsSelectedChanged(object sender, EventArgs e)
        {
            SelectFilesStepRowView row = (SelectFilesStepRowView)sender;

            if (row.IsSelected)
            {
                if (!_assignedFiles.Any(a => a.PlanFile == row.PlanFile))
                {
                    _assignedFiles.Add(row);
                }
            }
            else
            {
                _assignedFiles.Remove(_assignedFiles.FirstOrDefault(p => p.PlanFile == row.PlanFile));
            }
        }

        #endregion

        #region Methods

        
        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return "Select Files"; }
        }

        public String StepDescription
        {
            get { return "Specify which files are to be affected by the workpackage."; }
        }

        public bool IsValidAndComplete
        {
            get
            {
                if (_assignedFiles.Count == 0)
                {
                    return false;
                }

                return true;
            }
        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizard_SelectFiles_StepDisabledReason; }
        }

        /// <summary>
        /// Carries out actions to complete this step.
        /// </summary>
        public void CompleteStep()
        {
            _workpackageData.AssignedFiles.Clear();
            _workpackageData.AssignedFiles.AddRange(_assignedFiles.Select(p => p.PlanFile));
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _availableFiles.Clear();
                    _availableFiles.BulkCollectionChanged -= AvailableFiles_BulkCollectionChanged;

                    _assignedFiles.Clear();
                }

                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[String columnName]
        {
            get 
            {
                if (columnName == SelectedDirectoryProperty.Path
                    && !this.IsSelectedDirectoryValid)
                {
                    return Message.WorkpackageWizardSelectFilesStep_InvalidDirectoryError;
                }
                
                return null;
            }
        }

        #endregion
    }

    #region Supporting Classes

    public sealed class SelectFilesStepRowView
    {
        #region Fields
        private String _planFile;
        private Boolean _isSelected;
        #endregion

        #region Event

        public event EventHandler IsSelectedChanged;

        private void RaiseIsSelectedChanged()
        {
            if (IsSelectedChanged != null)
            {
                IsSelectedChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Properties

        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                RaiseIsSelectedChanged();
            }
        }

        public String PlanFile
        {
            get { return _planFile; }
        }
        
        #endregion

        #region Constructor

        public SelectFilesStepRowView(String planFile, Boolean isSelected)
        {
            _planFile = planFile;
            _isSelected = isSelected;
        }

        #endregion
    }

    #endregion
}