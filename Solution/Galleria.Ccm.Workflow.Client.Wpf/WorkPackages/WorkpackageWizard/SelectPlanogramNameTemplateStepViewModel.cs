﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM802
// V8-29026 : D.Pleasance ~ Created.
#endregion

#region Version History: CCM811
// V8-30261 : M.Pettit
//  Renamed PlanogramNameTemplateSeparatorTypeHelper Underline to Underscore
#endregion
#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
// V8-31831 : A.Probyn
//  Reworked for formulae. Only difference from maintenance is that SelectedAttribute is readonly and hardcoded to planogram name.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Input;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.PlanogramNameTemplateMaintenance;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Text;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controlling the Select planogram name template step of the workpackage wizard.
    /// </summary>
    public sealed class SelectPlanogramNameTemplateStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Fields

        private WorkpackageWizardData _workpackageData;

        private readonly PlanogramNameTemplateInfoListViewModel _planogramNameTemplateInfoListViewModel = new PlanogramNameTemplateInfoListViewModel();

        private PlanogramNameTemplate _selectedPlanogramNameTemplate;
        private ReadOnlyCollection<FieldSelectorGroup> _fieldGroups;
        private FieldSelectorGroup _selectedFieldGroup;
        private FieldSelectorGroup _allFieldsGroup;

        private readonly BulkObservableCollection<ObjectFieldInfo> _selectedGroupFields = new BulkObservableCollection<ObjectFieldInfo>();
        private ReadOnlyBulkObservableCollection<ObjectFieldInfo> _selectedGroupFieldsRO;
        private ObjectFieldInfo _selectedField;

        private ReadOnlyCollection<ObjectFieldExpressionOperator> _availableOperators;

        private String _fieldFormula;
        private String _fieldFormulaError;
        private Int32 _caretIndex; //the position of the caret

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedPlanogramNameTemplateProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.SelectedPlanogramNameTemplate);
        public static readonly PropertyPath AvailableFieldGroupsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.AvailableFieldGroups);
        public static readonly PropertyPath SelectedFieldGroupProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedFieldGroup);
        public static readonly PropertyPath SelectedGroupFieldsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedGroupFields);
        public static readonly PropertyPath SelectedFieldProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedField);
        public static readonly PropertyPath AvailableOperatorsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.AvailableOperators);
        public static readonly PropertyPath BuilderFormulaProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.BuilderFormula);
        public static readonly PropertyPath FieldTextProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.FieldText);
        public static readonly PropertyPath SelectedAttributeProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.SelectedAttribute);
        public static readonly PropertyPath CaretIndexProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.CaretIndex);

        //Commands
        public static PropertyPath SelectPlanogramNameTemplateCommandProperty = WpfHelper.GetPropertyPath<SelectPlanogramNameTemplateStepViewModel>(p => p.SelectPlanogramNameTemplateCommand);
        public static PropertyPath ClearCommandProperty = WpfHelper.GetPropertyPath<SelectPlanogramNameTemplateStepViewModel>(p => p.ClearCommand);
        
        #endregion

        #region Properties

        public bool IsValidAndComplete
        {
            get
            {
                if (this.SelectedPlanogramNameTemplate.BuilderFormula.Length == 0)
                {
                    return false;
                }

                return true;
            }
        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizard_SelectPlanogramNameTemplate_StepDisabledReason; }
        }

        /// <summary>
        /// Returns the selected PlanogramNameTemplate model.
        /// </summary>
        public PlanogramNameTemplate SelectedPlanogramNameTemplate
        {
            get { return _selectedPlanogramNameTemplate; }
            private set
            {
                _selectedPlanogramNameTemplate = value;
                OnPropertyChanged(SelectedPlanogramNameTemplateProperty);
                OnPropertyChanged(SelectedAttributeProperty);
                OnPropertyChanged(BuilderFormulaProperty);
                OnSelectedPlanogramNameTemplateChanged();
            }
        }

        /// <summary>
        /// Gets the list of available field groups
        /// </summary>
        public ReadOnlyCollection<FieldSelectorGroup> AvailableFieldGroups
        {
            get { return _fieldGroups; }
        }
        /// <summary>
        /// Gets/Sets the selected field group
        /// </summary>
        public FieldSelectorGroup SelectedFieldGroup
        {
            get { return _selectedFieldGroup; }
            set
            {
                _selectedFieldGroup = value;
                OnPropertyChanged(SelectedFieldGroupProperty);

                OnSelectedFieldGroupChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of fields available for the selected item type.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ObjectFieldInfo> SelectedGroupFields
        {
            get
            {
                if (_selectedGroupFieldsRO == null)
                {
                    _selectedGroupFieldsRO = new ReadOnlyBulkObservableCollection<ObjectFieldInfo>(_selectedGroupFields);
                }
                return _selectedGroupFieldsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected field
        /// </summary>
        public ObjectFieldInfo SelectedField
        {
            get { return _selectedField; }
            set
            {
                _selectedField = value;
                OnPropertyChanged(SelectedFieldProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available operators.
        /// </summary>
        public ReadOnlyCollection<ObjectFieldExpressionOperator> AvailableOperators
        {
            get { return _availableOperators; }
        }

        /// <summary>
        /// Gets/Sets the text being edited by the user.
        /// </summary>
        public String FieldText
        {
            get { return _fieldFormula; }
            set
            {
                _fieldFormula = value;
                OnFieldTextChanged(value);
                OnPropertyChanged(FieldTextProperty);
            }
        }

        /// <summary>
        /// Gets the final text that will assigned.
        /// </summary>
        public String BuilderFormula
        {
            get { return _selectedPlanogramNameTemplate.BuilderFormula; }
            private set
            {
                _selectedPlanogramNameTemplate.BuilderFormula = value;
                OnPropertyChanged(BuilderFormulaProperty);
            }
        }

        public PlanogramNameTemplatePlanogramAttributeType SelectedAttribute
        {
            get { return PlanogramNameTemplatePlanogramAttributeType.PlanogramName; }
        }

        /// <summary>
        /// Gets/Sets the position of the caret on the text.
        /// </summary>
        public Int32 CaretIndex
        {
            get { return _caretIndex; }
            set
            {
                if (_caretIndex != value)
                {
                    _caretIndex = value;
                    OnPropertyChanged(CaretIndexProperty);
                }
            }
        }

        #endregion

        #region Commands

        #region SelectPlanogramNameTemplateCommand

        private RelayCommand _selectPlanogramNameTemplateCommand;

        /// <summary>
        /// Shows the select planogram name template dialog
        /// </summary>
        public RelayCommand SelectPlanogramNameTemplateCommand
        {
            get
            {
                if (_selectPlanogramNameTemplateCommand == null)
                {
                    _selectPlanogramNameTemplateCommand = new RelayCommand(
                        p => SelectPlanogramNameTemplate_Executed(),
                        p => SelectPlanogramNameTemplate_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                }
                return _selectPlanogramNameTemplateCommand;
            }
        }

        private Boolean SelectPlanogramNameTemplate_CanExecute()
        {
            //user must have get permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(PlanogramNameTemplate)))
            {
                this.SelectPlanogramNameTemplateCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SelectPlanogramNameTemplate_Executed()
        {
            PlanogramNameTemplateSelectionWindow planogramNameTemplateSelectionWindow = new PlanogramNameTemplateSelectionWindow();
            App.ShowWindow(planogramNameTemplateSelectionWindow, true);

            if (planogramNameTemplateSelectionWindow.SelectionResult != null)
            {
                this.SelectedPlanogramNameTemplate = PlanogramNameTemplate.FetchById(planogramNameTemplateSelectionWindow.SelectionResult.Id);
                this.SelectedFieldGroup = _fieldGroups[0];
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SelectPlanogramNameTemplateStepViewModel(WorkpackageWizardData data)
        {
            _workpackageData = data;

            List<FieldSelectorGroup> availableFieldGroups = PlanogramNameTemplateMaintenanceViewModel.PlanogramNameFieldSelectorGroups();
            _allFieldsGroup = new FieldSelectorGroup("All", availableFieldGroups.SelectMany(f => f.Fields).Distinct());

            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();
            groups.Add(_allFieldsGroup);
            groups.AddRange(availableFieldGroups);
            _fieldGroups = groups.AsReadOnly();
            this.SelectedFieldGroup = this.AvailableFieldGroups.First();

            _availableOperators = ObjectFieldExpression.GetSupportedOperators().AsReadOnly();

            _planogramNameTemplateInfoListViewModel.FetchAllForEntity();

            if (data.AssignedPlanogramNameTemplate != null)
            {
                this.SelectedPlanogramNameTemplate = data.AssignedPlanogramNameTemplate;
            }
            else
            {
                if (_planogramNameTemplateInfoListViewModel.Model.Any())
                {
                    this.SelectedPlanogramNameTemplate = PlanogramNameTemplate.FetchById(_planogramNameTemplateInfoListViewModel.Model.First().Id);
                }
                else
                {
                    this.SelectedPlanogramNameTemplate = PlanogramNameTemplate.NewPlanogramNameTemplate(App.ViewState.EntityId);
                }
            }

            OnSelectedPlanogramNameTemplateChanged();
        }

        #endregion

        #region Commands

        #region Clear

        private RelayCommand _clearCommand;

        /// <summary>
        /// Clears out the current formula
        /// </summary>
        public RelayCommand ClearCommand
        {
            get
            {
                if (_clearCommand == null)
                {
                    _clearCommand = new RelayCommand(
                        p => Clear_Executed())
                    {
                        FriendlyName = Message.PlanogramNameTemplateMaintenance_ClearFormula,
                        SmallIcon = ImageResources.PlanogramNameTemplateMaintenance_ClearFormula
                    };
                }
                return _clearCommand;
            }
        }

        private void Clear_Executed()
        {
            this.FieldText = null;
        }

        #endregion

        #endregion

        #region Event Handlers


        /// <summary>
        /// Adds the given field to the text.
        /// </summary>
        /// <param name="field"></param>
        public void AddFieldToText(ObjectFieldInfo field)
        {
            String addText = field.FieldFriendlyPlaceholder;
            Int32 caretIdx = Math.Max(0, this.CaretIndex);

            StringBuilder sb = new StringBuilder(this.FieldText);

            Int32 initialSize = sb.Length;

            //When caret is precluding an operator by either 1 or 2 indexes then we increase caret to insert next field in the correct position. 
            //(Fields always succeed Operands).
            if (caretIdx > 0 && caretIdx < sb.Length && (AvailableOperators.Any(t => t.Symbol == sb[caretIdx].ToString())))
            {
                caretIdx++;
            }
            else if (caretIdx > 0 && caretIdx + 1 < sb.Length && AvailableOperators.Any(t => t.Symbol == sb[caretIdx + 1].ToString()))
            {
                caretIdx++;
                caretIdx++;
            }

            if (caretIdx >= sb.Length)
            {
                sb.Append(" ");
                sb.Append(addText);
            }
            else
            {
                sb.Insert(caretIdx, " ");
                sb.Insert(caretIdx + 1, addText);
            }

            this.FieldText = sb.ToString().Trim();

            Int32 finalSize = sb.Length;
            this.CaretIndex = caretIdx + (finalSize - initialSize);
        }

        /// <summary>
        /// Adds the given operator to the end of the field text
        /// </summary>
        /// <param name="fieldOperator"></param>
        public void AddOperator(ObjectFieldExpressionOperator fieldOperator)
        {
            if (fieldOperator.IsFunction)
            {
                //add the function name plus an opening brace to the caret position
                String functionText = fieldOperator.Symbol + "(";
                this.FieldText = this.FieldText.Insert(Math.Max(0, this.CaretIndex), functionText);
                this.CaretIndex = Math.Max(0, this.CaretIndex) + functionText.Length;
                return;
            }



            String addText = fieldOperator.Symbol;
            Int32 origCaretIdx = Math.Max(0, this.CaretIndex);
            Int32 caretIdx = Math.Max(0, this.CaretIndex);
            StringBuilder sb = new StringBuilder(this.FieldText);

            Boolean addBraces = (fieldOperator.Symbol != "(" && fieldOperator.Symbol != ")");

            //Add the start brace:
            if (addBraces)
            {
                String beforeCaret = (caretIdx > 0) ? this.FieldText.Substring(0, caretIdx - 1) : String.Empty;

                //get the index of the last sq bracket
                Int32 startBraceIdx = 0;
                if (beforeCaret.Contains(ObjectFieldInfo.FieldStart))
                {
                    startBraceIdx = Math.Max(0, beforeCaret.LastIndexOf(ObjectFieldInfo.FieldStart) - 1);
                }

                //If we are not in {} allready then lets go and add them else we dont need to.
                Int32 openCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaStart);
                Int32 closeCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaEnd);

                //-1 for covering when not found (frst time only).
                if (closeCurlyIndex == -1)
                {
                    sb.Insert(startBraceIdx, ObjectFieldInfo.FormulaStart + " ");
                    caretIdx += 2;
                }
                else
                {
                    //HERE THE must be some check about ptr being after that last } else add
                    if (closeCurlyIndex < openCurlyIndex || caretIdx > closeCurlyIndex)
                    {
                        sb.Insert(startBraceIdx, ObjectFieldInfo.FormulaStart + " ");
                        caretIdx += 2;
                    }
                }

                //What is this doing and do we need it? probably wants moving into the conditions above
                if (startBraceIdx != 0)
                {
                    sb.Insert(startBraceIdx, " ");
                    caretIdx += 1;
                };
            }


            Int32 initialSize = sb.Length;

            if (caretIdx >= sb.Length)
            {
                sb.Append(" ");
                sb.Append(addText);

                //Add the end brace:
                if (addBraces)
                {
                    sb.Append(" " + ObjectFieldInfo.FormulaEnd);
                }

            }
            else
            {
                sb.Insert(caretIdx, " ");
                sb.Insert(caretIdx + 1, addText);
                sb.Insert(caretIdx + 1 + addText.Length, " ");

                //Add the end brace:
                if (addBraces)
                {
                    //Again, we only add the braces when we aren't already in a set {}.
                    Int32 openCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaStart);
                    Int32 closeCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaEnd);

                    if (closeCurlyIndex < openCurlyIndex)
                    {
                        sb.Insert(caretIdx + 1 + addText.Length + 1, " " + ObjectFieldInfo.FormulaEnd);
                    }
                }
            }


            this.FieldText = sb.ToString().Trim();

            Int32 finalSize = sb.Length;

            if (addBraces)
            {
                //if braces were added place the caret just within the closing one.
                this.CaretIndex = origCaretIdx + (finalSize - initialSize) - 1;
            }
            else
            {
                this.CaretIndex = origCaretIdx + (finalSize - initialSize);
            }
        }

        private void OnSelectedPlanogramNameTemplateChanged()
        {
            //set the text value
            this.FieldText = ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(this.BuilderFormula, _allFieldsGroup.Fields);

            //initialize the caret to the end
            this.CaretIndex =
                (!String.IsNullOrWhiteSpace(this.FieldText)) ?
                this.FieldText.Length
                : 0;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to complete this step.
        /// </summary>
        public void CompleteStep()
        {
            _workpackageData.AssignedPlanogramNameTemplate = this.SelectedPlanogramNameTemplate;
        }

        /// <summary>
        /// Called when the selected item type property value changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedFieldGroupChanged(FieldSelectorGroup newValue)
        {
            _selectedGroupFields.Clear();

            _selectedGroupFields.AddRange(newValue.Fields);
        }

        /// <summary>
        /// Called whenever the FieldText property value changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnFieldTextChanged(String newValue)
        {
            //update the field text
            this.BuilderFormula = ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(this.FieldText, _allFieldsGroup.Fields);

            _fieldFormulaError = ObjectFieldInfo.ValidateFieldText(newValue, _allFieldsGroup.Fields, true);
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return Message.WorkpackageWizardSelectPlanogramNameTemplateStep_StepHeader; }
        }

        public String StepDescription
        {
            get { return Message.WorkpackageWizardSelectPlanogramNameTemplateStep_StepDescription; }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _planogramNameTemplateInfoListViewModel.Dispose();
                    _selectedPlanogramNameTemplate = null;
                    _selectedField = null;
                    _selectedFieldGroup = null;
                    _availableOperators = null;
                    _allFieldsGroup = null;
                    _fieldGroups = null;
                    _selectedGroupFields.Clear();

                    base.IsDisposed = true;
                }
            }

        #endregion
        }
    }
}