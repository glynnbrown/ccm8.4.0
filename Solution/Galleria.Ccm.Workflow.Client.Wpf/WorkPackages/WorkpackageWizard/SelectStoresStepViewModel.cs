﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controller for the select stores wizard step.
    /// </summary>
    public sealed class SelectStoresStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Fields

        private readonly WorkpackageWizardData _data;
        private readonly LocationInfoListViewModel _locationListView = new LocationInfoListViewModel();

        private readonly BulkObservableCollection<SelectStoresStepRowView> _locationSearchResults = new BulkObservableCollection<SelectStoresStepRowView>();
        private ReadOnlyBulkObservableCollection<SelectStoresStepRowView> _locationSearchResultsRO;
        private Boolean _isLocationSearchResultsBusy;

        private readonly BulkObservableCollection<LocationInfo> _assignedLocations = new BulkObservableCollection<LocationInfo>();

        private Boolean _showAllAssigned;
        private String _nameSearchCriteria;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath IsLocationSearchResultsBusyProperty = WpfHelper.GetPropertyPath<SelectStoresStepViewModel>(p => p.IsLocationSearchResultsBusy);
        public static readonly PropertyPath LocationSearchResultsProperty = WpfHelper.GetPropertyPath<SelectStoresStepViewModel>(p => p.LocationSearchResults);
        public static readonly PropertyPath ShowAllAssignedProperty = WpfHelper.GetPropertyPath<SelectStoresStepViewModel>(p => p.ShowAllAssigned);
        public static readonly PropertyPath NameSearchCriteriaProperty = WpfHelper.GetPropertyPath<SelectStoresStepViewModel>(p => p.NameSearchCriteria);

        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this step is valid
        /// and ready to proceed
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get
            {
                //if (_assignedLocations.Count == 0)
                //{
                //    return false;
                //}

                return false;
            }
        }

        public String StepDisabledReason
        {
            get { return String.Empty; }
        }

        /// <summary>
        /// Returns the list of locations which may be selected.
        /// </summary>
        public ReadOnlyBulkObservableCollection<SelectStoresStepRowView> LocationSearchResults
        {
            get
            {
                if (_locationSearchResultsRO == null)
                {
                    _locationSearchResultsRO = new ReadOnlyBulkObservableCollection<SelectStoresStepRowView>(_locationSearchResults);
                }
                return _locationSearchResultsRO;
            }
        }

        /// <summary>
        /// Returns true if the LocationSearchResults property is loading
        /// </summary>
        public Boolean IsLocationSearchResultsBusy
        {
            get { return _isLocationSearchResultsBusy; }
            private set
            {
                _isLocationSearchResultsBusy = value;
                OnPropertyChanged(IsLocationSearchResultsBusyProperty);
            }
        }

        /// <summary>
        /// Gets/Sets whether the search results 
        /// should show assigned locs.
        /// </summary>
        public Boolean ShowAllAssigned
        {
            get { return _showAllAssigned; }
            set
            {
                _showAllAssigned = value;
                OnPropertyChanged(ShowAllAssignedProperty);

                RefreshSearchResults();
            }
        }

        /// <summary>
        /// Gets/Sets the name search criteria to use.
        /// </summary>
        public String NameSearchCriteria
        {
            get { return _nameSearchCriteria; }
            set
            {
                _nameSearchCriteria = value;
                OnPropertyChanged(NameSearchCriteriaProperty);

                if (!ShowAllAssigned)
                {
                    RefreshSearchResults();
                }
            }
        }


        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SelectStoresStepViewModel(WorkpackageWizardData data)
        {
            _data = data;
            _locationListView.FetchForCurrentEntityAsync();

            _locationSearchResults.BulkCollectionChanged += LocationSearchResults_BulkCollectionChanged;

            _locationListView.ModelChanged += LocationInfoListView_ModelChanged;

            //load in any infos for assigned plans that we have
            _assignedLocations.AddRange(_data.AssignedStores);


            //if plans are already assigned then show them immediately.
            this.ShowAllAssigned = (data.AssignedStores.Count > 0);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the plan info list model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<LocationInfoList> e)
        {
            if (_locationSearchResults.Count > 0) _locationSearchResults.Clear();

            if (e.NewModel != null)
            {
                List<SelectStoresStepRowView> resultRows = new List<SelectStoresStepRowView>(e.NewModel.Count);

                Int16[] assignedIds = _assignedLocations.Select(p => p.Id).ToArray();
                foreach (LocationInfo info in _locationListView.Model)
                {
                    if (String.IsNullOrEmpty(this.NameSearchCriteria) || Regex.IsMatch(info.Name, this.NameSearchCriteria))
                    {
                        resultRows.Add(new SelectStoresStepRowView(info, assignedIds.Contains(info.Id)));
                    }
                }
                _locationSearchResults.AddRange(resultRows);
            }

            this.IsLocationSearchResultsBusy = false;
        }

        /// <summary>
        /// Called whenever the items held by the location search results collection change.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationSearchResults_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (SelectStoresStepRowView row in e.ChangedItems)
                    {
                        row.IsSelectedChanged += RowView_IsSelectedChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (SelectStoresStepRowView row in e.ChangedItems)
                    {
                        row.IsSelectedChanged -= RowView_IsSelectedChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (SelectStoresStepRowView row in e.ChangedItems)
                        {
                            row.IsSelectedChanged -= RowView_IsSelectedChanged;
                        }
                    }
                    foreach (SelectStoresStepRowView row in LocationSearchResults)
                    {
                        row.IsSelectedChanged += RowView_IsSelectedChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Called whenever the is selected value of a row changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RowView_IsSelectedChanged(object sender, EventArgs e)
        {
            SelectStoresStepRowView row = (SelectStoresStepRowView)sender;

            if (row.IsSelected)
            {
                if (!_assignedLocations.Any(a => a.Id == row.Model.Id))
                {
                    _assignedLocations.Add(row.Model);
                }
            }
            else
            {
                _assignedLocations.Remove(row.Model);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to finalize 
        /// the selections made in this step
        /// </summary>
        public void CompleteStep()
        {
            _data.AssignedStores.Clear();
            _data.AssignedStores.AddRange(_assignedLocations);
        }

        /// <summary>
        /// Updates the search results
        /// </summary>
        private void RefreshSearchResults()
        {
            if (_locationSearchResults.Count > 0) _locationSearchResults.Clear();

            if (this.ShowAllAssigned)
            {
                List<SelectStoresStepRowView> resultRows = new List<SelectStoresStepRowView>(_assignedLocations.Count);
                foreach (LocationInfo info in _assignedLocations)
                {
                    resultRows.Add(new SelectStoresStepRowView(info, true));
                }
                _locationSearchResults.AddRange(resultRows);
            }
            else if (!String.IsNullOrEmpty(this.NameSearchCriteria))
            {
                this.IsLocationSearchResultsBusy = true;

                LocationInfoListView_ModelChanged(_locationListView, 
                    new ViewStateObjectModelChangedEventArgs<LocationInfoList>(ViewStateObjectModelChangeReason.Fetch,
                        _locationListView.Model, null, null));

            }
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return "Select Locations"; }
        }

        public String StepDescription
        {
            get { return "Specify which locations should be used to create store specific plans"; }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _locationListView.Dispose();
                }

                base.IsDisposed = true;
            }
        }

        #endregion

    }

    #region Supporting Classes

    public sealed class SelectStoresStepRowView
    {
        #region Fields
        private readonly LocationInfo _location;
        private Boolean _isSelected;
        #endregion

        #region Event

        public event EventHandler IsSelectedChanged;

        private void RaiseIsSelectedChanged()
        {
            if (IsSelectedChanged != null)
            {
                IsSelectedChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Properties

        public Boolean IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                RaiseIsSelectedChanged();
            }
        }

        public LocationInfo Model
        {
            get { return _location; }
        }

        public String Code
        {
            get { return _location.Code; }
        }

        public String Name
        {
            get { return _location.Name; }
        }

        #endregion

        #region Constructor

        public SelectStoresStepRowView(LocationInfo locInfo, Boolean isSelected)
        {
            _location = locInfo;
            _isSelected = isSelected;
        }

        #endregion

    }

    #endregion
}
