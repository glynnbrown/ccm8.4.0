﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Added new StepDisabledReason property
#endregion

#endregion


using System;
using System.Windows.Media;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Defines the expected members of a WorkpackageWizard step viewmodel
    /// </summary>
    public interface IWizardStepViewModel : IDisposable
    {
        /// <summary>
        /// Returns the step header text
        /// </summary>
        String StepHeader { get; }

        /// <summary>
        /// Returns the step description text
        /// </summary>
        String StepDescription { get; }

        /// <summary>
        /// Returns true if the step is valid and 
        /// complete and we can move on.
        /// </summary>
        Boolean IsValidAndComplete { get; }

        /// <summary>
        /// Returns the disabled reason for the current step (Default is blank)
        /// </summary>
        /// <remarks>Overwrite where needed</remarks>
        String StepDisabledReason { get; }

        /// <summary>
        /// Carries out actions to complete this step.
        /// </summary>
        void CompleteStep();
    }

}
