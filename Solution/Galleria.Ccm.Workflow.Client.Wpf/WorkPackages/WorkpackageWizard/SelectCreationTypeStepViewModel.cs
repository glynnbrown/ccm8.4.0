﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson
//  Created.
// V8-28046 : N.Foster
//  Added ModifyExisting workpackage type
#endregion
#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion
#endregion

using System;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controller for the select creation type wizard step
    /// </summary>
    public sealed class SelectCreationTypeStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Fields
        private readonly WorkpackageWizardData _workpackageData;
        private WorkpackageType _selectedCreationType;
        #endregion

        #region Binding Property Paths

        public static PropertyPath SelectedCreationTypeProperty = WpfHelper.GetPropertyPath<SelectCreationTypeStepViewModel>(p => p.SelectedWorkpackageType);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the selected creation type.
        /// </summary>
        public WorkpackageType SelectedWorkpackageType
        {
            get { return _selectedCreationType; }
            set
            {
                _selectedCreationType = value;
                OnPropertyChanged(SelectedCreationTypeProperty);
            }
        }

        /// <summary>
        /// Returns true if this step is valid
        /// and ready to proceed
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get 
            { 
                if (_selectedCreationType == WorkpackageType.Unknown) return false;
                
                return true; 
            }
        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizard_SelectCreationType_StepDisabledReason; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SelectCreationTypeStepViewModel(WorkpackageWizardData data)
        {
            _workpackageData = data;
            this.SelectedWorkpackageType = data.Workpackage.WorkpackageType;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to finalize 
        /// the selections made in this step
        /// </summary>
        public void CompleteStep()
        {
            _workpackageData.Workpackage.WorkpackageType = this.SelectedWorkpackageType;
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return Message.WorkpackageWizard_SelectCreationType_Header; }
        }

        public String StepDescription
        {
            get { return Message.WorkpackageWizard_SelectCreationType_Desc; }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                }

                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
