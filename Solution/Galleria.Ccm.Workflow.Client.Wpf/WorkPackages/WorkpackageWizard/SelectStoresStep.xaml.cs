﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SelectStoresStep.xaml
    /// </summary>
    public partial class SelectStoresStep : UserControl
    {
        #region Properties

        #region ViewModel Property

        private SelectStoresStepViewModel _viewModel; //unloaded reference to the viewmodel.

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectStoresStepViewModel), typeof(SelectStoresStep),
            new PropertyMetadata(null));

        public SelectStoresStepViewModel ViewModel
        {
            get { return (SelectStoresStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stepViewModel"></param>
        public SelectStoresStep(IWizardStepViewModel stepViewModel)
        {
            InitializeComponent();
            _viewModel = stepViewModel as SelectStoresStepViewModel;

            this.Loaded += new RoutedEventHandler(SelectStoresStep_Loaded);
            this.Unloaded += new RoutedEventHandler(SelectStoresStep_Unloaded);
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions when this step is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectStoresStep_Loaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = _viewModel;
        }

        /// <summary>
        /// Carries out actions when this step is unloaded.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectStoresStep_Unloaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel = null;
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            
        }

        /// <summary>
        /// Called when the user clicks the assign selected menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssignSelectedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (SelectPlanogramsStepRowView row in this.LocationsGrid.SelectedItems)
            {
                row.IsSelected = true;
            }
        }

        /// <summary>
        /// Called when the user clicks the unassign selected menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnassignSelectedMenuItem_Click(object sender, RoutedEventArgs e)
        {
            foreach (SelectPlanogramsStepRowView row in this.LocationsGrid.SelectedItems)
            {
                row.IsSelected = false;
            }
        }


        /// <summary>
        /// Called when the user types into a search textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchCriteriaTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.ViewModel != null && this.ViewModel.ShowAllAssigned)
            {
                this.ViewModel.ShowAllAssigned = false;
            }
        }

        #endregion

    }
}
