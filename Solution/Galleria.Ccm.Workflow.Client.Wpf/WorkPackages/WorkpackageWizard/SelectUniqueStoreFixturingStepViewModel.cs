﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion

#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Viewmodel controller for the select unique store fixturing step of the wizard
    /// </summary>
    public sealed class SelectUniqueStoreFixturingStepViewModel : ViewModelObject, IWizardStepViewModel
    {
        #region Properties

        /// <summary>
        /// Returns true if this step is valid
        /// and ready to proceed
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get { return false; }
        }

        public String StepDisabledReason
        {
            get { return String.Empty; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SelectUniqueStoreFixturingStepViewModel(WorkpackageWizardData data)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to finalize 
        /// the selections made in this step
        /// </summary>
        public void CompleteStep()
        {
            
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return "Select Unique Fixturing"; }
        }

        public String StepDescription
        {
            get { return null; }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
