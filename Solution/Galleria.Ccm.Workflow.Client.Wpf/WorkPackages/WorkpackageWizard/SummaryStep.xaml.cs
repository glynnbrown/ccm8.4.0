﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25460 : L.Ineson ~ Created.
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
//      Extended for PlanogramLocationType
#endregion

#region Version History: (CCM 8.20)
// V8-31095 : A.Probyn
//  Implemented new StepDisabledReason property
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard
{
    /// <summary>
    /// Interaction logic for SummaryStep.xaml
    /// </summary>
    public partial class SummaryStep : UserControl
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SummaryStepViewModel), typeof(SummaryStep),
            new PropertyMetadata(null));

        public SummaryStepViewModel ViewModel
        {
            get { return (SummaryStepViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="stepViewModel"></param>
        public SummaryStep(IWizardStepViewModel stepViewModel)
        {
            InitializeComponent();
            this.ViewModel = stepViewModel as SummaryStepViewModel;
        }

        #endregion
    }

    /// <summary>
    /// Viewmodel controller for the workpackage wizard summary step.
    /// </summary>
    public sealed class SummaryStepViewModel : ViewModelObject, IWizardStepViewModel, IDataErrorInfo
    {
        #region Fields
        private WorkpackageWizardData _data;
        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath WorkpackageNameProperty = WpfHelper.GetPropertyPath<SummaryStepViewModel>(p => p.WorkpackageName);
        public static readonly PropertyPath WorkflowNameProperty = WpfHelper.GetPropertyPath<SummaryStepViewModel>(p => p.WorkflowName);
        public static readonly PropertyPath PlanogramCountProperty = WpfHelper.GetPropertyPath<SummaryStepViewModel>(p => p.PlanogramCount);
        public static readonly PropertyPath RunNowProperty = WpfHelper.GetPropertyPath<SummaryStepViewModel>(p => p.RunNow);
        public static readonly PropertyPath PlanogramLocationTypeProperty = WpfHelper.GetPropertyPath<SummaryStepViewModel>(p => p.PlanogramLocationType);
        public static readonly PropertyPath WorkpackageTypeProperty = WpfHelper.GetPropertyPath<SummaryStepViewModel>(p => p.WorkpackageType);

        #endregion

        #region Properties

        /// <summary>
        /// Returns true if this step is valid
        /// and ready to proceed
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get { return !String.IsNullOrEmpty(this.WorkpackageName); }
        }

        public String StepDisabledReason
        {
            get { return Message.WorkpackageWizard_SummaryStep_StepDisabledReason; }
        }

        /// <summary>
        /// Gets/Sets the name of the workpackage
        /// </summary>
        public String WorkpackageName
        {
            get { return _data.Workpackage.Name; }
            set
            {
                _data.Workpackage.Name = value;
                OnPropertyChanged(WorkpackageNameProperty);
            }
        }

        /// <summary>
        /// Gets the name of the assigned workflow
        /// </summary>
        public String WorkflowName
        {
            get { return _data.Workflow.Name; }
        }

        /// <summary>
        /// Gets a count of the planograms to be output.
        /// </summary>
        public Int32 PlanogramCount
        {
            get { return _data.WorkpackagePlans.Count; }
        }

        /// <summary>
        /// Gets/Sets whether the package should be executed immediately.
        /// </summary>
        public Boolean RunNow
        {
            get { return _data.RunNow; }
            set
            {
                _data.RunNow = value;
                OnPropertyChanged(RunNowProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the name of the planogram location type
        /// </summary>
        public WorkpackagePlanogramLocationType PlanogramLocationType
        {
            get { return _data.Workpackage.PlanogramLocationType; }
            set
            {
                _data.Workpackage.PlanogramLocationType = value;
                OnPropertyChanged(PlanogramLocationTypeProperty);
            }
        }

        /// <summary>
        /// Gets the type of the workpackage
        /// </summary>
        public WorkpackageType WorkpackageType
        {
            get { return _data.Workpackage.WorkpackageType; }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="data"></param>
        public SummaryStepViewModel(WorkpackageWizardData data)
        {
            _data = data;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Carries out actions to finalize 
        /// the selections made in this step
        /// </summary>
        public void CompleteStep()
        {
            //If user selected source file, resolve planogram name
            //must do it this as the end of the wizard incase user changes their mind during it.
            if ((this._data.Workpackage.WorkpackageType == Model.WorkpackageType.NewFromExisting || this._data.Workpackage.WorkpackageType == Model.WorkpackageType.StoreSpecific)
                && this.PlanogramLocationType == WorkpackagePlanogramLocationType.InSourceFolder)
            {
                foreach (WorkpackagePlanogram workpackagePlan in this._data.Workpackage.Planograms)
                {
                    WorkpackagePlanogram.ApplyWorkpackageSourceFolderPostfix(workpackagePlan, this._data.Workpackage.Name);
                }
            }
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return Message.WorkpackageWizardSummaryStep_StepHeader; }
        }

        public String StepDescription
        {
            get { return Message.WorkpackageWizardSummaryStep_StepDescription; }
        }


        #endregion

        #region IDataErrorInfo

        public String Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                if (columnName == WorkpackageNameProperty.Path)
                {
                    return ((IDataErrorInfo)_data.Workpackage)[Workpackage.NameProperty.Name];
                }
                return null;
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                }

                base.IsDisposed = true;
            }
        }

        #endregion

        
    }
}
