﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-25438 : L.Ineson 
//  Created.
// CCM-25736 : N.Haywood
//  Added property changed event for SelectedPlanogramThumbnailProperty
// V8-26472 : A.Probyn 
//  Updated reference to ColumnLayoutManager Constructor
// V8-27160 : L.Ineson
//  Moved column manager ui code into ui code behind.
// V8-27186 : A.Silva  ~ Amended to use PlanogramRepositoryRow, instead of PlanogramInfo.
// V8-27058 : A.Probyn ~ Changed over to use PlanogramMetadataImageViewModel for thumbnail
// V8-27362 : A.Silva  ~ Corrected bindings for Planogram Information.
// V8-27411 : M.Pettit ~ Reconfigured to use WorkpackageRow instead of WorkpackageInfo
// V8-28250 : L.ineson
//   Added paging.
// V8-28290 : L.Ineson
//  Added start and stop polling methods.
// V8-28153 : J.Pickup
//  Now need execute permissions to open a workpackage as you can now only 'run now' from the workpackage wizzard.
// V8-28412 : L.Ineson
//  Deleting a workpackage now clears plan rows rather than leave them selected till the next poll refresh.
// V8-28252 : J.Pickup
//  Deleting a workpackage planogram now requires correct permission and validates your request to delete.
// V8-28315 : D.Pleasance 
//  Added ability to filter workpackages by automation statuses and hold detail about the counts of these types
// V8-28493 : L.Ineson
//  Changes to improve performance
// V8-28501 : A.Silva
//      Amended look up for validation infos in CurrentWorkpackagePlansView_ModelChanged. Changed from a dictionary to a simple list.
// V8-28574 : D.Pleasance
//  Amended ShowWorkpackageDebugCommand's can execute to not display if selected plan is failed.
// V8-28574 : L.Ineson
//  Added extra checks to ensure the plan is complete and not any other status. Also added disabled reason and command desc text.

#endregion

#region Version History: CCM801

// V8-28690 : D.Pleasance
//  Added Workpackages filtering
// V8-28251 : L.Ineson
//  Improvements to DeleteWorkpackage and DeletePlan commands.
// V8-28758 : A.Kuszyk
//  Amended CurrentWorkpackagePlansView_ModelChanged to arrange planogram rows into a lookup rather than a dictionary
//  in case there are duplicate rows when this method is called.
// V8-28507 : D.Pleasance
//  Reworked paging functionality

#endregion

#region Version History: CCM803

// V8-29590 : A.Probyn
//	Extended for new logging properties and event logging window.
// V8-29668 : L.Ineson
//  Default plan grid processing status filter type is now complete.

#endregion

#region Version History: CCM810

// V8-29973 : N.Foster
//  Removed FindWorkpackagePlanogramGroupId from PlanogramRepositoryUIHelper
// V8-29864 : M.Pettit
//  Can open task explorer a  workpackage plan once it is complete - no need to wait for entire workpackage to be complete
// V8-30184 : A.Kuszyk
//  Updated OpenPlanogram_CanExecute to allow complete with errors.
// V8-29163 : M.Pettit
//  Workpackage delete message improved
// V8-30160 : M.Brumby
//  Prevented "Cannot change ObservableCollection during a CollectionChanged event" error some users were getting (including me)
// V8-30305 : A.Silva
//  Amended ShowWorkpackageDebug_CanExecute to allow opening for Complete With Errors.

#endregion

#region Version History: CCM811
// V8-30256 : A.Probyn
//  Altered ProcessingStatusAllowsOpeningPlan to include failed too
//  Extended OpenPlan_CanExecute so that a plan must have at least 1 bay
// V8-30284 : M.Shelley
//  Modified the OpenPlan_Executed method to actually open all the plans when multiple rows are selected.
// V8-30394 : I.George
//  Added SmallIcon to commands missing it.
#endregion

#region Version History: CCM820
// V8-30880 : L.Luong
//  Changes to make Pending and Queued joined as Queued
// V8-30432 : M.Shelley
//  Add a right-click context menu to pre-filter the workpackage event data by planogram name.
// V8-30646 : M.Shelley
//  Added a call to the WorkpackageProcessingStatus.SetPending method to set the work package status to "Waiting" when
//  the user clicks the "Run" button
#endregion

#region Version History: (CCM 8.3.0)
// V8-32810 : L.Ineson
//  Added Export to File
// CCM-13673 : G.Richards
//  Passed the workpackage info to the planogram repository row to translate the initial automation status
#endregion


#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using Csla;
using Csla.Rules;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageDebug;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageEventLog;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages
{
    /// <summary>
    /// Viewmodel controller for WorkpackagesOrganiser
    /// </summary>
    public sealed class WorkPackagesViewModel : ViewModelAttachedControlObject<WorkPackagesOrganiser>
    {
        #region Fields

        /// <summary>
        ///     Contains a List of any <see cref="ProcessingStatus"/> that would allow opening a plan.
        /// </summary>
        private static readonly List<ProcessingStatus> ProcessingStatusAllowsOpeningPlan =
            new List<ProcessingStatus>
            {
                ProcessingStatus.Complete, 
                ProcessingStatus.CompleteWithWarnings,
                ProcessingStatus.Failed
            };

        private Boolean _isUpdating;

        private readonly WorkpackageInfoListViewModel _availableWorkpackagesView = new WorkpackageInfoListViewModel();
        private readonly BulkObservableCollection<WorkpackageRow> _availableWorkpackageRows = new BulkObservableCollection<WorkpackageRow>();
        private ReadOnlyBulkObservableCollection<WorkpackageRow> _availableWorkpackageRowsRO;
        private WorkpackageRow _selectedWorkpackageRow;

        private readonly WorkflowTaskInfoListViewModel _currentWorkpackageTasksView = new WorkflowTaskInfoListViewModel();

        private readonly PlanogramInfoListViewModel _currentWorkpackagePlansView = new PlanogramInfoListViewModel();
        private Boolean _isFetchingPlanogramRows;
        private readonly BulkObservableCollection<PlanogramRepositoryRow> _planogramRows = new BulkObservableCollection<PlanogramRepositoryRow>();
        private ReadOnlyBulkObservableCollection<PlanogramRepositoryRow> _planogramRowsRO;
        private readonly ObservableCollection<PlanogramRepositoryRow> _selectedPlanogramRows = new ObservableCollection<PlanogramRepositoryRow>();
        private PlanogramRepositoryRow _selectedPlanogramRow;

        private ProcessingStatusFilterType _selectedProcessingStatusFilterType = ProcessingStatusFilterType.All;
        private WorkpackageFilterType _selectedFilterType = WorkpackageFilterType.ShowUserOnly;
        private Int32 _allCount = 0;
        private Int32 _pendingCount = 0;
        private Int32 _queuedCount = 0;
        private Int32 _processingCount = 0;
        private Int32 _completeCount = 0;
        private Int32 _failedCount = 0;
        private ProcessingStatusFilterType _selectedWorkpackageProcessingStatusFilterType = ProcessingStatusFilterType.All;
        private Byte _currentPage = 1;
        private Byte _pageCount = 1;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableWorkpackagesProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.AvailableWorkpackages);
        public static readonly PropertyPath AvailableWorkpackageRowsProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.AvailableWorkpackageRows);
        public static readonly PropertyPath SelectedWorkpackageRowProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.SelectedWorkpackageRow);
        public static readonly PropertyPath PlanogramRowsProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.PlanogramRows);
        public static readonly PropertyPath SelectedPlanogramRowProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.SelectedPlanogramRow);
        public static readonly PropertyPath SelectedPlanogramRowsProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.SelectedPlanogramRows);
        public static readonly PropertyPath SelectedPlanogramInfoProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.SelectedPlanogramInfo);
        public static readonly PropertyPath CurrentWorkpackageTasksProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.CurrentWorkpackageTasks);
        public static readonly PropertyPath IsUpdatingProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.IsUpdating);
        public static readonly PropertyPath SelectedProcessingStatusFilterTypeProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.SelectedProcessingStatusFilterType);
        public static readonly PropertyPath IsFetchingPlanogramRowsProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.IsFetchingPlanogramRows);
        public static readonly PropertyPath SelectedFilterTypeProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.SelectedFilterType);
        public static readonly PropertyPath AllCountProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.AllCount);
        public static readonly PropertyPath PendingCountProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.PendingCount);
        public static readonly PropertyPath QueuedCountProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.QueuedCount);
        public static readonly PropertyPath ProcessingCountProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.ProcessingCount);
        public static readonly PropertyPath CompleteCountProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.CompleteCount);
        public static readonly PropertyPath FailedCountProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.FailedCount);
        public static readonly PropertyPath SelectedWorkpackageProcessingStatusFilterTypeProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.SelectedWorkpackageProcessingStatusFilterType);
        public static readonly PropertyPath CurrentPageProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.CurrentPage);
        public static readonly PropertyPath PageCountProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.PageCount);
                
        //Commands
        public static readonly PropertyPath NewWorkpackageCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.NewWorkpackageCommand);
        public static readonly PropertyPath OpenWorkpackageCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.OpenWorkpackageCommand);
        public static readonly PropertyPath DeleteWorkpackageCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.DeleteWorkpackageCommand);
        public static readonly PropertyPath RunWorkpackageCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.RunWorkpackageCommand);
        public static readonly PropertyPath ShowWorkpackageDebugCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.ShowWorkpackageDebugCommand);
        public static readonly PropertyPath OpenPlanCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.OpenPlanCommand);
        public static readonly PropertyPath DeletePlanCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.DeletePlanCommand);
        //public static readonly PropertyPath AcceptActionCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p=> p.AcceptActionCommand);
        //public static readonly PropertyPath DeclineActionCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p=> p.DeclineActionCommand);
        public static readonly PropertyPath NextPlanogamPageCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.NextPlanogamPageCommand);
        public static readonly PropertyPath PrevPlanogamPageCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.PrevPlanogamPageCommand);
        public static readonly PropertyPath FirstPlanogamPageCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.FirstPlanogamPageCommand);
        public static readonly PropertyPath LastPlanogamPageCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.LastPlanogamPageCommand);
        public static readonly PropertyPath EventLogCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.EventLogCommand);
        public static readonly PropertyPath ExportToFileCommandProperty = WpfHelper.GetPropertyPath<WorkPackagesViewModel>(p => p.ExportToFileCommand);


        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of work packages to be shown for the currently selected group
        /// </summary>
        public ReadOnlyBulkObservableCollection<WorkpackageInfo> AvailableWorkpackages
        {
            get { return _availableWorkpackagesView.BindableCollection; }
        }

        /// <summary>
        /// Returns the collection of work packages rows to be shown for the currently selected group
        /// </summary>
        public ReadOnlyBulkObservableCollection<WorkpackageRow> AvailableWorkpackageRows
        {
            get
            {
                return _availableWorkpackageRowsRO ??
                       (_availableWorkpackageRowsRO =
                       new ReadOnlyBulkObservableCollection<WorkpackageRow>(_availableWorkpackageRows));
            }
        }

        /// <summary>
        /// Gets/Sets the selected filter type
        /// </summary>
        public WorkpackageFilterType SelectedFilterType
        {
            get { return _selectedFilterType; }
            set
            {
                _selectedFilterType = value;
                OnPropertyChanged(SelectedFilterTypeProperty);

                //refilter
                RefreshFilter();
            }
        }

        /// <summary>
        /// Gets/Sets the selected processing status filter
        /// </summary>
        public ProcessingStatusFilterType SelectedWorkpackageProcessingStatusFilterType
        {
            get { return _selectedWorkpackageProcessingStatusFilterType; }
            set
            {
                _selectedWorkpackageProcessingStatusFilterType = value;
                OnPropertyChanged(SelectedWorkpackageProcessingStatusFilterTypeProperty);

                //refilter
                RefreshFilter();
            }
        }

        /// <summary>
        /// Gets/Sets the selected workpackage row.
        /// </summary>
        public WorkpackageRow SelectedWorkpackageRow
        {
            get { return _selectedWorkpackageRow; }
            set
            {
                //don't do anything if the views are updating.
                if (IsUpdating) return;

                WorkpackageRow oldValue = _selectedWorkpackageRow;

                _selectedWorkpackageRow = value;
                OnPropertyChanged(SelectedWorkpackageRowProperty);

                OnSelectedWorkpackageRowChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the workflow for the currently selected workpackage.
        /// </summary>
        public IEnumerable<WorkflowTaskInfo> CurrentWorkpackageTasks
        {
            get { return _currentWorkpackageTasksView.Model; }
        }

        /// <summary>
        ///  Returns the collection of planogram rows for the currently selected workpackage.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramRepositoryRow> PlanogramRows
        {
            get
            {
                if (_planogramRowsRO == null)
                {
                    _planogramRowsRO = new ReadOnlyBulkObservableCollection<PlanogramRepositoryRow>(_planogramRows);
                }
                return _planogramRowsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected planogram rows.
        /// </summary>
        public ObservableCollection<PlanogramRepositoryRow> SelectedPlanogramRows
        {
            get { return _selectedPlanogramRows; }
        }

        /// <summary>
        /// Gets/Sets the selected planogram row
        /// </summary>
        public PlanogramRepositoryRow SelectedPlanogramRow
        {
            get { return _selectedPlanogramRow; }
            set
            {
                if (_selectedPlanogramRow == value) return;

                //don't do anything if the views are updating.
                if (IsUpdating || IsFetchingPlanogramRows) return;

                _selectedPlanogramRow = value;
                
                OnPropertyChanged(SelectedPlanogramRowProperty);
                OnPropertyChanged(SelectedPlanogramInfoProperty);
                
            }
        }

        /// <summary>
        /// Returns the currently selected planogram info.
        /// </summary>
        public PlanogramInfo SelectedPlanogramInfo
        {
            get
            {
                return _selectedPlanogramRow != null ? _selectedPlanogramRow.Info : null;
            }
        }

        /// <summary>
        /// Returns true if this is currently refreshing data.
        /// </summary>
        public Boolean IsUpdating
        {
            get { return _isUpdating; }
            private set
            {
                _isUpdating = value;
                OnPropertyChanged(IsUpdatingProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected processing status filter type.
        /// </summary>
        public ProcessingStatusFilterType SelectedProcessingStatusFilterType
        {
            get
            {
                return _selectedProcessingStatusFilterType;
            }
            set
            {
                ProcessingStatusFilterType oldValue = _selectedProcessingStatusFilterType;

                _selectedProcessingStatusFilterType = value;
                OnPropertyChanged(SelectedProcessingStatusFilterTypeProperty);

                OnSelectedProcessingStatusFilterTypeChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Flag to indicate when the plan rows collection is being refreshed.
        /// </summary>
        public Boolean IsFetchingPlanogramRows
        {
            get { return _isFetchingPlanogramRows; }
            private set
            {
                _isFetchingPlanogramRows = value;
                OnPropertyChanged(IsFetchingPlanogramRowsProperty);
            }
        }

        /// <summary>
        /// Returns a count for the all filter.
        /// </summary>
        public Int32 AllCount
        {
            get { return _allCount; }
            private set
            {
                _allCount = value;
                OnPropertyChanged(AllCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the pending filter.
        /// </summary>
        public Int32 PendingCount
        {
            get { return _pendingCount; }
            private set
            {
                _pendingCount = value;
                OnPropertyChanged(PendingCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the Queued filter.
        /// </summary>
        public Int32 QueuedCount
        {
            get { return _queuedCount; }
            private set
            {
                _queuedCount = value;
                OnPropertyChanged(QueuedCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the Processing filter.
        /// </summary>
        public Int32 ProcessingCount
        {
            get { return _processingCount; }
            private set
            {
                _processingCount = value;
                OnPropertyChanged(ProcessingCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the Complete filter.
        /// </summary>
        public Int32 CompleteCount
        {
            get { return _completeCount; }
            private set
            {
                _completeCount = value;
                OnPropertyChanged(CompleteCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the Failed filter.
        /// </summary>
        public Int32 FailedCount
        {
            get { return _failedCount; }
            private set
            {
                _failedCount = value;
                OnPropertyChanged(FailedCountProperty);
            }
        }

        /// <summary>
        /// Returns the current page
        /// </summary>
        public Byte CurrentPage
        {
            get { return _currentPage; }
            set
            {
                _currentPage = value;
                OnPropertyChanged(CurrentPageProperty);
            }
        }
        
        /// <summary>
        /// Returns the page count
        /// </summary>
        public Byte PageCount
        {
            get { return _pageCount; }
            set
            {
                _pageCount = value;                
                OnPropertyChanged(PageCountProperty);
            }
        }
                
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkPackagesViewModel()
        {
            _currentWorkpackageTasksView.ModelChanged += CurrentWorkpackageTasksView_ModelChanged;

            _availableWorkpackagesView.ModelChanging += AvailableWorkpackagesView_ModelChanging;
            _availableWorkpackagesView.ModelChanged += AvailableWorkpackagesView_ModelChanged;
            _currentWorkpackagePlansView.ModelChanged += CurrentWorkpackagePlansView_ModelChanged;

            _selectedPlanogramRows.CollectionChanged += SelectedPlanogramRows_CollectionChanged;

            _availableWorkpackagesView.BeginPollingFetchForCurrentEntity();
        }

        #endregion

        #region Commands

        #region NewWorkpackage

        private RelayCommand _newWorkpackageCommand;

        /// <summary>
        /// Shows the workpackage wizard to create a new work package
        /// </summary>
        public RelayCommand NewWorkpackageCommand
        {
            get
            {
                if (_newWorkpackageCommand == null)
                {
                    _newWorkpackageCommand = new RelayCommand(
                        p => NewWorkpackage_Executed(),
                        p => NewWorkpackage_CanExecute())
                        {
                            FriendlyName = Message.WorkPackages_NewWorkpackage,
                            FriendlyDescription = Message.WorkPackages_NewWorkpackage_Desc,
                            Icon = ImageResources.WorkPackages_NewWorkpackage,
                            SmallIcon = ImageResources.WorkPackages_NewWorkpackage_16
                        };
                    base.ViewModelCommands.Add(_newWorkpackageCommand);

                }
                return _newWorkpackageCommand;
            }
        }

        private Boolean NewWorkpackage_CanExecute()
        {
            //must have permission to create
            if (!BusinessRules.HasPermission(AuthorizationActions.CreateObject, typeof(Workpackage)))
            {
                this.NewWorkpackageCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void NewWorkpackage_Executed()
        {
            WorkpackageWizard.WizardViewModel wizardViewModel = new WorkpackageWizard.WizardViewModel();
            wizardViewModel.ShowDialog();

            //if the wizard completed successfully reload the available workpackages list
            if (wizardViewModel.DialogResult == true)
            {
                _availableWorkpackagesView.FetchForCurrentEntity();
                this.SelectedWorkpackageRow = this.AvailableWorkpackageRows.FirstOrDefault(w => w.Info.Id == wizardViewModel.CreatedPackageId);
            }
        }

        #endregion

        #region OpenWorkpackage

        private RelayCommand _openWorkpackageCommand;

        /// <summary>
        /// Open the current work package
        /// </summary>
        public RelayCommand OpenWorkpackageCommand
        {
            get
            {
                if (_openWorkpackageCommand == null)
                {
                    _openWorkpackageCommand = new RelayCommand(
                        p => OpenWorkpackage_Executed(),
                        p => OpenWorkpackage_CanExecute())
                    {
                        FriendlyName = Message.WorkPackages_OpenWorkpackage,
                        FriendlyDescription = Message.WorkPackages_OpenWorkpackage_Desc,
                        Icon = ImageResources.WorkPackages_OpenWorkpackage,
                        SmallIcon = ImageResources.WorkPackages_OpenPlan_16   
                    };
                    base.ViewModelCommands.Add(_openWorkpackageCommand);

                }
                return _openWorkpackageCommand;
            }
        }

        private Boolean OpenWorkpackage_CanExecute()
        {
            // must have permission to edit
            if (!BusinessRules.HasPermission(AuthorizationActions.EditObject, typeof(Workpackage)))
            {
                this.OpenWorkpackageCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            // must have permission to execute (GEM:28153)
            if (!BusinessRules.HasPermission(AuthorizationActions.EditObject, typeof(Workpackage.ExecuteCommand)))
            {
                this.OpenWorkpackageCommand.DisabledReason = Message.Generic_NoExecutePermission;
                return false;
            }

            // must have a workpackage selected
            if (this.SelectedWorkpackageRow == null)
            {
                this.OpenWorkpackageCommand.DisabledReason = Message.WorkPackages_NoWorkpackageSelected;
                return false;
            }


            return true;
        }

        private void OpenWorkpackage_Executed()
        {
            WorkpackageWizard.WizardViewModel wizardViewModel = new WorkpackageWizard.WizardViewModel(this.SelectedWorkpackageRow.Info);
            wizardViewModel.ShowDialog();

            //if the wizard completed successfully reload the available workpackages list
            if (wizardViewModel.DialogResult == true)
            {
                _availableWorkpackagesView.FetchForCurrentEntity();
                this.SelectedWorkpackageRow = this.AvailableWorkpackageRows.FirstOrDefault(w => w.Info.Id == wizardViewModel.CreatedPackageId);
            }
        }

        #endregion

        #region DeleteWorkpackage

        private RelayCommand _deleteWorkpackageCommand;

        /// <summary>
        /// Deletes the current work package
        /// </summary>
        public RelayCommand DeleteWorkpackageCommand
        {
            get
            {
                if (_deleteWorkpackageCommand == null)
                {
                    _deleteWorkpackageCommand = new RelayCommand(
                        p => DeleteWorkpackage_Executed(),
                        p => DeleteWorkpackage_CanExecute())
                    {
                        FriendlyName = Message.WorkPackages_DeleteWorkpackage,
                        FriendlyDescription = Message.WorkPackages_DeleteWorkpackage_Desc,
                        Icon = ImageResources.WorkPackages_DeleteWorkpackage,
                        SmallIcon = ImageResources.WorkPackages_DeleteWorkpackage,
                    };
                    base.ViewModelCommands.Add(_deleteWorkpackageCommand);

                }
                return _deleteWorkpackageCommand;
            }
        }

        private Boolean DeleteWorkpackage_CanExecute()
        {
            //must have permission to delete
            if (!BusinessRules.HasPermission(AuthorizationActions.DeleteObject, typeof(Workpackage)))
            {
                this.DeleteWorkpackageCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must have a workpackage selected
            if (this.SelectedWorkpackageRow == null)
            {
                this.DeleteWorkpackageCommand.DisabledReason = Message.WorkPackages_NoWorkpackageSelected;
                return false;
            }

            return true;
        }

        private void DeleteWorkpackage_Executed()
        {
            WorkpackageRow row = this.SelectedWorkpackageRow;
            WorkpackageInfo itemToDelete = row.Info;

            //confirm the delete with the user.

            ModalMessageResult messageResult = CommonHelper.GetWindowService().ShowMessage(
                    MessageWindowType.Warning,
                    Message.Workpackages_DeleteWorkpackage_DialogHeader,
                    String.Format(CultureInfo.CurrentCulture,
                        Message.Workpackages_DeleteWorkpackage_DialogDescription, itemToDelete.Name),
                         Message.Generic_Continue, Message.Generic_Cancel);
            if (messageResult != ModalMessageResult.Button1) return;

            StopPolling();

            //perform the delete
            WorkpackageDeleteWork work = new WorkpackageDeleteWork(itemToDelete);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);

            //tidy up
            _planogramRows.Clear();

            this.SelectedWorkpackageRow =
                _availableWorkpackageRows.OrderByDescending(w => w.Info.DateCreated)
                .FirstOrDefault(w => w != this.SelectedWorkpackageRow);

            _availableWorkpackageRows.Remove(row);

            StartPolling();
        }

        private sealed class WorkpackageDeleteWork : IModalBusyWork
        {
            #region Nested Classes

            private sealed class WorkArgs
            {
                public Int32 WorkpackageId { get; set; }
                public Int32? UserId { get; set; }
            }

            #endregion

            #region Fields
            private Int32 _workpackageId;
            private String _workpackageName;
            #endregion

            #region Properties

            public Boolean ReportsProgress
            {
                get { return true; }
            }

            public Boolean SupportsCancellation
            {
                get { return true; }
            }

            #endregion

            #region Constructor
            public WorkpackageDeleteWork(WorkpackageInfo workpackage)
            {
                _workpackageId = workpackage.Id;
                _workpackageName = workpackage.Name;
            }
            #endregion

            #region Methods

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                CommonHelper.GetWindowService().ShowDeterminateBusy(
                    Message.Workpackages_DeleteWorkpackage_BusyHeader,
                    _workpackageName,
                    cancelEventHandler);
            }

            public object GetWorkerArgs()
            {
                return new WorkArgs()
                {
                    WorkpackageId = _workpackageId,
                    UserId = DomainPrincipal.CurrentUserId
                };
            }

            public void OnDoWork(IModalBusyWorkerService sender, DoWorkEventArgs e)
            {
                // get the workpackage deletion arguments
                WorkArgs args = (WorkArgs)e.Argument;

                // fetch the workpackage
                Workpackage workpackage = Workpackage.FetchById(args.WorkpackageId);

                // determine the percentage per item
                Double perPercent = 100 / (Double)(workpackage.Planograms.Count);
                Double curPercent = 0;

                // NOTE: We no longer need to delete
                // the debug planograms, as the processor
                // will clean these up for us as part of
                // the maintenance routing
                foreach (WorkpackagePlanogram workpackagePlanogram in workpackage.Planograms.ToList())
                {
                    // remove the workpackage from the planogram
                    workpackage.Planograms.Remove(workpackagePlanogram);

                    // update the progress
                    curPercent += perPercent;
                    sender.ReportProgress((Int32)curPercent);
                }

                // delete the workpackage
                workpackage.Delete();
                workpackage.Save();
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, ProgressChangedEventArgs e)
            {
                //update the modal busy with the new percentage
                CommonHelper.GetWindowService().UpdateBusy(e.ProgressPercentage);
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, RunWorkerCompletedEventArgs e)
            {
                //show error if required
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(_workpackageName, OperationType.Delete);
                }
            }

            #endregion
        }

        #endregion

        #region RunWorkpackage

        private RelayCommand _runWorkpackageCommand;

        /// <summary>
        /// Runs the selected workpackage
        /// </summary>
        public RelayCommand RunWorkpackageCommand
        {
            get
            {
                if (_runWorkpackageCommand == null)
                {
                    _runWorkpackageCommand = new RelayCommand(
                        p => RunWorkpackage_Executed(),
                        p => RunWorkpackage_CanExecute())
                    {
                        FriendlyName = Message.WorkPackages_RunWorkpackage,
                        FriendlyDescription = Message.WorkPackages_RunWorkpackage_Desc,
                        Icon = ImageResources.WorkPackages_RunWorkpackage,
                        SmallIcon = ImageResources.WorkPackages_RunWorkpackage,
                    };
                    base.ViewModelCommands.Add(_runWorkpackageCommand);

                }
                return _runWorkpackageCommand;
            }
        }

        private Boolean RunWorkpackage_CanExecute()
        {
            // must have permission to execute
            if (!BusinessRules.HasPermission(AuthorizationActions.EditObject, typeof(Workpackage.ExecuteCommand)))
            {
                this.RunWorkpackageCommand.DisabledReason = Message.Generic_NoExecutePermission;
                return false;
            }

            if (this.SelectedWorkpackageRow == null)
            {
                this.RunWorkpackageCommand.DisabledReason = Message.WorkPackages_NoWorkpackageSelected;
                return false;
            }

            return true;
        }

        private void RunWorkpackage_Executed()
        {
            if (this.SelectedWorkpackageRow != null)
            {
                WorkpackageProcessingStatus.SetPending(SelectedWorkpackageRow.Info.Id);
                this.SelectedWorkpackageRow.Info.Execute();
            }

            //update the the available workpackages immediately.
            _availableWorkpackagesView.FetchForCurrentEntityAsync();
        }
        #endregion

        #region ShowWorkpackageDebug

        private RelayCommand _showWorkpackageDebugCommand;

        /// <summary>
        /// Launches the workpackage debug window.
        /// Param: (optional) The workpackage plan to select
        /// </summary>
        public RelayCommand ShowWorkpackageDebugCommand
        {
            get
            {
                if (_showWorkpackageDebugCommand == null)
                {
                    _showWorkpackageDebugCommand = new RelayCommand(
                        p => ShowWorkpackageDebug_Executed(p),
                        p => ShowWorkpackageDebug_CanExecute(p))
                        {
                            FriendlyName = Message.Workpackages_ShowWorkpackageDebug,
                            FriendlyDescription = Message.Workpackages_ShowWorkpackageDebug_Desc,
                            Icon = ImageResources.Workpackages_ShowWorkpackageDebug_16,
                            SmallIcon = ImageResources.Workpackages_ShowWorkpackageDebug_16
                        };
                    base.ViewModelCommands.Add(_showWorkpackageDebugCommand);
                }
                return _showWorkpackageDebugCommand;
            }
        }

        private Boolean ShowWorkpackageDebug_CanExecute(Object args)
        {
            //must have a workpackage selected
            if (this.SelectedWorkpackageRow == null
                || this.SelectedWorkpackageRow.Info == null)
            {
                this.ShowWorkpackageDebugCommand.DisabledReason = Message.Workpackages_ShowWorkpackageDebug_DisabledNoValidPlanogram;
                return false;
            }

            //workpackage must be complete - not required so long as selected plan is complete
            //if (this.SelectedWorkpackageRow.Info.ProcessingStatus != ProcessingStatus.Complete)
            //{
            //    this.ShowWorkpackageDebugCommand.DisabledReason = Message.Workpackages_ShowWorkpackageDebug_DisabledNoValidPlanogram;
            //    return false;
            //}

            //must have a plan selected
            if (this.SelectedPlanogramRow == null || this.SelectedPlanogramRow.Info == null)
            {
                this.ShowWorkpackageDebugCommand.DisabledReason = Message.Workpackages_ShowWorkpackageDebug_DisabledNoValidPlanogram;
                return false;
            }

            //plan must be complete
            if (!ProcessingStatusAllowsOpeningPlan.Contains(this.SelectedPlanogramRow.Info.AutomationProcessingStatus))
            {
                this.ShowWorkpackageDebugCommand.DisabledReason = Message.Workpackages_ShowWorkpackageDebug_DisabledNoValidPlanogram;
                return false;
            }

            return true;
        }

        private void ShowWorkpackageDebug_Executed(Object args)
        {
            base.ShowWaitCursor(true);

            //fetch the workpackage
            Workpackage package;
            try
            {
                package = Workpackage.FetchById(this.SelectedWorkpackageRow.Info.Id);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                Exception rootEx = ex.GetBaseException();
                LocalHelper.RecordException(rootEx);

                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                    this.SelectedWorkpackageRow.Info.Name, OperationType.Open);

                return;
            }


            //get the planogram to select
            WorkpackagePlanogram planToSelect = args as WorkpackagePlanogram;
            if (planToSelect == null)
            {
                PlanogramInfo info = args as PlanogramInfo;
                if (info == null) info = this.SelectedPlanogramInfo;
                if (info != null)
                {
                    planToSelect = package.Planograms.FirstOrDefault(p => p.DestinationPlanogramId == info.Id);
                }
            }

            base.ShowWaitCursor(false);


            if (planToSelect != null)
            {
                WorkpackageDebugViewModel viewModel = new WorkpackageDebugViewModel(package);
                viewModel.SelectedWorkpackagePlan = planToSelect;
                viewModel.ShowDialog();
            }

        }

        #endregion

        #region OpenPlan

        private RelayCommand _openPlanCommand;

        /// <summary>
        /// Open the current plan
        /// </summary>
        public RelayCommand OpenPlanCommand
        {
            get
            {
                if (_openPlanCommand == null)
                {
                    _openPlanCommand = new RelayCommand(
                        p => OpenPlan_Executed(),
                        p => OpenPlan_CanExecute())
                    {
                        FriendlyName = Message.WorkPackages_OpenPlan,
                        FriendlyDescription = Message.WorkPackages_OpenPlan_Desc,
                        Icon = ImageResources.WorkPackages_OpenPlan,
                        SmallIcon = ImageResources.WorkPackages_OpenPlan
                                          
                    };
                    base.ViewModelCommands.Add(_openPlanCommand);

                }
                return _openPlanCommand;
            }
        }

        private Boolean OpenPlan_CanExecute()
        {
            //must have access to an editor client.
            if (!App.ViewState.IsEditorClientAvailable)
            {
                this.OpenPlanCommand.DisabledReason = Message.Generic_DisabledNoEditorClient;
                return false;
            }

            //must have a planogram selected.
            if (this.SelectedPlanogramRow == null || SelectedPlanogramRow.Info == null)
            {
                this.OpenPlanCommand.DisabledReason = Message.WorkPackages_NoPlanogramSelected;
                return false;
            }

            //plan must be complete
            if (!ProcessingStatusAllowsOpeningPlan.Contains(SelectedPlanogramRow.Info.AutomationProcessingStatus))
            {
                this.OpenPlanCommand.DisabledReason = Message.Workpackages_ShowWorkpackageDebug_DisabledNoValidPlanogram;
                return false;
            }

            return true;
        }

        private void OpenPlan_Executed()
        {
            if (this.AttachedControl != null)
            {
                var infoList = this.SelectedPlanogramRows.Select(x => x.Info).ToList();
                LocalHelper.OpenPlanogramsInEditor(infoList);
            }
        }

        #endregion

        #region DeletePlan

        private RelayCommand _deletePlanCommand;

        /// <summary>
        /// Deletes the selected workpackage planograms.
        /// </summary>
        public RelayCommand DeletePlanCommand
        {
            get
            {
                if (_deletePlanCommand == null)
                {
                    _deletePlanCommand = new RelayCommand(
                        p => DeletePlan_Executed(),
                        p => DeletePlan_CanExecute())
                    {
                        FriendlyName = Message.WorkPackages_DeletePlan,
                        FriendlyDescription = Message.WorkPackages_DeletePlan_Desc,
                        Icon = ImageResources.WorkPackages_DeletePlan,
                        SmallIcon = ImageResources.WorkPackages_DeletePlan,
                    };
                    base.ViewModelCommands.Add(_deletePlanCommand);

                }
                return _deletePlanCommand;
            }
        }

        private Boolean DeletePlan_CanExecute()
        {
            //must have permission to delete
            if (!ApplicationContext.User.IsInRole(DomainPermission.PlanogramDelete.ToString()))
            {
                this.DeletePlanCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must have permission to delete
            if (!BusinessRules.HasPermission(AuthorizationActions.DeleteObject, typeof(WorkpackagePlanogram)))
            {
                this.DeletePlanCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must have a planogram selected
            if (this.SelectedPlanogramRow == null || SelectedPlanogramRow.Info == null)
            {
                this.DeletePlanCommand.DisabledReason = Message.WorkPackages_NoPlanogramSelected;
                return false;
            }


            return true;
        }

        private void DeletePlan_Executed()
        {
            IEnumerable<PlanogramInfo> destinationPlanInfos = this.SelectedPlanogramRows.Select(p => p.Info).ToList();

            //if we have selected all plans in the workpackge then just call the command to delete the workpackage as a whole.
            if (destinationPlanInfos.Count() == this.SelectedWorkpackageRow.Info.PlanogramCount)
            {
                this.DeleteWorkpackageCommand.Execute();
                return;
            }
            else
            {
                //confirm with the user.
                ModalMessageResult messageResult = CommonHelper.GetWindowService().ShowMessage(
                    MessageWindowType.Warning,
                    Message.Workpackages_DeletePlan_WarningHeader,
                    String.Format(CultureInfo.CurrentCulture,
                        Message.Workpackages_DeletePlan_WarningDesc, destinationPlanInfos.Count()),
                         Message.Generic_Continue, Message.Generic_Cancel);
                if (messageResult != ModalMessageResult.Button1) return;

                //stop polling so that we don't update randomly.
                StopPolling();

                //run the delete work
                WorkpackagePlanogramDeleteWork work = new WorkpackagePlanogramDeleteWork(this.SelectedWorkpackageRow.Info, destinationPlanInfos);
                work.ItemDeleted += OnWorkpackagePlanogramDeleted;

                CommonHelper.GetModalBusyWorkerService().RunWorker(work);

                work.ItemDeleted -= OnWorkpackagePlanogramDeleted;

                //restart polling
                StartPolling();
            }
        }

        private void OnWorkpackagePlanogramDeleted(object sender, EventArgs<PlanogramInfo> e)
        {
            //remove the workpackage planogram row.
            PlanogramRepositoryRow row = this.PlanogramRows.FirstOrDefault(r => r.Info.Id == e.ReturnValue.Id);
            _planogramRows.Remove(row);
        }

        private sealed class WorkpackagePlanogramDeleteWork : IModalBusyWork
        {
            #region Nested Classes

            private sealed class WorkArgs
            {
                public Int32? UserId { get; set; }
                public Int32 WorkpackageId { get; set; }
                public Int32[] DestinationPlanIds { get; set; }
            }

            #endregion

            #region Fields
            private Int32 _workpackageId;
            private String _workpackageName;
            private WorkpackageInfo _parentWorkpackage;
            private List<PlanogramInfo> _destPlansToDelete;
            private Queue<PlanogramInfo> _deletionQueue;
            private Int32 _currentPlanDeleteNo;
            #endregion

            #region Properties

            public Boolean ReportsProgress
            {
                get { return true; }
            }

            public Boolean SupportsCancellation
            {
                get { return true; }
            }

            #endregion

            #region Events

            public event EventHandler<EventArgs<PlanogramInfo>> ItemDeleted;

            private void RaiseItemDeleted(PlanogramInfo item)
            {
                if (ItemDeleted != null)
                    ItemDeleted(this, new EventArgs<PlanogramInfo>(item));
            }

            #endregion

            #region Constructor
            public WorkpackagePlanogramDeleteWork(WorkpackageInfo parentWorkpackage, IEnumerable<PlanogramInfo> destPlansToDelete)
            {
                _workpackageId = parentWorkpackage.Id;
                _workpackageName = parentWorkpackage.Name;

                _destPlansToDelete = new List<PlanogramInfo>(destPlansToDelete);

                _deletionQueue = new Queue<PlanogramInfo>(_destPlansToDelete);
                _currentPlanDeleteNo = 1;
            }
            #endregion

            #region Methods

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                if (_destPlansToDelete.Count == 0) return;

                CommonHelper.GetWindowService().ShowDeterminateBusy(
                    Message.Workpackages_DeletePlan_BusyHeader,
                    String.Format(CultureInfo.CurrentCulture, "1/{0} - {1}",
                    _destPlansToDelete.Count, _destPlansToDelete.First().Name),
                    cancelEventHandler);

            }

            public object GetWorkerArgs()
            {
                return new WorkArgs()
                {
                    UserId = DomainPrincipal.CurrentUserId,
                    WorkpackageId = _workpackageId,
                    DestinationPlanIds = _destPlansToDelete.Select(p => p.Id).ToArray()
                };
            }

            public void OnDoWork(IModalBusyWorkerService sender, DoWorkEventArgs e)
            {
                // get the workpackage delete arguments
                WorkArgs args = (WorkArgs)e.Argument;

                // if there are no planograms to delete, then do nothing
                if (args.DestinationPlanIds.Length == 0) return;

                // fetch the workpackage
                Workpackage workpackage = Workpackage.FetchById(args.WorkpackageId);

                // determine the percentage per plan
                Double perPercent = 100 / (Double)args.DestinationPlanIds.Length; // subtract one as the destination plan is also a debug plan
                Double curPercent = 0;

                // delete all debug planograms associated
                // to the specified planograms that are to be deleted
                // NOTE: We do not need to delete debug planograms as
                // the engine will delete these for us as part of the
                // maintenance routine
                foreach (Int32 destinationPlanogramId in args.DestinationPlanIds)
                {
                    // remove the destination planogram from the workpackage
                    workpackage.Planograms.Remove(workpackage.Planograms.FirstOrDefault(p => p.DestinationPlanogramId == destinationPlanogramId));

                    // save the planogram
                    workpackage = workpackage.Save();

                    // report progress
                    curPercent += perPercent;
                    sender.ReportProgress((Int32)curPercent);
                }
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, ProgressChangedEventArgs e)
            {
                PlanogramInfo lastPlan = _deletionQueue.Dequeue();
                PlanogramInfo nextPlan = (_deletionQueue.Count > 0) ? _deletionQueue.Peek() : null;

                _currentPlanDeleteNo++;

                //update the modal busy with the new percentage
                if (nextPlan != null)
                {
                    CommonHelper.GetWindowService().UpdateBusy(
                        String.Format(CultureInfo.CurrentCulture, "{0}/{1} - {2}",
                        _currentPlanDeleteNo, _destPlansToDelete.Count, nextPlan.Name),
                        e.ProgressPercentage);
                }
                else
                {
                    CommonHelper.GetWindowService().UpdateBusy(String.Empty, 100);
                }


                //Raise the progress changed event
                RaiseItemDeleted(lastPlan);
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, RunWorkerCompletedEventArgs e)
            {
                PlanogramInfo nextPlan = (_deletionQueue.Count > 0) ? _deletionQueue.Peek() : null;
                _deletionQueue = null;

                //show error if required
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(nextPlan.Name, OperationType.Delete);
                }
            }

            #endregion

        }

        #endregion

        #region NextPlanogramPage

        private RelayCommand _nextPlanogramPageCommand;

        public RelayCommand NextPlanogamPageCommand
        {
            get
            {
                if (_nextPlanogramPageCommand == null)
                {
                    _nextPlanogramPageCommand =
                        new RelayCommand(
                        p => NextPlanogramPage_Executed(),
                        p => NextPlanogramPage_CanExecute())
                        {
                            SmallIcon = ImageResources.Workpackages_NextPage
                        };
                }
                return _nextPlanogramPageCommand;
            }
        }

        private Boolean NextPlanogramPage_CanExecute()
        {
            return (CurrentPage < PageCount);
        }

        private void NextPlanogramPage_Executed()
        {
            CurrentPage++;
            RefreshPlanogramRows(true);
        }

        #endregion

        #region PrevPlanogramPage

        private RelayCommand _prevPlanogramPageCommand;

        public RelayCommand PrevPlanogamPageCommand
        {
            get
            {
                if (_prevPlanogramPageCommand == null)
                {
                    _prevPlanogramPageCommand =
                        new RelayCommand(
                        p => PrevPlanogramPage_Executed(),
                        p => PrevPlanogramPage_CanExecute())
                        {
                            SmallIcon = ImageResources.Workpackages_PrevPage
                        };
                }
                return _prevPlanogramPageCommand;
            }
        }

        private Boolean PrevPlanogramPage_CanExecute()
        {
            return (CurrentPage > 1);
        }

        private void PrevPlanogramPage_Executed()
        {
            CurrentPage--;
            RefreshPlanogramRows(true);
        }

        #endregion

        #region FirstPlanogramPage

        private RelayCommand _firstPlanogramPageCommand;

        public RelayCommand FirstPlanogamPageCommand
        {
            get
            {
                if (_firstPlanogramPageCommand == null)
                {
                    _firstPlanogramPageCommand =
                        new RelayCommand(
                        p => FirstPlanogramPage_Executed(),
                        p => FirstPlanogramPage_CanExecute())
                        {
                            SmallIcon = ImageResources.Workpackages_FirstPage
                        };
                }
                return _firstPlanogramPageCommand;
            }
        }

        private Boolean FirstPlanogramPage_CanExecute()
        {
            return (CurrentPage > 1);
        }

        private void FirstPlanogramPage_Executed()
        {
            CurrentPage = 1;
            RefreshPlanogramRows(true);
        }

        #endregion

        #region LastPlanogramPage

        private RelayCommand _lastPlanogramPageCommand;

        public RelayCommand LastPlanogamPageCommand
        {
            get
            {
                if (_lastPlanogramPageCommand == null)
                {
                    _lastPlanogramPageCommand =
                        new RelayCommand(
                        p => LastPlanogramPage_Executed(),
                        p => LastPlanogramPage_CanExecute())
                        {
                            SmallIcon = ImageResources.Workpackages_LastPage
                        };
                }
                return _lastPlanogramPageCommand;
            }
        }

        private Boolean LastPlanogramPage_CanExecute()
        {
            return (CurrentPage < PageCount);
        }

        private void LastPlanogramPage_Executed()
        {
            CurrentPage = PageCount;
            RefreshPlanogramRows(true);
        }

        #endregion
        
        #region EventLog

        private RelayCommand _eventLogCommand;

        public RelayCommand EventLogCommand
        {
            get
            {
                if (_eventLogCommand == null)
                {
                    _eventLogCommand =
                        new RelayCommand(
                        p => EventLog_Executed(p),
                        p => EventLog_CanExecute())
                        {
                            FriendlyName = Message.WorkPackages_EventLog,
                            FriendlyDescription = Message.WorkPackages_EventLog_Desc,
                            Icon = ImageResources.WorkPackages_EventLog,
                            DisabledReason = Message.WorkPackages_EventLog_DisabledReason,
                            SmallIcon = ImageResources.WorkPackages_EventLog
                        };
                }
                return _eventLogCommand;
            }
        }

        private Boolean EventLog_CanExecute()
        {
            return this.SelectedWorkpackageRow != null;
        }

        private void EventLog_Executed(Object selectedPlanObject)
        {
            if (this.SelectedWorkpackageRow != null)
            {
                base.ShowWaitCursor(true);

                //fetch the workpackage
                Workpackage package;
                try
                {
                    package = Workpackage.FetchById(this.SelectedWorkpackageRow.Info.Id);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    Exception rootEx = ex.GetBaseException();
                    LocalHelper.RecordException(rootEx);

                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                        this.SelectedWorkpackageRow.Info.Name, OperationType.Open);

                    return;
                }

                base.ShowWaitCursor(false);

                // Check if there is a parameter specified i.e. the command was initiated by 
                // a right-click context menu item on the PlanogramGrid
                String selectedPlanName = selectedPlanObject as String;

                WorkpackageEventLogViewModel viewModel = new WorkpackageEventLogViewModel(package, selectedPlanName);
                viewModel.ShowDialog();
            }
        }

        #endregion

        #region ExportToFile

        private RelayCommand _exportToFileCommand;

        /// <summary>
        /// Shows the export planograms window
        /// </summary>
        public RelayCommand ExportToFileCommand
        {
            get
            {
                if (_exportToFileCommand == null)
                {
                    _exportToFileCommand =
                        PlanogramExportHelper.CreateShowExportWindowCommand(
                        this.SelectedPlanogramRows.Select(p => p.Info));
                    base.ViewModelCommands.Add(_exportToFileCommand);
                }
                return _exportToFileCommand;
            }
        }

        #endregion
        
        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the available workpackages model is about to change.
        /// </summary>
        private void AvailableWorkpackagesView_ModelChanging(object sender, ViewStateObjectModelChangingEventArgs<WorkpackageInfoList> e)
        {
            this.IsUpdating = true;
        }

        /// <summary>
        /// Called whenever the list of available workpackages changes.
        /// </summary>
        private void AvailableWorkpackagesView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<WorkpackageInfoList> e)
        {
            WorkpackageRow oldSelected = this.SelectedWorkpackageRow;

            if (e.NewModel != null)
            {
                UpdateWorkpackageSelection(oldSelected, e.NewModel.ToList());
            }
            else
            {
                _availableWorkpackageRows.Clear();
                this.IsUpdating = false;
                this.SelectedWorkpackageRow = null;
                this.PendingCount = 0;
                this.QueuedCount = 0;
                this.ProcessingCount = 0;
                this.CompleteCount = 0;
                this.FailedCount = 0;
            }
        }

        /// <summary>
        /// Carries out actions when the selected workpackage info property value changes.
        /// </summary>
        private void OnSelectedWorkpackageRowChanged(WorkpackageRow oldValue, WorkpackageRow newValue)
        {
            Boolean isSameWorkpackage = (oldValue != null && newValue != null && oldValue.Info.Id == newValue.Info.Id);

            if (newValue != null)
            {
                //refresh planogram rows
                RefreshPlanogramRows(/*clearExisting*/!isSameWorkpackage);

                if (!isSameWorkpackage)
                {
                    //start fetching the related tasks as the workflow has changed
                    _currentWorkpackageTasksView.FetchByWorkpackageIdAsync(newValue.Info.Id);
                }
            }
            else
            {
                _planogramRows.Clear();
                _currentWorkpackageTasksView.Model = null;
                _currentWorkpackagePlansView.Model = null;
                this.SelectedPlanogramRow = null;
                this.CurrentPage = 1;
                this.PageCount = 1;
            }

        }

        /// <summary>
        /// Called whenever the value of the SelectedProcessingStatusFilterType property changes
        /// </summary>
        private void OnSelectedProcessingStatusFilterTypeChanged(ProcessingStatusFilterType oldValue, ProcessingStatusFilterType newValue)
        {
            RefreshPlanogramRows(true);
        }

        /// <summary>
        /// Called whenever the CurrentWorkpackageTasksView model changes
        /// </summary>
        private void CurrentWorkpackageTasksView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<WorkflowTaskInfoList> e)
        {
            OnPropertyChanged(CurrentWorkpackageTasksProperty);
        }

        /// <summary>
        /// Called whenever the current workpackages plans list model changes.
        /// </summary>
        private void CurrentWorkpackagePlansView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramInfoList> e)
        {
            var planInfos = e.NewModel;
            IsFetchingPlanogramRows = false;

            //if the model is null then just tidy up.
            if (planInfos == null)
            {
                if (_planogramRows.Count > 0) _planogramRows.Clear();
                return;
            }

            //  Prefetch all the validation infos that might exist for the currently displayed planograms.
            var validationInfos = PlanogramValidationInfoList.FetchByPlanogramIds(planInfos.Select(i => i.Id).Distinct());

            if (_planogramRows.Count == 0)
            {
                //we are starting from scratch so just reload.
                var planogramRows = new List<PlanogramRepositoryRow>(planInfos.Count);
                if (_selectedWorkpackageRow != null)
                {
                    planogramRows.AddRange(
                        planInfos.Select(
                            i => new PlanogramRepositoryRow(_selectedWorkpackageRow.Info, i, validationInfos.FirstOrDefault(vi => vi.PlanogramId == i.Id))));
               
                    _planogramRows.AddRange(planogramRows);
                }

                SelectedPlanogramRow = planogramRows.FirstOrDefault();
            }
            else
            {
                //try to update whereever possible.

                //remove all old rows
                _planogramRows.RemoveRange(
                    _planogramRows.Where(p => !planInfos.Select(i => i.Id).ToList().Contains(p.Info.Id))
                    .ToList());

                // Planograms may not be unique in the list (due to the actions of multiple processors), 
                // but we still want to index the rows for retrieval later. Rather than using a dictionary
                // we can key them into a lookup.
                ILookup<Int32,PlanogramRepositoryRow> planogramRowsById = _planogramRows.ToLookup(p => p.Info.Id);

                //update or add new ones
                List<PlanogramRepositoryRow> newRows = new List<PlanogramRepositoryRow>();
                foreach (PlanogramInfo planInfo in planInfos)
                {
                    var validationInfo = validationInfos.FirstOrDefault(vi => vi.PlanogramId == planInfo.Id);

                    if (planogramRowsById.Contains(planInfo.Id))
                    {
                        foreach (PlanogramRepositoryRow row in planogramRowsById[planInfo.Id])
                        {
                            row.UpdateInfo(_selectedWorkpackageRow.Info, planInfo, validationInfo);
                        }
                    }
                    else
                    {
                        newRows.Add(new PlanogramRepositoryRow(_selectedWorkpackageRow.Info, planInfo, validationInfo));
                    }

                }
                if (newRows.Count > 0) _planogramRows.AddRange(newRows);

                //reset the selected row.
                if (this.SelectedPlanogramRow == null
                    || !_planogramRows.Contains(this.SelectedPlanogramRow))
                {
                    this.SelectedPlanogramRow = this.PlanogramRows.FirstOrDefault();
                }

            }

            Int32 availablePlanCount = 0;
            if (this.SelectedWorkpackageRow != null)
            {
                switch (this.SelectedProcessingStatusFilterType)
                {
                    case ProcessingStatusFilterType.All:
                        availablePlanCount = this.SelectedWorkpackageRow.Info.PlanogramCount;
                        break;
                    //case ProcessingStatusFilterType.Pending:
                    //    availablePlanCount = this.SelectedWorkpackageRow.Info.PlanogramPendingCount;
                    //    break;
                    case ProcessingStatusFilterType.Queued:
                        availablePlanCount = this.SelectedWorkpackageRow.Info.PlanogramQueuedCount + this.SelectedWorkpackageRow.Info.PlanogramPendingCount;
                        break;
                    case ProcessingStatusFilterType.Processing:
                        availablePlanCount = this.SelectedWorkpackageRow.Info.PlanogramProcessingCount;
                        break;
                    case ProcessingStatusFilterType.Complete:
                        availablePlanCount = this.SelectedWorkpackageRow.Info.PlanogramCompleteCount;
                        break;
                    case ProcessingStatusFilterType.Failed:
                        availablePlanCount = this.SelectedWorkpackageRow.Info.PlanogramFailedCount;
                        break;
                    default:
                        break;
                }
            }
            this.PageCount = availablePlanCount > 0 ? (Byte)Math.Ceiling((double)(availablePlanCount / (double)100)) : (Byte)1;
            if (this.CurrentPage > this.PageCount) this.CurrentPage = 1;
        }

        /// <summary>
        /// Called whenever the collection of selected planogram infos changes.
        /// </summary>
        private void SelectedPlanogramRows_CollectionChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            
            //Set the last selected row as the active planogram
            PlanogramRepositoryRow lastSelected = SelectedPlanogramRows.LastOrDefault();

            if (_selectedPlanogramRow != lastSelected)
            {
                //V8-30160: 
                //The usual on property change update is not used so that the following error does not occur:
                //  "Cannot change ObservableCollection during a CollectionChanged event".
                //Oddly does did not happen for all users! An explicit update for the planogram info panel is
                //now used to handle planogram row changes. So that visually the control works the same as before.
                _selectedPlanogramRow = (lastSelected != null) ? lastSelected : null; ;
                OnPropertyChanged(SelectedPlanogramInfoProperty);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the available workpackages view.
        /// </summary>
        public void RefreshAvailableWorkpackages()
        {
            _availableWorkpackagesView.FetchForCurrentEntityAsync();
        }

        /// <summary>
        /// Refreshes workpackage data to selected filter type
        /// </summary>
        public void RefreshFilter()
        {
            UpdateWorkpackageSelection(this.SelectedWorkpackageRow, this.AvailableWorkpackages.ToList());
        }

        /// <summary>
        /// Refreshes the planogram rows collection asyncronously.
        /// </summary>
        /// <param name="clearExisting">if true, existing rows will be cleared.</param>
        private void RefreshPlanogramRows(Boolean clearExisting)
        {
            if (clearExisting)
            {
                _planogramRows.Clear();
            }

            if (this.SelectedWorkpackageRow == null) return;

            //set the busy flag
            this.IsFetchingPlanogramRows = true;

            //fetch by selected processing type.
            Int32 workpackageId = this.SelectedWorkpackageRow.Info.Id;
            switch (this.SelectedProcessingStatusFilterType)
            {
                case ProcessingStatusFilterType.All:
                    _currentWorkpackagePlansView.FetchByWorkpackageIdPagingCriteriaAsync(workpackageId, CurrentPage, 100);
                    break;

                case ProcessingStatusFilterType.Complete:
                    _currentWorkpackagePlansView.FetchByWorkpackageIdAutomationStatusPagingCriteriaAsync(workpackageId,
                        new List<ProcessingStatus>{ProcessingStatus.Complete, ProcessingStatus.CompleteWithWarnings}, 
                        CurrentPage, 100);
                    break;

                case ProcessingStatusFilterType.Failed:
                    _currentWorkpackagePlansView.FetchByWorkpackageIdAutomationStatusPagingCriteriaAsync(workpackageId, ProcessingStatus.Failed, CurrentPage, 100);
                    break;

                case ProcessingStatusFilterType.Pending:
                    _currentWorkpackagePlansView.FetchByWorkpackageIdAutomationStatusPagingCriteriaAsync(workpackageId, ProcessingStatus.Pending, CurrentPage, 100);
                    break;

                case ProcessingStatusFilterType.Processing:
                    _currentWorkpackagePlansView.FetchByWorkpackageIdAutomationStatusPagingCriteriaAsync(workpackageId, ProcessingStatus.Processing, CurrentPage, 100);
                    break;

                case ProcessingStatusFilterType.Queued:
                    _currentWorkpackagePlansView.FetchByWorkpackageIdAutomationStatusPagingCriteriaAsync(workpackageId, new List<ProcessingStatus> { ProcessingStatus.Queued, ProcessingStatus.Pending }, CurrentPage, 100);
                    break;
            }
        }

        /// <summary>
        /// Starts this screen polling for data.
        /// </summary>
        public void StartPolling()
        {
            if (!_availableWorkpackagesView.IsPolling)
            {
                _availableWorkpackagesView.BeginPollingFetchForCurrentEntity();
            }
        }

        /// <summary>
        /// Stops this screen polling for data.
        /// </summary>
        public void StopPolling()
        {
            _availableWorkpackagesView.StopPollingAsync();
        }

        /// <summary>
        /// Refreshes workpacakge selections based upon filter 
        /// </summary>
        /// <param name="oldSelected">The current selected workpacakge row</param>
        /// <param name="availableWorkpackageInfos">The available workpackage info list</param>
        private void UpdateWorkpackageSelection(WorkpackageRow oldSelected, List<WorkpackageInfo> availableWorkpackageInfos)
        {
            List<WorkpackageInfo> filteredList = new List<WorkpackageInfo>();

            switch (this.SelectedFilterType)
            {
                case WorkpackageFilterType.ShowAll:
                    filteredList = availableWorkpackageInfos.ToList();
                    break;
                case WorkpackageFilterType.ShowUserOnly:
                    filteredList = availableWorkpackageInfos.Where(p => p.UserId == DomainPrincipal.CurrentUserId).ToList();
                    break;
                default:
                    break;
            }

            this.AllCount = filteredList.Count;
            this.PendingCount = filteredList.Count(ConvertProcessingStatusFilterType(ProcessingStatusFilterType.Pending));
            this.QueuedCount = filteredList.Count(ConvertProcessingStatusFilterType(ProcessingStatusFilterType.Queued)) + PendingCount;
            this.ProcessingCount = filteredList.Count(ConvertProcessingStatusFilterType(ProcessingStatusFilterType.Processing));
            this.CompleteCount = filteredList.Count(ConvertProcessingStatusFilterType(ProcessingStatusFilterType.Complete));
            this.FailedCount = filteredList.Count(ConvertProcessingStatusFilterType(ProcessingStatusFilterType.Failed));

            if (this.SelectedWorkpackageProcessingStatusFilterType == ProcessingStatusFilterType.Queued)
            {
                filteredList = filteredList.Where(p => p.ProcessingStatus == WorkpackageProcessingStatusType.Queued || p.ProcessingStatus == WorkpackageProcessingStatusType.Pending).ToList();
            }
            else
            {
                filteredList = filteredList.Where(ConvertProcessingStatusFilterType(this.SelectedWorkpackageProcessingStatusFilterType)).ToList();
            }

            //update the workpackage rows.
            Dictionary<Int32, WorkpackageRow> rowToId = this.AvailableWorkpackageRows.ToDictionary(w => w.Info.Id);

            //remove any old rows
            _availableWorkpackageRows.RemoveRange(
                _availableWorkpackageRows.Where(r => !(filteredList.Select(w => w.Id)).Contains(r.Info.Id))
                .ToList());

            List<WorkpackageRow> newRows = new List<WorkpackageRow>();

            //cycle through updating existing and adding new
            foreach (WorkpackageInfo info in filteredList)
            {
                WorkpackageRow row;
                if (rowToId.TryGetValue(info.Id, out row))
                {
                    row.UpdateInfo(info);
                }
                else
                {
                    newRows.Add(new WorkpackageRow(info));
                }
            }
            if (newRows.Count > 0) _availableWorkpackageRows.AddRange(newRows);

            this.IsUpdating = false;

            //try to reselect the orginal workpackage info or if not select the first available
            this.SelectedWorkpackageRow =
                (oldSelected != null) ?
                    _availableWorkpackageRows.FirstOrDefault(w => w.Info.Id == oldSelected.Info.Id) :
                   _availableWorkpackageRows.OrderByDescending(w => w.Info.DateCreated).FirstOrDefault();
        }

        /// <summary>
        /// Converts ProcessingStatusFilterType to ProcessingStatus
        /// </summary>
        /// <param name="selectedProcessingStatusFilterType">The ProcessingStatusFilterType</param>
        /// <returns></returns>
        private static Func<WorkpackageInfo, Boolean> ConvertProcessingStatusFilterType(ProcessingStatusFilterType selectedProcessingStatusFilterType)
        {
            switch (selectedProcessingStatusFilterType)
            {
                case ProcessingStatusFilterType.All:
                    return p => true;
                case ProcessingStatusFilterType.Pending:
                    return p => p.ProcessingStatus == WorkpackageProcessingStatusType.Pending;
                case ProcessingStatusFilterType.Queued:
                    return p => p.ProcessingStatus == WorkpackageProcessingStatusType.Queued;
                case ProcessingStatusFilterType.Processing:
                    return p => p.ProcessingStatus == WorkpackageProcessingStatusType.Processing;
                case ProcessingStatusFilterType.Complete:
                    return p => p.ProcessingStatus == WorkpackageProcessingStatusType.Complete
                        || p.ProcessingStatus == WorkpackageProcessingStatusType.CompleteWithAllWarnings
                        || p.ProcessingStatus == WorkpackageProcessingStatusType.CompleteWithSomeFailed
                        || p.ProcessingStatus == WorkpackageProcessingStatusType.CompleteWithSomeWarnings;
                case ProcessingStatusFilterType.Failed:
                    return p => p.ProcessingStatus == WorkpackageProcessingStatusType.Failed;
            }
            return p => p.ProcessingStatus == WorkpackageProcessingStatusType.Complete;
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _availableWorkpackagesView.ModelChanging -= AvailableWorkpackagesView_ModelChanging;
                    _availableWorkpackagesView.ModelChanged -= AvailableWorkpackagesView_ModelChanged;
                    _currentWorkpackageTasksView.ModelChanged -= CurrentWorkpackageTasksView_ModelChanged;
                    _currentWorkpackagePlansView.ModelChanged -= CurrentWorkpackagePlansView_ModelChanged;
                    _selectedPlanogramRows.CollectionChanged -= SelectedPlanogramRows_CollectionChanged;

                    _availableWorkpackagesView.Dispose();
                    _currentWorkpackagePlansView.Dispose();
                    _currentWorkpackageTasksView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
        
    }

    #region Supporting Classes

    public enum WorkpackageFilterType
    {
        ShowAll,
        ShowUserOnly
    }

    #endregion
}
