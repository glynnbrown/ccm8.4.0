﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25736 : L.Ineson ~ Created.
// CCM-25736 : N.Haywood
//  Added SelectedPlanogramInfo
// V8-28127 : A.Probyn
//  Added defensive code to OpenPlanogram_Executed
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageDebug
{
    /// <summary>
    /// Viewmodel controller for the workpackage debug window.
    /// </summary>
    public sealed class WorkpackageDebugViewModel : ViewModelAttachedControlObject<WorkpackageDebugWindow>
    {
        #region Fields
        private Boolean? _dialogResult;

        private readonly Workpackage _workpackage;
        private readonly WorkflowTaskInfoList _workflowTasks;
        private WorkpackagePlanogram _selectedWorkpackagePlan;

        private readonly BulkObservableCollection<WorkpackageDebugItem> _taskPlanograms = new BulkObservableCollection<WorkpackageDebugItem>();
        private ReadOnlyBulkObservableCollection<WorkpackageDebugItem> _taskPlanogramsRO;
        private WorkpackageDebugItem _selectedTaskPlanogram;
        #endregion

        #region Binding Property paths

        //Properties
        public static readonly PropertyPath WorkpackageProperty = WpfHelper.GetPropertyPath<WorkpackageDebugViewModel>(p => p.Workpackage);
        public static readonly PropertyPath SelectedPlanogramProperty = WpfHelper.GetPropertyPath<WorkpackageDebugViewModel>(p => p.SelectedWorkpackagePlan);
        public static readonly PropertyPath TaskPlanogramsProperty = WpfHelper.GetPropertyPath<WorkpackageDebugViewModel>(p => p.TaskPlanograms);
        public static readonly PropertyPath SelectedTaskPlanogramProperty = WpfHelper.GetPropertyPath<WorkpackageDebugViewModel>(p => p.SelectedTaskPlanogram);
        public static readonly PropertyPath SelectedPlanogramInfoProperty = WpfHelper.GetPropertyPath<WorkpackageDebugViewModel>(p => p.SelectedPlanogramInfo);

        //Commands
        public static readonly PropertyPath OpenPlanogramProperty = WpfHelper.GetPropertyPath<WorkpackageDebugViewModel>(p => p.OpenPlanogramCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<WorkpackageDebugViewModel>(p => p.CloseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Gets the workpackage context
        /// </summary>
        public Workpackage Workpackage
        {
            get { return _workpackage; }
        }

        /// <summary>
        /// Gets/Sets the selected planogram
        /// </summary>
        public WorkpackagePlanogram SelectedWorkpackagePlan
        {
            get { return _selectedWorkpackagePlan; }
            set
            {
                _selectedWorkpackagePlan = value;
                OnPropertyChanged(SelectedPlanogramProperty);

                OnSelectedWorkpackagePlanChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of task planograms for the selected workpackage plan.
        /// </summary>
        public ReadOnlyBulkObservableCollection<WorkpackageDebugItem> TaskPlanograms
        {
            get
            {
                if (_taskPlanogramsRO == null)
                {
                    _taskPlanogramsRO = new ReadOnlyBulkObservableCollection<WorkpackageDebugItem>(_taskPlanograms);
                }
                return _taskPlanogramsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected task planogram.
        /// </summary>
        public WorkpackageDebugItem SelectedTaskPlanogram
        {
            get { return _selectedTaskPlanogram; }
            set
            {
                if (_selectedTaskPlanogram != value)
                {
                    _selectedTaskPlanogram = value;
                    OnPropertyChanged(SelectedTaskPlanogramProperty);
                    OnPropertyChanged(SelectedPlanogramInfoProperty);
                }
            }
        }

        /// <summary>
        /// Returns the info the the selected task planogram.
        /// </summary>
        public PlanogramInfo SelectedPlanogramInfo
        {
            get
            {
                if (_selectedTaskPlanogram != null)
                {
                    return _selectedTaskPlanogram.Planogram;
                }
                return null;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkpackageDebugViewModel(Workpackage workpackage)
        {
            _workpackage = workpackage;
            _workflowTasks = WorkflowTaskInfoList.FetchByWorkflowId(_workpackage.WorkflowId);
        }

        #endregion

        #region Commands

        #region OpenPlanogram Command

        private RelayCommand _openPlanogramCommand;

        /// <summary>
        /// Opens the currently selected planogram in the editor.
        /// </summary>
        public RelayCommand OpenPlanogramCommand
        {
            get
            {
                if (_openPlanogramCommand == null)
                {
                    _openPlanogramCommand = new RelayCommand(
                        p => OpenPlanogram_Executed(),
                        p => OpenPlanogram_CanExecute())
                        {
                            FriendlyName = Message.WorkPackages_OpenPlan,
                            FriendlyDescription = Message.WorkPackages_OpenPlan_Desc,
                            SmallIcon = ImageResources.WorkPackages_OpenPlan,
                        };
                    base.ViewModelCommands.Add(_openPlanogramCommand);
                }
                return _openPlanogramCommand;
            }
        }

        private Boolean OpenPlanogram_CanExecute()
        {
            if (!App.ViewState.IsEditorClientAvailable)
            {
                return false;
            }

            if (this.SelectedTaskPlanogram == null || this.SelectedTaskPlanogram.Planogram == null)
            {
                this.OpenPlanogramCommand.DisabledReason = Message.WorkPackages_NoPlanogramSelected;
                return false;
            }

            return true;
        }

        private void OpenPlanogram_Executed()
        {
            if (this.AttachedControl != null)
            {
                if (this.SelectedTaskPlanogram.Planogram != null)
                {
                    LocalHelper.OpenPlanogramsInEditor(new List<PlanogramInfo> { this.SelectedTaskPlanogram.Planogram });
                }
            }
        }

        #endregion

        #region Close Command

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the current window.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => CloseCommand_Execute())
                        {
                            FriendlyName = Message.Generic_Close
                        };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void CloseCommand_Execute()
        {
            this.DialogResult = true;
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenver the value of the selected planogram property changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedWorkpackagePlanChanged(WorkpackagePlanogram newValue)
        {
            _taskPlanograms.Clear();
            this.SelectedTaskPlanogram = null;

            if (newValue != null)
            {
                //Fetch the list of debug plans and their respective Planogram Infos.
                WorkpackagePlanogramDebugList debugPlanList = null;
                PlanogramInfoList planInfos = null;
                try
                {
                    debugPlanList = WorkpackagePlanogramDebugList.FetchBySourcePlanogramId(newValue.DestinationPlanogramId);

                    planInfos = PlanogramInfoList.FetchByIds(debugPlanList.Select(p => p.DebugPlanogramId));
                }
                catch (DataPortalException ex)
                {
                    Exception rootEx = ex.GetBaseException();
                    LocalHelper.RecordException(rootEx);

                    CommonHelper.GetWindowService().ShowErrorMessage(
                        Message.WorkpackageDebug_DebugPlanFetchFailed_Header, rootEx.Message);
                }

                if (debugPlanList != null)
                {
                    //add the infos for each task.
                    foreach (WorkflowTaskInfo task in _workflowTasks.OrderBy(t => t.SequenceId))
                    {
                        PlanogramInfo info = null;

                        //get the info for the debug plan for this task.
                        WorkpackagePlanogramDebug debugPlan =
                            debugPlanList.FirstOrDefault(d => d.WorkflowTaskId == task.Id);
                        if (debugPlan != null)
                        {
                            info = planInfos.FirstOrDefault(p => p.Id == debugPlan.DebugPlanogramId);
                        }


                        _taskPlanograms.Add(new WorkpackageDebugItem(task, info));
                    }

                    //set the planogram for the last task as this is the final result.
                    this.SelectedTaskPlanogram = _taskPlanograms.LastOrDefault();
                }
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {

                }

                IsDisposed = true;
            }
        }

        #endregion
    }


}
