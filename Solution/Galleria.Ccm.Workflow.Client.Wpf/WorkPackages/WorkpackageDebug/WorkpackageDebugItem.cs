﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26477 : L.Ineson
//  Put into own file.
// V8-27058 : A.Probyn ~ Changed over to use PlanogramMetadataImageViewModel for thumbnail
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageDebug
{
    /// <summary>
    /// View of a workpackage debug planogram item.
    /// </summary>
    public sealed class WorkpackageDebugItem : INotifyPropertyChanged
    {
        #region Fields

        private readonly WorkflowTaskInfo _taskDetails;
        private readonly PlanogramInfo _planogram;
        private readonly PlanogramMetadataImageViewModel _selectedPlanogramThumbnailView = new PlanogramMetadataImageViewModel();
        private Boolean _isLoadingThumbnail;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath PlanogramThumbnailProperty = WpfHelper.GetPropertyPath<WorkpackageDebugItem>(p => p.PlanogramThumbnail);
        public static readonly PropertyPath IsLoadingThumbnailProperty = WpfHelper.GetPropertyPath<WorkpackageDebugItem>(p => p.IsLoadingThumbnail);


        #endregion

        #region Properties

        /// <summary>
        /// The task that this item links to.
        /// </summary>
        public WorkflowTaskInfo TaskDetails
        {
            get { return _taskDetails; }
        }

        /// <summary>
        /// The actual item plan info.
        /// </summary>
        public PlanogramInfo Planogram
        {
            get { return _planogram; }
        }

        /// <summary>
        /// The name of the task.
        /// </summary>
        public String TaskName
        {
            get
            {
                return String.Format(CultureInfo.CurrentCulture,
                    "{0}: {1}", TaskDetails.SequenceId + 1, TaskDetails.Details.Name);
            }
        }

        /// <summary>
        /// The plan thumbnail itemsource.
        /// </summary>
        public ImageSource PlanogramThumbnail
        {
            get
            {
                if (_selectedPlanogramThumbnailView.Model != null)
                {
                    return ImageHelper.GetBitmapImage(_selectedPlanogramThumbnailView.Model.ImageData);
                }
                return null;
            }
        }

        /// <summary>
        /// Returns true if the thumbnail is loading.
        /// </summary>
        public Boolean IsLoadingThumbnail
        {
            get { return _isLoadingThumbnail; }
            private set
            {
                _isLoadingThumbnail = value;
                OnPropertyChanged(IsLoadingThumbnailProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public WorkpackageDebugItem(WorkflowTaskInfo task, PlanogramInfo plan)
        {
            _taskDetails = task;
            _planogram = plan;

            if (plan != null)
            {
                this.IsLoadingThumbnail = true;
                _selectedPlanogramThumbnailView.ModelChanged += SelectedPlanogramThumbnailView_ModelChanged;
                _selectedPlanogramThumbnailView.FetchThumbnailByPlanogramIdAsync(plan.Id, renderIfNotAvailable:true);
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenev the SelectedPlanogramThumbnailView model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedPlanogramThumbnailView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramMetadataImage> e)
        {
            _selectedPlanogramThumbnailView.ModelChanged -= SelectedPlanogramThumbnailView_ModelChanged;
            OnPropertyChanged(PlanogramThumbnailProperty);

            this.IsLoadingThumbnail = false;
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
