﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25736 : L.Ineson
//  Created
// CCM-26477 : L.Ineson
//  Added LoadPropertiesPanelDefinitions
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.PropertyEditor;
using Galleria.Framework.Planograms.Model;
using System.ComponentModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageDebug
{
    /// <summary>
    /// Interaction logic for WorkpackageDebugWindow.xaml
    /// </summary>
    public sealed partial class WorkpackageDebugWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(WorkpackageDebugViewModel), typeof(WorkpackageDebugWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));


        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WorkpackageDebugWindow senderControl = (WorkpackageDebugWindow)obj;

            if (e.OldValue != null)
            {
                WorkpackageDebugViewModel oldModel = (WorkpackageDebugViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                WorkpackageDebugViewModel newModel = (WorkpackageDebugViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }

        }

        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public WorkpackageDebugViewModel ViewModel
        {
            get { return (WorkpackageDebugViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public WorkpackageDebugWindow(WorkpackageDebugViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.WorkpackageTaskExplorer);

            this.ViewModel = viewModel;

            this.Loaded += WorkpackageDebugWindow_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WorkpackageDebugWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= WorkpackageDebugWindow_Loaded;

            LoadPropertiesPanelDefinitions();

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }), priority: DispatcherPriority.Background);
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == WorkpackageDebugViewModel.SelectedPlanogramInfoProperty.Path)
            {
                LoadPropertiesPanelDefinitions();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the property definitions into the properties panel.
        /// </summary>
        private void LoadPropertiesPanelDefinitions()
        {
            var propertyPanel = this.propEditor;

            //clear out any existing definitions.
            if (propertyPanel.PropertyDescriptions.Count > 0) propertyPanel.PropertyDescriptions.Clear();

            if (this.ViewModel != null && this.ViewModel.SelectedPlanogramInfo != null)
            {
                //load in the new ones.
                DisplayUnitOfMeasureCollection uomValues = 
                    DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(this.ViewModel.SelectedPlanogramInfo);

                //properties already displayed in other section so can be excluded here:
                List<IModelPropertyInfo> excludedDefs =
                    new List<IModelPropertyInfo>()
                {
                    PlanogramInfo.NameProperty,
                    PlanogramInfo.StatusProperty,
                     PlanogramInfo.DateCreatedProperty,
                     PlanogramInfo.DateLastModifiedProperty,
                     PlanogramInfo.DateMetadataCalculatedProperty,
                     PlanogramInfo.DateValidationDataCalculatedProperty,
                     PlanogramInfo.UserNameProperty,
                     PlanogramInfo.PlanogramGroupNameProperty
                };

                List<PropertyItemDescription> definitions = new List<PropertyItemDescription>();
                foreach (IModelPropertyInfo propertyInfo in PlanogramInfo.EnumerateDisplayablePropertyInfos())
                {
                    if (!excludedDefs.Contains(propertyInfo))
                    {
                        definitions.Add(CommonHelper.GetPropertyDescription(propertyInfo, uomValues));
                    }
                }
                propertyPanel.PropertyDescriptions.AddRange(definitions);      
            }
        }


        #endregion

        #region Window close

        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    IDisposable disposableViewModel = (IDisposable)this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }

        }


        #endregion

        /// <summary>
        /// Key event to get out of the properties grid without tabbing through every attribute
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void propEditor_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                Keyboard.Focus(btnCancel);
             

                e.Handled = true;              
            }
        }
    }
}
