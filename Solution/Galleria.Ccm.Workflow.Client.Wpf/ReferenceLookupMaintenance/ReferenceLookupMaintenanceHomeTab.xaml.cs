﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 :J.Pickup
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReferenceLookupMaintenance
{
    /// <summary>
    /// Interaction logic for ReferenceLookupMaintenanceHomeTab.xaml
    /// </summary>
    public partial class ReferenceLookupMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReferenceLookupMaintenanceViewModel), typeof(ReferenceLookupMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReferenceLookupMaintenanceViewModel ViewModel
        {
            get { return (ReferenceLookupMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReferenceLookupMaintenanceHomeTab senderControl = (ReferenceLookupMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                ReferenceLookupMaintenanceViewModel oldModel = (ReferenceLookupMaintenanceViewModel)e.OldValue;

            }

            if (e.NewValue != null)
            {
                ReferenceLookupMaintenanceViewModel newModel = (ReferenceLookupMaintenanceViewModel)e.NewValue;


            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ReferenceLookupMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
