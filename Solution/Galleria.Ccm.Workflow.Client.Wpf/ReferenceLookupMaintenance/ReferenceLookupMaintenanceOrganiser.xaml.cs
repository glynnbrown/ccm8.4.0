﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReferenceLookupMaintenance
{
    /// <summary>
    /// Interaction logic for ReferenceLookupMaintenanceOrganiser.xaml
    /// </summary>
    public partial class ReferenceLookupMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReferenceLookupMaintenanceViewModel), typeof(ReferenceLookupMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReferenceLookupMaintenanceViewModel ViewModel
        {
            get { return (ReferenceLookupMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReferenceLookupMaintenanceOrganiser senderControl = (ReferenceLookupMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                ReferenceLookupMaintenanceViewModel oldModel = (ReferenceLookupMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ReferenceLookupMaintenanceViewModel newModel = (ReferenceLookupMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ReferenceLookupMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            //TODOJP:
            //HelpFileKeys.SetHelpFile(this, HelpFileKeys);

            this.ViewModel = new ReferenceLookupMaintenanceViewModel();

            this.Loaded += new RoutedEventHandler(ProductMaintenanceOrganiser_Loaded);

        }

        private void ProductMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProductMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        #endregion

        #region Window Close


        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }


        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
