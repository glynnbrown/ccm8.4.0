﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 :J.Pickup
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Input;
using System.Diagnostics;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common.ModelObjectViewModels;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReferenceLookupMaintenance
{
    public class ReferenceLookupMaintenanceViewModel: ViewModelAttachedControlObject<ReferenceLookupMaintenanceOrganiser>
    {
        #region Fields

        const String _exCategory = "ProductMaintenance";

        //Permissions
        private Boolean _userHasReferenceLookupCreatePerm;
        private Boolean _userHasReferenceLookupFetchPerm;
        private Boolean _userHasReferenceLookupEditPerm;
        private Boolean _userHasReferenceLookupDeletePerm;

        //Instance Variables
        private ReadOnlyCollection<ProductGroup> _productGroups;
        private ObservableCollection<ProductGroupViewModel> _productGroupViewModels = new ObservableCollection<ProductGroupViewModel>();
        private ProductGroupViewModel _selectedProductGroupViewModel;
        private ProductGroupViewModel _currentProductGroupViewModel = new ProductGroupViewModel(null);

        private ContentLookupRowViewModel _contentLookupRows = new ContentLookupRowViewModel();
        private BulkObservableCollection<ContentLookupRow> _selectedContentLookupRows = new BulkObservableCollection<ContentLookupRow>();

        #endregion

        #region Property Paths
        
        //Properties
        public static readonly PropertyPath ProductGroupsProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ProductGroups);
        public static readonly PropertyPath ProductGroupViewModelsProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ProductGroupViewModels);
        public static readonly PropertyPath SelectedProductGroupViewModelProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.SelectedProductGroupViewModel);
        public static readonly PropertyPath CurrentProductGroupViewModelProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.CurrentProductGroupViewModel);
        public static readonly PropertyPath SelectedContentLookupRowsProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p._selectedContentLookupRows);
        public static readonly PropertyPath ContentLookupRowsProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ContentLookupRows);

        //Commands
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath AutoAssignProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.AutoAssignProductUniverseCommand);
        public static PropertyPath ManualAssignProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ManualAssignProductUniverseCommand);
        public static PropertyPath ClearProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ClearProductUniverseCommand);
        public static PropertyPath AutoAssignAssortmentCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.AutoAssignAssortmentCommand);
        public static PropertyPath ManuallyAssignAssortmentCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ManuallyAssignAssortmentCommand);
        public static PropertyPath ClearAssortmentCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ClearAssortmentCommand);
        public static PropertyPath AutoAssignBlockingCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.AutoAssignBlockingCommand);
        public static PropertyPath ManuallyAssignMetricProfileCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ManuallyAssignMetricProfileCommand);
        public static PropertyPath ClearMetricProfileCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ClearMetricProfileCommand);
        public static PropertyPath ManuallyAssignInventoryProfileCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ManuallyAssignInventoryProfileCommand);
        public static PropertyPath ClearInventoryProfileCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ClearInventoryProfileCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath AutoAssignAllCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.AutoAssignAllCommand);
        public static PropertyPath AutoAssignSequenceCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.AutoAssignSequenceCommand);
        public static PropertyPath ManuallyAssignSequenceCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ManuallyAssignSequenceCommand);
        public static PropertyPath ClearSequenceCommandProperty = WpfHelper.GetPropertyPath<ReferenceLookupMaintenanceViewModel>(p => p.ClearSequenceCommand);
        

        #endregion

        #region Properties

        public ReadOnlyCollection<ProductGroup> ProductGroups
        {
            get { return _productGroups; }
        }

        public ObservableCollection<ProductGroupViewModel> ProductGroupViewModels
        {
            get { return _productGroupViewModels; }
            set 
            { 
                _productGroupViewModels = value;
                OnPropertyChanged(ProductGroupViewModelsProperty);
            }
        }

        public ProductGroupViewModel SelectedProductGroupViewModel
        {
            get { return _selectedProductGroupViewModel; }
            set 
            { 
                _selectedProductGroupViewModel = value;
                OnPropertyChanged(SelectedProductGroupViewModelProperty);
            }
        }

        public ProductGroupViewModel CurrentProductGroupViewModel
        {
            get { return _currentProductGroupViewModel; }
            set 
            { 
                _currentProductGroupViewModel = value; 
                OnPropertyChanged(CurrentProductGroupViewModelProperty);
            }
        }

        public ContentLookupRowViewModel ContentLookupRows
        {
            get { return _contentLookupRows; }
            set { _contentLookupRows = value; }
        }

        public BulkObservableCollection<ContentLookupRow> SelectedContentLookupRows
        {
            get { return _selectedContentLookupRows; }
            set { _selectedContentLookupRows = value; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Reference Lookup Maintenance View Model Contstructor
        /// </summary>
        public ReferenceLookupMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            UpdateAvailableProductGroups();
        }

        #endregion

        #region Commands

        #region Save

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasReferenceLookupEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (!ContentLookupRows.IsDirty() == true)
            {
                _saveCommand.DisabledReason = Message.Generic_Save_DisabledReasonNotDirty;
                return false;
            }

            if (this.AttachedControl != null)
            {
                //bindings must be valid
                if (!LocalHelper.AreBindingsValid(this.AttachedControl))
                {
                    this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    return false;
                }
            }

            return true;
        }

        private void Save_Executed()
        {
            base.ShowWaitCursor(true);

            if (!SaveContentLookupItems())
            {
                throw new Exception();
            }

            base.ShowWaitCursor(false);
        }

        private Boolean SaveContentLookupItems()
        {
            Boolean saved = true;

            try
            {
                _contentLookupRows.SaveContentLookupItems();
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);

                LocalHelper.RecordException(ex, _exCategory);
                Exception rootException = ex.GetBaseException();

                //If it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(SelectedProductGroupViewModel.ProductGroup.Name);
                    if (itemReloadRequired)
                    {
                        this.ContentLookupRows.UpdateContentLookupRows(_currentProductGroupViewModel.ProductGroup.Code);
                    }
                }
                else
                {
                    //Otherwise just show the user an error has occurred.
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(SelectedProductGroupViewModel.ProductGroup.Name, OperationType.Save);
                }

                ShowWaitCursor(true);
            }

            return saved;
        }

        #endregion

        #region SaveAndClose

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16,
                        DisabledReason = SaveCommand.DisabledReason,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean saved = SaveContentLookupItems();

            if(saved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            } 
            else 
            {
                //throw error
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Delete

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Delete,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            return true;
            //TODOJP: Not yet impemented
        }

        private void Delete_Executed()
        {
            MessageBox.Show("TODOJP: Not yet implemented.");
        }

        #endregion

        #region Open

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Opens the current Merchandising hierrarchy with the given id
        /// param: Int32 merchHierrarchyId
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(p =>
                        Open_Executed(), p => Open_CanExecute())
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute()
        {
            //must not be null
            if (this.SelectedProductGroupViewModel == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed()
        {
            //JPTODO: finnish implementation
            base.ShowWaitCursor(true);

            CurrentProductGroupViewModel = this._selectedProductGroupViewModel;
            CurrentProductGroupViewModel_CategoryChanged();
            
            //close the backstage
            LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            base.ShowWaitCursor(false);
        }

        #endregion

        #region AutoAssign

        #region AutoAssignAll

        private RelayCommand _autoAssignAllCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand AutoAssignAllCommand
        {
            get
            {
                if (_autoAssignAllCommand == null)
                {
                    _autoAssignAllCommand = new RelayCommand(
                        p => AutoAssignAllCommand_Executed(), p => AutoAssignAllCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignAllCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignAllCommand_Description,
                        Icon = ImageResources.ContentLookup_AutoAssign32
                    };
                    base.ViewModelCommands.Add(_autoAssignAllCommand);
                }
                return _autoAssignAllCommand;
            }
        }

        private Boolean AutoAssignAllCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignAllCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignAllCommand_Executed()
        {
            if (AutoAssignAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupAutoAssign newAutoAssignment = new ContentLookupAutoAssign();
                newAutoAssignment.Owner = this.AttachedControl;
                newAutoAssignment.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;

                newAutoAssignment.ShowDialog();

                if (newAutoAssignment.DialogResult == true)
                {
                    if (newAutoAssignment.ViewModel.AutoAssignProductUniverse)
                    {
                        this.AutoAssignProductUniverseCommand.Execute();
                    }

                    if (newAutoAssignment.ViewModel.AutoAssignAssortment)
                    {
                        this.AutoAssignAssortmentCommand.Execute();
                    }

                    if (newAutoAssignment.ViewModel.AutoAssignSequence)
                    {
                        this.AutoAssignSequenceCommand.Execute();
                    }
                }

                ShowWaitCursor(false);
            }


        }

        #endregion

        #region Product Universe Assignments

        #region AutoAssignProductUniverse

        private RelayCommand _autoAssignProductUniverseCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand AutoAssignProductUniverseCommand
        {
            get
            {
                if (_autoAssignProductUniverseCommand == null)
                {
                    _autoAssignProductUniverseCommand = new RelayCommand(
                        p => AutoAssignProductUniverseCommand_Executed(), p => AutoAssignProductUniverseCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignProductUniverseCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignProductUniverseCommand_Description,
                        Icon = ImageResources.ContentLookup_ProductUniverse,
                        SmallIcon = ImageResources.ContentLookup_ProductUniverse16,
                    };
                    base.ViewModelCommands.Add(_autoAssignProductUniverseCommand);
                }
                return _autoAssignProductUniverseCommand;
            }
        }

        private Boolean AutoAssignProductUniverseCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignProductUniverseCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignProductUniverseCommand_Executed()
        {
            if (AutoAssignAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Get all product universes for current category
                ProductUniverseInfoList associatedProductUniverses = ProductUniverseInfoList.FetchByProductGroupId(_selectedProductGroupViewModel.ProductGroup.Id);

                //Foreach Row:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    if (associatedProductUniverses.Count == 0)
                    {
                        //If the category has no associated product universes create info message
                        ShowCouldNotAutoAssign(Message.ContentLookup_ShowCouldNotAutoAssign_ProductUniverse);

                    }
                    else if (associatedProductUniverses.Count == 1)
                    {
                        //Only the one product universe so this is the most 'appropriate to add'.
                        currentRow.ContentLookup.ProductUniverseName = associatedProductUniverses.First().Name;
                    }
                    else
                    {
                        //If more than one how many products in product universe have the same products as in planogram.
                        Package currentPackage = Package.FetchById(currentRow.PlanogramInfo.Id);

                        if (currentPackage.Planograms != null && currentPackage.Planograms.Count > 0)
                        {
                            Planogram myPlanogram = currentPackage.Planograms.FirstOrDefault();
                            Dictionary<ProductUniverse, Int32> productCountDict = new Dictionary<ProductUniverse, Int32>();


                            //For each product universe we want to get a count of matching products
                            foreach (ProductUniverseInfo currentUniverse in associatedProductUniverses)
                            {
                                //At this point we now know we want full productUniverse (We may not always need this before now). 
                                ProductUniverse currentUniverseToAssess = ProductUniverse.FetchById(currentUniverse.Id);
                                Int32 countForUniverse = 0;

                                //Go through each product in planogram and count the matching products
                                foreach (PlanogramProduct planoProduct in myPlanogram.Products)
                                {
                                    Int32 cnt = currentUniverseToAssess.Products.Where(o => o.Gtin == planoProduct.Gtin).Count();
                                    countForUniverse += cnt;
                                }

                                productCountDict.Add(currentUniverseToAssess, countForUniverse);
                            }

                            //Highest key
                            var dictEntry = productCountDict.Aggregate((l, r) => l.Value > r.Value ? l : r);
                            Int32 max = dictEntry.Value;

                            //Is Distinct?
                            Int32 distinctCount = productCountDict.Values.Where(k => k == max).Count();

                            if (distinctCount == 1)
                            {
                                //When rating is distinct this is our appropriate universe.
                                currentRow.ContentLookup.ProductUniverseName = dictEntry.Key.Name;
                            }
                            else
                            {
                                //If rating not unqiue then we fall back to getting the most recently modified of the several non distinct.
                                List<ProductUniverse> universes = new List<ProductUniverse>();

                                foreach (KeyValuePair<ProductUniverse, Int32> entry in productCountDict)
                                {
                                    if (entry.Value == max)
                                    {
                                        universes.Add(entry.Key);
                                    }
                                }

                                //Unverses now is from relevant universes
                                DateTime latest = universes.Max(u => u.DateLastModified);
                                ProductUniverse appropriateUniverse = universes.Where(u => u.DateLastModified == latest).First();

                                //set name
                                currentRow.ContentLookup.ProductUniverseName = appropriateUniverse.Name;
                            }
                        }
                    }
                }

                ShowWaitCursor(false);
            }


        }

        #endregion

        #region ManualAssignProductUniverse

        private RelayCommand _manualAssignProductUniverseCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand ManualAssignProductUniverseCommand
        {
            get
            {
                if (_manualAssignProductUniverseCommand == null)
                {
                    _manualAssignProductUniverseCommand = new RelayCommand(
                        p => ManualAssignProductUniverseCommand_Executed(), p => ManualAssignProductUniverseCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManualAssignProductUniverseCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManualAssignProductUniverseCommand_Description,
                        Icon = ImageResources.ContentLookup_ProductUniverse,
                        SmallIcon = ImageResources.ContentLookup_ProductUniverse16,
                    };
                    base.ViewModelCommands.Add(_manualAssignProductUniverseCommand);
                }
                return _manualAssignProductUniverseCommand;
            }
        }

        private Boolean ManualAssignProductUniverseCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manualAssignProductUniverseCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManualAssignProductUniverseCommand_Executed()
        {
            if (ManualAssignProductUniverseCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.ProductUniverse);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.ProductUniverseName = ((ProductUniverseInfo)newManualAssign.ViewModel.SelectedItem).Name;
                    }
                }
                
                ShowWaitCursor(false);
            }
        }

        #endregion

        #region ClearProductUniverse

        private RelayCommand _clearProductUniverseCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand ClearProductUniverseCommand
        {
            get
            {
                if (_clearProductUniverseCommand == null)
                {
                    _clearProductUniverseCommand = new RelayCommand(
                        p => ClearProductUniverseCommand_Executed(), p => ClearProductUniverseCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearProductUniverseCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearProductUniverseCommand_Description,
                        Icon = ImageResources.ContentLookup_ProductUniverse,
                        SmallIcon = ImageResources.ContentLookup_ProductUniverse16,
                    };
                    base.ViewModelCommands.Add(_clearProductUniverseCommand);
                }
                return _clearProductUniverseCommand;
            }
        }

        private Boolean ClearProductUniverseCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearProductUniverseCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearProductUniverseCommand_Executed()
        {
            if (ClearProductUniverseCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Row:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.ProductUniverseName = String.Empty;
                }

                ShowWaitCursor(false);
            }
        }

        #endregion

        #endregion

        #region Assortment Assignments

        #region AutoAssignAssortment

        private RelayCommand _autoAssignAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with Assortment
        /// </summary>
        public RelayCommand AutoAssignAssortmentCommand
        {
            get
            {
                if (_autoAssignAssortmentCommand == null)
                {
                    _autoAssignAssortmentCommand = new RelayCommand(
                        p => AutoAssignAssortmentCommand_Executed(), p => AutoAssignAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignAssortmentCommand_Description,
                        Icon = ImageResources.ContentLookup_Assortment,
                        SmallIcon = ImageResources.ContentLookup_Assortment16,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _autoAssignAssortmentCommand;
            }
        }

        private Boolean AutoAssignAssortmentCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignAssortmentCommand_Executed()
        {
            if (AutoAssignAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Get all Assortments for current category
                AssortmentInfoList associatedAssortments = AssortmentInfoList.FetchByProductGroupId(_selectedProductGroupViewModel.ProductGroup.Id);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    if (associatedAssortments.Count == 0)
                    {
                        //No associated assortments found so tell user
                        ShowCouldNotAutoAssign(Message.ContentLookup_ShowCouldNotAutoAssign_Assortment);
                    }
                    else if (associatedAssortments.Count == 1)
                    {
                        //Only one found so assign
                        currentRow.ContentLookup.AssortmentName = associatedAssortments.First().Name;
                    }
                    else
                    {
                        //More than one found - analyse further

                        //Get Full Assortments
                        List<Assortment> assortments = new List<Assortment>();
                        foreach (AssortmentInfo currentInfo in associatedAssortments)
                        {
                            assortments.Add(Assortment.GetById(currentInfo.Id));
                        }

                        //Get full Planogram
                        Package currentPackage = Package.FetchById(currentRow.PlanogramInfo.Id);

                        if (currentPackage.Planograms != null && currentPackage.Planograms.Count > 0)
                        {
                            Planogram myPlanogram = currentPackage.Planograms.FirstOrDefault();
                            Dictionary<Assortment, Int32> productCountDict = new Dictionary<Assortment, Int32>();

                            //For each product universe we want to get a count of matching products
                            foreach (Assortment currentAssortmentToAssess in assortments)
                            {
                                Int32 countForAssortment = 0;

                                //Go through each product in planogram and count the matching products
                                foreach (PlanogramProduct planoProduct in myPlanogram.Products)
                                {
                                    Int32 cnt = currentAssortmentToAssess.Products.Where(o => o.GTIN == planoProduct.Gtin).Count();
                                    countForAssortment += cnt;
                                }

                                productCountDict.Add(currentAssortmentToAssess, countForAssortment);
                            }


                            //Highest key
                            var dictEntry = productCountDict.Aggregate((l, r) => l.Value > r.Value ? l : r);
                            Int32 max = dictEntry.Value;

                            //Is Distinct?
                            Int32 distinctCount = productCountDict.Values.Where(k => k == max).Count();

                            if (distinctCount == 1)
                            {
                                //When rating is distinct this is our appropriate universe.
                                currentRow.ContentLookup.AssortmentName = dictEntry.Key.Name;
                            }
                            else
                            {
                                //If rating not unqiue then we fall back to getting the most recently modified of the several non distinct.
                                List<Assortment> assortmentsToSearch = new List<Assortment>();

                                foreach (KeyValuePair<Assortment, Int32> entry in productCountDict)
                                {
                                    if (entry.Value == max)
                                    {
                                        assortmentsToSearch.Add(entry.Key);
                                    }
                                }

                                //Universes now is from relevant universes
                                DateTime latest = assortmentsToSearch.Max(u => u.DateLastModified);
                                Assortment appropriateAssortment = assortmentsToSearch.Where(u => u.DateLastModified == latest).First();

                                //set name
                                currentRow.ContentLookup.AssortmentName = appropriateAssortment.Name;
                            }
                        }
                    }
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ClearAssortment

        private RelayCommand _clearAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with Assortment
        /// </summary>
        public RelayCommand ClearAssortmentCommand
        {
            get
            {
                if (_clearAssortmentCommand == null)
                {
                    _clearAssortmentCommand = new RelayCommand(
                        p => ClearAssortmentCommand_Executed(), p => ClearAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearAssortmentCommand_Description,
                        Icon = ImageResources.ContentLookup_Assortment,
                        SmallIcon = ImageResources.ContentLookup_Assortment16,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _clearAssortmentCommand;
            }
        }

        private Boolean ClearAssortmentCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearAssortmentCommand_Executed()
        {
            if (ClearAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.AssortmentName = String.Empty;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignAssortment

        private RelayCommand _manuallyAssignAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with Assortment
        /// </summary>
        public RelayCommand ManuallyAssignAssortmentCommand
        {
            get
            {
                if (_manuallyAssignAssortmentCommand == null)
                {
                    _manuallyAssignAssortmentCommand = new RelayCommand(
                        p => ManuallyAssignAssortmentCommand_Executed(), p => ManuallyAssignAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignAssortmentCommand_Description,
                        Icon = ImageResources.ContentLookup_Assortment16,
                        SmallIcon = ImageResources.ContentLookup_Assortment16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _manuallyAssignAssortmentCommand;
            }
        }

        private Boolean ManuallyAssignAssortmentCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignAssortmentCommand_Executed()
        {
            if (ManuallyAssignAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.Assortment);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.AssortmentName = ((AssortmentInfo)newManualAssign.ViewModel.SelectedItem).Name;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region Metric Profile

        #region ManuallyAssignMetricProfile

        private RelayCommand _ManuallyAssignMetricProfileCommand;

        /// <summary>
        /// Handles Manually Assignment with MetricProfile
        /// </summary>
        public RelayCommand ManuallyAssignMetricProfileCommand
        {
            get
            {
                if (_ManuallyAssignMetricProfileCommand == null)
                {
                    _ManuallyAssignMetricProfileCommand = new RelayCommand(
                        p => ManuallyAssignMetricProfileCommand_Executed(), p => ManuallyAssignMetricProfileCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignMetricProfileCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignMetricProfileCommand_Description,
                        Icon = ImageResources.ContentLookup_MetricProfile,
                        SmallIcon = ImageResources.ContentLookup_MetricProfile16,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _ManuallyAssignMetricProfileCommand;
            }
        }

        private Boolean ManuallyAssignMetricProfileCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ManuallyAssignMetricProfileCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignMetricProfileCommand_Executed()
        {
            if (ManuallyAssignMetricProfileCommand.CanExecute())
            {
                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.MetricProfile);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.MetricProfileName = ((MetricProfileInfo)newManualAssign.ViewModel.SelectedItem).Name;
                    }
                }
            }
        }

        #endregion

        #region ClearMetricProfile

        private RelayCommand _ClearMetricProfileCommand;

        /// <summary>
        /// Handles Manually Assignment with MetricProfile
        /// </summary>
        public RelayCommand ClearMetricProfileCommand
        {
            get
            {
                if (_ClearMetricProfileCommand == null)
                {
                    _ClearMetricProfileCommand = new RelayCommand(
                        p => ClearMetricProfileCommand_Executed(), p => ClearMetricProfileCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearMetricProfileCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearMetricProfileCommand_Description,
                        Icon = ImageResources.ContentLookup_MetricProfile,
                        SmallIcon = ImageResources.ContentLookup_MetricProfile16,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _ClearMetricProfileCommand;
            }
        }

        private Boolean ClearMetricProfileCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ClearMetricProfileCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearMetricProfileCommand_Executed()
        {
            //Foreach Planogram:
            foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
            {
                currentRow.ContentLookup.MetricProfileName = String.Empty;
            }
        }

        #endregion

        #endregion

        #region Inventory Profile

        #region ManuallyAssignInventoryProfile

        private RelayCommand _ManuallyAssignInventoryProfileCommand;

        /// <summary>
        /// Handles Manually Assignment with InventoryProfile
        /// </summary>
        public RelayCommand ManuallyAssignInventoryProfileCommand
        {
            get
            {
                if (_ManuallyAssignInventoryProfileCommand == null)
                {
                    _ManuallyAssignInventoryProfileCommand = new RelayCommand(
                        p => ManuallyAssignInventoryProfileCommand_Executed(), p => ManuallyAssignInventoryProfileCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignInventoryProfileCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignInventoryProfileCommand_Description,
                        Icon = ImageResources.ContentLookup_InventoryProfile,
                        SmallIcon = ImageResources.ContentLookup_InventoryProfile16,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _ManuallyAssignInventoryProfileCommand;
            }
        }

        private Boolean ManuallyAssignInventoryProfileCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ManuallyAssignInventoryProfileCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignInventoryProfileCommand_Executed()
        {
            if (ManuallyAssignInventoryProfileCommand.CanExecute())
            {
                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.InventoryProfile);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.InventoryProfileName = ((InventoryProfileInfo)newManualAssign.ViewModel.SelectedItem).Name;
                    }
                }
            }
        }

        #endregion

        #region ClearInventoryProfile

        private RelayCommand _ClearInventoryProfileCommand;

        /// <summary>
        /// Handles Manually Assignment with InventoryProfile
        /// </summary>
        public RelayCommand ClearInventoryProfileCommand
        {
            get
            {
                if (_ClearInventoryProfileCommand == null)
                {
                    _ClearInventoryProfileCommand = new RelayCommand(
                        p => ClearInventoryProfileCommand_Executed(), p => ClearInventoryProfileCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearInventoryProfileCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearInventoryProfileCommand_Description,
                        Icon = ImageResources.ContentLookup_InventoryProfile,
                        SmallIcon = ImageResources.ContentLookup_InventoryProfile16,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _ClearInventoryProfileCommand;
            }
        }

        private Boolean ClearInventoryProfileCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ClearInventoryProfileCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearInventoryProfileCommand_Executed()
        {
            //Foreach Planogram:
            foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
            {
                currentRow.ContentLookup.InventoryProfileName = String.Empty;
            }
        }

        #endregion

        #endregion

        #region AutoAssignBlocking

        private RelayCommand _autoAssignBlockingCommand;

        /// <summary>
        /// Handles Auto Assignment with Blocking
        /// </summary>
        public RelayCommand AutoAssignBlockingCommand
        {
            get
            {
                if (_autoAssignBlockingCommand == null)
                {
                    _autoAssignBlockingCommand = new RelayCommand(
                        p => AutoAssignBlockingCommand_Executed(), p => AutoAssignBlockingCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignBlockingCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignBlockingCommand_Description
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _autoAssignBlockingCommand;
            }
        }

        private Boolean AutoAssignBlockingCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignBlockingCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignBlockingCommand_Executed()
        {
            if (AutoAssignBlockingCommand_CanExecute())
            {
                //TODO: Blocking has not yet been implemented in V8 so this is a placeholder
            }
        }

        #endregion

        #region Sequence Assignments

        #region AutoAssignSequence

        private RelayCommand _autoAssignSequenceCommand;

        /// <summary>
        /// Handles Auto Assignment with Sequence
        /// </summary>
        public RelayCommand AutoAssignSequenceCommand
        {
            get
            {
                if (_autoAssignSequenceCommand == null)
                {
                    _autoAssignSequenceCommand = new RelayCommand(
                        p => AutoAssignSequenceCommand_Executed(), p => AutoAssignSequenceCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignSequenceCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignSequenceCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ProductSequence16,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _autoAssignSequenceCommand;
            }
        }

        private Boolean AutoAssignSequenceCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignSequenceCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignSequenceCommand_Executed()
        {
            if (AutoAssignSequenceCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Get all sequences for current category
                SequenceInfoList associatedSequences = SequenceInfoList.FetchByProductGroupId(_selectedProductGroupViewModel.ProductGroup.Id);

                //Foreach Row:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    if (associatedSequences.Count == 0)
                    {
                        //If the category has no associated product sequences create info message
                        ShowCouldNotAutoAssign(Message.ContentLookup_ShowCouldNotAutoAssign_Sequence);
                    }
                    else if (associatedSequences.Count == 1)
                    {
                        //Only the one product sequence so this is the most 'appropriate to add'.
                        currentRow.ContentLookup.SequenceName = associatedSequences.First().Name;
                    }
                    else
                    {
                        //If more than one how many products in product sequence have the same products as in planogram.
                        Package currentPackage = Package.FetchById(currentRow.PlanogramInfo.Id);

                        if (currentPackage.Planograms != null && currentPackage.Planograms.Count > 0)
                        {
                            Planogram myPlanogram = currentPackage.Planograms.FirstOrDefault();
                            Dictionary<Sequence, Int32> productCountDict = new Dictionary<Sequence, Int32>();


                            //For each product sequence we want to get a count of matching products
                            foreach (SequenceInfo currentSequence in associatedSequences)
                            {
                                //At this point we now know we want full sequence (We may not always need this before now). 
                                Sequence currentSequenceToAssess = Sequence.FetchById(currentSequence.Id);
                                Int32 countForSequence = 0;

                                //Because SequenceProduct does not have Gtin bring back a list of gtins from products for the product ids 
                                List<String> gtinList = new List<String>();
                                foreach (SequenceProduct currentSequenceProduct in currentSequenceToAssess.Products)
                                {
                                    gtinList.Add(Product.FetchById(currentSequenceProduct.ProductId).Gtin);
                                }

                                //Go through each product in planogram and count the matching products
                                foreach (PlanogramProduct planoProduct in myPlanogram.Products)
                                {

                                    Int32 cnt = gtinList.Where(o => o == planoProduct.Gtin).Count();
                                    countForSequence += cnt;
                                }

                                productCountDict.Add(currentSequenceToAssess, countForSequence);
                            }

                            //Highest key
                            var dictEntry = productCountDict.Aggregate((l, r) => l.Value > r.Value ? l : r);
                            Int32 max = dictEntry.Value;

                            //Is Distinct?
                            Int32 distinctCount = productCountDict.Values.Where(k => k == max).Count();

                            if (distinctCount == 1)
                            {
                                //When rating is distinct this is our appropriate sequence.
                                currentRow.ContentLookup.SequenceName = dictEntry.Key.Name;
                            }
                            else
                            {
                                //If rating not unqiue then we fall back to getting the most recently modified of the several non distinct.
                                List<Sequence> sequences = new List<Sequence>();

                                foreach (KeyValuePair<Sequence, Int32> entry in productCountDict)
                                {
                                    if (entry.Value == max)
                                    {
                                        sequences.Add(entry.Key);
                                    }
                                }

                                //Unverses now is from relevant sequences
                                DateTime latest = sequences.Max(u => u.DateLastModified);
                                Sequence appropriateSequence = sequences.Where(u => u.DateLastModified == latest).First();

                                //set name
                                currentRow.ContentLookup.SequenceName = appropriateSequence.Name;
                            }
                        }
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #region ClearSequence

        private RelayCommand _clearSequenceCommand;

        /// <summary>
        /// Handles Auto Assignment with Sequence
        /// </summary>
        public RelayCommand ClearSequenceCommand
        {
            get
            {
                if (_clearSequenceCommand == null)
                {
                    _clearSequenceCommand = new RelayCommand(
                        p => ClearSequenceCommand_Executed(), p => ClearSequenceCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearSequenceCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearSequenceCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ProductSequence16,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _clearSequenceCommand;
            }
        }

        private Boolean ClearSequenceCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearSequenceCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearSequenceCommand_Executed()
        {
            if (ClearSequenceCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.SequenceName = String.Empty;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignSequence

        private RelayCommand _manuallyAssignSequenceCommand;

        /// <summary>
        /// Handles Auto Assignment with Sequence
        /// </summary>
        public RelayCommand ManuallyAssignSequenceCommand
        {
            get
            {
                if (_manuallyAssignSequenceCommand == null)
                {
                    _manuallyAssignSequenceCommand = new RelayCommand(
                        p => ManuallyAssignSequenceCommand_Executed(), p => ManuallyAssignSequenceCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignSequenceCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignSequenceCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ProductSequence16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignSequenceCommand);
                }
                return _manuallyAssignSequenceCommand;
            }
        }

        private Boolean ManuallyAssignSequenceCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignSequenceCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignSequenceCommand_Executed()
        {
            if (ManuallyAssignSequenceCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.Sequence);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.SequenceName = ((SequenceInfo)newManualAssign.ViewModel.SelectedItem).Name;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #endregion

        #endregion

        #region Methods

        #region UpdatePermissonFlags

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //JPTODO: Theese permissions are currently static. Update once models have been created.
            _userHasReferenceLookupCreatePerm = true;
            _userHasReferenceLookupDeletePerm = true;
            _userHasReferenceLookupEditPerm = true;
            _userHasReferenceLookupFetchPerm = true;
                
            ////Product premissions
            ////create
            //_userHasReferenceLookupCreatePerm = Csla.Rules.BusinessRules.HasPermission(
            //    Csla.Rules.AuthorizationActions.CreateObject, typeof(Product));

            ////fetch
            //_userHasReferenceLookupFetchPerm = Csla.Rules.BusinessRules.HasPermission(
            //    Csla.Rules.AuthorizationActions.GetObject, typeof(Product));

            ////edit
            //_userHasReferenceLookupEditPerm = Csla.Rules.BusinessRules.HasPermission(
            //    Csla.Rules.AuthorizationActions.EditObject, typeof(Product));

            ////delete
            //_userHasReferenceLookupDeletePerm = Csla.Rules.BusinessRules.HasPermission(
            //    Csla.Rules.AuthorizationActions.DeleteObject, typeof(Product));
        }
        
        #endregion

        #region UpdateAvailableProductGroups

        private void UpdateAvailableProductGroups()
        {
            base.ShowWaitCursor(true);

            //Reload Merch hierarchy from data store
            ProductHierarchy merchHierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);
            _productGroups = new ReadOnlyCollection<ProductGroup>(merchHierarchy.FetchAllGroups());

            foreach (ProductGroup group in _productGroups)
            {
                ProductGroupViewModels.Add(new ProductGroupViewModel(group));
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ContinueWithItemChange

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.ContentLookupRows.IsDirty())
                {
                    ModalMessageResult actionResult = ModalMessageResult.Button1;

                    ModalMessage ShouldOverwritePrompt = new ModalMessage();
                    ShouldOverwritePrompt.Title = Message.ContentLookup_ContinueWithChange_Title;
                    ShouldOverwritePrompt.MessageIcon = ImageResources.DialogInformation;
                    ShouldOverwritePrompt.Header = String.Format(Message.ContentLookup_ContinueWithChange_Header, CurrentProductGroupViewModel.ProductGroupLabel);
                    ShouldOverwritePrompt.Description = Message.ContentLookup_ContinueWithChange_Description;
                    ShouldOverwritePrompt.ButtonCount = 3;
                    ShouldOverwritePrompt.Button1Content = Message.Generic_Save;
                    ShouldOverwritePrompt.Button2Content = Message.Generic_Continue;
                    ShouldOverwritePrompt.Button3Content = Message.Generic_Cancel;
                    ShouldOverwritePrompt.Owner = this.AttachedControl;
                    ShouldOverwritePrompt.WindowStartupLocation = WindowStartupLocation.CenterOwner;


                    ShouldOverwritePrompt.ShowDialog();

                    if (ShouldOverwritePrompt.Result == ModalMessageResult.Button1)
                    {
                        //Save
                        this.SaveCommand.Execute();
                        continueExecute = true;
                    }

                    if (ShouldOverwritePrompt.Result == ModalMessageResult.Button2)
                    {
                        //Don't save just quit
                        continueExecute = true;
                    }

                    if (ShouldOverwritePrompt.Result == ModalMessageResult.Button3)
                    {
                        //Cancel
                        continueExecute = false;
                    }
                }
            }
            return continueExecute;
        }

        #endregion

        #region ShowCouldNotAutoAssign

        private void ShowCouldNotAutoAssign(String itemName)
        {
            ModalMessage dialog =
                       new ModalMessage()
                       {
                           Title = Message.ContentLookup_ShowCouldNotAutoAssign_Title,
                           Header = Message.ContentLookup_ShowCouldNotAutoAssign_Header,
                           Description = String.Format(Message.ContentLookup_ShowCouldNotAutoAssign_Description, Message.ContentLookup_ShowCouldNotAutoAssign_ProductUniverse),
                           ButtonCount = 1,
                           Button1Content = Message.Generic_Ok,
                           DefaultButton = ModalMessageButton.Button1,
                           CancelButton = ModalMessageButton.Button1,
                           MessageIcon = ImageResources.Dialog_Information
                       };

            if (this.AttachedControl != null)
            {
                dialog.Owner = this.AttachedControl;
                WindowStartupLocation startupLocation = WindowStartupLocation.CenterOwner;
                dialog.WindowStartupLocation = startupLocation;
            }
            
            dialog.ShowDialog();

        }

        #endregion

        #endregion

        #region Event Handlers

        void CurrentProductGroupViewModel_CategoryChanged()
        {
            //Logic for when a new Product Group is Openened
            _contentLookupRows.UpdateContentLookupRows(_currentProductGroupViewModel.ProductGroup.Code);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //Remove event handlers
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
