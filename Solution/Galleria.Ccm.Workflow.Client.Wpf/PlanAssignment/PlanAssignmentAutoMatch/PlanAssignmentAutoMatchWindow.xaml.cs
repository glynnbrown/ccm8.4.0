﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM810
// V8-29598 : L.Luong
//  Created
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;
using System.Windows.Input;
using System.Windows.Controls;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.PlanAssignmentAutoMatch
{
    /// <summary>
    /// Interaction logic for PlanAssignmentAutoMatchWindow.xaml
    /// </summary>
    public partial class PlanAssignmentAutoMatchWindow
    {
        #region Properties

        #region DisplayText

        public static readonly DependencyProperty DisplayTextProperty = DependencyProperty.Register("DisplayText",
            typeof(String), typeof(PlanAssignmentAutoMatchWindow),
            new PropertyMetadata(null));

        private String DisplayText
        {
            get { return (String)GetValue(DisplayTextProperty); }
            set { SetValue(DisplayTextProperty, value); }
        }

        #endregion

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanAssignmentAutoMatchViewModel), typeof(PlanAssignmentAutoMatchWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanAssignmentAutoMatchWindow)obj;

            if (e.OldValue != null)
            {
                PlanAssignmentAutoMatchViewModel oldModel = (PlanAssignmentAutoMatchViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                PlanAssignmentAutoMatchViewModel newModel = (PlanAssignmentAutoMatchViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        /// <summary>
        ///     Gets or sets the viewmodel controller for <see cref="PlanRepositoryOrganiser"/>.
        /// </summary>
        public PlanAssignmentAutoMatchViewModel ViewModel
        {
            get { return (PlanAssignmentAutoMatchViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanAssignmentAutoMatchWindow(PlanAssignmentAutoMatchViewModel viewModel)
        {
            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanAssignmentAutoMatch);

            ViewModel = viewModel;

            Loaded += PlanAssignmentAutoMatchWindow_Loaded;
        }

        private void PlanAssignmentAutoMatchWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Loaded -= PlanAssignmentAutoMatchWindow_Loaded;
            Dispatcher.BeginInvoke((Action)(() => Mouse.OverrideCursor = null), DispatcherPriority.Background);
        }

        #endregion

        #region Event Handlers
        
        /// <summary>
        /// This provides more intuitive tabbing when in the Datagrid as the Alt+Tab function
        /// didn't work on the comboboxes (this is for 508 compliance) 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExtendedDataGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab && e.KeyboardDevice.Modifiers == ModifierKeys.Shift)
            {
                // if the focus is on the first item of the grid Alt+Tab will give focus to the previous control
                if (dataGrid.SelectedIndex == 0)
                {
                    dataGrid.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
                }

                // Alt+Tab allows the user to move back a full row 
                // unable to implement normal functionality of moving back by cell
                else
                {
                    DataGridRow dgr = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromItem(dataGrid.Items[dataGrid.SelectedIndex]);
                    dgr.MoveFocus(new TraversalRequest(FocusNavigationDirection.Previous));
                }
            }
        }

        #endregion

        #region window close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    }
}
