﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM810
// V8-29598 : L.Luong
//  Created
// V8-30132 : L.Luong
//  Added remove cluster scheme button
// V8-30213 : L.Luong
//  Doesnt require Planogram Group to do the search
#endregion

#region Version History: CCM811
// V8-30310 : L.Luong
//  Added Remove Planogram Group button
#endregion

#region Version History : CCM830

// V8-31917 : A.Silva
//  Refactored AutoMatchWork.OnDoWork to make it easier to debug, and hopefully more clear.
// CCM-13612 : Greg Richards
//  Changed the matches by location to only compare location code as location name can contain other content
//  whereas location code should only contain a valid relationship.

#endregion

#endregion

using System;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.PlanAssignmentAutoMatch
{
    public sealed class PlanAssignmentAutoMatchViewModel : WindowViewModelBase
    {
        #region Nested Classes

        private sealed class AutoMatchWork : IModalBusyWork
        {
            #region Nested Classes

            private sealed class WorkArgs
            {
                public PlanAssignmentAutoMatchViewModel AutoMatchViewModel { get; set; }
            }

            #endregion

            #region Fields

            private readonly PlanAssignmentAutoMatchViewModel _autoMatchViewModel;

            #endregion

            #region Properties

            private Int32 NumberOfMatchedPlans { get; set; }

            public Boolean ReportsProgress { get { return true; } }

            public Boolean SupportsCancellation { get { return true; } }

            #endregion

            #region Constructor

            public AutoMatchWork(PlanAssignmentAutoMatchViewModel autoMatchViewModel)
            {
                _autoMatchViewModel = autoMatchViewModel;
            }

            #endregion

            #region Methods

            public Object GetWorkerArgs()
            {
                return new WorkArgs {AutoMatchViewModel = _autoMatchViewModel};
            }

            public void ShowModalBusy(IModalBusyWorkerService sender, EventHandler cancelEventHandler)
            {
                CommonHelper.GetWindowService().ShowDeterminateBusy(
                    Message.PlanAssignmentAutoMatch_Matching_BusyHeader,
                    Message.PlanAssignmentAutoMatch_Matching_Desc,
                    cancelEventHandler);
            }

            //public void OnDoWorkOld(IModalBusyWorkerService sender, DoWorkEventArgs e)
            //{
            //    // Fields
            //    WorkArgs args = (WorkArgs)e.Argument;
            //    PlanAssignmentAutoMatchViewModel autoMatchViewModel = args.AutoMatchViewModel;
            //    Int32 count = 0;
            //    ProductGroupInfo productGroup = autoMatchViewModel.ProductGroup;
            //    PlanogramGroup planogramGroupId = autoMatchViewModel.SelectedPlanogramGroup;
            //    Boolean useClusterSchemes = autoMatchViewModel.UseClusterSchemes;
            //    ClusterSchemeInfo clusterScheme = autoMatchViewModel.SelectedClusterScheme;
            //    List<ClusterSchemeInfo> clusterSchemeList = null;
            //    PlanAssignmentStoreSpaceInfoList planStoreSpaceInfoList = null;
            //    LocationPlanAssignmentList planAssignmentList = null;
            //    Boolean isMatch = false;
            //    Boolean isLocationPlan = false;
            //    PlanogramInfoList planInfoList = null;
            //    List<PlanogramInfo> planInfos = new List<PlanogramInfo>();
            //    User currentUser = User.GetCurrentUser();
            //    DateTime currentDate = DateTime.UtcNow;

            //    // Get cluster scheme
            //    if (useClusterSchemes)
            //    {
            //        clusterSchemeList = ClusterSchemeInfoList.FetchByEntityId(autoMatchViewModel.EntityId).Where(c => c.ProductGroupId != null).ToList();

            //        if (!clusterSchemeList.Any())
            //        {
            //            useClusterSchemes = false;
            //        }
            //    }

            //    // get store space
            //    if (autoMatchViewModel.ViewType == PlanAssignmentViewType.ViewCategory)
            //    {
            //        planStoreSpaceInfoList = PlanAssignmentStoreSpaceInfoList.FetchByEntityIdProductGroupId(autoMatchViewModel.EntityId, productGroup.Id, !useClusterSchemes && clusterScheme != null ? (Int32?)clusterScheme.Id : null);
            //        planAssignmentList = LocationPlanAssignmentList.FetchByEntityIdProductGroupId(autoMatchViewModel.EntityId, productGroup.Id);
            //    }
            //    else
            //    {
            //        planStoreSpaceInfoList = PlanAssignmentStoreSpaceInfoList.FetchByEntityIdLocationId(autoMatchViewModel.EntityId, autoMatchViewModel.Location.Id, !useClusterSchemes && clusterScheme != null ? (Int32?)clusterScheme.Id : null);
            //        planAssignmentList = LocationPlanAssignmentList.FetchByEntityIdLocationId(autoMatchViewModel.EntityId, autoMatchViewModel.Location.Id);
            //    }

            //    // get plans in selected group
            //    if (planogramGroupId != null)
            //    {
            //        planInfoList = PlanogramInfoList.FetchByPlanogramGroupId(planogramGroupId.Id);
            //    }
            //    else
            //    {
            //        // if there is no selected group fetch by either category code or location code
            //        if (autoMatchViewModel.ViewType == PlanAssignmentViewType.ViewCategory)
            //        {
            //            planInfoList = PlanogramInfoList.FetchNonDebugByCategoryCode(productGroup.Code);
            //        }
            //        else
            //        {
            //            planInfoList = PlanogramInfoList.FetchNonDebugByLocationCode(autoMatchViewModel.Location.Code);
            //        }
            //    }

            //    // get the plans with the correct status type and cluster scheme 
            //    foreach (PlanogramInfo planInfo in planInfoList)
            //    {
            //        if (planInfo.Status == autoMatchViewModel.StatusType || autoMatchViewModel.StatusType == PlanogramStatusType.Unknown)
            //        {
            //            if (!useClusterSchemes && clusterScheme != null)
            //            {
            //                if (planInfo.ClusterSchemeName != null)
            //                {
            //                    if (planInfo.ClusterSchemeName.Equals(clusterScheme.Name))
            //                    {
            //                        planInfos.Add(planInfo);
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                planInfos.Add(planInfo);
            //            }
            //        }
            //    }

            //    //determine the percentage per item
            //    Double itemPercent = 100 / (Double)(planStoreSpaceInfoList.Count + 1);
            //    Double curPercent = 0;

            //    //start cycling through store spaces.
            //    while (!sender.CancellationPending && count < planStoreSpaceInfoList.Count)
            //    {
            //        PlanAssignmentStoreSpaceInfo nextWPlan = planStoreSpaceInfoList.ElementAt(count);
            //        if (nextWPlan != null)
            //        {
            //            LocationPlanAssignment planProductMatch = null;
            //            List<PlanogramInfo> filteredPlanInfos = new List<PlanogramInfo>();
            //            List<PlanogramInfo> tempPlanInfos = new List<PlanogramInfo>();
            //            isLocationPlan = false;
            //            isMatch = false;

            //            // if store space has plan assigned should ignore
            //            if (planAssignmentList != null)
            //            {
            //                planProductMatch = planAssignmentList.Where(x => x.ProductGroupId == nextWPlan.ProductGroupId && x.LocationId == nextWPlan.LocationId).FirstOrDefault();
            //            }

            //            // check if there is a correct cluster scheme for he store space
            //            if (useClusterSchemes)
            //            {
            //                ClusterSchemeInfo clusterSchemeInfo = clusterSchemeList.Where(c => c.ProductGroupId == nextWPlan.ProductGroupId)
            //                    .OrderByDescending(m => m.DateLastModified).FirstOrDefault();
            //                if (clusterSchemeInfo != null)
            //                {
            //                    foreach (PlanogramInfo planInfo in planInfos)
            //                    {
            //                        if (planInfo.ClusterSchemeName != null &&
            //                            planInfo.ClusterName != null)
            //                        {
            //                            if (planInfo.ClusterSchemeName.Equals(clusterSchemeInfo.Name) && planInfo.ClusterName.Equals(nextWPlan.ClusterName))
            //                            {
            //                                filteredPlanInfos.Add(planInfo);
            //                            }
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    // if there is not correct cluster get the default one
            //                    foreach (PlanogramInfo planInfo in planInfos)
            //                    {
            //                        if (clusterScheme != null)
            //                        {
            //                            if (planInfo.ClusterSchemeName != null &&
            //                                planInfo.ClusterName != null)
            //                            {
            //                                if (planInfo.ClusterSchemeName.Equals(clusterScheme.Name) && planInfo.ClusterName.Equals(nextWPlan.ClusterName))
            //                                {
            //                                    filteredPlanInfos.Add(planInfo);
            //                                }
            //                            }
            //                        }
            //                        else
            //                        {
            //                            filteredPlanInfos.Add(planInfo);
            //                        }
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                // match the clusters for the plans 
            //                if (clusterScheme != null)
            //                {
            //                    foreach (PlanogramInfo planInfo in planInfos)
            //                    {
            //                        if (planInfo.ClusterName != null)
            //                        {
            //                            if (planInfo.ClusterName.Equals(nextWPlan.ClusterName))
            //                            {
            //                                filteredPlanInfos.Add(planInfo);
            //                            }
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    filteredPlanInfos = planInfos;
            //                }
            //            }

            //            // check if the plan is in the correct category
            //            foreach (PlanogramInfo plan in filteredPlanInfos)
            //            {
            //                if (plan.CategoryName != null && plan.CategoryCode != null)
            //                {
            //                    if (plan.CategoryName.Equals(nextWPlan.ProductGroupName) && plan.CategoryCode.Equals(nextWPlan.ProductGroupCode))
            //                    {
            //                        tempPlanInfos.Add(plan);
            //                    }
            //                }
            //            }
            //            filteredPlanInfos = tempPlanInfos;

            //            // if store specific ignore the priorities
            //            if (autoMatchViewModel.IsStoreSpecific)
            //            {
            //                var matchedPlans = new List<PlanogramInfo>();
            //                foreach (PlanogramInfo planInfo in filteredPlanInfos)
            //                {
            //                    if (planInfo.LocationName != null && planInfo.LocationCode != null)
            //                    {
            //                        if (planInfo.LocationCode.Equals(nextWPlan.LocationCode) && planInfo.LocationName.Equals(nextWPlan.LocationName))
            //                        {
            //                            isMatch = true;
            //                            matchedPlans.Add(planInfo);
            //                        }
            //                    }
            //                }
            //                if (isMatch)
            //                {
            //                    // assign plan 
            //                    if (planProductMatch == null)
            //                    {
            //                        if (autoMatchViewModel.ViewType == PlanAssignmentViewType.ViewCategory)
            //                        {
            //                            planAssignmentList.Add(LocationPlanAssignment.NewLocationPlanAssignment(productGroup.Id, matchedPlans.LastOrDefault().Id, nextWPlan.LocationId, currentUser, currentDate));
            //                        }
            //                        else
            //                        {
            //                            planAssignmentList.Add(LocationPlanAssignment.NewLocationPlanAssignment(nextWPlan.ProductGroupId, matchedPlans.LastOrDefault().Id, autoMatchViewModel.Location.Id, currentUser, currentDate));
            //                        }
            //                        NumberOfMatchedPlans++;
            //                    }
            //                }
            //            }

            //            // if there is no store specific match or user has not selected store specific first
            //            if (!isMatch)
            //            {
            //                // Match using match properties
            //                IEnumerable<PlanogramInfo> plans = filteredPlanInfos;
            //                List<PlanogramInfo> locationplans = new List<PlanogramInfo>();
            //                foreach (AutoMatchRowView rowView in autoMatchViewModel.AvailableAutoMatchRows)
            //                {
            //                    plans = rowView.OrderItems(plans);
            //                    plans = rowView.FilterItems(plans, nextWPlan);
            //                }

            //                if (plans.Any())
            //                {
            //                    isMatch = true;
            //                }

            //                if (isMatch)
            //                {
            //                    if (planProductMatch == null)
            //                    {
            //                        // check if there is a location matching the store space and use that plan to assign if so
            //                        foreach (PlanogramInfo plan in plans)
            //                        {
            //                            if (plan.LocationName != null && plan.LocationCode != null)
            //                            {
            //                                if (plan.LocationCode.Equals(nextWPlan.LocationCode) && plan.LocationName.Equals(nextWPlan.LocationName))
            //                                {
            //                                    locationplans.Add(plan);
            //                                    isLocationPlan = true;
            //                                }
            //                            }
            //                        }

            //                        // if there is no location get all plans without location linked to plan
            //                        if (isLocationPlan == true)
            //                        {
            //                            plans = locationplans;
            //                        }
            //                        else
            //                        {
            //                            plans = plans.Where(p => String.IsNullOrEmpty(p.LocationCode) && String.IsNullOrEmpty(p.LocationName));
            //                        }

            //                        // assign plan 
            //                        if (plans.Any())
            //                        {
            //                            if (autoMatchViewModel.ViewType == PlanAssignmentViewType.ViewCategory)
            //                            {
            //                                planAssignmentList.Add(LocationPlanAssignment.NewLocationPlanAssignment(productGroup.Id, plans.LastOrDefault().Id, nextWPlan.LocationId, currentUser, currentDate));
            //                            }
            //                            else
            //                            {
            //                                planAssignmentList.Add(LocationPlanAssignment.NewLocationPlanAssignment(nextWPlan.ProductGroupId, plans.LastOrDefault().Id, autoMatchViewModel.Location.Id, currentUser, currentDate));
            //                            }
            //                            NumberOfMatchedPlans++;
            //                        }
            //                    }
            //                }
            //            }
            //        }

            //        //report progress
            //        curPercent += itemPercent;
            //        count++;
            //        sender.ReportProgress((Int32)curPercent);
            //    }

            //    planAssignmentList.Save();
            //}

            public void OnDoWork(IModalBusyWorkerService sender, DoWorkEventArgs e)
            {
                PlanAssignmentAutoMatchViewModel autoMatchViewModel = ((WorkArgs) e.Argument).AutoMatchViewModel;

                LocationPlanAssignmentList locationPlanAssignments = autoMatchViewModel.FetchLocationPlanAssignments();
                List<ClusterSchemeInfo> clusterSchemeInfos = autoMatchViewModel.FetchClusterSchemesIfAllowed();
                Boolean useClusterSchemes = clusterSchemeInfos.Any();
                List<PlanogramInfo> planInfos = autoMatchViewModel.GetValidPlanInfos(useClusterSchemes);
                PlanAssignmentStoreSpaceInfoList storeSpaceInfos = autoMatchViewModel.FetchStoreSpaceInfos(useClusterSchemes);

                //determine the percentage per item
                Double itemPercent = 100/(Double) (storeSpaceInfos.Count + 1);
                Double curPercent = 0;
                var count = 0;

                //start cycling through store spaces.
                while (!sender.CancellationPending &&
                       count < storeSpaceInfos.Count)
                {
                    PlanAssignmentStoreSpaceInfo storeSpaceInfo = storeSpaceInfos.ElementAt(count);
                    if (storeSpaceInfo != null)
                    {
                        List<PlanogramInfo> matchingPlanInfos = autoMatchViewModel.FilterPlanInfos(planInfos, clusterSchemeInfos, storeSpaceInfo);

                        LocationPlanAssignment assignment = autoMatchViewModel.FindLocationPlanAssignment(matchingPlanInfos, locationPlanAssignments, storeSpaceInfo);

                        if (assignment != null)
                        {
                            locationPlanAssignments.Add(assignment);
                            NumberOfMatchedPlans++;
                        }
                    }

                    //report progress
                    curPercent += itemPercent;
                    count++;
                    sender.ReportProgress((Int32) curPercent);
                }

                locationPlanAssignments.Save();
            }

            public void OnProgressChanged(IModalBusyWorkerService sender, ProgressChangedEventArgs e)
            {
                //update the modal busy with the new percentage
                CommonHelper.GetWindowService().UpdateBusy(e.ProgressPercentage);
            }

            public void OnWorkCompleted(IModalBusyWorkerService sender, RunWorkerCompletedEventArgs e)
            {
                CommonHelper.GetWindowService()
                            .ShowOkMessage(MessageWindowType.None,
                                           Message.PlanAssignmentAutoMatch_Finished_Header,
                                           String.Format(Message.PlanAssignmentAutoMatch_Finished_Description, NumberOfMatchedPlans));
                //show error if required
                if (e.Error != null)
                {
                    CommonHelper.RecordException(e.Error);
                    CommonHelper.GetWindowService().ShowErrorOccurredMessage(Message.Screen_PlanAssignment, OperationType.Save);
                }
            }

            #endregion
        }

        #endregion

        #region Fields
        private Boolean? _dialogResult;
        private Boolean _isStoreSpecific;
        private Boolean _useClusterSchemes;
        private Int32 _entityId;
        private LocationInfo _location;
        private PlanogramStatusType _statusType = PlanogramStatusType.Approved;
        private ClusterSchemeInfo _selectedClusterScheme;
        private ProductGroupInfo _productGroup;
        private PlanogramGroup _selectedPlanogramGroup;
        private PlanAssignmentViewType _viewType;
        private List<ObjectFieldInfo> _availableFields = new List<ObjectFieldInfo>();
        private ObservableCollection<AutoMatchRowView> _selectedAutoMatchRows = new ObservableCollection<AutoMatchRowView>();
        private BulkObservableCollection<AutoMatchRowView> _availbleAutoMatchRows = new BulkObservableCollection<AutoMatchRowView>();

        #endregion

        #region Property Binding Paths

        // Properties
        public static readonly PropertyPath IsStoreSpecificProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.IsStoreSpecific);
        public static readonly PropertyPath UseClusterSchemesProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.UseClusterSchemes);
        public static readonly PropertyPath AvailableAutoMatchRowsProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(p => p.AvailableAutoMatchRows);
        public static readonly PropertyPath SelectedAutoMatchRowsProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(p => p.SelectedAutoMatchRows);
        public static readonly PropertyPath StatusTypeProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(p => p.StatusType);
        public static readonly PropertyPath SelectedClusterSchemeProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(p => p.SelectedClusterScheme);
        public static readonly PropertyPath SelectedPlanogramGroupProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(p => p.SelectedPlanogramGroup);

        // Commands
        public static readonly PropertyPath PlanogramGroupSelectCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.PlanogramGroupSelectCommand);
        public static readonly PropertyPath RemovePlanogramGroupCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.RemovePlanogramGroupCommand);
        public static readonly PropertyPath ClusterSchemeSelectCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.ClusterSchemeSelectCommand);
        public static readonly PropertyPath RemoveClusterSchemeCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.RemoveClusterSchemeCommand);
        public static readonly PropertyPath AddFieldCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.AddFieldCommand);
        public static readonly PropertyPath RemoveFieldCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.RemoveFieldCommand);
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.OkCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentAutoMatchViewModel>(o => o.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
            }
        }

        /// <summary>
        ///     Gets or sets if it is store specific 
        /// </summary>
        public Boolean IsStoreSpecific
        {
            get { return _isStoreSpecific; }
            set 
            { 
                _isStoreSpecific = value;

                OnPropertyChanged(IsStoreSpecificProperty);
            }
        }

        /// <summary>
        ///     Gets or sets if the auto match uses the cluster schemes
        /// </summary>
        public Boolean UseClusterSchemes
        {
            get { return _useClusterSchemes; }
            set 
            {
                _useClusterSchemes = value;

                OnPropertyChanged(UseClusterSchemesProperty);
            }
        }

        /// <summary>
        ///     Gets or sets the entityId
        /// </summary>
        public Int32 EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }

        /// <summary>
        ///     Gets or sets the StatusType of the planogarm
        /// </summary>
        public PlanogramStatusType StatusType
        {
            get { return _statusType; }
            set
            {
                _statusType = value;
                OnPropertyChanged(StatusTypeProperty);
            }
        }

        /// <summary>
        /// gets the ClusterScheme
        /// </summary>
        public ClusterSchemeInfo SelectedClusterScheme
        {
            get { return _selectedClusterScheme; }
            set
            {
                _selectedClusterScheme = value;
                OnPropertyChanged(SelectedClusterSchemeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the currently selected product group
        /// </summary>
        public ProductGroupInfo ProductGroup
        {
            get { return _productGroup; }
            set { _productGroup = value; }
        }

        /// <summary>
        /// Gets/Sets the currently selected planogram group
        /// </summary>
        public PlanogramGroup SelectedPlanogramGroup
        {
            get { return _selectedPlanogramGroup; }
            set 
            {
                _selectedPlanogramGroup = value;

                OnPropertyChanged(SelectedPlanogramGroupProperty);
            }
        }

        /// <summary>
        ///     Gets or sets the LocationInfo.
        /// </summary>
        public LocationInfo Location
        {
            get { return _location ; }
            set { _location = value; }
        }

        /// <summary>
        /// gets or sets the view Type
        /// </summary>
        public PlanAssignmentViewType ViewType
        {
            get { return _viewType; }
            set { _viewType = value; }
        }

        /// <summary>
        /// Returns a collection of available auto match rows
        /// </summary>
        public BulkObservableCollection<AutoMatchRowView> AvailableAutoMatchRows
        {
            get { return _availbleAutoMatchRows; }
        }

        /// <summary>
        /// Returns a collection of available auto match rows
        /// </summary>
        public ObservableCollection<AutoMatchRowView> SelectedAutoMatchRows
        {
            get { return _selectedAutoMatchRows; }
            set 
            {
                _selectedAutoMatchRows = value;

                OnPropertyChanged(SelectedAutoMatchRowsProperty);
            }
        }

        #endregion

        #region Constructor

        public PlanAssignmentAutoMatchViewModel()
        {
            _entityId = App.ViewState.EntityId;

            try
            {
                GetPropertiesToMatch();
            }
            catch (Csla.DataPortalException exc)
            {
                return;
            }
        }

        #endregion

        #region Commands

        #region PlanogramGroupSelectCommand

        private RelayCommand _planogramGroupSelectCommand;

        /// <summary>
        ///     Opens the Repository Folder Selector
        /// </summary>
        public RelayCommand PlanogramGroupSelectCommand
        {
            get
            {
                if (_planogramGroupSelectCommand != null) return _planogramGroupSelectCommand;

                _planogramGroupSelectCommand = new RelayCommand(o => PlanogramGroupSelectCommand_Executed())
                {
                    FriendlyName = Message.Generic_Ellipsis
                };
                RegisterCommand(_planogramGroupSelectCommand);

                return _planogramGroupSelectCommand;
            }
        }

        private void PlanogramGroupSelectCommand_Executed()
        {
            PlanogramHierarchyTreeSelectionWindow dialog =
                new PlanogramHierarchyTreeSelectionWindow();
            CommonHelper.GetWindowService().ShowDialog<PlanogramHierarchyTreeSelectionWindow>(dialog);

            if (dialog.DialogResult == true)
            {
                this.SelectedPlanogramGroup = dialog.SelectedGroup;
            }
        }

        #endregion

        #region RemovePlanogramGroupCommand

        private RelayCommand _removePlanogramGroupCommand;

        /// <summary>
        /// Clears the Planogram Group for the auto match
        /// </summary>
        public RelayCommand RemovePlanogramGroupCommand
        {
            get
            {
                if (_removePlanogramGroupCommand == null)
                {
                    _removePlanogramGroupCommand = new RelayCommand(
                        p => RemovePlanogramGroupCommand_Executed(),
                        p => RemovePlanogramGroupCommand_CanExecute())
                    {
                        FriendlyName = String.Empty,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_removePlanogramGroupCommand);
                }
                return _removePlanogramGroupCommand;
            }
        }

        private Boolean RemovePlanogramGroupCommand_CanExecute()
        {
            // If Planogram Group is null no point removing 
            if (this.SelectedPlanogramGroup == null)
            {
                _removePlanogramGroupCommand.DisabledReason = Message.PlanAssignmentAutoMatch_RemoveProductGroupDisabled_NoProductGroupToRemove;
                return false;
            }
            return true;
        }

        private void RemovePlanogramGroupCommand_Executed()
        {
            // Clear SelectedPlanogramGroup
            this.SelectedPlanogramGroup = null;
        }

        #endregion

        #region ClusterSchemeSelectCommand

        private RelayCommand _clusterSchemeSelectCommand;

        /// <summary>
        /// Gets the Cluster Scheme for the auto match
        /// </summary>
        public RelayCommand ClusterSchemeSelectCommand
        {
            get
            {
                if (_clusterSchemeSelectCommand == null)
                {
                    _clusterSchemeSelectCommand = new RelayCommand(
                        p => ClusterSchemeSelectCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    RegisterCommand(_clusterSchemeSelectCommand);
                }
                return _clusterSchemeSelectCommand;
            }
        }

        private void ClusterSchemeSelectCommand_Executed()
        {
            ClusterSchemeSelectionViewModel viewModel = new ClusterSchemeSelectionViewModel();
            CommonHelper.GetWindowService().ShowDialog<ClusterSchemeSelectionWindow>(viewModel);

            if (viewModel.DialogResult == true)
            {
                // Check if the selected cluster scheme has changed
                if (_selectedClusterScheme != viewModel.SelectedClusterSchemeInfo)
                {
                    // Update the selected cluster scheme from the view model
                    this.SelectedClusterScheme = viewModel.SelectedClusterSchemeInfo;
                }
            }
        }

        #endregion

        #region RemoveClusterSchemeCommand

        private RelayCommand _removeClusterSchemeCommand;

        /// <summary>
        /// Clears the Cluster Scheme for the auto match
        /// </summary>
        public RelayCommand RemoveClusterSchemeCommand
        {
            get
            {
                if (_removeClusterSchemeCommand == null)
                {
                    _removeClusterSchemeCommand = new RelayCommand(
                        p => RemoveClusterSchemeCommand_Executed(),
                        p => RemoveClusterSchemeCommand_CanExecute())
                    {
                        FriendlyName = String.Empty,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_removeClusterSchemeCommand);
                }
                return _removeClusterSchemeCommand;
            }
        }

        private Boolean RemoveClusterSchemeCommand_CanExecute()
        {
            // If Cluster Scheme is null no point removing 
            if (this.SelectedClusterScheme == null)
            {
                _removeClusterSchemeCommand.DisabledReason = Message.PlanAssignmentAutoMatch_RemoveClusterSchemeDisabled_NoClusterSchemeToRemove;
                return false;
            }
            return true;
        }

        private void RemoveClusterSchemeCommand_Executed()
        {
            // Clear SelectedClusterScheme
            this.SelectedClusterScheme = null;
        }

        #endregion

        #region AddFieldCommand

        private RelayCommand _addFieldCommand;
        /// <summary>
        /// Remove selected row from the grid
        /// </summary>
        public RelayCommand AddFieldCommand
        {
            get
            {
                if (_addFieldCommand == null)
                {
                    _addFieldCommand = new RelayCommand(p => AddField_Executed(), p => AddField_CanExecute())
                    {
                        FriendlyName = String.Empty,
                        SmallIcon = ImageResources.Add_16
                    };
                    RegisterCommand(_addFieldCommand);
                }
                return _addFieldCommand;
            }
        }

        private Boolean AddField_CanExecute()
        {
            return true;
        }

        private void AddField_Executed()
        {
            String fieldText = Message.PlanAssignmentAutoMatch_FieldText_MatchBy;

            if (_availbleAutoMatchRows.Any())
            {
                fieldText = Message.PlanAssignmentAutoMatch_FieldText_ThenBy;
            }
            _availbleAutoMatchRows.Add(new AutoMatchRowView(_availableFields.FirstOrDefault(), MatchType.ExactMatch, _availableFields, fieldText));
        }

        #endregion

        #region RemoveFieldCommand

        private RelayCommand _removeFieldCommand;
        /// <summary>
        /// Adds a row to the grid
        /// </summary>
        public RelayCommand RemoveFieldCommand
        {
            get
            {
                if (_removeFieldCommand == null)
                {
                    _removeFieldCommand = new RelayCommand(p => RemoveField_Executed())
                    {
                        FriendlyName = String.Empty,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_removeFieldCommand);
                }
                return _removeFieldCommand;
            }
        }

        private void RemoveField_Executed()
        {
            _availbleAutoMatchRows.RemoveRange(SelectedAutoMatchRows);
            SelectedAutoMatchRows.Clear();
            if (_availbleAutoMatchRows.Any())
            {
                _availbleAutoMatchRows.FirstOrDefault().Text = Message.PlanAssignmentAutoMatch_FieldText_MatchBy;
            }
        }

        #endregion

        #region OkCommand

        private RelayCommand _okCommand;

        /// <summary>
        ///     does the match, then closes the window.
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand != null) return _okCommand;

                _okCommand = new RelayCommand(o => OkCommand_Executed())
                {
                    FriendlyName = Message.Generic_Ok,
                    InputGestureKey = Key.Enter
                };
                RegisterCommand(_okCommand);

                return _okCommand;
            }
        }

        private void OkCommand_Executed()
        {
            // perform the match
            List<AutoMatchRowView> autoMatchRows = AvailableAutoMatchRows.ToList();
            AutoMatchWork work = new AutoMatchWork(this);
            CommonHelper.GetModalBusyWorkerService().RunWorker(work);

            // then close the window
            DialogResult = true;
            CloseWindow();
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        ///     Cancels the change and closes the window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand != null) return _cancelCommand;

                _cancelCommand = new RelayCommand(o => CancelCommand_Executed())
                {
                    FriendlyName = Message.Generic_Cancel
                };
                RegisterCommand(_cancelCommand);

                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            DialogResult = false;

            CloseWindow();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// sets the default fields for the match properties
        /// </summary>
        public void GetPropertiesToMatch()
        {
            List<ObjectFieldInfo> availableProperties = Planogram.EnumerateDisplayableFieldInfos(true).ToList();
            String fieldText = Message.PlanAssignmentAutoMatch_FieldText_MatchBy;

            _availableFields.Add(availableProperties.FirstOrDefault(p => p.PropertyName.Equals(Planogram.MetaBayCountProperty.Name)));
            _availableFields.Add(availableProperties.FirstOrDefault(p => p.PropertyName.Equals(Planogram.WidthProperty.Name)));
            _availableFields.Add(availableProperties.FirstOrDefault(p => p.PropertyName.Equals(Planogram.HeightProperty.Name)));

            _availbleAutoMatchRows.Clear();
            // loads the performance selection name from the performance source
            foreach (ObjectFieldInfo field in _availableFields)
            {
                if (_availbleAutoMatchRows.Any()) 
                {
                    fieldText = Message.PlanAssignmentAutoMatch_FieldText_ThenBy;
                }
                _availbleAutoMatchRows.Add(new AutoMatchRowView(field, MatchType.ExactMatch, _availableFields, fieldText));
            }
        }

        #endregion

        #region IDisposable members

        protected override void Dispose(Boolean disposing)
        {
            if (!IsDisposed)
            {
                if (disposing)
                {
                    DisposeBase();
                }

                IsDisposed = true;
            }
        }

        #endregion

        /// <summary>
        ///     Fetch a list of <see cref="ClusterSchemeInfo"/> if the user set <see cref="UseClusterSchemes"/>.
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        ///     If the user does not wish to use cluster schemes, or there are none, an empty list will be returned.
        /// </remarks>
        private List<ClusterSchemeInfo> FetchClusterSchemesIfAllowed()
        {
            return UseClusterSchemes
                       ? ClusterSchemeInfoList.FetchByEntityId(EntityId).Where(c => c.ProductGroupId != null).ToList()
                       : new List<ClusterSchemeInfo>();
        }

        private PlanAssignmentStoreSpaceInfoList FetchStoreSpaceInfos(Boolean useClusterSchemes)
        {
            Boolean hasSelectedClusterScheme = !useClusterSchemes && SelectedClusterScheme != null;
            Int32? clusterSchemeId = hasSelectedClusterScheme ? (Int32?) SelectedClusterScheme.Id : null;
            return ViewType == PlanAssignmentViewType.ViewCategory
                       ? PlanAssignmentStoreSpaceInfoList.FetchByEntityIdProductGroupId(EntityId, ProductGroup.Id, clusterSchemeId)
                       : PlanAssignmentStoreSpaceInfoList.FetchByEntityIdLocationId(EntityId, Location.Id, clusterSchemeId);
        }

        private LocationPlanAssignmentList FetchLocationPlanAssignments()
        {
            return ViewType == PlanAssignmentViewType.ViewCategory
                       ? LocationPlanAssignmentList.FetchByEntityIdProductGroupId(EntityId, ProductGroup.Id)
                       : LocationPlanAssignmentList.FetchByEntityIdLocationId(EntityId, Location.Id);
        }

        private List<PlanogramInfo> GetValidPlanInfos(Boolean useClusterSchemes)
        {
            // get plans in selected group
            IEnumerable<PlanogramInfo> validPlansForAutoMatch;
            if (SelectedPlanogramGroup != null)
                validPlansForAutoMatch = PlanogramInfoList.FetchByPlanogramGroupId(SelectedPlanogramGroup.Id);
            else if (ViewType == PlanAssignmentViewType.ViewCategory)
                validPlansForAutoMatch = PlanogramInfoList.FetchNonDebugByCategoryCode(ProductGroup.Code);
            else
                validPlansForAutoMatch = PlanogramInfoList.FetchNonDebugByLocationCode(Location.Code);

            // get the plans with the correct status type and cluster scheme
            Boolean hasStatusType = StatusType != PlanogramStatusType.Unknown;
            if (hasStatusType) validPlansForAutoMatch = validPlansForAutoMatch.Where(info => info.Status == StatusType);

            Boolean hasSelectedClusterScheme = useClusterSchemes && SelectedClusterScheme != null;
            if (hasSelectedClusterScheme)
            {
                validPlansForAutoMatch =
                    validPlansForAutoMatch.Where(info =>
                                                 String.Equals(info.ClusterSchemeName, SelectedClusterScheme.Name));
            }

            return validPlansForAutoMatch.ToList();
        }

        private List<PlanogramInfo> FilterPlanInfos(IEnumerable<PlanogramInfo> planInfos, List<ClusterSchemeInfo> clusterSchemeInfos, PlanAssignmentStoreSpaceInfo storeSpaceInfo)
        {
            IEnumerable<PlanogramInfo> filteredPlanInfos = planInfos;

            // check if there is a correct cluster scheme for he store space
            if (clusterSchemeInfos.Any())
            {
                String clusterSchemeName;
                if (TryGetClusterSchemeName(clusterSchemeInfos, storeSpaceInfo.ProductGroupId, out clusterSchemeName))
                {
                    filteredPlanInfos = filteredPlanInfos.Where(info =>
                                                                String.Equals(info.ClusterSchemeName, clusterSchemeName) &&
                                                                String.Equals(info.ClusterName, storeSpaceInfo.ClusterName));
                }
            }
            else if (SelectedClusterScheme != null)
            {
                // match the clusters for the plans 
                filteredPlanInfos = filteredPlanInfos.Where(info =>
                                                            String.Equals(info.ClusterName, storeSpaceInfo.ClusterName));
            }

            // check if the plan is in the correct category
            filteredPlanInfos = filteredPlanInfos.Where(info =>
                                                        String.Equals(info.CategoryName, storeSpaceInfo.ProductGroupName) &&
                                                        String.Equals(info.CategoryCode, storeSpaceInfo.ProductGroupCode));

            return filteredPlanInfos.ToList();
        }

        private Boolean TryGetClusterSchemeName(IEnumerable<ClusterSchemeInfo> clusterSchemeInfos, Int32 categoryId, out String clusterSchemeName)
        {
            ClusterSchemeInfo clusterSchemeInfo = clusterSchemeInfos.Where(info => info.ProductGroupId == categoryId)
                                                                    .OrderByDescending(info => info.DateLastModified)
                                                                    .FirstOrDefault();
            if (clusterSchemeInfo != null)
                clusterSchemeName = clusterSchemeInfo.Name;
            else if (SelectedClusterScheme != null)
                clusterSchemeName = SelectedClusterScheme.Name;
            else
                clusterSchemeName = null;

            return !String.IsNullOrWhiteSpace(clusterSchemeName);
        }

        private List<PlanogramInfo> FilterUsingMatchRules(IEnumerable<PlanogramInfo> planInfos, PlanAssignmentStoreSpaceInfo storeSpaceInfo)
        {
            IEnumerable<PlanogramInfo> plans = planInfos;
            foreach (AutoMatchRowView rowView in AvailableAutoMatchRows)
            {
                plans = rowView.OrderItems(plans);
                plans = rowView.FilterItems(plans, storeSpaceInfo);
            }
            return plans.ToList();
        }

        private LocationPlanAssignment AssignPlanById(Int32 planogramId, PlanAssignmentStoreSpaceInfo storeSpaceInfo)
        {
            Int32 productGroupId = ViewType == PlanAssignmentViewType.ViewCategory
                                       ? ProductGroup.Id
                                       : storeSpaceInfo.ProductGroupId;
            Int16 locationId = ViewType == PlanAssignmentViewType.ViewCategory
                                   ? storeSpaceInfo.LocationId
                                   : Location.Id;
            LocationPlanAssignment planAssignment = LocationPlanAssignment.NewLocationPlanAssignment(productGroupId,
                                                                                                     planogramId,
                                                                                                     locationId,
                                                                                                     User.GetCurrentUser(),
                                                                                                     DateTime.UtcNow);
            return planAssignment;
        }

        private LocationPlanAssignment FindLocationPlanAssignment(List<PlanogramInfo> planInfos,
                                                                  LocationPlanAssignmentList existingAssignments,
                                                                  PlanAssignmentStoreSpaceInfo storeSpaceInfo)
        {
            //  Do not assign if already assigned to the category and location.
            if (storeSpaceInfo.FirstMatchByCategoryAndLocationOrDefault(existingAssignments) != null) return null;

            PlanogramInfo match;
            if (IsStoreSpecific &&
                storeSpaceInfo.TryMatchByLocation(planInfos, out match))
            {
                // if store specific ignore the priorities
                return AssignPlanById(match.Id, storeSpaceInfo);
            }

            //  The assignment is not Store Specific, or none was found.
            //  Apply the matching rules.
            
            List<PlanogramInfo> matchesByRules = FilterUsingMatchRules(planInfos, storeSpaceInfo);
            if (!matchesByRules.Any()) return null;

            //  Match by location. If no plan for location is found, assign all without a location.
            List<PlanogramInfo> planInfosForLocation = storeSpaceInfo.MatchesByLocation(matchesByRules);
            List<PlanogramInfo> matchesByLocation =
                planInfosForLocation.Any()
                    ? planInfosForLocation
                    : matchesByRules.Where(p => String.IsNullOrWhiteSpace(p.LocationCode)).ToList();

            //  Return any assigned location.
            return matchesByLocation.Any() ? AssignPlanById(matchesByLocation.Last().Id, storeSpaceInfo) : null;
        }
    }

    public sealed class AutoMatchRowView : ViewModelObject
    {
        #region FieldNames
        ObjectFieldInfo _property;
        MatchType _matchType;
        String _text;
        List<ObjectFieldInfo> _availableFields;
        #endregion

        #region Property Paths
        public static readonly PropertyPath MatchTypeProperty = WpfHelper.GetPropertyPath<AutoMatchRowView>(p => p.MatchType);
        public static readonly PropertyPath PropertyProperty = WpfHelper.GetPropertyPath<AutoMatchRowView>(p => p.Property);
        public static readonly PropertyPath TextProperty = WpfHelper.GetPropertyPath<AutoMatchRowView>(p => p.Text);
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="model"></param>
        public AutoMatchRowView(ObjectFieldInfo property, MatchType matchType, List<ObjectFieldInfo> availableProperties, String text)
        {
            _property = property;
            _matchType = matchType;
            _availableFields = availableProperties;
            _text = text;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the fields available for auto creation
        /// </summary>
        public IEnumerable<ObjectFieldInfo> AvailableFields
        {
            get { return _availableFields; }
        }

        /// <summary>
        /// returns the metric property
        /// </summary>
        public ObjectFieldInfo Property
        {
            get { return _property; }
            set
            {
                _property = value;
                OnPropertyChanged(MatchTypeProperty);
            }
        }

        /// <summary>
        /// returns the order text
        /// </summary>
        public String Text
        {
            get { return _text; }
            set 
            {
                _text = value;
                OnPropertyChanged(TextProperty);
            }
        }

        /// <summary>
        /// returns the metric name
        /// </summary>
        public String Name
        {
            get { return _property.PropertyName; }
        }

        /// <summary>
        /// Gets/Sets the match type
        /// </summary>
        public MatchType MatchType
        {
            get { return _matchType; }
            set
            {
                _matchType = value;
                OnPropertyChanged(MatchTypeProperty);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Orders the plans so that they can filtered for closest above or below
        /// </summary>
        /// <param name="planogramInfos"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramInfo> OrderItems(IEnumerable<PlanogramInfo> planogramInfos)
        {
            if (planogramInfos == null) return new List<PlanogramInfo>();

            String propertyName = Property.PropertyName;
            switch (MatchType)
            {
                case MatchType.ClosestAbove:
                    if (propertyName.Equals(PlanogramInfo.WidthProperty.Name)) return planogramInfos.OrderByDescending(p => p.Width);
                    if (propertyName.Equals(PlanogramInfo.HeightProperty.Name)) return planogramInfos.OrderByDescending(p => p.Height);
                    break;
                case MatchType.ClosestBelow:
                    if (propertyName.Equals(PlanogramInfo.WidthProperty.Name)) return planogramInfos.OrderBy(p => p.Width);
                    if (propertyName.Equals(PlanogramInfo.HeightProperty.Name)) return planogramInfos.OrderBy(p => p.Height);
                    break;
            }
            return planogramInfos;
        }

        /// <summary>
        /// filters the items through the condition in the match row
        /// </summary>
        /// <param name="planogramInfos"></param>
        /// <param name="storeSpaceInfo"></param>
        /// <returns></returns>
        public IEnumerable<PlanogramInfo> FilterItems(IEnumerable<PlanogramInfo> planogramInfos, PlanAssignmentStoreSpaceInfo storeSpaceInfo)
        {
            if (planogramInfos == null) return new List<PlanogramInfo>();

            return planogramInfos.Select(planogram => new {planogram, planMatch = IsPlanMatch(storeSpaceInfo, planogram)})
                                 .Where(t => t.planMatch)
                                 .Select(t => t.planogram);
        }

        private Boolean IsPlanMatch(PlanAssignmentStoreSpaceInfo storeSpaceInfo, PlanogramInfo planogram)
        {
            switch (MatchType)
            {
                case MatchType.ExactMatch:
                    return IsExactMatch(storeSpaceInfo, planogram);
                case MatchType.ClosestAbove:
                    return IsAboveMatch(storeSpaceInfo, planogram);
                case MatchType.ClosestBelow:
                    return IsBelowMatch(storeSpaceInfo, planogram);
                case MatchType.NoMatch:
                    return IsNoMatch(storeSpaceInfo, planogram);
                default:
                    return false;
            }
        }

        private Boolean IsNoMatch(PlanAssignmentStoreSpaceInfo storeSpaceInfo, PlanogramInfo planogram)
        {
            if (Property.PropertyName.Equals(PlanogramInfo.WidthProperty.Name))
                return storeSpaceInfo.BayTotalWidth > 0 && !planogram.Width.EqualTo(storeSpaceInfo.BayTotalWidth);

            if (Property.PropertyName.Equals(PlanogramInfo.MetaBayCountProperty.Name))
                return storeSpaceInfo.BayCount > 0 && planogram.MetaBayCount != storeSpaceInfo.BayCount;

            if (Property.PropertyName.Equals(PlanogramInfo.HeightProperty.Name))
                return storeSpaceInfo.BayHeight > 0 && !planogram.Height.EqualTo(storeSpaceInfo.BayHeight);

            return false;
        }

        private Boolean IsBelowMatch(PlanAssignmentStoreSpaceInfo storeSpaceInfo, PlanogramInfo planogram)
        {
            if (Property.PropertyName.Equals(PlanogramInfo.WidthProperty.Name))
                return planogram.Width <= storeSpaceInfo.BayTotalWidth && storeSpaceInfo.BayTotalWidth > 0;

            if (Property.PropertyName.Equals(PlanogramInfo.MetaBayCountProperty.Name))
                return planogram.MetaBayCount <= storeSpaceInfo.BayCount && storeSpaceInfo.BayCount > 0;

            if (Property.PropertyName.Equals(PlanogramInfo.HeightProperty.Name))
                return planogram.Height <= storeSpaceInfo.BayHeight && storeSpaceInfo.BayHeight > 0;

            return false;
        }

        private Boolean IsAboveMatch(PlanAssignmentStoreSpaceInfo storeSpaceInfo, PlanogramInfo planogram)
        {
            if (Property.PropertyName.Equals(PlanogramInfo.WidthProperty.Name))
                return storeSpaceInfo.BayTotalWidth > 0 && planogram.Width >= storeSpaceInfo.BayTotalWidth;

            if (Property.PropertyName.Equals(PlanogramInfo.MetaBayCountProperty.Name))
                return storeSpaceInfo.BayCount > 0 && planogram.MetaBayCount >= storeSpaceInfo.BayCount;

            if (Property.PropertyName.Equals(PlanogramInfo.HeightProperty.Name))
                return storeSpaceInfo.BayHeight > 0 && planogram.Height >= storeSpaceInfo.BayHeight;

            return false;
        }

        private Boolean IsExactMatch(PlanAssignmentStoreSpaceInfo storeSpaceInfo, PlanogramInfo planogram)
        {
            if (Property.PropertyName.Equals(PlanogramInfo.WidthProperty.Name))
                return storeSpaceInfo.BayTotalWidth > 0 && planogram.Width.EqualTo(storeSpaceInfo.BayTotalWidth);

            if (Property.PropertyName.Equals(PlanogramInfo.MetaBayCountProperty.Name))
                return storeSpaceInfo.BayCount > 0 && planogram.MetaBayCount == storeSpaceInfo.BayCount;

            if (Property.PropertyName.Equals(PlanogramInfo.HeightProperty.Name))
                return storeSpaceInfo.BayHeight > 0 && planogram.Height.EqualTo(storeSpaceInfo.BayHeight);
            return false;
        }

        #endregion

        #region IDisposable members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing) {}
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
