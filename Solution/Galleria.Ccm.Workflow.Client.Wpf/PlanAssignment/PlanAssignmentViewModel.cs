﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)

// CCM-25879 : N.Haywood
//	Created
// V8-27783 : M.Brumby
//  Added create Publish workpackage
// V8-28002 : L.Ineson
//  Removed save command and changed filtering.
//  Moved column creation to code behind
// V8-28192/V8-28195 : M.Shelley
//  Allow all levels of the product group hierarchy to be selected.
// V8-28212 : L.Ineson
//  Counters now consider location space checkbox.
// V8-28259 : A.Kuszyk
//  Added check to DoPlanAssignment for plan/location category mismatches.
// CCM-27599 : J.Pickup
//  Added Entity to FetchByPlanogramSelectorViewModel Constructor

#endregion Version History: (CCM 8.0.0)

#region Version History: (CCM 8.0.1)

// V8-28481 : M.Shelley
//  Added a refresh button to display updated data after running a publish workflow
// V8-28576 : M.Shelley
//  Add a polling mechanism to update the assigned plan list to allow for plan changes
//  and any changes to location space values

#endregion Version History: (CCM 8.0.1)

#region Version History: (CCM 8.0.2)

// V8-28986 : M.Shelley
//  Added plan assignment by location functionality
//	Added custom columns
// V8-29071 : M.Shelley
//  Fixed the auto-refresh so that a user's selected rows are saved during the refresh.
// V8-29071 : M.Shelley
//  Add a fix for the auto-refresh clearing the user's selection

#endregion Version History: (CCM 8.0.2)

#region Version History: (CCM 8.0.3)

// V8-29174 : M.Shelley
//  Add setting to save user's last Plan Assignment "View By" selection
// V8-29386 : M.Shelley
//  Added a fix to clear the master display collection when the user changes the selected location
// V8-29392 : M.Shelley
//  Added a fix for the refresh button so the enabled status is set according to the current view type
//  and the appropriate tooltip is displayed
// V8-29326 : M.Shelley
//  Add the cluster details to the assignment grid data for the location and planogram
//  Added cluster scheme select button

#endregion Version History: (CCM 8.0.3)

#region Version History: (CCM 8.1.0)

// V8-28242 : M.Shelley
//  Added "right-click" date property editor for the plan assignment grid rows
// V8-29598 : L.Luong
//  Added Auto Match
// V8-30057 : M.Shelley
//  Added a Planogram "Open" button

#endregion Version History: (CCM 8.1.0)

#region Version History: (CCM 8.1.1)

// V8-30417 : M.Brumby
//  When in location view publishing will now filter its location assignments by
//  the selected location.
// V8-30463 : L.Ineson
//  Refactored code to improve performance.
// V8-30561 : M.Brumby
//  Only publish plans if they have changed or if a new store is assigned to it.
// V8-30621 : D.Pleasance
//  Amended clear PlanPublishDate and PlanPublishUser when new plans are assigned (DoPlanAssignment).

#endregion Version History: (CCM 8.1.1)

#region Version History: (CCM 8.2.0)
// V8-29387 : M.Shelley
//  If the user is in the location view mode of plan assignment, disable the "Assign" button if multiple
//  rows are selected as a planogram should not be assigned to multiple product groups.
// V8-30732 : M.Shelley
//  Advanced planogram assignment enhancement - allow multiple plans to be assigned to a given location.
// V8-30802 : M.Shelley
//  Added a warning message when a database timeout is encountered (usually due to an on-going sync process)
//  and also added a refresh button thta only displays under these circumstances.
// V8-30940 : M.Shelley
//  Changed the post-assignment dialog window to display an information "i" icon if there are no actual warnings/errors
//  Warnings (bay plan mismatch / incorrect product groups) are still highlighted with a "!" icon as before
// V8-31571 : L.Ineson
//  No longer tries to load rows for deleted locations.
#endregion Version History: (CCM 8.2.0)

#region Version History: (CCM 8.3.0)
// V8-32495 : L.Ineson
//  Publish command now goes to plan selection page of wizard.
// V8-32810 : L.Ineson
//  Added Export to File
// V8-32061 : M.Brumby
//  When viewing by location, fix for product groups without location space not showing linked plans
// CCM-18393 : M.Brumby
//  PCR01601 - Improvements to CCM publishing - Workpakcage will now auto setup category if on category view
#endregion

#endregion Header Information

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.PlanAssignmentAutoMatch;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement;
using Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.ImportFromFile;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    /// <summary>
    /// Viewmodel controller for the planogram assigment screen.
    /// </summary>
    public sealed class PlanAssignmentViewModel : ViewModelAttachedControlObject<PlanAssignmentOrganiser>
    {
        #region Constants

        internal const String ScreenKey = "PlanAssignment.Workflow";

        #endregion Constants

        #region Fields

        //permissions:
        private readonly ModelPermission<ProductGroup> _productGroupPerms = new ModelPermission<ProductGroup>();

        private readonly Boolean _planogramFetchPerm;

        //data:
        private readonly ProductHierarchyViewModel _productHierarchyView = new ProductHierarchyViewModel();

        private Dictionary<Int32, ProductGroupInfo> _productGroupInfosById;
        private readonly LocationInfoListViewModel _locationInfoListView = new LocationInfoListViewModel();
        private readonly UserInfoListViewModel _userInfoListView = new UserInfoListViewModel();
        private LocationPlanAssignmentList _masterPublishPlanList;

        private readonly BulkObservableCollection<PlanAssignmentRowViewModel> _masterAssignmentItemRows = new BulkObservableCollection<PlanAssignmentRowViewModel>();
        private readonly BulkObservableCollection<PlanAssignmentRowViewModel> _filteredAssignmentItemRows = new BulkObservableCollection<PlanAssignmentRowViewModel>();
        private ReadOnlyBulkObservableCollection<PlanAssignmentRowViewModel> _filteredAssignmentItemRowsRo;
        private readonly ObservableCollection<PlanAssignmentRowViewModel> _planAssignmentSelectedItems = new ObservableCollection<PlanAssignmentRowViewModel>();

        private PlanAssignmentFilterType _selectedFilterType = PlanAssignmentFilterType.ShowAll;
        private Boolean _isFilteredToValidLocationSpace = true;
        private ProductGroupInfo _selectedProductGroup;
        private PlanogramInfo _activePlanogramInfo;

        private Int32 _showAllCount;
        private Int32 _assignedCount;
        private Int32 _unassignedCount;
        private Int32 _compatibleCount;
        private Int32 _incompatibleCount;
        private Int32 _multiplePlanCount;

        private PlanAssignmentViewType _currentViewType;
        private LocationInfo _selectedLocation;
        private PlanAssignmentAutoMatchViewModel _autoMatchViewModel;
        private ClusterSchemeInfo _selectedClusterScheme;

        private Boolean _disableCommands;
        String _messageDisplayTitle;
        String _messageDisplayHeader;
        String _messageDisplayText;

        #endregion Fields

        #region Property Binding Paths

        // Properties
        public static readonly PropertyPath SelectedProductGroupProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.SelectedProductGroup);
        public static readonly PropertyPath SelectedClusterSchemeProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.SelectedClusterScheme);
        public static readonly PropertyPath SelectedFilterTypeProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.SelectedFilterType);
        public static readonly PropertyPath IsFilteredToValidLocationSpaceProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.IsFilteredToValidLocationSpace);
        public static readonly PropertyPath FilteredAssignmentItemRowsProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.FilteredAssignmentItemRows);
        public static readonly PropertyPath PlanAssignmentSelectedItemsProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.PlanAssignmentSelectedItems);
        public static readonly PropertyPath ActivePlanogramInfoProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ActivePlanogramInfo);
        public static readonly PropertyPath ActivePlanogramRowProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ActivePlanogramRow);
        public static readonly PropertyPath ShowAllCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ShowAllCount);
        public static readonly PropertyPath AssignedCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.AssignedCount);
        public static readonly PropertyPath UnassignedCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.UnassignedCount);
        public static readonly PropertyPath CompatibleCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.CompatibleCount);
        public static readonly PropertyPath IncompatibleCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.IncompatibleCount);
        public static readonly PropertyPath MultiplePlanCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.MultiplePlanCount);
        public static readonly PropertyPath SelectedLocationProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.SelectedLocation);
        public static readonly PropertyPath CurrentViewTypeProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.CurrentViewType);
        public static readonly PropertyPath DisableCommandsProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.DisableCommands);

        // Commands
        public static readonly PropertyPath SelectProductGroupCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.SelectProductGroupCommand);
        public static readonly PropertyPath AssignPlanogramsCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.AssignPlanogramsCommand);
        public static readonly PropertyPath RemovePlanogramCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.RemovePlanogramCommand);
        public static readonly PropertyPath RemoveAllPlanogramsCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.RemoveAllPlanogramsCommand);
        public static readonly PropertyPath PublishPlanogramsCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.PublishPlanogramsCommand);
        public static readonly PropertyPath ViewByProductGroupCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ViewByProductGroupCommand);
        public static readonly PropertyPath ViewByLocationCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ViewByLocationCommand);
        public static readonly PropertyPath SelectLocationCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.SelectLocationCommand);
        public static readonly PropertyPath ClusterSchemeSelectCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ClusterSchemeSelectCommand);
        public static readonly PropertyPath AutoMatchCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.AutoMatchCommand);
        public static readonly PropertyPath ImportFromFileCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ImportFromFileCommand);
        public static readonly PropertyPath OpenPlanogramCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.OpenPlanogramCommand);
        public static readonly PropertyPath RefreshCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.RefreshCommand);
        public static readonly PropertyPath ExportToFileCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ExportToFileCommand);
        public static readonly PropertyPath ExportDataCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentViewModel>(p => p.ExportDataCommand);


        #endregion 

        #region Properties

        /// <summary>
        /// Gets/Sets the currently selected group
        /// </summary>
        public ProductGroupInfo SelectedProductGroup
        {
            get { return _selectedProductGroup; }
            set
            {
                _selectedProductGroup = value;
                OnPropertyChanged(SelectedProductGroupProperty);
                OnSelectedProductGroupChanged();
            }
        }

        /// <summary>
        /// Gets/Sets the selected filter type
        /// </summary>
        public PlanAssignmentFilterType SelectedFilterType
        {
            get { return _selectedFilterType; }
            set
            {
                _selectedFilterType = value;
                OnPropertyChanged(SelectedFilterTypeProperty);

                //refilter
                FilterLocationAssignedItems();
            }
        }

        /// <summary>
        /// Gets/Sets whether only rows with valid location
        /// space should be displayed.
        /// </summary>
        public Boolean IsFilteredToValidLocationSpace
        {
            get { return _isFilteredToValidLocationSpace; }
            set
            {
                _isFilteredToValidLocationSpace = value;
                OnPropertyChanged(IsFilteredToValidLocationSpaceProperty);

                //refilter
                FilterLocationAssignedItems();
            }
        }

        /// <summary>
        /// The list of all the locations / assigned plans
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanAssignmentRowViewModel> FilteredAssignmentItemRows
        {
            get
            {
                if (_filteredAssignmentItemRowsRo == null)
                {
                    _filteredAssignmentItemRowsRo = new ReadOnlyBulkObservableCollection<PlanAssignmentRowViewModel>(_filteredAssignmentItemRows);
                }
                return _filteredAssignmentItemRowsRo;
            }
        }

        /// <summary>
        /// The list of items selected by the user on the main grid
        /// </summary>
        public ObservableCollection<PlanAssignmentRowViewModel> PlanAssignmentSelectedItems
        {
            get { return _planAssignmentSelectedItems; }
        }

        /// <summary>
        /// Returns the currently active plan to show info for.
        /// </summary>
        public PlanogramInfo ActivePlanogramInfo
        {
            get { return _activePlanogramInfo; }
            private set
            {
                if (_activePlanogramInfo != value)
                {
                    _activePlanogramInfo = value;
                    OnPropertyChanged(ActivePlanogramInfoProperty);
                    OnPropertyChanged(ActivePlanogramRowProperty);
                }
            }
        }

        /// <summary>
        /// Returns the planogram thumbnail for the active planogram info.
        /// </summary>
        public PlanogramRepositoryRow ActivePlanogramRow
        {
            get
            {
                if (this.ActivePlanogramInfo == null) return null;
                return new PlanogramRepositoryRow(this.ActivePlanogramInfo, null);
            }
        }

        /// <summary>
        /// Returns a count for the ShowAll filter type.
        /// </summary>
        public Int32 ShowAllCount
        {
            get { return _showAllCount; }
            private set
            {
                _showAllCount = value;
                OnPropertyChanged(ShowAllCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the Assigned filter type.
        /// </summary>
        public Int32 AssignedCount
        {
            get { return _assignedCount; }
            private set
            {
                _assignedCount = value;
                OnPropertyChanged(AssignedCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the Unassigned filter type.
        /// </summary>
        public Int32 UnassignedCount
        {
            get { return _unassignedCount; }
            private set
            {
                _unassignedCount = value;
                OnPropertyChanged(UnassignedCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the Compatible filter type.
        /// </summary>
        public Int32 CompatibleCount
        {
            get { return _compatibleCount; }
            private set
            {
                _compatibleCount = value;
                OnPropertyChanged(CompatibleCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the Incompatible filter type.
        /// </summary>
        public Int32 IncompatibleCount
        {
            get { return _incompatibleCount; }
            private set
            {
                _incompatibleCount = value;
                OnPropertyChanged(IncompatibleCountProperty);
            }
        }

        /// <summary>
        /// Returns a count for the number of locations / product groups that have multiple plans assigned
        /// </summary>
        public Int32 MultiplePlanCount
        {
            get { return _multiplePlanCount; }
            set
            {
                _multiplePlanCount = value;
                OnPropertyChanged(MultiplePlanCountProperty);
            }
        }

        /// <summary>
        /// Returns true if this viewmodel is polling.
        /// </summary>
        public Boolean IsPolling
        {
            get { return _isPolling; }
            private set { _isPolling = value; }
        }

        /// <summary>
        /// Gets/Sets the currently selected location
        /// </summary>
        public LocationInfo SelectedLocation
        {
            get { return _selectedLocation; }
            set
            {
                _selectedLocation = value;
                OnPropertyChanged(SelectedLocationProperty);
                OnSelectedLocationChanged();
            }
        }

        /// <summary>
        /// Gets/Sets the current view type.
        /// </summary>
        public PlanAssignmentViewType CurrentViewType
        {
            get { return _currentViewType; }
            private set
            {
                _currentViewType = value;
                OnPropertyChanged(CurrentViewTypeProperty);
            }
        }

        public Boolean DisableCommands
        {
            get { return _disableCommands; }
            internal set
            {
                _disableCommands = value;
                OnPropertyChanged(DisableCommandsProperty);
            }
        }

        public ClusterSchemeInfo SelectedClusterScheme
        {
            get { return _selectedClusterScheme; }
            internal set 
            {
                _selectedClusterScheme = value;
                OnPropertyChanged(SelectedClusterSchemeProperty);
            }
        }

        internal BulkObservableCollection<PlanAssignmentRowViewModel> MasterAssignmentItemRows
        {
            get { return _masterAssignmentItemRows; }
        }

        internal String MessageDisplayTitle
        {
            get { return _messageDisplayTitle; }
        }

        internal String MessageDisplayHeader
        {
            get { return _messageDisplayHeader; }
        }

        internal String MessageDisplayText
        {
            get { return _messageDisplayText; }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanAssignmentViewModel()
        {
            _planogramFetchPerm = ApplicationContext.User.IsInRole(DomainPermission.PlanogramGet.ToString());

            _planAssignmentSelectedItems.CollectionChanged += PlanAssignmentSelectedItems_CollectionChanged;

            //set the view type according to user settings.
            UserSystemSetting userSettings = App.GetSystemSettings();
            _currentViewType = userSettings.PlanAssignmentViewBy;

            // NB: All data fetches are now performed on a lazy basis.

            // Start the automatic data refresh
            StartPolling();
        }

        #endregion Constructor

        #region Commands

        #region SelectProductGroupCommand

        private RelayCommand _selectProductGroupCommand;

        /// <summary>
        /// Shows the select Product group dialog
        /// </summary>
        public RelayCommand SelectProductGroupCommand
        {
            get
            {
                if (_selectProductGroupCommand == null)
                {
                    _selectProductGroupCommand = new RelayCommand(
                        p => SelectProductGroup_Executed(),
                        p => SelectProductGroup_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    base.ViewModelCommands.Add(_selectProductGroupCommand);
                }
                return _selectProductGroupCommand;
            }
        }

        private Boolean SelectProductGroup_CanExecute()
        {
            //user must have get permission
            if (!_productGroupPerms.CanFetch)
            {
                this.SelectProductGroupCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SelectProductGroup_Executed()
        {
            if (this.AttachedControl == null) return;

            MerchGroupSelectionWindow dialog = null;

            StopPollingAsync();

            Boolean isCancel = false;

            try
            {
                base.ShowWaitCursor(true);

                // make sure the hierarchy is fully up to date.
                _productHierarchyView.FetchMerchandisingHierarchyForCurrentEntity();
                dialog = new MerchGroupSelectionWindow(_productHierarchyView.Model, null, DataGridSelectionMode.Single);

                base.ShowWaitCursor(false);

                DisableCommands = false;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);

                return;
            }

            //if an error ocurred then just restart polling and cancel out.
            if (isCancel || dialog == null)
            {
                StartPolling();
                return;
            }

            CommonHelper.GetWindowService().ShowDialog<MerchGroupSelectionWindow>(dialog);

            if (dialog.DialogResult == true)
            {
                ProductGroup selectedGroup = dialog.SelectionResult.FirstOrDefault();
                if (selectedGroup != null)
                {
                    _masterAssignmentItemRows.Clear();
                    this.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(selectedGroup);
                }
            }

            //restart polling
            StartPolling();
        }

        #endregion SelectProductGroupCommand

        #region SelectLocationCommand

        private RelayCommand _selectLocationCommand;

        /// <summary>
        /// Shows the dialog allowing the user to select a location.
        /// </summary>
        public RelayCommand SelectLocationCommand
        {
            get
            {
                if (_selectLocationCommand == null)
                {
                    _selectLocationCommand = new RelayCommand(
                        p => SelectLocationCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    base.ViewModelCommands.Add(_selectLocationCommand);
                }
                return _selectLocationCommand;
            }
        }

        private void SelectLocationCommand_Executed()
        {
            //stop polling
            StopPollingAsync();

            LocationHierarchyViewModel locationHierarchyView = new LocationHierarchyViewModel();
            try
            {
                base.ShowWaitCursor(true);

                locationHierarchyView.FetchForCurrentEntity();
                _locationInfoListView.FetchAllForEntity();

                base.ShowWaitCursor(false);

                DisableCommands = false;
            }
            catch (Exception ex)
            {
                ExceptionHandler(ex);

                return;
            }

            //Show the location selection dialog.
            LocationSelectionViewModel winView =
                new LocationSelectionViewModel(_locationInfoListView.Model,
                    new ObservableCollection<LocationInfo>(),
                    locationHierarchyView.Model, DataGridSelectionMode.Single);

            CommonHelper.GetWindowService().ShowDialog<LocationSelectionWindow>(winView);

            if (winView.DialogResult == true)
            {
                //load the new selection.
                _masterAssignmentItemRows.Clear();

                LocationInfoRowViewModel selItem = winView.SelectedAvailableLocations.SingleOrDefault();
                this.SelectedLocation = (selItem != null ? selItem.Location : null);
            }

            //restart polling.
            StartPolling();
        }

        #endregion SelectLocationCommand

        #region AssignPlanogramsCommand

        private RelayCommand _assignPlanogramsCommand;

        /// <summary>
        /// Shows the select Product group dialog
        /// </summary>
        public RelayCommand AssignPlanogramsCommand
        {
            get
            {
                if (_assignPlanogramsCommand == null)
                {
                    _assignPlanogramsCommand = new RelayCommand(
                        p => AssignPlanograms_Executed(),
                        p => AssignPlanograms_CanExecute())
                    {
                        FriendlyName = Message.PlanAssignment_AssignPlanograms,
                        FriendlyDescription = Message.PlanAssignment_AssignPlanograms_Desc,
                        Icon = ImageResources.PlanAssign_SelectItems,
                    };
                    base.ViewModelCommands.Add(_assignPlanogramsCommand);
                }
                return _assignPlanogramsCommand;
            }
        }

        private Boolean AssignPlanograms_CanExecute()
        {
            Boolean isEnabled = true;
            String disabledReason = String.Empty;

            // V8-29387: Check if we are in the assign by product group view and if the user has
            // selected multiple rows that have different assigned plans
            // N.B. a row with no plan assignment and rows with the same assigned plan are ok, but
            // selected rows with different assigned plans cannot be assigned

            // Do a group by on the selected rows
            var groupList = _planAssignmentSelectedItems.GroupBy(x => x.PlanogramInfo).ToList();
    
            if (_planAssignmentSelectedItems == null || (!_planAssignmentSelectedItems.Any()))
            {
                disabledReason = Message.Generic_NoItemSelected;
                isEnabled = false;
            }
            // V8-29387: Check if we are in the assign by Location view and if the user has selected
            // multiple rows i.e. multiple product categories
            else if (_currentViewType == PlanAssignmentViewType.ViewLocation && _planAssignmentSelectedItems.Count() > 1)
            {
                disabledReason = Message.PlanAssignment_AssignDisabled_MultiCategory;
                isEnabled = false;
            }
            else if (groupList.Count() > 1)
            {
                // get a list of the keys
                var keyList = groupList.Select(x => x.Key).ToList();

                // and check if one of them is "null" and the count is 2 - this is ok (see above)
                if (keyList.Contains(null) && groupList.Count() == 2)
                {
                    isEnabled = true;
                }
                else
                {
                    disabledReason = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.PlanAssignment_SelectSamePlanogram_Warning;
                    isEnabled = false;
                }
            }
            //  V8-30802 : disable the command buttons if the database access is currently timing out due to a sync process.
            else if (_disableCommands)
            {
                disabledReason = Message.PlanAssignment_DatabaseTimedOut;
                isEnabled = false;
            }

            this.AssignPlanogramsCommand.DisabledReason = disabledReason;
            return isEnabled;
        }

        private void AssignPlanograms_Executed()
        {
            // Stop refreshing the grid as the user has selected some rows
            StopPollingAsync();

            DoPlanAssignment();

            // Start refreshing the grid again
            StartPolling();
        }

        #endregion AssignPlanogramsCommand

        #region ViewByProductGroupCommand

        private RelayCommand _viewByProductGroupCommand;

        /// <summary>
        /// Changes the view type to category
        /// </summary>
        public RelayCommand ViewByProductGroupCommand
        {
            get
            {
                if (_viewByProductGroupCommand == null)
                {
                    _viewByProductGroupCommand = new RelayCommand(
                        p => ViewByProductGroupCommand_Executed())
                    {
                        FriendlyName = Message.PlanAssignment_Category,
                        FriendlyDescription = Message.PlanAssignment_ViewByCategory_Tooltip,
                        Icon = ImageResources.PlanAssign_ProductGroup,
                    };
                    base.ViewModelCommands.Add(_viewByProductGroupCommand);
                }
                return _viewByProductGroupCommand;
            }
        }

        private void ViewByProductGroupCommand_Executed()
        {
            ChangeViewType(PlanAssignmentViewType.ViewCategory);
        }

        #endregion ViewByProductGroupCommand

        #region ViewByLocationCommand

        private RelayCommand _viewByLocationCommand;

        /// <summary>
        /// Changes the view type to location
        /// </summary>
        public RelayCommand ViewByLocationCommand
        {
            get
            {
                if (_viewByLocationCommand == null)
                {
                    _viewByLocationCommand = new RelayCommand(
                        p => ViewByLocationCommand_Executed())
                    {
                        FriendlyName = Message.PlanAssignment_Location,
                        FriendlyDescription = Message.PlanAssignment_ViewByLocation_Tooltip,
                        Icon = ImageResources.PlanAssign_Location,
                    };
                    base.ViewModelCommands.Add(_viewByLocationCommand);
                }
                return _viewByLocationCommand;
            }
        }

        private void ViewByLocationCommand_Executed()
        {
            ChangeViewType(PlanAssignmentViewType.ViewLocation);
        }

        #endregion ViewByLocationCommand

        #region RemovePlanogramCommand

        private RelayCommand _removePlanogramCommand;

        /// <summary>
        /// Remove selected plan assignments
        /// </summary>
        public RelayCommand RemovePlanogramCommand
        {
            get
            {
                if (_removePlanogramCommand == null)
                {
                    _removePlanogramCommand = new RelayCommand(
                        p => RemovePlanogram_Executed(),
                        p => RemovePlanogram_CanExecute())
                    {
                        FriendlyName = Message.PlanAssignment_RemovePlanogram,
                        FriendlyDescription = Message.PlanAssignment_RemovePlanogram_Desc,
                        Icon = ImageResources.Delete_32,
                        SmallIcon = ImageResources.Delete_16,
                    };
                    base.ViewModelCommands.Add(_removePlanogramCommand);
                }
                return _removePlanogramCommand;
            }
        }

        private Boolean RemovePlanogram_CanExecute()
        {
            bool isEnabled = true;
            String disabledReason = String.Empty;

            // Check that items are selected
            if (_planAssignmentSelectedItems == null || !_planAssignmentSelectedItems.Any())
            {
                disabledReason = Message.Generic_NoItemSelected;
                isEnabled = false;
            }
            else if (!_planAssignmentSelectedItems.Any(x => !String.IsNullOrEmpty(x.PlanName)))
            {
                disabledReason = Message.PlanAssignment_RemoveNoPlansSelected;
                isEnabled= false;
            }
            //  V8-30802 : disable the command buttons if the database access is currently timing out due to a sync process.
            else  if (_disableCommands)
            {
                disabledReason = Message.PlanAssignment_DatabaseTimedOut;
                isEnabled = false;
            }

            this.RemovePlanogramCommand.DisabledReason = disabledReason;
            return isEnabled;
        }

        // Remove selected planogram assignments
        private void RemovePlanogram_Executed()
        {
            if (this._planAssignmentSelectedItems == null)
            {
                return;
            }

            var selectedItemsCopyList = this._planAssignmentSelectedItems.ToList();

            foreach (var locationItem in selectedItemsCopyList)
            {
                RemovePlanogramItem(locationItem);
            }

            //commit changes
            Save();
        }

        #endregion RemovePlanogramCommand

        #region RemoveAllPlanogramsCommand

        private RelayCommand _removeAllPlanogramsCommand;

        /// <summary>
        /// Allow the user to remove all active plan assignments
        /// </summary>
        public RelayCommand RemoveAllPlanogramsCommand
        {
            get
            {
                if (_removeAllPlanogramsCommand == null)
                {
                    _removeAllPlanogramsCommand = new RelayCommand(
                        p => RemoveAllPlanogramsCommand_Executed(),
                        p => RemoveAllPlanogramsCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_RemoveAllItems,
                        FriendlyDescription = Message.Generic_RemoveAllFromAssigned,
                        Icon = ImageResources.Delete_32,
                        SmallIcon = ImageResources.Delete_16,
                    };
                    base.ViewModelCommands.Add(_removeAllPlanogramsCommand);
                }
                return _removeAllPlanogramsCommand;
            }
        }

        private Boolean RemoveAllPlanogramsCommand_CanExecute()
        {
            bool isEnabled = true;
            String disabledReason = "";

            //  V8-30802 : disable the command buttons if the database access is currently timing out due to a sync process.
            if (_disableCommands)
            {
                disabledReason = Message.PlanAssignment_DatabaseTimedOut;
                isEnabled = false;
            }

            this.RemoveAllPlanogramsCommand.DisabledReason = disabledReason;
            return isEnabled;
        }

        // Remove all current planogram assignments for the selected product group
        private void RemoveAllPlanogramsCommand_Executed()
        {
            // Get a list of all the locations that have been assigned
            var allAssignedLocations = _filteredAssignmentItemRows.Where(x => !String.IsNullOrEmpty(x.PlanName)).ToList();

            foreach (var locationItem in allAssignedLocations)
            {
                RemovePlanogramItem(locationItem);
            }

            //commit changes
            Save();
        }

        #endregion RemoveAllPlanogramsCommand

        #region PublishPlanogramsCommand

        private RelayCommand _publishPlanogramsCommand;

        /// <summary>
        /// Launches the workpackage wizard to publish planograms
        /// </summary>
        public RelayCommand PublishPlanogramsCommand
        {
            get
            {
                if (_publishPlanogramsCommand == null)
                {
                    _publishPlanogramsCommand = new RelayCommand(
                        p => PublishPlanogramsCommand_Executed(),
                        p => PublishPlanogramsCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Publish,
                        FriendlyDescription = Message.PlanAssignment_PublishToGfs,
                        Icon = ImageResources.PlanAssignment_PublishToGfs,
                        SmallIcon = ImageResources.PlanAssignment_PublishToGfs,
                    };
                    base.ViewModelCommands.Add(_publishPlanogramsCommand);
                }
                return _publishPlanogramsCommand;
            }
        }

        private Boolean PublishPlanogramsCommand_CanExecute()
        {
            // There must be plans to publish
            if (_masterPublishPlanList == null || _masterPublishPlanList.Count == 0)
            {
                this.PublishPlanogramsCommand.DisabledReason = Message.PlanAssignment_Publish_NoAssignedPlanograms;
                return false;
            }
            // The connection entity needs to be set
            else if (!App.ViewState.CurrentEntityView.Model.GFSId.HasValue)
            {
                this.PublishPlanogramsCommand.DisabledReason =
                    Galleria.Ccm.Common.Wpf.Resources.Language.Message.GFSConnectionHelper_EntityNotSet;
                return false;
            }
            // There must be a connection path set (but not in unit testing)
            else if (!App.IsUnitTesting &&
                String.IsNullOrEmpty(App.ViewState.CurrentEntityView.Model.SystemSettings.FoundationServicesEndpoint))
            {
                this.PublishPlanogramsCommand.DisabledReason =
                    Galleria.Ccm.Common.Wpf.Resources.Language.Message.GFSConnectionHelper_PathNotSet;
                return false;
            }
            //  V8-30802 : disable the command buttons if the database access is currently timing out due to a sync process.
            else if (_disableCommands)
            {
                this.PublishPlanogramsCommand.DisabledReason = Message.PlanAssignment_DatabaseTimedOut;
                return false;
            }

            this.PublishPlanogramsCommand.DisabledReason = String.Empty;
            return true;
        }

        private void PublishPlanogramsCommand_Executed()
        {
            if (_masterPublishPlanList == null || _masterPublishPlanList.Count == 0) return;

            EntityViewModel curEntityView = App.ViewState.CurrentEntityView;
            curEntityView.CheckGFSConnectionState();

            if (curEntityView.ServicesConnectionState != EntityViewModel.GFSConnectionState.Successful) return;

            WizardViewModel workpackageViewModel = new WizardViewModel();

            //Modify existing as we don't need a new planogram to be created.
            (workpackageViewModel.CurrentStepViewModel as SelectCreationTypeStepViewModel).SelectedWorkpackageType = WorkpackageType.ModifyExisting;

            //Fetch our planograms.
            PlanogramInfoList planogramInfos = PlanogramInfoList.FetchByIds(_masterPublishPlanList.Select(a => a.PlanogramId).Distinct());

            //Assign the planograms before the SelectPlanogramStep so that they will be pre selected
            foreach (PlanogramInfo info in planogramInfos)
            {
                //Only publish the plan if it has changed or if a new store is assigned to it.
                if (_masterPublishPlanList.Any(p => p.PlanogramId == info.Id && (!p.DatePublished.HasValue || p.DatePublished < info.DateLastModified)))
                {
                    workpackageViewModel.WorkpackageData.AssignedPlanograms.Add(info);
                }
            }

            //if we have plans continue
            if (workpackageViewModel.WorkpackageData.AssignedPlanograms.Any())
            {
                //if we are viewing by location then the publish will be setup so it only
                //publishes to the selected location.
                if (this.CurrentViewType == PlanAssignmentViewType.ViewLocation && this.SelectedLocation != null)
                {
                    workpackageViewModel.WorkpackageData.AssignedStores.Add(this.SelectedLocation);
                    workpackageViewModel.WorkpackageData.AutoAssignLocationParameters = true;
                }

                if (this.CurrentViewType == PlanAssignmentViewType.ViewCategory && this.SelectedProductGroup != null)
                {
                    workpackageViewModel.WorkpackageData.AssignedProductGroup = this.SelectedProductGroup;
                    workpackageViewModel.WorkpackageData.AutoAssignCategoryParameters= true;
                }
                //goto select planogram step
                workpackageViewModel.NextCommand.Execute();

                //show the dialog.
                CommonHelper.GetWindowService().ShowDialog<WizardWindow>(workpackageViewModel);
            }
            else
            {
                CommonHelper.GetWindowService().ShowOkMessage(Ccm.Common.Wpf.Services.MessageWindowType.Info,
                    Message.PlanAssignment_PublishingNotRequired_Title,
                    Message.PlanAssignment_PublishingNotRequired_Description);
            }
        }

        #endregion PublishPlanogramsCommand

        #region ClusterSchemeSelectCommand

        private RelayCommand _clusterSchemeSelectCommand;

        /// <summary>
        /// Shows the dialog to allow the user to select a cluster scheme.
        /// </summary>
        public RelayCommand ClusterSchemeSelectCommand
        {
            get
            {
                if (_clusterSchemeSelectCommand == null)
                {
                    _clusterSchemeSelectCommand = new RelayCommand(
                        p => ClusterSchemeSelectCommand_Executed(),
                        p=>ClusterSchemeSelectCommand_CanExecute())
                    {
                        FriendlyName = Message.PlanAssignment_ClusterSelect,
                        FriendlyDescription = Message.PlanAssignment_ClusterSelectTooltip,
                        Icon = ImageResources.PlanAssignment_SelectCluster,
                        SmallIcon = ImageResources.PlanAssignment_SelectCluster
                    };
                    base.ViewModelCommands.Add(_clusterSchemeSelectCommand);
                }
                return _clusterSchemeSelectCommand;
            }
        }

        private Boolean ClusterSchemeSelectCommand_CanExecute()
        {
            //  V8-30802 : disable the command buttons if the database access is currently timing out due to a sync process.
            if (_disableCommands)
            {
                this.ClusterSchemeSelectCommand.DisabledReason = Message.PlanAssignment_DatabaseTimedOut;
                return false;
            }

			return true;
		}

        private void ClusterSchemeSelectCommand_Executed()
        {
            //create and show the dialog.
            ClusterSchemeSelectionViewModel viewModel = new ClusterSchemeSelectionViewModel();
            CommonHelper.GetWindowService().ShowDialog<ClusterSchemeSelectionWindow>(viewModel);
            if (viewModel.DialogResult != true) return;

            // Check if the selected cluster scheme has changed
            if (SelectedClusterScheme != null &&
                SelectedClusterScheme.Id == viewModel.SelectedClusterSchemeInfo.Id) return;

            // Update the selected cluster scheme from the view model
            //clone it so that the rest of the cluster schemes can be dropped.
            SelectedClusterScheme = viewModel.SelectedClusterSchemeInfo.Clone();

            // Update the grid using the selected cluster
            _masterAssignmentItemRows.Clear();
            RefreshAvailablePlanAssignmentItemRows();
        }

        #endregion ClusterSchemeSelectCommand

        #region AutoMatchCommand

        private RelayCommand _autoMatchCommand;

        /// <summary>
        /// Opens the auto match dialog.
        /// </summary>
        public RelayCommand AutoMatchCommand
        {
            get
            {
                if (_autoMatchCommand == null)
                {
                    _autoMatchCommand = new RelayCommand(
                        p => AutoMatchCommand_Executed(),
                        p => AutoMatchCommand_CanExecute())
                    {
                        FriendlyName = Message.PlanAssignment_AutoMatch,
                        FriendlyDescription = Message.PlanAssignment_AutoMatchTooltip,
                        Icon = ImageResources.PlanAssignment_AutoMatch,
                    };
                    base.ViewModelCommands.Add(_autoMatchCommand);
                }
                return _autoMatchCommand;
            }
        }

        private Boolean AutoMatchCommand_CanExecute()
        {
            // if there is a location or a category for auto match
            if (_currentViewType == PlanAssignmentViewType.ViewCategory)
            {
                if (this.SelectedProductGroup == null)
                {
                    _autoMatchCommand.DisabledReason = Message.PlanAssignment_AutoMatchDisabled_NoProductCategory;
                    return false;
                }
            }
            else
            {
                if (this.SelectedLocation == null)
                {
                    _autoMatchCommand.DisabledReason = Message.PlanAssignment_AutoMatchDisabled_NoLocation;
                    return false;
                }
            }

            //  V8-30802 : disable the command buttons if the database access is currently timing out due to a sync process.
            if (_disableCommands)
            {
                this.AutoMatchCommand.DisabledReason = Message.PlanAssignment_DatabaseTimedOut;
                return false;
            }

            return true;
        }

        private void AutoMatchCommand_Executed()
        {
            //create the view if it does not yet exist.
            if (_autoMatchViewModel == null)
            {
                _autoMatchViewModel = new PlanAssignmentAutoMatchViewModel();
            }

            //update view properties
            PlanAssignmentAutoMatchViewModel autoMatchViewModel = _autoMatchViewModel;
            autoMatchViewModel.Location = _selectedLocation;
            autoMatchViewModel.ViewType = CurrentViewType;
            autoMatchViewModel.ProductGroup = _selectedProductGroup;
            autoMatchViewModel.SelectedClusterScheme = SelectedClusterScheme;

            //show the dialog.
            CommonHelper.GetWindowService().ShowDialog<PlanAssignmentAutoMatchWindow>(autoMatchViewModel);

            if (autoMatchViewModel.DialogResult == true)
            {
                // refresh once it is done
                _masterAssignmentItemRows.Clear();
                RefreshAvailablePlanAssignmentItemRows();
            }
        }

        #endregion AutoMatchCommand

        #region OpenPlanogramCommand

        private RelayCommand _openPlanogramCommand;

        /// <summary>
        /// Opens the currently selected planograms in space planning.
        /// </summary>
        public RelayCommand OpenPlanogramCommand
        {
            get
            {
                if (_openPlanogramCommand == null)
                {
                    _openPlanogramCommand = new RelayCommand(
                        p => OpenPlanogramCommand_Executed(),
                        p => OpenPlanogramCommand_CanExecute())
                    {
                        FriendlyName = Message.PlanRepository_OpenPlanogramCommand,
                        FriendlyDescription = Message.PlanRepository_OpenPlanogramCommand_Description,
                        Icon = ImageResources.PlanogramRepository_OpenPlanogram,
                        DisabledReason = Message.PlanRepository_OpenPlanogramCommand_DisabledReason,
                    };
                    base.ViewModelCommands.Add(_openPlanogramCommand);
                }
                return _openPlanogramCommand;
            }
        }

        private Boolean OpenPlanogramCommand_CanExecute()
        {
            bool isEnabled = true;
            String disabledReason = "";

            // Must have open permission for planogram
            if (!_planogramFetchPerm)
            {
                disabledReason = Message.Generic_NoFetchPermission;
                isEnabled= false;
            }
            // Check if there are any selected rows that have planograms assigned
            else if (this.PlanAssignmentSelectedItems.All(x => x.PlanogramInfo == null))
            {
                disabledReason = Message.PlanogramRepository_OpenPlanogram_DisabledNoSelection;
                isEnabled= false;
            }

            this.OpenPlanogramCommand.DisabledReason = disabledReason;
            return isEnabled;
        }

        private void OpenPlanogramCommand_Executed()
        {
            // Get the list of planograms selected and make it distinct so we do not try to open
            // the same plan multiple times and get the editor "Plan already open" warning
            LocalHelper.OpenPlanogramsInEditor(
                this.PlanAssignmentSelectedItems
                .Where(x => x.PlanogramInfo != null)
                .Select(y => y.PlanogramInfo).Distinct().ToList());
        }

        #endregion OpenPlanogramCommand

        #region RefreshCommand

        private RelayCommand _refreshCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand RefreshCommand
        {
            get
            {
                if (_refreshCommand == null)
                {
                    _refreshCommand = new RelayCommand(
                        p => RefreshCommand_Executed(),
                        p => RefreshCommand_CanExecute())
                    {
                        FriendlyName = Message.EventLog_Refresh,
                        FriendlyDescription = Message.EventLog_Refresh_Description,
                        Icon = ImageResources.EventLog_Refresh,
                    };
                    base.ViewModelCommands.Add(_refreshCommand);
                }
                return _refreshCommand;
            }
        }

        private bool RefreshCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            if (!_disableCommands)
            {
                enableCommand = false;
            }

            // Check if the chosen view is by product category or by location and set 
            // the disabled status accordingly
            if (this._currentViewType == PlanAssignmentViewType.ViewCategory)
            {
                if (this.SelectedProductGroup == null)
                {
                    enableCommand = false;
                    // "No product category is selected.";
                    disabledReason = Message.PlanAssignment_AutoMatchDisabled_NoProductCategory;
                }
            }
            else
            {
                if (this.SelectedLocation == null)
                {
                    enableCommand = false;
                    // "No location is selected.";
                    disabledReason = Message.PlanAssignment_AutoMatchDisabled_NoLocation;
                }
            }

            _refreshCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void RefreshCommand_Executed()
        {
            base.ShowWaitCursor(true);

            RefreshAvailablePlanAssignmentItemRows();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ExportToFile

        private RelayCommand _exportToFileCommand;

        /// <summary>
        /// Shows the export planograms window
        /// </summary>
        public RelayCommand ExportToFileCommand
        {
            get
            {
                if (_exportToFileCommand == null)
                {
                    _exportToFileCommand =
                        PlanogramExportHelper.CreateShowExportWindowCommand(
                        this.PlanAssignmentSelectedItems.Select(p => p.PlanogramInfo));
                    base.ViewModelCommands.Add(_exportToFileCommand);
                }
                return _exportToFileCommand;
            }
        }

        #endregion

        #region ImportFromFileCommand

        private RelayCommand _importFromFileCommand;

        /// <summary>
        /// Opens the import from file wizard.
        /// </summary>
        public RelayCommand ImportFromFileCommand
        {
            get
            {
                if (_importFromFileCommand == null)
                {
                    _importFromFileCommand = new RelayCommand(
                        p => ImportFromFileCommand_Executed(),
                        p => ImportFromFileCommand_CanExecute())
                    {
                        FriendlyName = Message.PlanAssignment_ImportFromFile,
                        FriendlyDescription = Message.PlanAssignment_ImportFromFile_Description,
                        Icon = ImageResources.PlanAssignment_ImportFromFile,
                        DisabledReason = Message.PlanAssignment_ImportFromFile_DisabledReason,
                    };
                    base.ViewModelCommands.Add(_importFromFileCommand);
                }
                return _importFromFileCommand;
            }
        }

        /// <summary>
        /// Returns true if either a Category or Location is selected.
        /// </summary>
        /// <returns></returns>
        private Boolean ImportFromFileCommand_CanExecute()
        {
            if(CurrentViewType == PlanAssignmentViewType.ViewCategory)
            {
                return SelectedProductGroup != null;
            }
            else
            {
                return SelectedLocation != null;
            }            
        }

        private void ImportFromFileCommand_Executed()
        {
            ImportViewModel viewModel = new ImportViewModel(
                _currentViewType == PlanAssignmentViewType.ViewCategory ? SelectedProductGroup : null,
                _currentViewType == PlanAssignmentViewType.ViewLocation ? SelectedLocation : null);
            CommonHelper.GetWindowService().ShowDialog<ImportOrganiser>(viewModel);
        }

        #endregion

        #region ExportDataCommand

        private RelayCommand _exportDataCommand;

        /// <summary>
        /// Exports the data currently displayed in the assignments grid.
        /// </summary>
        public RelayCommand ExportDataCommand
        {
            get
            {
                if (_exportDataCommand == null)
                {
                    _exportDataCommand = new RelayCommand(
                        p => ExportDataCommand_Executed(),
                        p => ExportDataCommand_CanExecute())
                    {
                        FriendlyName = Message.PlanAssignment_ExportData,
                        FriendlyDescription = Message.PlanAssignment_ExportData_Description,
                        Icon = ImageResources.PlanAssignment_ExportData,
                        DisabledReason = Message.PlanAssignment_ExportData_DisabledReason,
                    };
                    base.ViewModelCommands.Add(_exportDataCommand);
                }
                return _exportDataCommand;
            }
        }

        /// <summary>
        /// Returns true if either a Category or Location is selected.
        /// </summary>
        /// <returns></returns>
        private Boolean ExportDataCommand_CanExecute()
        {
            if (CurrentViewType == PlanAssignmentViewType.ViewCategory
                && SelectedProductGroup == null)
                return false;

            if (CurrentViewType == PlanAssignmentViewType.ViewLocation
                && SelectedLocation == null)
                return false;

            return true;
        }

        private void ExportDataCommand_Executed()
        {
            //executed by a handler in the code behind.
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the selected items collection changes
        /// </summary>
        private void PlanAssignmentSelectedItems_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // Update the current active planogram:
            // If the current row has no assigned plan, clear the active planogram setting
            // If multiple rows are selected, find the first selected item that has an assigned plan and
            // use that as the current active plan

            PlanAssignmentRowViewModel firstPlan =
                _planAssignmentSelectedItems.FirstOrDefault(x => !String.IsNullOrEmpty(x.PlanName));

            this.ActivePlanogramInfo =
                (firstPlan != null) ? firstPlan.PlanogramInfo : null;
        }

        /// <summary>
        ///     Whenever the selected location is changed.
        /// </summary>
        private void OnSelectedLocationChanged()
        {
            //  Check whether the cluster scheme selection is missing OR
            //      it is category specific.
            if (SelectedClusterScheme == null ||
                SelectedClusterScheme.ProductGroupId != null)
            {
                //  Locations must have a non category specific cluster scheme. Find the right one.
                ClusterSchemeInfoList allClusterSchemeInfos = ClusterSchemeInfoList.FetchByEntityId(App.ViewState.EntityId);

                //  Filter out any that is non generic. Order them by default and most recent. If there are none, jsut deselect the cluster scheme.
                List<ClusterSchemeInfo> genericClusterSchemeInfos =
                    allClusterSchemeInfos.Where(info => info.ProductGroupId == null)
                                         .OrderByDescending(info => info.IsDefault)
                                         .ThenByDescending(info => info.DateLastModified)
                                         .ToList();
                SelectedClusterScheme = genericClusterSchemeInfos.FirstOrDefault();
            }

            RefreshAvailablePlanAssignmentItemRows();
        }

        /// <summary>
        ///     Whenever the selected product group (category) is changed.
        /// </summary>
        private void OnSelectedProductGroupChanged()
        {
            //  Check whether the cluster scheme selection is missing OR
            //      it is specific to another category (or none).
            if (SelectedProductGroup != null)
            {
                Int32 productGroupId = SelectedProductGroup.Id;
                if (SelectedClusterScheme == null ||
                    SelectedClusterScheme.ProductGroupId != productGroupId)
                {
                    //  Categories must have a category specific cluster scheme for their category OR a generic one if missing. Find the right one.
                    ClusterSchemeInfoList allClusterSchemeInfos = ClusterSchemeInfoList.FetchByEntityId(App.ViewState.EntityId);

                    //  Filter out any that is not specific to the category or generic. Order them by category specific and most recent. If there are none, jsut deselect the cluster scheme.
                    List<ClusterSchemeInfo> categorySpecificClusterSchemeInfos =
                        allClusterSchemeInfos.Where(info => info.ProductGroupId == null || info.ProductGroupId == productGroupId)
                                             .OrderByDescending(info => info.ProductGroupId)
                                             .ThenByDescending(info => info.IsDefault)
                                             .ThenByDescending(info => info.DateLastModified)
                                             .ToList();
                    SelectedClusterScheme = categorySpecificClusterSchemeInfos.FirstOrDefault();
                }
            }

            RefreshAvailablePlanAssignmentItemRows();
        }

        #endregion Event Handlers

        #region Methods

        /// <summary>
        /// Refreshes the assignment rows according to
        /// the current view selection.
        /// </summary>
        private void RefreshAvailablePlanAssignmentItemRows()
        {
            // Display rows by category or locaiton
            if (_currentViewType == PlanAssignmentViewType.ViewCategory)
            {
                RefreshCategoryItemRows();
            }
            else
            {
                RefreshLocationItemRows();
            }
        }

        /// <summary>
        /// Add a row item for the "View by Product Group" datagrid
        /// </summary>
        /// <param name="planStoreItem">The store space info object</param>
        /// <param name="keyToExistingRow">The key to check</param>
        /// <param name="rowKeys">The list of existing row keys</param>
        /// <param name="newRows">The list of new rows added</param>
        /// <param name="planInfos">The list of planogram info objects</param>
        /// <param name="locInfo">The location info object to add</param>
        /// <param name="localAssignment">The new plan assignment record to check / add</param>
        private void AddLocationRowItem(
            PlanAssignmentStoreSpaceInfo planStoreItem,
            Dictionary<PlanAssignmentRowKey, PlanAssignmentRowViewModel> keyToExistingRow,
            List<PlanAssignmentRowKey> rowKeys,
            List<PlanAssignmentRowViewModel> newRows,
            PlanogramInfoList planInfos,
            LocationInfo locInfo ,
            LocationPlanAssignment localAssignment)
        {
            //Get the plan info.
            PlanogramInfo planInfo = (localAssignment != null) ? planInfos.FindById(localAssignment.PlanogramId) : null;

            //get the user infos.
            UserInfo assignedUser = (localAssignment != null) ? FindUserInfoById(localAssignment.AssignedByUserId) : null;
            UserInfo publishUser = (localAssignment != null) ? FindUserInfoById(localAssignment.PublishedByUserId) : null;

            //create the row key
            PlanAssignmentRowKey rowKey = new PlanAssignmentRowKey(planStoreItem, localAssignment, PlanAssignmentViewType.ViewCategory);
            rowKeys.Add(rowKey);

            //If the row, already exists then just update it, otherwise create a new one.
            PlanAssignmentRowViewModel existingRow;
            if (keyToExistingRow.TryGetValue(rowKey, out existingRow))
            {
                existingRow.UpdateRow(planStoreItem, localAssignment, planInfo, locInfo, _selectedProductGroup,
                    PlanAssignmentViewType.ViewCategory, assignedUser, publishUser);
            }
            else
            {
                newRows.Add(new PlanAssignmentRowViewModel(planStoreItem, localAssignment, planInfo, locInfo, _selectedProductGroup,
                        PlanAssignmentViewType.ViewCategory, assignedUser, publishUser));
            }
        }

        /// <summary>
        /// Populate the data grid with the locations and assigned plan details for the currently selected product group
        /// Add all the data to the master assignment items and then filter this.
        /// </summary>
        private void RefreshCategoryItemRows()
        {
            // Check if we are viewing by category and there is no category selected
            if (_selectedProductGroup == null)
            {
                FilterLocationAssignedItems();
                return;
            }

            Int32 productGroupId = _selectedProductGroup.Id;
            Int32? clusterSchemeId = (SelectedClusterScheme != null) ? (Int32?)SelectedClusterScheme.Id : null;
            PlanAssignmentStoreSpaceInfoList planStoreList = null;
            LocationPlanAssignmentList planAssignments = null;
            PlanogramInfoList planInfos = null;

            try
            {
                // Fetch the store space details
                planStoreList = PlanAssignmentStoreSpaceInfoList.FetchByEntityIdProductGroupId(App.ViewState.EntityId, productGroupId, clusterSchemeId);

                // Fetch existing store plan assignments
                planAssignments = LocationPlanAssignmentList.FetchByEntityIdProductGroupId(App.ViewState.EntityId, productGroupId);
                _masterPublishPlanList = planAssignments;

                //get planogram infos for all items
                List<Int16> storeIds = planStoreList.Select(p => p.LocationId).ToList();
                IEnumerable<Int32> planIds = planAssignments.Where(p => storeIds.Contains(p.LocationId)).Select(p => p.PlanogramId).Distinct();
                planInfos = PlanogramInfoList.FetchByIds(planIds);

                DisableCommands = false;
            }
            catch (Exception ex)
            {
                // Stop polling, otherwise we get 2 error messages
                StopPollingAsync();

                ExceptionHandler(ex);

                return;
            }

            // Get a list of rows to be created and update any existing ones.
            List<PlanAssignmentRowKey> rowKeys = new List<PlanAssignmentRowKey>();
            Dictionary<PlanAssignmentRowKey, PlanAssignmentRowViewModel> keyToExistingRow = _masterAssignmentItemRows.ToDictionary(r => r.RowKey);
            List<PlanAssignmentRowViewModel> newRows = new List<PlanAssignmentRowViewModel>();

            //Cycle through store space creating/updating rows as required.
            foreach (PlanAssignmentStoreSpaceInfo planStoreItem in planStoreList)
            {
                var localAssignmentList = planAssignments.Where(x => x.LocationId == planStoreItem.LocationId).ToList();

                //get the location info
                LocationInfo locInfo = FindLocationInfoById(planStoreItem.LocationId);
                if (locInfo == null) continue;

                if (localAssignmentList.Count == 0)
                {
                    LocationPlanAssignment localAssignment = planAssignments.FirstOrDefault(x => x.LocationId == planStoreItem.LocationId);
                    AddLocationRowItem(planStoreItem, keyToExistingRow, rowKeys, newRows, planInfos, locInfo, localAssignment);
                }
                else
                {
                    foreach (var localAssignment in localAssignmentList)
                    {
                        AddLocationRowItem(planStoreItem, keyToExistingRow, rowKeys, newRows, planInfos, locInfo, localAssignment);
                    }
                }
            }

            // Update the _masterAssignmentItemRows collection:
            // Remove any old rows
            _masterAssignmentItemRows.RemoveRange(_masterAssignmentItemRows.Where(r => !rowKeys.Contains(r.RowKey)).ToList());

            // Add the new ones
            _masterAssignmentItemRows.AddRange(newRows);

            // Apply the filtering
            FilterLocationAssignedItems();
        }

        /// <summary>
        /// Add a row item for the "View by Location" datagrid
        /// </summary>
        /// <param name="planStoreItem">The store space info object</param>
        /// <param name="keyToExistingRow">The key to check</param>
        /// <param name="rowKeys">The list of existing row keys</param>
        /// <param name="newRows">The list of new rows added</param>
        /// <param name="planInfos">The list of planogram info objects</param>
        /// <param name="prodGroupInfo">The product group info object to add</param>
        /// <param name="localAssignment">The new plan assignment record to check / add</param>
        private void AddProductGroupRowItem(
            PlanAssignmentStoreSpaceInfo planStoreItem,
            Dictionary<PlanAssignmentRowKey, PlanAssignmentRowViewModel> keyToExistingRow,
            List<PlanAssignmentRowKey> rowKeys,
            List<PlanAssignmentRowViewModel> newRows,
            PlanogramInfoList planInfos,
            ProductGroupInfo prodGroupInfo,
            LocationPlanAssignment localAssignment)
        {
            //get the plan info
            PlanogramInfo planInfo = (localAssignment != null) ? planInfos.FindById(localAssignment.PlanogramId) : null;

            //get the user infos.
            UserInfo assignedUser = (localAssignment != null) ? FindUserInfoById(localAssignment.AssignedByUserId) : null;
            UserInfo publishUser = (localAssignment != null) ? FindUserInfoById(localAssignment.PublishedByUserId) : null;

            //create the row key
            PlanAssignmentRowKey rowKey = new PlanAssignmentRowKey(planStoreItem, localAssignment, PlanAssignmentViewType.ViewLocation);
            rowKeys.Add(rowKey);

            //update the row if it already exists or create a new one.
            PlanAssignmentRowViewModel existingRow;
            if (keyToExistingRow.TryGetValue(rowKey, out existingRow))
            {
                existingRow.UpdateRow(planStoreItem, localAssignment, planInfo, _selectedLocation, prodGroupInfo,
                    PlanAssignmentViewType.ViewLocation, assignedUser, publishUser);
            }
            else
            {
                newRows.Add(new PlanAssignmentRowViewModel(planStoreItem, localAssignment, planInfo, _selectedLocation, prodGroupInfo,
                        PlanAssignmentViewType.ViewLocation, assignedUser, publishUser));
            }
        }


        /// <summary>
        /// Populate the data grid with the product categories assigned to the specified location
        /// </summary>
        private void RefreshLocationItemRows()
        {
            // Check if we are viewing by location and there is no location selected
            if (_selectedLocation == null)
            {
                FilterLocationAssignedItems();
                return;
            }

            Int16 locationId = _selectedLocation.Id;
            Int32? clusterSchemeId = SelectedClusterScheme != null ? (Int32?)SelectedClusterScheme.Id : null;
            Int32 entityId = App.ViewState.EntityId;


            if(App.IsUnitTesting)
            {
                //fetch and update immediately
                var values = FetchLocationItemRowData(locationId, clusterSchemeId, entityId);
                RefreshLocationItemRows(values.Item1, values.Item2, values.Item3);
            }
            else
            {

                //fetch in the background so that the ui does not hang when polling.
                Csla.Threading.BackgroundWorker worker = new Csla.Threading.BackgroundWorker();
                worker.DoWork +=
                    (s,e) =>
                    {
                        Object[] args = (Object[])e.Argument;
                        e.Result  = FetchLocationItemRowData((Int16)args[0], (Int32?)args[1], (Int32)args[2]);
                    };

                worker.RunWorkerCompleted +=
                    (s,e)=>
                    {
                        if (e.Error != null)
                        {
                            //if we have errored then stop polling
                            // otherwise it will just keep spamming.
                            StopPollingAsync();
                            ExceptionHandler(e.Error);
                        }
                        else
                        {
                            var values =
                            (Tuple<PlanAssignmentStoreSpaceInfoList, LocationPlanAssignmentList, PlanogramInfoList>)e.Result;
                            RefreshLocationItemRows(values.Item1, values.Item2, values.Item3);
                        }

                        //dispose of the worker.
                        ((Csla.Threading.BackgroundWorker)s).Dispose();
                    };

                worker.RunWorkerAsync(new Object[] { locationId, clusterSchemeId, entityId });
            }
            
        }

        

        /// <summary>
        /// Called to update the location rows.
        /// </summary>
        /// <param name="productGroupStoreList"></param>
        /// <param name="planAssignments"></param>
        /// <param name="planInfos"></param>
        private void RefreshLocationItemRows(
            PlanAssignmentStoreSpaceInfoList productGroupStoreList, 
            LocationPlanAssignmentList planAssignments, 
            PlanogramInfoList planInfos)
        {

            _masterPublishPlanList = planAssignments;
            DisableCommands = false;

            List<Int16> productGroupIdsList = productGroupStoreList.Select(p => p.LocationId).ToList();

            // Get a list of rows to be created and update any existing ones.
            List<PlanAssignmentRowKey> rowKeys = new List<PlanAssignmentRowKey>();
            Dictionary<PlanAssignmentRowKey, PlanAssignmentRowViewModel> keyToExistingRow = _masterAssignmentItemRows.ToDictionary(r => r.RowKey);
            List<PlanAssignmentRowViewModel> newRows = new List<PlanAssignmentRowViewModel>();

            foreach (PlanAssignmentStoreSpaceInfo planStoreSpaceItem in productGroupStoreList)
            {
                //if we are filtering to valid store space info
                // the skip this row if not required to save having to loop through checking tonnes of invalid data.
                if(this.IsFilteredToValidLocationSpace)
                {
                    Int32? storeSpaceBayCount = (planStoreSpaceItem.BayCount < 1 ? null : (Int32?)planStoreSpaceItem.BayCount);
                    if (!storeSpaceBayCount.HasValue) continue;

                    Double? storeSpaceBayTotalWidth = planStoreSpaceItem.BayTotalWidth.EqualTo(0) ? null : (Double?)planStoreSpaceItem.BayTotalWidth;
                    if (!storeSpaceBayTotalWidth.HasValue) continue;

                    Double? storeSpaceBayHeight = planStoreSpaceItem.BayHeight.EqualTo(0) ? null : (Double?)planStoreSpaceItem.BayHeight;
                    if (!storeSpaceBayHeight.HasValue) continue;
                }



                var allLocalAssignments = planAssignments.Where(x => x.ProductGroupId == planStoreSpaceItem.ProductGroupId).ToList();

                //get the product group info
                ProductGroupInfo matchingGroupInfo;
                if (!String.IsNullOrWhiteSpace(planStoreSpaceItem.ProductGroupCode) && !String.IsNullOrWhiteSpace(planStoreSpaceItem.ProductGroupName))
                {
                    matchingGroupInfo = ProductGroupInfo.NewProductGroupInfo(
                        planStoreSpaceItem.ProductGroupId, planStoreSpaceItem.ProductGroupCode, planStoreSpaceItem.ProductGroupName);
                }
                else
                {
                    matchingGroupInfo = FindProductGroupInfoById(planStoreSpaceItem.ProductGroupId);
                } 


                if (!allLocalAssignments.Any())
                {
                    LocationPlanAssignment localAssignment = planAssignments.FirstOrDefault(x => x.ProductGroupId == planStoreSpaceItem.ProductGroupId);
                    AddProductGroupRowItem(planStoreSpaceItem, keyToExistingRow, rowKeys, newRows, planInfos, matchingGroupInfo, localAssignment);
                }
                else
                {
                    foreach (var localAssignment in allLocalAssignments)
                    {
                        AddProductGroupRowItem(planStoreSpaceItem, keyToExistingRow, rowKeys, newRows, planInfos, matchingGroupInfo, localAssignment);
                    }
                }
            }

            // Update the _masterAssignmentItemRows collection:
            // Remove any old rows
            _masterAssignmentItemRows.RemoveRange(_masterAssignmentItemRows.Where(r => !rowKeys.Contains(r.RowKey)).ToList());

            // Add the new ones
            _masterAssignmentItemRows.AddRange(newRows);

            FilterLocationAssignedItems();
        }


        /// <summary>
        /// Validates a store space published item row to see if it should be displayed or not
        /// </summary>
        private Boolean RowPassesFilter(PlanAssignmentRowViewModel row,
            PlanAssignmentFilterType filterType, Boolean filterToValidLocSpace)
        {

            //return false if the row does not have valid loc space
            if (filterToValidLocSpace && !row.HasValidLocationSpace()) return false;


            Boolean isAssigned = !String.IsNullOrEmpty(row.PlanName);

            switch (filterType)
            {
                case PlanAssignmentFilterType.ShowAll:
                    //do nothing.
                    break;

                case PlanAssignmentFilterType.ShowAssigned:
                    // only show items which have had a plan assigned to them.
                    if (!isAssigned) return false;
                    break;

                case PlanAssignmentFilterType.ShowUnassigned:
                    //only show items which are not assigned
                    if (isAssigned) return false;
                    break;

                case PlanAssignmentFilterType.ShowCompatible:
                    //only show items which are assigned and the store space info matches the assigned plan
                    if (!isAssigned) return false;
                    if (row.PlanBayCount == null || row.PlanTotalWidth == null || row.PlanHeight == null) return false;
                    if (row.PlanBayCount != row.StoreSpaceBayCount) return false;
                    if (!row.PlanTotalWidth.Value.EqualTo(Convert.ToSingle(row.StoreSpaceBayTotalWidth))) return false;
                    if (!row.PlanHeight.Value.EqualTo(Convert.ToSingle(row.StoreSpaceBayHeight))) return false;
                    break;

                case PlanAssignmentFilterType.ShowIncompatible:
                    //only show items which are assigned and the store space info does not match the assigned plan
                    if (!isAssigned) return false;
                    if (row.PlanBayCount == null || row.PlanTotalWidth == null || row.PlanHeight == null) return false;

                    if ((row.PlanBayCount == row.StoreSpaceBayCount)
                        && row.PlanTotalWidth.Value.EqualTo(Convert.ToSingle(row.StoreSpaceBayTotalWidth))
                        && row.PlanHeight.Value.EqualTo(Convert.ToSingle(row.StoreSpaceBayHeight)))
                    {
                        return false;
                    }
                    break;

                case PlanAssignmentFilterType.ShowMultiplePlans:
                    // Only show rows that have multiple plans assigned
                    if (_currentViewType == PlanAssignmentViewType.ViewCategory)
                    {
                        // Check if there are other rows that share the same location
                        Int32 matchingRowCount = _masterAssignmentItemRows.Count(x => x.Location.Id == row.Location.Id);
                        if (matchingRowCount < 2) return false;
                    }
                    else
                    {
                        // Check if there are other rows that shae the same product group.
                        if (row.ProductGroup == null) return false;

                        if (!_masterAssignmentItemRows.Any(r => r.ProductGroup != null && r.ProductGroup.Id == row.ProductGroup.Id && r != row))
                            return false;


                        //matchingRowCount = _masterAssignmentItemRows.Count(x => x.ProductGroup != null && x.ProductGroup.Id == row.ProductGroup.Id);
                    }
                    break;
            }

            

            return true;
        }

        /// <summary>
        /// Filter the master list of store space assigned items according to the filter checkboxes
        /// on the side bar control panel.
        /// </summary>
        private void FilterLocationAssignedItems()
        {
            List<PlanAssignmentRowViewModel> tmpFilteredItems = new List<PlanAssignmentRowViewModel>();

            foreach (PlanAssignmentRowViewModel assignedItem in _masterAssignmentItemRows)
            {
                // Add store space published item to the filtered list if it is to be
                // visible to the user.
                if (RowPassesFilter(assignedItem, this.SelectedFilterType, this.IsFilteredToValidLocationSpace))
                {
                    tmpFilteredItems.Add(assignedItem);
                }
            }

            _filteredAssignmentItemRows.RaiseListChangedEvents = true;

            //remove old rows.
            List<PlanAssignmentRowViewModel> oldRows = _filteredAssignmentItemRows.Where(r => !tmpFilteredItems.Contains(r)).ToList();
            if (oldRows.Count > 0) _filteredAssignmentItemRows.RemoveRange(oldRows);


            //add any new ones
            List<PlanAssignmentRowViewModel> newRows = tmpFilteredItems.Where(r => !_filteredAssignmentItemRows.Contains(r)).ToList();
            if (newRows.Count > 0) _filteredAssignmentItemRows.AddRange(newRows);


            // Update all counts
            List<PlanAssignmentRowViewModel> countItems =
                (this.IsFilteredToValidLocationSpace) ?
                _masterAssignmentItemRows.Where(r => r.HasValidLocationSpace()).ToList()
                : _masterAssignmentItemRows.ToList();

            this.ShowAllCount = countItems.Count(r => RowPassesFilter(r, PlanAssignmentFilterType.ShowAll, this.IsFilteredToValidLocationSpace));
            this.AssignedCount = countItems.Count(r => RowPassesFilter(r, PlanAssignmentFilterType.ShowAssigned, this.IsFilteredToValidLocationSpace));
            this.UnassignedCount = countItems.Count(r => RowPassesFilter(r, PlanAssignmentFilterType.ShowUnassigned, this.IsFilteredToValidLocationSpace));
            this.CompatibleCount = countItems.Count(r => RowPassesFilter(r, PlanAssignmentFilterType.ShowCompatible, this.IsFilteredToValidLocationSpace));
            this.IncompatibleCount = countItems.Count(r => RowPassesFilter(r, PlanAssignmentFilterType.ShowIncompatible, this.IsFilteredToValidLocationSpace));
            this.MultiplePlanCount = countItems.Count(r => RowPassesFilter(r, PlanAssignmentFilterType.ShowMultiplePlans, this.IsFilteredToValidLocationSpace));
        }

        /// <summary>
        /// Utility method to remove a planogram assignment
        /// </summary>
        /// <param name="planRowItem">The PlanAssignmentRowViewModel item to remove</param>
        /// <returns>True if the assignment was removed, false otherwise</returns>
        private Boolean RemovePlanogramItem(PlanAssignmentRowViewModel planRowItem)
        {
            if (planRowItem == null) return false;

            // check if the master list is null
            if (_masterPublishPlanList == null)
            {
                // it is, so return a failure
                return false;
            }

            // Get the item in the master plan list
            // First check which view the user is in (i.e. category or location)
            if (_currentViewType == PlanAssignmentViewType.ViewCategory)
            {
                // Check for a matching location id and selected planogram id
                var multiList = _masterPublishPlanList.Where(x => x.LocationId == planRowItem.LocationId && x.PlanogramId == planRowItem.PlanogramInfo.Id).ToList();
                //var oneList = _masterPublishPlanList.FirstOrDefault(x => x.LocationId == planRowItem.LocationId);

                //masterMatch = _masterPublishPlanList.SingleOrDefault(x => x.LocationId == planRowItem.LocationId) as LocationPlanAssignment;

                foreach (var masterMatch in multiList)
                {
                    // We did not find a match so skip this iteration
                    if (masterMatch == null)
                    {
                        return false;
                    }

                    // Remove the item from the master plan list
                    _masterPublishPlanList.Remove(masterMatch);

                    // Clear the display info item
                    planRowItem.ClearPlanogram();
                }
            }
            else
            {
                //var masterMatch = _masterPublishPlanList.SingleOrDefault(x => x.ProductGroupId == planRowItem.ProductGroupId) as LocationPlanAssignment;

                // Check for a matching product group id and selected planogram id
                var multiList =
                    _masterPublishPlanList.Where(x => x.ProductGroupId == planRowItem.ProductGroupId && x.PlanogramId == planRowItem.PlanogramInfo.Id).ToList();

                foreach (var masterMatch in multiList)
                {
                    // Remove the item from the master plan list
                    _masterPublishPlanList.Remove(masterMatch);

                    // Clear the display info item
                    planRowItem.ClearPlanogram();
                }
            }

            return true;
        }

        // The old version of the planogram selector that uses the common WPF selector window to select a single plan.
        // Retained in the eventuality that the requirements change.
        private void OldPlanSelector()
        {
            //show the selector dialog.
            PlanogramHierarchyViewModel hierarchyView = new PlanogramHierarchyViewModel();
            hierarchyView.FetchForCurrentEntity();

            PlanogramGroup selectedPlanogramGroup = null;
            if (SelectedProductGroup != null)
            {
                Int32 productGroupId = SelectedProductGroup.Id;
                selectedPlanogramGroup = hierarchyView.Model.EnumerateAllGroups().FirstOrDefault(o => o.ProductGroupId == productGroupId);
            }
            PlanogramSelectorViewModel winView = new PlanogramSelectorViewModel(App.ViewState.CustomColumnLayoutFolderPath, App.ViewState.EntityId, hierarchyView, selectedPlanogramGroup);
            CommonHelper.GetWindowService().ShowDialog<PlanogramSelectorWindow>(winView);
        }

        private void DoPlanAssignment()
        {
            DoPlanAssignment(null);
        }

        /// <summary>
        /// Perform the plan selection and plan -> location assignment.
        /// </summary>
        internal void DoPlanAssignment(ObservableCollection<PlanogramInfoView> unitTestSelectedPlans)
        {
            PlanogramHierarchyViewModel hierarchyView = new PlanogramHierarchyViewModel();
            hierarchyView.FetchForCurrentEntity();

            PlanogramGroup selectedPlanogramGroup = null;

            if (_currentViewType == PlanAssignmentViewType.ViewCategory)
            {
                if (SelectedProductGroup != null)
                {
                    Int32 productGroupId = SelectedProductGroup.Id;
                    selectedPlanogramGroup = hierarchyView.Model.EnumerateAllGroups().FirstOrDefault(x => x.ProductGroupId == productGroupId);
                }
            }
            else
            {
                // Set the selected group to be that of the currently selected row
                // Get the product group of the current selected row
                var selectedCategory = _planAssignmentSelectedItems.FirstOrDefault().ProductGroup;
                selectedPlanogramGroup = 
                    hierarchyView.Model.EnumerateAllGroups().FirstOrDefault(x => selectedCategory != null && x.ProductGroupId == selectedCategory.Id);
            }

            // Get a list of all the locations / product groups assignments that match the currently selected location / product group
            // i.e. display all the assignments rather than just those in the selected rows

            IEnumerable<PlanAssignmentRowViewModel> allPlansAssigned = null;

            // Get a unique list of all the plans in the user selected rows
            var distinctPlansList = _planAssignmentSelectedItems.Select(x => x.PlanogramInfo).Where(y => y != null).Distinct().ToList();
            if (distinctPlansList.Any())
            {
                allPlansAssigned =
                    _masterAssignmentItemRows.Where(x => x.PlanogramInfo != null && x.PlanogramInfo.Id == distinctPlansList.First().Id).Distinct().ToList();
            }

            SelectPlanogramViewModel selectPlansVm =
                new SelectPlanogramViewModel(App.ViewState.CustomColumnLayoutFolderPath,
                                             App.ViewState.EntityId, hierarchyView, selectedPlanogramGroup, allPlansAssigned);

            // Check if we running in an application context, rather than unit testing
            if (!App.IsUnitTesting)
            {
                CommonHelper.GetWindowService().ShowDialog<SelectPlanogramOrganiser>(selectPlansVm);

                if (selectPlansVm.DialogResult != true) return;
            }

            // Check if there were any plans injected from a unit test or
            // if the user selected some plans and clicked OK
            ObservableCollection<PlanogramInfo> actualSelectedPlans;
            if (unitTestSelectedPlans == null || !unitTestSelectedPlans.Any())
            {
                actualSelectedPlans = new ObservableCollection<PlanogramInfo>(selectPlansVm.AssignedPlans.Select(v=> v.Model));
            }
            else
            {
                actualSelectedPlans = new ObservableCollection<PlanogramInfo>(unitTestSelectedPlans.Select(v => v.Model));
            }

            if (actualSelectedPlans == null || !actualSelectedPlans.Any()) return;

            // Get the current local date in UTC
            DateTime currentLocalDate = DateTime.UtcNow;

            // Get the current logged on user
            Int32 domainUserId = (DomainPrincipal.CurrentUserId == null ? -1 : (Int32)DomainPrincipal.CurrentUserId);
            User currentUser = User.FetchById(domainUserId);
            String userFriendlyName = (currentUser == null ? "" : currentUser.FirstName + " " + currentUser.LastName);

            base.ShowWaitCursor(true);

            // Aggregate all the warnings so that only one pop-up warning message is
            // displayed, rather than one for each returned plan

            // Counts for plans with a bay width / bay count mismatch, product category mismatch and the
            // number of locations / product groups that have multiple plans assigned
            Int32 wrongSizePlanCount = 0;
            Int32 wrongProductGroupCount = 0;
            Int32 multiplePlanCount = 0;

            // Calculate the number of locations / product group items that have plans that do not match the location space details
            foreach (var rowItem in _planAssignmentSelectedItems)
            {
                foreach (var selectedPlan in actualSelectedPlans)
                {
                    // Validate plan selected against the plan bay count and plan width
                    Int32? selectedPlanBayCount = selectedPlan.MetaBayCount;
                    Double selectedPlanWidth = selectedPlan.Width;
                    Double selectedPlanHeight = selectedPlan.Height;

                    // For all of the selected locations, check the location bay count, width and height against the
                    // selected planogram bay count, width and height
                    Int32 incorrectCount = _planAssignmentSelectedItems.
                        Where(x =>
                            x.StoreSpaceBayCount != selectedPlanBayCount ||
                            (x.StoreSpaceBayTotalWidth.HasValue ?
                            !x.StoreSpaceBayTotalWidth.Value.EqualTo(selectedPlanWidth)
                            : false) ||
                            (x.StoreSpaceBayHeight.HasValue ?
                            !x.StoreSpaceBayHeight.Value.EqualTo(selectedPlanHeight)
                            : false)
                            ).
                        Count();

                    if (incorrectCount > 0)
                    {
                        // There were some bay count or width mismatches so tell the user
                        wrongSizePlanCount++;
                        break;
                    }
                }
            }

            // Check for a category mis-match and prompt user if necessary.
            foreach (var selectedPlan in actualSelectedPlans)
            {
                // Check for a category mis-match and prompt user if necessary.
                Boolean isCategoryMisMatch = _planAssignmentSelectedItems.
                    Any(i => !i.ProductGroup.Code.Equals(selectedPlan.CategoryCode, StringComparison.InvariantCultureIgnoreCase));

                // Get the number of stores that have a category mismatch
                if (isCategoryMisMatch)
                {
                    wrongProductGroupCount++;
                }
            }

            // Calculate the number of locations / product group items that have multiple plans assigned
            Int32 plansAssignedCount = 0;

            if (_currentViewType == PlanAssignmentViewType.ViewCategory)
            {
                // Create a distinct list of all the locations in the selected rows
                // as the user can select multiple rows
                var allLocationsList = _planAssignmentSelectedItems.OrderBy(x => x.Location.Id).Select(x => x.Location.Id).Distinct().ToList();

                foreach (var selectedLocation in allLocationsList)
                {
                    // Check if there are already multiple plans
                    plansAssignedCount = _masterAssignmentItemRows.Where(x => x.PlanogramInfo != null && x.Location.Id == selectedLocation).Count();

                    // Also check if the user has chosen multiple plans
                    if (plansAssignedCount > 1 || actualSelectedPlans.Count() > 1)
                    {
                        multiplePlanCount++;
                    }
                }
            }
            else
            {
                // For assignment by location life is easier as only one row can be selected at a time.
                if (actualSelectedPlans.Count > 1)
                {
                    multiplePlanCount++;
                }
            }

            bool isActualWarning = false;
            String affectedString = (_currentViewType == PlanAssignmentViewType.ViewCategory ? 
                                    Message.PlanAssignment_PlanBayWarning_LocationType : Message.PlanAssignment_PlanBayWarning_ProductGroupType);
            _messageDisplayText = String.Format(Message.PlanAssignment_PlanBayWarning_AffectedItems,
                                                    _planAssignmentSelectedItems.Count(), affectedString);

            if (wrongSizePlanCount > 0)
            {
                // This is a warning
                isActualWarning = true;
                _messageDisplayText += System.Environment.NewLine;
                _messageDisplayText += String.Format(Message.PlanAssignment_PlanBayWarning_SizeMismatch, wrongSizePlanCount, affectedString);
            }

            if (wrongProductGroupCount > 0)
            {
                // This is a warning
                isActualWarning = true;
                _messageDisplayText += System.Environment.NewLine;
                _messageDisplayText += String.Format(Message.PlanAssignment_CategoryMismatchWarning_Content, wrongProductGroupCount);
            }

            if (multiplePlanCount > 0)
            {
                // This is informational
                _messageDisplayText += System.Environment.NewLine;
                _messageDisplayText += String.Format(Message.PlanAssignment_MultiplePlanogramsAssigned, multiplePlanCount, affectedString);
            }

            if (!String.IsNullOrEmpty(_messageDisplayText))
            {
                _messageDisplayText += System.Environment.NewLine + System.Environment.NewLine;
                _messageDisplayText += Message.PlanAssignment_ContinueMessage;
            }

            if (!String.IsNullOrEmpty(_messageDisplayText))
            {
                _messageDisplayTitle = (isActualWarning ? Message.PlanAssignment_Warning_Title : Message.PlanAssignment_Information_Title);
                _messageDisplayHeader = (isActualWarning ? Message.PlanAssignment_PlanBayWarning_Header : Message.PlanAssignment_Information_Header);

                if (!App.IsUnitTesting)
                {
                    base.ShowWaitCursor(false);
                    Boolean dialogResult =
                        LocalHelper.ContinueWithActionPrompt(_messageDisplayTitle, _messageDisplayHeader, _messageDisplayText, isActualWarning);

                    if (!dialogResult)
                    {
                        DoPlanAssignment();
                        return;
                    }
                }
            }

            var selectedPlanIds = actualSelectedPlans.OrderBy(x => x.Id).Select(y => y.Id).ToList();
            foreach (var selectedPlan in actualSelectedPlans)
            {
                // Now we have a plan selected update the master list of locations / plans
                foreach (var selectedRowItem in _planAssignmentSelectedItems)
                {
                    // Check if an item is already publish for the location
                    // As there is only 1 plan / location we need to replace the exisiting plan if there is one or
                    // add a new plan when there is no existing assignment for the location
                    IEnumerable<LocationPlanAssignment> itemMatches = null;

                    Int16 locationMatchId;
                    Int32 productGroupMatchId;
                    if (_currentViewType == PlanAssignmentViewType.ViewCategory)
                    {
                        // Check if the plan assigned to the current selected row is not present in the list of selected plans
                        if (selectedRowItem.PlanogramInfo != null && !selectedPlanIds.Contains(selectedRowItem.PlanogramInfo.Id))
                        {
                            // It is not, so un-assign the plan
                            RemovePlanogramItem(selectedRowItem);
                        }

                        // Get a list of plan assignments that match the current selected row location and plan
                        itemMatches = _masterPublishPlanList.Where(x => x.LocationId == selectedRowItem.LocationId &&
                                                                    selectedRowItem.PlanogramInfo != null &&
                                                                    x.PlanogramId == selectedPlan.Id).ToList();
                        var itemMatchesString = String.Join(", ", itemMatches.Select(x => x.PlanogramId));

                        locationMatchId = selectedRowItem.LocationId;
                        productGroupMatchId = _selectedProductGroup.Id;
                    }
                    else
                    {
                        // Check if the plan assigned to the current selected row is not present in the list of selected plans
                        if (selectedRowItem.PlanogramInfo != null && !selectedPlanIds.Contains(selectedRowItem.PlanogramInfo.Id))
                        {
                            // It is not, so un-assign the plan
                            RemovePlanogramItem(selectedRowItem);
                        }

                        itemMatches = _masterPublishPlanList.Where(x => x.ProductGroupId == selectedRowItem.ProductGroupId &&
                                                                    selectedRowItem.PlanogramInfo != null &&
                                                                    x.PlanogramId == selectedPlan.Id).ToList();
                        var itemMatchesString = String.Join(", ", itemMatches.Select(x => x.PlanogramId));

                        locationMatchId = _selectedLocation.Id;
                        productGroupMatchId = selectedRowItem.ProductGroupId;
                    }

                    if (!itemMatches.Any())
                    {
                        // This is a new plan assignment for this location so create a new item
                        LocationPlanAssignment newPlanAssign = LocationPlanAssignment.NewLocationPlanAssignment(
                                productGroupMatchId, selectedPlan.Id,
                                locationMatchId, currentUser, currentLocalDate);

                        _masterPublishPlanList.Add(newPlanAssign);
                    }
                    else
                    {
                        //There is an existing assignment so update the item (if it is not the same plan)
                        LocationPlanAssignment firstItem = itemMatches.FirstOrDefault();
                        if (firstItem != null && firstItem.PlanogramId != selectedPlan.Id)
                        {
                            firstItem.PlanogramId = selectedPlan.Id;
                            firstItem.AssignedByUserId = currentUser.Id;
                            //firstItem.DateAssigned = currentLocalDate;
                            firstItem.SetAssignedDate(currentLocalDate);
                            //firstItem.DatePublished = null;
                            firstItem.SetPublishedDate(null);
                            firstItem.PublishedByUserId = null;
                        }
                    }

                    // Update the display with the new plan selection
                    selectedRowItem.SetPlanogram(selectedPlan, currentLocalDate, userFriendlyName);
                }
            }

            base.ShowWaitCursor(false);

            //commit changes
            Save();
        }

        /// <summary>
        /// Perform any save and refresh operations that are needed.
        /// </summary>
        private void Save()
        {
            if (_masterPublishPlanList == null) return;

            base.ShowWaitCursor(true);

            try
            {
                _masterPublishPlanList.Save();
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex);
                Exception rootException = ex.GetBaseException();

                //otherwise just show the user an error has occurred.
                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(Message.Screen_PlanAssignment, OperationType.Save);

                base.ShowWaitCursor(true);
            }

            // Refresh the display by re-fetching the plan assignments for the current product group
            _masterAssignmentItemRows.Clear();
            RefreshAvailablePlanAssignmentItemRows();

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Utility method to switch between view by category and location
        /// </summary>
        private void ChangeViewType(PlanAssignmentViewType newViewType)
        {
            // If the new view type is the same as the current type, don't do anything
            if (newViewType == _currentViewType) return;

            //reget the settings so that we are fully up to date.
            UserSystemSetting currentSettings = App.GetSystemSettings();

            // Update the user's settings
            currentSettings.PlanAssignmentViewBy = newViewType;

            // Clear the master lists - otherwise we can end up displaying Category related items when "View By Location" is chosen
            if (_masterPublishPlanList != null) _masterPublishPlanList.Clear();

            base.ShowWaitCursor(true);

            _masterAssignmentItemRows.Clear();
            FilterLocationAssignedItems();

            // So we have a different view type selected, so switch the views and
            // change the data grid columns accordingly
            CurrentViewType = newViewType;

            RefreshAvailablePlanAssignmentItemRows();

            //save the change
            try
            {
                currentSettings.Save();
            }
            catch (DataPortalException ex)
            {
                //no point crying over this, just record it and move on.
                CommonHelper.RecordException(ex);
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Saved the date information
        /// </summary>
        /// <param name="dateCommunicated"></param>
        /// <param name="dateLive"></param>
        internal void UpdateDates(DateTime? dateCommunicated, DateTime? dateLive)
        {
            // Loop through all the selected fields and create a record for items that are selected
            // and have a plan assignment

            foreach (PlanAssignmentRowViewModel selItem in _planAssignmentSelectedItems)
            {
                // Skip this row if there is no plan assigned to it
                if (selItem.LocationPlanAssignment == null) continue;

                //selItem.DateCommunicated = dateCommunicated;
                //selItem.DateLive = dateLive;

                // locate the plan assignment item in the master list
                LocationPlanAssignment matchItem = _masterPublishPlanList.Where(x => x.Id == selItem.LocationPlanAssignment.Id).SingleOrDefault();
                if (matchItem == null) continue;

                //matchItem.DateCommunicated = dateCommunicated;
                //matchItem.DateLive = dateLive;
                matchItem.SetCommunicatedDate(dateCommunicated);
                matchItem.SetLiveDate(dateLive);
            }

            Save();
        }

        /// <summary>
        /// Common exception handler to display a different message for database timeouts
        /// </summary>
        /// <param name="localException">The reported exception</param>
        private void ExceptionHandler(Exception localException)
        {
            base.ShowWaitCursor(false);

            // Add exception handling
            CommonHelper.RecordException(localException);

            // Check if there is a timeout localexcpetionception - usually caused by synch running at the same time
            if (localException.Message.Contains("timeout exception"))
            {
                CommonHelper.GetWindowService().ShowErrorMessage(Message.PlanAssignment_WarningHeader, Message.PlanAssignment_TimeoutWarning);
            }
            else
            {
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(Message.PlanAssignment_LoadErrorByProductGroup, OperationType.Open);
            }

            DisableCommands = true;
        }

        #region Data Helpers

        /// <summary>
        /// Returns the userinfo for the given id or null
        /// </summary>
        private UserInfo FindUserInfoById(Int32? userId)
        {
            if (!userId.HasValue) return null;

            if (_userInfoListView.Model == null)
            {
                _userInfoListView.FetchAll();
            }

            UserInfo info = _userInfoListView.Model.FindById(userId.Value);
            if (info == null)
            {
                //if the key does not exist its likely that the dictionary is out of date
                // so just refresh and try again.
                _userInfoListView.FetchAll();
                info = _userInfoListView.Model.FindById(userId.Value);
            }
            return info;
        }

        /// <summary>
        /// Returns the LocationInfo for the given id or null
        /// </summary>
        private LocationInfo FindLocationInfoById(Int16 locationId)
        {
            if (_locationInfoListView.Model == null)
            {
                _locationInfoListView.FetchAllForEntity();
            }

            LocationInfo info = _locationInfoListView.Model.FindById(locationId);
            if (info == null)
            {
                //if the key does not exist its likely that the dictionary is out of date
                // so just refresh and try again.
                _locationInfoListView.FetchAllForEntity();
                info = _locationInfoListView.Model.FindById(locationId);
            }
            return info;
        }

        /// <summary>
        /// Returns the ProductGroupInfo for the given id or null
        /// </summary>
        private ProductGroupInfo FindProductGroupInfoById(Int32 productGroupId)
        {
            ProductGroupInfo info = null;

            if (_productHierarchyView.Model == null
                || _productGroupInfosById == null
                || !_productGroupInfosById.TryGetValue(productGroupId, out info))
            {
                //refresh data
                _productHierarchyView.FetchMerchandisingHierarchyForCurrentEntity();
                _productHierarchyView.Model.FetchAllGroups();

                _productGroupInfosById = _productHierarchyView.Model.EnumerateAllGroups()
                    .ToDictionary(g => g.Id, g => ProductGroupInfo.NewProductGroupInfo(g));

                //try again
                _productGroupInfosById.TryGetValue(productGroupId, out info);
            }

            return info;
        }

        /// <summary>
        /// Fetches and returns all data required to create rows for the location view.
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="clusterSchemeId"></param>
        /// <param name="entityId"></param>
        /// <returns></returns>
        private static Tuple<PlanAssignmentStoreSpaceInfoList, LocationPlanAssignmentList, PlanogramInfoList>
            FetchLocationItemRowData(Int16 locationId, Int32? clusterSchemeId, Int32 entityId)
        {
            // Fetch the store space details for each product group
            PlanAssignmentStoreSpaceInfoList productGroupStoreList =
                PlanAssignmentStoreSpaceInfoList.FetchByEntityIdLocationId(entityId, locationId, clusterSchemeId);

            // Fetch the plan assignments for the given store for each product group
            LocationPlanAssignmentList planAssignments = LocationPlanAssignmentList.FetchByEntityIdLocationId(entityId, locationId);

            //get planogram infos for all items
            PlanogramInfoList planInfos = PlanogramInfoList.FetchByIds(planAssignments.Select(p => p.PlanogramId).Distinct());


            return new Tuple<PlanAssignmentStoreSpaceInfoList, LocationPlanAssignmentList, PlanogramInfoList>(
                productGroupStoreList, planAssignments, planInfos);
        }

        #endregion

        private void SetupDebug()
        {
            // This code is used for debugging to automatically select a product group and location
            SelectedProductGroup = ProductGroupInfo.FetchById(14);  // Lay006 Rice
            var locList = LocationInfoList.FetchByEntityIdLocationCodes(App.ViewState.EntityId, new List<String>() { "7" });
            SelectedLocation = locList.Where(x => x.Id == 3).FirstOrDefault();               // Austin Towers :)
        }

        #endregion

        #region Polling

        private Boolean _isPolling;
        private Csla.Threading.BackgroundWorker _worker;

        internal void StartPolling()
        {
            if (!IsPolling && !App.IsUnitTesting)
            {
                IsPolling = true;
                DisableCommands = false;

                ExecutePollingMethod();
            }
        }

        /// <summary>
        /// Stops any ongoing polling operation
        /// asynchronously.
        /// </summary>
        public void StopPollingAsync()
        {
            _isPolling = false;
        }

        private void ExecutePollingMethod()
        {
            Debug.WriteLine(System.DateTime.Now + " : Polling - " + this._currentViewType);

            //Save the old selection
            List<PlanAssignmentRowViewModel> oldSelection = _planAssignmentSelectedItems.ToList();

            RefreshAvailablePlanAssignmentItemRows();

            // Start waiting again
            BeginPollingWait();
        }

        private void BeginPollingWait()
        {
            if (_worker == null)
            {
                _worker = new Csla.Threading.BackgroundWorker();
                _worker.DoWork += PollingWorker_DoWork;
                _worker.RunWorkerCompleted += PollingWorker_RunWorkerCompleted;
                _worker.RunWorkerAsync();
            }
            else
            {
                if (!_worker.IsBusy)
                {
                    _worker.DoWork += PollingWorker_DoWork;
                    _worker.RunWorkerCompleted += PollingWorker_RunWorkerCompleted;
                    _worker.RunWorkerAsync();
                }
            }
        }

        private void PollingWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            Thread.Sleep(Galleria.Ccm.Common.Wpf.Constants.PollingPeriod);
        }

        private void PollingWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            Csla.Threading.BackgroundWorker worker = (Csla.Threading.BackgroundWorker)sender;
            worker.DoWork -= PollingWorker_DoWork;
            worker.RunWorkerCompleted -= PollingWorker_RunWorkerCompleted;

            if (IsPolling)
            {
                ExecutePollingMethod();
            }
            else
            {
                _worker = null;
            }
        }

        #endregion Polling

        #region IDisposable members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _planAssignmentSelectedItems.CollectionChanged -= PlanAssignmentSelectedItems_CollectionChanged;

                    this.AttachedControl = null;

                    StopPollingAsync();
                }
                base.IsDisposed = true;
            }
        }

        #endregion IDisposable members
    }

    public enum PlanAssignmentFilterType
    {
        ShowAll,
        ShowAssigned,
        ShowUnassigned,
        ShowCompatible,
        ShowIncompatible,
        ShowMultiplePlans,
    }
}
