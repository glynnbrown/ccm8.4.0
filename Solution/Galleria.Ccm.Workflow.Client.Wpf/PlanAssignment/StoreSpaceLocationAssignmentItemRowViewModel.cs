﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//	Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    public class StoreSpaceLocationAssignmentItemRowViewModel : ViewModelObject
    {
        #region fields

        private PlanAssignmentViewModel _parent;
        private LocationSpace _locationSpace;
        private LocationPlanAssignment _locationPlanAssignment;
        //private LocationPlanAssignmentLocation _locationPlanAssignmentLocation;
        private Location _location;
        private PlanogramInfo _planogramInfo;
        private ProductGroupInfo _productGroupInfo;

        #endregion

        #region properties

        public LocationSpace LocationSpace
        {
            get { return _locationSpace; }
        }

        public String CategoryCode
        {
            get
            {
                if (_productGroupInfo != null)
                {
                    return _productGroupInfo.Code;
                }
                return "";
            }
        }

        public String CategoryName
        {
            get
            {
                if (_productGroupInfo != null)
                {
                    return _productGroupInfo.Name;
                }
                return "";
            }
        }

        public String LocationCode
        {
            get
            {
                if (_location != null)
                {
                    return _location.Code;
                }
                return "";
            }
        }

        public String LocationName
        {
            get
            {
                if (_location != null)
                {
                    return _location.Name;
                }
                return "";
            }
        }

        public String PlanName
        {
            get
            {
                if (_planogramInfo != null)
                {
                    return _planogramInfo.Name;
                }
                return "";
            }
        }

        public Int32? PlanBayCount
        {
            get
            {
                if (_planogramInfo != null)
                {
                    return _planogramInfo.MetaBayCount;
                }
                return 0;
            }
        }

        public Single PlanTotalWidth
        {
            get
            {
                if (_planogramInfo != null)
                {
                    return _planogramInfo.Width;
                }
                return 0;
            }
        }

        public Location Location
        {
            get
            {
                return _location;
            }
        }

        public LocationPlanAssignment LocationPlanAssignment
        {
            get { return _locationPlanAssignment; }
            set
            {
                _locationPlanAssignment = value;
            }
        }

        //public LocationPlanAssignmentLocation LocationPlanAssignmentLocation
        //{
        //    get { return _locationPlanAssignmentLocation; }
        //    set
        //    {
        //        _locationPlanAssignmentLocation = value;
        //    }
        //}

        public PlanogramInfo PlanogramInfo
        {
            get { return _planogramInfo; }
            set
            {
                _planogramInfo = value;
                OnPropertyChanged(PlanNameProperty);
                OnPropertyChanged(PlanBayCountProperty);
                OnPropertyChanged(PlanTotalWidthProperty);
            }
        }

        #endregion

        #region binding property paths

        public static readonly PropertyPath CategoryCodeProperty = WpfHelper.GetPropertyPath<StoreSpaceLocationAssignmentItemRowViewModel>(p => p.CategoryCode);
        public static readonly PropertyPath CategoryNameProperty = WpfHelper.GetPropertyPath<StoreSpaceLocationAssignmentItemRowViewModel>(p => p.CategoryName);
        public static readonly PropertyPath LocationCodeProperty = WpfHelper.GetPropertyPath<StoreSpaceLocationAssignmentItemRowViewModel>(p => p.LocationCode);
        public static readonly PropertyPath LocationNameProperty = WpfHelper.GetPropertyPath<StoreSpaceLocationAssignmentItemRowViewModel>(p => p.LocationName);
        public static readonly PropertyPath PlanNameProperty = WpfHelper.GetPropertyPath<StoreSpaceLocationAssignmentItemRowViewModel>(p => p.PlanName);
        public static readonly PropertyPath PlanBayCountProperty = WpfHelper.GetPropertyPath<StoreSpaceLocationAssignmentItemRowViewModel>(p => p.PlanBayCount);
        public static readonly PropertyPath PlanTotalWidthProperty = WpfHelper.GetPropertyPath<StoreSpaceLocationAssignmentItemRowViewModel>(p => p.PlanTotalWidth);

        #endregion

        #region Constructor

        public StoreSpaceLocationAssignmentItemRowViewModel(PlanAssignmentViewModel parent, LocationSpace locationSpace,
                                                LocationPlanAssignment locationPlanAssignment,
                                                Location location, PlanogramInfo planogramInfo)
        {
            _parent = parent;
            _productGroupInfo = parent.SelectedProductGroup;
            _locationSpace = locationSpace;
            _locationPlanAssignment = locationPlanAssignment;
            _location = location;
            _planogramInfo = planogramInfo;
        }


        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
