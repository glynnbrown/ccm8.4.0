﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//	Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    /// <summary>
    /// Interaction logic for PlanAssignmentHomeTab.xaml
    /// </summary>
    public partial class PlanAssignmentHomeTab : RibbonTabItem
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(PlanAssignmentViewModel), typeof(PlanAssignmentHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public PlanAssignmentViewModel ViewModel
        {
            get { return (PlanAssignmentViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanAssignmentHomeTab senderControl = (PlanAssignmentHomeTab)obj;

            if (e.OldValue != null)
            {
                PlanAssignmentViewModel oldModel = (PlanAssignmentViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                PlanAssignmentViewModel newModel = (PlanAssignmentViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanAssignmentHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
