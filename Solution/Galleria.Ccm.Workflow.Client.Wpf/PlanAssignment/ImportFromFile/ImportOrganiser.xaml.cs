﻿using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement;
using Galleria.Framework.Controls.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.ImportFromFile
{
    /// <summary>
    /// Interaction logic for ImportOrganiser.xaml
    /// </summary>
    public partial class ImportOrganiser : ExtendedRibbonWindow
    {
        #region Fields

        private ImportSelectFile _selectFileStep;
        private ImportColumnMapping _columnMappingStep;
        private ImportDataValidation _dataValidationStep;

        private ImportDataTextColumnsPreview _textColumnsPreviewStep;
        private ImportDataTextColumnsFixedWidth _textColumnsFixedWidthStep;
        private ImportDataTextColumnsDelimited _textColumnsDelimitedStep;

        #endregion

        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportViewModel), typeof(ImportOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ImportViewModel ViewModel
        {
            get { return (ImportViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ImportOrganiser senderControl = (ImportOrganiser)obj;

            if (e.OldValue != null)
            {
                ImportViewModel oldModel = (ImportViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                senderControl.DestroyAllPages();
            }


            if (e.NewValue != null)
            {
                ImportViewModel newModel = (ImportViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                senderControl.OnCurrentStepChanged();
            }
        }

        #endregion

        #region Constructors
        public ImportOrganiser(ImportViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            ViewModel = viewModel;
            this.Loaded += new RoutedEventHandler(ImportOrganiser_Loaded);
        }
        
        #endregion

        #region Methods

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ImportViewModel.CurrentStepProperty.Path)
            {
                OnCurrentStepChanged();
            }
        }

        private void OnCurrentStepChanged()
        {
            if (ViewModel == null) return;

            this.contentArea.Children.Clear();

            switch(ViewModel.CurrentStep)
            {
                case ImportStepsType.SelectFile:
                    if(_selectFileStep == null)
                    {
                        _selectFileStep = new ImportSelectFile(ViewModel);
                    }
                    contentArea.Children.Add(_selectFileStep);
                    break;

                case ImportStepsType.ColumnMapping:
                    if(_columnMappingStep == null)
                    {
                        _columnMappingStep = new ImportColumnMapping(ViewModel.ColumnMappingViewModel);
                    }
                    contentArea.Children.Add(_columnMappingStep);
                    break;

                case ImportStepsType.DataValidation:
                    if (_dataValidationStep == null)
                    {
                        _dataValidationStep = new ImportDataValidation(ViewModel);
                    }
                    contentArea.Children.Add(_dataValidationStep);
                    break;

                case ImportStepsType.TextColumnsPreview:
                    if (_textColumnsPreviewStep == null)
                    {
                        _textColumnsPreviewStep = new ImportDataTextColumnsPreview(this.ViewModel.TextColumnsViewModel);
                    }
                    this.contentArea.Children.Add(_textColumnsPreviewStep);
                    break;

                case ImportStepsType.TextColumnsFixedWidth:
                    if (_textColumnsFixedWidthStep == null)
                    {
                        _textColumnsFixedWidthStep = new ImportDataTextColumnsFixedWidth(this.ViewModel.TextColumnsViewModel);
                    }
                    this.contentArea.Children.Add(_textColumnsFixedWidthStep);
                    break;

                case ImportStepsType.TextColumnsDelimited:
                    if (_textColumnsDelimitedStep == null)
                    {
                        _textColumnsDelimitedStep = new ImportDataTextColumnsDelimited(this.ViewModel.TextColumnsViewModel);
                    }
                    this.contentArea.Children.Add(_textColumnsDelimitedStep);
                    break;
            }
        }

        private void DestroyAllPages()
        {
            contentArea.Children.Clear();

            if (_selectFileStep != null)
            {
                _selectFileStep = null;
            }

            if (_columnMappingStep != null)
            {
                _columnMappingStep = null;
            }

            if (_textColumnsPreviewStep != null)
            {
                _textColumnsPreviewStep.ViewModel = null;
                _textColumnsPreviewStep = null;
            }

            if (_textColumnsFixedWidthStep != null)
            {
                _textColumnsFixedWidthStep.ViewModel = null;
                _textColumnsFixedWidthStep = null;
            }

            if (_textColumnsDelimitedStep != null)
            {
                _textColumnsDelimitedStep.ViewModel = null;
                _textColumnsDelimitedStep = null;
            }

            if (_dataValidationStep != null)
            {
                _dataValidationStep = null;
            }            
        }

        #endregion

        #region Events

        private void ImportOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ImportOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion
    }
}
