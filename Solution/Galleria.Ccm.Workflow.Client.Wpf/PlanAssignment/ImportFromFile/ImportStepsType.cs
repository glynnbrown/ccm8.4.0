﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.ImportFromFile
{
    public enum ImportStepsType
    {
        SelectFile = 0,
        TextColumnsPreview = 1,
        TextColumnsDelimited = 2,
        TextColumnsFixedWidth = 3,
        ColumnMapping = 4,
        DataValidation = 5,
    }
}