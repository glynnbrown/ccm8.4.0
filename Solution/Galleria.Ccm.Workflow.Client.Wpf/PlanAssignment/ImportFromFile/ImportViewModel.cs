﻿using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.ViewModel;
using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System.Data;
using System.Windows.Input;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Processes;
using Galleria.Framework.Collections;
using Galleria.Ccm.Imports.Processes;
using System.Collections.Generic;
using System.IO;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow.DataManagement;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.ImportFromFile
{
    public class ImportViewModel : ViewModelAttachedControlObject<ImportOrganiser>
    {
        public enum ImportFileType
        {
            Excel,
            Text
        }

        #region Fields

        private Stopwatch _timer = new Stopwatch();
        private ModalBusy _busyDialog;
        private ImportFileData _fileData;
        private Boolean _hasOutOfMemoryException;
        private Boolean _hasIOException;

        private ImportFileType _importFileType;
        private ImportStepsType _currentStep;
        private ImportColumnMappingViewModel _columnMappingViewModel;
        private ImportDataTextColumnsViewModel _textColumnsViewModel;

        private Boolean _showMoreSuccessful = false;
        private Boolean _showMoreFailed = true;
        private Int32 _newPlanAssignmentCount;
        private Int32 _updatedPlanAssignmentCount;
        private Int32 _ignoredPlanAssignmentCount;
        private Int32 _totalPlanAssignmentCount;
         
        // Validation
        private ImportFileData _validatedFileData;
        private BulkObservableCollection<ValidationErrorGroup> _worksheetErrors = new BulkObservableCollection<ValidationErrorGroup>();

        private readonly BulkObservableCollection<ImportPlanogramAssignmentRow> _planogramAssignmentRows = new BulkObservableCollection<ImportPlanogramAssignmentRow>();
        private ReadOnlyBulkObservableCollection<ImportPlanogramAssignmentRow> _planogramAssignmentRowsRO;

        private readonly BulkObservableCollection<ImportPlanogramAssignmentRow> _planogramAssignmentErrorRows = new BulkObservableCollection<ImportPlanogramAssignmentRow>();
        private ReadOnlyBulkObservableCollection<ImportPlanogramAssignmentRow> _planogramAssignmentErrorRowsRO;

        #region Select File - Fields

        private String _filePath;
        private PlanogramGroup _selectedPlanogramGroup;
        private PlanogramAssignmentStatusType _selectedPlanogramAssignmentStatusType = PlanogramAssignmentStatusType.Any;
        private ProductGroupInfo _selectedProductGroup;
        private LocationInfo _selectedLocation;

        #endregion

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath StepHeaderProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.StepHeader);
        public static readonly PropertyPath PlanogramAssignmentRowsProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.PlanogramAssignmentRows);
        public static readonly PropertyPath PlanogramAssignmentErrorRowsProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.PlanogramAssignmentErrorRows);
        public static readonly PropertyPath ShowMoreSuccessfulProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.ShowMoreSuccessful);
        public static readonly PropertyPath SuccessfulRowsShowDescriptionProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.SuccessfulRowsShowDescription);
        public static readonly PropertyPath ShowMoreFailedProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.ShowMoreFailed);
        public static readonly PropertyPath FailedRowsShowDescriptionProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.FailedRowsShowDescription);

        public static readonly PropertyPath IsPreviousCommandVisibleProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.IsPreviousCommandVisible);
        public static readonly PropertyPath IsNextCommandVisibleProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.IsNextCommandVisible);

        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.NextCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.PreviousCommand);

        #region Select File - Binding Property Paths

        public static readonly PropertyPath FilePathProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.FilePath);
        public static readonly PropertyPath BrowseForFileCommandProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.BrowseForFileCommand);
        public static readonly PropertyPath BrowseForRepositoryFolderCommandProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.BrowseForRepositoryFolderCommand);
        public static readonly PropertyPath SelectedPlanogramGroupProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.SelectedPlanogramGroup);
        public static readonly PropertyPath SelectedPlanogramGroupNameProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.SelectedPlanogramGroupName);
        public static readonly PropertyPath SelectedPlanogramAssignmentStatusTypeProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.SelectedPlanogramAssignmentStatusType);
        public static readonly PropertyPath SelectedProductGroupProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.SelectedProductGroup);
        public static readonly PropertyPath SelectedLocationProperty = WpfHelper.GetPropertyPath<ImportViewModel>(p => p.SelectedLocation);
        
        #endregion
        
        #endregion

        #region Constructors

        public ImportViewModel(ProductGroupInfo selectedProductGroup, LocationInfo selectedLocation)
        {
            _currentStep = ImportStepsType.SelectFile;
            SelectedProductGroup = selectedProductGroup;
            SelectedLocation = selectedLocation;            
        }

        #endregion

        #region Properties

        #region Select File Properties

        /// <summary>
        /// Gets/Sets the selected file path
        /// </summary>
        public String FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                OnPropertyChanged(FilePathProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the current planogram group
        /// </summary>
        public PlanogramGroup SelectedPlanogramGroup
        {
            get { return _selectedPlanogramGroup; }
            set
            {
                _selectedPlanogramGroup = value;
                OnPropertyChanged(SelectedPlanogramGroupProperty);
                OnPropertyChanged(SelectedPlanogramGroupNameProperty);
            }
        }

        /// <summary>
        /// Gets the selected planogram friendly name
        /// </summary>
        public String SelectedPlanogramGroupName
        {
            get { return _selectedPlanogramGroup != null ? _selectedPlanogramGroup.GetFriendlyFullPath() : null; }
        }
        
        /// <summary>
        /// Gets/Sets the selected planogram status type
        /// </summary>
        public PlanogramAssignmentStatusType SelectedPlanogramAssignmentStatusType
        {
            get { return _selectedPlanogramAssignmentStatusType; }
            set
            {
                _selectedPlanogramAssignmentStatusType = value;
                OnPropertyChanged(SelectedPlanogramAssignmentStatusTypeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected product group
        /// </summary>
        public ProductGroupInfo SelectedProductGroup
        {
            get { return _selectedProductGroup; }
            set
            {
                _selectedProductGroup = value;
                OnPropertyChanged(SelectedProductGroupProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the currently selected location
        /// </summary>
        public LocationInfo SelectedLocation
        {
            get { return _selectedLocation; }
            set
            {
                _selectedLocation = value;
                OnPropertyChanged(SelectedLocationProperty);
            }
        }

        #endregion
        
        /// <summary>
        /// Returns the viewmodel for the column definition step
        /// </summary>
        public ImportColumnMappingViewModel ColumnMappingViewModel
        {
            get
            {
                if (_columnMappingViewModel == null)
                {
                    _columnMappingViewModel = new ImportColumnMappingViewModel(_fileData, this);
                    _columnMappingViewModel.PropertyChanged += new PropertyChangedEventHandler(ColumnMappingViewModel_PropertyChanged);
                }
                return _columnMappingViewModel;
            }
        }

        /// <summary>
        /// Returns the viewmodel for the text columns steps
        /// </summary>
        public ImportDataTextColumnsViewModel TextColumnsViewModel
        {
            get
            {
                if (_textColumnsViewModel == null)
                {
                    _textColumnsViewModel = new ImportDataTextColumnsViewModel(_fileData);
                }
                return _textColumnsViewModel;
            }
        }

        /// <summary>
        /// Returns true if the cancel command should be available on the current step
        /// </summary>
        public Boolean IsPreviousCommandVisible
        {
            get
            {
                if (
                        this.CurrentStep == ImportStepsType.TextColumnsPreview ||
                        this.CurrentStep == ImportStepsType.TextColumnsDelimited ||
                        this.CurrentStep == ImportStepsType.TextColumnsFixedWidth ||
                        this.CurrentStep == ImportStepsType.ColumnMapping ||
                        this.CurrentStep == ImportStepsType.DataValidation
                    )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns true if the next command should be available
        /// for the current step
        /// </summary>
        public Boolean IsNextCommandVisible
        {
            get
            {
                if (
                        this.CurrentStep == ImportStepsType.TextColumnsPreview ||
                        this.CurrentStep == ImportStepsType.TextColumnsDelimited ||
                        this.CurrentStep == ImportStepsType.TextColumnsFixedWidth ||
                        this.CurrentStep == ImportStepsType.ColumnMapping ||
                        this.CurrentStep == ImportStepsType.DataValidation ||
                        this.CurrentStep == ImportStepsType.SelectFile
                    )
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the current step
        /// </summary>
        public ImportStepsType CurrentStep
        {
            get { return _currentStep; }
            set
            {
                _currentStep = value;

                if(_currentStep == ImportStepsType.DataValidation)
                {
                    _nextCommand.FriendlyName = Message.Generic_Finish;
                }
                else
                {
                    _nextCommand.FriendlyName = Message.Generic_Next;
                }

                OnPropertyChanged(CurrentStepProperty);
                OnPropertyChanged(IsNextCommandVisibleProperty);
                OnPropertyChanged(IsPreviousCommandVisibleProperty);
                OnPropertyChanged(StepHeaderProperty);
            }
        }

        /// <summary>
        /// Returns the validated file data
        /// </summary>
        public ImportFileData ValidatedFileData
        {
            get { return _validatedFileData; }
            private set
            {
                _validatedFileData = value;
            }
        }

        /// <summary>
        /// Gets the current step description stage
        /// </summary>
        public String StepHeader
        {
            get
            {
                switch (_currentStep)
                {
                    case ImportStepsType.SelectFile:
                    case ImportStepsType.TextColumnsPreview:
                    case ImportStepsType.TextColumnsDelimited:
                    case ImportStepsType.TextColumnsFixedWidth:
                        return Message.PlanAssignmentImportFromFile_Step1;
                    case ImportStepsType.ColumnMapping:
                        return Message.PlanAssignmentImportFromFile_Step2;
                    case ImportStepsType.DataValidation:
                        return Message.PlanAssignmentImportFromFile_Step3;
                    default:
                        return Message.PlanAssignmentImportFromFile_Step1;
                }                
            }
        }

        /// <summary>
        /// Gets the planogram assignments to be imported
        /// </summary>
        public ReadOnlyBulkObservableCollection<ImportPlanogramAssignmentRow> PlanogramAssignmentRows
        {
            get
            {
                if (_planogramAssignmentRowsRO == null)
                {
                    _planogramAssignmentRowsRO = new ReadOnlyBulkObservableCollection<ImportPlanogramAssignmentRow>(_planogramAssignmentRows);
                }
                return _planogramAssignmentRowsRO;
            }
        }
        
        /// <summary>
        /// Gets the planogram assignments failed validation
        /// </summary>
        public ReadOnlyBulkObservableCollection<ImportPlanogramAssignmentRow> PlanogramAssignmentErrorRows
        {
            get
            {
                if (_planogramAssignmentErrorRowsRO == null)
                {
                    _planogramAssignmentErrorRowsRO = new ReadOnlyBulkObservableCollection<ImportPlanogramAssignmentRow>(_planogramAssignmentErrorRows);
                }
                return _planogramAssignmentErrorRowsRO;
            }
        }

        public Boolean ShowMoreSuccessful
        {
            get
            {
                return _showMoreSuccessful;
            }
            set
            {
                _showMoreSuccessful = value;
                OnPropertyChanged(ShowMoreSuccessfulProperty);
                OnPropertyChanged(SuccessfulRowsShowDescriptionProperty);
            }
        }

        public String SuccessfulRowsShowDescription
        {
            get
            {
                if (_showMoreSuccessful)
                {
                    return Message.PlanAssignmentImportFromFile_ShowLess;
                }
                else
                {
                    return Message.PlanAssignmentImportFromFile_ShowMore;
                }
            }
        }

        public Boolean ShowMoreFailed
        {
            get
            {
                return _showMoreFailed;
            }
            set
            {
                _showMoreFailed = value;
                OnPropertyChanged(ShowMoreFailedProperty);
                OnPropertyChanged(FailedRowsShowDescriptionProperty);
            }
        }

        public String FailedRowsShowDescription
        {
            get
            {
                if (_showMoreFailed)
                {
                    return Message.PlanAssignmentImportFromFile_ShowLess;
                }
                else
                {
                    return Message.PlanAssignmentImportFromFile_ShowMore;
                }
            }
        }

        /// <summary>
        /// Gets the number of new planogram assignments being imported
        /// </summary>
        public Int32 NewPlanAssignmentCount
        {
            get
            {
                return _newPlanAssignmentCount;
            }
        }

        /// <summary>
        /// Gets the number of planogram assignments being updated
        /// </summary>
        public Int32 UpdatedPlanAssignmentCount
        {
            get
            {
                return _updatedPlanAssignmentCount;
            }
        }

        /// <summary>
        /// Gets the number of planogram assignments being excluded
        /// </summary>
        public Int32 IgnoredPlanAssignmentCount
        {
            get
            {
                return _ignoredPlanAssignmentCount;
            }
        }

        /// <summary>
        /// Gets the total number of planogram assignments considered to be imported
        /// </summary>
        public Int32 TotalPlanAssignmentCount
        {
            get
            {
                return _totalPlanAssignmentCount;
            }
        }
        
        #endregion

        #region Commands

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the operation
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }
        #endregion

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// Advances the process to the next step
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next,
                        DisabledReason = Message.PlanAssignmentImportFromFile_NextCommandDisabled
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }
        
        private Boolean Next_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case ImportStepsType.SelectFile:
                    return (FilePath != null && SelectedPlanogramGroup != null);

                case ImportStepsType.TextColumnsPreview:
                case ImportStepsType.TextColumnsDelimited:
                case ImportStepsType.TextColumnsFixedWidth:
                    //nothing to check
                    return true;

                case ImportStepsType.ColumnMapping:                    
                    //mappings must be complete and valid
                    if (this.ColumnMappingViewModel.AreMappingsAllValid)
                    {
                        //Set disabled reason
                        this.NextCommand.DisabledReason = Message.DataManagement_NextCommand_MappingsNotUniqueDisabledReason;
                        if (this.ColumnMappingViewModel.AreMappingsAllUnique)
                        {
                            return true;
                        };
                    }
                    return false;
            }

            return true;
        }

        private void Next_Executed()
        {
            base.ShowWaitCursor(true);

            switch (this.CurrentStep)
            {
                case ImportStepsType.SelectFile:

                    //if import is null or source does not match selected filepath we need a new file
                    bool requiresNewImportFile = (_fileData == null);
                    if (!requiresNewImportFile) { requiresNewImportFile = (_fileData.SourceFilePath != FilePath); }

                    switch (_importFileType)
                    {
                        case ImportFileType.Text:
                            if (requiresNewImportFile)
                            {
                                try
                                {
                                    _fileData = ImportFileData.NewImportFileData();
                                    OnImportFileDataChanged();
                                    //Only reset if we need to as it clears some values user has set in _fileData
                                    //  SA-22473 : or file data has no rows which may indicate that the file was in use when attempted to load originally
                                    if (this.TextColumnsViewModel.LoadFilePath != this.FilePath
                                        || _fileData.Rows.Count == 0)
                                    {
                                        this.TextColumnsViewModel.LoadFilePath = this.FilePath;
                                        _fileData.SourceFilePath = this.FilePath;
                                    }
                                    this.CurrentStep = ImportStepsType.TextColumnsPreview;
                                }
                                catch (IOException exception)
                                {
                                    DisplayImportExceptionMessage(exception);
                                    break;
                                }
                                catch (OutOfMemoryException exception)
                                {
                                    DisplayImportMemoryExceptionMessage(exception);
                                    break;
                                }
                            }
                            else
                            {
                                //update the mapping list if required
                                _fileData.MappingList = PlanogramAssignmentImportMappingList.NewPlanogramAssignmentImportMappingList();
                                this.CurrentStep = ImportStepsType.TextColumnsPreview;
                            }
                            break;

                        case ImportFileType.Excel:
                            if (requiresNewImportFile)
                            {
                                Int32 workSheetIndex = (_columnMappingViewModel != null) ? _columnMappingViewModel.GetSelectedWorksheetIndex() : 0;
                                LoadPlanAssignmentFromExcelFile(FilePath, workSheetIndex);
                            }
                            else
                            {
                                this.CurrentStep = ImportStepsType.ColumnMapping;
                            }
                            break;
                    }
                    break;
                    
                case ImportStepsType.TextColumnsPreview:
                    if (this.TextColumnsViewModel.IsFixedWidth)
                    {
                        this.CurrentStep = ImportStepsType.TextColumnsFixedWidth;
                    }
                    else
                    {
                        this.TextColumnsViewModel.UpdateColumns();
                        this.CurrentStep = ImportStepsType.TextColumnsDelimited;
                    }
                    break;

                case ImportStepsType.TextColumnsDelimited:
                case ImportStepsType.TextColumnsFixedWidth:
                    this.TextColumnsViewModel.UpdateColumns();
                    this.CurrentStep = ImportStepsType.ColumnMapping;
                    break;

                case ImportStepsType.ColumnMapping:
                    ValidatePlanAssignments(_columnMappingViewModel.ImportStartRowNumber);
                    break;

                case ImportStepsType.DataValidation:
                    CancelCommand.Execute();
                    break;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Previous

        private RelayCommand _previousCommand;

        /// <summary>
        /// Moves the process back a step
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Previous_CanExecute()
        {
            return (this.CurrentStep != ImportStepsType.SelectFile);
        }

        private void Previous_Executed()
        {
            switch (this.CurrentStep)
            {
                case ImportStepsType.SelectFile:
                    break;
                    
                case ImportStepsType.TextColumnsPreview:
                    this.CurrentStep = ImportStepsType.SelectFile;
                    break;


                case ImportStepsType.TextColumnsDelimited:
                case ImportStepsType.TextColumnsFixedWidth:
                    this.CurrentStep = ImportStepsType.TextColumnsPreview;
                    break;


                case ImportStepsType.ColumnMapping:
                    if (_importFileType == ImportFileType.Text)
                    {
                        this.CurrentStep =
                            (this.TextColumnsViewModel.IsFixedWidth) ?
                            ImportStepsType.TextColumnsFixedWidth :
                            ImportStepsType.TextColumnsDelimited;
                    }
                    else
                    {
                        this.CurrentStep = ImportStepsType.SelectFile;
                    }
                    break;

                case ImportStepsType.DataValidation:
                    this.CurrentStep = ImportStepsType.ColumnMapping;
                    break;
            }
        }

        #endregion
        
        #region BrowseForFile

        private RelayCommand _browseForFileCommand;

        /// <summary>
        /// Shows the file open dialog
        /// </summary>
        public RelayCommand BrowseForFileCommand
        {
            get
            {
                if (_browseForFileCommand == null)
                {
                    _browseForFileCommand = new RelayCommand(
                        p => BrowseForFile_Executed())
                    {
                        FriendlyName = Message.Generic_Browse
                    };
                }
                return _browseForFileCommand;
            }
        }

        private void BrowseForFile_Executed()
        {
            //show the file open dialog
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = Message.ImportFileTypeFilter_Excel2007 + "|" + Message.ImportFileTypeFilter_Excel2003 + "|" + Message.ImportFileTypeFilter_Text + "|" + Message.ImportFileTypeFilter_Csv;
            
            //update the selected file to the filepath
            if (ofd.ShowDialog() == true)
            {
                FilePath = ofd.FileName;

                String extension = Path.GetExtension(FilePath);
                _importFileType = (extension == ".xlsx" || extension == ".xls") ? ImportFileType.Excel : ImportFileType.Text;
            }
        }

        #endregion

        #region BrowseForRepositoryFolder

        private RelayCommand _browseForRepositoryFolderCommand;

        /// <summary>
        /// Shows the file open dialog
        /// </summary>
        public RelayCommand BrowseForRepositoryFolderCommand
        {
            get
            {
                if (_browseForRepositoryFolderCommand == null)
                {
                    _browseForRepositoryFolderCommand = new RelayCommand(
                        p => BrowseForRepositoryFolder_Executed())
                    {
                        FriendlyName = Message.Generic_Browse
                    };
                }
                return _browseForRepositoryFolderCommand;
            }
        }

        private void BrowseForRepositoryFolder_Executed()
        {
            PlanogramHierarchySelectorWindowViewModel viewModel = new PlanogramHierarchySelectorWindowViewModel();
            PlanogramHierarchySelectorWindow win = new PlanogramHierarchySelectorWindow(viewModel);
            CommonHelper.GetWindowService().ShowDialog<PlanogramHierarchySelectorWindow>(win);

            if (win.DialogResult == true && win.ViewModel.SelectedPlanogramGroup !=null)
            {
                SelectedPlanogramGroup = viewModel.SelectedPlanogramGroup;
            }
        }

        #endregion

        #endregion
        
        #region Processes
        
        #region Load

        /// <summary>
        /// Loads the given excel file into the importfiledata object
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileData"></param>
        private void LoadPlanAssignmentFromExcelFile(String filePath, Int32 worksheetIndex)
        {
            _timer.Restart();

            //start the import worker
            BackgroundWorker importWorker = new BackgroundWorker();
            importWorker.DoWork += LoadWorker_DoWork;
            importWorker.RunWorkerCompleted += LoadWorker_RunWorkerCompleted;
            importWorker.WorkerReportsProgress = false;
            importWorker.WorkerSupportsCancellation = false;

            importWorker.RunWorkerAsync(new object[] { filePath, worksheetIndex });

            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.DataManagement_Validation_BusyImportingHeader;
                _busyDialog.Description = Message.DataManagement_Validation_BusyImportingDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }


        private void LoadWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            object[] args = (object[])e.Argument;
            String filePath = (String)args[0];
            Int32 worksheetIndex = (Int32)args[1];
            _hasOutOfMemoryException = false;
            _hasIOException = false;
            
            //create this as a new object so that we dont get threading issues
            ImportFileData fileData = ImportFileData.NewImportFileData();
            fileData.SourceFilePath = filePath;

            DataTable tableData = FileHelper.LoadSpecificWorksheet(filePath, worksheetIndex, /*isFirstRowHeaders*/false, ref _hasOutOfMemoryException, ref _hasIOException);

            if (tableData != null)
            {
                //setup columns
                Int32 colNumber = 1;
                foreach (DataColumn col in tableData.Columns)
                {
                    ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(colNumber);
                    importColumn.Header = col.ColumnName;
                    importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                    fileData.Columns.Add(importColumn);

                    colNumber++;
                }

                //setup rows
                Int32 rowNumber = 1;
                foreach (DataRow row in tableData.Rows)
                {
                    ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowNumber);
                    Int32 rowItemArrayCount = row.ItemArray.Count();

                    //cycle through adding cells
                    for (Int32 cellIndex = 0; cellIndex < rowItemArrayCount; cellIndex++)
                    {
                        ImportFileDataCell importCell =
                            ImportFileDataCell.NewImportFileDataCell(cellIndex + 1, row.ItemArray[cellIndex]);
                        importRow.Cells.Add(importCell);
                    }

                    //cycle through adding rows
                    fileData.Rows.Add(importRow);

                    rowNumber++;
                }
            }

            e.Result = fileData;
        }

        private void LoadWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker importWorker = (BackgroundWorker)sender;
            importWorker.DoWork -= LoadWorker_DoWork;
            importWorker.RunWorkerCompleted -= LoadWorker_RunWorkerCompleted;

            if (_hasOutOfMemoryException)
            {
                //Hide busy cursor if exception is thrown
                try { Mouse.OverrideCursor = null; }
                catch (InvalidOperationException) { } //stop busy cursor

                ModalMessage errorMessageBox = new ModalMessage();
                errorMessageBox.Header = Message.DataManagement_BackstageImportTitle_FileTooBig;
                errorMessageBox.Description = Message.DataManagement_BackstageFileTooLarge;
                errorMessageBox.ButtonCount = 1;
                errorMessageBox.Button1Content = Message.Generic_Ok;
                App.ShowWindow(errorMessageBox, true);
            }
            else if (_hasIOException)
            {
                //Hide busy cursor if exception is thrown
                try { Mouse.OverrideCursor = null; }
                catch (InvalidOperationException) { } //stop busy cursor

                ModalMessage errorMessageBox = new ModalMessage();
                errorMessageBox.Header = Message.DataManagement_BackstageImportTitle;
                errorMessageBox.Description = Message.DataManagement_BackstageFileInUse;
                errorMessageBox.ButtonCount = 1;
                errorMessageBox.Button1Content = Message.Generic_Ok;
                App.ShowWindow(errorMessageBox, true);
            }
            else if (e.Error != null)
            {
                //Hide busy 
                if (_busyDialog != null)
                {
                    _busyDialog.Close();
                    _busyDialog = null;
                }

                //If any other exception is detected which is not handled
                if (AttachedControl != null)
                {
                    var errorMessageBox = new ModalMessage();
                    errorMessageBox.Header = Message.DataManagement_Import_FailHeader;
                    errorMessageBox.Description = Message.DataManagement_Import_FailMessage;
                    errorMessageBox.ButtonCount = 1;
                    errorMessageBox.Button1Content = Message.Generic_Ok;
                    errorMessageBox.ShowInTaskbar = true;
                    App.ShowWindow(errorMessageBox, true);
                }
            }
            else
            {
                //The file data was imported so we can continue
                _fileData = (ImportFileData)e.Result;
                OnImportFileDataChanged();

                //move to the next step
                this.CurrentStep = ImportStepsType.ColumnMapping;
            }

            //Hide busy - queued as render of step might take a sec
            //- not sure how im getting away with this as technically the thread should be blocked?
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            _timer.Stop();
            Debug.WriteLine(String.Format("Import from excel: {0} secs", _timer.Elapsed.TotalSeconds));
        }

        #endregion

        #region Validation

        /// <summary>
        /// Starts the validation process for the given type
        /// </summary>
        /// <param name="dataType"></param>
        private void ValidatePlanAssignments(Int32 startRow)
        {
            _timer.Restart();
            Int32 entityId = App.ViewState.EntityId;

            _fileData.RaiseChangeEvents = false;

            ProcessFactory<ValidatePlanogramAssignmentProcess> locationProcessFactory = new ProcessFactory<ValidatePlanogramAssignmentProcess>();
            locationProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ValidatePlanogramAssignmentProcess>>(OnValidationProcessCompleted);
            locationProcessFactory.Execute(new ValidatePlanogramAssignmentProcess(entityId, _selectedPlanogramGroup, _selectedProductGroup,
                                _selectedLocation, _selectedPlanogramAssignmentStatusType, _fileData, startRow, false));

            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.DataManagement_Validation_BusyValidatingHeader;
                _busyDialog.Description = Message.DataManagement_Validation_BusyValidatingDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }


        /// <summary>
        /// Handles the completion of the validation process
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnValidationProcessCompleted<T>(object sender, ProcessCompletedEventArgs<T> e)
            where T : Galleria.Ccm.Imports.Processes.ValidateProcessBase<T>
        {
            // unsubscribe events
            ProcessFactory<T> processFactory = (ProcessFactory<T>)sender;
            processFactory.ProcessCompleted -= OnValidationProcessCompleted;

            if (e.Error != null)
            {
                if (this.AttachedControl != null)
                {
                    ModalMessage errorWindow = new ModalMessage();
                    errorWindow.Description = Message.DataManagement_Validation_FailMessage;
                    errorWindow.Header = Message.DataManagement_Validation_FailHeader;
                    errorWindow.Button1Content = Message.Generic_Ok;
                    errorWindow.ButtonCount = 1;
                    errorWindow.MessageIcon = ImageResources.ErrorCritical;
                    App.ShowWindow(errorWindow, AttachedControl, true);
                }
            }
            else
            {
                LoadProcessResults(e);
            }

            _fileData.RaiseChangeEvents = true;

            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            if (e.Error == null)
            {
                ModalMessage messageWindow = new ModalMessage();
                messageWindow.Description = String.Format(Message.PlanAssignmentImportFromFile_AboutToImportDescription, _totalPlanAssignmentCount, _newPlanAssignmentCount, _updatedPlanAssignmentCount);
                messageWindow.Header = Message.PlanAssignmentImportFromFile_AboutToImportHeader;
                messageWindow.Button1Content = Message.Generic_Continue;
                messageWindow.Button2Content = Message.Generic_Cancel;
                messageWindow.ButtonCount = 2;
                messageWindow.MessageIcon = ImageResources.DialogWarning;
                App.ShowWindow(messageWindow, AttachedControl, true);

                if (messageWindow.Result == ModalMessageResult.Button1)
                {
                    ImportPlanogramAssignments(ValidatedFileData);
                    this.CurrentStep = ImportStepsType.DataValidation;
                }
            }

            _timer.Stop();
            Debug.WriteLine(String.Format("Validation: {0} secs", _timer.Elapsed.TotalSeconds));
        }

        #endregion

        #region Import Data

        private void ImportPlanogramAssignments(ImportFileData importData)
        {
            _timer.Restart();
            Int32 entityId = App.ViewState.EntityId;
            
            ProcessFactory<ImportPlanogramAssignmentProcess> planogramAssignmentProcessFactory = new ProcessFactory<ImportPlanogramAssignmentProcess>();
            planogramAssignmentProcessFactory.ProcessCompleted += new EventHandler<ProcessCompletedEventArgs<ImportPlanogramAssignmentProcess>>(OnImportProcessCompleted);
            planogramAssignmentProcessFactory.Execute(new ImportPlanogramAssignmentProcess(entityId, _selectedPlanogramGroup, _selectedProductGroup,
                                                                                        _selectedLocation, _selectedPlanogramAssignmentStatusType, importData));
            
            //show busy
            if (this.AttachedControl != null)
            {
                _busyDialog = new ModalBusy();
                _busyDialog.Header = Message.DataManagement_Validation_BusyUpdatingDatabaseHeader;
                _busyDialog.Description = Message.DataManagement_Validation_BusyUpdatingDatabaseDescription;
                _busyDialog.IsDeterminate = false;
                _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);
                App.ShowWindow(_busyDialog, /*isModal*/true);
            }
        }

        /// <summary>
        /// Handles the complete of the import process
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnImportProcessCompleted<T>(object sender, ProcessCompletedEventArgs<T> e)
             where T : Galleria.Ccm.Imports.Processes.ImportProcessBase<T>
        {
            // unsubscribe events
            ProcessFactory<T> processFactory = (ProcessFactory<T>)sender;
            processFactory.ProcessCompleted -= OnImportProcessCompleted;

            Int32 numRowsImported = e.Process.ImportData.Rows.Count;

            //Hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            _timer.Stop();
            Debug.WriteLine(String.Format("Import to database: {0} secs", _timer.Elapsed.TotalSeconds));

            if (e.Error != null)
            {
                if (this.AttachedControl != null)
                {
                    ModalMessage errorWindow = new ModalMessage();
                    errorWindow.Description =
                        String.Format("{0}{1}{2}",
                        Message.DataManagement_Import_FailMessage, Environment.NewLine, e.Error.Message);
                    errorWindow.Header = Message.DataManagement_Import_FailHeader;
                    errorWindow.Button1Content = Message.Generic_Ok;
                    errorWindow.ButtonCount = 1;
                    errorWindow.MessageIcon = ImageResources.ErrorCritical;
                    App.ShowWindow(errorWindow, AttachedControl, true);
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Loads in the given process results
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="e"></param>
        private void LoadProcessResults<T>(ProcessCompletedEventArgs<T> e) where T : Imports.Processes.ValidateProcessBase<T>
        {
            this.ValidatedFileData = e.Process.ValidatedData;

            //clear the errors collection
            if (_worksheetErrors.Count > 0) { _worksheetErrors.Clear(); }

            if (e.Process.Errors.Count != 0)
            {
                //update the errors collection
                _worksheetErrors.AddRange(e.Process.Errors);
            }

            // Dictionary of error row numbers and the problem descriptions for the row
            Dictionary<Int32, String> errorRows = new Dictionary<Int32, String>();

            // Get column mappings
            ImportMapping locationCodeMappingForImportType = ValidatedFileData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId).FirstOrDefault();
            ImportMapping productGroupMappingForImportType = ValidatedFileData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.ProductGroupCodeMappingId).FirstOrDefault();
            ImportMapping planogramNameMappingForImportType = ValidatedFileData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId).FirstOrDefault();
            Int32 locationCodeMappingColumnNumber = _fileData.GetMappedColNumber(locationCodeMappingForImportType);
            Int32 productGroupMappingColumnNumber = _fileData.GetMappedColNumber(productGroupMappingForImportType);
            Int32 planogramNameMappingColumnNumber = _fileData.GetMappedColNumber(planogramNameMappingForImportType);

            // reset plan assignment import details
            _planogramAssignmentErrorRows.Clear();
            _planogramAssignmentRows.Clear();
            _newPlanAssignmentCount = 0;
            _updatedPlanAssignmentCount = 0;
            _ignoredPlanAssignmentCount = 0;
            _totalPlanAssignmentCount = 0;

            // load error info
            foreach (var worksheetError in _worksheetErrors)
            {
                foreach (ValidationErrorItem validationErrorItem in worksheetError.Errors)
                {
                    if (!errorRows.ContainsKey(validationErrorItem.RowNumber))
                    {
                        errorRows.Add(validationErrorItem.RowNumber, validationErrorItem.ProblemDescription);
                    }
                    else
                    {
                        // update error row
                        errorRows[validationErrorItem.RowNumber] += Environment.NewLine + Environment.NewLine + validationErrorItem.ProblemDescription;
                    }
                }
            }
            
            foreach (var row in ValidatedFileData.Rows)
            {
                if (!row.IsIgnored)
                {
                    if (!errorRows.ContainsKey(row.RowNumber))
                    {
                        // success passed validation
                        _planogramAssignmentRows.Add(new ImportPlanogramAssignmentRow(row.RowNumber, row[locationCodeMappingColumnNumber].ToString(),
                                                    row[productGroupMappingColumnNumber].ToString(), row[planogramNameMappingColumnNumber].ToString(), "Successfully imported"));

                        if ((row.Cells[0].ToString() == "Add") && (row.Cells[3].ToString() != null))
                        {
                            _newPlanAssignmentCount++;
                        }
                        else if((row.Cells[0].ToString() == "Update"))
                        {
                            _updatedPlanAssignmentCount++;
                        }
                    }
                    else
                    {
                        // failed validation
                        _planogramAssignmentErrorRows.Add(new ImportPlanogramAssignmentRow(row.RowNumber, row[locationCodeMappingColumnNumber].ToString(),
                                                    row[productGroupMappingColumnNumber].ToString(), row[planogramNameMappingColumnNumber].ToString(), errorRows[row.RowNumber]));

                    }

                    _totalPlanAssignmentCount++;
                }
                else
                {
                    //check if this was ignored because it is a code which is completely invalid.
                    // in which case we should show it as an error instead.
                    String errorInfo = null;
                    if (this.SelectedProductGroup != null)
                    {
                        String productCode = row[productGroupMappingColumnNumber].ToString()?.Trim();
                        if(!ProductGroupInfoList.FetchByProductGroupCodes(/*App.ViewState.EntityId,*/ new List<String> { productCode }).Any())
                        {
                            errorInfo = Message.PlanAssignmentImportFromFile_RowIgnoredCategoryInvalid;
                        }
                    }
                    else
                    {
                        String locationCode = row[locationCodeMappingColumnNumber].ToString()?.Trim();
                        if(!LocationInfoList.FetchByEntityIdLocationCodes(App.ViewState.EntityId, new List<String> { locationCode }).Any())
                        {
                            errorInfo = Message.PlanAssignmentImportFromFile_RowIgnoredLocationInvalid;
                        }
                    }

                    if(errorInfo != null)
                    {
                        //add to the failed validation list as the code does not match any valid item.
                        _planogramAssignmentErrorRows.Add(new ImportPlanogramAssignmentRow(row.RowNumber,
                                                   row[locationCodeMappingColumnNumber].ToString(),
                                                   row[productGroupMappingColumnNumber].ToString(),
                                                   row[planogramNameMappingColumnNumber].ToString(),
                                                   errorInfo));

                        _totalPlanAssignmentCount++;
                    }
                    else
                    {
                        //this has been ignored for a reason.
                        _ignoredPlanAssignmentCount++;
                    }
                }
            }            
        }
                
        #endregion
        
        #region Helper Methods

        /// <summary>
        /// Displays a message box containing the exception's message
        /// </summary>
        /// <param name="exception"></param>
        private void DisplayImportExceptionMessage(IOException exception)
        {
            base.ShowWaitCursor(false);
            _fileData = null;

            ModalMessage message = new ModalMessage();
            message.Header = Message.ImportData_FileAccessException;
            message.Description = exception.Message;
            message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            message.ButtonCount = 1;
            message.Button1Content = Message.Generic_Ok;
            message.DefaultButton = ModalMessageButton.Button1;
            message.MessageIcon = ImageResources.DialogError;
            App.ShowWindow(message, true);
        }

        /// <summary>
        /// Displays a message box containing the exception's message
        /// </summary>
        /// <param name="exception"></param>
        private void DisplayImportMemoryExceptionMessage(OutOfMemoryException exception)
        {
            base.ShowWaitCursor(false);
            _fileData = null;

            ModalMessage message = new ModalMessage();
            message.Header = Message.ImportData_SystemMemoryException;
            message.Description = String.Format(Message.ImportData_SystemMemoryExceptionDescription, exception.Message);
            message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            message.ButtonCount = 1;
            message.Button1Content = Message.Generic_Ok;
            message.DefaultButton = ModalMessageButton.Button1;
            message.MessageIcon = ImageResources.DialogError;
            App.ShowWindow(message, true);
        }

        #endregion
        
        #region Events

        private void OnImportFileDataChanged()
        {
            //get its mapping list
            _fileData.MappingList = PlanogramAssignmentImportMappingList.NewPlanogramAssignmentImportMappingList();
            _fileData.SourceFilePath = FilePath;

            if (_textColumnsViewModel != null)
            {
                _textColumnsViewModel.FileData = _fileData;
            }

            //update the views as required.
            if (_columnMappingViewModel != null)
            {
                //drop handler while we do this
                _columnMappingViewModel.PropertyChanged -= ColumnMappingViewModel_PropertyChanged;
                _columnMappingViewModel.FileData = _fileData;
                _columnMappingViewModel.PropertyChanged += ColumnMappingViewModel_PropertyChanged;
            }
        }

        private void ColumnMappingViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ImportColumnMappingViewModel.SelectedWorksheetProperty.Path)
            {
                LoadPlanAssignmentFromExcelFile(this.FilePath, ColumnMappingViewModel.GetSelectedWorksheetIndex());
            }
        }

        #endregion

        #region IDispose

        protected override void Dispose(Boolean disposing)
        {
            if (_columnMappingViewModel != null)
            {
                _columnMappingViewModel.PropertyChanged -= ColumnMappingViewModel_PropertyChanged;
                _columnMappingViewModel = null;
            }

            _textColumnsViewModel = null;
            _validatedFileData = null;
            _worksheetErrors = null;
            _planogramAssignmentRows.Clear();
            _planogramAssignmentErrorRows.Clear();
        }

        #endregion
    }
}

#region supporting class

public sealed class ImportPlanogramAssignmentRow
{
    #region Fields

    Int32 _rowNumber;
    String _locationCode;
    String _productGroupCode;
    String _planogramName;
    String _information;

    #endregion

    #region Binding Property Paths

    #endregion

    #region Properties

    public Int32 RowNumber
    {
        get { return _rowNumber; }
    }

    public String LocationCode
    {
        get { return _locationCode; }
    }

    public String ProductGroupCode
    {
        get { return _productGroupCode; }
    }

    public String PlanogramName
    {
        get { return _planogramName; }
    }

    public String Information
    {
        get { return _information; }
        set { _information = value; }
    }

    #endregion

    #region Constructor

    /// <summary>
    /// Creates a new instance of this type.
    /// </summary>
    public ImportPlanogramAssignmentRow(Int32 rowNumber, String locationCode, String productGroupCode, String planogramName, String information)
    {
        _rowNumber = rowNumber;
        _locationCode = locationCode;
        _productGroupCode = productGroupCode;
        _planogramName = planogramName;
        _information = information;
    }

    #endregion    
}

#endregion