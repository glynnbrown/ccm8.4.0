﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.ImportFromFile
{
    /// <summary>
    /// Interaction logic for ImportSelectFile.xaml
    /// </summary>
    public partial class ImportSelectFile : UserControl
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportViewModel),
            typeof(ImportSelectFile));

        public ImportViewModel ViewModel
        {
            get { return (ImportViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="viewModel"></param>
        public ImportSelectFile(ImportViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;
        }

        #endregion
        
    }
}
