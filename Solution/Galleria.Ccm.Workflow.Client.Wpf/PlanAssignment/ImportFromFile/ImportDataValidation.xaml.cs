﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.ImportFromFile
{
    /// <summary>
    /// Interaction logic for ImportDataValidation.xaml
    /// </summary>
    public partial class ImportDataValidation : UserControl
    {
        public ImportViewModel ViewModel { get; }
        public ImportDataValidation(ImportViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
        }
        
        private void ShowMoreSuccessful_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.ShowMoreSuccessful = !ViewModel.ShowMoreSuccessful;
            }
        }

        private void ShowMoreFailed_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.ShowMoreFailed = !ViewModel.ShowMoreFailed;
            }
        }

        private void TextBlock_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (ViewModel != null)
                {
                    ViewModel.ShowMoreSuccessful = !ViewModel.ShowMoreSuccessful;
                }
            }
            else
            {
                return;
            }
        }

        private void TextBlock_PreviewKeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (ViewModel != null)
                {
                    ViewModel.ShowMoreFailed = !ViewModel.ShowMoreFailed;
                }
            }
            else
            {
                return;
            }
        }
    }
}
