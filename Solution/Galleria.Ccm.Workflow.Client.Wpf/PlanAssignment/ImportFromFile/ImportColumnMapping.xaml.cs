﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment.ImportFromFile
{
    /// <summary>
    /// Interaction logic for ImportColumnMapping.xaml
    /// </summary>
    public partial class ImportColumnMapping : UserControl
    {
        #region Constants
        const String AvailableColumnsCollectionKey = "ImportDataColumnDefinition_AvailableColumnsCollection";
        #endregion

        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ImportColumnMappingViewModel), typeof(ImportColumnMapping),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ImportColumnMappingViewModel ViewModel
        {
            get { return (ImportColumnMappingViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ImportColumnMapping senderControl = (ImportColumnMapping)obj;

            String clearSelectedMappingsCommandKey = "ClearSelectedMappingsCommand";
            String clearSelectedMappingCommandKey = "ClearSelectedMappingCommand";

            if (e.OldValue != null)
            {
                ImportColumnMappingViewModel oldModel = (ImportColumnMappingViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(clearSelectedMappingsCommandKey);
                senderControl.Resources.Remove(clearSelectedMappingCommandKey);
                ((INotifyCollectionChanged)oldModel.AvailableColumns).CollectionChanged -= senderControl.ViewModelAvailableColumns_CollectionChanged;
            }

            if (e.NewValue != null)
            {
                ImportColumnMappingViewModel newModel = (ImportColumnMappingViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(clearSelectedMappingsCommandKey, newModel.ClearSelectedMappingsCommand);
                senderControl.Resources.Add(clearSelectedMappingCommandKey, newModel.ClearSelectedMappingCommand);
                ((INotifyCollectionChanged)newModel.AvailableColumns).CollectionChanged += senderControl.ViewModelAvailableColumns_CollectionChanged;
                senderControl.ResetAvailableColumnsCollection(newModel.AvailableColumns);

            }
        }


        #endregion

        #region Constructor

        public ImportColumnMapping(ImportColumnMappingViewModel viewModel)
        {
            InitializeComponent();
            this.ViewModel = viewModel;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the view model available columns collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModelAvailableColumns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            ColumnHeaderCollection resCollection = this.Resources[AvailableColumnsCollectionKey] as ColumnHeaderCollection;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (String s in e.NewItems)
                    {
                        resCollection.Add(s);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (String s in e.OldItems)
                    {
                        resCollection.Remove(s);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    ResetAvailableColumnsCollection((IEnumerable<String>)sender);
                    break;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Resynchs the available columns resource collection
        /// </summary>
        private void ResetAvailableColumnsCollection(IEnumerable<String> sourceCollection)
        {
            ColumnHeaderCollection resCollection = this.Resources[AvailableColumnsCollectionKey] as ColumnHeaderCollection;
            if (resCollection != null)
            {
                resCollection.Clear();

                if (sourceCollection != null)
                {
                    if (this.ViewModel.IsFirstRowColumnHeaders)
                    {
                        foreach (String columnName in sourceCollection.OrderBy(s => s))
                        {
                            resCollection.Add(columnName);
                        }
                    }
                    else
                    {
                        foreach (String columnName in sourceCollection)
                        {
                            resCollection.Add(columnName);
                        }
                    }
                }
            }
        }

        #endregion
    }

    /// <summary>
    /// Custom string collection for the available columns drop down list
    /// </summary>
    public class ColumnHeaderCollection : ObservableCollection<String>
    {
        public ColumnHeaderCollection() { }
    }
}
