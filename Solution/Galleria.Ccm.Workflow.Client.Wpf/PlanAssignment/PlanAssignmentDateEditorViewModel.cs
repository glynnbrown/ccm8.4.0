﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.1.0)
// V8-28242 : M.Shelley
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    public sealed class PlanAssignmentDateEditorViewModel : ViewModelAttachedControlObject<PlanAssignmentDateEditorWindow>
    {
        #region Fields

        private Boolean _isGfsConnected;
        private DateTime? _dateLive;
        private DateTime? _dateCommunicated;
        private Int32 _selectedCount = 0;
        private Boolean? _dialogResult;

        #endregion

        #region Property Binding Paths

        // Properties
        public static readonly PropertyPath DateLiveProperty = WpfHelper.GetPropertyPath<PlanAssignmentDateEditorViewModel>(p => p.DateLive);
        public static readonly PropertyPath DateCommunicatedProperty = WpfHelper.GetPropertyPath<PlanAssignmentDateEditorViewModel>(p => p.DateCommunicated);
        public static readonly PropertyPath SelectedCountProperty = WpfHelper.GetPropertyPath<PlanAssignmentDateEditorViewModel>(p => p.SelectedCount);
        public static readonly PropertyPath AreDatesEditableProperty = WpfHelper.GetPropertyPath<PlanAssignmentDateEditorViewModel>(p => p.AreDatesEditable);

        // Commands
        public static readonly PropertyPath OKCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentDateEditorViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<PlanAssignmentDateEditorViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        #region DateLive property

        public DateTime? DateLive
        {
            get { return _dateLive; }
            set
            {
                _dateLive = value;
                OnPropertyChanged(DateLiveProperty);
            }
        }

        #endregion

        #region DateCommunicated property

        public DateTime? DateCommunicated
        {
            get { return _dateCommunicated; }
            set
            {
                _dateCommunicated = value;
                OnPropertyChanged(DateCommunicatedProperty);
            }
        }

        #endregion

        #region SelectedCount property

        public Int32 SelectedCount
        {
            get { return _selectedCount; }
        }

        #endregion

        #region AreDatesEditable property

        public Boolean AreDatesEditable 
        { 
            get { return !_isGfsConnected; } 
        }

        #endregion

        #region DialogResult property

        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
				{
                    this.AttachedControl.DialogResult = value;
				}
            }
        }

        #endregion

        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OKCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private bool OKCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            _okCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void OKCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed(),
                        p => CancelCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private bool CancelCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            _cancelCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Constructor(s)

        public PlanAssignmentDateEditorViewModel(Boolean isGfsConnected, Int32 selectedItemsCount)
        {
            _isGfsConnected = isGfsConnected;
            _selectedCount = selectedItemsCount;
        }

        public PlanAssignmentDateEditorViewModel(Boolean isGfsConnected, Int32 selectedItemsCount,
                                                   DateTime? displayDateCommunicated, DateTime? displayDateLive)
        {
            _isGfsConnected = isGfsConnected;
            _selectedCount = selectedItemsCount;

            if (displayDateCommunicated != null)
            {
                DateCommunicated = displayDateCommunicated;
            }

            if (displayDateLive!= null)
            {
                DateLive= displayDateLive;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
