﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.1.0)
// V8-28242 : M.Shelley
//	Created
#endregion

#endregion

using System.Windows;
using Galleria.Framework.Controls.Wpf;
using System;
using System.Windows.Input;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    /// <summary>
    /// Interaction logic for Plan_AssignmentDateEditorWindow.xaml
    /// </summary>
    public partial class PlanAssignmentDateEditorWindow : ExtendedRibbonWindow
    {
        #region Property

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanAssignmentDateEditorViewModel), typeof(PlanAssignmentDateEditorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanAssignmentDateEditorWindow senderControl = (PlanAssignmentDateEditorWindow)obj;

            if (e.OldValue != null)
            {
                PlanAssignmentDateEditorViewModel oldModel = (PlanAssignmentDateEditorViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                PlanAssignmentDateEditorViewModel newModel = (PlanAssignmentDateEditorViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel controller
        /// </summary>
        public PlanAssignmentDateEditorViewModel ViewModel
        {
            get { return (PlanAssignmentDateEditorViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanAssignmentDateEditorWindow(PlanAssignmentDateEditorViewModel viewModel)
        {
            InitializeComponent();

            this.ViewModel = viewModel;
            this.Loaded += PlanAssignmentDateEditorWindow_Loaded;
        }

        #endregion

        #region Event Handlers

        void PlanAssignmentDateEditorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanAssignmentDateEditorWindow_Loaded;

            // Cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion
    }
}
