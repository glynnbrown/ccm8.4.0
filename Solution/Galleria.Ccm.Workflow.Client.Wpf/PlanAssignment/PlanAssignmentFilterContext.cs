﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
//{
//    public class PlanAssignmentFilterContext
//    {
//        #region Fields

//        private Boolean _showOnlyAssignedStoreSpacePublishedItemData = false;
//        private Boolean _hideAssignedStoreSpacePublishedItemData = false;
//        private Boolean _showInCompatibleStoreSpaceItemsData = false;
//        private Boolean _hideMissingStoreSpace = true;

//        #endregion

//        #region Properties

//        #region HideMissingStoreSpace property

//        /// <summary>
//        /// Hide all items that are missing full store space info
//        /// </summary>
//        public Boolean HideMissingStoreSpace
//        {
//            get { return _hideMissingStoreSpace; }
//            set
//            {
//                _hideMissingStoreSpace = value;
//            }
//        }

//        #endregion

//        #region ShowOnlyAssignedStoreSpacePublishedItemData property

//        /// <summary>
//        /// Show Only all items which have an assigned plan
//        /// </summary>
//        public Boolean ShowOnlyAssignedStoreSpacePublishedItemData
//        {
//            get { return _showOnlyAssignedStoreSpacePublishedItemData; }
//            set
//            {
//                _showOnlyAssignedStoreSpacePublishedItemData = value;

//                //HideAssignedStoreSpacePublishedItemData cannot be true if 
//                //ShowOnlyAssignedStoreSpacePublishedItemData is true as one
//                //shows assined items while the other hides them!
//                if (HideAssignedStoreSpacePublishedItemData && value)
//                {
//                    HideAssignedStoreSpacePublishedItemData = false;
//                }
//            }
//        }

//        #endregion

//        #region HideAssignedStoreSpacePublishedItemData property

//        /// <summary>
//        /// Hide all items which have an assigned plan
//        /// </summary>
//        public Boolean HideAssignedStoreSpacePublishedItemData
//        {
//            get { return _hideAssignedStoreSpacePublishedItemData; }
//            set
//            {
//                _hideAssignedStoreSpacePublishedItemData = value;

//                //ShowInCompatibleStoreSpaceItemsData cannot be true if 
//                //HideAssignedStoreSpacePublishedItemData is true as one
//                //shows assined items while the other hides them!
//                if (ShowInCompatibleStoreSpaceItemsData && value)
//                {
//                    ShowInCompatibleStoreSpaceItemsData = false;
//                }

//                //ShowOnlyAssignedStoreSpacePublishedItemData cannot be true if 
//                //HideAssignedStoreSpacePublishedItemData is true as one
//                //shows assined items while the other hides them!             
//                if (ShowOnlyAssignedStoreSpacePublishedItemData && value)
//                {
//                    ShowOnlyAssignedStoreSpacePublishedItemData = false;
//                }
//            }
//        }

//        #endregion

//        #region ShowInCompatibleStoreSpaceItemsData property

//        /// <summary>
//        /// Show only incompatible store space items. These are items which have had a plan
//        /// assigned to them but the store space info does not match the assigned plan.
//        /// </summary>
//        public Boolean ShowInCompatibleStoreSpaceItemsData
//        {
//            get { return _showInCompatibleStoreSpaceItemsData; }
//            set
//            {

//                _showInCompatibleStoreSpaceItemsData = value;

//                //HideAssignedStoreSpacePublishedItemData cannot be true if 
//                //ShowInCompatibleStoreSpaceItemsData is true as one
//                //shows assined items while the other hides them!
//                if (HideAssignedStoreSpacePublishedItemData && value)
//                {
//                    HideAssignedStoreSpacePublishedItemData = false;
//                }

//                //ShowOnlyAssignedStoreSpacePublishedItemData must be true if 
//                //ShowInCompatibleStoreSpaceItemsData is true as the latter
//                //shows a subset of all assined items!             
//                if (!ShowOnlyAssignedStoreSpacePublishedItemData && value)
//                {
//                    ShowOnlyAssignedStoreSpacePublishedItemData = true;
//                }
//            }
//        }

//        #endregion

//        #endregion
//    }
//}
