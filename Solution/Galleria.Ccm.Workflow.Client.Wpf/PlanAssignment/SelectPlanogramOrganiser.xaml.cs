﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-25879 : N.Haywood
//	Created
#endregion

#region Version History: (CCM 8.2.0)
// V8-30732 : M.Shelley
//  Advanced planogram assignment enhancement - allow multiple plans to be assigned to a given location.
//  Modified to include the planogram selector window and a list of selected planograms
//  allowing addition / removal of selected planograms
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion


#region Version History: (CCM 8.3.0)
//V8-31832 : L.Ineson
//  Added support for calculated columns.
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    /// <summary>
    /// Interaction logic for SelectPlanogramOrganiser.xaml
    /// </summary>
    public partial class SelectPlanogramOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        private ColumnLayoutManager _columnLayoutManager;
        private ColumnLayoutManager _columnLayoutManager2;

        const String _screenKey = "PlanRepository.Workflow";
        const CustomColumnLayoutType _columnLayoutType = CustomColumnLayoutType.Planogram;
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(SelectPlanogramViewModel), typeof(SelectPlanogramOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (SelectPlanogramOrganiser)obj;

            // Detach previous viemodel if there was one.
            if (e.OldValue != null)
            {
                SelectPlanogramViewModel oldModel = e.OldValue as SelectPlanogramViewModel;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                SelectPlanogramViewModel newModel = e.NewValue as SelectPlanogramViewModel;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
            senderControl.UpdateBreadcrumb();
        }

        public SelectPlanogramViewModel ViewModel
        {
            get { return (SelectPlanogramViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public SelectPlanogramOrganiser(SelectPlanogramViewModel view)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = view;

            this.Loaded += PlanogramSelectorWindow_Loaded;
        }

        private void PlanogramSelectorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramSelectorWindow_Loaded;

            // Initialize the custom column manager.
            DisplayUnitOfMeasureCollection uom = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(CCMClient.ViewState.EntityId);
            PlanogramInfoColumnLayoutFactory factory = new PlanogramInfoColumnLayoutFactory();

            _columnLayoutManager = new ColumnLayoutManager(factory, uom,_screenKey, /*pathMask*/"Model.{0}");
            _columnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(xPlanogramGrid);

            _columnLayoutManager2 = new ColumnLayoutManager(factory, uom, _screenKey, /*pathMask*/"Model.{0}");
            _columnLayoutManager2.CalculatedColumnPath = "CalculatedValueResolver";
            _columnLayoutManager2.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager2.AttachDataGrid(xSelectedPlanogramGrid);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }
        #endregion

        #region Event Handlers

        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Prefix with additional column:

            //add hardcoded name column.
            DataGridExtendedTextColumn nameGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramNameColumnHeader,
                Binding = new Binding("Model.Name"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = false,
                MinWidth = 50,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(0, nameGridCol);
        }

        /// <summary>
        ///     Handles the change of selection for the current <see cref="PlanogramInfo" />.
        /// </summary>
        private void ViewModel_PropertyChanged(Object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == SelectPlanogramViewModel.SelectedPlanogramGroupProperty.Path)
            {
                UpdateBreadcrumb();
            }
        }

        

        #region XPlanogramGrid handlers


        /// <summary>
        /// Called whenever a planogram row is right clicked.
        /// </summary>
        private void xPlanogramGrid_RowItemMouseRightClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel == null) { return; }

            //show the row context menu
            var contextMenu = new Fluent.ContextMenu();

            AddCommandToMenu(contextMenu, this.ViewModel.RefreshPlanogramsCommand);

            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
            contextMenu.IsOpen = true;
        }

        /// <summary>
        /// Called whenever a planogram is double clicked in the top planogram finder grid to add the selected planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XPlanogramGrid_OnRowItemMouseDoubleClick(Object sender, ExtendedDataGridItemEventArgs e)
        {
            if (ViewModel.AddSelectedCommand.CanExecute())
            {
                ViewModel.AddSelectedCommand.Execute();
            }
        }

        /// <summary>
        /// Adds selected item to xSelectedPlanogramGrid on Enter key pressed
        /// Also gives focus to xSelecetedPlanogram grid when space-bar pressed for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XPlanogramGrid_OnKeyDown(Object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && ViewModel.AddSelectedCommand.CanExecute())
            {
                ViewModel.AddSelectedCommand.Execute();
                e.Handled = true;
            }
            
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(xSelectedPlanogramGrid);
            }

            else
            {
                return;
            }
        }

        /// <summary>
        /// Called when a planogram is double clicked in the bottom selected planograms grid to remove the selected planogram
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSelectedPlanogramGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (ViewModel.RemoveSelectedCommand.CanExecute())
            {
                ViewModel.RemoveSelectedCommand.Execute();
            }
        }

        /// <summary>
        /// Removes selected item from xSelectedPlanogramGrid when Enter is pressed and keeps focus on window
        /// Also gives focus back to xPlanogramGrid window when space-bar pressed for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xSelectedPlanogramGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && ViewModel.RemoveSelectedCommand.CanExecute())
            {
                ViewModel.RemoveSelectedCommand.Execute();
                Keyboard.Focus(xSelectedPlanogramGrid);
            }

            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(xPlanogramGrid);
            }

            else
            {
                return;
            }
        }

        #endregion

        /// <summary>
        /// Called whenever a breadcrumb is clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Breadcrumb_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel == null) return;

            TextBlock breadcrumb = (TextBlock)sender;
            PlanogramGroup group = breadcrumb.Tag as PlanogramGroup;
            if (group != null)
            {
                this.ViewModel.SelectedPlanogramGroup = group;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Adds the given command to the context menu, using its Friendly Name and Small Icon.
        /// </summary>
        private void AddCommandToMenu(Fluent.ContextMenu contextMenu, RelayCommand command)
        {
            contextMenu.Items.Add(new Fluent.MenuItem
            {
                Command = command,
                Header = command.FriendlyName,
                //Icon = command.SmallIcon
            });
        }


        /// <summary>
        /// Updates the breadcurb area
        /// </summary>
        private void UpdateBreadcrumb()
        {
            if (this.BreadcrumbStackPanel == null) return;

            //clear out any existing items.
            foreach (TextBlock tb in this.BreadcrumbStackPanel.Children.OfType<TextBlock>())
            {
                tb.MouseLeftButtonDown -= Breadcrumb_MouseLeftButtonDown;
            }
            this.BreadcrumbStackPanel.Children.Clear();

            //load in the path for the selected plan group
            if (this.ViewModel != null && this.ViewModel.SelectedPlanogramGroup != null)
            {
                //add in the icon
                var icon = new System.Windows.Controls.Image()
                {
                    Height = 15,
                    Width = 15,
                    Source = Galleria.Ccm.Common.Wpf.Resources.ImageResources.TreeViewItem_OpenFolder,
                    Margin = new Thickness(1, 1, 4, 1),
                    VerticalAlignment = System.Windows.VerticalAlignment.Center
                };
                this.BreadcrumbStackPanel.Children.Add(icon);

                Style itemStyle = (Style)this.Resources["PlanogramSelector_StyBreadcrumbItem"];
                Style sepStyle = (Style)this.Resources["PlanogramSelector_StyBreadcrumbSeparator"];

                //load in the breadcrumb parts
                foreach (var group in this.ViewModel.SelectedPlanogramGroup.FetchFullPath())
                {
                    TextBlock breadcrumb = new TextBlock();
                    breadcrumb.Style = itemStyle;
                    breadcrumb.Text = group.Name;
                    breadcrumb.Tag = group;
                    breadcrumb.MouseLeftButtonDown += Breadcrumb_MouseLeftButtonDown;
                    this.BreadcrumbStackPanel.Children.Add(breadcrumb);

                    Path separator = new Path();
                    separator.Style = sepStyle;
                    this.BreadcrumbStackPanel.Children.Add(separator);
                }
            }
        }

        #endregion

        #region Window Close

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {

                    IDisposable dis = this.ViewModel;

                    _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                    _columnLayoutManager.Dispose();


                    _columnLayoutManager2.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                    _columnLayoutManager2.Dispose();

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion      
    }
}
