﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25879 : N.Haywood
//	Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    /// <summary>
    /// Interaction logic for PlanAssignmentSidePanel.xaml
    /// </summary>
    public partial class PlanAssignmentSidePanel : UserControl
    {
        #region Properties
        #region ViewModel Property

        /// <summary>
        ///     Viewmodel controller for <see cref="PlanAssignmentSidePanel"/>.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof(PlanAssignmentViewModel), typeof(PlanAssignmentSidePanel),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanAssignmentSidePanel)obj;

            if (e.OldValue != null)
            {
                var oldValue = (PlanAssignmentViewModel)e.OldValue;
                //oldValue.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue == null) return;

            var newValue = (PlanAssignmentViewModel)e.NewValue;
            //newValue.PropertyChanged += senderControl.ViewModel_PropertyChanged;
        }

        /// <summary>
        ///     Gets and sets the viewmodel controller for the <see cref="PlanAssignmentSidePanel"/>.
        /// </summary>
        public PlanAssignmentViewModel ViewModel
        {
            get { return (PlanAssignmentViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        public PlanAssignmentSidePanel()
        {
            InitializeComponent();
        }

        private void planAssignmentSidePanel_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.StartPolling();
            }
        }

        void PlanAssignmentSidePanel_Unloaded(object sender, RoutedEventArgs e)
        {
            // The view has been unloaded, so stop polling
            if (this.ViewModel != null)
            {
                this.ViewModel.StopPollingAsync();
            }
        }

        #region Event Handlers

        #endregion
    }
}
