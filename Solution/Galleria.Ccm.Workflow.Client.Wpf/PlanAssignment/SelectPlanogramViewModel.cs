﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-25879 : N.Haywood
//	Created
#endregion

#region Version History: (CCM 8.2.0)
// V8-30732 : M.Shelley
//  Advanced planogram assignment enhancement - allow multiple plans to be assigned to a given location.
//  The common planogram selector has been copied and modified to add a list of selected planograms to 
//  allow addition / removal of selected planograms.
#endregion


#region Version History: (CCM 8.3.0)
//V8-31832 : L.Ineson
//  Added support for calculated columns.
//V8-32052 : L.Ineson
//  Added support for multiselection.
// V8-32652 : A.Silva
//  Refactored Add Remove Selected commands to use the common icons.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    public class SelectPlanogramViewModel : ViewModelAttachedControlObject<SelectPlanogramOrganiser>
	{
        #region Fields

        private Boolean? _dialogResult;
        private Int32 _currentEntityId;
        private String _custColumnPath;

        private PlanogramGroup _selectedPlanogramGroup;
        private readonly PlanogramHierarchyViewModel _masterPlanogramHierarchyView = new PlanogramHierarchyViewModel();
        private readonly UserPlanogramGroupListViewModel _userFavouritePlanGroupsView = new UserPlanogramGroupListViewModel();
        private String _planogramSearchCriteria;
        private readonly PlanogramInfoListViewModel _planogramInfoListViewModel = new PlanogramInfoListViewModel();
        private readonly BulkObservableCollection<PlanogramInfoView> _availablePlans = new BulkObservableCollection<PlanogramInfoView>();
        private readonly ReadOnlyBulkObservableCollection<PlanogramInfoView> _availablePlansRO;
        private readonly ObservableCollection<PlanogramInfoView> _selectedAvailablePlans = new ObservableCollection<PlanogramInfoView>();

        private readonly ObservableCollection<PlanogramInfoView> _assignedPlans = new ObservableCollection<PlanogramInfoView>();
        private readonly ObservableCollection<PlanogramInfoView> _selectedAssignedPlans = new ObservableCollection<PlanogramInfoView>();
  
        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath PlanogramHierarchyViewProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.PlanogramHierarchyView);
        public static readonly PropertyPath SelectedPlanogramGroupProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.SelectedPlanogramGroup);
        public static readonly PropertyPath UserFavouriteGroupsViewProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.UserFavouriteGroupsView);
        public static readonly PropertyPath PlanogramSearchCriteriaProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.PlanogramSearchCriteria);
        public static readonly PropertyPath AvailablePlanogramInfosProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.AvailablePlanogramInfos);
        public static readonly PropertyPath SelectedAvailablePlansProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.SelectedAvailablePlans);
        public static readonly PropertyPath AssignedPlansProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.AssignedPlans);
        public static readonly PropertyPath SelectedAssignedPlansProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.SelectedAssignedPlans);
        public static readonly PropertyPath SelectedCountProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.SelectedCount);

        //Commands
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.OKCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.CancelCommand);
        public static readonly PropertyPath RefreshPlanogramsCommandProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.RefreshPlanogramsCommand);
        public static readonly PropertyPath AddSelectedCommandProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.AddSelectedCommand);
        public static readonly PropertyPath RemoveSelectedCommandProperty = WpfHelper.GetPropertyPath<SelectPlanogramViewModel>(p => p.RemoveSelectedCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            internal set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Returns the path of the custom column
        /// layout directory.
        /// </summary>
        public String CustomColumnPath
        {
            get { return _custColumnPath; }
        }

        /// <summary>
        /// Returns the master planogram hierarchy view.
        /// </summary>
        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return _masterPlanogramHierarchyView; }
        }

        /// <summary>
        /// Gets/Sets the selected planogram group.
        /// </summary>
        public PlanogramGroup SelectedPlanogramGroup
        {
            get { return _selectedPlanogramGroup; }
            set
            {
                _selectedPlanogramGroup = value;
                OnPropertyChanged(SelectedPlanogramGroupProperty);
                OnSelectedPlanogramGroupChanged();
            }
        }

        /// <summary>
        /// Returns the view of the current user favourite groups.
        /// </summary>
        public UserPlanogramGroupListViewModel UserFavouriteGroupsView
        {
            get { return _userFavouritePlanGroupsView; }
        }

        /// <summary>
        /// Gets/Sets the search criteria to use when fetching planograms.
        /// </summary>
        public String PlanogramSearchCriteria
        {
            get { return _planogramSearchCriteria; }
            set
            {
                _planogramSearchCriteria = value;
                OnPropertyChanged(PlanogramSearchCriteriaProperty);

                OnPlanogramSearchCriteriaChanged();
            }
        }

        /// <summary>
        /// Returns the collection of available planograms
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramInfoView> AvailablePlanogramInfos
        {
            get { return _availablePlansRO; }
        }

        /// <summary>
        /// Returns the subset of available plans that have been selected by the user.
        /// </summary>
        public ObservableCollection<PlanogramInfoView> SelectedAvailablePlans
        {
            get { return _selectedAvailablePlans; }
        }

        /// <summary>
        /// Returns the collection of plans that have been assigned.
        /// </summary>
        public ObservableCollection<PlanogramInfoView> AssignedPlans
        {
            get { return _assignedPlans; }
        }

        /// <summary>
        /// Returns the subset of assigned plans that have been selected.
        /// </summary>
        public ObservableCollection<PlanogramInfoView> SelectedAssignedPlans
        {
            get { return _selectedAssignedPlans; }
        }

        public Int32 SelectedCount
        {
            get 
            {
                return (_assignedPlans == null ? 0 : _assignedPlans.Count()); 
            }
        }

        #endregion

        #region Commands

        #region OK command

        private RelayCommand _okCommand;

        /// <summary>
        /// OKs this window.
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OkCommand_Executed(),
                        p=> OkCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);

                }
                return _okCommand;
            }
        }

        private Boolean OkCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            if (!_assignedPlans.Any())
            {
                disabledReason = Message.Generic_NoItemSelected;
                enableCommand= false;
            }

            _okCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void OkCommand_Executed()
        {
            this.DialogResult = true;
        }

        #endregion

        #region Cancel command

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels this window.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed())
                        {
                            FriendlyName = Message.Generic_Cancel
                        };
                    base.ViewModelCommands.Add(_cancelCommand);

                }
                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #region RefreshPlanogramsCommand

        private RelayCommand _refreshPlanogramsCommand;

        /// <summary>
        /// Refreshes the current available planograms
        /// </summary>
        public RelayCommand RefreshPlanogramsCommand
        {
            get
            {
                if (_refreshPlanogramsCommand == null)
                {
                    _refreshPlanogramsCommand = new RelayCommand(
                        p => RefreshPlanograms_Executed(),
                        p => RefreshPlanograms_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanogramSelector_Refresh,
                        SmallIcon = Galleria.Ccm.Common.Wpf.Resources.ImageResources.PlanogramSelector_Refresh16,
                        InputGestureKey = Key.F5,
                    };
                    base.ViewModelCommands.Add(_refreshPlanogramsCommand);

                }
                return _refreshPlanogramsCommand;
            }
        }

        private Boolean RefreshPlanograms_CanExecute()
        {
            if (this.SelectedPlanogramGroup == null)
            {
                return false;
            }

            return true;
        }

        private void RefreshPlanograms_Executed()
        {
            UpdateAvailablePlanogramInfos();
        }

        #endregion

        #region AddSelectedCommand

        private RelayCommand _addSelectedCommand;

        /// <summary>
        /// Add selected items on the plan selector window
        /// </summary>
        public RelayCommand AddSelectedCommand
        {
            get
            {
                if (_addSelectedCommand != null) return _addSelectedCommand;

                _addSelectedCommand =
                    new RelayCommand(p => AddSelectedCommand_Executed(), p => AddSelectedCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_AddSelectedItems,
                        Icon = ImageResources.Add_32
                    };
                ViewModelCommands.Add(_addSelectedCommand);
                return _addSelectedCommand;
            }
        }

        private Boolean AddSelectedCommand_CanExecute()
        {
            //must have plans selected
            if (!this.SelectedAvailablePlans.Any())
            {
                this.AddSelectedCommand.DisabledReason = Message.PlanAssignment_PlanSelector_AddItemsWarning;
                return false;
            }

            //must not already be selected
            if (this.SelectedAvailablePlans.Count == 1
                && _assignedPlans.Any(x => x.Id == this.SelectedAvailablePlans[0].Id))
            {
                this.AddSelectedCommand.DisabledReason = Message.PlanAssignment_PlanSelector_ItemAlreadyAdded;
                return false;
            }

            // ToDo: Check if the plan selected by the user has already been assigned previously
            // but not in the set of currently selected rows for the given 
            // Location / ProductGroup depending on the view

            return true;
        }

        private void AddSelectedCommand_Executed()
        {
            IEnumerable<PlanogramInfoView> plansToAdd = this.SelectedAvailablePlans.ToList();

            //add the plan if its not already assigned
            foreach (PlanogramInfoView selectedPlan in plansToAdd)
            {
                if (!_assignedPlans.Any(x => x.Id == selectedPlan.Id))
                {
                    _assignedPlans.Add(selectedPlan);
                }
            }

            OnPropertyChanged(SelectedCountProperty);
            if(this.SelectedAvailablePlans.Any()) this.SelectedAvailablePlans.Clear();
        }

        #endregion

        #region RemoveSelectedCommand

        private RelayCommand _removeSelectedCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand RemoveSelectedCommand
        {
            get
            {
                if (_removeSelectedCommand != null) return _removeSelectedCommand;

                _removeSelectedCommand =
                    new RelayCommand(p => RemoveSelectedCommand_Executed(), p => RemoveSelectedCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_RemoveSelectedItems,
                        Icon = ImageResources.Delete_32,
                    };
                ViewModelCommands.Add(_removeSelectedCommand);
                return _removeSelectedCommand;
            }
        }

        private Boolean RemoveSelectedCommand_CanExecute()
        {
            //must have something selected.
            if (!this.SelectedAssignedPlans.Any())
            {
                this.RemoveSelectedCommand.DisabledReason = Message.PlanAssignment_PlanSelector_SelectItemRemove;
                return false;
            }

            return true;
        }

        private void RemoveSelectedCommand_Executed()
        {
            IEnumerable<PlanogramInfoView> plansToRemove = this.SelectedAssignedPlans.ToList();
            foreach (PlanogramInfoView plan in plansToRemove)
            {
                _assignedPlans.Remove(plan);

            }
            OnPropertyChanged(SelectedCountProperty);
            if (this.SelectedAssignedPlans.Any()) this.SelectedAssignedPlans.Clear();
        }

        #endregion

        #region RemoveAllCommand

        private RelayCommand _removeAllCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand RemoveAllCommand
        {
            get
            {
                if (_removeAllCommand == null)
                {
                    _removeAllCommand = new RelayCommand(
                        p => RemoveAllCommand_Executed(),
                        p => RemoveAllCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_RemoveAllItems,
                        Icon = ImageResources.PlanAssignment_SelectorRemoveAll,
                    };
                    base.ViewModelCommands.Add(_removeAllCommand);
                }
                return _removeAllCommand;
            }
        }

        private bool RemoveAllCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            if (_assignedPlans == null || !_assignedPlans.Any())
            {
                disabledReason = Message.PlanAssignment_PlanSelector_SelectItemRemove;
                enableCommand = false;
            }

            _removeAllCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void RemoveAllCommand_Executed()
        {
            if (RemoveAllCommand.CanExecute())
            {
                _assignedPlans.Clear();
                OnPropertyChanged(SelectedCountProperty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Create a viewmodel for use with the SelectPlanogramOrganiser window
        /// </summary>
        /// <param name="custColumnPath">The directory path for the custom column definitions</param>
        /// <param name="entityId">The id of the current entity</param>
        /// <param name="hierarchyView">A populated instance of the planogram hierarchy viewmodel</param>
        /// <param name="groupToSelect">The planogram group to select</param>
        /// <param name="assignedPlanInfoList">A list of the plan assignment rows the user has selected</param>
        public SelectPlanogramViewModel(String custColumnPath, Int32 entityId, PlanogramHierarchyViewModel hierarchyView,
                                        PlanogramGroup groupToSelect, IEnumerable<PlanAssignmentRowViewModel> assignedPlanInfoList)
        {
            _availablePlansRO = new ReadOnlyBulkObservableCollection<PlanogramInfoView>(_availablePlans);
            _planogramInfoListViewModel.ModelChanged += PlanogramInfoListViewModel_ModelChanged;

            _custColumnPath = custColumnPath;
            _currentEntityId = entityId;

            if (hierarchyView == null) return;

            // Load the hierarchy model.
            _masterPlanogramHierarchyView = hierarchyView;
            if (_masterPlanogramHierarchyView.Model == null) _masterPlanogramHierarchyView.FetchForCurrentEntity();

            // Set the selected group.
            this.SelectedPlanogramGroup = (groupToSelect != null) ? groupToSelect : _masterPlanogramHierarchyView.Model.RootGroup;

            // Add any assigned planograms to the selected list
            if (assignedPlanInfoList != null && assignedPlanInfoList.Any())
            {
                foreach (var planInfoItem in assignedPlanInfoList)
                {
                    var isThere = AssignedPlans.Any(x => planInfoItem.PlanogramInfo != null && x.Id == planInfoItem.PlanogramInfo.Id);
                    if (!isThere)
                    {
                        AssignedPlans.Add(new PlanogramInfoView(planInfoItem.PlanogramInfo));
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the planogram 
        /// </summary>
        private void OnPlanogramSearchCriteriaChanged()
        {
            UpdateAvailablePlanogramInfos();
        }

        /// <summary>
        /// Called whenever the value of the SelectedPlanogramGroup changes
        /// </summary>
        /// <param name="value"></param>
        private void OnSelectedPlanogramGroupChanged()
        {
            UpdateAvailablePlanogramInfos();
        }

        /// <summary>
        /// Called whenever the planogram info list viewmodel changes.
        /// </summary>
        private void PlanogramInfoListViewModel_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<PlanogramInfoList> e)
        {
            if (_availablePlans.Any()) _availablePlans.Clear();

            if (e.NewModel != null)
            {
                _availablePlans.AddRange(e.NewModel.Select(p => new PlanogramInfoView(p)));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the collection of available planograms.
        ///     If the root planogram group is selected, use the search filter and look recursively down 
        ///     the planogram group hierarchy for the specified search term
        ///     
        ///     If the root planogram group is selected and no search term is specified, only show plans from 
        ///     the root planogram group
        ///     
        ///     If the user has a sub group selected, do not search down the planogram group hierarchy even if 
        ///     there is a search term specified
        /// </summary>
        private void UpdateAvailablePlanogramInfos()
        {
            PlanogramGroup selectedGroup = this.SelectedPlanogramGroup;

            if (selectedGroup != null)
            {                
                //start an async search.
                // Check if the root planogram group is selected, in which case we need to allow searching down the planogram group hierarchy

                Int32? searchPlanGroupId = selectedGroup.Id;
                if (selectedGroup.ParentGroup == null && !String.IsNullOrEmpty(PlanogramSearchCriteria))
                {
                    searchPlanGroupId = null;
                }
                _planogramInfoListViewModel.FetchBySearchCriteriaAsync(this.PlanogramSearchCriteria, searchPlanGroupId, _currentEntityId);
            }
            else
            {
                _planogramInfoListViewModel.Model = null;
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                _planogramInfoListViewModel.ModelChanged -= PlanogramInfoListViewModel_ModelChanged;
                _planogramInfoListViewModel.Dispose();
                _userFavouritePlanGroupsView.Dispose();
            }

            IsDisposed = true;
        }

        #endregion
    }
}
