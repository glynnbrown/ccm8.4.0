﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-25879 : N.Haywood
//	Created
// V8-28282 : L.Ineson
// Amended how columns are created.
#endregion

#region Version History: (CCM 8.0.1)
// V8-28481 : M.Shelley
//  Added published status and location published display column
#endregion

#region Version History: (CCM 8.1.0)
// V8-28242 : M.Shelley
//  Added "right-click" date property editor for the plan assignment grid rows
// V8-30001 : M.Shelley
//  Changed the right click mouse behaviour to use the RowItemMouseRightClick rather than the data grid 
//  PreviewMouseRightButtonDown otherwise the column header context menu becomes unavailable
// V8-29999 : M.Shelley
//  Prevent the "Edit custom column layout" context menu item appearing multiple times
#endregion

#region Version History: (CCM 8.1.0)
// V8-30283 : M.Shelley
//  Only load the grid column set when the window is instanciated, not each time it is loaded.
//  Otherwise the users' column settings get over ridden and any values entered in the column filter 
//  text boxes are not displayed when the user switches back to the tab - even though the filter
//  is still active
#endregion

#region Version History: (CCM 8.1.1)

// V8-30469 : A.Silva
//  Amended PathMask value assigned in PlanAssignmentOrganiser_FirstLoaded when intializing the ColumnLayoutManager so that the path is not trimmed.

#endregion

#region Version History: (CCM 8.2.0)
// V8-30308 : M.Shelley
//  Hide the "Plan Publish Status" column and rename the "Plan Assignment Status" to "Plan Publish Status"
// V8-30734 : M.Shelley
//  Added helpfile key for plan assignment
//V8-30958 : L.Ineson
//  Updated after column manager changes.
#endregion

#region Version History: CCM830
//V8-31832 : L.Ineson
//  Added support for calculated columns
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Fluent;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Converters;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanAssignment
{
    /// <summary>
    /// Interaction logic for PlanAssignmentOrganiser.xaml
    /// </summary>
    public sealed partial class PlanAssignmentOrganiser
    {
        #region Constants

        public static readonly String PlanPublishStatusTemplateName = "PlanPublishStatusTpl";
        public static readonly String PlanAssignStatusTemplateName = "PlanAssignStatusTpl";

        public const CustomColumnLayoutType ColumnLayoutType = CustomColumnLayoutType.DataSheet;

        #endregion

        #region Fields

        private readonly PlanAssignmentSidePanel _sidePanel;
        private readonly List<RibbonTabItem> _ribbonTabList;
        private GridLength _bottomPanelHeight = new GridLength(420.0);

        private Dictionary<String, DataTemplate> _dataTemplates = new Dictionary<String, DataTemplate>();

        private ColumnLayoutManager _columnLayoutManager;

        private Boolean _isGfsConfigured = false;

        #endregion

        #region Properties

        public PlanAssignmentSidePanel SidePanel
        {
            get { return _sidePanel; }
        }

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanAssignmentViewModel), typeof(PlanAssignmentOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanAssignmentOrganiser)obj;

            PlanAssignmentViewModel oldModel = e.OldValue as PlanAssignmentViewModel;
            if (oldModel != null)
            {
                oldModel.AttachedControl = null;
                oldModel.ExportDataCommand.Executed -= senderControl.ViewModel_ExportDataCommandExecuted;
            }

            PlanAssignmentViewModel newModel = e.NewValue as PlanAssignmentViewModel;
            if (newModel != null)
            {
                newModel.AttachedControl = senderControl;
                newModel.ExportDataCommand.Executed += senderControl.ViewModel_ExportDataCommandExecuted;

                // Check if we need to add data templates

                if (!senderControl.DataTemplates.Any())
                {
                    //// Set up the Plan Publish Status data template
                    //DataTemplate planPublishStatusTemplate = senderControl.FindResource(PlanPublishStatusTemplateName) as DataTemplate;
                    //if (planPublishStatusTemplate != null)
                    //{
                    //    senderControl.DataTemplates.Add(PlanPublishStatusTemplateName, planPublishStatusTemplate);
                    //}

                    // Set up the Plan Publish Status data template
                    DataTemplate planAssignStatusTemplate = senderControl.FindResource(PlanAssignStatusTemplateName) as DataTemplate;
                    if (planAssignStatusTemplate != null)
                    {
                        senderControl.DataTemplates.Add(PlanAssignStatusTemplateName, planAssignStatusTemplate);
                    }
                }
            }
        }

        

        /// <summary>
        /// A collection of data template available from attached controls
        /// </summary>
        public Dictionary<String, DataTemplate> DataTemplates
        {
            get { return _dataTemplates; }
        }

        /// <summary>
        ///     Gets or sets the viewmodel controller for <see cref="PlanRepositoryOrganiser"/>.
        /// </summary>
        public PlanAssignmentViewModel ViewModel
        {
            get { return (PlanAssignmentViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanAssignmentOrganiser()
        {
            InitializeComponent();

            // Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanAssignment);

            // create the ribbon & side panel 
            PlanAssignmentHomeTab _ribbonTab = new PlanAssignmentHomeTab();
            BindingOperations.SetBinding(_ribbonTab, PlanAssignmentHomeTab.ViewModelProperty, new Binding(ViewModelProperty.Name) { Source = this });
            _ribbonTabList = new List<RibbonTabItem>() { _ribbonTab };

            _sidePanel = new PlanAssignmentSidePanel();
            BindingOperations.SetBinding(_sidePanel, PlanAssignmentSidePanel.ViewModelProperty, new Binding(ViewModelProperty.Name) { Source = this });

            Loaded += PlanAssignmentOrganiser_FirstLoaded;
            Loaded += PlanAssignmentOrganiser_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        private void PlanAssignmentOrganiser_FirstLoaded(Object sender, RoutedEventArgs e)
        {
            Loaded -= PlanAssignmentOrganiser_FirstLoaded;

            //Load the viewmodel if we have not already done so.
            if (ViewModel == null) this.ViewModel = new PlanAssignmentViewModel();

            // Initialize the custom column manager.
            _columnLayoutManager = new ColumnLayoutManager(
                new PlanAssignmentColumnLayoutFactory(),
                DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId),
                PlanAssignmentViewModel.ScreenKey);

            //turned off as this is causing performance issues.
            _columnLayoutManager.IncludeUnseenColumns = false;

            _columnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
            _columnLayoutManager.RegisterDataTemplate<PlanAssignmentRowViewModel>(CreateDataTemplate);
            _columnLayoutManager.ColumnSetChanging += ColumnLayoutManager_ColumnSetChanging;
            _columnLayoutManager.AttachDataGrid(xAssignmentGrid);
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Carries out actions whenever this screen loads.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanAssignmentOrganiser_Loaded(Object sender, RoutedEventArgs e)
        {
            // send ribbon request
            App.ViewState.ShowRibbonTabs(_ribbonTabList);
            // send sidepanel request
            App.ViewState.ShowSidePanel(_sidePanel);
        }

        /// <summary>
        /// Called when the viewmodel ExportDataCommand is executed.
        /// As the export is of the datagrid we need to do the actual work here.
        /// </summary>
        private void ViewModel_ExportDataCommandExecuted(object sender, EventArgs e)
        {
            String initialFileName = String.Empty;
            if (this.ViewModel.CurrentViewType == PlanAssignmentViewType.ViewCategory
                && this.ViewModel.SelectedProductGroup != null)
            {
                initialFileName =
                    String.Format(Message.PlanAssignment_ExportData_FileName,
                    this.ViewModel.SelectedProductGroup.Code + " " + this.ViewModel.SelectedProductGroup.Name);
            }
            else if(this.ViewModel.CurrentViewType == PlanAssignmentViewType.ViewLocation)
            {
                initialFileName =
                    String.Format(Message.PlanAssignment_ExportData_FileName,
                    this.ViewModel.SelectedLocation.Code + " " + this.ViewModel.SelectedLocation.Name);
            }

            ExtendedDataGrid.ExportDataGridToExcel(this.xAssignmentGrid, initialFileName);
        }


        private void ColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //prefix the set with additional columns.
            Int32 curIdx = 0;

            // Get the PublishStatus data template
            //DataTemplate planPublishTemplate = null;
            //this.DataTemplates.TryGetValue(PlanPublishStatusTemplateName, out planPublishTemplate);

            //// Plan Publish Status - hidden (see V8-30308)
            //DataGridExtendedTemplateColumn publishStatusCol = new DataGridExtendedTemplateColumn();
            //publishStatusCol.Header = Message.PlanAssignmentHeader_PublishingStatus;
            //publishStatusCol.IsReadOnly = true;
            //publishStatusCol.ColumnGroupName = "Plan Status";
            //publishStatusCol.SortMemberPath = PlanAssignmentRowViewModel.FriendlyPublishStatusProperty.Path;
            //publishStatusCol.CanUserFilter = true;
            //publishStatusCol.CanUserSort = true;
            //publishStatusCol.CanUserResize = true;

            //publishStatusCol.CellTemplate = planPublishTemplate;
            //ExtendedDataGrid.SetColumnCellAlignment(publishStatusCol, HorizontalAlignment.Center);

            //_columnSet.Add(publishStatusCol);

            DataTemplate planAssignTemplate = null;
            this.DataTemplates.TryGetValue(PlanAssignStatusTemplateName, out planAssignTemplate);

            // Plan Location Publish Status
            DataGridExtendedTemplateColumn planAssignStatus = new DataGridExtendedTemplateColumn();
            planAssignStatus.Header = Message.PlanAssignmentHeader_PublishingStatus;
            planAssignStatus.IsReadOnly = true;
            planAssignStatus.ColumnGroupName = "Plan Status";
            planAssignStatus.SortMemberPath = PlanAssignmentRowViewModel.FriendlyAssignStatusProperty.Path;
            planAssignStatus.CanUserFilter = true;
            planAssignStatus.CanUserSort = true;
            planAssignStatus.CanUserResize = true;

            planAssignStatus.CellTemplate = planAssignTemplate;
            ExtendedDataGrid.SetColumnCellAlignment(planAssignStatus, HorizontalAlignment.Center);

            columnSet.Insert(curIdx++, planAssignStatus);

        }


        private void XBottomPanelExpander_OnCollapsed(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetRow((Expander)sender);
            var row = xBaseGrid.RowDefinitions[index];
            _bottomPanelHeight = row.Height;
            row.Height = GridLength.Auto;
        }

        private void XBottomPanelExpander_OnExpanded(Object sender, RoutedEventArgs e)
        {
            var index = Grid.GetRow((Expander)sender);
            var row = xBaseGrid.RowDefinitions[index];
            row.Height = _bottomPanelHeight;
        }

        private void XAssignmentGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                ViewModel.AssignPlanogramsCommand.Execute();
            }
        }

        /// <summary>
        /// Allow the user to right click on the datagrid rows and display the date editor window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void xAssignmentGrid_RowItemMouseRightClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            // First check if there are any selected rows - if not exit gracefully
            if (ViewModel.PlanAssignmentSelectedItems == null || ViewModel.PlanAssignmentSelectedItems.Count == 0)
            {
                return;
            }

            // First check the selected column(s) have at least one plan assignment
            var planSelectCheckList = ViewModel.PlanAssignmentSelectedItems.Where(f => !String.IsNullOrEmpty(f.PlanName)).ToList();
            if (!planSelectCheckList.Any())
            {
                return;
            }

            //Boolean gfsConnected = _isGfsConfigured == EntityViewModel.GFSConnectionState.Successful);

            // Check if there are GFS web service details
            _isGfsConfigured = CheckGFSDetails();

            // First work out if there is a single date for the live and communicated dates to display in the date editor
            // If the selected items have multiple dates then we cannot display these
            DateTime? dateCommuncatedDisplay = null;

            var distinctCommunicated = ViewModel.PlanAssignmentSelectedItems.Where(x => x.LocationPlanAssignment != null && x.LocationPlanAssignment.DateCommunicated != null)
                            .Select(x => x.LocationPlanAssignment.DateCommunicated).Distinct().ToList();
            if (distinctCommunicated.Count() == 1)
            {
                dateCommuncatedDisplay = distinctCommunicated.First();
            }

            DateTime? dateLiveDisplay = null;

            var distinctLive = ViewModel.PlanAssignmentSelectedItems.Where(x => x.LocationPlanAssignment != null && x.LocationPlanAssignment.DateLive != null)
                            .Select(x => x.LocationPlanAssignment.DateLive).Distinct().ToList();
            if (distinctLive.Count() == 1)
            {
                dateLiveDisplay = distinctLive.First();
            }

            // Create the viewmodel and open the properties window
            PlanAssignmentDateEditorViewModel dateEditViewModel = 
                new PlanAssignmentDateEditorViewModel(_isGfsConfigured, ViewModel.PlanAssignmentSelectedItems.Count,
                                                        dateCommuncatedDisplay, dateLiveDisplay);
            CommonHelper.GetWindowService().ShowDialog<PlanAssignmentDateEditorWindow>(dateEditViewModel);

            if (dateEditViewModel.DialogResult == true && !_isGfsConfigured)
            {
                // The user clicked OK and we are not connected to GFS, so update the 
                // date communicated and date live values in the selected plan assignment rows.
                ViewModel.UpdateDates(dateEditViewModel.DateCommunicated, dateEditViewModel.DateLive);
            }
        }

        /// <summary>
        /// Mimics mouse double-click event when return key pressed to assign
        /// and gives focus to the Bottom Expander when Space-Bar pressed so Planogram 
        /// details of currently selected item can be viewed (for 508 compliance)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignmentGrid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && xAssignmentGrid.SelectedItem != null)
            {
                if (this.ViewModel != null)
                {
                    ViewModel.AssignPlanogramsCommand.Execute();
                    e.Handled = true;
                }
            }

            else if (e.Key == Key.Space && xAssignmentGrid.SelectedItem != null)
            {
                Keyboard.Focus(XBottomPanelExpander);
            }

            else
            {
                return;
            }
        }

        /// <summary>
        /// Expands and collapses Bottom Expander using Return key for 508 compliance
        /// Also gives focus back to xAssignmentGrid when Space-Bar pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XBottomPanelExpander_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (!XBottomPanelExpander.IsExpanded)
                {
                    XBottomPanelExpander.IsExpanded = true;
                }

                else
                {
                    XBottomPanelExpander.IsExpanded = false;
                }
            }
            else if (e.Key == Key.Space)
            {
                Keyboard.Focus(xAssignmentGrid);
            }

            else
            {
                return;
            }
        }

        #endregion

        #region Methods

        private Boolean CheckGFSDetails()
        {
            Boolean connectDetailsSet = false;
            var curEntityView = App.ViewState.CurrentEntityView;

            if (curEntityView != null && curEntityView.Model.SystemSettings != null)
            {
                connectDetailsSet = (!String.IsNullOrEmpty(curEntityView.Model.SystemSettings.FoundationServicesEndpoint));
            }

            return connectDetailsSet;
        }


        /// <summary>
        /// Setup the data grid columns
        /// </summary>
        //private void LoadStoreSpacePublishedItemsColumnSetRenamed()
        //{
        //    var dg = this.xAssignmentGrid;
        //    if (dg == null) return;

        //    // Get the PublishStatus data template
        //    DataTemplate planPublishTemplate = null;
        //    this.DataTemplates.TryGetValue(PlanPublishStatusTemplateName, out planPublishTemplate);

        //    DataTemplate planAssignTemplate = null;
        //    this.DataTemplates.TryGetValue(PlanAssignStatusTemplateName, out planAssignTemplate);

        //    dg.Columns.Clear();

        //    DataGridColumn col;
        //    DisplayUnitOfMeasureCollection uoms = App.ViewState.DisplayUnits;

        //    // Plan Publish Status
        //    DataGridExtendedTemplateColumn publishStatusCol = new DataGridExtendedTemplateColumn();
        //    publishStatusCol.Header = Message.PlanAssignmentHeader_PublishingStatus;
        //    publishStatusCol.IsReadOnly = true;
        //    publishStatusCol.ColumnGroupName = Message.PlanAssignment_ColGroup_PlanogramAttributes;
        //    publishStatusCol.SortMemberPath = PlanAssignmentRowViewModel.FriendlyPublishStatusProperty.Path;
        //    publishStatusCol.CanUserFilter = true;
        //    publishStatusCol.CanUserSort = true;
        //    publishStatusCol.CanUserResize = true;

        //    publishStatusCol.CellTemplate = planPublishTemplate;
        //    ExtendedDataGrid.SetColumnCellAlignment(publishStatusCol, HorizontalAlignment.Center);

        //    dg.Columns.Add(publishStatusCol);

        //    // Plan Location Publish Status
        //    DataGridExtendedTemplateColumn planAssignStatus = new DataGridExtendedTemplateColumn();
        //    planAssignStatus.Header = Message.PlanAssignmentHeader_LocationPublishingStatus;
        //    planAssignStatus.IsReadOnly = true;
        //    planAssignStatus.ColumnGroupName = Message.PlanAssignment_ColGroup_PlanogramAttributes;
        //    planAssignStatus.SortMemberPath = PlanAssignmentRowViewModel.FriendlyAssignStatusProperty.Path;
        //    planAssignStatus.CanUserFilter = true;
        //    planAssignStatus.CanUserSort = true;
        //    planAssignStatus.CanUserResize = true;

        //    planAssignStatus.CellTemplate = planAssignTemplate;
        //    ExtendedDataGrid.SetColumnCellAlignment(planAssignStatus, HorizontalAlignment.Center);

        //    dg.Columns.Add(planAssignStatus);

        //    ////LocationCode
        //    //col = ExtendedDataGrid.CreateReadOnlyTextColumn(
        //    //        Message.PlanAssignmentHeader_LocationCode,
        //    //        PlanAssignmentRowViewModel.LocationCodeProperty.Path,
        //    //        HorizontalAlignment.Left,
        //    //        Framework.Controls.Wpf.InputType.None,
        //    //        Message.PlanAssignment_ColGroup_LocationAttributes);
        //    //col.SortDirection = ListSortDirection.Ascending;
        //    //dg.Columns.Add(col);
            
        //    //LocationName
        //    //dg.Columns.Add(
        //    //    ExtendedDataGrid.CreateReadOnlyTextColumn(
        //    //        Message.PlanAssignmentHeader_LocationName,
        //    //        PlanAssignmentRowViewModel.LocationNameProperty.Path,
        //    //        HorizontalAlignment.Left,
        //    //        Framework.Controls.Wpf.InputType.None,
        //    //        Message.PlanAssignment_ColGroup_LocationAttributes));

        //    //StoreSpaceBayCount
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //        PlanAssignmentRowViewModel.StoreSpaceBayCountProperty.Path, typeof(Int32),
        //        Message.PlanAssignmentHeader_StoreSpaceBayCount,
        //        ModelPropertyDisplayType.None,
        //        uoms, Message.PlanAssignment_ColGroup_LocationAttributes));

        //    //StoreSpaceBayTotalWidth
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //        PlanAssignmentRowViewModel.StoreSpaceBayTotalWidthProperty.Path, typeof(Single),
        //        Message.PlanAssignmentHeader_StoreSpaceBayTotalWidth,
        //        ModelPropertyDisplayType.Length,
        //        uoms, Message.PlanAssignment_ColGroup_LocationAttributes));

        //    //ProductGroupCode
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //      PlanAssignmentRowViewModel.ProductGroupCodeProperty.Path, typeof(String),
        //      Message.PlanAssignmentHeader_ProductGroupCode,
        //      ModelPropertyDisplayType.None,
        //      uoms, Message.PlanAssignment_ColGroup_CategoryAttributes));


        //    //ProductGroupName
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //      PlanAssignmentRowViewModel.ProductGroupNameProperty.Path, typeof(String),
        //      Message.PlanAssignmentHeader_ProductGroupName,
        //      ModelPropertyDisplayType.None,
        //      uoms, Message.PlanAssignment_ColGroup_CategoryAttributes));


        //    //Plan Name
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //       PlanAssignmentRowViewModel.PlanNameProperty.Path, typeof(String),
        //       Message.PlanAssignmentHeader_PlanName,
        //       ModelPropertyDisplayType.None,
        //       uoms, Message.PlanAssignment_ColGroup_PlanogramAttributes));


        //    //Plan Bay Count
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //        PlanAssignmentRowViewModel.PlanBayCountProperty.Path, typeof(Int32), 
        //        Message.PlanAssignmentHeader_PlanBayCount, 
        //        ModelPropertyDisplayType.None,
        //        uoms, Message.PlanAssignment_ColGroup_PlanogramAttributes));

        //    //PlanTotalWidth
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //        PlanAssignmentRowViewModel.PlanTotalWidthProperty.Path, typeof(Single),
        //        Message.PlanAssignmentHeader_PlanTotalWidth,
        //        ModelPropertyDisplayType.Length,
        //        uoms, Message.PlanAssignment_ColGroup_PlanogramAttributes));

        //    //PlanType
        //    //_locationSpaceLocationAssignmentItemColumnSet.Add(
        //    //    ExtendedDataGrid.CreateReadOnlyNumericColumn(
        //    //        Message.PlanAssignmentHeader_PlanType,
        //    //        PlanAssignmentRowViewModel.PlanTypeProperty.Path,
        //    //        HorizontalAlignment.Left,
        //    //        false,
        //    //        PlanogramAttributesName));

        //    //PlanAssignedByUser
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //     PlanAssignmentRowViewModel.PlanAssignedByUserProperty.Path, typeof(String),
        //     Message.PlanAssignmentHeader_PlanAssignedByUser,
        //     ModelPropertyDisplayType.None,
        //     uoms, Message.PlanAssignment_ColGroup_PlanogramAttributes));

        //    //PlanAssignedDate
        //    DataGridDateTimeColumn assignDateColumn = 
        //        CommonHelper.CreateReadOnlyColumn(
        //            PlanAssignmentRowViewModel.PlanAssignedDateProperty.Path, typeof(DateTime),
        //            Message.PlanAssignmentHeader_PlanAssignedDate,
        //            ModelPropertyDisplayType.None,
        //            uoms, Message.PlanAssignment_ColGroup_PlanogramAttributes) as DataGridDateTimeColumn;
        //    assignDateColumn.SelectedDateFormat = DatePickerFormat.Short;
        //    dg.Columns.Add(assignDateColumn);


        //    //PlanPublishUser
        //    dg.Columns.Add(CommonHelper.CreateReadOnlyColumn(
        //     PlanAssignmentRowViewModel.PlanPublishUserProperty.Path, typeof(String),
        //     Message.PlanAssignmentHeader_PlanPublishUser,
        //     ModelPropertyDisplayType.None,
        //     uoms, Message.PlanAssignment_ColGroup_PlanogramAttributes));


        //    //PlanPublishDate
        //    DataGridDateTimeColumn publishDateColumn =
        //        CommonHelper.CreateReadOnlyColumn(
        //            PlanAssignmentRowViewModel.PlanPublishDateProperty.Path, typeof(DateTime),
        //            Message.PlanAssignmentHeader_PlanPublishDate,
        //            ModelPropertyDisplayType.None,
        //            uoms, Message.PlanAssignment_ColGroup_PlanogramAttributes) as DataGridDateTimeColumn;
        //    publishDateColumn.SelectedDateFormat = DatePickerFormat.Short;
        //    dg.Columns.Add(publishDateColumn);
        //}

        ///// <summary>
        ///// Utility method to create a DataGridDateTimeColumn column and configure the 
        ///// binding to converter between UTC and Local time
        ///// N.B. 
        ///// </summary>
        ///// <param name="bindingPath">The binding property path for the column</param>
        ///// <returns>The new DataGridDateTimeColumn instance</returns>
        //private DataGridDateTimeColumn CreateDateTimeColumn(String header, String bindingPath, String groupName)
        //{
        //    DataGridDateTimeColumn newDateColumn = null;

        //    // Test for running as a unit test as the DataGridDateTimeColumn constructor blows up
        //    // when running under nunit
        //    if (Application.Current != null)
        //    {
        //        newDateColumn = new DataGridDateTimeColumn();
        //        newDateColumn.Header = header;
        //        newDateColumn.IsReadOnly = true;
        //        newDateColumn.SelectedDateFormat = DatePickerFormat.Short;
        //        newDateColumn.ColumnGroupName = groupName;
        //        ExtendedDataGrid.SetColumnCellAlignment(newDateColumn, HorizontalAlignment.Center);

        //        Binding bind = new Binding(bindingPath);
        //        bind.Mode = BindingMode.OneWay;
        //        bind.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
        //        bind.Converter = new Galleria.Framework.Controls.Wpf.Converters.UTCToLocalDateConverter();
        //        newDateColumn.Binding = bind;
        //    }

        //    return newDateColumn;
        //}
        #endregion

        #region Custom columns


        public static DataTemplate CreateDataTemplate(ObjectFieldInfo field, DisplayUnitOfMeasureCollection uomCol)
        {
            var dataTemplate = new DataTemplate(typeof(PlanAssignmentRowViewModel));
            var panelElement = new FrameworkElementFactory(typeof(DockPanel));
            panelElement.SetValue(DockPanel.LastChildFillProperty, true);
            panelElement.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Stretch);

            panelElement.AppendChild(CreateIconElement(field));
            panelElement.AppendChild(CreateTextBlockElement(field, uomCol));

            dataTemplate.VisualTree = panelElement;

            return dataTemplate;
        }

        private static FrameworkElementFactory CreateIconElement(ObjectFieldInfo field)
        {
            var elementFactory = new FrameworkElementFactory(typeof(System.Windows.Controls.Image));
            var elementSize = new Size(16, 16);
            var marginThickness = new Thickness(3, 0, 3, 0);
            var multiBinding = new MultiBinding { Converter = new DictionaryKeyToValueConverter() };
            multiBinding.Bindings.Add(new Binding { Source = field.PropertyName });
            multiBinding.Bindings.Add(new Binding("MetricValidationResultTypes"));

            elementFactory.SetBinding(System.Windows.Controls.Image.SourceProperty, multiBinding);
            elementFactory.SetValue(FrameworkElement.WidthProperty, elementSize.Width);
            elementFactory.SetValue(FrameworkElement.HeightProperty, elementSize.Height);
            elementFactory.SetValue(FrameworkElement.MarginProperty, marginThickness);
            return elementFactory;
        }

        private static FrameworkElementFactory CreateTextBlockElement(ObjectFieldInfo field,
            DisplayUnitOfMeasureCollection uomCol)
        {
            var valueBinding = new Binding("Info." + field.PropertyName);
            var marginThickness = new Thickness(3, 0, 3, 0);
            var elementFactory = new FrameworkElementFactory(typeof(TextBlock));
            elementFactory.SetBinding(TextBlock.TextProperty, valueBinding);
            elementFactory.SetValue(TextBlock.TextAlignmentProperty, TextAlignment.Right);
            elementFactory.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Stretch);
            elementFactory.SetValue(FrameworkElement.MarginProperty, marginThickness);
            uomCol.ApplyConverter(valueBinding, field.PropertyDisplayType, field.PropertyType);
            return elementFactory;
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (_isDisposed) return;

            Loaded -= PlanAssignmentOrganiser_Loaded;

            xAssignmentGrid.RowItemMouseRightClick -= xAssignmentGrid_RowItemMouseRightClick;

            ViewModel.Dispose();
            ViewModel = null;

            if (_columnLayoutManager != null)
            {
                _columnLayoutManager.ColumnSetChanging -= ColumnLayoutManager_ColumnSetChanging;
                _columnLayoutManager.Dispose();
            }

            _isDisposed = true;
        }

        #endregion
    }
}
