﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25451 : N.Haywood
//	Added from SA
#endregion
#endregion

using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence
{
    /// <summary>
    /// Interaction logic for ProductMaintenanceBackstageOpen.xaml
    /// </summary>
    public sealed partial class ProductMaintenanceBackstageOpen : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductMaintenanceViewModel), typeof(ProductMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public ProductMaintenanceViewModel ViewModel
        {
            get { return (ProductMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ProductMaintenanceBackstageOpen()
        {

            InitializeComponent();
        }

        #endregion

        #region Event Handler

        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                ProductInfo productToOpen = (ProductInfo)senderControl.SelectedItem;

                if (productToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(productToOpen.Id);
                }
            }
        }

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var selected = searchResultsListBox.SelectedItem as ProductInfo;
            if(selected != null)
                this.ViewModel.OpenCommand.Execute(selected.Id);
        }

        #endregion
    }
}
