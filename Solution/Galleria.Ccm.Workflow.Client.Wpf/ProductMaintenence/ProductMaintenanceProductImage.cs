﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26217 : A.Kuszyk
//  Created (copied from Galleria.Ccm.Editor.Client.Wpf).
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence
{
    /// <summary>
    /// Control used to display and manage a product image.
    /// </summary>
    public sealed class ProductMaintenanceProductImage : Control
    {
        #region Properties

        #region Header Property

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(String), typeof(ProductMaintenanceProductImage),
            new PropertyMetadata(String.Empty));

        /// <summary>
        /// Gets/Sets the image header text.
        /// </summary>
        public String Header
        {
            get { return (String)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        #endregion

        #region ImageName

        public static readonly DependencyProperty ImageNameProperty =
            DependencyProperty.Register("ImageName", typeof(String), typeof(ProductMaintenanceProductImage),
            new PropertyMetadata(String.Empty));

        /// <summary>
        /// Gets/Sets the name of the image this represents.
        /// </summary>
        public String ImageName
        {
            get { return (String)GetValue(ImageNameProperty); }
            set { SetValue(ImageNameProperty, value); }
        }

        #endregion

        #region SourceProperty

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(ImageSource), typeof(ProductMaintenanceProductImage),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the image source
        /// </summary>
        public ImageSource Source
        {
            get { return (ImageSource)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }

        #endregion

        #region IsLoading Property

        public static readonly DependencyProperty IsLoadingProperty =
            DependencyProperty.Register("IsLoading", typeof(Boolean), typeof(ProductMaintenanceProductImage),
            new PropertyMetadata(true));

        /// <summary>
        /// Gets/Sets whether the image loading busy should be displayed.
        /// </summary>
        public Boolean IsLoading
        {
            get { return (Boolean)GetValue(IsLoadingProperty); }
            set { SetValue(IsLoadingProperty, value); }
        }

        #endregion

        #region ImageType

        public static readonly DependencyProperty ImageTypeProperty = 
            DependencyProperty.Register("ImageType", typeof(ProductImageType), typeof(ProductMaintenanceProductImage),
            new PropertyMetadata(ProductImageType.None));

        /// <summary>
        /// The type of image being displayed.
        /// </summary>
        public ProductImageType ImageType
        {
            get { return (ProductImageType)GetValue(ImageTypeProperty); }
            set { SetValue(ImageTypeProperty, value); }
        }

        #endregion

        #region FacingType

        public static readonly DependencyProperty FacingTypeProperty =
            DependencyProperty.Register("FacingType", typeof(ProductImageFacing), typeof(ProductMaintenanceProductImage),
            new PropertyMetadata(ProductImageFacing.Front));

        /// <summary>
        /// The facing of image being displayed.
        /// </summary>
        public ProductImageFacing FacingType
        {
            get { return (ProductImageFacing)GetValue(FacingTypeProperty); }
            set { SetValue(FacingTypeProperty, value); }
        }

        #endregion

        #endregion

        #region Events

        #region EditRequested

        public static readonly RoutedEvent EditRequestedEvent =
            EventManager.RegisterRoutedEvent("EditRequested", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ProductMaintenanceProductImage));

        public event RoutedEventHandler EditRequested
        {
            add { AddHandler(EditRequestedEvent, value); }
            remove { RemoveHandler(EditRequestedEvent, value); }
        }

        private void OnEditRequested()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(ProductMaintenanceProductImage.EditRequestedEvent));
        }

        #endregion

        #region ClearRequested

        public static readonly RoutedEvent ClearRequestedEvent =
            EventManager.RegisterRoutedEvent("ClearRequested", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ProductMaintenanceProductImage));

        public event RoutedEventHandler ClearRequested
        {
            add { AddHandler(ClearRequestedEvent, value); }
            remove { RemoveHandler(ClearRequestedEvent, value); }
        }

        private void OnClearRequested()
        {
            //raise out the selected event
            RaiseEvent(new RoutedEventArgs(ProductMaintenanceProductImage.ClearRequestedEvent));
        }

        #endregion

        #endregion

        #region Constructor

        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline",
            Justification = "Dependency properties are initialized in-line.")]
        static ProductMaintenanceProductImage()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(ProductMaintenanceProductImage), new FrameworkPropertyMetadata(typeof(ProductMaintenanceProductImage)));
        }

        public ProductMaintenanceProductImage() { }

        #endregion

        #region Commands

        #region EditCommand

        private RelayCommand _editCommand;

        /// <summary>
        /// Triggers the event to allow the image to be set.
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                {
                    _editCommand = new RelayCommand(
                        p => Edit_Executed(),
                        p => Edit_CanExecute())
                    {
                        FriendlyName = Message.ProductMaintenance_ProductImage_Edit, 
                        SmallIcon = ImageResources.ProductMaintenance_ProductImage_Edit
                    };
                }
                return _editCommand;
            }
        }

        private Boolean Edit_CanExecute()
        {
            if (this.IsLoading) { return false; }
            return true;
        }

        private void Edit_Executed()
        {
            OnEditRequested();
        }

        #endregion

        #region ClearCommand

        private RelayCommand _clearCommand;

        public RelayCommand ClearCommand
        {
            get
            {
                if (_clearCommand == null)
                {
                    _clearCommand = new RelayCommand(
                        p => Clear_Executed(),
                        p => Clear_CanExecute())
                    {
                        FriendlyName = Message.ProductMaintenance_ProductImage_Clear,
                        SmallIcon = ImageResources.ProductMaintenance_ProductImage_Clear
                    };
                }
                return _clearCommand;
            }
        }

        private Boolean Clear_CanExecute()
        {
            if (this.IsLoading) { return false; }
            if (this.Source == null) { return false; }
            return true;
        }

        private void Clear_Executed()
        {
            OnClearRequested();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out actions on mouse double click.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            OnEditRequested();
        }

        #endregion
    }
}
