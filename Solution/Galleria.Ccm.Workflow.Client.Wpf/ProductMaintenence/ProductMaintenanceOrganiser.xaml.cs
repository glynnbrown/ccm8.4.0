﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25451 : N.Haywood
//	Added from SA
// V8-26217 : A.Kuszyk
//  Added handlers for ProductMaintenanceProductImage.
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence
{
    /// <summary>
    /// Interaction logic for ProductMaintenanceOrganiser.xaml
    /// </summary>
    public partial class ProductMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        public ObservableCollection<Byte> _numPegHoles = new ObservableCollection<Byte>();
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductMaintenanceViewModel), typeof(ProductMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ProductMaintenanceViewModel ViewModel
        {
            get { return (ProductMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductMaintenanceOrganiser senderControl = (ProductMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                ProductMaintenanceViewModel oldModel = (ProductMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ProductMaintenanceViewModel newModel = (ProductMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }

        #endregion

        #region NumPegHoles Property

        public ObservableCollection<Byte> NumPegHoles
        {
            get { return _numPegHoles; }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ProductMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ProductMaintenance);

            this.ViewModel = new ProductMaintenanceViewModel();

            _numPegHoles.Add(0);
            _numPegHoles.Add(1);
            _numPegHoles.Add(2);
            _numPegHoles.Add(3);

            this.AddHandler(ProductMaintenanceProductImage.EditRequestedEvent, (RoutedEventHandler)OnProductImageEditRequested);
            this.AddHandler(ProductMaintenanceProductImage.ClearRequestedEvent, (RoutedEventHandler)OnProductImageClearRequested);

            this.Loaded += new RoutedEventHandler(ProductMaintenanceOrganiser_Loaded);

        }


        private void ProductMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProductMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }


        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        /// <summary>
        /// Called when the user requests to edit a product image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProductImageEditRequested(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                ProductMaintenanceProductImage senderControl = (ProductMaintenanceProductImage)e.OriginalSource;
                var dlg = new System.Windows.Forms.OpenFileDialog();
                //String defaultImageDirectory = App.ViewState.Settings.Model.ImageLocation;
                //if (Directory.Exists(defaultImageDirectory))
                //{
                //    dlg.InitialDirectory = defaultImageDirectory;
                //}
                //dlg.Filter = Message.PlanItemProperties_SelectProductImageFilter;
                dlg.Multiselect = false;
                dlg.CheckFileExists = true;
                if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;
                this.ViewModel.SetProductImage(dlg.FileName, senderControl.ImageType, senderControl.FacingType);
            }
        }
            
        /// <summary>
        /// Called when the user requests to clear a product image
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProductImageClearRequested(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                ProductMaintenanceProductImage senderControl = (ProductMaintenanceProductImage)e.OriginalSource;
                this.ViewModel.ClearProductImage(senderControl.ImageType,senderControl.FacingType);
            }
        }

        #endregion

        #region Window Close


        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion

    }

    #region Version History: (GFS 1.0)
    // GFS-14156 : N.Donohoe
    //  Created
    #endregion
    public class CustomAttributeDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;

            if (element != null && item != null)
            {
                //if (item is ProductMaintenanceCustomAttribute)
                //{
                //    ProductMaintenanceCustomAttribute attribute = item as ProductMaintenanceCustomAttribute;

                //    switch (attribute.AttributeDataType)
                //    {
                //        case ProductAttributeDataType.Boolean:
                //            return element.FindResource("ProductMaintenance_DTCustomAttributeBoolean") as DataTemplate;
                //        case ProductAttributeDataType.Currency:
                //            return element.FindResource("ProductMaintenance_DTCustomAttributeCurrency") as DataTemplate;
                //        case ProductAttributeDataType.Decimal:
                //            return element.FindResource("ProductMaintenance_DTCustomAttributeDecimal") as DataTemplate;
                //        case ProductAttributeDataType.Integer:
                //            return element.FindResource("ProductMaintenance_DTCustomAttributeInteger") as DataTemplate;
                //        case ProductAttributeDataType.Text:
                //            return element.FindResource("ProductMaintenance_DTCustomAttributeText") as DataTemplate;
                //    }
                //}
            }

            return null;
        }
    }
}
