﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25451 : N.Haywood
//	Added from SA
// V8-26217 : A.Kuszyk
//  Added image properties and SetProductImage/ClearProductImage.
#endregion
#region Version History: (CCM 8.1.1)
// V8-30325 : I.George
//  Added friendl description SaveAs Command and Delete Command
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence
{
    public sealed class ProductMaintenanceViewModel : ViewModelAttachedControlObject<ProductMaintenanceOrganiser>, IDataErrorInfo
    {
        #region Fields

        const String _exCategory = "ProductMaintenance";

        //permissions
        private Boolean _userHasProductCreatePerm;
        private Boolean _userHasProductFetchPerm;
        private Boolean _userHasProductEditPerm;
        private Boolean _userHasProductDeletePerm;
        private Boolean _userHasProductAttributeCreatePerm;
        private Boolean _userHasProductAttributeDeletePerm;

        //product related
        private String _productSearchCriteria;
        private Boolean _isProductSearchQueued;
        private Boolean _isProcessingProductSearch;
        private readonly ProductInfoListViewModel _productInfoListView = new ProductInfoListViewModel();
        private Product _selectedProduct;

        //attribute related
        //private readonly ProductAttributeListViewModel _masterAttributeListView = new ProductAttributeListViewModel();
        //private ProductAttribute _newAttribute;
        //private readonly BulkObservableCollection<ProductMaintenanceCustomAttribute> _attributeValueViews = new BulkObservableCollection<ProductMaintenanceCustomAttribute>();
        //private ReadOnlyBulkObservableCollection<ProductMaintenanceCustomAttribute> _attributeValueViewsRO;

        #endregion

        #region Binding Property paths

        //properties
        public static readonly PropertyPath ProductSearchCriteriaProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.ProductSearchCriteria);
        public static readonly PropertyPath IsProcessingProductSearchProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.IsProcessingProductSearch);
        public static readonly PropertyPath ProductSearchResultsProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.ProductSearchResults);
        public static readonly PropertyPath SelectedProductProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.SelectedProduct);
        //public static readonly PropertyPath AvailableAttributesProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.AvailableAttributes);
        //public static readonly PropertyPath CurrentEditingAttributeProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.CurrentEditingAttribute);
        //public static readonly PropertyPath AttributeValuesProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.AttributeValues);
        public static readonly PropertyPath MinDeepProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.MinDeep);
        public static readonly PropertyPath MaxDeepProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.MaxDeep);

        // Image properties.
        public static readonly PropertyPath NoneFrontImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.NoneFrontImage);
        public static readonly PropertyPath NoneBackImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.NoneBackImage);
        public static readonly PropertyPath NoneTopImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.NoneTopImage);
        public static readonly PropertyPath NoneBottomImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.NoneBottomImage);
        public static readonly PropertyPath NoneLeftImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.NoneLeftImage);
        public static readonly PropertyPath NoneRightImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.NoneRightImage);
        public static readonly PropertyPath DisplayFrontImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.DisplayFrontImage);
        public static readonly PropertyPath DisplayBackImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.DisplayBackImage);
        public static readonly PropertyPath DisplayTopImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.DisplayTopImage);
        public static readonly PropertyPath DisplayBottomImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.DisplayBottomImage);
        public static readonly PropertyPath DisplayLeftImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.DisplayLeftImage);
        public static readonly PropertyPath DisplayRightImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.DisplayRightImage);
        public static readonly PropertyPath PointOfPurchaseFrontImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.PointOfPurchaseFrontImage);
        public static readonly PropertyPath PointOfPurchaseBackImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.PointOfPurchaseBackImage);
        public static readonly PropertyPath PointOfPurchaseTopImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.PointOfPurchaseTopImage);
        public static readonly PropertyPath PointOfPurchaseBottomImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.PointOfPurchaseBottomImage);
        public static readonly PropertyPath PointOfPurchaseLeftImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.PointOfPurchaseLeftImage);
        public static readonly PropertyPath PointOfPurchaseRightImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.PointOfPurchaseRightImage);
        public static readonly PropertyPath UnitFrontImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.UnitFrontImage);
        public static readonly PropertyPath UnitBackImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.UnitBackImage);
        public static readonly PropertyPath UnitTopImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.UnitTopImage);
        public static readonly PropertyPath UnitBottomImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.UnitBottomImage);
        public static readonly PropertyPath UnitLeftImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.UnitLeftImage);
        public static readonly PropertyPath UnitRightImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.UnitRightImage);
        public static readonly PropertyPath TrayFrontImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.TrayFrontImage);
        public static readonly PropertyPath TrayBackImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.TrayBackImage);
        public static readonly PropertyPath TrayTopImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.TrayTopImage);
        public static readonly PropertyPath TrayBottomImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.TrayBottomImage);
        public static readonly PropertyPath TrayLeftImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.TrayLeftImage);
        public static readonly PropertyPath TrayRightImageProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.TrayRightImage);
        

        //commands
        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.SaveAndNewCommand);
        //public static PropertyPath AddProductAttributeCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.AddProductAttributeCommand);
        //public static PropertyPath RemoveProductAttributeCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(p => p.RemoveProductAttributeCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<ProductMaintenanceViewModel>(v => v.CloseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the criteria to use when searching for products
        /// </summary>
        public String ProductSearchCriteria
        {
            get { return _productSearchCriteria; }
            set
            {
                _productSearchCriteria = value;
                OnPropertyChanged(ProductSearchCriteriaProperty);
                OnProductSearchCriteriaChanged();
            }
        }

        /// <summary>
        /// Returns true if a product search is currently being processed.
        /// </summary>
        public Boolean IsProcessingProductSearch
        {
            get { return _isProcessingProductSearch; }
            private set
            {
                _isProcessingProductSearch = value;
                OnPropertyChanged(IsProcessingProductSearchProperty);
            }
        }

        /// <summary>
        /// Return the collection of available products based on the search criteria
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> ProductSearchResults
        {
            get { return _productInfoListView.BindingView; }
        }

        /// <summary>
        /// Gets/Sets the selected product
        /// </summary>
        public Product SelectedProduct
        {
            get { return _selectedProduct; }
            private set
            {
                Product oldValue = _selectedProduct;

                _selectedProduct = value;
                OnPropertyChanged(MinDeepProperty);
                OnPropertyChanged(MaxDeepProperty);
                OnPropertyChanged(SelectedProductProperty);

                // Image changes.
                OnPropertyChanged(NoneFrontImageProperty);
                OnPropertyChanged(NoneBackImageProperty);
                OnPropertyChanged(NoneTopImageProperty);
                OnPropertyChanged(NoneBottomImageProperty);
                OnPropertyChanged(NoneLeftImageProperty);
                OnPropertyChanged(NoneRightImageProperty);
                OnPropertyChanged(DisplayRightImageProperty);
                OnPropertyChanged(DisplayFrontImageProperty);
                OnPropertyChanged(DisplayBackImageProperty);
                OnPropertyChanged(DisplayTopImageProperty);
                OnPropertyChanged(DisplayBottomImageProperty);
                OnPropertyChanged(DisplayLeftImageProperty);
                OnPropertyChanged(DisplayRightImageProperty);
                OnPropertyChanged(PointOfPurchaseFrontImageProperty);
                OnPropertyChanged(PointOfPurchaseBackImageProperty);
                OnPropertyChanged(PointOfPurchaseTopImageProperty);
                OnPropertyChanged(PointOfPurchaseBottomImageProperty);
                OnPropertyChanged(PointOfPurchaseLeftImageProperty);
                OnPropertyChanged(PointOfPurchaseRightImageProperty);
                OnPropertyChanged(UnitFrontImageProperty);
                OnPropertyChanged(UnitBackImageProperty);
                OnPropertyChanged(UnitTopImageProperty);
                OnPropertyChanged(UnitBottomImageProperty);
                OnPropertyChanged(UnitLeftImageProperty);
                OnPropertyChanged(UnitRightImageProperty);
                OnPropertyChanged(TrayFrontImageProperty);
                OnPropertyChanged(TrayBackImageProperty);
                OnPropertyChanged(TrayTopImageProperty);
                OnPropertyChanged(TrayBottomImageProperty);
                OnPropertyChanged(TrayLeftImageProperty);
                OnPropertyChanged(TrayRightImageProperty);
                
                OnSelectedProductChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the collection of available product attributes.
        /// </summary>
        /// <remarks>NB: this is exposed directly as it is ok to edit
        /// and the property changed will only be fired once.</remarks>
        //public ReadOnlyBulkObservableCollection<ProductAttribute> AvailableAttributes
        //{
        //    get
        //    {
        //        return _masterAttributeListView.BindingView;
        //    }
        //}

        /// <summary>
        /// The current product attribute loaded ready for adding.
        /// </summary>
        //public ProductAttribute CurrentEditingAttribute
        //{
        //    get { return _newAttribute; }
        //    private set
        //    {
        //        _newAttribute = value;
        //        OnPropertyChanged(CurrentEditingAttributeProperty);
        //    }
        //}

        /// <summary>
        /// Returns the collection of attribute columns
        /// </summary>
        //public ReadOnlyBulkObservableCollection<ProductMaintenanceCustomAttribute> AttributeValues
        //{
        //    get
        //    {
        //        if (_attributeValueViewsRO == null)
        //        {
        //            _attributeValueViewsRO = new ReadOnlyBulkObservableCollection<ProductMaintenanceCustomAttribute>(_attributeValueViews);
        //        }
        //        return _attributeValueViewsRO;
        //    }
        //}

        /// <summary>
        /// Returns the MinDeep of the product
        /// </summary>
        public Byte MinDeep
        {
            get { return _selectedProduct.MinDeep; }
            set
            {
                _selectedProduct.MinDeep = value;
                OnPropertyChanged(MinDeepProperty);
                OnPropertyChanged(MaxDeepProperty);
            }
        }

        /// <summary>
        /// Returns the MaxDeep of the product
        /// </summary>
        public Byte MaxDeep
        {
            get { return _selectedProduct.MaxDeep; }
            set
            {
                _selectedProduct.MaxDeep = value;
                OnPropertyChanged(MinDeepProperty);
                OnPropertyChanged(MaxDeepProperty);
            }
        }

        #region Image Properties

        #region None
        /// <summary>
        /// None Front Image
        /// </summary>
        public Byte[] NoneFrontImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.None && img.FacingType == ProductImageFacing.Front)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// None Back Image
        /// </summary>
        public Byte[] NoneBackImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.None && img.FacingType == ProductImageFacing.Back)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// None Top Image
        /// </summary>
        public Byte[] NoneTopImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.None && img.FacingType == ProductImageFacing.Top)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// None Bottom Image
        /// </summary>
        public Byte[] NoneBottomImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.None && img.FacingType == ProductImageFacing.Bottom)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// None Left Image
        /// </summary>
        public Byte[] NoneLeftImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.None && img.FacingType == ProductImageFacing.Left)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// None Right Image
        /// </summary>
        public Byte[] NoneRightImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.None && img.FacingType == ProductImageFacing.Right)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        } 
        #endregion

        #region Display
        /// <summary>
        /// Display Front Image
        /// </summary>
        public Byte[] DisplayFrontImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Display && img.FacingType == ProductImageFacing.Front)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Display Back Image
        /// </summary>
        public Byte[] DisplayBackImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Display && img.FacingType == ProductImageFacing.Back)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Display Top Image
        /// </summary>
        public Byte[] DisplayTopImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Display && img.FacingType == ProductImageFacing.Top)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Display Bottom Image
        /// </summary>
        public Byte[] DisplayBottomImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Display && img.FacingType == ProductImageFacing.Bottom)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Display Left Image
        /// </summary>
        public Byte[] DisplayLeftImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Display && img.FacingType == ProductImageFacing.Left)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Display Right Image
        /// </summary>
        public Byte[] DisplayRightImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Display && img.FacingType == ProductImageFacing.Right)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }
        #endregion

        #region PointOfPurchase
        /// <summary>
        /// PointOfPurchase Front Image
        /// </summary>
        public Byte[] PointOfPurchaseFrontImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.PointOfPurchase && img.FacingType == ProductImageFacing.Front)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// PointOfPurchase Back Image
        /// </summary>
        public Byte[] PointOfPurchaseBackImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.PointOfPurchase && img.FacingType == ProductImageFacing.Back)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// PointOfPurchase Top Image
        /// </summary>
        public Byte[] PointOfPurchaseTopImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.PointOfPurchase && img.FacingType == ProductImageFacing.Top)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// PointOfPurchase Bottom Image
        /// </summary>
        public Byte[] PointOfPurchaseBottomImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.PointOfPurchase && img.FacingType == ProductImageFacing.Bottom)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// PointOfPurchase Left Image
        /// </summary>
        public Byte[] PointOfPurchaseLeftImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.PointOfPurchase && img.FacingType == ProductImageFacing.Left)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// PointOfPurchase Right Image
        /// </summary>
        public Byte[] PointOfPurchaseRightImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.PointOfPurchase && img.FacingType == ProductImageFacing.Right)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }
        #endregion

        #region Unit
        /// <summary>
        /// Unit Front Image
        /// </summary>
        public Byte[] UnitFrontImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Unit && img.FacingType == ProductImageFacing.Front)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Unit Back Image
        /// </summary>
        public Byte[] UnitBackImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Unit && img.FacingType == ProductImageFacing.Back)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Unit Top Image
        /// </summary>
        public Byte[] UnitTopImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Unit && img.FacingType == ProductImageFacing.Top)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Unit Bottom Image
        /// </summary>
        public Byte[] UnitBottomImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Unit && img.FacingType == ProductImageFacing.Bottom)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Unit Left Image
        /// </summary>
        public Byte[] UnitLeftImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Unit && img.FacingType == ProductImageFacing.Left)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Unit Right Image
        /// </summary>
        public Byte[] UnitRightImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Unit && img.FacingType == ProductImageFacing.Right)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }
        #endregion

        #region Tray
        /// <summary>
        /// Tray Front Image
        /// </summary>
        public Byte[] TrayFrontImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Tray && img.FacingType == ProductImageFacing.Front)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Tray Back Image
        /// </summary>
        public Byte[] TrayBackImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Tray && img.FacingType == ProductImageFacing.Back)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Tray Top Image
        /// </summary>
        public Byte[] TrayTopImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Tray && img.FacingType == ProductImageFacing.Top)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Tray Bottom Image
        /// </summary>
        public Byte[] TrayBottomImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Tray && img.FacingType == ProductImageFacing.Bottom)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Tray Left Image
        /// </summary>
        public Byte[] TrayLeftImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Tray && img.FacingType == ProductImageFacing.Left)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }

        /// <summary>
        /// Tray Right Image
        /// </summary>
        public Byte[] TrayRightImage
        {
            get
            {
                var image = _selectedProduct.ImageList
                    .Where(img => img.ImageType == ProductImageType.Tray && img.FacingType == ProductImageFacing.Right)
                    .FirstOrDefault();
                if (image == null) return null;
                return image.ProductImageData.OriginalFile.FileBlob.Data;
            }
        }
        #endregion

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Testing Constructor
        /// </summary>
        public ProductMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            //products
            _productInfoListView.ModelChanged += ProductInfoListView_ModelChanged;

            //attributes
            //_attributeValueViews.BulkCollectionChanged += AttributeValueViews_BulkCollectionChanged;
            //_masterAttributeListView.BindingView.BulkCollectionChanged += AvailableProductAttributes_BulkCollectionChanged;
            //_masterAttributeListView.FetchAllForEntity();

            //_newAttribute = ProductAttribute.NewProductAttribute(App.ViewState.EntityId);

            //load a new item
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region New

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new item
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userHasProductCreatePerm)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            //warn the user if the current item is dirty
            if (ContinueWithItemChange())
            {
                //load a new item
                this.SelectedProduct = Product.NewProduct(App.ViewState.EntityId);

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }
        }

        #endregion

        #region Open

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Opens the product with the given id
        /// param: Int32 productId
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(p =>
                        Open_Executed(p), p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? productId)
        {
            //user must have get permission
            if (!_userHasProductFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //must not be null
            if (productId == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? productId)
        {
            //warn the user if the current item is dirty
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //fetch the item
                    this.SelectedProduct = Product.FetchById(productId.Value);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region Save

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasProductEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.SelectedProduct == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }


            //must be valid
            if (!this.SelectedProduct.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //if (AttributeValues != null)
            //{
            //    foreach (ProductMaintenanceCustomAttribute attribute in AttributeValues)
            //    {
            //        if (!attribute.IsValid)
            //        {
            //            this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
            //            this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
            //            this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
            //            return false;
            //        }
            //    }
            //}

            if (this.AttachedControl != null)
            {
                //bindings must be valid
                if (!LocalHelper.AreBindingsValid(this.AttachedControl))
                {
                    this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    return false;
                }
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //** check the item unique value
            String newCode = this.SelectedProduct.Gtin;
            Int32 productId = this.SelectedProduct.Id;

            Boolean codeAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       IEnumerable<ProductInfo> matchingCodeProducts =
                       ProductInfoList.FetchByEntityIdProductGtins(App.ViewState.EntityId,
                           new List<String>() { s })
                           .Where(p => p.Id != productId);

                       return !(matchingCodeProducts.Any());
                   };

                codeAccepted =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(String.Empty, String.Empty,
                    Message.Generic_CodeIsNotUnique, Product.GtinProperty.FriendlyName, 14,
                    /*forceFirstShow*/false, isUniqueCheck, this.SelectedProduct.Gtin, out newCode);
            }
            else
            {
                //force a unique value for unit testing
                IEnumerable<ProductInfo> matchingCodeProducts =
                       ProductInfoList.FetchByEntityIdProductGtins(App.ViewState.EntityId,
                           new List<String>() { this.SelectedProduct.Gtin })
                           .Where(p => p.Id != productId);

                if (matchingCodeProducts.Any())
                {
                    newCode = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedProduct.Gtin, new List<String>() { this.SelectedProduct.Gtin });
                }
            }

            //set the code
            if (codeAccepted)
            {
                if (this.SelectedProduct.Gtin != newCode)
                {
                    this.SelectedProduct.Gtin = newCode;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.SelectedProduct = this.SelectedProduct.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //[SA-17668] set return flag to false as save was not completed.
                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedProduct.Name);
                        if (itemReloadRequired)
                        {
                            this.SelectedProduct = Product.FetchById(this.SelectedProduct.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.SelectedProduct.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //nb can't update available infos as don't know what search criteria was.

                base.ShowWaitCursor(false);
            }

            return continueWithSave;

        }

        #endregion

        #region SaveAs

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }


        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_userHasProductCreatePerm)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.SelectedProduct == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedProduct.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the code to save as
            String copyCode;

            Boolean codeAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       IEnumerable<ProductInfo> matchingCodeProducts =
                       ProductInfoList.FetchByEntityIdProductGtins(App.ViewState.EntityId,
                           new List<String>() { s });

                       return !(matchingCodeProducts.Any());
                   };


                codeAccepted =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(Message.Generic_SaveAs, Message.Generic_SaveAs_CodeDescription,
                    Message.Generic_CodeIsNotUnique, Location.CodeProperty.FriendlyName, 50,
                    /*forceFirstShow*/true, isUniqueCheck, String.Empty, out copyCode);
            }
            else
            {
                //force a unique value for unit testing
                copyCode = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedProduct.Gtin, new List<String>() { this.SelectedProduct.Gtin });
            }

            if (codeAccepted)
            {
                base.ShowWaitCursor(true);

                //copy the item and save
                Product itemCopy = this.SelectedProduct.Copy();
                itemCopy.Gtin = copyCode;
                itemCopy = itemCopy.Save();

                //set the copy as the new selected item
                this.SelectedProduct = itemCopy;

                //nb can't update available infos as don't know what search criteria was.

                base.ShowWaitCursor(false);
            }

        }

        #endregion

        #region SaveAndClose

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }

        }

        #endregion

        #region SaveAndNew

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Saves the current item and creates a new one
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region Delete

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Deletes the current item and creates a new one
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_userHasProductDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.SelectedProduct == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.SelectedProduct.IsNew)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                //confirm the delete with the user
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedProduct.ToString());
            }


            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                Product itemToDelete = this.SelectedProduct;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }


                RefreshProductSearchResults();

                base.ShowWaitCursor(false);
            }

        }

        #endregion

        //#region AddProductAttribute

        //private RelayCommand _addProductAttributeCommand;

        ///// <summary>
        ///// Adds the current editing attribute to the attribute list
        ///// </summary>
        //public RelayCommand AddProductAttributeCommand
        //{
        //    get
        //    {
        //        if (_addProductAttributeCommand == null)
        //        {
        //            _addProductAttributeCommand = new RelayCommand(
        //                p => AddProductAttribute_Executed(),
        //                p => AddProductAttribute_CanExecute())
        //            {
        //                FriendlyDescription = Message.ProductMaintenance_AddProductAttribute,
        //                SmallIcon = ImageResources.ProductMaintenance_AddProductAttribute
        //            };
        //            base.ViewModelCommands.Add(_addProductAttributeCommand);
        //        }
        //        return _addProductAttributeCommand;
        //    }
        //}

        //[DebuggerStepThrough]
        //private Boolean AddProductAttribute_CanExecute()
        //{
        //    //user must have create permission
        //    if (!_userHasProductAttributeCreatePerm)
        //    {
        //        _addProductAttributeCommand.DisabledReason = Message.Generic_NoCreatePermission;
        //        return false;
        //    }

        //    //max 50 attributes
        //    if (this.AvailableAttributes.Count() >= 50)
        //    {
        //        _addProductAttributeCommand.DisabledReason = String.Empty;
        //        return false;
        //    }

        //    //name must be populated
        //    if (String.IsNullOrEmpty(this.CurrentEditingAttribute.Name))
        //    {
        //        _addProductAttributeCommand.DisabledReason = Message.ProductMaintenance_AddProductAttribute_DisabledNameIsEmpty;
        //        return false;
        //    }

        //    //name must be unique
        //    IEnumerable<String> attributeNames = this.AvailableAttributes.Select(a => a.Name);
        //    if (attributeNames.Contains(this.CurrentEditingAttribute.Name))
        //    {
        //        _addProductAttributeCommand.DisabledReason = Message.ProductMaintenance_AddProductAttribute_DisabledNonUniqueName;
        //        return false;
        //    }


        //    return true;
        //}

        //private void AddProductAttribute_Executed()
        //{
        //    base.ShowWaitCursor(true);

        //    ProductAttribute addItem = this.CurrentEditingAttribute;

        //    //load a new editing item
        //    this.CurrentEditingAttribute = ProductAttribute.NewProductAttribute(App.ViewState.EntityId);

        //    //set the column number
        //    IEnumerable<Byte> takenColumnNumbers = this.AvailableAttributes.Select(a => a.ColumnNumber);
        //    Byte nextColumnNumber = 1;
        //    while (nextColumnNumber < 51)
        //    {
        //        if (takenColumnNumbers.Contains(nextColumnNumber))
        //        {
        //            nextColumnNumber++;
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }
        //    Debug.Assert(nextColumnNumber != 51, "Suitable product attribute column number not found");
        //    addItem.ColumnNumber = nextColumnNumber;

        //    //add to the master list
        //    _masterAttributeListView.Model.Add(addItem);

        //    //save straight away as we need the id
        //    try
        //    {
        //        _masterAttributeListView.Save();
        //    }
        //    catch (DataPortalException ex)
        //    {
        //        base.ShowWaitCursor(false);

        //        LocalHelper.RecordException(ex, _exCategory);
        //        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(addItem.Name, OperationType.Save);

        //        base.ShowWaitCursor(true);
        //    }

        //    base.ShowWaitCursor(false);
        //}

        //#endregion

        //#region RemoveProductAttribute

        //private RelayCommand<ProductAttribute> _removeProductAttributeCommand;

        ///// <summary>
        ///// Removes the given attribute from the list.
        ///// </summary>
        //public RelayCommand<ProductAttribute> RemoveProductAttributeCommand
        //{
        //    get
        //    {
        //        if (_removeProductAttributeCommand == null)
        //        {
        //            _removeProductAttributeCommand = new RelayCommand<ProductAttribute>(
        //                p => RemoveProductAttribute_Executed(p),
        //                p => RemoveProductAttribute_CanExecute(p))
        //            {
        //                FriendlyDescription = Message.ProductAttribute_RemoveProductAttribute,
        //                SmallIcon = ImageResources.ProductMaintenance_RemoveProductAttribute
        //            };
        //            base.ViewModelCommands.Add(_removeProductAttributeCommand);
        //        }
        //        return _removeProductAttributeCommand;
        //    }
        //}

        //[DebuggerStepThrough]
        //private Boolean RemoveProductAttribute_CanExecute(ProductAttribute attributeToRemove)
        //{
        //    //user must have delete permission
        //    if (!_userHasProductAttributeDeletePerm)
        //    {
        //        _addProductAttributeCommand.DisabledReason = Message.Generic_NoDeletePermission;
        //        return false;
        //    }

        //    //must not be null
        //    if (attributeToRemove == null)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //private void RemoveProductAttribute_Executed(ProductAttribute attributeToRemove)
        //{
        //    base.ShowWaitCursor(true);

        //    //remove from the master list
        //    _masterAttributeListView.Model.Remove(attributeToRemove);

        //    try
        //    {
        //        //save immediately
        //        _masterAttributeListView.Save();
        //    }
        //    catch (Exception ex)
        //    {
        //        base.ShowWaitCursor(false);

        //        LocalHelpers.RecordException(ex, _exCategory);
        //        Helpers.ShowErrorOccurredMessage(attributeToRemove.Name, OperationType.Save);

        //        base.ShowWaitCursor(true);

        //        //refresh the list so it gets readded
        //        _masterAttributeListView.FetchAllForEntity();
        //    }

        //    base.ShowWaitCursor(false);
        //}

        //#endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(), p => Close_CanExecute())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //Product premissions
            //create
            _userHasProductCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(Product));

            //fetch
            _userHasProductFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(Product));

            //edit
            _userHasProductEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(Product));

            //delete
            _userHasProductDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(Product));

            //ProductAttribute permissions
            ////create
            //_userHasProductAttributeCreatePerm = Csla.Rules.BusinessRules.HasPermission(
            //    Csla.Rules.AuthorizationActions.CreateObject, typeof(ProductAttribute));

            ////delete
            //_userHasProductAttributeDeletePerm = Csla.Rules.BusinessRules.HasPermission(
            //    Csla.Rules.AuthorizationActions.DeleteObject, typeof(ProductAttribute));
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;

            //do not show save dialog on delete
            if (this.SelectedProduct != null)
            {
                if (this.SelectedProduct.IsDeleted)
                {
                    return continueExecute;
                }
            }

            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedProduct, this.SaveCommand);
            }
            return continueExecute;
        }

        /// <summary>
        /// Researches for products based on the product search criteria
        /// </summary>
        private void RefreshProductSearchResults()
        {
            if (!this.IsProcessingProductSearch)
            {
                _isProductSearchQueued = false;
                this.IsProcessingProductSearch = true;

                //search asyncronously
                _productInfoListView.BeginFetchForCurrentEntityBySearchCriteria(_productSearchCriteria);
            }
            else
            {
                _isProductSearchQueued = true;
            }
        }

        /// <summary>
        /// Launches the window to select a product image.
        /// </summary>
        /// <param name="propertyName"></param>
        public void SetProductImage(String fileName, ProductImageType imageType, ProductImageFacing facingType)
        {
            if (String.IsNullOrEmpty(fileName)) throw new ArgumentOutOfRangeException("fileName", "Cannot be null or empty");
            if (SelectedProduct == null) return;
            
            // Ensure a Compression Id is available.
            var compressionList = CompressionList.FetchAll();
            if (compressionList.Count == 0)
            {
                var compression = Compression.NewCompression();
                compression.Name = "Default";
                compressionList.Add(compression);
                compressionList.Save();
            }

            // Prepare ProductImage
            var file = File.NewFile(App.ViewState.EntityId, fileName, DomainPrincipal.CurrentUserId);
            var image = Galleria.Ccm.Model.Image.NewImage(file);
            image.CompressionId = compressionList.First().Id;
            var imageFacingType = new ProductImageImageFacingType()
            {
                FacingType = facingType,
                ImageType = imageType
            };
            var productImage = ProductImage.NewProductImage(image, imageFacingType, SelectedProduct.Id);
            
            // Set image
            var existingImage = SelectedProduct.ImageList
                .Where(img=>img.ImageType==imageType && img.FacingType==facingType)
                .FirstOrDefault();
            if (existingImage != null)
            {
                SelectedProduct.ImageList.Remove(existingImage);
            }
            SelectedProduct.ImageList.Add(productImage);
            
            // Update UI
            var propertyPath = this.GetType().GetField(String.Format("{0}{1}ImageProperty",imageType,facingType));
            if (propertyPath == null) return;
            OnPropertyChanged((PropertyPath)propertyPath.GetValue(null));
        }

        public void ClearProductImage(ProductImageType imageType, ProductImageFacing facingType)
        {
            if (SelectedProduct == null) return;

            // Remove image
            var imagesToRemove = SelectedProduct.ImageList
                .Where(img => img.FacingType == facingType && img.ImageType == imageType)
                .ToList();
            SelectedProduct.ImageList.RemoveList(imagesToRemove);

            // Update UI
            var propertyPath = this.GetType().GetField(String.Format("{0}{1}ImageProperty", imageType, facingType));
            if (propertyPath == null) return;
            OnPropertyChanged((PropertyPath)propertyPath.GetValue(null));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the avaialble attributes collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void AvailableProductAttributes_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        //{
        //    switch (e.Action)
        //    {
        //        case NotifyCollectionChangedAction.Add:
        //            if (this.SelectedProduct != null)
        //            {
        //                List<ProductMaintenanceCustomAttribute> addItems = new List<ProductMaintenanceCustomAttribute>();
        //                foreach (ProductAttribute attribute in e.ChangedItems)
        //                {
        //                    addItems.Add(new ProductMaintenanceCustomAttribute(attribute, this.SelectedProduct.AttributeData));
        //                }
        //                _attributeValueViews.AddRange(addItems);
        //            }
        //            break;

        //        case NotifyCollectionChangedAction.Remove:

        //            List<ProductMaintenanceCustomAttribute> removeItems = new List<ProductMaintenanceCustomAttribute>();
        //            foreach (ProductAttribute attribute in e.ChangedItems)
        //            {
        //                String attributeName = attribute.Name;
        //                ProductMaintenanceCustomAttribute view = _attributeValueViews.FirstOrDefault(p => p.AttributeName == attributeName);
        //                if (view != null)
        //                {
        //                    removeItems.Add(view);
        //                }
        //            }
        //            _attributeValueViews.RemoveRange(removeItems);
        //            break;

        //        case NotifyCollectionChangedAction.Reset:

        //            // clear down all existing items
        //            _attributeValueViews.Clear();

        //            //add in any new
        //            if (this.SelectedProduct != null && this.AvailableAttributes.Count > 0)
        //            {
        //                List<ProductMaintenanceCustomAttribute> resetAddItems = new List<ProductMaintenanceCustomAttribute>();
        //                foreach (ProductAttribute attribute in this.AvailableAttributes)
        //                {
        //                    resetAddItems.Add(new ProductMaintenanceCustomAttribute(attribute, this.SelectedProduct.AttributeData));
        //                }
        //                _attributeValueViews.AddRange(resetAddItems);
        //            }
        //            break;
        //    }
        //}

        /// <summary>
        /// Responds to changes in the attribute value views collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void AttributeValueViews_BulkCollectionChanged(object sender, Framework.Model.BulkCollectionChangedEventArgs e)
        //{
        //    switch (e.Action)
        //    {
        //        case NotifyCollectionChangedAction.Remove:
        //            foreach (ProductMaintenanceCustomAttribute attribute in e.ChangedItems)
        //            {
        //                attribute.Dispose();
        //            }
        //            break;

        //        case NotifyCollectionChangedAction.Reset:
        //            Debug.Assert((e.ChangedItems != null), "Could not unsubscribe - ProductMaintenanceCustomAttributes leaked.");
        //            if (e.ChangedItems != null)
        //            {
        //                foreach (ProductMaintenanceCustomAttribute attribute in e.ChangedItems)
        //                {
        //                    attribute.Dispose();
        //                }
        //            }
        //            break;
        //    }
        //}

        /// <summary>
        /// Responds to a change of the selected product
        /// </summary>
        private void OnSelectedProductChanged(Product oldModel, Product newModel)
        {
            ////remove all the existing attribute values
            //if (_attributeValueViews.Count > 0)
            //{
            //    _attributeValueViews.RemoveRange(_attributeValueViews.ToList());
            //}

            //if (newModel != null)
            //{
            //    //create new attributes
            //    List<ProductMaintenanceCustomAttribute> customAttributeList = new List<ProductMaintenanceCustomAttribute>();
            //    foreach (ProductAttribute attribute in this.AvailableAttributes)
            //    {
            //        customAttributeList.Add(new ProductMaintenanceCustomAttribute(attribute, newModel.AttributeData));
            //    }
            //    _attributeValueViews.AddRange(customAttributeList);
            //}
        }

        /// <summary>
        /// Responds to a change of product search criteria
        /// </summary>
        private void OnProductSearchCriteriaChanged()
        {
            RefreshProductSearchResults();
        }

        /// <summary>
        /// Responds to the return of results from the product search.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
        {
            //mark as finished searching
            this.IsProcessingProductSearch = false;

            //check if another search is queued
            if (_isProductSearchQueued)
            {
                RefreshProductSearchResults();
            }
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                if (columnName == MinDeepProperty.Path)
                {
                    if (MaxDeep > 0 && MinDeep > MaxDeep)
                        result = String.Format(CultureInfo.CurrentCulture, Message.ProductMaintenance_MinBelowMax);
                }
                if (columnName == MaxDeepProperty.Path)
                {
                    if (MaxDeep > 0 && MinDeep > MaxDeep)
                        result = String.Format(CultureInfo.CurrentCulture, Message.ProductMaintenance_MaxAboveMin);
                }
                return result; // return result
            }
        }
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //save the attribute list so any edits are committed.
                    //_masterAttributeListView.Save();

                    //remove all the collection values so they get disposed.
                    //foreach (ProductAttribute item in _masterAttributeListView.BindingView)
                    //{
                    //    String attributeName = item.Name;
                    //    ProductMaintenanceCustomAttribute view = _attributeValueViews.FirstOrDefault(p => p.AttributeName == attributeName);
                    //    if (view != null)
                    //    {
                    //        _attributeValueViews.Remove(view);
                    //    }
                    //}

                    //unsubscribe
                    //_attributeValueViews.BulkCollectionChanged -= AttributeValueViews_BulkCollectionChanged;
                    //_masterAttributeListView.BindingView.BulkCollectionChanged -= AvailableProductAttributes_BulkCollectionChanged; // SA-14701


                    //drop references
                    _selectedProduct = null;
                    this.AttachedControl = null;

                    _productInfoListView.Dispose();
                    //_masterAttributeListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// TEMP 
    /// Validation rule for use when binding to model objects
    /// </summary>
    public class NullInt16ValidationRule : ValidationRule
    {
        #region Fields

        private Int16 _max = Int16.MaxValue;
        private Int16 _min = 0;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public NullInt16ValidationRule() : base(ValidationStep.RawProposedValue, true) { }

        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;

            Int16 smallValue = 0;
            if (value != null)
            {
                try
                {
                    if (((String)value).Length > 0)
                    {
                        // if doesn't contain only digits
                        if (!((String)value).All(char.IsDigit))
                        {
                            return new ValidationResult(false, Message.ProductMaintenance_NullInt16ValidationRule_DigitsOnly);
                        }
                        else
                        {
                            //try parse to Int16
                            smallValue = Int16.Parse((String)value);
                        }
                    }
                }
                catch (OverflowException e)
                {
                    //value is out of Int16 bounds
                    // min and max should really be Int16.MinValue and Int16.MaxValue respectively
                    //  but, this is a dirty implementation so i am asking user to put value directly in valid bounds
                    return new ValidationResult(false, String.Format(
                         Message.ProductMaintenance_NullInt16ValidationRule_OutOfBounds, _min, _max));
                }

                if ((smallValue < _min) || (smallValue > _max))
                {
                    return new ValidationResult(false, String.Format(
                         Message.ProductMaintenance_NullInt16ValidationRule_OutOfBounds, _min, _max));
                }
                else
                {
                    return new ValidationResult(true, null);
                }
            }
            return validationResult;
        }
        #endregion
    }
}
