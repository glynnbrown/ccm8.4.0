﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25451 : N.Haywood
//	Added from SA
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence
{
    /// <summary>
    /// Interaction logic for ProductMaintenanceHomeTab.xaml
    /// </summary>
    public partial class ProductMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductMaintenanceViewModel), typeof(ProductMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ProductMaintenanceViewModel ViewModel
        {
            get { return (ProductMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductMaintenanceHomeTab senderControl = (ProductMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                ProductMaintenanceViewModel oldModel = (ProductMaintenanceViewModel)e.OldValue;

            }

            if (e.NewValue != null)
            {
                ProductMaintenanceViewModel newModel = (ProductMaintenanceViewModel)e.NewValue;


            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ProductMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
