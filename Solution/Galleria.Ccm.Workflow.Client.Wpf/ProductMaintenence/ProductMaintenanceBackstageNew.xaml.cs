﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25451 : N.Haywood
//	Added from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductMaintenence
{
    /// <summary>
    /// Interaction logic for ProductMaintenanceBackstageNew.xaml
    /// </summary>
    public partial class ProductMaintenanceBackstageNew : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductMaintenanceViewModel), typeof(ProductMaintenanceBackstageNew),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ProductMaintenanceViewModel ViewModel
        {
            get { return (ProductMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        #endregion


        #endregion

        #region Constructor

        public ProductMaintenanceBackstageNew()
        {
            InitializeComponent();
        }

        #endregion
    }
}
