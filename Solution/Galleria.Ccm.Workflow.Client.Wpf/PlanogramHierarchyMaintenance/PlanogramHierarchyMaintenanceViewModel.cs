﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-25871 : A.Kuszyk
//  Created.
// V8-26284 : A.Kuszyk
//  Amended to use Common.Wpf selector and added Save commands.
// V8-27964 : A.Silva
//      Added ProductUniverse selection.
//      Amended RenameGroup to be EditGroup.

#endregion

#region Version History: (CCM v8.01)
// V8-28251 : L.Ineson
//  Commented out save commands as this now saves immediately.
#endregion

#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.Linq.Expressions;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows.Input;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramHierarchyMaintenance
{
    //TODO: Get rid of dependency on the selector viewmodel.

    public sealed class PlanogramHierarchyMaintenanceViewModel : ViewModelAttachedControlObject<PlanogramHierarchyMaintenanceOrganiser>
    {
        #region Fields

        private readonly PlanogramHierarchyViewModel _masterPlanogramHierarchyView = new PlanogramHierarchyViewModel();
        private PlanogramGroup _selectedPlanogramGroup;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath PlanogramHierarchyViewProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.PlanogramHierarchyView);
        public static readonly PropertyPath SelectedPlanogramGroupProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.SelectedPlanogramGroup);

        // Commands
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath NewGroupCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.NewGroupCommand);
        //public static readonly PropertyPath MoveGroupCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.MoveGroupCommand);
        public static readonly PropertyPath DeleteGroupCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.DeleteGroupCommand);
        public static readonly PropertyPath EditGroupCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.EditGroupCommand);
        public static readonly PropertyPath ExpandAllCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.ExpandAllCommand);
        public static readonly PropertyPath CollapseAllCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.CollapseAllCommand);
        //public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.SaveCommand);
        //public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<PlanogramHierarchyMaintenanceViewModel>(p => p.SaveAndCloseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the master planogram hierarchy view.
        /// </summary>
        public PlanogramHierarchyViewModel PlanogramHierarchyView
        {
            get { return _masterPlanogramHierarchyView; }
        }

        /// <summary>
        ///Gets/Sets the currently selected group.
        /// </summary>
        public PlanogramGroup SelectedPlanogramGroup
        {
            get { return _selectedPlanogramGroup; }
            set
            {
                _selectedPlanogramGroup = value;
                OnPropertyChanged(SelectedPlanogramGroupProperty);
            }
        }

        /// <summary>
        /// TODO: GET RID!!!
        /// </summary>
        private PlanogramHierarchySelectorViewModel SelectorViewModel
        {
            get
            {
                if (AttachedControl == null || AttachedControl.xTreeViewSelector == null) return null;
                return AttachedControl.xTreeViewSelector.ViewModel;
            }
        }

        #endregion

        #region Constructor
        public PlanogramHierarchyMaintenanceViewModel() 
        {
            StartPolling();
        }
        #endregion

        #region Commands

        #region SaveCommand

        //private RelayCommand _saveCommand;

        ///// <summary>
        ///// Saves the current hierarchy
        ///// </summary>
        //public RelayCommand SaveCommand
        //{
        //    get
        //    {
        //        if (_saveCommand == null)
        //        {
        //            _saveCommand = new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
        //            {
        //                FriendlyName = Message.Generic_Save,
        //                FriendlyDescription = Message.Generic_Save_Tooltip,
        //                Icon = ImageResources.Save_32,
        //                SmallIcon = ImageResources.Save_16,
        //                InputGestureModifiers = ModifierKeys.Control,
        //                InputGestureKey = Key.S,
        //                DisabledReason = Message.PlanogramHierarchyMaintenance_SaveDisabledReason
        //            };
        //            this.ViewModelCommands.Add(_saveCommand);
        //        }
        //        return _saveCommand;
        //    }
        //}

        //private Boolean Save_CanExecute()
        //{
        //    if (SelectorViewModel == null || 
        //        SelectorViewModel.PlanogramHierarchyView == null) return false;
        //    return this.SelectorViewModel.PlanogramHierarchyView.IsValid &&
        //        this.SelectorViewModel.PlanogramHierarchyView.Model.IsDirty;
        //}

        //private void Save_Executed()
        //{
        //    SelectorViewModel.PlanogramHierarchyView.Save();
        //}

        #endregion

        #region SaveAndCloseCommand

        //private RelayCommand _saveAndCloseCommand;

        ///// <summary>
        ///// Saves and closes the current hierarchy
        ///// </summary>
        //public RelayCommand SaveAndCloseCommand
        //{
        //    get
        //    {
        //        if (_saveAndCloseCommand == null)
        //        {
        //            _saveAndCloseCommand = new RelayCommand(p => SaveAndClose_Executed(), p => SaveAndClose_CanExecute())
        //            {
        //                FriendlyName = Message.Generic_SaveAndClose,
        //                FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
        //                Icon = ImageResources.SaveAndClose_32,
        //                SmallIcon = ImageResources.SaveAndClose_16,
        //                DisabledReason = Message.PlanogramHierarchyMaintenance_SaveDisabledReason
        //            };
        //            this.ViewModelCommands.Add(_saveAndCloseCommand);
        //        }
        //        return _saveAndCloseCommand;
        //    }
        //}

        //private Boolean SaveAndClose_CanExecute()
        //{
        //    return Save_CanExecute();
        //}

        //private void SaveAndClose_Executed()
        //{
        //    SelectorViewModel.PlanogramHierarchyView.Save();
        //    this.AttachedControl.Close();
        //}

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            CommonHelper.GetWindowService().CloseWindow(this.AttachedControl);
        }

        #endregion

        #region NewGroupCommand

        private RelayCommand _newGroupCommand;
        /// <summary>
        /// Adds a new PlanogramGroup to the Hierarchy.
        /// </summary>
        public RelayCommand NewGroupCommand
        {
            get
            {
                if (_newGroupCommand == null)
                {
                    _newGroupCommand = new RelayCommand(
                        p => NewGroup_Executed(),
                        p => NewGroup_CanExecute())
                    {
                        FriendlyName = Message.PlanogramHierarchyMaintenance_NewGroup,
                        FriendlyDescription = Message.PlanogramHierarchyMaintenance_NewGroup_Description,
                        Icon = ImageResources.PlanogramHierarchyMaintenance_NewGroup_32,
                        SmallIcon = ImageResources.PlanogramHierarchyMaintenance_NewGroup_16
                    };
                    base.ViewModelCommands.Add(_newGroupCommand);
                }
                return _newGroupCommand;
            }
        }

        public Boolean NewGroup_CanExecute()
        {
            return this.SelectedPlanogramGroup != null;
        }

        public void NewGroup_Executed()
        {
            StopPolling();

            Int32? groupIdToSelect = PlanogramRepositoryUIHelper.CreateNewPlanogramGroup(this.SelectedPlanogramGroup);

            StartPolling();

            //try to select the new group
            if (groupIdToSelect.HasValue)
            {
                PlanogramGroup newGroup = _masterPlanogramHierarchyView.Model.EnumerateAllGroups().FirstOrDefault(i => i.Id == groupIdToSelect.Value);
                if (newGroup != null) this.SelectedPlanogramGroup = newGroup;
            }

            //if (SelectorViewModel == null) return;
            //SelectorViewModel.AddNewPlanogramGroup(SelectorViewModel.SelectedGroup);
        }

        #endregion

        #region DeleteGroupCommand

        private RelayCommand _deleteGroupCommand;
        /// <summary>
        /// Deletes a new PlanogramGroup from the Hierarchy.
        /// </summary>
        public RelayCommand DeleteGroupCommand
        {
            get
            {
                if (_deleteGroupCommand == null)
                {
                    _deleteGroupCommand = new RelayCommand(
                        p => DeleteGroup_Executed(),
                        p => DeleteGroup_CanExecute())
                    {
                        FriendlyName = Message.PlanogramHierarchyMaintenance_DeleteGroup,
                        FriendlyDescription = Message.PlanogramHierarchyMaintenance_DeleteGroup_Description,
                        Icon = ImageResources.PlanogramHierarchyMaintenance_DeleteGroup_32,
                        SmallIcon = ImageResources.PlanogramHierarchyMaintenance_DeleteGroup_16
                    };
                    base.ViewModelCommands.Add(_deleteGroupCommand);
                }
                return _deleteGroupCommand;
            }
        }

        public Boolean DeleteGroup_CanExecute()
        {
            return PlanogramRepositoryUIHelper.CanDeletePlanogramGroup(this.SelectedPlanogramGroup);
        }

        public void DeleteGroup_Executed()
        {
            PlanogramGroup groupToDelete = this.SelectedPlanogramGroup;

            //Stop polling while the delete is carried out
            StopPolling();

            //Perform the delete.
            PlanogramRepositoryUIHelper.DeletePlanogramGroup(groupToDelete, OnPlanogramGroupItemDeleted);

            //start polling again
            StartPolling();


            //if (SelectorViewModel == null) return;
            //SelectorViewModel.DeletePlanogramGroup(SelectorViewModel.SelectedGroup);
        }

        /// <summary>
        /// Called whenever a planogram group or planogram delete
        /// is completed during the process of deleteing a planogram
        /// group and its child items.
        /// </summary>
        private void OnPlanogramGroupItemDeleted(Object sender, EventArgs<Object> args)
        {
            PlanogramGroup deletedGroup = args.ReturnValue as PlanogramGroup;
            if (deletedGroup != null)
            {
                //remove the group from its parent group view.
                foreach (PlanogramGroupViewModel parentGroup in _masterPlanogramHierarchyView.RootGroup.EnumerateAllChildGroups())
                {
                    PlanogramGroupViewModel groupView = parentGroup.ChildViews.FirstOrDefault(g => g.PlanogramGroup.Id == deletedGroup.Id);
                    if (groupView != null)
                    {
                        parentGroup.RemoveGroup(groupView);
                        break;
                    }
                }
            }
        }

        #endregion

        #region EditGroupCommand

        private RelayCommand _editGroupCommand;

        /// <summary>
        /// Edits a PlanogramGroup in the Hierarchy.
        /// </summary>
        public RelayCommand EditGroupCommand
        {
            get
            {
                if (_editGroupCommand == null)
                {
                    _editGroupCommand = new RelayCommand(
                        p => EditGroup_Executed(),
                        p => EditGroup_CanExecute())
                    {
                        FriendlyName = Message.PlanogramHierarchyMaintenance_EditGroup,
                        FriendlyDescription = Message.PlanogramHierarchyMaintenance_EditGroup_Description,
                        Icon = ImageResources.PlanogramHierarchyMaintenance_EditGroup_32,
                        SmallIcon = ImageResources.PlanogramHierarchyMaintenance_EditGroup_16
                    };
                    base.ViewModelCommands.Add(_editGroupCommand);
                }
                return _editGroupCommand;
            }
        }

        public Boolean EditGroup_CanExecute()
        {
            return this.SelectedPlanogramGroup != null;
        }

        public void EditGroup_Executed()
        {
            StopPolling();

            PlanogramRepositoryUIHelper.ShowPlanogramGroupEditWindow(this.SelectedPlanogramGroup, true);

            StartPolling();

            //if (SelectorViewModel == null) return;
            //SelectorViewModel.EditPlanogramGroup(SelectorViewModel.SelectedGroup, showMerchGroupSelector: true);
        }

        #endregion

        
        #region ExpandAllCommand

        private RelayCommand _expandAllCommand;
        /// <summary>
        /// Expands all the Planogram Groups in the displayed Hierarchy.
        /// </summary>
        public RelayCommand ExpandAllCommand
        {
            get
            {
                if (_expandAllCommand == null)
                {
                    _expandAllCommand = new RelayCommand(p => ExpandAll_Executed())
                    {
                        FriendlyName = Message.PlanogramHierarchyMaintenance_ExpandAll,
                        FriendlyDescription = Message.PlanogramHierarchyMaintenance_ExpandAll_Description,
                        Icon = ImageResources.PlanogramHierarchyMaintenance_ExpandAll_32,
                        SmallIcon = ImageResources.PlanogramHierarchyMaintenance_ExpandAll_32
                    };
                    base.ViewModelCommands.Add(_expandAllCommand);
                }
                return _expandAllCommand;
            }
        }

        public void ExpandAll_Executed()
        {
            if (SelectorViewModel == null || SelectorViewModel.AvailableGroups == null) return;
            base.ShowWaitCursor(true);
            ExpandAllItems(SelectorViewModel.AvailableGroups, true);
            base.ShowWaitCursor(false);
        }

        #endregion

        #region CollapseAllCommand

        private RelayCommand _collapseAllCommand;

        /// <summary>
        /// Collapses all the Planogram Groups in the displayed Hierarchy.
        /// </summary>
        public RelayCommand CollapseAllCommand
        {
            get
            {
                if (_collapseAllCommand == null)
                {
                    _collapseAllCommand = new RelayCommand(p => CollapseAll_Executed())
                    {
                        FriendlyName = Message.PlanogramHierarchyMaintenance_CollapseAll,
                        FriendlyDescription = Message.PlanogramHierarchyMaintenance_CollapseAll_Description,
                        Icon = ImageResources.PlanogramHierarchyMaintenance_CollapseAll_32,
                        SmallIcon = ImageResources.PlanogramHierarchyMaintenance_CollapseAll_32
                    };
                    base.ViewModelCommands.Add(_collapseAllCommand);
                }
                return _collapseAllCommand;
            }
        }

        public void CollapseAll_Executed()
        {
            if (SelectorViewModel == null || SelectorViewModel.AvailableGroups == null) return;
            base.ShowWaitCursor(true);
            ExpandAllItems(SelectorViewModel.AvailableGroups, false);
            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Methods

        ///// <summary>
        ///// Shows a warning requesting user ok to continue if current
        ///// item is dirty
        ///// </summary>
        ///// <returns>true if the action may continue</returns>
        //public Boolean ContinueWithItemChange()
        //{
        //    Boolean continueExecute = true;
        //    if (this.AttachedControl != null)
        //    {
        //        continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithItemChangeDialog(
        //            this.SelectorViewModel.PlanogramHierarchyView.Model.Name,
        //            this.SelectorViewModel.PlanogramHierarchyView.Model.IsSavable,
        //            this.SaveCommand);
        //    }
        //    return continueExecute;
        //}

        private void ExpandAllItems(IEnumerable<PlanogramGroupViewModel> items, Boolean expandState)
        {
            if (items == null) return;
            foreach (var item in items)
            {
                if (expandState && item.ChildViews.Count == 0) continue;
                item.IsExpanded = expandState;
                ExpandAllItems(item.ChildViews, expandState);
            }
        }


        /// <summary>
        /// Starts the data polling.
        /// </summary>
        public void StartPolling()
        {
            if (!_masterPlanogramHierarchyView.IsPolling)
            {
                _masterPlanogramHierarchyView.BeginPollingFetchForCurrentEntity();
            }
        }

        /// <summary>
        /// Stops the data polling
        /// </summary>
        public void StopPolling()
        {
            _masterPlanogramHierarchyView.StopPollingAsync();
        }

        public void MoveGroup(PlanogramGroupViewModel groupToMove, PlanogramGroupViewModel target)
        {
            StartPolling();
            PlanogramRepositoryUIHelper.MovePlanogramGroup(groupToMove.PlanogramGroup, target.PlanogramGroup);
            StopPolling();
        }

        #endregion

        #region ViewModelAttachedControlObjectMembers
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                _masterPlanogramHierarchyView.StopPollingAsync();
                AttachedControl = null;

                base.IsDisposed = true;
            }
        }
        #endregion

    }

    #region OLD

    #region MoveGroupCommand (Commented out, because it is not currently required)

    //private RelayCommand _moveGroupCommand;
    ///// <summary>
    ///// Deletes a new PlanogramGroup from the Hierarchy.
    ///// </summary>
    //public RelayCommand MoveGroupCommand
    //{
    //    get
    //    {
    //        if (_moveGroupCommand == null)
    //        {
    //            _moveGroupCommand = new RelayCommand(
    //                p => MoveGroup_Executed(),
    //                p => MoveGroup_CanExecute())
    //            {
    //                FriendlyName = "Move Group", 
    //                FriendlyDescription = "Move",
    //                Icon = ImageResources.Save_32,
    //                SmallIcon = ImageResources.Save_16
    //            };
    //            base.ViewModelCommands.Add(_moveGroupCommand);
    //        }
    //        return _moveGroupCommand;
    //    }
    //}

    //public Boolean MoveGroup_CanExecute()
    //{
    //    return IsGroupSelected();
    //}

    //public void MoveGroup_Executed()
    //{
    //    var prompt = new PlanogramHierarchyMaintenanceGroupSelector();
    //    if(prompt.ShowDialog() == false) return;
    //    _treeViewSelectorViewModel.MovePlanogramGroup(
    //        _treeViewSelectorViewModel.SelectedGroup.PlanogramGroup,
    //        prompt.SelectedGroup.PlanogramGroup);
    //}

    #endregion

    #endregion
}
