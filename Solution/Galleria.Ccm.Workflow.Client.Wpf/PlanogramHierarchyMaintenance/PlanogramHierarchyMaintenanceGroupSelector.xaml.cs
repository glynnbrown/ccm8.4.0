﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25871 : A.Kuszyk
//  Created
// V8-26284 : A.Kuszyk
//  Amended to use Common.Wpf selector.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramHierarchyMaintenance
{
    /// <summary>
    /// Interaction logic for ProvideValueFromDropDownDialog.xaml
    /// </summary>
    public partial class PlanogramHierarchyMaintenanceGroupSelector: ExtendedRibbonWindow
    {
        #region Properties

        public PlanogramGroupViewModel SelectedGroup
        {
            get
            {
                return xTreeViewSelector.SelectedGroup;
            }
        }

        #endregion

        #region Constructors

        public PlanogramHierarchyMaintenanceGroupSelector()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #endregion
    }
}
