﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-25871 : A.Kuszyk
//  Created.
// V8-26284 : A.Kuszyk
//  Amended to use Common.Wpf selector and added OnClosing override.
// V8-26822 : L.Ineson
//  Swapped TreeViewItem for TreeNode
// V8-27964 : A.Silva
//      Amended ViewModel to be a DependencyProperty.
//      Added ProductUniverse.

#endregion

#region Version History: (CCM v8.01)
// V8-28251 : L.Ineson
//  Added dispose of viewmodel when closing!!
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramHierarchyMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramHierarchyOrganiser.xaml
    /// </summary>
    public partial class PlanogramHierarchyMaintenanceOrganiser
    {
        #region Constants

        private const Int32 DistanceMouseCanMoveBeforeDrag = 5;

        #endregion

        #region Fields
        private Point _lastMousePosition;
        #endregion

        #region Properties

        public static readonly DependencyProperty ViewModelProperty = 
            DependencyProperty.Register("ViewModel", typeof (PlanogramHierarchyMaintenanceViewModel), typeof (PlanogramHierarchyMaintenanceOrganiser), 
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PlanogramHierarchyMaintenanceOrganiser senderControl = (PlanogramHierarchyMaintenanceOrganiser)sender;
          

            PlanogramHierarchyMaintenanceViewModel oldViewModel = e.OldValue as PlanogramHierarchyMaintenanceViewModel;
            if (oldViewModel != null)
            {
                oldViewModel.AttachedControl = null;
            }

            PlanogramHierarchyMaintenanceViewModel newViewModel = e.NewValue as PlanogramHierarchyMaintenanceViewModel;
            if (newViewModel != null)
            {
                newViewModel.AttachedControl = senderControl;
            }
        }

        public PlanogramHierarchyMaintenanceViewModel ViewModel
        {
            get { return (PlanogramHierarchyMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructors

        public PlanogramHierarchyMaintenanceOrganiser()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new PlanogramHierarchyMaintenanceViewModel();

            //set the help file key
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramHierarchyMaintenance);

            // Attach treeview event handlers.
            xTreeViewSelector.AddHandler(UIElement.PreviewMouseLeftButtonDownEvent, (MouseButtonEventHandler)TreeViewItem_MouseLeftButtonDown);
            xTreeViewSelector.AddHandler(UIElement.PreviewMouseMoveEvent, (MouseEventHandler)TreeViewItem_PreviewMouseMove);
            xTreeViewSelector.AddHandler(UIElement.MouseUpEvent, (MouseButtonEventHandler)TreeViewItem_MouseLeftButtonUp);
            xTreeViewSelector.AddHandler(UIElement.DropEvent, (DragEventHandler)TreeViewItem_Drop);

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        private void xTreeViewSelector_NodeContextMenuShowing(object sender, Ccm.Common.Wpf.Selectors.PlanogramHierarchyContextEventArgs e)
        {
            var contextMenu = e.ContextMenu;

            contextMenu.Items.Clear();

            AddCommandToMenu(contextMenu, ViewModel.NewGroupCommand);
            AddCommandToMenu(contextMenu, ViewModel.EditGroupCommand);
            //AddCommandToMenu(contextMenu, ViewModel.MoveGroupCommand);
            AddCommandToMenu(contextMenu, ViewModel.DeleteGroupCommand);
        }

        private void TreeViewItem_Drop(Object sender, DragEventArgs e)
        {
            // Check that target item is a PlanogramGroupSelectorView
            var targetItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<TreeNode>();
            if (targetItem == null || !(targetItem.Item is PlanogramGroupViewModel)) return;
            var targetGroup = (PlanogramGroupViewModel)targetItem.Item;

            // PlanogramGroup
            if (DragDropBehaviour.IsUnpackedType<PlanogramGroupDropEventArgs>(e.Data))
            {
                var draggedGroup = DragDropBehaviour.UnpackData<PlanogramGroupDropEventArgs>(e.Data).PlanogramGroupView;
                if (draggedGroup.IsRecentWorkGroup || draggedGroup.IsMyCategoriesGroup) return;
                this.ViewModel.MoveGroup(draggedGroup, targetGroup);
                e.Handled = true;
                //e.Handled = xTreeViewSelector.ViewModel.MovePlanogramGroup(draggedGroup, targetGroup);
            }
        }

        /// <summary>
        /// Called when a click has completed on a tree view item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_MouseLeftButtonUp(Object sender, MouseButtonEventArgs e)
        {
            ViewModel.SelectedPlanogramGroup = 
                xTreeViewSelector.SelectedGroup==null ? null : xTreeViewSelector.SelectedGroup.PlanogramGroup;
        }

        /// <summary>
        /// Called when the left mouse button is clicked on a TreeNode.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeViewItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _lastMousePosition = e.GetPosition(this);
        }


        /// <summary>
        /// Called when a TreeNode is dragged.
        /// </summary>
        private void TreeViewItem_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && MouseIsMoving(e.GetPosition(this)))
            {
                // Get TreeNode?
                var item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<TreeNode>();
                if (item == null) return;

                // Get PlanogramGroup from TreeNode?
                var planogramGroup = item.Item as PlanogramGroupViewModel;
                if (planogramGroup == null) return;

                // Create drag data to accompany drag.
                var dragData = new PlanogramGroupDropEventArgs(planogramGroup);

                // Begin drag behaviour.
                var dragDrop = new DragDropBehaviour(dragData);
                //dragDrop.DragArea = xTreeViewSelector as FrameworkElement;

                FrameworkElement dragScope = this as FrameworkElement;
                Window dragScopeWin = this.FindVisualAncestor<Window>();
                if (dragScopeWin != null)
                {
                    dragScope = dragScopeWin.Content as FrameworkElement;
                }
                dragDrop.DragArea = dragScope;
                dragDrop.SetAdornerFromImageSource(ImageResources.TreeViewItem_ClosedFolder);
                dragDrop.BeginDrag();
            }
        }

        
        #endregion

        #region Methods

        /// <summary>
        /// Indicates whether or not the mouse has moved from the previously saved position.
        /// </summary>
        /// <param name="currentPosition">The current position of the mouse cursor.</param>
        /// <returns>True if the mouse has moved beyond a defined threshold.</returns>
        private Boolean MouseIsMoving(Point currentPosition)
        {
            Boolean distanceIndicatesDragging =
                Math.Abs(currentPosition.X - _lastMousePosition.X) > DistanceMouseCanMoveBeforeDrag ||
                Math.Abs(currentPosition.Y - _lastMousePosition.Y) > DistanceMouseCanMoveBeforeDrag;
            return distanceIndicatesDragging;
        }

        /// <summary>
        /// Adds the given command to the context menu, using its Friendly Name and Small Icon.
        /// </summary>
        private void AddCommandToMenu(Fluent.ContextMenu contextMenu, RelayCommand command)
        {
            if (command.CanExecute())
            {
                contextMenu.Items.Add(new Fluent.MenuItem()
                {
                    Command = command,
                    Header = command.FriendlyName,
                    Icon = command.SmallIcon
                });
            }
        }

        #endregion

        #region Window Close

        //protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        //{
        //    base.OnClosing(e);

        //    if (!e.Cancel)
        //    {
        //        //check if we need to save
        //        if (ViewModel != null && xTreeViewSelector != null && xTreeViewSelector.PlanogramHierarchyView!=null && 
        //            xTreeViewSelector.PlanogramHierarchyView.Model!=null && xTreeViewSelector.PlanogramHierarchyView.Model.IsDirty)
        //        {
        //            e.Cancel = !this.ViewModel.ContinueWithItemChange();
        //        }
        //    }
        //}

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}
