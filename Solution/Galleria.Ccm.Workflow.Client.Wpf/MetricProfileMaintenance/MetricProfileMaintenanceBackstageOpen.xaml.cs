﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26123 : L.Ineson
//	Created
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using System.IO;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Text.RegularExpressions;

namespace Galleria.Ccm.Workflow.Client.Wpf.MetricProfileMaintenance
{
    /// <summary>
    /// Interaction logic for MetricProfileMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class MetricProfileMaintenanceBackstageOpen : UserControl
    {

        #region Fields
        private ObservableCollection<MetricProfileInfo> _searchResults = new ObservableCollection<MetricProfileInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(MetricProfileMaintenanceViewModel), typeof(MetricProfileMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public MetricProfileMaintenanceViewModel ViewModel
        {
            get { return (MetricProfileMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MetricProfileMaintenanceBackstageOpen senderControl = (MetricProfileMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                MetricProfileMaintenanceViewModel oldModel = (MetricProfileMaintenanceViewModel)e.OldValue;
                oldModel.AvailableItems.BulkCollectionChanged -= senderControl.ViewModel_AvailableMetricProfilesBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                MetricProfileMaintenanceViewModel newModel = (MetricProfileMaintenanceViewModel)e.NewValue;
                newModel.AvailableItems.BulkCollectionChanged += senderControl.ViewModel_AvailableMetricProfilesBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(MetricProfileMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MetricProfileMaintenanceBackstageOpen senderControl = (MetricProfileMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<MetricProfileInfo>),
            typeof(MetricProfileMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<MetricProfileInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<MetricProfileInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public MetricProfileMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<MetricProfileInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                String searchPattern = LocalHelper.GetRexexKeywordPattern(this.SearchText);

                var results =
                    this.ViewModel.AvailableItems
                    .Where(w => Regex.IsMatch(w.Name, searchPattern, RegexOptions.IgnoreCase))
                    .OrderBy(l => l.ToString());

                foreach (var info in results.OrderBy(l => l.ToString()))
                {
                    _searchResults.Add(info);
                }
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Updates the search results when the available locations list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableMetricProfilesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// Opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                MetricProfileInfo storeToOpen = (MetricProfileInfo)senderControl.SelectedItem;

                if (storeToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                }
            }
        }

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                ListBox senderControl = (ListBox)sender;
                ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
                if (clickedItem != null)
                {
                    MetricProfileInfo storeToOpen = (MetricProfileInfo)senderControl.SelectedItem;

                    if (storeToOpen != null)
                    {
                        //send the store to be opened
                        this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                    }
                }
            }
        }

        #endregion
    }
}
