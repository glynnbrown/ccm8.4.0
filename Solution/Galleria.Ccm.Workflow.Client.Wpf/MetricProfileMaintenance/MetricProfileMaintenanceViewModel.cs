﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26123 : L.Ineson
//	Created
// CCM:26697 : I.George
// Added ContinueWithClose Method
// CCM:27067 : I.George
// Removed the ContinueWithCloseMethd
#endregion
#region Version History: (CCM 8.1.1)
// CCM:30325 : I.George
// Added friendly description to SaveAs and Delete commands
#endregion

#endregion

using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Workflow.Client.Wpf.MetricProfileMaintenance
{
    /// <summary>
    /// Viewmodel controller for MetricProfileMaintenanceWindow
    /// </summary>
    public sealed class MetricProfileMaintenanceViewModel : ViewModelAttachedControlObject<MetricProfileMaintenanceOrganiser>
    {
        #region Fields

        const String _exCcategory = "MetricProfileMaintenance";//Category name for any exceptions raised to gibraltar from here

        private ModelPermission<MetricProfile> _itemPermissions = new ModelPermission<MetricProfile>();

        private readonly MetricProfileInfoListViewModel _masterMetricProfilesView = new MetricProfileInfoListViewModel();
        private MetricProfile _currentItem;

        private readonly MetricList _masterMetricList;
        private readonly BulkObservableCollection<Metric> _availableMetrics = new BulkObservableCollection<Metric>();
        private ReadOnlyBulkObservableCollection<Metric> _availableMetricsRO;
        private Metric _selectedMetric;

        private readonly BulkObservableCollection<MetricRow> _metricRows = new BulkObservableCollection<MetricRow>();
        private ReadOnlyBulkObservableCollection<MetricRow> _metricRowsRO;
        private MetricRow _selectedMetricRow = null;

        private Boolean _updatingRatios = false;
        private Boolean _updatingPercentages = false;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableItemsProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.AvailableItems);
        public static readonly PropertyPath CurrentItemProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.CurrentItem);
        public static readonly PropertyPath AvailableMetricsProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.AvailableMetrics);
        public static readonly PropertyPath SelectedMetricProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.SelectedMetric);
        public static readonly PropertyPath MetricRowsProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.MetricRows);
        public static readonly PropertyPath SelectedMetricRowProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.SelectedMetricRow);

        //Commands
        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(v => v.CloseCommand);
        public static PropertyPath AddMetricCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.AddMetricCommand);
        public static PropertyPath DeleteMetricCommandProperty = WpfHelper.GetPropertyPath<MetricProfileMaintenanceViewModel>(p => p.DeleteMetricCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available performance selections
        /// </summary>
        public ReadOnlyBulkObservableCollection<MetricProfileInfo> AvailableItems
        {
            get { return _masterMetricProfilesView.BindingView; }
        }

        /// <summary>
        /// Returns the current performance selection.
        /// </summary>
        public MetricProfile CurrentItem
        {
            get { return _currentItem; }
            private set
            {
               MetricProfile oldValue = _currentItem;

                _currentItem = value;
                OnPropertyChanged(CurrentItemProperty);

               OnCurrentItemChanged(oldValue, value);
            }
        }

        /// <summary>
        /// All metrics available for selection
        /// </summary>
        public ReadOnlyBulkObservableCollection<Metric> AvailableMetrics
        {
            get
            {
                if (_availableMetricsRO == null)
                {
                    _availableMetricsRO = new ReadOnlyBulkObservableCollection<Metric>(_availableMetrics);
                }
                return _availableMetricsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected metric
        /// </summary>
        public Metric SelectedMetric
        {
            get { return _selectedMetric; }
            set
            {
                _selectedMetric = value;
                OnPropertyChanged(SelectedMetricProperty);
            }
        }

        /// <summary>
        /// The collection of assigned metrics
        /// </summary>
        public ReadOnlyBulkObservableCollection<MetricRow> MetricRows
        {
            get
            {
                if (_metricRowsRO == null)
                {
                    _metricRowsRO = new ReadOnlyBulkObservableCollection<MetricRow>(_metricRows);
                }
                return _metricRowsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected metric row
        /// </summary>
        public MetricRow SelectedMetricRow
        {
            get
            {
                return _selectedMetricRow;
            }
            set
            {
                _selectedMetricRow = value;
                OnPropertyChanged(SelectedMetricRowProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public MetricProfileMaintenanceViewModel()
        {
            _metricRows.BulkCollectionChanged += MetricRows_BulkCollectionChanged;

            _masterMetricList = MetricList.FetchByEntityId(App.ViewState.EntityId);
            _masterMetricProfilesView.FetchForCurrentEntity();

            //load a new item.
            NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Loads a new store
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(
                            p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //create a new item
                MetricProfile newItem = MetricProfile.NewMetricProfile(App.ViewState.EntityId);

                newItem.MarkGraphAsInitialized();

                //close the backstage
                this.AttachedControl.SetRibbonBackstageState(false);
                this.CurrentItem = newItem;
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand _openCommand;
        /// <summary>
        /// Loads the requested store
        /// parameter = the id of the store to open
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Object id)
        {
            //user must have get permission
            if (!_itemPermissions.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (id == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Object id)
        {
            Int32? itemId = id as Int32?;
            if (itemId.HasValue)
            {
                //check if current item requires save first
                if (ContinueWithItemChange())
                {
                    base.ShowWaitCursor(true);

                    try
                    {
                        //fetch the requested item
                        this.CurrentItem = MetricProfile.FetchById(itemId.Value);
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        LocalHelper.RecordException(ex);
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                        base.ShowWaitCursor(true);
                    }


                    //close the backstage
                    LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                    base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_itemPermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.CurrentItem == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentItem.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            //ensure the metric profile is up to date.
            UpdateMetricProfile();


            Boolean continueWithSave = true;

            Int32 itemId = this.CurrentItem.Id;

            //** check the item unique value
            String newName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                _masterMetricProfilesView.FetchForCurrentEntity();

                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(
                    (s) =>
                    {
                        return !this.AvailableItems.Any(p =>
                        p.Id != itemId && p.Name.ToLowerInvariant() == s.ToLowerInvariant());

                    },
                    this.CurrentItem.Name, out newName);
            }
            else
            {
                //force a unique value for unit testing
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.CurrentItem.Name,
                    this.AvailableItems.Where(p => p.Id != itemId).Select(p => p.Name));
            }

            //set the code
            if (nameAccepted)
            {
                if (this.CurrentItem.Name != newName)
                {
                    this.CurrentItem.Name = newName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.CurrentItem = this.CurrentItem.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //[SA-17668] set return flag to false as save was not completed.
                    continueWithSave = false;

                    LocalHelper.RecordException(ex);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.CurrentItem.Name);
                        if (itemReloadRequired)
                        {
                            this.CurrentItem = MetricProfile.FetchById(this.CurrentItem.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.CurrentItem.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //refresh the info list
                _masterMetricProfilesView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }

            return continueWithSave;

        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(p),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //must be able to execute save
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAsCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed(Object args)
        {
            //** confirm the name to save as
            String copyName;
            Boolean nameAccepted = true;

            if (this.AttachedControl != null)
            {
                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptForSaveAsName((s) =>
                {
                    return !this.AvailableItems.Any(p => p.Name.ToLowerInvariant() == s.ToLowerInvariant());

                }, String.Empty, out copyName);
            }
            else
            {
                //use the name that was passed through.
                copyName = args as String;
            }

            if (nameAccepted)
            {
                base.ShowWaitCursor(true);

                //copy the item.
                MetricProfile itemCopy = this.CurrentItem.Copy();
                itemCopy.Name = copyName;
                this.CurrentItem = itemCopy;

                base.ShowWaitCursor(false);

                //save it.
                SaveCurrentItem();
            }
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// Parameter = store to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_itemPermissions.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.CurrentItem == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.CurrentItem.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.CurrentItem.Name.ToString());
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                var itemToDelete = this.CurrentItem;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //update the available items
                _masterMetricProfilesView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region AddMetricCommand

        private RelayCommand _addMetricCommand;

        /// <summary>
        /// Adds the selected metric to the assigned list
        /// </summary>
        public RelayCommand AddMetricCommand
        {
            get
            {
                if (_addMetricCommand == null)
                {
                    _addMetricCommand = new RelayCommand(
                        p => AddMetric_Executed(),
                        p => AddMetric_CanExecute())
                    {
                        FriendlyName = Message.Generic_Add,
                        FriendlyDescription = Message.MetricProfileMaintenance_AddMetric_Desc,
                        Icon = ImageResources.Add_32,
                        SmallIcon = ImageResources.Add_32,
                    };
                    base.ViewModelCommands.Add(_addMetricCommand);
                }
                return _addMetricCommand;
            }
        }

        private Boolean AddMetric_CanExecute()
        {
            //must have a selected Metric
            if (this.SelectedMetric == null)
            {
                _addMetricCommand.DisabledReason = 
                    Message.MetricProfileMaintenance_AddMetric_DisabledNoSelected;
                return false;
            }

            //can only have 20 selected
            if (this.MetricRows.Count >= 20)
            {
                _addMetricCommand.DisabledReason =
                   Message.MetricProfileMaintenance_AddMetric_DisabledTooMany;
                return false;
            }

            return true;
        }

        private void AddMetric_Executed()
        {
            MetricRow row = AddMetricRow(this.SelectedMetric.Id, 1);

            if (row != null)
            {
                this.SelectedMetricRow = row;
            }
        }

        #endregion

        #region DeleteMetricCommand

        private RelayCommand _deleteMetricCommand;

        /// <summary>
        /// Adds the selected metric to the assigned list
        /// </summary>
        public RelayCommand DeleteMetricCommand
        {
            get
            {
                if (_deleteMetricCommand == null)
                {
                    _deleteMetricCommand = new RelayCommand(
                        p => DeleteMetric_Executed(),
                        p => DeleteMetric_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.MetricProfileMaintenance_DeleteMetric_Desc,
                        Icon = ImageResources.Delete_32,
                        SmallIcon = ImageResources.Delete_32,
                    };
                    base.ViewModelCommands.Add(_deleteMetricCommand);
                }
                return _deleteMetricCommand;
            }
        }

        private Boolean DeleteMetric_CanExecute()
        {
            if (this.SelectedMetricRow == null)
            {
                _deleteMetricCommand.DisabledReason = 
                    Message.MetricProfileMaintenance_DeleteMetric_DisabledNoSelected;
                return false;
            }

            return true;
        }

        private void DeleteMetric_Executed()
        {
            RemoveMetricRow(this.SelectedMetricRow);
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current performance selection property value changes.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnCurrentItemChanged(MetricProfile oldValue,  MetricProfile newValue)
        {
            //reset the list of available metrics
            _availableMetrics.Clear();
            _availableMetrics.AddRange(_masterMetricList);
            

            //clear the rows
            _metricRows.Clear();

            if (newValue != null)
            {
                //add the metric rows.
                AddMetricRow(newValue.Metric1MetricId, newValue.Metric1Ratio, false);
                AddMetricRow(newValue.Metric2MetricId, newValue.Metric2Ratio, false);
                AddMetricRow(newValue.Metric3MetricId, newValue.Metric3Ratio, false);
                AddMetricRow(newValue.Metric4MetricId, newValue.Metric4Ratio, false);
                AddMetricRow(newValue.Metric5MetricId, newValue.Metric5Ratio, false);
                AddMetricRow(newValue.Metric6MetricId, newValue.Metric6Ratio, false);
                AddMetricRow(newValue.Metric7MetricId, newValue.Metric7Ratio, false);
                AddMetricRow(newValue.Metric8MetricId, newValue.Metric8Ratio, false);
                AddMetricRow(newValue.Metric9MetricId, newValue.Metric9Ratio, false);
                AddMetricRow(newValue.Metric10MetricId, newValue.Metric10Ratio, false);
                AddMetricRow(newValue.Metric11MetricId, newValue.Metric11Ratio, false);
                AddMetricRow(newValue.Metric12MetricId, newValue.Metric12Ratio, false);
                AddMetricRow(newValue.Metric13MetricId, newValue.Metric13Ratio, false);
                AddMetricRow(newValue.Metric14MetricId, newValue.Metric14Ratio, false);
                AddMetricRow(newValue.Metric15MetricId, newValue.Metric15Ratio, false);
                AddMetricRow(newValue.Metric16MetricId, newValue.Metric16Ratio, false);
                AddMetricRow(newValue.Metric17MetricId, newValue.Metric17Ratio, false);
                AddMetricRow(newValue.Metric18MetricId, newValue.Metric18Ratio, false);
                AddMetricRow(newValue.Metric19MetricId, newValue.Metric19Ratio, false);
                AddMetricRow(newValue.Metric20MetricId, newValue.Metric20Ratio, false);

            }

          

            this.SelectedMetric = _availableMetrics.FirstOrDefault();
            this.SelectedMetricRow = this.MetricRows.FirstOrDefault();
        }

        /// <summary>
        /// Responds to collection changes on the metric rows collection
        /// </summary>
        private void MetricRows_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (e.ChangedItems.Count > 0)
                    {
                        foreach (MetricRow row in e.ChangedItems)
                        {
                            row.PropertyChanged += MetricRow_PropertyChanged;
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (MetricRow row in e.ChangedItems)
                    {
                        row.PropertyChanged -= MetricRow_PropertyChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (MetricRow row in e.ChangedItems)
                        {
                            row.PropertyChanged -= MetricRow_PropertyChanged;
                        }
                    }
                    foreach (MetricRow row in _metricRows)
                    {
                        row.PropertyChanged += MetricRow_PropertyChanged;
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to property changes on a metric row.
        /// </summary>
        private void MetricRow_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            MetricRow row = sender as MetricRow;
            if (row != null)
            {
                //rounded ratio
                if (e.PropertyName == MetricRow.RoundedRatioProperty.Path)
                {
                    OnMetricRatioChanged(row);
                }

                //ratio
                if (e.PropertyName == MetricRow.RatioProperty.Path)
                {
                    OnMetricRatioChanged(row);
                }

                // percentage
                else if (e.PropertyName == MetricRow.PercentageProperty.Path)
                {

                    OnMetricPercentageChanged(row);
                }
            }
        }


        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.CurrentItem, this.SaveCommand);
            }
            else if (this.CurrentItem != null && this.CurrentItem.IsDirty && !this.CurrentItem.IsInitialized)
            {
                //we are unit testing so just fire off a property change if the dialog would be shown.
                OnPropertyChanged("ContinueWithItemChange");
            }
            return continueExecute;
        }

        /// <summary>
        /// Creates the metric row.
        /// </summary>
        private MetricRow AddMetricRow(Int32? metricId, Single? ratio, Boolean updateSelections = true)
        {
            MetricRow row = null;

            if (metricId.HasValue && metricId > 0
                && ratio.HasValue && ratio > 0)
            {
                Metric metric = _availableMetrics.FirstOrDefault(m => m.Id == metricId.Value);
                if (metric != null)
                {
                    row = new MetricRow(metric, ratio.Value, 0);

                    //add to rows collection
                    _metricRows.Add(row);

                    //remove from the available
                    _availableMetrics.Remove(metric);

                    UpdateMetricPercentages();

                    if (updateSelections)
                    {

                        //select the new row
                        this.SelectedMetricRow = row;

                        //select the next available metric
                        if (this.SelectedMetric == metric
                            || this.SelectedMetric == null)
                        {
                            this.SelectedMetric = this.AvailableMetrics.FirstOrDefault();
                        }
                    }
                }
            }

            return row;
        }

        /// <summary>
        /// Removes the given metric row.
        /// </summary>
        /// <param name="row"></param>
        private void RemoveMetricRow(MetricRow row)
        {
            if (row != null)
            {
                //remove row
                _metricRows.Remove(row);

                //add back to the available
                _availableMetrics.Add(row.Metric);

                UpdateMetricPercentages();

                if (this.SelectedMetricRow == row)
                {
                    this.SelectedMetricRow = this.MetricRows.FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Iterates through all the assigned metrics updating the percentages
        /// so that the supplied metric percentage stays the same. The supplied
        /// metric then has its ratio updated to that it warrents its percentage.
        /// </summary>
        /// <param name="metric"></param>
        private void OnMetricPercentageChanged(MetricRow metric)
        {
            if (!_updatingPercentages && !_updatingRatios)
            {
                _updatingRatios = true;

                if (_metricRows.Count == 1)
                {
                    metric.Ratio = 1;
                    metric.Percentage = 100;
                }
                else
                {
                    Single availablePercent = 100 - metric.Percentage;
                    Single totalCurrentPercent = _metricRows.Where(m => !m.Equals(metric)).Select(p => p.Percentage).Sum();
                    Int32 metricCount = _metricRows.Where(m => !m.Equals(metric)).Count();

                    //re-set all other row percentages by this
                    foreach (MetricRow row in _metricRows)
                    {
                        if (row != metric)
                        {
                            if (totalCurrentPercent != 0)
                            {
                                row.Percentage = (Single)Math.Round(Math.Abs(row.Percentage / (totalCurrentPercent / availablePercent)));
                            }
                            else if (availablePercent != 0)
                            {
                                row.Percentage = (Single)Math.Round(Math.Abs(availablePercent / metricCount));
                            }
                            else
                            {
                                row.Percentage = 0;
                            }
                        }
                    }

                    //reset all row ratios based on the new percentages
                    Single[] values = _metricRows.Select(r => r.Percentage).ToArray();

                    //get the highest common denom
                    Int32 highestCommonDenom = 100;
                    for (Int32 i = 1; i <= (Int32)values.Min(); i++)
                    {
                        Boolean isCommon = true;
                        foreach (Single p in values)
                        {
                            if (p % i != 0)
                            {
                                isCommon = false;
                                break;
                            }
                        }

                        if (isCommon)
                        {
                            highestCommonDenom = i;
                        }
                    }

                    for (Int32 i = 0; i < values.Length; i++)
                    {
                        values[i] = (Single)Math.Round((values[i] / (Single)highestCommonDenom), 4);
                    }

                    if (!values.Any(v => v == 1))
                    {
                        //divide by the lowest value
                        Single lowestVal = values.Min();
                        for (Int32 i = 0; i < values.Length; i++)
                        {
                            values[i] = (Single)Math.Round((values[i] / lowestVal), 4);
                        }
                    }

                    for (Int32 i = 0; i < _metricRows.Count; i++)
                    {
                        _metricRows[i].Ratio = values[i];
                    }

                }

                _updatingRatios = false;
            }


        }

        /// <summary>
        /// Updates all other ratios after one has changed
        /// </summary>
        /// <param name="metric"></param>
        private void OnMetricRatioChanged(MetricRow metric)
        {
            if (!_updatingPercentages && !_updatingRatios)
            {
                _updatingRatios = true;

                //if all other ratios are 0 then reset to 1.
                if (_metricRows.All(m => m.Ratio == 0))
                {
                    foreach (MetricRow row in _metricRows)
                    {
                        if (row != metric)
                        {
                            row.Ratio = 1;
                        }
                    }
                }

                _updatingRatios = false;
            }

            UpdateMetricPercentages();
        }

        /// <summary>
        /// Iterates through all the assigned metrics updating their percentages
        /// in relation to their ratios.
        /// </summary>
        private void UpdateMetricPercentages()
        {
            if (!_updatingPercentages && !_updatingRatios)
            {
                _updatingPercentages = true;

                Single ratioSum = _metricRows.Select(p => p.Ratio).Sum();
                foreach (MetricRow row in _metricRows)
                {
                    if (row.Ratio == 0)
                    {
                        row.Percentage = 0;
                    }
                    else
                    {
                        row.Percentage = Math.Abs(row.Ratio / ratioSum * 100);
                    }
                }
                _updatingPercentages = false;
            }
        }

        /// <summary>
        /// Updates all the profile metrics with the metrics in
        /// the assigned metric list.
        /// </summary>
        private void UpdateMetricProfile()
        {
            MetricProfile profile = this.CurrentItem;
            if (profile == null) { return; }

            profile.ClearMetrics();

            Int32 orderNo = 1;
            foreach (var row in this.MetricRows)
            {
                if (row.Ratio != 0)
                {
                    profile.SetMetric(orderNo, row.Metric.Id, row.Ratio);
                    orderNo++;
                }
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                _masterMetricProfilesView.Dispose();

                base.IsDisposed = true;
            }
        }

        #endregion

    }

}
