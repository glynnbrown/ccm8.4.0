﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26123 : L.Ineson
//	Created
// V8-29218 : L.Luong
//  Added new property to show a rounded value as actual ratio value needs to be more precise
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.MetricProfileMaintenance
{
    public sealed class MetricRow : INotifyPropertyChanged
    {
        #region Fields
        private Metric _metric = null;
        private Single _ratio = 1;
        private Single _percentage = 100;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<MetricRow>(p => p.Name);
        public static readonly PropertyPath RatioProperty = WpfHelper.GetPropertyPath<MetricRow>(p => p.Ratio);
        public static readonly PropertyPath RoundedRatioProperty = WpfHelper.GetPropertyPath<MetricRow>(p => p.RoundedRatio);
        public static readonly PropertyPath PercentageProperty = WpfHelper.GetPropertyPath<MetricRow>(p => p.Percentage);
        public static readonly PropertyPath MetricProperty = WpfHelper.GetPropertyPath<MetricRow>(p => p.Metric);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the metric represented by this row.
        /// </summary>
        public Metric Metric
        {
            get
            {
                return _metric;
            }
        }

        /// <summary>
        /// Returns the metric name
        /// </summary>
        public String Name
        {
            get
            {
                if (_metric != null)
                {
                    return _metric.Name;
                }
                return null;
            }
        }

        /// <summary>
        /// gets a rounded ratio for user to see
        /// </summary>
        public Single RoundedRatio
        {
            get 
            { 
                return (Single)Math.Round((double)_ratio, 2, MidpointRounding.AwayFromZero); 
            }
            set
            {
                if (Single.IsInfinity(value) || Single.IsNaN(value))
                {
                    value = 1;
                }
                else
                {
                    _ratio = value;
                }
                OnPropertyChanged(RoundedRatioProperty);
                OnPropertyChanged(RatioProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the metric ratio 
        /// </summary>
        public Single Ratio
        {
            get
            {
                return _ratio;
            }
            set
            {
                if (Single.IsInfinity(value) || Single.IsNaN(value))
                {
                    value = 1;
                }
                else
                {
                    _ratio = value;
                }
                OnPropertyChanged(RatioProperty);
                OnPropertyChanged(RoundedRatioProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the metric percentage
        /// </summary>
        public Single Percentage
        {
            get
            {
                return _percentage;
            }
            set
            {
                if (value > 100 || Single.IsInfinity(value))
                {
                    _percentage = 100;
                }
                else if (value < 0 || Single.IsNaN(value))
                {
                    _percentage = 0;
                }
                else
                {
                    _percentage = Convert.ToSingle(Math.Round((Double)value, 2, MidpointRounding.AwayFromZero));
                }
                OnPropertyChanged(PercentageProperty);
            }
        }

        #endregion

        #region Constuctor

        /// <summary>
        /// Constructor
        /// </summary>
        public MetricRow(Metric metric, Single ratio, Single percentage)
        {
            _metric = metric;
            _ratio = ratio;
            _percentage = Convert.ToSingle(Math.Round((Double)percentage, 2, MidpointRounding.AwayFromZero));
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
