﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26123 : L.Ineson
//	Created
#endregion

#endregion

using System.Windows;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.MetricProfileMaintenance
{
    /// <summary>
    /// Interaction logic for MetricProfileMaintenanceHomeTab.xaml
    /// </summary>
    public partial class MetricProfileMaintenanceHomeTab : RibbonTabItem
    {

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(MetricProfileMaintenanceViewModel), typeof(MetricProfileMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public MetricProfileMaintenanceViewModel ViewModel
        {
            get { return (MetricProfileMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MetricProfileMaintenanceHomeTab senderControl = (MetricProfileMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                MetricProfileMaintenanceViewModel oldModel = (MetricProfileMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                MetricProfileMaintenanceViewModel newModel = (MetricProfileMaintenanceViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MetricProfileMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion

    }
}
