﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26159 : L.Ineson
//	Created
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Galleria.Framework.Controls.Wpf;
using System.Diagnostics.CodeAnalysis;
using Fluent;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.MetricProfileMaintenance
{
    /// <summary>
    /// Interaction logic for MetricProfileMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class MetricProfileMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        const String _deleteMetricCommandKey = "DeleteMetricCommand";
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(MetricProfileMaintenanceViewModel), typeof(MetricProfileMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public MetricProfileMaintenanceViewModel ViewModel
        {
            get { return (MetricProfileMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MetricProfileMaintenanceOrganiser senderControl = (MetricProfileMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                MetricProfileMaintenanceViewModel oldModel = (MetricProfileMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(_deleteMetricCommandKey);
            }

            if (e.NewValue != null)
            {
                MetricProfileMaintenanceViewModel newModel = (MetricProfileMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Add(_deleteMetricCommandKey, newModel.DeleteMetricCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MetricProfileMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.MetricProfileMaintenance);

            this.ViewModel = new MetricProfileMaintenanceViewModel();


            this.Loaded += MetricProfileMaintenanceOrganiser_Loaded;
        }

        private void MetricProfileMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MetricProfileMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close

        /// <summary>
        /// On the application being closed, checked if changes may require saving
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }

            }

        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion

    }


}
