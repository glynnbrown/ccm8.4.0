﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32041 : A.Silva
//  Created.
// V8-32149 : A.Silva
//  Synced the selected fields before saving the item edit.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Input;
using Aspose.Pdf;
using Csla;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramComparisonTemplateMaintenance
{
    /// <summary>
    ///     Viewmodel controller for PlanogramComparisonTemplateMaintenanceOrganiser
    /// </summary>
    public sealed class PlanogramComparisonTemplateMaintenanceViewModel : WindowViewModelBase
    {
        #region Fields

        /// <summary>
        ///     The current user's permission regarding instances of <see cref="PlanogramComparisonTemplate"/>.
        /// </summary>
        private readonly ModelPermission<PlanogramComparisonTemplate> _itemPermissions;

        /// <summary>
        ///     The collection view holding all <see cref="PlanogramComparisonTemplate"/> items for the current Entity.
        /// </summary>
        private readonly PlanogramComparisonTemplateInfoListView _masterItems = new PlanogramComparisonTemplateInfoListView();

        /// <summary>
        ///     Whether the field picker viewmodels have been intialized already or not.
        /// </summary>
        private Boolean _fieldPickerViewModelsInitialized;

        #endregion

        #region Properties

        #region AvailableItems

        public ReadOnlyBulkObservableCollection<PlanogramComparisonTemplateInfo> AvailableItems { get { return _masterItems.BindableCollection; } }

        #endregion

        #region ProductFieldPickerViewModel

        /// <summary>
        ///     The <see cref="FieldPickerViewModel"/> that is holding the current item's product comparison fields.
        /// </summary>
        private FieldPickerViewModel _productFieldPickerViewModel;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="ProductFieldPickerViewModel"/> property.
        /// </summary>
        public static readonly PropertyPath ProductFieldPickerViewModelProperty = NewPropertyPath(p => p.ProductFieldPickerViewModel);

        /// <summary>
        ///     Get the <see cref="FieldPickerViewModel"/> that holds the currently edited item's product comparison fields.
        /// </summary>
        public FieldPickerViewModel ProductFieldPickerViewModel
        {
            get { return _productFieldPickerViewModel; }
            private set
            {
                _productFieldPickerViewModel = value;
                OnPropertyChanged(ProductFieldPickerViewModelProperty);
            }
        }

        #endregion

        #region PositionFieldPickerViewModel

        /// <summary>
        ///     The <see cref="FieldPickerViewModel"/> that is holding the current item's position comparison fields.
        /// </summary>
        private FieldPickerViewModel _positionFieldPickerViewModel;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="ItemEdit"/> property.
        /// </summary>
        public static readonly PropertyPath PositionFieldPickerViewModelProperty = NewPropertyPath(p => p.PositionFieldPickerViewModel);

        /// <summary>
        ///     Get the <see cref="FieldPickerViewModel"/> that holds the currently edited item's position comparison fields.
        /// </summary>
        public FieldPickerViewModel PositionFieldPickerViewModel
        {
            get { return _positionFieldPickerViewModel; }
            private set
            {
                _positionFieldPickerViewModel = value;
                OnPropertyChanged(PositionFieldPickerViewModelProperty);
            }
        }

        #endregion

        #region ItemEdit

        /// <summary>
        ///     The <see cref="PlanogramComparisonTemplate"/> that is being currently edited.
        /// </summary>
        private PlanogramComparisonTemplate _itemEdit;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="ItemEdit"/> property.
        /// </summary>
        private static readonly PropertyPath PlanogramComparisonTemplateEditProperty = NewPropertyPath(p => p.ItemEdit);

        /// <summary>
        ///     Get the <see cref="PlanogramComparisonTemplate"/> currently being edited.
        /// </summary>
        public PlanogramComparisonTemplate ItemEdit
        {
            get { return _itemEdit; }
            private set
            {
                _itemEdit = value;
                OnPropertyChanged(PlanogramComparisonTemplateEditProperty);
                OnItemEditChanged(_itemEdit);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="PlanogramComparisonTemplateMaintenanceViewModel"/>.
        /// </summary>
        public PlanogramComparisonTemplateMaintenanceViewModel()
        {
            //  Get the user permissions.
            _itemPermissions = new ModelPermission<PlanogramComparisonTemplate>(PlanogramComparisonTemplate.GetUserPermissions());

            //  Fetch all existing.
            _masterItems.FetchAllForEntity();

            InitializeFieldPickerViewModels();

            //  Create a new item.
            NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region New

        /// <summary>
        ///     The <see cref="RelayCommand"/> that creates a new item as the current item.
        /// </summary>
        private RelayCommand _newCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="NewCommand"/> property.
        /// </summary>
        public static readonly PropertyPath NewCommandProperty = NewPropertyPath(p => p.NewCommand);

        /// <summary>
        ///     Get the New command.
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                //  Check whether the command is already initialized.
                if (_newCommand != null) return _newCommand;

                //  Initialize the command.
                _newCommand =
                    new RelayCommand(o => New_Executed(), o => New_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                RegisterCommand(_newCommand);
                return _newCommand;
            }
        }

        private Boolean New_CanExecute()
        {
            //  NB: The Create permission is checked when saving.

            //  Can execute.
            return true;
        }

        private void New_Executed()
        {
            if (!ContinueWithItemChange()) return;

            //  Create a new item.
            PlanogramComparisonTemplate newItem = PlanogramComparisonTemplate.NewPlanogramComparisonTemplate(App.ViewState.EntityId);
            newItem.MarkGraphAsInitialized();

            ItemEdit = newItem;

            //  Close the Backstage.
            CloseRibbonBackstage();
        }

        #endregion

        #region Open

        /// <summary>
        ///     The <see cref="RelayCommand"/> that opens an existing item as the current item by its template Id.
        /// </summary>
        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="OpenCommand"/> property.
        /// </summary>
        public static readonly PropertyPath OpenCommandProperty = NewPropertyPath(p => p.OpenCommand);

        /// <summary>
        ///     Get the Open command.
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                //  Check whether the command is already initialized.
                if (_openCommand != null) return _openCommand;

                //  Initialize the command.
                _openCommand =
                    new RelayCommand<Int32?>(Open_Executed, Open_CanExecute)
                    {
                        FriendlyName = Message.Generic_Open,
                        FriendlyDescription = Message.PlanogramComparisonTemplateMaintenance_Open_Description,
                        SmallIcon = ImageResources.Open_16
                    };
                RegisterCommand(_openCommand);
                return _openCommand;
            }
        }

        private Boolean Open_CanExecute(Int32? itemId)
        {
            //  Check whether the user does not have fetch permission.
            if (!_itemPermissions.CanFetch)
            {
                OpenCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //  Check whether there is an item to open at all.
            if (!itemId.HasValue)
            {
                OpenCommand.DisabledReason = String.Empty;
                return false;
            }
            //  Can execute.
            return true;
        }

        private void Open_Executed(Int32? itemId)
        {
            //  Check whether there is really an id to fetch and 
            //  that the user is fine with losing previous changes.
            if (!itemId.HasValue ||
                !ContinueWithItemChange()) return;

            ShowWaitCursor(true);

            try
            {
                ItemEdit = PlanogramComparisonTemplate.FetchById(itemId.Value);
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            //  Close the backstage.
            CloseRibbonBackstage();

            ShowWaitCursor(false);
        }

        #endregion

        #region Save

        /// <summary>
        ///     The <see cref="RelayCommand"/> that saves the current item to the DAL.
        /// </summary>
        private RelayCommand _saveCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="SaveCommand"/> property.
        /// </summary>
        public static readonly PropertyPath SaveCommandProperty = NewPropertyPath(p => p.SaveCommand);

        /// <summary>
        ///     Get the Save command.
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                //  Check whether the command is already initialized.
                if (_saveCommand != null) return _saveCommand;

                //  Initialize the command.
                _saveCommand =
                    new RelayCommand(o => Save_Executed(), o => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                RegisterCommand(_saveCommand);
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //  Check whether the current item is null.
            if (ItemEdit == null)
            {
                SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            //  Check whether the user does not have create permission.
            if (ItemEdit.IsNew &&
                !_itemPermissions.CanCreate)
            {
                SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //  Check whether the user does not have edit permission.
            if (!ItemEdit.IsNew &&
                !_itemPermissions.CanEdit)
            {
                SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //  Check whether the current item is not valid.
            if (!ItemEdit.IsValid)
            {
                SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //  Can execute.
            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        #endregion

        #region SaveAs

        /// <summary>
        ///     The <see cref="RelayCommand"/> that Saves the current item as a new item to the DAL.
        /// </summary>
        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="SaveAsCommand"/> property.
        /// </summary>
        public static readonly PropertyPath SaveAsCommandProperty = NewPropertyPath(p => p.SaveAsCommand);

        /// <summary>
        ///     Get the SaveAs command.
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                //  Check whether the command is already initialized.
                if (_saveAsCommand != null) return _saveAsCommand;

                //  Initialize the command.
                _saveAsCommand =
                    new RelayCommand(o => SaveAs_Executed(), o => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F12
                    };
                RegisterCommand(_saveAsCommand);
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //  Check whether the current item is null.
            if (ItemEdit == null)
            {
                SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //  Check whether the user does not have create permission.
            if (!_itemPermissions.CanCreate)
            {
                SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //  Check whether the current item is not valid.
            if (!ItemEdit.IsValid)
            {
                SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //  Can execute.
            return true;
        }

        private void SaveAs_Executed()
        {
            //  Confirm the new name with the user.
            String saveAsName;
            Boolean nameAccepted =
                GetWindowService()
                    .PromptForSaveAsName(
                        name => AvailableItems.Any(item => String.Equals(item.Name, name, StringComparison.InvariantCultureIgnoreCase)),
                        String.Empty,
                        out saveAsName);
            if (!nameAccepted) return;

            //  Copy the item and update the chosen name.
            ShowWaitCursor(true);

            PlanogramComparisonTemplate saveAsItem = ItemEdit.Copy();
            saveAsItem.Name = saveAsName;
            ItemEdit = saveAsItem;

            ShowWaitCursor(false);

            //  Save the item.
            SaveCurrentItem();
        }

        #endregion

        #region SaveAndNew

        /// <summary>
        ///     The <see cref="RelayCommand"/> that saves the current item to the DAL and creates a new one for editing.
        /// </summary>
        private RelayCommand _saveAndNewCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="SaveAndNewCommand"/> property.
        /// </summary>
        public static readonly PropertyPath SaveAndNewCommandProperty = NewPropertyPath(p => p.SaveAndNewCommand);

        /// <summary>
        ///     Get the SaveAndNew command.
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                //  Check whether the command is already initialized.
                if (_saveAndNewCommand != null) return _saveAndNewCommand;

                //  Initialize the command.
                _saveAndNewCommand =
                    new RelayCommand(o => SaveAndNew_Executed(), o => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                RegisterCommand(_saveAndNewCommand);
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //  Create a new item if the save is succesful.
            if (SaveCurrentItem())
            {
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndClose

        /// <summary>
        ///     The <see cref="RelayCommand"/> that saves the current item to the DAL and closes the screen.
        /// </summary>
        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="SaveAndCloseCommand"/> property.
        /// </summary>
        public static readonly PropertyPath SaveAndCloseCommandProperty = NewPropertyPath(p => p.SaveAndCloseCommand);

        /// <summary>
        ///     Get the SaveAndClose command.
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                //  Check whether the command is already initialized.
                if (_saveAndCloseCommand != null) return _saveAndCloseCommand;

                //  Initialize the command.
                _saveAndCloseCommand =
                    new RelayCommand(o => SaveAndClose_Executed(), o => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                RegisterCommand(_saveAndCloseCommand);
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //  Close the window if the save is succesful.
            if (SaveCurrentItem())
            {
                CloseWindow();
            }
        }

        #endregion

        #region Delete

        /// <summary>
        ///     The <see cref="RelayCommand"/> that deletes the current item from the DAL.
        /// </summary>
        private RelayCommand _deleteCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="DeleteCommand"/> property.
        /// </summary>
        public static readonly PropertyPath DeleteCommandProperty = NewPropertyPath(p => p.DeleteCommand);

        /// <summary>
        ///     Get the Delete command.
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                //  Check whether the command is already initialized.
                if (_deleteCommand != null) return _deleteCommand;

                //  Initialize the command.
                _deleteCommand =
                    new RelayCommand(o => Delete_Executed(), o => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                RegisterCommand(_deleteCommand);
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //  Check whether the user does not have delete permission.
            if (!_itemPermissions.CanDelete)
            {
                DeleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //  Check whether there is an item to Delete at all.
            if (ItemEdit == null)
            {
                DeleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //  Check whether the item is new.
            if (ItemEdit.IsNew)
            {
                DeleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //  Can execute.
            return true;
        }

        private void Delete_Executed()
        {
            //  Check whether the user really wants to delete.
            if (!GetWindowService().ConfirmDeleteWithUser(ItemEdit.Name)) return;

            ShowWaitCursor(true);

            //  Mark the item as deleted so that the Item Changed warning does not show.
            PlanogramComparisonTemplate itemToDelete = ItemEdit;
            itemToDelete.Delete();

            //  Create a new item.
            NewCommand.Execute();

            //  Commit the delete.
            try
            {
                itemToDelete.Save();
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Delete);
                return;
            }

            //  Refresh the master items list.
            _masterItems.FetchAllForEntity();

            ShowWaitCursor(false);
        }

        #endregion

        #region Close

        /// <summary>
        ///     The <see cref="RelayCommand"/> that closes the screen if the user confirms losing any changes.
        /// </summary>
        private RelayCommand _closeCommand;

        /// <summary>
        ///     The static <see cref="PropertyPath"/> used to refer to the <see cref="CloseCommand"/> property.
        /// </summary>
        public static readonly PropertyPath CloseCommandProperty = NewPropertyPath(p => p.CloseCommand);

        /// <summary>
        ///     Get the Close command.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                //  Check whether the command is already initialized.
                if (_closeCommand != null) return _closeCommand;

                //  Initialize the command.
                _closeCommand =
                    new RelayCommand(o => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                RegisterCommand(_closeCommand);
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            CloseWindow();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Initialize the field picker viewmodels so the have the currently avaiable fields to display to the user.
        /// </summary>
        /// <param name="dispose">Whether to just dispose of the view models rather than initializing them.</param>
        private void InitializeFieldPickerViewModels(Boolean dispose = false)
        {
            if (_fieldPickerViewModelsInitialized)
            {
                _fieldPickerViewModelsInitialized = false;

                _productFieldPickerViewModel.Dispose();
                _positionFieldPickerViewModel.Dispose();

                _productFieldPickerViewModel = null;
                _positionFieldPickerViewModel = null;
            }

            if (dispose) return;

            List<ObjectFieldInfo> fieldInfos = ListModelObjectFieldInfos();
            _productFieldPickerViewModel = new FieldPickerViewModel(fieldInfos.Where(IsProductFieldGroup));
            _positionFieldPickerViewModel = new FieldPickerViewModel(fieldInfos);

            _fieldPickerViewModelsInitialized = true;
        }

        /// <summary>
        ///     Sets the currently selected fields in each of the field picker viewmodels, 
        ///     and according to the current <see cref="ItemEdit"/> comparison fields.
        /// </summary>
        private void SetComparisonFields()
        {
            //  Check whether ItemEdit is actually set.
            if (ItemEdit == null) return;

            Dictionary<PlanogramItemType, List<IPlanogramComparisonSettingsField>> fieldsByType =
                ItemEdit.ComparisonFields.Cast<IPlanogramComparisonSettingsField>()
                        .GroupBy(o => o.ItemType)
                        .ToLookupDictionary(g => g.Key, g => g.ToList());

            List<IPlanogramComparisonSettingsField> comparisonFields;
            fieldsByType.TryGetValue(PlanogramItemType.Product, out comparisonFields);
            if (comparisonFields == null ||
                comparisonFields.Count == 0)
                comparisonFields = PlanogramComparerHelper.EnumerateDefaultProductComparisonFields().ToList();
            _productFieldPickerViewModel.SetObjectFieldInfos(comparisonFields);

            fieldsByType.TryGetValue(PlanogramItemType.Position, out comparisonFields);
            if (comparisonFields == null ||
                comparisonFields.Count == 0)
                comparisonFields = PlanogramComparerHelper.EnumerateDefaultPositionComparisonFields().ToList();
            _positionFieldPickerViewModel.SetObjectFieldInfos(comparisonFields);
        }

        /// <summary>
        ///     Display a message requesting the user to confirm continuing even if the current item is has changes.
        /// </summary>
        /// <returns><c>True</c> if the user wants the action to continue regardless of lost changes. <c>False</c> otherwise.</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(ItemEdit, SaveCommand);
        }

        /// <summary>
        ///     Save the current <see cref="ItemEdit"/> to the DAL.
        /// </summary>
        /// <returns><c>True</c> if the save was succesful, <c>false</c> if not.</returns>
        private Boolean SaveCurrentItem()
        {
            //  Check whether the item's name is a unique value
            Object itemId = ItemEdit.Id;
            String newName;
            ValidationTemplateInfoList existingItems = ValidationTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);

            Boolean nameAccepted = GetWindowService().PromptIfNameIsNotUnique(
                s => { return !existingItems.Any(p => !Equals(p.Id, itemId) && String.Equals(p.Name, s, StringComparison.InvariantCultureIgnoreCase)); },
                ItemEdit.Name, out newName);
            if (!nameAccepted) return false;

            //  If the user changed the name to make it valid, update their decision.
            if (ItemEdit.Name != newName) ItemEdit.Name = newName;

            //  Perform the save.
            ShowWaitCursor(true);
            try
            {
                SyncComparisonFields(ItemEdit);
                ItemEdit = ItemEdit.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);

                RecordException(ex);
                Exception rootException = ex.GetBaseException();

                //  If it is a concurrency exception check whether the user wants to reload.
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(ItemEdit.Name);
                    if (itemReloadRequired)
                    {
                        ItemEdit = PlanogramComparisonTemplate.FetchById(itemId);
                    }
                }
                else
                {
                    //  Otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(ItemEdit.Name, OperationType.Save);
                }
                return false;
            }

            //  Refresh the master items list.
            _masterItems.FetchAllForEntity();

            ShowWaitCursor(false);

            return true;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked whenever the <see cref="ItemEdit"/> property is updated.
        /// </summary>
        /// <param name="newValue">The new value for the property.</param>
        private void OnItemEditChanged(PlanogramComparisonTemplate newValue)
        {
            if (newValue != null) SetComparisonFields();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="PlanogramComparisonTemplateMaintenanceViewModel" />
        ///     property indicated by the <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath NewPropertyPath(Expression<Func<PlanogramComparisonTemplateMaintenanceViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        /// <summary>
        ///     Lists all Model Object Field Infos in a planogram.
        /// </summary>
        /// <returns>A new <see cref="List{ObjectFieldInfo}"/> with the <see cref="ObjectFieldInfo"/> present in a Planogram.</returns>
        private static List<ObjectFieldInfo> ListModelObjectFieldInfos()
        {
            List<ObjectFieldInfo> fieldInfos = PlanogramFieldHelper.EnumerateAllFields().ToList();
            foreach (ObjectFieldInfo field in fieldInfos)
            {
                field.GroupName = field.OwnerFriendlyName;
                field.PropertyName = String.Format("{0}.{1}", field.OwnerType, field.PropertyName);
            }
            return fieldInfos;
        }

        /// <summary>
        ///     Whether the given <paramref name="field"/> is one to be included for Products.
        /// </summary>
        /// <param name="field">The <see cref="ObjectFieldInfo"/> to determine whether to include for Products or not.</param>
        /// <returns><c>True</c> when the field should be included for Products, <c>false</c> otherwise.</returns>
        private static Boolean IsProductFieldGroup(ObjectFieldInfo field)
        {
            var allowedTypes = new List<Type> { typeof(PlanogramProduct), typeof(Planogram), typeof(PlanogramInventory) };
            return allowedTypes.Contains(field.OwnerType);
        }

        private void SyncComparisonFields(IPlanogramComparisonSettings settings)
        {
            settings.ResetComparisonFields();
            settings.AddComparisonFields(PlanogramItemType.Product, ProductFieldPickerViewModel.FieldPickerRows);
            settings.AddComparisonFields(PlanogramItemType.Position, PositionFieldPickerViewModel.FieldPickerRows);
        }

        #endregion

        #region IDisposable Support

        protected override void Dispose(Boolean disposing)
        {
            //  No need to dispose if already disposed.
            if (IsDisposed) return;

            //  Check whether disponsing is needed.
            if (disposing)
            {
                //  Dispose of any resources that need to.
                DisposeBase();
            }

            //  Mark the instance as disposed so that it is not disposed again.
            IsDisposed = true;
        }

        #endregion
    }
}