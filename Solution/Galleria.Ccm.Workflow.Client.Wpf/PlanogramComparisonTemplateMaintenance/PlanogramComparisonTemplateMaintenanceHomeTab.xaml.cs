﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32041 : A.Silva
//  Created.

#endregion

#endregion

using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramComparisonTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for ValidateTemplateMaintenanceHomeTab.xaml
    /// </summary>
    public partial class PlanogramComparisonTemplateMaintenanceHomeTab
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof (PlanogramComparisonTemplateMaintenanceViewModel), typeof (PlanogramComparisonTemplateMaintenanceHomeTab),
            new PropertyMetadata(null));

        public PlanogramComparisonTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramComparisonTemplateMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramComparisonTemplateMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
