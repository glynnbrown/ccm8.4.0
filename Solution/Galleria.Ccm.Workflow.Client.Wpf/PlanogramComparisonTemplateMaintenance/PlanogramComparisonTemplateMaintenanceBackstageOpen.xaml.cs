﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32041 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Model;
using Galleria.Reporting.Controls.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramComparisonTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for ValidateTemplateMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class PlanogramComparisonTemplateMaintenanceBackstageOpen
    {
        #region Fields

        private readonly ObservableCollection<PlanogramComparisonTemplateInfo> _searchResults = new ObservableCollection<PlanogramComparisonTemplateInfo>();
        
        #endregion
        
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramComparisonTemplateMaintenanceViewModel), typeof(PlanogramComparisonTemplateMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = (PlanogramComparisonTemplateMaintenanceBackstageOpen)obj;

            var oldModel = e.OldValue as PlanogramComparisonTemplateMaintenanceViewModel;
            if (oldModel != null)
            {
                oldModel.AvailableItems.BulkCollectionChanged -= senderControl.ViewModel_AvailableValidationTemplatesBulkCollectionChanged;
            }

            var newModel = e.NewValue as PlanogramComparisonTemplateMaintenanceViewModel;
            if (newModel != null)
            {
                newModel.AvailableItems.BulkCollectionChanged += senderControl.ViewModel_AvailableValidationTemplatesBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }

        public PlanogramComparisonTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramComparisonTemplateMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SearchResults Property

        public static readonly DependencyProperty SearchResultsProperty = 
            DependencyProperty.Register("SearchResults", typeof (ReadOnlyObservableCollection<PlanogramComparisonTemplateInfo>),
            typeof (PlanogramComparisonTemplateMaintenanceBackstageOpen),  
            new PropertyMetadata(default(ReadOnlyObservableCollection<PlanogramComparisonTemplateInfo>)));

        public ReadOnlyObservableCollection<PlanogramComparisonTemplateInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<PlanogramComparisonTemplateInfo>) GetValue(SearchResultsProperty); }
            set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #region SearchText Property

        public static readonly DependencyProperty SearchTextProperty = 
            DependencyProperty.Register("SearchText", typeof (String), typeof (PlanogramComparisonTemplateMaintenanceBackstageOpen), 
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((PlanogramComparisonTemplateMaintenanceBackstageOpen)obj).UpdateSearchResults();
        }

        /// <summary>
        /// Gets/Sets the criteria to search items for
        /// </summary>
        public String SearchText
        {
            get { return (String) GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramComparisonTemplateMaintenanceBackstageOpen()
        {
            SearchResults = new ReadOnlyObservableCollection<PlanogramComparisonTemplateInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked when the collection of available validation templates changes.
        /// </summary>
        private void ViewModel_AvailableValidationTemplatesBulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }


        /// <summary>
        ///     Invoked when the user double clicks on the search list.
        /// </summary>
        private void XSearchResults_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var senderControl = sender as ListBox;
            if (senderControl == null) return;

            var item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (item == null) return;

            var target = senderControl.SelectedItem as PlanogramComparisonTemplateInfo;
            if (target == null) return;

            ViewModel.OpenCommand.Execute(target.Id);
        }

        #endregion

        #region Methods

        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (ViewModel == null) return;

            String searchPattern = LocalHelper.GetRexexKeywordPattern(SearchText);

            var results =
                ViewModel.AvailableItems
                .Where(w => Regex.IsMatch(w.Name, searchPattern, RegexOptions.IgnoreCase))
                .OrderBy(l => l.ToString());

            foreach (var info in results.OrderBy(l => l.ToString()))
            {
                _searchResults.Add(info);
            }

        }

        #endregion

        private void XSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {

                var senderControl = sender as ListBox;
                if (senderControl == null) return;

                var item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
                if (item == null) return;

                var target = senderControl.SelectedItem as PlanogramComparisonTemplateInfo;
                if (target == null) return;

                ViewModel.OpenCommand.Execute(target.Id);
            }
        }
    }
}