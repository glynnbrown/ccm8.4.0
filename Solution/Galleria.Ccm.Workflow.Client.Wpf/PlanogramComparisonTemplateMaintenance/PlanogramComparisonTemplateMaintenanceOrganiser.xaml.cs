﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-32041 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramComparisonTemplateMaintenance
{
    /// <summary>
    ///     This Screen allows the user to perform various maintenance tasks related to Planogram Comparison Templates.
    /// </summary>
    public sealed partial class PlanogramComparisonTemplateMaintenanceOrganiser
    {
        #region ViewModel

        /// <summary>
        ///     The instance of <see cref="PlanogramComparisonTemplateMaintenanceViewModel"/> acting as this View's ViewModel.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                        typeof (PlanogramComparisonTemplateMaintenanceViewModel),
                                        typeof (PlanogramComparisonTemplateMaintenanceOrganiser),
                                        new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        ///     The instance of <see cref="PlanogramComparisonTemplateMaintenanceViewModel"/> acting as this View's ViewModel.
        /// </summary>
        public PlanogramComparisonTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramComparisonTemplateMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        
        /// <summary>
        ///     Invoked whenever the value of the <see cref="ViewModel"/> property for this instance changes.
        /// </summary>
        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var sender = (PlanogramComparisonTemplateMaintenanceOrganiser) obj;

            var oldModel = e.OldValue as PlanogramComparisonTemplateMaintenanceViewModel;
            if (oldModel != null)
            {
                oldModel.UnregisterWindowControl();
            }

            var newModel = e.NewValue as PlanogramComparisonTemplateMaintenanceViewModel;
            if (newModel != null)
            {
                newModel.RegisterWindowControl(sender);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initialize a new instance of <see cref="PlanogramComparisonTemplateMaintenanceOrganiser"/>.
        /// </summary>
        public PlanogramComparisonTemplateMaintenanceOrganiser()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //TODO: HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramComparisonTemplateMaintenance);

            ViewModel = new PlanogramComparisonTemplateMaintenanceViewModel();

            Loaded += OnLoaded;
        }

        #endregion

        #region Window Close

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Window.Closing"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.ComponentModel.CancelEventArgs"/> that contains the event data.</param>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            //  Check whether the closing has already been canceled.
            if (e.Cancel) return;

            //  Confirm that it is fine to close.
            if (!ViewModel.ContinueWithItemChange())
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Window.Closed"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke((Action) (DisposeViewModel), DispatcherPriority.Background);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked when the screen has finished loading.
        /// </summary>
        private void OnLoaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= OnLoaded;
            Dispatcher.BeginInvoke((Action) (StopOverrideCursor));
        }

        #endregion

        #region Dispatchable Actions

        /// <summary>
        ///     Stop overriding the mouse cursor.
        /// </summary>
        private static void StopOverrideCursor()
        {
            Mouse.OverrideCursor = null;
        }

        /// <summary>
        ///     Set the <see cref="ViewModel"/> to null and dispose of it if it implements <see cref="IDisposable"/>.
        /// </summary>
        private void DisposeViewModel()
        {
            var disposableViewModel = ViewModel as IDisposable;
            ViewModel = null;

            if (disposableViewModel != null) disposableViewModel.Dispose();
        }

        #endregion
    }
}