﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25438 : L.Hodson ~ Created empty structure
// V8-25788 : M.Pettit ~ Fleshed out
// V8-26411 : M.Pettit
//  Removed Favourite Reports filter option from sidebar for initial release
// V8-26819 : M.Pettit
//  Selected Report cleared before refreshing search filter results
// GFS-26843 : M.Pettit
//  SelectedReport is now a ReportInfo rather than Report model object
// V8-26902 : M.Pettit
//  Updates to Reporting Dlls required corresonding minor client code changes
// V8-27290 : M.Pettit
//  Validate report when opening 
// V8-27290/V8-28107 : M.Pettit
//  Corrected issue whereby current reportlist is empty if validation changes selected report
// V8-28107 : M.Pettit
//  Corrected issue where pressing cancel during report validation opens the report anyway.
#endregion
#region Version History: CCM803
// V8-29345 : M.Pettit
//  ReportId is now an Object datatype
// V8-29471 : MN.Pettit
//  DataGrid viewer column order now follows UI order rules (by section/component.x-position)
#endregion
#region Version History: CCM811
// V8-30621 : M.Pettit
//  Added missing wait cursors to commands
#endregion
#endregion

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using Csla;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;
using Galleria.Reporting.Model;
using Galleria.Reporting.Processes.RenderPdfReport;
using Galleria.Ccm.Workflow.Client.Wpf.ReportSetup;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Processes.FetchReportData;
using Galleria.Reporting.Helpers;
using Galleria.Reporting.Controls.Wpf.ParameterSelector;
using Galleria.Reporting.Controls.Wpf.ReportGridViewer;

namespace Galleria.Ccm.Workflow.Client.Wpf.Reports
{
    /// <summary>
    /// Viewmodel controller for Reports Organiser
    /// </summary>
    public sealed class ReportsViewModel : ViewModelAttachedControlObject<ReportsOrganiser>
    {
        #region Constants
        private const Int32 FetchDataProgress_StartFetch = 10;
        private const Int32 FetchDataProgress_Complete = 100;

        private const Int32 ReportProgress_BuildingReport = 10;
        private const Int32 ReportProgress_SavingReport = 50;
        private const Int32 ReportProgress_OpeningReport = 90;
        #endregion

        #region Fields
        private ReportInfo _selectedReport;
        private ReadOnlyCollection<ReportInfo> _availableReports;
        private BulkObservableCollection<ReportInfo> _reportInfos = new BulkObservableCollection<ReportInfo>();
        private Int32 _reportCount = 0;

        private String _searchFilter = String.Empty;
        private ReadOnlyCollection<ReportFilterOptionItem> _filterOptions;
        private ReportFilterOptionItem _selectedFilterOption;


        //Print Preview
        private ModalBusy _previewBusy;
        private Exception _processException = null;
        #endregion

        #region Binding Properties

        public static readonly PropertyPath SelectedReportProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.SelectedReport);
        public static readonly PropertyPath AvailableReportsProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.AvailableReports);
        public static readonly PropertyPath SearchFilterProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.SearchFilter);
        public static readonly PropertyPath FilterOptionsProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.FilterOptions);
        public static readonly PropertyPath SelectedFilterOptionProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.SelectedFilterOption);

        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.NewCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.DeleteCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.OpenCommand);
        public static readonly PropertyPath ShowPdfViewerCommandProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.ShowPdfViewerCommand);
        public static readonly PropertyPath ShowDataViewerCommandProperty = WpfHelper.GetPropertyPath<ReportsViewModel>(c => c.ShowDataViewerCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of available reports 
        /// </summary>
        public ReadOnlyCollection<ReportInfo> AvailableReports
        {
            get { return _availableReports; }
            set 
            { 
                _availableReports = value;
                OnPropertyChanged(AvailableReportsProperty);
            }
        }

        /// <summary>
        /// Returns the selected report
        /// </summary>
        public ReportInfo SelectedReport
        {
            get { return _selectedReport; }
            set
            {
                _selectedReport = value;
                OnPropertyChanged(SelectedReportProperty);
                OnPropertyChanged(ShowPdfViewerCommandProperty);
                OnPropertyChanged(ShowDataViewerCommandProperty);
                OnPropertyChanged(OpenCommandProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the search criteria to use when filtering the available reports list.
        /// </summary>
        public String SearchFilter
        {
            get { return _searchFilter; }
            set
            {
                _searchFilter = value;
                OnPropertyChanged(SearchFilterProperty);

                OnSearchFilterChanged(value);
            }
        }

        public ReadOnlyCollection<ReportFilterOptionItem> FilterOptions
        {
            get { return _filterOptions; }
            set
            {
                _filterOptions = value;
                OnPropertyChanged(FilterOptionsProperty);
            }
        }

        public ReportFilterOptionItem SelectedFilterOption
        {
            get { return _selectedFilterOption; }
            set
            {
                _selectedFilterOption = value;
                OnPropertyChanged(SelectedFilterOptionProperty);

                OnSearchFilterChanged(_searchFilter);
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for the reports screen
        /// </summary>
        public ReportsViewModel()
        {
            //Debug only - update test reports
            //Galleria.Reporting.Model.DataModel.LoadDataModelFromFile(@"C:\Planograms.xml");

            InitialiseFilterOptionsList();

            OnSearchFilterChanged(this.SearchFilter);
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Opens a report in the the report builder window
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute(),
                        attachToCommandManager: false)
                    {
                        FriendlyName = Message.Reports_NewReport_Name,
                        FriendlyDescription = Message.Reports_NewReport_Description,
                        Icon = ImageResources.New_32
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            return true;
        }

        private void New_Executed()
        {
            base.ShowWaitCursor(true);

            try
            {
                // create and show the main window
                ReportSetupViewModel viewModel = new ReportSetupViewModel(false);
                ReportSetupWindow win = new ReportSetupWindow(viewModel);
                App.ShowWindow(win, true);
            }
            catch (Csla.DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex);
                Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                    String.Empty, OperationType.Open);

                base.ShowWaitCursor(true);
            }

            OnSearchFilterChanged(_searchFilter);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region OpenCommand

        private RelayCommand _openCommand;

        /// <summary>
        /// Opens a report in the the report builder window
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(
                        p => Open_Executed(),
                        p => Open_CanExecute(),
                        attachToCommandManager: true)
                    {
                        FriendlyName = Message.Reports_OpenReport_Name,
                        FriendlyDescription = Message.Reports_OpenReport_Description,
                        Icon = ImageResources.Report_Open_32
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute()
        {
            _openCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _openCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void Open_Executed()
        {
            if (this.SelectedReport != null)
            {
                base.ShowWaitCursor(true);

                try
                {
                    // create and show the main window
                    Object selectedReportId = this.SelectedReport.Id;

                    Report report = Report.FetchById(selectedReportId);

                    base.ShowWaitCursor(false); 
                    Boolean fileIsOkToOpen = Galleria.Reporting.Controls.Wpf.Common.Helpers.ValidateReport(App.Current.MainWindow, report);

                    if (fileIsOkToOpen)
                    {
                        base.ShowWaitCursor(true);
                        
                        //Reset this viewmodel's selected reportslist and current report
                        OnSearchFilterChanged(this.SearchFilter);
                        this.SelectedReport = this.AvailableReports.FirstOrDefault(r => r.Id.Equals(selectedReportId));

                        //Open the converted report
                        using (ReportSetupViewModel viewModel = new ReportSetupViewModel(false))
                        {
                            ReportSetupWindow win = new ReportSetupWindow(viewModel);

                            viewModel.SelectedReport = report;
                            App.ShowWindow(win, true);

                            //Update the filter results in case a new report was created
                            //or a report as deleted
                            OnSearchFilterChanged(this.SearchFilter);

                        }
                    }
                }
                catch (Csla.DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }


                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Opens the report builder window
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(),
                        p => Delete_CanExecute(),
                        attachToCommandManager: true)
                    {
                        FriendlyName = Message.Reports_DeleteReport_Name,
                        FriendlyDescription = Message.Reports_DeleteReport_Description,
                        Icon = ImageResources.Delete_32
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            _deleteCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _deleteCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                //confirm the delete with the user
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedReport.Name);
            }

            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                try
                {
                    Report reportToDelete = Report.FetchById(this.SelectedReport.Id);

                    reportToDelete.Delete();
                    reportToDelete.Save();
                    this.SelectedReport = null;
                }
                catch (Csla.DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                        String.Empty, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                OnSearchFilterChanged(this.SearchFilter);
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ShowPdfViewerCommand

        private RelayCommand<Int32?> _showPdfViewerCommand;

        /// <summary>
        /// Opens the report viewer window
        /// </summary>
        public RelayCommand<Int32?> ShowPdfViewerCommand
        {
            get
            {
                if (_showPdfViewerCommand == null)
                {
                    _showPdfViewerCommand = new RelayCommand<Int32?>(
                        p => ShowPdfViewer_Executed(p),
                        p => ShowPdfViewer_CanExecute(p),
                        attachToCommandManager: false)
                    {
                        FriendlyName = Message.Reports_ReportViewer_Title,
                        FriendlyDescription = Message.Reports_ReportViewer_Description,
                        Icon = ImageResources.PrintPreviewPdf_32
                    };
                    base.ViewModelCommands.Add(_showPdfViewerCommand);
                }
                return _showPdfViewerCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ShowPdfViewer_CanExecute(Int32? reportId)
        {
            return true;
        }

        private void ShowPdfViewer_Executed(Int32? reportId)
        {
            if (reportId != null)
            {
				base.ShowWaitCursor(true);
				
				//Select Report
                Report currentReport = Report.FetchById((Int32)reportId);

				base.ShowWaitCursor(false);

                //Build the report
                if (CreatePdf(currentReport))
                {
					base.ShowWaitCursor(true); 
					
					currentReport.DateLastExecuted = DateTime.UtcNow;
                    currentReport.Save();

                    OnSearchFilterChanged(this.SearchFilter);

					base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #region ShowDataViewer

        private RelayCommand<Int32?> _showDataViewerCommand;

        /// <summary>
        /// Opens the data viewer window
        /// </summary>
        public RelayCommand<Int32?> ShowDataViewerCommand
        {
            get
            {
                if (_showDataViewerCommand == null)
                {
                    _showDataViewerCommand = new RelayCommand<Int32?>(
                        p => ShowDataViewer_Executed(p),
                        p => ShowDataViewer_CanExecute(p),
                        attachToCommandManager: false)
                    {
                        FriendlyName = Message.Reports_DataViewer_Title,
                        FriendlyDescription = Message.Reports_DataViewer_Description,
                        Icon = ImageResources.PrintPreviewGrid_32
                    };
                    base.ViewModelCommands.Add(_showDataViewerCommand);
                }
                return _showDataViewerCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ShowDataViewer_CanExecute(Int32? reportId)
        {
            return true;
        }

        private void ShowDataViewer_Executed(Int32? reportId)
        {
            if (reportId != null)
            {
				base.ShowWaitCursor(true);
				
				//Select Report
                Report currentReport = Report.FetchById((Int32)reportId);

				base.ShowWaitCursor(false);

                #region Parameters
                Boolean parametersSelected =
                    Galleria.Reporting.Controls.Wpf.Common.Helpers.PromptParameterValues(currentReport);

                if (!parametersSelected)
                {
                    // Show a cancel message to the user
                    ShowCancelWindow();

                    return;
                }
                #endregion

				base.ShowWaitCursor(true);
                
                //Create the viewer view model
                ReportGridPreviewViewModel previewVM = new ReportGridPreviewViewModel(currentReport);
            
                //create the pdf and display it
                previewVM.BuildDataSet();

                if (previewVM.Results != null)
                {
                    Boolean canContinue = true;

                    //check if results row count is large
                    if (previewVM.Results.Rows.Count > ReportHelper.MaxRowCountWithoutWarningUser)
                    {
						base.ShowWaitCursor(false);
						
						canContinue = ShowLargeDataRowWarningDialog(previewVM.Results.Rows.Count);
                    }

                    if (canContinue)
                    {
						base.ShowWaitCursor(false); 
						
						ReportGridPreviewWindow win = new ReportGridPreviewWindow(previewVM);
                        if (this.AttachedControl != null)
                        {
							base.ShowWaitCursor(false); 
							
							App.ShowWindow(win, true);
                        }
                    }
                }

				base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region ExcelExportCommand

        private RelayCommand<Int32?> _excelExportCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand<Int32?> ExcelExportCommand
        {
            get
            {
                if (_excelExportCommand == null)
                {
                    _excelExportCommand = new RelayCommand<Int32?>(
                        p => ExcelExportCommand_Executed(p),
                        p => ExcelExportCommand_CanExecute(p))
                    {
                        FriendlyName = Message.ReportSetup_ExcelExport_Title,
                        FriendlyDescription = Message.ReportSetup_ExcelExport_Description,
                        Icon = ImageResources.ReportExcelExport_32,
                    };
                    base.ViewModelCommands.Add(_excelExportCommand);
                }
                return _excelExportCommand;
            }
        }

        private bool ExcelExportCommand_CanExecute(Int32? reportId)
        {
            Boolean enableCommand = true;
            String disabledReason = String.Empty;

            _excelExportCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void ExcelExportCommand_Executed(Int32? reportId)
        {
            if (reportId != null)
            {
				base.ShowWaitCursor(true);
				
				//Select Report
                Report currentReport = Report.FetchById((Int32)reportId);

				base.ShowWaitCursor(false);

                #region Configure Parameters

                Boolean parametersSelected =
                    Galleria.Reporting.Controls.Wpf.Common.Helpers.PromptParameterValues(currentReport);

                if (!parametersSelected)
                {
                    // Show a cancel message to the user
                    ShowCancelWindow();

                    return;
                }

                #endregion

				base.ShowWaitCursor(true);
                
                // Create the viewer view model
                ReportGridPreviewViewModel previewVM = new ReportGridPreviewViewModel(currentReport);
            
                // create the pdf and display it
                previewVM.BuildDataSet();

                if (previewVM.Results != null)
                {
                    Boolean canContinue = true;

                    // Check if there are too many result rows
                    if (previewVM.Results.Rows.Count > ReportHelper.MaxRowCountWithoutWarningUser)
                    {
                        base.ShowWaitCursor(false);

                        canContinue = ShowLargeDataRowWarningDialog(previewVM.Results.Rows.Count);
                    }

                    if (canContinue)
                    {
                        base.ShowWaitCursor(false);

                        // Set up the data table with the column header ordering
                        previewVM.CreateOrderedDataTable();

                        // Generate Excel
                        previewVM.ExportUsingTemplateCommand.Execute();

                        // Check if the file exists and if so, open it
                        var exportedFile = previewVM.ExcelExportFilename;

                        if (!String.IsNullOrEmpty(exportedFile) && File.Exists(exportedFile))
                        {
                            // ToDo: Check the file is actually an Excel file and the user has Excel installed
                            // rather than relying on file associations to open the exported Excel file.

                            System.Diagnostics.Process.Start(exportedFile);

                            base.ShowWaitCursor(false);
                        }
                    }
                }
            }
        }

        #endregion

        #endregion 

        #region Methods

        private void InitialiseFilterOptionsList()
        {
            List<ReportFilterOptionItem> filterOptions = new List<ReportFilterOptionItem>();

            filterOptions.Add(new ReportFilterOptionItem(ReportFilterOptionType.All));
            filterOptions.Add(new ReportFilterOptionItem(ReportFilterOptionType.Recent));
            //filterOptions.Add(new ReportFilterOptionItem(ReportFilterOptionType.Favourites));

            this.FilterOptions = new ReadOnlyCollection<ReportFilterOptionItem>(filterOptions);

            this.SelectedFilterOption = this.FilterOptions[0];
        }

        /// <summary>
        /// When the user hits the new tabular report button, get the list of report models and display 
        /// a popup window to allow the user to select a report data model.
        /// </summary>
        private void SelectNewReportType()
        {
            DataModelInfoList dataModelInfos = DataModelInfoList.FetchAll();

            // oh dear, there are no report data models defined / returned - let the user know
            if (dataModelInfos == null || dataModelInfos.Count == 0)
            {
                MessageBox.Show("No data models loaded");
                return;
            }

            //// create the viewmodel from the list of data models
            //var newTypesVm = new ReportSetupNewReportViewModel(dataModelInfos);
            //ReportSetupNewReportTypeWindow newReportTypeWin = new ReportSetupNewReportTypeWindow(newTypesVm);

            //// and show the window
            //if (this.AttachedControl != null)
            //{
            //    App.ShowWindow(newReportTypeWin, this.AttachedControl, true);
            //}

            //// get the model the user selected
            //var selectedModelType = newTypesVm.SelectedModel;

            //// and check if the user did not cancel
            //if (selectedModelType != null && !newTypesVm.UserCancelled)
            //{
            //    //Create the new report
            //    Report report = Report.NewReport(selectedModelType.Id);
            //    report.Description = "New Report Description";
            //    _reportCount++;
            //    report.Name = String.Format("{0} {1}", report.Name, _reportCount.ToString());

            //    //Update the report name textbox
            //    ReportSection rptHeader = report.Sections.First(s => s.SectionType == ReportSectionType.ReportHeader);
            //    ReportComponent rptHeaderTitleTextBox = rptHeader.Components.First(c => c.ComponentType == ReportComponentType.TextBox);
            //    rptHeaderTitleTextBox.ComponentDetails.TextBoxText = report.Name;
            //    rptHeaderTitleTextBox.Width = 350;
            //    rptHeaderTitleTextBox.Height = 25;

            //    //Load the report
            //    this.SelectedReport = report;
            //}
        }

        /// <summary>
        /// Called whenever the search filter property value changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSearchFilterChanged(String searchText)
        {
            this.SelectedReport = null;

            ReportInfoList allReportInfos = null;
            try
            {
                //Get the list of all reports
                allReportInfos = ReportInfoList.FetchAll();
            }
            catch (Csla.DataPortalException exc)
            {
                CommonHelper.GetWindowService().ShowErrorMessage(Message.Application_Database_ConnectionLostHeader, Message.Application_Database_ConnectionLostDescription);
                return;
            }

            //clear list of visible reports
            _reportInfos.Clear();
            
            Boolean isFiltering = !String.IsNullOrEmpty(searchText);

            //all reports
            if (isFiltering)
            {
                //filter is applied so search all reports for matches
                //show reports created in the last week
                CultureInfo culture = CultureInfo.CurrentCulture;
                foreach (ReportInfo reportInfo in allReportInfos)
                {
                    if (culture.CompareInfo.IndexOf(reportInfo.Name, searchText, CompareOptions.IgnoreCase) >= 0)
                    {
                        _reportInfos.Add(reportInfo);
                    }
                }
            }
            else
            {
                switch (_selectedFilterOption.Type)
                {
                    case ReportFilterOptionType.Recent:
                        //show reports created in the last week
                        foreach (ReportInfo reportInfo in allReportInfos)
                        {
                            if (reportInfo.DateLastExecuted != null)
                            {
                                if (((DateTime)reportInfo.DateLastExecuted).AddDays(7) > DateTime.UtcNow)
                                {
                                    _reportInfos.Add(reportInfo);
                                }
                            }
                        }
                        break;

                    case ReportFilterOptionType.Favourites:
                        //show favourite reports ?? TODO
                        break;

                    case ReportFilterOptionType.All:
                        //show all reports
                        _reportInfos.AddRange(allReportInfos);
                        break;
                }
            }

            this.AvailableReports = new ReadOnlyCollection<ReportInfo>(_reportInfos);
        }

        #endregion

        #region Print Preview Methods

        public Boolean CreatePdf(Report report)
        {
            //Store whether report execution was successful so that we know whether to
            //update the last executed datetime value for the report
            Boolean createSuccessful = false;

            #region Parameters
            Boolean parametersSelected =
                Galleria.Reporting.Controls.Wpf.Common.Helpers.PromptParameterValues(report);

            if (!parametersSelected)
            {
                // Show a cancel message to the user
                ShowCancelWindow();

                return false;
            }
            #endregion

            base.ShowWaitCursor(true);

            #region Get File Name
            String pdfFileName = CreateUniqueFileName(Path.GetTempPath(), report);
            //update the report object
            report.PreviewFilePath = Path.Combine(Path.GetTempPath(), pdfFileName);
            #endregion

            if (pdfFileName.Length > 0)
            {
                Boolean canContinue = true;
                ResultSetDto results = null;
                String query = String.Empty;

                #region Step 1: Fetch Data
                CreatePdfFileExecuteQuery(report, out query, out results);

                if (results == null)
                {
                    //the query was not completed and no data was returned possibly due to an error
                    canContinue = false;          
                }
                else
                {
                    if (results.Rows.Count > ReportHelper.MaxRowCountWithoutWarningUser)
                    {
                        base.ShowWaitCursor(false);

                        canContinue = ShowLargeDataRowWarningDialog(results.Rows.Count);

                        base.ShowWaitCursor(true);
                    }
                }
                #endregion

                #region Step2 : Create output PDF File
                if (canContinue)
                {
                    createSuccessful = CreatePdfFile(report, query, results);
                }
                #endregion
            }

            base.ShowWaitCursor(false);

            return createSuccessful;
        }

        public Boolean CreatePdfFile(Report report, String query, ResultSetDto queryResults)
        {
            Boolean createSuccessful = false; 
            
            _previewBusy = new ModalBusy();
            _previewBusy.IsDeterminate = true;
            _previewBusy.Header = Message.ReportViewer_BusyHeader;
            _previewBusy.Description = Message.ReportViewer_BusyDescription;

            Csla.Threading.BackgroundWorker createPdfFileWorker = new Csla.Threading.BackgroundWorker();
            createPdfFileWorker.WorkerReportsProgress = true;
            createPdfFileWorker.ProgressChanged += CreatePdfFileWorker_ProgressChanged;

            _processException = null;
            RenderResult renderResult = RenderResult.None;

            createPdfFileWorker.DoWork +=
                (s, e) =>
                {
                    //start new process to create the pdf
                    createPdfFileWorker.ReportProgress(ReportProgress_BuildingReport);

                    Galleria.Reporting.Processes.RenderPdfReport.Process process =
                        ProcessFactory.Execute<Galleria.Reporting.Processes.RenderPdfReport.Process>(
                        new Galleria.Reporting.Processes.RenderPdfReport.Process(report, query, queryResults));

                    renderResult = process.RenderResult;

                    //check for error during process
                    if (process.Exception == null && renderResult == Galleria.Reporting.Processes.RenderPdfReport.RenderResult.Success)
                    {
                        try
                        {
                            createPdfFileWorker.ReportProgress(ReportProgress_SavingReport);
                            //Save the report file
                            process.Result.Save(report.PreviewFilePath);

                            createPdfFileWorker.ReportProgress(ReportProgress_OpeningReport);
                        }
                        catch (Exception ex)
                        {
                            _processException = ex;
                        }
                    }
                    else
                    {
                        _processException = process.Exception;
                    }
                };

            createPdfFileWorker.RunWorkerCompleted +=
                (s, e) =>
                {
                    //cancel the busy window
                    if (_previewBusy != null)
                    {
                        _previewBusy.Close();
                        _previewBusy = null;
                    }

                    createPdfFileWorker.ProgressChanged -= CreatePdfFileWorker_ProgressChanged;

                    if (renderResult == Galleria.Reporting.Processes.RenderPdfReport.RenderResult.Success)
                    {
                        createSuccessful = true;
                        #region Open Report
                        base.ShowWaitCursor(false);
                        //open the pdf
                        OpenFile(report.PreviewFilePath);
                        #endregion
                    }
                    else if (renderResult == Galleria.Reporting.Processes.RenderPdfReport.RenderResult.CancelledByUser)
                    {
                        #region Report Cancelled

                        ModalMessage dialog =
                            new ModalMessage()
                            {
                                Title = Message.ReportViewer_MessageTitle,
                                Header = Message.ReportExecute_ReportCancelled_Header,
                                Description = Message.ReportExecute_ReportCancelled_Description,
                                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                                ButtonCount = 1,
                                Button1Content = Message.Generic_Ok,
                                DefaultButton = ModalMessageButton.Button1,
                                CancelButton = ModalMessageButton.Button1
                            };

                        base.ShowWaitCursor(false);

                        App.ShowWindow(dialog, true);

                        base.ShowWaitCursor(true);

                        #endregion
                    }
                    else
                    {
                        #region Report Error
                        //Set the message
                        String description =
                            String.Format(
                            "{0}\r\n\r\n{1}: {2}",
                            Message.ReportExecute_ReportFailed_FailedToBuild,
                            Message.ReportExecute_ReportFailed_StepThatFailed,
                        renderResult.ToString());
                        if (_processException != null)
                        {
                            description =
                                String.Format(
                                "{0}\r\n\r\n{1}: {2}",
                                Message.ReportExecute_ReportFailed_ErrorOccurred,
                                Message.ReportExecute_ReportFailed_ErrorMessage,
                                _processException.Message);
                        }
                        //inform user that error has occurred
                        ModalMessage dialog =
                            new ModalMessage()
                            {
                                Title = Message.ReportViewer_MessageTitle,
                                Header = Message.ReportExecute_ReportFailed_Header,
                                Description = description,
                                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                                ButtonCount = 1,
                                Button1Content = Message.Generic_Ok,
                                DefaultButton = ModalMessageButton.Button1,
                                CancelButton = ModalMessageButton.Button1
                            };

                        base.ShowWaitCursor(false);

                        App.ShowWindow(dialog, true);

                        base.ShowWaitCursor(true);

                        #endregion
                    }

                    createPdfFileWorker = null;
                };

            //run the worker and show the busy window
            createPdfFileWorker.RunWorkerAsync();
            App.ShowWindow(_previewBusy, true);

            return createSuccessful;
        }

        private void CreatePdfFileWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            Int32 currentProgress = e.ProgressPercentage;

            _previewBusy.SetValue(ModalBusy.ProgressPercentageProperty, currentProgress);

            if (currentProgress == ReportProgress_BuildingReport)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_BuildingReport);
            }

            if (currentProgress == ReportProgress_SavingReport)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_SavingReport);
            }

            if (currentProgress == ReportProgress_OpeningReport)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_OpeningReport);
            }
        }

        private void CreatePdfFileExecuteQuery(Report report, out String query, out ResultSetDto results)
        {
            //Int32 queryRowCount = -1;
            ResultSetDto queryResults = null;
            String sqlQuery = String.Empty;

            _previewBusy = new ModalBusy();
            _previewBusy.IsDeterminate = true;
            _previewBusy.Header = Message.ReportViewer_BusyHeader;
            _previewBusy.Description = Message.ReportViewer_BusyDescription;

            Csla.Threading.BackgroundWorker fetchPdfDataCountWorker = new Csla.Threading.BackgroundWorker();
            fetchPdfDataCountWorker.WorkerReportsProgress = true;
            fetchPdfDataCountWorker.ProgressChanged += CreatePdfFileExecuteQueryWorker_ProgressChanged;

            _processException = null;
            FetchDataResult fetchDataResult = FetchDataResult.None;

            fetchPdfDataCountWorker.DoWork +=
                (s, e) =>
                {
                    //start new process to create the pdf
                    fetchPdfDataCountWorker.ReportProgress(FetchDataProgress_StartFetch);

                    Galleria.Reporting.Processes.FetchReportData.Process process =
                        ProcessFactory.Execute<Galleria.Reporting.Processes.FetchReportData.Process>(
                        new Galleria.Reporting.Processes.FetchReportData.Process(report));

                    fetchDataResult = process.FetchDataResult;

                    //check for error during process
                    if (process.Exception == null
                        && fetchDataResult == Galleria.Reporting.Processes.FetchReportData.FetchDataResult.Success)
                    {
                        try
                        {
                            //Save the report file
                            //queryRowCount = process.ReportRowCount;
                            sqlQuery = process.Query;
                            queryResults = process.Results;
                            fetchPdfDataCountWorker.ReportProgress(FetchDataProgress_Complete);
                        }
                        catch (Exception ex)
                        {
                            _processException = ex;
                        }
                    }
                    else
                    {
                        _processException = process.Exception;
                    }
                };

            fetchPdfDataCountWorker.RunWorkerCompleted +=
                (s, e) =>
                {
                    //cancel the busy window
                    if (_previewBusy != null)
                    {
                        _previewBusy.Close();
                        _previewBusy = null;
                    }

                    fetchPdfDataCountWorker.ProgressChanged -= CreatePdfFileExecuteQueryWorker_ProgressChanged;

                    if (fetchDataResult != Galleria.Reporting.Processes.FetchReportData.FetchDataResult.Success)
                    {
                        #region Report Error
                        //Set the message
                        String description =
                            String.Format(
                            "{0}\r\n\r\n{1}: {2}",
                            Message.ReportExecute_ReportFailed_FailedToBuild,
                            Message.ReportExecute_ReportFailed_StepThatFailed,
                        fetchDataResult.ToString());
                        if (_processException != null)
                        {
                            description =
                                String.Format(
                                "{0}\r\n\r\n{1}: {2}",
                                Message.ReportExecute_ReportFailed_ErrorOccurred,
                                Message.ReportExecute_ReportFailed_ErrorMessage,
                                _processException.Message);
                        }
                        //inform user that error has occurred
                        ModalMessage dialog =
                            new ModalMessage()
                            {
                                Title = Message.ReportViewer_MessageTitle,
                                Header = Message.ReportExecute_ReportFailed_Header,
                                Description = description,
                                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                                ButtonCount = 1,
                                Button1Content = Message.Generic_Ok,
                                DefaultButton = ModalMessageButton.Button1,
                                CancelButton = ModalMessageButton.Button1
                            };

                        base.ShowWaitCursor(false);

                        App.ShowWindow(dialog, true);

                        base.ShowWaitCursor(true);

                        #endregion
                    }

                    fetchPdfDataCountWorker = null;
                };

            //run the worker and show the busy window
            fetchPdfDataCountWorker.RunWorkerAsync();
            App.ShowWindow(_previewBusy, true);

            if (sqlQuery.Length > 0)
            {
                query = sqlQuery;
            }
            else
            {
                query = String.Empty;
            }
            if (queryResults != null)
            {
                results = queryResults;
            }
            else
            {
                results = null;
            }
        }

        private void CreatePdfFileExecuteQueryWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            Int32 currentProgress = e.ProgressPercentage;

            _previewBusy.SetValue(ModalBusy.ProgressPercentageProperty, currentProgress);

            if (currentProgress == FetchDataProgress_StartFetch)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_FetchingData);
            }

            if (currentProgress == FetchDataProgress_Complete)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_CheckingData);
            }
        }

        /// <summary>
        /// Delete the existing file if it exists, or inform user if the file cannot be deleted
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private Boolean DeleteExistingFile(String filePath)
        {
            Boolean isSuccessful = true;

            if (System.IO.File.Exists(filePath))
            {
                //check if file is already open
                if (!FileHelper.IsFileLocked(new System.IO.FileInfo(filePath)))
                {
                    //delete the file
                    System.IO.File.Delete(filePath);
                }
                else
                {
                    isSuccessful = false;

                    #region Display message
                    //tell the user to close the pdf
                    ModalMessage dialog =
                        new ModalMessage()
                        {
                            Title = Message.ReportViewer_MessageTitle,
                            Header = Message.ReportViewer_FileInUse_Title,
                            Description = String.Format(Message.ReportViewer_FileInUse_Message, filePath),
                            WindowStartupLocation = WindowStartupLocation.CenterOwner,
                            ButtonCount = 1,
                            Button1Content = Message.Generic_Ok,
                            DefaultButton = ModalMessageButton.Button1,
                            CancelButton = ModalMessageButton.Button1
                        };

                    App.ShowWindow(dialog, true);
                    #endregion
                }
            }
            return isSuccessful;
        }

        /// <summary>
        /// Creates a unique filename based on the report Name
        /// </summary>
        /// <param name="proposedFileName"></param>
        /// <returns></returns>
        private String CreateUniqueFileName(String filepath, Report report)
        {
            Int32 fileNumber = 1;

            String reportName = report.Name;
            foreach (Char c in Path.GetInvalidFileNameChars())
            {
                reportName = reportName.Replace(c, '_');
            }
            String newFileName = String.Format("{0}.pdf", reportName);

            Boolean isSuccessful = false;

            while (!isSuccessful)
            {
                String fileFullPathAndName = Path.Combine(filepath, newFileName);

                if (System.IO.File.Exists(fileFullPathAndName))
                {
                    //check if file is already open
                    if (!FileHelper.IsFileLocked(new System.IO.FileInfo(fileFullPathAndName)))
                    {
                        //delete the file
                        System.IO.File.Delete(fileFullPathAndName);
                        isSuccessful = true;
                    }
                    else
                    {
                        isSuccessful = false;
                        //file exists and is in use so increment number
                        newFileName = String.Format("{0} ({1}).pdf", reportName, fileNumber.ToString());
                        fileNumber++;
                    }
                }
                else
                {
                    isSuccessful = true;
                }
            }

            return newFileName;
        }

        /// <summary>
        /// Open the existing file
        /// </summary>
        private void OpenFile(String filePath)
        {
            if (!String.IsNullOrWhiteSpace(filePath))
            {
                //check it was saved
                if (System.IO.File.Exists(filePath))
                {
                    System.Diagnostics.Process.Start(filePath);
                }
            }
        }

        /// <summary>
        /// The warning method called by the background process when the 
        /// report will contain more than the limit number of rows
        /// </summary>
        /// <returns></returns>
        public Boolean ShowLargeDataRowWarningDialog(Int32 reportRowCount)
        {
            if (this.AttachedControl != null)
            {
                ModalMessage msg = new ModalMessage()
                {
                    Title = Reporting.Resources.Language.Message.ReportExecute_RowCountDialog_Title,
                    Header = Reporting.Resources.Language.Message.ReportExecute_RowCountDialog_Header,
                    Description = String.Format(Reporting.Resources.Language.Message.ReportExecute_RowCountDialog_Text, reportRowCount.ToString()),
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ButtonCount = 2,
                    Button1Content = Message.Generic_Ok,
                    Button2Content = Message.Generic_Cancel,
                    DefaultButton = ModalMessageButton.Button1,
                    CancelButton = ModalMessageButton.Button2
                };

                App.ShowWindow(msg, true);

                return (msg.Result == ModalMessageResult.Button1);
            }
            return true;
        }

        private void ShowCancelWindow()
        {
            ModalMessage dialog =
                new ModalMessage()
                {
                    Title = Message.ReportViewer_MessageTitle,
                    Header = Message.ReportExecute_ReportCancelled_Header,
                    Description = Message.ReportExecute_ReportCancelled_Description,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ButtonCount = 1,
                    Button1Content = Message.Generic_Ok,
                    DefaultButton = ModalMessageButton.Button1,
                    CancelButton = ModalMessageButton.Button1
                };

            App.ShowWindow(dialog, true);
        }

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Helper method to get the property path for the <see cref="PlanogramInfoSelection" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(Expression<Func<ReportsViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion

        #region IDisposable


        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                    this.SelectedReport = null;
                    this.AvailableReports = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Class describing the items in the report filters treeview
    /// </summary>
    public class ReportFilterOptionItem: INotifyPropertyChanged
    {
        #region Fields
        private String _name;
        private ReportFilterOptionType _type = ReportFilterOptionType.All;
        #endregion

        #region Binding Properties
        public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<ReportFilterOptionItem>(c => c.Name);
        public static readonly PropertyPath TypeProperty = WpfHelper.GetPropertyPath<ReportFilterOptionItem>(c => c.Type);
        #endregion

        #region Properties
        public String Name
        {
            get { return _name; }
            set
            {
                _name = value;
                OnPropertyChanged(NameProperty);
            }
        }

        public ReportFilterOptionType Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged(TypeProperty);
            }
        }
        #endregion

        #region Constructors

        public ReportFilterOptionItem(ReportFilterOptionType type)
        {
            _type = type;
            _name = ReportFilterOptionTypeHelper.FriendlyNames[type];
        }
        #endregion


        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}
