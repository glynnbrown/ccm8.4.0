﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25788 : M.Pettit ~ Created.
#endregion

#endregion

using System;
using System.Windows;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.Reports
{
    /// <summary>
    /// Interaction logic for ReportsHomeTab.xaml
    /// </summary>
    public partial class ReportsHomeTab : RibbonTabItem
    {
        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(ReportsViewModel), typeof(ReportsHomeTab),
            new PropertyMetadata(default(ReportsViewModel)));

        public ReportsViewModel ViewModel
        {
            get { return (ReportsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        public ReportsHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
