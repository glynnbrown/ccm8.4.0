﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25521 : M.Pettit
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;


namespace Galleria.Ccm.Workflow.Client.Wpf.ReportViewer
{
    /// <summary>
    /// Interaction logic for ReportGridPreviewWindow.xaml
    /// </summary>
    public partial class ReportGridPreviewWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportGridPreviewViewModel), typeof(ReportGridPreviewWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportGridPreviewViewModel ViewModel
        {
            get { return (ReportGridPreviewViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportGridPreviewWindow senderControl = (ReportGridPreviewWindow)obj;

            if (e.OldValue != null)
            {
                ReportGridPreviewViewModel oldModel = (ReportGridPreviewViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ReportGridPreviewViewModel newModel = (ReportGridPreviewViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor
        public ReportGridPreviewWindow(ReportGridPreviewViewModel viewModel)
        {
            InitializeComponent();

            //Set the viewmodel for the window
            this.ViewModel = viewModel;

            this.Loaded += ReportGridViewerWindow_Loaded;
        }

        void ReportGridViewerWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ReportGridViewerWindow_Loaded;
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #region Window Close

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);
            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                IDisposable disposableViewModel = this.ViewModel;
                this.ViewModel = null;

                if (disposableViewModel != null)
                {
                    disposableViewModel.Dispose();
                }
            }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #endregion
    }
}
