﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25521 : M.Pettit
//  Created
#endregion

#region Version History: (CCM 8.0.1)
// V8-28943 : M.Shelley
//  Extended the exception handling around the creation of the Excel spreadsheet export.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Model;
using Galleria.Reporting.Processes.RenderGridReport;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportViewer
{
    /// <summary>
    /// ViewModel for the Report Data Viewer screen
    /// </summary>
    public class ReportGridPreviewViewModel : ViewModelAttachedControlObject<ReportGridPreviewWindow>
    {
        #region Constants
        Int32 ReportProgress_BuildingReport = 20;
        Int32 ReportProgress_ExecutingQuery = 50;
        Int32 ReportProgress_OpeningReport = 90;
        #endregion

        #region Fields
        private Report _selectedReport;
        private Window _parentWindow;
        private ModalBusy _previewBusy;
        private Exception _processException = null;
        private ResultSetDto _results;
        private Boolean _showTotals = false;
        
        private ModalBusy _exportBusy;
        private Boolean _exporting = false;

        private Type[] _exceptionTypes = new Type[] 
        {
            typeof(System.IO.IOException),
            typeof(System.ArgumentException),
            // ... add other exception types here - although there is no current code to generate 
            // a specific localised error message for other exceptions
        };

        #endregion

        #region Events
        public event EventHandler ExportComplete;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath WindowTitleProperty = WpfHelper.GetPropertyPath<ReportGridPreviewViewModel>(p => p.WindowTitle);
        public static readonly PropertyPath SelectedReportProperty = WpfHelper.GetPropertyPath<ReportGridPreviewViewModel>(c => c.SelectedReport);
        public static readonly PropertyPath ResultsProperty = WpfHelper.GetPropertyPath<ReportGridPreviewViewModel>(c => c.Results);
        public static readonly PropertyPath ShowTotalsProperty = WpfHelper.GetPropertyPath<ReportGridPreviewViewModel>(c => c.ShowTotals);
        
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<ReportGridPreviewViewModel>(c => c.CloseCommand);
        public static readonly PropertyPath ExportCommandProperty = WpfHelper.GetPropertyPath<ReportGridPreviewViewModel>(c => c.ExportCommand);
        public static readonly PropertyPath ShowTotalsCommandProperty = WpfHelper.GetPropertyPath<ReportGridPreviewViewModel>(c => c.ShowTotalsCommand);

        public static PropertyPath ExportingProperty = WpfHelper.GetPropertyPath<ReportGridPreviewViewModel>(p => p.Exporting);

        #endregion

        #region Properties

        /// <summary>
        /// Sets the title of the grid viewer window
        /// </summary>
        public String WindowTitle
        {
            get
            {
                if (_selectedReport != null)
                {
                    return String.Format("{0} - {1}", Message.ReportGridViewer_Title, _selectedReport.Name);
                }
                return Message.ReportGridViewer_Title;
            }
        }

        /// <summary>
        /// Returns the selected report
        /// </summary>
        public Report SelectedReport
        {
            get { return _selectedReport; }
            set
            {
                _selectedReport = value;
            }
        }

        /// <summary>
        /// Returns the results from the report
        /// </summary>
        public ResultSetDto Results
        {
            get { return _results; }
            set { _results = value; }
        }

        /// <summary>
        /// Gets/Sets whether the grid shows totals for each column
        /// </summary>
        public Boolean ShowTotals
        {
            get { return _showTotals; }
            set
            {
                _showTotals = value;
                OnPropertyChanged(ShowTotalsProperty);
            }
        }

        /// <summary>
        /// Returns the parent window that created the viewmodel
        /// </summary>
        public Window ParentWindow
        {
            get { return _parentWindow; }
            set { _parentWindow = value; }
        }

        public Boolean Exporting
        {
            get
            {
                return _exporting;
            }
            set
            {
                _exporting = value;
                OnPropertyChanged(ExportingProperty);
            }
        }

        #endregion

        #region Constructor
        public ReportGridPreviewViewModel(Report report)
        {
            this.SelectedReport = report;
            this.ShowTotals = false;
        }

        public ReportGridPreviewViewModel(Report report, Window parentWindow)
        {
            this.SelectedReport = report;
            this.ShowTotals = false;
            this.ParentWindow = parentWindow;
        }
        #endregion

        #region Commands

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// closes the window
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed(),
                        p => Close_CanExecute())
                    {
                        FriendlyName = Message.ReportViewer_CloseCommand_Name,
                        FriendlyDescription = Message.ReportViewer_CloseCommand_Description,
                        Icon = ImageResources.PrintPreviewClose_32
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region ExportCommand

        private RelayCommand _exportCommand;

        /// <summary>
        /// closes the window
        /// </summary>
        public RelayCommand ExportCommand
        {
            get
            {
                if (_exportCommand == null)
                {
                    _exportCommand = new RelayCommand(
                        p => Export_Executed(),
                        p => Export_CanExecute())
                    {
                        FriendlyName = Message.ReportViewer_ExportCommand_Name,
                        FriendlyDescription = Message.ReportViewer_ExportCommand_Description,
                        Icon = ImageResources.ExportToExcel_32
                    };
                    base.ViewModelCommands.Add(_exportCommand);
                }
                return _exportCommand;
            }
        }

        private Boolean Export_CanExecute()
        {
            _exportCommand.DisabledReason = String.Empty;
            if (this.Results == null)
            {
                return false;
            }
            if (this.Results.Rows.Count == 0)
            {
                return false;
            }
            return true;
        }

        private void Export_Executed()
        {
            //get the filename to save as
            Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
            saveDialog.CreatePrompt = false;
            saveDialog.Filter = Message.ReportViewer_ExportFilter;
            saveDialog.FilterIndex = 3;
            saveDialog.OverwritePrompt = true;

            saveDialog.FileName = _selectedReport.Name;

            if (saveDialog.ShowDialog() == true)
            {
                this.ShowWaitCursor(true);
                this.ExportComplete += Report_ExportComplete;
                Aspose.Cells.SaveFormat saveFormat;
                switch (saveDialog.FilterIndex)
                {
                    case 1:
                        saveFormat = Aspose.Cells.SaveFormat.CSV;
                        break;
                    case 2:
                        saveFormat = Aspose.Cells.SaveFormat.Excel97To2003;
                        break;
                    case 3:
                        saveFormat = Aspose.Cells.SaveFormat.Xlsx;
                        break;
                    default:
                        saveFormat = Aspose.Cells.SaveFormat.Xlsx;
                        break;
                }

                ExportReportToExcel(saveDialog.FileName, _selectedReport, saveFormat);
                _exportCommand.RaiseCanExecuteChanged();

                this.ShowWaitCursor(false);

            }
        }

        private void Report_ExportComplete(object sender, EventArgs e)
        {
            this.ExportComplete -= Report_ExportComplete;
            _exportCommand.RaiseCanExecuteChanged();
        }

        #endregion

        #region ShowTotalsCommand

        private RelayCommand _showTotalsCommand;

        /// <summary>
        /// shows the totals in the grid
        /// </summary>
        public RelayCommand ShowTotalsCommand
        {
            get
            {
                if (_showTotalsCommand == null)
                {
                    _showTotalsCommand = new RelayCommand(
                        p => ShowTotals_Executed(),
                        p => ShowTotals_CanExecute())
                    {
                        FriendlyName = Message.ReportViewer_ShowTotalsCommand_Name,
                        FriendlyDescription = Message.ReportViewer_ShowTotalsCommand_Description,
                        Icon = ImageResources.ShowTotals_32
                    };
                    base.ViewModelCommands.Add(_showTotalsCommand);
                }
                return _showTotalsCommand;
            }
        }

        private Boolean ShowTotals_CanExecute()
        {
            _exportCommand.DisabledReason = String.Empty;
            if (this.Results == null)
            {
                return false;
            }
            if (this.Results.Rows.Count == 0)
            {
                return false;
            }
            return true;
        }

        private void ShowTotals_Executed()
        {
            if (this.Results != null)
            {
                if (this.Results.Rows.Count > 0)
                {
                    this.ShowTotals = !this.ShowTotals;
                } 
            }
        }

        #endregion

        #endregion

        #region Export Methods

        /// <summary>
        /// Export worker thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>requires non async GetChachedData to work better</remarks>
        private void Export_DoWork(object sender, DoWorkEventArgs e)
        {
            Exporting = true;
            Object[] args = (Object[])e.Argument;
            String fileName = (String)args[0];
            Report report = (Report)args[1];
            Aspose.Cells.SaveFormat saveFormat = (Aspose.Cells.SaveFormat)args[2];

            //wait for a short time to give the UI a chance to update.
            System.Threading.Thread.Sleep(100);

            ExportData(fileName, report, this.Results, saveFormat);

            e.Result = new Object[]
            {
                fileName, report , this.Results, saveFormat
            };

            //assume all rows have been retrived
            if (_exportBusy != null)
            {
                _exportBusy.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    (SendOrPostCallback)delegate
                    {
                        _exportBusy.SetValue(ModalBusy.ProgressPercentageProperty, 100);
                    },
                    DispatcherPriority.ApplicationIdle);
            }
        }

        /// <summary>
        /// Export view
        /// </summary>
        /// <param name="fileName">Save destination</param>
        /// <param name="view">iew to be exported</param>
        /// <param name="saveFormat">save file type</param>
        public void ExportReportToExcel(String fileName, Report report, Aspose.Cells.SaveFormat saveFormat)
        {
            //Create new Model busy window to show export progress
            _exportBusy = new ModalBusy();
            _exportBusy.IsDeterminate = true;
            _exportBusy.Header = Message.ReportViewerExportProgress_Busy;

            //create a new background worker to perform the export.
            BackgroundWorker exportExcel = new BackgroundWorker();
            exportExcel.DoWork += new DoWorkEventHandler(Export_DoWork);
            exportExcel.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Export_RunWorkerCompleted);
            exportExcel.WorkerReportsProgress = true;
            exportExcel.ProgressChanged += new ProgressChangedEventHandler(exportToExcel_ProgressChanged);
            exportExcel.RunWorkerAsync(new Object[] { fileName, report, saveFormat });

            //Show the busy modal window
            App.ShowWindow(_exportBusy, true);
        }

        void exportToExcel_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _exportBusy.SetValue(ModalBusy.ProgressPercentageProperty, e.ProgressPercentage);
        }

        /// <summary>
        /// Export complete actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Export_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Object[] args = (Object[])e.Result;
            String fileName = (String)args[0];
            Report report = (Report)args[1];
            ResultSetDto resultsData = (ResultSetDto)args[2];
            Aspose.Cells.SaveFormat saveFormat = (Aspose.Cells.SaveFormat)args[3];

            Exporting = false;
            _exportBusy.Close();
            if (ExportComplete != null) ExportComplete(resultsData, new EventArgs());
        }

        /// <summary>
        /// Export view
        /// </summary>
        /// <param name="fileName">Save destination</param>
        /// <param name="view">view to be exported</param>
        /// <param name="rows">row data for the view</param>
        /// <param name="saveFormat">save file type</param>
        public void ExportData(String fileName, Report report, ResultSetDto resultsData, Aspose.Cells.SaveFormat saveFormat)
        {
            const Int32 decimalPlaces = 2;
            const Int32 percentageDecimalPlaces = 4;

            // create a new workbook to hold the export
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
            workbook.Worksheets.Clear();

            //Create a new worksheet using the view's name.
            Aspose.Cells.Worksheet workSheet = workbook.Worksheets.Add(report.Name);

            Int32 rowIndex = 0;
            Int32 columnIndex = 0;

            if (_exportBusy != null)
            {
                //Use the dispatcher to update the progress percentage to get round cross threading issues.
                _exportBusy.Dispatcher.BeginInvoke(
                    DispatcherPriority.Background,
                    (SendOrPostCallback)delegate
                    {
                        _exportBusy.SetValue(ModalBusy.ProgressPercentageProperty, (Int32)((Single)resultsData.Rows.Count() / resultsData.Rows.Count() * 100));
                    },
                    DispatcherPriority.ApplicationIdle);
            }

            //Set headers
            foreach (ResultSetColumnDto column in resultsData.Columns)
            {
                workSheet.Cells[rowIndex, columnIndex].Value = column.Name;
                columnIndex++;
            }

            // Wrap the Aspose Excel creation in the exception handler to prevent
            // a system error if the Excel 2007 maximum row count of 1048576 
            // is exceeded.
            try
            {
                rowIndex++;
                //Set row data 
                foreach (ResultSetRowDto dataRow in resultsData.Rows)
                {
                    columnIndex = 0;

                    //Set row data for each column
                    foreach (ResultSetColumnDto column in resultsData.Columns)
                    {
                        //Retrive the row-column data from the source object.
                        Boolean isDataTypePercentage = false;
                        Object value = dataRow.Values[resultsData.Columns.IndexOf(column)];

                        if (value != null)
                        {
                            if (value is Double)
                            {
                                workSheet.Cells[rowIndex, columnIndex].Value = isDataTypePercentage ?
                                    Math.Round((Double)value, percentageDecimalPlaces) : Math.Round((Double)value, decimalPlaces);
                            }
                            else if (value is Single)
                            {
                                workSheet.Cells[rowIndex, columnIndex].Value = isDataTypePercentage ?
                                    Math.Round((Single)value, percentageDecimalPlaces) : Math.Round((Single)value, decimalPlaces);
                            }
                            else if (value is Decimal)
                            {
                                workSheet.Cells[rowIndex, columnIndex].Value = isDataTypePercentage ?
                                    Math.Round((Decimal)value, percentageDecimalPlaces) : Math.Round((Decimal)value, decimalPlaces);
                            }
                            else if (value is DateTime)
                            {
                                DateTime dateValue;
                                if (DateTime.TryParse(value.ToString(), out dateValue))
                                {
                                    workSheet.Cells[rowIndex, columnIndex].Value = dateValue.ToString();
                                }
                                else
                                {
                                    workSheet.Cells[rowIndex, columnIndex].Value = value;
                                }
                            }
                            else
                            {
                                workSheet.Cells[rowIndex, columnIndex].Value = value;
                            }
                        }
                        columnIndex++;


                    }
                    //Increment row index
                    rowIndex++;
                }

                workSheet.AutoFitColumns();
                ApplyStyle(workSheet, 1, rowIndex - 1, false, 0);

                workbook.Save(fileName, saveFormat);
            }
            catch (Exception exp)
            {
                if (_exceptionTypes.Contains(exp.GetType()))
                {
                    ExceptionHandler(fileName, exp);
                }
                else
                {
                    throw;
                }
            }
        }

        private void ExceptionHandler(String fileName, Exception exp)
        {
            String errMessage = String.Empty;

            // Get the mmessage to display depending on the exception type
            if (exp is System.IO.IOException)
            {
                errMessage = Message.ReportViewer_CannotExport_Description;
            }
            else if (exp is System.ArgumentException)
			{
				errMessage = Message.ReportViewer_TooLarge_Description;
			}
			else
			{
				errMessage = String.Format("{0} - {1}", 
                    Message.ReportViewer_GeneralError_Description, exp.Message);
			}

            // And display this is in a seperate Single-Threaded COM Apartment (STA) thread as we are 
            // using WPF components outside of a windowing environment. 
            var dialogLambda = new ThreadStart(
                () => { ShowDialog(fileName, errMessage);
                });
            Thread dialogThread = new Thread(dialogLambda);
            dialogThread.SetApartmentState(ApartmentState.STA);

            dialogThread.Start();
            dialogThread.Join();
        }

        [STAThread]
		private void ShowDialog(String fileName, String errMessage)
		{
            //cannot access file
            ModalMessage warning = new ModalMessage();
            warning.Header = Message.ReportViewer_CannotExport_Header;
            warning.Description = String.Format(CultureInfo.CurrentCulture, errMessage, fileName);
            warning.ButtonCount = 1;
            warning.Button1Content = Message.Generic_Ok;
            warning.MessageIcon = ImageResources.Dialog_Error;

            // We cannot use the generic CCM app window helper as it is in a different thread and causes
            // a System.InvalidOperationException : 
            //  "The calling thread cannot access this object because a different thread owns it."
            //App.ShowWindow(warning, /*isModal*/true);

            // So just use the standard windows .ShowDialog method
            warning.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            warning.ShowDialog();
		}

        private void UpdateExportProgress(Int32 progress)
        {
            if (_exportBusy != null)
            {
                //Use the dispatcher to update the progress percentage to get round cross threading issues.
                _exportBusy.Dispatcher.BeginInvoke(
                    DispatcherPriority.Send,
                    (SendOrPostCallback)delegate
                    {
                        _exportBusy.SetValue(ModalBusy.ProgressPercentageProperty, progress);
                    },
                    DispatcherPriority.Send);
            }
        }

        /// <summary>
        /// Apply a default style to the supplied worksheet.
        /// </summary>
        /// <param name="workSheet">Worksheet to add style to</param>
        /// <param name="headerRows">number of header rows</param>
        /// <param name="dataRows">number of data rows</param>
        /// <param name="formattedAsTable">if the worksheet has been formatted as a table</param>
        private void ApplyStyle(Aspose.Cells.Worksheet workSheet, Int32 headerRows, Int32 dataRows, Boolean formattedAsTable = true, Int32 startRow = 0)
        {
            Aspose.Cells.Workbook workbook = workSheet.Workbook;

            Color galleriaBlue = Color.FromArgb(40, 56, 124);
            Int32 groupStyleId = workbook.Styles.Add();
            Aspose.Cells.Style headerStyle = workbook.Styles[groupStyleId];

            headerStyle.HorizontalAlignment = Aspose.Cells.TextAlignmentType.Center;
            headerStyle.Borders[Aspose.Cells.BorderType.TopBorder].Color = Color.LightGray;
            headerStyle.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;

            headerStyle.Borders[Aspose.Cells.BorderType.BottomBorder].Color = Color.LightGray;
            headerStyle.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;

            headerStyle.Borders[Aspose.Cells.BorderType.LeftBorder].Color = Color.LightGray;
            headerStyle.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;

            headerStyle.Borders[Aspose.Cells.BorderType.RightBorder].Color = Color.LightGray;
            headerStyle.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            headerStyle.Pattern = Aspose.Cells.BackgroundType.Solid;
            headerStyle.ForegroundColor = galleriaBlue;
            headerStyle.Font.Color = Color.White;
            headerStyle.Font.IsBold = true;

            //Creating StyleFlag
            Aspose.Cells.StyleFlag styleFlag = new Aspose.Cells.StyleFlag();
            styleFlag.HorizontalAlignment = true;
            styleFlag.VerticalAlignment = true;
            styleFlag.ShrinkToFit = true;
            styleFlag.Borders = true;
            styleFlag.FontColor = true;
            styleFlag.CellShading = true;
            styleFlag.FontBold = true;

            if (formattedAsTable)
            {
                //Add the header style to all but the last header row
                for (Int32 x = 0; x < headerRows - 1; x++)
                {
                    workSheet.Cells.ApplyRowStyle(x + startRow, headerStyle, styleFlag);
                }

                Aspose.Cells.StyleFlag headerFlag = new Aspose.Cells.StyleFlag();
                headerFlag.HorizontalAlignment = false;
                headerFlag.VerticalAlignment = true;
                headerFlag.ShrinkToFit = true;
                headerFlag.Borders = true;
                headerFlag.FontColor = true;
                headerFlag.CellShading = true;
                headerFlag.FontBold = true;

                //Last header row style leaves out the horizontal alignement so that it is
                //left aligned. This is due to it looking nicer when there is the drop down
                //arrows visible added by 
                workSheet.Cells.ApplyRowStyle(headerRows - 1 + startRow, headerStyle, headerFlag);
            }
            else
            {
                //Add the header style to all but the header rows
                for (Int32 x = 0; x < headerRows; x++)
                {
                    workSheet.Cells.ApplyRowStyle(x + startRow, headerStyle, styleFlag);
                }
            }

            Aspose.Cells.StyleFlag styleFlagAll = new Aspose.Cells.StyleFlag();
            styleFlagAll.HorizontalAlignment = false;
            styleFlagAll.VerticalAlignment = false;
            styleFlagAll.ShrinkToFit = true;
            styleFlagAll.Borders = true;
            styleFlagAll.FontColor = false;
            styleFlagAll.CellShading = false;

            Int32 rowStyleId = workbook.Styles.Add();
            Aspose.Cells.Style rowStyle = workbook.Styles[groupStyleId];

            rowStyle.Borders[Aspose.Cells.BorderType.TopBorder].Color = Color.LightGray; //galleriaBlue;
            rowStyle.Borders[Aspose.Cells.BorderType.TopBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;

            rowStyle.Borders[Aspose.Cells.BorderType.BottomBorder].Color = Color.LightGray; //galleriaBlue;
            rowStyle.Borders[Aspose.Cells.BorderType.BottomBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;

            rowStyle.Borders[Aspose.Cells.BorderType.LeftBorder].Color = galleriaBlue; //Color.LightGray;
            rowStyle.Borders[Aspose.Cells.BorderType.LeftBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;

            rowStyle.Borders[Aspose.Cells.BorderType.RightBorder].Color = galleriaBlue; //Color.LightGray;
            rowStyle.Borders[Aspose.Cells.BorderType.RightBorder].LineStyle = Aspose.Cells.CellBorderType.Thin;
            rowStyle.Pattern = Aspose.Cells.BackgroundType.Solid;
            //rowStyle.ForegroundColor = galleriaBlue;
            //rowStyle.Font.Color = Color.White;

            for (Int32 x = headerRows; x < dataRows + headerRows; x++)
            {
                workSheet.Cells.ApplyRowStyle(x + startRow, headerStyle, styleFlagAll);
            }
        }


        #endregion

        #region Methods
        public void BuildDataSet()
        {
            base.ShowWaitCursor(true);

             _previewBusy = new ModalBusy();
            _previewBusy.IsDeterminate = true;
            _previewBusy.Header = Message.ReportViewer_BusyHeader;
            _previewBusy.Description = Message.ReportViewer_BusyDescription;

            Csla.Threading.BackgroundWorker printPreviewWorker = new Csla.Threading.BackgroundWorker();
            printPreviewWorker.WorkerReportsProgress = true;
            printPreviewWorker.ProgressChanged += PrintPreviewWorker_ProgressChanged;

            _processException = null;
            RenderResult renderResult = RenderResult.None;

            printPreviewWorker.DoWork +=
                (s, e) =>
                {
                    //start new process to create the dataset
                    printPreviewWorker.ReportProgress(ReportProgress_BuildingReport);

                    Galleria.Reporting.Processes.RenderGridReport.Process process =
                        ProcessFactory.Execute<Galleria.Reporting.Processes.RenderGridReport.Process>(
                        new Galleria.Reporting.Processes.RenderGridReport.Process(this.SelectedReport));

                    printPreviewWorker.ReportProgress(ReportProgress_ExecutingQuery);

                    renderResult = process.RenderResult;

                    //check for error during process
                    if (process.Exception == null && renderResult == Galleria.Reporting.Processes.RenderGridReport.RenderResult.Success)
                    {
                        try
                        {
                            //Save the report file
                            this.Results = process.Results;

                            printPreviewWorker.ReportProgress(ReportProgress_OpeningReport);
                        }
                        catch (Exception ex)
                        {
                            _processException = ex;
                        }
                    }
                    else
                    {
                        _processException = process.Exception;
                    }
                };

            printPreviewWorker.RunWorkerCompleted +=
                (s, e) =>
                {
                    //cancel the busy window
                    if (_previewBusy != null)
                    {
                        _previewBusy.Close();
                        _previewBusy = null;
                    }

                    printPreviewWorker.ProgressChanged -= PrintPreviewWorker_ProgressChanged;

                    if (renderResult == Galleria.Reporting.Processes.RenderGridReport.RenderResult.CancelledByUser)
                    {
                        #region Report Cancelled

                        ModalMessage dialog =
                            new ModalMessage()
                            {
                                Title = Message.ReportViewer_MessageTitle,
                                Header = Message.ReportExecute_ReportCancelled_Header,
                                Description = Message.ReportExecute_ReportCancelled_Description,
                                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                                ButtonCount = 1,
                                Button1Content = Message.Generic_Ok,
                                DefaultButton = ModalMessageButton.Button1,
                                CancelButton = ModalMessageButton.Button1
                            };

                        base.ShowWaitCursor(false);

                        App.ShowWindow(dialog, true);

                        base.ShowWaitCursor(true);

                        #endregion
                    }
                    else if (renderResult != Galleria.Reporting.Processes.RenderGridReport.RenderResult.Success)
                    {
                        #region Report Error

                        //Set the message
                        String description = String.Format("{0}\r\n\r\n{1}: {2}",
                               Message.ReportExecute_ReportFailed_FailedToBuild,
                               Message.ReportExecute_ReportFailed_StepThatFailed,
                               renderResult.ToString());
                        if (_processException != null)
                        {
                            description = String.Format("{0}\r\n\r\n{1}: {2}",
                                    Message.ReportExecute_ReportFailed_ErrorOccurred,
                                    Message.ReportExecute_ReportFailed_ErrorMessage,
                                    _processException.Message);
                        }
                        //inform user that error has occurred
                        ModalMessage dialog = new ModalMessage()
                        {
                            Title = Message.ReportViewer_MessageTitle,
                            Header = Message.ReportExecute_ReportFailed_Header,
                            Description = description,
                            WindowStartupLocation = WindowStartupLocation.CenterOwner,
                            ButtonCount = 1,
                            Button1Content = Message.Generic_Ok,
                            DefaultButton = ModalMessageButton.Button1,
                            CancelButton = ModalMessageButton.Button1
                        };

                        base.ShowWaitCursor(false);

                        App.ShowWindow(dialog, true);

                        base.ShowWaitCursor(true);

                        #endregion
                    }

                    printPreviewWorker = null;

                    printPreviewWorker = null;
                };

            //run the worker and show the busy window
            printPreviewWorker.RunWorkerAsync();
            App.ShowWindow(_previewBusy, this.ParentWindow, true);

            base.ShowWaitCursor(false);
        }
        #endregion

        #region Event handlers

        void PrintPreviewWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            Int32 currentProgress = e.ProgressPercentage;

            _previewBusy.SetValue(ModalBusy.ProgressPercentageProperty, currentProgress);

            if (currentProgress == ReportProgress_BuildingReport)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, "Building Report..");
            }

            if (currentProgress == ReportProgress_ExecutingQuery)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, "Executing Query..");
            }

            if (currentProgress == ReportProgress_OpeningReport)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, "Saving Report to File..");
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this._selectedReport = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
