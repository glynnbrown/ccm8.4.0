﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25521 : M.Pettit
//  Created
#endregion

#endregion

using System;
using System.Windows;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportViewer
{
    /// <summary>
    /// Interaction logic for ReportGridViewerHomeTab.xaml
    /// </summary>
    public partial class ReportGridPreviewHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportGridPreviewViewModel), typeof(ReportGridPreviewHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportGridPreviewViewModel ViewModel
        {
            get { return (ReportGridPreviewViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ReportGridPreviewHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
