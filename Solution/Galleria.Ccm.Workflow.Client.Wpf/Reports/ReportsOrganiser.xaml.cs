﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
// V8-26843 : M.Pettit
//  SelectedReport is now a ReportInfo rather than Report model object
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using Fluent;
using System.Windows.Data;
using Galleria.Ccm.Model;
using Galleria.Reporting.Model;
using System.Windows.Media;
using System.Windows.Input;

namespace Galleria.Ccm.Workflow.Client.Wpf.Reports
{
    /// <summary>
    /// Interaction logic for ReportsOrganiser.xaml
    /// </summary>
    public sealed partial class ReportsOrganiser : UserControl, IDisposable
    {
        #region Fields

        private readonly List<RibbonTabItem> _ribbonTabList;
        private readonly ReportsSidePanel _sidePanel;
        private GridLength _bottomPanelHeigth = new GridLength(300.0);

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportsViewModel), typeof(ReportsOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportsOrganiser senderControl = (ReportsOrganiser)obj;

            if (e.OldValue != null)
            {
                ReportsViewModel oldModel = (ReportsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ReportsViewModel newModel = (ReportsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        /// <summary>
        /// Viewmodel controller
        /// </summary>
        public ReportsViewModel ViewModel
        {
            get { return (ReportsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ReportsOrganiser()
        {
            InitializeComponent();
            this.ViewModel = new ReportsViewModel();

            //Add link to helpfile
            //Help.SetFilename((DependencyObject)this, App.ViewState.HelpFilePath);
            //Help.SetKeyword((DependencyObject)this, "3");

            //create the ribbon
            var homeTab = new ReportsHomeTab();
            BindingOperations.SetBinding(homeTab, ReportsHomeTab.ViewModelProperty, new Binding(ViewModelProperty.Name) { Source = this });

            _ribbonTabList = new List<RibbonTabItem> { homeTab };

            //create sidepanel
            _sidePanel = new ReportsSidePanel();
            BindingOperations.SetBinding(_sidePanel, 
                ReportsSidePanel.ViewModelProperty,
                new Binding(ReportsSidePanel.ViewModelProperty.Name) { Source = this });


            this.Loaded += ReportsOrganiser_Loaded;
        }

        /// <summary>
        /// Carries out actions whenever this screen loads.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReportsOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            //send ribbon request
            App.ViewState.ShowRibbonTabs(_ribbonTabList);

            //send sidepanel request
            App.ViewState.ShowSidePanel(_sidePanel);
        }

        #endregion

        #region Event Handlers

        private void reportInfosListBox_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Boolean eventHandledByButton = false; //was the event handled by a button press on the listbox item?
            
            ListBoxItem clickedItem = null;

            DependencyObject obj = (DependencyObject)e.OriginalSource;

            while (obj != null && obj != reportInfosListBox)
            {
                if (obj.GetType() == typeof(System.Windows.Controls.Button))
                {
                    eventHandledByButton = true;
                    break;
                }
                
                if (obj.GetType() == typeof(ListBoxItem))
                {
                    clickedItem = (ListBoxItem)obj;
                    break;
                }
                obj = VisualTreeHelper.GetParent(obj);
            }

            if (!eventHandledByButton)
            {
                ListBox senderControl = (ListBox)sender;

                if (clickedItem != null)
                {
                    //get the selected report info
                    ReportInfo reportToOpen = clickedItem.Content as ReportInfo;
                    if (reportToOpen != null)
                    {
                        //ensure we are opening the correct report
                        if (this.ViewModel.SelectedReport.Id != reportToOpen.Id)
                        {
                            this.ViewModel.SelectedReport = reportToOpen;
                        }
                        //open it
                        this.ViewModel.OpenCommand.Execute();
                    }
                }
            }
        }

        private void xShowPdfButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button senderButton = (System.Windows.Controls.Button)sender;
            
            var selectedListBoxItem =
                ((ListBoxItem)reportInfosListBox.ContainerFromElement(senderButton)).Content;

            if (selectedListBoxItem != null)
            {
                ReportInfo selectedReport = (ReportInfo)selectedListBoxItem;
                if (this.ViewModel != null)
                {
                    this.ViewModel.ShowPdfViewerCommand.Execute(selectedReport.Id); 
                }
            }
        }

        private void xShowDataButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button senderButton = (System.Windows.Controls.Button)sender;

            var selectedListBoxItem =
                ((ListBoxItem)reportInfosListBox.ContainerFromElement(senderButton)).Content;

            if (selectedListBoxItem != null)
            {
                ReportInfo selectedReport = (ReportInfo)selectedListBoxItem;
                if (this.ViewModel != null)
                {
                    this.ViewModel.ShowDataViewerCommand.Execute(selectedReport.Id);
                }
            }
        }

        private void xExportExcelButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button senderButton = (System.Windows.Controls.Button)sender;

            var selectedListBoxItem =
                ((ListBoxItem)reportInfosListBox.ContainerFromElement(senderButton)).Content;

            if (selectedListBoxItem != null)
            {
                ReportInfo selectedReport = (ReportInfo)selectedListBoxItem;
                if (this.ViewModel != null)
                {
                    this.ViewModel.ExcelExportCommand.Execute(selectedReport.Id);
                }
            }
        }
        /// <summary>
        /// Uses funcionality of the mouse double-click event triggered by the return key for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reportInfosListBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (reportInfosListBox != null && reportInfosListBox.SelectedItem != null)
                {
                    //get the selected report info
                    ReportInfo reportToOpen = reportInfosListBox.SelectedItem as ReportInfo;
                    if (reportToOpen != null)
                    {
                        //ensure we are opening the correct report
                        if (this.ViewModel.SelectedReport.Id != reportToOpen.Id)
                        {
                            this.ViewModel.SelectedReport = reportToOpen;
                        }
                        //open it
                        this.ViewModel.OpenCommand.Execute();
                    }
                }
            }
            else
            {
                return;
            }
        }
        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.Loaded -= ReportsOrganiser_Loaded;

                IDisposable disposableViewModel = this.ViewModel;
                this.ViewModel = null;
                if (disposableViewModel != null)
                {
                    disposableViewModel.Dispose();
                }

                _isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }

        #endregion

        private void reportInfosListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (reportInfosListBox != null)
            {
                if (reportInfosListBox.SelectedItem != null)
                {
                    var selectedListBoxItem = reportInfosListBox.SelectedItem;

                    if (selectedListBoxItem != null)
                    {
                        ReportInfo selectedReport = (ReportInfo)selectedListBoxItem;
                        if (this.ViewModel != null)
                        {
                            this.ViewModel.SelectedReport = selectedReport;
                            CommandManager.InvalidateRequerySuggested();
                        }
                    }
                }
            }
        }
    }
}
