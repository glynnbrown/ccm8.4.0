﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25788 : M.Pettit ~ Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.Reports
{
    /// <summary>
    /// Interaction logic for ReportsSidePanel.xaml
    /// </summary>
    public partial class ReportsSidePanel : UserControl
    {
        #region Properties

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
            typeof(ReportsViewModel),
            typeof(ReportsSidePanel),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportsSidePanel senderControl = (ReportsSidePanel)obj;

            if (e.OldValue != null)
            {
                ReportsViewModel oldModel = (ReportsViewModel)e.OldValue;
                //oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                ReportsViewModel newModel = (ReportsViewModel)e.NewValue;
                //newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public ReportsViewModel ViewModel
        {
            get { return (ReportsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor
        public ReportsSidePanel()
        {
            InitializeComponent();
        }
        #endregion

        #region Event Handlers
        /// <summary>
        /// Handles user selection of a treeview report filter option item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xFilterOptionsTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (xFilterOptionsTreeView != null)
            {
                if (xFilterOptionsTreeView.SelectedItem != null)
                {
                    if (this.ViewModel != null)
                    {
                        //select the item
                        this.ViewModel.SelectedFilterOption = (ReportFilterOptionItem)e.NewValue;
                    }
                }
            }
        }

        private void searchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (xFilterOptionsTreeView != null)
            {
                if (xFilterOptionsTreeView.Items != null && xFilterOptionsTreeView.Items.Count > 0)
                {
                    //Select the All reports item
                }
            }
        }

        #endregion
    }
}
