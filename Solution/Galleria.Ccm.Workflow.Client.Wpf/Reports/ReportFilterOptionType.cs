﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25788 : M.Pettit ~ Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.Reports
{
    public enum ReportFilterOptionType
    {
        All = 0,
        Recent = 1,
        Favourites = 2
    }

    public static class ReportFilterOptionTypeHelper
    {
        public static readonly Dictionary<ReportFilterOptionType, String> FriendlyNames =
           new Dictionary<ReportFilterOptionType, String>()
            {
                {ReportFilterOptionType.All, Message.Enum_ReportFilterOptionType_All_Name },
                {ReportFilterOptionType.Recent, Message.Enum_ReportFilterOptionType_Recent_Name },
                {ReportFilterOptionType.Favourites, Message.Enum_ReportFilterOptionType_Favourite_Name }
            };
    }
}
