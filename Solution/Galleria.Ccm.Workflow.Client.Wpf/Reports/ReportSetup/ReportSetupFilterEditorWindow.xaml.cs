﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25692 : M. Shelley
// 2014-06-13	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for ReportSetupFilterEditorWindow.xaml
    /// </summary>
    public partial class ReportSetupFilterEditorWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportSetupFilterEditorViewModel), typeof(ReportSetupFilterEditorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportSetupFilterEditorViewModel ViewModel
        {
            get { return (ReportSetupFilterEditorViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportSetupFilterEditorWindow senderControl = obj as ReportSetupFilterEditorWindow;

            ReportSetupFilterEditorViewModel oldModel = e.OldValue as ReportSetupFilterEditorViewModel;
            ReportSetupFilterEditorViewModel newModel = e.NewValue as ReportSetupFilterEditorViewModel;

            if (e.OldValue != null)
            {
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        public ReportSetupFilterEditorWindow(ReportSetupFilterEditorViewModel viewModel)
        {
            InitializeComponent();

			// Setup the viewmodel for this window
			this.ViewModel = viewModel;
        }
    }
}
