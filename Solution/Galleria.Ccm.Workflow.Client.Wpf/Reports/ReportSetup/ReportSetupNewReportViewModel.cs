﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25034 : M.Shelley
//  Created
#endregion

#endregion

using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Galleria.Reporting.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    public class ReportSetupNewReportViewModel : ViewModelAttachedControlObject<ReportSetupNewReportTypeWindow>
    {
        #region Fields

        private DataModelInfoList _modelsList = null;
        private DataModelInfo _selectedModel = null;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ModelsListProperty = WpfHelper.GetPropertyPath<ReportSetupNewReportViewModel>(c => c.ModelsList);
        public static readonly PropertyPath SelectedModelProperty = WpfHelper.GetPropertyPath<ReportSetupNewReportViewModel>(c => c.SelectedModel);

        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<ReportSetupNewReportViewModel>(p => p.ReportTypeOkCommand);

        #endregion

        #region Properties

        /// <summary>
        /// List of the defined report data models
        /// </summary>
        public DataModelInfoList ModelsList
        {
            get { return _modelsList; }
            private set 
            { 
                _modelsList = value;
                OnPropertyChanged(ModelsListProperty);
            }
        }

        /// <summary>
        /// The report model selected by the user
        /// </summary>
        public DataModelInfo SelectedModel
        {
            get { return _selectedModel; }
            set
            {
                _selectedModel = value;
                OnPropertyChanged(SelectedModelProperty);
            }
        }

        public Boolean UserCancelled
        {
            get; set;
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _reportTypeOkCommand;

        /// <summary>
        /// Creates a new report
        /// </summary>
        public RelayCommand ReportTypeOkCommand
        {
            get
            {
                if (_reportTypeOkCommand == null)
                {
                    _reportTypeOkCommand = new RelayCommand(
                        p => ReportTypeOk_Executed(),
                        p => ReportTypeOk_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        Icon = null,
                    };
                    base.ViewModelCommands.Add(_reportTypeOkCommand);
                }
                return _reportTypeOkCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ReportTypeOk_CanExecute()
        {
            return (this.SelectedModel != null);
        }

        private void ReportTypeOk_Executed()
        {
            UserCancelled = false;
            if (this.AttachedControl != null)
            {
                //And close the form
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion


        #region  Constructors

        public ReportSetupNewReportViewModel(DataModelInfoList dataModelInfos)
        {
            ModelsList = dataModelInfos;
            UserCancelled = true;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            // ToDo: dispose of any objects
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.ModelsList = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
