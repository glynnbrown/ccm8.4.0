﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (GRB100)
// V8-26996 : M.Pettit
//      Created
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Reporting.Security;
using System.Windows.Input;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Reporting.Model;
using System.IO;
using System.Globalization;
using Csla;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    public class ReportSetupBackstageHelpViewModel : ViewModelAttachedControlObject<ReportSetupBackstageHelp>
    {
        #region Fields
        private String _buildVersion = String.Empty;
        private String _reportingDllVersion = String.Empty;
        private String _reportingControlsDllVersion = String.Empty;
        const String _exCategory = "ReportSetupBackstageHelp"; //category name for any exceptions raised to gibraltar from here
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath BuildVersionProperty = WpfHelper.GetPropertyPath<ReportSetupBackstageHelpViewModel>(p => p.BuildVersion);
        public static readonly PropertyPath ReportingDllVersionProperty = WpfHelper.GetPropertyPath<ReportSetupBackstageHelpViewModel>(p => p.ReportingDllVersion);
        public static readonly PropertyPath ReportingControlsDllVersionProperty = WpfHelper.GetPropertyPath<ReportSetupBackstageHelpViewModel>(p => p.ReportingControlsDllVersion);

        public static readonly PropertyPath OnlineHelpCommandProperty = WpfHelper.GetPropertyPath<ReportSetupBackstageHelpViewModel>(p => p.OnlineHelpCommand);
        public static readonly PropertyPath ContactUsCommandProperty = WpfHelper.GetPropertyPath<ReportSetupBackstageHelpViewModel>(p => p.ContactUsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The version of the current build
        /// </summary>
        public String BuildVersion
        {
            get { return _buildVersion; }
            private set
            {
                _buildVersion = value;
                OnPropertyChanged(BuildVersionProperty);
            }
        }

        public String ReportingDllVersion
        {
            get { return _reportingDllVersion; }
            private set
            {
                _reportingDllVersion = value;
                OnPropertyChanged(ReportingDllVersionProperty);
            }
        }

        public String ReportingControlsDllVersion
        {
            get { return _reportingControlsDllVersion; }
            private set
            {
                _reportingControlsDllVersion = value;
                OnPropertyChanged(ReportingControlsDllVersionProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="state"></param>
        public ReportSetupBackstageHelpViewModel()
        {
            const String reportDllFileName = "Galleria.Reporting.dll";
            const String reportControlsDllFileName = "Galleria.Reporting.Controls.Wpf.dll";

            String dllFilePath = String.Empty;

            //Get Version Information
            this.BuildVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            String currentPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            dllFilePath = Path.Combine(currentPath, reportDllFileName);
            if (System.IO.File.Exists(dllFilePath))
            {
                FileVersionInfo reportingDllInfo = FileVersionInfo.GetVersionInfo(dllFilePath);
                this.ReportingDllVersion = reportingDllInfo.ProductVersion;
            }
            else
            {
                this.ReportingDllVersion = Message.BackstageHelp_Version_NotAvailable;
            }
            dllFilePath = Path.Combine(currentPath, reportControlsDllFileName);
            if (System.IO.File.Exists(dllFilePath))
            {
                FileVersionInfo reportingControlsDllInfo = FileVersionInfo.GetVersionInfo(dllFilePath);
                this.ReportingControlsDllVersion = reportingControlsDllInfo.ProductVersion;
            }
            else
            {
                this.ReportingControlsDllVersion = Message.BackstageHelp_Version_NotAvailable;
            }
        }

        #endregion

        #region Commands

        #region OnlineHelpCommand

        private RelayCommand _onlineHelpCommand;

        public RelayCommand OnlineHelpCommand
        {
            get
            {
                if (_onlineHelpCommand == null)
                {
                    this._onlineHelpCommand = new RelayCommand(p => OnlineHelp_Executed())
                    {
                        FriendlyName = Message.BackstageHelp_OnlineHelp_Button,
                        Icon = ImageResources.BackstageHelp_Help_32
                    };
                    this.ViewModelCommands.Add(_onlineHelpCommand);
                }
                return _onlineHelpCommand;
            }
        }

        private void OnlineHelp_Executed()
        {
            if (this.AttachedControl != null)
            {
                String loc = Assembly.GetExecutingAssembly().Location;
                String path = System.IO.Path.GetDirectoryName(loc);

                String fileToOpen = System.IO.Path.Combine(path, App.ViewState.HelpFilePath);

                if (System.IO.File.Exists(fileToOpen))
                {
                    Process.Start(fileToOpen);
                }
                else
                {
                    #region Inform User
                    ModalMessage msg = new ModalMessage();
                    msg.Title = System.Windows.Application.Current.MainWindow.GetType().Assembly.GetName().Name;
                    msg.Header = Message.BackstageHelp_CannotFindFile_Header;
                    msg.Description = String.Format(Message.BackstageHelp_CannotFindHelpFile_Message, fileToOpen);
                    msg.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    msg.ButtonCount = 1;
                    msg.Button1Content = Message.Generic_Ok;
                    msg.MessageIcon = ImageResources.DialogWarning;

                    App.ShowWindow(msg, true);
                    #endregion
                }
            }
        }

        #endregion

        #region ContactUsCommand

        private RelayCommand _contactUsCommand;

        public RelayCommand ContactUsCommand
        {
            get
            {
                if (_contactUsCommand == null)
                {
                    this._contactUsCommand = new RelayCommand(p => ContactUs_Executed())
                    {
                        FriendlyName = Message.BackstageHelp_ContactUs_Button,
                        Icon = ImageResources.BackstageHelp_ContactUs_32
                    };
                    this.ViewModelCommands.Add(_contactUsCommand);
                }
                return _contactUsCommand;
            }
        }

        private void ContactUs_Executed()
        {
            if (this.AttachedControl != null)
            {
                App.ShowWindow(new Galleria.Ccm.Common.Wpf.Help.ContactUsWindow(), true);
            }
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //_state = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
