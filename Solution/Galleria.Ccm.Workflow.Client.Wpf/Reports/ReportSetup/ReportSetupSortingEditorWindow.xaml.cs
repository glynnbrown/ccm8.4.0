﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25940 : M.Shelley
//  2014-07-08	Created
#endregion
#endregion

using Galleria.Framework.Controls.Wpf;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for ReportSetupSortingEditorWindow.xaml
    /// </summary>
    public partial class ReportSetupSortingEditorWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportSetupSortingEditorViewModel), typeof(ReportSetupSortingEditorWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportSetupSortingEditorViewModel ViewModel
        {
            get { return (ReportSetupSortingEditorViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportSetupSortingEditorWindow senderControl = obj as ReportSetupSortingEditorWindow;

            ReportSetupSortingEditorViewModel oldModel = e.OldValue as ReportSetupSortingEditorViewModel;
            ReportSetupSortingEditorViewModel newModel = e.NewValue as ReportSetupSortingEditorViewModel;

            if (e.OldValue != null)
            {
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public ReportSetupSortingEditorWindow(ReportSetupSortingEditorViewModel viewModel)
        {
            InitializeComponent();

            // Setup the viewmodel for this window
            this.ViewModel = viewModel;
        }

        #endregion
    }
}
