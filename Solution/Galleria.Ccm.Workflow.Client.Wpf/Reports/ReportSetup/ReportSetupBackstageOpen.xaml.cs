﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: GRB 100
// GRB-25034 : M.Pettit
//  Created
// V8-27291 : M.Pettit
//  Allows user to delete reports without opening them (although the model is still loaded..)
// V8-28122 : M.Pettit
// Fixed issue with "Open" button being permanently disabled
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Reporting.Model;
using Galleria.Framework.Model;
using System.Windows.Media;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for ReportSetupBackstageOpen.xaml
    /// </summary>
    public partial class ReportSetupBackstageOpen : UserControl
    {
        #region Constants
        const String DeleteBackstageSelectedReportCommandKey = "DeleteBackstageSelectedReportCommand";
        #endregion

        #region Fields
        private ObservableCollection<ReportInfo> _searchResults = new ObservableCollection<ReportInfo>();
        #endregion
        
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportSetupViewModel), typeof(ReportSetupBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportSetupViewModel ViewModel
        {
            get { return (ReportSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportSetupBackstageOpen senderControl = (ReportSetupBackstageOpen)obj;

            if (e.OldValue != null)
            {
                ReportSetupViewModel oldModel = (ReportSetupViewModel)e.OldValue;
                senderControl.Resources.Remove(DeleteBackstageSelectedReportCommandKey);
                oldModel.AvailableReports.BulkCollectionChanged -= senderControl.ViewModel_AvailableReportsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                ReportSetupViewModel newModel = (ReportSetupViewModel)e.NewValue;
                senderControl.Resources.Add(DeleteBackstageSelectedReportCommandKey, newModel.DeleteBackstageSelectedReportCommand);
                newModel.AvailableReports.BulkCollectionChanged += senderControl.ViewModel_AvailableReportsBulkCollectionChanged;
            }
        }

        #endregion

        #region GridSelectedReportProperty

        public static readonly DependencyProperty GridSelectedReportProperty =
            DependencyProperty.Register("GridSelectedReport", typeof(ReportInfo), typeof(ReportSetupBackstageOpen));

        /// <summary>
        /// Gets/Sets the report selected in the open grid
        /// </summary>
        public ReportInfo GridSelectedReport
        {
            get { return (ReportInfo)GetValue(GridSelectedReportProperty); }
            set { SetValue(GridSelectedReportProperty, value); }
        }

        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(ReportSetupBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportSetupBackstageOpen senderControl = (ReportSetupBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<ReportInfo>),
            typeof(ReportSetupBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<ReportInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<ReportInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ReportSetupBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<ReportInfo>(_searchResults); 
            
            InitializeComponent();

            this.Loaded += ReportSetupBackstageOpen_Loaded;
        }

        private void ReportSetupBackstageOpen_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateSearchResults();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                IEnumerable<ReportInfo> results = this.ViewModel.GetMatchingReports(this.SearchText.ToUpperInvariant());

                foreach (ReportInfo locInfo in results)
                {
                    _searchResults.Add(locInfo);
                }
            }
        }

        #endregion

        #region Event Handlers

        private void ViewModel_AvailableReportsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// Handles event when user doble-clicks item in reportInfos list grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ReportInfo reportToOpen = (ReportInfo)searchResultsGrid.SelectedItem;

            if (reportToOpen != null)
            {
                //send the report to be opened
                this.ViewModel.OpenCommand.Execute(reportToOpen.Id);
            }
        }

        /// <summary>
        /// Handles event when the user has moved the scrollbar of the reports list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsGrid_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            //Force Can_Execute to be fired against all open report infos 
            //in list to update their button states. Without this, the visibility 
            //of the buttons is not correct as new items appear during scrolling
            CommandManager.InvalidateRequerySuggested();
        }

        #endregion
    }
}
