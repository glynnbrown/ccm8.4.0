﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: GRB 100
// GRB-25034 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for ReportSetupBackstageNew.xaml
    /// </summary>
    public partial class ReportSetupBackstageNew : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportSetupViewModel), typeof(ReportSetupBackstageNew),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportSetupViewModel ViewModel
        {
            get { return (ReportSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportSetupBackstageNew senderControl = (ReportSetupBackstageNew)obj;

            if (e.OldValue != null)
            {
                ReportSetupViewModel oldModel = (ReportSetupViewModel)e.OldValue;
            }

            if (e.NewValue != null)
            {
                ReportSetupViewModel newModel = (ReportSetupViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor
        public ReportSetupBackstageNew()
        {
            InitializeComponent();
        }
        #endregion
    }
}
