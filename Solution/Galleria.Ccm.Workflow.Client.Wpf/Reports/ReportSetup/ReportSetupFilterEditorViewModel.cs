﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Helpers;
using Galleria.Reporting;
using Galleria.Reporting.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    public class ReportSetupFilterEditorViewModel : ViewModelAttachedControlObject<ReportSetupFilterEditorWindow>
    {
        #region Fields

        private Report _selectedReport;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath SelectedReportProperty = WpfHelper.GetPropertyPath<ReportSetupFilterEditorViewModel>(c => c.SelectedReport);

        #endregion

        #region Properties
        ///
        /// <summary>
        /// Property for the selected report
        /// </summary>
        public Report SelectedReport
        {
            get { return _selectedReport; }
            set
            {
                Report oldSelectedReport = _selectedReport;
                _selectedReport = value;
                OnPropertyChanged(SelectedReportProperty);
            }
        }

        #endregion

        #region Commands

        #endregion

        #region Constructor

        public ReportSetupFilterEditorViewModel(Report report)
        {
            this.SelectedReport = report;

            this.SelectedReport.BeginEdit();
        }

        #endregion

        #region Methods

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                // ToDo: dispose of resources
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
