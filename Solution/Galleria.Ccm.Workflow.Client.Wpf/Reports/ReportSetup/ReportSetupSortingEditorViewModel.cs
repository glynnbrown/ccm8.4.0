﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25940 : M.Shelley
//  2014-07-08	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Reporting.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    public class ReportSetupSortingEditorViewModel : ViewModelAttachedControlObject<ReportSetupSortingEditorWindow>
    {
        #region Fields

        private Report _selectedReport;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath SelectedReportProperty = WpfHelper.GetPropertyPath<ReportSetupSortingEditorViewModel>(c => c.SelectedReport);

        #endregion

        #region Properties
        ///
        /// <summary>
        /// Property for the selected report
        /// </summary>
        public Report SelectedReport
        {
            get { return _selectedReport; }
            set
            {
                Report oldSelectedReport = _selectedReport;
                _selectedReport = value;
                OnPropertyChanged(SelectedReportProperty);
            }
        }

        #endregion

        #region Constructor

        public ReportSetupSortingEditorViewModel(Report report)
        {
            this.SelectedReport = report;

            this.SelectedReport.BeginEdit();
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                // ToDo: dispose of resources
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}

