﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25034 : M.Shelley
//  Created
#endregion
#region Version History: (GRB 1.0.4)
// GRB-29254/V8-29195 : M.Pettit
//  Added support for receiving PropertiesPanelVisibilityChangedEvent events
// V8-29646 : M.Shelley
//  Allow user to specify returning distinct rows only in the query results to avoid duplicates
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

using Fluent;

using Galleria.Reporting.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for BaseWindowHeaderTab.xaml
    /// </summary>
    public partial class ReportSetupHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportSetupViewModel), typeof(ReportSetupHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportSetupViewModel ViewModel
        {
            get { return (ReportSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ReportSetupHomeTab()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(ReportSetupHomeTab_Loaded);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Loaded event handler - unfortunately this is required to setup the command targets of the 
        /// ribbon buttons that use commands in the ReportEditorSections control.
        /// Something in logical tree is taking focus and preventing the commands CanExecute method 
        /// being called. Also because the ReportEditorSection control is not a firect ancester of the
        /// ribbon buttons, we have to locate the parent window and then traverse the viusal tree to locate the
        /// ReportEditorSections instance and then wire up the command targets.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ReportSetupHomeTab_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(ReportSetupHomeTab_Loaded);

            // Setup command target for the buttons that need to target the ReportEditorSections component
            // first get the parent window
            var parentWindow = Window.GetWindow(this);

            // then try and locate the ReportEditorSections control
            var sectionsChildren = Galleria.Reporting.Controls.Wpf.Common.Helpers.FindVisualChildren<Galleria.Reporting.Controls.Wpf.ReportEditorSections>(parentWindow);
            var reportSections = sectionsChildren.FirstOrDefault();

            // Now wire up the command targets for the "awkward" command buttons to the ReportEditorSection control
            if (reportSections != null)
            {
                // get a reference to the "Add Text" button button
                AddTextToggleButton.CommandTarget = reportSections;
                //AddLineToggleButton.CommandTarget = reportSections;
                RemoveItemButton.CommandTarget = reportSections;
                AddChartToggleButton.CommandTarget = reportSections;
            }

            // Now do the same for the buttons commands to the ReportEditorPane control
            var paneChildren = Galleria.Reporting.Controls.Wpf.Common.Helpers.FindVisualChildren<Galleria.Reporting.Controls.Wpf.ReportEditorPane>(parentWindow);
            var reportEditorPane = paneChildren.FirstOrDefault();

            if (reportEditorPane != null)
            {
                ShowPropertiesButton.CommandTarget = reportEditorPane;
                SelectDistinctButton.CommandTarget = reportEditorPane;
            }

            // Subscribe to the DrawingFinishedEvent in the ReportEditorSections class so that the 
            // current drawing component can be reset
            EventManager.RegisterClassHandler(
                typeof(ReportEditorSections),
                ReportEditorSections.DrawingFinishedEvent,
                new RoutedEventHandler(OnDrawingFinishedEvent));

            // Subscribe to the PropertiesPanelVisibilityChangedEvent in the ReportEditorPane class so that the 
            // current drawing component can be reset
            EventManager.RegisterClassHandler(
                typeof(ReportEditorPane),
                ReportEditorPane.PropertiesPanelVisibilityChangedEvent,
                new RoutedEventHandler(OnPropertiesPanelVisibilityChangedEvent));
        }

        /// <summary>
        /// Responds to the New command executing
        /// </summary>
        private void ViewModel_NewCommandExecuted(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Responds to the Save command executing
        /// </summary>
        private void ViewModel_SaveCommandExecuted(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Event handler for the view model property changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //If the selected section has changed
            //if (e.PropertyName == PrintOptionSetupViewModel.SelectedPrintOptionSectionProperty.Path)
            //{
            //    OnSelectedPrintOptionSectionChanged();
            //}
        }

        /// <summary>
        /// Event handler to detect when the ReportEditorSections control has finished actually 
        /// drawing a component. Used to reset the state of the drawing component buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDrawingFinishedEvent(object sender, RoutedEventArgs e)
        {
            var reportSectionsControl = sender as ReportEditorSections;

            if (reportSectionsControl == null)
            {
                return;
            }

            // we have a valid instance of the ReportEditorSections control, so reset the 
            // drawing component type in the viewmodel
            var setupVm = this.ViewModel as ReportSetupViewModel;
            if (setupVm == null)
            {
                return;
            }

            setupVm.SelectedReportComponent = Reporting.Model.ReportComponentType.None;
        }

        /// <summary>
        /// Event handler to detect when the visibility of the properties Panel has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPropertiesPanelVisibilityChangedEvent(object sender, RoutedEventArgs e)
        {
            var editorPanel = sender as ReportEditorPane;
            if (editorPanel == null) return;

            var vm = this.ViewModel as ReportSetupViewModel;
            if (vm == null) return;
            //Update the properties button
            vm.PropertiesPanelIsVisible = editorPanel.PropertiesPanelIsVisible;
            e.Handled = true;
        }

        #endregion
    }
}
