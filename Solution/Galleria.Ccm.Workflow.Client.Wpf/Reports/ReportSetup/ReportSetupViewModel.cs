﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0.0)
// GRB-25034 : M.Pettit
//  Created
// GRB-25519 M.Pettit
//  Added info panel
// V8-25788 M.Pettit
//  Implemented in CCM V8
// V8-26902 : M.Pettit
//  Updates to Reporting Dlls required corresonding client code changes
// GRB-26955 : M.Shelley
//  Changes to disable all the ribbon bar buttons and menus if no report is currently loaded
// V8-27291 : M.Pettit
//  Added DeleteBackstageSelectedReportCommand 
// V8-27290 : M.Pettit
//  Validate report when opening 
#endregion

#region Version History: (CCM 8.0.2)
// GRB-29254/V8-29195 : M.Pettit
//  Added PropertiesPanelIsVisible property
#endregion

#region Version History: CCM803
// V8-29345 : M.Pettit
//  ReportId is now an Object datatype
// V8-29471 : M.Pettit
//  DataGrid viewer column order now follows UI order rules (by section/component.x-position)
// V8-29300 : M.Pettit
//  Added ExportToFile, ImportFromFile commands
// V8-29646 : M.Shelley
//  Allow user to specify returning distinct rows only in the query results to avoid duplicates
#endregion

#region Version History: CCM820
// V8-30785 : M.Pettit
//	Moved New Report Type selector window into controls.wpf project
// V8-30832 / GRB-29315 : M.Pettit
//	Save, Print commands now check a report's suppress detail setting is valid before allowing the operation to continue
// V8-30949 : M.Pettit
//	Added support for Arrange tab (Grid Spacing, Grid Colour properties)
// V8-31296 / GRB-31292 : M.Pettit;
//	Importing reports from flat file source validates report before opening
#endregion

#region Version History: CCM830
// V8-31837 / GRB-32043 : M.Shelley
//  Added the ReportAttachTemplateCommand to open the report info window with the "template" "tab" visible
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Processes;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Reporting.Controls.Wpf.FilterEditor;
using Galleria.Reporting.Controls.Wpf.FormulaFieldEditor;
using Galleria.Reporting.Controls.Wpf.GroupEditor;
using Galleria.Reporting.Controls.Wpf.ParametersEditor;
using Galleria.Reporting.Controls.Wpf.ReportGridViewer;
using Galleria.Reporting.Controls.Wpf.ReportInfoEditor;
using Galleria.Reporting.Controls.Wpf.SortingEditor;
using Galleria.Reporting.Controls.Wpf.SubTotalFieldEditor;
using Galleria.Reporting.Dal.DataTransferObjects;
using Galleria.Reporting.Helpers;
using Galleria.Reporting.Model;
using Galleria.Reporting.Processes.FetchReportData;
using Galleria.Reporting.Processes.RenderPdfReport;
using Microsoft.Win32;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    public class ReportSetupViewModel : ViewModelAttachedControlObject<ReportSetupWindow>
    {
        #region Constants

        private const Int32 FetchDataProgress_StartFetch = 10;
        private const Int32 FetchDataProgress_Complete = 100;

        private const Int32 ReportProgress_BuildingReport = 10;
        private const Int32 ReportProgress_SavingReport = 50;
        private const Int32 ReportProgress_OpeningReport = 90;

        const String _exCategory = "Reports";
        #endregion
        
        #region Fields
        private Report _selectedReport;
        private BulkObservableCollection<ReportInfo> _reportInfos = new BulkObservableCollection<ReportInfo>();

        private ReportComponentType _selectedComponent = ReportComponentType.None;

		private Boolean _propertiesPanelIsVisible = false;

		private Boolean _showDebug = false;
		private Boolean _selectDistinct = false;

        //Print Preview
        private ModalBusy _previewBusy;
        private Exception _processException = null;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath WindowTitleProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.WindowTitle);
        public static readonly PropertyPath SelectedReportProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(c => c.SelectedReport);
        public static readonly PropertyPath AvailableReportsProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(c => c.AvailableReports);
		public static readonly PropertyPath ReportIsLoadedProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(c => c.ReportIsLoaded);
        
        public static readonly PropertyPath PrintPreviewAsPdfInExternalViewerCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.PrintPreviewAsPdfInExternalViewerCommand); 
        public static readonly PropertyPath PrintPreviewAsGridCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.PrintPreviewAsGridCommand);
        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.NewCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath ExportToFileCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ExportToFileCommand);
        public static readonly PropertyPath ImportFromFileCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ImportFromFileCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath ReportInfoCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ReportInfoCommand);
        public static readonly PropertyPath ReportTemplateCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ReportAttachTemplateCommand);

        public static readonly PropertyPath ReportPortraitCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ReportPortraitCommand);
        public static readonly PropertyPath ReportLandscapeCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ReportLandscapeCommand);

        public static readonly PropertyPath ReportSelectCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ReportSelectComponentCommand);
        public static readonly PropertyPath ReportAddPictureCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ReportAddPictureCommand);

        public static readonly PropertyPath EditGroupsCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.EditGroupsCommand);
        public static readonly PropertyPath EditFiltersCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.EditFiltersCommand);
        public static readonly PropertyPath EditSortingCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.EditSortingCommand);
        public static readonly PropertyPath EditParametersCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.EditParametersCommand);

        public static readonly PropertyPath AddFormulaFieldCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.AddFormulaFieldCommand);

        public static readonly PropertyPath AddSubTotalFieldCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.AddSubTotalFieldCommand);
        
        public static readonly PropertyPath ReportSelectedComponentProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SelectedReportComponent);
        public static readonly PropertyPath ReportPaperSizeProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SelectedPaperSize);
        public static readonly PropertyPath ReportPageOrientationProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SelectedPageOrientation);
        public static readonly PropertyPath ReportPaperMarginProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SelectedPaperMargin);

		public static readonly PropertyPath ShowGridCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ShowGridCommand);
		public static readonly PropertyPath ShowGridProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.ShowGrid);
		public static readonly PropertyPath SnapToGridCommandProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SnapToGridCommand);
		public static readonly PropertyPath SnapToGridProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SnapToGrid);
		public static readonly PropertyPath GridSpacingProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.GridSpacing);
		public static readonly PropertyPath GridColourProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.GridColour);

		public static readonly PropertyPath PropertiesPanelIsVisibleProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.PropertiesPanelIsVisible);
		public static readonly PropertyPath SelectDistinctProperty = WpfHelper.GetPropertyPath<ReportSetupViewModel>(p => p.SelectDistinct);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the list of available reports 
        /// </summary>
        public BulkObservableCollection<ReportInfo> AvailableReports
        {
            get { return _reportInfos; }
        }

        /// <summary>
        /// Returns the window's title
        /// </summary>
        public String WindowTitle
        {
            get
            {
                if (this.SelectedReport != null)
                {
                    return String.Format("{0} - {1}", Message.ReportBuilderWindow_Title, this.SelectedReport.Name);
                }
                return Message.ReportBuilderWindow_Title;
            }
        }

        /// <summary>
        /// Returns the selected report
        /// </summary>
        public Report SelectedReport
        {
            get { return _selectedReport; }
            set
            {
				Report oldSelectedReport = _selectedReport;
				_selectedReport = value;
				OnPropertyChanged(SelectedReportProperty);
				OnPropertyChanged(ReportIsLoadedProperty);
				OnPropertyChanged(WindowTitleProperty);
				OnPropertyChanged(ReportPaperSizeProperty);
				OnPropertyChanged(ReportPageOrientationProperty);
				OnPropertyChanged(ReportPaperMarginProperty);
				OnPropertyChanged(GridColourProperty);
				OnPropertyChanged(GridSpacingProperty);
				OnPropertyChanged(SnapToGridProperty);
				OnPropertyChanged(ShowGridProperty);
				OnSelectedReportChanged(oldSelectedReport, value);

                if (_selectedReport != null)
                {
                    SelectDistinct = (_selectedReport.SelectDistinct == null ? false : (Boolean)_selectedReport.SelectDistinct);
                }
            }
        }

		/// <summary>
		/// Returns whether the selected report is loaded
		/// </summary>
		public Boolean ReportIsLoaded
		{
			get { return _selectedReport != null; }
		}

        /// <summary>
        /// Property for the currently selected component type - linked to the "Tools" toggle buttons 
        /// on the home tab.
        /// </summary>
        public ReportComponentType SelectedReportComponent
        {
            get { return _selectedComponent; }
            set
            {
                _selectedComponent = value;
                OnPropertyChanged(ReportSelectedComponentProperty);
            }
        }

        /// <summary>
        /// Property holding whether the Properties Panel should be visible or hidden
        /// </summary>
        public Boolean PropertiesPanelIsVisible
        {
            get
            {
                return _propertiesPanelIsVisible;
            }
            set
            {
                _propertiesPanelIsVisible = value;
                OnPropertyChanged(PropertiesPanelIsVisibleProperty);
            }
        }

        /// <summary>
        /// Property definition for the selected page orientation
        /// </summary>
        public ReportPaperOrientation SelectedPageOrientation
        {
            get
            {
                if (_selectedReport != null)
                {
                    return _selectedReport.PaperOrientation;
                }
                else
                {
                    return ReportPaperOrientationHelper.DefaultOrientation();
                }
            }
            set
            {
                if (_selectedReport == null)
                {
                    return;
                }

                _selectedReport.PaperOrientation = value;
                OnPropertyChanged(ReportPageOrientationProperty);
            }
        }

        /// <summary>
        /// Property definition for the selected page margin
        /// </summary>
        public ReportPaperMargin SelectedPaperMargin
        {
            get
            {
                if (_selectedReport != null)
                {
                    return _selectedReport.PaperMarginType;
                }
                else
                {
                    return ReportPaperMarginHelper.DefaultMargin();
                }
            }
            set
            {
                if (_selectedReport == null)
                {
                    return;
                }

                _selectedReport.PaperMarginType = value;
                OnPropertyChanged(ReportPaperMarginProperty);
            }
        }

        /// <summary>
        /// Property definition for the selected paper size
        /// </summary>
        public ReportPaperSize SelectedPaperSize
        {
            get 
            {
                if (_selectedReport != null)
                {
                    return _selectedReport.PaperSize;
                }
                else
                {
                    return ReportPaperSizeHelper.DefaultSize();
                }
            }
            set
            {
                if (_selectedReport == null)
                {
                    return;
                }

                _selectedReport.PaperSize = value;
                OnPropertyChanged(ReportPaperSizeProperty);
            }
        }

		/// <summary>
		/// Property for the Show Grid option - linked to the "Arrange" toggle buttons 
		/// on the arrange tab.
		/// </summary>
		public Boolean ShowGrid
		{
			get
			{
				if (_selectedReport == null) return false;
				return _selectedReport.ShowGrid;
			}
			set
			{
				if (_selectedReport == null) return;
				_selectedReport.ShowGrid = value;
				OnPropertyChanged(ShowGridProperty);
			}
		}

		/// <summary>
		/// Property for the Snap To Grid option - linked to the "Arrange" toggle buttons 
		/// on the arrange tab.
		/// </summary>
		public Boolean SnapToGrid
		{
			get
			{
				if (_selectedReport == null) return false;
				return _selectedReport.SnapToGrid;
			}
			set
			{
				if (_selectedReport == null) return;
				_selectedReport.SnapToGrid = value;
				OnPropertyChanged(SnapToGridProperty);
			}
		}

		/// <summary>
		/// Property for the GridColour option - linked to the "Arrange" toggle buttons 
		/// on the arrange tab.
		/// </summary>
		public System.Windows.Media.Color GridColour
		{
			get
			{
				if (_selectedReport == null) return System.Windows.Media.Colors.DarkGray;
				return ReportHelper.IntToColor(_selectedReport.GridColour);
			}
			set
			{
				if (_selectedReport == null) return;
				_selectedReport.GridColour = ReportHelper.ColorToInt(value);
				OnPropertyChanged(GridColourProperty);
			}
		}

		/// <summary>
		/// Property for the Grid Spacing option - linked to the "Arrange" toggle buttons 
		/// on the arrange tab.
		/// </summary>
		public Double GridSpacing
		{
			get
			{
				if (_selectedReport == null) return 1F;
				return Reporting.Controls.Wpf.Common.Helpers.ToUnitConversion(_selectedReport.GridSpacing);
			}
			set
			{
				if (_selectedReport == null) return;
				_selectedReport.GridSpacing = Reporting.Controls.Wpf.Common.Helpers.ToPointConversion(value);
				OnPropertyChanged(GridSpacingProperty);
			}
		}

        /// <summary>
        /// Property of the report as to whether the DISTINCT clause is applied to a report query.
        /// </summary>
        public Boolean SelectDistinct
        {
            get { return _selectDistinct; }
            set
            {
                _selectDistinct = value;
                OnPropertyChanged(SelectDistinctProperty);
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
		public ReportSetupViewModel(Boolean showDebug)
			: this(1, showDebug)
        { }

        /// <summary>
        /// Testing constructor
        /// </summary>
		public ReportSetupViewModel(Int32 entityId, Boolean showDebug)
		{
			_showDebug = showDebug;

			//String dataModelsPath = @"C:\Dev\CCMV8\Solution\Galleria.Ccm.Dal.Mssql\Scripts\0009.0000\DataModels\";
            //Reporting.Model.DataModel.LoadDataModelFromFile(dataModelsPath + "Assortment Rules CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(dataModelsPath + "Location Space CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(dataModelsPath + "Planogram Assignment CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(dataModelsPath + "Planogram CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(dataModelsPath + "Planogram Performance CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(dataModelsPath + "Planogram Products CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(dataModelsPath + "Product Distribution Overview CCM V8 Data Model.xml");
            //Reporting.Model.DataModel.LoadDataModelFromFile(dataModelsPath + "Workpackage Summary CCM V8 Data Model.xml");

            //Get existing reports
            FetchAllReportInfos();
        }

        #endregion

        #region Commands

        #region New, Open, Save, Delete Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new report
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute())
                    {
                        FriendlyName = Message.Report_NewCommand,
                        FriendlyDescription = Message.Report_NewCommand_Description,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                SelectNewReportType();
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Opens assortment for the given id
        /// param: Int32 the content id to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        DisabledReason = Message.Report_OpenCommand_DisabledReason
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? reportId)
        {
            if (reportId == null) return false;
            return true;
        }

        private void Open_Executed(Int32? reportId)
        {
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //Fetch the report
                    Report currentReport = Report.FetchById(reportId.Value);

                    base.ShowWaitCursor(false);

                    //validate and if ok open the report
                    if (Galleria.Reporting.Controls.Wpf.Common.Helpers.ValidateReport(this.AttachedControl, currentReport))
                    {
                        base.ShowWaitCursor(true);

                        this.SelectedReport = currentReport;

						if (_showDebug)
						{
							this.SelectedReport.DumpDebugReport();
						}
                    }
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(_exCategory, OperationType.Open);
                }

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current report
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Report_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            _saveCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _saveCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            if (!this.SelectedReport.IsDirty)
            {
                _saveCommand.DisabledReason = Message.ReportSetup_ReportNotChanged; 
                return false;
            }
			if (!this.SelectedReport.IsSuppressDetailAllowed)
			{
				if (this.SelectedReport.SuppressDetail == true)
				{
					_saveCommand.DisabledReason = Reporting.Resources.Language.Message.Report_BusinessRule_IsSuppressDetailNotAllowedMessage;
					return false;
				}
			}
			if (!this.SelectedReport.IsValid)
			{
				Csla.Rules.BrokenRulesCollection brokenRules = this.SelectedReport.GetAllBrokenRules();
				if (brokenRules.Count > 0)
				{
					_saveCommand.DisabledReason = brokenRules[0].Description;
					return false;
				}
				_saveCommand.DisabledReason = Message.ReportSetup_ReportNotValid;
				return false;
			}
            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        private Boolean SaveCurrentItem()
        {
            //** check the item unique value
            String newName;
            Object reportId = this.SelectedReport.Id;

            IEnumerable<String> takenReportNames =
                _reportInfos.Where(c => !c.Id.Equals(reportId)).Select(c => c.Name);

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                nameAccepted =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(
                        s => !takenReportNames.Contains(s), this.SelectedReport.Name, out newName);
            }
            else
            {
                //force a unique value for unit testing
                newName =
                    Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(
                        this.SelectedReport.Name, takenReportNames);
            }

            //set the name
            if (nameAccepted)
            {
                if (this.SelectedReport.Name != newName)
                {
                    this.SelectedReport.Name = newName;
                }
            }
            else
            {
                return false; // Do not continue with the save.
            }
         
            //** save the item
            ShowWaitCursor(true);

            Boolean success = TrySave();

            //update the info list
            FetchAllReportInfos();

            ShowWaitCursor(false);

            return success;
        }

        /// <summary>
        /// Try saving the <see cref="SelectedReport"/> handling errors if any.
        /// </summary>
        /// <returns></returns>
        private Boolean TrySave()
        {
            var success = true;
            try
            {
                SelectedReport = SelectedReport.Save();
            }
            catch (DataPortalException ex)

            {
                ShowWaitCursor(false);

                success = false;

                LocalHelper.RecordException(ex, _exCategory);
                Exception rootException = ex.GetBaseException();

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired =
                        Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedReport.Name);
                    if (itemReloadRequired)
                    {
                        SelectedReport = Report.FetchById(this.SelectedReport.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(_exCategory, OperationType.Save);
                }

                ShowWaitCursor(true);
            }
            catch (InvalidOperationException ex)
            {
                ShowWaitCursor(false);
                success = false;
                LocalHelper.RecordException(ex, _exCategory);
                Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(_exCategory, OperationType.Save);
                ShowWaitCursor(true);
            }
            return success;
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Saves the current report and creates a new report
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => SaveAndNew_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Report_SaveAndNew_Tooltip,
                        Icon = ImageResources.SaveAndNew_16,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAndNew_CanExecute()
        {
            _saveAndNewCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _saveAndNewCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            if (!this.SelectedReport.IsDirty)
            {
                _saveAndNewCommand.DisabledReason = Message.ReportSetup_ReportNotChanged;
                return false;
            }
			if (!this.SelectedReport.IsSuppressDetailAllowed)
			{
				if (this.SelectedReport.SuppressDetail == true)
				{
					_saveAndNewCommand.DisabledReason = Reporting.Resources.Language.Message.Report_BusinessRule_IsSuppressDetailNotAllowedMessage;
					return false;
				}
			}
			if (!this.SelectedReport.IsValid)
			{
				Csla.Rules.BrokenRulesCollection brokenRules = this.SelectedReport.GetAllBrokenRules();
				if (brokenRules.Count > 0)
				{
					_saveAndNewCommand.DisabledReason = brokenRules[0].Description;
					return false;
				}
				_saveAndNewCommand.DisabledReason = Message.ReportSetup_ReportNotValid;
				return false;
			}
            return true;
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current report
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => SaveAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Report_SaveAndClose_Tooltip,
                        Icon = ImageResources.SaveAndClose_32,
                        SmallIcon = ImageResources.Save_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAndClose_CanExecute()
        {
            _saveAndCloseCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _saveAndCloseCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            if (!this.SelectedReport.IsDirty)
            {
                _saveAndCloseCommand.DisabledReason = Message.ReportSetup_ReportNotChanged;
                return false;
            }
			if (!this.SelectedReport.IsSuppressDetailAllowed)
			{
				if (this.SelectedReport.SuppressDetail == true)
				{
					_saveAndCloseCommand.DisabledReason = Reporting.Resources.Language.Message.Report_BusinessRule_IsSuppressDetailNotAllowedMessage;
					return false;
				}
			}
			if (!this.SelectedReport.IsValid)
			{
				Csla.Rules.BrokenRulesCollection brokenRules = this.SelectedReport.GetAllBrokenRules();
				if (brokenRules.Count > 0)
				{
					_saveAndCloseCommand.DisabledReason = brokenRules[0].Description;
					return false;
				}
				_saveAndCloseCommand.DisabledReason = Message.ReportSetup_ReportNotValid;
				return false;
			}
            return true;
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current report
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Report_SaveAs_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            _saveAsCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _saveAsCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
			if (!this.SelectedReport.IsSuppressDetailAllowed)
			{
				if (this.SelectedReport.SuppressDetail == true)
				{
					_saveAsCommand.DisabledReason = Reporting.Resources.Language.Message.Report_BusinessRule_IsSuppressDetailNotAllowedMessage;
					return false;
				}
			}
			if (!this.SelectedReport.IsValid)
			{
				Csla.Rules.BrokenRulesCollection brokenRules = this.SelectedReport.GetAllBrokenRules();
				if (brokenRules.Count > 0)
				{
					_saveAsCommand.DisabledReason = brokenRules[0].Description;
					return false;
				}
				_saveAsCommand.DisabledReason = Message.ReportSetup_ReportNotValid;
				return false;
			}
            return true;
        }

        private void SaveAs_Executed()
        {
            String selectedName;
            Boolean nameIsUnique = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                    (s) =>
                    {
                        IEnumerable<ReportInfo> matchingNames =
                            _reportInfos.Where(p => p.Name == s);
                        return !(matchingNames.Any());
                    };

                nameIsUnique =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(
                        isUniqueCheck, this.SelectedReport.Name, out selectedName);
            }
            else
            {
                selectedName =
                    Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(
                        this.SelectedReport.Name, _reportInfos.Select(p => p.Name));
            }

            if (nameIsUnique)
            {
                base.ShowWaitCursor(true);

                Report itemCopy = this.SelectedReport.Copy();
                itemCopy.Name = selectedName;
                itemCopy = itemCopy.Save();

                this.SelectedReport = itemCopy;

                FetchAllReportInfos();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region ExportToFileCommand

        private RelayCommand _exportToFileCommand;

        /// <summary>
        /// Saves the current report to a file
        /// </summary>
        public RelayCommand ExportToFileCommand
        {
            get
            {
                if (_exportToFileCommand == null)
                {
                    _exportToFileCommand = new RelayCommand(
                        p => ExportToFile_Executed(),
                        p => ExportToFile_CanExecute())
                    {
                        FriendlyName = Message.Report_ExportToFileCommand,
                        FriendlyDescription = Message.Report_ExportToFileCommand_Tooltip,
                        Icon = ImageResources.ExportReportToFile_32,
                        SmallIcon = ImageResources.ExportReportToFile_16
                    };
                    base.ViewModelCommands.Add(_exportToFileCommand);
                }
                return _exportToFileCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ExportToFile_CanExecute()
        {
            _exportToFileCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _exportToFileCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
			if (!this.SelectedReport.IsSuppressDetailAllowed)
			{
				if (this.SelectedReport.SuppressDetail == true)
				{
					_exportToFileCommand.DisabledReason = Reporting.Resources.Language.Message.Report_BusinessRule_IsSuppressDetailNotAllowedMessage;
					return false;
				}
			}
			if (!this.SelectedReport.IsValid)
			{
				Csla.Rules.BrokenRulesCollection brokenRules = this.SelectedReport.GetAllBrokenRules();
				if (brokenRules.Count > 0)
				{
					_exportToFileCommand.DisabledReason = brokenRules[0].Description;
					return false;
				}
				_exportToFileCommand.DisabledReason = Message.ReportSetup_ReportNotValid;
				return false;
			}
            return true;
        }

        private void ExportToFile_Executed()
        {
            String saveAsPath = null;

            Window parentWindow = AttachedControl;

            if (parentWindow != null)
            {
                //Show the dialog so that the user can pick a new filepath.
                SaveFileDialog dlg = new SaveFileDialog
                {
                    Filter = String.Format(Message.Report_GalleriaReportFileFilter, Galleria.Reporting.Model.Report.FileExtension),    //Message.Generic_ReportFileDialogFilter
                    FileName = this.SelectedReport.Name,
                    AddExtension = true,
                    DefaultExt = Galleria.Reporting.Model.Report.FileExtension,
                    OverwritePrompt = true
                };

                if (dlg.ShowDialog(parentWindow) == true)
                {
                    saveAsPath = dlg.FileName;
                }

                if (!String.IsNullOrEmpty(saveAsPath))
                {
                    base.ShowWaitCursor(true);

                    //Check if file is read-only
                    if (File.Exists(saveAsPath))
                    {
                        if (File.GetAttributes(saveAsPath).HasFlag(FileAttributes.ReadOnly))
                        {
                            base.ShowWaitCursor(false);
                            MessageBox.Show(Message.Report_ExportToFile_FileExistsMessage);
                            return;
                        }
                    }

                    //Save the file
                    try
                    {
                        Report reportToSave = this.SelectedReport.Clone();
                        reportToSave.SaveAsFile(saveAsPath);
                    }
                    catch (Exception ex)
                    {
                        base.ShowWaitCursor(false);

                        Exception rootException = ex.GetBaseException();
                        //LocalHelper.RecordException(rootException);

                        //For now show error
                        if (rootException is System.IO.IOException)
                        {
                            MessageBox.Show(ex.Message);
                        }

                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                    }

                    base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #region ImportFromFileCommand

        private RelayCommand _importFromFileCommand;

        /// <summary>
        /// Imports a report from a file
        /// </summary>
        public RelayCommand ImportFromFileCommand
        {
            get
            {
                if (_importFromFileCommand == null)
                {
                    _importFromFileCommand = new RelayCommand(
                        p => ImportFromFile_Executed(),
                        p => ImportFromFile_CanExecute())
                    {
                        FriendlyName = Message.Report_ImportFromFileCommand,
                        FriendlyDescription = Message.Report_ImportFromFileCommand_Tooltip,
                        Icon = ImageResources.ReportImportFromFile_48
                    };
                    base.ViewModelCommands.Add(_importFromFileCommand);
                }
                return _importFromFileCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ImportFromFile_CanExecute()
        {
            _importFromFileCommand.DisabledReason = String.Empty;
            return true;
        }

        private void ImportFromFile_Executed()
        {
			String importFilePath = null;
			Boolean canContinue = true;
			Window parentWindow = AttachedControl;
			Report currentReport = null;

            if (parentWindow != null)
            {
                //Show the dialog so that the user can pick a file
                OpenFileDialog dlg = new OpenFileDialog
                {
                    Filter = String.Format(Message.Report_GalleriaReportFileFilter, Galleria.Reporting.Model.Report.FileExtension),    //Message.Generic_ReportFileDialogFilter
                    AddExtension = true,
                    DefaultExt = Galleria.Reporting.Model.Report.FileExtension
                };

                if (dlg.ShowDialog(parentWindow) == true)
                {
                    importFilePath = dlg.FileName;
                }

                if (!String.IsNullOrEmpty(importFilePath))
                {
                    base.ShowWaitCursor(true);

                    //Import the file
                    try
                    {
						//Import the file
						currentReport = Report.FetchByFileName(importFilePath);

						canContinue = (currentReport != null);
                    }
                    catch (Exception ex)
                    {
                        base.ShowWaitCursor(false);

						canContinue = false;

                        Exception rootException = ex.GetBaseException();
                        LocalHelper.RecordException(rootException);

                        //For now show error
                        if (rootException is System.IO.IOException)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        //Show error if data model is not in current application
                        if (rootException is Reporting.Security.DataModelDoesNotExistException)
                        {
                            Reporting.Controls.Wpf.Common.Helpers.ShowDataModelDoesNotExistExceptionErrorMessage(
                                this.AttachedControl, rootException.Message);

                            return;
                        }

                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                    }

					if (canContinue)
					{
						//validate and if ok open the report
						if (Galleria.Reporting.Controls.Wpf.Common.Helpers.ValidateReport(this.AttachedControl, currentReport))
						{
							base.ShowWaitCursor(true);

							this.SelectedReport = currentReport;

							if (_showDebug)
							{
								this.SelectedReport.DumpDebugReport();
							}
						}
					}

                    base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Saves the current report
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(), 
                        p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.ReportSetup_Delete_Tooltip,
                        Icon = ImageResources.Delete_32,
                        SmallIcon = ImageResources.Delete_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.D
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            _deleteCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _deleteCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                //confirm the delete with the user
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedReport.Name);
            }

            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                Report itemToDelete = this.SelectedReport;
                itemToDelete.Delete();
                //load a new item
                this.SelectedReport = null;
                if (this.AttachedControl != null)
                {
                    LocalHelper.SetRibbonBackstageState(this.AttachedControl, true);
                }

                //commit the delete
                try
                {
                    itemToDelete.Save();
                    this.SelectedReport = null;
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                        _exCategory, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                FetchAllReportInfos();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region DeleteBackstageSelectedReportCommand

        private RelayCommand<ReportInfo> _deleteBackstageSelectedReportCommand;

        /// <summary>
        /// deletes the report selected in the backstage open screen
        /// </summary>
        public RelayCommand<ReportInfo> DeleteBackstageSelectedReportCommand
        {
            get
            {
                if (_deleteBackstageSelectedReportCommand == null)
                {
                    _deleteBackstageSelectedReportCommand = new RelayCommand<ReportInfo>(
                        p => DeleteBackstageSelectedReport_Executed(p),
                        p => DeleteBackstageSelectedReport_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.ReportSetup_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_deleteBackstageSelectedReportCommand);
                }
                return _deleteBackstageSelectedReportCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean DeleteBackstageSelectedReport_CanExecute(ReportInfo reportToDelete)
        {
            _deleteBackstageSelectedReportCommand.DisabledReason = String.Empty;
            if (reportToDelete == null)
            {
                _deleteBackstageSelectedReportCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }

            if (this.SelectedReport != null)
            {
                if (reportToDelete.Id.Equals(this.SelectedReport.Id))
                {
					_deleteBackstageSelectedReportCommand.DisabledReason = Message.ReportSetup_CannotDeleteSelectedReport;
					return false;
                }
            }

            return true;
        }

        private void DeleteBackstageSelectedReport_Executed(ReportInfo reportToDelete)
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                //confirm the delete with the user
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(reportToDelete.Name);
            }

            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                Report itemToDelete = Report.FetchById(reportToDelete.Id);
                itemToDelete.Delete();

                if (this.AttachedControl != null)
                {
                    LocalHelper.SetRibbonBackstageState(this.AttachedControl, true);
                }

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                        String.Empty, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                FetchAllReportInfos();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #endregion

        #region PrintPreviewAsPdfInExternalViewerCommand

        private RelayCommand _printPreviewAsPdfInExternalViewerCommand;

        /// <summary>
        /// Shows a print preview using the current report
        /// </summary>
        public RelayCommand PrintPreviewAsPdfInExternalViewerCommand
        {
            get
            {
                if (_printPreviewAsPdfInExternalViewerCommand == null)
                {
                    _printPreviewAsPdfInExternalViewerCommand = new RelayCommand(
                        p => PrintPreviewAsPdfInExternalViewer_Executed(),
                        p => PrintPreviewAsPdfInExternalViewer_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_PrintPreview_Pdf,
                        FriendlyDescription = Message.ReportSetup_PrintPreviewViewPdfInExternalViewerCommand_Description,
                        Icon = ImageResources.PrintPreviewPdf_32
                    };
                    base.ViewModelCommands.Add(_printPreviewAsPdfInExternalViewerCommand);
                }
                return _printPreviewAsPdfInExternalViewerCommand;
            }
        }

        private Boolean PrintPreviewAsPdfInExternalViewer_CanExecute()
        {
            _printPreviewAsPdfInExternalViewerCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _printPreviewAsPdfInExternalViewerCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            if (!this.SelectedReport.SelectedFields.Any())
            {
                _printPreviewAsPdfInExternalViewerCommand.DisabledReason = Message.ReportSetup_ReportHasNoFields;
                return false;
            }
			if (!this.SelectedReport.IsSuppressDetailAllowed)
			{
				if (this.SelectedReport.SuppressDetail == true)
				{
					_printPreviewAsPdfInExternalViewerCommand.DisabledReason = Reporting.Resources.Language.Message.Report_BusinessRule_IsSuppressDetailNotAllowedMessage;
					return false;
				}
			}
			if (!this.SelectedReport.IsValid)
			{
				Csla.Rules.BrokenRulesCollection brokenRules = this.SelectedReport.GetAllBrokenRules();
				if (brokenRules.Count > 0)
				{
					_printPreviewAsPdfInExternalViewerCommand.DisabledReason = brokenRules[0].Description;
					return false;
				}
				_printPreviewAsPdfInExternalViewerCommand.DisabledReason = Message.ReportSetup_ReportNotValid;
				return false;
			}
            return true;
        }

        private void PrintPreviewAsPdfInExternalViewer_Executed()
        {
            //Build the report
            CreatePdf();
        }
        #endregion

        #region PrintPreviewAsGridCommand

        private RelayCommand _printPreviewAsGridCommand;

        /// <summary>
        /// Shows a print preview using the current report
        /// </summary>
        public RelayCommand PrintPreviewAsGridCommand
        {
            get
            {
                if (_printPreviewAsGridCommand == null)
                {
                    _printPreviewAsGridCommand = new RelayCommand(
                        p => PrintPreviewAsGrid_Executed(), 
                        p => PrintPreviewAsGrid_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_PrintPreview_Grid,
                        FriendlyDescription = Message.ReportSetup_PrintPreviewAsGrid_Description,
                        Icon = ImageResources.PrintPreviewGrid_32
                    };
                    base.ViewModelCommands.Add(_printPreviewAsGridCommand);
                }
                return _printPreviewAsGridCommand;
            }
        }

        private bool PrintPreviewAsGrid_CanExecute()
        {
            _printPreviewAsGridCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _printPreviewAsGridCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            if (!this.SelectedReport.SelectedFields.Any())
            {
                _printPreviewAsGridCommand.DisabledReason = Message.ReportSetup_ReportHasNoFields;
                return false;
            }
			if (!this.SelectedReport.IsSuppressDetailAllowed)
			{
				if (this.SelectedReport.SuppressDetail == true)
				{
					_printPreviewAsGridCommand.DisabledReason = Reporting.Resources.Language.Message.Report_BusinessRule_IsSuppressDetailNotAllowedMessage;
					return false;
				}
			}
			if (!this.SelectedReport.IsValid)
			{
				Csla.Rules.BrokenRulesCollection brokenRules = this.SelectedReport.GetAllBrokenRules();
				if (brokenRules.Count > 0)
				{
					_printPreviewAsGridCommand.DisabledReason = brokenRules[0].Description;
					return false;
				}
				_printPreviewAsGridCommand.DisabledReason = Message.ReportSetup_ReportNotValid;
				return false;
			}
            return true;
        }

        private void PrintPreviewAsGrid_Executed()
        {
            #region Parameters
            Boolean parametersSelected = 
                Galleria.Reporting.Controls.Wpf.Common.Helpers.PromptParameterValues(SelectedReport);
            if (!parametersSelected)
            {
                // Show a cancel message to the user
                ShowCancelWindow();

                return;
            }
            #endregion

			base.ShowWaitCursor(true);
            
            //Create the viewer view model
            ReportGridPreviewViewModel previewVM = new ReportGridPreviewViewModel(this.SelectedReport);
            //create the pdf and display it
            previewVM.BuildDataSet();

            if (previewVM.Results != null)
            {
                Boolean canContinue = true;

                //check if results row count is large
                if (previewVM.Results.Rows.Count > ReportHelper.MaxRowCountWithoutWarningUser)
                {
					base.ShowWaitCursor(false);
					
					canContinue = ShowLargeDataRowWarningDialog(previewVM.Results.Rows.Count);

					base.ShowWaitCursor(true);
                }

                if (canContinue)
                {
                    ReportGridPreviewWindow win = new ReportGridPreviewWindow(previewVM);
                    if (this.AttachedControl != null)
                    {
						base.ShowWaitCursor(false); 
						
						App.ShowWindow(win, this.AttachedControl, true);
                    }
                }
            }

			base.ShowWaitCursor(false);
        }
        #endregion

        #region ReportInfoCommand

        private RelayCommand _reportInfoCommand;

        /// <summary>
        /// Shows an information window for the current report
        /// </summary>
        public RelayCommand ReportInfoCommand
        {
            get
            {
                if (_reportInfoCommand == null)
                {
                    _reportInfoCommand = new RelayCommand(
                        p => ReportInfo_Executed(),
                        p => ReportInfo_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_ReportInfo,
                        FriendlyDescription = Message.ReportSetup_ReportInfo_Description,
                        Icon = ImageResources.ReportInfo_32
                    };
                    base.ViewModelCommands.Add(_reportInfoCommand);
                }
                return _reportInfoCommand;
            }
        }

        [DebuggerStepThrough]
        private bool ReportInfo_CanExecute()
        {
            _reportInfoCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _reportInfoCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void ReportInfo_Executed()
        {
			base.ShowWaitCursor(true);
			
            // Show report details tab by default
            ReportInfoEditorViewModel vm = new ReportInfoEditorViewModel(this.SelectedReport, true);
            ReportInfoEditorWindow win = new ReportInfoEditorWindow(vm);
            if (this.AttachedControl != null)
            {
				base.ShowWaitCursor(false);
				
				App.ShowWindow(win, this.AttachedControl, true);
            }

			base.ShowWaitCursor(false);
        }

        #endregion

        #region ReportAttachTemplateCommand

        private RelayCommand _reportAttachTemplateCommand;

        /// <summary>
        /// 
        /// </summary>
        public RelayCommand ReportAttachTemplateCommand
        {
            get
            {
                if (_reportAttachTemplateCommand == null)
                {
                    _reportAttachTemplateCommand = new RelayCommand(
                        p => ReportAttachTemplateCommand_Executed(),
                        p => ReportAttachTemplateCommand_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_ReportAttachExcelTemplate,
                        FriendlyDescription = Message.ReportSetup_ReportTemplate_Description,
                        Icon = ImageResources.ReportAttachTemplate_32,
                    };
                    base.ViewModelCommands.Add(_reportAttachTemplateCommand);
                }
                return _reportAttachTemplateCommand;
            }
        }

        private bool ReportAttachTemplateCommand_CanExecute()
        {
            bool enableCommand = true;
            String disabledReason = String.Empty;

            if (this.SelectedReport == null)
            {
                disabledReason = Message.ReportSetup_NoReportSelected;
                enableCommand = false;
            }

            _reportAttachTemplateCommand.DisabledReason = disabledReason;

            return enableCommand;
        }

        private void ReportAttachTemplateCommand_Executed()
        {
            base.ShowWaitCursor(true);

            ReportInfoEditorViewModel vm = new ReportInfoEditorViewModel(this.SelectedReport, false);
            ReportInfoEditorWindow win = new ReportInfoEditorWindow(vm);
            if (this.AttachedControl != null)
            {
                base.ShowWaitCursor(false);

                App.ShowWindow(win, this.AttachedControl, true);
            }

            base.ShowWaitCursor(false);
        }

        #endregion
				
        #region Page Setup Commands

        #region ReportPortraitCommand

        private RelayCommand _reportPortraitCommand;

        /// <summary>
        /// Shows an information window for the current report
        /// </summary>
        public RelayCommand ReportPortraitCommand
        {
            get
            {
                if (_reportPortraitCommand == null)
                {
                    _reportPortraitCommand = new RelayCommand(
                        p => ReportPortrait_Executed(),
                        p => ReportPortrait_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_Portrait,
                        FriendlyDescription = Message.ReportSetup_Portrait_Description,
                        Icon = ImageResources.PageOrientation_Portrait_32
                    };
                    base.ViewModelCommands.Add(_reportPortraitCommand);
                }
                return _reportPortraitCommand;
            }
        }

        private bool ReportPortrait_CanExecute()
        {
            _reportPortraitCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _reportPortraitCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void ReportPortrait_Executed()
        {
            SelectedPageOrientation = ReportPaperOrientation.Portrait;
        }

        #endregion

        #region ReportLandscapeCommand

        private RelayCommand _reportLandscapeCommand;

        /// <summary>
        /// Shows an information window for the current report
        /// </summary>
        public RelayCommand ReportLandscapeCommand
        {
            get
            {
                if (_reportLandscapeCommand == null)
                {
                    _reportLandscapeCommand = new RelayCommand(
                        p => ReportLandscape_Executed(),
                        p => ReportLandscape_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_Landscape,
                        FriendlyDescription = Message.ReportSetup_Landscape_Description,
                        Icon = ImageResources.PageOrientation_Landscape_32
                    };
                    base.ViewModelCommands.Add(_reportLandscapeCommand);
                }
                return _reportLandscapeCommand;
            }
        }

        private bool ReportLandscape_CanExecute()
        {
            _reportLandscapeCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _reportLandscapeCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void ReportLandscape_Executed()
        {
            SelectedPageOrientation = ReportPaperOrientation.Landscape;
        }

        #endregion

        #region ReportPaperSizeCommand

        private RelayCommand _reportSetupPaperSizeCommand;

        /// <summary>
        /// This is a dummy command to allow the disabling of the Paper Size drop down menu
        /// </summary>
        public RelayCommand ReportSetupPaperSizeCommand
        {
            get
            {
                if (_reportSetupPaperSizeCommand == null)
                {
                    _reportSetupPaperSizeCommand = new RelayCommand(
                        p => ReportSetupPaperSizeCommand_Executed(),
                        p => ReportSetupPaperSizeCommand_CanExecute())
                    {
                    };
                    base.ViewModelCommands.Add(_reportSetupPaperSizeCommand);
                }
                return _reportSetupPaperSizeCommand;
            }
        }

        private bool ReportSetupPaperSizeCommand_CanExecute()
        {
            return (this.SelectedReport != null);
        }

        private void ReportSetupPaperSizeCommand_Executed()
        {
        }

        #endregion

        #region ReportSetupMarginCommand

        private RelayCommand _reportSetupMarginCommand;

        /// <summary>
        /// This is a dummy command to allow the disabling of the page margin drop down menu
        /// </summary>
        public RelayCommand ReportSetupMarginCommand
        {
            get
            {
                if (_reportSetupMarginCommand == null)
                {
                    _reportSetupMarginCommand = new RelayCommand(
                        p => ReportSetupMarginCommand_Executed(),
                        p => ReportSetupMarginCommand_CanExecute())
                    {
                    };
                    base.ViewModelCommands.Add(_reportSetupMarginCommand);
                }
                return _reportSetupMarginCommand;
            }
        }

        private bool ReportSetupMarginCommand_CanExecute()
        {
			return (this.SelectedReport != null);
        }

        private void ReportSetupMarginCommand_Executed()
        {
        }

        #endregion

        #endregion

        #region Data Commands

        #region EditGroupsCommand

        private RelayCommand _editGroupsCommand;

        /// <summary>
        /// Edits the groups in the current report
        /// </summary>
        public RelayCommand EditGroupsCommand
        {
            get
            {
                if (_editGroupsCommand == null)
                {
                    _editGroupsCommand = new RelayCommand(
                        p => EditGroups_Executed(), p => EditGroups_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_EditGroupsCommand_Text,
						FriendlyDescription = Message.ReportSetup_EditGroupsCommand_Description,
                        Icon = ImageResources.ReportGroup_32
                    };
                    base.ViewModelCommands.Add(_editGroupsCommand);
                }
                return _editGroupsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean EditGroups_CanExecute()
        {
            _editGroupsCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _editGroupsCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void EditGroups_Executed()
        {
            ReportGroupEditorViewModel vm = new ReportGroupEditorViewModel(this.SelectedReport);
            ReportGroupEditorWindow win = new ReportGroupEditorWindow(vm);
            if (this.AttachedControl != null)
            {
                App.ShowWindow(win, this.AttachedControl, true);
            }
        }

        #endregion

        #region EditFiltersCommand

        private RelayCommand _editFiltersCommand;

        /// <summary>
        /// Edits the filters in the current report
        /// </summary>
        public RelayCommand EditFiltersCommand
        {
            get
            {
                if (_editFiltersCommand == null)
                {
                    _editFiltersCommand = new RelayCommand(
                        p => EditFiltersCommand_Executed(),
                        p => EditFiltersCommand_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_EditFiltersCommand_Text,
                        FriendlyDescription = Message.ReportSetup_EditFiltersCommand_Description,
                        Icon = ImageResources.Filter_32
                    };
                    base.ViewModelCommands.Add(_editFiltersCommand);
                }
                return _editFiltersCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean EditFiltersCommand_CanExecute()
        {
            return (this.SelectedReport != null);
        }

        private void EditFiltersCommand_Executed()
        {
			if (this.SelectedReport == null) return;

			base.ShowWaitCursor(true);

            ReportSetupFilterEditorViewModel vm = new ReportSetupFilterEditorViewModel(this.SelectedReport);
            ReportSetupFilterEditorWindow win = new ReportSetupFilterEditorWindow(vm);

			base.ShowWaitCursor(false);
            if (this.AttachedControl != null)
            {
                App.ShowWindow(win, this.AttachedControl, true);
            }

			if (_showDebug)
			{
				this.SelectedReport.DumpDebugReport();
			}
        }

        #endregion

        #region EditSortingCommand

        private RelayCommand _editSortingCommand;

        /// <summary>
        /// Edits the filters in the current report
        /// </summary>
        public RelayCommand EditSortingCommand
        {
            get
            {
                if (_editSortingCommand == null)
                {
                    _editSortingCommand = new RelayCommand(
                        p => EditSortingCommand_Executed(),
                        p => EditSortingCommand_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_EditSortingCommand_Text,
                        FriendlyDescription = Message.ReportSetup_EditSortingCommand_Description,
                        Icon = ImageResources.ReportFieldSort_32
                    };
                    base.ViewModelCommands.Add(_editSortingCommand);
                }
                return _editSortingCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean EditSortingCommand_CanExecute()
        {
            return (this.SelectedReport != null);
        }

        private void EditSortingCommand_Executed()
        {
            if (this.SelectedReport == null)
            {
                return;
            }

            ReportSetupSortingEditorViewModel vm = new ReportSetupSortingEditorViewModel(this.SelectedReport);
            ReportSetupSortingEditorWindow win = new ReportSetupSortingEditorWindow(vm);

            if (this.AttachedControl != null)
            {
                App.ShowWindow(win, this.AttachedControl, true);
            }
        }

        #endregion

        #region EditParametersCommand

        private RelayCommand _editParametersCommand;

        /// <summary>
        /// Edits the filters in the current report
        /// </summary>
        public RelayCommand EditParametersCommand
        {
            get
            {
                if (_editParametersCommand == null)
                {
                    _editParametersCommand = new RelayCommand(
                        p => EditParametersCommand_Executed(),
                        p => EditParametersCommand_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_EditParametersCommand_Text,
                        FriendlyDescription = Message.ReportSetup_EditParametersCommand_Description,
                        Icon = ImageResources.ReportParameters_32
                    };
                    base.ViewModelCommands.Add(_editParametersCommand);
                }
                return _editParametersCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean EditParametersCommand_CanExecute()
        {
            return (this.SelectedReport != null);
        }

        private void EditParametersCommand_Executed()
        {
            if (this.SelectedReport == null)
            {
                return;
            }

            ReportParametersEditorViewModel vm = new ReportParametersEditorViewModel(this.SelectedReport, null);
            ParameterEditorControlWindow win = new ParameterEditorControlWindow(vm);

            if (this.AttachedControl != null)
            {
                App.ShowWindow(win, this.AttachedControl, true);
            }
        }

        #endregion

        #endregion

        #region Tools Commands
        // The component toggle buttons are mutually execlusive i.e. only one can be selected at a time

        #region ReportSelectComponentCommand

        private RelayCommand _reportSelectCommand;

        /// <summary>
        /// Shows an information window for the current report
        /// </summary>
        public RelayCommand ReportSelectComponentCommand
        {
            get
            {
                if (_reportSelectCommand == null)
                {
                    _reportSelectCommand = new RelayCommand(
                        p => ReportSelect_Executed(),
                        p => ReportSelect_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_SelectInfo,
                        FriendlyDescription = Message.ReportSetup_SelectInfo_Description,
                        Icon = ImageResources.Report_SelectComponent_32
                    };
                    base.ViewModelCommands.Add(_reportSelectCommand);
                }
                return _reportSelectCommand;
            }
        }

        private bool ReportSelect_CanExecute()
        {
            _reportSelectCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _reportSelectCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void ReportSelect_Executed()
        {
            SelectedReportComponent = ReportComponentType.None;
        }

        #endregion

        #region ReportAddPictureCommand

        private RelayCommand _reportAddPictureCommand;

        /// <summary>
        /// Shows an information window for the current report
        /// </summary>
        public RelayCommand ReportAddPictureCommand
        {
            get
            {
                if (_reportAddPictureCommand == null)
                {
                    _reportAddPictureCommand = new RelayCommand(
                        p => ReportAddPicture_Executed(),
                        p => ReportAddPicture_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_AddPicture,
                        FriendlyDescription = Message.ReportSetup_AddPicture_Description,
                        Icon = ImageResources.Picture_16
                    };
                    base.ViewModelCommands.Add(_reportAddPictureCommand);
                }
                return _reportAddPictureCommand;
            }
        }

        private bool ReportAddPicture_CanExecute()
        {
            _reportAddPictureCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _reportAddPictureCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void ReportAddPicture_Executed()
        {
            SelectedReportComponent = ReportComponentType.Image;
        }

        #endregion

        #endregion

        #region Formula Fields Commands

        #region AddFormulaFieldCommand

        private RelayCommand _addFormulaFieldCommand;

        /// <summary>
        /// Edits the groups in the current report
        /// </summary>
        public RelayCommand AddFormulaFieldCommand
        {
            get
            {
                if (_addFormulaFieldCommand == null)
                {
                    _addFormulaFieldCommand = new RelayCommand(
                        p => AddFormulaField_Executed(),
                        p => AddFormulaField_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_AddFormulaFieldCommand_Text,
                        FriendlyDescription = Message.ReportSetup_AddFormulaFieldCommand_Description,
                        Icon = ImageResources.FormulaField_32
                    };
                    base.ViewModelCommands.Add(_addFormulaFieldCommand);
                }
                return _addFormulaFieldCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddFormulaField_CanExecute()
        {
            _addFormulaFieldCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _addFormulaFieldCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void AddFormulaField_Executed()
        {
            if (this.SelectedReport == null)
            {
                return;
            }

            //load the editor window
            ReportFormulaFieldEditorViewModel vm =
                new ReportFormulaFieldEditorViewModel(this.SelectedReport, null);
            ReportFormulaFieldEditorWindow win = new ReportFormulaFieldEditorWindow(vm);
            if (this.AttachedControl != null)
            {
                App.ShowWindow(win, this.AttachedControl, true);
            }
        }

        #endregion

        #endregion

        #region Sub Totals Field Commands

        #region AddSubTotalFieldCommand

        private RelayCommand _addSubTotalFieldCommand;

        /// <summary>
        /// Edits the Sub totals in the current report
        /// </summary>
        public RelayCommand AddSubTotalFieldCommand
        {
            get
            {
                if (_addSubTotalFieldCommand == null)
                {
                    _addSubTotalFieldCommand = new RelayCommand(
                        p => AddSubTotalField_Executed(),
                        p => AddSubTotalField_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_AddSubTotalFieldCommand_Text,
                        FriendlyDescription = Message.ReportSetup_AddSubTotalFieldCommand_Description,
                        Icon = ImageResources.SubTotalField_32
                    };
                    base.ViewModelCommands.Add(_addSubTotalFieldCommand);
                }
                return _addSubTotalFieldCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddSubTotalField_CanExecute()
        {
            _addSubTotalFieldCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _addSubTotalFieldCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void AddSubTotalField_Executed()
        {
			if (this.SelectedReport == null) { return; }

			base.ShowWaitCursor(true);

            //create the viewmodel
            ReportSubTotalFieldEditorViewModel vm =
                new ReportSubTotalFieldEditorViewModel(this.SelectedReport, null);
            ReportSubTotalFieldEditorWindow win = new ReportSubTotalFieldEditorWindow(vm);

            if (this.AttachedControl != null)
            {
				base.ShowWaitCursor(false); 
				App.ShowWindow(win, this.AttachedControl, true);
            }

			base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Arrange Tab Commands

        #region ShowGridCommand

        private RelayCommand _showGridCommand;

        /// <summary>
        /// Shows a settings  window for the current grid
        /// </summary>
        public RelayCommand ShowGridCommand
        {
            get
            {
                if (_showGridCommand == null)
                {
                    _showGridCommand = new RelayCommand(
                        p => ShowGrid_Executed(),
                        p => ShowGrid_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_ShowGridCommand_Text,
                        FriendlyDescription = Message.ReportSetup_ShowGridCommand_Description,
                        Icon = ImageResources.ShowGrid_16
                    };
                    base.ViewModelCommands.Add(_showGridCommand);
                }
                return _showGridCommand;
            }
        }

        private bool ShowGrid_CanExecute()
        {
            _showGridCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _showGridCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void ShowGrid_Executed()
        {
            //Value change handled by xaml toggle button
        }

        #endregion

        #region SnapToGridCommand

        private RelayCommand _snapToGridCommand;

        /// <summary>
        /// Toggles whether new components should snap to the grid or not
        /// </summary>
        public RelayCommand SnapToGridCommand
        {
            get
            {
                if (_snapToGridCommand == null)
                {
                    _snapToGridCommand = new RelayCommand(
                        p => SnapToGrid_Executed(),
                        p => SnapToGrid_CanExecute())
                    {
                        FriendlyName = Message.ReportSetup_SnapToGridCommand_Text,
                        FriendlyDescription = Message.ReportSetup_SnapToGridCommand_Description,
                        Icon = ImageResources.SnapToGrid_16
                    };
                    base.ViewModelCommands.Add(_snapToGridCommand);
                }
                return _snapToGridCommand;
            }
        }

        private bool SnapToGrid_CanExecute()
        {
            _snapToGridCommand.DisabledReason = String.Empty;
            if (this.SelectedReport == null)
            {
                _snapToGridCommand.DisabledReason = Message.ReportSetup_NoReportSelected;
                return false;
            }
            return true;
        }

        private void SnapToGrid_Executed()
        {
            //Value change handled by xaml toggle button
        }

        #endregion

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of the selected Report
        /// </summary>
        private void OnSelectedReportChanged(Report oldReport, Report newReport)
        {
            if (oldReport != null)
            {
                oldReport.PropertyChanged -= SelectedReport_PropertyChanged;
            }

            if (newReport != null)
            {
                newReport.PropertyChanged += SelectedReport_PropertyChanged;
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.IsEnabled = false;
                    //pass flag to control to indicate data is being loaded
                    //this.AttachedControl.reportControl.IsLoadingData = true;
                }

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.IsEnabled = true;
                    //pass flag to control to indicate data is loaded
                    //this.AttachedControl.reportControl.IsLoadingData = false;

                    this.AttachedControl.reportEditorControl.CurrentReport = this.SelectedReport;

                    //Display the main form's Home tab
                    LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
                }
            }
        }

        /// <summary>
        /// Handles changes to the selected report where these affect the parent Report screen (not the control itself)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedReport_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(SelectedReportProperty);
            OnPropertyChanged(WindowTitleProperty);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(
                    this.SelectedReport, this.SaveCommand);
            }
            return continueExecute;
        }

        private void FetchAllReportInfos()
        {
            //Get existing reports
            _reportInfos.Clear();
            ReportInfoList reportInfos = ReportInfoList.FetchAll();
            _reportInfos.AddRange(reportInfos);
            OnPropertyChanged(AvailableReportsProperty);
        }

        /// <summary>
        /// Returns a list of reports that match the given criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public IEnumerable<ReportInfo> GetMatchingReports(String criteria)
        {
            if (criteria.Length > 0)
            {
                return this.AvailableReports.Where(p => p.Name.ToUpperInvariant().Contains(criteria));
            }
            return this.AvailableReports;
        }

        /// <summary>
        /// When the user hits the new tabular report button, get the list of report models and display 
        /// a popup window to allow the user to select a report data model.
        /// </summary>
        private void SelectNewReportType()
        {
			base.ShowWaitCursor(true);
			
			DataModelInfoList dataModelInfos = DataModelInfoList.FetchAll();

            // oh dear, there are no report data models defined / returned - let the user know
            if (dataModelInfos == null || dataModelInfos.Count == 0)
            {
				base.ShowWaitCursor(false); 
				MessageBox.Show("No data models loaded");
                return;
            }

			// create the viewmodel from the list of data models
			var newTypesVm =
				new Galleria.Reporting.Controls.Wpf.ReportTypeSelector.ReportSetupNewReportViewModel(dataModelInfos);
			Galleria.Reporting.Controls.Wpf.ReportTypeSelector.ReportSetupNewReportTypeWindow newReportTypeWin =
				new Galleria.Reporting.Controls.Wpf.ReportTypeSelector.ReportSetupNewReportTypeWindow(newTypesVm);

            // and show the window
            if (this.AttachedControl != null)
            {
				base.ShowWaitCursor(false);

				App.ShowWindow(newReportTypeWin, this.AttachedControl, true);
            }

			base.ShowWaitCursor(true);

            // get the model the user selected
            var selectedModelType = newTypesVm.SelectedModel;

            // and check if the user did not cancel
            if (selectedModelType != null && !newTypesVm.UserCancelled)
            {
                //Create the new report
                Report report = Report.NewReport(selectedModelType.Id);

                //Update the report name textbox
                ReportSection rptHeader = report.Sections.First(s => s.SectionType == ReportSectionType.ReportHeader);
                ReportComponent rptHeaderTitleTextBox = rptHeader.Components.First(c => c.ComponentType == ReportComponentType.TextBox);

                //Load the report
                this.SelectedReport = report;
				if (_showDebug)
				{
					this.SelectedReport.DumpDebugReport();
				}
            }

			base.ShowWaitCursor(false);
        }

        #endregion

        #region Print Preview Methods

        public void CreatePdf()
        {
            #region Parameters
            Boolean parametersSelected = 
                Galleria.Reporting.Controls.Wpf.Common.Helpers.PromptParameterValues(this.SelectedReport);
            if (!parametersSelected)
            {
                // Show a cancel message to the user
                ShowCancelWindow();

                return;
            }
            #endregion
            
            base.ShowWaitCursor(true);

            #region Get File Name
            String pdfFileName = CreateUniqueFileName(Path.GetTempPath(), this.SelectedReport);
            //update the report object
            this.SelectedReport.PreviewFilePath = Path.Combine(Path.GetTempPath(), pdfFileName);
            #endregion

            if (pdfFileName.Length > 0)
            {
                Boolean canContinue = true;
                ResultSetDto results = null;
                String query = String.Empty;

                #region Step 1: Fetch Data
                CreatePdfFileExecuteQuery(out query, out results);

                if (results == null)
                {
                    //the query was not completed and no data was returned possibly due to an error
                    canContinue = false;
                }
                else
                {
                    //check row count is not excessive and prompt user if unsure
                    if (results.Rows.Count > ReportHelper.MaxRowCountWithoutWarningUser)
                    {
                        base.ShowWaitCursor(false);

                        canContinue = ShowLargeDataRowWarningDialog(results.Rows.Count);

                        base.ShowWaitCursor(true);
                    }
                }
                #endregion

                #region Step2 : Create output PDF File
                if (canContinue)
                {
                    CreatePdfFile(query, results);
                }
                #endregion
            }

            base.ShowWaitCursor(false);
        }

        public void CreatePdfFile(String query, ResultSetDto queryResults)
        {
            _previewBusy = new ModalBusy();
            _previewBusy.IsDeterminate = true;
            _previewBusy.Header = Message.ReportViewer_BusyHeader;
            _previewBusy.Description = Message.ReportViewer_BusyDescription;

            Csla.Threading.BackgroundWorker createPdfFileWorker = new Csla.Threading.BackgroundWorker();
            createPdfFileWorker.WorkerReportsProgress = true;
            createPdfFileWorker.ProgressChanged += CreatePdfFileWorker_ProgressChanged;

            _processException = null;
            RenderResult renderResult = RenderResult.None;

            createPdfFileWorker.DoWork +=
                (s, e) =>
                {
                    //start new process to create the pdf
                    createPdfFileWorker.ReportProgress(ReportProgress_BuildingReport);

                    Galleria.Reporting.Processes.RenderPdfReport.Process process =
                        ProcessFactory.Execute<Galleria.Reporting.Processes.RenderPdfReport.Process>(
                        new Galleria.Reporting.Processes.RenderPdfReport.Process(this.SelectedReport, query, queryResults));

                    renderResult = process.RenderResult;

                    //check for error during process
                    if (process.Exception == null && renderResult == Galleria.Reporting.Processes.RenderPdfReport.RenderResult.Success)
                    {
                        try
                        {
                            createPdfFileWorker.ReportProgress(ReportProgress_SavingReport);
                            //Save the report file
                            process.Result.Save(this.SelectedReport.PreviewFilePath);

                            createPdfFileWorker.ReportProgress(ReportProgress_OpeningReport);
                        }
                        catch (Exception ex)
                        {
                            _processException = ex;
                        }
                    }
                    else
                    {
                        _processException = process.Exception;
                    }
                };

            createPdfFileWorker.RunWorkerCompleted +=
                (s, e) =>
                {
                    //cancel the busy window
                    if (_previewBusy != null)
                    {
                        _previewBusy.Close();
                        _previewBusy = null;
                    }

                    createPdfFileWorker.ProgressChanged -= CreatePdfFileWorker_ProgressChanged;

                    if (renderResult == Galleria.Reporting.Processes.RenderPdfReport.RenderResult.Success)
                    {
                        #region Open Report
                        base.ShowWaitCursor(false);
                        //open the pdf
                        OpenFile(this.SelectedReport.PreviewFilePath);
                        #endregion
                    }
                    else if (renderResult == Galleria.Reporting.Processes.RenderPdfReport.RenderResult.CancelledByUser)
                    {
                        #region Report Cancelled

                        ModalMessage dialog =
                            new ModalMessage()
                            {
                                Title = Message.ReportViewer_MessageTitle,
                                Header = Message.ReportExecute_ReportCancelled_Header,
                                Description = Message.ReportExecute_ReportCancelled_Description,
                                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                                ButtonCount = 1,
                                Button1Content = Message.Generic_Ok,
                                DefaultButton = ModalMessageButton.Button1,
                                CancelButton = ModalMessageButton.Button1
                            };

                        base.ShowWaitCursor(false);

                        App.ShowWindow(dialog, this.AttachedControl, true);

                        base.ShowWaitCursor(true);

                        #endregion
                    }
                    else
                    {
                        #region Report Error
                        //Set the message
                        String description =
                            String.Format(
                            "{0}\r\n\r\n{1}: {2}",
                            Message.ReportExecute_ReportFailed_FailedToBuild,
                            Message.ReportExecute_ReportFailed_StepThatFailed,
                        renderResult.ToString());
                        if (_processException != null)
                        {
                            description =
                                String.Format(
                                "{0}\r\n\r\n{1}: {2}",
                                Message.ReportExecute_ReportFailed_ErrorOccurred,
                                Message.ReportExecute_ReportFailed_ErrorMessage,
                                _processException.Message);
                        }
                        //inform user that error has occurred
                        ModalMessage dialog =
                            new ModalMessage()
                            {
                                Title = Message.ReportViewer_MessageTitle,
                                Header = Message.ReportExecute_ReportFailed_Header,
                                Description = description,
                                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                                ButtonCount = 1,
                                Owner = this.AttachedControl,
                                Button1Content = Message.Generic_Ok,
                                DefaultButton = ModalMessageButton.Button1,
                                CancelButton = ModalMessageButton.Button1
                            };

                        base.ShowWaitCursor(false);

                        App.ShowWindow(dialog, this.AttachedControl, true);

                        base.ShowWaitCursor(true);

                        #endregion
                    }

                    createPdfFileWorker = null;
                };

            //run the worker and show the busy window
            createPdfFileWorker.RunWorkerAsync();
            App.ShowWindow(_previewBusy, this.AttachedControl, true);
        }

        private void CreatePdfFileWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            Int32 currentProgress = e.ProgressPercentage;

            _previewBusy.SetValue(ModalBusy.ProgressPercentageProperty, currentProgress);

            if (currentProgress == ReportProgress_BuildingReport)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_BuildingReport);
            }

            if (currentProgress == ReportProgress_SavingReport)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_SavingReport);
            }

            if (currentProgress == ReportProgress_OpeningReport)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_OpeningReport);
            }
        }

        private void CreatePdfFileExecuteQuery(out String query, out ResultSetDto results)
        {
            //Int32 queryRowCount = -1;
            ResultSetDto queryResults = null;
            String sqlQuery = String.Empty;

            _previewBusy = new ModalBusy();
            _previewBusy.IsDeterminate = true;
            _previewBusy.Header = Message.ReportViewer_BusyHeader;
            _previewBusy.Description = Message.ReportViewer_BusyDescription;

            Csla.Threading.BackgroundWorker fetchPdfDataCountWorker = new Csla.Threading.BackgroundWorker();
            fetchPdfDataCountWorker.WorkerReportsProgress = true;
            fetchPdfDataCountWorker.ProgressChanged += CreatePdfFileExecuteQueryWorker_ProgressChanged;

            _processException = null;
            FetchDataResult fetchDataResult = FetchDataResult.None;

            fetchPdfDataCountWorker.DoWork +=
                (s, e) =>
                {
                    //start new process to create the pdf
                    fetchPdfDataCountWorker.ReportProgress(FetchDataProgress_StartFetch);

                    Galleria.Reporting.Processes.FetchReportData.Process process =
                        ProcessFactory.Execute<Galleria.Reporting.Processes.FetchReportData.Process>(
                        new Galleria.Reporting.Processes.FetchReportData.Process(this.SelectedReport));

                    fetchDataResult = process.FetchDataResult;

                    //check for error during process
                    if (process.Exception == null
                        && fetchDataResult == Galleria.Reporting.Processes.FetchReportData.FetchDataResult.Success)
                    {
                        try
                        {
                            //Save the report file
                            //queryRowCount = process.ReportRowCount;
                            sqlQuery = process.Query;
                            queryResults = process.Results;
                            fetchPdfDataCountWorker.ReportProgress(FetchDataProgress_Complete);
                        }
                        catch (Exception ex)
                        {
                            _processException = ex;
                        }
                    }
                    else
                    {
                        _processException = process.Exception;
                    }
                };

            fetchPdfDataCountWorker.RunWorkerCompleted +=
                (s, e) =>
                {
                    //cancel the busy window
                    if (_previewBusy != null)
                    {
                        _previewBusy.Close();
                        _previewBusy = null;
                    }

                    fetchPdfDataCountWorker.ProgressChanged -= CreatePdfFileExecuteQueryWorker_ProgressChanged;

                    if (fetchDataResult != Galleria.Reporting.Processes.FetchReportData.FetchDataResult.Success)
                    {
                        #region Report Error
                        //Set the message
                        String description =
                            String.Format(
                            "{0}\r\n\r\n{1}: {2}",
                            Message.ReportExecute_ReportFailed_FailedToBuild,
                            Message.ReportExecute_ReportFailed_StepThatFailed,
                        fetchDataResult.ToString());
                        if (_processException != null)
                        {
                            description =
                                String.Format(
                                "{0}\r\n\r\n{1}: {2}",
                                Message.ReportExecute_ReportFailed_ErrorOccurred,
                                Message.ReportExecute_ReportFailed_ErrorMessage,
                                _processException.Message);
                        }
                        //inform user that error has occurred
                        ModalMessage dialog =
                            new ModalMessage()
                            {
                                Title = Message.ReportViewer_MessageTitle,
                                Header = Message.ReportExecute_ReportFailed_Header,
                                Description = description,
                                WindowStartupLocation = WindowStartupLocation.CenterOwner,
                                ButtonCount = 1,
                                Button1Content = Message.Generic_Ok,
                                DefaultButton = ModalMessageButton.Button1,
                                CancelButton = ModalMessageButton.Button1
                            };

                        base.ShowWaitCursor(false);

                        App.ShowWindow(dialog, this.AttachedControl, true);

                        base.ShowWaitCursor(true);

                        #endregion
                    }

                    fetchPdfDataCountWorker = null;
                };

            //run the worker and show the busy window
            fetchPdfDataCountWorker.RunWorkerAsync();
            App.ShowWindow(_previewBusy, this.AttachedControl, true);

            if (sqlQuery.Length > 0)
            {
                query = sqlQuery;
            }
            else
            {
                query = String.Empty;
            }
            if (queryResults != null)
            {
                results = queryResults;
            }
            else
            {
                results = null;
            }
        }

        private void CreatePdfFileExecuteQueryWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            Int32 currentProgress = e.ProgressPercentage;

            _previewBusy.SetValue(ModalBusy.ProgressPercentageProperty, currentProgress);

            if (currentProgress == FetchDataProgress_StartFetch)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_FetchingData);
            }

            if (currentProgress == FetchDataProgress_Complete)
            {
                _previewBusy.SetValue(ModalBusy.DescriptionProperty, Message.ReportExecute_CheckingData);
            }
        }

        /// <summary>
        /// The warning method called by the background process when the 
        /// report will contain more than the limit number of rows
        /// </summary>
        /// <returns></returns>
        public Boolean ShowLargeDataRowWarningDialog(Int32 reportRowCount)
        {
            if (this.AttachedControl != null)
            {
                ModalMessage msg = new ModalMessage()
                    {
                        Title = Reporting.Resources.Language.Message.ReportExecute_RowCountDialog_Title,
                        Header = Reporting.Resources.Language.Message.ReportExecute_RowCountDialog_Header,
                        Description = String.Format(Reporting.Resources.Language.Message.ReportExecute_RowCountDialog_Text, reportRowCount.ToString()),
                        WindowStartupLocation = WindowStartupLocation.CenterOwner,
                        ButtonCount = 2,
                        Button1Content = Message.Generic_Ok,
                        Button2Content = Message.Generic_Cancel,
                        DefaultButton = ModalMessageButton.Button1,
                        CancelButton = ModalMessageButton.Button2
                    };

                App.ShowWindow(msg, this.AttachedControl, true);

                return (msg.Result == ModalMessageResult.Button1);
            }
            return true;
        }

        /// <summary>
        /// Delete the existing file if it exists, or inform user if the file cannot be deleted
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private Boolean DeleteExistingFile(String filePath)
        {
            Boolean isSuccessful = true;

            if (System.IO.File.Exists(filePath))
            {
                //check if file is already open
                if (!FileHelper.IsFileLocked(new System.IO.FileInfo(filePath)))
                {
                    //delete the file
                    System.IO.File.Delete(filePath);
                }
                else
                {
                    isSuccessful = false;

                    #region Display message
                    //tell the user to close the pdf
                    ModalMessage dialog =
                        new ModalMessage()
                        {
                            Title = Message.ReportViewer_MessageTitle,
                            Header = Message.ReportViewer_FileInUse_Title,
                            Description = String.Format(Message.ReportViewer_FileInUse_Message, filePath),
                            WindowStartupLocation = WindowStartupLocation.CenterOwner,
                            ButtonCount = 1,
                            Button1Content = Message.Generic_Ok,
                            DefaultButton = ModalMessageButton.Button1,
                            CancelButton = ModalMessageButton.Button1
                        };

                    App.ShowWindow(dialog, this.AttachedControl, true);
                    #endregion
                }
            }
            return isSuccessful;
        }

        /// <summary>
        /// Creates a unique filename based on the report Name
        /// </summary>
        /// <param name="proposedFileName"></param>
        /// <returns></returns>
        private String CreateUniqueFileName(String filepath, Report report)
        {
            Int32 fileNumber = 1;

            String reportName = report.Name;
            foreach (Char c in Path.GetInvalidFileNameChars())
            {
                reportName = reportName.Replace(c, '_');
            }
            String newFileName = String.Format("{0}.pdf", reportName);

            Boolean isSuccessful = false;

            while (!isSuccessful)
            {
                String fileFullPathAndName = Path.Combine(filepath, newFileName);

                if (System.IO.File.Exists(fileFullPathAndName))
                {
                    //check if file is already open
                    if (!FileHelper.IsFileLocked(new System.IO.FileInfo(fileFullPathAndName)))
                    {
                        //delete the file
                        System.IO.File.Delete(fileFullPathAndName);
                        isSuccessful = true;
                    }
                    else
                    {
                        isSuccessful = false;
                        //file exists and is in use so increment number
                        newFileName = String.Format("{0} ({1}).pdf", reportName, fileNumber.ToString());
                        fileNumber++;
                    }
                }
                else
                {
                    isSuccessful = true;
                }
            }

            return newFileName;
        }

        /// <summary>
        /// Open the existing file
        /// </summary>
        private void OpenFile(String filePath)
        {
            if (!String.IsNullOrWhiteSpace(filePath))
            {
                //check it was saved
                if (System.IO.File.Exists(filePath))
                {
                    System.Diagnostics.Process.Start(filePath);

                    base.ShowWaitCursor(false);
                }
            }
        }

        private void ShowCancelWindow()
        {
            ModalMessage dialog =
                new ModalMessage()
                {
                    Title = Message.ReportViewer_MessageTitle,
                    Header = Message.ReportExecute_ReportCancelled_Header,
                    Description = Message.ReportExecute_ReportCancelled_Description,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    ButtonCount = 1,
                    Button1Content = Message.Generic_Ok,
                    DefaultButton = ModalMessageButton.Button1,
                    CancelButton = ModalMessageButton.Button1
                };

            App.ShowWindow(dialog, this.AttachedControl, true);
        }
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (this.SelectedReport != null)
                    {
                        this.SelectedReport.PropertyChanged -= SelectedReport_PropertyChanged;
                    }
                    this.SelectedReport = null;
                    _reportInfos = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
