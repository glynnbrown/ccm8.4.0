﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: GRB 100
// V8-26996 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Reporting.Model;
using Galleria.Framework.Model;
using System.Windows.Media;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for ReportSetupBackstageHelp.xaml
    /// </summary>
    public partial class ReportSetupBackstageHelp : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportSetupBackstageHelpViewModel), typeof(ReportSetupBackstageHelp),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public ReportSetupBackstageHelpViewModel ViewModel
        {
            get { return (ReportSetupBackstageHelpViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportSetupBackstageHelp senderControl = (ReportSetupBackstageHelp)obj;

            if (e.OldValue != null)
            {
                ReportSetupBackstageHelpViewModel oldModel = (ReportSetupBackstageHelpViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ReportSetupBackstageHelpViewModel newModel = (ReportSetupBackstageHelpViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public ReportSetupBackstageHelp()
        {
            InitializeComponent();

            //Add link to the helpfile
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.Reports);

            this.ViewModel = new ReportSetupBackstageHelpViewModel();
        }

        #endregion
    }
}
