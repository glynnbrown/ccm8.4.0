﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25034 : M.Pettit
//  Created
#endregion

#region Version History: (V8 820)
// V8-30966 : M.Pettit
//	Set min/max values for GridSpacing
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for ReportSetupArrangeTab.xaml
    /// </summary>
    public partial class ReportSetupArrangeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportSetupViewModel), typeof(ReportSetupArrangeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportSetupViewModel ViewModel
        {
            get { return (ReportSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor
        
        public ReportSetupArrangeTab()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

		/// <summary>
		/// Responds to key up events on the Grid spacing textbox.
		/// Specifically, this forces the textbox value to be validated when the user has finished typing.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void xGridSpacingTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (e.Key == System.Windows.Input.Key.Return)
			{
				//force the controls value to match the source property value
				xGridSpacingTextBox.GetBindingExpression(
					Galleria.Framework.Controls.Wpf.ExtendedTextBox.TextProperty).UpdateSource();

				//clear focus from the control
				Keyboard.ClearFocus();
			}
		}

        #endregion
    }
}
