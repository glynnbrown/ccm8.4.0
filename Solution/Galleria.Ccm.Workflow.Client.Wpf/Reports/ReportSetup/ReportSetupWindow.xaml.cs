﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25034 : M.Shelley
//  Created
#endregion

#endregion

using System.Windows;
using System.Windows.Input;
using System;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for ReportSetupWindow.xaml
    /// </summary>
    public partial class ReportSetupWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReportSetupViewModel), typeof(ReportSetupWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportSetupViewModel ViewModel
        {
            get { return (ReportSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportSetupWindow senderControl = (ReportSetupWindow)obj;

            if (e.OldValue != null)
            {
                ReportSetupViewModel oldModel = (ReportSetupViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                ReportSetupViewModel newModel = (ReportSetupViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public ReportSetupWindow(ReportSetupViewModel viewModel)
        {
            InitializeComponent();

            this.Icon = ImageResources.CCMIcon;

            this.ViewModel = viewModel;

            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ReportBuilder);

            //Display the main form's Backstage tab
           LocalHelper.SetRibbonBackstageState(this, true);

            this.Loaded += ReportSetupWindow_Loaded;
        }

        private void ReportSetupWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ReportSetupWindow_Loaded;

            //Initial ViewModel settings

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Preps and opens the given window as a child of this
        /// </summary>
        /// <param name="window"></param>
        /// <param name="isModal"></param>
        public void OpenChildWindow(Window window, bool isModal)
        {
            //prep the window
            if (window.Owner == null)
            {
                window.Owner = this;
            }
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Icon = ImageResources.CCMIcon;
            window.Closed += new EventHandler(window_Closed);

            if (isModal)
            {
                window.ShowDialog();
            }
            else
            {
                window.Show();
            }
        }

        /// <summary>
        /// Preps and opens the given window as a child of the specified parent
        /// </summary>
        /// <param name="window"></param>
        /// <param name="isModal"></param>
        public void OpenChildWindow(Window window, Window parent, Boolean isModal)
        {
            //prep the window
            if (parent != null)
            {
                window.Owner = parent;
            }
            else if (window.Owner == null)
            {
                window.Owner = this;
            }
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.Icon = ImageResources.CCMIcon;
            window.Closed += new EventHandler(window_Closed);

            if (isModal)
            {
                window.ShowDialog();
            }
            else
            {
                window.Show();
            }
        }

        /// <summary>
        /// Activates the owning window after its child has closed
        /// to fix the issue of it hiding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void window_Closed(object sender, EventArgs e)
        {
            Window senderControl = (Window)sender;
            senderControl.Closed -= window_Closed;

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    senderControl.Owner.Activate();

                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        #region Event Handlers

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //ViewModel change handler - clear out old values
        }

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        #endregion

        #region Window Close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            this.ViewModel.PropertyChanged -= ViewModel_PropertyChanged;

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                IDisposable disposableViewModel = this.ViewModel;
                this.ViewModel = null;

                if (disposableViewModel != null)
                {
                    disposableViewModel.Dispose();
                }
            }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
