﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GRB 1.0.0)
// GRB-25034 : M.Shelley
//  Created
#endregion

#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Reporting.Model;
using Galleria.Reporting.Controls.Wpf.ViewModels;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.ReportSetup
{
    /// <summary>
    /// Interaction logic for ReportSetupNewReportTypeWindow.xaml
    /// </summary>
    public partial class ReportSetupNewReportTypeWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", 
                typeof(ReportSetupNewReportViewModel), typeof(ReportSetupNewReportTypeWindow),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ReportSetupNewReportViewModel ViewModel
        {
            get { return (ReportSetupNewReportViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReportSetupNewReportTypeWindow senderControl = (ReportSetupNewReportTypeWindow)obj;

            ReportSetupNewReportViewModel oldModel = (ReportSetupNewReportViewModel)e.OldValue;
            ReportSetupNewReportViewModel newModel = (ReportSetupNewReportViewModel)e.NewValue;

            if (e.OldValue != null)
            {
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public ReportSetupNewReportTypeWindow(ReportSetupNewReportViewModel reportTypesModel)
        {
            InitializeComponent();

            //Set the viewmodel for the window
            this.ViewModel = reportTypesModel;
        }

        #endregion

        #region Methods

        #endregion

        #region Event Handlers

        /// <summary>
        /// Allow the user to double-click a report model and skip the "OK" button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // the user has double-clicked on a report model, so fire the OK command
            var vm = this.ViewModel as ReportSetupNewReportViewModel;

            if (vm != null)
            {
                vm.ReportTypeOkCommand.Execute();
            }
        }

        #endregion

        #region Window Close

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);
            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                IDisposable disposableViewModel = this.ViewModel;
                this.ViewModel = null;

                if (disposableViewModel != null)
                {
                    disposableViewModel.Dispose();
                }
            }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
