﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM 8.3.0
// V8-32790 : M.Pettit
//  Created
// V8-32790 : M.Pettit
//  Added OpenFromSourceType support for redesigned backstage UI
// V8-32790 : M.Pettit
//  Added OpenFromSourceType support for redesigned backstage UI
// V8-32823  : J.Pickup
//  Export visbiltity work (New properties).
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla.Core;
using Csla.Server;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;
using FrameworkHelpers = Galleria.Framework.Controls.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Helpers;
using System.Globalization;
using System.IO;
using Galleria.Ccm.Workflow.Client.Wpf.Common;


namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramExportTemplateMaintenance
{
    /// <summary>
    /// Viewmodel controller for PlanogramExportTemplateMaintenanceViewModel
    /// </summary>
    public sealed class PlanogramExportTemplateMaintenanceViewModel : WindowViewModelBase
    {
        #region Fields

        const String _exCategory = "PlanogramExportTemplateMaintenance"; //Category names to any exception thrown to gibraltar from here
        private ModelPermission<PlanogramExportTemplate> _itemPermissions;

        private readonly PlanogramExportTemplateInfoListViewModel _planogramExportTemplateInfoListView = new PlanogramExportTemplateInfoListViewModel();
        private PlanogramExportTemplate _selectedItem;
        private PlanogramExportTemplateMaintenancePerformanceMetricWindow _metricWindow;
        private PerformanceMetricRow _selectedPerformanceMetric;
        private String _mappingTemplateName;

        private ReadOnlyCollection<String> _availableVersions;
        private Dictionary<PlanogramFieldMappingType, ReadOnlyCollection<PlanogramExportTemplateFieldInfo>> _externalFieldDict;

        private readonly BulkObservableCollection<MappingRow> _mappingRows = new BulkObservableCollection<MappingRow>();
        private readonly BulkObservableCollection<PerformanceMetricRow> _performanceMetricRows = new BulkObservableCollection<PerformanceMetricRow>();
        private ReadOnlyBulkObservableCollection<PerformanceMetricRow> _performanceMetricRowsRO;
        private OpenFromSourceType _openSourceType = OpenFromSourceType.OpenFromRepository;

        #endregion

        #region Binding Property paths

        // properties
        public static readonly PropertyPath SelectedItemProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SelectedItem);
        public static readonly PropertyPath MappingTemplateNameProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.MappingTemplateName);
        public static readonly PropertyPath SelectedFileTypeProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SelectedFileType);
        public static readonly PropertyPath AvailableVersionsProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.AvailableVersions);
        public static readonly PropertyPath SelectedVersionProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SelectedVersion);
        public static readonly PropertyPath ProductMappingsProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.ProductMappings);
        public static readonly PropertyPath ComponentMappingsProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.ComponentMappings);
        public static readonly PropertyPath BayMappingsProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.BayMappings);
        public static readonly PropertyPath PlanogramMappingsProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.PlanogramMappings);
        public static readonly PropertyPath PerformanceMetricsProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.PerformanceMetrics);
        public static readonly PropertyPath SelectedPerformanceMetricProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SelectedPerformanceMetric);
        public static readonly PropertyPath OpenFromSourceTypeProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.OpenFromSourceType);
        public static readonly PropertyPath AvailablePlanogramExportTemplatesProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.AvailablePlanogramExportTemplates);
        public static readonly PropertyPath AvailableExternalTypeFriendlyNamesProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.AvailableExternalTypeFriendlyNames);
        
        //Commands
        public static readonly PropertyPath ClearAllCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.ClearAllMappingsCommand);
        public static readonly PropertyPath ClearMappingCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.ClearMappingCommand);
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath OpenFromFileCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.OpenFromFileCommand);
        public static readonly PropertyPath SaveAsToRepositoryCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SaveAsToRepositoryCommand);
        public static readonly PropertyPath SaveAsToFileCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SaveAsToFileCommand);
        public static readonly PropertyPath NewCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.NewCommand);
        public static readonly PropertyPath SaveCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath DeleteCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath CloseCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath NewMetricCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.NewMetricCommand);
        public static readonly PropertyPath ViewMetricCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.ViewMetricCommand);
        public static readonly PropertyPath RemoveMetricCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.RemoveMetricCommand);
        public static readonly PropertyPath MetricSaveCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.MetricSaveCommand);
        public static readonly PropertyPath MetricCancelCommandProperty = GetPropertyPath<PlanogramExportTemplateMaintenanceViewModel>(p => p.MetricCancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Determnines the source of the open method selected by the user
        /// </summary>
        public OpenFromSourceType OpenFromSourceType
        {
            get { return _openSourceType; }
            set
            {
                _openSourceType = value;
                OnPropertyChanged(OpenFromSourceTypeProperty);
            }
        }

        public DisplayUnitOfMeasureCollection DisplayUOMs
        {
            get
            {
                return DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.EntityId);
            }
        }
        /// <summary>
        /// Return the collection of available products based on the search criteria
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramExportTemplateInfo> AvailablePlanogramExportTemplates
        {
            get { return _planogramExportTemplateInfoListView.BindableCollection; }
        }

        /// <summary>
        /// Gets/Sets the currently selected template
        /// </summary>
        public PlanogramExportTemplate SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                PlanogramExportTemplate oldValue = _selectedItem;

                _selectedItem = value;
                OnPropertyChanged(SelectedItemProperty);

                OnSelectedItemChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns a readonly collection of performance metrics
        /// </summary>
        public ReadOnlyBulkObservableCollection<PerformanceMetricRow> PerformanceMetrics
        {
            get
            {
                if (_performanceMetricRowsRO == null)
                {
                    _performanceMetricRowsRO = new ReadOnlyBulkObservableCollection<PerformanceMetricRow>(_performanceMetricRows);
                }
                return _performanceMetricRowsRO;
            }
        }

        /// <summary>
        /// The currently selected Performance Metrics
        /// </summary>
        public PerformanceMetricRow SelectedPerformanceMetric
        {
            get { return _selectedPerformanceMetric; }
            set
            {
                _selectedPerformanceMetric = value;
                OnPropertyChanged(SelectedPerformanceMetricProperty);
            }
        }

        /// <summary>
        /// Returns the name of the mapping template in use
        /// or custom.
        /// </summary>
        public String MappingTemplateName
        {
            get { return _mappingTemplateName; }
            set
            {
                _mappingTemplateName = value;
                OnPropertyChanged(MappingTemplateNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected file type.
        /// </summary>
        public PlanogramExportFileType SelectedFileType
        {
            get
            {
                if (SelectedItem == null)
                {
                    if (AvailableExternalTypeFriendlyNames.Keys.Count() > 0)
                    {
                       return  AvailableExternalTypeFriendlyNames.Keys.First();
                    }
                    else 
                    {
                        return PlanogramExportFileType.Spaceman;
                    }
                }
                else 
                {
                    return SelectedItem.FileType;
                }
            }
            set
            {
                if (SelectedItem == null) return;

                SelectedItem.FileType = value;
                if (SelectedItem.FileType == PlanogramExportFileType.Spaceman)
                {
                    SelectedVersion = "9";
                }
                OnSelectedFileTypeChanged(value);
                OnPropertyChanged(SelectedFileTypeProperty);
            }
        }

        //List of external type sonly (w/o POG) for what is currently set as available
        Dictionary<PlanogramExportFileType, String> _availableTypes;
        public Dictionary<PlanogramExportFileType, String> AvailableExternalTypeFriendlyNames
        {
            get 
            {
                if (_availableTypes == null)
                { 
                    _availableTypes = PlanogramExportFileTypeHelper.GetAvailableExternalTypeFriendlyNames(App.ViewState.IsExportSpacemanAvailable, App.ViewState.IsExportJDAAvailable, App.ViewState.IsExportApolloAvailable);
                }
                return _availableTypes;
            }
        }
            

        /// <summary>
        /// Returns the collection of versions available for the selected file type.
        /// </summary>
        public ReadOnlyCollection<String> AvailableVersions
        {
            get { return _availableVersions; }
            private set
            {
                _availableVersions = value;
                OnPropertyChanged(AvailableVersionsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected version
        /// </summary>
        public String SelectedVersion
        {
            get
            {
                if (SelectedItem == null) return String.Empty;
                return SelectedItem.FileVersion;
            }
            set
            {
                if (SelectedItem == null) return;
                SelectedItem.FileVersion = value;
                OnPropertyChanged(SelectedVersionProperty);
            }
        }

        /// <summary>
        /// Returns the collection of product mappings
        /// </summary>
        public IEnumerable<MappingRow> ProductMappings
        {
            get
            {
                return _mappingRows.Where(m => m.MappingType == PlanogramFieldMappingType.Product).ToList();
            }
        }

        /// <summary>
        /// Returns the collection of component mappings
        /// </summary>
        public IEnumerable<MappingRow> ComponentMappings
        {
            get
            {
                return _mappingRows.Where(m => m.MappingType == PlanogramFieldMappingType.Component).ToList();
            }
        }

        /// <summary>
        /// Returns the collection of bay mappings
        /// </summary>
        public IEnumerable<MappingRow> BayMappings
        {
            get
            {
                return _mappingRows.Where(m => m.MappingType == PlanogramFieldMappingType.Fixture).ToList();
            }
        }

        /// <summary>
        /// Returns the collection of planogram mappings.
        /// </summary>
        public IEnumerable<MappingRow> PlanogramMappings
        {
            get
            {
                return _mappingRows.Where(m => m.MappingType == PlanogramFieldMappingType.Planogram).ToList();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PlanogramExportTemplateMaintenanceViewModel(PlanogramExportFileType fileType)
        {
            //set perms
            _itemPermissions = new ModelPermission<PlanogramExportTemplate>(PlanogramExportTemplate.GetUserPermissions());

            //master info list
            _planogramExportTemplateInfoListView.FetchForCurrentEntity();

            //create a new universe
            this.NewCommand.Execute();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the selected item changes.
        /// </summary>
        /// <param name="value"></param>
        private void OnSelectedItemChanged(PlanogramExportTemplate oldValue, PlanogramExportTemplate newValue)
        {
            //clear out the old mapping rows
            _mappingRows.Clear();
            _performanceMetricRows.Clear();

            if (oldValue != null)
            {
                oldValue.ChildChanged -= SelectedItem_ChildChanged;
            }

            if (newValue != null)
            {
                //ensure that all mappble ccm fields have a value
                newValue.UpdateFromCcmFieldList();

                OnPropertyChanged(SelectedFileTypeProperty);
                OnSelectedFileTypeChanged(this.SelectedFileType);

                UpdateDoubleMappingFlags();

                newValue.ChildChanged += SelectedItem_ChildChanged;
            }
        }

        /// <summary>
        /// Called whenever a child object of the selected item changes.
        /// </summary>
        private void SelectedItem_ChildChanged(object sender, ChildChangedEventArgs e)
        {
            if (e.PropertyChangedArgs != null
                && e.PropertyChangedArgs.PropertyName == PlanogramExportTemplateMapping.ExternalFieldProperty.Name)
            {
                this.MappingTemplateName = Message.PlanogramExportTemplateMaintenance_CustomTemplate;
                UpdateDoubleMappingFlags();
            }
        }

        /// <summary>
        /// Called whenever the selected file type changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedFileTypeChanged(PlanogramExportFileType newValue)
        {
            String curSelectedVersion = this.SelectedVersion;

            //update the list of available versions and selected version.
            switch (newValue)
            {
                case PlanogramExportFileType.Spaceman:
                    this.AvailableVersions = new ReadOnlyCollection<String>(SpacemanFieldHelper.AvailableVersions);
                    break;

                case PlanogramExportFileType.Apollo:
                    this.AvailableVersions = new ReadOnlyCollection<String>(ApolloExportHelper.AvailableVersions);
                    break;

                case PlanogramExportFileType.JDA:
                    this.AvailableVersions = new ReadOnlyCollection<String>(JDAExportHelper.AvailableVersions);
                    break;

                default: throw new NotImplementedException();
            }

            SelectedItem.FileVersion = (this.AvailableVersions.Contains(curSelectedVersion)) ? curSelectedVersion : this.AvailableVersions.LastOrDefault();
            OnPropertyChanged(SelectedVersionProperty);
            //get the external field list
            RefreshMappingRows();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        ///     Gets the New command.
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute(),
                        attachToCommandManager: false)
                    {

                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    RegisterCommand(_newCommand);
                }
                return _newCommand;
            }
        }

        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                NewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }
            return true;
        }

        private void New_Executed()
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;

            // Create a new item.
            PlanogramExportTemplate newItem = PlanogramExportTemplate.NewPlanogramExportTemplate(App.ViewState.EntityId, SelectedFileType);

            // Re-initialize as property was changed.
            newItem.MarkGraphAsInitialized();

            this.SelectedItem = newItem;

            //close the backstage
            CloseRibbonBackstage();

        }

        #endregion

        #region OpenCommand

        private RelayCommand<Object> _openCommand;

        /// <summary>
        ///     Gets the Open command.
        /// </summary>
        public RelayCommand<Object> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {

                    _openCommand = new RelayCommand<Object>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        private Boolean Open_CanExecute(Object itemId)
        {
            //user must have get permission
            if (!_itemPermissions.CanFetch)
            {
                OpenCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (itemId == null)
            {
                OpenCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Object itemId)
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;
            if (itemId == null) return;

            base.ShowWaitCursor(true);

            try
            {
                //fetch the requested item
                this.SelectedItem = PlanogramExportTemplate.FetchById(itemId);
                //mark it as initialized so warnings do not flag until something is changed by the user.
                this.SelectedItem.MarkGraphAsInitialized();
            }
            catch (DataPortalException e)
            {
                base.ShowWaitCursor(false);
                RecordException(e, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }
            //close the backstage
            CloseRibbonBackstage();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region OpenFromFileCommand

        private RelayCommand _openFromFileCommand;

        /// <summary>
        ///     Gets the Open command.
        /// </summary>
        public RelayCommand OpenFromFileCommand
        {
            get
            {
                if (_openFromFileCommand == null)
                {

                    _openFromFileCommand = new RelayCommand(
                        p => OpenFromFile_Executed(p))
                    {
                        FriendlyName = Message.Generic_BrowseForFile,
                        Icon = ImageResources.Open_48,
                        SmallIcon = ImageResources.Open_16
                    };
                    RegisterCommand(_openFromFileCommand);
                }
                return _openFromFileCommand;
            }
        }

        private void OpenFromFile_Executed(Object args)
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;

            base.ShowWaitCursor(true);

            String file = args as String;
            if (String.IsNullOrEmpty(file))
            {
                Boolean result =
                GetWindowService().ShowOpenFileDialog(
                App.ViewState.GetSessionDirectory(SessionDirectory.PlanogramExportFileTemplate),
                    String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ExportFilter, PlanogramExportTemplate.FileExtension),
                    out file);

                if (!result) return;

                //update the session directory
                App.ViewState.SetSessionDirectory(SessionDirectory.PlanogramExportFileTemplate, Path.GetDirectoryName(file));
            }

            base.ShowWaitCursor(true);

            try
            {
                //fetch the requested item
                this.SelectedItem = PlanogramExportTemplate.FetchByFilename(file, /*asReadOnly*/true);
                this.SelectedItem.MarkGraphAsInitialized();
            }
            catch (DataPortalException e)
            {
                base.ShowWaitCursor(false);

                base.ShowWaitCursor(false);
                RecordException(e, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            //close the backstage
            CloseRibbonBackstage();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    RegisterCommand(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.SelectedItem == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //if item is new, must have create
            if (this.SelectedItem.IsNew && !_itemPermissions.CanCreate)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //if item is old, user must have edit permission
            if (!this.SelectedItem.IsNew && !_itemPermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid            
            if (!this.SelectedItem.IsValid || !_mappingRows.All(p => p.IsValid))
            {
                String invalidMessage = Message.Generic_Save_DisabledReasonInvalidData;
                if (this.PerformanceMetrics.Any(p => !p.IsValid))
                {
                    invalidMessage = String.Format("{0} {1}", invalidMessage, Message.PlanogramExportTemplateMaintenance_Save_InvalidMetricsDisabledReason);
                }
                this.SaveCommand.DisabledReason = invalidMessage;
                this.SaveAndNewCommand.DisabledReason = invalidMessage;
                this.SaveAndCloseCommand.DisabledReason = invalidMessage;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        private Boolean SaveCurrentItem()
        {
            Object itemId = this.SelectedItem.Id;

            #region check the item unique name
            String newName;
            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       ShowWaitCursor(true);

                       foreach (PlanogramExportTemplateInfo info in _planogramExportTemplateInfoListView.Model)
                       {
                           if (!info.Id.Equals(itemId)
                               && info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       ShowWaitCursor(false);

                       return returnValue;
                   };

            Boolean nameAccepted = GetWindowService().PromptIfNameIsNotUnique(isUniqueCheck, this.SelectedItem.Name, out newName);
            if (!nameAccepted) return false;

            //set the name
            if (this.SelectedItem.Name != newName) this.SelectedItem.Name = newName;
            #endregion

            //** save the item
            ShowWaitCursor(true);

            try
            {
                this.SelectedItem = this.SelectedItem.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);

                Exception rootException = ex.GetBaseException();
                RecordException(rootException, _exCategory);

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(this.SelectedItem.Name);
                    if (itemReloadRequired)
                    {
                        this.SelectedItem = PlanogramExportTemplate.FetchById(this.SelectedItem.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(this.SelectedItem.Name, OperationType.Save);
                }
                return false;
            }

            //refresh the info list
            _planogramExportTemplateInfoListView.FetchForCurrentEntity();

            ShowWaitCursor(false);

            return true;

        }

        #endregion

        #region SaveAsToRepositoryCommand

        private RelayCommand _saveAsToRepositoryCommand;

        /// <summary>
        ///     Gets the SaveAsToRepositoryCommand command.
        /// </summary>
        public RelayCommand SaveAsToRepositoryCommand
        {
            get
            {
                if (_saveAsToRepositoryCommand == null)
                {
                    _saveAsToRepositoryCommand = new RelayCommand(
                        p => SaveAsToRepository_Executed(),
                        p => SaveAsToRepository_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAsToRepository,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    RegisterCommand(_saveAsToRepositoryCommand);
                }
                return _saveAsToRepositoryCommand;
            }
        }

        private Boolean SaveAsToRepository_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.SelectedItem == null)
            {
                SaveAsToRepositoryCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedItem.IsValid)
            {
                this.SaveAsToRepositoryCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAsToRepository_Executed()
        {
            //** confirm the name to save as
            String copyName = null;

            #region check the item unique name
            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (PlanogramExportTemplateInfo info in _planogramExportTemplateInfoListView.Model)
                       {
                           if (info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       base.ShowWaitCursor(false);

                       return returnValue;
                   };


            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;
            #endregion

            //Copy the item and rename
            base.ShowWaitCursor(true);

            PlanogramExportTemplate itemCopy = this.SelectedItem.Copy();
            itemCopy.Name = copyName;
            this.SelectedItem = itemCopy;

            try
            {
                //Make sure the entityId is set
                itemCopy.EntityId = App.ViewState.EntityId;
                this.SelectedItem = itemCopy.SaveAs();
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                CommonHelper.RecordException(ex);
                CommonHelper.GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }
            //refresh the info list
            _planogramExportTemplateInfoListView.FetchForCurrentEntity();

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveAsToFileCommand

        private RelayCommand _saveAsToFileCommand;

        /// <summary>
        ///     Gets the SaveAsToFileCommand command.
        /// </summary>
        public RelayCommand SaveAsToFileCommand
        {
            get
            {
                if (_saveAsToFileCommand == null)
                {
                    _saveAsToFileCommand = new RelayCommand(
                        p => SaveAsToFile_Executed(p),
                        p => SaveAsToFile_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAsToFile,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    RegisterCommand(_saveAsToFileCommand);
                }
                return _saveAsToFileCommand;
            }
        }

        private Boolean SaveAsToFile_CanExecute()
        {
            //may not be null
            if (this.SelectedItem == null)
            {
                this.SaveAsToFileCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedItem.IsValid)
            {
                this.SaveAsToFileCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAsToFile_Executed(Object args)
        {
            SaveAsToFile(args as String);
        }

        private void SaveAsToFile(String filePath = null)
        {
            PlanogramExportTemplate itemToSave = this.SelectedItem;

            if (String.IsNullOrEmpty(filePath))
            {
                //show dialog to get path
                Boolean result = GetWindowService().ShowSaveFileDialog(
                    this.SelectedItem.Name,
                    App.ViewState.GetSessionDirectory(SessionDirectory.PlanogramExportFileTemplate),
                    String.Format(CultureInfo.InvariantCulture, Message.PlanogramFileTemplateEditor_ExportFilter, PlanogramExportTemplate.FileExtension),
                    out filePath);

                if (!result) return;

                //update the session directory.
                App.ViewState.SetSessionDirectory(SessionDirectory.PlanogramExportFileTemplate, Path.GetDirectoryName(filePath));
            }

            base.ShowWaitCursor(true);

            //save
            try
            {
                //update the item name to the chosen file one.
                itemToSave.Name = Path.GetFileNameWithoutExtension(filePath);

                this.SelectedItem = itemToSave.SaveAsFile(filePath);

                //unlock immediately.
                PlanogramExportTemplate.UnlockPlanogramExportTemplateByFileName(filePath);
            }
            catch (Exception ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Save);
                return;
            }
            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        ///     Gets the SaveAndNew command.
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    RegisterCommand(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        ///     Gets the SaveAndClose command.
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    RegisterCommand(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed close the window.
            if (itemSaved)
            {
                CloseWindow();
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        ///     Gets the Delete command.
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(),
                        p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_itemPermissions.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.SelectedItem == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.SelectedItem.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            //confirm with user
            if (!GetWindowService().ConfirmDeleteWithUser(this.SelectedItem.ToString())) return;

            ShowWaitCursor(true);

            //mark the item as deleted so item changed does not show.
            PlanogramExportTemplate itemToDelete = this.SelectedItem;
            itemToDelete.Delete();


            //load a new item
            NewCommand.Execute();

            //commit the delete
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);
                return;
            }

            //update the available items
            _planogramExportTemplateInfoListView.FetchForCurrentEntity();

            ShowWaitCursor(false);

        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        ///     Gets the Close command.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    RegisterCommand(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            //close the window
            CloseWindow();
        }

        #endregion

        #region ClearAllMappingsCommand

        private RelayCommand _clearAllMappingsCommand;

        /// <summary>
        /// Clears all assigned mappings
        /// </summary>
        public RelayCommand ClearAllMappingsCommand
        {
            get
            {
                if (_clearAllMappingsCommand == null)
                {
                    _clearAllMappingsCommand = new RelayCommand(
                        p => ClearAllMappings_Executed())
                    {
                        FriendlyName = Message.PlanogramExportTemplateMaintenance_ClearAllMappings
                    };
                    RegisterCommand(_clearAllMappingsCommand);
                }
                return _clearAllMappingsCommand;
            }
        }

        private void ClearAllMappings_Executed()
        {
            foreach (MappingRow row in _mappingRows)
            {
                row.ExternalField = null;
            }
        }

        #endregion

        #region ClearMappingCommand

        private RelayCommand _clearMappingCommand;

        /// <summary>
        /// Clears the mapping of the given row.
        /// </summary>
        public RelayCommand ClearMappingCommand
        {
            get
            {
                if (_clearMappingCommand == null)
                {
                    _clearMappingCommand = new RelayCommand(
                        p => ClearMapping_Executed(p))
                    {
                        FriendlyName = Message.PlanogramExportTemplateMaintenance_ClearMapping,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_clearMappingCommand);
                }
                return _clearMappingCommand;
            }
        }

        private void ClearMapping_Executed(Object args)
        {
            MappingRow row = args as MappingRow;
            if (row == null) return;

            row.ExternalField = null;
        }

        #endregion

        #region NewMetricCommand

        private RelayCommand _newMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand NewMetricCommand
        {
            get
            {
                if (_newMetricCommand == null)
                {
                    _newMetricCommand = new RelayCommand(
                        p => NewMetric_Executed(),
                        p => NewMetric_CanExecute())
                    {
                        FriendlyName = Message.Generic_Add,
                        FriendlyDescription = Message.PlanogramExportTemplateMaintenance_AddMetric_Description,
                        SmallIcon = ImageResources.MetricMaintenance_AddMetric,
                        DisabledReason = Message.PlanogramExportTemplateMaintenance_AddMetric_DisabledReason
                    };
                    RegisterCommand(_newMetricCommand);
                }
                return _newMetricCommand;
            }
        }

        private Boolean NewMetric_CanExecute()
        {
            return PerformanceMetrics.Count < 20;
        }

        private void NewMetric_Executed()
        {
            //Adds a new metric.
            SelectedPerformanceMetric = new PerformanceMetricRow(PlanogramExportTemplatePerformanceMetric.NewPlanogramExportTemplatePerformanceMetric(), _externalFieldDict[PlanogramFieldMappingType.Performance], null);
            SelectedPerformanceMetric.PerformanceMetric.MetricId = (Byte)(PerformanceMetrics.Count + 1);
            this.SelectedItem.PerformanceMetrics.Add(SelectedPerformanceMetric.PerformanceMetric);
            _performanceMetricRows.Add(SelectedPerformanceMetric);

            //show window
            _metricWindow = new PlanogramExportTemplateMaintenancePerformanceMetricWindow(this, true);
            GetWindowService().ShowDialog<PlanogramExportTemplateMaintenancePerformanceMetricWindow>(_metricWindow);
        }

        #endregion

        #region ViewMetricCommand

        private RelayCommand _viewMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand ViewMetricCommand
        {
            get
            {
                if (_viewMetricCommand == null)
                {
                    _viewMetricCommand = new RelayCommand(
                        p => ViewMetric_Executed(),
                        p => ViewMetric_CanExecute())
                    {
                        FriendlyName = Message.PlanogramExportTemplateMaintenance_EditMetric,
                        FriendlyDescription = Message.PlanogramExportTemplateMaintenance_EditMetric_Description,
                        SmallIcon = ImageResources.MetricMaintenance_EditMetric,
                        DisabledReason = Message.PlanogramExportTemplateMaintenance_EditMetric_DisabledReason
                    };
                    RegisterCommand(_viewMetricCommand);
                }
                return _viewMetricCommand;
            }
        }

        private Boolean ViewMetric_CanExecute()
        {
            return this.SelectedPerformanceMetric != null;
        }

        private void ViewMetric_Executed()
        {
            _metricWindow = new PlanogramExportTemplateMaintenancePerformanceMetricWindow(this, false);
            GetWindowService().ShowDialog<PlanogramExportTemplateMaintenancePerformanceMetricWindow>(_metricWindow);
        }

        #endregion

        #region RemoveMetricCommand

        private RelayCommand _removeMetricCommand;

        /// <summary>
        /// Creates a new metric
        /// </summary>
        public RelayCommand RemoveMetricCommand
        {
            get
            {
                if (_removeMetricCommand == null)
                {
                    _removeMetricCommand = new RelayCommand(
                        p => RemoveMetric_Executed(),
                        p => RemoveMetric_CanExecute())
                    {
                        FriendlyName = Message.Generic_Remove,
                        FriendlyDescription = Message.PlanogramExportTemplateMaintenance_RemoveMetric_Description,
                        SmallIcon = ImageResources.MetricMaintenance_RemoveMetric,
                        DisabledReason = Message.PlanogramExportTemplateMaintenance_RemoveMetric_DisabledReason
                    };
                    RegisterCommand(_removeMetricCommand);
                }
                return _removeMetricCommand;
            }
        }

        private Boolean RemoveMetric_CanExecute()
        {
            return this.SelectedPerformanceMetric != null;
        }

        private void RemoveMetric_Executed()
        {
            this.SelectedItem.PerformanceMetrics.Remove(this.SelectedPerformanceMetric.PerformanceMetric);
            _performanceMetricRows.Remove(this.SelectedPerformanceMetric);

            Byte metricId = 1;
            foreach (PerformanceMetricRow perfRow in _performanceMetricRows)
            {
                perfRow.PerformanceMetric.MetricId = metricId;
                metricId++;
            }

            this.SelectedPerformanceMetric = null;
        }

        #endregion

        #region MetricSaveCommand

        private RelayCommand _metricSaveCommand;

        public RelayCommand MetricSaveCommand
        {
            get
            {
                if (_metricSaveCommand == null)
                {
                    _metricSaveCommand = new RelayCommand(
                        p => MetricSave_Executed(),
                        p => MetricSave_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                        DisabledReason = Message.PlanogramExportTemplateMaintenance_MetricSave_DisabledReason
                    };
                }
                return _metricSaveCommand;
            }
        }

        private Boolean MetricSave_CanExecute()
        {
            if (SelectedPerformanceMetric == null) return false;
            return SelectedPerformanceMetric.PerformanceMetric.IsValid;
        }

        private void MetricSave_Executed()
        {
            if (_metricWindow != null)
            {
                _metricWindow.Close();
                _metricWindow = null;
            }
        }

        #endregion

        #region MetricCancelCommand

        private RelayCommand _metricCancelCommand;

        public RelayCommand MetricCancelCommand
        {
            get
            {
                if (_metricCancelCommand == null)
                {
                    _metricCancelCommand = new RelayCommand(p => MetricCancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                }
                return _metricCancelCommand;
            }
        }

        private void MetricCancel_Executed()
        {
            if (_metricWindow != null)
            {
                // If New Metric, remove from list
                if (_metricWindow.NewMetric)
                {
                    this.SelectedItem.PerformanceMetrics.Remove(SelectedPerformanceMetric.PerformanceMetric);
                    _performanceMetricRows.Remove(this.SelectedPerformanceMetric);
                    SelectedPerformanceMetric = null;
                }
                else // Replace metric with original
                {
                    SelectedPerformanceMetric.PerformanceMetric.CopyValues(_metricWindow.OriginalMetric);
                    PlanogramExportTemplateFieldInfo planogramExportTemplateFieldInfo = SelectedPerformanceMetric.AvailableExternalFields.Where(p => p.Field == _metricWindow.OriginalMetric.ExternalField).FirstOrDefault();
                    if (planogramExportTemplateFieldInfo != null && SelectedPerformanceMetric.ExternalField != planogramExportTemplateFieldInfo)
                    {
                        SelectedPerformanceMetric.ExternalField = planogramExportTemplateFieldInfo;
                    }
                }

                _metricWindow.Close();
                _metricWindow = null;
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(this.SelectedItem, this.SaveCommand);
        }


        private void RefreshMappingRows()
        {
            if (_mappingRows.Count > 0) _mappingRows.Clear();
            if (_performanceMetricRows.Count > 0) _performanceMetricRows.Clear();

            //update the external field dict
            var dict = new Dictionary<PlanogramFieldMappingType, ReadOnlyCollection<PlanogramExportTemplateFieldInfo>>();

            PlanogramExportTemplateFieldInfoList externalFieldList =
                PlanogramExportTemplateFieldInfoList.NewPlanogramExportTemplateFieldInfoList(this.SelectedFileType, this.SelectedVersion);

            foreach (var mapGroup in externalFieldList.GroupBy(f => f.FieldType))
            {
                dict.Add(mapGroup.Key, mapGroup.ToList().AsReadOnly());
            }
            _externalFieldDict = dict;


            if (this.SelectedItem != null)
            {
                var ccmFieldList = PlanogramExportTemplateFieldInfoList.NewPlanogramExportTemplateFieldInfoList(null, null);

                foreach (var mappingGroup in this.SelectedItem.Mappings.GroupBy(g => g.FieldType))
                {
                    ReadOnlyCollection<PlanogramExportTemplateFieldInfo> availableExternal = _externalFieldDict[mappingGroup.Key];
                    var externalLookup = availableExternal.ToDictionary(f => f.Field);

                    foreach (PlanogramExportTemplateMapping mapping in mappingGroup)
                    {
                        PlanogramExportTemplateFieldInfo ccmField = ccmFieldList.FirstOrDefault(c => c.Field == mapping.Field && c.FieldType == mapping.FieldType);
                        if (ccmField == null) continue;

                        PlanogramExportTemplateFieldInfo externalField = null;
                        externalLookup.TryGetValue(mapping.ExternalField, out externalField);
                        _mappingRows.Add(new MappingRow(mapping, ccmField, availableExternal, externalField));

                    }
                }

                ReadOnlyCollection<PlanogramExportTemplateFieldInfo> availablePerformanceExternal = _externalFieldDict[PlanogramFieldMappingType.Performance];
                var externalPerformanceLookup = availablePerformanceExternal.ToDictionary(f => f.Field);

                // create performance rows
                foreach (PlanogramExportTemplatePerformanceMetric performanceMetric in this.SelectedItem.PerformanceMetrics)
                {
                    PlanogramExportTemplateFieldInfo externalField = null;
                    externalPerformanceLookup.TryGetValue(performanceMetric.ExternalField, out externalField);
                    _performanceMetricRows.Add(new PerformanceMetricRow(performanceMetric, availablePerformanceExternal, externalField));
                }
            }

            //fire off all related property changes
            OnPropertyChanged(PlanogramMappingsProperty);
            OnPropertyChanged(BayMappingsProperty);
            OnPropertyChanged(ComponentMappingsProperty);
            OnPropertyChanged(ProductMappingsProperty);
        }

        /// <summary>
        /// Identifies double assignments and updates
        /// the double mapping flag for all rows.
        /// </summary>
        private void UpdateDoubleMappingFlags()
        {
            String[] doubleMaps =
                _mappingRows.GroupBy(m => (m.ExternalField != null) ? m.ExternalField.Field : null)
                .Where(g => g.Key != null && g.Count() > 1).Select(g => g.Key).ToArray();

            foreach (MappingRow row in _mappingRows)
            {
                row.IsDoubleMapped = (row.ExternalField != null && doubleMaps.Contains(row.ExternalField.Field));
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    OnSelectedItemChanged(this.SelectedItem, null);
                    _planogramExportTemplateInfoListView.Dispose();

                    DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }


    #region supporting class

    public sealed class MappingRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        private PlanogramExportTemplateMapping _sourceMapping;
        private PlanogramExportTemplateFieldInfo _ccmField;
        private IEnumerable<PlanogramExportTemplateFieldInfo> _availableExternalFields;
        private PlanogramExportTemplateFieldInfo _externalField;
        private Boolean _isDoubleMapped;
        private Boolean _isValid = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath FieldFriendlyNameProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.FieldFriendlyName);
        public static readonly PropertyPath AvailableExternalFieldsProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.AvailableExternalFields);
        public static readonly PropertyPath ExternalFieldProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.ExternalField);
        public static readonly PropertyPath IsDoubleMappedProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.IsDoubleMapped);
        public static readonly PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<MappingRow>(p => p.IsValid);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the type of mapping this row relates to.
        /// </summary>
        public PlanogramFieldMappingType MappingType
        {
            get { return _sourceMapping.FieldType; }
        }

        public String Field
        {
            get { return _sourceMapping.Field; }
        }

        public String FieldFriendlyName
        {
            get { return _ccmField.DisplayName; }
        }

        public IEnumerable<PlanogramExportTemplateFieldInfo> AvailableExternalFields
        {
            get { return _availableExternalFields; }
        }

        public PlanogramExportTemplateFieldInfo ExternalField
        {
            get { return _externalField; }
            set
            {
                _externalField = value;
                OnPropertyChanged(ExternalFieldProperty);

                _sourceMapping.ExternalField = (value != null) ? value.Field : null;
            }
        }

        public Boolean IsDoubleMapped
        {
            get { return _isDoubleMapped; }
            set
            {
                if (_isDoubleMapped != value)
                {
                    _isDoubleMapped = value;
                    OnPropertyChanged(IsDoubleMappedProperty);
                }
            }
        }

        public Boolean IsValid
        {
            get { return _isValid; }
            set
            {
                _isValid = value;
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public MappingRow(PlanogramExportTemplateMapping mapping, PlanogramExportTemplateFieldInfo ccmField,
            IEnumerable<PlanogramExportTemplateFieldInfo> availableFields, PlanogramExportTemplateFieldInfo selectedField)
        {
            _sourceMapping = mapping;
            _ccmField = ccmField;
            _availableExternalFields = availableFields;
            this.ExternalField = selectedField;
            IsValid = ValidMapping();
        }

        #endregion

        #region Methods
        private Boolean ValidMapping()
        {
            if ((MappingType == PlanogramFieldMappingType.Product && (Field == "Name" || Field == "Gtin")) ||
                (MappingType == PlanogramFieldMappingType.Component && (Field == "Name")))
            {
                if (this.ExternalField == null)
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                String result = null;
                if (columnName == ExternalFieldProperty.Path)
                {
                    IsValid = ValidMapping();
                    if (!IsValid)
                    {
                        result = Message.PlanogramExportTemplateMaintenance_MappingRequired;
                    }
                }
                return result;
            }
        }

        #endregion
    }

    public sealed class PerformanceMetricRow : INotifyPropertyChanged, IDataErrorInfo
    {
        #region Fields

        private PlanogramExportTemplatePerformanceMetric _performanceMetric;
        private IEnumerable<PlanogramExportTemplateFieldInfo> _availableExternalFields;
        private PlanogramExportTemplateFieldInfo _externalField;
        private Boolean _isValid = true;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AvailableExternalFieldsProperty = WpfHelper.GetPropertyPath<PerformanceMetricRow>(p => p.AvailableExternalFields);
        public static readonly PropertyPath ExternalFieldProperty = WpfHelper.GetPropertyPath<PerformanceMetricRow>(p => p.ExternalField);
        public static readonly PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<PerformanceMetricRow>(p => p.IsValid);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the type of mapping this row relates to.
        /// </summary>
        public PlanogramExportTemplatePerformanceMetric PerformanceMetric
        {
            get { return _performanceMetric; }
        }

        public IEnumerable<PlanogramExportTemplateFieldInfo> AvailableExternalFields
        {
            get { return _availableExternalFields; }
        }

        public PlanogramExportTemplateFieldInfo ExternalField
        {
            get { return _externalField; }
            set
            {
                _externalField = value;
                OnPropertyChanged(ExternalFieldProperty);

                _performanceMetric.ExternalField = (value != null) ? value.Field : null;
            }
        }

        public Boolean IsValid
        {
            get { return _isValid; }
            set
            {
                _isValid = value;
                OnPropertyChanged(IsValidProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public PerformanceMetricRow(PlanogramExportTemplatePerformanceMetric performanceMetric, IEnumerable<PlanogramExportTemplateFieldInfo> availableFields, PlanogramExportTemplateFieldInfo selectedField)
        {
            _performanceMetric = performanceMetric;
            _availableExternalFields = availableFields;
            this.ExternalField = selectedField;
            this.IsValid = ValidMapping();
        }

        #endregion

        #region Methods

        public Boolean ValidMapping()
        {
            return (this.ExternalField != null && !String.IsNullOrEmpty(this.ExternalField.Field));
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

        #region IDataErrorInfo
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public string this[string columnName]
        {
            get
            {
                String result = null;
                if (columnName == ExternalFieldProperty.Path)
                {
                    IsValid = ValidMapping();
                    if (!IsValid)
                    {
                        result = Message.PlanogramExportTemplateMaintenance_MappingRequired;
                    }
                }
                return result;
            }
        }
        #endregion
    }

    #endregion
}