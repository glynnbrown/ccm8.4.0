﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// V8-32790 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramExportTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramExportTemplateMaintenanceHomeTab.xaml
    /// </summary>
    public partial class PlanogramExportTemplateMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramExportTemplateMaintenanceViewModel), typeof(PlanogramExportTemplateMaintenanceHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public PlanogramExportTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramExportTemplateMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramExportTemplateMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
