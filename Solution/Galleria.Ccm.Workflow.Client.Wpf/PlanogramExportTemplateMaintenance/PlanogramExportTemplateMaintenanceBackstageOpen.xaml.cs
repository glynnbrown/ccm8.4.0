﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.3.0)
// V8-32790 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.ViewModel;
using System.Windows.Media;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramExportTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramExportTemplateMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class PlanogramExportTemplateMaintenanceBackstageOpen
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(PlanogramExportTemplateMaintenanceViewModel), typeof(PlanogramExportTemplateMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        public PlanogramExportTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramExportTemplateMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramExportTemplateMaintenanceBackstageOpen()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void XSearchResults_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                PlanogramExportTemplateInfo itemToOpen = (PlanogramExportTemplateInfo)senderControl.SelectedItem;

                if (itemToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                }
            }
        }

        #endregion
    }
}