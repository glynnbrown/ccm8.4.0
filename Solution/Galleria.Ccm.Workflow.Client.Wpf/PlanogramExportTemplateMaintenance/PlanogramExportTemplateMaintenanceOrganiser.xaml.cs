﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.1)
// V8-32790 : M.Pettit
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.ComponentModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramExportTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramExportTemplateMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class PlanogramExportTemplateMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        const String _clearMappingCommandKey = "ClearMappingCommand";
        const String _clearAllMappingsCommandKey = "ClearAllMappingsCommand";
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramExportTemplateMaintenanceViewModel), typeof(PlanogramExportTemplateMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public PlanogramExportTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramExportTemplateMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramExportTemplateMaintenanceOrganiser senderControl = (PlanogramExportTemplateMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                PlanogramExportTemplateMaintenanceViewModel oldModel = (PlanogramExportTemplateMaintenanceViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
                senderControl.Resources.Remove(_clearMappingCommandKey);
                senderControl.Resources.Remove(_clearAllMappingsCommandKey);
            }

            if (e.NewValue != null)
            {
                PlanogramExportTemplateMaintenanceViewModel newModel = (PlanogramExportTemplateMaintenanceViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
                senderControl.Resources.Add(_clearMappingCommandKey, newModel.ClearMappingCommand);
                senderControl.Resources.Add(_clearAllMappingsCommandKey, newModel.ClearAllMappingsCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramExportTemplateMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link - TODO : No help File yet
            //HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramExportTemplateMaintenance);

            this.ViewModel = new PlanogramExportTemplateMaintenanceViewModel(Galleria.Framework.Planograms.Model.PlanogramExportFileType.Spaceman);

            this.Loaded += new RoutedEventHandler(PlanogramExportTemplateMaintenanceOrganiser_Loaded);

            this.ViewModel.PropertyChanged +=ViewModel_PropertyChanged;
        }

        private void PlanogramExportTemplateMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramExportTemplateMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region methods

        private void ScrollGridToTop(DependencyObject obj)
        {
            var grid = obj as ExtendedDataGrid;
            if (grid != null)
            {
                var scrollViewer = grid.ScrollViewerChild as ScrollViewer;
                if (scrollViewer != null)
                {
                    scrollViewer.ScrollToTop();
                }
            }
        }

        #endregion

        #region Event Handlers

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanogramExportTemplateMaintenanceViewModel.PlanogramMappingsProperty.Path)
            {
                ScrollGridToTop(xPlanogramMappingsGrid);
            }
            else if (e.PropertyName == PlanogramExportTemplateMaintenanceViewModel.BayMappingsProperty.Path)
            {
                ScrollGridToTop(xBayMappingsGrid);
            }
            else if (e.PropertyName == PlanogramExportTemplateMaintenanceViewModel.ComponentMappingsProperty.Path)
            {
                ScrollGridToTop(xComponentMappingsGrid);
            }
            else if (e.PropertyName == PlanogramExportTemplateMaintenanceViewModel.ProductMappingsProperty.Path)
            {
                ScrollGridToTop(xProductMappingsGrid);
            }
        }

        /// <summary>
        /// calles when ever the keys O+Ctrl is pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        private void OnPerformanceMetricDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            var command = ViewModel.ViewMetricCommand;
            if (command.CanExecute()) command.Execute();
        }

        #endregion

        #region Window Close

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion


    }
}