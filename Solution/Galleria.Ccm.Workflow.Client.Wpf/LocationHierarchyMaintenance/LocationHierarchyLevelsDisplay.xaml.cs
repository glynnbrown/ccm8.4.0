﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;



namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    /// <summary>
    /// Display for the product hierarchy levels
    /// </summary>
    public partial class LocationHierarchyLevelsDisplay : UserControl
    {
        #region Fields
        private bool _supressSelectionChanged;
        #endregion

        #region Properties

        #region RootLevelProperty

        public static readonly DependencyProperty RootLevelProperty =
            DependencyProperty.Register("RootLevel", typeof(LocationLevel),
            typeof(LocationHierarchyLevelsDisplay),
            new PropertyMetadata(null, OnRootLevelPropertyChanged));

        /// <summary>
        /// Gets/Sets the diagram root level
        /// </summary>
        public LocationLevel RootLevel
        {
            get { return (LocationLevel)GetValue(RootLevelProperty); }
            set { SetValue(RootLevelProperty, value); }
        }


        private static void OnRootLevelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationHierarchyLevelsDisplay senderControl = (LocationHierarchyLevelsDisplay)obj;

            if (e.OldValue != null)
            {
                LocationLevel oldValue = (LocationLevel)e.OldValue;
                oldValue.ChildChanged -= senderControl.RootLevel_ChildChanged;
                oldValue.PropertyChanged -= senderControl.RootLevel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                LocationLevel newValue = (LocationLevel)e.NewValue;
                newValue.ChildChanged += senderControl.RootLevel_ChildChanged;
                newValue.PropertyChanged += senderControl.RootLevel_PropertyChanged;
            }

            senderControl.OnRootNodeChanged();
        }

        #endregion

        #region SelectedLevelProperty

        public static readonly DependencyProperty SelectedLevelProperty =
            DependencyProperty.Register("SelectedLevel", typeof(LocationLevel),
            typeof(LocationHierarchyLevelsDisplay),
            new PropertyMetadata(null, OnSelectedLevelPropertyChanged));

        public LocationLevel SelectedLevel
        {
            get { return (LocationLevel)GetValue(SelectedLevelProperty); }
            set { SetValue(SelectedLevelProperty, value); }
        }

        private static void OnSelectedLevelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationHierarchyLevelsDisplay senderControl = (LocationHierarchyLevelsDisplay)obj;
            senderControl.OnSelectedLevelChanged();
        }

        private void OnSelectedLevelChanged()
        {
            //dont do anything if we are supressed
            if (_supressSelectionChanged) { return; }

            _supressSelectionChanged = true;

            //update the diagram selected level to match the property here
            LocationHierarchyLevelNode diagramSelectedNodeBase =
                (diagram.SelectedItems.Count > 0) ?
                (LocationHierarchyLevelNode)diagram.SelectedItems.First() : null;
            
            if ((diagramSelectedNodeBase == null) || // SA-14465
                 (diagramSelectedNodeBase.LevelContext != this.SelectedLevel))
            {
                diagram.SelectedItems.Clear();

                LocationHierarchyLevelNode nodeToSelect =
                    diagram.Items.Cast<LocationHierarchyLevelNode>()
                    .FirstOrDefault(n => n.LevelContext == this.SelectedLevel);

                if (nodeToSelect != null)
                {
                    diagram.SelectedItems.Add(nodeToSelect);
                }
            }

            _supressSelectionChanged = false;

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationHierarchyLevelsDisplay()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to change of selected node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void diagram_NodeSelectionChanged(object sender, EventArgs e)
        {
            if (!_supressSelectionChanged)
            {
                _supressSelectionChanged = true;

                //update the property to match the diagram
                LocationHierarchyLevelNode diagramSelectedNodeBase = (LocationHierarchyLevelNode)diagram.SelectedItems.FirstOrDefault();
                if (diagramSelectedNodeBase != null)
                {
                    this.SelectedLevel = diagramSelectedNodeBase.LevelContext;
                }

                _supressSelectionChanged = false;
            }
        }


        private void RootLevel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationLevel.ChildLevelProperty)
            {
                //redraw the root node
                OnRootNodeChanged();
            }
        }

        private void RootLevel_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            //looking for a childproperty changed
            if (e.PropertyChangedArgs != null)
            {
                if (e.PropertyChangedArgs.PropertyName == LocationLevel.ChildLevelProperty)
                {
                    diagram.IsAutoArrangeOn = false;

                    LocationLevel changedChildLevel = (LocationLevel)e.ChildObject;
                    LocationHierarchyLevelNode changedChildNode =
                        diagram.Items.Cast<LocationHierarchyLevelNode>().FirstOrDefault(n => n.LevelContext == changedChildLevel);

                    //draw the levels from and including that child
                    DrawChildLevels(changedChildNode);
                    diagram.ArrangeDiagram();

                    diagram.IsAutoArrangeOn = true;
                }
            }
        }

        private void OnRootNodeChanged()
        {
            diagram.Items.Clear();

            if (this.RootLevel != null)
            {
                diagram.IsAutoArrangeOn = false;

                //draw the root node
                LocationHierarchyLevelNode rootLevelNode = new LocationHierarchyLevelNode(this.RootLevel);
                diagram.Items.Add(rootLevelNode);

                //draw all children down
                DrawChildLevels(rootLevelNode);


                diagram.IsAutoArrangeOn = true;
            }
        }

        #endregion

        #region Methods

        private void DrawChildLevels(LocationHierarchyLevelNode parent)
        {
            //clear any existing decendents
            LocalHelper.ClearDescendantVisuals(diagram, parent);

            //get the child
            LocationLevel childLevel = parent.LevelContext.ChildLevel;

            if (childLevel != null)
            {
                //draw the child
                LocationHierarchyLevelNode childLevelNode = new LocationHierarchyLevelNode(childLevel);
                diagram.Items.Add(childLevelNode);
                diagram.Links.Add(new DiagramItemLink(parent, childLevelNode));

                //recurse with the child as the new parent
                DrawChildLevels(childLevelNode);
            }
        }

        #endregion
    }
}
