﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{

    public sealed partial class LocationHierarchyItemsAssignedWarning : ExtendedRibbonWindow
    {
        #region Fields

        private LocationHierarchy _locationHierarchy;

        #endregion

        #region Properties

        #region LocationGroupNameProperty

        public static readonly DependencyProperty LocationGroupNameProperty =
            DependencyProperty.Register("LocationGroupName", typeof(String), typeof(LocationHierarchyItemsAssignedWarning));

        public String LocationGroupName
        {
            get { return (String)GetValue(LocationGroupNameProperty); }
            set { SetValue(LocationGroupNameProperty, value); }
        }

        #endregion

        #region AvailableGroupsProperty

        public static readonly DependencyProperty AvailableGroupsProperty =
            DependencyProperty.Register("AvailableGroups", typeof(IList<LocationGroupViewModel>),
            typeof(LocationHierarchyItemsAssignedWarning));

        /// <summary>
        /// Gets/Sets the collection of available groups
        /// </summary>
        public IList<LocationGroupViewModel> AvailableGroups
        {
            get { return (IList<LocationGroupViewModel>)GetValue(AvailableGroupsProperty); }
            set { SetValue(AvailableGroupsProperty, value); }
        }

        #endregion

        #region SelectedGroupProperty

        public static readonly DependencyProperty SelectedGroupProperty =
            DependencyProperty.Register("SelectedGroup", typeof(LocationGroupViewModel), typeof(LocationHierarchyItemsAssignedWarning),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the selected group
        /// </summary>
        public LocationGroupViewModel SelectedGroup
        {
            get { return (LocationGroupViewModel)GetValue(SelectedGroupProperty); }
            set { SetValue(SelectedGroupProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationHierarchyItemsAssignedWarning(LocationHierarchy hierarchy)
        {
            _locationHierarchy = hierarchy;
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            ReloadColumns();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Reloads level grid columns
        /// </summary>
        private void ReloadColumns()
        {
            if (flattenedGrid != null)
            {
                //clear out the existing columns
                flattenedGrid.Columns.Clear();

                //add in the new columns for the hierarchy
                Boolean includeRootColumn = this.AvailableGroups.Any(g=> g.LocationGroup.IsRoot);
                List<DataGridColumn> columnSet = DataObjectViewHelper.GetLocationGroupColumnSet(_locationHierarchy, includeRootColumn);
                if (columnSet.Count > 0)
                {
                    columnSet.ForEach(c => flattenedGrid.Columns.Add(c));
                }
            }
        }

        #endregion
    }
}
