﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    /// <summary>
    /// Interaction logic for LocationHierarchyLocationAssign.xaml
    /// </summary>
    public sealed partial class LocationHierarchyLocationAssign : ExtendedRibbonWindow
    {
        #region constants
        const String _productAssignSetRowCommandKey = "LocationHierarchyLocationAssign_SetRowLocationGroupCommandKey";
        #endregion

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationHierarchyLocationAssignViewModel),
            typeof(LocationHierarchyLocationAssign),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationHierarchyLocationAssign senderControl = (LocationHierarchyLocationAssign)obj;

            if (e.OldValue != null)
            {
                LocationHierarchyLocationAssignViewModel oldModel = (LocationHierarchyLocationAssignViewModel)e.OldValue;
                oldModel.AttachedControl = null;

                senderControl.Resources.Remove(_productAssignSetRowCommandKey);
            }

            if (e.NewValue != null)
            {
                LocationHierarchyLocationAssignViewModel newModel = (LocationHierarchyLocationAssignViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

                senderControl.Resources.Remove(_productAssignSetRowCommandKey);
                senderControl.Resources.Add(_productAssignSetRowCommandKey, newModel.SetRowLocationGroupCommand);
            }
        }

        public LocationHierarchyLocationAssignViewModel ViewModel
        {
            get { return (LocationHierarchyLocationAssignViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }


        #endregion

        #region Constructor


        /// <summary>
        /// Constructor
        /// </summary>
        public LocationHierarchyLocationAssign()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new LocationHierarchyLocationAssignViewModel();

            this.Loaded += new RoutedEventHandler(LocationHierarchyLocationAssign_Loaded);
        }

        private void LocationHierarchyLocationAssign_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationHierarchyLocationAssign_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        private void ExtendedDataGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.OpenLocationRowCommand.Execute(e.RowItem);
            }
        }

        private void UIElement_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                this.ViewModel.OpenLocationRowCommand.Execute(grid.CurrentItem);
        }
        #endregion

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }

        }

        #endregion
    }
}
