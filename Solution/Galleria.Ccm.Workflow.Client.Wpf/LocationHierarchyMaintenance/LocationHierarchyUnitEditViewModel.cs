﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    public sealed class LocationHierarchyUnitEditViewModel
        : ViewModelAttachedControlObject<LocationHierarchyUnitEditWindow>, IDataErrorInfo
    {
        #region Fields
        private readonly LocationHierarchy _structure;
        private readonly Dictionary<String, Int32> _dbDeletedGroupCodesLookup = new Dictionary<String,Int32>(StringComparer.OrdinalIgnoreCase);

        private LocationGroup _selectedGroup;

        private readonly BulkObservableCollection<LocationLevel> _availableLevels = new BulkObservableCollection<LocationLevel>();
        private ReadOnlyBulkObservableCollection<LocationLevel> _availableLevelsRO;
        private LocationLevel _selectedLevel;

        private BulkObservableCollection<LocationGroup> _availableParents = new BulkObservableCollection<LocationGroup>();
        private ReadOnlyBulkObservableCollection<LocationGroup> _availableParentsRO;
        private LocationGroup _selectedParent;

        #endregion

        #region Binding Property Paths
        //properties
        public static readonly PropertyPath SelectedGroupProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.SelectedGroup);
        public static readonly PropertyPath AvailableLevelsProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.AvailableLevels);
        public static readonly PropertyPath SelectedLevelProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.SelectedLevel);
        public static readonly PropertyPath AvailableParentsProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.AvailableParents);
        public static readonly PropertyPath SelectedParentProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.SelectedParent);
        public static readonly PropertyPath GroupNameProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.GroupName);
        public static readonly PropertyPath GroupCodeProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.GroupCode);

        //commands
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath ApplyAndNewCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUnitEditViewModel>(p => p.ApplyAndNewCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the selected group context
        /// </summary>
        public LocationGroup SelectedGroup
        {
            get { return _selectedGroup; }
            private set
            {
                LocationGroup oldValue = _selectedGroup;

                _selectedGroup = value;
                OnPropertyChanged(SelectedGroupProperty);

                OnSelectedGroupChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the collection of levels available in the hierarchy
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationLevel> AvailableLevels
        {
            get
            {
                if (_availableLevelsRO == null)
                {
                    _availableLevelsRO = new ReadOnlyBulkObservableCollection<LocationLevel>(_availableLevels);
                }
                return _availableLevelsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the level at which the selected group should be placed.
        /// </summary>
        public LocationLevel SelectedLevel
        {
            get { return _selectedLevel; }
            set
            {
                _selectedLevel = value;
                OnPropertyChanged(SelectedLevelProperty);

                OnSelectedLevelChanged(value);

                Debug.Assert((value != null) ? this.AvailableLevels.Contains(value) : true, "Value is not an available level");
            }
        }

        /// <summary>
        /// Returns the collection of available parents
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationGroup> AvailableParents
        {
            get
            {
                if (_availableParentsRO == null)
                {
                    _availableParentsRO = new ReadOnlyBulkObservableCollection<LocationGroup>(_availableParents);
                }
                return _availableParentsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the parent group to which the selected group should be assigned.
        /// </summary>
        public LocationGroup SelectedParent
        {
            get { return _selectedParent; }
            set
            {
                _selectedParent = value;
                OnPropertyChanged(SelectedParentProperty);
                OnSelectedParentChanged(value);

                Debug.Assert((value != null) ? _availableParents.Contains(value) : true, "Value is not an available parent");
            }
        }

        /// <summary>
        ///Gets/Sets the name of the selected product group
        /// </summary>
        public String GroupName
        {
            get
            {
                if (_selectedGroup != null)
                {
                    return _selectedGroup.Name;
                }
                return null;
            }
            set
            {
                if (_selectedGroup != null)
                {
                    _selectedGroup.Name = value;
                    OnPropertyChanged(GroupNameProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the code of the selected product group
        /// </summary>
        public String GroupCode
        {
            get
            {
                if (_selectedGroup != null)
                {
                    return _selectedGroup.Code;
                }
                return null;
            }
            set
            {
                if (_selectedGroup != null)
                {
                    _selectedGroup.Code = value;
                    OnPropertyChanged(GroupCodeProperty);
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="hierarchyView"></param>
        /// <param name="selectedGroup"></param>
        public LocationHierarchyUnitEditViewModel(LocationHierarchy hierarchy, LocationGroup selectedGroup)
        {
            _structure = hierarchy;
            Debug.Assert((selectedGroup != null) ? (selectedGroup.ParentHierarchy == _structure) : true, "SelectedGroup does not belong to the given hierarchy");

            //get the collection of deleted groups for code check
            List<Int32> activeGroupIds = _structure.EnumerateAllGroups().Select(g => g.Id).Distinct().ToList();
            foreach (LocationGroupInfo dbGroupInfo in LocationGroupInfoList.FetchDeletedByLocationHierarchyId(hierarchy.Id))
            {
                //if the group is deleted in the db or not present amongst the active hierarchy groups
                // then add it to the deleted groups lookup.
                if (dbGroupInfo.DateDeleted.HasValue || !activeGroupIds.Contains(dbGroupInfo.Id))
                {
                    _dbDeletedGroupCodesLookup.Add(dbGroupInfo.Code, dbGroupInfo.Id);
                }
            }



            UpdateAvailableLevels();

            //set the selected group
            if (selectedGroup == null)
            {
                this.SelectedGroup = LocationGroup.NewLocationGroup(_availableLevels.FirstOrDefault().Id);
                this.SelectedGroup.Code = hierarchy.GetNextAvailableDefaultGroupCode();
            }
            else
            {
                this.SelectedGroup = selectedGroup;
            }
        }

        #endregion

        #region Commands

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;

        /// <summary>
        /// Commits the edit made. If the item is new, adds it to its chosen parent.
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndCloseCommand_Executed(),
                        p => ApplyAndCloseCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ApplyAndCloseCommand_CanExecute()
        {
            // must be valid.
            if (!this.SelectedGroup.IsValid)
            {
                this.ApplyAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must have a parent group if it's not the root
            if (!this.SelectedGroup.IsRoot &&
                this.SelectedParent == null)
            {
                this.ApplyAndCloseCommand.DisabledReason = Message.Hierarchy_InvalidParentGroup;
                return false;
            }

            //code must not have an error
            if (!String.IsNullOrEmpty(((IDataErrorInfo)this)[GroupCodeProperty.Path]))
            {
                this.ApplyAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have no binding errors
            if (!LocalHelper.AreBindingsValid(this.AttachedControl))
            {
                this.ApplyAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void ApplyAndCloseCommand_Executed()
        {
            ApplyChanges();

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region ApplyAndNewCommand

        private RelayCommand _applyAndNewCommand;

        /// <summary>
        /// Calls the apply command then loads a new item.
        /// </summary>
        public RelayCommand ApplyAndNewCommand
        {
            get
            {
                if (_applyAndNewCommand == null)
                {
                    _applyAndNewCommand = new RelayCommand(
                        p => ApplyAndNewCommand_Executed(),
                        p => ApplyAndNewCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndNew
                    };
                    base.ViewModelCommands.Add(_applyAndNewCommand);
                }
                return _applyAndNewCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ApplyAndNewCommand_CanExecute()
        {
            //apply and close must be enabled.
            if (!this.ApplyAndCloseCommand.CanExecute())
            {
                this.ApplyAndNewCommand.DisabledReason = this.ApplyAndCloseCommand.DisabledReason;
                return false;
            }

            //user must have create perm?


            //there must be a selectedlevel
            //otherwise we can't add the item at any level
            if (this.SelectedLevel == null)
            {
                this.ApplyAndNewCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void ApplyAndNewCommand_Executed()
        {
            LocationGroup parentGroup = this.SelectedParent;

            ApplyChanges();

            //load a new group for the selected parent
            this.SelectedGroup = LocationGroup.NewLocationGroup(this.SelectedLevel.Id);
            this.SelectedGroup.Code = parentGroup.ParentHierarchy.GetNextAvailableDefaultGroupCode();
            this.SelectedParent = parentGroup;
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected group
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedGroupChanged(LocationGroup oldValue, LocationGroup newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedGroup_PropertyChanged;

                //cancel the existing edit
                oldValue.CancelEdit();
            }


            if (newValue != null)
            {
                newValue.PropertyChanged += SelectedGroup_PropertyChanged;

                if (!newValue.IsRoot)
                {
                    //update the selected level and parent
                    this.SelectedLevel = _availableLevels.First(l => l.Id == _selectedGroup.LocationLevelId);
                    this.SelectedParent = (newValue.ParentGroup != null) ? newValue.ParentGroup : _availableParents.First();
                }
                else
                {
                    //set both to null as group is the root
                    this.SelectedLevel = null;
                    this.SelectedParent = null;
                }

                //begin an edit
                newValue.BeginEdit();
            }

            OnPropertyChanged(GroupNameProperty);
            OnPropertyChanged(GroupCodeProperty);
        }

        private void SelectedGroup_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationGroup.NameProperty.Name)
            {
                OnPropertyChanged(GroupNameProperty);
            }
            else if (e.PropertyName == LocationGroup.CodeProperty.Name)
            {
                OnPropertyChanged(GroupCodeProperty);
            }
        }

        /// <summary>
        /// Responds to a change of selected level
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedLevelChanged(LocationLevel newValue)
        {
            //load the available parents list
            if (_availableParents.Count > 0)
            {
                _availableParents.Clear();
            }

            if (newValue != null)
            {
                //parents are on the level above that selected.
                Int32 parentLevelId = newValue.ParentLevel.Id;

                //add all available parents to the collection
                LocationGroup selectedGroup = this.SelectedGroup;

                Boolean isNewGroup = this.SelectedGroup == null || (selectedGroup.ParentGroup == null);
                foreach (LocationGroup group in _structure.EnumerateAllGroups())
                {
                    //if the group is for the correct level
                    if (Object.Equals(group.LocationLevelId, parentLevelId))
                    {
                        if (
                            //is a new group and the parent may be added to
                            (isNewGroup && String.IsNullOrEmpty(LocationHierarchy.IsAddGroupAllowed(group)))

                            //or group is being moved and parent is valid.
                            || (!isNewGroup && String.IsNullOrEmpty(LocationHierarchy.IsMoveGroupAllowed(selectedGroup, group)))

                          )
                        {
                            _availableParents.Add(group);
                        }
                    }
                }

                //select a parent if required
                if (!_availableParents.Contains(this.SelectedParent))
                {
                    this.SelectedParent = _availableParents.FirstOrDefault();
                }

            }
        }

        /// <summary>
        /// Responds to a change of selected parent
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedParentChanged(LocationGroup newValue)
        {
            if (newValue != null)
            {
                //get the level of the parent
                LocationLevel parentLevel = _structure.EnumerateAllLevels().FirstOrDefault(l => l.Id == newValue.LocationLevelId);

                if (this.SelectedLevel != parentLevel.ChildLevel)
                {
                    //set the selected level as the child level
                    this.SelectedLevel = parentLevel.ChildLevel;
                }
            }
        }

        /// <summary>
        /// Populates the available levels collection
        /// </summary>
        private void UpdateAvailableLevels()
        {
            LocationLevel preSelectedLevel = this.SelectedLevel;


            if (_availableLevels.Count > 0)
            {
                _availableLevels.Clear();
            }


            //get available levels with all except root and those without parent groups
            IEnumerable<LocationGroup> groups = _structure.EnumerateAllGroups();
            List<LocationLevel> validLevels = new List<LocationLevel>();
            foreach (LocationLevel level in _structure.EnumerateAllLevels())
            {
                Boolean isValidLevel = (!level.IsRoot);
                if (isValidLevel)
                {
                    isValidLevel = (groups.Any(g => Object.Equals(g.LocationLevelId, level.ParentLevel.Id)));
                }

                if (isValidLevel)
                {
                    validLevels.Add(level);
                }

            }

            //populate the collection.
            _availableLevels.AddRange(validLevels);

            //reselect the level from before
            this.SelectedLevel = (this.AvailableLevels.Contains(preSelectedLevel)) ? preSelectedLevel : this.AvailableLevels.LastOrDefault();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies changes made to the current item
        /// </summary>
        private void ApplyChanges()
        {
            LocationGroup group = this.SelectedGroup;

            if (group != null)
            {
                if (!group.IsRoot)
                {
                    group.LocationLevelId = this.SelectedLevel.Id;

                    //apply the item edit
                    group.ApplyEdit();

                    //use the model helper to set the group against the correct parent.
                    LocationHierarchy.AssignGroupParent(group, this.SelectedParent);

                    //begin a new edit
                    group.BeginEdit();

                    //update the available levels in case this as changed them
                    UpdateAvailableLevels();
                }
                else
                {
                    //if a root group just apply the edit and begin a new one.
                    group.ApplyEdit();
                    group.BeginEdit();
                }
            }
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.SelectedGroup = null;
                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                if (this.SelectedGroup != null)
                {
                    if (columnName == GroupNameProperty.Path)
                    {
                        return (SelectedGroup as IDataErrorInfo)[LocationGroup.NameProperty.Name];
                    }
                    else if (columnName == GroupCodeProperty.Path)
                    {
                        String codeError = (SelectedGroup as IDataErrorInfo)[LocationGroup.CodeProperty.Name];
                        if (String.IsNullOrEmpty(codeError))
                        {
                            LocationGroup currentGroup = this.SelectedGroup;
                            String newCodeValue = this.GroupCode;

                            LocationGroup matchingGroup =
                                _structure.EnumerateAllGroups().FirstOrDefault(
                                g => g != currentGroup && String.Compare(g.Code, newCodeValue, StringComparison.OrdinalIgnoreCase) == 0);
                            if (matchingGroup != null)
                            {
                                codeError = Message.LocationHierarchyUnitEdit_GroupCodeNotUnique;
                            }
                            else
                            {
                                Int32 matchingGroupId;
                                if (_dbDeletedGroupCodesLookup.TryGetValue(newCodeValue, out matchingGroupId))
                                {
                                    if (currentGroup.Id != matchingGroupId)
                                    {
                                        codeError = Message.LocationHierarchyUnitEdit_GroupCodeNotUnique;
                                    }
                                }
                            }

                        }

                        return codeError;
                    }
                }
                return null;
            }
        }

        #endregion
    }


}
