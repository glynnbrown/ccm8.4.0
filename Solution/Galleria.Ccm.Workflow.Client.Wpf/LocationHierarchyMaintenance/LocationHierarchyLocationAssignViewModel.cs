﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using System.ComponentModel;


namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    /// <summary>
    /// ViewModel controller for the LocationHierarchyLocationAssign window.
    /// </summary>
    public sealed class LocationHierarchyLocationAssignViewModel : ViewModelAttachedControlObject<LocationHierarchyLocationAssign>
    {
        #region Fields

        private LocationHierarchy _masterHierarchy;
        private ReadOnlyCollection<Int32> _groupExcludeIds;

        private LocationGroup _selectedFilterLocationGroup;
        private LocationList _currentLocList;

        private readonly BulkObservableCollection<LocationGroupAssignmentRow> _rows = new BulkObservableCollection<LocationGroupAssignmentRow>();
        private ReadOnlyBulkObservableCollection<LocationGroupAssignmentRow> _rowsRO;

        private readonly ObservableCollection<LocationGroupAssignmentRow> _selectedRows = new ObservableCollection<LocationGroupAssignmentRow>();

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath SelectedFilterLocationGroupProperty = WpfHelper.GetPropertyPath<LocationHierarchyLocationAssignViewModel>(p => p.SelectedFilterLocationGroup);
        public static readonly PropertyPath RowsProperty = WpfHelper.GetPropertyPath<LocationHierarchyLocationAssignViewModel>(p => p.Rows);
        public static readonly PropertyPath SelectedRowsProperty = WpfHelper.GetPropertyPath<LocationHierarchyLocationAssignViewModel>(p => p.SelectedRows);


        //Commands
        public static readonly PropertyPath SelectFilterLocationGroupCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyLocationAssignViewModel>(p => p.SelectFilterLocationGroupCommand);
        public static readonly PropertyPath SetRowLocationGroupCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyLocationAssignViewModel>(p => p.SetRowLocationGroupCommand);
        public static readonly PropertyPath SetSelectedRowsLocationGroupCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyLocationAssignViewModel>(p => p.SetSelectedRowsLocationGroupCommand);
        public static readonly PropertyPath OpenLocationRowCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyLocationAssignViewModel>(p => p.OpenLocationRowCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyLocationAssignViewModel>(p => p.CloseCommand);


        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the selected product group to show products for.
        /// </summary>
        public LocationGroup SelectedFilterLocationGroup
        {
            get { return _selectedFilterLocationGroup; }
            set
            {
                _selectedFilterLocationGroup = value;
                OnPropertyChanged(SelectedFilterLocationGroupProperty);

                OnSelectedFilterLocationGroupChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of product rows available to edit.
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationGroupAssignmentRow> Rows
        {
            get
            {
                if (_rowsRO == null)
                {
                    _rowsRO = new ReadOnlyBulkObservableCollection<LocationGroupAssignmentRow>(_rows);
                }
                return _rowsRO;
            }
        }

        /// <summary>
        /// Gets the editable collection of selected rows
        /// </summary>
        public ObservableCollection<LocationGroupAssignmentRow> SelectedRows
        {
            get { return _selectedRows; }
        }

        #endregion

        #region Constructor

        public LocationHierarchyLocationAssignViewModel()
        {
            UpdateLocationGroupExcludeIds();
        }

        #endregion

        #region Commands

        #region SelectFilterLocationGroup

        private RelayCommand _selectFilterLocationGroupCommand;

        /// <summary>
        /// Shows the dialog to allow the selecetdfilterproductgroup property to be set.
        /// </summary>
        public RelayCommand SelectFilterLocationGroupCommand
        {
            get
            {
                if (_selectFilterLocationGroupCommand == null)
                {
                    _selectFilterLocationGroupCommand = new RelayCommand(
                        p => SelectFilterLocationGroupCommand_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_selectFilterLocationGroupCommand);
                }
                return _selectFilterLocationGroupCommand;
            }
        }

        private void SelectFilterLocationGroupCommand_Executed()
        {
            if (this.AttachedControl != null)
            {
                //show the select window.
                LocationGroupSelectionWindow win = new LocationGroupSelectionWindow(_masterHierarchy, _groupExcludeIds);
                win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                App.ShowWindow(win, this.AttachedControl, true);

                if (win.DialogResult == true
                    && win.SelectionResult != null
                    && win.SelectionResult.Count == 1)
                {
                    this.SelectedFilterLocationGroup = win.SelectionResult[0];
                }
            }
        }

        #endregion

        #region SetRowLocationGroupCommand

        private RelayCommand _setRowLocationGroupCommand;

        /// <summary>
        /// Set the product group for an individual row.
        /// </summary>
        public RelayCommand SetRowLocationGroupCommand
        {
            get
            {
                if (_setRowLocationGroupCommand == null)
                {
                    _setRowLocationGroupCommand = new RelayCommand(
                        p => SetRowLocationGroupId_Executed(p))
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                    };
                    base.ViewModelCommands.Add(_setRowLocationGroupCommand);
                }
                return _setRowLocationGroupCommand;
            }
        }

        private void SetRowLocationGroupId_Executed(Object arg)
        {
            LocationGroupAssignmentRow row = arg as LocationGroupAssignmentRow;
            if (row != null)
            {
                LocationGroup newGroup = null;

                if (this.AttachedControl != null)
                {

                    //show the select window.
                    LocationGroupSelectionWindow win = new LocationGroupSelectionWindow(_masterHierarchy, _groupExcludeIds);
                    win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                    App.ShowWindow(win, this.AttachedControl, true);

                    if (win.DialogResult == true
                        && win.SelectionResult != null
                        && win.SelectionResult.Count == 1)
                    {
                        newGroup = win.SelectionResult[0];
                    }
                }
                else
                {
                    //for testing
                    newGroup = _masterHierarchy.EnumerateAllGroups().FirstOrDefault(g => g.Id == row.LocationGroupId);
                }

                if (newGroup != null)
                {
                    //update the row & save the change immediately
                    base.ShowWaitCursor(true);

                    try
                    {
                        //update the location that the row relates to.
                        Int32 locId = row.LocationId;
                        Location loc = _currentLocList.FirstOrDefault(l => l.Id == locId);
                        if (loc != null)
                        {
                            loc.LocationGroupId = newGroup.Id;
                        }

                        //save the loc list
                        _currentLocList = _currentLocList.Save();

                        //update the row.
                        row.UpdateLocationGroup(newGroup);
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        LocalHelper.RecordException(ex, LocationHierarchyUIViewModel._exceptionCategory);
                        Exception rootException = ex.GetBaseException();

                        //otherwise just show the user an error has occurred.
                        if (this.AttachedControl != null)
                        {
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                row.Name, OperationType.Save, this.AttachedControl);
                        }
                    }

                    base.ShowWaitCursor(false);
                }
            }
        }

        #endregion

        #region SetSelectedRowsLocationGroupCommand

        private RelayCommand _setSelectedRowsLocationGroupCommand;

        /// <summary>
        /// Sets the selected product group id against all the selected rows.
        /// </summary>
        public RelayCommand SetSelectedRowsLocationGroupCommand
        {
            get
            {
                if (_setSelectedRowsLocationGroupCommand == null)
                {
                    _setSelectedRowsLocationGroupCommand = new RelayCommand(
                        p => SetSelectedRowsLocationGroup_Executed(),
                        p => SetSelectedRowsLocationGroup_CanExecute())
                    {
                        FriendlyName = Message.LocationHierarchy_LocationAssignment_SetSelected
                    };
                    base.ViewModelCommands.Add(_setSelectedRowsLocationGroupCommand);
                }
                return _setSelectedRowsLocationGroupCommand;
            }

        }

        private Boolean SetSelectedRowsLocationGroup_CanExecute()
        {
            if (this.SelectedRows.Count == 0)
            {
                this.SetSelectedRowsLocationGroupCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void SetSelectedRowsLocationGroup_Executed()
        {
            List<LocationGroupAssignmentRow> rows = this.SelectedRows.ToList();
            if (rows.Count > 0)
            {
                LocationGroup newGroup = null;

                if (this.AttachedControl != null)
                {

                    //show the select window.
                    LocationGroupSelectionWindow win = new LocationGroupSelectionWindow(_masterHierarchy, _groupExcludeIds);
                    win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                    App.ShowWindow(win, this.AttachedControl, true);

                    if (win.DialogResult == true
                        && win.SelectionResult != null
                        && win.SelectionResult.Count == 1)
                    {
                        newGroup = win.SelectionResult[0];
                    }
                }
                else
                {
                    //for testing
                    newGroup = _masterHierarchy.EnumerateAllGroups().FirstOrDefault(g => g.Id == rows.First().LocationGroupId);
                }

                if (newGroup != null)
                {
                    base.ShowWaitCursor(true);

                    //update the row & save the change immediately
                    try
                    {
                        //update all affected locations
                        List<Int16> selectedLocIds = rows.Select(r => r.LocationId).ToList();
                        foreach (Location loc in _currentLocList)
                        {
                            if (selectedLocIds.Contains(loc.Id))
                            {
                                loc.LocationGroupId = newGroup.Id;
                            }
                        }

                        //save the loc list
                        _currentLocList = _currentLocList.Save();

                        //update all rows.
                        rows.ForEach(r => r.UpdateLocationGroup(newGroup));
                    }
                    catch (DataPortalException ex)
                    {
                        base.ShowWaitCursor(false);

                        LocalHelper.RecordException(ex, LocationHierarchyUIViewModel._exceptionCategory);
                        Exception rootException = ex.GetBaseException();

                        //otherwise just show the user an error has occurred.
                        if (this.AttachedControl != null)
                        {
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                newGroup.Name, OperationType.Save, this.AttachedControl);
                        }
                    }

                    base.ShowWaitCursor(false);
                }
            }

        }

        #endregion

        #region OpenLocationRowCommand

        private RelayCommand _openLocationRowCommand;

        /// <summary>
        /// Opens the product represented by the given row
        /// Param: LocationGroupAssignmentRow row to open
        /// </summary>
        public RelayCommand OpenLocationRowCommand
        {
            get
            {
                if (_openLocationRowCommand == null)
                {
                    _openLocationRowCommand = new RelayCommand(
                        p => OpenLocation_Executed(p),
                        p => OpenLocationRow_CanExecute(p))
                    {
                    };
                    base.ViewModelCommands.Add(_openLocationRowCommand);
                }
                return _openLocationRowCommand;
            }
        }

        private Boolean OpenLocationRow_CanExecute(Object arg)
        {
            LocationGroupAssignmentRow row = arg as LocationGroupAssignmentRow;

            //param must be valid.
            if (row == null)
            {
                this.OpenLocationRowCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void OpenLocation_Executed(Object arg)
        {
            LocationGroupAssignmentRow row = arg as LocationGroupAssignmentRow;
            if (row != null)
            {
                App.MainPageViewModel.ShowLocationMaintenanceCommand.Execute();
                
                LocationMaintenance.LocationMaintenanceOrganiser locationOrg =
                    Galleria.Framework.Controls.Wpf.Helpers.GetWindow<LocationMaintenance.LocationMaintenanceOrganiser>();
                if (locationOrg != null)
                {
                    locationOrg.ViewModel.OpenCommand.Execute(row.LocationId);
                }

                //get the screen and attach to its closed event
                locationOrg.Closed += LocationWin_Closed;
            }
        }

        private void LocationWin_Closed(object sender, EventArgs e)
        {
            Window win = (Window)sender;
            win.Closed -= LocationWin_Closed;

            //refresh the filter results.
            OnSelectedFilterLocationGroupChanged(this.SelectedFilterLocationGroup);
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the attached window.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Generic_Close
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Reponds to a change of selected filter product group.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedFilterLocationGroupChanged(LocationGroup newValue)
        {
            //clear any existing rows
            if (_rows.Count > 0) { _rows.Clear(); }


            if (newValue != null)
            {
                base.ShowWaitCursor(true);

                LocationList matchingLocations = null;
                try
                {
                    matchingLocations = LocationList.FetchByLocationGroupId(newValue.Id);
                    _currentLocList = matchingLocations;
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, LocationHierarchyUIViewModel._exceptionCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }

                if (matchingLocations != null)
                {
                    List<LocationGroupAssignmentRow> newRows = new List<LocationGroupAssignmentRow>();
                    foreach (Location info in matchingLocations)
                    {
                        newRows.Add(new LocationGroupAssignmentRow(info, newValue));
                    }
                    _rows.AddRange(newRows);
                }

                base.ShowWaitCursor(false);
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the list of product group ids to be excluded from the available selection list.
        /// </summary>
        private void UpdateLocationGroupExcludeIds()
        {
            //refetch the heirarchy
            _masterHierarchy = LocationHierarchy.FetchByEntityId(App.ViewState.EntityId);

            //add all groups that are not leaf nodes to the exclude list
            List<Int32> excludeIds = new List<Int32>();
            foreach (LocationGroup group in _masterHierarchy.EnumerateAllGroups())
            {
                if (group.ChildList.Count > 0)
                {
                    excludeIds.Add(group.Id);
                }
            }
            _groupExcludeIds = new ReadOnlyCollection<Int32>(excludeIds);
        }

        #endregion

        #region IDisposabled

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {


                base.IsDisposed = true;
            }
        }

        #endregion
    }

    #region Supporting Classes

    public sealed class LocationGroupAssignmentRow : INotifyPropertyChanged
    {
        #region Fields
        private Int32 _locGroupId;
        private String _locGroupName;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CodeProperty = WpfHelper.GetPropertyPath<LocationGroupAssignmentRow>(p => p.Code);
        public static readonly PropertyPath NameProperty = WpfHelper.GetPropertyPath<LocationGroupAssignmentRow>(p => p.Name);
        public static readonly PropertyPath LocationGroupIdProperty = WpfHelper.GetPropertyPath<LocationGroupAssignmentRow>(p => p.LocationGroupId);
        public static readonly PropertyPath LocationGroupNameProperty = WpfHelper.GetPropertyPath<LocationGroupAssignmentRow>(p => p.LocationGroupName);

        #endregion

        #region Properties

        public Int16 LocationId { get; private set; }

        public String Code { get; private set; }

        public String Name { get; private set; }

        public String LocationGroupName
        {
            get { return _locGroupName; }
            private set
            {
                _locGroupName = value;
                OnPropertyChanged(LocationGroupNameProperty);
            }
        }

        public Int32 LocationGroupId
        {
            get { return _locGroupId; }
            private set
            {
                _locGroupId = value;
                OnPropertyChanged(LocationGroupIdProperty);
            }
        }

        #endregion

        #region Constructor

        public LocationGroupAssignmentRow(Location loc, LocationGroup productGroup)
        {
            this.LocationId = loc.Id;
            this.Code = loc.Code;
            this.Name = loc.Name;

            UpdateLocationGroup(productGroup);
        }

        #endregion

        #region Methods

        public void UpdateLocationGroup(LocationGroup newLocationGroup)
        {
            this.LocationGroupId = newLocationGroup.Id;
            this.LocationGroupName = newLocationGroup.ToString();
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion

    }

    #endregion
}
