﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    /// <summary>
    /// Interaction logic for LocationHierarchyHomeTab.xaml
    /// </summary>
    public partial class LocationHierarchyHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationHierarchyUIViewModel), typeof(LocationHierarchyHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public LocationHierarchyUIViewModel ViewModel
        {
            get { return (LocationHierarchyUIViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationHierarchyHomeTab senderControl = (LocationHierarchyHomeTab)obj;

            if (e.OldValue != null)
            {
                LocationHierarchyUIViewModel oldModel = (LocationHierarchyUIViewModel)e.OldValue;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                LocationHierarchyUIViewModel newModel = (LocationHierarchyUIViewModel)e.NewValue;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;

                if (newModel.SelectedLevel != null)
                {
                    senderControl.SelectedShapeIndex = (byte)(newModel.SelectedLevel.ShapeNo - 1);
                }

            }
        }

        #endregion

        #region Selected Shape Index Property

        public static readonly DependencyProperty SelectedShapeIndexProperty =
            DependencyProperty.Register("SelectedShapeIndex", typeof(Byte), typeof(LocationHierarchyHomeTab),
            new PropertyMetadata((Byte)0, OnSelectedShapeIndexPropertyChanged));

        /// <summary>
        /// Gets/Sets the selected shape index
        /// </summary>
        public Byte SelectedShapeIndex
        {
            get { return (Byte)GetValue(SelectedShapeIndexProperty); }
            set { SetValue(SelectedShapeIndexProperty, value); }
        }


        private static void OnSelectedShapeIndexPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationHierarchyHomeTab senderControl = (LocationHierarchyHomeTab)obj;

            if (senderControl.ViewModel != null)
            {
                Byte newValue = (Byte)((Byte)e.NewValue + 1);
                if (senderControl.ViewModel.SelectedLevel.ShapeNo != newValue)
                {
                    senderControl.ViewModel.SelectedLevel.ShapeNo = newValue;
                }
            }
        }


        #endregion

        #endregion

        #region Constructor

        public LocationHierarchyHomeTab()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the viewmodel property changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationHierarchyUIViewModel.SelectedLevelProperty.Path)
            {
                if (this.ViewModel.SelectedLevel != null)
                {
                    if (this.ViewModel.SelectedLevel.ShapeNo != 0)
                    {
                        this.SelectedShapeIndex = (byte)(this.ViewModel.SelectedLevel.ShapeNo - 1);
                    }
                    else
                    {
                        this.SelectedShapeIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Responds to the shape gallery selection change as two way binding does not seem to be working.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShapeGallery_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Fluent.Gallery senderControl = (Fluent.Gallery)sender;
            this.SelectedShapeIndex = (Byte)senderControl.SelectedIndex;
        }

        #endregion
    }
}
