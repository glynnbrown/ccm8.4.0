﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
// CCM-25807 : N.Haywood
//  Changed MoveSelectedUnitCommand to add levels as required when moving groups with children
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    /// <summary>
    /// ViewModel controller for the location hierarchy maintenance screen
    /// </summary>
    public sealed class LocationHierarchyUIViewModel : ViewModelAttachedControlObject<LocationHierarchyOrganiser>
    {
        #region Fields

        public static String _exceptionCategory = "LocationHierarchyMaintenance";

        private readonly LocationHierarchyViewModel _structureViewModel = new LocationHierarchyViewModel();

        private readonly BulkObservableCollection<LocationLevel> _flattenedLevels = new BulkObservableCollection<LocationLevel>();
        private ReadOnlyBulkObservableCollection<LocationLevel> _flattenedLevelsRO;
        private LocationLevel _selectedLevel;


        private readonly BulkObservableCollection<LocationGroupViewModel> _flattenedUnits = new BulkObservableCollection<LocationGroupViewModel>();
        private ReadOnlyBulkObservableCollection<LocationGroupViewModel> _flattenedUnitsRO;
        private LocationGroupViewModel _selectedUnit;

        private Boolean _allGroupsValid;
        private String _originalCode;
        private String _duplicateCode;

        private Boolean _suppressHierarchyChangeHandler;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CurrentStructureProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.CurrentStructure);
        public static readonly PropertyPath FlattenedLevelsProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.FlattenedLevels);
        public static readonly PropertyPath SelectedLevelProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.SelectedLevel);
        public static readonly PropertyPath FlattenedUnitsProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.FlattenedUnits);
        public static readonly PropertyPath SelectedUnitProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.SelectedUnit);

        //commands
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath AddLevelCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.AddLevelCommand);
        public static readonly PropertyPath RemoveLevelCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.RemoveLevelCommand);
        public static readonly PropertyPath AddUnitCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.AddUnitCommand);
        public static readonly PropertyPath RemoveUnitCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.RemoveUnitCommand);
        public static readonly PropertyPath EditUnitCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.EditUnitCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath ExpandAllCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.ExpandAllCommand);
        public static readonly PropertyPath CollapseAllCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.CollapseAllCommand);
        public static readonly PropertyPath ShowLocationAssignmentCommandProperty = WpfHelper.GetPropertyPath<LocationHierarchyUIViewModel>(p => p.ShowLocationAssignmentCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current structure being edited
        /// </summary>
        public LocationHierarchy CurrentStructure
        {
            get { return _structureViewModel.Model; }
        }

        /// <summary>
        /// Returns the flattened collection of levels within the structure
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationLevel> FlattenedLevels
        {
            get
            {
                if (_flattenedLevelsRO == null)
                {
                    _flattenedLevelsRO =
                        new ReadOnlyBulkObservableCollection<LocationLevel>(_flattenedLevels);
                }
                return _flattenedLevelsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the current selected level
        /// </summary>
        public LocationLevel SelectedLevel
        {
            get { return _selectedLevel; }
            set
            {
                _selectedLevel = value;
                OnPropertyChanged(SelectedLevelProperty);
                OnSelectedLevelChanged();
            }
        }

        /// <summary>
        /// Returns the flattened collection of units within the structure
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationGroupViewModel> FlattenedUnits
        {
            get
            {
                if (_flattenedUnitsRO == null)
                {
                    _flattenedUnitsRO = new ReadOnlyBulkObservableCollection<LocationGroupViewModel>(_flattenedUnits);
                }
                return _flattenedUnitsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the current selected unit
        /// </summary>
        public LocationGroupViewModel SelectedUnit
        {
            get { return _selectedUnit; }
            set
            {
                _selectedUnit = value;
                OnPropertyChanged(SelectedUnitProperty);
                OnSelectedUnitChanged();
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LocationHierarchyUIViewModel()
        {
            UpdatePermissionFlags();

            _flattenedLevels.BulkCollectionChanged += FlattenedLevels_BulkCollectionChanged;

            _structureViewModel.ModelChanged += StructureViewModel_ModelChanged;
            _structureViewModel.FetchForCurrentEntity();
        }

        #endregion

        #region Commands

        #region Save Command

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the hierarchy
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //Edit must be allowed
            if (!_userHasLocationHierarchyEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //The model must be valid
            if (!_structureViewModel.Model.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            //Do not allow duplicate group codes
            if (!_allGroupsValid)
            {
                this.SaveCommand.DisabledReason = String.Format(Message.Hierarchy_CodeDuplicate, _duplicateCode, _originalCode);
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            base.ShowWaitCursor(true);
            _suppressHierarchyChangeHandler = true;

            try
            {
                _structureViewModel.Save();
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                continueWithSave = false;

                LocalHelper.RecordException(ex, _exceptionCategory);
                Exception rootException = ex.GetBaseException();

                //GFS-22254 if it is the duplicate key row exception inform the user 
                if (rootException is DuplicateException)
                {
                    //Inform user that a duplicate already exists
                    LocalHelper.ShowDuplicateExceptionMessage(this.AttachedControl, (DuplicateException)rootException);
                }
                //if it is a concurrency exception check if the user wants to reload
                else if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.CurrentStructure.Name);
                    if (itemReloadRequired)
                    {
                        _structureViewModel.FetchForCurrentEntity();
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    if (this.AttachedControl != null)
                    {
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.CurrentStructure.Name, OperationType.Save, this.AttachedControl);
                    }
                }

                base.ShowWaitCursor(true);
            }

            _suppressHierarchyChangeHandler = false;
            base.ShowWaitCursor(false);

            return continueWithSave;
        }

        #endregion

        #region SaveAndClose Command

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current structure and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => SaveAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private Boolean SaveAndClose_CanExecute()
        {
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAndCloseCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }

        }
        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region ExpandAllCommand

        private RelayCommand _expandAllCommand;
        /// <summary>
        /// expandAlls the current maintenance form
        /// </summary>
        public RelayCommand ExpandAllCommand
        {
            get
            {
                if (_expandAllCommand == null)
                {
                    _expandAllCommand = new RelayCommand(p => ExpandAll_Executed())
                    {
                        FriendlyName = Message.HierarchyMaintenance_ExpandAll,
                        FriendlyDescription = Message.HierarchyMaintenance_ExpandAll_Desc,
                        Icon = ImageResources.HierarchyMaintenance_ExpandAll,
                    };
                    base.ViewModelCommands.Add(_expandAllCommand);
                }
                return _expandAllCommand;
            }
        }

        private void ExpandAll_Executed()
        {
            //handled in code behind
        }

        #endregion

        #region CollapseAllCommand

        private RelayCommand _collapseAllCommand;
        /// <summary>
        /// minimizeAlls the current maintenance form
        /// </summary>
        public RelayCommand CollapseAllCommand
        {
            get
            {
                if (_collapseAllCommand == null)
                {
                    _collapseAllCommand = new RelayCommand(p => CollapseAll_Executed())
                    {
                        FriendlyName = Message.HierarchyMaintenance_CollapseAll,
                        FriendlyDescription = Message.HierarchyMaintenance_CollapseAll_Desc,
                        Icon = ImageResources.HierarchyMaintenance_CollapseAll,
                    };
                    base.ViewModelCommands.Add(_collapseAllCommand);
                }
                return _collapseAllCommand;
            }
        }

        private void CollapseAll_Executed()
        {
            //handled in code behind
        }

        #endregion

        #region AddUnitCommand

        private RelayCommand _addUnitCommand;

        /// <summary>
        /// Shows the dialog to add a new unit.
        /// </summary>
        public RelayCommand AddUnitCommand
        {
            get
            {
                if (_addUnitCommand == null)
                {
                    _addUnitCommand = new RelayCommand(
                       p => AddUnit_Executed(), p => AddUnit_CanExecute())
                    {
                        FriendlyName = Message.LocationHierarchy_AddUnit,
                        FriendlyDescription = Message.LocationHierarchy_AddUnit_Description,
                        Icon = ImageResources.LocationHierarchy_AddUnit
                    };
                    base.ViewModelCommands.Add(_addUnitCommand);
                }
                return _addUnitCommand;
            }
        }

        private Boolean AddUnit_CanExecute()
        {
            //user has permission to create a group
            if (!_userHasLocationHierarchyEditPerm)
            {
                this.AddUnitCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            LocationGroup selectedGroup = (this.SelectedUnit != null) ? this.SelectedUnit.LocationGroup : null;

            //use the model helper to check if the action is legal
            String actionError = LocationHierarchy.IsAddGroupAllowed(selectedGroup);
            if (!(String.IsNullOrEmpty(actionError)))
            {
                this.AddUnitCommand.DisabledReason = actionError;
                return false;
            }


            //if selected node has locations
            //then user has permission to move locations
            if (this.SelectedUnit.LocationGroup.AssignedLocationsCount > 0 && !_userHasLocationEditPerm)
            {
                this.RemoveUnitCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            return true;
        }

        private void AddUnit_Executed()
        {
            _suppressHierarchyChangeHandler = true;

            LocationGroup selectedParentGroup = this.SelectedUnit.LocationGroup;

            //get the level on which the new unit will reside - do not add one if null.
            LocationLevel newUnitLevel = this.SelectedUnit.AssociatedLevel.ChildLevel;
            if (newUnitLevel != null)
            {
                if (this.AttachedControl != null)
                {
                    //show the edit node window
                    LocationHierarchyUnitEditWindow win = new LocationHierarchyUnitEditWindow(this.CurrentStructure);
                    win.ViewModel.SelectedParent = selectedParentGroup;
                    App.ShowWindow(win, this.AttachedControl, /*isModal*/true);
                }
                else
                {
                    //add a unit for testing purposes.
                    LocationGroup newProductGroup = LocationGroup.NewLocationGroup(newUnitLevel.Id);
                    newProductGroup.Code = this.CurrentStructure.GetNextAvailableDefaultGroupCode();
                    newProductGroup.Name = newProductGroup.Code;
                    selectedParentGroup.ChildList.Add(newProductGroup);
                }
            }

            //reset the heirarchy units collection
            RecreateFlattenedUnitsCollection();


            _suppressHierarchyChangeHandler = false;
        }

        #endregion

        #region EditUnitCommand

        private RelayCommand _editUnitCommand;

        /// <summary>
        /// Shows the properties window for the selected unit.
        /// </summary>
        public RelayCommand EditUnitCommand
        {
            get
            {
                if (_editUnitCommand == null)
                {
                    _editUnitCommand = new RelayCommand(
                        p => EditUnit_Executed(), p => EditUnit_CanExecute())
                    {
                        FriendlyName = Message.LocationHierarchy_ShowUnitEditWindow,
                        FriendlyDescription = Message.LocationHierarchy_ShowUnitEditWindow_FriendlyDescription,
                        Icon = ImageResources.LocationHierarchy_ShowUnitEditWindow
                    };
                    base.ViewModelCommands.Add(_editUnitCommand);
                }
                return _editUnitCommand;
            }
        }

        private Boolean EditUnit_CanExecute()
        {
            //must have a unit selected
            if (this.SelectedUnit == null)
            {
                this.EditUnitCommand.DisabledReason = Message.LocationHierarchy_UnitCommandDisabledReason;
                return false;
            }

            return true;
        }

        private void EditUnit_Executed()
        {
            _suppressHierarchyChangeHandler = true;

            if (this.AttachedControl != null)
            {
                //show the window as modal
                LocationHierarchyUnitEditWindow win = new LocationHierarchyUnitEditWindow(this.SelectedUnit.LocationGroup);
                App.ShowWindow(win, this.AttachedControl, /*isModal*/true);
            }


            //reset the heirarchy units collection
            RecreateFlattenedUnitsCollection();


            _suppressHierarchyChangeHandler = false;

        }

        #endregion

        #region RemoveUnitCommand

        private RelayCommand _removeUnitComamnd;

        /// <summary>
        /// Removes the currently selected group
        /// </summary>
        public RelayCommand RemoveUnitCommand
        {
            get
            {
                if (_removeUnitComamnd == null)
                {
                    _removeUnitComamnd = new RelayCommand(
                      p => RemoveUnit_Executed(), p => RemoveUnit_CanExecute())
                    {
                        FriendlyName = Message.LocationHierarchy_RemoveUnit,
                        FriendlyDescription = Message.LocationHierarchy_RemoveUnit_Description,
                        Icon = ImageResources.LocationHierarchy_RemoveUnit,
                        InputGestureKey = Key.Delete
                    };
                    base.ViewModelCommands.Add(_removeUnitComamnd);
                }
                return _removeUnitComamnd;
            }
        }

        private Boolean RemoveUnit_CanExecute()
        {
            //user has permission to remove a group
            if (!_userHasLocationHierarchyEditPerm)
            {
                this.RemoveUnitCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            LocationGroup selectedGroup = (this.SelectedUnit != null) ? this.SelectedUnit.LocationGroup : null;

            //check the action is legal
            String actionError = LocationHierarchy.IsRemoveGroupAllowed(selectedGroup);
            if (!String.IsNullOrEmpty(actionError))
            {
                this.RemoveUnitCommand.DisabledReason = actionError;
                return false;
            }


            //if the selected unit has products user must have permission to edit products
            if (this.SelectedUnit.LocationGroup.AssignedLocationsCount > 0 && !_userHasLocationEditPerm)
            {
                this.RemoveUnitCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            return true;
        }

        private void RemoveUnit_Executed()
        {
            _suppressHierarchyChangeHandler = true;

            LocationGroup groupToRemove = this.SelectedUnit.LocationGroup;
            List<LocationGroup> fullRemoveGroupsList = groupToRemove.EnumerateAllChildGroups().ToList();

            #region Deal with any assigned locations

            //calculate total assigned locations
            Int32 allLocationsCount = 0;
            foreach (Int32 count in fullRemoveGroupsList.Select(p => p.AssignedLocationsCount))
            {
                allLocationsCount += count;
            }

            //Show the move products window if node has child products
            if (allLocationsCount > 0)
            {

                // get the list of groups we can reassign the items to.
                //this is just leaf groups for the financial hierarchy
                List<LocationGroupViewModel> availableReassignGroups =
                    this.FlattenedUnits.Where(g =>
                        g.LocationGroup.ChildList.Count == 0
                        && g.LocationGroup.Code != groupToRemove.Code
                        && !fullRemoveGroupsList.Contains(g.LocationGroup)).ToList();

                LocationGroupViewModel parentGroupView = null;
                if (groupToRemove.ParentGroup.ChildList.Count == 1)
                {
                    parentGroupView = this.FlattenedUnits.FirstOrDefault(g => g.LocationGroup == groupToRemove.ParentGroup);
                    if (parentGroupView != null)
                    {
                        //add the parent of the group to remove if it was the only child
                        availableReassignGroups.Add(parentGroupView);
                    }
                }


                //indicate the best group to move to
                LocationGroupViewModel selectedMoveToGroup = parentGroupView;
                if (!availableReassignGroups.Contains(parentGroupView))
                {
                    selectedMoveToGroup = availableReassignGroups.FirstOrDefault();
                }

                Boolean shouldReassignItems = true;
                if (this.AttachedControl != null)
                {
                    //warn the user
                    LocationHierarchyItemsAssignedWarning msg = new LocationHierarchyItemsAssignedWarning(this.CurrentStructure)
                    {
                        LocationGroupName = groupToRemove.ToString(),
                        AvailableGroups = availableReassignGroups,
                        SelectedGroup = null,
                    };
                    App.ShowWindow(msg, this.AttachedControl, /*isModal*/true);

                    shouldReassignItems = (msg.DialogResult == true);
                    selectedMoveToGroup = msg.SelectedGroup;
                }

                if (shouldReassignItems)
                {
                    base.ShowWaitCursor(true);

                    //get all child locations
                    List<LocationGroupLocation> childLocationsList = new List<LocationGroupLocation>();
                    foreach (LocationGroup group in fullRemoveGroupsList)
                    {
                        foreach (LocationGroupLocation loc in group.AssignedLocations)
                        {
                            childLocationsList.Add(loc);
                        }
                    }

                    //add the locations to the move group and remove from the original parents.
                    selectedMoveToGroup.LocationGroup.AssignedLocations.AddList(childLocationsList);
                    foreach (LocationGroup group in fullRemoveGroupsList)
                    {
                        if (group.AssignedLocations.Count > 0)
                        {
                            group.AssignedLocations.Clear();
                        }
                    }


                    base.ShowWaitCursor(false);

                }
                else
                {
                    //action cancelled so just return out.
                    return;
                }

            }
            else if (groupToRemove.ChildList.Count > 0)
            {
                //+ just warn the user that child groups will also be removed.
                if (this.AttachedControl != null)
                {
                    ModalMessage msg = new ModalMessage()
                    {
                        Header = groupToRemove.Name,
                        Description =
                            String.Format(CultureInfo.CurrentCulture,
                                Message.Hierarchy_RemoveUnit_HasChildGroupsWarning,
                                groupToRemove.Name),
                        Button1Content = Message.Generic_Continue,
                        Button2Content = Message.Generic_Cancel,
                        ButtonCount = 2,
                        MessageIcon = ImageResources.DialogWarning
                    };
                    App.ShowWindow(msg, this.AttachedControl, true);
                    if (msg.Result != ModalMessageResult.Button1)
                    {
                        //cancel out.
                        return;
                    }
                }

            }



            #endregion

            base.ShowWaitCursor(true);

            String parentUnitCode = groupToRemove.ParentGroup.Code;

            //get a list of affected views.
            List<LocationGroupViewModel> viewsToRemove = new List<LocationGroupViewModel>();
            foreach (LocationGroup removeGroup in fullRemoveGroupsList)
            {
                LocationGroupViewModel view = _flattenedUnits.FirstOrDefault(g => g.LocationGroup == removeGroup);
                if (view != null)
                {
                    viewsToRemove.Add(view);
                }
            }


            //remove the unit from its parent
            groupToRemove.ParentGroup.ChildList.Remove(groupToRemove);

            //remove all affected group views
            _flattenedUnits.RemoveRange(viewsToRemove);


            base.ShowWaitCursor(false);


            //select the parent unit
            LocationGroupViewModel parentUnitView = _flattenedUnits.FirstOrDefault(g => g.LocationGroup.Code.Equals(parentUnitCode));
            this.SelectedUnit = parentUnitView;

            _suppressHierarchyChangeHandler = false;

        }

        #endregion

        #region MoveUnitCommand

        private RelayCommand _moveSelectedUnitCommand;

        /// <summary>
        /// Moves the currently selected unit to be a child of the given unit.
        /// Param: FinancialGroupView which is to be the new parent
        /// </summary>
        public RelayCommand MoveSelectedUnitCommand
        {
            get
            {
                if (_moveSelectedUnitCommand == null)
                {
                    _moveSelectedUnitCommand = new RelayCommand(
                        p => MoveSelectedUnit_Executed(p),
                        p => MoveSelectedUnit_CanExecute(p),
                        attachToCommandManager: false)
                    {
                        //nb -this command is never actually shown.
                    };
                    base.ViewModelCommands.Add(_moveSelectedUnitCommand);
                }
                return _moveSelectedUnitCommand;
            }
        }

        private Boolean MoveSelectedUnit_CanExecute(Object arg)
        {
            //user has permission to move a group
            if (!_userHasLocationHierarchyEditPerm)
            {
                this.MoveSelectedUnitCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }


            LocationGroupViewModel newParent = arg as LocationGroupViewModel;
            LocationGroup newParentGroup = (newParent != null) ? newParent.LocationGroup : null;
            LocationGroup selectedGroup = (this.SelectedUnit != null) ? this.SelectedUnit.LocationGroup : null;

            //check the move is legal
            String moveActionError = LocationHierarchy.IsMoveGroupAllowed(selectedGroup, newParentGroup);
            if (!String.IsNullOrEmpty(moveActionError))
            {
                this.MoveSelectedUnitCommand.DisabledReason = moveActionError;
                return false;
            }

            return true;
        }

        private void MoveSelectedUnit_Executed(Object arg)
        {
            LocationGroupViewModel newParent = arg as LocationGroupViewModel;
            LocationGroupViewModel groupViewToMove = this.SelectedUnit;
            if (newParent != null && groupViewToMove != null)
            {
                if (groupViewToMove.LocationGroup.ParentGroup != newParent.LocationGroup)
                {
                    //warn the user that this is about to happen as this action is on drag & drop.
                    ModalMessageResult result = ModalMessageResult.Button1;
                    if (this.AttachedControl != null)
                    {
                        ModalMessage msg;
                        if (groupViewToMove.LocationGroup.ChildList.Any())
                        {
                            msg = new ModalMessage()
                            {
                                Header = Message.Hierarchy_MoveUnit_WarningHeader,
                                Description = String.Format(CultureInfo.CurrentCulture,
                                                Message.Hierarchy_MoveUnitWithChildren_WarningDesc,
                                                groupViewToMove.LocationGroup.Name, newParent.LocationGroup.Name),
                                ButtonCount = 2,
                                Button1Content = Message.Generic_Continue,
                                Button2Content = Message.Generic_Cancel,
                                MessageIcon = ImageResources.DialogQuestion
                            };
                        }
                        else
                        {
                            msg = new ModalMessage()
                            {
                                Header = Message.Hierarchy_MoveUnit_WarningHeader,
                                Description = String.Format(CultureInfo.CurrentCulture,
                                                Message.Hierarchy_MoveUnit_WarningDesc,
                                                groupViewToMove.LocationGroup.Name, newParent.LocationGroup.Name),
                                ButtonCount = 2,
                                Button1Content = Message.Generic_Continue,
                                Button2Content = Message.Generic_Cancel,
                                MessageIcon = ImageResources.DialogQuestion
                            };
                        }
                        App.ShowWindow(msg, this.AttachedControl, true);
                        result = msg.Result;
                    }

                    if (result == ModalMessageResult.Button1)
                    {
                        base.ShowWaitCursor(true);
                        _suppressHierarchyChangeHandler = true;

                        //use the model helper to set the group against the new parent
                        LocationGroup movedGroup = LocationHierarchy.AssignGroupParent(groupViewToMove.LocationGroup, newParent.LocationGroup);

                        if (groupViewToMove.LocationGroup.ChildList.Any())
                        {
                            DealWithChildren(movedGroup);
                        }
                        
                        _suppressHierarchyChangeHandler = false;

                        //Update the unit collection as the group instances will have changed
                        RecreateFlattenedUnitsCollection();

                        base.ShowWaitCursor(false);


                        //select the newly added group
                        LocationGroupViewModel newGroupView = this.FlattenedUnits.FirstOrDefault(g => g.LocationGroup == movedGroup);
                        if (newGroupView != null)
                        {
                            this.SelectedUnit = newGroupView;
                        }
                    }
                }
            }
        }

        private void DealWithChildren(LocationGroup parent)
        {
            foreach (LocationGroup group in parent.ChildList)
            {
                LocationLevel level = parent.GetAssociatedLevel();
                if (level.ChildLevel == null)
                {
                    this.SelectedLevel = level;
                    this.AddLevel_Executed();
                }
                LocationGroup movedGroup = LocationHierarchy.AssignGroupParent(group, parent);
                DealWithChildren(movedGroup);
            }
        }

        #endregion

        #region AddLevelCommand

        private RelayCommand _addLevelCommand;

        /// <summary>
        /// Adds a new level beneath to the current selected level
        /// </summary>
        public RelayCommand AddLevelCommand
        {
            get
            {
                if (_addLevelCommand == null)
                {
                    _addLevelCommand = new RelayCommand(
                        p => AddLevel_Executed(), p => AddLevel_CanExecute())
                    {
                        FriendlyName = Message.LocationHierarchy_AddLevel,
                        FriendlyDescription = Message.LocationHierarchy_AddLevel_Description,
                        Icon = ImageResources.LocationHierarchy_AddLevel
                    };
                    base.ViewModelCommands.Add(_addLevelCommand);
                }
                return _addLevelCommand;
            }
        }

        private Boolean AddLevel_CanExecute()
        {
            //user has permission to edit a level
            if (!_userHasLocationHierarchyEditPerm)
            {
                this.AddLevelCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //check the action is legal
            String actionError = LocationHierarchy.IsAddLevelAllowed(this.SelectedLevel);
            if (!String.IsNullOrEmpty(actionError))
            {
                this.AddLevelCommand.DisabledReason = actionError;
                return false;
            }

            return true;
        }

        private void AddLevel_Executed()
        {
            LocationLevel parentLevel = this.SelectedLevel;
            if (parentLevel != null)
            {
                base.ShowWaitCursor(true);
                _suppressHierarchyChangeHandler = true;

                //create and add the new level
                LocationLevel newLevel = LocationLevel.NewLocationLevel();
                newLevel.Name = this.CurrentStructure.GetNextAvailableLevelName(parentLevel);
                this.CurrentStructure.AddLevel(newLevel, parentLevel);

                //select new level
                this.SelectedLevel = newLevel;

                //add the level to the flattened levels collection
                _flattenedLevels.Add(newLevel);


                _suppressHierarchyChangeHandler = false;
                base.ShowWaitCursor(false);
            }

        }

        #endregion

        #region RemoveLevelCommand

        private RelayCommand _removeLevelComamnd;

        /// <summary>
        /// Removes the selected level
        /// </summary>
        public RelayCommand RemoveLevelCommand
        {
            get
            {
                if (_removeLevelComamnd == null)
                {
                    _removeLevelComamnd = new RelayCommand(
                        p => RemoveLevel_Executed(), p => RemoveLevel_CanExecute())
                    {
                        FriendlyName = Message.LocationHierarchy_RemoveLevel,
                        FriendlyDescription = Message.LocationHierarchy_RemoveLevel_Description,
                        Icon = ImageResources.LocationHierarchy_RemoveLevel
                    };
                    base.ViewModelCommands.Add(_removeLevelComamnd);
                }
                return _removeLevelComamnd;
            }
        }

        private Boolean RemoveLevel_CanExecute()
        {
            //user has permission to remove a level
            if (!_userHasLocationHierarchyEditPerm)
            {
                this.RemoveLevelCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //check the action is legal
            String actionError = LocationHierarchy.IsRemoveLevelAllowed(this.SelectedLevel);
            if (!String.IsNullOrEmpty(actionError))
            {
                this.RemoveLevelCommand.DisabledReason = actionError;
                return false;
            }

            return true;
        }

        private void RemoveLevel_Executed()
        {
            LocationLevel levelToRemove = this.SelectedLevel;
            LocationLevel parentLevel = levelToRemove.ParentLevel;

            //prompt the user if the level has groups that will also be deleted
            Boolean continueWithRemove = true;
            Boolean levelHasGroups = this.FlattenedUnits.Any(g => g.AssociatedLevel == levelToRemove);
            if (levelHasGroups)
            {
                ModalMessage msg = new ModalMessage()
                {
                    Header = levelToRemove.Name,
                    Description = Message.Hierarchy_RemoveLevel_HasGroupsWarning,
                    Button1Content = Message.Generic_Continue,
                    Button2Content = Message.Generic_Cancel,
                    ButtonCount = 2,
                    MessageIcon = ImageResources.DialogWarning
                };
                App.ShowWindow(msg, this.AttachedControl, true);
                continueWithRemove = (msg.Result == ModalMessageResult.Button1);
            }


            if (continueWithRemove)
            {

                base.ShowWaitCursor(true);
                _suppressHierarchyChangeHandler = true;

                //remove the level
                this.CurrentStructure.RemoveLevel(levelToRemove);

                //remove from the flattened levels collection
                RecreateFlattenedLevelsCollection();
                if (levelHasGroups)
                {
                    RecreateFlattenedUnitsCollection();
                }

                //select the parent level of that removed
                this.SelectedLevel = parentLevel;

                _suppressHierarchyChangeHandler = false;
                base.ShowWaitCursor(false);
            }

        }
        #endregion

        #region ShowLocationAssignmentCommand

        private RelayCommand _showLocationAssignmentCommand;

        /// <summary>
        /// Opens the window to allow location assignments to location groups to be changed.
        /// </summary>
        public RelayCommand ShowLocationAssignmentCommand
        {
            get
            {
                if (_showLocationAssignmentCommand == null)
                {
                    _showLocationAssignmentCommand = new RelayCommand(
                        p => ShowLocationAssignment_Executed(),
                        p => ShowLocationAssignment_CanExecute())
                        {
                            FriendlyName = Message.LocationHierarchy_LocationAssignment,
                            FriendlyDescription = Message.LocationHierarchy_LocationAssignment_Desc,
                            Icon = ImageResources.LocationHierarchy_LocationAssignment
                        };
                    base.ViewModelCommands.Add(_showLocationAssignmentCommand);
                }
                return _showLocationAssignmentCommand;
            }
        }

        private Boolean ShowLocationAssignment_CanExecute()
        {
            //user must have permission to edit products
            if (!_userHasLocationEditPerm)
            {
                this.ShowLocationAssignmentCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be able to save
            if (!this.SaveCommand.CanExecute())
            {
                this.ShowLocationAssignmentCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void ShowLocationAssignment_Executed()
        {
            Boolean continueWithSave = true;

            //if the hierarchy is dirty then ask the user if they want to continue
            //as we must save before this screen can be accessed.
            if (this.CurrentStructure.IsDirty)
            {
                if (this.AttachedControl != null)
                {
                    ModalMessage msg = new ModalMessage()
                    {
                        Header = Message.LocationHierarchy_LocationAssignment_SaveHeader,
                        Description = Message.LocationHierarchy_LocationAssignment_SaveDesc,
                        MessageIcon = ImageResources.DialogQuestion,
                        ButtonCount = 2,
                        Button1Content = Message.Generic_Save,
                        Button2Content = Message.Generic_Cancel
                    };
                    App.ShowWindow(msg, this.AttachedControl, true);
                    continueWithSave = (msg.Result == ModalMessageResult.Button1);
                }
            }

            if (continueWithSave)
            {
                LocationGroupViewModel selectedGroup = this.SelectedUnit;

                //save the hierarchy
                this.SaveCommand.Execute();


                //show the window
                LocationHierarchyLocationAssign win = new LocationHierarchyLocationAssign();

                //if we have a unit with direct child products already selected then 
                // show that as the initial filter
                if (selectedGroup != null
                    && selectedGroup.LocationGroup.ChildList.Count == 0)
                {
                    win.ViewModel.SelectedFilterLocationGroup = this.SelectedUnit.LocationGroup;
                }

                App.ShowWindow(win, this.AttachedControl, true);


                //when the window closes reload the current hierarchy so that we are up to date.
                _structureViewModel.FetchForCurrentEntity();
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of the current structure
        /// </summary>
        private void StructureViewModel_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<LocationHierarchy> e)
        {
            if (e.OldModel != null)
            {
                e.OldModel.ChildChanged -= CurrentStructure_ChildChanged;
            }

            if (e.NewModel != null)
            {
                e.NewModel.ChildChanged += CurrentStructure_ChildChanged;
            }

            //notify of the current structure property change
            OnPropertyChanged(CurrentStructureProperty);

            //recreate the levels collection
            RecreateFlattenedLevelsCollection();

            //recreate the units collection.
            RecreateFlattenedUnitsCollection();

            //select the root
            if (this.SelectedUnit == null)
            {
                this.SelectedUnit = this.FlattenedUnits.FirstOrDefault(g => g.LocationGroup.IsRoot);
            }

        }

        /// <summary>
        /// Reponds to a change in the hierarchy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentStructure_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            if (!_suppressHierarchyChangeHandler)
            {
                if (e.PropertyChangedArgs != null)
                {
                    if (e.ChildObject is LocationGroup)
                    {
                        //if the product group code changed update the duplicate code check flag.
                        if (e.PropertyChangedArgs.PropertyName == LocationGroup.CodeProperty.Name)
                        {
                            UpdateAllGroupsValid();
                        }
                        else if (e.PropertyChangedArgs.PropertyName == LocationGroup.LocationLevelIdProperty.Name)
                        {
                            //associated level changed so updated all view paths
                            foreach (LocationGroupViewModel groupView in this.FlattenedUnits)
                            {
                                groupView.RefreshAll();
                            }
                        }
                    }
                }
                else if (e.CollectionChangedArgs != null)
                {
                    if (e.ChildObject is LocationGroupList)
                    {
                        RecreateFlattenedUnitsCollection();
                    }
                    else if (e.ChildObject is LocationLevelList)
                    {
                        RecreateFlattenedLevelsCollection();
                        foreach (LocationGroupViewModel groupView in this.FlattenedUnits)
                        {
                            groupView.RefreshAll();
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Responds to changes to the flattened levels collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FlattenedLevels_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            //update group view properties
            foreach (LocationGroupViewModel groupView in this.FlattenedUnits)
            {
                groupView.RefreshAll();
            }
        }

        /// <summary>
        /// Responds to a change of selected level
        /// </summary>
        private void OnSelectedLevelChanged()
        {
            //if a selection was made deselect the current level
            if (this.SelectedLevel != null)
            {
                this.SelectedUnit = null;
            }
        }

        /// <summary>
        /// Responds to a change of selected unit
        /// </summary>
        private void OnSelectedUnitChanged()
        {
            //if a selection was made deselect the current level
            if (this.SelectedUnit != null)
            {
                this.SelectedLevel = null;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.CurrentStructure, this.SaveCommand);
            }
            return continueExecute;
        }

        /// <summary>
        /// Recreates the entire flattened units collection.
        /// </summary>
        private void RecreateFlattenedUnitsCollection()
        {
            //get the current selected group
            String selectedUnitCode = null;
            if (this.SelectedUnit != null)
            {
                selectedUnitCode = this.SelectedUnit.LocationGroup.Code;
                this.SelectedUnit = null;
            }


            List<LocationGroup> expectedGroups =
            (this.CurrentStructure != null) ?
            this.CurrentStructure.EnumerateAllGroups().ToList() : new List<LocationGroup>();


            //compile a list of views to be removed
            if (_flattenedUnits.Count > 0)
            {
                List<LocationGroupViewModel> removeViewList = new List<LocationGroupViewModel>();
                foreach (LocationGroupViewModel view in this.FlattenedUnits)
                {
                    if (expectedGroups.Contains(view.LocationGroup))
                    {
                        expectedGroups.Remove(view.LocationGroup);
                    }
                    else
                    {
                        removeViewList.Add(view);
                    }
                }

                if (_flattenedUnits.Count != removeViewList.Count)
                {
                    _flattenedUnits.RemoveRange(removeViewList);
                }
                else
                {
                    _flattenedUnits.Clear();
                }
            }


            //add in any missing group views
            if (expectedGroups.Count > 0)
            {
                List<LocationGroupViewModel> addViewList = new List<LocationGroupViewModel>();
                foreach (LocationGroup group in expectedGroups)
                {
                    addViewList.Add(new LocationGroupViewModel(group));
                }
                _flattenedUnits.AddRange(addViewList);
            }



            //try to reselect the original selected group
            if (selectedUnitCode != null)
            {
                LocationGroupViewModel newSelectedGroup = this.FlattenedUnits.FirstOrDefault(g => g.LocationGroup.Code.Equals(selectedUnitCode));
                if (newSelectedGroup != null)
                {
                    this.SelectedUnit = newSelectedGroup;
                }
            }


            UpdateAllGroupsValid();
        }

        /// <summary>
        /// Recreates the entire flattened levels collection
        /// </summary>
        private void RecreateFlattenedLevelsCollection()
        {
            if (_flattenedLevels.Count > 0)
            {
                _flattenedLevels.Clear();
            }

            if (this.CurrentStructure != null)
            {
                _flattenedLevels.AddRange(this.CurrentStructure.EnumerateAllLevels());
            }
        }

        /// <summary>
        /// Updates _allGroupsValid to be false if a group code is a duplicate
        /// </summary>
        private void UpdateAllGroupsValid()
        {
            IEnumerable<LocationGroup> groupList = this.CurrentStructure.EnumerateAllGroups();

            //changed to use groups as this is considerably faster than linq where clause
            var groups =
                from locationGroup in groupList
                group locationGroup by locationGroup.Code;

            foreach (var group in groups.Where(g => g.Count() > 1))
            {
                _originalCode = String.Format("\"{0} {1}\"", group.First().Code, group.First().Name);
                _duplicateCode = String.Format("\"{0} {1}\"", group.Last().Code, group.Last().Name);
                _allGroupsValid = false;
                return;
            }

            _allGroupsValid = true;
        }

        /// <summary>
        /// Gets the viewmodel of the given group and selects it
        /// </summary>
        /// <param name="groupToSelect"></param>
        public void SetSelectedGroup(LocationGroup groupToSelect)
        {
            String groupCode = groupToSelect.Code;

            LocationGroupViewModel unitToFocus =
                this.FlattenedUnits.FirstOrDefault(u => u.LocationGroup.Code.Equals(groupCode));

            if (unitToFocus != null)
            {
                this.SelectedUnit = unitToFocus;
            }
        }

        #endregion

        #region Screen Permissions

        private Boolean _userHasLocationHierarchyEditPerm;
        private Boolean _userHasLocationEditPerm;

        /// <summary>
        /// Refreshes the value of the permission flags for the current user.
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //hierarchy group premissions
            _userHasLocationHierarchyEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(LocationHierarchy));

            //Location
            _userHasLocationEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(Location));
        }

        /// <summary>
        /// Returns true if the current user has sufficient permissions to access this screen.
        /// </summary>
        /// <returns></returns>
        internal static Boolean UserHasRequiredAccessPerms()
        {
            return

                //need loc hierarchy edit permission
                Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.EditObject, typeof(LocationHierarchy));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _structureViewModel.ModelChanged -= StructureViewModel_ModelChanged;
                    _flattenedLevels.BulkCollectionChanged -= FlattenedLevels_BulkCollectionChanged;
                    this.CurrentStructure.ChildChanged -= CurrentStructure_ChildChanged;

                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
