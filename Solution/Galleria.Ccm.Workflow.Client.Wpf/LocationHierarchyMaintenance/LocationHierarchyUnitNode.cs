﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    /// <summary>
    /// Diagram node content depicting a product hierarchy unit
    /// </summary>
    public sealed class LocationHierarchyUnitNode : ContentControl
    {
        #region Properties

        #region Unit Context Property

        public static readonly DependencyProperty UnitContextProperty =
            DependencyProperty.Register("UnitContext", typeof(LocationGroupViewModel),
            typeof(LocationHierarchyUnitNode),
            new PropertyMetadata(null, OnUnitContextPropertyChanged));

        private static void OnUnitContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationHierarchyUnitNode senderControl = (LocationHierarchyUnitNode)obj;
            senderControl.Content = e.NewValue;
        }

        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public LocationGroupViewModel UnitContext
        {
            get { return (LocationGroupViewModel)GetValue(UnitContextProperty); }
            set { SetValue(UnitContextProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static LocationHierarchyUnitNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(LocationHierarchyUnitNode), new FrameworkPropertyMetadata(typeof(LocationHierarchyUnitNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitContext"></param>
        public LocationHierarchyUnitNode(LocationGroupViewModel unitContext)
        {
            this.UnitContext = unitContext;

        }

        #endregion

        #region Overrides

        /// <summary>
        /// To string override so that this node is sorted correctly.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (this.UnitContext != null
                && this.UnitContext.LocationGroup != null)
            {
                return this.UnitContext.LocationGroup.Name;
            }
            else { return base.ToString(); }
        }

        #endregion
    }
}
