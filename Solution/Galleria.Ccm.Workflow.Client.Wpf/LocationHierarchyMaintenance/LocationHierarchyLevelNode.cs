﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    /// <summary>
    /// Diagram node content for depicting a product hierarchy level
    /// </summary>
    public sealed class LocationHierarchyLevelNode : ContentControl
    {
        #region Properties

        public static readonly DependencyProperty LevelContextProperty =
            DependencyProperty.Register("LevelContext", typeof(LocationLevel),
            typeof(LocationHierarchyLevelNode),
            new PropertyMetadata(null, OnLevelContextPropertyChanged));

        /// <summary>
        /// Gets/Sets the product hierarchy level represented by this node
        /// </summary>
        public LocationLevel LevelContext
        {
            get { return (LocationLevel)GetValue(LevelContextProperty); }
            private set { SetValue(LevelContextProperty, value); }
        }

        private static void OnLevelContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationHierarchyLevelNode senderControl = (LocationHierarchyLevelNode)obj;
            senderControl.Content = e.NewValue;
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static LocationHierarchyLevelNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(LocationHierarchyLevelNode), new FrameworkPropertyMetadata(typeof(LocationHierarchyLevelNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="level"></param>
        public LocationHierarchyLevelNode(LocationLevel level)
        {
            this.LevelContext = level;
        }

        #endregion
    }
}
