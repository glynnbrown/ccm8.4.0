﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25554 : L.Ineson ~ Copied from GFS.
#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationHierarchyMaintenance
{
    /// <summary>
    /// Interaction logic for LocationHierarchyUnitEditWindow.xaml
    /// </summary>
    public sealed partial class LocationHierarchyUnitEditWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationHierarchyUnitEditViewModel), typeof(LocationHierarchyUnitEditWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationHierarchyUnitEditViewModel ViewModel
        {
            get { return (LocationHierarchyUnitEditViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationHierarchyUnitEditWindow senderControl = (LocationHierarchyUnitEditWindow)obj;

            if (e.OldValue != null)
            {
                LocationHierarchyUnitEditViewModel oldModel = (LocationHierarchyUnitEditViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationHierarchyUnitEditViewModel newModel = (LocationHierarchyUnitEditViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }



        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationHierarchyUnitEditWindow(LocationHierarchy hierarchy)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new LocationHierarchyUnitEditViewModel(hierarchy, null);

            this.Loaded += new RoutedEventHandler(LocationHierarchyUnitEditWindow_Loaded);
        }

        /// <summary>LocationHierarchyUnitEditViewModel
        /// Constructor
        /// </summary>
        public LocationHierarchyUnitEditWindow(LocationGroup unitContext)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new LocationHierarchyUnitEditViewModel(unitContext.ParentHierarchy, unitContext);

            this.Loaded += new RoutedEventHandler(LocationHierarchyUnitEditWindow_Loaded);
        }

        private void LocationHierarchyUnitEditWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationHierarchyUnitEditWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window close

        /// <summary>
        /// Closes this window when requested.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }

        }
        #endregion
    }
}
