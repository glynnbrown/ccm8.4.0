﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion

using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;



namespace Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance
{
    /// <summary>
    /// Display for the product hierarchy levels
    /// </summary>
    public partial class MerchHierarchyLevelsDisplay : UserControl
    {
        #region Fields
        private bool _supressSelectionChanged;
        #endregion

        #region Properties

        #region RootLevelProperty

        public static readonly DependencyProperty RootLevelProperty =
            DependencyProperty.Register("RootLevel", typeof(ProductLevel),
            typeof(MerchHierarchyLevelsDisplay),
            new PropertyMetadata(null, OnRootLevelPropertyChanged));

        /// <summary>
        /// Gets/Sets the diagram root level
        /// </summary>
        public ProductLevel RootLevel
        {
            get { return (ProductLevel)GetValue(RootLevelProperty); }
            set { SetValue(RootLevelProperty, value); }
        }


        private static void OnRootLevelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MerchHierarchyLevelsDisplay senderControl = (MerchHierarchyLevelsDisplay)obj;

            if (e.OldValue != null)
            {
                ProductLevel oldValue = (ProductLevel)e.OldValue;
                oldValue.ChildChanged -= senderControl.RootLevel_ChildChanged;
                oldValue.PropertyChanged -= senderControl.RootLevel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                ProductLevel newValue = (ProductLevel)e.NewValue;
                newValue.ChildChanged += senderControl.RootLevel_ChildChanged;
                newValue.PropertyChanged += senderControl.RootLevel_PropertyChanged;
            }

            senderControl.OnRootNodeChanged();
        }

        #endregion

        #region SelectedLevelProperty

        public static readonly DependencyProperty SelectedLevelProperty =
            DependencyProperty.Register("SelectedLevel", typeof(ProductLevel),
            typeof(MerchHierarchyLevelsDisplay),
            new PropertyMetadata(null, OnSelectedLevelPropertyChanged));

        public ProductLevel SelectedLevel
        {
            get { return (ProductLevel)GetValue(SelectedLevelProperty); }
            set { SetValue(SelectedLevelProperty, value); }
        }

        private static void OnSelectedLevelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MerchHierarchyLevelsDisplay senderControl = (MerchHierarchyLevelsDisplay)obj;
            senderControl.OnSelectedLevelChanged();
        }

        private void OnSelectedLevelChanged()
        {
            //dont do anything if we are supressed
            if (_supressSelectionChanged) { return; }

            _supressSelectionChanged = true;

            //update the diagram selected level to match the property here
            MerchHierarchyLevelNode diagramSelectedNodeBase =
                (diagram.SelectedItems.Count > 0) ?
                (MerchHierarchyLevelNode)diagram.SelectedItems.First() : null;

            if ((diagramSelectedNodeBase == null) || // SA-14465
                (diagramSelectedNodeBase.LevelContext != this.SelectedLevel))
            {
                diagram.SelectedItems.Clear();

                MerchHierarchyLevelNode nodeToSelect =
                    diagram.Items.Cast<MerchHierarchyLevelNode>()
                    .FirstOrDefault(n => n.LevelContext == this.SelectedLevel);

                if (nodeToSelect != null)
                {
                    diagram.SelectedItems.Add(nodeToSelect);
                }
            }

            _supressSelectionChanged = false;

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MerchHierarchyLevelsDisplay()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to change of selected node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void diagram_NodeSelectionChanged(object sender, EventArgs e)
        {
            if (!_supressSelectionChanged)
            {
                _supressSelectionChanged = true;

                //update the property to match the diagram
                MerchHierarchyLevelNode diagramSelectedNodeBase = (MerchHierarchyLevelNode)diagram.SelectedItems.FirstOrDefault();
                if (diagramSelectedNodeBase != null)
                {
                    this.SelectedLevel = diagramSelectedNodeBase.LevelContext;
                }

                _supressSelectionChanged = false;
            }
        }


        private void RootLevel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ProductLevel.ChildLevelProperty)
            {
                //redraw the root node
                OnRootNodeChanged();
            }
        }

        private void RootLevel_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            //looking for a childproperty changed
            if (e.PropertyChangedArgs != null)
            {
                if (e.PropertyChangedArgs.PropertyName == ProductLevel.ChildLevelProperty)
                {
                    diagram.IsAutoArrangeOn = false;

                    ProductLevel changedChildLevel = (ProductLevel)e.ChildObject;
                    MerchHierarchyLevelNode changedChildNode =
                        diagram.Items.Cast<MerchHierarchyLevelNode>().FirstOrDefault(n => n.LevelContext == changedChildLevel);

                    //draw the levels from and including that child
                    DrawChildLevels(changedChildNode);
                    diagram.ArrangeDiagram();

                    diagram.IsAutoArrangeOn = true;
                }
            }
        }

        private void OnRootNodeChanged()
        {
            diagram.Items.Clear();

            if (this.RootLevel != null)
            {
                diagram.IsAutoArrangeOn = false;

                //draw the root node
                MerchHierarchyLevelNode rootLevelNode = new MerchHierarchyLevelNode(this.RootLevel);
                diagram.Items.Add(rootLevelNode);

                //draw all children down
                DrawChildLevels(rootLevelNode);


                diagram.IsAutoArrangeOn = true;
            }
        }

        #endregion

        #region Methods

        private void DrawChildLevels(MerchHierarchyLevelNode parent)
        {
            //clear any existing decendents
            LocalHelper.ClearDescendantVisuals(diagram, parent);

            //get the child
            ProductLevel childLevel = parent.LevelContext.ChildLevel;

            if (childLevel != null)
            {
                //draw the child
                MerchHierarchyLevelNode childLevelNode = new MerchHierarchyLevelNode(childLevel);
                diagram.Items.Add(childLevelNode);
                diagram.Links.Add(new DiagramItemLink(parent, childLevelNode));

                //recurse with the child as the new parent
                DrawChildLevels(childLevelNode);
            }
        }

        #endregion
    }
}
