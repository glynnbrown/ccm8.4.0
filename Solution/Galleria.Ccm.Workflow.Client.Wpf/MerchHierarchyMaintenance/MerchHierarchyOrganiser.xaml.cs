﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance
{
    /// <summary>
    /// Interaction logic for ProductHierarchyOrganiser.xaml
    /// </summary>
    public sealed partial class MerchHierarchyOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        private Boolean _suppressDiagramSelectionChangedHandling;
        private Boolean _isFirstLoad = true;
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MerchHierarchyViewModel), typeof(MerchHierarchyOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public MerchHierarchyViewModel ViewModel
        {
            get { return (MerchHierarchyViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MerchHierarchyOrganiser senderControl = (MerchHierarchyOrganiser)obj;

            if (e.OldValue != null)
            {
                MerchHierarchyViewModel oldModel = (MerchHierarchyViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.FlattenedLevels.BulkCollectionChanged -= senderControl.ViewModel_FlattenedLevelsBulkCollectionChanged;
                oldModel.FlattenedUnits.BulkCollectionChanged -= senderControl.ViewModel_FlattenedUnitsBulkCollectionChanged;
                oldModel.ExpandAllCommand.Executed -= senderControl.ViewModel_ExpandAllExecuted;
                oldModel.CollapseAllCommand.Executed -= senderControl.ViewModel_CollapseAllExecuted;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                MerchHierarchyViewModel newModel = (MerchHierarchyViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.FlattenedLevels.BulkCollectionChanged += senderControl.ViewModel_FlattenedLevelsBulkCollectionChanged;
                newModel.FlattenedUnits.BulkCollectionChanged += senderControl.ViewModel_FlattenedUnitsBulkCollectionChanged;
                newModel.ExpandAllCommand.Executed += senderControl.ViewModel_ExpandAllExecuted;
                newModel.CollapseAllCommand.Executed += senderControl.ViewModel_CollapseAllExecuted;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }

            if (!senderControl._isFirstLoad)
            {
                senderControl.RedrawAllDiagramNodes();
            }
            senderControl._isFirstLoad = false;
        }

        #endregion

        #endregion

        #region Constructor

        public MerchHierarchyOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            this.AddHandler(MerchHierarchyUnitNode.MouseDoubleClickEvent, new MouseButtonEventHandler(ProductHierarchyUnitNode_MouseDoubleClicked));

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.MerchandisingHierarchyMaintenance);

            this.ViewModel = new MerchHierarchyViewModel();

            this.Loaded += new RoutedEventHandler(ProductHierarchyOrganiser_Loaded);
        }

        /// <summary>
        /// Carries out initial loaded actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductHierarchyOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProductHierarchyOrganiser_Loaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }), priority: DispatcherPriority.Background);
        }

        #endregion

        #region Event Handlers

        #region ViewModel

        /// <summary>
        /// Forces a column reload when the flattened levels collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_FlattenedLevelsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            //recontstruct the columns collection
            ReloadUnitsDatagridColumns();
        }

        /// <summary>
        /// Updates the diagram when the flattened units collection changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_FlattenedUnitsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (diagram != null && this.ViewModel != null)
            {
                _suppressDiagramSelectionChangedHandling = true;

                switch (e.Action)
                {
                    #region Add
                    case NotifyCollectionChangedAction.Add:
                        {
                            Boolean requiresFullRedraw = (e.ChangedItems.Count == this.ViewModel.FlattenedUnits.Count);
                            if (!requiresFullRedraw)
                            {
                                //+ The full node collection was not added so
                                // try to add only the new nodes.
                                diagram.IsAutoArrangeOn = false;

                                List<ProductGroupViewModel> addItems = e.ChangedItems.Cast<ProductGroupViewModel>().ToList();
                                while (addItems.Count > 0)
                                {
                                    ProductGroupViewModel groupView = addItems.First();
                                    addItems.Remove(groupView);

                                    //try to find the parent node.
                                    ProductGroup parentGroup = groupView.ProductGroup.ParentGroup;
                                    if (parentGroup != null)
                                    {
                                        //check if the parent is due to be added
                                        ProductGroupViewModel parentToAdd = addItems.FirstOrDefault(g => g.ProductGroup == parentGroup);
                                        if (parentToAdd != null)
                                        {
                                            //shuffle so that the parent gets added first
                                            addItems.Remove(parentToAdd);
                                            addItems.Insert(0, parentToAdd);
                                            addItems.Insert(1, groupView);
                                        }
                                        else
                                        {
                                            //the parent node should already be in the diagram
                                            MerchHierarchyUnitNode parentNode =
                                                diagram.Items.Cast<MerchHierarchyUnitNode>()
                                                .FirstOrDefault(n => n.UnitContext.ProductGroup == parentGroup);
                                            if (parentNode != null)
                                            {
                                                //add the new node
                                                MerchHierarchyUnitNode childNode = new MerchHierarchyUnitNode(groupView);
                                                diagram.Items.Add(childNode);
                                                diagram.Links.Add(new DiagramItemLink(parentNode, childNode));
                                            }
                                            else
                                            {
                                                //something has gone very wrong so break out and redraw all
                                                requiresFullRedraw = true;
                                                break;
                                            }
                                        }
                                    }
                                    else if (groupView.ProductGroup.IsRoot)
                                    {
                                        //just add the node if it is the root.
                                        diagram.Items.Add(new MerchHierarchyUnitNode(groupView));
                                    }
                                }
                                diagram.IsAutoArrangeOn = true;
                            }

                            //failed to draw an added group so redraw everything instead.
                            if (requiresFullRedraw)
                            {
                                RedrawAllDiagramNodes();
                            }
                        }
                        break;
                    #endregion

                    #region Remove
                    case NotifyCollectionChangedAction.Remove:
                        {
                            diagram.IsAutoArrangeOn = false;

                            List<MerchHierarchyUnitNode> nodes = diagram.Items.Cast<MerchHierarchyUnitNode>().ToList();
                            foreach (ProductGroupViewModel groupView in e.ChangedItems)
                            {
                                MerchHierarchyUnitNode childNodeBase = nodes.FirstOrDefault(n => n.UnitContext == groupView);
                                diagram.Items.Remove(childNodeBase);
                            }

                            diagram.IsAutoArrangeOn = true;
                        }
                        break;
                    #endregion

                    case NotifyCollectionChangedAction.Reset:
                        RedrawAllDiagramNodes();
                        break;
                }

                _suppressDiagramSelectionChangedHandling = false;
                OnSelectedUnitChanged();
            }
        }

        /// <summary>
        /// EVent handler for the expand all command behind executed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_ExpandAllExecuted(object sender, EventArgs e)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //Expand all nodes
            Diagram.ExpandAllCommand.Execute(null, this.diagram);

            //remove busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }), priority: DispatcherPriority.Background);
        }

        /// <summary>
        /// EVent handler for the collapse all command behind executed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_CollapseAllExecuted(object sender, EventArgs e)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //Collapse all nodes
            Diagram.CollapseAllCommand.Execute(null, this.diagram);

            //remove busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }), priority: DispatcherPriority.Background);
        }

        /// <summary>
        /// Reponds to viewmodel property changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == MerchHierarchyViewModel.SelectedUnitProperty.Path)
            {
                OnSelectedUnitChanged();
            }
        }

        #endregion

        #region Units Datagrid

        private void flattenedGrid_Loaded(object sender, RoutedEventArgs e)
        {
            ExtendedDataGrid senderControl = (ExtendedDataGrid)sender;
            senderControl.Loaded -= flattenedGrid_Loaded;

            ReloadUnitsDatagridColumns();
        }

        private void flattenedGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.EditUnitCommand.Execute();
            }
        }

        /// <summary>
        /// Scrolls the selected item into view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flattenedGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ExtendedDataGrid senderControl = (ExtendedDataGrid)sender;

            if (senderControl.SelectedItem != null)
            {
                senderControl.ScrollIntoView(senderControl.SelectedItem);
            }
        }

        /// <summary>
        /// Custom prefilter handler for the groups datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void flattenedGrid_PrefilterItem(object sender, ExtendedDataGridFilterEventArgs e)
        {
            if (this.flattenedGrid != null)
            {
                String prefilter = this.flattenedGrid.PrefilterText;
                if (!String.IsNullOrEmpty(prefilter))
                {
                    ProductGroupViewModel row = e.Item as ProductGroupViewModel;
                    if (row != null)
                    {
                        if (row.ProductGroup == null)
                        {
                            e.Accepted = false;
                        }
                        else
                        {
                            String productGroupDesc = row.ProductGroup.ToString().ToLowerInvariant();
                            if (!productGroupDesc.Contains(prefilter.ToLowerInvariant()))
                            {
                                e.Accepted = false;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Units Diagram

        private void diagram_Loaded(object sender, RoutedEventArgs e)
        {
            Diagram senderControl = (Diagram)sender;
            senderControl.Loaded -= diagram_Loaded;

            RedrawAllDiagramNodes();
        }

        private void xZoomBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    if (diagramArrangeMethod != null && this.ViewModel != null)
                    {
                        //get the root node
                        ProductGroupViewModel rootGroupView = this.ViewModel.FlattenedUnits.FirstOrDefault(g => g.ProductGroup.IsRoot);
                        if (rootGroupView != null)
                        {
                            MerchHierarchyUnitNode rootNode =
                                this.diagram.Items.Cast<MerchHierarchyUnitNode>().FirstOrDefault(n => n.UnitContext == rootGroupView);
                            if (rootNode != null)
                            {
                                //update the root offset if different than the current
                                Double newOffset = xZoomBox.ViewportWidth / 2 - (rootNode.Width / 4);
                                if (diagramArrangeMethod.MinRootHorizontalOffset != newOffset)
                                {
                                    //update the root offset
                                    diagramArrangeMethod.MinRootHorizontalOffset = newOffset;
                                    diagram.ArrangeDiagram();
                                }
                            }
                        }
                    }
                }), priority: DispatcherPriority.Background);
        }

        /// <summary>
        /// Responds to change of selected node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void diagram_NodeSelectionChanged(object sender, EventArgs e)
        {
            //return out if this is suppressed
            if (_suppressDiagramSelectionChangedHandling) { return; }

            _suppressDiagramSelectionChangedHandling = true;

            if (diagram.SelectedItems.Count > 0)
            {
                //update the property to match the diagram
                MerchHierarchyUnitNode diagramSelectedNodeBase = (MerchHierarchyUnitNode)diagram.SelectedItems.First();
                this.ViewModel.SelectedUnit = diagramSelectedNodeBase.UnitContext;
            }
            else
            {
                this.ViewModel.SelectedUnit = null;
            }

            _suppressDiagramSelectionChangedHandling = false;
        }

        /// <summary>
        /// Reponds to the event of one node being 'dropped' on another
        /// </summary>
        /// <param name="sender">The node hit by the drop</param>
        /// <param name="e"></param>
        private void diagram_ItemDroppedOn(object sender, DiagramNodeDropArgs e)
        {
            MerchHierarchyUnitNode moveToNode = e.HitItem as MerchHierarchyUnitNode;
            if (moveToNode != null)
            {
                if (this.ViewModel != null
                    && this.ViewModel.MoveSelectedUnitCommand.CanExecute(moveToNode.UnitContext))
                {
                    this.ViewModel.MoveSelectedUnitCommand.Execute(moveToNode.UnitContext);
                }
            }
        }

        /// <summary>
        /// Called when the diagram expand all or collapse all command has executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void diagram_ExpandCollapseAllCompleted(object sender, RoutedEventArgs e)
        {
            this.xZoomBox.ZoomToFit();
        }


        private void ProductHierarchyUnitNode_MouseDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            if (((DependencyObject)e.OriginalSource).FindVisualAncestor<MerchHierarchyUnitNode>() != null)
            {
                if (this.ViewModel != null && this.ViewModel.EditUnitCommand.CanExecute())
                {
                    this.ViewModel.EditUnitCommand.Execute();
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Reloads level grid columns
        /// </summary>
        private void ReloadUnitsDatagridColumns()
        {
            if (flattenedGrid != null)
            {
                //clear out the existing columns
                if (flattenedGrid.Columns.Count > 0)
                {
                    flattenedGrid.Columns.Clear();
                }

                //add in the new columns
                List<DataGridColumn> columnSet =
                    DataObjectViewHelper.GetProductGroupColumnSet(this.ViewModel.CurrentStructure, /*includeRootCol*/true);
                if (columnSet.Count > 0)
                {
                    columnSet.ForEach(c => flattenedGrid.Columns.Add(c));
                }
            }
        }

        /// <summary>
        /// Completely redraws the diagram.
        /// </summary>
        private void RedrawAllDiagramNodes()
        {
            //load new items
            if (diagram.IsLoaded && this.ViewModel != null && this.ViewModel.FlattenedUnits.Count > 0)
            {
                //turn off the auto arrange to make loading faster
                diagram.IsAutoArrangeOn = false;

                Dictionary<ProductGroup, ProductGroupViewModel> groupToViewDict =
                    this.ViewModel.FlattenedUnits.ToDictionary(g => g.ProductGroup);


                List<ProductGroupViewModel> groupViews = this.ViewModel.FlattenedUnits.ToList();
                ProductGroupViewModel rootGroupView = groupViews.FirstOrDefault(g => g.ProductGroup.IsRoot);
                if (rootGroupView != null)
                {
                    List<MerchHierarchyUnitNode> nodeList = new List<MerchHierarchyUnitNode>();
                    List<DiagramItemLink> linkList = new List<DiagramItemLink>();


                    //create the root node first
                    MerchHierarchyUnitNode rootNode = new MerchHierarchyUnitNode(rootGroupView);
                    nodeList.Add(rootNode);

                    //create child nodes and links
                    CreateChildren(rootNode, groupToViewDict, nodeList, linkList);

                    //mass add to the diagram
                    Diagram.LoadDiagram(diagram, nodeList, linkList, /*collapseAll*/true, /*clearExisting*/true);

                    diagram.SetItemExpandedState(rootNode, true);

                    //update the root offset
                    diagramArrangeMethod.MinRootHorizontalOffset = xZoomBox.ViewportWidth / 2 - (rootNode.Width / 4);
                }

                //turn auto arrange back on
                diagram.IsAutoArrangeOn = true;


            }
            else
            {
                //just clear existing 
                if (diagram.Items.Count > 0) { diagram.Items.Clear(); }
            }


            OnSelectedUnitChanged();
        }


        /// <summary>
        /// Creates all the nodes and links to be loaded into the diagram
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="groupToViewDict"></param>
        /// <param name="nodeList"></param>
        /// <param name="linkList"></param>
        private void CreateChildren(MerchHierarchyUnitNode parent,
            Dictionary<ProductGroup, ProductGroupViewModel> groupToViewDict,
            List<MerchHierarchyUnitNode> nodeList,
            List<DiagramItemLink> linkList)
        {
            foreach (ProductGroup childGroup in parent.UnitContext.ProductGroup.ChildList)
            {
                //get the child view
                ProductGroupViewModel childView;
                if (groupToViewDict.TryGetValue(childGroup, out childView))
                {
                    //create the child and link to parent
                    MerchHierarchyUnitNode childNode = new MerchHierarchyUnitNode(childView);
                    nodeList.Add(childNode);

                    DiagramItemLink link = new DiagramItemLink(parent, childNode);
                    linkList.Add(link);

                    //recurse
                    CreateChildren(childNode, groupToViewDict, nodeList, linkList);
                }
            }

        }


        private void OnSelectedUnitChanged()
        {
            //return out if this is suppressed.
            if (_suppressDiagramSelectionChangedHandling || this.ViewModel == null) { return; }

            _suppressDiagramSelectionChangedHandling = true;

            //get the current select unit in the diagram
            MerchHierarchyUnitNode diagramSelectedNodeBase =
                 (diagram.SelectedItems.Count > 0) ? (MerchHierarchyUnitNode)diagram.SelectedItems.First() : null;
            ProductGroupViewModel selectedUnitView = (diagramSelectedNodeBase != null) ? diagramSelectedNodeBase.UnitContext : null;

            if (selectedUnitView != this.ViewModel.SelectedUnit)
            {
                //turn off the auto arrange for speed
                diagram.IsAutoArrangeOn = false;

                //clear any existing selection
                diagram.SelectedItems.Clear();


                MerchHierarchyUnitNode nodeVisual =
                    diagram.Items.Cast<MerchHierarchyUnitNode>().FirstOrDefault(n => n.UnitContext == this.ViewModel.SelectedUnit);

                if (nodeVisual != null)
                {
                    //add the node to the selection
                    diagram.SelectedItems.Add(nodeVisual);


                    //expand all its parents
                    FrameworkElement currentParentNode =
                       diagram.Links.Where(l => l.EndItem == nodeVisual).Select(l => l.StartItem).FirstOrDefault();

                    while (currentParentNode != null)
                    {
                        diagram.SetItemExpandedState(currentParentNode, true);

                        currentParentNode =
                            diagram.Links.Where(l => l.EndItem == currentParentNode).Select(l => l.StartItem).FirstOrDefault();
                    }

                }

                diagram.IsAutoArrangeOn = true;
                if (nodeVisual != null)
                {
                    //focus on the item
                    diagram.BringItemIntoView(nodeVisual);
                }
            }

            _suppressDiagramSelectionChangedHandling = false;

        }


        #endregion

        #region Window close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }

            }

        }


        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    IDisposable disposableViewModel = (IDisposable)this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }

        }


        #endregion

        private void FlattenedGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                this.ViewModel.EditUnitCommand.Execute();
        }
    }
}
