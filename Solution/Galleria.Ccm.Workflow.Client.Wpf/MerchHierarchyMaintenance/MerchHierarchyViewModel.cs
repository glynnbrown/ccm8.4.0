﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;


namespace Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance
{
    /// <summary>
    /// ViewModel controller for the merchandising hierarchy maintenance screen
    /// </summary>
    public sealed class MerchHierarchyViewModel : ViewModelAttachedControlObject<MerchHierarchyOrganiser>
    {
        #region Fields

        const String _exceptionCategory = "MerchandisingHierarchyMaintenance";

        private readonly ProductHierarchyViewModel _structureViewModel = new ProductHierarchyViewModel();

        private readonly BulkObservableCollection<ProductLevel> _flattenedLevels = new BulkObservableCollection<ProductLevel>();
        private ReadOnlyBulkObservableCollection<ProductLevel> _flattenedLevelsRO;
        private ProductLevel _selectedLevel;

        private readonly BulkObservableCollection<ProductGroupViewModel> _flattenedUnits = new BulkObservableCollection<ProductGroupViewModel>();
        private ReadOnlyBulkObservableCollection<ProductGroupViewModel> _flattenedUnitsRO;
        private ProductGroupViewModel _selectedUnit;

        private Boolean _allGroupsValid;
        private String _originalCode;
        private String _duplicateCode;

        private Boolean _suppressHierarchyChangeHandler;
        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath CurrentStructureProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.CurrentStructure);
        public static readonly PropertyPath FlattenedLevelsProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.FlattenedLevels);
        public static readonly PropertyPath SelectedLevelProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.SelectedLevel);
        public static readonly PropertyPath FlattenedUnitsProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.FlattenedUnits);
        public static readonly PropertyPath SelectedUnitProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.SelectedUnit);

        //commands
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath AddLevelCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.AddLevelCommand);
        public static readonly PropertyPath RemoveLevelCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.RemoveLevelCommand);
        public static readonly PropertyPath AddUnitCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.AddUnitCommand);
        public static readonly PropertyPath RemoveUnitCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.RemoveUnitCommand);
        public static readonly PropertyPath EditUnitCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.EditUnitCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath ExpandAllCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.ExpandAllCommand);
        public static readonly PropertyPath CollapseAllCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyViewModel>(p => p.CollapseAllCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current structure being edited
        /// </summary>
        public ProductHierarchy CurrentStructure
        {
            get { return _structureViewModel.Model; }
        }

        /// <summary>
        /// Returns the flattened collection of levels within the structure
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductLevel> FlattenedLevels
        {
            get
            {
                if (_flattenedLevelsRO == null)
                {
                    _flattenedLevelsRO =
                        new ReadOnlyBulkObservableCollection<ProductLevel>(_flattenedLevels);
                }
                return _flattenedLevelsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the current selected level
        /// </summary>
        public ProductLevel SelectedLevel
        {
            get { return _selectedLevel; }
            set
            {
                _selectedLevel = value;
                OnPropertyChanged(SelectedLevelProperty);
                OnSelectedLevelChanged();
            }
        }

        /// <summary>
        /// Returns the flattened collection of units within the structure
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductGroupViewModel> FlattenedUnits
        {
            get
            {
                if (_flattenedUnitsRO == null)
                {
                    _flattenedUnitsRO = new ReadOnlyBulkObservableCollection<ProductGroupViewModel>(_flattenedUnits);
                }
                return _flattenedUnitsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the current selected unit
        /// </summary>
        public ProductGroupViewModel SelectedUnit
        {
            get { return _selectedUnit; }
            set
            {
                _selectedUnit = value;
                OnPropertyChanged(SelectedUnitProperty);
                OnSelectedUnitChanged();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MerchHierarchyViewModel()
        {
            UpdatePermissionFlags();

            _flattenedLevels.BulkCollectionChanged += FlattenedLevels_BulkCollectionChanged;

            _structureViewModel.ModelChanged += StructureViewModel_ModelChanged;
            _structureViewModel.FetchMerchandisingHierarchyForCurrentEntity();
        }

        #endregion

        #region Commands

        #region Save Command

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the hierarchy
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }


        private Boolean Save_CanExecute()
        {
            //Edit must be allowed
            if (!_userHasProductHierarchyEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //Do not allow duplicate group codes
            if (!_allGroupsValid)
            {
                String disabledReason = String.Format(Message.Hierarchy_CodeDuplicate, _duplicateCode, _originalCode);
                this.SaveCommand.DisabledReason = disabledReason;
                return false;
            }

            //The model must be valid
            if (!_structureViewModel.Model.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveHierarchy();
        }

        private Boolean SaveHierarchy()
        {
            Boolean continueWithSave = true;

            base.ShowWaitCursor(true);
            _suppressHierarchyChangeHandler = true;

            try
            {
                _structureViewModel.Save();
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                continueWithSave = false;

                LocalHelper.RecordException(ex, _exceptionCategory);
                Exception rootException = ex.GetBaseException();

                //GFS-22254 if it is the duplicate key row exception inform the user 
                if (rootException is DuplicateException)
                {
                    //Inform user that a duplicate already exists
                    LocalHelper.ShowDuplicateExceptionMessage(this.AttachedControl, (DuplicateException)rootException);
                }
                //if it is a concurrency exception check if the user wants to reload
                else if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.CurrentStructure.Name);
                    if (itemReloadRequired)
                    {
                        _structureViewModel.FetchMerchandisingHierarchyForCurrentEntity();
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.CurrentStructure.Name, OperationType.Save);
                }

                base.ShowWaitCursor(true);
            }

            _suppressHierarchyChangeHandler = false;
            base.ShowWaitCursor(false);

            return continueWithSave;
        }

        #endregion

        #region SaveAndClose Command

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current structure and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => SaveAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private Boolean SaveAndClose_CanExecute()
        {
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAndCloseCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveHierarchy();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }

        }
        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region ExpandAllCommand

        private RelayCommand _expandAllCommand;
        /// <summary>
        /// expandAlls the current maintenance form
        /// </summary>
        public RelayCommand ExpandAllCommand
        {
            get
            {
                if (_expandAllCommand == null)
                {
                    _expandAllCommand = new RelayCommand(p => ExpandAll_Executed())
                    {
                        FriendlyName = Message.HierarchyMaintenance_ExpandAll,
                        FriendlyDescription = Message.HierarchyMaintenance_ExpandAll_Desc,
                        Icon = ImageResources.HierarchyMaintenance_ExpandAll,
                    };
                    base.ViewModelCommands.Add(_expandAllCommand);
                }
                return _expandAllCommand;
            }
        }

        private void ExpandAll_Executed()
        {
            //handled in code behind
        }

        #endregion

        #region CollapseAllCommand

        private RelayCommand _collapseAllCommand;
        /// <summary>
        /// minimizeAlls the current maintenance form
        /// </summary>
        public RelayCommand CollapseAllCommand
        {
            get
            {
                if (_collapseAllCommand == null)
                {
                    _collapseAllCommand = new RelayCommand(p => CollapseAll_Executed())
                    {
                        FriendlyName = Message.HierarchyMaintenance_CollapseAll,
                        FriendlyDescription = Message.HierarchyMaintenance_CollapseAll_Desc,
                        Icon = ImageResources.HierarchyMaintenance_CollapseAll,
                    };
                    base.ViewModelCommands.Add(_collapseAllCommand);
                }
                return _collapseAllCommand;
            }
        }

        private void CollapseAll_Executed()
        {
            //handled in code behind
        }

        #endregion

        #region AddUnitCommand

        private RelayCommand _addUnitCommand;

        /// <summary>
        /// Shows the dialog to add a new unit.
        /// </summary>
        public RelayCommand AddUnitCommand
        {
            get
            {
                if (_addUnitCommand == null)
                {
                    _addUnitCommand = new RelayCommand(
                       p => AddUnit_Executed(), p => AddUnit_CanExecute())
                    {
                        FriendlyName = Message.MerchHierarchy_AddUnit,
                        FriendlyDescription = Message.MerchHierarchy_AddUnit_Description,
                        Icon = ImageResources.MerchHierarchy_AddUnit
                    };
                    base.ViewModelCommands.Add(_addUnitCommand);
                }
                return _addUnitCommand;
            }
        }


        private Boolean AddUnit_CanExecute()
        {
            //user has permission to create a group
            if (!_userHasProductHierarchyEditPerm)
            {
                this.AddUnitCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            ProductGroup selectedGroup = (this.SelectedUnit != null) ? this.SelectedUnit.ProductGroup : null;

            //use the model helper to check if the action is legal
            String actionError = ProductHierarchy.IsAddGroupAllowed(selectedGroup);
            if (!(String.IsNullOrEmpty(actionError)))
            {
                this.AddUnitCommand.DisabledReason = actionError;
                return false;
            }

            return true;
        }

        private void AddUnit_Executed()
        {
            _suppressHierarchyChangeHandler = true;

            ProductGroup selectedParentGroup = this.SelectedUnit.ProductGroup;

            //get the level on which the new unit will reside - do not add one if null.
            ProductLevel newUnitLevel = this.SelectedUnit.AssociatedLevel.ChildLevel;
            if (newUnitLevel != null)
            {
                if (this.AttachedControl != null)
                {
                    //show the edit node window
                    MerchHierarchyUnitEditWindow win = new MerchHierarchyUnitEditWindow(this.CurrentStructure);
                    win.ViewModel.SelectedParent = selectedParentGroup;
                    App.ShowWindow(win, this.AttachedControl, /*isModal*/true);
                }
                else
                {
                    //add a unit for testing purposes.
                    ProductGroup newProductGroup = ProductGroup.NewProductGroup(newUnitLevel.Id);
                    newProductGroup.Code = this.CurrentStructure.GetNextAvailableDefaultGroupCode();
                    newProductGroup.Name = newProductGroup.Code;
                    selectedParentGroup.ChildList.Add(newProductGroup);
                }
            }

            //reset the heirarchy units collection
            RecreateFlattenedUnitsCollection();


            _suppressHierarchyChangeHandler = false;

        }

        #endregion

        #region EditUnitCommand

        private RelayCommand _editUnitCommand;

        /// <summary>
        /// Shows the properties window for the selected unit.
        /// </summary>
        public RelayCommand EditUnitCommand
        {
            get
            {
                if (_editUnitCommand == null)
                {
                    _editUnitCommand = new RelayCommand(
                        p => EditUnit_Executed(), p => EditUnit_CanExecute())
                    {
                        FriendlyName = Message.MerchHierarchy_ShowUnitEditWindow,
                        FriendlyDescription = Message.MerchHierarchy_ShowUnitEditWindow_FriendlyDescription,
                        Icon = ImageResources.MerchHierarchy_ShowUnitEditWindow
                    };
                    base.ViewModelCommands.Add(_editUnitCommand);
                }
                return _editUnitCommand;
            }
        }

        private Boolean EditUnit_CanExecute()
        {
            //user has permission to edit a group
            if (!_userHasProductHierarchyEditPerm)
            {
                this.EditUnitCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must have a unit selected
            if (this.SelectedUnit == null)
            {
                this.EditUnitCommand.DisabledReason = Message.MerchHierarchy_UnitCommandDisabledReason;
                return false;
            }

            return true;
        }

        private void EditUnit_Executed()
        {
            _suppressHierarchyChangeHandler = true;

            if (this.AttachedControl != null)
            {
                //show the window as modal
                MerchHierarchyUnitEditWindow win = new MerchHierarchyUnitEditWindow(this.SelectedUnit.ProductGroup);
                App.ShowWindow(win, this.AttachedControl, /*isModal*/true);
            }


            //reset the heirarchy units collection
            RecreateFlattenedUnitsCollection();


            _suppressHierarchyChangeHandler = false;
        }

        #endregion

        #region RemoveUnitCommand

        private RelayCommand _removeUnitComamnd;

        /// <summary>
        /// Removes the currently selected group
        /// </summary>
        public RelayCommand RemoveUnitCommand
        {
            get
            {
                if (_removeUnitComamnd == null)
                {
                    _removeUnitComamnd = new RelayCommand(
                      p => RemoveUnit_Executed(), p => RemoveUnit_CanExecute())
                    {
                        FriendlyName = Message.MerchHierarchy_RemoveUnit,
                        FriendlyDescription = Message.MerchHierarchy_RemoveUnit_Description,
                        Icon = ImageResources.MerchHierarchy_RemoveUnit,
                        InputGestureKey = Key.Delete
                    };
                    base.ViewModelCommands.Add(_removeUnitComamnd);
                }
                return _removeUnitComamnd;
            }
        }

        private Boolean RemoveUnit_CanExecute()
        {
            //user has permission to remove a group
            if (!_userHasProductHierarchyEditPerm)
            {
                this.RemoveUnitCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            ProductGroup selectedGroup = (this.SelectedUnit != null) ? this.SelectedUnit.ProductGroup : null;

            //check the action is legal
            String actionError = ProductHierarchy.IsRemoveGroupAllowed(selectedGroup);
            if (!String.IsNullOrEmpty(actionError))
            {
                this.RemoveUnitCommand.DisabledReason = actionError;
                return false;
            }

            return true;
        }

        private void RemoveUnit_Executed()
        {
            _suppressHierarchyChangeHandler = true;

            ProductGroup groupToRemove = this.SelectedUnit.ProductGroup;
            List<ProductGroup> fullRemoveGroupsList = groupToRemove.EnumerateAllChildGroups().ToList();

            //if the group has any children
            //warn the user that child groups will also be removed.
            if (groupToRemove.ChildList.Count > 0
                && this.AttachedControl != null)
            {
                ModalMessage msg = new ModalMessage()
                {
                    Header = groupToRemove.Name,
                    Description =
                        String.Format(CultureInfo.CurrentCulture,
                            Message.Hierarchy_RemoveUnit_HasChildGroupsWarning,
                            groupToRemove.Name),
                    Button1Content = Message.Generic_Continue,
                    Button2Content = Message.Generic_Cancel,
                    ButtonCount = 2,
                    MessageIcon = ImageResources.DialogWarning
                };
                App.ShowWindow(msg, this.AttachedControl, true);

                if (msg.Result != ModalMessageResult.Button1)
                {
                    //cancel out.
                    return;
                }
            }


            base.ShowWaitCursor(true);

            Int32 parentUnitId = groupToRemove.ParentGroup.Id;

            //get a list of affected views.
            List<ProductGroupViewModel> viewsToRemove = new List<ProductGroupViewModel>();
            foreach (ProductGroup removeGroup in fullRemoveGroupsList)
            {
                ProductGroupViewModel view = _flattenedUnits.FirstOrDefault(g => g.ProductGroup == removeGroup);
                if (view != null)
                {
                    viewsToRemove.Add(view);
                }
            }

            //remove the unit from its parent
            groupToRemove.ParentGroup.ChildList.Remove(groupToRemove);

            //remove all affected group views
            _flattenedUnits.RemoveRange(viewsToRemove);


            base.ShowWaitCursor(false);

            //select the parent unit
            ProductGroupViewModel parentUnitView = _flattenedUnits.FirstOrDefault(g => g.ProductGroup.Id.Equals(parentUnitId));
            this.SelectedUnit = parentUnitView;

            _suppressHierarchyChangeHandler = false;

        }

        #endregion

        #region MoveUnitCommand

        private RelayCommand _moveSelectedUnitCommand;

        /// <summary>
        /// Moves the currently selected unit to be a child of the given unit.
        /// Param: FinancialGroupView which is to be the new parent
        /// </summary>
        public RelayCommand MoveSelectedUnitCommand
        {
            get
            {
                if (_moveSelectedUnitCommand == null)
                {
                    _moveSelectedUnitCommand = new RelayCommand(
                        p => MoveSelectedUnit_Executed(p),
                        p => MoveSelectedUnit_CanExecute(p),
                        attachToCommandManager: false)
                    {
                        //nb -this command is never actually shown.
                    };
                    base.ViewModelCommands.Add(_moveSelectedUnitCommand);
                }
                return _moveSelectedUnitCommand;
            }
        }

        private Boolean MoveSelectedUnit_CanExecute(Object arg)
        {
            //user has permission to move a group
            if (!_userHasProductHierarchyEditPerm)
            {
                this.MoveSelectedUnitCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }


            ProductGroupViewModel newParent = arg as ProductGroupViewModel;
            ProductGroup newParentGroup = (newParent != null) ? newParent.ProductGroup : null;
            ProductGroup selectedGroup = (this.SelectedUnit != null) ? this.SelectedUnit.ProductGroup : null;

            //check the move is legal
            String moveActionError = ProductHierarchy.IsMoveGroupAllowed(selectedGroup, newParentGroup);
            if (!String.IsNullOrEmpty(moveActionError))
            {
                this.MoveSelectedUnitCommand.DisabledReason = moveActionError;
                return false;
            }

            return true;
        }

        private void MoveSelectedUnit_Executed(Object arg)
        {
            ProductGroupViewModel newParent = arg as ProductGroupViewModel;
            ProductGroupViewModel groupViewToMove = this.SelectedUnit;
            if (newParent != null && groupViewToMove != null)
            {
                if (groupViewToMove.ProductGroup.ParentGroup != newParent.ProductGroup)
                {

                    //warn the user that this is about to happen as this action is on drag & drop.
                    ModalMessageResult result = ModalMessageResult.Button1;
                    if (this.AttachedControl != null)
                    {
                        ModalMessage msg = new ModalMessage()
                        {
                            Header = Message.Hierarchy_MoveUnit_WarningHeader,
                            Description = String.Format(CultureInfo.CurrentCulture,
                                            Message.Hierarchy_MoveUnit_WarningDesc,
                                            groupViewToMove.ProductGroup.Name, newParent.ProductGroup.Name),
                            ButtonCount = 2,
                            Button1Content = Message.Generic_Continue,
                            Button2Content = Message.Generic_Cancel,
                            MessageIcon = ImageResources.DialogQuestion
                        };
                        App.ShowWindow(msg, this.AttachedControl, true);
                        result = msg.Result;
                    }

                    if (result == ModalMessageResult.Button1)
                    {
                        base.ShowWaitCursor(true);
                        _suppressHierarchyChangeHandler = true;

                        //use the model helper to set the group against the new parent
                        ProductGroup movedGroup = ProductHierarchy.AssignGroupParent(groupViewToMove.ProductGroup, newParent.ProductGroup);

                        //Update all of the unit collections as the group instances will have changed
                        RecreateFlattenedUnitsCollection();

                        base.ShowWaitCursor(false);
                        _suppressHierarchyChangeHandler = false;


                        //select the newly added group
                        ProductGroupViewModel newGroupView = this.FlattenedUnits.FirstOrDefault(g => g.ProductGroup == movedGroup);
                        if (newGroupView != null)
                        {
                            this.SelectedUnit = newGroupView;
                        }
                    }
                }
            }
        }

        #endregion

        #region AddLevelCommand

        private RelayCommand _addLevelCommand;

        /// <summary>
        /// Adds a new level beneath to the current selected level
        /// </summary>
        public RelayCommand AddLevelCommand
        {
            get
            {
                if (_addLevelCommand == null)
                {
                    _addLevelCommand = new RelayCommand(
                        p => AddLevel_Executed(), p => AddLevel_CanExecute())
                    {
                        FriendlyName = Message.MerchHierarchy_AddLevel,
                        FriendlyDescription = Message.MerchHierarchy_AddLevel_Description,
                        Icon = ImageResources.MerchHierarchy_AddLevel
                    };
                    base.ViewModelCommands.Add(_addLevelCommand);
                }
                return _addLevelCommand;
            }
        }

        private Boolean AddLevel_CanExecute()
        {
            //user has permission to create a level
            if (!_userHasProductHierarchyEditPerm)
            {
                this.AddLevelCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //check the action is legal
            String actionError = ProductHierarchy.IsAddLevelAllowed(this.SelectedLevel);
            if (!String.IsNullOrEmpty(actionError))
            {
                this.AddLevelCommand.DisabledReason = actionError;
                return false;
            }

            return true;
        }

        private void AddLevel_Executed()
        {
            ProductLevel parentLevel = this.SelectedLevel;
            if (parentLevel != null)
            {
                base.ShowWaitCursor(true);
                _suppressHierarchyChangeHandler = true;

                //create and add the new level
                ProductLevel newLevel = ProductLevel.NewProductLevel();
                newLevel.Name = this.CurrentStructure.GetNextAvailableLevelName(parentLevel);
                this.CurrentStructure.AddLevel(newLevel, parentLevel);

                //select new level
                this.SelectedLevel = newLevel;

                //add the level to the flattened levels collection
                _flattenedLevels.Add(newLevel);


                _suppressHierarchyChangeHandler = false;
                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region RemoveLevelCommand

        private RelayCommand _removeLevelComamnd;

        /// <summary>
        /// Removes the selected level
        /// </summary>
        public RelayCommand RemoveLevelCommand
        {
            get
            {
                if (_removeLevelComamnd == null)
                {
                    _removeLevelComamnd = new RelayCommand(
                        p => RemoveLevel_Executed(), p => RemoveLevel_CanExecute())
                    {
                        FriendlyName = Message.MerchHierarchy_RemoveLevel,
                        FriendlyDescription = Message.MerchHierarchy_RemoveLevel_Description,
                        Icon = ImageResources.MerchHierarchy_RemoveLevel
                    };
                    base.ViewModelCommands.Add(_removeLevelComamnd);
                }
                return _removeLevelComamnd;
            }
        }

        private Boolean RemoveLevel_CanExecute()
        {
            //user has permission to remove the level
            if (!_userHasProductHierarchyEditPerm)
            {
                this.RemoveLevelCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //check the action is legal
            String actionError = ProductHierarchy.IsRemoveLevelAllowed(this.SelectedLevel);
            if (!String.IsNullOrEmpty(actionError))
            {
                this.RemoveLevelCommand.DisabledReason = actionError;
                return false;
            }

            return true;
        }

        private void RemoveLevel_Executed()
        {
            ProductLevel levelToRemove = this.SelectedLevel;
            ProductLevel parentLevel = levelToRemove.ParentLevel;

            //prompt the user if the level has groups that will also be deleted
            Boolean continueWithRemove = true;
            Boolean levelHasGroups = this.FlattenedUnits.Any(g => g.AssociatedLevel == levelToRemove);
            if (levelHasGroups)
            {
                ModalMessage msg = new ModalMessage()
                {
                    Header = levelToRemove.Name,
                    Description = Message.Hierarchy_RemoveLevel_HasGroupsWarning,
                    Button1Content = Message.Generic_Continue,
                    Button2Content = Message.Generic_Cancel,
                    ButtonCount = 2,
                    MessageIcon = ImageResources.DialogWarning
                };
                App.ShowWindow(msg, this.AttachedControl, true);
                continueWithRemove = (msg.Result == ModalMessageResult.Button1);
            }


            if (continueWithRemove)
            {
                base.ShowWaitCursor(true);
                _suppressHierarchyChangeHandler = true;

                //remove the level
                this.CurrentStructure.RemoveLevel(levelToRemove);

                //remove from the flattened levels collection
                RecreateFlattenedLevelsCollection();
                if (levelHasGroups)
                {
                    RecreateFlattenedUnitsCollection();
                }

                //select the parent level of that removed
                this.SelectedLevel = parentLevel;

                _suppressHierarchyChangeHandler = false;
                base.ShowWaitCursor(false);
            }
        }
        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of the current structure
        /// </summary>
        private void StructureViewModel_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductHierarchy> e)
        {
            if (e.OldModel != null)
            {
                e.OldModel.ChildChanged -= CurrentStructure_ChildChanged;
            }

            if (e.NewModel != null)
            {
                e.NewModel.ChildChanged += CurrentStructure_ChildChanged;
            }

            //notify of the current structure property change
            OnPropertyChanged(CurrentStructureProperty);

            //recreate the levels collection
            RecreateFlattenedLevelsCollection();

            //recreate the units collection.
            RecreateFlattenedUnitsCollection();

            //select the root
            if (this.SelectedUnit == null)
            {
                this.SelectedUnit = this.FlattenedUnits.FirstOrDefault(g => g.ProductGroup.IsRoot);
            }
        }

        /// <summary>
        /// Reponds to a change in the hierarchy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentStructure_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            if (!_suppressHierarchyChangeHandler)
            {
                if (e.PropertyChangedArgs != null)
                {
                    if (e.ChildObject is ProductGroup)
                    {
                        //if the product group code changed update the duplicate code check flag.
                        if (e.PropertyChangedArgs.PropertyName == ProductGroup.CodeProperty.Name)
                        {
                            UpdateAllGroupsValid();
                        }
                        else if (e.PropertyChangedArgs.PropertyName == ProductGroup.ProductLevelIdProperty.Name)
                        {
                            //associated level changed so updated all view paths
                            foreach (ProductGroupViewModel groupView in this.FlattenedUnits)
                            {
                                groupView.RefreshAll();
                            }
                        }
                    }
                }
                else if (e.CollectionChangedArgs != null)
                {
                    if (e.ChildObject is ProductGroupList)
                    {
                        RecreateFlattenedUnitsCollection();
                    }
                    else if (e.ChildObject is ProductLevelList)
                    {
                        RecreateFlattenedLevelsCollection();
                        foreach (ProductGroupViewModel groupView in this.FlattenedUnits)
                        {
                            groupView.RefreshAll();
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Responds to changes to the flattened levels collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FlattenedLevels_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            //update group view properties
            foreach (ProductGroupViewModel groupView in this.FlattenedUnits)
            {
                groupView.RefreshAll();
            }
        }

        /// <summary>
        /// Responds to a change of selected level
        /// </summary>
        private void OnSelectedLevelChanged()
        {
            //if a selection was made deselect the current level
            if (this.SelectedLevel != null)
            {
                this.SelectedUnit = null;
            }
        }

        /// <summary>
        /// Responds to a change of selected unit
        /// </summary>
        private void OnSelectedUnitChanged()
        {
            //if a selection was made deselect the current level
            if (this.SelectedUnit != null)
            {
                this.SelectedLevel = null;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.CurrentStructure, this.SaveCommand);
            }
            return continueExecute;
        }

        /// <summary>
        /// Recreates the entire flattened units collection.
        /// </summary>
        private void RecreateFlattenedUnitsCollection()
        {
            //get the current selected group
            String selectedUnitCode = null;
            if (this.SelectedUnit != null)
            {
                selectedUnitCode = this.SelectedUnit.ProductGroup.Code;
                this.SelectedUnit = null;
            }


            List<ProductGroup> expectedGroups =
            (this.CurrentStructure != null) ?
            this.CurrentStructure.EnumerateAllGroups().ToList() : new List<ProductGroup>();


            //compile a list of views to be removed
            if (_flattenedUnits.Count > 0)
            {
                List<ProductGroupViewModel> removeViewList = new List<ProductGroupViewModel>();
                foreach (ProductGroupViewModel view in this.FlattenedUnits)
                {
                    if (expectedGroups.Contains(view.ProductGroup))
                    {
                        expectedGroups.Remove(view.ProductGroup);
                    }
                    else
                    {
                        removeViewList.Add(view);
                    }
                }

                if (_flattenedUnits.Count != removeViewList.Count)
                {
                    _flattenedUnits.RemoveRange(removeViewList);
                }
                else
                {
                    _flattenedUnits.Clear();
                }
            }


            //add in any missing group views
            if (expectedGroups.Count > 0)
            {
                List<ProductGroupViewModel> addViewList = new List<ProductGroupViewModel>();
                foreach (ProductGroup group in expectedGroups)
                {
                    addViewList.Add(new ProductGroupViewModel(group));
                }
                _flattenedUnits.AddRange(addViewList);
            }



            //try to reselect the original selected group
            if (selectedUnitCode != null)
            {
                ProductGroupViewModel newSelectedGroup = this.FlattenedUnits.FirstOrDefault(g => g.ProductGroup.Code.Equals(selectedUnitCode));
                if (newSelectedGroup != null)
                {
                    this.SelectedUnit = newSelectedGroup;
                }
            }


            UpdateAllGroupsValid();
        }

        /// <summary>
        /// Recreates the entire flattened levels collection
        /// </summary>
        private void RecreateFlattenedLevelsCollection()
        {
            if (_flattenedLevels.Count > 0)
            {
                _flattenedLevels.Clear();
            }

            if (this.CurrentStructure != null)
            {
                _flattenedLevels.AddRange(this.CurrentStructure.EnumerateAllLevels());
            }
        }

        /// <summary>
        /// Updates _allGroupsValid to be false if a group code is a duplicate
        /// </summary>
        private void UpdateAllGroupsValid()
        {
            IEnumerable<ProductGroup> groupList = this.CurrentStructure.EnumerateAllGroups();

            //changed to use groups as this is considerably faster than linq where clause
            var groups =
                from productGroup in groupList
                group productGroup by productGroup.Code;

            foreach (var group in groups.Where(g => g.Count() > 1))
            {
                _originalCode = String.Format("\"{0} {1}\"", group.First().Code, group.First().Name);
                _duplicateCode = String.Format("\"{0} {1}\"", group.Last().Code, group.Last().Name);
                _allGroupsValid = false;
                return;
            }

            _allGroupsValid = true;
        }

        /// <summary>
        /// Gets the viewmodel of the given group and selects it
        /// </summary>
        /// <param name="groupToSelect"></param>
        public void SetSelectedGroup(ProductGroup groupToSelect)
        {
            String groupCode = groupToSelect.Code;

            ProductGroupViewModel unitToFocus =
                this.FlattenedUnits.FirstOrDefault(u => u.ProductGroup.Code.Equals(groupCode));

            if (unitToFocus != null)
            {
                this.SelectedUnit = unitToFocus;
            }

        }

        #endregion

        #region Screen Permissions

        private Boolean _userHasProductHierarchyEditPerm;

        /// <summary>
        /// Refreshes the value of the permission flags for the current user.
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //hierarchy group premissions
            _userHasProductHierarchyEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(ProductHierarchy));

        }

        /// <summary>
        /// Returns true if the current user has sufficient permissions to access this screen.
        /// </summary>
        /// <returns></returns>
        internal static Boolean UserHasRequiredAccessPerms()
        {
            return

                //product hierarchy edit
                Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.EditObject, typeof(ProductHierarchy));

        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _structureViewModel.ModelChanged -= StructureViewModel_ModelChanged;
                    _flattenedLevels.BulkCollectionChanged -= FlattenedLevels_BulkCollectionChanged;
                    this.CurrentStructure.ChildChanged -= CurrentStructure_ChildChanged;

                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
