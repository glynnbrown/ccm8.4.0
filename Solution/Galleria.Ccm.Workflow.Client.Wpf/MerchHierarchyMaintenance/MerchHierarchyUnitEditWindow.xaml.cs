﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance
{
    /// <summary>
    /// Interaction logic for ProductHierarchyUnitEditWindow.xaml
    /// </summary>
    public sealed partial class MerchHierarchyUnitEditWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MerchHierarchyUnitEditViewModel), typeof(MerchHierarchyUnitEditWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public MerchHierarchyUnitEditViewModel ViewModel
        {
            get { return (MerchHierarchyUnitEditViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MerchHierarchyUnitEditWindow senderControl = (MerchHierarchyUnitEditWindow)obj;

            if (e.OldValue != null)
            {
                MerchHierarchyUnitEditViewModel oldModel = (MerchHierarchyUnitEditViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                MerchHierarchyUnitEditViewModel newModel = (MerchHierarchyUnitEditViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }


        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MerchHierarchyUnitEditWindow(ProductHierarchy hierarchy)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new MerchHierarchyUnitEditViewModel(hierarchy, null);

            this.Loaded += new RoutedEventHandler(MerchHierarchyUnitEditWindow_Loaded);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MerchHierarchyUnitEditWindow(ProductGroup selectedGroup)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new MerchHierarchyUnitEditViewModel(selectedGroup.ParentHierarchy, selectedGroup);

            this.Loaded += new RoutedEventHandler(MerchHierarchyUnitEditWindow_Loaded);
        }

        private void MerchHierarchyUnitEditWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= MerchHierarchyUnitEditWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }

        }

        #endregion
    }
}
