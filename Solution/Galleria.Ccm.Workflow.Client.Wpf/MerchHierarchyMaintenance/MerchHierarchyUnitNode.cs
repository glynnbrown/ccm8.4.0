﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance
{
    /// <summary>
    /// Diagram node content depicting a product hierarchy unit
    /// </summary>
    public sealed class MerchHierarchyUnitNode : ContentControl
    {
        #region Properties

        #region Unit Context Property

        public static readonly DependencyProperty UnitContextProperty =
            DependencyProperty.Register("UnitContext", typeof(ProductGroupViewModel),
            typeof(MerchHierarchyUnitNode),
            new PropertyMetadata(null, OnUnitContextPropertyChanged));

        private static void OnUnitContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MerchHierarchyUnitNode senderControl = (MerchHierarchyUnitNode)obj;
            senderControl.Content = e.NewValue;
        }

        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public ProductGroupViewModel UnitContext
        {
            get { return (ProductGroupViewModel)GetValue(UnitContextProperty); }
            set { SetValue(UnitContextProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static MerchHierarchyUnitNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(MerchHierarchyUnitNode), new FrameworkPropertyMetadata(typeof(MerchHierarchyUnitNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitContext"></param>
        public MerchHierarchyUnitNode(ProductGroupViewModel unitContext)
        {
            this.UnitContext = unitContext;

        }

        #endregion

        #region Overrides

        /// <summary>
        /// To string override so that this node is sorted correctly.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            if (this.UnitContext != null)
            {
                if (this.UnitContext.ProductGroup != null)
                {
                    return this.UnitContext.ProductGroup.Name;
                }
            }

            return base.ToString();
        }

        #endregion
    }
}
