﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion

using System;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance
{
    /// <summary>
    /// Interaction logic for ProductHierarchyUnitsDisplay.xaml
    /// </summary>
    public partial class MerchHierarchyUnitsDisplay : UserControl
    {
        #region Fields
        private Boolean _suppressSelectionChangedHandling;
        #endregion

        #region Properties

        #region UnitViewContextProperty

        public static readonly DependencyProperty RootUnitViewProperty =
            DependencyProperty.Register("RootUnitView", typeof(ProductGroupViewModel),
            typeof(MerchHierarchyUnitsDisplay),
            new PropertyMetadata(null, OnRootUnitViewPropertyChanged));

        private static void OnRootUnitViewPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MerchHierarchyUnitsDisplay senderControl = (MerchHierarchyUnitsDisplay)obj;

            if (e.OldValue != null)
            {
                ProductGroupViewModel oldValue = (ProductGroupViewModel)e.OldValue;
                oldValue.ChildCollectionChanged -= senderControl.UnitView_ChildCollectionChanged;
            }

            if (e.NewValue != null)
            {
                ProductGroupViewModel newValue = (ProductGroupViewModel)e.NewValue;
                newValue.ChildCollectionChanged += senderControl.UnitView_ChildCollectionChanged;
            }

            senderControl.OnRootNodeChanged();
        }


        /// <summary>
        /// Gets/Sets the root unit for the tree display
        /// </summary>
        public ProductGroupViewModel RootUnitView
        {
            get { return (ProductGroupViewModel)GetValue(RootUnitViewProperty); }
            set { SetValue(RootUnitViewProperty, value); }
        }

        #endregion

        #region SelectedUnitProperty

        public static readonly DependencyProperty SelectedUnitProperty =
            DependencyProperty.Register("SelectedUnit", typeof(ProductGroupViewModel), typeof(MerchHierarchyUnitsDisplay),
            new PropertyMetadata(null, OnSelectedUnitPropertyChanged));

        private static void OnSelectedUnitPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MerchHierarchyUnitsDisplay senderControl = (MerchHierarchyUnitsDisplay)obj;
            senderControl.OnSelectedUnitChanged();
        }

        private void OnSelectedUnitChanged()
        {
            //return out if this is suppressed.
            if (_suppressSelectionChangedHandling) { return; }

            _suppressSelectionChangedHandling = true;

            //get the current select unit in the diagram
            MerchHierarchyUnitNode diagramSelectedNodeBase =
                 (diagram.SelectedItems.Count > 0) ? (MerchHierarchyUnitNode)diagram.SelectedItems.First() : null;
            ProductGroupViewModel selectedUnitView = (diagramSelectedNodeBase != null) ? diagramSelectedNodeBase.UnitContext : null;

            if (selectedUnitView != this.SelectedUnit)
            {
                //turn off the auto arrange for speed
                diagram.IsAutoArrangeOn = false;

                //clear any existing selection
                diagram.SelectedItems.Clear();


                MerchHierarchyUnitNode nodeVisual =
                    diagram.Items.Cast<MerchHierarchyUnitNode>().FirstOrDefault(n => n.UnitContext == this.SelectedUnit);

                if (nodeVisual != null)
                {
                    //add the node to the selection
                    diagram.SelectedItems.Add(nodeVisual);

                    ////collapse all nodes to start
                    //foreach (MerchHierarchyUnitNode node in diagram.Items)
                    //{
                    //    diagram.SetItemExpandedState(node, false);
                    //}

                    //expand all its parents
                    FrameworkElement currentParentNode =
                       diagram.Links.Where(l => l.EndItem == nodeVisual).Select(l => l.StartItem).FirstOrDefault();

                    while (currentParentNode != null)
                    {
                        diagram.SetItemExpandedState(currentParentNode, true);

                        currentParentNode =
                            diagram.Links.Where(l => l.EndItem == currentParentNode).Select(l => l.StartItem).FirstOrDefault();
                    }

                }

                diagram.IsAutoArrangeOn = true;
                if (nodeVisual != null)
                {
                    //focus on the item
                    diagram.BringItemIntoView(nodeVisual);
                }
            }

            _suppressSelectionChangedHandling = false;

        }

        /// <summary>
        /// Gets/Sets the selected unit of the tree display
        /// </summary>
        public ProductGroupViewModel SelectedUnit
        {
            get { return (ProductGroupViewModel)GetValue(SelectedUnitProperty); }
            set { SetValue(SelectedUnitProperty, value); }
        }


        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MerchHierarchyUnitsDisplay()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Redraws when the root node changes
        /// </summary>
        private void OnRootNodeChanged()
        {
            diagram.Items.Clear();

            if (this.RootUnitView != null && diagram.IsLoaded)
            {
                //turn off the auto arrange to make loading faster
                diagram.IsAutoArrangeOn = false;

                //draw the root node
                MerchHierarchyUnitNode rootNode = new MerchHierarchyUnitNode(this.RootUnitView);
                diagram.Items.Add(rootNode);

                //draw all children down
                DrawChildren(rootNode);

                //update the root offset
                diagramArrangeMethod.MinRootHorizontalOffset = xZoomBox.ViewportWidth / 2 - (rootNode.Width / 4);

                //turn auto arrange back on
                diagram.IsAutoArrangeOn = true;
            }
        }

        /// <summary>
        /// Responds to change of selected node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void diagram_NodeSelectionChanged(object sender, EventArgs e)
        {
            //return out if this is suppressed
            if (_suppressSelectionChangedHandling) { return; }

            _suppressSelectionChangedHandling = true;

            if (diagram.SelectedItems.Count > 0)
            {
                //update the property to match the diagram
                MerchHierarchyUnitNode diagramSelectedNodeBase = (MerchHierarchyUnitNode)diagram.SelectedItems.First();
                this.SelectedUnit = diagramSelectedNodeBase.UnitContext;
            }
            else
            {
                this.SelectedUnit = null;
            }

            _suppressSelectionChangedHandling = false;
        }

        /// <summary>
        /// Redraws if a nodes child collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnitView_ChildCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            ProductGroupViewModel senderUnitView = (ProductGroupViewModel)sender;
            MerchHierarchyUnitNode senderNodeBase = diagram.Items.Cast<MerchHierarchyUnitNode>()
               .FirstOrDefault(n => n.UnitContext == senderUnitView);

            if (senderNodeBase != null)
            {
                //turn off auto arrange to load faster
                diagram.IsAutoArrangeOn = false;

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (ProductGroupViewModel addedChildView in e.ChangedItems)
                        {
                            AddNode(addedChildView, senderNodeBase);
                        }
                        //select the last added child
                        //this.SelectedUnit = e.ChangedItems.Cast<ProductGroupViewModel>().Last();
                        break;

                    case NotifyCollectionChangedAction.Remove:

                        foreach (ProductGroupViewModel removedChildView in e.ChangedItems)
                        {
                            //find the child node
                            MerchHierarchyUnitNode childNodeBase = diagram.Items.Cast<MerchHierarchyUnitNode>()
                                    .FirstOrDefault(n => n.UnitContext == removedChildView);
                            
                            //clear all its child nodes
                            LocalHelper.ClearDescendantVisuals(diagram, childNodeBase);

                            //remove
                            diagram.Items.Remove(childNodeBase);
                        }
                        break;

                    default:
                        //draw the nodes recursively
                        DrawChildren(senderNodeBase);
                        break;
                }

                //turn auto arrange back on
                diagram.IsAutoArrangeOn = true;
            }
        }

        /// <summary>
        /// Reponds to the event of one node being 'dropped' on another
        /// </summary>
        /// <param name="sender">The node hit by the drop</param>
        /// <param name="e"></param>
        private void diagram_ItemDroppedOn(object sender, DiagramNodeDropArgs e)
        {
            MerchHierarchyUnitNode moveToNode = e.HitItem as MerchHierarchyUnitNode;

            if (moveToNode != null)
            {
                ProductGroupViewModel moveToUnitView = moveToNode.UnitContext;

                //only allow if the moveto node is on the same level as the units current parent
                if (this.SelectedUnit.AssociatedLevel.ParentLevel == moveToUnitView.AssociatedLevel)
                {
                    ProductHierarchy ownerStructure = this.RootUnitView.ProductGroup.ParentHierarchy;
                    ProductGroup unitToMove = this.SelectedUnit.ProductGroup;

                    //Do not attempt to move if the group is already a child of this node
                    if (!moveToUnitView.ProductGroup.ChildList.Contains(this.SelectedUnit.ProductGroup))
                    {
                        //move the unit to the new list
                        moveToUnitView.ProductGroup.ChildList.Add(unitToMove);
                        //remove from the old parent
                        unitToMove.ParentGroup.ChildList.Remove(unitToMove);

                        //re-select the group - will be the last node on the new parent
                        ProductGroup selectGroup = moveToUnitView.ProductGroup.ChildList.Last();
                        this.SelectedUnit =
                        this.RootUnitView.GetAllChildUnits().FirstOrDefault(v => v.ProductGroup == selectGroup);
                    }
                   
                }
            }
        }

        /// <summary>
        /// Called when the diagram expand all or collapse all command has executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void diagram_ExpandCollapseAllCompleted(object sender, RoutedEventArgs e)
        {
            this.xZoomBox.ZoomToFit();
        }

        #endregion

        #region Methods

        /// <summary>
        /// draws all children beneath the given parent
        /// </summary>
        /// <param name="parent"></param>
        private void DrawChildren(MerchHierarchyUnitNode parent)
        {
            LocalHelper.ClearDescendantVisuals(diagram, parent);

            foreach (ProductGroupViewModel childView in parent.UnitContext.ChildViews)
            {
                //draw the child
                MerchHierarchyUnitNode childNode = new MerchHierarchyUnitNode(childView);
                diagram.Items.Add(childNode);
                diagram.Links.Add(new DiagramItemLink(parent, childNode));


                //collapse all initially
                if (!childView.AssociatedLevel.IsRoot)
                {
                    diagram.SetItemExpandedState(childNode, false);
                }


                //recurse
                DrawChildren(childNode);
            }

        }

        private void AddNode(ProductGroupViewModel newChildView, MerchHierarchyUnitNode parent)
        {
            MerchHierarchyUnitNode childNode =
                diagram.Items.Cast<MerchHierarchyUnitNode>().FirstOrDefault(n => n.UnitContext == newChildView);

            if (childNode == null)
            {

                //draw the child
                childNode = new MerchHierarchyUnitNode(newChildView);
                diagram.Items.Add(childNode);
                diagram.Links.Add(new DiagramItemLink(parent, childNode));

                //collapse all initially
                if (!newChildView.AssociatedLevel.IsRoot)
                {
                    diagram.SetItemExpandedState(childNode, false);
                }
            }

            //recurse through its children
            DrawChildren(childNode);
        }

        #endregion

        

    }
}
