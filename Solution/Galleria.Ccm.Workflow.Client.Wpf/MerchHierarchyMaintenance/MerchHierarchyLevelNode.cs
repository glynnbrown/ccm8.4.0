﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion

using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance
{
    /// <summary>
    /// Diagram node content for depicting a product hierarchy level
    /// </summary>
    public sealed class MerchHierarchyLevelNode : ContentControl
    {
        #region Properties

        public static readonly DependencyProperty LevelContextProperty =
            DependencyProperty.Register("LevelContext", typeof(ProductLevel),
            typeof(MerchHierarchyLevelNode),
            new PropertyMetadata(null, OnLevelContextPropertyChanged));

        /// <summary>
        /// Gets/Sets the product hierarchy level represented by this node
        /// </summary>
        public ProductLevel LevelContext
        {
            get { return (ProductLevel)GetValue(LevelContextProperty); }
            private set { SetValue(LevelContextProperty, value); }
        }

        private static void OnLevelContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MerchHierarchyLevelNode senderControl = (MerchHierarchyLevelNode)obj;
            senderControl.Content = e.NewValue;
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static MerchHierarchyLevelNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(MerchHierarchyLevelNode), new FrameworkPropertyMetadata(typeof(MerchHierarchyLevelNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="level"></param>
        public MerchHierarchyLevelNode(ProductLevel level)
        {
            this.LevelContext = level;
        }

        #endregion
    }
}
