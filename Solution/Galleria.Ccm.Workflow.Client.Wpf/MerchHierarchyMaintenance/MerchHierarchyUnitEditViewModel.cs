﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//  Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.MerchHierarchyMaintenance
{
    public sealed class MerchHierarchyUnitEditViewModel
       : ViewModelAttachedControlObject<MerchHierarchyUnitEditWindow>, IDataErrorInfo
    {
        #region Fields

        private ProductHierarchy _structure;
        private readonly Dictionary<String, Int32> _dbDeletedGroupCodesLookup = new Dictionary<String, Int32>();

        private ProductGroup _selectedGroup;

        private readonly BulkObservableCollection<ProductLevel> _availableLevels = new BulkObservableCollection<ProductLevel>();
        private ReadOnlyBulkObservableCollection<ProductLevel> _availableLevelsRO;
        private ProductLevel _selectedLevel;

        private readonly BulkObservableCollection<ProductGroup> _availableParents = new BulkObservableCollection<ProductGroup>();
        private ReadOnlyBulkObservableCollection<ProductGroup> _availableParentsRO;
        private ProductGroup _selectedParent;

        #endregion

        #region Binding Property Paths
        //properties
        public static readonly PropertyPath SelectedGroupProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.SelectedGroup);
        public static readonly PropertyPath AvailableLevelsProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.AvailableLevels);
        public static readonly PropertyPath SelectedLevelProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.SelectedLevel);
        public static readonly PropertyPath AvailableParentsProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.AvailableParents);
        public static readonly PropertyPath SelectedParentProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.SelectedParent);
        public static readonly PropertyPath GroupNameProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.GroupName);
        public static readonly PropertyPath GroupCodeProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.GroupCode);

        //commands
        public static readonly PropertyPath ApplyAndCloseCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.ApplyAndCloseCommand);
        public static readonly PropertyPath ApplyAndNewCommandProperty = WpfHelper.GetPropertyPath<MerchHierarchyUnitEditViewModel>(p => p.ApplyAndNewCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the selected group context
        /// </summary>
        public ProductGroup SelectedGroup
        {
            get { return _selectedGroup; }
            private set
            {
                ProductGroup oldValue = _selectedGroup;

                _selectedGroup = value;
                OnPropertyChanged(SelectedGroupProperty);

                OnSelectedGroupChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the collection of levels available in the hierarchy
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductLevel> AvailableLevels
        {
            get
            {
                if (_availableLevelsRO == null)
                {
                    _availableLevelsRO = new ReadOnlyBulkObservableCollection<ProductLevel>(_availableLevels);
                }
                return _availableLevelsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the level at which the selected group should be placed.
        /// </summary>
        public ProductLevel SelectedLevel
        {
            get { return _selectedLevel; }
            set
            {
                _selectedLevel = value;
                OnPropertyChanged(SelectedLevelProperty);

                OnSelectedLevelChanged(value);

                Debug.Assert((value != null) ? this.AvailableLevels.Contains(value) : true, "Value is not an available level");
            }
        }

        /// <summary>
        /// Returns the collection of available parents
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductGroup> AvailableParents
        {
            get
            {
                if (_availableParentsRO == null)
                {
                    _availableParentsRO = new ReadOnlyBulkObservableCollection<ProductGroup>(_availableParents);
                }
                return _availableParentsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the parent group to which the selected group should be assigned.
        /// </summary>
        public ProductGroup SelectedParent
        {
            get { return _selectedParent; }
            set
            {
                _selectedParent = value;
                OnPropertyChanged(SelectedParentProperty);
                OnSelectedParentChanged(value);

                Debug.Assert((value != null) ? _availableParents.Contains(value) : true, "Value is not an available parent");
            }
        }

        /// <summary>
        ///Gets/Sets the name of the selected product group
        /// </summary>
        public String GroupName
        {
            get
            {
                if (_selectedGroup != null)
                {
                    return _selectedGroup.Name;
                }
                return null;
            }
            set
            {
                if (_selectedGroup != null)
                {
                    _selectedGroup.Name = value;
                    OnPropertyChanged(GroupNameProperty);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the code of the selected product group
        /// </summary>
        public String GroupCode
        {
            get
            {
                if (_selectedGroup != null)
                {
                    return _selectedGroup.Code;
                }
                return null;
            }
            set
            {
                if (_selectedGroup != null)
                {
                    _selectedGroup.Code = value;
                    OnPropertyChanged(GroupCodeProperty);
                }
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="hierarchyView"></param>
        /// <param name="selectedGroup"></param>
        public MerchHierarchyUnitEditViewModel(ProductHierarchy hierarchy, ProductGroup selectedGroup)
        {
            _structure = hierarchy;
            Debug.Assert((selectedGroup != null) ? (selectedGroup.ParentHierarchy == _structure) : true, "SelectedGroup does not belong to the given hierarchy");

            //get the collection of deleted groups for code check
            List<Int32> activeGroupIds = _structure.EnumerateAllGroups().Select(g => g.Id).Distinct().ToList();
            foreach (ProductGroupInfo dbGroupInfo in ProductGroupInfoList.FetchDeletedByProductHierarchyId(hierarchy.Id))
            {
                //if the group is deleted in the db or not present amongst the active hierarchy groups
                // then add it to the deleted groups lookup.
                if (dbGroupInfo.DateDeleted.HasValue || !activeGroupIds.Contains(dbGroupInfo.Id))
                {
                    _dbDeletedGroupCodesLookup.Add(dbGroupInfo.Code, dbGroupInfo.Id);
                }
            }

            UpdateAvailableLevels();

            //set the selected group
            if (selectedGroup == null)
            {
                this.SelectedGroup = ProductGroup.NewProductGroup(_availableLevels.FirstOrDefault().Id);
                this.SelectedGroup.Code = hierarchy.GetNextAvailableDefaultGroupCode();
            }
            else
            {
                this.SelectedGroup = selectedGroup;
            }

        }

        #endregion

        #region Commands

        #region ApplyAndCloseCommand

        private RelayCommand _applyAndCloseCommand;

        /// <summary>
        /// Commits the edit made. If the item is new, adds it to its chosen parent.
        /// </summary>
        public RelayCommand ApplyAndCloseCommand
        {
            get
            {
                if (_applyAndCloseCommand == null)
                {
                    _applyAndCloseCommand = new RelayCommand(
                        p => ApplyAndCloseCommand_Executed(),
                        p => ApplyAndCloseCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose
                    };
                    base.ViewModelCommands.Add(_applyAndCloseCommand);
                }
                return _applyAndCloseCommand;
            }
        }

        private Boolean ApplyAndCloseCommand_CanExecute()
        {
            // must be valid.
            if (!this.SelectedGroup.IsValid)
            {
                this.ApplyAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must have a parent group if it's not the root
            if (!this.SelectedGroup.IsRoot &&
                this.SelectedParent == null)
            {
                this.ApplyAndCloseCommand.DisabledReason = Message.Hierarchy_InvalidParentGroup;
                return false;
            }

            //code must not have an error
            if (!String.IsNullOrEmpty(((IDataErrorInfo)this)[GroupCodeProperty.Path]))
            {
                this.ApplyAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }


            //must have no binding errors
            if (!LocalHelper.AreBindingsValid(this.AttachedControl))
            {
                this.ApplyAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void ApplyAndCloseCommand_Executed()
        {
            ApplyChanges();

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region ApplyAndNewCommand

        private RelayCommand _applyAndNewCommand;

        /// <summary>
        /// Calls the apply command then loads a new item.
        /// </summary>
        public RelayCommand ApplyAndNewCommand
        {
            get
            {
                if (_applyAndNewCommand == null)
                {
                    _applyAndNewCommand = new RelayCommand(
                        p => ApplyAndNewCommand_Executed(),
                        p => ApplyAndNewCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndNew
                    };
                    base.ViewModelCommands.Add(_applyAndNewCommand);
                }
                return _applyAndNewCommand;
            }
        }

        private Boolean ApplyAndNewCommand_CanExecute()
        {
            //apply and close must be enabled.
            if (!this.ApplyAndCloseCommand.CanExecute())
            {
                this.ApplyAndNewCommand.DisabledReason = this.ApplyAndCloseCommand.DisabledReason;
                return false;
            }

            //user must have create perm?

            //there must be a selectedlevel
            //otherwise we can't add the item at any level
            if (this.SelectedLevel == null)
            {
                this.ApplyAndNewCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void ApplyAndNewCommand_Executed()
        {
            ProductGroup parentGroup = this.SelectedParent;

            ApplyChanges();

            //load a new group for the selected parent
            this.SelectedGroup = ProductGroup.NewProductGroup(this.SelectedLevel.Id);
            this.SelectedGroup.Code = parentGroup.ParentHierarchy.GetNextAvailableDefaultGroupCode();
            this.SelectedParent = parentGroup;
        }

        #endregion


        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected group
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedGroupChanged(ProductGroup oldValue, ProductGroup newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedGroup_PropertyChanged;

                //cancel the existing edit
                oldValue.CancelEdit();
            }


            if (newValue != null)
            {
                newValue.PropertyChanged += SelectedGroup_PropertyChanged;

                if (!newValue.IsRoot)
                {
                    //update the selected level and parent
                    this.SelectedLevel = _availableLevels.First(l => Object.Equals(l.Id, _selectedGroup.ProductLevelId));
                    this.SelectedParent = (newValue.ParentGroup != null) ? newValue.ParentGroup : _availableParents.First();
                }
                else
                {
                    //set both to null as group is the root
                    this.SelectedLevel = null;
                    this.SelectedParent = null;
                }

                //begin an edit
                newValue.BeginEdit();
            }

            OnPropertyChanged(GroupNameProperty);
            OnPropertyChanged(GroupCodeProperty);

        }

        private void SelectedGroup_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ProductGroup.NameProperty.Name)
            {
                OnPropertyChanged(GroupNameProperty);
            }
            else if (e.PropertyName == ProductGroup.CodeProperty.Name)
            {
                OnPropertyChanged(GroupCodeProperty);
            }
        }

        /// <summary>
        /// Responds to a change of selected level
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedLevelChanged(ProductLevel newValue)
        {
            //load the available parents list
            if (_availableParents.Count > 0)
            {
                _availableParents.Clear();
            }

            if (newValue != null)
            {
                //parents are on the level above that selected.
                Object parentLevelId = newValue.ParentLevel.Id;

                //add all available parents to the collection
                ProductGroup selectedGroup = this.SelectedGroup;

                Boolean isNewGroup = this.SelectedGroup == null || (selectedGroup.ParentGroup == null);
                foreach (ProductGroup group in _structure.EnumerateAllGroups())
                {
                    //if the group is for the correct level
                    if (Object.Equals(group.ProductLevelId, parentLevelId))
                    {
                        if (
                            //is a new group and the parent may be added to
                            (isNewGroup && String.IsNullOrEmpty(ProductHierarchy.IsAddGroupAllowed(group)))

                            //or group is being moved and parent is valid.
                            || (!isNewGroup && String.IsNullOrEmpty(ProductHierarchy.IsMoveGroupAllowed(selectedGroup, group)))

                          )
                        {
                            _availableParents.Add(group);
                        }
                    }
                }


                //select a parent if required
                if (!_availableParents.Contains(this.SelectedParent))
                {
                    this.SelectedParent = _availableParents.FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Responds to a change of selected parent
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedParentChanged(ProductGroup newValue)
        {
            if (newValue != null)
            {
                //get the level of the parent
                ProductLevel parentLevel = _structure.EnumerateAllLevels().FirstOrDefault(l => Object.Equals(l.Id, newValue.ProductLevelId));

                if (this.SelectedLevel != parentLevel.ChildLevel)
                {
                    //set the selected level as the child level
                    this.SelectedLevel = parentLevel.ChildLevel;
                }
            }
        }

        /// <summary>
        /// Populates the available levels collection
        /// </summary>
        private void UpdateAvailableLevels()
        {
            ProductLevel preSelectedLevel = this.SelectedLevel;
            ProductGroup preSelectedParent = this.SelectedParent;

            if (_availableLevels.Count > 0)
            {
                _availableLevels.Clear();
            }


            //get available levels with all except root and those without parent groups
            IEnumerable<ProductGroup> groups = _structure.EnumerateAllGroups();
            List<ProductLevel> validLevels = new List<ProductLevel>();
            foreach (ProductLevel level in _structure.EnumerateAllLevels())
            {
                Boolean isValidLevel = (!level.IsRoot);
                if (isValidLevel)
                {
                    isValidLevel = (groups.Any(g => Object.Equals(g.ProductLevelId, level.ParentLevel.Id)));
                }

                if (isValidLevel)
                {
                    validLevels.Add(level);
                }

            }

            //populate the collection.
            _availableLevels.AddRange(validLevels);

            //reselect the level and parent from before
            this.SelectedLevel = (this.AvailableLevels.Contains(preSelectedLevel)) ? preSelectedLevel : this.AvailableLevels.LastOrDefault();
            this.SelectedParent = (this.AvailableParents.Contains(preSelectedParent)) ? preSelectedParent : this.AvailableParents.LastOrDefault();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Applies changes made to the current item
        /// </summary>
        private void ApplyChanges()
        {
            ProductGroup group = this.SelectedGroup;

            if (group != null)
            {
                if (!group.IsRoot)
                {
                    group.ProductLevelId = this.SelectedLevel.Id;

                    //apply the item edit
                    group.ApplyEdit();

                    //use the model helper to set the group against the correct parent.
                    ProductHierarchy.AssignGroupParent(group, this.SelectedParent);

                    //begin a new edit
                    group.BeginEdit();

                    //update the available levels in case this as changed them
                    UpdateAvailableLevels();
                }
                else
                {
                    //if a root group just apply the edit and begin a new one.
                    group.ApplyEdit();
                    group.BeginEdit();
                }
            }
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.SelectedGroup = null;
                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        public String this[String columnName]
        {
            get
            {
                if (this.SelectedGroup != null)
                {
                    if (columnName == GroupNameProperty.Path)
                    {
                        return (SelectedGroup as IDataErrorInfo)[ProductGroup.NameProperty.Name];
                    }
                    else if (columnName == GroupCodeProperty.Path)
                    {
                        String codeError = (SelectedGroup as IDataErrorInfo)[ProductGroup.CodeProperty.Name];
                        if (String.IsNullOrEmpty(codeError))
                        {
                            ProductGroup currentGroup = this.SelectedGroup;
                            String newCodeValue = this.GroupCode;

                            ProductGroup matchingGroup =
                                _structure.EnumerateAllGroups().FirstOrDefault(
                                g => g != currentGroup && String.Compare(g.Code, newCodeValue, StringComparison.OrdinalIgnoreCase) == 0);
                            if (matchingGroup != null)
                            {
                                codeError = Message.MerchandisingHierarchyUnitEdit_GroupCodeNotUnique1;
                            }
                            else
                            {
                                Int32 matchingGroupId;
                                if (_dbDeletedGroupCodesLookup.TryGetValue(newCodeValue, out matchingGroupId))
                                {
                                    if (currentGroup.Id != matchingGroupId)
                                    {
                                        codeError = Message.MerchandisingHierarchyUnitEdit_GroupCodeNotUnique1;
                                    }
                                }
                            }

                        }

                        return codeError;
                    }
                }
                return null;
            }
        }

        #endregion
    }

}
