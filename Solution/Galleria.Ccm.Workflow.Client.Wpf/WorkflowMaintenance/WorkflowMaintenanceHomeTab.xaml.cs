﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25732 : L.Ineson
//	Created
#endregion

#endregion

using System.Windows;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance
{
    /// <summary>
    /// Interaction logic for WorkflowMaintenanceHomeTab.xaml
    /// </summary>
    public sealed partial class WorkflowMaintenanceHomeTab : RibbonTabItem
    {
        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(WorkflowMaintenanceViewModel), typeof(WorkflowMaintenanceHomeTab),
            new PropertyMetadata(null));

        public WorkflowMaintenanceViewModel ViewModel
        {
            get { return (WorkflowMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkflowMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
