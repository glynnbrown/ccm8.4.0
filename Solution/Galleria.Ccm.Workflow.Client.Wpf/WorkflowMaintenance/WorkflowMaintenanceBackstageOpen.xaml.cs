﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25732 : L.Ineson
//	Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using System.IO;
using Galleria.Framework.Controls.Wpf;
using System.Text.RegularExpressions;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance
{
    /// <summary>
    /// Interaction logic for WorkflowMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class WorkflowMaintenanceBackstageOpen : UserControl
    {

        #region Fields
        private ObservableCollection<WorkflowInfo> _searchResults = new ObservableCollection<WorkflowInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(WorkflowMaintenanceViewModel), typeof(WorkflowMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public WorkflowMaintenanceViewModel ViewModel
        {
            get { return (WorkflowMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WorkflowMaintenanceBackstageOpen senderControl = (WorkflowMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                WorkflowMaintenanceViewModel oldModel = (WorkflowMaintenanceViewModel)e.OldValue;
                oldModel.AvailableWorkflows.BulkCollectionChanged -= senderControl.ViewModel_AvailableWorkflowsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                WorkflowMaintenanceViewModel newModel = (WorkflowMaintenanceViewModel)e.NewValue;
                newModel.AvailableWorkflows.BulkCollectionChanged += senderControl.ViewModel_AvailableWorkflowsBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(WorkflowMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WorkflowMaintenanceBackstageOpen senderControl = (WorkflowMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<WorkflowInfo>),
            typeof(WorkflowMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<WorkflowInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<WorkflowInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public WorkflowMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<WorkflowInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Updates the search results when the available locations list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableWorkflowsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// Opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                WorkflowInfo itemToOpen = (WorkflowInfo)senderControl.SelectedItem;

                if (itemToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                String searchPattern = LocalHelper.GetRexexKeywordPattern(this.SearchText);

                IEnumerable<WorkflowInfo> results =
                    this.ViewModel.AvailableWorkflows
                    .Where(w => Regex.IsMatch(w.Name, searchPattern, RegexOptions.IgnoreCase))
                    .OrderBy(l => l.ToString());

                foreach (WorkflowInfo locInfo in results)
                {
                    _searchResults.Add(locInfo);
                }
            }
        }

        #endregion

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selected = searchResultsListBox.SelectedItem as WorkflowInfo;
                if(selected != null)
                    this.ViewModel.OpenCommand.Execute(selected.Id);
            }
        }
    }
}
