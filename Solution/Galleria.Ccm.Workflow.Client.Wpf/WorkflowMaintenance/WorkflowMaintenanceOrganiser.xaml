﻿<!--Header Information-->
<!--Copyright © Galleria RTS Ltd 2014.-->

<!-- Version History : (8.0.0)
   V8-25732 : L.Ineson
	~ Created
-->

<!-- Version History : (8.1.1)
    V8-28224 : M.Shelley
        Added a few UI tweaks and the currently selected work flow task description.
-->
<!-- Version History : (8.1.1)
    V8-30325 : I.George ~ Added tooltip to the backstage commands
    V8-30412 : I.George 
      Removed the border surrounding task description, Renamed task properties to task description
    V8-29640 : A.Probyn
      Added task description tooltip to the available tasks list view.
-->
<!-- Version History : (8.3.0)
    V8-32357 : M.Brumby ~ [PCR01561] Ability Customise the names of Workflow Tasks
    V8-32451 : M.Brumby ~ Control tidyup
    V8-32639 : A.Heathcote ~ Increaced MinWidth of Available Tasks Grid.
-->

<g:ExtendedRibbonWindow 
    x:Class="Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance.WorkflowMaintenanceOrganiser"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:scm="clr-namespace:System.ComponentModel;assembly=WindowsBase"
    xmlns:g="clr-namespace:Galleria.Framework.Controls.Wpf;assembly=Galleria.Framework.Controls.Wpf"
    xmlns:help="clr-namespace:Galleria.Ccm.Common.Wpf.Help;assembly=Galleria.Ccm.Common.Wpf"
    xmlns:fluent="clr-namespace:Fluent;assembly=Fluent"
    xmlns:gDiagram="clr-namespace:Galleria.Framework.Controls.Wpf.Diagram;assembly=Galleria.Framework.Controls.Wpf"
    xmlns:objectModel="clr-namespace:Galleria.Ccm.Model;assembly=Galleria.CCM"
    xmlns:root="clr-namespace:Galleria.Ccm.Workflow.Client.Wpf"
    xmlns:lg="clr-namespace:Galleria.Ccm.Workflow.Client.Wpf.Resources.Language"
    xmlns:local="clr-namespace:Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance"
    x:Name="rootControl">

    <g:ExtendedRibbonWindow.Resources>

        <local:WorkflowParameterTemplateSelector x:Key="WorkflowParameterTemplateSelector"/>

        <DataTemplate x:Key="Workflow_DTTaskNode">
            <Grid ToolTip="{Binding Path=VisibleDescription, Mode=OneWay}" ToolTipService.ShowDuration="60000">

                <ContentControl DataContext="{Binding RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:WorkflowTaskNode}}}"
                                Style="{StaticResource {x:Static root:ResourceKeys.Diagram_StyNodeShapeSelector}}"
                                Content="{Binding Path=ShapeNo}"
                                Background="{Binding Path=Background}"
                                BorderBrush="{Binding Path=BorderBrush}"
                                BorderThickness="{Binding Path=BorderThickness}"/>

                <TextBlock Text="{Binding Path=VisibleName, Mode=OneWay}" MaxWidth="125" TextWrapping="Wrap" TextAlignment="Center"
                           HorizontalAlignment="Center" VerticalAlignment="Center"/>
            </Grid>
        </DataTemplate>

        <Style TargetType="{x:Type local:WorkflowTaskNode}" BasedOn="{StaticResource {x:Type ContentControl}}">
            <Setter Property="Height" Value="50"/>
            <Setter Property="Width" Value="130"/>
            <Setter Property="BorderBrush" Value="{StaticResource {x:Static root:ResourceKeys.Diagram_BrushNodeNormalBorder}}"/>
            <Setter Property="BorderThickness" Value="1"/>
            <Setter Property="ContentTemplate" Value="{StaticResource Workflow_DTTaskNode}"/>
            <Setter Property="Background" Value="{x:Static SystemColors.ControlBrush}"/>
            <Style.Triggers>
                <Trigger Property="gDiagram:DiagramItem.IsSelected" Value="True">
                    <Setter Property="BorderThickness" Value="5"/>
                    <Setter Property="BorderBrush" Value="{StaticResource {x:Static g:ResourceKeys.Main_BrushSelected}}"/>
                </Trigger>
            </Style.Triggers>
        </Style>

    </g:ExtendedRibbonWindow.Resources>


    <g:ExtendedRibbonWindow.Title>
        <MultiBinding Converter="{StaticResource {x:Static g:ResourceKeys.ConverterStringFormat}}">
            <Binding Source="[ {0} ] - {1}"/>
            <Binding Path="ViewModel.CurrentWorkflow.Name" ElementName="rootControl"/>
            <Binding Source="{x:Static lg:Message.WorkflowMaintenance_Title}"/>
        </MultiBinding>
    </g:ExtendedRibbonWindow.Title>

    <Grid DataContext="{Binding Path=ViewModel, ElementName=rootControl}">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*" />
        </Grid.RowDefinitions>

        <fluent:Ribbon x:Name="Ribbon" IsQuickAccessToolBarVisible="False">
            <fluent:Ribbon.Menu>
                <fluent:Backstage>
                    <fluent:BackstageTabControl>

                        <fluent:Button CanAddToQuickAccessToolBar="False" Command="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.SaveCommandProperty}}"
                           Header="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"
                           Icon="{Binding Path=Command.SmallIcon, RelativeSource={RelativeSource Self}}"
                                       g:CommandToolTip.SourceCommand="{Binding SaveCommand}"
                                       fluent:KeyTip.Keys="{x:Static lg:KeyTips.Save}"
                                       fluent:KeyTip.AutoPlacement="False"
                                       fluent:KeyTip.HorizontalAlignment="Left"/>

                        <fluent:Button CanAddToQuickAccessToolBar="False" Command="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.SaveAsCommandProperty}}"
                           Header="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"
                           Icon="{Binding Path=Command.SmallIcon, RelativeSource={RelativeSource Self}}"
                                       g:CommandToolTip.SourceCommand="{Binding SaveAsCommand}"
                                       fluent:KeyTip.Keys="{x:Static lg:KeyTips.SaveAs}"
                                       fluent:KeyTip.AutoPlacement="False"
                                       fluent:KeyTip.HorizontalAlignment="Left"/>

                        <fluent:Button CanAddToQuickAccessToolBar="False" Command="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.DeleteCommandProperty}}"
                           Header="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"
                           Icon="{Binding Path=Command.SmallIcon, RelativeSource={RelativeSource Self}}"
                                       g:CommandToolTip.SourceCommand="{Binding DeleteCommand}"
                                       fluent:KeyTip.Keys="{x:Static lg:KeyTips.Delete}"
                                       fluent:KeyTip.AutoPlacement="False"
                                       fluent:KeyTip.HorizontalAlignment="Left"/>

                        <fluent:Button CanAddToQuickAccessToolBar="False" Command="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.NewCommandProperty}}"
                           Header="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"
                           Icon="{Binding Path=Command.SmallIcon, RelativeSource={RelativeSource Self}}"
                                       g:CommandToolTip.SourceCommand="{Binding NewCommand}"
                                       fluent:KeyTip.Keys="{x:Static lg:KeyTips.New}"
                                       fluent:KeyTip.AutoPlacement="False"
                                       fluent:KeyTip.HorizontalAlignment="Left"/>


                        <fluent:BackstageTabItem x:Name="xBackstageOpen" Header="{x:Static lg:Message.Generic_Open}"
                                                 fluent:KeyTip.Keys="{x:Static lg:KeyTips.Open}"
                                                 fluent:KeyTip.AutoPlacement="False"
                                                 fluent:KeyTip.HorizontalAlignment="Left">
                            <local:WorkflowMaintenanceBackstageOpen Margin="30" ViewModel="{Binding}"/>
                        </fluent:BackstageTabItem>

                        <fluent:BackstageTabItem Header="{x:Static lg:Message.Generic_Help}"
                                                 fluent:KeyTip.Keys="{x:Static lg:KeyTips.Help}"
                                                 fluent:KeyTip.HorizontalAlignment="Left"
                                                 fluent:KeyTip.AutoPlacement="False">
                            <help:BackstageHelp />
                        </fluent:BackstageTabItem>

                        <fluent:Button CanAddToQuickAccessToolBar="False" Command="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.CloseCommandProperty}}"
                           Header="{Binding Path=Command.FriendlyName, RelativeSource={RelativeSource Self}}"
                           Icon="{Binding Path=Command.SmallIcon, RelativeSource={RelativeSource Self}}"
                           fluent:KeyTip.Keys="{x:Static lg:KeyTips.Close}"
                           fluent:KeyTip.HorizontalAlignment="Left"
                           fluent:KeyTip.AutoPlacement="False"/>


                    </fluent:BackstageTabControl>
                </fluent:Backstage>
            </fluent:Ribbon.Menu>
            <local:WorkflowMaintenanceHomeTab ViewModel="{Binding Path=., Mode=OneTime}"/>
        </fluent:Ribbon>

        <Grid Grid.Row="1"
              Background="{StaticResource {x:Static g:ResourceKeys.ThemeColour_BrushWindowContentAreaBackground}}" >
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>
            <Grid.ColumnDefinitions>
                <ColumnDefinition x:Name="xSplitterCol" Width="Auto" MinWidth="349" MaxWidth="500"/>
                <ColumnDefinition Width="Auto"/>
                <ColumnDefinition Width="*" MinWidth="50"/>
                <ColumnDefinition Width="Auto"/>
                <ColumnDefinition Width="300" MinWidth="30"/>
            </Grid.ColumnDefinitions>

            <!--region: Main info-->
            <Border Grid.Row="0" Grid.Column="2" Grid.ColumnSpan="99" Margin="5"  Background="{StaticResource {x:Static g:ResourceKeys.Main_BrushPaleMain}}"
                    DataContext="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.CurrentWorkflowProperty}}">
                <Grid Margin="2">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto"/>
                        <ColumnDefinition Width="Auto"/>
                        <ColumnDefinition Width="Auto"/>
                        <ColumnDefinition Width="*"/>
                    </Grid.ColumnDefinitions>

                    <TextBlock Text="{x:Static lg:Message.WorkflowMaintenance_CurrentWorkflow}"
                                   Margin="2" Grid.Column="0"/>

                    <!-- Name -->
                    <g:ExtendedTextBox Grid.Column="1" Width="250" Margin="8,2,2,2" VerticalAlignment="Center"
                               AllowNullTextValue="False" MaxLength="100"
                               PromptText="{Binding Path=FriendlyName, Source={x:Static objectModel:Workflow.NameProperty}, Mode=OneTime}"
                               Text="{Binding Path=Name, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}">
                        <g:ExtendedTextBox.ToolTip>
                            <TextBlock Text="{Binding Path=FriendlyName, Source={x:Static objectModel:Workflow.NameProperty}, Mode=OneTime}"/>
                        </g:ExtendedTextBox.ToolTip>
                    </g:ExtendedTextBox>

                    <!--Description -->
                    <TextBlock Text="{Binding Path=FriendlyName, Source={x:Static objectModel:Workflow.DescriptionProperty}, Mode=OneTime}"
                                   Margin="20,2,2,2" Grid.Column="2"/>

                    <g:ExtendedTextBox Grid.Column="3" HorizontalAlignment="Stretch" Margin="8,2,5,2" VerticalAlignment="Center" MaxLength="255"
                               Text="{Binding Path=Description, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ValidatesOnDataErrors=True}">
                        <g:ExtendedTextBox.ToolTip>
                            <TextBlock Text="{Binding Path=FriendlyName, Source={x:Static objectModel:Workflow.DescriptionProperty}, Mode=OneTime}"/>
                        </g:ExtendedTextBox.ToolTip>
                    </g:ExtendedTextBox>


                </Grid>
            </Border>
            <!--endregion: Main info-->

            
            <!--Available Tasks -->
            <Grid x:Name="xTaskGrid"  Grid.Column="0" Grid.Row="0" Grid.RowSpan="2"
                         MinWidth="349" HorizontalAlignment="Stretch" Margin="0,0,1,0">

                <g:SidePanel HorizontalAlignment="Stretch" Margin="0,0,1,0" Width="{Binding Path=Width, ElementName=xTaskGrid}">
                <Grid HorizontalAlignment="Stretch">
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="*"/>
                    </Grid.RowDefinitions>


                    <TextBlock Grid.Row="0" FontWeight="Bold" Margin="5,15,5,8" FontSize="12"  x:Name="lblavailable"
                               Text="{x:Static lg:Message.WorkflowMaintenance_AvailableTasksHeader}"/>

                    <g:ExtendedTextBox Grid.Row="1" HorizontalAlignment="Stretch" AutomationProperties.LabeledBy="{Binding ElementName=lblavailable}"
                                       Margin="2,4,2,4" IsSearchBox="True"
                                       Text="{Binding Path=EngineTasksSearchCriteria, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged, ElementName=rootControl}"/>


                    <ListBox Grid.Row="2" HorizontalAlignment="Stretch"  VerticalAlignment="Stretch" Margin="4,0,0,0"
                         Background="Transparent" BorderThickness="0"  x:Name="lblgrid"
                         SelectedValue="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.SelectedEngineTaskProperty}}"
                         DisplayMemberPath="Name"
                             MouseDoubleClick="AvailableTasks_MouseDoubleClick"
                             PreviewKeyDown="UIElement_OnPreviewKeyDown"
                             PreviewMouseMove="AvailableTasks_PreviewMouseMove">
                        <ListBox.GroupStyle>
                            <GroupStyle>
                                <GroupStyle.HeaderTemplate>
                                    <DataTemplate>
                                        <TextBlock FontWeight="Bold" Text="{Binding Path=Name}" Margin="0,8,0,0"/>
                                    </DataTemplate>
                                </GroupStyle.HeaderTemplate>
                            </GroupStyle>
                        </ListBox.GroupStyle>
                        <ListBox.ItemsSource>
                            <Binding Path="{x:Static local:WorkflowMaintenanceViewModel.AvailableEngineTasksProperty}"
                                     Converter="{StaticResource {x:Static g:ResourceKeys.ConverterCollectionToView}}">
                                <Binding.ConverterParameter>
                                    <CollectionViewSource x:Name="EngineTasksSource"  Filter="EngineTasksSource_Filter">
                                        <CollectionViewSource.SortDescriptions>
                                            <scm:SortDescription PropertyName="Category"/>
                                            <scm:SortDescription PropertyName="Name"/>
                                        </CollectionViewSource.SortDescriptions>
                                        <CollectionViewSource.GroupDescriptions>
                                            <PropertyGroupDescription PropertyName="Category"/>
                                        </CollectionViewSource.GroupDescriptions>
                                    </CollectionViewSource>
                                </Binding.ConverterParameter>
                            </Binding>
                        </ListBox.ItemsSource>
                        <ListBox.ItemContainerStyle>
                            <Style TargetType="{x:Type ListBoxItem}" BasedOn="{StaticResource {x:Type ListBoxItem}}">
                                <Setter Property="Padding" Value="2,3,2,3"/>
                                <Setter Property="BorderThickness" Value="0"/>
                                <Setter Property="ToolTip" Value="{Binding Path=Description}"/>
                            </Style>
                        </ListBox.ItemContainerStyle>
                    </ListBox>
                </Grid>
            </g:SidePanel>

            </Grid>
            <!-- endregion: Available Tasks -->

            <!--Available Tasks Grid splitter -->
            <GridSplitter x:Name="xAvailableTasksSplitter" Grid.Column="1" Grid.Row="0" Grid.RowSpan="2"
                          Width="2" ResizeBehavior="PreviousAndNext"
                          ResizeDirection="Columns" VerticalAlignment="Stretch"/>

            <!-- Workflow diagram -->
            <Border x:Name="xDiagramArea" Grid.Column="2"  Grid.Row="1" Background="{x:Static SystemColors.WindowBrush}"
                    HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
                    Drop="xDiagramArea_Drop">

                <ScrollViewer Background="{x:Null}" HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
                              HorizontalContentAlignment="Center" Padding="0,10,0,0"
                              HorizontalScrollBarVisibility="Hidden" VerticalScrollBarVisibility="Auto">
                    <gDiagram:Diagram x:Name="xDiagram" Width="150"
                                      HorizontalAlignment="Center" VerticalAlignment="Stretch"
                                      ItemMoveMode="None" ItemSelectionMode="Single" IsItemExpandCollapseEnabled="False"  
                                      IsVirtualizationEnabled="False" ClipToBounds="True"
                                      NodeSelectionChanged="xDiagram_NodeSelectionChanged">
                        <gDiagram:Diagram.ArrangeMethod>
                            <gDiagram:DiagramArrangeHierarchyTree x:Name="diagramArrangeMethod" HorizontalSpacing="0" VerticalSpacing="15" />
                        </gDiagram:Diagram.ArrangeMethod>
                    </gDiagram:Diagram>
                </ScrollViewer>
            </Border>

            <TextBlock Grid.Column="2" Grid.Row="1"
                       HorizontalAlignment="Center" VerticalAlignment="Center"
                       Visibility="{Binding Path=CurrentWorkflow.Tasks.Count, ConverterParameter=equals:0,
                            Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}}"
                       Text="{x:Static lg:Message.WorkflowMaintenance_WorkflowHasNoTasks}"
                       Style="{StaticResource {x:Static g:ResourceKeys.Text_StyDescription}}"
                       TextAlignment="Center"/>
            <!-- endregion: Workflow diagram -->

            <GridSplitter Grid.Column="3" Grid.Row="1"
                          Width="2" ResizeBehavior="PreviousAndNext"
                          ResizeDirection="Columns" VerticalAlignment="Stretch"/>
            
            <!-- Selected Task Detail -->
            <DockPanel Grid.Column="4" Grid.Row="1" Margin="5,5">
                <StackPanel Orientation="Vertical" DockPanel.Dock="Top"
                            Visibility="{Binding Path=CurrentWorkflow.Tasks.Count, ConverterParameter=notEquals:0,
                                Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}}" >

                    <TextBlock HorizontalAlignment="Left" Margin="5,5" FontWeight="Bold"
                               Text="{x:Static lg:Message.WorkflowMaintenance_TaskProperties_Name}" />
                    <TextBlock HorizontalAlignment="Left"  Margin="5,5" 
                               Text="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.SelectedWorkflowTaskDefaultNameProperty}}" />

                    <TextBlock HorizontalAlignment="Left" Margin="5,5" FontWeight="Bold"  x:Name="lbldisplayNamePrompt"
                                   Text="{x:Static lg:Message.WorkflowMaintenance_TaskProperties_DisplayName}" />

                    <g:ExtendedTextBox HorizontalAlignment="Stretch" Margin="5,5"
                                   PromptText="{x:Static lg:Message.WorkflowMaintenance_TaskProperties_DisplayName_Prompt}"
                                   Text="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.SelectedWorkflowTaskDisplayNameProperty}, UpdateSourceTrigger=PropertyChanged, Mode=TwoWay}"/>
                    
                    <TextBlock HorizontalAlignment="Left" 
                               Margin="5,5" FontWeight="Bold"
                                   Text="{x:Static lg:Message.WorkflowMaintenance_TaskProperties_Description}" />
                    <TextBlock HorizontalAlignment="Left" Margin="5,5"
                                   Text="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.SelectedWorkflowTaskDescriptionProperty}}" />

                    <TextBlock HorizontalAlignment="Left"  x:Name="lbldisplaydesc"
                               Margin="5,5" FontWeight="Bold"
                                   Text="{x:Static lg:Message.WorkflowMaintenance_TaskProperties_DisplayDescription}" />
                    <g:ExtendedTextBox HorizontalAlignment="Stretch"  Margin="5,5" TextWrapping="Wrap" AcceptsReturn="True"
                                       AutomationProperties.LabeledBy="{Binding ElementName=lbldisplaydesc}"
                                   PromptText="{x:Static lg:Message.WorkflowMaintenance_TaskProperties_DisplayDescription_Prompt}"
                                   Text="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.SelectedWorkflowTaskDisplayDescriptionProperty}, UpdateSourceTrigger=PropertyChanged, Mode=TwoWay}"/>

                </StackPanel>

                <ScrollViewer 
                    Grid.Row="1" Background="Transparent" 
                    HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
                    HorizontalScrollBarVisibility="Disabled" VerticalScrollBarVisibility="Auto">

                    <ItemsControl ItemsSource="{Binding Path={x:Static local:WorkflowMaintenanceViewModel.CurrentTaskParametersProperty}}">
                        <ItemsControl.ItemTemplate>
                            <DataTemplate>
                                <Border BorderBrush="LightGray" BorderThickness="1" Margin="4">
                                    <Grid>
                                        <Grid.RowDefinitions>
                                            <RowDefinition Height="Auto"/>
                                            <RowDefinition Height="Auto"/>
                                            <RowDefinition Height="Auto"/>
                                            <RowDefinition Height="Auto"/>
                                            <RowDefinition Height="Auto"/>
                                        </Grid.RowDefinitions>

                                        <TextBlock Grid.Row="0" Margin="4,4,2,4"
                                                       Text="{Binding Path=Name}" FontWeight="Bold"/>

                                        <TextBlock Grid.Row="1" Margin="6,2,2,2"
                                                       Style="{StaticResource {x:Static g:ResourceKeys.Text_StyDescription}}"
                                                       Text="{Binding Path=Description}"/>

                                        <ContentControl Grid.Row="2" Margin="6,4,2,5"
                                                            Content="{Binding}"
                                                            ContentTemplateSelector="{StaticResource WorkflowParameterTemplateSelector}"/>

                                        <StackPanel Orientation="Horizontal" Grid.Row="3" HorizontalAlignment="Left">

                                            <CheckBox Grid.Row="3" Margin="4"
                                                          Content="{x:Static lg:Message.WorkflowMaintenance_ParameterIsHidden}" 
                                                          IsChecked="{Binding Path=IsHidden}"/>

                                            <CheckBox Grid.Row="4" Margin="8,4,4,4"
                                                          Content="{x:Static lg:Message.WorkflowMaintenance_ParameterIsReadOnly}" 
                                                          IsChecked="{Binding Path=IsReadOnly}"/>
                                        </StackPanel>

                                    </Grid>
                                </Border>
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                    </ItemsControl>
                </ScrollViewer>
            </DockPanel>
            <!-- endregion: Selected Task Detail -->

        </Grid>

    </Grid>

</g:ExtendedRibbonWindow>
