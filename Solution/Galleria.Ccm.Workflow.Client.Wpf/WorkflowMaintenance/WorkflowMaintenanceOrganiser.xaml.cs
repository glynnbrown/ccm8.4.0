﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25732 : L.Ineson
//	Created
#endregion
#region Version History: (CCM 8.1.1)
// V8-29640 : A.Probyn
//  Added OnApplyTemplate override to ensure first column grid splitter resizes on render correctly first time .
#endregion

#endregion

using System;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Diagram;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance
{
    /// <summary>
    /// Interaction logic for WorkflowMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class WorkflowMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(WorkflowMaintenanceViewModel), typeof(WorkflowMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public WorkflowMaintenanceViewModel ViewModel
        {
            get { return (WorkflowMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WorkflowMaintenanceOrganiser senderControl = (WorkflowMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                WorkflowMaintenanceViewModel oldModel = (WorkflowMaintenanceViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.WorkflowTasksChanged -= senderControl.ViewModel_WorkflowTasksChanged;
            }

            if (e.NewValue != null)
            {
                WorkflowMaintenanceViewModel newModel = (WorkflowMaintenanceViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.WorkflowTasksChanged += senderControl.ViewModel_WorkflowTasksChanged;
            }

            senderControl.ReloadTaskNodes();
        }

        #endregion

        #region EngineTasksSearchCriteria Property

        public static readonly DependencyProperty EngineTasksSearchCriteriaProperty = 
            DependencyProperty.Register("EngineTasksSearchCriteria", typeof(String), typeof(WorkflowMaintenanceOrganiser),
            new PropertyMetadata(String.Empty, OnEngineTasksSearchCriteriaPropertyChanged));

        private static void OnEngineTasksSearchCriteriaPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            WorkflowMaintenanceOrganiser senderControl = (WorkflowMaintenanceOrganiser)obj;

            senderControl._searchPattern = LocalHelper.GetRexexKeywordPattern(e.NewValue as String);


            if (senderControl.EngineTasksSource != null)
            {

                senderControl.EngineTasksSource.View.Refresh();
            }
        }

        public String EngineTasksSearchCriteria
        {
            get { return (String)GetValue(EngineTasksSearchCriteriaProperty); }
            set { SetValue(EngineTasksSearchCriteriaProperty, value); }
        }

        private String _searchPattern = String.Empty;

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public WorkflowMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.WorkflowMaintenance);

            this.ViewModel = new WorkflowMaintenanceViewModel();


            this.Loaded += new RoutedEventHandler(WorkflowMaintenanceOrganiser_Loaded);
        }


        private void WorkflowMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(WorkflowMaintenanceOrganiser_Loaded);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            xSplitterCol.Width = new GridLength(250);
        }


        #endregion

        #region Event Handlers

        #region ViewModel Handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == WorkflowMaintenanceViewModel.SelectedWorkflowTaskProperty.Path)
            {
                SetDiagramSelectedTaskNode(this.ViewModel.SelectedWorkflowTask);
            }
        }

        /// <summary>
        /// Called whenever the viewmodel notifies that workflow tasks have changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_WorkflowTasksChanged(object sender, EventArgs e)
        {
            ReloadTaskNodes();
        }

        #endregion

        #region Organiser Controls

        /// <summary>
        /// Called whenever a key is pressed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        /// <summary>
        /// Called whenever the engine tasks source has an item to process for
        /// filtering.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EngineTasksSource_Filter(object sender, FilterEventArgs e)
        {
            EngineTaskInfo info = e.Item as EngineTaskInfo;
            if (info != null)
            {
                e.Accepted = Regex.IsMatch(info.Name, _searchPattern, RegexOptions.IgnoreCase);
            }
            else
            {
                e.Accepted = false;
            }
        }

        /// <summary>
        /// Called when the user double clicks on the available tasks list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableTasks_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (item != null)
            {
                this.ViewModel.AddTaskCommand.Execute();
            }
        }

        /// <summary>
        /// Called whenever the user moves the mouse over the available tasks list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AvailableTasks_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (!DragDropBehaviour.IsDragging && e.LeftButton == MouseButtonState.Pressed)
            {
                ListBoxItem item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
                if (item != null)
                {
                    EngineTaskInfo taskInfo = item.Content as EngineTaskInfo;
                    if (taskInfo != null)
                    {
                        DragDropBehaviour dragArgs = new DragDropBehaviour(taskInfo);
                        dragArgs.DragArea = this;
                        dragArgs.BeginDrag();
                    }
                }
            }
        }

        /// <summary>
        /// Called whenever the drag behaviour drops onto the diagram area.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xDiagramArea_Drop(object sender, DragEventArgs e)
        {
            if (this.ViewModel != null && DragDropBehaviour.IsUnpackedType<EngineTaskInfo>(e.Data))
            {
                this.ViewModel.AddTaskCommand.Execute();
            }
        }

        /// <summary>
        /// Called whenever the node selection is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xDiagram_NodeSelectionChanged(object sender, EventArgs e)
        {
            if (this.ViewModel != null)
            {
                var selectedNode = xDiagram.SelectedItem as WorkflowTaskNode;
                if (selectedNode != null)
                {
                    if (this.ViewModel.SelectedWorkflowTask != selectedNode.Context)
                    {
                        this.ViewModel.SelectedWorkflowTask = selectedNode.Context;
                    }
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Reloads the tasks diagram
        /// </summary>
        private void ReloadTaskNodes()
        {
            var diagram = this.xDiagram;

            if (diagram != null)
            {
                diagram.Items.Clear();

                if (this.ViewModel != null
                    && this.ViewModel.CurrentWorkflow != null
                    && this.ViewModel.CurrentWorkflow.Tasks.Count > 0)
                {

                    diagram.IsAutoArrangeOn = false;

                    WorkflowTaskNode lastNode = null;
                    foreach (var task in this.ViewModel.CurrentWorkflow.Tasks.OrderBy(t => t.SequenceId))
                    {
                        WorkflowTaskNode taskNode = new WorkflowTaskNode(task);
                        diagram.Items.Add(taskNode);

                        //force a layout update.
                        DiagramItemContainer container = GetContainerFromItem(taskNode);
                        if (container != null)
                        {
                            container.UpdateLayout();
                        }

                        if (lastNode != null)
                        {
                            diagram.Links.Add(new DiagramItemLink(lastNode, taskNode));
                        }

                        lastNode = taskNode;
                    }

                    diagram.IsAutoArrangeOn = true;

                    //reselect the correct task
                    SetDiagramSelectedTaskNode(this.ViewModel.SelectedWorkflowTask);
                }

                xDiagramArea.UpdateLayout();
            }
        }

        /// <summary>
        /// Sets the selected node on the diagram
        /// </summary>
        /// <param name="context"></param>
        private void SetDiagramSelectedTaskNode(WorkflowTask context)
        {
            if (xDiagram != null)
            {
                xDiagram.SelectedItem =
                    xDiagram.Items.Cast<WorkflowTaskNode>().FirstOrDefault(n => n.Context == context);
            }

        }

        /// <summary>
        /// Returns the container for the given diagram item
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private DiagramItemContainer GetContainerFromItem(FrameworkElement item)
        {
            return this.xDiagram.Children.OfType<DiagramItemContainer>().FirstOrDefault(d => d.UIElement == item);
        }

        #endregion

        #region Window Close

        /// <summary>
        /// On the application being closed, checked if changes may require saving
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }

            }

        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion

        private void xAvailableTasksSplitter_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {

        }

        private void UIElement_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                this.ViewModel.AddTaskCommand.Execute();
        }
    }

    #region Supporting Classes

    /// <summary>
    /// Diagram node for depicting a workflow task
    /// </summary>
    public sealed class WorkflowTaskNode : ContentControl
    {
        #region Properties

        #region Context Property
        public static readonly DependencyProperty ContextProperty =
            DependencyProperty.Register("Context", typeof(WorkflowTask), typeof(WorkflowTaskNode),
            new PropertyMetadata(null, OnContextPropertyChanged));

        public WorkflowTask Context
        {
            get { return (WorkflowTask)GetValue(ContextProperty); }
            private set { SetValue(ContextProperty, value); }
        }

        private static void OnContextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((WorkflowTaskNode)obj).Content = e.NewValue;
        }
        #endregion

        public Int32 ShapeNo
        {
            get { return 0; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static WorkflowTaskNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(WorkflowTaskNode), new FrameworkPropertyMetadata(typeof(WorkflowTaskNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="level"></param>
        public WorkflowTaskNode(WorkflowTask task)
        {
            this.Context = task;
        }

        #endregion
    }

    #endregion
}
