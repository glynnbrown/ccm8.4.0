﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25732 : L.Ineson
//	Created
// V8-28437 : A.Kuszyk
//  Exposed Validate method and changed it to return a boolean representing the validation result.
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  TaskParameterHelper.GetHelper paramter change. Event to force the property change even when value changes externally. 
#endregion
#region Version History : CCM 820
// V8-30728 : A.Kuszyk
//   Added SelectValues_CanExecute.
// V8-30761 : A.Kuszyk
//   Added IsRequired property.
#endregion
#region Version History : CCM 830
// V8-31830 : A.Probyn
//  Added OnValueChanged to SelectValues_Executed method so that any child paramter objects can be altered upon
//  the user selecting new values.
// V8-32086 : N.Haywood
//  Added category code
// V8-32133 : A.Probyn
//  Added new AlternateDisplayValue to return the helper value (if setup).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Engine;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance
{

    /// <summary>
    /// A view of a workflow task parameter model object
    /// </summary>
    public sealed class WorkflowParameterView : ModelView<WorkflowTaskParameter>, IDataErrorInfo
    {
        #region Fields
        private ITaskParameterHelper _taskTypeHelper;
        #endregion

        #region Binding Property Paths

        //Properties
        public static PropertyPath NameProperty = WpfHelper.GetPropertyPath<WorkflowParameterView>(p => p.Name);
        public static PropertyPath DescriptionProperty = WpfHelper.GetPropertyPath<WorkflowParameterView>(p => p.Description);
        public static PropertyPath ParameterTypeProperty = WpfHelper.GetPropertyPath<WorkflowParameterView>(p => p.ParameterType);
        public static PropertyPath EnumValuesProperty = WpfHelper.GetPropertyPath<WorkflowParameterView>(p => p.EnumValues);
        public static PropertyPath IsRequiredProperty = WpfHelper.GetPropertyPath<WorkflowParameterView>(p => p.IsRequired);
        public static PropertyPath IsRequiredTooltipProperty = WpfHelper.GetPropertyPath<WorkflowParameterView>(p => p.IsRequiredTooltip);

        #endregion

        #region Properties

        /// <summary>
        /// Indicates if this parameter value is required.
        /// </summary>
        public Boolean IsRequired
        {
            get { return String.IsNullOrEmpty(Model.IsRequired()); }
        }

        /// <summary>
        /// The reason that <see cref="IsRequired"/> might be false.
        /// </summary>
        public String IsRequiredTooltip
        {
            get { return Model.IsRequired(); }
        }

        /// <summary>
        /// Returns the parameter name
        /// </summary>
        public String Name
        {
            get { return Model.ParameterDetails.Name; }
        }

        /// <summary>
        /// Returns the parameter description
        /// </summary>
        public String Description
        {
            get { return Model.ParameterDetails.Description; }
        }

        /// <summary>
        /// Gets the type of parameter this represents
        /// </summary>
        public TaskParameterType ParameterType
        {
            get { return Model.ParameterDetails.ParameterType; }
        }

        /// <summary>
        /// Gets/Sets whether the parameter should be hidden from the bag grid.
        /// </summary>
        public Boolean IsHidden
        {
            get { return Model.IsHidden; }
            set
            {
                Model.IsHidden = value;
                OnPropertyChanged(WorkflowTaskParameter.IsHiddenProperty.Name);
            }
        }

        /// <summary>
        /// Gets/Sets whether the parameter should be readonly in the bag grid.
        /// </summary>
        public Boolean IsReadOnly
        {
            get { return Model.IsReadOnly; }
            set
            {
                Model.IsReadOnly = value;
                OnPropertyChanged(WorkflowTaskParameter.IsReadOnlyProperty.Name);
            }
        }

        /// <summary>
        /// Returns the collection of possible values for an enum type parameter.
        /// </summary>
        public EngineTaskParameterEnumValueInfoList EnumValues
        {
            get { return Model.ParameterDetails.EnumValues; }
        }

        /// <summary>
        /// Gets/Sets the fixed value of this parameter.
        /// Used for single value parameter types.
        /// </summary>
        public Object Value
        {
            get { return Model.Value; }
            set
            {
                Model.Value = value;
                
            }
        }

        /// <summary>
        /// Returns the collection of parameter values.
        /// </summary>
        public WorkflowTaskParameterValueList Values
        {
            get { return Model.Values; }
        }

        /// <summary>
        /// Returns the helper alternate display value (if setup)
        /// </summary>
        public Object AlternateDisplayValue
        {
            get { return _taskTypeHelper.AlternateDisplayValue; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="model"></param>
        public WorkflowParameterView(WorkflowTaskParameter model)
            : base(model)
        {
            _taskTypeHelper = TaskParameterHelper.GetHelper(model);

            this.Model.PropertyChanged += Model_PropertyChanged;

            this.Model.Parent.ChildChanged += Parent_ChildChanged;
        }

        #endregion

        #region Commands

        #region SelectValuesCommand

        private RelayCommand _setValueCommand;

        /// <summary>
        /// Sets the value based on the parameter type.
        /// </summary>
        public RelayCommand SelectValuesCommand
        {
            get
            {
                if (_setValueCommand == null)
                {
                    _setValueCommand = new RelayCommand(
                        p => SelectValues_Executed(),p => SelectValues_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        SmallIcon = ImageResources.Add_16
                    };
                }
                return _setValueCommand;
            }
        }


        private void SelectValues_Executed()
        {
            Window parentWindow = Galleria.Framework.Controls.Wpf.Helpers.GetWindow<WorkflowMaintenanceOrganiser>();
            List<ITaskParameterValue> newValues = _taskTypeHelper.SelectValues(this.Values, parentWindow, "");
            
            this.Values.Clear();
            foreach (ITaskParameterValue value in newValues)
            {
                this.Values.Add(WorkflowTaskParameterValue.NewWorkflowTaskParameterValue(value));
            }

            Validate();
            OnPropertyChanged("Value");

            //Perform any helper-specific tasks when the value changes (i.e. clear any child parameters)
            _taskTypeHelper.OnValueChanged();

            OnPropertyChanged("AlternateDisplayValue");

            parentWindow.UpdateLayout(); //JP?
        }

        private Boolean SelectValues_CanExecute()
        {
            if (Model != null)
            {
                String disabledReason = Model.IsRequired();
                if (!String.IsNullOrEmpty(disabledReason))
                {
                    SelectValuesCommand.DisabledReason = disabledReason;
                    return false;
                }
            }
            return true;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called the task validation method for the current values and returns the result.
        /// </summary>
        /// <returns>True if the parameter's value was valid, false if not.</returns>
        public Boolean Validate()
        {
            
            return String.IsNullOrEmpty(Error);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when a workflow task parameter in the parent task changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Parent_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            if (e.ChildObject is WorkflowTaskParameter)
            {
                OnPropertyChanged(IsRequiredProperty);
                OnPropertyChanged(IsRequiredTooltipProperty);
            }
        }

        /// <summary>
        /// Forces the views value property changed event when the models changes externally. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
                Validate();
                OnPropertyChanged("Value");
                OnPropertyChanged("AlternateDisplayValue");
            }
            else if (e.PropertyName == "Values")
            {
                OnPropertyChanged("AlternateDisplayValue");
            }
        }

        #endregion

        #region IDataErrorInfo Members

        private String _error;

        public String Error
        {
            get { return _taskTypeHelper.Validate(this.Values, /*isNullAllowed*/true); }
           
        }

        public String this[String columnName]
        {
            get
            {
                return Error;
            }
        }

        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public override void Dispose()
        {
            if (!_isDisposed)
            {
                this.Model.PropertyChanged -= Model_PropertyChanged;
                this.Model.Parent.ChildChanged -= Parent_ChildChanged;
                _isDisposed = true;
            }
            GC.SuppressFinalize(this);
        }

        #endregion
    }


    /// <summary>
    /// Collection of WorkflowParameterViews
    /// </summary>
    public sealed class WorkflowParameterViewCollection :
           ModelViewCollection<WorkflowParameterView, WorkflowTaskParameterList, WorkflowTaskParameter>
    {
    }
}
