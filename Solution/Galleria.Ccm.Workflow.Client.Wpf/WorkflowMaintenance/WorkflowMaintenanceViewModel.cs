﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25732 : L.Ineson
//	Created
// V8-28437 : A.Kuszyk
//  Added task parameter validation to Save_CanExecute and also validated values everytime
//  the task selection changes.
#endregion

#region Version History: (CCM 8.01)
// CCM-28755 : L.Ineson
//  Amended to use new window service
#endregion

#region Version History: (CCM 8.01)
// V8-28224 : M.Shelley
//  Added the currently selected work flow task description
#endregion

#region Version History: (CCM 8.1.1)
// V8-30325 : I.George
//  Added friendly description message to delete command
#endregion

#region Version History: CCM830
// V8-32357 : M.Brumby
//  [PCR01561] Ability Customise the names of Workflow Tasks
//V8-32387 : L.Ineson
//  Updated delete error message to indicate that linked workpackages might still exist.
#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance
{
    /// <summary>
    /// Viewmodel controller for the Workflow maintenance organiser
    /// </summary>
    public sealed class WorkflowMaintenanceViewModel : WindowViewModelBase
    {
        #region Fields

        const String _exCategory = "WorkflowMaintenance"; //Category name for any exceptions raised to gibraltar from here
        private ModelPermission<Model.Workflow> _itemPermissions = new ModelPermission<Model.Workflow>();

        private readonly WorkflowInfoListViewModel _masterWorkflowInfoListView = new WorkflowInfoListViewModel();
        private Model.Workflow _currentWorkflow;

        private readonly EngineTaskInfoList _availableEngineTasks;
        private EngineTaskInfo _selectedEngineTask;

        private WorkflowTask _selectedWorkflowTask;

        private readonly WorkflowParameterViewCollection _currentTaskParameters = new WorkflowParameterViewCollection();

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableWorkflowsProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.AvailableWorkflows);
        public static readonly PropertyPath CurrentWorkflowProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.CurrentWorkflow);
        public static readonly PropertyPath AvailableEngineTasksProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.AvailableEngineTasks);
        public static readonly PropertyPath SelectedEngineTaskProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SelectedEngineTask);
        public static readonly PropertyPath SelectedWorkflowTaskProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SelectedWorkflowTask);
        public static readonly PropertyPath SelectedWorkflowTaskDescriptionProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SelectedWorkflowTaskDescription);
        public static readonly PropertyPath SelectedWorkflowTaskNameProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SelectedWorkflowTaskName);
        public static readonly PropertyPath SelectedWorkflowTaskDefaultNameProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SelectedWorkflowTaskDefaultName);
        public static readonly PropertyPath SelectedWorkflowTaskDisplayNameProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SelectedWorkflowTaskDisplayName);
        public static readonly PropertyPath SelectedWorkflowTaskDisplayDescriptionProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SelectedWorkflowTaskDisplayDescription);          
        public static readonly PropertyPath CurrentTaskParametersProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.CurrentTaskParameters);
        public static readonly PropertyPath SelectedWorkflowTaskDescriptionToolTipProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SelectedWorkflowTaskDescriptionToolTip);          
        
        
        //Commands
        public static PropertyPath NewCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath OpenCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath SaveCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath SaveAndCloseCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath CloseCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(v => v.CloseCommand);
        public static PropertyPath AddTaskCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(v => v.AddTaskCommand);
        public static PropertyPath RemoveTaskCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(v => v.RemoveTaskCommand);
        public static PropertyPath MoveTaskUpCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(v => v.MoveTaskUpCommand);
        public static PropertyPath MoveTaskDownCommandProperty = GetPropertyPath<WorkflowMaintenanceViewModel>(v => v.MoveTaskDownCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available workflows.
        /// </summary>
        public ReadOnlyBulkObservableCollection<WorkflowInfo> AvailableWorkflows
        {
            get { return _masterWorkflowInfoListView.BindableCollection; }
        }

        /// <summary>
        /// Gets/Sets the currently loaded workflow.
        /// </summary>
        public Model.Workflow CurrentWorkflow
        {
            get { return _currentWorkflow; }
            private set
            {
                Model.Workflow oldValue = _currentWorkflow;

                _currentWorkflow = value;
                OnPropertyChanged(CurrentWorkflowProperty);

                OnCurrentWorkflowChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the collection of available engine tasks.
        /// </summary>
        public EngineTaskInfoList AvailableEngineTasks
        {
            get { return _availableEngineTasks; }
        }

        /// <summary>
        /// Gets/Sets the selected engine task.
        /// </summary>
        public EngineTaskInfo SelectedEngineTask
        {
            get { return _selectedEngineTask; }
            set
            {
                _selectedEngineTask = value;
                OnPropertyChanged(SelectedEngineTaskProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected workflow task.
        /// </summary>
        public WorkflowTask SelectedWorkflowTask
        {
            get { return _selectedWorkflowTask; }
            set
            {
                _selectedWorkflowTask = value;
                OnPropertyChanged(SelectedWorkflowTaskProperty);
                OnPropertyChanged(SelectedWorkflowTaskNameProperty);
                OnPropertyChanged(SelectedWorkflowTaskDefaultNameProperty);
                OnPropertyChanged(SelectedWorkflowTaskDisplayNameProperty);
                OnPropertyChanged(SelectedWorkflowTaskDescriptionProperty);
                OnPropertyChanged(SelectedWorkflowTaskDisplayDescriptionProperty);
                OnPropertyChanged(SelectedWorkflowTaskDescriptionToolTipProperty);
                

                OnSelectedWorkflowTaskChanged(value);
            }
        }

        public String SelectedWorkflowTaskDefaultName
        {
            get
            {
                return ((_selectedWorkflowTask == null) ? "" : _selectedWorkflowTask.Details.Name);
            }
        }

        public String SelectedWorkflowTaskDisplayName
        {
            get
            {
                return ((_selectedWorkflowTask == null) ? "" : _selectedWorkflowTask.DisplayName);
            }
            set
            {
                if (_selectedWorkflowTask != null)
                {
                    _selectedWorkflowTask.DisplayName = value;
                    OnPropertyChanged(SelectedWorkflowTaskDisplayNameProperty);
                }
            }
        }
        
        public String SelectedWorkflowTaskDescription
        {
            get
            {
                return ((_selectedWorkflowTask == null) ? "" : _selectedWorkflowTask.Details.Description);
            }
        }

        public String SelectedWorkflowTaskDisplayDescription
        {
            get
            {
                return ((_selectedWorkflowTask == null) ? "" : _selectedWorkflowTask.DisplayDescription);
            }
            set
            {
                if (_selectedWorkflowTask != null)
                {
                    _selectedWorkflowTask.DisplayDescription = value;
                    OnPropertyChanged(SelectedWorkflowTaskDisplayDescriptionProperty);
                }
            }
        }

        public String SelectedWorkflowTaskName
        {
            get
            {
                return (_selectedWorkflowTask == null) ? "" : _selectedWorkflowTask.VisibleName;
            }
        }
        public String SelectedWorkflowTaskDescriptionToolTip
        {
            get
            {
                return (_selectedWorkflowTask == null) ? "" : _selectedWorkflowTask.VisibleDescription;
            }
        }

        /// <summary>
        /// Returns the collection of parameter views for
        /// the currently selected task.
        /// </summary>
        public WorkflowParameterViewCollection CurrentTaskParameters
        {
            get { return _currentTaskParameters; }
        }

        #endregion

        #region Events

        #region WorkflowTasksChanged

        /// <summary>
        /// Notifies that the list of workflow tasks has changed.
        /// </summary>
        public event EventHandler WorkflowTasksChanged;

        private void RaiseWorkflowTasksChanged()
        {
            if (WorkflowTasksChanged != null)
            {
                WorkflowTasksChanged(this, EventArgs.Empty);
            }
        }

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public WorkflowMaintenanceViewModel()
        {
            _availableEngineTasks = EngineTaskInfoList.FetchAll();
            _masterWorkflowInfoListView.FetchForCurrentEntity();

            //Load a new item.
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Loads a new store
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(
                            p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    RegisterCommand(_newCommand);
                }
                return _newCommand;
            }
        }

        private Boolean New_CanExecute()
        {
            //must have create permission
            if (!_itemPermissions.CanCreate)
            {
                this.NewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;

            //create a new item
            Model.Workflow newWorkflow = Model.Workflow.NewWorkflow(App.ViewState.EntityId);
            newWorkflow.Name = null;
            newWorkflow.MarkGraphAsInitialized();

            this.CurrentWorkflow = newWorkflow;

            //close the backstage
            CloseRibbonBackstage();
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;
        /// <summary>
        /// Loads the requested workflow
        /// parameter = the id of the workflow to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        private Boolean Open_CanExecute(Int32? workflowId)
        {
            //user must have get permission
            if (!_itemPermissions.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (workflowId == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? workflowId)
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;


            ShowWaitCursor(true);

            try
            {
                //fetch the requested item
                this.CurrentWorkflow = Model.Workflow.FetchById(workflowId.Value);
            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);
                RecordException(ex, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }


            //close the backstage
            CloseRibbonBackstage();

            ShowWaitCursor(false);

        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    RegisterCommand(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.CurrentWorkflow == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //if item is new, must have create
            if (this.CurrentWorkflow.IsNew && !_itemPermissions.CanCreate)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //if item is old, user must have edit permission
            if (!this.CurrentWorkflow.IsNew && !_itemPermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.CurrentWorkflow.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            //bindings must be valid
            if (!this.AreBindingsValid())
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            // All the task parameters must be valid.
            if (!this.CurrentWorkflow.Tasks.SelectMany(t => t.Parameters).All(p => new WorkflowParameterView(p).Validate()))
            {
                this.SaveCommand.DisabledReason = Message.WorkflowMaintenance_InvalidParameterValue;
                this.SaveAndNewCommand.DisabledReason = Message.WorkflowMaintenance_InvalidParameterValue;
                this.SaveAndCloseCommand.DisabledReason = Message.WorkflowMaintenance_InvalidParameterValue;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Int32 itemId = this.CurrentWorkflow.Id;

            //** check the item unique value
            String newName;


            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       ShowWaitCursor(true);

                       foreach (WorkflowInfo locInfo in _masterWorkflowInfoListView.Model)
                       {
                           if (locInfo.Id != itemId
                               && locInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       ShowWaitCursor(false);

                       return returnValue;
                   };


            Boolean nameAccepted = GetWindowService().PromptIfNameIsNotUnique(isUniqueCheck, this.CurrentWorkflow.Name, out newName);
            if (!nameAccepted) return false;

            //set the name
            if (this.CurrentWorkflow.Name != newName) this.CurrentWorkflow.Name = newName;

            //** save the item
            ShowWaitCursor(true);

            try
            {
                this.CurrentWorkflow = this.CurrentWorkflow.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);

                Exception rootException = ex.GetBaseException();
                RecordException(rootException, _exCategory);

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(this.CurrentWorkflow.Name);
                    if (itemReloadRequired)
                    {
                        this.CurrentWorkflow = Model.Workflow.FetchById(this.CurrentWorkflow.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(this.CurrentWorkflow.Name, OperationType.Save);
                }

                return false;
            }

            //refresh the info list
            _masterWorkflowInfoListView.FetchForCurrentEntity();

            ShowWaitCursor(false);

            return true;
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// Optional param: new name
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    RegisterCommand(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.CurrentWorkflow == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentWorkflow.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the name to save as
            String copyName = null;

            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (WorkflowInfo info in _masterWorkflowInfoListView.Model)
                       {
                           if (info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       base.ShowWaitCursor(false);

                       return returnValue;
                   };


            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;


            //Copy the item and rename
            base.ShowWaitCursor(true);

            Model.Workflow itemCopy = this.CurrentWorkflow.Copy();
            itemCopy.Name = copyName;
            this.CurrentWorkflow = itemCopy;

            base.ShowWaitCursor(false);

            //save it.
            SaveCurrentItem();
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    RegisterCommand(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    RegisterCommand(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed close the window.
            if (itemSaved)
            {
                CloseWindow();
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// Parameter = store to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_itemPermissions.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.CurrentWorkflow == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.CurrentWorkflow.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            //confirm with user
            if (!GetWindowService().ConfirmDeleteWithUser(this.CurrentWorkflow.ToString())) return;

            ShowWaitCursor(true);

            //mark the item as deleted so item changed does not show.
            Model.Workflow itemToDelete = this.CurrentWorkflow;
            itemToDelete.Delete();

            //load a new item
            NewCommand.Execute();

            //commit the delete
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex, _exCategory);

                //show a message to the user,
                // it may be the workflow is still attached to a workpackage.

                GetWindowService().ShowErrorMessage(itemToDelete.Name, 
                    Message.WorkflowMaintenance_DeleteFailed);
                return;
            }

            //update the available items
            _masterWorkflowInfoListView.FetchForCurrentEntity();

            ShowWaitCursor(false);

        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    RegisterCommand(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            //close the window
            CloseWindow();
        }

        #endregion

        #region AddTaskCommand

        private RelayCommand _addTaskCommand;

        /// <summary>
        /// Adds the selected engine task
        /// as a new workflow task
        /// </summary>
        public RelayCommand AddTaskCommand
        {
            get
            {
                if (_addTaskCommand == null)
                {
                    _addTaskCommand = new RelayCommand(
                        p => AddTask_Executed(),
                        p => AddTask_CanExecute())
                        {
                            FriendlyName = Message.WorkflowMaintenance_AddTask,
                            FriendlyDescription = Message.WorkflowMaintenance_AddTask_Desc,
                            Icon = ImageResources.WorkflowMaintenance_AddTask_32,
                            SmallIcon = ImageResources.WorkflowMaintenance_AddTask_16
                        };
                    RegisterCommand(_addTaskCommand);
                }
                return _addTaskCommand;
            }
        }

        private Boolean AddTask_CanExecute()
        {
            if (this.SelectedEngineTask == null)
            {
                this.AddTaskCommand.DisabledReason = Message.WorkflowMaintenance_NoEngineTaskSelected;
                return false;
            }

            if (this.CurrentWorkflow == null)
            {
                this.AddTaskCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void AddTask_Executed()
        {
            WorkflowTask newTask = this.CurrentWorkflow.Tasks.Add(this.SelectedEngineTask);

            // adding a task to a new workflow still makes the the workflow initialized
            // so forcing the workflow to uninitialize when adding a task for a temporary fix
            if (this.CurrentWorkflow.IsInitialized)
            {
                var name = this.CurrentWorkflow.Name;
                this.CurrentWorkflow.Name = String.Format("{0}(1)", this.CurrentWorkflow.Name);
                this.CurrentWorkflow.Name = name;
            }

            RaiseWorkflowTasksChanged();

            this.SelectedWorkflowTask = newTask;
        }

        #endregion

        #region RemoveTaskCommand

        private RelayCommand _removeTaskCommand;

        /// <summary>
        /// Removes the selected engine task
        /// as a new workflow task
        /// </summary>
        public RelayCommand RemoveTaskCommand
        {
            get
            {
                if (_removeTaskCommand == null)
                {
                    _removeTaskCommand = new RelayCommand(
                        p => RemoveTask_Executed(),
                        p => RemoveTask_CanExecute())
                    {
                        FriendlyName = Message.WorkflowMaintenance_RemoveTask,
                        FriendlyDescription = Message.WorkflowMaintenance_RemoveTask_Desc,
                        Icon = ImageResources.WorkflowMaintenance_RemoveTask_32,
                        SmallIcon = ImageResources.WorkflowMaintenance_RemoveTask_16,
                        InputGestureKey = Key.Delete
                    };
                    RegisterCommand(_removeTaskCommand);
                }
                return _removeTaskCommand;
            }
        }

        private Boolean RemoveTask_CanExecute()
        {
            if (this.SelectedWorkflowTask == null)
            {
                this.RemoveTaskCommand.DisabledReason = Message.WorkflowMaintenance_NoWorkflowTaskSelected;
                return false;
            }

            if (this.CurrentWorkflow == null)
            {
                this.RemoveTaskCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void RemoveTask_Executed()
        {
            this.CurrentWorkflow.Tasks.Remove(this.SelectedWorkflowTask);
            RaiseWorkflowTasksChanged();

            this.SelectedWorkflowTask = this.CurrentWorkflow.Tasks.FirstOrDefault();
        }

        #endregion

        #region MoveTaskUpCommand

        private RelayCommand _moveTaskUpCommand;

        /// <summary>
        /// Adds the selected engine task
        /// as a new workflow task
        /// </summary>
        public RelayCommand MoveTaskUpCommand
        {
            get
            {
                if (_moveTaskUpCommand == null)
                {
                    _moveTaskUpCommand = new RelayCommand(
                        p => MoveTaskUp_Executed(),
                        p => MoveTaskUp_CanExecute())
                    {
                        FriendlyName = Message.WorkflowMaintenance_MoveTaskUp,
                        FriendlyDescription = Message.WorkflowMaintenance_MoveTaskUp_Desc,
                        SmallIcon = ImageResources.WorkflowMaintenance_MoveTaskUp_16
                    };
                    RegisterCommand(_moveTaskUpCommand);
                }
                return _moveTaskUpCommand;
            }
        }

        private Boolean MoveTaskUp_CanExecute()
        {
            if (this.SelectedWorkflowTask == null)
            {
                this.MoveTaskUpCommand.DisabledReason = Message.WorkflowMaintenance_NoWorkflowTaskSelected;
                return false;
            }

            return true;
        }

        private void MoveTaskUp_Executed()
        {
            this.CurrentWorkflow.Tasks.MoveUp(this.SelectedWorkflowTask);
            RaiseWorkflowTasksChanged();
        }

        #endregion

        #region MoveTaskDownCommand

        private RelayCommand _moveTaskDownCommand;

        /// <summary>
        /// Adds the selected engine task
        /// as a new workflow task
        /// </summary>
        public RelayCommand MoveTaskDownCommand
        {
            get
            {
                if (_moveTaskDownCommand == null)
                {
                    _moveTaskDownCommand = new RelayCommand(
                        p => MoveTaskDown_Executed(),
                        p => MoveTaskDown_CanExecute())
                    {
                        FriendlyName = Message.WorkflowMaintenance_MoveTaskDown,
                        FriendlyDescription = Message.WorkflowMaintenance_MoveTaskDown_Desc,
                        SmallIcon = ImageResources.WorkflowMaintenance_MoveTaskDown_16
                    };
                    RegisterCommand(_moveTaskDownCommand);
                }
                return _moveTaskDownCommand;
            }
        }

        private Boolean MoveTaskDown_CanExecute()
        {
            if (this.SelectedWorkflowTask == null)
            {
                this.MoveTaskDownCommand.DisabledReason = Message.WorkflowMaintenance_NoWorkflowTaskSelected;
                return false;
            }

            return true;
        }

        private void MoveTaskDown_Executed()
        {
            this.CurrentWorkflow.Tasks.MoveDown(this.SelectedWorkflowTask);
            RaiseWorkflowTasksChanged();
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current workflow changes.
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnCurrentWorkflowChanged(Model.Workflow oldValue, Model.Workflow newValue)
        {
            RaiseWorkflowTasksChanged();

            this.SelectedWorkflowTask = (newValue != null) ? newValue.Tasks.FirstOrDefault() : null;
        }

        /// <summary>
        /// Called whenever the value of the SelectedWorkflowTask property changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedWorkflowTaskChanged(WorkflowTask newValue)
        {
            _currentTaskParameters.Dettach();

            if (newValue != null)
            {
                _currentTaskParameters.Attach(newValue.Parameters);
                // Now, explicitly validate all the parameters in the selected task.
                foreach (var taskParameterView in _currentTaskParameters)
                {
                    taskParameterView.Validate();
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(this.CurrentWorkflow, this.SaveCommand);
        }

        public static Boolean CanUserAccessScreen()
        {
            //must have fetch perm
            return Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(Model.Workflow));
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.CurrentWorkflow = null;
                    _masterWorkflowInfoListView.Dispose();

                    DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }

}
