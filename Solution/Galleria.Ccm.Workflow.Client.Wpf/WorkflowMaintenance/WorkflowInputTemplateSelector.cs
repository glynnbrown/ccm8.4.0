﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25732 : L.Ineson
//	Created
#endregion
#region Version History : (CCM8.1.1)
// V8-30429  : J.Pickup
//  TaskParameterHelper.GetHelper paramter change.
#endregion

#endregion

using System.Windows;
using System.Windows.Controls;
using System;
using Galleria.Ccm.Workflow.Client.Wpf.Common.EngineTasks;

namespace Galleria.Ccm.Workflow.Client.Wpf.WorkflowMaintenance
{
    /// <summary>
    /// Template selector for inputting the workflow parameter values
    /// </summary>
    public sealed class WorkflowParameterTemplateSelector : DataTemplateSelector
    {

        #region Methods

        /// <summary>
        /// Returns the correct template for the given item.
        /// </summary>
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            WorkflowParameterView view = item as WorkflowParameterView;
            if (view != null)
            {
                var taskTypeHelper = TaskParameterHelper.GetHelper(view.Model);
                return taskTypeHelper.GetDataTemplate();
            }

            return base.SelectTemplate(item, container);
        }

        #endregion
    }
}
