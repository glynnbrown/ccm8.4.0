﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-25438 : L.Ineson 
//     Created.
// V8-25460 : L.Ineson
//  Added IsEditorClientAvailable
// V8-26172 : A.Silva 
//  Added AppFolderName and CustomColumnLayoutFolderPath.
// V8:24264 : L.Ineson
//  Added Unit of measure properties
// V8-27114 : A.Silva   
//  Added EditorClientPath.
// V8-27114 : A.Silva   
//  Added EditorClientPath.
// V8-27114 : J.Pickup
//  EditorClientPath rewritten to now pull path back from registry falling back onto DevMode and finally String.Empty as last resort.
// V8-28006 : J.Pickup
//  Added PercentageNoMultiply as a temporary solution to duplicate converter issues.

#endregion
#region Version History: (CCM 8.1.1)
// V8-30078 : I.George
//  Added Explicit implementation for EntityIdChanged event 
#endregion
#region Version History: (CCM 8.3.0)
// V8-32017 : N.Haywood
//  Added SessionPlanogramGroupSelection
// V8-32823  : J.Pickup
//  Export visbiltity work (New properties).
#endregion

#endregion

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Fluent;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Microsoft.Win32;
using System.Diagnostics;
using Galleria.Framework.Controls.Wpf.HelperClasses;

namespace Galleria.Ccm.Workflow.Client.Wpf
{
    /// <summary>
    /// Contains state information available to the entire application.
    /// </summary>
    public sealed class ViewState : IViewState, INotifyPropertyChanged
    {
        #region Constants

        const String HelpFileName = "CCM_Space_Automation.chm";
        const String registrySubkeyForAppPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\";

        /// <summary>
        ///     String similar to ..\..\..\Galleria.Ccm.Editor.Client.Wpf\bin\Debug\
        /// </summary>
        const string EditorDevFolder = @"..\..\..\Galleria.Ccm.Editor.Client.Wpf\bin\Debug\";

        /// <summary>
        ///     String similar to CCM Space Planning.exe
        /// </summary>
        const string EditorFileName = @"CCM Space Planning.exe";

        #endregion

        #region Fields

        private readonly EntityViewModel _currentEntityView = new EntityViewModel();
        private Int32 _currentEntityId = 0;
        private Int32 _currentUserId = 0;
        private UserEditorSettingsViewModel _settingsView;
        private DisplayUnitOfMeasureCollection _displayUnits;

        private String _helpFilePath;
        private readonly Dictionary<SessionDirectory, String> _sessionDirectories = new Dictionary<SessionDirectory, String>();
        private Boolean _isEditorClientAvailable = true;

        Boolean _isExportSpacemanAvailable = false;
        Boolean _isExportJDAAvailable = false;
        Boolean _isExportApolloAvailable = false;

        #endregion

        #region Properties

        /// <summary>
        /// Returns true if we have access to the editor client
        /// </summary>
        public Boolean IsEditorClientAvailable
        {
            get { return _isEditorClientAvailable; }
        }

        /// <summary>
        /// Returns the id of the current entity that the application is connected to.
        /// </summary>
        public Int32 EntityId
        {
            get { return _currentEntityId; }
            set
            {
                _currentEntityId = value;

                //if the entity view is loaded 
                // then refetch it.
                if (_currentEntityView.Model != null)
                {
                    _currentEntityView.FetchCurrent();
                }
            }
        }

        /// <summary>
        /// View of the current entity.
        /// </summary>
        public EntityViewModel CurrentEntityView
        {
            get
            {
                if (_currentEntityView.Model == null)
                {
                    _currentEntityView.FetchCurrent();
                }
                return _currentEntityView;
            }
        }

        /// <summary>
        /// Returns the id of the current user of the application.
        /// </summary>
        public Int32 UserId
        {
            get { return _currentUserId; }
            private set { _currentUserId = value; }
        }

        /// <summary>
        /// Returns the display unit of measure collection
        /// for the current entity.
        /// </summary>
        public DisplayUnitOfMeasureCollection DisplayUnits
        {
            get
            {
                //if the entity model is null force it to load.
                var entityView = CurrentEntityView;

                return _displayUnits;
            }
            private set
            {
                _displayUnits = value;
                OnPropertyChanged("DisplayUnits");
            }
        }

        /// <summary>
        /// Returns the application settings.
        /// </summary>
        public UserEditorSettingsViewModel Settings
        {
            get
            {
                if (_settingsView == null)
                {
                    _settingsView = new UserEditorSettingsViewModel();
                    _settingsView.Fetch();
                }
                return _settingsView;
            }
        }

        /// <summary>
        /// Returns the path for the SA.chm
        /// </summary>
        public String HelpFilePath
        {
            get
            {
                if (String.IsNullOrEmpty(_helpFilePath))
                {
                    String loc = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    String path = System.IO.Path.GetDirectoryName(loc);
                    _helpFilePath = System.IO.Path.Combine(path, HelpFileName);

                    if (!System.IO.File.Exists(_helpFilePath))
                    {
                        //we are debugging
                        _helpFilePath = "Resources\\HelpFiles\\" + HelpFileName;
                    }
                }
                return _helpFilePath;
            }
        }

        /// <summary>
        /// Gets or sets the path to the executable for the Editor Client from registry, else attempts to use a devleopmentpath if available.
        /// </summary>
        /// <remarks>It will return an empty <see cref="String" /> if there is not executable to be found. (Not Installed & Not Development).</remarks>
        public String EditorClientPath
        {
            get
            {
                //If editor is not available then don't allow to open
                if (!IsEditorClientAvailable) return String.Empty;

                //Try to load the most recent value from the registry
                try
                {
                    String value64 = String.Empty;
                    String value32 = String.Empty;
                    const String pathKeyName = "Path";

                    //Attempt 64bit registry locations
                    RegistryKey localKey = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry64);
                    localKey = localKey.OpenSubKey(registrySubkeyForAppPath + EditorFileName);
                    if (localKey != null)
                    {
                        if (localKey.GetValue(pathKeyName).ToString() != null)
                        {
                            value64 = localKey.GetValue(pathKeyName).ToString();
                            String fullyQualifiedPath = value64 + EditorFileName;

                            if (!String.IsNullOrEmpty(fullyQualifiedPath) && File.Exists(fullyQualifiedPath))
                                return fullyQualifiedPath;
                        }
                    }

                    //Attempt 32bit registry locations
                    RegistryKey localKey32 = RegistryKey.OpenBaseKey(Microsoft.Win32.RegistryHive.LocalMachine, RegistryView.Registry32);
                    localKey32 = localKey32.OpenSubKey(registrySubkeyForAppPath + EditorFileName);
                    if (localKey32 != null)
                    {
                        if (localKey32.GetValue(pathKeyName).ToString() != null)
                        {
                            value32 = localKey32.GetValue(pathKeyName).ToString();

                            String fullyQualifiedPath = value32 + EditorFileName;

                            if (!String.IsNullOrEmpty(fullyQualifiedPath) && File.Exists(fullyQualifiedPath))
                                return fullyQualifiedPath;
                        }
                    }
                }
                catch (Exception)
                {
                    //The registry key probably doesn't exist or a problem reading the key in which case we are in dev mode?
                }

                //The registry key probably doesn't exist in which case we are in dev mode?
                var fullDevPath = String.Concat(EditorDevFolder, EditorFileName);
                return File.Exists(fullDevPath) ? fullDevPath : String.Empty;
            }
        }

        /// <summary>
        ///     Gets the root folder for local Custom Column Layouts.
        /// </summary>
        public String CustomColumnLayoutFolderPath
        {
            get
            {
                //refetch settings so that we are always up to date.
                this.Settings.Fetch();

                return this.Settings.Model.ColumnLayoutLocation;
            }
        }

        /// <summary>
        /// The current user selected planogram group.
        /// </summary>
        public SessionPlanogramGroupSelection SessionPlanogramGroupSelection { get; set; }

        /// <summary>
        /// Returns true if ExportSpacemanVisible true in config
        /// </summary>
        public Boolean IsExportSpacemanAvailable { get { return _isExportSpacemanAvailable; } }

        /// <summary>
        /// Returns true if ExportJDAVisible true in config
        /// </summary>
        public Boolean IsExportJDAAvailable { get { return _isExportJDAAvailable; } }

        /// <summary>
        /// Returns true if ExportApolloVisible true in config
        /// </summary>
        public Boolean IsExportApolloAvailable { get { return _isExportApolloAvailable; } }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ViewState()
        {
            _currentEntityView.ModelChanged += CurrentEntityView_ModelChanged;

            this.LoadAppSettingConfigIntoViewState();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the entity model changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CurrentEntityView_ModelChanged(object sender, Framework.ViewModel.ViewStateObjectModelChangedEventArgs<Model.Entity> e)
        {
            if (e.NewModel != null)
            {
                this.DisplayUnits = DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(e.NewModel);

            }
            else
            {
                this.DisplayUnits = DisplayUnitOfMeasureCollection.Empty;
            }
        }

        #endregion

        #region Command Events

        #region ShowRibbonTabs

        public event EventHandler<ViewStateEventArgs<IEnumerable<RibbonTabItem>>> OnShowRibbonTabs;
        public void ShowRibbonTabs(IEnumerable<RibbonTabItem> ribbonTabList)
        {
            if (OnShowRibbonTabs != null)
            {
                OnShowRibbonTabs(this, new ViewStateEventArgs<IEnumerable<RibbonTabItem>>(ribbonTabList));
            }
        }

        #endregion

        #region ShowRibbonContextGroups

        public event EventHandler<ViewStateEventArgs<IEnumerable<RibbonContextualTabGroup>>> OnShowRibbonContextGroups;
        public void ShowRibbonContextGroups(IEnumerable<RibbonContextualTabGroup> contextGroupList)
        {
            if (OnShowRibbonContextGroups != null)
            {
                OnShowRibbonContextGroups(this, new ViewStateEventArgs<IEnumerable<RibbonContextualTabGroup>>(contextGroupList));
            }
        }

        #endregion

        #region ShowSidePanel

        public event EventHandler<ViewStateEventArgs<Object>> OnShowSidePanel;
        public void ShowSidePanel(Object content)
        {
            if (this.OnShowSidePanel != null)
            {
                this.OnShowSidePanel(this, new ViewStateEventArgs<Object>(content));
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Gets the current session directory path to use for the given type.
        /// </summary>
        public String GetSessionDirectory(SessionDirectory type)
        {
            //return the session directory if it exists
            String sessionDir = null;
            if (_sessionDirectories.TryGetValue(type, out sessionDir)
                && !String.IsNullOrWhiteSpace(sessionDir)
                && Directory.Exists(sessionDir))
            {
                return sessionDir;
            }

            //otherwise return the settings directory
            return SessionDirectoryHelper.GetOrCreateSettingsDirectory(type, this.Settings.Model);
        }

        /// <summary>
        /// Sets the current session directory path to use for the given type.
        /// </summary>
        public void SetSessionDirectory(SessionDirectory type, String directory)
        {
            if (!String.IsNullOrWhiteSpace(directory) && Directory.Exists(directory))
            {
                _sessionDirectories[type] = directory;
            }
        }

        /// <summary>
        /// Loads relevant properties into the current viewstate instance from the app settings within the configuration file. ( 
        /// </summary>
        private void LoadAppSettingConfigIntoViewState()
        {
            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.ToObservableCollection().Contains(Galleria.Ccm.Constants.exportSpacemanAvailable))
            {
                _isExportSpacemanAvailable = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings[Galleria.Ccm.Constants.exportSpacemanAvailable]);
            }
            else
            {
                _isExportSpacemanAvailable = false;
            }

            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.ToObservableCollection().Contains(Galleria.Ccm.Constants.exportJDAAvailable))
            {
                _isExportJDAAvailable = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings[Galleria.Ccm.Constants.exportJDAAvailable]);
            }
            else
            {
                _isExportJDAAvailable = false;
            }

            if (System.Configuration.ConfigurationManager.AppSettings.AllKeys.ToObservableCollection().Contains(Galleria.Ccm.Constants.exportApolloAvailable))
            {
                _isExportApolloAvailable = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings[Galleria.Ccm.Constants.exportApolloAvailable]);
            }
            else
            {
                _isExportApolloAvailable = false;
            }
        }

        /// <summary>
        /// Applies the color as user recent color if it doesnt already exist (Max of 30 colors)
        /// </summary>
        /// <param name="color">The color to add</param>
        internal void ApplyRecentColor(System.Windows.Media.Color color)
        {
            Int32 colorValue = ColorHelper.ColorToInt(color);

            if (!Settings.Model.Colors.Where(p => p.Color == colorValue).Any())
            {
                if (Settings.Model.Colors.Count == 30)
                {
                    Settings.Model.Colors.Remove(Settings.Model.Colors.First().Color);
                }

                Settings.Model.Colors.Add(colorValue);
            }
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region IViewState members

        event EventHandler IViewState.EntityIdChanged
        {
            add {  }
            remove {  }
        }

        Boolean IViewState.IsConnectedToRepository
        {
            get { return true; } //always connected.
        }

        #endregion

    }

    /// <summary>
    /// Our view state event arguments
    /// </summary>
    public class ViewStateEventArgs<T> : EventArgs
    {
        #region Fields
        private T _userState; // stores a user state passed within an event
        #endregion

        #region Constructors
        /// <summary>
        /// Public constructor that takes a user state object
        /// </summary>
        /// <param name="userState">An object passed through in the event</param>
        public ViewStateEventArgs(T userState)
        {
            _userState = userState;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Returns the event arguments user state
        /// </summary>
        public T UserState
        {
            get { return _userState; }
        }
        #endregion
    }

    #region Supporting Classes

    /// <summary>
    /// Represents the selection of a Planogram Group based on it's Id and whether or not it
    /// is a member of the Recent Work group.
    /// </summary>
    public sealed class SessionPlanogramGroupSelection
    {
        /// <summary>
        /// True if the Planogram Group is a member of Recent Work.
        /// </summary>
        public Boolean IsRecentWorkGroup { get; set; }

        /// <summary>
        /// The Id of the Planogram Group.
        /// </summary>
        public Int32 PlanogramGroupId { get; set; }

        /// <summary>
        /// Instantiates a selection using values from the given Planogram Group view model.
        /// </summary>
        /// <param name="planogramGroupViewModel"></param>
        public SessionPlanogramGroupSelection(Galleria.Ccm.Model.PlanogramGroup planogramGroup)
        {
            Debug.Assert((planogramGroup != null), "planogramGroup is null");
            if (planogramGroup == null) return;

            PlanogramGroupId = planogramGroup.Id;
        }

        /// <summary>
        /// Instantiates a selection using values from the given Planogram Group view model.
        /// </summary>
        /// <param name="planogramGroupViewModel"></param>
        public SessionPlanogramGroupSelection(PlanogramGroupViewModel planogramGroupViewModel)
        {
            Debug.Assert((planogramGroupViewModel != null), "planogramGroupViewModel is null");
            if (planogramGroupViewModel == null) return;

            IsRecentWorkGroup = planogramGroupViewModel.IsRecentWorkGroup;
            PlanogramGroupId = planogramGroupViewModel.PlanogramGroup.Id;
        }
    }

    #endregion
}