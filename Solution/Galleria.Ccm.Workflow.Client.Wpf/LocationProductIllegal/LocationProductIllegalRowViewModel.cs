﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History : (CCM 8.3.0)
// V8-32245 : N.Haywood
//  Changed from location to location info
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductIllegalMaintenance
{
    public class LocationProductIllegalRowViewModel : ViewModelObject
    {
        #region Fields
        private ProductInfo _productInfo;
        private Dictionary<Int16, Boolean> _locationIllegalLinks;
        private List<Int16> _initialLinks = new List<Int16>();
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductInfoProperty = WpfHelper.GetPropertyPath<LocationProductIllegalRowViewModel>(p => p.ProductInfo);
        public static readonly PropertyPath LocationIllegalLinksProperty = WpfHelper.GetPropertyPath<LocationProductIllegalRowViewModel>(p => p.LocationIllegalLinks);
        public static readonly PropertyPath InitialLinksProperty = WpfHelper.GetPropertyPath<LocationProductIllegalRowViewModel>(p => p.InitialLinks);
        public static readonly PropertyPath CurrentLinksProperty = WpfHelper.GetPropertyPath<LocationProductIllegalRowViewModel>(p => p.CurrentLinks);

        #endregion

        #region Properties

        public ProductInfo ProductInfo
        {
            get { return _productInfo; }
        }

        public Dictionary<Int16, Boolean> LocationIllegalLinks
        {
            get
            {
                return _locationIllegalLinks;
            }
        }

        public List<Int16> InitialLinks
        {
            get { return _initialLinks; }
        }

        public List<Int16> CurrentLinks
        {
            get { return _locationIllegalLinks.Where(p => p.Value == true).Select(p => p.Key).ToList(); }
        }

        #endregion

        #region Constructor

        public LocationProductIllegalRowViewModel(ProductInfo productInfo, List<LocationInfo> locationInfoList,
            Dictionary<Tuple<Int32, Int16>, LocationProductIllegal> linkDictionary)
        {
            _productInfo = productInfo;
            PopulateDictionary(locationInfoList, linkDictionary);
        }

        #endregion

        #region Methods

        public void PopulateDictionary(List<LocationInfo> locationInfoList,
            Dictionary<Tuple<Int32, Int16>, LocationProductIllegal> linkDictionary)
        {
            Dictionary<Int16, Boolean> locationIllegalDict = new Dictionary<Int16, Boolean>();
            _initialLinks.Clear();
            foreach (LocationInfo info in locationInfoList)
            {
                //Find if illegal connection exists
                LocationProductIllegal output;
                Boolean exists = linkDictionary.TryGetValue(new Tuple<Int32, Int16>(_productInfo.Id, info.Id), out output);
                //Add illegal connection to dictionary
                locationIllegalDict.Add(info.Id, exists);
                if (exists)
                {
                    _initialLinks.Add(info.Id);
                }
            }
            _locationIllegalLinks = locationIllegalDict;
        }

        public void EditDictionaryValue(Int16 key, Boolean value)
        {
            LocationIllegalLinks[key] = value;
            OnPropertyChanged(LocationIllegalLinksProperty);
        }

        public void ClearLinks()
        {
            while (!LocationIllegalLinks.All(p => p.Value == false))
            {
                Int16 key = LocationIllegalLinks.FirstOrDefault(p => p.Value != false).Key;
                LocationIllegalLinks[key] = false;
            }
            OnPropertyChanged(LocationIllegalLinksProperty);
        }

        public void ResetInitialLinks()
        {
            _initialLinks = _locationIllegalLinks.Where(p => p.Value == true).Select(p => p.Key).ToList();
            OnPropertyChanged(InitialLinksProperty);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {

                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
