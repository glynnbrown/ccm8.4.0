﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductIllegalMaintenance
{
    /// <summary>
    /// Interaction logic for LocationToProductIllegalHomeTab.xaml
    /// </summary>
    public partial class LocationToProductIllegalHomeTab : RibbonTabItem
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationToProductIllegalViewModel), typeof(LocationToProductIllegalHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationToProductIllegalViewModel ViewModel
        {
            get { return (LocationToProductIllegalViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationToProductIllegalHomeTab senderControl = (LocationToProductIllegalHomeTab)obj;

            if (e.OldValue != null)
            {
                LocationToProductIllegalViewModel oldModel = (LocationToProductIllegalViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationToProductIllegalViewModel newModel = (LocationToProductIllegalViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationToProductIllegalHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
