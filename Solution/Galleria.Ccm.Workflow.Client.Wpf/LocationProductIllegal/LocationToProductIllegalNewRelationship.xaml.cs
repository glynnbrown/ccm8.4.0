﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History : (CCM 8.3.0)
// V8-32245 : N.Haywood
//  Changed from location to location info
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductIllegalMaintenance
{
    /// <summary>
    /// Interaction logic for LocationToProductIllegalNewRelationship.xaml
    /// </summary>
    public sealed partial class LocationToProductIllegalNewRelationship : ExtendedRibbonWindow
    {
        #region Properties

        #region View Model Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationToProductIllegalNewRelationshipViewModel),
            typeof(LocationToProductIllegalNewRelationship),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationToProductIllegalNewRelationshipViewModel ViewModel
        {
            get { return (LocationToProductIllegalNewRelationshipViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationToProductIllegalNewRelationship senderControl = (LocationToProductIllegalNewRelationship)obj;

            if (e.OldValue != null)
            {
                LocationToProductIllegalNewRelationshipViewModel oldModel = (LocationToProductIllegalNewRelationshipViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationToProductIllegalNewRelationshipViewModel newModel = (LocationToProductIllegalNewRelationshipViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationToProductIllegalNewRelationship(IEnumerable<LocationInfo> availableLocations,
            IEnumerable<ProductInfo> availableProducts,
            IEnumerable<LocationProductIllegalRowViewModel> masterLinksCollection)
        {
            InitializeComponent();

            this.ViewModel = new LocationToProductIllegalNewRelationshipViewModel(
                availableLocations, availableProducts, masterLinksCollection);
        }


        #endregion

        #region Event Handlers

        /// <summary>
        /// Calls the viewmodel's event handler for filtering locations based on product selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.ViewModel.OnProductsGrid_SelectedChanged(sender, e);
        }

        #endregion
    }
}
