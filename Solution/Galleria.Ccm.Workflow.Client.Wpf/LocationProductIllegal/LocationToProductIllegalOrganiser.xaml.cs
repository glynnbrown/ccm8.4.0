﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductIllegalMaintenance
{
    /// <summary>
    /// Interaction logic for LocationToProductIllegalOrganiser.xaml
    /// </summary>
    public partial class LocationToProductIllegalOrganiser : ExtendedRibbonWindow
    {
        #region Constants

        const String AddIllegalLinkContextCommandKey = "AddIllegalLinksContextCommand";
        const String RemoveIllegalLinkContextCommandKey = "RemoveIllegalLinksCommand";

        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationToProductIllegalViewModel), typeof(LocationToProductIllegalOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationToProductIllegalViewModel ViewModel
        {
            get { return (LocationToProductIllegalViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationToProductIllegalOrganiser senderControl = (LocationToProductIllegalOrganiser)obj;

            if (e.OldValue != null)
            {
                LocationToProductIllegalViewModel oldModel = (LocationToProductIllegalViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(AddIllegalLinkContextCommandKey);
                senderControl.Resources.Remove(RemoveIllegalLinkContextCommandKey);
            }

            if (e.NewValue != null)
            {
                LocationToProductIllegalViewModel newModel = (LocationToProductIllegalViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(AddIllegalLinkContextCommandKey, senderControl.AddIllegalLinksContextCommand);
                senderControl.Resources.Add(RemoveIllegalLinkContextCommandKey, senderControl.RemoveIllegalLinksCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationToProductIllegalOrganiser()
        {
            //show busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = new LocationToProductIllegalViewModel();
            LocalHelper.SetRibbonBackstageState(this.ViewModel.AttachedControl, true);

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.LocationProductIllegalMaintenance);

            this.Loaded += new RoutedEventHandler(LocationToProductIllegalOrganiser_Loaded);
        }

        void LocationToProductIllegalOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationToProductIllegalOrganiser_Loaded;
            this.ViewModel.ReloadColumns();

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Commands

        #region AddIllegalLinksContextCommand

        private RelayCommand _addIllegalLinksContextCommand;
        /// <summary>
        /// Add illegal links command to be accessed from the right click context menu on the
        /// data grid, checks all selected CheckBox cells
        /// </summary>
        public RelayCommand AddIllegalLinksContextCommand
        {
            get
            {
                if (_addIllegalLinksContextCommand == null)
                {
                    _addIllegalLinksContextCommand = new RelayCommand(p => AddIllegalLinksContext_Executed(), p => AddIllegalLinksContext_CanExecute())
                    {
                        FriendlyName = Message.LocationToProductIllegal_AddIllegalLinksContext,
                    };
                }
                return _addIllegalLinksContextCommand;
            }
        }

        private Boolean AddIllegalLinksContext_CanExecute()
        {
            if (this.IllegalDataGrid.SelectedCells.Count > 0)
            {
                foreach (DataGridCellInfo cell in IllegalDataGrid.SelectedCells)
                {
                    if ((cell.Column.GetCellContent(cell.Item) as CheckBox) != null)
                    {
                        //if one of the selected cells is a checkbox then return true
                        return true;
                    }
                }
            }
            return false;
        }

        private void AddIllegalLinksContext_Executed()
        {
            foreach (DataGridCellInfo cell in IllegalDataGrid.SelectedCells)
            {
                DataGridExtendedCheckBoxColumn checkBoxCol = cell.Column as DataGridExtendedCheckBoxColumn;
                if (checkBoxCol == null) continue;

                String colPath = ExtendedDataGrid.GetBindingProperty(checkBoxCol);

                if (!colPath.StartsWith("LocationIllegalLinks[")) continue;

                Int16 locId;
                if (!Int16.TryParse(colPath.Substring(21, colPath.Length - 22), out locId)) continue;


                LocationProductIllegalRowViewModel rowItem = cell.Item as LocationProductIllegalRowViewModel;
                if (rowItem == null) continue;

                //set to checked
                rowItem.EditDictionaryValue(locId, true);
            }
        }

        #endregion

        #region RemoveIllegalLinksCommand

        private RelayCommand _removeIllegalLinksCommand;
        /// <summary>
        /// Unchecks all selected CheckBox cells
        /// </summary>
        public RelayCommand RemoveIllegalLinksCommand
        {
            get
            {
                if (_removeIllegalLinksCommand == null)
                {
                    _removeIllegalLinksCommand = new RelayCommand(p => RemoveIllegalLinks_Executed(), p => RemoveIllegalLinks_CanExecute())
                    {
                        FriendlyName = Message.LocationToProductIllegal_DataGrid_Uncheck,
                        FriendlyDescription = Message.LocationToProductIllegal_RemoveIllegalLink_Description
                    };
                }
                return _removeIllegalLinksCommand;
            }
        }

        private Boolean RemoveIllegalLinks_CanExecute()
        {
            if (this.IllegalDataGrid.SelectedCells.Count > 0)
            {
                foreach (DataGridCellInfo cell in IllegalDataGrid.SelectedCells)
                {
                    CheckBox cellCheckBox = (cell.Column.GetCellContent(cell.Item) as CheckBox);
                    if (cellCheckBox != null)
                    {
                        if (cellCheckBox.IsChecked == true)
                        {
                            //if one of the selected cells is a checked checkbox cell then return true
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void RemoveIllegalLinks_Executed()
        {
            foreach (DataGridCellInfo cell in IllegalDataGrid.SelectedCells)
            {
                DataGridExtendedCheckBoxColumn checkBoxCol = cell.Column as DataGridExtendedCheckBoxColumn;
                if (checkBoxCol == null) continue;

                String colPath = ExtendedDataGrid.GetBindingProperty(checkBoxCol);

                if (!colPath.StartsWith("LocationIllegalLinks[")) continue;

                Int16 locId;
                if (!Int16.TryParse(colPath.Substring(21, colPath.Length - 22), out locId)) continue;


                LocationProductIllegalRowViewModel rowItem = cell.Item as LocationProductIllegalRowViewModel;
                if (rowItem == null) continue;

                //set to checked
                rowItem.EditDictionaryValue(locId, false);
            }
        }

        #endregion

        #endregion

        #region Methods

        #endregion

        #region Window close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if needs saving
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
