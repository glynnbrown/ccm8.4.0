﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
// CCM-26099 : I.George
//  Changed the FetchByProductGroupId to FetchByMerchandisingGroupId
#endregion
#region Version History: (CCM 8.3.0)
// V8-32243 : N.Haywood
//  Made add links only be able to be selected for the locations that show in the grid
//  Added location selection warning
// V8-32245 : N.Haywood
//  Changed to use generic location selection
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductIllegalMaintenance
{
    public sealed class LocationToProductIllegalViewModel : ViewModelAttachedControlObject<LocationToProductIllegalOrganiser>
    {
        #region Fields

        const String _exceptionCategory = "LocationToProductIllegalMaintenance";

        private readonly ReadOnlyCollection<ProductGroup> _availableProductGroups;
        private ProductGroup _selectedProductGroup;

        private LocationProductIllegalListViewModel _masterLocationProductIllegalListView;
        private BulkObservableCollection<LocationProductIllegalRowViewModel> _availableIllegalLinkRows = new BulkObservableCollection<LocationProductIllegalRowViewModel>();

        private readonly ReadOnlyCollection<LocationInfo> _masterLocationsList;
        private readonly BulkObservableCollection<LocationInfo> _selectedLocations = new BulkObservableCollection<LocationInfo>();


        private readonly BulkObservableCollection<ProductInfo> _availableProductInfos = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _availableProductInfosRO;

        private Boolean _filterIllegalLinks;
        private DataGridColumnCollection _columnSet = new DataGridColumnCollection();

        private Dictionary<Tuple<Int32, Int16>, LocationProductIllegal> _illegalLinksDictionary = new Dictionary<Tuple<int, short>, LocationProductIllegal>();

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableProductGroupsProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(c => c.AvailableProductGroups);
        public static readonly PropertyPath SelectedProductGroupProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.SelectedProductGroup);
        public static readonly PropertyPath AvailableLocationInfosProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedLocationsProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.SelectedLocations);
        public static readonly PropertyPath AvailableProductInfosProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.AvailableProductInfos);
        public static readonly PropertyPath AvailableIllegalLinkRowsProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.AvailableIllegalLinkRows);
        public static readonly PropertyPath FilterIllegalLinksProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.FilterIllegalLinks);
        public static readonly PropertyPath ColumnSetProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.ColumnSet);

        //commands
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(c => c.OpenCommand);
        public static readonly PropertyPath AddIllegalLinkCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(v => v.AddIllegalLinkCommand);
        public static readonly PropertyPath RemoveIllegalLinkCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(v => v.RemoveIllegalLinkCommand);
        public static readonly PropertyPath RemoveAllIllegalLinksCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(v => v.RemoveAllIllegalLinksCommand);
        public static readonly PropertyPath AutoFilterExistingLinksCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(v => v.AutoFilterExistingLinksCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(v => v.CloseCommand);
        public static readonly PropertyPath SetSelectedLocationsCommandProperty = WpfHelper.GetPropertyPath<LocationToProductIllegalViewModel>(v => v.SetSelectedLocationsCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the readonly collection  of product groups available for selection.
        /// </summary>
        public ReadOnlyCollection<ProductGroup> AvailableProductGroups
        {
            get { return _availableProductGroups; }
        }

        /// <summary>
        /// Gets/Sets the selected financial group
        /// </summary>
        public ProductGroup SelectedProductGroup
        {
            get { return _selectedProductGroup; }
            set
            {
                _selectedProductGroup = value;
                OnPropertyChanged(SelectedProductGroupProperty);

                OnSelectedProductGroupChanged(value);
            }
        }

        /// <summary>
        /// Represents all available product infos for the current product selection
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> AvailableProductInfos
        {
            get
            {
                if (_availableProductInfosRO == null)
                {
                    _availableProductInfosRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_availableProductInfos);
                }
                return _availableProductInfosRO;
            }
        }

        /// <summary>
        /// Represents all available location infos for the entity
        /// </summary>
        public ReadOnlyCollection<LocationInfo> AvailableLocations
        {
            get { return _masterLocationsList; }
        }

        /// <summary>
        /// Returns the editable collection of location selected for display.
        /// </summary>
        public BulkObservableCollection<LocationInfo> SelectedLocations
        {
            get { return _selectedLocations; }
        }

        /// <summary>
        /// Represents whether grid should be auto filtered by existing links
        /// </summary>
        public Boolean FilterIllegalLinks
        {
            get { return _filterIllegalLinks; }
            set
            {
                _filterIllegalLinks = value;
                OnPropertyChanged(FilterIllegalLinksProperty);
            }
        }

        /// <summary>
        /// Represents the column set of the man data grid
        /// </summary>
        public DataGridColumnCollection ColumnSet
        {
            get { return _columnSet; }
        }

        /// <summary>
        /// Represents all available illegal link rows for the current current product group
        /// </summary>
        public BulkObservableCollection<LocationProductIllegalRowViewModel> AvailableIllegalLinkRows
        {
            get { return _availableIllegalLinkRows; }
        }


        #endregion

        #region Constructor

        public LocationToProductIllegalViewModel()
        {
            //Update permissions flags
            UpdatePermissionFlags();

            //master locations
            _masterLocationsList = new ReadOnlyCollection<LocationInfo>(LocationInfoList.FetchByEntityId(App.ViewState.EntityId));
            _selectedLocations.BulkCollectionChanged += SelectedLocations_BulkCollectionChanged;


            //financial hierarchy
            ProductHierarchy financialHierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);
            _availableProductGroups = new ReadOnlyCollection<ProductGroup>(financialHierarchy.EnumerateAllGroups().ToList());


            //LocationproductIllegalListViewModel
            _masterLocationProductIllegalListView = new LocationProductIllegalListViewModel();
            _masterLocationProductIllegalListView.ModelChanged += MasterLocationProductIllegalListView_ModelChanged;
            _masterLocationProductIllegalListView.FetchForCurrentEntity();



            //Turn AutoFilterIllegalLinks on as default
            this.FilterIllegalLinks = false;
        }

        #endregion

        #region Commands

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S,
                            DisabledReason = Message.Generic_Save_DisabledReasonInvalidData
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //user must have edit permission 
            if (!_userHasLocationProductIllegalEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            #region Check IllegalLinks
            if (_availableIllegalLinkRows.Count > 0)
            {
                foreach (LocationProductIllegalRowViewModel row in _availableIllegalLinkRows)
                {
                    if (row.InitialLinks.Count != row.CurrentLinks.Count)
                    {
                        //If counts differ then something must have been added/removed
                        return true;
                    }
                    else
                    {
                        //if equal can still have been changed
                        foreach (Int16 id in row.CurrentLinks)
                        {
                            if (!row.InitialLinks.Contains(id))
                            {
                                return true;
                            }
                        }
                    }
                }
            }
            else
            {
                return false;
            }
            #endregion

            return false;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    //Add/Remove items
                    foreach (LocationProductIllegalRowViewModel row in AvailableIllegalLinkRows)
                    {
                        foreach (KeyValuePair<Int16, Boolean> pair in row.LocationIllegalLinks)
                        {
                            LocationProductIllegal link;
                            Boolean linkExists = _illegalLinksDictionary.TryGetValue(new Tuple<Int32, Int16>(row.ProductInfo.Id, pair.Key), out link);
                            if (!linkExists)
                            {
                                if (pair.Value == true)
                                {
                                    _masterLocationProductIllegalListView.Model.Add(LocationProductIllegal.NewLocationProductIllegal(pair.Key, row.ProductInfo.Id, App.ViewState.EntityId));
                                }
                            }
                            else
                            {
                                if (pair.Value == false)
                                {
                                    _masterLocationProductIllegalListView.Model.Remove(link);
                                }
                            }
                        }
                        row.ResetInitialLinks();
                    }

                    //Save Item
                    _masterLocationProductIllegalListView.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exceptionCategory);
                    Exception rootException = ex.GetBaseException();

                    if (this.AttachedControl != null)
                    {
                        //just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            Message.LocationToProductIllegal_ItemsFriendlyName,
                            OperationType.Save,
                            this.AttachedControl);
                    }

                    base.ShowWaitCursor(true);
                }

                base.ShowWaitCursor(false);
            }

            return continueWithSave;

        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then the close command
        /// The organiser should handle this command to peform the close action
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => SaveAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private Boolean SaveAndClose_CanExecute()
        {
            if (!this.SaveCommand.CanExecute())
            {
                this.SaveAndCloseCommand.DisabledReason = this.SaveCommand.DisabledReason;
                return false;
            }


            return true;
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(p => Open_Executed(p), p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        FriendlyDescription = Message.LocationToProductIllegal_Open_Description
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? id)
        {
            //must not be null
            if (id == null)
            {
                _openCommand.DisabledReason = Message.LocationToProductIllegal_NoGroupSelectedDisabledReason;
                return false;
            }

            //user must have permission
            if (!_userHasLocationProductIllegalFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? id)
        {
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                this.SelectedProductGroup = _availableProductGroups.FirstOrDefault(p => p.Id == id.Value);

                base.ShowWaitCursor(false);

                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                //show the select locations window
                if (this.AttachedControl != null)
                {
                    this.ShowLocationsWindow();
                }
            }
        }

        #endregion

        #region AddIllegalLinkCommand

        private RelayCommand _addIllegalLinkCommand;
        /// <summary>
        /// Shows the AddIllegalLink Window and handles the window result
        /// </summary>
        public RelayCommand AddIllegalLinkCommand
        {
            get
            {
                if (_addIllegalLinkCommand == null)
                {
                    _addIllegalLinkCommand = new RelayCommand(p => AddIllegalLink_Executed(), p => AddIllegalLink_CanExecute())
                    {
                        FriendlyName = Message.LocationToProductIllegal_AddIllegalLink,
                        FriendlyDescription = Message.LocationToProductIllegal_AddIllegalLink_Description,
                        Icon = ImageResources.LocationToProductIllegal_AddIllegalLink
                    };
                    this.ViewModelCommands.Add(_addIllegalLinkCommand);
                }
                return _addIllegalLinkCommand;
            }
        }

        private Boolean AddIllegalLink_CanExecute()
        {
            return this.AvailableProductInfos.Count > 0;
        }

        private void AddIllegalLink_Executed()
        {
            //show the dialog
            LocationToProductIllegalNewRelationship win = new LocationToProductIllegalNewRelationship(this.SelectedLocations,
                 _availableProductInfos, this.AvailableIllegalLinkRows);

            App.ShowWindow(win, this.AttachedControl, /*IsModal*/true);
        }

        #endregion

        #region RemoveIllegalLinkCommand

        private RelayCommand _removeIllegalLinkCommand;
        /// <summary>
        /// Unchecks all selected CheckBox cells
        /// </summary>
        public RelayCommand RemoveIllegalLinkCommand
        {
            get
            {
                if (_removeIllegalLinkCommand == null)
                {
                    _removeIllegalLinkCommand = new RelayCommand(p => RemoveIllegalLink_Executed(), p => RemoveIllegalLink_CanExecute())
                    {
                        FriendlyName = Message.LocationToProductIllegal_RemoveIllegalLink,
                        FriendlyDescription = Message.LocationToProductIllegal_RemoveIllegalLink_Description,
                        Icon = ImageResources.LocationToProductIllegal_RemoveIllegalLink
                    };
                    this.ViewModelCommands.Add(_removeIllegalLinkCommand);
                }
                return _removeIllegalLinkCommand;
            }
        }

        private Boolean RemoveIllegalLink_CanExecute()
        {
            if (this.AttachedControl != null)
            {
                return this.AttachedControl.IllegalDataGrid.SelectedCells.Count > 0;
            }
            else
            {
                return false;
            }
        }

        private void RemoveIllegalLink_Executed()
        {
            if (this.AttachedControl != null)
            {
                List<DataGridCellInfo> infoList = this.AttachedControl.IllegalDataGrid.SelectedCells.ToList();
                for (Int16 i = 0; i < infoList.Count; i++)
                {
                    //Uncheck cells
                    if ((infoList[i].Column.GetCellContent(infoList[i].Item) as CheckBox) != null)
                    {
                        (infoList[i].Column.GetCellContent(infoList[i].Item) as CheckBox).IsChecked = false;
                    }
                }
            }
        }

        #endregion

        #region RemoveAllIllegalLinksCommand

        private RelayCommand _removeAllIllegalLinksCommand;
        /// <summary>
        /// Unchecks all CheckBox cells
        /// </summary>
        public RelayCommand RemoveAllIllegalLinksCommand
        {
            get
            {
                if (_removeAllIllegalLinksCommand == null)
                {
                    _removeAllIllegalLinksCommand = new RelayCommand(p => RemoveAllIllegalLinks_Executed(), p => RemoveAllIllegalLinks_CanExecute())
                    {
                        FriendlyName = Message.LocationToProductIllegal_RemoveAllIllegalLinks,
                        FriendlyDescription = Message.LocationToProductIllegal_RemoveAllIllegalLinks_Description,
                        Icon = ImageResources.LocationToProductIllegal_RemoveAllIllegalLinks
                    };
                    this.ViewModelCommands.Add(_removeAllIllegalLinksCommand);
                }
                return _removeAllIllegalLinksCommand;
            }
        }

        private Boolean RemoveAllIllegalLinks_CanExecute()
        {
            return this.AvailableIllegalLinkRows.Count > 0;
        }

        private void RemoveAllIllegalLinks_Executed()
        {
            ShowWaitCursor(true);
            foreach (LocationProductIllegalRowViewModel row in AvailableIllegalLinkRows)
            {
                row.ClearLinks();
            }
            ShowWaitCursor(false);
        }

        #endregion

        #region AutoFilterExistingLinksCommand

        private RelayCommand _autoFilterExistingLinksCommand;
        /// <summary>
        /// Toggles between AutoFilter on and AutoFilter off, reloads columns on click to current filter type
        /// </summary>
        public RelayCommand AutoFilterExistingLinksCommand
        {
            get
            {
                if (_autoFilterExistingLinksCommand == null)
                {
                    _autoFilterExistingLinksCommand = new RelayCommand(p => AutoFilterExistingLinks_Executed())
                    {
                        FriendlyName = Message.LocationToProductIllegal_AutoFilterExistingLinks,
                        FriendlyDescription = Message.LocationToProductIllegal_AutoFilterExistingLinks_Description,
                        Icon = ImageResources.LocationToProductIllegal_AutoFilterExistingLinks
                    };
                    this.ViewModelCommands.Add(_autoFilterExistingLinksCommand);
                }
                return _autoFilterExistingLinksCommand;
            }
        }

        private void AutoFilterExistingLinks_Executed()
        {
            //Filter locations/products
            ReloadColumns();
        }

        #endregion

        #region SetSelectedLocationCommand

        private RelayCommand _setSelectedLocationsCommand;

        public RelayCommand SetSelectedLocationsCommand
        {
            get
            {
                if (_setSelectedLocationsCommand == null)
                {
                    _setSelectedLocationsCommand = new RelayCommand(
                        p => SetSelectedLocations_Executed(),
                        p => SetSelectedLocations_CanExecute())
                    {
                        FriendlyName = Message.LocationToProductIllegal_SetSelectedLocations,
                        FriendlyDescription = Message.LocationToProductIllegal_SetSelectedLocations_Desc,
                        Icon = ImageResources.LocationToProductIllegal_SetSelectedLocations
                    };
                    base.ViewModelCommands.Add(_setSelectedLocationsCommand);
                }
                return _setSelectedLocationsCommand;
            }
        }

        private Boolean SetSelectedLocations_CanExecute()
        {
            if (this.AvailableProductInfos.Count == 0)
            {
                this.SetSelectedLocationsCommand.DisabledReason = Message.LocationToProductIllegal_NoFilterSelected;
                return false;
            }

            return true;
        }

        private void SetSelectedLocations_Executed()
        {
            //Warn the user about change loss
            ModalMessage win = new ModalMessage();
            win.Header = Message.Generic_Warning;
            win.Description = String.Format(Message.LocationProductLegal_SelectLocations_Warning);
            win.MessageIcon = ImageResources.Warning_32;
            win.ButtonCount = 2;
            win.Button1Content = Message.Generic_Ok;
            win.Button2Content = Message.Generic_Cancel;
            win.DefaultButton = ModalMessageButton.Button1;
            win.CancelButton = ModalMessageButton.Button2;
            App.ShowWindow(win, /*isModal*/true);


            if (win.Result == ModalMessageResult.Button1)
            {
                ShowLocationsWindow();
            }
        }

        private void ShowLocationsWindow()
        {
            LocationHierarchyViewModel locationHierarchyViewModel = new LocationHierarchyViewModel();
            locationHierarchyViewModel.FetchForCurrentEntity();
            LocationSelectionWindow win = new LocationSelectionWindow(_masterLocationsList.ToObservableCollection(),
                   this.SelectedLocations.ToObservableCollection(), locationHierarchyViewModel.Model);
            App.ShowWindow(win, this.AttachedControl, true);

            if (win.DialogResult == true)
            {
                this.SelectedLocations.Clear();
                this.SelectedLocations.AddRange(win.ViewModel.TakenLocations.Select(p => p.Location).ToObservableCollection());
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(), p => Close_CanExecute())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (SaveCommand.CanExecute())
                {
                    continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithItemChangeDialog("Location Product Illegal Links", true, SaveCommand);
                }
            }
            return continueExecute;
        }

        /// <summary>
        /// Returns a list of groups that match the given criteria
        /// </summary>
        /// <param name="codeOrNameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<ProductGroup> GetMatchingProductGroups(String codeOrNameCriteria)
        {
            return this._availableProductGroups.Where(
                p => p.Name.ToUpperInvariant().Contains(codeOrNameCriteria) ||
                    p.Code.ToUpperInvariant().Contains(codeOrNameCriteria));
        }

        /// <summary>
        /// Reloads the columns in the datagrid, takes filtering into account
        /// </summary>
        public void ReloadColumns()
        {
            this.ShowWaitCursor(true);

            //Clear existing columns
            _columnSet.Clear();

            #region Product Column
            //Add product column
            DataGridTextColumn productColumn =
                ExtendedDataGrid.CreateReadOnlyTextColumn(
                Message.LocationToProductIllegal_DataGrid_Products,
                "ProductInfo.FullName", HorizontalAlignment.Left);

            productColumn.Width = new DataGridLength(270);

            //Add column to set
            _columnSet.Add(productColumn);

            #endregion

            #region Location Columns
            const Double locColWidth = 80;


            DataTemplate headerTemplate = this.AttachedControl.Resources["LocationToProductIllegal_DTLocationColumnHeader"] as DataTemplate;

            if (this.FilterIllegalLinks)
            {
                if (this.AvailableIllegalLinkRows.Count > 0)
                {
                    //Update the illegal links for the selected locations as 
                    //they are no longer all loaded up front.
                    foreach (var row in this.AvailableIllegalLinkRows)
                    {
                        row.PopulateDictionary(this.SelectedLocations.ToList(), _illegalLinksDictionary);
                    }


                    foreach (LocationInfo location in this.SelectedLocations)
                    {
                        Boolean hasLink = this.AvailableIllegalLinkRows.Max(p => p.LocationIllegalLinks[location.Id]);

                        if (hasLink)
                        {
                            DataGridExtendedCheckBoxColumn col =
                                ExtendedDataGrid.CreateCheckBoxColumn(location.ToString(),
                                String.Format(CultureInfo.InvariantCulture, "LocationIllegalLinks[{0}]", location.Id),
                                /*isReadOnly*/false);

                            col.Width = new DataGridLength(locColWidth);
                            col.HeaderTemplate = headerTemplate;

                            _columnSet.Add(col);
                        }
                    }
                }
            }
            else
            {
                //Update the illegal links for the selected locations as
                //they are no longer all loaded up front.
                foreach (var row in this.AvailableIllegalLinkRows)
                {
                    row.PopulateDictionary(this.SelectedLocations.ToList(), _illegalLinksDictionary);
                }

                //Load column for every location associated with the current entity
                foreach (LocationInfo location in this.SelectedLocations)
                {
                    DataGridExtendedCheckBoxColumn col =
                            ExtendedDataGrid.CreateCheckBoxColumn(location.ToString(),
                            String.Format(CultureInfo.InvariantCulture, "LocationIllegalLinks[{0}]", location.Id),
                        /*isReadOnly*/false);

                    col.Width = new DataGridLength(locColWidth);
                    col.HeaderTemplate = headerTemplate;

                    _columnSet.Add(col);

                }

            }
            #endregion
            this.ShowWaitCursor(false);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected product group
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedProductGroupChanged(ProductGroup newValue)
        {
            _availableProductInfos.Clear();
            if (_availableIllegalLinkRows.Count > 0)
            {
                _availableIllegalLinkRows.Clear();
            }
            if (newValue != null)
            {
                //Prevent change events occuring as it will exponentially increase the load
                //time as each new row will call the datatable to refresh.
                _availableIllegalLinkRows.RaiseListChangedEvents = false;

                //fetch and add products for the selected group          
                //and populate AvailableIllegalLinkRows
                foreach (ProductInfo product in ProductInfoList.FetchByMerchandisingGroupId(newValue.Id).OrderBy(p => p.Gtin))
                {
                    _availableProductInfos.Add(product);

                    //Only use the selected location list when loading to cut down on load times.
                    _availableIllegalLinkRows.Add(new LocationProductIllegalRowViewModel(product,
                      this.SelectedLocations.ToList(), _illegalLinksDictionary));
                }

                //we are now ok to let changed events occur again
                _availableIllegalLinkRows.RaiseListChangedEvents = true;

            }


            if (this.AttachedControl != null)
            {
                //Send of a reset to the illegallink rows to update the datatable
                //use a dispatcher so that the screen will switch immediately to the
                //product screen.
                this.AttachedControl.Dispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        _availableIllegalLinkRows.Reset();
                    }),
                    priority: System.Windows.Threading.DispatcherPriority.ContextIdle);
            }
        }                                             

        /// <summary>
        /// Responds to changes to the selected locations collection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLocations_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //reload the columns collection
            ReloadColumns();
        }

        private void MasterLocationProductIllegalListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<LocationProductIllegalList> e)
        {
            _illegalLinksDictionary.Clear();
            foreach (LocationProductIllegal link in e.NewModel)
            {
                _illegalLinksDictionary.Add(new Tuple<Int32, Int16>(link.ProductId, link.LocationId), link);
            }
        }
        #endregion

        #region Screen Permissions

        private Boolean _userHasLocationProductIllegalFetchPerm;
        private Boolean _userHasLocationProductIllegalEditPerm;

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //LocationProductIllegal permissions
            //nb - we fetch/save the list object so perms are against that.

            //fetch
            _userHasLocationProductIllegalFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(LocationProductIllegalList));

            //edit
            _userHasLocationProductIllegalEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(LocationProductIllegalList));
        }

        /// <summary>
        /// Returns true if the current user has the permissions required to simply open this screen.
        /// </summary>
        /// <returns></returns>
        internal static Boolean UserHasRequiredAccessPerms()
        {
            return

                //LocationProductIllegalList
                Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(LocationProductIllegalList))

                //Location
                && Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(Location));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedLocations.BulkCollectionChanged -= SelectedLocations_BulkCollectionChanged;
                    _masterLocationProductIllegalListView.ModelChanged -= MasterLocationProductIllegalListView_ModelChanged;

                    //Remove collection values so they get disposed
                    _availableProductInfos.Clear();
                    _illegalLinksDictionary.Clear();
                    _availableIllegalLinkRows.Clear();
                    _masterLocationProductIllegalListView = null;


                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}
