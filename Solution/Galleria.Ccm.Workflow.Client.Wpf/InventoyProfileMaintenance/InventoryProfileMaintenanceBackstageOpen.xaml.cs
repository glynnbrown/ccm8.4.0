﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26124 : I.George
//	Initial version
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.InventoryProfileMaintenance
{
    /// <summary>
    /// Interaction logic for InventoryProfileMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class InventoryProfileMaintenanceBackstageOpen : UserControl
    {
        #region Fields
        private ObservableCollection<InventoryProfileInfo> _searchResults = new ObservableCollection<InventoryProfileInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(InventoryProfileMaintenanceViewModel), typeof(InventoryProfileMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public InventoryProfileMaintenanceViewModel ViewModel
        {
            get { return (InventoryProfileMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            InventoryProfileMaintenanceBackstageOpen senderControl = (InventoryProfileMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                InventoryProfileMaintenanceViewModel oldModel = (InventoryProfileMaintenanceViewModel)e.OldValue;
                oldModel.MasterInventoryProfileList.BulkCollectionChanged -= senderControl.ViewModel_MasterInventoryProfileListChanged;
            }

            if (e.NewValue != null)
            {
                InventoryProfileMaintenanceViewModel newModel = (InventoryProfileMaintenanceViewModel)e.NewValue;
                newModel.MasterInventoryProfileList.BulkCollectionChanged += senderControl.ViewModel_MasterInventoryProfileListChanged;
            }
            senderControl.UpdateSearchResults();
        }

        #endregion
        #endregion

        #region Search Text Property

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(InventoryProfileMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// gets/sets the criteria to search the product
        /// </summary>
        public string SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            InventoryProfileMaintenanceBackstageOpen senderControl = (InventoryProfileMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }
        #endregion

        #region SearchResultProerty

        public static readonly DependencyProperty SearchResultProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<InventoryProfileInfo>), typeof(InventoryProfileMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of searched results
        /// </summary>
        public ReadOnlyObservableCollection<InventoryProfileInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<InventoryProfileInfo>)GetValue(SearchResultProperty); }
            private set { SetValue(SearchResultProperty, value); }
        }
        #endregion

        #region Constructor

        public InventoryProfileMaintenanceBackstageOpen()
        {
            InitializeComponent();

            this.SearchResults = new ReadOnlyObservableCollection<InventoryProfileInfo>(_searchResults);
        }

        #endregion

        #region Methods

        private void UpdateSearchResults()
        {
            _searchResults.Clear();
            if(this.ViewModel != null)
            {
                IEnumerable<InventoryProfileInfo> results = this.ViewModel.GettingMatchingInventoryProfile(this.SearchText);

                foreach (InventoryProfileInfo iProInfo in results.OrderBy(l => l.ToString()))
                {
                    _searchResults.Add(iProInfo);
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Search the result when the MasterInventoryProfileList changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_MasterInventoryProfileListChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();

            if (clickedItem != null)
            {
                InventoryProfileInfo storeToOpen = (InventoryProfileInfo)senderControl.SelectedItem;

                if (storeToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                }
            }
        }
        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                ListBox senderControl = (ListBox)sender;
                ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();

                if (clickedItem != null)
                {
                    InventoryProfileInfo storeToOpen = (InventoryProfileInfo)senderControl.SelectedItem;

                    if (storeToOpen != null)
                    {
                        //send the store to be opened
                        this.ViewModel.OpenCommand.Execute(storeToOpen.Id);
                    }
                }
            }
        }
        #endregion      
    }
}
