﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26124 : I.George
//	Initial version
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.InventoryProfileMaintenance
{
    /// <summary>
    /// Interaction logic for InventoryProfileMaintenanceHomeTab.xaml
    /// </summary>
    public partial class InventoryProfileMaintenanceHomeTab : RibbonTabItem
    {
        #region properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(InventoryProfileMaintenanceViewModel), typeof(InventoryProfileMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public InventoryProfileMaintenanceViewModel ViewModel
        {
            get { return (InventoryProfileMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            InventoryProfileMaintenanceHomeTab senderControl = (InventoryProfileMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                InventoryProfileMaintenanceViewModel oldModel = (InventoryProfileMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                InventoryProfileMaintenanceViewModel newModel = (InventoryProfileMaintenanceViewModel)e.NewValue;
              
            }
        }
        #endregion
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public InventoryProfileMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
