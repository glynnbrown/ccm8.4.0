﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25534 : I.George
//	Initial version
// CCM:26698 : I.George
//  Added the ContinueWithClose Method
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;

using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.ComponentModel;


namespace Galleria.Ccm.Workflow.Client.Wpf.InventoryProfileMaintenance
{
    /// <summary>
    /// Interaction logic for InventoryProfileMaintenanceOrganiser.xaml
    /// </summary>
    public partial class InventoryProfileMaintenanceOrganiser : ExtendedRibbonWindow
    {

        #region properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(InventoryProfileMaintenanceViewModel), typeof(InventoryProfileMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public InventoryProfileMaintenanceViewModel ViewModel
        {
            get { return (InventoryProfileMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            InventoryProfileMaintenanceOrganiser senderControl = (InventoryProfileMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                InventoryProfileMaintenanceViewModel oldModel = (InventoryProfileMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                InventoryProfileMaintenanceViewModel newModel = (InventoryProfileMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }
        #endregion

        #endregion

        #region Constructor

        public InventoryProfileMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;
            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.InventoryProfileMaintenance);

            this.ViewModel = new InventoryProfileMaintenanceViewModel();
            this.Loaded += new RoutedEventHandler(InventoryProfileOrganiser_Loaded);
        }

        private void InventoryProfileOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(InventoryProfileOrganiser_Loaded);

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                // this.xBackstageOpen.IsSelected = true;
            }
        }

        #endregion

        #region Window Close
        /// <summary>
        /// when the application wants to be closed, check if it 
        /// needs to be saved
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }
            }
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                    {
                        IDisposable disposableViewModel = this.ViewModel as IDisposable;
                        this.ViewModel = null;

                        if (disposableViewModel != null)

                            if (disposableViewModel != null)
                            {
                                disposableViewModel.Dispose();
                            }
                    }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
