﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26124 : I.George
//	Initial version
#endregion
#region Version History: (CCM 8.1.1)
// CCM-30325 : I.George
//	Added friendly description to SaveAs and Delete commands
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.InventoryProfileMaintenance
{
    /// <summary>
    /// Viewmodel controller for InventoyProfileMaintenanceOrganiser
    /// </summary>
    public sealed class InventoryProfileMaintenanceViewModel : ViewModelAttachedControlObject<InventoryProfileMaintenanceOrganiser>
    {
        #region Fields
        const String _exCategory = "InventoryProfileMaintenance"; //Category names to any exception thrown to gibraltar from here

        private Boolean _userHasInventoryProfileDeletePerm;
        private Boolean _userHasInventoryProfileCreatePerm;
        private Boolean _userHasInventoryProfileFetchPerm;
        private Boolean _userHasInventoryProfileEditPerm;

        private readonly InventoryProfileInfoListViewModel _masterInventoryInfoListView = new InventoryProfileInfoListViewModel();
        private InventoryProfile _selectedInventoryProfile;
        private InventoryRuleForceType _selectedForceType;
        private InventoryRuleWasteHurdleType _selectedWasteHurdleType;

        #endregion

        #region Binding Property Path

        public static readonly PropertyPath SelectedInventoryProfileProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.SelectedInventoryProfile);
        public static readonly PropertyPath MasterInventoryProfileListProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.MasterInventoryProfileList);
        public static readonly PropertyPath ForceValueProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.ForceValue);
        public static readonly PropertyPath SelectedForceTypeProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.SelectedForceType);
        public static readonly PropertyPath SelectedWasteHurdleTypeProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.SelectedWasteHurdleType);
        public static readonly PropertyPath WasteHurdleValueProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.WasteHurdleValue);


        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(v => v.CloseCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<InventoryProfileMaintenanceViewModel>(p => p.OpenCommand);

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public InventoryProfileMaintenanceViewModel()
        {
            //entity infos
            _masterInventoryInfoListView.FetchCurrentEntity();

            UpdatePermissionFlags();

            // load a new item
            this.NewCommand.Execute();

        }

        #endregion

        #region Properties
        /// <summary>
        /// Get/sets the selected metric
        /// </summary>
        public InventoryProfile SelectedInventoryProfile
        {
            get { return _selectedInventoryProfile; }
            private set
            {
                InventoryProfile oldValue = _selectedInventoryProfile;
                _selectedInventoryProfile = value;
                OnPropertyChanged(SelectedInventoryProfileProperty);
                OnInventoryProfileChanged(oldValue, value);
            }
        }
        public ReadOnlyBulkObservableCollection<InventoryProfileInfo> MasterInventoryProfileList
        {
            get
            {
                return _masterInventoryInfoListView.BindingView;
            }
        }

        /// <summary>
        /// HelperProperty.
        /// Gets/Sets the dorce value based method
        /// the selected force type
        /// </summary>
        public Int32 ForceValue
        {
            get
            {
                if (this.SelectedInventoryProfile != null)
                {
                    if (this.SelectedForceType == InventoryRuleForceType.Facings)
                    {
                        return this.SelectedInventoryProfile.MinFacings;
                    }
                    else
                    {
                        return this.SelectedInventoryProfile.MinUnits;
                    }
                }
                else { return 0; }
            }
            set
            {
                if (this.SelectedInventoryProfile != null)
                {
                    if (this.SelectedForceType == InventoryRuleForceType.Facings)
                    {
                        this.SelectedInventoryProfile.MinFacings = value;
                    }
                    else
                    {
                        this.SelectedInventoryProfile.MinUnits = value;
                    }
                }
                OnPropertyChanged(ForceValueProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected Force type
        /// </summary>
        public InventoryRuleForceType SelectedForceType
        {
            get { return _selectedForceType; }
            set
            {
                _selectedForceType = value;
                OnPropertyChanged(SelectedForceTypeProperty);
                OnSelectedForceTypeChanged(value);
            }
        }

        public Single WasteHurdleValue
        {
            get
            {
                if (this.SelectedInventoryProfile != null)
                {
                    if (this.SelectedWasteHurdleType == InventoryRuleWasteHurdleType.CasePacks)
                    {
                        return this.SelectedInventoryProfile.WasteHurdleCasePack;
                    }
                    else
                    {
                        return this.SelectedInventoryProfile.WasteHurdleUnits;
                    }
                }
                else { return 0; }
            }
            set
            {
                if (this.SelectedInventoryProfile != null)
                {
                    if (this.SelectedWasteHurdleType == InventoryRuleWasteHurdleType.CasePacks)
                    {
                        this.SelectedInventoryProfile.WasteHurdleCasePack = value;
                    }
                    else
                    {
                        this.SelectedInventoryProfile.WasteHurdleUnits = value;
                    }
                }
                OnPropertyChanged(WasteHurdleValueProperty);
            }
        }
        /// <summary>
        /// Gets/sets the selected waste hurdle Property
        /// </summary>
        public InventoryRuleWasteHurdleType SelectedWasteHurdleType
        {
            get { return _selectedWasteHurdleType; }
            set
            {
                _selectedWasteHurdleType = value;
                OnPropertyChanged(SelectedWasteHurdleTypeProperty);
                OnSelectedWasteHurdleTypeChanged(value);
            }
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// load a new inventory profil
        /// </summary>

        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(p => New_Executed(),
                            p => New_CanExecute(),
                            attachToCommandManager: false)
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        public Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userHasInventoryProfileCreatePerm)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }
            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //create a new item
                InventoryProfile newInventoryProfile = InventoryProfile.NewInventoryProfile(App.ViewState.EntityId);

                //re-initialize as property was changed.
                newInventoryProfile.MarkGraphAsInitialized();

                this.SelectedInventoryProfile = newInventoryProfile;
            }
        }
        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current store
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.SelectedInventoryProfile == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have edit permission
            if (!_userHasInventoryProfileEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.SelectedInventoryProfile.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            if (this.AttachedControl != null)
            {
                //bindings must be valid
                if (!LocalHelper.AreBindingsValid(this.AttachedControl))
                {
                    this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    return false;
                }
            }
            return true;
        }

        public void Save_Executed()
        {
            if (this.Save_CanExecute())
            {
                SaveCurrentItem();
            }
        }

        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //check the item unique value
            String newName = this.SelectedInventoryProfile.Name;
            Int32 InvProfileId = this.SelectedInventoryProfile.Id;

            IEnumerable<String> takenIpNames = _masterInventoryInfoListView.Model.Where(c => c.Id != InvProfileId).Select(c => c.Name);

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(s => !takenIpNames.Contains(s),  /*force first show*/  this.SelectedInventoryProfile.Name, out newName);
            }
            else
            {
                //force a unique value for unit testing
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedInventoryProfile.Name,
                    takenIpNames);
            }

            //set the name
            if (nameAccepted)
            {
                if (this.SelectedInventoryProfile.Name != newName)
                {
                    this.SelectedInventoryProfile.Name = newName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.SelectedInventoryProfile = this.SelectedInventoryProfile.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //set return flag to false as save is not completed
                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception, check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedInventoryProfile.Name);
                        if (itemReloadRequired)
                        {
                            this.SelectedInventoryProfile = InventoryProfile.FetchById(this.SelectedInventoryProfile.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error occured
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.SelectedInventoryProfile.Name, OperationType.Save);
                    }
                    base.ShowWaitCursor(true);
                }

                //refresh the info list
                _masterInventoryInfoListView.FetchCurrentEntity();
                base.ShowWaitCursor(false);
            }
            return continueWithSave;
        }
        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();
            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }
        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save

            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }
        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        SmallIcon = ImageResources.SaveAs_16,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_userHasInventoryProfileCreatePerm)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.SelectedInventoryProfile == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;

            }

            //must be valid
            if (!this.SelectedInventoryProfile.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            String selectedName;
            Boolean nameIsUnique = true;

            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck = (s) =>
                {
                    IEnumerable<InventoryProfileInfo> matchingNames = _masterInventoryInfoListView.Model.Where(p => p.Name == s);
                    return !(matchingNames.Any());
                };

                nameIsUnique = Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(Message.Generic_SaveAs, Message.Generic_SaveAs_CodeDescription,
                    Message.Generic_CodeIsNotUnique, Entity.NameProperty.FriendlyName, 50,
                    /*forceFirstShow*/true, isUniqueCheck, String.Empty, out selectedName);
            }
            else
            {
                selectedName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedInventoryProfile.Name, _masterInventoryInfoListView.Model.Select(p => p.Name));
            }
            if(nameIsUnique)
            {
                base.ShowWaitCursor(true);

                InventoryProfile itemCopy = this.SelectedInventoryProfile.Copy();
                itemCopy.Name = selectedName;
                itemCopy = itemCopy.Save();

                this.SelectedInventoryProfile = itemCopy;
                _masterInventoryInfoListView.FetchCurrentEntity();

                base.ShowWaitCursor(false);
            }
        }
        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the passed store
        /// Parameter = store to delete
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //must not be null
            if (this.SelectedInventoryProfile == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have delete permission
            if (!_userHasInventoryProfileDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be new
            if (this.SelectedInventoryProfile.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                //confirm the delete with the user
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedInventoryProfile.Name);
            }

            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                InventoryProfile itemToDelete = this.SelectedInventoryProfile;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                Boolean deleteSuccessful = true;
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    deleteSuccessful = false;
                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                if (deleteSuccessful)
                {
                    _masterInventoryInfoListView.FetchCurrentEntity();
                }
                base.ShowWaitCursor(false);
            }
        }


        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed(),
                        p => Close_CanExecute(),
                        attachToCommandManager: false)

                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }
        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;
        /// <summary>
        /// Loads the requested store
        /// parameter = the id of the entity to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? id)
        {
            //user must have get permission
            if (!_userHasInventoryProfileFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (id == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }
            return true;
        }

        private void Open_Executed(Int32? id)
        {
            //check if current item requires save first
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //fetch the requested item
                    this.SelectedInventoryProfile = InventoryProfile.FetchById(id.Value);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                base.ShowWaitCursor(false);
            }
        }
        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //save the inventory profile type list so that any edits are committed
                    _selectedInventoryProfile = null;
                    this.AttachedControl = null;

                    _masterInventoryInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
                
        #region Methods
        /// <summary>
        /// updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //InventoryProfile premissions
            //create
            _userHasInventoryProfileCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(InventoryProfile));

            //fetch
            _userHasInventoryProfileFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(InventoryProfile));

            //edit
            _userHasInventoryProfileEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(InventoryProfile));

            //delete
            _userHasInventoryProfileDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(InventoryProfile));
        }
        /// <summary>
        /// Shows a warning requesst user ok to continue id current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedInventoryProfile , this.SaveCommand);
            }
            return continueExecute;
        }
        /// <summary>
        /// returns a list of inventory profile that match the given criteria
        /// </summary>
        /// <param name="codeOrNameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<InventoryProfileInfo> GettingMatchingInventoryProfile(string codeOrNameCriteria)
        {
            string invariantCriteria = codeOrNameCriteria.ToLowerInvariant();
            return this.MasterInventoryProfileList.Where(p => p.Name.ToLowerInvariant().Contains(invariantCriteria));
        }

        #endregion

        #region EventHandlers
        /// <summary>
        /// Responds to a change of Inventory Profile
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnInventoryProfileChanged(InventoryProfile oldValue, InventoryProfile newValue)
        {
            if (newValue != null)
            {
                this.SelectedForceType = (newValue.MinFacings != 0) ? InventoryRuleForceType.Facings : InventoryRuleForceType.Units;
                this.SelectedWasteHurdleType = (newValue.WasteHurdleCasePack != 0) ? InventoryRuleWasteHurdleType.CasePacks : InventoryRuleWasteHurdleType.Units;

                OnPropertyChanged(ForceValueProperty);
                OnPropertyChanged(WasteHurdleValueProperty);
            }
        }
        /// <summary>
        /// Responds to a change on selected force type changed
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedForceTypeChanged(InventoryRuleForceType newValue)
        {
            InventoryProfile currentRule = this.SelectedInventoryProfile;
            if (currentRule != null)
            {
                //swap the values if we need to
                if (newValue == InventoryRuleForceType.Facings)
                {
                    if (currentRule.MinFacings == 0 && currentRule.MinUnits != 0)
                    {
                        currentRule.MinFacings = currentRule.MinUnits;
                        currentRule.MinUnits = 0;
                    }
                }
                else
                {
                    if (currentRule.MinUnits == 0 && currentRule.MinFacings != 0)
                    {
                        currentRule.MinUnits = currentRule.MinFacings;
                        currentRule.MinFacings = 0;
                    }
                }
                OnPropertyChanged(ForceValueProperty);
            }
        }

        /// <summary>
        /// Responds to a change on selected waste hurdle type
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedWasteHurdleTypeChanged(InventoryRuleWasteHurdleType newValue)
        {
            InventoryProfile currentRule = this.SelectedInventoryProfile;
            if (currentRule != null)
            {
                //swap the values if we need to
                if (newValue == InventoryRuleWasteHurdleType.CasePacks)
                {
                    if (currentRule.WasteHurdleCasePack == 0 && currentRule.WasteHurdleUnits != 0)
                    {
                        currentRule.WasteHurdleCasePack = currentRule.WasteHurdleUnits;
                        currentRule.WasteHurdleUnits = 0;
                    }
                }
                else
                {
                    if (currentRule.WasteHurdleUnits == 0 && currentRule.WasteHurdleCasePack != 0)
                    {
                        currentRule.WasteHurdleUnits = currentRule.WasteHurdleCasePack;
                        currentRule.WasteHurdleCasePack = 0;
                    }
                }
                OnPropertyChanged(ForceValueProperty);
            }
        }

        #endregion

    }
}
