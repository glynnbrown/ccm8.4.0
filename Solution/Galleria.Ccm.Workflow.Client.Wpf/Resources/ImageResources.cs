﻿#region Header Information

// Copyright © Galleria RTS Ltd 2016

#region Version History: (CCM 8.0.0)

// V8-25438: L.Hodson ~ Created.
// CCM-25444 : N.Haywood
//  Added location maintenance
// CCM-25449 : N.Haywood
//  Added location cluster maintenance
// V8-25462 : A.Silva ~ Added Planogram Repository icons.
// CCM-25452 : N.Haywood
//  Added product universe product selection buttons
// V8-25788 : M.Pettit
//  Added Reports icons
// V8-25453 : A.Kuszyk
//  Added Consumer Decision Tree maintenance icons.
//  Added Modelling region (with RemoveRange).
// CCM-25447 : N.Haywood
//  Added location product illegal
// CCM-25454 : J.Pickup
//  Added assortment maintenance imagery
// V8-25871 : A.Kuszyk
//  Added Planogram Hierarchy Maintenance icons and Planogram icon.
// V8-25788 : M.Pettit
//  Added more Reports icons
// V8-25455 : J.Pickup
//  Added Minor Assortment Maintenance 
// V8-25664 : A.Kuszyk
//  Added Favourites icons.
// V8-26217 : A.Kuszyk
//  Added icons for Product Maintenance Images.
// V8-26400 : A.Silva ~ Added temp icon for Validation Template Maintenance.
// V8-26401 : A.Silva ~ Added icons for validation results.
// V8-26560 : A.Probyn ~ Added Sequence Maintenance related.
// V8-26911 : L.Luong
//  Added EventLog icons
// V8-27411 : M.Pettit
//  Added PlanogramRepository_Locked16 icon
// V8-27940 : L.Luong
//  Added Label Maintenance images
// V8-27964 : A.Silva
//      Renamed PlanogramHierarchyMaintenance_RenameGroup_ to PlanogramHierarchyMaintenance_EditGroup_
// V8-27941 : J.Pickup
//      Added Highlights
// V8-24779 : D.Pleasance
//  Added PlanogramImportTemplate
// V8-27912 : M.Pettit
//  Updated most icons to flat modern look
// V8-27709 : L.Luong
//  Added Options Icon
// V8-27966 : A.Silva
//      Added context menu icons for the plan hierarchy selector when in the Plan Repository screen.
//      Added context menu icons for cut, copy and paste.
// V8-27891 : J.Pickup
//      Added 16px renumbering stratgegy and validation template.

#endregion

#region Version History: (CCM 8.0.1)

// V8-28701 : J.Pickup
//      Added Unlock consts.
// V8-28481 : M.Shelley
//      Added plan assignment / publish status icons
// V8-28717 : A.Kuszyk
//      Renamed _productDistribution_48 to _32 and changed path to -32.png.
#endregion

#region Version History: (CCM 8.0.2)
// V8-28986 : M.Shelley
//  Added plan assignment location, category and view by icons
// V8-29010 : D.Pleasance
//  Added PlanogramNameTemplate
// V8-29026 : D.Pleasance
//  Added WorkpackageWizard_SelectStoreSpecific images
// V8-29255 : L.Ineson
// Removed old sequence maintenance keys.
#endregion

#region Version History: (CCM 8.0.3)
// V8-29479 : N.Haywood
//  Added project template field up/down buttons
// V8-29590 : A.Probyn
//	Added new workpackage event log icons.
// V8-29646 : M.Shelley
//  Added "Select Distinct" icon for reporting toolbar
#endregion

#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//  Added Auto Match Icon
#endregion

#region Version History: (CCM 8.1.1)
// V8-27486 : M.Pettit
//  Updated PlanogramRepository_OpenPlanogram icon to use standard folder open as per Space Planning
// V8-30394 : I.George
//   Added SmallIcon for new and OpenFile Command
#endregion

#region Version History: (CCM 8.3.0)
// V8-31831 : A.Probyn
//  ADded ContentLookup_PlanogramNameTemplate_16
// V8-32560 : A.Silva
//  Removed AddAll and RemoveAll icons for the store specific planogram step screen.
// V8-32790 : M.Pettit
//  Added Open_32, Open_48 properties
// V8-32810 : M.Pettit
//  Added Printtemplate 16 and 32 icons
// CCM-13756 : M.Pettit
//   Updated Automatch to flat style Autoassign icon
// CCM-18316 : A.Heathcote
//  Added ProductBuddy image resource for ProductBuddyAdvancedAddManualProductBuddyWindow
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Reflection;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Galleria.Ccm.Workflow.Client.Wpf
{
    /// <summary>
    ///     Holds static references to all image sources required for the solution.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores",
        Justification = "NamingConvention ok here.")]
    public static class ImageResources
    {
        #region Helper Methods

        private static readonly Dictionary<String, ImageSource> _sourceDict = new Dictionary<String, ImageSource>();

        private static ImageSource GetSource(String key)
        {
            ImageSource src;
            if (!_sourceDict.TryGetValue(key, out src))
            {
                src = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _iconFolderPath, key));
                _sourceDict[key] = src;
            }
            return src;
        }

        /// <summary>
        ///     Helper method to create a frozen bitmap image source
        ///     This prevents issue where static sources can memory leak
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        private static BitmapImage CreateAsFrozen(String imagePath)
        {
            var src = new BitmapImage(new Uri(imagePath));
            src.Freeze();
            return src;
        }

        #endregion

        #region Constant Image Paths

        private static readonly String _graphicsFolderPath = String.Format(
            CultureInfo.InvariantCulture, "pack://application:,,,/{0};component/Resources/Graphics/{{0}}",
            Assembly.GetExecutingAssembly().GetName().Name);

        private static readonly String _iconFolderPath = String.Format(CultureInfo.InvariantCulture, _graphicsFolderPath,
            "{0}");

        //NB - these private fields are now just Strings so that image sources are lazy loaded in as required.

        //A
        private const String _accept_32 = "Accept-32-Office.png";
        private const String _add_16 = "Add-16-Database.png";
        private const String _add_32 = "Add-32-Database.png";
        private const String _addAfter_16 = "AddAfter-16.png";
        private const String _addBefore_16 = "AddBefore-16.png";
        private const String _alignCentre_16 = "AlignCentre-16.png";
        private const String _alignLeft_16 = "AlignLeft-16.png";
        private const String _alignRight_16 = "AlignRight-16.png";
        private const String _apply_16 = "Apply-16.png";
        private const String _apply_32 = "Apply-32.png";
        private const String _applyAndClose_16 = "ApplyAndClose-16.png";
        private const String _applyAndNew_16 = "ApplyAndNew-16.png";
        private const String _assortment_32 = "Assortment-32.png";
        private const String _assortment_16 = "Assortment-16.png";
        private const String _assortmentProductRule_32 = "AssortmentProductRule-32.png";
        private const String _assortmentRegionalProduct_32 = "RegionalProductSetup-32.png";
        private const String _assortmentLocalProduct_32 = "AssortmentLocalProducts-32.png";
        private const String _assortmentInventoryRules_32 = "AssortmentInventoryRules-32.png";
        private const String _assortmentInventoryRules_16 = "AssortmentInventoryRules-16.png";
        private const String _assortmentFile_32 = "AssortmentFile-32.png";
        private const String _approve_32 = "Approve-32-File.png";
        private const String _addAll_24 = "AddAll-24.png";
        private const String _autoAssign_32 = "AutoAssign-32.png";
        private const String _autoUnassign_32 = "AutoUnassign-32.png";
        private const String _autoMatch_32 = "AutoMatch-32.png";

        //B
        private const String _back_16 = "Back-16-Toolbar.png";
        private const String _back_20 = "Back-20-Office.png";
        private const String _blankIcon_AwaitingDesign_32 = "BlankIcon-AwaitingDesign-32.png";
        private const String _back_24 = "Back-24-Toolbar.png";
        private const String _blockMerchandiser_32 = "BlockMerchandiser-32.png";
        private const String _blockMerchandiser_16 = "BlockMerchandiser-16.png";

        //C
        private const String _cancel_20 = "Cancel-20-Office.png";
        private const String _collapse_10 = "Collapse-10.png";
        private const String _ccmAboutScreen = "CCM-AboutScreen.png";
        private const String _ccmIcon = "CCMIcon.ico";
        private const String _ccmSplashScreen = "CCM-Splashscreen.png";
        private const String _cdt_16 = "CDT-16.png";
        private const String _cdt_32 = "CDT-32.png";
        private const String _chart_16 = "Chart-16.png";
        private const String _checkbox_32_checked = "Checkbox-32-Checked.png";
        private const String _checkbox_32_unchecked = "Checkbox-32-Unchecked.png";
        private const String _clearDocument_32 = "ClearDocument-32-Database.png";
        private const String _clipboard_32 = "Clipboard-32.png";
        private const String _clipboardAll_32 = "Clipboard_All-32.png";
        private const String _clipboardSelection_32 = "Clipboard_Selection-32.png";
        private const String _close_16 = "ClosedFolder-16-File.png";
        private const String _compass_20 = "Compass-20.png";
        private const String _configuration_32 = "Configuration-32-Toolbar.png";
        private const String _cut_16 = "Cut-16-Design.png";
        private const String _cut_32 = "Cut-32-Design.png";
        private const String _cargo_20 = "Cargo-20-Warehouse.png";
        private const String _calendar_20 = "Calendar-20-Time.png";
        private const String _copy_16 = "Copy-16-Toolbar.png";
        private const String _copy_32 = "Copy-32-Toolbar.png";
        private const String _contentLookup_32 = "ContentLookup-32.png";
        private const String _connect_32 = "Connect-32.png";
        private const String _cursor_32 = "Cursor-32-Design.png";
        const String _colorLayers_32 = "ColorLayers-32-Design.png";

        //D
        private const String _danger_24 = "Danger-24-Office.png";
        private const String _danger_32 = "Danger-32-Office.png";
        private const String _database_24 = "Database-24-Database.png";
        private const String _database_32 = "Database-32-Database.png";
        private const String _danger_16 = "Danger-16-Office.png";
        private const String _delete_16 = "Delete-16-Database.png";
        private const String _delete_32 = "Delete-32-Database.png";
        private const String _clearDatabaseData_32 = "DeleteData-32.Png";
        private const String _deleteDivider_32 = "DeleteDivider-32.png";
        private const String _declineFile_32 = "Decline-32-File.png";
        private const String _deleteFile_32 = "Delete-32-File.png";
        private const String _downArrow_16 = "DownArrow-16.png";
        private const String _distribution_32 = "Distribution-32.png";
        private const String _dateAndTime_32 = "DateAndTime-32-Database.png";
        private const String _dateAndTime_16 = "DateAndTime-16-Database.png";

        //E
        private const String _edit_16_Computer = "Edit-16-Computer.png";
        private const String _entity_32 = "Entity-32.png";
        private const String _entityFilter_32 = "EntityFilter-32.png";
        private const String _entityFilterOff_32 = "EntityFilterOff-32.png";
        private const String _errorFilter_32 = "ErrorFilter-32.png";
        private const String _eventLog_32 = "EventLog-32.png";
        private const String _excelFile_32 = "ExcelFile-32.png";
        private const String _exportGridAllToExcel_32 = "ExportAllGridToExcel-32.png";
        private const String _exportGridSelectionToExcel_32 = "ExportSelectedGridToExcel-32.png";
        private const String _exportText_32 = "ExportFileToExcel-32.png";
        private const String _exportDBToExcel_32 = "ExportDBToExcel-32.png";
        private const String _exportGridToExcel_32 = "ExportGridToExcel-32.png";
        private const String _exit_16 = "Exit-16-Toolbar.png";
        private const String _editFile_32 = "EditFile-32.png";

        //F
        private const String _favouritesAdd_32 = "FavouritesAdd-32.png";
        private const String _favouritesAdd_16 = "FavouritesAdd-16-Toolbar.png";
        private const String _favouritesDelete_32 = "FavouritesDelete-32.png";
        private const String _favouritesDelete_16 = "FavouritesDelete-16-Toolbar.png";
        private const String _familyRules_32 = "FamilyRules-32.png";
        private const String _fieldSort_32 = "ReportFieldSort-32.png";
        private const String _filter_32 = "Filter-32.png";
        private const String _filter_16 = "Filter-16.png";
        private const String _folder_32 = "Folder-32.png";
        private const String _folderClosed_14 = "FolderClosed-14.png";
        private const String _folderOpen_14 = "FolderOpen-14.png";
        private const String _folderOpen_32 = "Folder_Open-32.png";
        private const String _folderOpen_48 = "FolderOpen-48.png";
        private const String _fontBold_16 = "FontBold-16.png";
        private const String _fontItalic_16 = "FontItalic-16.png";
        private const String _fontUnderline_16 = "FontUnderline-16.png";
        private const String _formulaField_32 = "FormulaField_32.png";
        private const String _formulaFieldNew_32 = "FormulaFieldAdd_32.png";
        private const String _formulaFieldDelete_32 = "FormulaFieldDelete_32.png";
        private const String _formulaFieldEdit_32 = "FormulaFieldEdit_32.png";
        private const String _forward_20 = "Forward-20-Office.png";
        private const String _folderTree_32 = "FolderTree-32-Toolbar.png";
        private const String _forward_16 = "Forward-16-Toolbar.png";
        private const String _forward_24 = "Forward-24-Toolbar.png";
        private const String _fitToHeight_32 = "FitToHeight-32.png";
        private const String _first_16 = "First-16-Toolbar.png";

        //G
        private const String _gettingStarted_32 = "GettingStarted-32.png";
        private const String _gridSettings_32 = "GridSettings-32.png";
        private const String _greyStar_16 = "GreyStar-16.png";
        private const String _gfsSetup_32 = "GfsSetup-32.png";

        //H
        private const String _hierarchyAddLevel_16 = "HierarchyAddLevel-16.png";
        private const String _hierarchyAddNode_16 = "HierarchyAddNode-16.png";
        private const String _hierarchyRemoveLevel_16 = "HierarchyRemoveLevel-16.png";
        private const String _hierarchyRemoveNode_16 = "HierarchyRemoveNode-16.png";
        private const String _hierarchyAddLevel_32 = "HierarchyAddLevel-32.png";
        private const String _hierarchyAddNode_32 = "HierarchyAddNode-32.png";
        private const String _hierarchyEditLevel_16 = "HierarchyEditLevel-16.png";
        private const String _hierarchyEditLevel_32 = "HierarchyEditLevel-32.png";
        private const String _hierarchyEditNode_32 = "HierarchyEditNode-32.png";
        private const String _hierarchyRemoveLevel_32 = "HierarchyRemoveLevel-32.png";
        private const String _hierarchyRemoveNode_32 = "HierarchyRemoveNode-32.png";
        private const String _hierarchyExpandAll_32 = "HierarchyExpandAll-32.png";
        private const String _hierarchyCollapseAll_32 = "HierarchyCollapseAll-32.png";
        private const String _highlights_32 = "Highlights-32.png";
        private const String _horizontalDivider_32 = "HorizontalDivider-32.png";

        //I
        private const String _importFromExcel_32 = "ImportFromExcel-32.png";
        private const String _importFromFile_32 = "ImportFromFile-32.png";
        private const String _info_32 = "Info-32-Toolbar.png";
        private const String _inventoryObjective_32 = "InventoryObjective-32.png";
        private const String _inventoryObjective_16 = "InventoryObjective-16.png";

        //J
        private const String _justifyText_20 = "JustifyText-20-Office.png";

        //L
        private const String _label_32 = "ProductLabel-32.png";
        private const String _line_16 = "Line-16.png";
        private const String _leftArrow_16 = "LeftArrow-16.png";
        private const String _locationBuddy_32 = "LocationBuddy-32.png";
        private const String _locationBuddyAdd_32 = "LocationBuddyAdd-32.png";
        private const String _locationClusters_32 = "LocationClusters-32.png";
        private const String _locationClusters_16 = "LocationClusters-16.png";
        private const String _locationHierarchy_32 = "LocationHierarchy-32.png";
        private const String _locationSpace_32 = "LocationSpace-32.png";
        private const String _locationIllegal_32 = "LocationIllegal-32.png";
        private const String _locationLegal_32 = "LocationToProductTick-32.png";
        private const String _locationToProduct_32 = "LocationToProduct-32.png";
        private const String _locationSpace_Grid_32 = "Grid-32.png";
        private const String _locationSpace_Group_32 = "LocationSpaceGroupBy-32.png";
        private const String _locationSpace_Slider_32 = "Slider-32.png";
        private const String _locationSpace_View_32 = "View-32.png";
        private const String _locked_16 = "Locked-16.png";
        private const String _lockedClients_32 = "LockedClients-32.png";
        private const String _last_16 = "Last-16-Toolbar.png";

        //M
        private const String _mappingTemplate_32 = "MappingTemplate-32.png";
        private const String _meeting_32 = "Meeting-32-People.png";
        private const String _merchHierarchy_32 = "MerchandisingHierarchy-32.png";
        private const String _minorReview_32 = "MinorReview-32.png";
        private const String _minorReview_16 = "MinorReview-16.png";
        private const String _minorReviewDistribution_32 = "MinorReviewDistribution-32.png";
        private const String _minorReviewDeList_32 = "MinorReviewDeList-32.png";
        private const String _minorReviewList_32 = "MinorReviewList-32.png";
        private const String _minorReviewReplace_32 = "MinorReviewReplace-32.png";
        private const String _move_16_design = "Move-16-Design.png";

        //N
        private const String _newFile_16 = "NewFile-16-File.png";
        private const String _newFile_32 = "NewFile-32-File.png";
        private const String _newFile_48 = "NewFile-48-File.png";

        //O
        private const String _ok_20 = "OK-20-Office.png";
        private const String _okTransparent_20 = "OK-Transparent-20-Office.png";
        private const String _open_16 = "Open-16-Toolbar.png";
        private const String _options_16 = "Options-16-Toolbar.png";
        private const String _options_32 = "Options-32-Toolbar.png";

        //P
        private const String _paperMarginNarrow_32 = "PaperMarginNarrow-32.png";
        private const String _paperMarginNormal_32 = "PaperMarginNormal-32.png";
        private const String _paperMarginWide_32 = "PaperMarginWide-32.png";
        private const String _pasteDocument_16 = "PasteDocument-16-Toolbar.png";
        private const String _pageOrientation_32 = "PageOrientation-32.png";
        private const String _pageOrientation_Landscape_32 = "PageOrientation_Landscape.png";
        private const String _pageOrientation_Portrait_32 = "PageOrientation_Portrait.png";
        private const String _pageSize_32 = "PageSize-32.png";
        private const String _petrol_20 = "Petrol-20.png";
        private const String _picture_16 = "Picture-16.png";
        private const String _planAssign_Header_16 = "PlanAssignment_24.png";
        private const String _planAssign_Search_16 = "Search_PlanAssign-16.png";
        private const String _planAssign_PublishSuccessful_16 = "SquareTick-16.png";
        private const String _planAssign_PublishFailed_16 = "SquareCross-16.png";
        private const String _planAssign_PublishException_16 = "SquareWarning-16.png";
        private const String _planAssign_Location_16 = "Location-16.png";
        private const String _planAssign_ProductGroup_16 = "ProductGroup-16.png";
        private const String _planAssign_ViewBy_32 = "ViewBy-32.png";
        private const String _planAssign_SelectorRemoveAll_24 = "PlanAssignRemoveAll-24.png";

        private const String _planDelete_32 = "PlanDelete-32.png";
        private const String _planGridView_32 = "PlanGridView-32.png";
        private const String _planogram_32 = "Planogram_32.png";
        private const String _planogramNoChanges_32 = "PlanogramNoChanges_32.png";
        private const String _planogramDelete_16 = "PlanogramDelete_16.png";
        private const String _planogramHierarchy_32 = "PlanogramHierarchy-32.png";
        private const String _planogramExportTemplate_32 = "PlanogramExportTemplate_32.png";
        private const String _planogramImportTemplate_32 = "PlanogramImportTemplate_32.png";
        private const String _planogramNameTemplate_32 = "PlanogramNameTemplate-32.png";
        private const String _planogramNameTemplate_16 = "PlanogramNameTemplate-16.png";
        private const String _planogramPublishToGFS_32 = "PlanogramPublish-32.png";
        private const String _planOpen_32 = "PlanOpen-32.png";
        private const String _planogramSelect_32 = "PlanogramSelect_32.png";
        private const String _planView_32 = "PlanView-32.png";
        private const String _printTemplate_16 = "PrintTemplate-16.png";
        private const String _printTemplate_32 = "PrintTemplate-32.png";
        private const String _printPreview_32 = "PrintPreview-32.png";
        private const String _printPreviewGrid_32 = "PrintPreviewGrid-32.png";
        private const String _printPreviewPdf_32 = "PrintPreviewPdf-32.png";
        private const String _productList_32 = "ProductList-32.png";
        private const String _position_16 = "Position-16-Design.png";
        private const String _products_32 = "Products-32-Warehouse.png";
        private const String _productBuddy_32 = "ProductBuddy-32.png";
        private const String _productBuddyAdd_32 = "ProductBuddyAdd-32.png";
        private const String _productUniverse_16 = "ProductUniverse-16.png";
        private const String _productUniverse_32 = "ProductUniverse-32.png";
        private const String _productDeList_32 = "ProductDelist-32.png";
        private const String _productDistribution_32 = "ProductDistribution-32.png";
        private const String _products_16 = "Products-16-Warehouse.png";
        private const String _productSequence_32 = "ProductSequence-32.png";
        private const String _productSequence_16 = "ProductSequence-16.png";
        private const String _performanceMetric_32 = "PerformanceMetric-32.png";
        private const String _performanceMetric_16 = "PerformanceMetric-16.png";
        private const String _performanceMetricSelection_32 = "PerformanceMetricsSelection-32.png";
        private const String _planOpen32 = "PlanOpen-32.png";

        //Q
        private const String _question_24 = "Question-24-Office.png";

        //R
        private const String _rightArrow_16 = "RightArrow-16.png";
        private const String _rangeRulesCopy_32 = "RangeRulesCopy-32.png";
        private const String _renumberingStrategyMaintenance_32 = "Sorting 9-1-Toolbar-32.png";
        private const String _renumberingStrategyMaintenance_16 = "Sorting 9-1-Toolbar-16.png";
        private const String _reportBuilderReport_24 = "ReportPage-24.png";
        private const String _reportBuilderReport_32 = "ReportPage-32.png";
        private const String _reportImportFromFile_48 = "ReportImportFromFile-48.png";
        private const String _reportInfo_32 = "ReportInfo-32.png";
        private const String _reportAttachTemplate_32 = "PaperClip-32.png";
        private const String _reportBuilderReport_48 = "ReportBuilderReport-48.png";
        private const String _report_AddLine_32 = "ReportAddLine-32.png";
        private const String _report_AddTextBox_32 = "ReportAddTextBox-32.png";
        private const String _reportExportToFile_16 = "ReportExport-16.png";
        private const String _reportExportToFile_32 = "ReportExport-32.png";
        private const String _reportGroup_32 = "ReportGroup-32.png";
        private const String _reportGroup_Add_32 = "ReportGroupAdd-32.png";
        private const String _reportGroup_Remove_32 = "ReportGroupRemove-32.png";
        private const String _report_itemProperties_16 = "Properties-16.png";
        private const String _report_itemProperties_32 = "Properties-32.png";
        private const String _report_Open_32 = "ReportOpen-32.png";
        private const String _reportParameters_32 = "ReportParameters-32.png";
        private const String _reportPreview_32 = "ReportPreview-32.png";
        private const String _report_RemoveItem_32 = "RemoveItem-32.png";
        private const String _report_SelectComponent_32 = "ReportSelectComponent-32.png";
        private const String _reportSubTotals_32 = "ReportSubTotals-32.png";
        private const String _retailShop_32 = "RetailShop-32-City.png";
        private const String _repositoryPlanogram_24 = "RepositoryPlanogram-24.png";
        private const String _rightSidebar_32 = "RightSideBar-32.png";
        private const String _retailShop_20 = "RetailShop-20-City.png";
        private const String _removeAll_24 = "RemoveAll-24.png";
        private const String _refresh_16 = "Refresh-16-Toolbar.png";
        private const String _refreshGrid_32 = "RefreshGrid-32.png";
        private const String _reportSelectDistinct_32 = "SelectDistinct-32.png";
        private const String _reportExcelExport_32 = "ReportExcelExport-32.png";

        //S
        private const String _save_16 = "Save-16-Toolbar.png";
        private const String _save_32 = "Save-32-Toolbar.png";
        private const String _saveAndClose_16 = "SaveAndClose-16.png";
        private const String _saveAndClose_32 = "SaveAndClose-32.png";
        private const String _saveAndNew_16 = "SaveAndNew-16.png";
        private const String _saveAs_16 = "SaveAs-16-Toolbar.png";
        private const String _saveAs_32 = "SaveAs-32-Toolbar.png";
        private const String _search_16 = "Search-16-Toolbar.png";
        private const String _showGrid_16 = "ShowGrid-16.png";
        private const String _showTotals_32 = "ShowTotals-32.png";
        private const String _snapToGrid_16 = "SnapToGrid-16.png";
        private const String _squareCross_16 = "SquareCross-16.png";
        private const String _squareCross_20 = "SquareCross-20.png";
        private const String _squareInformation_16 = "SquareInformation-16.png";
        private const String _squareTick_16 = "SquareTick-16.png";
        private const String _summariseTime_32 = "SummariseTime-32.png";
        private const String _summariseTime00Sec_32 = "SummariseTime00-32.png";
        private const String _summariseTime03Sec_32 = "SummariseTime03-32.png";
        private const String _summariseTime05Sec_32 = "SummariseTime05-32.png";
        private const String _summariseTime10Sec_32 = "SummariseTime10-32.png";
        private const String _summariseTime60Sec_32 = "SummariseTime60-32.png";
        private const String _support_32 = "Support-32-Office.png";
        private const String _star_16 = "Star-16-Blog.png ";

        //T
        private const String _textBox_16 = "TextBox-16.png";
        private const String _turnOff_16 = "TurnOff-16-Toolbar.png";
        private const String _turnOff_32 = "TurnOff-32-Toolbar.png";
        private const String _tv_20 = "TV-20-Business.png";
        private const String _time_20 = "Time-20-Toolbar.png";

        //U
        private const String _upArrow_16 = "UpArrow-16.png";
        private const String _user_20 = "User-20-Toolbar.png";
        private const String _user_32 = "User-32-Toolbar.png";
        private const String _units_20 = "Units-20-Toolbar.png";
        private const String _Unlock_32 = "Unlocked-32.png";

        //V
        private const String _validationTemplateMaintenance_32 = "ValidationTemplate-32.png";
        private const String _validationTemplateMaintenance_16 = "ValidationTemplate-16.png";
        private const String _viewFile_32 = "ViewFile-32.png";
        private const String _viewDatabaseData_32 = "ViewData-32.png";
        private const String _verticalDivider_32 = "VerticalDivider-32.png";

        //W
        private const String _warning_16 = "Warning-16-Toolbar.png";
        private const String _warning_20 = "Warning-20-Toolbar.png";
        private const String _warning_32 = "Warning-32-Toolbar.png";
        private const String _workflow_32 = "Workflow-32.png";
        private const String _workpackage_24 = "Workpackage-24.png";
        private const String _workpackageDelete_32 = "WorkPackageDelete-32.png";
        private const String _workpackageGridView_32 = "WorkPackageGridView-32.png";
        private const String _workpackageDebug_16 = "WorkpackageDebug-16.png";
        private const String _workpackageEdit_32 = "WorkPackageEdit-32.png";
        private const String _workpackageNew_32 = "WorkPackageNew-32.png";
        private const String _workpackageNewFromExisting_32 = "WorkpackageNewFromExisting-32.png";
        private const String _workpackageNewFromFile_32 = "WorkpackageNewFromFile-32.png";
        private const String _workpackageOpen_32 = "WorkPackageOpen-32.png";
        private const String _workpackageRun_16 = "WorkPackageRun-16.png";
        private const String _workpackageRun_32 = "WorkPackageRun-32.png";
        private const String _world_20 = "World-20-Toolbar.png";

        //Z
        private const String _zoomIn_16 = "ZoomIn-16-Toolbar.png";
        private const String _zoomOut_16 = "ZoomOut-16-Toolbar.png";
        private const String _zoomToFit_16 = "ZoomToFit-16.png";

        #endregion

        public static ImageSource TODO
        {
            get { return GetSource(_support_32); }
        }

        #region General

        #region Icon, Splash and About page backgrounds

        public static ImageSource CCMSplashscreen
        {
            get { return GetSource(_ccmSplashScreen); }
        }

        public static ImageSource CCMAboutScreen
        {
            get { return GetSource(_ccmAboutScreen); }
        }

        public static ImageSource CCMIcon
        {
            get { return GetSource(_ccmIcon); }
        }

        #endregion

        public static ImageSource Add_16
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource Add_32
        {
            get { return GetSource(_add_32); }
        }

        public static ImageSource Apply_32
        {
            get { return GetSource(_apply_32); }
        }

        public static ImageSource Apply_16
        {
            get { return GetSource(_apply_16); }
        }

        public static ImageSource ApplyAndClose_16
        {
            get { return GetSource(_applyAndClose_16); }
        }

        public static ImageSource ApplyAndNew_16
        {
            get { return GetSource(_applyAndNew_16); }
        }

        public static ImageSource Backstage_Close
        {
            get { return GetSource(_turnOff_16); }
        }
      
        public static ImageSource Copy_16 { get { return GetSource(_copy_16); } }

        public static ImageSource Cut_16 { get { return GetSource(_cut_16); } }

        public static ImageSource Delete_16
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource Delete_32
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource New_16
        {
            get { return GetSource(_newFile_16); }
        }

        public static ImageSource New_32
        {
            get { return GetSource(_newFile_32); }
        }

        public static ImageSource Open_16
        {
            get { return GetSource(_open_16); }
        }

        public static ImageSource Open_32
        {
            get { return GetSource(_folderOpen_32); }
        }

        public static ImageSource Open_48
        {
            get { return GetSource(_folderOpen_48); }
        }
        
        public static ImageSource Paste_16 { get { return GetSource(_pasteDocument_16); } }

        public static ImageSource Save_16
        {
            get { return GetSource(_save_16); }
        }

        public static ImageSource Save_32
        {
            get { return GetSource(_save_32); }
        }

        public static ImageSource SaveAndClose_16
        {
            get { return GetSource(_saveAndClose_16); }
        }

        public static ImageSource SaveAndNew_16
        {
            get { return GetSource(_saveAndNew_16); }
        }

        public static ImageSource SaveAs_16
        {
            get { return GetSource(_saveAs_16); }
        }

        public static ImageSource SaveAs_32
        {
            get { return GetSource(_saveAs_32); }
        }

        public static ImageSource TreeViewItem_ClosedFolder
        {
            get { return GetSource(_folderClosed_14); }
        }

        public static ImageSource TreeViewItem_OpenFolder
        {
            get { return GetSource(_folderOpen_14); }
        }

        public static ImageSource Warning_16
        {
            get { return GetSource(_warning_16); }
        }

        public static ImageSource Warning_32
        {
            get { return GetSource(_warning_32); }
        }

        public static ImageSource ErrorCritical
        {
            get { return GetSource(_danger_24); }
        }

        public static ImageSource DialogError
        {
            get { return GetSource(_danger_32); }
        }

        public static ImageSource DialogInformation
        {
            get { return GetSource(_info_32); }
        }

        public static ImageSource DialogQuestion
        {
            get { return TODO; }
        }

        public static ImageSource DialogWarning
        {
            get { return GetSource(_warning_32); }
        }

        public static ImageSource Tick
        {
            get { return GetSource(_checkbox_32_checked); }
        }

        public static ImageSource UnTick
        {
            get { return GetSource(_checkbox_32_unchecked); }
        }

        public static ImageSource AddSelectedItems
        {
            get { return GetSource(_forward_24); }
        }

        public static ImageSource AddAllItems
        {
            get { return GetSource(_addAll_24); }
        }

        public static ImageSource RemoveSelectedItems
        {
            get { return GetSource(_back_24); }
        }

        public static ImageSource RemoveAllItems
        {
            get { return GetSource(_removeAll_24); }
        }

        #endregion

        #region Ribbon

        public static ImageSource Screen_PlanRepository
        {
            get { return GetSource(_repositoryPlanogram_24); }
        }

        public static ImageSource Screen_WorkPackages
        {
            get { return GetSource(_workpackage_24); }
        }

        public static ImageSource Screen_Reports
        {
            get { return GetSource(_reportBuilderReport_24); }
        }

        public static ImageSource Ribbon_Options
        {
            get { return GetSource(_options_16); }
        }

        public static ImageSource Ribbon_Exit_16
        {
            get { return GetSource(_exit_16); }
        }

        #endregion

        #region Backstage Data Management

        public static ImageSource DataManagement_ServerImage
        {
            get { return GetSource(_world_20); }
        }

        public static ImageSource DataManagement_OKState
        {
            get { return GetSource(_ok_20); }
        }

        public static ImageSource DataManagement_ErrorState
        {
            get { return GetSource(_squareCross_20); }
        }

        public static ImageSource DataManagement_MixedState
        {
            get { return GetSource(_question_24); }
        }

        public static ImageSource DataManagement_WarningState
        {
            get { return GetSource(_warning_20); }
        }

        public static ImageSource DataManagement_NotSetState
        {
            get { return GetSource(_okTransparent_20); }
        }

        public static ImageSource DataManagement_DataImage
        {
            get { return GetSource(_justifyText_20); }
        }

        public static ImageSource DataManagement_ExcelFile
        {
            get { return GetSource(_excelFile_32); }
        }


        public static ImageSource DataManagement_ImportData
        {
            get { return GetSource(_importFromExcel_32); }
        }

        public static ImageSource DataManagement_ViewData
        {
            get { return GetSource(_viewDatabaseData_32); }
        }

        public static ImageSource DataManagement_SaveImportTemplate
        {
            get { return GetSource(_mappingTemplate_32); }
        }

        public static ImageSource DataManagement_ExportData
        {
            get { return GetSource(_exportDBToExcel_32); }
        }

        public static ImageSource DataManagementt_DeleteData
        {
            get { return GetSource(_clearDatabaseData_32); }
        }

        public static ImageSource DataManagement_ColumnDefinition_ClearMapping
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource DataManagement_ColumnDefinition_Duplicate
        {
            get { return GetSource(_warning_20); }
        }

        public static ImageSource DataManagement_ViewEditData_Edit
        {
            get { return GetSource(_edit_16_Computer); }
        }

        #endregion

        #region Backstage Help
        public static ImageSource BackstageHelp_GettingStarted
        {
            get { return GetSource(_gettingStarted_32); }
        }
        
        public static ImageSource BackstageHelp_Help_32
        {
            get { return GetSource(_support_32); }
        }

        //public static ImageSource BackstageHelp_Email_48 { get { return GetSource(_email_48); } }
        public static ImageSource BackstageHelp_ContactUs_32
        {
            get { return GetSource(_meeting_32); }
        }

        //public static ImageSource BackstageHelp_GalleriaAddress_48 { get { return GetSource(_office_48); } }
        //public static ImageSource BackstageHelp_Web_48 { get { return GetSource(_web_48); } }
        //public static ImageSource BackstageHelp_Telephone_48 { get { return GetSource(_telephone_48); } }

        #endregion

        #region Maintenance Screens

        #region Assortment Maintenance

        public static ImageSource AssortmentMaintenance
        {
            get { return GetSource(_assortment_32); }
        }

        public static ImageSource AssortmentSetup_AddSelected
        {
            get { return GetSource(_forward_24); }
        }

        public static ImageSource AssortmentSetup_RemoveAll
        {
            get { return GetSource(_removeAll_24); }
        }

        public static ImageSource AssortmentSetup_RemoveSelected
        {
            get { return GetSource(_back_24); }
        }

        public static ImageSource AssortmentSetup_AddAll
        {
            get { return GetSource(_addAll_24); }
        }

        public static ImageSource Dialog_Information
        {
            get { return GetSource(_info_32); }
        }

        public static ImageSource Dialog_Warning
        {
            get { return GetSource(_warning_32); }
        }

        public static ImageSource AssortmentSetup_AddFromCDT
        {
            get { return GetSource(_cdt_16); }
        }

        public static ImageSource AssortmentSetup_AddFromProductUniverse
        {
            get { return GetSource(_productUniverse_16); }
        }

        public static ImageSource AssortmentSetup_AddLocations
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource AssortmentSetup_AddProducts
        {
            get { return GetSource(_add_32); }
        }

        public static ImageSource AssortmentSetup_RemoveSelectedroducts
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource AssortmentSetup_FamilyRules
        {
            get { return GetSource(_familyRules_32); }
        }

        public static ImageSource AssortmentSetup_ProductRules
        {
            get { return GetSource(_assortmentProductRule_32); }
        }

        public static ImageSource AssortmentSetup_RegionalProducts
        {
            get { return GetSource(_assortmentRegionalProduct_32); }
        }

        public static ImageSource AssortmentSetup_LocalProducts
        {
            get { return GetSource(_assortmentLocalProduct_32); }
        }

        public static ImageSource AssortmentSetup_InventoryRules
        {
            get { return GetSource(_assortmentInventoryRules_32); }
        }

        public static ImageSource AssortmentSetup_LocationBuddys_AddNew
        {
            get { return GetSource(_locationBuddyAdd_32); }
        }

        public static ImageSource AssortmentSetup_LocationBuddys_Review
        {
            get { return GetSource(_locationBuddy_32); }
        }

        public static ImageSource AssortmentSetup_ProductBuddys
        {
            get { return GetSource(_productBuddy_32); }
        }

        public static ImageSource AssortmentSetup_ProductBuddys_AddNew
        {
            get { return GetSource(_productBuddyAdd_32); }
        }

        public static ImageSource AssortmentSetup_ProductBuddys_Wizard
        {
            get { return GetSource(_productBuddyAdd_32); }
        }

        public static ImageSource AssortmentSetup_ProductBuddys_Advanced
        {
            get { return GetSource(_productBuddyAdd_32); }
        }        

        public static ImageSource AssortmentSetup_RemoveSelectedProducts
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource AssortmentSetup_AttachFile
        {
            get { return GetSource(_assortmentFile_32); }
        }

        public static ImageSource Collapse_10_OutlookStyle
        {
            get { return GetSource(_collapse_10); }
        }

        public static ImageSource AssortmentSetup_ShowLocations
        {
            get { return GetSource(_rightSidebar_32); }
        }

        public static ImageSource AssortmentSetup_RemoveLocations
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource AssortmentProductBuddyWizard_RemoveSourceProduct
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource AssortmentProductBuddyWizard_AddSourceProduct
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource AssortmentProductBuddyAdvancedAdd_RowStatusInvalid
        {
            get { return GetSource(_warning_16); }
        }

        public static ImageSource AssortmentProductBuddyAdvancedAdd_RowStatusNew
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource AssortmentSetupCopyRules_SelectAll
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource AssortmentSetupCopyRules_ClearAll
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource AssortmentSetup_CopyRules
        {
            get { return GetSource(_rangeRulesCopy_32); }        
        }

        #endregion

        #region Blocking Maintenance

        public static ImageSource BlockingMaintenance
        {
            get { return GetSource(_blockMerchandiser_32); }
        }

        public static ImageSource BlockingMaintenance_VerticalTool32
        {
            get { return GetSource(_verticalDivider_32); }
        }

        public static ImageSource BlockingMaintenance_HorizontalTool32
        {
            get { return GetSource(_horizontalDivider_32); }
        }

        public static ImageSource BlockingMaintenance_ConnectTool32
        {
            get { return GetSource(_connect_32); }
        }

        public static ImageSource BlockingMaintenance_PointerTool32
        {
            get { return GetSource(_cursor_32); }
        }

        public static ImageSource BlockingMaintenance_DeleteDividerTool32
        {
            get { return GetSource(_deleteDivider_32); }
        }

        public static ImageSource BlockingMaintenance_ZoomToFitHeight32
        {
            get { return GetSource(_fitToHeight_32); }
        }

        public static ImageSource BlockingMaintenance_ZoomToFitHeight16
        {
            get { return GetSource(_fitToHeight_32); }
        }

        public static ImageSource BlockingMaintenance_ZoomToFit16
        {
            get { return GetSource(_zoomToFit_16); }
        }

        public static ImageSource BlockingMaintenance_ZoomIn16
        {
            get { return GetSource(_zoomIn_16); }
        }

        public static ImageSource BlockingMaintenance_ZoomOut16
        {
            get { return GetSource(_zoomOut_16); }
        }

        #endregion

        #region ConsumerDecisionTreeMaintenance

        public static ImageSource ConsumerDecisionTreeMaintenance
        {
            get { return GetSource(_cdt_32); }
        }

        public static ImageSource CdtMaintenance_AddLevel
        {
            get { return GetSource(_hierarchyAddLevel_32); }
        }

        public static ImageSource CdtMaintenance_AddLevel_16
        {
            get { return GetSource(_hierarchyAddLevel_16); }
        }

        public static ImageSource CdtMaintenance_RemoveLevel
        {
            get { return GetSource(_hierarchyRemoveLevel_32); }
        }

        public static ImageSource CdtMaintenance_AddNode
        {
            get { return GetSource(_hierarchyAddNode_32); }
        }

        public static ImageSource CdtMaintenance_RemoveNode
        {
            get { return GetSource(_hierarchyRemoveNode_32); }
        }

        public static ImageSource CdtMaintenance_RemoveNode_16
        {
            get { return GetSource(_hierarchyRemoveNode_16); }
        }

        public static ImageSource CdtMaintenance_SplitNode_32
        {
            get { return GetSource(_cut_32); }
        }

        public static ImageSource CdtMaintenance_AssignProducts
        {
            get { return GetSource(_products_32); }
        }

        public static ImageSource CdtMaintenance_ShowUnitEditWindow
        {
            get { return GetSource(_hierarchyEditNode_32); }
        }

        public static ImageSource CdtMaintenanceEditNode_ProductsTab
        {
            get { return GetSource(_products_32); }
        }

        public static ImageSource CdtMaintenance_AutoCreateNodes
        {
            get { return GetSource(_configuration_32); }
        }

        public static ImageSource CdtWizard_TypeSelection
        {
            get { return GetSource(_configuration_32); }
        }

        public static ImageSource ConsumerDecisionTree_AddFromProductUniverse
        {
            get { return GetSource(_productUniverse_16); }
        }

        public static ImageSource ConsumerDecisionTree_AddFromCDT
        {
            get { return GetSource(_cdt_16); }
        }

        public static ImageSource Modelling_RemoveRange
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource LocationModel_AddNode
        {
            get { return GetSource(_hierarchyAddNode_32); }
        }

        #endregion

        #region Content Lookup Maintenance

        public static ImageSource ContentLookup
        {
            get { return GetSource(_contentLookup_32); }
        }

        public static ImageSource ContentLookup_ProductUniverse
        {
            get { return GetSource(_productUniverse_32); }
        }

        public static ImageSource ContentLookup_Assortment
        {
            get { return GetSource(_assortment_32); }
        }

        public static ImageSource ContentLookup_MetricProfile
        {
            get { return GetSource(_performanceMetric_32); }
        }

        public static ImageSource ContentLookup_InventoryProfile
        {
            get { return GetSource(_inventoryObjective_32); }
        }

        public static ImageSource ContentLookup_AutoAssign32
        {
            get { return GetSource(_autoAssign_32); }
        }

        public static ImageSource ContentLookup_AutoUnassign32
        {
            get { return GetSource(_autoUnassign_32); }
        }

        public static ImageSource ContentLookup_ProductUniverse16
        {
            get { return GetSource(_productUniverse_16); }
        }

        public static ImageSource ContentLookup_Assortment16
        {
            get { return GetSource(_assortment_16); }
        }

        public static ImageSource ContentLookup_MetricProfile16
        {
            get { return GetSource(_performanceMetric_16); }
        }

        public static ImageSource ContentLookup_InventoryProfile16
        {
            get { return GetSource(_inventoryObjective_16); }
        }

        public static ImageSource ContentLookup_ProductSequence16
        {
            get { return GetSource(_productSequence_16); }
        }

        public static ImageSource ContentLookup_ConsumerDecisionTree16
        {
            get { return GetSource(_cdt_16); }
        }

        public static ImageSource ContentLookup_AssortmentMinorRevision16
        {
            get { return GetSource(_minorReview_16); }
        }

        public static ImageSource ContentLookup_PerformanceSelectionMaintenance_16
        {
            get { return GetSource(_dateAndTime_16); }
        }

        public static ImageSource ContentLookup_Blocking_16
        {
            get { return GetSource(_blockMerchandiser_16); }
        }

        public static ImageSource ContentLookup_Clusters_16
        {
            get { return GetSource(_locationClusters_16); }
        }

        public static ImageSource ContentLookup_RenumberingStrategy_16
        {
            get { return GetSource(_renumberingStrategyMaintenance_16); }
        }

        public static ImageSource ContentLookup_ValidationTemplate_16
        {
            get { return GetSource(_validationTemplateMaintenance_16); }
        }

        public static ImageSource ContentLookup_PlanogramNameTemplate_16
        {
            get { return GetSource(_planogramNameTemplate_16); }
        }

        public static ImageSource ContentLookup_PrintTemplate_16
        {
            get { return GetSource(_printTemplate_16); }
        }

        #endregion

        #region Highlights

        public static ImageSource View_ManageHighlights_32 { get { return GetSource(_highlights_32); } }

        #endregion

        #region InventoryProfile Maintenance

        public static ImageSource InventoryProfileMaintenance_32
        {
            get { return GetSource(_inventoryObjective_32); }
        }

        #endregion

        #region Label Maintenance

        public static ImageSource LabelMaintenance
        {
            get { return GetSource(_label_32); }
        }

        #endregion

        #region Location Cluster Maintenance

        public static ImageSource LocationClusterMaintenance
        {
            get { return GetSource(_locationClusters_32); }
        }

        public static ImageSource LocationClusterMaintenance_NewScheme
        {
            get { return GetSource(_newFile_48); }
        }

        public static ImageSource LocationClusterMaintenance_NewCluster
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource LocationClusterMaintenance_DeleteCluster
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource LocationClusterMaintenance_RemoveAllClusters
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource LocationClusterMaintenance_AutoCreate
        {
            get { return GetSource(_folderTree_32); }
        }

        public static ImageSource LocationClusterMaintenance_NewImportFromExcelCommand
        {
            get { return GetSource(_excelFile_32); }
        }

        public static ImageSource LocationClusterMaintenance_Remove
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource LocationClusterMaintenance_Add
        {
            get { return GetSource(_add_32); }
        }

        public static ImageSource LocationSelectionDeselect
        {
            get { return GetSource(_delete_32); }
        }

        #endregion

        #region Location Maintenance

        public static ImageSource LocationMaintenance
        {
            get { return GetSource(_retailShop_32); }
        }

        public static ImageSource LocationMaintenance_AddLocationType
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource LocationMaintenance_RemoveLocationType
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource LocationMaintenance_SectionContact
        {
            get { return GetSource(_retailShop_20); }
        }

        public static ImageSource LocationMaintenance_SectionLocation
        {
            get { return GetSource(_compass_20); }
        }

        public static ImageSource LocationMaintenance_SectionContacts
        {
            get { return GetSource(_user_20); }
        }

        public static ImageSource LocationMaintenance_SectionOpeningTimes
        {
            get { return GetSource(_time_20); }
        }

        public static ImageSource LocationMaintenance_SectionDates
        {
            get { return GetSource(_calendar_20); }
        }

        public static ImageSource LocationMaintenance_SectionDistribution
        {
            get { return GetSource(_cargo_20); }
        }

        public static ImageSource LocationMaintenance_SectionFacilities
        {
            get { return GetSource(_petrol_20); }
        }

        public static ImageSource LocationMaintenance_SectionGeographic
        {
            get { return GetSource(_world_20); }
        }

        public static ImageSource LocationMaintenance_SectionMarketing
        {
            get { return GetSource(_tv_20); }
        }

        public static ImageSource LocationMaintenance_SectionSize
        {
            get { return GetSource(_units_20); }
        }

        public static ImageSource LocationMaintenance_SectionStore
        {
            get { return GetSource(_retailShop_20); }
        }

        #endregion

        #region Location Hierarchy Maintenance

        public static ImageSource LocationHierarchyMaintenance
        {
            get { return GetSource(_locationHierarchy_32); }
        }

        public static ImageSource LocationHierarchy_AddLevel
        {
            get { return GetSource(_hierarchyAddLevel_32); }
        }

        public static ImageSource LocationHierarchy_RemoveLevel
        {
            get { return GetSource(_hierarchyRemoveLevel_32); }
        }

        public static ImageSource LocationHierarchy_AddUnit
        {
            get { return GetSource(_hierarchyAddNode_32); }
        }

        public static ImageSource LocationHierarchy_RemoveUnit
        {
            get { return GetSource(_hierarchyRemoveNode_32); }
        }

        public static ImageSource LocationHierarchy_ShowUnitEditWindow
        {
            get { return GetSource(_hierarchyEditNode_32); }
        }

        public static ImageSource LocationHierarchy_LocationAssignment
        {
            get { return GetSource(_retailShop_32); }
        }

        #endregion

        #region Location Product Attribute Maintenance

        public static ImageSource LocationProductAttributeMaintenance
        {
            get { return GetSource(_locationToProduct_32); }
        }

        #endregion

        #region Location Product Illegal Maintenance

        public static ImageSource LocationProductIllegalMaintenance
        {
            get { return GetSource(_locationIllegal_32); }
        }

        public static ImageSource LocationToProductIllegal_AddIllegalLink
        {
            get { return GetSource(_add_32); }
        }

        public static ImageSource LocationToProductIllegal_RemoveIllegalLink
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource LocationToProductIllegal_RemoveAllIllegalLinks
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource LocationToProductIllegal_AutoFilterExistingLinks
        {
            get { return GetSource(_filter_32); }
        }

        public static ImageSource LocationToProductIllegal_SetSelectedLocations
        {
            get { return GetSource(_retailShop_32); }
        }

        #endregion

        #region Location Product Legal Maintenance

        public static ImageSource LocationProductLegalMaintenance
        {
            get { return GetSource(_locationLegal_32); }
        }

        public static ImageSource LocationProductLegal_AddLegalLink
        {
            get { return GetSource(_add_32); }
        }

        public static ImageSource LocationProductLegal_RemoveLegalLink
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource LocationProductLegal_RemoveAllLegalLinks
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource LocationProductLegal_AutoFilterExistingLinks
        {
            get { return GetSource(_filter_32); }
        }

        public static ImageSource LocationProductLegal_SetSelectedLocations
        {
            get { return GetSource(_retailShop_32); }
        }

        #endregion

        #region Location Space Maintenance

        public static ImageSource LocationSpaceMaintenance_Copy_32
        {
            get { return GetSource(_copy_32); }
        }


        public static ImageSource LocationSpaceMaintenance
        {
            get { return GetSource(_locationSpace_32); }
        }

        public static ImageSource LocationSpace_Add
        {
            get { return GetSource(_add_32); }
        }

        public static ImageSource LocationSpace_Remove
        {
            get { return GetSource(_delete_32); }
        }

        public static ImageSource LocationSpace_AddElement
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource LocationSpace_CopyElement
        {
            get { return GetSource(_copy_16); }
        }

        public static ImageSource LocationSpace_RemoveElement
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource LocationSpace_MoveBayLeft_16
        {
            get { return GetSource(_leftArrow_16); }
        }
        public static ImageSource LocationSpace_MoveBayRight_16
        {
            get { return GetSource(_rightArrow_16); }
        }


        public static ImageSource LocationSpace_AddFixtureBay
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource LocationSpace_CopyFixtureBay
        {
            get { return GetSource(_copy_16); }
        }

        public static ImageSource LocationSpace_RemoveFixtureBay
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource LocationSpace_AddFixtureAfter_16
        {
            get{ return GetSource(_addAfter_16); }
        }
        public static ImageSource LocationSpace_AddFixtureBefore_16
        {
            get { return GetSource(_addBefore_16); }
        }


        public static ImageSource LocationSpace_IncreaseBaysSelectedElement
        {
            get { return GetSource(_forward_20); }
        }

        public static ImageSource LocationSpace_DecreaseBaysSelectedElement
        {
            get { return GetSource(_back_20); }
        }

        public static ImageSource LocationSpace_Grid
        {
            get { return GetSource(_locationSpace_Grid_32); }
        }

        public static ImageSource LocationSpace_Group
        {
            get { return GetSource(_locationSpace_Group_32); }
        }

        public static ImageSource LocationSpace_Main
        {
            get { return GetSource(_locationSpace_32); }
        }

        public static ImageSource LocationSpace_Slider
        {
            get { return GetSource(_locationSpace_Slider_32); }
        }

        public static ImageSource LocationSpace_View
        {
            get { return GetSource(_locationSpace_View_32); }
        }

        public static ImageSource LocationSpace_Edit
        {
            get { return GetSource(_editFile_32); }
        }

        #endregion

        #region Merchandising Hierarchy Maintenance

        public static ImageSource MerchHierarchyMaintenance
        {
            get { return GetSource(_merchHierarchy_32); }
        }

        public static ImageSource MerchHierarchy_AddLevel
        {
            get { return GetSource(_hierarchyAddLevel_32); }
        }

        public static ImageSource MerchHierarchy_RemoveLevel
        {
            get { return GetSource(_hierarchyRemoveLevel_32); }
        }

        public static ImageSource MerchHierarchy_AddUnit
        {
            get { return GetSource(_hierarchyAddNode_32); }
        }

        public static ImageSource MerchHierarchy_RemoveUnit
        {
            get { return GetSource(_hierarchyRemoveNode_32); }
        }

        public static ImageSource MerchHierarchy_ShowUnitEditWindow
        {
            get { return GetSource(_hierarchyEditNode_32); }
        }

        public static ImageSource MerchandisingHierarchyEditUnit_PreviousUnit
        {
            get { return GetSource(_back_20); }
        }

        public static ImageSource MerchandisingHierarchyEditUnit_NextUnit
        {
            get { return GetSource(_forward_20); }
        }

        public static ImageSource MerchandisingHierarchyEditUnit_MoveUpLevel
        {
            get { return GetSource(_upArrow_16); }
        }

        public static ImageSource MerchandisingHierarchyEditUnit_MoveDownLevel
        {
            get { return GetSource(_downArrow_16); }
        }

        public static ImageSource HierarchyMaintenance_ExpandAll
        {
            get { return GetSource(_hierarchyExpandAll_32); }
        }

        public static ImageSource HierarchyMaintenance_CollapseAll
        {
            get { return GetSource(_hierarchyCollapseAll_32); }
        }

        #endregion

        #region Minor Assortment Maintenance

        public static ImageSource MinorAssortmentMaintenance
        {
            get { return GetSource(_minorReview_32); }
        }

        public static ImageSource MinorReview_Star
        {
            get { return GetSource(_star_16); }
        }

        public static ImageSource MinorReview_GreyStar
        {
            get { return GetSource(_greyStar_16); }
        }

        public static ImageSource Actions_DecreaseActionPriority
        {
            get { return GetSource(_downArrow_16); }
        }

        public static ImageSource Actions_IncreaseActionPriority
        {
            get { return GetSource(_upArrow_16); }
        }

        public static ImageSource Actions_RemoveAction
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource Actions_RemoveActionLocation
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource BackstageDataManagement_ExportData
        {
            get { return GetSource(_exportText_32); }
        }

        public static ImageSource MinorReview_AmendDistribution
        {
            get { return GetSource(_minorReviewDistribution_32); }
        }

        public static ImageSource MinorReview_DeListProduct
        {
            get { return GetSource(_minorReviewDeList_32); }
        }

        public static ImageSource MinorReview_ListProduct
        {
            get { return GetSource(_minorReviewList_32); }
        }

        public static ImageSource MinorReview_ReplaceProduct
        {
            get { return GetSource(_minorReviewReplace_32); }
        }

        public static ImageSource MinorRevision_AddAllProducts
        {
            get { return GetSource(_addAll_24); }
        }

        public static ImageSource MinorRevision_AddSelectedProducts
        {
            get { return GetSource(_forward_24); }
        }

        public static ImageSource MinorRevision_RemoveAllProducts
        {
            get { return GetSource(_removeAll_24); }
        }

        public static ImageSource MinorRevision_RemoveSelectedProducts
        {
            get { return GetSource(_back_24); }
        }

        public static ImageSource MasterDataManagement_LocationSetup
        {
            get { return GetSource(_retailShop_32); }
        }

        public static ImageSource AmendDistribution_SelectAvailableProducts
        {
            get { return GetSource(_blankIcon_AwaitingDesign_32); }
        }

        public static ImageSource MinorRevision_RemoveSelectedLocations
        {
            get { return GetSource(_back_24); }
        }

        public static ImageSource MinorRevision_RemoveAllLocations
        {
            get { return GetSource(_removeAll_24); }
        }

        public static ImageSource MinorRevision_AddSelectedLocations
        {
            get { return GetSource(_forward_24); }
        }

        public static ImageSource MinorRevision_AddAllLocations
        {
            get { return GetSource(_addAll_24); }
        }

        public static ImageSource ListProduct_ProductSelection
        {
            get { return GetSource(_productList_32); }
        }

        public static ImageSource ListProduct_TargetSelection
        {
            get { return GetSource(_retailShop_32); }
        }

        public static ImageSource ListProduct_CDTSelection
        {
            get { return GetSource(_cdt_32); }
        }

        public static ImageSource ReplaceProduct_ProductSelection
        {
            get { return GetSource(_minorReviewReplace_32); }
        }

        public static ImageSource AmendDistribution_UpdateDistribution
        {
            get { return GetSource(_distribution_32); }
        }

        public static ImageSource DeListProduct_ProductSelection
        {
            get { return GetSource(_productDeList_32); }
        }

        public static ImageSource AmendDistribution_ProductSelection
        {
            get { return GetSource(_productDistribution_32); }
        }

        public static ImageSource MinorRevision_ProductSelection
        {
            get { return GetSource(_products_16); }
        }

        public static ImageSource Dialog_Error_16
        {
            get { return GetSource(_danger_16); }
        }

        #endregion

        #region Metric Maintenance

        public static ImageSource MetricMaintenance_32
        {
            get { return GetSource(_performanceMetric_32); }
        }

        public static ImageSource MetricMaintenance_AddMetric
        {
            get { return GetSource(_add_16); }
        }

        public static ImageSource MetricMaintenance_EditMetric
        {
            get { return GetSource(_edit_16_Computer); }
        }

        public static ImageSource MetricMaintenance_RemoveMetric
        {
            get { return GetSource(_delete_16); }
        }

        #endregion

        #region Metric Profile Maintenance

        public static ImageSource MetricProfileMaintenance_32
        {
            get { return GetSource(_performanceMetricSelection_32); }
        }

        #endregion

        #region Planogram Hierarchy Maintenance

        public static ImageSource PlanogramHierarchyMaintenance
        {
            get { return GetSource(_planogramHierarchy_32); }
        }

        public static ImageSource PlanogramHierarchyMaintenance_NewGroup_32
        {
            get { return GetSource(_hierarchyAddLevel_32); }
        }

        public static ImageSource PlanogramHierarchyMaintenance_NewGroup_16
        {
            get { return GetSource(_hierarchyAddLevel_16); }
        }

        public static ImageSource PlanogramHierarchyMaintenance_EditGroup_32
        {
            get { return GetSource(_hierarchyEditLevel_32); }
        }

        public static ImageSource PlanogramHierarchyMaintenance_EditGroup_16
        {
            get { return GetSource(_hierarchyEditLevel_16); }
        }

        public static ImageSource PlanogramHierarchyMaintenance_DeleteGroup_32
        {
            get { return GetSource(_hierarchyRemoveLevel_32); }
        }

        public static ImageSource PlanogramHierarchyMaintenance_DeleteGroup_16
        {
            get { return GetSource(_hierarchyRemoveLevel_16); }
        }

        public static ImageSource PlanogramHierarchyMaintenance_ExpandAll_32
        {
            get { return GetSource(_hierarchyExpandAll_32); }
        }

        public static ImageSource PlanogramHierarchyMaintenance_CollapseAll_32
        {
            get { return GetSource(_hierarchyCollapseAll_32); }
        }

        #endregion

        #region PlanogramImportTemplate

        public static ImageSource PlanogramImportTemplateMaintenance { get { return GetSource(_planogramImportTemplate_32); } }
        public static ImageSource PlanogramImportTemplate_IsDoubleMapped { get { return GetSource(_warning_20); } }

        #endregion

        #region PlanogramExportTemplate

        public static ImageSource PlanogramExportTemplateMaintenance { get { return GetSource(_planogramExportTemplate_32); } }

        #endregion

        #region PlanogramNameTemplate

        public static ImageSource PlanogramNameTemplateMaintenance { get { return GetSource(_planogramNameTemplate_32); } }
        public static ImageSource PlanogramNameTemplateMaintenance_ClearFormula { get { return GetSource(_delete_16); } }

        #endregion


        #region PrintTemplate

        public static ImageSource PrintTemplateMaintenance_32 { get { return GetSource(_printTemplate_32); } }
        public static ImageSource PrintTemplateMaintenance_ClearFormula { get { return GetSource(_delete_16); } }

        #endregion



        #region Product Maintenance

        public static ImageSource ProductMaintenance
        {
            get { return GetSource(_products_32); }
        }

        public static ImageSource ProductMaintenance_ProductImage_Edit
        {
            get { return GetSource(_edit_16_Computer); }
        }

        public static ImageSource ProductMaintenance_ProductImage_Clear
        {
            get { return GetSource(_delete_16); }
        }

        #endregion

        #region Product Universe Maintenance

        public static ImageSource ProductUniverseMaintenance
        {
            get { return GetSource(_productUniverse_32); }
        }

        public static ImageSource ProductUniverseMaintenance_AddSelectedProducts
        {
            get { return GetSource(_forward_24); }
        }

        public static ImageSource ProductUniverseMaintenance_AddAllProducts
        {
            get { return GetSource(_addAll_24); }
        }

        public static ImageSource ProductUniverseMaintenance_RemoveSelectedProducts
        {
            get { return GetSource(_back_24); }
        }

        public static ImageSource ProductUniverseMaintenance_RemoveAllProducts
        {
            get { return GetSource(_removeAll_24); }
        }

        #endregion

        #region PerformanceSelectionMaintenance

        public static ImageSource PerformanceSelectionMaintenance_32
        {
            get { return GetSource(_dateAndTime_32); }
        }

        #endregion

        #region Validation Template Maintenance

        public static ImageSource ValidationTemplateMaintenance_32
        {
            get { return GetSource(_validationTemplateMaintenance_32); }
        }

        public static ImageSource ValidationResult_OK_32
        {
            get { return GetSource(_accept_32); }
        }

        public static ImageSource ValidationResult_Warning_32
        {
            get { return GetSource(_warning_32); }
        }

        public static ImageSource ValidationResult_Fail_32
        {
            get { return GetSource(_danger_32); }
        }

        #endregion

        #region Renumbering Strategy Maintenance

        /// <summary>
        ///     Icon for the Renumbering Strategy Maintenance.
        /// </summary>
        public static ImageSource RenumberingStrategyMaintenance_32
        {
            get { return GetSource(_renumberingStrategyMaintenance_32); }
        }

        #endregion

        #region Workflow Maintenance

        public static ImageSource WorkflowMaintenance
        {
            get { return GetSource(_workflow_32); }
        }

        public static ImageSource WorkflowMaintenance_AddTask_32
        {
            get { return GetSource(_hierarchyAddLevel_32); }
        }

        public static ImageSource WorkflowMaintenance_AddTask_16
        {
            get { return GetSource(_hierarchyAddLevel_16); }
        }

        public static ImageSource WorkflowMaintenance_RemoveTask_32
        {
            get { return GetSource(_hierarchyRemoveLevel_32); }
        }

        public static ImageSource WorkflowMaintenance_RemoveTask_16
        {
            get { return GetSource(_hierarchyRemoveLevel_16); }
        }

        public static ImageSource WorkflowMaintenance_MoveTaskUp_16
        {
            get { return GetSource(_upArrow_16); }
        }

        public static ImageSource WorkflowMaintenance_MoveTaskDown_16
        {
            get { return GetSource(_downArrow_16); }
        }

        #endregion

        #endregion

        #region System Admin Screens

        #region Entity maintenance

        public static ImageSource SysAdmin_EntityMaintenance
        {
            get { return GetSource(_entity_32); }
        }

        public static ImageSource EntitySetup_TabHeader
        {
            get { return GetSource(_entity_32); }
        }

        #endregion

        #region User Maintenance

        public static ImageSource UserMaintenance_32
        {
            get { return GetSource(_user_32); }
        }

        #endregion

        #region GFS Setup

        public static ImageSource GFSSetup_32
        {
            get { return GetSource(_gfsSetup_32); }
        }

        #endregion

        #region RolesAndPermissions

        public static ImageSource RolesAndPermissions_32
        {
            get { return GetSource(_lockedClients_32); }
        }

        #endregion

        #region Event Log Maintenance

        public static ImageSource EventLog_32
        {
            get { return GetSource(_eventLog_32); }
        }

        public static ImageSource EventLog_ExportToExcel
        {
            get { return GetSource(_exportGridToExcel_32); }
        }

        public static ImageSource EventLog_ExportToExcelAll
        {
            get { return GetSource(_exportGridAllToExcel_32); }
        }

        public static ImageSource EventLog_ExportToExcelSelection
        {
            get { return GetSource(_exportGridSelectionToExcel_32); }
        }

        public static ImageSource EventLog_Refresh
        {
            get { return GetSource(_refreshGrid_32); }
        }

        public static ImageSource EventLog_CopyToClipboard
        {
            get { return GetSource(_clipboard_32); }
        }

        public static ImageSource EventLog_CopyToClipboardAll
        {
            get { return GetSource(_clipboardAll_32); }
        }

        public static ImageSource EventLog_CopyToClipboardSelection
        {
            get { return GetSource(_clipboardSelection_32); }
        }

        public static ImageSource EventLog_SummariseTime
        {
            get { return GetSource(_summariseTime_32); }
        }

        public static ImageSource EventLog_FilterErrors
        {
            get { return GetSource(_errorFilter_32); }
        }

        public static ImageSource EventLog_FilterEntity
        {
            get { return GetSource(_entityFilter_32); }
        }

        public static ImageSource EventLog_FilterOffEntity
        {
            get { return GetSource(_entityFilterOff_32); }
        }

        public static ImageSource EventLog_SummariseTime00Sec
        {
            get { return GetSource(_summariseTime00Sec_32); }
        }

        public static ImageSource EventLog_SummariseTime03Sec
        {
            get { return GetSource(_summariseTime03Sec_32); }
        }

        public static ImageSource EventLog_SummariseTime05Sec
        {
            get { return GetSource(_summariseTime05Sec_32); }
        }

        public static ImageSource EventLog_SummariseTime10Sec
        {
            get { return GetSource(_summariseTime10Sec_32); }
        }

        public static ImageSource EventLog_SummariseTime60Sec
        {
            get { return GetSource(_summariseTime60Sec_32); }
        }

        public static ImageSource EventLog_RemoveAll
        {
            get { return GetSource(_delete_32); }
        }

        #endregion

        #endregion

        #region Planogram Repository

        public static ImageSource PlanogramRepository_OpenPlanogram
        {
            get { return GetSource(_folderOpen_32); }
        }

        public static ImageSource PlanogramRepository_UnlockPlanogram
        {
            get { return GetSource(_Unlock_32); }
        }

        public static ImageSource PlanogramRepository_Planogram
        {
            get { return GetSource(_planogramNoChanges_32); }
        }

        public static ImageSource PlanogramRepository_AddToFavourites32
        {
            get { return GetSource(_favouritesAdd_32); }
        }

        public static ImageSource PlanogramRepository_AddToFavourites16
        {
            get { return GetSource(_favouritesAdd_16); }
        }

        public static ImageSource PlanogramRepository_DeleteFromFavourites32
        {
            get { return GetSource(_favouritesDelete_32); }
        }

        public static ImageSource PlanogramRepository_DeleteFromFavourites16
        {
            get { return GetSource(_favouritesDelete_16); }
        }

        public static ImageSource PlanRepository_Refresh16
        {
            get { return GetSource(_refresh_16); }
        }

        public static ImageSource PlanogramRepository_Locked16
        {
            get { return GetSource(_locked_16); }
        }

        public static ImageSource PlanogramRepository_NewGroup_32
        {
            get { return GetSource(_hierarchyAddLevel_32); }
        }

        public static ImageSource PlanogramRepository_NewGroup_16
        {
            get { return GetSource(_hierarchyAddLevel_16); }
        }

        public static ImageSource PlanogramRepository_EditGroup_32
        {
            get { return GetSource(_hierarchyEditLevel_32); }
        }

        public static ImageSource PlanogramRepository_EditGroup_16
        {
            get { return GetSource(_hierarchyEditLevel_16); }
        }

        public static ImageSource PlanogramRepository_DeleteGroup_32
        {
            get { return GetSource(_hierarchyRemoveLevel_32); }
        }

        public static ImageSource PlanogramRepository_DeleteGroup_16
        {
            get { return GetSource(_hierarchyRemoveLevel_16); }
        }

        public static ImageSource PlanogramRepository_EditPlanogramProperties
        {
            get { return GetSource(_report_itemProperties_16); }
        }

        #endregion

        #region Work Packages

        public static ImageSource WorkPackages_NewWorkpackage { get { return GetSource(_newFile_32); } }
        public static ImageSource WorkPackages_OpenWorkpackage { get { return GetSource(_folderOpen_32); } }
        public static ImageSource WorkPackages_DeleteWorkpackage { get { return GetSource(_delete_16); } }
        public static ImageSource WorkPackages_RunWorkpackage { get { return GetSource(_workpackageRun_16); } }
        public static ImageSource WorkPackages_WorkPackageGridView { get { return GetSource(_workpackageGridView_32); } }
        public static ImageSource WorkPackages_OpenPlan { get { return GetSource(_planOpen_32); } }
        public static ImageSource WorkPackages_Edit { get { return GetSource(_workpackageEdit_32); } }
        public static ImageSource WorkPackages_LoadFromExistingWorkpackage { get { return GetSource(_workpackageOpen_32); } }
        public static ImageSource WorkPackages_NewFromExistingWorkpackage { get { return GetSource(_workpackageNewFromExisting_32); } }
        public static ImageSource WorkPackages_NewFromFile { get { return GetSource(_workpackageNewFromFile_32); } }
        public static ImageSource WorkPackages_DeletePlan { get { return GetSource(_planogramDelete_16); } }
        public static ImageSource WorkPackages_PlanGridView { get { return GetSource(_planGridView_32); } }
        
        public static ImageSource WorkPackages_ProgressComplete { get { return GetSource(_accept_32); } }
        public static ImageSource WorkpackageWizardSetTaskParametersStep_ShowValueSelector { get { return GetSource(_position_16); } }
        public static ImageSource Workpackages_ShowWorkpackageDebug_16 { get { return GetSource(_workpackageDebug_16); } }

        public static ImageSource Workpackages_FirstPage { get { return GetSource(_first_16); } }
        public static ImageSource Workpackages_PrevPage { get { return GetSource(_back_16); } }
        public static ImageSource Workpackages_NextPage { get { return GetSource(_forward_16); } }
        public static ImageSource Workpackages_LastPage { get { return GetSource(_last_16); } }

        public static ImageSource WorkPackages_EventLog { get { return GetSource(_eventLog_32); } }
        public static ImageSource Workpackages_EventLogFilter_16 { get { return GetSource(_filter_16); } }
        public static ImageSource WorkPackages_NewWorkpackage_16 { get { return GetSource(_newFile_16); } }
        public static ImageSource WorkPackages_OpenPlan_16 { get { return GetSource(_open_16); } }

        #endregion

        #region Reports

        public static ImageSource Reports_ReportBuilder_32
        {
            get { return GetSource(_reportBuilderReport_32); }
        }

        public static ImageSource Report_Open_32
        {
            get { return GetSource(_report_Open_32); }
        }

        public static ImageSource Reports_ReportViewer_32
        {
            get { return GetSource(_reportBuilderReport_32); }
        }

        public static ImageSource Reports_DataViewer_32 { get { return GetSource(_reportBuilderReport_32); } }

        public static ImageSource ExportReportToFile_16 { get { return GetSource(_reportExportToFile_16); } }
        public static ImageSource ExportReportToFile_32 { get { return GetSource(_reportExportToFile_32); } }
        public static ImageSource ReportImportFromFile_48 { get { return GetSource(_reportImportFromFile_48); } }


        // Dialogs
        public static ImageSource Dialog_Error
        {
            get { return GetSource(_danger_32); }
        }

        // PageSetup Icons
        public static ImageSource PageOrientation_32
        {
            get { return GetSource(_pageOrientation_32); }
        }

        public static ImageSource PageOrientation_Landscape_32
        {
            get { return GetSource(_pageOrientation_Landscape_32); }
        }

        public static ImageSource PageOrientation_Portrait_32
        {
            get { return GetSource(_pageOrientation_Portrait_32); }
        }

        public static ImageSource PageSize_32
        {
            get { return GetSource(_pageSize_32); }
        }

        public static ImageSource PaperSizeMenuItem
        {
            get { return GetSource(_newFile_32); }
        }

        // Paper Margin icons
        public static ImageSource PaperMarginMenuItem_Narrow
        {
            get { return GetSource(_paperMarginNarrow_32); }
        }

        public static ImageSource PaperMarginMenuItem_Normal
        {
            get { return GetSource(_paperMarginNormal_32); }
        }

        public static ImageSource PaperMarginMenuItem_Wide
        {
            get { return GetSource(_paperMarginWide_32); }
        }

        // Print Preview Icons
        public static ImageSource PrintPreview_32
        {
            get { return GetSource(_printPreview_32); }
        }

        public static ImageSource ReportExcelExport_32
        {
            get { return GetSource(_reportExcelExport_32); }
        }

        public static ImageSource PrintPreviewGrid_32
        {
            get { return GetSource(_printPreviewGrid_32); }
        }

        public static ImageSource PrintPreviewPdf_32
        {
            get { return GetSource(_printPreviewPdf_32); }
        }

        public static ImageSource PrintPreviewClose_32
        {
            get { return GetSource(_turnOff_32); }
        }

        public static ImageSource ExportToExcel_32
        {
            get { return GetSource(_exportDBToExcel_32); }
        }

        public static ImageSource ShowTotals_32
        {
            get { return GetSource(_showTotals_32); }
        }

        // Report Object Icons
        public static ImageSource Chart_16
        {
            get { return GetSource(_chart_16); }
        }

        public static ImageSource Line_16
        {
            get { return GetSource(_line_16); }
        }

        public static ImageSource Picture_16
        {
            get { return GetSource(_picture_16); }
        }

        public static ImageSource TextBox_16
        {
            get { return GetSource(_textBox_16); }
        }

        // Report Icons
        public static ImageSource ReportInfo_32
        {
            get { return GetSource(_reportInfo_32); }
        }

        public static ImageSource ReportAttachTemplate_32
        {
            get { return GetSource(_reportAttachTemplate_32); }
        }

        public static ImageSource Report_24
        {
            get { return GetSource(_reportBuilderReport_24); }
        }

        public static ImageSource Report_32
        {
            get { return GetSource(_reportBuilderReport_32); }
        }

        public static ImageSource Report_48
        {
            get { return GetSource(_reportBuilderReport_48); }
        }

        public static ImageSource ReportSelectDistinct_32
        {
            get { return GetSource(_reportSelectDistinct_32); }
        }

        // ReportGrouping Icons
        public static ImageSource ReportGroup_32
        {
            get { return GetSource(_reportGroup_32); }
        }

        // Filter icons
        public static ImageSource Filter_32
        {
            get { return GetSource(_filter_32); }
        }

       // Sorting icons
        public static ImageSource ReportFieldSort_32
        {
            get { return GetSource(_fieldSort_32); }
        }

        //Save icons
        public static ImageSource SaveAndClose_32
        {
            get { return GetSource(_saveAndClose_32); }
        }

        // Formula Field icons
        public static ImageSource FormulaField_32
        {
            get { return GetSource(_formulaField_32); }
        }

        public static ImageSource FormulaFieldDelete_16
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource FormulaFieldEdit_16
        {
            get { return GetSource(_edit_16_Computer); }
        }

        // Component icons
        public static ImageSource Report_SelectComponent_32
        {
            get { return GetSource(_report_SelectComponent_32); }
        }

        public static ImageSource Report_AddTextBox_32
        {
            get { return GetSource(_report_AddTextBox_32); }
        }

        public static ImageSource Report_AddLine_32
        {
            get { return GetSource(_report_AddLine_32); }
        }

        public static ImageSource Report_RemoveItem_32
        {
            get { return GetSource(_report_RemoveItem_32); }
        }

        public static ImageSource Report_ItemProperties_16
        {
            get { return GetSource(_report_itemProperties_16); }
        }

        public static ImageSource Report_ItemProperties_32
        {
            get { return GetSource(_report_itemProperties_32); }
        }

        // Arrange icons
        public static ImageSource ShowGrid_16
        {
            get { return GetSource(_showGrid_16); }
        }

        public static ImageSource SnapToGrid_16
        {
            get { return GetSource(_snapToGrid_16); }
        }

        public static ImageSource GridSettings_32
        {
            get { return GetSource(_gridSettings_32); }
        }

        // Parameter Icons
        public static ImageSource ReportParameters_32
        {
            get { return GetSource(_reportParameters_32); }
        }

        // Report Preview Icons
        public static ImageSource ReportPreview_32
        {
            get { return GetSource(_reportPreview_32); }
        }

        // Sub Total Icons
        public static ImageSource SubTotalField_32
        {
            get { return GetSource(_reportSubTotals_32); }
        }

        public static ImageSource SubTotalFieldDelete_16
        {
            get { return GetSource(_delete_16); }
        }

        public static ImageSource SubTotalFieldEdit_16
        {
            get { return GetSource(_edit_16_Computer); }
        }

        #endregion

        #region StartUp

        public static ImageSource Startup_Database
        {
            get { return GetSource(_database_24); }
        }

        public static ImageSource Startup_Database32
        {
            get { return GetSource(_database_32); }
        }

        #endregion

        #region Engine Status
        public static ImageSource EngineStatusError_16
        {
            get { return GetSource(_squareCross_16); }
        }
        public static ImageSource EngineStatusInformation_16
        {
            get { return GetSource(_squareInformation_16); }
        }
        public static ImageSource EngineStatusOk_16
        {
            get { return GetSource(_squareTick_16); }
        }
        #endregion

        #region Plan Assignment

        public static ImageSource PlanAssign_MerchCatSearch
        {
            get { return GetSource(_planAssign_Search_16); }
        }

        public static ImageSource Screen_PlanAssignment
        {
            get { return GetSource(_planAssign_Header_16); }
        }

        public static ImageSource PlanAssign_SelectItems
        {
            get { return GetSource(_planogramSelect_32); }
        }

        public static ImageSource PlanAssignment_PublishToGfs
        {
            get { return GetSource(_planogramPublishToGFS_32); }
        }
        
        public static ImageSource PlanAssign_PublishSuccessful
        {
            get { return GetSource(_planAssign_PublishSuccessful_16); }
        }

        public static ImageSource PlanAssign_PublishFailed
        {
            get { return GetSource(_planAssign_PublishFailed_16); }
        }

        public static ImageSource PlanAssign_PublishWarning
        {
            get { return GetSource(_planAssign_PublishException_16); }
        }

        public static ImageSource PlanAssign_Location
        {
            get { return GetSource(_planAssign_Location_16); }
        }

        public static ImageSource PlanAssign_ProductGroup
        {
            get { return GetSource(_planAssign_ProductGroup_16); }
        }

        public static ImageSource PlanAssign_ViewBy
        {
            get { return GetSource(_planAssign_ViewBy_32); }
        }

        public static ImageSource PlanAssignment_SelectCluster
        {
            get { return GetSource(_locationClusters_32); }
        }

        public static ImageSource PlanAssignment_AutoMatch
        {
            get { return GetSource(_autoAssign_32); }
        }

        public static ImageSource PlanAssignment_SelectorAddSelected
        {
            get { return GetSource(_downArrow_16); }
        }

        public static ImageSource PlanAssignment_SelectorRemoveSelected
        {
            get { return GetSource(_upArrow_16); }
        }

        public static ImageSource PlanAssignment_SelectorRemoveAll
        {
            get { return GetSource(_planAssign_SelectorRemoveAll_24); }
        }

        public static ImageSource PlanAssignment_ImportFromFile
        {
            get { return GetSource(_importFromFile_32); }
        }

        public static ImageSource PlanAssignment_ExportData { get { return GetSource(_exportDBToExcel_32); } }

        #endregion                 

        #region Options

        public static ImageSource Options_Header
        {
            get { return GetSource(_options_32); }
        }

        #endregion

        #region Product Buddy
        public static ImageSource ProductBuddy { get { return GetSource(_productBuddy_32); } }
       
        #endregion
    }
}
