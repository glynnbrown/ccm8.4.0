﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Ineson
//	Created
// V8-26560 : A.Probyn
//  Added SequenceMaintenance
// V8-27079 : L.Luong
//  Added BackstageMasterDataSetup
// V8-27940 : L.Luong
//  Added Label Maintenance
// V8-27941 : J.Pickup
//  Added Highlight Maintenance
#endregion
#region Version History: (CCM 8.1.0)
// V8-30275 : M.Pettit
//  Updated Help keys for new file
#endregion
#region Version History: (CCM 8.2.0)
// V8-31403 : A.Probyn
//  Updated help keys for new file.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf
{
    /// <summary>
    /// Static class containing the helpfile keys for this client.
    /// </summary>
    public static class HelpFileKeys
    {
        #region Helper Method
        public static void SetHelpFile(DependencyObject obj, String screenKey)
        {
            Help.SetFilename(obj, App.ViewState.HelpFilePath);

            if (!String.IsNullOrEmpty(screenKey))
            {
                Help.SetKeyword(obj, screenKey);
            }
            else
            {
                Help.SetKeyword(obj, "1");
            }
        }
        #endregion


        //Main screens
        public const String PlanRepository = "2";
        public const String PlanAssignment = "3";
        public const String Workpackages = "4";
        public const String Reports = "5";
        public const String WorkpackageTaskExplorer = "48";
        public const String WorkpackageWizard = "6";
        public const String ReportBuilder = "7";

        //Backstage
        public const String BackstageSysAdmin = "36";
        public const String BackstageSetup = "8";
        public const String BackstageDataManagement = "42";
        public const String BackstageMasterDataSetup = "15";
        public const String BackstageContentSetup = "24";
        public const String BackstageHelp = "43";
        public const String Options = "44";
        public const String BackstageDataManagementImportDataWizard = "45";
        public const String BackstageDataManagementViewData = "46";
 
        //Maintenance forms
        public const String AssortmentMaintenance = "29";
        public const String AssortmentMinorRevisionMaintenance = "30";
        public const String BlockingMaintenance = "26";
        public const String ConsumerDecisionTreeMaintenance = "28";
        public const String HighlightMaintenance = "12";
        public const String InventoryProfileMaintenance = "33";
        public const String LabelMaintenance = "11";
        public const String LocationClusterMaintenance = "31";
        public const String LocationMaintenance = "16";
        public const String LocationHierarchyMaintenance = "17";
        public const String LocationProductAttributeMaintenance = "18";
        public const String LocationProductIllegalMaintenance = "19";
        public const String LocationSpaceMaintenance = "20";
        public const String MetricMaintenance = "23";
        public const String MetricProfileMaintenance = "32";
        public const String MerchandisingHierarchyMaintenance = "21";
        public const String PerformanceSelectionMaintenance = "34";
        public const String PlanogramContentLinkMaintenance = "25";
        public const String PlanogramHierarchyMaintenance = "9";
        public const String PlanogramImportTemplateMaintenance = "13";
        public const String PlanogramExportTemplateMaintenance = "";
        public const String PlanogramNameTemplateMaintenance = "50";
        public const String ProductMaintenance = "22";
        public const String ProductUniverseMaintenance = "27";
        public const String RenumberingStrategyMaintenance = "35";
        public const String SequenceMaintenance = "";
        public const String WorkflowMaintenance = "14";
        public const String ValidationTemplateMaintenance = "10"; 
        

        //Admin
        public const String EntityMaintenance = "39";
        public const String RolePermissions = "38";
        public const String GFSSetup = "40";
        public const String UserMaintenance = "37";
        public const String EventLogs = "41";

        //Other
        public const String CustomColumnsEditor = "47";
        public const String PlanAssignmentAutoMatch = "51";
        public const String WorkpackagePlanogramEventLog = "49";
        public const String PrintTemplateSetup = "52";

    }
}
