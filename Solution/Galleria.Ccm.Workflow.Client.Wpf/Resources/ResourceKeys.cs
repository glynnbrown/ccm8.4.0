﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25438 : L.Hodson ~ Created.
// CCM-25444 : N.haywood
//  Added uom converter and style
// V8-25462 : A.Silva ~ Added ConverterAreEqual constant String.
// V8-26217 : A.Kuszyk
//  Added ConverterBytesToImageSource.
// V8-26244 : A.Kuszyk
//  Added ConverterIntToColor.
// V8-26560 : A.Probyn
//  Added Main_StyHorizontalSeparator
// V8-27153 : A.Silva   ~ Added ConverterEnumToBool.
// V8-27940 : L.Luong
//  Added ConverterFieldTextToDisplay

#endregion

#region Version History: (CCM 8.1)

// V8-29434 : Added ConverterToPercentage

#endregion

#region Version History: CCM811
// V8-29307 : L.Ineson
//  Removed old ConverterIntToColor as this is now in framework.
// V8-30455 : D.Pleasance
//  Added ConverterEnumValueToFriendlyName
#endregion

#endregion

using System;

namespace Galleria.Ccm.Workflow.Client.Wpf
{
    /// <summary>
    /// Holds the resource keys for all resources 
    /// defined in the application resource dictionaries.
    /// </summary>
    public static class ResourceKeys
    {
        #region Main

        /// <summary>
        /// ContentControl control template -  Bullet decorator
        /// </summary>
        public static readonly String Main_tmpBulletContent = "Main_tmpBulletContent";

        /// <summary>
        /// Border style - border and size definition for maintenance page link items displayed in the root window content.
        /// </summary>
        public static readonly String Main_StyMaintenanceWindowButton = "Main_StyMaintenanceWindowButton";

        public static readonly String Buttons_TmpHyperlinkButton = "Buttons_TmpHyperlinkButton";
        public static readonly String Buttons_StyHyperlinkButton = "Buttons_StyHyperlinkButton";
        public static readonly String Text_StyDataManagementErrorCommands = "Text_StyDataManagementErrorCommands";

        public static readonly String DataGrid_StyNullProductGroupRow = "DataGrid_StyNullProductGroupRow";
        public static readonly String DataGrid_StyNullLocationGroupRow = "DataGrid_StyNullLocationGroupRow";

        public static readonly String Main_StySetFieldButton = "Main_StySetFieldButton";

        /// <summary>
        /// TextBlock style - used for uom labels
        /// </summary>
        public static readonly String Text_StyDisplayUom = "Text_StyDisplayUom";

        /// <summary>
        /// A lighter version of the datagrid style
        /// </summary>
        public static readonly String Main_StyLightDatagrid = "Main_StyLightDatagrid";

        /// <summary>
        /// StretchTreeViewItem Control Template - Tree View Item that stretch
        /// </summary>
        public static readonly String Treeview_TmpStretchTreeViewItem = "Treeview_TmpStretchTreeViewItem";
        public static readonly String Treeview_StyStretchTreeViewItem = "Treeview_StyStretchTreeViewItem";

        /// <summary>
        /// Simple border style to draw a horizontal line
        /// </summary>
        public static readonly String Main_StyHorizontalSeparator = "Main_StyHorizontalSeparator";

        #endregion

        #region Res_Diagram

        /// <summary>
        /// Brush - colour used for shine on nodes
        /// </summary>
        public static readonly String Diagram_BrushNodeShineBackground = "Diagram_BrushNodeShineBackground";

        public static readonly String Diagram_BrushNodeNormalBorder = "Diagram_BrushNodeNormalBorder";

        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNodeTriangleShape = "Diagram_TmpNodeTriangleShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNodeRectShape = "Diagram_TmpNodeRectShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNodeTrapezoidShape = "Diagram_TmpNodeTrapezoidShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNodeEllipseShape = "Diagram_TmpNodeEllipseShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNodeDiamondShape = "Diagram_TmpNodeDiamondShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNodePentagonShape = "Diagram_TmpNodePentagonShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNodeHexagonShape = "Diagram_TmpNodeHexagonShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNode180TriangleShape = "Diagram_TmpNode180TriangleShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNode180TrapezoidShape = "Diagram_TmpNode180TrapezoidShape";
        /// <summary>
        /// ContentControl control template - shape definition used by Diagram_StyNodeShapeSelector
        /// </summary>
        public static readonly String Diagram_TmpNode180PentagonShape = "Diagram_TmpNode180PentagonShape";
        /// <summary>
        /// ContentControl style - style for a control which shows a shape based on the given content number 
        /// </summary>
        public static readonly String Diagram_StyNodeShapeSelector = "Diagram_StyNodeShapeSelector";

        /// <summary>
        /// DataTemplate for MerchHierarchyLevelNode
        /// </summary>
        public static readonly String Diagram_DTMerchHierarchyLevel = "Diagram_DTMerchHierarchyLevel";

        /// <summary>
        /// DataTemplate for MerchHierarchyUnitNode
        /// </summary>
        public static readonly String Diagram_DTMerchHierarchyUnit = "Diagram_DTMerchHierarchyUnit";

        /// <summary>
        /// DataTemplate for LocationHierarchyLevelNode
        /// </summary>
        public static readonly String Diagram_DTLocationHierarchyLevel = "Diagram_DTLocationHierarchyLevel";
        /// <summary>
        /// DataTemplate for LocationHierarchyUnitNode
        /// </summary>
        public static readonly String Diagram_DTLocationHierarchyUnit = "Diagram_DTLocationHierarchyUnit";
        #endregion

        #region Converters

        public static readonly String ConverterSourceToDisplayUnitOfMeasure = "ConverterSourceToDisplayUnitOfMeasure";
        public static readonly String ConverterAreEqual =  Galleria.Ccm.Common.Wpf.Resources.ResourceKeys.ConverterAreEqual;
        public static readonly String ConverterBytesToImageSource =  "ConverterBytesToImageSource";
        public static readonly String ConverterUnitDisplay = "ConverterUnitDisplay";
        public static readonly String ConverterFieldTextToDisplay = "ConverterFieldTextToDisplay";
        public static readonly String ConverterEnumTypeToFriendlyNames = Galleria.Ccm.Common.Wpf.Resources.ResourceKeys.ConverterEnumTypeToFriendlyNames;
        public static readonly String ConverterEnumValueToFriendlyName = Galleria.Ccm.Common.Wpf.Resources.ResourceKeys.ConverterEnumValueToFriendlyName;
        public static readonly String ConverterToPercentage = "ConverterToPercentage";

        #endregion


        #region From Common

        public static readonly String Main_StyTopRightTabControl = Ccm.Common.Wpf.Resources.ResourceKeys.Main_StyTopRightTabControl;
        public static readonly String Main_StyTopLeftTabControl = Ccm.Common.Wpf.Resources.ResourceKeys.Main_StyTopLeftTabControl;
        public static readonly String Main_StySplitButton = Ccm.Common.Wpf.Resources.ResourceKeys.Main_StySplitButton;

        #endregion
    }
}
