﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
// V8-24264 : A.Probyn
//  Added fix to ConvertBack to use Convert method instead of casting.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;

namespace Galleria.Ccm.Workflow.Client.Wpf.Resources.Converters
{
    //TODO: Get Rid!

    public enum UnitOfMeasure
    {
        CM,
        MM,
        Inches,
        Feet
    }

    public class SourceToDisplayUnitOfMeasure : IValueConverter
    {
        //CM,
        //MM,
        //Inches,
        //Feet,
        //Bays

        #region IValueConverter methods
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) { return null; }

            UnitOfMeasure sourceType = UnitOfMeasure.CM; //App.ViewState.CurrentEntity.Model.SystemSettings.DataSourceUnitOfMeasure;

            //get the display unit type from the parameter
            //or from the ui settings if the parameter is null
            UnitOfMeasure displayType;
            String paramString = parameter as String;

            if (!String.IsNullOrEmpty(paramString))
            {
                displayType = (UnitOfMeasure)Enum.Parse(typeof(UnitOfMeasure), paramString);
            }
            else
            {
                displayType = UnitOfMeasure.CM; //App.SettingsState.DisplayUnitOfMeasure;
            }


            Double doubleValue = System.Convert.ToDouble(value, CultureInfo.InvariantCulture);

            //convert & return
            Double convertedValue = Convert(doubleValue, sourceType, displayType);
            return Math.Round(convertedValue, 2);
        }



        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            UnitOfMeasure sourceType = UnitOfMeasure.CM;//App.ViewState.CurrentEntity.Model.SystemSettings.DataSourceUnitOfMeasure;

            //get the display unit type from the parameter
            //or from the ui settings if the parameter is null
            UnitOfMeasure displayType;
            String paramString = parameter as String;

            if (!String.IsNullOrEmpty(paramString))
            {
                displayType = (UnitOfMeasure)Enum.Parse(typeof(UnitOfMeasure), paramString);
            }
            else
            {
                displayType = UnitOfMeasure.CM; //App.SettingsState.DisplayUnitOfMeasure;
            }

            //value is a string convert to a float
            Single inputValue;

            if (Single.TryParse(System.Convert.ToString(value), out inputValue))
            {
                //value will be of the display type so convert back and return
                return Convert(inputValue, displayType, sourceType);
            }

            return 0;
        }
        #endregion

        #region Static Converters

        /// <summary>
        /// returns the converted value based on the config specified
        /// source and display uoms
        /// </summary>
        /// <param name="value"></param>
        public static double Convert(double value)
        {
            return
                Math.Round(
                Convert(value,
                UnitOfMeasure.CM,//App.ViewState.CurrentEntity.Model.SystemSettings.DataSourceUnitOfMeasure,
                UnitOfMeasure.CM),//App.SettingsState.DisplayUnitOfMeasure),
                2);
        }

        public static double Convert(double value, UnitOfMeasure toType)
        {
            return
                Math.Round(
                Convert(
                value,
                UnitOfMeasure.CM,//App.ViewState.CurrentEntity.Model.SystemSettings.DataSourceUnitOfMeasure,
                toType),
                2);
        }

        /// <summary>
        /// The main conversion method
        /// </summary>
        /// <param name="value"></param>
        /// <param name="fromType"></param>
        /// <param name="toType"></param>
        /// <returns></returns>
        public static double Convert(double value, UnitOfMeasure fromType, UnitOfMeasure toType)
        {
            //straight return if the same type
            if (fromType == toType)
            {
                return value;
            }

            //single methods for conversion to & from bays
            //if (fromType == UnitOfMeasure.Bays)
            //{
            //    return BaysToSourceUnits(value);
            //}
            //if (toType == UnitOfMeasure.Bays)
            //{
            //    return SourceUnitsToBays(value);
            //}


            //convert the value & return
            switch (fromType)
            {
                case UnitOfMeasure.CM:

                    switch (toType)
                    {
                        case UnitOfMeasure.Feet: return CMToFeet(value);
                        case UnitOfMeasure.Inches: return CMToInches(value);
                        case UnitOfMeasure.MM: return CMToMM(value);
                    }
                    break;

                case UnitOfMeasure.Feet:
                    switch (toType)
                    {
                        case UnitOfMeasure.CM: return FeetToCM(value);
                        case UnitOfMeasure.Inches: return FeetToInches(value);
                        case UnitOfMeasure.MM: return FeetToMM(value);
                    }
                    break;

                case UnitOfMeasure.Inches:
                    switch (toType)
                    {
                        case UnitOfMeasure.CM: return InchesToCM(value);
                        case UnitOfMeasure.Feet: return InchesToFeet(value);
                        case UnitOfMeasure.MM: return InchesToMM(value);
                    }
                    break;

                case UnitOfMeasure.MM:
                    switch (toType)
                    {
                        case UnitOfMeasure.CM: return MMToCM(value);
                        case UnitOfMeasure.Feet: return MMToFeet(value);
                        case UnitOfMeasure.Inches: return MMToInches(value);
                    }
                    break;
            }

            //throw exception if no conversion can be made
            throw new ArgumentOutOfRangeException("fromType", "Conversion is not available for the specified types - needs to be added");
        }

        #region From CM
        private static double CMToMM(double sourceValue)
        {
            return (double)(sourceValue * 10);
        }

        private static double CMToInches(double sourceValue)
        {
            return (double)(sourceValue / 2.5);
        }

        private static double CMToFeet(double sourceValue)
        {
            return (double)((sourceValue / 2.5) / 12);
        }
        #endregion

        #region From MM
        private static double MMToCM(double sourceValue)
        {
            return (double)(sourceValue / 10);
        }

        private static double MMToInches(double sourceValue)
        {
            return (double)((sourceValue / 10) / 2.5);
        }

        private static double MMToFeet(double sourceValue)
        {
            return (double)(((sourceValue / 10) / 2.5) / 12);
        }
        #endregion

        #region From Inches
        private static double InchesToCM(double sourceValue)
        {
            return (double)(sourceValue * 2.5);
        }

        private static double InchesToMM(double sourceValue)
        {
            return (double)((sourceValue * 2.5) * 10);
        }

        private static double InchesToFeet(double sourceValue)
        {
            return (double)(sourceValue / 12);
        }
        #endregion

        #region From Feet
        private static double FeetToCM(double sourceValue)
        {
            return (double)((sourceValue * 12) * 2.5);
        }
        private static double FeetToMM(double sourceValue)
        {
            return (double)(((sourceValue * 12) * 2.5) * 10);
        }
        private static double FeetToInches(double sourceValue)
        {
            return (double)(sourceValue * 12);
        }
        #endregion

        #endregion
    }
}
