﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-27940 : L.Luong 
//  Created.

#endregion

#endregion

using System;
using System.Linq;
using System.Windows.Data;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.Resources.Converters
{
    /// <summary>
    /// Converters the given field text to a display friendly version
    /// </summary>
    public sealed class FieldTextToDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(value as String, PlanogramFieldHelper.EnumerateAllFields(null).ToList());
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(value as String, PlanogramFieldHelper.EnumerateAllFields(null).ToList());
        }
    }
}
