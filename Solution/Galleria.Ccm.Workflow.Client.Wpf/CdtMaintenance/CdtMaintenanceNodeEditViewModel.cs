﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf;
using Galleria.Framework.Collections;
using System.Globalization;
using System.ComponentModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// ViewModel controller for CdtMaintenanceNodeEditWindow
    /// </summary>
    public sealed class CdtMaintenanceNodeEditViewModel : ViewModelAttachedControlObject<CdtMaintenanceNodeEditWindow>, IDataErrorInfo
    {
        #region Fields
        private ConsumerDecisionTree _structure;
        private ConsumerDecisionTreeNode _selectedNode;

        private BulkObservableCollection<Product> _products = new BulkObservableCollection<Product>();
        private ReadOnlyBulkObservableCollection<Product> _productsRO;
        private List<String> _siblingNames = new List<String>();
        private String _editNodeName;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath SelectedNodeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceNodeEditViewModel>(p => p.SelectedNode);
        public static readonly PropertyPath ProductsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceNodeEditViewModel>(p => p.Products);

        public static readonly PropertyPath EditNodeNameProperty = WpfHelper.GetPropertyPath<CdtMaintenanceNodeEditViewModel>(p => p.EditNodeName);
        public static readonly PropertyPath ApplyCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceNodeEditViewModel>(p => p.ApplyCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the selected unit context to edit.
        /// </summary>
        public ConsumerDecisionTreeNode SelectedNode
        {
            get { return _selectedNode; }
            set
            {
                _selectedNode = value;
                OnPropertyChanged(SelectedNodeProperty);
                OnSelectedNodeChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of products assigned to the node
        /// or its children
        /// </summary>
        public ReadOnlyBulkObservableCollection<Product> Products
        {
            get
            {
                if (_productsRO == null)
                {
                    _productsRO = new ReadOnlyBulkObservableCollection<Product>(_products);
                }
                return _productsRO;
            }
        }

        public String EditNodeName
        {
            get
            {
                return _editNodeName;
            }
            set
            {
                _editNodeName = value;
                OnPropertyChanged(EditNodeNameProperty);
            }
        }

        #endregion

        #region Commands

        private RelayCommand _applyCommand;

        /// <summary>
        /// Shows the edit window for the selected node
        /// </summary>
        public RelayCommand ApplyCommand
        {
            get
            {
                if (_applyCommand == null)
                {
                    _applyCommand = new RelayCommand(
                        p => ApplyCommand_Executed(),
                        p => ApplyCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_ApplyAndClose,
                    };
                    base.ViewModelCommands.Add(_applyCommand);
                }
                return _applyCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ApplyCommand_CanExecute()
        {
            if (_siblingNames.Contains(_editNodeName.TrimEnd(' ').ToUpperInvariant()))
            {
                _applyCommand.DisabledReason = Message.CdtMaintenance_DuplicateName;
                return false;
            }

            return true;
        }

        private void ApplyCommand_Executed()
        {
            this.SelectedNode.Name = _editNodeName;

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CdtMaintenanceNodeEditViewModel(ConsumerDecisionTreeNode selectedNode)
        {
            _structure = selectedNode.ParentConsumerDecisionTree;
            this.SelectedNode = selectedNode;
            _editNodeName = this.SelectedNode.Name;
            if (this.SelectedNode.ParentNode != null)
            {
                foreach (ConsumerDecisionTreeNode node in this.SelectedNode.ParentNode.ChildList)
                {
                    if (this.SelectedNode != node)
                    {
                        _siblingNames.Add(node.Name.ToUpperInvariant());
                    }
                }
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected node
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedNodeChanged(ConsumerDecisionTreeNode newValue)
        {
            _products.Clear();

            if (newValue != null)
            {

                IEnumerable<ConsumerDecisionTreeNodeProduct> nodeProducts =
                this.SelectedNode.FetchAllChildConsumerDecisionTreeNodeProducts();

                //[TODO] ProductList.FetchByIds
                foreach (ConsumerDecisionTreeNodeProduct nodeProduct in nodeProducts)
                {
                    _products.Add(Product.FetchById(nodeProduct.ProductId));
                }
            }

        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;
                List<String> siblingNames = new List<String>();
                if (columnName == EditNodeNameProperty.Path)
                {
                    if (_siblingNames.Contains(_editNodeName.TrimEnd(' ').ToUpperInvariant()))
                    {
                        result = String.Format(CultureInfo.CurrentCulture, Message.CdtModel_DuplicateNodeName, SelectedNode.ParentNode.Name);
                    }
                }

                return result; // return result
            }
        }
        #endregion

        #region IDisposable

        /// <summary>
        /// IDisposable implementation
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
