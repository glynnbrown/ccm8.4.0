﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
// V8-26705 : A.Kuszyk
//  Changed ProductSearchType to Common.Wpf version.
// V8-27890 : A.Kuszyk
//  Fixed problem with RemoveNodeCommand removing nodes.
// V8-28409 : A.Kuszyk
//  Added warning prompt for remove nodes and removing levels with child nodes.
// V8-28566 : J.Pickup
//  Removing a level no longer leaves it selected once it has been deleted. (Defaults to root).
// V8-28617 : N.Haywood
//  Fixed a crash caused by deleting a node that hasn't been saved
#endregion
#region Version History: (CCM 8.1.1)
//V8-30325 : I.George
//  Added friendly description messages to SaveAsCommand and DeleteCommand
#endregion
#region Version History : CCM830
// V8-31559 : A.Kuszyk
//  Modified to use GenericSingleItemSelectorWindow.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    public sealed class CdtMaintenanceViewModel : ViewModelAttachedControlObject<CdtMaintenanceOrganiser>
    {
        #region Fields

        const String _exCategory = "CDTMaintenance";

        //permissions
        private Boolean _userHasCDTCreatePerm;
        private Boolean _userHasCDTFetchPerm;
        private Boolean _userHasCDTEditPerm;
        private Boolean _userHasCDTDeletePerm;
        private Boolean _userHasProductGroupFetchPerm;

        private readonly ConsumerDecisionTreeInfoListViewModel _masterCdtListView = new ConsumerDecisionTreeInfoListViewModel();

        //The CDT and Root Node
        private ConsumerDecisionTree _currentCdt;
        private ConsumerDecisionTreeNode _rootNode;

        //Levels
        private Object _selectedLevelId;

        //Nodes DataGrid
        private BulkObservableCollection<ConsumerDecisionTreeNode> _selectedNodes = new BulkObservableCollection<ConsumerDecisionTreeNode>();

        //Products
        private ObservableCollection<ProductInfo> _selectedUnassignedProducts = new ObservableCollection<ProductInfo>();
        private BulkObservableCollection<ProductInfo> _unassignedProducts = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _unassignedProductsRO;

        private ObservableCollection<ProductInfo> _selectedAssignedProducts = new ObservableCollection<ProductInfo>();
        private BulkObservableCollection<ProductInfo> _selectedNodeProducts = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _selectedNodeProductsRO;

        //Product Group
        private Boolean _copyProductGroupProductsToNode = true;
        private ProductGroupInfo _selectedProductGroup;
        private BulkObservableCollection<Int32> _availableProductGroupProductIds = new BulkObservableCollection<Int32>();
        private String _selectedProductGroupName = "";

        //product search related
        private String _productSearchCriteria;
        private Boolean _isProductSearchQueued;
        private Boolean _isProcessingProductSearch;
        private ProductInfoListViewModel _productInfoListView;
        private ProductSearchType _searchType = ProductSearchType.Criteria;
        private ProductUniverseInfo _selectedProductUniverse;
        private ProductGroup _selectedFinancialGroup;

        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath AvailableCdtsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.AvailableCdts);
        public static readonly PropertyPath CurrentCdtProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.CurrentCdt);
        public static readonly PropertyPath RootNodeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.RootNode);
        public static readonly PropertyPath SelectedProductGroupProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedProductGroup);
        public static readonly PropertyPath SelectedLevelIdProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedLevelId);
        public static readonly PropertyPath SelectedNodesProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedNodes);
        public static readonly PropertyPath SelectedUnassignedProductsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedUnassignedProducts);
        public static readonly PropertyPath UnassignedProductsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.UnassignedProducts);
        public static readonly PropertyPath SelectedAssignedProductsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedAssignedProducts);
        public static readonly PropertyPath SelectedNodeProductsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedNodeProducts);
        public static readonly PropertyPath SelectedProductGroupNameProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedProductGroupName);

        //Product search related
        public static readonly PropertyPath ProductSearchCriteriaProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(c => c.ProductSearchCriteria);
        public static readonly PropertyPath IsProcessingProductSearchProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(c => c.IsProcessingProductSearch);
        public static readonly PropertyPath SelectedProductUniverseProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedProductUniverse);
        public static readonly PropertyPath SelectedFinancialGroupProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectedFinancialGroup);
        public static readonly PropertyPath SearchTypeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SearchType);


        //commands
        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath AddNodeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.AddNodeCommand);
        public static readonly PropertyPath RemoveNodeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.RemoveNodeCommand);
        public static readonly PropertyPath EditNodeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.EditNodeCommand);
        public static readonly PropertyPath SplitNodeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SplitNodeCommand);
        public static readonly PropertyPath SelectProductGroupCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectProductGroupCommand);
        public static readonly PropertyPath AddLevelCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.AddLevelCommand);
        public static readonly PropertyPath RemoveLevelCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.RemoveLevelCommand);
        public static readonly PropertyPath AddSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.AddSelectedProductsCommand);
        public static readonly PropertyPath RemoveSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.RemoveSelectedProductsCommand);
        public static readonly PropertyPath AddAllProductsCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.AddAllProductsCommand);
        public static readonly PropertyPath RemoveAllProductsCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.RemoveAllProductsCommand);
        public static readonly PropertyPath MoreSplitPropertiesCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.MoreSplitPropertiesCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath NewFromProductUniverseCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(c => c.NewFromProductUniverseCommand);
        public static readonly PropertyPath NewFromConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(c => c.NewFromConsumerDecisionTreeCommand);
        public static readonly PropertyPath SelectFinancialGroupCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectFinancialGroupCommand);
        public static readonly PropertyPath SelectProductUniverseCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceViewModel>(p => p.SelectProductUniverseCommand);


        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available cdts
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeInfo> AvailableCdts
        {
            get { return _masterCdtListView.BindingView; }
        }

        /// <summary>
        /// Gets the current cdt context
        /// </summary>
        public ConsumerDecisionTree CurrentCdt
        {
            get { return _currentCdt; }
            private set
            {
                ConsumerDecisionTree oldValue = _currentCdt;

                _currentCdt = value;
                OnPropertyChanged(CurrentCdtProperty);
                OnCurrentCdtChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Returns the root node view for the current cdt
        /// </summary>
        public ConsumerDecisionTreeNode RootNode
        {
            get { return _rootNode; }
            private set
            {
                ConsumerDecisionTreeNode oldValue = _rootNode;

                _rootNode = value;
                OnPropertyChanged(RootNodeProperty);
                OnRootNodeChanged(value);
            }
        }

        /// <summary>
        /// Gets/Sets the Assigned Product Group for the current CDT
        /// </summary>
        public ProductGroupInfo SelectedProductGroup
        {
            get { return _selectedProductGroup; }
            set
            {
                _selectedProductGroup = value;
                OnPropertyChanged(SelectedProductGroupProperty);

                if (value != null)
                {
                    _currentCdt.ProductGroupId = _selectedProductGroup.Id;
                    _selectedProductGroupName = value.ToString();
                }
                else
                {
                    _selectedProductGroupName = "";
                }

                OnPropertyChanged(SelectedProductGroupNameProperty);

                OnSelectedProductGroupChanged(value);
            }
        }

        /// <summary>
        /// Returns the name of the selected product group
        /// </summary>
        public String SelectedProductGroupName
        {
            get { return _selectedProductGroupName; }
        }

        /// <summary>
        /// Gets/Sets the id of the selected level
        /// </summary>
        public Object SelectedLevelId
        {
            get { return _selectedLevelId; }
            set { _selectedLevelId = value; }
        }

        /// <summary>
        /// Returns the editable collection of selected nodes
        /// </summary>
        public BulkObservableCollection<ConsumerDecisionTreeNode> SelectedNodes
        {
            get { return _selectedNodes; }
        }

        /// <summary>
        /// Returns the collection of selected unassigned products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedUnassignedProducts
        {
            get { return _selectedUnassignedProducts; }
        }

        /// <summary>
        /// Returns the collection of unassigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> UnassignedProducts
        {
            get
            {
                if (_unassignedProductsRO == null)
                {
                    _unassignedProductsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_unassignedProducts);
                }
                return _unassignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected assigned products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedAssignedProducts
        {
            get { return _selectedAssignedProducts; }
        }

        /// <summary>
        /// Returns the collection of products linked to the selected nodes
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> SelectedNodeProducts
        {
            get
            {
                if (_selectedNodeProductsRO == null)
                {
                    _selectedNodeProductsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_selectedNodeProducts);
                }
                return _selectedNodeProductsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the criteria to use when searching for products
        /// </summary>
        public String ProductSearchCriteria
        {
            get { return _productSearchCriteria; }
            set
            {
                _productSearchCriteria = value;
                OnPropertyChanged(ProductSearchCriteriaProperty);
                OnProductSearchCriteriaChanged();
            }
        }

        /// <summary>
        /// Returns true if a product search is currently being processed
        /// </summary>
        public Boolean IsProcessingProductSearch
        {
            get { return _isProcessingProductSearch; }
            private set
            {
                _isProcessingProductSearch = value;
                OnPropertyChanged(IsProcessingProductSearchProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selection type for searching
        /// </summary>
        public ProductSearchType SearchType
        {
            get { return _searchType; }
            set
            {
                _searchType = value;
                OnPropertyChanged(SearchTypeProperty);
                RefreshProductSearchResults();
            }
        }

        /// <summary>
        /// Gets/Sets the selected financial group search criteria
        /// </summary>
        public ProductGroup SelectedFinancialGroup
        {
            get { return _selectedFinancialGroup; }
            set
            {
                _selectedFinancialGroup = value;
                OnPropertyChanged(SelectedFinancialGroupProperty);
                RefreshProductSearchResults();
            }
        }

        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ProductUniverseInfo SelectedProductUniverse
        {
            get { return _selectedProductUniverse; }
            set
            {
                _selectedProductUniverse = value;
                OnPropertyChanged(SelectedProductUniverseProperty);
                RefreshProductSearchResults();
            }
        }


        #endregion

        #region Events

        public event EventHandler MajorNodeChangeStarting;
        private void OnMajorNodeChangeStarting()
        {
            if (this.MajorNodeChangeStarting != null)
            {
                this.MajorNodeChangeStarting(this, EventArgs.Empty);
            }
        }

        public event EventHandler MajorNodeChangeComplete;
        private void OnMajorNodeChangeComplete()
        {
            if (this.MajorNodeChangeComplete != null)
            {
                this.MajorNodeChangeComplete(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CdtMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            _selectedNodes.BulkCollectionChanged += SelectedNodes_BulkCollectionChanged;

            //products
            _productInfoListView = new ProductInfoListViewModel();
            _productInfoListView.ModelChanged += ProductInfoListView_ModelChanged;

            //master cdt list
            _masterCdtListView.FetchForCurrentEntity();

            //load a new bank item
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new item (default)
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    this.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userHasCDTCreatePerm)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //load a new item
                this.CurrentCdt = ConsumerDecisionTree.NewConsumerDecisionTree(App.ViewState.EntityId);

                this.SelectedProductUniverse = null;
                this.SelectedFinancialGroup = null;
                this.SearchType = ProductSearchType.Criteria;

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }
        }

        #endregion

        #region NewFromProductUniverseCommand

        private RelayCommand _newFromProductUniverseCommand;
        /// <summary>
        /// Launches a window to create a new Consumer Decision Tree
        /// </summary>
        public RelayCommand NewFromProductUniverseCommand
        {
            get
            {
                if (_newFromProductUniverseCommand == null)
                {
                    _newFromProductUniverseCommand = new RelayCommand(p => NewFromProductUniverse_Executed(), p => New_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTree_NewFromProductUniverse,
                        FriendlyDescription = Message.ConsumerDecisionTree_New_Description,
                        Icon = ImageResources.ConsumerDecisionTree_AddFromProductUniverse,
                        SmallIcon = ImageResources.New_16,
                        InputGestureKey = Key.N,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    this.ViewModelCommands.Add(_newFromProductUniverseCommand);
                }
                return _newFromProductUniverseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean NewFromProductUniverse_CanExecute()
        {
            return true;
        }

        private void NewFromProductUniverse_Executed()
        {
            if (ContinueWithItemChange())
            {
                //show universe selection screen
                if (this.AttachedControl != null)
                {
                    this.CurrentCdt = ConsumerDecisionTree.NewConsumerDecisionTree(App.ViewState.EntityId);

                    CdtMaintenanceCreateOrganiser newWindow = new CdtMaintenanceCreateOrganiser(this.CurrentCdt, null, CdtWizardType.Create);

                    newWindow.ViewModel.SelectedProductUniverse = this.SelectedProductUniverse;

                    this.SelectedProductUniverse = null;
                    this.SelectedFinancialGroup = null;
                    this.SearchType = ProductSearchType.Criteria;

                    App.ShowWindow(newWindow, this.AttachedControl, true);

                    if (newWindow.DialogResult != null && (Boolean)newWindow.DialogResult && newWindow.ViewModel.SelectedProductUniverse.ProductGroupId != null)
                    {
                        this.SelectedProductGroup = ProductGroupInfo.FetchById((Int32)newWindow.ViewModel.SelectedProductUniverse.ProductGroupId);
                        this.SelectedProductUniverse = newWindow.ViewModel.SelectedProductUniverse;
                    }
                }

            }
        }

        #endregion

        #region NewFromConsumerDecisionTreeCommand

        private RelayCommand _newFromConsumerDecisionTreeCommand;
        /// <summary>
        /// Launches a window to create a new Consumer Decision Tree
        /// </summary>
        public RelayCommand NewFromConsumerDecisionTreeCommand
        {
            get
            {
                if (_newFromConsumerDecisionTreeCommand == null)
                {
                    _newFromConsumerDecisionTreeCommand = new RelayCommand(p => NewFromConsumerDecisionTree_Executed(), p => New_CanExecute())
                    {
                        FriendlyName = Message.ConsumerDecisionTree_NewFromConsumerDecisionTree,

                        FriendlyDescription = Message.ConsumerDecisionTree_New_Description,
                        Icon = ImageResources.ConsumerDecisionTree_AddFromCDT,
                        SmallIcon = ImageResources.New_16,
                        InputGestureKey = Key.N,
                        InputGestureModifiers = ModifierKeys.Control
                    };
                    this.ViewModelCommands.Add(_newFromConsumerDecisionTreeCommand);
                }
                return _newFromConsumerDecisionTreeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean NewFromConsumerDecisionTree_CanExecute()
        {
            return true;
        }

        private void NewFromConsumerDecisionTree_Executed()
        {
            if (ContinueWithItemChange())
            {
                //show universe selection screen
                if (this.AttachedControl != null)
                {
                    this.CurrentCdt = ConsumerDecisionTree.NewConsumerDecisionTree(App.ViewState.EntityId);

                    CdtMaintenanceCreateOrganiser newWindow = new CdtMaintenanceCreateOrganiser(this.CurrentCdt, null, CdtWizardType.Merge);

                    newWindow.ViewModel.SelectedProductUniverse = this.SelectedProductUniverse;

                    this.SelectedProductUniverse = null;
                    this.SelectedFinancialGroup = null;
                    this.SearchType = ProductSearchType.Criteria;

                    App.ShowWindow(newWindow, this.AttachedControl, true);

                    if (newWindow.DialogResult != null && (Boolean)newWindow.DialogResult && newWindow.ViewModel.SelectedProductUniverse.ProductGroupId != null)
                    {
                        this.SelectedProductGroup = ProductGroupInfo.FetchById((Int32)newWindow.ViewModel.SelectedProductUniverse.ProductGroupId);
                        this.SelectedProductUniverse = newWindow.ViewModel.SelectedProductUniverse;
                    }
                }
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Opens the cdt with the given id
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? id)
        {
            //must not be null
            if (id == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have get permission
            if (!_userHasCDTFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? id)
        {
            //warn the user if the current item is dirty
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //fetch the item
                    this.CurrentCdt = ConsumerDecisionTree.FetchById(id.Value);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                    base.ShowWaitCursor(true);
                }

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current cdt
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasCDTEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be null
            if (this.CurrentCdt == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentCdt.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //levels must have unique names
            IEnumerable<ConsumerDecisionTreeLevel> levels = this.CurrentCdt.FetchAllLevels();
            if (levels.Select(p => p.Name).Distinct().Count() < levels.Count())
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            //save the item
            SaveCurrentItem();
        }

        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //** check the item unique value
            String newName = this.CurrentCdt.Name;
            Int32 cdtId = this.CurrentCdt.Id;

            IEnumerable<String> takenCdtNames =
                _masterCdtListView.Model.Where(c => c.Id != cdtId).Select(c => c.Name);

            Boolean codeAccepted = true;
            if (this.AttachedControl != null)
            {
                codeAccepted =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(s => !takenCdtNames.Contains(s), this.CurrentCdt.Name, out newName);
            }
            else
            {
                //force a unique value for unit testing
                newName = 
                    Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.CurrentCdt.Name, takenCdtNames);
            }

            //set the code
            if (codeAccepted)
            {
                if (this.CurrentCdt.Name != newName)
                {
                    this.CurrentCdt.Name = newName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.CurrentCdt = this.CurrentCdt.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired =
                            Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.CurrentCdt.Name);
                        if (itemReloadRequired)
                        {
                            this.CurrentCdt = ConsumerDecisionTree.FetchById(this.CurrentCdt.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.CurrentCdt.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //update the info list
                _masterCdtListView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }

            return continueWithSave;
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a new copy
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    this.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //user must have edit permission
            if (!_userHasCDTCreatePerm)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.CurrentCdt == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.CurrentCdt.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //levels must have unique names
            IEnumerable<ConsumerDecisionTreeLevel> levels = this.CurrentCdt.FetchAllLevels();
            if (levels.Select(p => p.Name).Distinct().Count() < levels.Count())
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            String selectedName;
            Boolean nameIsUnique = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                    (s) =>
                    {
                        IEnumerable<ConsumerDecisionTreeInfo> matchingNames =
                            _masterCdtListView.Model.Where(p => p.Name == s);
                        return !(matchingNames.Count() > 0);
                    };

                nameIsUnique =
                    Galleria.Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(isUniqueCheck, this.CurrentCdt.Name, out selectedName);
            }
            else
            {
                selectedName =
                    Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.CurrentCdt.Name, _masterCdtListView.Model.Select(p => p.Name));
            }

            if (nameIsUnique)
            {
                base.ShowWaitCursor(true);

                ConsumerDecisionTree itemCopy = this.CurrentCdt.Copy();
                itemCopy.Name = selectedName;
                itemCopy = itemCopy.Save();

                this.CurrentCdt = itemCopy;

                _masterCdtListView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region SaveAndClose

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current model and closes the
        /// attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(p => SaveAndClose_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    this.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region SaveAndNew

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Saves the current item and creates a new one
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Deletes the current item
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    this.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //must not be null
            if (this.CurrentCdt == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have delete permission
            if (!_userHasCDTDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be new
            if (this.CurrentCdt.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                //confirm the delete with the user
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.CurrentCdt.Name);
            }

            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the item as deleted so item changed does not show.
                ConsumerDecisionTree itemToDelete = this.CurrentCdt;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                Boolean deleteSuccessful = true;
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    deleteSuccessful = false;
                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                if (deleteSuccessful)
                {
                    _masterCdtListView.FetchForCurrentEntity();
                }

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region AddNodeCommand

        private RelayCommand _addNodeCommand;

        /// <summary>
        /// Adds a new node underneath the selected nodes
        /// </summary>
        public RelayCommand AddNodeCommand
        {
            get
            {
                if (_addNodeCommand == null)
                {
                    _addNodeCommand = new RelayCommand(
                        p => AddNode_Executed(),
                        p => AddNode_CanExecute())
                    {
                        FriendlyName = Message.CdtMaintenance_AddNode,
                        FriendlyDescription = Message.CdtMaintenance_AddNode_Description,
                        Icon = ImageResources.CdtMaintenance_AddNode
                    };
                    base.ViewModelCommands.Add(_addNodeCommand);
                }
                return _addNodeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AddNode_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddNode_Executed()
        {
            base.ShowWaitCursor(true);

            List<ConsumerDecisionTreeNode> addToNodeList = this.SelectedNodes.ToList();

            //clear selected items
            this.SelectedNodes.Clear();

            List<ConsumerDecisionTreeNode> addedNodes = new List<ConsumerDecisionTreeNode>();

            foreach (ConsumerDecisionTreeNode parentNode in addToNodeList)
            {
                //add the node
                ConsumerDecisionTreeNode newNode = parentNode.AddNewChildNode();

                if (newNode != null)
                {
                    addedNodes.Add(newNode);
                }
            }

            this.SelectedNodes.AddRange(addedNodes);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region RemoveNodeCommand

        private RelayCommand _removeNodeCommand;

        /// <summary>
        /// Removes the selected nodes
        /// </summary>
        public RelayCommand RemoveNodeCommand
        {
            get
            {
                if (_removeNodeCommand == null)
                {
                    _removeNodeCommand = new RelayCommand(
                        p => RemoveNode_Executed(),
                        p => RemoveNode_CanExecute())
                    {
                        FriendlyName = Message.CdtMaintenance_RemoveNode,
                        FriendlyDescription = Message.CdtMaintenance_RemoveNode_Description,
                        Icon = ImageResources.CdtMaintenance_RemoveNode,
                        SmallIcon = ImageResources.CdtMaintenance_RemoveNode_16
                    };
                    base.ViewModelCommands.Add(_removeNodeCommand);
                }
                return _removeNodeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RemoveNode_CanExecute()
        {
            //at least one node must be selected
            if (this.SelectedNodes.Count == 0)
            {
                this.RemoveNodeCommand.DisabledReason = Message.CdtMaintenance_RemoveNode_DisabledNoItems;
                return false;
            }

            //if only a single node is selected it must not be the root
            if (this.SelectedNodes.Count == 1)
            {
                if (this.SelectedNodes[0] != null && this.SelectedNodes[0].IsRoot)
                {
                    this.RemoveNodeCommand.DisabledReason = Message.CdtMaintenance_RemoveNode_DisabledRootNode;
                    return false;
                }
            }

            return true;
        }

        private void RemoveNode_Executed()
        {
            //Continue with action, default to true for unit tests
            Boolean continueWithAction = true;
            if (this.AttachedControl != null)
            {
                continueWithAction = LocalHelper.ContinueWithActionPrompt(Message.CdtMaintenance_RemoveNode, Message.CdtMaintenance_RemoveNode, Message.CdtMaintenance_RemoveNode_ConfirmationDesc);
            }

            if (continueWithAction)
            {

                base.ShowWaitCursor(true);

                List<ConsumerDecisionTreeNode> removeNodeList = this.SelectedNodes.ToList();

                //clear selected items
                this.SelectedNodes.Clear();

                foreach (ConsumerDecisionTreeNode removeNode in removeNodeList)
                {
                    if (removeNode != null)
                    {
                        //Fetch levels
                        IEnumerable<ConsumerDecisionTreeLevel> levels = this.CurrentCdt.FetchAllLevels();

                        //Find node level
                        ConsumerDecisionTreeLevel level = levels.FirstOrDefault(p => p.Id.Equals(removeNode.ConsumerDecisionTreeLevelId));

                        //Get all nodes at same level
                        IEnumerable<ConsumerDecisionTreeNode> levelNodes = this.RootNode.FetchAllChildNodes().
                            Where(p => p.ConsumerDecisionTreeLevelId.Equals(removeNode.ConsumerDecisionTreeLevelId) && p != removeNode);

                        //Flag to update levels
                        Boolean isLevelsRemoved = false;

                        //Get assigned products
                        IEnumerable<Int32> productIds = removeNode.Products.Select(p => p.ProductId);

                        //Add locations to unassigned list
                        this._unassignedProducts.AddRange(ProductInfoList.FetchByEntityIdProductIds(this.CurrentCdt.EntityId, productIds));

                        //Check if this is the last node at level, if so, remove level
                        if (!levelNodes.Any())
                        {
                            //Remove level
                            this.CurrentCdt.RemoveLevel(level, false);

                            //Set flag to update levels
                            isLevelsRemoved = true;
                        }
                        else
                        {
                            //remove the node
                            removeNode.ParentNode.ChildList.Remove(removeNode);
                        }

                        if (this.AttachedControl != null)
                        {
                            if (isLevelsRemoved)
                            {
                                //Manually call UpdateLevels if the selected nodes collection was not changed
                                this.AttachedControl.UpdateLevels();
                            }
                        }
                    }
                }

                //Reset available products
                ResetAvailableProducts();

                //Update node product properties
                UpdateNodeProductProperties();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region EditNodeCommandCommand

        private RelayCommand _editNodeCommand;

        /// <summary>
        /// Shows the edit window for the selected node
        /// </summary>
        public RelayCommand EditNodeCommand
        {
            get
            {
                if (_editNodeCommand == null)
                {
                    _editNodeCommand = new RelayCommand(
                        p => EditNodeCommand_Executed(),
                        p => EditNodeCommand_CanExecute())
                    {
                        FriendlyName = Message.CdtMaintenance_EditNode,
                        FriendlyDescription = Message.CdtMaintenance_EditNode_Description,
                        Icon = ImageResources.CdtMaintenance_ShowUnitEditWindow,
                        SmallIcon = ImageResources.CdtMaintenance_ShowUnitEditWindow
                    };
                    base.ViewModelCommands.Add(_editNodeCommand);
                }
                return _editNodeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean EditNodeCommand_CanExecute()
        {
            //must contain 1 node and only 1 node
            if (this.SelectedNodes.Count != 1)
            {
                this.EditNodeCommand.DisabledReason = Message.CdtMaintenance_EditNode_DisabledMulitpleItems;
                return false;
            }

            return true;
        }

        private void EditNodeCommand_Executed()
        {
            if (this.AttachedControl != null)
            {
                CdtMaintenanceNodeEditWindow win =
                    new CdtMaintenanceNodeEditWindow(this.SelectedNodes[0]);

                App.ShowWindow(win, this.AttachedControl, true);
            }
        }

        #endregion

        #region SplitNodeCommand

        private RelayCommand<ObjectFieldInfo> _splitNodeCommand;

        /// <summary>
        /// Splits the selected node by the given property
        /// </summary>
        public RelayCommand<ObjectFieldInfo> SplitNodeCommand
        {
            get
            {
                if (_splitNodeCommand == null)
                {
                    _splitNodeCommand = new RelayCommand<ObjectFieldInfo>(
                        p => SplitNode_Executed(p),
                        p => SplitNode_CanExecute(p))
                    {
                        FriendlyName = Message.CdtMaintenance_SplitNode,
                        FriendlyDescription = Message.CdtMaintenance_SplitNode_Description,
                        Icon = ImageResources.CdtMaintenance_SplitNode_32
                    };
                    base.ViewModelCommands.Add(_splitNodeCommand);
                }
                return _splitNodeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SplitNode_CanExecute(ObjectFieldInfo property)
        {
            //property must not be null
            if (property == null)
            {
                SplitNodeCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must not be null
            if (this.SelectedNodes.Count == 0)
            {
                SplitNodeCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must be leaf
            if (this.SelectedNodes.Any(n =>
            {
                if (n != null)
                {
                    return (n.ChildList.Count != 0);
                }
                return false;
            }))
            {
                SplitNodeCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must have products
            if (!this.SelectedNodes.Any(n =>
            {
                if (n != null)
                {
                    return (n.TotalProductCount > 0);
                }
                return false;
            }
                ))
            {
                SplitNodeCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void SplitNode_Executed(ObjectFieldInfo property)
        {
            OnMajorNodeChangeStarting();

            base.ShowWaitCursor(true);

            List<ConsumerDecisionTreeNode> splitNodeList = this.SelectedNodes.ToList();
            List<ConsumerDecisionTreeNode> createdNodes = new List<ConsumerDecisionTreeNode>();

            this.SelectedNodes.Clear();

            List<ConsumerDecisionTreeNode> nullValueNodes = new List<ConsumerDecisionTreeNode>();
            List<Product> products = new List<Product>();

            foreach (ConsumerDecisionTreeNode splitNode in splitNodeList)
            {
                //get the products for the node
                List<Product> nodeProducts = new List<Product>();
                IEnumerable<Int32> attachedProductIds =
                    splitNode.FetchAllChildConsumerDecisionTreeNodeProducts().Select(p => p.ProductId);

                ProductList assignedProducts = ProductList.FetchByProductIds(attachedProductIds);

                nodeProducts.AddRange(assignedProducts);
                products.AddRange(assignedProducts);

                //split
                ConsumerDecisionTreeNode nullValueNode = splitNode.SplitByProductProperties
                    (new List<ObjectFieldInfo>() { property }, nodeProducts);

                //If theere are null values represented by a node, add to the list
                if (nullValueNode != null)
                {
                    nullValueNodes.Add(nullValueNode);
                }

                createdNodes.AddRange(splitNode.ChildList);
            }

            //If null nodes exist
            if (nullValueNodes.Count > 0)
            {
                base.ShowWaitCursor(false);
                //Get properties
                IEnumerable<ObjectFieldInfo> properties = Product.EnumerateDisplayableFieldInfos();

                //Show dialog
                CdtMaintenanceSplitByResultsWindow win = new CdtMaintenanceSplitByResultsWindow(this.CurrentCdt, products, properties, nullValueNodes);
                App.ShowWindow(win, this.AttachedControl, true);

                base.ShowWaitCursor(true);
            }

            //Fetch all levels in descending order
            IEnumerable<ConsumerDecisionTreeLevel> currentLevels = this.CurrentCdt.FetchAllLevels().OrderByDescending(p => p.LevelNumber);
            //Enumerate through levels
            foreach (ConsumerDecisionTreeLevel level in currentLevels)
            {
                //Ensure name is unique
                level.Name = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(level.Name, currentLevels.Where(p => p != level).Select(p => p.Name));
            }

            OnMajorNodeChangeComplete();

            this.UpdateNodeProductProperties();
            this.SelectedNodes.AddRange(createdNodes);

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AssignProductsCommand

        //private RelayCommand _assignProductsCommand;

        //public RelayCommand AssignProductsCommand
        //{
        //    get
        //    {
        //        if (_assignProductsCommand == null)
        //        {
        //            _assignProductsCommand = new RelayCommand(
        //                p => AssignProducts_Executed(),
        //                p => AssignProducts_CanExecute())
        //            {
        //                FriendlyName = Message.CdtMaintenance_AssignProducts,
        //                Icon = ImageResources.CdtMaintenance_AssignProducts
        //            };
        //        }
        //        return _assignProductsCommand;
        //    }
        //}

        //private Boolean AssignProducts_CanExecute()
        //{
        //    //Can only assign products to one node
        //    if (this.SelectedNodes.Count > 1)
        //    {
        //        this.AssignProductsCommand.DisabledReason = String.Empty;
        //        return false;
        //    }

        //    //Must have a selected node
        //    if (this.SelectedNode == null)
        //    {
        //        this.AssignProductsCommand.DisabledReason = String.Empty;
        //        return false;
        //    }

        //    //Must be a leaf node
        //    if (this.SelectedNode.ChildViews.Count > 0)
        //    {
        //        this.AssignProductsCommand.DisabledReason = String.Empty;
        //        return false;
        //    }

        //    return true;
        //}

        //private void AssignProducts_Executed()
        //{
        //    if (this.AttachedControl != null &&
        //        ContinueAsManual())
        //    {
        //    }
        //}

        #endregion

        #region SelectProductGroupCommand

        private RelayCommand _selectProductGroupCommand;

        /// <summary>
        /// Shows the select product group dialog
        /// </summary>
        public RelayCommand SelectProductGroupCommand
        {
            get
            {
                if (_selectProductGroupCommand == null)
                {
                    _selectProductGroupCommand = new RelayCommand(
                        p => SelectProductGroup_Executed(),
                        p => SelectProductGroup_CanExecute())
                    {
                    };
                    base.ViewModelCommands.Add(_selectProductGroupCommand);
                }
                return _selectProductGroupCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SelectProductGroup_CanExecute()
        {
            //user must have get permission
            if (!_userHasProductGroupFetchPerm)
            {
                SelectProductGroupCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SelectProductGroup_Executed()
        {
            if (AttachedControl != null)
            {
                if (ContinueWithRecreate())
                {
                    MerchGroupSelectionWindow win = new MerchGroupSelectionWindow();
                    win.SelectionMode = DataGridSelectionMode.Single;
                    App.ShowWindow(win, AttachedControl, /*isModal*/true);

                    if (win.SelectionResult != null && win.SelectionResult.Count > 0)
                    {
                        ProductGroup selectedGroup = win.SelectionResult.FirstOrDefault();
                        if (selectedGroup != null)
                        {
                            this.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(selectedGroup);
                        }
                    }
                }
            }

        }

        #endregion

        #region SelectAddProductsNodeCommand

        //private RelayCommand _selectAddProductsNodeCommand;

        ///// <summary>
        ///// Shows the dialog to allow a node to be selected to which
        ///// the selected producst will be added
        ///// </summary>
        //public RelayCommand SelectAddProductsNodeCommand
        //{
        //    get
        //    {
        //        if (_selectAddProductsNodeCommand == null)
        //        {
        //            _selectAddProductsNodeCommand = new RelayCommand(
        //                p => SelectAddProductsNode_Executed(),
        //                p => SelectAddProductsNode_CanExecute())
        //                {
        //                    FriendlyName = Message.CdtMaintenance_SelectAddProductsNode
        //                };
        //            base.ViewModelCommands.Add(_selectAddProductsNodeCommand);
        //        }
        //        return _selectAddProductsNodeCommand;
        //    }
        //}

        //[DebuggerStepThrough]
        //private Boolean SelectAddProductsNode_CanExecute()
        //{
        //    //products must be selected
        //    if (this.SelectedProductUniverseProducts.Count == 0)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //private void SelectAddProductsNode_Executed()
        //{
        //    if (this.AttachedControl != null)
        //    {
        //        //selection is from leaf nodes
        //        QuickItemsSelectDialog dia = new QuickItemsSelectDialog();
        //        dia.ItemSource = this.CurrentCdt.RootNode.FetchAllChildNodes().Where(n => n.ChildList.Count == 0).ToList();
        //        dia.SelectionMode = DataGridSelectionMode.Single;

        //        //show the dialog
        //        App.ShowWindow(dia, true);

        //        if (dia.DialogResult == true)
        //        {
        //            base.ShowWaitCursor(true);

        //            ConsumerDecisionTreeNode node = dia.SelectedItems.Cast<ConsumerDecisionTreeNode>().First();

        //            List<ConsumerDecisionTreeNodeProduct> products = new List<ConsumerDecisionTreeNodeProduct>();
        //            foreach (ProductInfo p in this.SelectedProductUniverseProducts)
        //            {
        //                products.Add(ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(p.Id));
        //            }

        //            node.Products.AddList(products);


        //            //remove the selected products from the available products list
        //            _availableProductGroupProducts.RemoveRange(this.SelectedProductUniverseProducts.ToList());
        //            this.SelectedProductUniverseProducts.Clear();

        //            base.ShowWaitCursor(false);
        //        }

        //    }
        //}

        #endregion

        #region AddLevelCommand

        private RelayCommand _addLevelCommand;

        public RelayCommand AddLevelCommand
        {
            get
            {
                if (_addLevelCommand == null)
                {
                    _addLevelCommand = new RelayCommand(
                        p => AddLevel_Executed(),
                        p => AddLevel_CanExecute())
                    {
                        FriendlyName = Message.CdtMaintenance_AddLevel,
                        FriendlyDescription = Message.CdtMaintenance_AddLevel_Description,
                        Icon = ImageResources.CdtMaintenance_AddLevel
                    };
                    base.ViewModelCommands.Add(_addLevelCommand);
                }
                return _addLevelCommand;
            }
        }

        private Boolean AddLevel_CanExecute()
        {
            //current cdt must not be null
            if (this.CurrentCdt == null)
            {
                this.AddLevelCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void AddLevel_Executed()
        {
            base.ShowWaitCursor(true);

            ConsumerDecisionTreeLevel newLevel = null;

            if (this.SelectedLevelId != null)
            {
                //If there is a level selected add the new level as a child
                ConsumerDecisionTreeLevel level = this.CurrentCdt.FetchAllLevels().FirstOrDefault(p => p.Id.Equals(this.SelectedLevelId));
                if (level != null)
                {
                    newLevel = this.CurrentCdt.InsertNewChildLevel(level);
                }
            }
            else
            {
                //There is no level selected, so add the child to the last level
                ConsumerDecisionTreeLevel level = this.CurrentCdt.FetchAllLevels().Last();
                newLevel = this.CurrentCdt.InsertNewChildLevel(level);
            }

            if (newLevel != null &&
                this.AttachedControl != null)
            {
                //Manually call UpdateLevels
                this.AttachedControl.UpdateLevels();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region RemoveLevelCommand

        private RelayCommand _removeLevelCommand;

        public RelayCommand RemoveLevelCommand
        {
            get
            {
                if (_removeLevelCommand == null)
                {
                    _removeLevelCommand = new RelayCommand(
                        p => RemoveLevel_Executed(),
                        p => RemoveLevel_CanExecute())
                    {
                        FriendlyName = Message.CdtMaintenance_RemoveLevel,
                        FriendlyDescription = Message.CdtMaintenance_RemoveLevel_Description,
                        Icon = ImageResources.CdtMaintenance_RemoveLevel,
                        SmallIcon = ImageResources.Modelling_RemoveRange
                    };
                    base.ViewModelCommands.Add(_removeLevelCommand);
                }
                return _removeLevelCommand;
            }
        }

        private Boolean RemoveLevel_CanExecute()
        {
            //current cdt must not be null
            if (this.CurrentCdt == null)
            {
                this.RemoveLevelCommand.DisabledReason = String.Empty;
                return false;
            }

            //items must be selected
            if (this.SelectedLevelId == null)
            {
                this.RemoveLevelCommand.DisabledReason = Message.CdtMaintenance_RemoveLevel_DisabledNoLevel;
                return false;
            }

            //must not be the root level
            if (this.SelectedLevelId.Equals(this.CurrentCdt.RootLevel.Id))
            {
                this.RemoveLevelCommand.DisabledReason = Message.CdtMaintenance_RemoveLevel_DisabledRootLevel;
                return false;
            }

            return true;
        }

        private void RemoveLevel_Executed()
        {
            //Continue with action, default to true for unit tests
            Boolean continueWithAction = true;
            ConsumerDecisionTreeLevel level = this.CurrentCdt.FetchAllLevels().FirstOrDefault(p => p.Id.Equals(this.SelectedLevelId));
            if (this.AttachedControl != null)
            {
                Boolean hasChildLevel = level.ChildLevel != null;
                Boolean hasChildNodes = this.CurrentCdt.FetchAllNodes().Any(n => n.ConsumerDecisionTreeLevelId == level.Id);
                if (hasChildLevel || hasChildNodes)
                {
                    continueWithAction = LocalHelper.ContinueWithActionPrompt(
                        Message.CdtMaintenance_RemoveLevel, 
                        level.Name, 
                        Message.CdtMaintenance_RemoveLevel_ConfirmationDesc);
                }

                //Ensure level is reset
                this.SelectedLevelId = this.CurrentCdt.RootLevel.Id;
            }

            if (continueWithAction)
            {

                base.ShowWaitCursor(true);

                if (level != null)
                {
                    this.CurrentCdt.RemoveLevel(level, true);
                }

                Boolean selectedNodesCleared = false;
                if (this.SelectedNodes.Count > 0)
                {
                    //ensure the selected list is clear
                    this.SelectedNodes.Clear();
                    selectedNodesCleared = true;
                }

                if (this.AttachedControl != null)
                {
                    if (!selectedNodesCleared)
                    {
                        //Manually call UpdateLevels if the selected nodes collection was not changed
                        this.AttachedControl.UpdateLevels();
                    }
                    this.AttachedControl.OnLevelBlockSelectionChanged();
                }

                //Reset available products
                ResetAvailableProducts();

                //Update node product properties
                UpdateNodeProductProperties();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.ProductUniverseMaintenance_AddSelectedProducts,
                        Icon = ImageResources.ProductUniverseMaintenance_AddSelectedProducts
                    };
                    base.ViewModelCommands.Add(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes == null)
            {
                this.AddSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have one node selected
            if (this.SelectedNodes.Count != 1)
            {
                this.AddSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Selected node must be a leaf
            if (this.SelectedNodes.First().ChildList.Count > 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must have products selected
            if (this.SelectedUnassignedProducts.Count == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.ProductUniverseMaintenance_AddSelectedProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void AddSelectedProducts_Executed()
        {
            //Return if the user doesn't want to convert to a manual cdt
            //if (this.AttachedControl != null &&
            //    !ContinueAsManual())
            //{
            //    return;
            //}

            List<ConsumerDecisionTreeNodeProduct> newProducts = new List<ConsumerDecisionTreeNodeProduct>();
            foreach (ProductInfo product in this.SelectedUnassignedProducts)
            {
                newProducts.Add(ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(product.Id));
            }

            this.SelectedNodes.First().Products.AddRange(newProducts);
            UpdateSelectedNodeProducts();

            //Remove the products from the unassigned range
            _unassignedProducts.RemoveRange(this.SelectedUnassignedProducts);
            this.SelectedUnassignedProducts.Clear();

            RefreshProductSearchResults();
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.ProductUniverseMaintenance_RemoveSelectedProducts,
                        Icon = ImageResources.ProductUniverseMaintenance_RemoveSelectedProducts
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        private Boolean RemoveSelectedProducts_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes == null)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have one node selected
            if (this.SelectedNodes.Count != 1)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Selected node must be a leaf
            if (this.SelectedNodes.First().ChildList.Count > 0)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must have products selected
            if (this.SelectedAssignedProducts.Count == 0)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = Message.ProductUniverseMaintenance_RemoveSelectedProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            //Return if the user doesn't want to convert to a manual cdt
            //if (this.AttachedControl != null &&
            //    !ContinueAsManual())
            //{
            //    return;
            //}

            List<ConsumerDecisionTreeNodeProduct> removedProducts = new List<ConsumerDecisionTreeNodeProduct>();
            foreach (ProductInfo product in this.SelectedAssignedProducts)
            {
                ConsumerDecisionTreeNodeProduct foundProduct = this.SelectedNodes.First().Products.FirstOrDefault(p => p.ProductId.Equals(product.Id));
                if (foundProduct != null)
                {
                    removedProducts.Add(foundProduct);
                }
            }

            //Add the products to the unassigned range
            _unassignedProducts.AddRange(this.SelectedAssignedProducts);

            //Remove the products from the node
            this.SelectedNodes.First().Products.RemoveList(removedProducts);
            UpdateSelectedNodeProducts();
        }

        #endregion

        #region AddAllProductsCommand

        private RelayCommand _addAllProductsCommand;

        public RelayCommand AddAllProductsCommand
        {
            get
            {
                if (_addAllProductsCommand == null)
                {
                    _addAllProductsCommand = new RelayCommand(
                        p => AddAllProducts_Executed(),
                        p => AddAllProducts_CanExecute())
                    {
                        FriendlyName = Message.ProductUniverseMaintenance_AddAllProducts,
                        Icon = ImageResources.ProductUniverseMaintenance_AddAllProducts
                    };
                    base.ViewModelCommands.Add(_addAllProductsCommand);
                }
                return _addAllProductsCommand;
            }
        }

        private Boolean AddAllProducts_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes == null)
            {
                this.AddAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have one node selected
            if (this.SelectedNodes.Count != 1)
            {
                this.AddAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Selected node must be a leaf
            if (this.SelectedNodes.First().ChildList.Count > 0)
            {
                this.AddAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must be products available to add
            if (this.UnassignedProducts.Count == 0)
            {
                this.AddAllProductsCommand.DisabledReason = Message.ProductUniverseMaintenance_AddAllProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void AddAllProducts_Executed()
        {
            //Return if the user doesn't want to convert to a manual cdt
            //if (this.AttachedControl != null &&
            //    !ContinueAsManual())
            //{
            //    return;
            //}

            List<ConsumerDecisionTreeNodeProduct> newProducts = new List<ConsumerDecisionTreeNodeProduct>();
            foreach (ProductInfo product in this.UnassignedProducts)
            {
                newProducts.Add(ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(product.Id));
            }

            this.SelectedNodes.First().Products.AddRange(newProducts);
            UpdateSelectedNodeProducts();

            //Remove the products from the unassigned range
            _unassignedProducts.Clear();
            this.SelectedUnassignedProducts.Clear();

            RefreshProductSearchResults();
        }

        #endregion

        #region RemoveAllProductsCommand

        private RelayCommand _removeAllProductsCommand;

        public RelayCommand RemoveAllProductsCommand
        {
            get
            {
                if (_removeAllProductsCommand == null)
                {
                    _removeAllProductsCommand = new RelayCommand(
                        p => RemoveAllProducts_Executed(),
                        p => RemoveAllProducts_CanExecute())
                    {
                        FriendlyName = Message.ProductUniverseMaintenance_RemoveAllProducts,
                        Icon = ImageResources.ProductUniverseMaintenance_RemoveAllProducts
                    };
                    base.ViewModelCommands.Add(_removeAllProductsCommand);
                }
                return _removeAllProductsCommand;
            }
        }

        private Boolean RemoveAllProducts_CanExecute()
        {
            //must have a node selected
            if (this.SelectedNodes == null)
            {
                this.RemoveAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have one node selected
            if (this.SelectedNodes.Count != 1)
            {
                this.RemoveAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Selected node must be a leaf
            if (this.SelectedNodes.First().ChildList.Count > 0)
            {
                this.RemoveAllProductsCommand.DisabledReason = String.Empty;
                return false;
            }

            //Must be products available to remove
            if (this.SelectedNodeProducts.Count == 0)
            {
                this.RemoveAllProductsCommand.DisabledReason = Message.ProductUniverseMaintenance_RemoveAllProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void RemoveAllProducts_Executed()
        {
            //Return if the user doesn't want to convert to a manual cdt
            //if (this.AttachedControl != null &&
            //    !ContinueAsManual())
            //{
            //    return;
            //}

            List<ConsumerDecisionTreeNodeProduct> removedProducts = new List<ConsumerDecisionTreeNodeProduct>();
            foreach (ProductInfo product in this.SelectedNodeProducts)
            {
                ConsumerDecisionTreeNodeProduct foundProduct = this.SelectedNodes.First().Products.FirstOrDefault(p => p.ProductId.Equals(product.Id));
                if (foundProduct != null)
                {
                    removedProducts.Add(foundProduct);
                }
            }

            //Add the products to the unassigned range
            _unassignedProducts.AddRange(this.SelectedNodeProducts);

            //Remove the products from the node
            this.SelectedNodes.First().Products.Clear();
            UpdateSelectedNodeProducts();
        }

        #endregion

        #region MoreSplitPropertiesCommand

        private RelayCommand _moreSplitPropertiesCommand;

        public RelayCommand MoreSplitPropertiesCommand
        {
            get
            {
                if (_moreSplitPropertiesCommand == null)
                {
                    _moreSplitPropertiesCommand = new RelayCommand(
                        p => MoreSplitProperties_Executed(),
                        p => MoreSplitProperties_CanExecute())
                    {
                        FriendlyName = Message.Modelling_SidePanelMore
                    };
                    base.ViewModelCommands.Add(_moreSplitPropertiesCommand);
                }
                return _moreSplitPropertiesCommand;
            }
        }

        private Boolean MoreSplitProperties_CanExecute()
        {
            //selected node must not be null
            if (this.SelectedNodes.Count == 0)
            {
                MoreSplitPropertiesCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must be leaf
            if (this.SelectedNodes.Any(n =>
            {
                if (n != null)
                {
                    return (n.ChildList.Count != 0);
                }
                return false;
            }))
            {
                MoreSplitPropertiesCommand.DisabledReason = String.Empty;
                return false;
            }

            //selected node must have products
            if (!this.SelectedNodes.Any(n =>
            {
                if (n != null)
                {
                    return (n.TotalProductCount > 0);
                }
                return false;
            }))
            {
                MoreSplitPropertiesCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void MoreSplitProperties_Executed()
        {
            if (this.AttachedControl == null) return;

            //Display window with more split properties
            GenericSingleItemSelectorWindow dialog = new GenericSingleItemSelectorWindow();
            dialog.ItemSource = Product.EnumerateDisplayableFieldInfos().ToList();
            dialog.SelectionMode = DataGridSelectionMode.Single;

            //create the columnset
            DataGridColumnCollection columnSet = new DataGridColumnCollection();

            columnSet.Add(
                ExtendedDataGrid.CreateReadOnlyTextColumn("Property", "PropertyFriendlyName", HorizontalAlignment.Left));

            dialog.ColumnSet = columnSet;

            //show the dialog
            App.ShowWindow(dialog, this.AttachedControl, true);

            if (dialog.DialogResult == true)
            {
                ObjectFieldInfo property = dialog.SelectedItems.Cast<ObjectFieldInfo>().FirstOrDefault();
                if (this.SplitNodeCommand.CanExecute(property))
                {
                    this.SplitNodeCommand.Execute(property);
                }
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region SelectFinancialGroup

        private RelayCommand _selectFinancialGroupCommand;

        /// <summary>
        /// Opens the financial group selection window.
        /// </summary>
        public RelayCommand SelectFinancialGroupCommand
        {
            get
            {
                if (_selectFinancialGroupCommand == null)
                {
                    _selectFinancialGroupCommand = new RelayCommand(
                        p => SelectFinancialGroup_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_selectFinancialGroupCommand);
                }
                return _selectFinancialGroupCommand;
            }
        }


        private void SelectFinancialGroup_Executed()
        {
            if (this.AttachedControl != null)
            {
                //launch the selection window in single mode.
                MerchGroupSelectionWindow win = new MerchGroupSelectionWindow();
                win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
                App.ShowWindow(win, /*isModal*/true);

                if (win.DialogResult == true)
                {
                    this.SelectedFinancialGroup = win.SelectionResult.First();
                }
            }
        }

        #endregion

        #region SelectProductUniverse

        private RelayCommand _selectProductUniverseCommand;

        public RelayCommand SelectProductUniverseCommand
        {
            get
            {
                if (_selectProductUniverseCommand == null)
                {
                    _selectProductUniverseCommand = new RelayCommand(
                        p => SelectProductUniverse_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    base.ViewModelCommands.Add(_selectProductUniverseCommand);
                }
                return _selectProductUniverseCommand;
            }
        }

        private void SelectProductUniverse_Executed()
        {
            if (this.AttachedControl != null)
            {
                ProductUniverseSelectionWindow productUniverseWindow = new ProductUniverseSelectionWindow();
                //productUniverseWindow.SelectionMode = DataGridSelectionMode.Single;

                App.ShowWindow(productUniverseWindow, this.AttachedControl, true);

                this.SelectedProductUniverse = productUniverseWindow.SelectedProductUniverse;
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to the return of results from the product search.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
        {
            //mark as finished searching
            this.IsProcessingProductSearch = false;
            _unassignedProducts.Clear();

            //check if another search is queued
            if (_isProductSearchQueued)
            {
                RefreshProductSearchResults();
            }
            else
            {
                _unassignedProducts.AddRange(e.NewModel);
            }

            ResetAvailableProducts();
        }

        /// <summary>
        /// Responds to a change of the current cdt
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnCurrentCdtChanged(ConsumerDecisionTree oldValue, ConsumerDecisionTree newValue)
        {
            this.SelectedNodes.Clear();

            ProductGroupInfo newSelectedProductGroup = null;

            if (newValue != null)
            {
                if (newValue.ProductGroupId.HasValue)
                {
                    newSelectedProductGroup = ProductGroupInfo.FetchById(newValue.ProductGroupId.Value);
                }

                this.SelectedLevelId = newValue.RootLevel.Id;
                this.RootNode = newValue.RootNode;

                ResetAvailableProducts();
            }
            else
            {
                this.RootNode = null;
            }

            //update the selected product group
            _copyProductGroupProductsToNode = false;
            this.SelectedProductGroup = newSelectedProductGroup;
            _copyProductGroupProductsToNode = true;
        }

        /// <summary>
        /// Method to handle the root node changing
        /// </summary>
        /// <param name="newValue"></param>
        private void OnRootNodeChanged(ConsumerDecisionTreeNode newValue)
        {
            ResetAvailableProducts();

            if (newValue != null)
            {
                this.SelectedNodes.Add(newValue);
            }
        }

        /// <summary>
        /// Responds to changes in the selected nodes collection
        /// </summary>
        private void SelectedNodes_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateSelectedNodeProducts();
        }

        /// <summary>
        /// Responds to a change of assigned product groups
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedProductGroupChanged(ProductGroupInfo newValue)
        {
            _availableProductGroupProductIds.Clear();

            if (newValue != null)
            {
                //Assign this product group's Id to the current CDT
                this.CurrentCdt.ProductGroupId = newValue.Id;

                if (_copyProductGroupProductsToNode)
                {
                    base.ShowWaitCursor(true);

                    //blarg
                    //CopyProductGroupProductsToNodes();

                    ResetAvailableProducts();

                    base.ShowWaitCursor(false);
                }
            }
        }

        /// <summary>
        /// Responds to a change of product search criteria
        /// </summary>
        private void OnProductSearchCriteriaChanged()
        {
            RefreshProductSearchResults();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //CDT premissions
            //create
            _userHasCDTCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(ConsumerDecisionTree));

            //fetch
            _userHasCDTFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(ConsumerDecisionTree));

            //edit
            _userHasCDTEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(ConsumerDecisionTree));

            //delete
            _userHasCDTDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(ConsumerDecisionTree));

            //product group
            _userHasProductGroupFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(ProductGroup));
        }

        /// <summary>
        /// Returns a list of cdts that match the given criteria
        /// </summary>
        /// <param name="codeOrNameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<ConsumerDecisionTreeInfo> GetMatchingCdts(String nameCriteria)
        {
            return this.AvailableCdts.Where(
                p => p.Name.ToUpperInvariant().Contains(nameCriteria.ToUpperInvariant()));
        }

        /// <summary>
        /// Updates the SelectedNodeProducts collection
        /// </summary>
        private void UpdateSelectedNodeProducts()
        {
            //clear any existing items
            if (_selectedNodeProducts.Count > 0)
            {
                _selectedNodeProducts.Clear();
                _selectedAssignedProducts.Clear();
            }

            if (this.SelectedNodes.Count > 0)
            {
                List<ConsumerDecisionTreeNodeProduct> prodsToAdd = new List<ConsumerDecisionTreeNodeProduct>();
                foreach (ConsumerDecisionTreeNode node in this.SelectedNodes)
                {
                    if (node != null)
                    {
                        prodsToAdd.AddRange(node.FetchAllChildConsumerDecisionTreeNodeProducts());
                    }
                }

                _selectedNodeProducts.AddRange(ProductInfoList.FetchByEntityIdProductIds(App.ViewState.EntityId, prodsToAdd.Select(p => p.ProductId).Distinct()));
            }

            //RefreshProductSearchResults();
        }

        /// <summary>
        /// Assigns the products in the linked product group to this CDT
        /// </summary>
        private void CopyProductGroupProductsToNodes()
        {
            ConsumerDecisionTree cdt = this.CurrentCdt;

            cdt.RootLevel.ChildLevel = null;
            cdt.RootNode.ChildList.Clear();
            cdt.RootNode.Products.Clear();

            foreach (Int32 productId in _availableProductGroupProductIds)
            {
                if (!cdt.RootNode.Products.Select(p => p.ProductId).Contains(productId))
                {
                    cdt.RootNode.Products.Add(ConsumerDecisionTreeNodeProduct.NewConsumerDecisionTreeNodeProduct(productId));
                }
            }

            UpdateSelectedNodeProducts();
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.CurrentCdt, this.SaveCommand);
            }
            return continueExecute;
        }

        /// <summary>
        /// Shows a confirmation message requestings user ok to convert the current
        /// CDT to manual
        /// </summary>
        /// <returns></returns>
        public Boolean ContinueAsManual()
        {
            if (this.CurrentCdt.Type == ConsumerDecisionTreeType.Automatic)
            {
                ModalMessageResult continueManual = ModalMessageResult.Button1;

                ModalMessage dialog = new ModalMessage();
                dialog.MessageIcon = ImageResources.Warning_32;
                dialog.Header = Message.CdtMaintenance_AutoWarning_Header;

                dialog.ButtonCount = 2;
                dialog.Button1Content = Message.Generic_Continue;
                dialog.Button2Content = Message.Generic_Cancel;

                dialog.DefaultButton = ModalMessageButton.Button1;
                dialog.CancelButton = ModalMessageButton.Button2;

                dialog.Description = Message.CdtMaintenance_AutoWarning;

                dialog.Owner = this.AttachedControl;
                dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                App.ShowWindow(dialog, /*IsModal*/true);
                continueManual = dialog.Result;

                //process the user input
                switch (continueManual)
                {
                    case ModalMessageResult.Button1:
                        this.CurrentCdt.Type = ConsumerDecisionTreeType.Manual;
                        OnPropertyChanged(CurrentCdtProperty);
                        return true;
                    case ModalMessageResult.Button2:
                    case ModalMessageResult.CrossClicked:
                    default:
                        return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Shows the user a message box requesting confirmation to recreate the CDT
        /// </summary>
        /// <returns></returns>
        public Boolean ContinueWithRecreate()
        {
            if (this.CurrentCdt.RootNode.ChildList.Count > 0)
            {
                ModalMessage dialog = new ModalMessage();
                dialog.MessageIcon = ImageResources.Warning_32;

                dialog.Header = Message.CdtMaintenance_ContinueWithRecreate_Header;

                dialog.ButtonCount = 2;
                dialog.Button1Content = Message.Generic_Continue;
                dialog.Button2Content = Message.Generic_Cancel;

                dialog.DefaultButton = ModalMessageButton.Button1;
                dialog.CancelButton = ModalMessageButton.Button2;

                dialog.Description = Message.CdtMaintenance_ContinueWithRecreate_Description;

                dialog.Owner = this.AttachedControl;
                dialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                App.ShowWindow(dialog, this.AttachedControl, true);

                if (dialog.Result == ModalMessageResult.Button1)
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Corrects the list of available products
        /// </summary>
        private void ResetAvailableProducts()
        {
            if (this.CurrentCdt != null)
            {
                //Get the assigned and unassigned product ids
                IEnumerable<Int32> currentAssignedProductIds = this.CurrentCdt.GetAssignedProductIds();
                IEnumerable<Int32> currentUnassignedProductIds = _unassignedProducts.Select(p => p.Id);

                //Remove the assigned products from the unassigned collection
                IEnumerable<Int32> productIdsToRemove = currentAssignedProductIds.Intersect(currentUnassignedProductIds);

                IEnumerable<ProductInfo> productsToRemove = _unassignedProducts.Where(p => productIdsToRemove.Contains(p.Id)).ToList();
                _unassignedProducts.RemoveRange(productsToRemove);

                if (this.SelectedProductGroup != null)
                {
                    currentAssignedProductIds = _availableProductGroupProductIds.Except(currentAssignedProductIds.Union(currentUnassignedProductIds).Distinct());
                    _unassignedProducts.AddRange(ProductInfoList.FetchByEntityIdProductIds(App.ViewState.EntityId, currentAssignedProductIds));
                }
            }
        }

        /// <summary>
        /// Triggers an update of the range location properties
        /// </summary>
        private void UpdateNodeProductProperties()
        {
            //Enumerate through nodes
            foreach (ConsumerDecisionTreeNode node in this.CurrentCdt.FetchAllNodes())
            {
                node.RaiseTotalProductCountChanged();
            }
        }

        private void RefreshProductSearchResults()
        {
            if (!this.IsProcessingProductSearch)
            {
                _isProductSearchQueued = false;

                this.IsProcessingProductSearch = true;

                switch (this.SearchType)
                {
                    case ProductSearchType.Criteria:
                        if (this.ProductSearchCriteria != null)
                        {
                            _productInfoListView.BeginFetchForCurrentEntityBySearchCriteria(this.ProductSearchCriteria);
                            if (this.AttachedControl == null)
                            {
                                //unit testing and search is async
                                Thread.Sleep(100);
                            }
                        }
                        break;

                    case ProductSearchType.ProductUniverse:
                        _productInfoListView.BeginFetchByProductUniverseId((this.SelectedProductUniverse != null) ? this.SelectedProductUniverse.Id : 0);
                        break;

                    default:
                        throw new NotImplementedException();
                }

                this.IsProcessingProductSearch = false;
            }
            else
            {
                _isProductSearchQueued = true;
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedNodes.BulkCollectionChanged -= SelectedNodes_BulkCollectionChanged;

                    _productInfoListView.ModelChanged -= ProductInfoListView_ModelChanged;

                    this.CurrentCdt = null;

                    _masterCdtListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

}
