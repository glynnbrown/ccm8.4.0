﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf.TreeMap;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using System.ComponentModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// TreeMapBlock for a Cdt maintenance level node
    /// </summary>
    public sealed class CdtMaintenanceLevelNode : TreeMapBlock, IDataErrorInfo
    {
        #region Fields

        private Boolean _isLevelNameValid = false;

        #endregion

        #region Events

        #region LevelChildChanged

        public static readonly RoutedEvent LevelChildChangedEvent =
         EventManager.RegisterRoutedEvent("LevelChildChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CdtMaintenanceLevelNode));

        public event RoutedEventHandler LevelChildChanged
        {
            add { AddHandler(LevelChildChangedEvent, value); }
            remove { RemoveHandler(LevelChildChangedEvent, value); }
        }

        private void RaiseLevelChildChangedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(CdtMaintenanceLevelNode.LevelChildChangedEvent);
            RaiseEvent(newEventArgs);
        }

        #endregion

        #endregion

        #region Properties

        #region Level Context Property

        public static readonly DependencyProperty LevelContextProperty =
            DependencyProperty.Register("LevelContext", typeof(ConsumerDecisionTreeLevel), typeof(CdtMaintenanceLevelNode));
        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public ConsumerDecisionTreeLevel LevelContext
        {
            get { return (ConsumerDecisionTreeLevel)GetValue(LevelContextProperty); }
            set { SetValue(LevelContextProperty, value); }
        }

        #endregion

        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public Boolean IsLevelNameValid
        {
            get { return _isLevelNameValid; }
            set { _isLevelNameValid = value; }
        }

        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public String LevelName
        {
            get { return this.LevelContext.Name; }
            set { this.LevelContext.Name = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static CdtMaintenanceLevelNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(CdtMaintenanceLevelNode), new FrameworkPropertyMetadata(typeof(CdtMaintenanceLevelNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitContext"></param>
        public CdtMaintenanceLevelNode(ConsumerDecisionTreeLevel levelContext)
            : base()
        {
            //Validate 1st time
            this.IsLevelNameValid = CdtMaintenanceLevelNode.ValidateLevelName(levelContext);

            //Set the level context
            this.LevelContext = levelContext;

            //Attach to property changed
            this.LevelContext.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(LevelContext_PropertyChanged);

            //Subscribe to child list collection changed
            this.LevelContext.ChildChanged += new EventHandler<Csla.Core.ChildChangedEventArgs>(LevelContext_ChildChanged);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Event handler for the child level changing
        /// </summary>
        private void LevelContext_ChildChanged(object sender, Csla.Core.ChildChangedEventArgs e)
        {
            RaiseLevelChildChangedEvent();
        }

        /// <summary>
        /// Property changed handler for the cdt level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LevelContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ConsumerDecisionTreeLevel.NameProperty.Name)
            {
                ConsumerDecisionTreeLevel level = sender as ConsumerDecisionTreeLevel;
                if (level != null)
                {
                    this.IsLevelNameValid = CdtMaintenanceLevelNode.ValidateLevelName(level);
                }
            }
        }

        /// <summary>
        /// Static method to validate the level name
        /// </summary>
        /// <param name="level"></param>
        private static Boolean ValidateLevelName(ConsumerDecisionTreeLevel level)
        {
            if (level != null)
            {
                //Fetch all levels in descending order
                IEnumerable<ConsumerDecisionTreeLevel> currentRangeModelLevels = level.ParentConsumerDecisionTree.FetchAllLevels();

                //Ensure name is unique
                return !(currentRangeModelLevels.Where(p => p.Name.Equals(level.Name)).Count() > 1);
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region IDataErrorInfo

        String IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        String IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;
                List<String> siblingNames = new List<String>();
                if (columnName == "LevelName")
                {
                    if (!this.IsLevelNameValid)
                    {
                        result = Message.CdtMaintenance_DuplicateLevelName;
                    }
                }

                return result; // return result
            }
        }
        #endregion

        #region IDisposable

        private Boolean _isDisposed;

        public void Dispose()
        {
            Dispose(true);
        }
        private void Dispose(Boolean isDisposing)
        {
            if (!_isDisposed)
            {
                if (isDisposing)
                {
                    this.LevelContext.PropertyChanged -= LevelContext_PropertyChanged;
                    this.LevelContext.ChildChanged -= LevelContext_ChildChanged;
                    this.LevelContext = null;
                }
                _isDisposed = true;
            }
        }

        #endregion
    }
}
