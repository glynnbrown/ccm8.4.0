﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Collections;
using System.Windows.Controls.DataVisualization.Charting;
using System.Reflection;
using Csla;
using System.Collections.ObjectModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Interaction logic for CdtMaintenanceSplitByResultsWindow.xaml
    /// </summary>
    public partial class CdtMaintenanceSplitByResultsWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(CdtMaintenanceSplitByResultsViewModel), typeof(CdtMaintenanceSplitByResultsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the viewmodel controller for this screen
        /// </summary>
        public CdtMaintenanceSplitByResultsViewModel ViewModel
        {
            get { return (CdtMaintenanceSplitByResultsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceSplitByResultsWindow senderControl = (CdtMaintenanceSplitByResultsWindow)obj;

            if (e.OldValue != null)
            {
                CdtMaintenanceSplitByResultsViewModel oldModel = (CdtMaintenanceSplitByResultsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                CdtMaintenanceSplitByResultsViewModel newModel = (CdtMaintenanceSplitByResultsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public CdtMaintenanceSplitByResultsWindow(ConsumerDecisionTree currentCdt, IEnumerable<Product> availableProducts, IEnumerable<ObjectFieldInfo> splitByProperties, List<ConsumerDecisionTreeNode> nullValueNodes)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            //Construct view model
            this.ViewModel = new CdtMaintenanceSplitByResultsViewModel(currentCdt, availableProducts, splitByProperties, nullValueNodes);

            InitializeComponent();

            this.Loaded += new RoutedEventHandler(CdtMaintenanceSplitByResultsWindow_Loaded);
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //load the columnset
            LoadColumnSet();
        }

        /// <summary>
        /// On loaded event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CdtMaintenanceSplitByResultsWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= CdtMaintenanceSplitByResultsWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the columns set from the viewmodel column definitions
        /// </summary>
        private void LoadColumnSet()
        {
            //Clear columns
            this.xScenarioLocationGrid.Columns.Clear();

            DataGridExtendedTextColumn codeCol = ExtendedDataGrid.CreateReadOnlyTextColumn(Product.GtinProperty.FriendlyName, String.Format("{0}.{1}", SplitByProductRow.ProductProperty.Path, Product.GtinProperty.Name), System.Windows.HorizontalAlignment.Left);
            codeCol.Visibility = Visibility.Visible;
            xScenarioLocationGrid.Columns.Add(codeCol);

            DataGridExtendedTextColumn nameCol = ExtendedDataGrid.CreateReadOnlyTextColumn(Product.NameProperty.FriendlyName, String.Format("{0}.{1}", SplitByProductRow.ProductProperty.Path, Product.NameProperty.Name), System.Windows.HorizontalAlignment.Left);
            nameCol.Visibility = Visibility.Visible;
            xScenarioLocationGrid.Columns.Add(nameCol);

            DataGridExtendedTextColumn rangeCol = ExtendedDataGrid.CreateReadOnlyTextColumn(Message.CdtMaintenanceSplitByResultsWindow_SelectedNode, SplitByProductRow.SelectedNodeProperty.Path, System.Windows.HorizontalAlignment.Left);
            rangeCol.Visibility = Visibility.Visible;
            xScenarioLocationGrid.Columns.Add(rangeCol);

            DataGridExtendedTextColumn levelCol = ExtendedDataGrid.CreateReadOnlyTextColumn(Message.CdtMaintenanceSplitByResultsWindow_NodeLevel, SplitByProductRow.ParentLevelProperty.Path, System.Windows.HorizontalAlignment.Left);
            levelCol.Visibility = Visibility.Visible;
            xScenarioLocationGrid.Columns.Add(levelCol);

            //Create column for each split by property
            foreach (ObjectFieldInfo property in this.ViewModel.SplitByProperties)
            {
                if (property.OwnerType == typeof(Product))
                {
                    DataGridExtendedTextColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(property.PropertyFriendlyName, String.Format("{0}.{1}", SplitByProductRow.ProductProperty.Path, property.PropertyName), System.Windows.HorizontalAlignment.Left);
                    col.ColumnGroupName = property.GroupName;
                    col.Visibility = Visibility.Collapsed;
                    xScenarioLocationGrid.Columns.Add(col);
                }
                //else if (property.OwnerType == typeof(Product))
                //{
                //    DataGridExtendedTextColumn col = ExtendedDataGrid.CreateReadOnlyTextColumn(property.FriendlyName, String.Format("{0}.{1}.{2}", SplitByProductRow.ProductProperty.Path, Product.AttributeDataProperty.Name, property.Name), System.Windows.HorizontalAlignment.Left);
                //    col.ColumnGroupName = property.GroupName;
                //    col.Visibility = Visibility.Collapsed;
                //    xScenarioLocationGrid.Columns.Add(col);
                //}
            }
        }

        #endregion

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}