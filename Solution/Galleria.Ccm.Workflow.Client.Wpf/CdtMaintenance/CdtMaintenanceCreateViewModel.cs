﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#region Version History : CCM830
// V8-31559 : A.Kuszyk
//  Modified to use GenericSingleItemSelectorWindow.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    public enum CdtWizardType
    {
        Create,
        Merge
    }

    public enum CdtWizardStep
    {
        Selection = 0,
        CdtCreation = 1,
        ValidationCdtProducts = 2,
        ValidationUniverseProducts = 3
    }

    public sealed class CdtMaintenanceCreateViewModel : ViewModelAttachedControlObject<CdtMaintenanceCreateOrganiser>
    {
        #region Fields

        private readonly ConsumerDecisionTreeInfoListViewModel _masterConsumerDecisionTreeInfoList = new ConsumerDecisionTreeInfoListViewModel();

        private ConsumerDecisionTree _currentTree;

        private CdtWizardType _wizardType = CdtWizardType.Create;
        //private ReadOnlyBulkObservableCollection<CdtWizardType> _availableWizardTypesRO;

        private CdtWizardStep _wizardStep = CdtWizardStep.Selection;

        private Boolean _isPreviousVisible = false;
        private Boolean _isNextVisible = true;
        private Boolean _isFinishVisible = false;
        private Boolean _isYesVisible = false;
        private Boolean _isNoVisible = false;
        private Boolean _isCancelVisible = true;

        private Boolean _displayDuplicateScreen = false;
        private Boolean _displayMissingScreen = false;
        private Boolean _hasMergeProblem = false;
        private Boolean _isComplete = false;
        private String _progressText = String.Empty;
        private BackgroundWorker _treeCreationWorker;
        private BackgroundWorker _treeMergeWorker;

        private ProductUniverseInfo _selectedProductUniverse;
        private readonly ProductUniverseInfoListViewModel _masterProductUniverseInfoListView = new ProductUniverseInfoListViewModel();

        private BulkObservableCollection<ProductInfo> _missingProducts = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _missingProductsRO;

        private BulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem> _availableAttributes = new BulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem> _availableAttributesRO;
        private BulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem> _takenAttributes = new BulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem> _takenAttributesRO;
        private List<ConsumerDecisionTreePropertyComboBoxItem> _splitAttributes = new List<ConsumerDecisionTreePropertyComboBoxItem>();
        private ConsumerDecisionTreePropertyComboBoxItem _selectedAttribute;
        private ConsumerDecisionTreePropertyComboBoxItem _selectedTakenAttribute;

        private BulkObservableCollection<ConsumerDecisionTreeInfo> _takenConsumerDecisionTrees = new BulkObservableCollection<ConsumerDecisionTreeInfo>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreeInfo> _takeConsumerDecisionTreesRO;
        private ConsumerDecisionTreeInfo _selectedTakenConsumerDecisionTree;

        private BulkObservableCollection<ConsumerDecisionTreeNodeProductItem> _duplicateProductGridItems = new BulkObservableCollection<ConsumerDecisionTreeNodeProductItem>();

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CurrentTreeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.CurrentTree);

        public static readonly PropertyPath WizardTypeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.WizardType);

        public static readonly PropertyPath WizardStepProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.WizardStep);

        public static readonly PropertyPath IsPreviousVisibleProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.IsPreviousVisible);
        public static readonly PropertyPath IsNextVisibleProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.IsNextVisible);
        public static readonly PropertyPath IsFinishVisibleProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.IsFinishVisible);
        public static readonly PropertyPath IsYesVisibleProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.IsYesVisible);
        public static readonly PropertyPath IsNoVisibleProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.IsNoVisible);
        public static readonly PropertyPath IsCancelVisibleProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.IsCancelVisible);

        public static readonly PropertyPath IsCompleteProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.IsComplete);
        public static readonly PropertyPath ProgressTextProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.ProgressText);

        public static readonly PropertyPath SelectedProductUniverseProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.SelectedProductUniverse);
        public static readonly PropertyPath MasterProductUniverseInfoListProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.MasterProductUniverseInfoList);

        public static readonly PropertyPath MissingProductsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.MissingProducts);

        public static readonly PropertyPath AvailableAttributesProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.AvailableAttributes);
        public static readonly PropertyPath TakenAttributesProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.TakenAttributes);
        public static readonly PropertyPath SelectedAttributeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.SelectedAttribute);
        public static readonly PropertyPath SelectedTakenAttributeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.SelectedTakenAttribute);

        public static readonly PropertyPath TakenConsumerDecisionTreesProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.TakenConsumerDecisionTrees);
        public static readonly PropertyPath SelectedTakenConsumerDecisionTreeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.SelectedTakenConsumerDecisionTree);

        public static readonly PropertyPath DuplicateProductGridItemsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.DuplicateProductGridItems);
        public static readonly PropertyPath HasMergeProblemProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.HasMergeProblem);


        //commands
        public static readonly PropertyPath AddAttributeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.AddAttributeCommand);
        public static readonly PropertyPath RemoveAttributeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.RemoveAttributeCommand);
        public static readonly PropertyPath AddConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.AddConsumerDecisionTreeCommand);
        public static readonly PropertyPath RemoveConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.RemoveConsumerDecisionTreeCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.PreviousCommand);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.NextCommand);
        public static readonly PropertyPath FinishCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.FinishCommand);
        public static readonly PropertyPath YesCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.YesCommand);
        public static readonly PropertyPath NoCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.NoCommand);
        public static readonly PropertyPath SelectProductUniverseCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceCreateViewModel>(c => c.SelectProductUniverseCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the selected cdt
        /// </summary>
        public ConsumerDecisionTree CurrentTree
        {
            get { return _currentTree; }
            private set
            {
                _currentTree = value;
                OnPropertyChanged(CurrentTreeProperty);
            }
        }

        /// <summary>
        /// Returns the current type of CDT being created, merge or creation
        /// </summary>
        public CdtWizardType WizardType
        {
            get { return _wizardType; }
            set
            {
                _wizardType = value;
                OnPropertyChanged(WizardTypeProperty);
            }
        }

        /// <summary>
        /// Returns the current step for the wizard
        /// </summary>
        public CdtWizardStep WizardStep
        {
            get { return _wizardStep; }
            private set
            {
                _wizardStep = value;
                OnPropertyChanged(WizardStepProperty);
                OnWizardStepChanged();
            }
        }

        /// <summary>
        /// Returns true if the previous button should be visible
        /// </summary>
        public Boolean IsPreviousVisible
        {
            get { return _isPreviousVisible; }
            private set
            {
                _isPreviousVisible = value;
                OnPropertyChanged(IsPreviousVisibleProperty);
            }
        }

        /// <summary>
        /// Returns true if the next button should be visible
        /// </summary>
        public Boolean IsNextVisible
        {
            get { return _isNextVisible; }
            private set
            {
                _isNextVisible = value;
                OnPropertyChanged(IsNextVisibleProperty);
            }
        }

        /// <summary>
        /// Returns true if the finish button should be visible
        /// </summary>
        public Boolean IsFinishVisible
        {
            get { return _isFinishVisible; }
            private set
            {
                _isFinishVisible = value;
                OnPropertyChanged(IsFinishVisibleProperty);
            }
        }

        /// <summary>
        /// Returns true if the yes button should be visible
        /// </summary>
        public Boolean IsYesVisible
        {
            get { return _isYesVisible; }
            private set
            {
                _isYesVisible = value;
                OnPropertyChanged(IsYesVisibleProperty);
            }
        }

        /// <summary>
        /// Returns true if the no button should be visible
        /// </summary>
        public Boolean IsNoVisible
        {
            get { return _isNoVisible; }
            private set
            {
                _isNoVisible = value;
                OnPropertyChanged(IsNoVisibleProperty);
            }
        }

        /// <summary>
        /// Returns true if the cancel button should be visible
        /// </summary>
        public Boolean IsCancelVisible
        {
            get { return _isCancelVisible; }
            private set
            {
                _isCancelVisible = value;
                OnPropertyChanged(IsCancelVisibleProperty);
            }
        }

        /// <summary>
        /// Returns true if the cdt creation is complete
        /// </summary>
        public Boolean IsComplete
        {
            get { return _isComplete; }
            private set
            {
                _isComplete = value;
                OnPropertyChanged(IsCompleteProperty);
            }
        }

        /// <summary>
        /// Returns the text to display for the progress bar
        /// </summary>
        public String ProgressText
        {
            get { return _progressText; }
            private set
            {
                _progressText = value;
                OnPropertyChanged(ProgressTextProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected product universe
        /// </summary>
        public ProductUniverseInfo SelectedProductUniverse
        {
            get { return _selectedProductUniverse; }
            set
            {
                ProductUniverseInfo oldValue = _selectedProductUniverse;
                _selectedProductUniverse = value;
                OnSelectedProductUniverseChanged(_selectedProductUniverse, oldValue);
                OnPropertyChanged(SelectedProductUniverseProperty);
            }
        }

        /// <summary>
        /// Gets the available product universes for selection
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductUniverseInfo> MasterProductUniverseInfoList
        {
            get { return _masterProductUniverseInfoListView.BindingView; }
        }

        /// <summary>
        /// Returns the collection of products in the CDT that are not contained in the selected product universe
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> MissingProducts
        {
            get
            {
                if (_missingProductsRO == null)
                {
                    _missingProductsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_missingProducts);
                }
                return _missingProductsRO;
            }
        }

        /// <summary>
        /// Gets the attributes available for auto creation
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem> AvailableAttributes
        {
            get
            {
                if (_availableAttributesRO == null)
                {
                    _availableAttributesRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem>(_availableAttributes);
                }
                return _availableAttributesRO;
            }
        }

        /// <summary>
        /// Gets the attributes taken for auto creation
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem> TakenAttributes
        {
            get
            {
                if (_takenAttributesRO == null)
                {
                    _takenAttributesRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreePropertyComboBoxItem>(_takenAttributes);
                }
                return _takenAttributesRO;
            }
        }

        /// <summary>
        /// Get/Set the current selected attribute for auto creation
        /// </summary>
        public ConsumerDecisionTreePropertyComboBoxItem SelectedAttribute
        {
            get { return _selectedAttribute; }
            set
            {
                _selectedAttribute = value;
                OnPropertyChanged(SelectedAttributeProperty);
            }
        }

        /// <summary>
        /// Get/Set the current selected attribute in the taken attributes grid
        /// </summary>
        public ConsumerDecisionTreePropertyComboBoxItem SelectedTakenAttribute
        {
            get { return _selectedTakenAttribute; }
            set
            {
                _selectedTakenAttribute = value;
                OnPropertyChanged(SelectedTakenAttributeProperty);
            }
        }

        /// <summary>
        /// Returns the collection of taken consumer decision tree to merge
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeInfo> TakenConsumerDecisionTrees
        {
            get
            {
                if (_takeConsumerDecisionTreesRO == null)
                {
                    _takeConsumerDecisionTreesRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreeInfo>(_takenConsumerDecisionTrees);
                }
                return _takeConsumerDecisionTreesRO;
            }
        }

        /// <summary>
        /// gets/Sets the selected taken consumer decision tree
        /// </summary>
        public ConsumerDecisionTreeInfo SelectedTakenConsumerDecisionTree
        {
            get { return _selectedTakenConsumerDecisionTree; }
            set
            {
                _selectedTakenConsumerDecisionTree = value;
                OnPropertyChanged(SelectedTakenConsumerDecisionTreeProperty);
            }
        }

        public BulkObservableCollection<ConsumerDecisionTreeNodeProductItem> DuplicateProductGridItems
        {
            get { return _duplicateProductGridItems; }
        }

        /// <summary>
        /// Returns true if the tree merge background worker is running, false otherwise.
        /// </summary>
        public Boolean HasMergeProblem
        {
            get { return _hasMergeProblem; }
            set
            {
                _hasMergeProblem = value;
                OnPropertyChanged(HasMergeProblemProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor - edits the given cdt
        /// </summary>
        public CdtMaintenanceCreateViewModel(ConsumerDecisionTree existingCdt, Int32? productUniverseId)
            : this(existingCdt, productUniverseId, null)
        { }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        public CdtMaintenanceCreateViewModel(
            ConsumerDecisionTree existingCdt,
            Int32? productUniverseId,
            CdtWizardType? wizardType)
        {
            //set current cdt
            this.CurrentTree = existingCdt;

            //master product universe info list
            _masterProductUniverseInfoListView.FetchAllForEntity();

            //cdt
            _masterConsumerDecisionTreeInfoList.FetchForCurrentEntity();

            //Select first product universe
            if (productUniverseId.HasValue)
            {
                this.SelectedProductUniverse = this.MasterProductUniverseInfoList.FirstOrDefault(p => p.Id == productUniverseId);

                //set attribute lists
                PopulateAvailableProperties(this.SelectedProductUniverse.Id);
            }

            //If a wizard type was provided skip the type selection step
            if (wizardType != null)
            {
                this.WizardType = wizardType.Value;
                this.WizardStep = CdtWizardStep.Selection;
            }
        }

        #endregion

        #region Commands

        #region AddAttributeCommand

        private RelayCommand _addAttributeCommand;
        /// <summary>
        /// Adds the selected attribute to the taken attributes grid
        /// </summary>
        public RelayCommand AddAttributeCommand
        {
            get
            {
                if (_addAttributeCommand == null)
                {
                    _addAttributeCommand = new RelayCommand(p => AddAttribute_Executed(), p => AddAttribute_CanExecute())
                    {
                        FriendlyName = String.Empty,
                        SmallIcon = ImageResources.Add_16
                    };
                    this.ViewModelCommands.Add(_addAttributeCommand);
                }
                return _addAttributeCommand;
            }
        }

        private Boolean AddAttribute_CanExecute()
        {
            //cdt must not be null
            if (this.CurrentTree == null)
            {
                return false;
            }

            //must have an attribute selected
            if (this.SelectedAttribute == null)
            {
                return false;
            }

            return true;
        }

        private void AddAttribute_Executed()
        {
            ConsumerDecisionTreePropertyComboBoxItem attributeToAdd = this.SelectedAttribute;

            //Add selected attribute to taken attributes
            _takenAttributes.Add(attributeToAdd);
            attributeToAdd.Index = _takenAttributes.Count;

            //Remove attribute from available
            _availableAttributes.Remove(attributeToAdd);
            this.SelectedAttribute = _availableAttributes.FirstOrDefault();

            //Set selected taken attribute to the attribute added
            this.SelectedTakenAttribute = _takenAttributes.Last();
        }

        #endregion

        #region RemoveAttributeCommand

        private RelayCommand _removeAttributeCommand;
        /// <summary>
        /// Removes the selected attribute from the taken attributes grid
        /// </summary>
        public RelayCommand RemoveAttributeCommand
        {
            get
            {
                if (_removeAttributeCommand == null)
                {
                    _removeAttributeCommand = new RelayCommand(p => RemoveAttribute_Executed(), p => RemoveAttribute_CanExecute())
                    {
                        FriendlyName = String.Empty,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeAttributeCommand);
                }
                return _removeAttributeCommand;
            }
        }

        private Boolean RemoveAttribute_CanExecute()
        {
            //cdt must not be null
            if (this.CurrentTree == null)
            {
                return false;
            }

            //must have taken attribute selected
            if (this.SelectedTakenAttribute == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveAttribute_Executed()
        {
            ConsumerDecisionTreePropertyComboBoxItem attributeToRemove = this.SelectedTakenAttribute;
            Int32 removedIndex = attributeToRemove.Index.HasValue ? attributeToRemove.Index.Value : 0;

            //Add selected taken attribute back to available
            List<ConsumerDecisionTreePropertyComboBoxItem> orderedAvailableAttributes = _availableAttributes.ToList();
            orderedAvailableAttributes.Add(attributeToRemove);
            orderedAvailableAttributes = orderedAvailableAttributes.OrderBy(s => s.FriendlyName).ToList();

            Int32 insertIndex = orderedAvailableAttributes.IndexOf(attributeToRemove);
            _availableAttributes.Insert(insertIndex, attributeToRemove);
            attributeToRemove.Index = null;

            //Remove selected taken attribute from taken attributes
            _takenAttributes.Remove(attributeToRemove);
            this.SelectedTakenAttribute = _takenAttributes.FirstOrDefault();

            //Correct taken attribute indexes
            foreach (ConsumerDecisionTreePropertyComboBoxItem taken in _takenAttributes)
            {
                if (taken.Index > removedIndex)
                {
                    taken.Index--;
                }
            }

            //select the first available attribute
            this.SelectedAttribute = _availableAttributes.FirstOrDefault();
        }

        #endregion

        #region AddConsumerDecisionTreeCommand

        private RelayCommand _addConsumerDecisionTreeCommand;

        public RelayCommand AddConsumerDecisionTreeCommand
        {
            get
            {
                if (_addConsumerDecisionTreeCommand == null)
                {
                    _addConsumerDecisionTreeCommand = new RelayCommand(
                        p => AddConsumerDecisionTree_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis,
                        Icon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addConsumerDecisionTreeCommand);
                }
                return _addConsumerDecisionTreeCommand;
            }
        }

        private void AddConsumerDecisionTree_Executed()
        {
            if (this.AttachedControl != null)
            {
                ObservableCollection<ConsumerDecisionTreeInfo> availableConsumerDecisionTrees =
                    _masterConsumerDecisionTreeInfoList.Model.Except(_takenConsumerDecisionTrees).ToObservableCollection();

                //Remove the current tree from the collection
                ConsumerDecisionTreeInfo currentTreeInfo = availableConsumerDecisionTrees.FirstOrDefault(p => p.Id == this.CurrentTree.Id);
                if (currentTreeInfo != null)
                {
                    availableConsumerDecisionTrees.Remove(currentTreeInfo);
                }

                //Create the selection dialog
                GenericSingleItemSelectorWindow selectDialog = new GenericSingleItemSelectorWindow();
                selectDialog.ItemSource = availableConsumerDecisionTrees;
                selectDialog.SelectionMode = DataGridSelectionMode.Extended;
                selectDialog.AllowDoubleClickOnExtendedSelection = true;

                App.ShowWindow(selectDialog, this.AttachedControl, true);

                if (selectDialog.DialogResult == true)
                {
                    _takenConsumerDecisionTrees.AddRange(selectDialog.SelectedItems.Cast<ConsumerDecisionTreeInfo>());
                }
            }
        }

        #endregion

        #region RemoveConsumerDecisionTreeCommand

        private RelayCommand<ConsumerDecisionTreeInfo> _removeConsumerDecisionTreeCommand;

        public RelayCommand<ConsumerDecisionTreeInfo> RemoveConsumerDecisionTreeCommand
        {
            get
            {
                if (_removeConsumerDecisionTreeCommand == null)
                {
                    _removeConsumerDecisionTreeCommand = new RelayCommand<ConsumerDecisionTreeInfo>(
                        p => RemoveConsumerDecisionTree_Executed(p),
                        p => RemoveConsumerDecisionTree_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Remove,
                        Icon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_removeConsumerDecisionTreeCommand);
                }
                return _removeConsumerDecisionTreeCommand;
            }
        }

        private Boolean RemoveConsumerDecisionTree_CanExecute(ConsumerDecisionTreeInfo cdtToRemove)
        {
            if (cdtToRemove == null)
            {
                return false;
            }

            return true;
        }

        private void RemoveConsumerDecisionTree_Executed(ConsumerDecisionTreeInfo cdtToRemove)
        {
            _takenConsumerDecisionTrees.Remove(cdtToRemove);
        }

        #endregion

        #region PreviousCommand

        private RelayCommand _previousCommand;

        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        private Boolean Previous_CanExecute()
        {
            //Cannot use previous on the first step
            if (this.WizardStep == CdtWizardStep.Selection)
            {
                return false;
            }

            if (_treeCreationWorker != null &&
                _treeCreationWorker.CancellationPending)
            {
                return false;
            }

            return true;
        }

        private void Previous_Executed()
        {
            if (_treeCreationWorker != null &&
                _treeCreationWorker.IsBusy)
            {
                this.ProgressText = Message.Generic_Cancelling;
                _treeCreationWorker.CancelAsync();
            }
            else if (_treeMergeWorker != null &&
                _treeMergeWorker.IsBusy)
            {
                this.ProgressText = Message.Generic_Cancelling;
                _treeMergeWorker.CancelAsync();
            }
            else
            {
                //Create has no steps to skip
                if (this.WizardType == CdtWizardType.Create)
                {
                    this.WizardStep = (CdtWizardStep)(this.WizardStep - 1);
                }
                else
                {
                    switch (this.WizardStep)
                    {
                        case CdtWizardStep.ValidationUniverseProducts:
                            if (_displayDuplicateScreen)
                            {
                                //Move to duplicate products step if there are duplicates
                                this.WizardStep = CdtWizardStep.ValidationCdtProducts;
                            }
                            else
                            {
                                //Else move to the creation step
                                this.WizardStep = CdtWizardStep.CdtCreation;
                            }
                            break;
                        default:
                            this.WizardStep = (CdtWizardStep)(this.WizardStep - 1);
                            break;
                    }
                }
            }
        }

        #endregion

        #region NextCommand

        private RelayCommand _nextCommand;

        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        private Boolean Next_CanExecute()
        {
            if (this.WizardType == CdtWizardType.Merge)
            {
                switch (this.WizardStep)
                {
                    case CdtWizardStep.Selection:
                        //User must have selected universe
                        if (_selectedProductUniverse == null)
                        {
                            this.NextCommand.DisabledReason = Message.CdtWizard_NextDisabled_RequiredUniverse;
                            return false;
                        }
                        //User must have selected CDTs
                        if (_takenConsumerDecisionTrees.Count <= 1)
                        {
                            this.NextCommand.DisabledReason = Message.CdtWizard_CdtSelection_RequiresCdts;
                            return false;
                        }
                        break;
                    case CdtWizardStep.CdtCreation:
                        //Merging must have finished
                        if (!_isComplete)
                        {
                            this.NextCommand.DisabledReason = String.Empty;
                            return false;
                        }
                        break;
                    case CdtWizardStep.ValidationCdtProducts:
                        //All the duplicate products need to have been assigned to a single node
                        if (_duplicateProductGridItems.Select(p => p.HasBeenAssigned).Contains(false))
                        {
                            this.NextCommand.DisabledReason = Message.CdtWizard_ProductDuplicate_NotAllAssigned;
                            return false;
                        }
                        break;
                }
            }
            else
            {
                switch (this.WizardStep)
                {
                    case CdtWizardStep.Selection:
                        //User must have selected universe
                        if (_selectedProductUniverse == null)
                        {
                            this.NextCommand.DisabledReason = Message.CdtWizard_NextDisabled_RequiredUniverse;
                            return false;
                        }
                        //User must have selected attributes
                        if (_takenAttributes.Count == 0)
                        {
                            this.NextCommand.DisabledReason = Message.CdtWizard_AttributeSelection_RequiresAttribute;
                            return false;
                        }

                        //Tree worker must not be busy
                        if (_treeCreationWorker != null &&
                            _treeCreationWorker.IsBusy)
                        {
                            this.NextCommand.DisabledReason = String.Empty;
                            return false;
                        }

                        break;
                    case CdtWizardStep.CdtCreation:
                        //Final step of wizard uses finish
                        this.NextCommand.DisabledReason = String.Empty;
                        return false;
                }
            }

            return true;
        }

        private void Next_Executed()
        {
            //Create has no steps to skip
            if (this.WizardType == CdtWizardType.Create)
            {
                this.WizardStep = (CdtWizardStep)(this.WizardStep + 1);
            }
            else
            {
                switch (this.WizardStep)
                {
                    case CdtWizardStep.CdtCreation:
                        if (_displayDuplicateScreen)
                        {
                            //Move to duplicate products step if there are duplicates
                            this.WizardStep = CdtWizardStep.ValidationCdtProducts;
                        }
                        else if (_displayMissingScreen)
                        {
                            //Else move to the universe products if required
                            this.WizardStep = CdtWizardStep.ValidationUniverseProducts;
                        }
                        else
                        {
                            //There are no duplicate or missing products so finish
                            this.FinishCommand.Execute();
                        }
                        break;
                    case CdtWizardStep.ValidationCdtProducts:
                        if (_displayMissingScreen)
                        {
                            //If there are missing products show the universe products step
                            this.WizardStep = CdtWizardStep.ValidationUniverseProducts;
                        }
                        else
                        {
                            //There are no missing products so finish
                            this.FinishCommand.Execute();
                        }
                        break;
                    default:
                        this.WizardStep = (CdtWizardStep)(this.WizardStep + 1);
                        break;
                }
            }
        }

        #endregion

        #region FinishCommand

        private RelayCommand _finishCommand;

        public RelayCommand FinishCommand
        {
            get
            {
                if (_finishCommand == null)
                {
                    _finishCommand = new RelayCommand(p => Finish_Executed(), p => Finish_CanExecute())
                    {
                        FriendlyName = Message.Generic_Finish,
                        FriendlyDescription = Message.CdtWizard_Finish_Description
                    };
                    base.ViewModelCommands.Add(_finishCommand);
                }
                return _finishCommand;
            }
        }

        private Boolean Finish_CanExecute()
        {
            //Tree should not be null
            if (this.CurrentTree == null)
            {
                this.FinishCommand.DisabledReason = String.Empty;
                return false;
            }

            if (!this.IsComplete)
            {
                this.FinishCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Finish_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = true;
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region YesCommand

        private RelayCommand _yesCommand;

        public RelayCommand YesCommand
        {
            get
            {
                if (_yesCommand == null)
                {
                    _yesCommand = new RelayCommand(
                        p => Yes_Executed())
                    {
                        FriendlyName = Message.Generic_Yes,
                        FriendlyDescription = Message.CdtWizard_YesCommand_Description
                    };
                    base.ViewModelCommands.Add(_yesCommand);
                }
                return _yesCommand;
            }
        }

        private void Yes_Executed()
        {
            if (this.AttachedControl != null)
            {
                base.ShowWaitCursor(true);

                ProductUniverse productUniverse = ProductUniverse.FetchById(_selectedProductUniverse.Id);
                foreach (ProductInfo product in _missingProducts)
                {
                    productUniverse.Products.Add(product);
                }

                productUniverse = productUniverse.Save();

                base.ShowWaitCursor(false);

                this.AttachedControl.DialogResult = true;
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region NoCommand

        private RelayCommand _noCommand;

        public RelayCommand NoCommand
        {
            get
            {
                if (_noCommand == null)
                {
                    _noCommand = new RelayCommand(
                        p => No_Executed())
                    {
                        FriendlyName = Message.Generic_No,
                        FriendlyDescription = Message.CdtWizard_NoCommand_Description
                    };
                    base.ViewModelCommands.Add(_noCommand);
                }
                return _noCommand;
            }
        }

        private void No_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = true;
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region SelectProductUniverse

        private RelayCommand _selectProductUniverseSelectCommand;

        public RelayCommand SelectProductUniverseCommand
        {
            get
            {
                if (_selectProductUniverseSelectCommand == null)
                {
                    _selectProductUniverseSelectCommand = new RelayCommand(
                        p => SelectProductUniverse_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    ViewModelCommands.Add(_selectProductUniverseSelectCommand);
                }
                return _selectProductUniverseSelectCommand;
            }
        }

        private void SelectProductUniverse_Executed()
        {
            //Display the selection dialog
            if (this.AttachedControl != null)
            {
                ProductUniverseSelectionWindow productUniverseWindow = new ProductUniverseSelectionWindow();
                App.ShowWindow(productUniverseWindow, this.AttachedControl, true);

                if (productUniverseWindow.SelectedProductUniverse != null)
                {
                    this.SelectedProductUniverse = productUniverseWindow.SelectedProductUniverse;
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Updates button visibility and calls appropriate methods for each step
        /// </summary>
        private void OnWizardStepChanged()
        {
            UpdateButtonVisibility();

            switch (this.WizardStep)
            {
                case CdtWizardStep.CdtCreation:
                    //Worker is required so the UI can update while the CDT is created
                    StartCreationWorker();
                    break;
                case CdtWizardStep.ValidationCdtProducts:
                    //Resize the window so that the validation grids fit easier
                    if (this.AttachedControl != null)
                    {
                        this.AttachedControl.Width = 660;
                    }
                    break;
                case CdtWizardStep.ValidationUniverseProducts:
                    //Assign the duplicate products to a single parent node
                    UpdateDuplicateProducts();
                    break;
            }
        }

        #region Create From Properties Worker

        private void CreateWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Dispatcher dispatcher = e.Argument as Dispatcher;

            //get the universe
            this.ProgressText = Message.CdtWizard_FetchingUniverse;
            ProductUniverse currentUniverse = ProductUniverse.FetchById(this.SelectedProductUniverse.Id);

            //Report the progress and check for cancellation
            _treeCreationWorker.ReportProgress(1);
            if (_treeCreationWorker.CancellationPending) { return; }

            //Gets products
            this.ProgressText = Message.CdtWizard_FetchingProducts;
            IEnumerable<Int32> productIds = currentUniverse.Products.Select(p => p.ProductId);
            ProductList products = ProductList.FetchByProductIds(productIds);

            //Add the products that are missing from the root node
            IEnumerable<Int32> productsToAdd = productIds.Except(this.CurrentTree.RootNode.Products.Select(p => p.ProductId)).ToList();
            this.CurrentTree.RootNode.Products.AddRange(productsToAdd);

            //Report the progress and check for cancellation
            _treeCreationWorker.ReportProgress(1);
            if (_treeCreationWorker.CancellationPending) { return; }

            //Get the expected node count and then the progress bar step distance
            if (this.AttachedControl != null)
            {
                this.ProgressText = Message.CdtWizard_Calculating;
                Int32 expectedNodeCount = this.CurrentTree.RootNode.CalculateExpectedNodeCount(_takenAttributes.Select(p => p.SourceProperty), products);

                //Set the expected node count for the progress bar
                dispatcher.Invoke((Action)(() => { this.AttachedControl.ExpectedNodeCount = expectedNodeCount; }));

                //Report the progress and check for cancellation
                _treeCreationWorker.ReportProgress(1);
                if (_treeCreationWorker.CancellationPending) { return; }
            }

            _splitAttributes = _takenAttributes.ToList();

            //Begin the split
            List<ConsumerDecisionTreeNode> nodeList = new List<ConsumerDecisionTreeNode>();
            nodeList.Add(this.CurrentTree.RootNode);
            this.SplitNodesByProperty(_splitAttributes.FirstOrDefault(), nodeList, products, dispatcher);
        }

        private void CreateWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateCreationProgressBar(e.ProgressPercentage);
        }

        private void CreateWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                throw e.Error;
            }

            //If the execution was cancelled go to the previous step
            if (e.Cancelled)
            {
                this.WizardStep = (CdtWizardStep)(this.WizardStep -= 1);
            }
            else
            {
                this.IsComplete = true;
                this.FinishCommand.RaiseCanExecuteChanged();
                this.ProgressText = Message.Generic_Completed;
            }
        }

        #endregion

        #region Merge Worker

        private void MergeWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            Dispatcher dispatcher = e.Argument as Dispatcher;
            Int32 entityId = this.CurrentTree.EntityId;

            //Get the taken CDTs
            this.ProgressText = Message.CdtWizard_FetchingCdts;

            ConsumerDecisionTreeList cdtsToMerge =
                ConsumerDecisionTreeList.FetchByEntityIdConsumerDecisionTreeIds(entityId, _takenConsumerDecisionTrees.Select(p => p.Id));

            //Report the progress and check for cancellation
            _treeMergeWorker.ReportProgress(1);
            if (_treeMergeWorker.CancellationPending) { return; }

            //Calculate expected node count
            Dictionary<Int32, Int32> cdtNodeCounts = new Dictionary<Int32, Int32>();
            if (this.AttachedControl != null)
            {
                this.ProgressText = Message.CdtWizard_Calculating;

                Int32 expectedNodeCount = 0;
                foreach (ConsumerDecisionTree tree in cdtsToMerge)
                {
                    Int32 treeNodesCount = tree.FetchAllNodes().Count;

                    //Add the tree id and node count to the dictionary so that the progress bar can be updated
                    cdtNodeCounts.Add(tree.Id, treeNodesCount);
                    expectedNodeCount += treeNodesCount;
                }

                //Set the expected node count for the progress bar
                dispatcher.Invoke((Action)(() => { this.AttachedControl.ExpectedNodeCount = expectedNodeCount; }));

                //Report the progress and check for cancellation
                _treeMergeWorker.ReportProgress(1);
                if (_treeMergeWorker.CancellationPending) { return; }
            }

            foreach (ConsumerDecisionTree cdtToMerge in cdtsToMerge)
            {
                this.ProgressText = String.Format(CultureInfo.CurrentCulture, Message.CdtWizard_Merging, cdtToMerge.Name);

                //Merge the trees
                dispatcher.Invoke((Action)(() => { this.CurrentTree.MergeTree(cdtToMerge); }));

                //Report the progress and check for cancellation
                dispatcher.Invoke((Action)(() => { UpdateCreationProgressBar(cdtNodeCounts[cdtToMerge.Id]); }));
                //_treeMergeWorker.ReportProgress(cdtNodeCounts[mergeTree.Id]);
                if (_treeMergeWorker.CancellationPending) { return; }
            }

            this.ProgressText = "Validating...";

            //Check for duplicate products on the nodes
            IEnumerable<IGrouping<Int32, ConsumerDecisionTreeNode>> duplicateProducts =
                this.CurrentTree.FetchAllNodes().SelectMany(p => p.Products)
                         .GroupBy(p => p.ProductId, p => p.ParentConsumerDecisionTreeNode)
                         .Where(p => p.Count() > 1)
                         .ToList();
            ProductInfoList duplicateProductInfos = ProductInfoList.FetchByEntityIdProductIds(entityId, duplicateProducts.Select(p => p.Key));
            _displayDuplicateScreen = duplicateProductInfos.Count > 0;

            dispatcher.Invoke((Action)(() => { _duplicateProductGridItems.RemoveRange(_duplicateProductGridItems.ToList()); }));
            foreach (IGrouping<Int32, ConsumerDecisionTreeNode> group in duplicateProducts)
            {
                ProductInfo foundProductInfo = duplicateProductInfos.FirstOrDefault(p => p.Id == group.Key);

                if (foundProductInfo != null)
                {
                    dispatcher.Invoke((Action)(() => { _duplicateProductGridItems.Add(new ConsumerDecisionTreeNodeProductItem(foundProductInfo, group)); }));
                }
            }

            //Check for products not in the selected product universe
            ProductUniverse selectedProductUniverse = ProductUniverse.FetchById(_selectedProductUniverse.Id);

            IEnumerable<Int32> universeProductIds = selectedProductUniverse.Products.Select(p => p.ProductId);
            IEnumerable<Int32> mergedTreeProductIds = this.CurrentTree.GetAssignedProductIds();

            //The ids in the CDT that are not in the product universe that was selected
            IEnumerable<Int32> missingProductIds = mergedTreeProductIds.Except(universeProductIds);
            ProductInfoList missingProducts = ProductInfoList.FetchByEntityIdProductIds(entityId, missingProductIds);
            _displayMissingScreen = missingProducts.Count > 0;
            dispatcher.Invoke((Action)(() => { _missingProducts.AddRange(missingProducts); }));
        }

        private void MergeWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UpdateCreationProgressBar(e.ProgressPercentage);
        }

        private void MergeWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            UpdateButtonVisibility();

            if (e.Error != null)
            {
                throw e.Error;
            }

            //If the execution was cancelled go to the previous step
            if (e.Cancelled)
            {
                this.WizardStep = (CdtWizardStep)(this.WizardStep -= 1);
            }
            else
            {
                this.IsComplete = true;
                this.NextCommand.RaiseCanExecuteChanged();
                this.ProgressText = Message.Generic_Completed;
                if (this._displayDuplicateScreen || this._displayMissingScreen)
                {
                    HasMergeProblem = true;
                }
                else
                {
                    HasMergeProblem = false;
                }
            }

            this.IsComplete = true;
            this.NextCommand.RaiseCanExecuteChanged();
            this.ProgressText = Message.Generic_Completed;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Creates and starts the creation worker for the selected CDT creation type
        /// </summary>
        private void StartCreationWorker()
        {
            //Set is complete to false
            this.IsComplete = false;

            base.ShowWaitCursor(true);

            //Reset the progress bar
            if (this.AttachedControl != null)
            {
                this.AttachedControl.ResetProgressBar();
            }

            this.ProgressText = Message.CdtWizard_Clearing;

            //clear down the existing tree
            foreach (ConsumerDecisionTreeNode node in this.CurrentTree.FetchAllNodes())
            {
                node.Products.Clear();
            }

            this.CurrentTree.RootLevel.ChildLevel = null;
            this.CurrentTree.RootNode.ChildList.Clear();
            //this.CurrentTree.ConsumerDecisionTreeSelections.Clear();
            this.CurrentTree.Type = ConsumerDecisionTreeType.Automatic;
            UpdateCreationProgressBar(1);

            if (this.WizardType == CdtWizardType.Create)
            {
                //Initialise the create worker
                if (_treeCreationWorker == null)
                {
                    _treeCreationWorker = new BackgroundWorker();
                    _treeCreationWorker.WorkerReportsProgress = true;
                    _treeCreationWorker.WorkerSupportsCancellation = true;
                    _treeCreationWorker.DoWork += new DoWorkEventHandler(CreateWorker_DoWork);
                    _treeCreationWorker.ProgressChanged += new ProgressChangedEventHandler(CreateWorker_ProgressChanged);
                    _treeCreationWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CreateWorker_RunWorkerCompleted);
                }

                if (this.AttachedControl != null)
                {
                    _treeCreationWorker.RunWorkerAsync(this.AttachedControl.Dispatcher);
                }
                else
                {
                    this.IsComplete = true;
                }
            }
            else
            {
                //Initialise the merge worker
                if (_treeMergeWorker == null)
                {
                    _treeMergeWorker = new BackgroundWorker();
                    _treeMergeWorker.WorkerReportsProgress = true;
                    _treeMergeWorker.WorkerSupportsCancellation = true;
                    _treeMergeWorker.DoWork += new DoWorkEventHandler(MergeWorker_DoWork);
                    _treeMergeWorker.ProgressChanged += new ProgressChangedEventHandler(MergeWorker_ProgressChanged);
                    _treeMergeWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(MergeWorker_RunWorkerCompleted);
                }

                if (this.AttachedControl != null)
                {
                    _treeMergeWorker.RunWorkerAsync(this.AttachedControl.Dispatcher);
                }
                else
                {
                    this.IsComplete = true;
                }
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Updates the duplicate products to only have a single parent node that was selected on the validation step
        /// </summary>
        private void UpdateDuplicateProducts()
        {
            base.ShowWaitCursor(true);

            //update duplicate products if required
            foreach (ConsumerDecisionTreeNodeProductItem duplicateProduct in _duplicateProductGridItems)
            {
                //Get the collection of parents that need to be removed from
                IEnumerable<ConsumerDecisionTreeNode> parentsToRemoveFrom =
                    duplicateProduct.ParentNodes.Where(p => p != duplicateProduct.SelectedParentNode);
                foreach (ConsumerDecisionTreeNode parent in parentsToRemoveFrom)
                {
                    if (parent != null)
                    {
                        //Get the product from the collection and remove it
                        ConsumerDecisionTreeNodeProduct product = parent.Products.FirstOrDefault(p => p.ProductId == duplicateProduct.SourceProductId);
                        if (product != null)
                        {
                            parent.Products.Remove(product);
                        }
                    }
                }
            }
            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Populates the list of available attributes for the given product universe
        /// </summary>
        /// <param name="productUniverseId">Product universe to return available attributes for</param>
        private void PopulateAvailableProperties(Int32 productUniverseId)
        {
            List<ObjectFieldInfo> availableProperties = Product.EnumerateDisplayableFieldInfos().ToList();

            //Clear current attributes
            _availableAttributes.Clear();
            this.SelectedAttribute = null;

            //Add to available attributes
            foreach (ObjectFieldInfo property in availableProperties)
            {
                _availableAttributes.Add(new ConsumerDecisionTreePropertyComboBoxItem(property));
            }

            //If taken attributes are in available attributes, remove them from available, else remove them from taken
            foreach (ConsumerDecisionTreePropertyComboBoxItem taken in _takenAttributes.ToList())
            {
                if (_availableAttributes.FirstOrDefault(a => a.SourceProperty == taken.SourceProperty) != null)
                {
                    _availableAttributes.Remove(taken);
                }
                else
                {
                    _takenAttributes.Remove(taken);
                }
            }

            //select the first attribute
            this.SelectedAttribute = _availableAttributes.FirstOrDefault();
        }

        /// <summary>
        /// Auto generates a cdt based on the selected merchandising hierarchy group and 
        /// taken attributes
        /// </summary>
        private void AutoGenerateTree()
        {
            //clear down the existing tree
            this.CurrentTree.RootNode.ChildList.Clear();
            this.CurrentTree.RootLevel.ChildLevel = null;

            //get the universe
            ProductUniverse currentUniverse = ProductUniverse.FetchById(this.SelectedProductUniverse.Id);

            //Gets products
            ProductList products = ProductList.FetchByProductIds(currentUniverse.Products.Select(p => p.ProductId));

            //Get the expected node count and then the progress bar step distance
            if (this.AttachedControl != null)
            {
                Int32 expectedNodeCount = this.CurrentTree.RootNode.CalculateExpectedNodeCount(_takenAttributes.Select(p => p.SourceProperty), products);
                this.AttachedControl.ExpectedNodeCount = expectedNodeCount;
            }

            //recurse through tree breadth first splitting nodes
            List<ConsumerDecisionTreeNode> nodeList = new List<ConsumerDecisionTreeNode>();
            nodeList.Add(this.CurrentTree.RootNode);
            SplitNodesByProperty(_takenAttributes.FirstOrDefault(), nodeList, products);
        }

        /// <summary>
        /// Method to handle the product universe changing
        /// </summary>
        private void OnSelectedProductUniverseChanged(ProductUniverseInfo newValue, ProductUniverseInfo oldValue)
        {
            //Only reload if needs be
            if (oldValue != null)
            {
                if (newValue.Id != oldValue.Id)
                {
                    this.ShowWaitCursor(true);

                    //Reload available properties
                    PopulateAvailableProperties(this.SelectedProductUniverse.Id);

                    this.ShowWaitCursor(false);
                }
            }
            else
            {
                this.ShowWaitCursor(true);

                //Reload available properties
                if (this.SelectedProductUniverse != null)
                {
                    PopulateAvailableProperties(this.SelectedProductUniverse.Id);
                }

                this.ShowWaitCursor(false);
            }
        }

        /// <summary>
        /// Splits the given nodes by the given attribute, then assigns the product list to the correct nodes,
        /// recurses until bottom of the tree
        /// </summary>
        /// <param name="attribute">Attribute to split nodes by</param>
        /// <param name="nodesToSplit">The nodes to split</param>
        /// <param name="productList">Products to assign to nodes</param>
        private void SplitNodesByProperty(ConsumerDecisionTreePropertyComboBoxItem attribute, List<ConsumerDecisionTreeNode> nodesToSplit, IEnumerable<Product> productList)
        {
            if (attribute != null)
            {
                List<ConsumerDecisionTreeNode> nodesCreated = new List<ConsumerDecisionTreeNode>();

                foreach (ConsumerDecisionTreeNode node in nodesToSplit)
                {
                    node.SplitByProductProperties(new List<ObjectFieldInfo>() { attribute.SourceProperty }, productList);
                    nodesCreated.AddRange(node.ChildList);

                    UpdateCreationProgressBar(node.ChildList.Count);
                }

                _takenAttributes.Remove(attribute);
                //recurse
                SplitNodesByProperty(_takenAttributes.FirstOrDefault(), nodesCreated, productList);
            }
        }

        /// <summary>
        /// Splits the given nodes by the given attribute, then assigns the product list to the correct nodes,
        /// recurses until bottom of the tree
        /// </summary>
        /// <param name="attribute">Attribute to split nodes by</param>
        /// <param name="nodesToSplit">The nodes to split</param>
        /// <param name="productList">Products to assign to nodes</param>
        private void SplitNodesByProperty(
            ConsumerDecisionTreePropertyComboBoxItem attribute,
            List<ConsumerDecisionTreeNode> nodesToSplit,
            IEnumerable<Product> productList,
            Dispatcher dispatcher)
        {
            if (attribute != null)
            {
                List<ConsumerDecisionTreeNode> nodesCreated = new List<ConsumerDecisionTreeNode>();

                foreach (ConsumerDecisionTreeNode node in nodesToSplit)
                {
                    this.ProgressText = String.Format(CultureInfo.CurrentCulture, Message.CdtWizard_Splitting, node.Name);

                    dispatcher.Invoke((Action)(() => { node.SplitByProductProperties(new List<ObjectFieldInfo>() { attribute.SourceProperty }, productList); }));
                    nodesCreated.AddRange(node.ChildList);

                    //Report the progress and check for cancellation
                    _treeCreationWorker.ReportProgress(nodesCreated.Count);
                    if (_treeCreationWorker.CancellationPending) { return; }
                }

                dispatcher.Invoke((Action)(() => { _splitAttributes.Remove(attribute); }));

                //recurse
                SplitNodesByProperty(_splitAttributes.FirstOrDefault(), nodesCreated, productList, dispatcher);
            }
        }

        /// <summary>
        /// Updates the visibility of the navigation buttons
        /// </summary>
        private void UpdateButtonVisibility()
        {
            switch (this.WizardStep)
            {
                case CdtWizardStep.Selection:
                    this.IsPreviousVisible = false;
                    this.IsNextVisible = true;
                    this.IsFinishVisible = false;
                    this.IsCancelVisible = true;
                    this.IsYesVisible = false;
                    this.IsNoVisible = false;
                    break;
                case CdtWizardStep.CdtCreation:
                    //This is the final step of the create type
                    if (this.WizardType == CdtWizardType.Create
                        || !this._displayDuplicateScreen && !this._displayMissingScreen)
                    {
                        this.IsPreviousVisible = false;
                        this.IsNextVisible = false;
                        this.IsFinishVisible = true;
                        this.IsCancelVisible = false;
                        this.IsYesVisible = false;
                        this.IsNoVisible = false;
                    }
                    else
                    {
                        this.IsPreviousVisible = false;
                        this.IsNextVisible = true;
                        this.IsFinishVisible = false;
                        this.IsCancelVisible = false;
                        this.IsYesVisible = false;
                        this.IsNoVisible = false;
                    }
                    break;
                case CdtWizardStep.ValidationCdtProducts:
                    //This is the final step of the merge type
                    if (this.WizardType == CdtWizardType.Merge)
                    {
                        this.IsPreviousVisible = false;
                        this.IsNextVisible = true;
                        this.IsFinishVisible = false;
                        this.IsCancelVisible = false;
                        this.IsYesVisible = false;
                        this.IsNoVisible = false;
                    }
                    break;
                case CdtWizardStep.ValidationUniverseProducts:
                    //This is the final step of the merge type
                    if (this.WizardType == CdtWizardType.Merge)
                    {
                        this.IsPreviousVisible = false;
                        this.IsNextVisible = false;
                        this.IsFinishVisible = false;
                        this.IsCancelVisible = false;
                        this.IsYesVisible = true;
                        this.IsNoVisible = true;
                    }
                    break;
            }
        }

        /// <summary>
        /// Updates the progress bar by the given integer value
        /// </summary>
        /// <param name="progress"></param>
        private void UpdateCreationProgressBar(Int32 progress)
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.UpdateProgressBar(progress);
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.CurrentTree = null;
                    this.AttachedControl = null;

                    _masterProductUniverseInfoListView.Dispose();
                    _masterConsumerDecisionTreeInfoList.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    public sealed class ConsumerDecisionTreePropertyComboBoxItem : INotifyPropertyChanged
    {
        #region Fields
        private ObjectFieldInfo _sourceProperty;
        private String _friendlyName;
        private Int32? _index;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath SourcePropertyProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePropertyComboBoxItem>(p => p.SourceProperty);
        public static readonly PropertyPath FriendlyNameProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreePropertyComboBoxItem>(p => p.FriendlyName);
        #endregion

        #region Properties

        public ObjectFieldInfo SourceProperty
        {
            get { return _sourceProperty; }
            private set
            {
                _sourceProperty = value;
                OnPropertyChanged(SourcePropertyProperty);
            }
        }

        public String FriendlyName
        {
            get { return _friendlyName; }
        }

        public Int32? Index
        {
            get { return _index; }
            set { _index = value; }
        }

        #endregion

        #region Constructor

        public ConsumerDecisionTreePropertyComboBoxItem(ObjectFieldInfo sourceProperty)
        {
            _sourceProperty = sourceProperty;
            _friendlyName = sourceProperty.PropertyFriendlyName;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }

    public sealed class ConsumerDecisionTreeNodeProductItem : INotifyPropertyChanged
    {
        #region Fields
        private Dictionary<String, ConsumerDecisionTreeNode> _parentNodeStringDict = new Dictionary<String, ConsumerDecisionTreeNode>();
        private BulkObservableCollection<ConsumerDecisionTreeNode> _parentNodes = new BulkObservableCollection<ConsumerDecisionTreeNode>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> _parentNodesRO;
        private ConsumerDecisionTreeNode _selectedParentNode;
        private Boolean _hasBeenAssigned = false;

        private BulkObservableCollection<String> _parentPaths = new BulkObservableCollection<String>();
        private ReadOnlyBulkObservableCollection<String> _parentPathsRO;

        private Int32 _sourceProductId;
        private ProductInfo _sourceProductInfo;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath ParentNodesProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreeNodeProductItem>(p => p.ParentNodes);
        public static readonly PropertyPath SelectedParentNodeProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreeNodeProductItem>(p => p.SelectedParentNode);
        public static readonly PropertyPath ParentPathsProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreeNodeProductItem>(p => p.ParentPaths);
        public static readonly PropertyPath SourceProductInfoProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreeNodeProductItem>(p => p.SourceProductInfo);
        public static readonly PropertyPath SourceProductIdProperty = WpfHelper.GetPropertyPath<ConsumerDecisionTreeNodeProductItem>(p => p.SourceProductId);
        #endregion

        #region Properties

        public Dictionary<String, ConsumerDecisionTreeNode> ParentNodesDictionary
        {
            get { return _parentNodeStringDict; }
        }

        /// <summary>
        /// Returns the collection of nodes this product is a member of
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> ParentNodes
        {
            get
            {
                if (_parentNodesRO == null)
                {
                    _parentNodesRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode>(_parentNodes);
                }
                return _parentNodesRO;
            }
        }

        /// <summary>
        /// Get/Sets the selected parent node for this product
        /// </summary>
        public ConsumerDecisionTreeNode SelectedParentNode
        {
            get { return _selectedParentNode; }
            set
            {
                _selectedParentNode = value;
                _hasBeenAssigned = (_selectedParentNode != null);

                OnPropertyChanged(SelectedParentNodeProperty);
            }
        }

        /// <summary>
        /// Returns  true if a selected parent node has been selected for this product
        /// </summary>
        public Boolean HasBeenAssigned
        {
            get { return _hasBeenAssigned; }
        }

        /// <summary>
        /// Returns the collection of parent node paths
        /// </summary>
        public ReadOnlyBulkObservableCollection<String> ParentPaths
        {
            get
            {
                if (_parentPathsRO == null)
                {
                    _parentPathsRO = new ReadOnlyBulkObservableCollection<String>(_parentPaths);
                }
                return _parentPathsRO;
            }
        }

        /// <summary>
        /// Returns the source product info
        /// </summary>
        public ProductInfo SourceProductInfo
        {
            get { return _sourceProductInfo; }
        }

        /// <summary>
        /// Returns the Id of the source product
        /// </summary>
        public Int32 SourceProductId
        {
            get { return _sourceProductId; }
        }

        #endregion

        #region Constructor

        public ConsumerDecisionTreeNodeProductItem(ProductInfo sourceProductInfo, IEnumerable<ConsumerDecisionTreeNode> parentNodes)
        {
            _sourceProductInfo = sourceProductInfo;
            _sourceProductId = sourceProductInfo.Id;

            _parentNodes.AddRange(parentNodes);
            ConstructParentPaths();

            //auto select the first option
            _selectedParentNode = _parentNodes.FirstOrDefault();
            _hasBeenAssigned = (_selectedParentNode != null);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Constructs the parent node paths for each of the parent nodes
        /// </summary>
        private void ConstructParentPaths()
        {
            foreach (ConsumerDecisionTreeNode parentNode in _parentNodes)
            {
                if (parentNode != null)
                {
                    List<ConsumerDecisionTreeNode> parentPathNodes = parentNode.FetchParentPath();

                    if (parentPathNodes.Count >= 2)
                    {
                        String key = String.Format("{0}:{1}", parentPathNodes[1].Name, parentNode.ToString());
                        if (!_parentNodeStringDict.ContainsKey(key))
                        {
                            _parentNodeStringDict.Add(key, parentNode);
                        }
                    }

                    String parentPath = String.Format("{0}:{1}", String.Join(":", parentPathNodes.Select(p => p.ToString())), parentNode.ToString());
                    _parentPaths.Add(parentPath);
                }
                else
                {
                    if (!_parentNodeStringDict.ContainsKey(String.Empty))
                    {
                        _parentNodeStringDict.Add(String.Empty, null);
                    }
                }
            }
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}

