﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Framework.Collections;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Interaction logic for CdtMaintenanceBackstageOpen.xaml
    /// </summary>
    public sealed partial class CdtMaintenanceBackstageOpen : UserControl
    {
        #region Fields
        private BulkObservableCollection<ConsumerDecisionTreeInfo> _searchResults = new BulkObservableCollection<ConsumerDecisionTreeInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(CdtMaintenanceViewModel), typeof(CdtMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public CdtMaintenanceViewModel ViewModel
        {
            get { return (CdtMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceBackstageOpen senderControl = (CdtMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                CdtMaintenanceViewModel oldModel = (CdtMaintenanceViewModel)e.OldValue;
                oldModel.AvailableCdts.BulkCollectionChanged -= senderControl.ViewModel_AvailableCdtsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                CdtMaintenanceViewModel newModel = (CdtMaintenanceViewModel)e.NewValue;
                newModel.AvailableCdts.BulkCollectionChanged += senderControl.ViewModel_AvailableCdtsBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(CdtMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceBackstageOpen senderControl = (CdtMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyBulkObservableCollection<ConsumerDecisionTreeInfo>),
            typeof(CdtMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeInfo> SearchResults
        {
            get { return (ReadOnlyBulkObservableCollection<ConsumerDecisionTreeInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public CdtMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreeInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                _searchResults.AddRange(this.ViewModel.GetMatchingCdts(this.SearchText));
            }
        }

        #endregion

        #region Event Handler

        private void ViewModel_AvailableCdtsBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }


        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ExtendedDataGrid senderControl = (ExtendedDataGrid)sender;
            DataGridRow clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<DataGridRow>();
            if (clickedItem != null)
            {
                ConsumerDecisionTreeInfo cdtToOpen = (ConsumerDecisionTreeInfo)senderControl.SelectedItem;

                if (cdtToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(cdtToOpen.Id);
                }
            }
        }

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                ExtendedDataGrid senderControl = (ExtendedDataGrid)sender;
                DataGridRow clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<DataGridRow>();
                if (clickedItem != null)
                {
                    ConsumerDecisionTreeInfo cdtToOpen = (ConsumerDecisionTreeInfo)senderControl.SelectedItem;

                    if (cdtToOpen != null)
                    {
                        //send the store to be opened
                        this.ViewModel.OpenCommand.Execute(cdtToOpen.Id);
                    }
                }
                e.Handled = true;
            }
        }

        #endregion
    }
}
