﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
// V8-27128 : L.Luong
//  Added loaded for the split node drop down button
#endregion
#endregion

using System;
using System.Windows;
using Fluent;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Interaction logic for CdtMaintenanceHomeTab.xaml
    /// </summary>
    public partial class CdtMaintenanceHomeTab : RibbonTabItem
    {
        Boolean firstLoad = true;

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(CdtMaintenanceViewModel), typeof(CdtMaintenanceHomeTab),
            new PropertyMetadata(null));

        public CdtMaintenanceViewModel ViewModel
        {
            get { return (CdtMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public CdtMaintenanceHomeTab()
        {
            InitializeComponent();

            this.Loaded -= new RoutedEventHandler(splitNodeButton_Loaded);
        }

        #endregion

        private void splitNodeButton_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.ViewModel != null && firstLoad)
            {
                Type type = typeof(Product);
                String typeFriendly = Product.FriendlyName;

                firstLoad = false;
                MenuItem size = new MenuItem();
                size.CommandParameter = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly,Product.SizeProperty);
                size.Command = this.ViewModel.SplitNodeCommand;
                size.Header = Product.SizeProperty.FriendlyName;
                splitNodeButton.Items.Add(size);


                MenuItem Brand = new MenuItem();
                Brand.CommandParameter = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.BrandProperty);
                Brand.Command = this.ViewModel.SplitNodeCommand;
                Brand.Header = Product.BrandProperty.FriendlyName;
                splitNodeButton.Items.Add(Brand);

                MenuItem UnitOfMeasure = new MenuItem();
                UnitOfMeasure.CommandParameter = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.UnitOfMeasureProperty);
                UnitOfMeasure.Command = this.ViewModel.SplitNodeCommand;
                UnitOfMeasure.Header = Product.UnitOfMeasureProperty.FriendlyName;
                splitNodeButton.Items.Add(UnitOfMeasure);

                MenuItem Vendor = new MenuItem();
                Vendor.CommandParameter = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.VendorProperty);
                Vendor.Command = this.ViewModel.SplitNodeCommand;
                Vendor.Header = Product.VendorProperty.FriendlyName;
                splitNodeButton.Items.Add(Vendor);

                MenuItem MoreProperties = new MenuItem();
                MoreProperties.Command = this.ViewModel.MoreSplitPropertiesCommand;
                MoreProperties.Header = this.ViewModel.MoreSplitPropertiesCommand.FriendlyName;
                splitNodeButton.Items.Add(MoreProperties);
            }
        }
    }
}
