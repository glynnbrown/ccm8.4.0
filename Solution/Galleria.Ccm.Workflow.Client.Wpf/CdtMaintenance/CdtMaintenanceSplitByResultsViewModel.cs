﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Diagnostics;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// ViewModel controller for the CdtMaintenanceSplitByResultsWindow
    /// </summary>
    public sealed class CdtMaintenanceSplitByResultsViewModel : ViewModelAttachedControlObject<CdtMaintenanceSplitByResultsWindow>
    {
        #region Fields

        private IEnumerable<ObjectFieldInfo> _splitByProperties;
        private BulkObservableCollection<SplitByProductRow> _productRows = new BulkObservableCollection<SplitByProductRow>();
        private ReadOnlyBulkObservableCollection<SplitByProductRow> _productRowsRO;
        private ObservableCollection<SplitByProductRow> _selectedProductRows = new ObservableCollection<SplitByProductRow>();
        private ConsumerDecisionTree _currentTree;
        private Boolean _isNodeSelectionEnabled;

        private BulkObservableCollection<ConsumerDecisionTreeNode> _availableSelectionNodes = new BulkObservableCollection<ConsumerDecisionTreeNode>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> _availableSelectionNodesRO;
        private Object _newSelectedNode;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductRowsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.ProductRows);
        public static readonly PropertyPath SelectedProductRowsProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.SelectedProductRows);
        public static readonly PropertyPath IsNodeSelectionEnabledProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.IsNodeSelectionEnabled);
        public static readonly PropertyPath NewSelectedNodeProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.NewSelectedNode);
        public static readonly PropertyPath AvailableSelectionNodesProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.AvailableSelectionNodes);
        public static readonly PropertyPath SplitByPropertiesProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.SplitByProperties);

        public static readonly PropertyPath AssignNodeCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.AssignNodeCommand);
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.OkCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<CdtMaintenanceSplitByResultsViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available split by properties
        /// </summary>
        public IEnumerable<ObjectFieldInfo> SplitByProperties
        {
            get
            {
                return _splitByProperties;
            }
        }

        /// <summary>
        /// Returns the collection of product rows
        /// </summary>
        public ReadOnlyBulkObservableCollection<SplitByProductRow> ProductRows
        {
            get
            {
                if (_productRowsRO == null)
                {
                    _productRowsRO = new ReadOnlyBulkObservableCollection<SplitByProductRow>(_productRows);
                }
                return _productRowsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected product nrows rows
        /// </summary>
        public ObservableCollection<SplitByProductRow> SelectedProductRows
        {
            get { return _selectedProductRows; }
        }

        /// <summary>
        /// Returns the collection of location rows
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> AvailableSelectionNodes
        {
            get
            {
                if (_availableSelectionNodesRO == null)
                {
                    _availableSelectionNodesRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode>(_availableSelectionNodes);
                }
                return _availableSelectionNodesRO;
            }
        }

        /// <summary>
        /// Gets/Sets the new selected node for the location rows
        /// </summary>
        public Object NewSelectedNode
        {
            get { return _newSelectedNode; }
            set
            {
                _newSelectedNode = value;
                OnPropertyChanged(NewSelectedNodeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets where the node selection is enabled to the user
        /// </summary>
        public Boolean IsNodeSelectionEnabled
        {
            get { return _isNodeSelectionEnabled; }
            set
            {
                _isNodeSelectionEnabled = value;
                OnPropertyChanged(IsNodeSelectionEnabledProperty);
            }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public CdtMaintenanceSplitByResultsViewModel(ConsumerDecisionTree currentTree, IEnumerable<Product> selectedProducts, IEnumerable<ObjectFieldInfo> splitByProperties, List<ConsumerDecisionTreeNode> nullValueNodes)
        {
            _currentTree = currentTree;
            _splitByProperties = splitByProperties;

            //Create product lookup dictionary
            Dictionary<Int32, Product> prodLookup = selectedProducts.ToDictionary(p => p.Id);

            //Split the nodes into the various row objects required
            foreach (ConsumerDecisionTreeNode node in nullValueNodes)
            {
                //Get scenario model levels
                ConsumerDecisionTreeLevel level = currentTree.GetLinkedLevel(node);

                //Enumerate through products
                foreach (ConsumerDecisionTreeNodeProduct nodeProduct in node.Products)
                {
                    //Lookup product
                    Product product;
                    if (prodLookup.TryGetValue(nodeProduct.ProductId, out product))
                    {
                        _productRows.Add(new SplitByProductRow(product, level, node));
                    }
                }
            }

            //Hook into the selected rows changed events
            this.SelectedProductRows.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(SelectedProductRows_CollectionChanged);
        }

        /// <summary>
        /// Handler for the selected product row collection changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedProductRows_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            //Ensure all of the selected rows are at the same level with the same parent
            if (this.SelectedProductRows.Select(p => p.ParentLevel).Distinct().Count() == 1 &&
                this.SelectedProductRows.Select(p => p.ParentNode).Distinct().Count() == 1)
            {
                //Update available nodes for selection
                UpdateAvailableSelectionNodes();

                //Set flag
                this.IsNodeSelectionEnabled = true;
            }
            else
            {
                this.IsNodeSelectionEnabled = false;
            }
        }

        #endregion

        #region Commands

        #region AssignNode

        private RelayCommand _assignNodeCommand;

        /// <summary>
        /// Assign the selected locations to the selected node
        /// </summary>
        public RelayCommand AssignNodeCommand
        {
            get
            {
                if (_assignNodeCommand == null)
                {
                    _assignNodeCommand = new RelayCommand(
                        p => AssignNode_Executed(),
                        p => AssignNode_CanExecute())
                    {
                        FriendlyName = Message.CdtModelSplitByResultsWindow_AssignNode,
                        FriendlyDescription = Message.CdtModelSplitByResultsWindow_AssignNode_Description,
                        Icon = ImageResources.Add_32,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_assignNodeCommand);
                }
                return _assignNodeCommand;
            }
        }

        private Boolean AssignNode_CanExecute()
        {
            //New selected node cannot be a duplicate of a sibling
            if (this.NewSelectedNode != null)
            {
                if (this.NewSelectedNode.GetType() == typeof(String))
                {
                    if (this.NewSelectedNode.Equals(String.Empty))
                    {
                        this.AssignNodeCommand.DisabledReason = Message.CdtModelSplitByResultsWindow_AssignNode_DisabledReasonNoNodeSelected;
                        return false;
                    }

                    this.AssignNodeCommand.DisabledReason = Message.CdtModelSplitByResultsWindow_AssignNode_DisabledReasonDuplicateName;
                    return (!this.AvailableSelectionNodes.Select(p => p.Name).Contains(this.NewSelectedNode));
                }
            }

            if (this.SelectedProductRows.Count <= 0)
            {
                this.AssignNodeCommand.DisabledReason = Message.CdtModelSplitByResultsWindow_AssignNode_DisabledReasonNoProductsSelected;
            }

            this.AssignNodeCommand.DisabledReason = Message.CdtModelSplitByResultsWindow_AssignNode_DisabledReasonNoNodeSelected;
            return (this.NewSelectedNode != null && this.NewSelectedNode.ToString() != String.Empty);
        }

        private void AssignNode_Executed()
        {
            //Enumerate through selected rows
            foreach (SplitByProductRow row in this.SelectedProductRows)
            {
                //Update the selected node
                row.SelectedNode = this.NewSelectedNode;
            }
        }

        #endregion

        #region Ok

        private RelayCommand _okCommand;

        /// <summary>
        /// Apply the changes
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => Ok_Executed(),
                        p => Ok_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok,
                        FriendlyDescription = Message.CdtModelSplitByResultsWindow_Ok_Description,
                        DisabledReason = Message.CdtModelSplitByResultsWindow_Ok_DisabledReason
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean Ok_CanExecute()
        {
            //Validate the rows
            return this.ProductRows.All(p => p.SelectedNode != null);
        }

        private void Ok_Executed()
        {
            //Apply the require changes to the cdt model structure
            IEnumerable<SplitByProductRow> rowsWithChanges = this.ProductRows.Where(p => p.SelectedNode != p.OriginalNode);

            if (rowsWithChanges.Any())
            {
                //Get distinct selected nodes
                IEnumerable<Object> nodesToUpdate = rowsWithChanges.Select(p => p.SelectedNode).Distinct();

                //Get parent of the rows
                ConsumerDecisionTreeNode parentNode = rowsWithChanges.Select(p => p.ParentNode).Distinct().FirstOrDefault();

                foreach (Object nodeToUpdate in nodesToUpdate)
                {
                    //If selection is another node
                    if (nodeToUpdate.GetType() == typeof(ConsumerDecisionTreeNode))
                    {
                        //Cast to node
                        ConsumerDecisionTreeNode node = (ConsumerDecisionTreeNode)nodeToUpdate;

                        //Get rows assigned to this node
                        IEnumerable<SplitByProductRow> nodeRows = rowsWithChanges.Where(p => p.SelectedNode.Equals(node));

                        //Remove from original node
                        foreach (SplitByProductRow row in nodeRows)
                        {
                            row.OriginalNode.Products.Remove(row.Product);
                        }

                        //Add products to it
                        node.Products.AddRange(nodeRows.Select(p => p.Product).Select(p => p.Id));
                    }
                    else if (nodeToUpdate.GetType() == typeof(String))
                    {
                        //Get rows assigned to this node
                        IEnumerable<SplitByProductRow> nodeRows = rowsWithChanges.Where(p => p.SelectedNode.Equals(nodeToUpdate));

                        if (parentNode != null)
                        {
                            //Remove from original node
                            foreach (SplitByProductRow row in nodeRows)
                            {
                                row.OriginalNode.Products.Remove(row.Product);
                            }

                            //Create new node
                            ConsumerDecisionTreeNode newNode = parentNode.AddNewChildNode();
                            newNode.Name = nodeToUpdate.ToString();

                            //Add locations to it
                            newNode.Products.AddRange(nodeRows.Select(p => p.Product).Select(p => p.Id));
                        }
                    }
                    else
                    {
                        //Should never hit this
                        Debug.Assert((nodeToUpdate.GetType() != typeof(String) || nodeToUpdate.GetType() == typeof(ConsumerDecisionTreeNode))
                            , "Show never get this, selection should either be ConsumerDecisionTreeNode or string");
                    }
                }

                //Ensure no ensure nodes are left
                foreach (ConsumerDecisionTreeNode node in parentNode.FetchAllChildNodes().ToList())
                {
                    if (node != null && !node.IsRoot)
                    {
                        if (node.Products.Count <= 0)
                        {
                            node.RemoveNode();
                        }
                    }
                }
            }

            if (this.AttachedControl != null)
            {
                //Close the window
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Apply the changes
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                        FriendlyDescription = Message.CdtModelSplitByResultsWindow_Cancel_Description
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                //Close the window
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Method to update the available selection nodes
        /// </summary>
        private void UpdateAvailableSelectionNodes()
        {
            //Clear existing nodes
            this._availableSelectionNodes.Clear();

            if (this.SelectedProductRows.Count > 0)
            {
                //Get common parent node
                ConsumerDecisionTreeNode parentNode = this.SelectedProductRows.FirstOrDefault().ParentNode;

                if (parentNode != null)
                {
                    //Add all of the parents child nodes (excl the node itself)
                    _availableSelectionNodes.AddRange(parentNode.ChildList.Where(p => p != parentNode));
                }
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this._newSelectedNode = null;
                    this._selectedProductRows.Clear();
                    this._splitByProperties = null;
                    this._availableSelectionNodes.Clear();
                    this._currentTree = null;
                    this._productRows.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Product row for the split by results window
    /// </summary>
    public sealed class SplitByProductRow : ViewModelObject
    {
        #region Fields
        private Product _product;
        private ConsumerDecisionTreeLevel _parentLevel;
        private ConsumerDecisionTreeNode _parentNode;
        private ConsumerDecisionTreeNode _originalNode;
        private Object _selectedNode;
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductProperty = WpfHelper.GetPropertyPath<SplitByProductRow>(p => p.Product);
        public static readonly PropertyPath ParentLevelProperty = WpfHelper.GetPropertyPath<SplitByProductRow>(p => p.ParentLevel);
        public static readonly PropertyPath SelectedNodeProperty = WpfHelper.GetPropertyPath<SplitByProductRow>(p => p.SelectedNode);
        public static readonly PropertyPath OriginalNodeProperty = WpfHelper.GetPropertyPath<SplitByProductRow>(p => p.OriginalNode);
        public static readonly PropertyPath ParentNodeProperty = WpfHelper.GetPropertyPath<SplitByProductRow>(p => p.ParentNode);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the product this row represents
        /// </summary>
        public Product Product
        {
            get { return _product; }
        }

        /// <summary>
        /// Gets the level number of the level in the location model that this location resides at
        /// </summary>
        public ConsumerDecisionTreeLevel ParentLevel
        {
            get { return _parentLevel; }
        }

        /// <summary>
        /// Returns the original node
        /// </summary>
        public ConsumerDecisionTreeNode OriginalNode
        {
            get { return _originalNode; }
        }

        /// <summary>
        /// Returns the locations parent node
        /// </summary>
        public ConsumerDecisionTreeNode ParentNode
        {
            get { return _parentNode; }
        }

        /// <summary>
        /// Gets/Sets the selected node for this location
        /// </summary>
        public Object SelectedNode
        {
            get { return _selectedNode; }
            set
            {
                _selectedNode = value;
                OnPropertyChanged(SelectedNodeProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="scenarioLocation"></param>
        /// <param name="scenarioModelLevel"></param>
        public SplitByProductRow(Product product, ConsumerDecisionTreeLevel parentLevel, ConsumerDecisionTreeNode originalNode)
        {
            _product = product;
            _originalNode = originalNode;
            _parentLevel = parentLevel;
            _parentNode = originalNode.ParentNode;
            _selectedNode = originalNode;
        }


        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _originalNode = null;
                    _parentNode = null;
                    _selectedNode = null;
                    _product = null;
                }
            }

        }

        #endregion
    }
}
