﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27128 : L.Luong
//      Created

#endregion

#endregion

using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Handles the notification of node changes for a tree node hierarchy
    /// </summary> 
    public sealed class CdtNodeTreeController
    {
        #region Fields
        
        private ConsumerDecisionTreeNode _rootNode;
        private BulkObservableCollection<ConsumerDecisionTreeNode> _handledNodes = new BulkObservableCollection<ConsumerDecisionTreeNode>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> _handledNodesRO;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of nodes that make up the joined tree
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> Nodes
        {
            get
            {
                if (_handledNodesRO == null)
                {
                    _handledNodesRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode>(_handledNodes);
                }
                return _handledNodesRO;
            }
        }

        /// <summary>
        /// Gets/Sets the root node to process from
        /// </summary>
        public ConsumerDecisionTreeNode RootNode
        {
            get { return _rootNode; }
            set
            {
                _rootNode = value;
                OnRootNodeChanged();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CdtNodeTreeController()
        {
            _handledNodes.BulkCollectionChanged += HandledNodes_BulkCollectionChanged;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to changes in the handled nodes collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandledNodes_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ConsumerDecisionTreeNode n in e.ChangedItems)
                    {
                        n.ChildList.CollectionChanged += Node_ChildCollectionChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ConsumerDecisionTreeNode n in e.ChangedItems)
                    {
                        n.ChildList.CollectionChanged -= Node_ChildCollectionChanged;
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    if (e.ChangedItems != null)
                    {
                        foreach (ConsumerDecisionTreeNode n in e.ChangedItems)
                        {
                            n.ChildList.CollectionChanged -= Node_ChildCollectionChanged;
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to changes in a node's child collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Node_ChildCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ConsumerDecisionTreeNode n in e.NewItems)
                    {
                        AddNodeToHandled(n);
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ConsumerDecisionTreeNode n in e.OldItems)
                    {
                        RemoveNodeFromHandled(n);
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:

                    //remove all handled children
                    ConsumerDecisionTreeNode parentNode = this.Nodes.First(n => n.ChildList == sender);
                    foreach (ConsumerDecisionTreeNode child in GetNodeHandledChildNodes(parentNode))
                    {
                        RemoveNodeFromHandled(child);
                    }

                    //add in all required children
                    foreach (ConsumerDecisionTreeNode child in (IEnumerable)sender)
                    {
                        AddNodeToHandled(child);
                    }
                    break;
            }
        }

        /// <summary>
        /// Responds to a change of the root node
        /// </summary>
        private void OnRootNodeChanged()
        {
            //clear the current handled nodes
            _handledNodes.RemoveRange(_handledNodes.ToList());

            if (_rootNode != null)
            {
                //add the root model node - the rest is recursive
                AddNodeToHandled(_rootNode);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the parent node of the given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public ConsumerDecisionTreeNode GetNodeHandledParent(ConsumerDecisionTreeNode node)
        {
            ConsumerDecisionTreeNode nodeToGetParentFor = node;

            if (nodeToGetParentFor != null)
            {
                return nodeToGetParentFor.ParentNode;
            }
            return default(ConsumerDecisionTreeNode);
        }

        /// <summary>
        /// Returns a list of the joined tree children of the given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public List<ConsumerDecisionTreeNode> GetNodeHandledChildNodes(ConsumerDecisionTreeNode node)
        {
            List<ConsumerDecisionTreeNode> returnList = new List<ConsumerDecisionTreeNode>();

            ConsumerDecisionTreeNode actualHandledNode = node;

            //add child nodes to the return list
            foreach (ConsumerDecisionTreeNode child in _handledNodes)
            {
                if (child.ParentNode != null)
                {
                    if (child.ParentNode.Equals(actualHandledNode))
                    {
                        returnList.Add(child);
                    }
                }
            }

            return returnList;
        }

        /// <summary>
        /// Adds the node to the handled 
        /// so that the event subscription can be tracked.
        /// </summary>
        /// <param name="n"></param>
        private void AddNodeToHandled(ConsumerDecisionTreeNode n)
        {
            ConsumerDecisionTreeNode actualNodeToAdd = n;

            if (actualNodeToAdd != null)
            {
                if (!_handledNodes.Contains(actualNodeToAdd))
                {
                    //add the node itself
                    _handledNodes.AddRange(GetNodeAndChildren(actualNodeToAdd));
                }
            }
        }

        /// <summary>
        /// Returns a node and all its children as an IEnumerable, so that they can be added to _handledNodes all at
        /// once rather than one at a time.
        /// </summary>
        private IEnumerable<ConsumerDecisionTreeNode> GetNodeAndChildren(ConsumerDecisionTreeNode n)
        {
            ConsumerDecisionTreeNode actualNodeToAdd = n;
            List<ConsumerDecisionTreeNode> returnValue = new List<ConsumerDecisionTreeNode> { n };
            //add all children recursively
            foreach (ConsumerDecisionTreeNode child in (IEnumerable)n.ChildList)
            {
                returnValue.AddRange(GetNodeAndChildren(child));
            }
            return returnValue;
        }

        /// <summary>
        /// Removes the node to the handled 
        /// so that the event unsubscription can be tracked.
        /// </summary>
        /// <param name="n"></param>
        private void RemoveNodeFromHandled(ConsumerDecisionTreeNode n)
        {
            ConsumerDecisionTreeNode actualRemoveNode = n;

            if (actualRemoveNode != null)
            {
                //remove the node itself
                _handledNodes.Remove(actualRemoveNode);

                //get and remove any child nodes recursively
                foreach (ConsumerDecisionTreeNode childNode in (IEnumerable)actualRemoveNode.ChildList)
                {
                    RemoveNodeFromHandled(childNode);
                }

            }
        }

        #endregion
    }
}
