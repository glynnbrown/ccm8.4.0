﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf.TreeMap;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using System.Globalization;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Diagram node content depicting a product hierarchy unit
    /// </summary>
    public sealed class CdtMaintenanceNode : TreeMapBlock, IDisposable
    {
        #region Events

        #region NodeChildrenChanged

        public static readonly RoutedEvent NodeChildrenChangedEvent =
         EventManager.RegisterRoutedEvent("NodeChildrenChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(CdtMaintenanceNode));

        public event RoutedEventHandler NodeChildrenChanged
        {
            add { AddHandler(NodeChildrenChangedEvent, value); }
            remove { RemoveHandler(NodeChildrenChangedEvent, value); }
        }

        private void RaiseNodeChildrenChangedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(CdtMaintenanceNode.NodeChildrenChangedEvent);
            RaiseEvent(newEventArgs);
        }

        #endregion

        #endregion

        #region Properties

        #region Node Context Property

        public static readonly DependencyProperty SourceNodeProperty =
            DependencyProperty.Register("SourceNode", typeof(ConsumerDecisionTreeNode), typeof(CdtMaintenanceNode),
            new PropertyMetadata(OnSourceNodePropertyChanged));
        /// <summary>
        /// Gets/ Sets the node content context
        /// </summary>
        public ConsumerDecisionTreeNode SourceNode
        {
            get { return (ConsumerDecisionTreeNode)GetValue(SourceNodeProperty); }
            set { SetValue(SourceNodeProperty, value); }
        }

        public static void OnSourceNodePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceNode senderControl = sender as CdtMaintenanceNode;
            if (e.OldValue != null)
            {
                ConsumerDecisionTreeNode oldModel = e.OldValue as ConsumerDecisionTreeNode;
                oldModel.PropertyChanged -= senderControl.NodeContext_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                ConsumerDecisionTreeNode newModel = e.NewValue as ConsumerDecisionTreeNode;
                newModel.PropertyChanged += senderControl.NodeContext_PropertyChanged;
            }
        }

        #endregion

        /// <summary>
        /// Percentage Node Contribution Property
        /// </summary>
        public static readonly DependencyProperty PercentageNodeContributionProperty =
            DependencyProperty.Register("PercentageNodeContribution", typeof(Double), typeof(CdtMaintenanceNode));
        public Double PercentageNodeContribution
        {
            get { return (Double)GetValue(PercentageNodeContributionProperty); }
            set { SetValue(PercentageNodeContributionProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Static constructor
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1810:InitializeReferenceTypeStaticFieldsInline", Justification = "Style key override")]
        static CdtMaintenanceNode()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(CdtMaintenanceNode), new FrameworkPropertyMetadata(typeof(CdtMaintenanceNode)));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="unitContext"></param>
        public CdtMaintenanceNode(ConsumerDecisionTreeNode sourceNode)
            : base()
        {
            //set source node
            this.SourceNode = sourceNode;

            //Subscribe to size changed
            this.SizeChanged += new SizeChangedEventHandler(CdtMaintenanceNode_SizeChanged);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to property changes on the node context
        /// </summary>
        private void NodeContext_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == ConsumerDecisionTreeNode.NameProperty.Name)
            {
                CdtMaintenanceNode_SizeChanged(null, null);
            }
        }

        private void CdtMaintenanceNode_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this.SourceNode != null)
            {
                this.ToolTip = String.Format(CultureInfo.CurrentCulture,
                    "{0}{1}Space %: {2}",
                   this.SourceNode.Name, Environment.NewLine,
                   this.SpacePercentage);
            }
        }

        #endregion

        #region IDisposable Members
        private Boolean _isDisposed;
        public void Dispose()
        {
            if (!_isDisposed)
            {
                this.SourceNode = null;
                _isDisposed = true;
            }
        }
        #endregion
    }
}
