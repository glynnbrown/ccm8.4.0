﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Interaction logic for CdtMaintenanceCreateOrganiser.xaml
    /// </summary>
    public partial class CdtMaintenanceCreateOrganiser : ExtendedRibbonWindow
    {
        #region Constants
        private const String RemoveConsumerDecisionTreeCommandKey = "RemoveConsumerDecisionTreeCommand";
        private const String RemoveAttributeCommandKey = "CdtMaintenanceCreateOrganiser_RemoveAttributeCommand";
        #endregion

        #region Fields
        private Double _progressBarStepping;
        private Int32 _expectedNodeCount;
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(CdtMaintenanceCreateViewModel), typeof(CdtMaintenanceCreateOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public CdtMaintenanceCreateViewModel ViewModel
        {
            get { return (CdtMaintenanceCreateViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceCreateOrganiser senderControl = (CdtMaintenanceCreateOrganiser)obj;

            if (e.OldValue != null)
            {
                CdtMaintenanceCreateViewModel oldModel = (CdtMaintenanceCreateViewModel)e.OldValue;

                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;

                senderControl.Resources.Remove(RemoveConsumerDecisionTreeCommandKey);
                senderControl.Resources.Remove(RemoveAttributeCommandKey);
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                CdtMaintenanceCreateViewModel newModel = (CdtMaintenanceCreateViewModel)e.NewValue;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;

                senderControl.Resources.Add(RemoveConsumerDecisionTreeCommandKey, newModel.RemoveConsumerDecisionTreeCommand);
                senderControl.Resources.Add(RemoveAttributeCommandKey, newModel.RemoveAttributeCommand);

                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region CurrentTree Property

        public static readonly DependencyProperty CurrentTreeProperty =
            DependencyProperty.Register("CurrentTree", typeof(ConsumerDecisionTree), typeof(CdtMaintenanceCreateOrganiser),
            new PropertyMetadata(null));

        public ConsumerDecisionTree CurrentTree
        {
            get { return (ConsumerDecisionTree)GetValue(CurrentTreeProperty); }
            set { SetValue(CurrentTreeProperty, value); }
        }

        #endregion

        public Int32 ExpectedNodeCount
        {
            get { return _expectedNodeCount; }
            set
            {
                _expectedNodeCount = value;
                _progressBarStepping = (100.0 - xCreateProgressBar.Value) / (Double)_expectedNodeCount;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for creating a new cdt
        /// </summary>
        public CdtMaintenanceCreateOrganiser(ConsumerDecisionTree currentTree, Int32? productUniverseId)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            this.Loaded += new RoutedEventHandler(CdtMaintenanceCreateOrganiser_Loaded);
            this.SizeChanged += new SizeChangedEventHandler(CdtMaintenanceCreateOrganiser_SizeChanged);

            InitializeComponent();

            this.ViewModel = new CdtMaintenanceCreateViewModel(currentTree, productUniverseId);
        }

        /// <summary>
        /// Constructor for creating a new cdt
        /// </summary>
        public CdtMaintenanceCreateOrganiser(ConsumerDecisionTree currentTree, Int32? productUniverseId, CdtWizardType wizardType)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            this.Loaded += new RoutedEventHandler(CdtMaintenanceCreateOrganiser_Loaded);
            this.SizeChanged += new SizeChangedEventHandler(CdtMaintenanceCreateOrganiser_SizeChanged);

            InitializeComponent();

            this.ViewModel = new CdtMaintenanceCreateViewModel(currentTree, productUniverseId, wizardType);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to the window finishing loading
        /// </summary>
        private void CdtMaintenanceCreateOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= CdtMaintenanceCreateOrganiser_Loaded;

            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Responds to a property change on the attached viewmodel
        /// </summary>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == CdtMaintenanceCreateViewModel.CurrentTreeProperty.Path)
            {
                this.CurrentTree = this.ViewModel.CurrentTree;
            }
        }

        /// <summary>
        /// Responds to a double click of a wizard type on the first step
        /// </summary>
        private void ListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //Execute the next command
            if (this.ViewModel != null)
            {
                this.ViewModel.NextCommand.Execute();
            }
        }

        /// <summary>
        /// Responds to a resize of the window, centering the window on the parent
        /// </summary>
        private void CdtMaintenanceCreateOrganiser_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize != e.PreviousSize)
            {
                CdtMaintenanceOrganiser parentWindow = this.Owner as CdtMaintenanceOrganiser;
                if (parentWindow != null)
                {
                    Double parentX = parentWindow.Left;
                    Double parentWidthCenter = parentWindow.Width / 2;
                    Double windowWidthCentre = this.Width / 2;

                    Double difference = parentWidthCenter - windowWidthCentre;

                    this.Left = parentX + difference;
                }
            }
        }

        /// <summary>
        /// Responds to a mouse wheel event on the duplicate products list box column
        /// Will scroll the grid as the list box is not scrollable
        /// </summary>
        private void GridCell_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (!e.Handled)
            {
                if (xDuplicateProductGrid.ScrollViewerChild != null)
                {
                    MouseWheelEventArgs mouseWheelEventArgs = new MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta);
                    mouseWheelEventArgs.RoutedEvent = UIElement.MouseWheelEvent;
                    mouseWheelEventArgs.Source = sender;

                    //Raise the event for the grid
                    xDuplicateProductGrid.ScrollViewerChild.RaiseEvent(mouseWheelEventArgs);
                }
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the progress bar to the correct position
        /// </summary>
        public void UpdateProgressBar(Int32 addNodeCount)
        {
            xCreateProgressBar.Value = Math.Min(100, xCreateProgressBar.Value + (addNodeCount * _progressBarStepping));
        }

        /// <summary>
        /// Resets the progress bar to 0 and resets the progress steps
        /// </summary>
        public void ResetProgressBar()
        {
            xCreateProgressBar.Value = 0;
            _progressBarStepping = 5;
        }

        #endregion

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (this.DialogResult == true)
            {
                this.CurrentTree = this.ViewModel.CurrentTree;
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel.PropertyChanged -= ViewModel_PropertyChanged;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
