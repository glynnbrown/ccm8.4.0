﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM 8.0.0
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
// V8-26041 : A.Kuszyk
//  Changed references to Image to System.Windows.Controls.Image due to conflict with model object.
// V8-27128 : L.Luong
//  Fixed loading issues when a node is selected after a splitnode command has been initialised
#endregion

#region Version History: CCM 8.1.1
// V8-30516 : M.Shelley
//  Added mouse double click behaviour to both the unassigned product and assigned product grids.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Interaction logic for CdtMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class CdtMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        private CdtNodeTreeController _treeController = new CdtNodeTreeController();
        private Boolean _suppressSelectionChangedHandler;
        private Boolean _suppressScrollChangedEvents;
        private CdtMaintenanceNode _marginNode;
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(CdtMaintenanceViewModel), typeof(CdtMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public CdtMaintenanceViewModel ViewModel
        {
            get { return (CdtMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceOrganiser senderControl = (CdtMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                CdtMaintenanceViewModel oldModel = (CdtMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.SelectedNodes.BulkCollectionChanged -= senderControl.ViewModel_SelectedNodesBulkCollectionChanged;
                oldModel.MajorNodeChangeStarting -= senderControl.ViewModel_MajorNodeChangeStarting;
                oldModel.MajorNodeChangeComplete -= senderControl.ViewModel_MajorNodeChangeComplete;
            }

            if (e.NewValue != null)
            {
                CdtMaintenanceViewModel newModel = (CdtMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.SelectedNodes.BulkCollectionChanged += senderControl.ViewModel_SelectedNodesBulkCollectionChanged;
                newModel.MajorNodeChangeStarting += senderControl.ViewModel_MajorNodeChangeStarting;
                newModel.MajorNodeChangeComplete += senderControl.ViewModel_MajorNodeChangeComplete;
            }

            senderControl.OnRootBlockChanged();
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CdtMaintenanceOrganiser()
        {
            this.AddHandler(CdtMaintenanceNode.MouseDoubleClickEvent, new MouseButtonEventHandler(CdtMaintenanceNode_MouseDoubleClicked));

            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ConsumerDecisionTreeMaintenance);

            _treeController.Nodes.BulkCollectionChanged += TreeController_NodesBulkCollectionChanged;

            this.ViewModel = new CdtMaintenanceViewModel();

            this.Loaded += new RoutedEventHandler(ModellingCDTProductFamiliesWindow_Loaded);
        }

        /// <summary>
        /// Carries out first load complete actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ModellingCDTProductFamiliesWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ModellingCDTProductFamiliesWindow_Loaded;

            treeMapControl.SelectedItems.BulkCollectionChanged += TreeMapControl_SelectedItemsBulkCollectionChanged;

            OnLevelBlockSelectionChanged();

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        /// <summary>
        /// Responds to property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == CdtMaintenanceViewModel.RootNodeProperty.Path)
            {
                OnRootBlockChanged();
            }
        }

        /// <summary>
        /// Responds to changes in the viewmodel selected nodes collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_SelectedNodesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (!_suppressSelectionChangedHandler)
            {
                _suppressSelectionChangedHandler = true;

                IEnumerable<CdtMaintenanceNode> blockList = treeMapControl.Blocks.Cast<CdtMaintenanceNode>();

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        this.treeMapControl.SelectedItems.Clear();

                        IEnumerable<CdtMaintenanceNode> availableBlocks =
                            this.treeMapControl.Blocks.Cast<CdtMaintenanceNode>().Where(b => e.ChangedItems.Contains(b.SourceNode));

                        //Set the selected level id
                        if (availableBlocks.Any())
                        {
                            this.ViewModel.SelectedLevelId = availableBlocks.First().SourceNode.ConsumerDecisionTreeLevelId;
                        }

                        this.treeMapControl.SelectedItems.AddRange(availableBlocks);
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        foreach (ConsumerDecisionTreeNode node in e.ChangedItems)
                        {
                            CdtMaintenanceNode block = blockList.FirstOrDefault(b => b.SourceNode == node);
                            if (block != null)
                            {
                                block.IsSelected = false;
                            }
                        }
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        foreach (CdtMaintenanceNode block in blockList)
                        {
                            ConsumerDecisionTreeNode content = block.SourceNode;
                            block.IsSelected = this.ViewModel.SelectedNodes.Contains(content);
                            if (block.IsSelected)
                            {
                                this.ViewModel.SelectedLevelId = block.SourceNode.ConsumerDecisionTreeLevelId;
                            }
                        }
                        break;
                }
                UpdateLevelSelection();
                _suppressSelectionChangedHandler = false;
            }
        }

        /// <summary>
        /// Responds to changes in the treemapcontrol selected items collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeMapControl_SelectedItemsBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            if (!_suppressSelectionChangedHandler)
            {

                _suppressSelectionChangedHandler = true;

                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        List<ConsumerDecisionTreeNode> selectedBlockAdd = new List<ConsumerDecisionTreeNode>();
                        foreach (CdtMaintenanceNode block in e.ChangedItems)
                        {
                            selectedBlockAdd.Add(block.SourceNode);
                            this.ViewModel.SelectedLevelId = block.SourceNode.ConsumerDecisionTreeLevelId;
                        }

                        this.ViewModel.SelectedNodes.AddRange(selectedBlockAdd);
                        break;

                    case NotifyCollectionChangedAction.Remove:
                        List<ConsumerDecisionTreeNode> selectedBlockRemove = new List<ConsumerDecisionTreeNode>();
                        foreach (CdtMaintenanceNode block in e.ChangedItems)
                        {
                            selectedBlockRemove.Add(block.SourceNode);
                        }
                        this.ViewModel.SelectedNodes.RemoveRange(selectedBlockRemove);
                        break;

                    case NotifyCollectionChangedAction.Reset:
                        foreach (CdtMaintenanceNode block in treeMapControl.Blocks)
                        {
                            ConsumerDecisionTreeNode content = block.SourceNode;
                            if (block.IsSelected)
                            {
                                if (!this.ViewModel.SelectedNodes.Contains(content))
                                {
                                    this.ViewModel.SelectedNodes.Add(content);
                                    this.ViewModel.SelectedLevelId = block.SourceNode.ConsumerDecisionTreeLevelId;
                                }
                            }
                            else
                            {
                                this.ViewModel.SelectedNodes.Remove(content);
                            }
                        }
                        break;
                }
                UpdateLevelSelection();
                _suppressSelectionChangedHandler = false;
            }
        }

        /// <summary>
        /// Responds to changes in the collection of nodes held by the tree controller
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeController_NodesBulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ConsumerDecisionTreeNode addedNode in e.ChangedItems)
                    {
                        ConsumerDecisionTreeNode parentNode = _treeController.GetNodeHandledParent(addedNode);
                        CdtMaintenanceNode nodeBlock = null;

                        if (parentNode != null)
                        {
                            CdtMaintenanceNode parentVisual =
                                treeMapControl.Blocks.Cast<CdtMaintenanceNode>()
                                .FirstOrDefault(n => n.SourceNode == parentNode);

                            nodeBlock = DrawNode(addedNode, parentVisual);
                        }
                        else
                        {
                            nodeBlock = DrawNode(addedNode, null);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    foreach (ConsumerDecisionTreeNode removedNode in e.ChangedItems)
                    {
                        IEnumerable<CdtMaintenanceNode> treeNodes =
                            treeMapControl.Blocks.Cast<CdtMaintenanceNode>();

                        //get the node visual
                        CdtMaintenanceNode nodeVisual =
                            treeNodes.FirstOrDefault(n => n.SourceNode == removedNode);

                        if (nodeVisual != null)
                        {
                            treeMapControl.Blocks.Remove(nodeVisual);
                        }
                    }
                    break;

                case NotifyCollectionChangedAction.Reset:
                    treeMapControl.Blocks.Clear();
                    break;
            }

            UpdateLevels();
        }

        /// <summary>
        /// Calls the edit node command on node double click
        /// </summary>
        private void CdtMaintenanceNode_MouseDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            if (((DependencyObject)e.OriginalSource).FindVisualAncestor<CdtMaintenanceNode>() != null)
            {
                this.ViewModel.EditNodeCommand.Execute();
            }
        }

        /// <summary>
        /// Responds to block right click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeMapControl_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            CdtMaintenanceNode clickedNode = ((DependencyObject)e.OriginalSource).FindVisualAncestor<CdtMaintenanceNode>();

            if (clickedNode != null)
            {
                //ensure the block is selected
                clickedNode.IsSelected = true;

                //display the context menu
                ShowNodeContextMenu(clickedNode);
            }
        }

        /// <summary>
        /// Responds to changes to the treemapcontrol scrollviewer
        /// Scrolls the levels scrollviewer to the same vertical offset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeMapControl_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (!_suppressScrollChangedEvents)
            {
                _suppressScrollChangedEvents = true;

                //scroll the levels stack scrollviewer to the same offset.
                levelsStackScrollviewer.ScrollToVerticalOffset(e.VerticalOffset);

                //Get the scrollableheight without the spacer row's height
                Double realScrollableHeight = levelsStackScrollviewer.ScrollableHeight - Math.Round(xSpacerRow_Levels.Height.Value);

                //Increase the height of the space as the tree map's horizontal scroll bar adds to it's scrollable height
                if (e.VerticalOffset > realScrollableHeight)
                {
                    xSpacerRow_Levels.Height = new GridLength(e.VerticalOffset - realScrollableHeight);
                }
                else if (xSpacerRow_Levels.Height.Value > 0)
                {
                    xSpacerRow_Levels.Height = new GridLength(0);
                }
            }
            else
            {
                _suppressScrollChangedEvents = false;
            }
        }

        /// <summary>
        /// Responds to changes to the levels scrollviewer
        /// Scrolls the node tree map to the same vertical offset
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LevelsStackScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (!_suppressScrollChangedEvents)
            {
                _suppressScrollChangedEvents = true;

                //scroll the node scrollviewer to the same offset.
                ScrollViewer scrollView = treeMapControl.FindVisualDescendent<ScrollViewer>();

                if (scrollView != null)
                {
                    scrollView.ScrollToVerticalOffset(e.VerticalOffset);
                }
            }
            else
            {
                _suppressScrollChangedEvents = false;
            }
        }

        /// <summary>
        /// Selects all range breaks on the clicked level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LevelBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            CdtMaintenanceLevelNode senderControl = (CdtMaintenanceLevelNode)sender;
            this.ViewModel.SelectedLevelId = senderControl.LevelContext.Id;

            OnLevelBlockSelectionChanged();
        }

        /// <summary>
        /// Selects all nodes on the selected level
        /// </summary>
        private void LevelBlockText_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox senderControl = (TextBox)sender;

            CdtMaintenanceLevelNode levelNode = senderControl.FindVisualAncestor<CdtMaintenanceLevelNode>();
            if (levelNode != null)
            {
                this.ViewModel.SelectedLevelId = levelNode.LevelContext.Id;
                OnLevelBlockSelectionChanged();
            }
        }

        /// <summary>
        /// Selects the nodes on the selected level
        /// </summary>
        public void OnLevelBlockSelectionChanged()
        {
            UpdateLevelSelection();

            this.treeMapControl.SelectedItems.Clear();
            IEnumerable<CdtMaintenanceNode> availableBlocks =
                this.treeMapControl.Blocks.Cast<CdtMaintenanceNode>().Where(b => b.SourceNode.ConsumerDecisionTreeLevelId.Equals(this.ViewModel.SelectedLevelId));
            this.treeMapControl.SelectedItems.AddRange(availableBlocks);
        }

        /// <summary>
        /// Resets the row height when the expander collapses
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedProducts_Collapsed(object sender, RoutedEventArgs e)
        {
            xExpanderColumn.Height = GridLength.Auto;
            xExpanderColumn.MinHeight = 18;
        }

        /// <summary>
        /// Set the row height of the assigned products expander to * when it expands
        /// </summary>
        private void xAssignedProducts_Expanded(object sender, RoutedEventArgs e)
        {
            xExpanderColumn.Height = new GridLength(1, GridUnitType.Star);
            xExpanderColumn.MinHeight = 200;
        }

        /// <summary>
        /// Responds to the viewmodel notification that a major node change is starting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_MajorNodeChangeStarting(object sender, EventArgs e)
        {
            //supress handlers for speed
            _treeController.Nodes.BulkCollectionChanged -= TreeController_NodesBulkCollectionChanged;
        }

        /// <summary>
        /// Reponds to the viewmodel notification that a major node change has completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_MajorNodeChangeComplete(object sender, EventArgs e)
        {
            //redraw and recalculate all.
            _treeController.Nodes.BulkCollectionChanged += TreeController_NodesBulkCollectionChanged;
            OnRootBlockChanged();
        }

        private void xNodeUnassignedProductsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xNodeUnassignedProductsGrid)) return;
            if (!ViewModel.SelectedUnassignedProducts.Any()) return;

            ViewModel.AddSelectedProductsCommand.Execute();
        }

        private void xNodeAssignedProductsGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.xNodeAssignedProductsGrid)) return;
            if (!ViewModel.SelectedAssignedProducts.Any()) return;

            ViewModel.RemoveSelectedProductsCommand.Execute();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Responds to a change of root block
        /// </summary>
        private void OnRootBlockChanged()
        {
            //clear out the old items
            if (treeMapControl.Blocks.Count > 0)
            {
                List<CdtMaintenanceNode> oldBlocks = treeMapControl.Blocks.Cast<CdtMaintenanceNode>().ToList();
                treeMapControl.Blocks.Clear();
                foreach (CdtMaintenanceNode oldBlock in oldBlocks)
                {
                    oldBlock.Dispose();
                }
            }
            _marginNode = null;

            if (this.ViewModel != null)
            {
                //load the tree
                _treeController.RootNode = this.ViewModel.CurrentCdt.RootNode;
            }
            else
            {
                _treeController.RootNode = null;
            }
        }

        /// <summary>
        /// Draws the visual for the given node
        /// </summary>
        /// <param name="nodeViewModel"></param>
        /// <param name="parentVisual"></param>
        /// <returns></returns>
        private CdtMaintenanceNode DrawNode(ConsumerDecisionTreeNode node, CdtMaintenanceNode parentVisual)
        {
            // Create the node
            CdtMaintenanceNode nodeVisual = new CdtMaintenanceNode(node);
            if (parentVisual != null)
            {
                nodeVisual.ParentBlock = parentVisual;
            }
            nodeVisual.SpacePercentage = node.GetPercentOfParent();

            treeMapControl.Blocks.Add(nodeVisual);

            return nodeVisual;
        }

        /// <summary>
        /// Updates the collection of level blocks
        /// </summary>
        public void UpdateLevels()
        {
            if (this.ViewModel != null)
            {
                //check if the levels display has changed
                Boolean reloadRequired = false;

                Int32 currentStackCount = this.cdtLevelsStack.Children.Count;

                List<ConsumerDecisionTreeLevel> levels = this.ViewModel.CurrentCdt.FetchAllLevels().ToList();
                Int32 reqLevelCount = levels.Count();

                if (currentStackCount != reqLevelCount)
                {
                    reloadRequired = true;
                }
                else
                {
                    for (Int32 i = 0; i < reqLevelCount; i++)
                    {
                        CdtMaintenanceLevelNode levelBlock = (CdtMaintenanceLevelNode)this.cdtLevelsStack.Children[i];
                        ConsumerDecisionTreeLevel level = levels[i];
                        if (levelBlock.LevelContext != level)
                        {
                            reloadRequired = true;
                            break;
                        }
                    }
                }

                if (reloadRequired)
                {
                    //clear existing blocks
                    if (this.cdtLevelsStack.Children.Count > 0)
                    {
                        foreach (CdtMaintenanceLevelNode levelNode in this.cdtLevelsStack.Children)
                        {
                            levelNode.Dispose();
                        }

                        this.cdtLevelsStack.Children.Clear();
                    }


                    ConsumerDecisionTreeLevel level = this.ViewModel.CurrentCdt.RootLevel;
                    while (level != null)
                    {
                        CdtMaintenanceLevelNode block = new CdtMaintenanceLevelNode(level);
                        block.Height = treeMapControl.BlockHeight;
                        block.MouseDown += new MouseButtonEventHandler(LevelBlock_MouseDown);

                        //if not the root level then add context menu options
                        if (!level.IsRoot)
                        {
                            System.Windows.Controls.ContextMenu blockContext = new System.Windows.Controls.ContextMenu();

                            //remove level ranges command
                            System.Windows.Controls.MenuItem removeLevel = new System.Windows.Controls.MenuItem();
                            removeLevel.Command = this.ViewModel.RemoveLevelCommand;
                            removeLevel.Header = this.ViewModel.RemoveLevelCommand.FriendlyName;
                            removeLevel.Icon = new System.Windows.Controls.Image() { Source = this.ViewModel.RemoveLevelCommand.SmallIcon, Margin = new Thickness(1) };
                            blockContext.Items.Add(removeLevel);

                            block.ContextMenu = blockContext;
                        }
                        else
                        {
                            block.IsSelected = true;
                        }

                        this.cdtLevelsStack.Children.Add(block);
                        level = level.ChildLevel;
                    }

                    UpdateNodeBlockMargin();
                    UpdateLevelSelection();
                }
            }
            else
            {
                //clear existing blocks
                if (this.cdtLevelsStack.Children.Count > 0)
                {
                    foreach (CdtMaintenanceLevelNode levelNode in this.cdtLevelsStack.Children)
                    {
                        levelNode.Dispose();
                    }

                    this.cdtLevelsStack.Children.Clear();
                }

            }
        }

        /// <summary>
        /// Updates the levels to highlight the selected level
        /// </summary>
        private void UpdateLevelSelection()
        {
            IEnumerable<CdtMaintenanceLevelNode> levelBlocks = this.cdtLevelsStack.Children.Cast<CdtMaintenanceLevelNode>();

            //Set the IsSelected property of the selected level to true
            CdtMaintenanceLevelNode selectedLevel = levelBlocks.FirstOrDefault(p => p.LevelContext.Id.Equals(this.ViewModel.SelectedLevelId));
            if (selectedLevel != null)
            {
                if (!selectedLevel.IsSelected)
                {
                    selectedLevel.IsSelected = true;
                }
            }

            //Update all the other level's IsSelected property to false
            IEnumerable<CdtMaintenanceLevelNode> availableLevels = levelBlocks.Where(b => !b.Equals(selectedLevel) && b.IsSelected);
            foreach (CdtMaintenanceLevelNode level in availableLevels)
            {
                level.IsSelected = false;
            }
        }

        /// <summary>
        /// Updates the margin on the last node block in the tree map so that the levels and nodes have the same scrollable area
        /// </summary>
        private void UpdateNodeBlockMargin()
        {
            //Get the last block in the tree map control to set a margin on if required
            CdtMaintenanceNode lastBlock = this.treeMapControl.Blocks.LastOrDefault() as CdtMaintenanceNode;
            if (lastBlock != null)
            {
                //reset the margin on the previous last block if required
                if (_marginNode != null &&
                    _marginNode != lastBlock)
                {
                    _marginNode.Margin = new Thickness(0);
                }

                Double levelScrollableHeight = Math.Min(0, levelsStackScrollviewer.ActualHeight - (cdtLevelsStack.Children.Count * lastBlock.Height)) * -1;
                Double nodeScrollableHeight = Math.Min(0, treeMapControl.ActualHeight - (treeMapControl.Blocks.Count * lastBlock.Height)) * -1;

                if (levelScrollableHeight > 0 ||
                    nodeScrollableHeight > 0)
                {
                    Int32 blockLevels = treeMapControl.Blocks.Select(p => p.BlockLevel).Distinct().Count();
                    lastBlock.Margin = new Thickness(0, 0, 0, Math.Max(0, (cdtLevelsStack.Children.Count - blockLevels) * lastBlock.Height));
                    _marginNode = lastBlock;
                }
                else if (lastBlock.Margin.Bottom > 0)
                {
                    lastBlock.Margin = new Thickness(0);
                }
            }
        }

        /// <summary>
        /// Displays the context menu for the clicked CdtMaintenanceNode
        /// </summary>
        private void ShowNodeContextMenu(CdtMaintenanceNode clickedNode)
        {
            System.Windows.Controls.ContextMenu nodeMenu = new System.Windows.Controls.ContextMenu();

            //Remove item
            MenuItem removeItem =
                new MenuItem()
                {
                    Header = this.ViewModel.RemoveNodeCommand.FriendlyName,
                    Command = this.ViewModel.RemoveNodeCommand,
                    IsEnabled = this.ViewModel.RemoveNodeCommand.CanExecute(),
                    Icon = new System.Windows.Controls.Image() { Height = 16, Width = 16, Margin = new Thickness(3, 1, 1, 1), Source = this.ViewModel.RemoveNodeCommand.SmallIcon }
                };
            if (!removeItem.IsEnabled)
            {
                ((System.Windows.Controls.Image)removeItem.Icon).Effect = new Fluent.GrayscaleEffect();
            }
            nodeMenu.Items.Add(removeItem);

            MenuItem editItem =
                new MenuItem()
                {
                    Header = this.ViewModel.EditNodeCommand.FriendlyName,
                    Command = this.ViewModel.EditNodeCommand,
                    IsEnabled = this.ViewModel.EditNodeCommand.CanExecute(),
                    Icon = new System.Windows.Controls.Image() { Height = 16, Width = 16, Margin = new Thickness(3, 1, 1, 1), Source = this.ViewModel.EditNodeCommand.SmallIcon }
                };
            if (!editItem.IsEnabled)
            {
                ((System.Windows.Controls.Image)editItem.Icon).Effect = new Fluent.GrayscaleEffect();
            }
            nodeMenu.Items.Add(editItem);

            nodeMenu.PlacementTarget = clickedNode;
            nodeMenu.StaysOpen = false;
            nodeMenu.IsOpen = true;
        }

        #endregion

        #region Window Close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            _treeController.Nodes.BulkCollectionChanged -= TreeController_NodesBulkCollectionChanged;
            treeMapControl.SelectedItems.BulkCollectionChanged -= TreeMapControl_SelectedItemsBulkCollectionChanged;

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        private void xNodeProductGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAssigned = senderControl?.Name == xNodeAssignedProductsGrid.Name;

            if (e.Key == Key.Return)
            {
                if (isAssigned) ViewModel?.RemoveSelectedProductsCommand?.Execute();
                else ViewModel?.AddSelectedProductsCommand?.Execute();
                Keyboard.Focus(isAssigned ? xNodeAssignedProductsGrid : xNodeUnassignedProductsGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAssigned ? xNodeUnassignedProductsGrid : xNodeAssignedProductsGrid);
                e.Handled = true;
            }
        }
    }
}
