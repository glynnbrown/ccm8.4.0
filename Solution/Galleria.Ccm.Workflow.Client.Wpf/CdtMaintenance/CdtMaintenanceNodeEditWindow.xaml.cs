﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System.Windows;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System;
using System.Windows.Input;


namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Interaction logic for CdtMaintenanceNodeEditWindow.xaml
    /// </summary>
    public partial class CdtMaintenanceNodeEditWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(CdtMaintenanceNodeEditViewModel), typeof(CdtMaintenanceNodeEditWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public CdtMaintenanceNodeEditViewModel ViewModel
        {
            get { return (CdtMaintenanceNodeEditViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceNodeEditWindow senderControl = (CdtMaintenanceNodeEditWindow)obj;

            if (e.OldValue != null)
            {
                CdtMaintenanceNodeEditViewModel oldModel = (CdtMaintenanceNodeEditViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                CdtMaintenanceNodeEditViewModel newModel = (CdtMaintenanceNodeEditViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CdtMaintenanceNodeEditWindow(ConsumerDecisionTreeNode nodeContext)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();
            this.ViewModel = new CdtMaintenanceNodeEditViewModel(nodeContext);

            this.Loaded += new RoutedEventHandler(CdtMaintenanceNodeEditWindow_Loaded);
        }

        private void CdtMaintenanceNodeEditWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= CdtMaintenanceNodeEditWindow_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
