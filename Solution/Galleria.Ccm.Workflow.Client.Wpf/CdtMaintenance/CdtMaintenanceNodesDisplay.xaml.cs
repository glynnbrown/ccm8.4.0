﻿// Copyright © Galleria RTS Ltd 2014
#region Header Information

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Framework.Controls.Wpf.Diagram;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.CdtMaintenance
{
    /// <summary>
    /// Interaction logic for CdtMaintenanceNodesDisplay.xaml
    /// </summary>
    public partial class CdtMaintenanceNodesDisplay : UserControl
    {
        #region Fields
        private Boolean _suppressSelectionChangedHandling;
        #endregion

        #region Properties

        #region UnitViewContextProperty

        public static readonly DependencyProperty RootUnitViewProperty =
            DependencyProperty.Register("RootUnitView", typeof(ConsumerDecisionTreeNodeViewModel),
            typeof(CdtMaintenanceNodesDisplay),
            new PropertyMetadata(null, OnRootUnitViewPropertyChanged));

        private static void OnRootUnitViewPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceNodesDisplay senderControl = (CdtMaintenanceNodesDisplay)obj;

            if (e.OldValue != null)
            {
                ConsumerDecisionTreeNodeViewModel oldValue = (ConsumerDecisionTreeNodeViewModel)e.OldValue;
                oldValue.ChildCollectionChanged -= senderControl.UnitView_ChildCollectionChanged;
            }

            if (e.NewValue != null)
            {
                ConsumerDecisionTreeNodeViewModel newValue = (ConsumerDecisionTreeNodeViewModel)e.NewValue;
                newValue.ChildCollectionChanged += senderControl.UnitView_ChildCollectionChanged;
            }

            senderControl.OnRootNodeChanged();
        }


        /// <summary>
        /// Gets/Sets the root unit for the tree display
        /// </summary>
        public ConsumerDecisionTreeNode RootUnitView
        {
            get { return (ConsumerDecisionTreeNode)GetValue(RootUnitViewProperty); }
            set { SetValue(RootUnitViewProperty, value); }
        }

        #endregion

        #region SelectedUnitProperty

        public static readonly DependencyProperty SelectedUnitProperty =
            DependencyProperty.Register("SelectedUnit", typeof(ConsumerDecisionTreeNodeViewModel), typeof(CdtMaintenanceNodesDisplay),
            new PropertyMetadata(null, OnSelectedUnitPropertyChanged));

        private static void OnSelectedUnitPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CdtMaintenanceNodesDisplay senderControl = (CdtMaintenanceNodesDisplay)obj;
            senderControl.OnSelectedUnitChanged();
        }

        private void OnSelectedUnitChanged()
        {
            //return out if this is suppressed.
            if (_suppressSelectionChangedHandling) { return; }

            _suppressSelectionChangedHandling = true;

            //get the current select unit in the diagram
            CdtMaintenanceNode diagramSelectedNodeBase =
                 (diagram.SelectedItems.Count > 0) ? (CdtMaintenanceNode)diagram.SelectedItems.First() : null;
            ConsumerDecisionTreeNode selectedUnitView = (diagramSelectedNodeBase != null) ?
                diagramSelectedNodeBase.SourceNode : null;

            if (selectedUnitView != this.SelectedUnit)
            {
                //turn off the auto arrange for speed
                diagram.IsAutoArrangeOn = false;

                //clear any existing selection
                diagram.SelectedItems.Clear();


                CdtMaintenanceNode nodeVisual =
                    diagram.Items.Cast<CdtMaintenanceNode>().FirstOrDefault(n => n.SourceNode == this.SelectedUnit);

                if (nodeVisual != null)
                {
                    //add the node to the selection
                    diagram.SelectedItems.Add(nodeVisual);

                    //collapse all nodes to start
                    foreach (CdtMaintenanceNode node in diagram.Items)
                    {
                        diagram.SetItemExpandedState(node, false);
                    }

                    //expand all its parents
                    FrameworkElement currentParentNode =
                       diagram.Links.Where(l => l.EndItem == nodeVisual).Select(l => l.StartItem).FirstOrDefault();

                    while (currentParentNode != null)
                    {
                        diagram.SetItemExpandedState(currentParentNode, true);

                        currentParentNode =
                            diagram.Links.Where(l => l.EndItem == currentParentNode).Select(l => l.StartItem).FirstOrDefault();
                    }

                }

                diagram.IsAutoArrangeOn = true;
                if (nodeVisual != null)
                {
                    //focus on the item
                    diagram.BringItemIntoView(nodeVisual);
                }
            }

            _suppressSelectionChangedHandling = false;

        }

        /// <summary>
        /// Gets/Sets the selected unit of the tree display
        /// </summary>
        public ConsumerDecisionTreeNode SelectedUnit
        {
            get { return (ConsumerDecisionTreeNode)GetValue(SelectedUnitProperty); }
            set { SetValue(SelectedUnitProperty, value); }
        }


        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CdtMaintenanceNodesDisplay()
        {
            InitializeComponent();
        }


        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            diagram.Loaded += new RoutedEventHandler(diagram_Loaded);
        }

        private void diagram_Loaded(object sender, RoutedEventArgs e)
        {
            diagram.Loaded -= diagram_Loaded;
            OnRootNodeChanged();
        }


        /// <summary>
        /// Redraws when the root node changes
        /// </summary>
        private void OnRootNodeChanged()
        {
            diagram.Items.Clear();

            if (this.RootUnitView != null && diagram.IsLoaded)
            {
                //turn off the auto arrange to make loading faster
                diagram.IsAutoArrangeOn = false;

                //draw the root node
                CdtMaintenanceNode rootNode = new CdtMaintenanceNode(this.RootUnitView);
                diagram.Items.Add(rootNode);

                //draw all children down
                DrawChildren(rootNode);

                //update the root offset
                diagramArrangeMethod.MinRootHorizontalOffset = xZoomBox.ViewportWidth / 2 - (rootNode.Width / 4);

                //turn auto arrange back on
                diagram.IsAutoArrangeOn = true;
            }
        }

        /// <summary>
        /// Responds to change of selected node
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void diagram_NodeSelectionChanged(object sender, EventArgs e)
        {
            //return out if this is suppressed
            if (_suppressSelectionChangedHandling) { return; }

            _suppressSelectionChangedHandling = true;

            if (diagram.SelectedItems.Count > 0)
            {
                //update the property to match the diagram
                CdtMaintenanceNode diagramSelectedNodeBase = (CdtMaintenanceNode)diagram.SelectedItems.First();
                this.SelectedUnit = diagramSelectedNodeBase.SourceNode;
            }
            else
            {
                this.SelectedUnit = null;
            }

            _suppressSelectionChangedHandling = false;
        }

        /// <summary>
        /// Redraws if a nodes child collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnitView_ChildCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            ConsumerDecisionTreeNode senderUnitView = (ConsumerDecisionTreeNode)sender;
            CdtMaintenanceNode senderNodeBase = diagram.Items.Cast<CdtMaintenanceNode>()
               .FirstOrDefault(n => n.SourceNode == senderUnitView);

            if (senderNodeBase != null)
            {
                //turn off auto arrange to load faster
                diagram.IsAutoArrangeOn = false;

                //draw the nodes recursively
                DrawChildren(senderNodeBase);


                //turn auto arrange back on
                diagram.IsAutoArrangeOn = true;
            }
        }


        /// <summary>
        /// Reponds to the event of one node being 'dropped' on another
        /// </summary>
        /// <param name="sender">The node hit by the drop</param>
        /// <param name="e"></param>
        private void diagram_ItemDroppedOn(object sender, DiagramNodeDropArgs e)
        {
            CdtMaintenanceNode moveToNode = e.HitItem as CdtMaintenanceNode;

            if (moveToNode != null)
            {
                ConsumerDecisionTreeNode moveToUnitView = moveToNode.SourceNode;

                //[TODO]
                ////only allow if the moveto node is on the same level as the units current parent
                //if (this.SelectedUnit.AssociatedLevel.ParentLevel == moveToUnitView.AssociatedLevel)
                //{
                //    ProductHierarchy ownerStructure = this.RootUnitView.ProductGroup.ParentHierarchy;

                //    //move the unit
                //    moveToUnitView.ProductGroup.ChildList.Add(this.SelectedUnit.ProductGroup);

                //}
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// draws all children beneath the given parent
        /// </summary>
        /// <param name="parent"></param>
        private void DrawChildren(CdtMaintenanceNode parent)
        {
            LocalHelper.ClearDescendantVisuals(diagram, parent);

            foreach (ConsumerDecisionTreeNode childView in parent.SourceNode.ChildList)
            {
                //draw the child
                CdtMaintenanceNode childNode = new CdtMaintenanceNode(childView);
                diagram.Items.Add(childNode);
                diagram.Links.Add(new DiagramItemLink(parent, childNode));

                //recurse
                DrawChildren(childNode);
            }

        }

        #endregion
    }
}
