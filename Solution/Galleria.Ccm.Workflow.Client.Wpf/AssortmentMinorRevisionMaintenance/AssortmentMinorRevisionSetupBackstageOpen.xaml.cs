﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Interaction logic for AssortmentMinorRevisionSetupBackstageOpen.xaml
    /// </summary>
    public sealed partial class AssortmentMinorRevisionSetupBackstageOpen : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentMinorRevisionSetupViewModel), typeof(AssortmentMinorRevisionSetupBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentMinorRevisionSetupViewModel ViewModel
        {
            get { return (AssortmentMinorRevisionSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentMinorRevisionSetupBackstageOpen senderControl = (AssortmentMinorRevisionSetupBackstageOpen)obj;

            if (e.OldValue != null)
            {
                AssortmentMinorRevisionSetupViewModel oldModel = (AssortmentMinorRevisionSetupViewModel)e.OldValue;
            }

            if (e.NewValue != null)
            {
                AssortmentMinorRevisionSetupViewModel newModel = (AssortmentMinorRevisionSetupViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor
        public AssortmentMinorRevisionSetupBackstageOpen()
        {
            InitializeComponent();
        }
        #endregion

        #region Event Handlers

        private void ExtendedDataGrid_RowItemMouseDoubleClick(object sender, Galleria.Framework.Controls.Wpf.ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                AssortmentMinorRevisionInfo itemToOpen = e.RowItem as AssortmentMinorRevisionInfo;
                if (itemToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                }
            }
        }

    
        #endregion
    }
}