﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using System.Windows.Forms;
using Galleria.Ccm.Model;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Binding = System.Windows.Data.Binding;
using Cursors = System.Windows.Input.Cursors;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using Message = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Interaction logic for AssortmentMinorRevisionSetupOrganiser.xaml
    /// </summary>
    public sealed partial class AssortmentMinorRevisionSetupOrganiser : ExtendedRibbonWindow
    {
        #region Constants

        const String RemoveActionCommandKey = "RemoveActionCommand";
        const String RemoveActionLocationCommandKey = "RemoveActionLocationCommand";

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentMinorRevisionSetupViewModel), typeof(AssortmentMinorRevisionSetupOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public AssortmentMinorRevisionSetupViewModel ViewModel
        {
            get { return (AssortmentMinorRevisionSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            AssortmentMinorRevisionSetupOrganiser senderControl = (AssortmentMinorRevisionSetupOrganiser)obj;

            if (e.OldValue != null)
            {
                AssortmentMinorRevisionSetupViewModel oldModel = (AssortmentMinorRevisionSetupViewModel)e.OldValue;
                senderControl.Resources.Remove(RemoveActionCommandKey);
                senderControl.Resources.Remove(RemoveActionLocationCommandKey);
                oldModel.AttachedControl = null;

                oldModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                AssortmentMinorRevisionSetupViewModel newModel = (AssortmentMinorRevisionSetupViewModel)e.NewValue;
                senderControl.Resources.Add(RemoveActionCommandKey, newModel.RemoveActionCommand);
                senderControl.Resources.Add(RemoveActionLocationCommandKey, newModel.RemoveActionLocationCommand);
                newModel.AttachedControl = senderControl;

                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }



        #endregion

        #endregion

        #region Constructor

        public AssortmentMinorRevisionSetupOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.AssortmentMinorRevisionMaintenance);

            this.ViewModel = new AssortmentMinorRevisionSetupViewModel();

            //Load column set on intial load
            LoadColumnSet();

            this.Loaded += AssortmentMinorRevisionSetupOrganiser_Loaded;
        }

        private void AssortmentMinorRevisionSetupOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= AssortmentMinorRevisionSetupOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Reponds to viewmodel property changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            //update the locations grid col set when it becomes visible or the selected action type changes.
            if (e.PropertyName == AssortmentMinorRevisionSetupViewModel.ShowLocationsProperty.Path
                || e.PropertyName == AssortmentMinorRevisionSetupViewModel.SelectedActionTypeProperty.Path)
            {
                LoadActionLocationsGridColumnSet();

                if (this.xActionCommandsStack != null)
                {
                    //nb - this is in code behind as for some reason Grid.Column is not working in a datatrigger setter.
                    if (this.ViewModel.ShowLocations)
                    {
                        Grid.SetColumn(this.xActionCommandsStack, 1);
                        Grid.SetRowSpan(this.xActionCommandsStack, 2);
                        this.xActionCommandsStack.Margin = new Thickness(0, 0, 4, 0);
                        this.xLocationsGridCol.Width = new GridLength(1, GridUnitType.Star);
                    }
                    else
                    {
                        Grid.SetColumn(this.xActionCommandsStack, 2);
                        Grid.SetRowSpan(this.xActionCommandsStack, 1);
                        this.xActionCommandsStack.Margin = new Thickness(0);
                        this.xLocationsGridCol.Width = new GridLength(0, GridUnitType.Auto);
                    }
                }
            }
        }
        /// <summary>
        /// gives focus to the increase/decrease priority buttons on Return key press for 508 compliance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void XActionsGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (!btnIncrease.IsEnabled) return;
                Keyboard.Focus(btnIncrease);
                e.Handled = true;
            }

        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to load the column set
        /// </summary>
        private void LoadColumnSet()
        {
            //Clear existing columns
            this.xActionsGrid.Columns.Clear();

            //Add columns
            this.xActionsGrid.Columns.Add(new DataGridExtendedTextColumn()
            {
                Header = Message.Actions_ActionsGrid_ActionType,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.TypeProperty.Path) { Mode = BindingMode.OneWay },
                IsReadOnly = true
            });

            this.xActionsGrid.Columns.Add(new DataGridExtendedTextColumn()
            {
                Header = Product.GtinProperty.FriendlyName,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.ProductGtinProperty.Path) { Mode = BindingMode.OneWay },
                IsReadOnly = true
            });

            this.xActionsGrid.Columns.Add(new DataGridExtendedTextColumn()
            {
                Header = Product.NameProperty.FriendlyName,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.ProductNameProperty.Path) { Mode = BindingMode.OneWay },
                IsReadOnly = true
            });

            this.xActionsGrid.Columns.Add(new DataGridExtendedTextColumn()
            {
                Header = Message.Actions_ActionsGrid_CDTNode,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.CDTNodeFullNameProperty.Path) { Mode = BindingMode.OneWay },
                IsReadOnly = true
            });

            this.xActionsGrid.Columns.Add(new DataGridExtendedTextColumn()
            {
                Header = Message.Actions_ActionsGrid_ReplacementProductGtin,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.ReplacementProductGtinProperty.Path) { Mode = BindingMode.OneWay },
                IsReadOnly = true
            });

            this.xActionsGrid.Columns.Add(new DataGridExtendedTextColumn()
            {
                Header = Message.Actions_ActionsGrid_ReplacementProductName,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.ReplacementProductNameProperty.Path) { Mode = BindingMode.OneWay },
                IsReadOnly = true
            });

            this.xActionsGrid.Columns.Add(new DataGridExtendedNumericColumn()
            {
                Header = Message.Actions_ActionsGrid_LocationsAffected,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.LocationsAffectedCountProperty.Path) { Mode = BindingMode.OneWay },
                IsReadOnly = true,
                TextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer
            });

            this.xActionsGrid.Columns.Add(new DataGridExtendedNumericColumn()
            {
                Header = AssortmentMinorRevisionListAction.PriorityProperty.FriendlyName,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.PriorityProperty.Path) { Mode = BindingMode.OneWay },
                IsReadOnly = true,
                TextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer
            });

            this.xActionsGrid.Columns.Add(new DataGridExtendedTextColumn()
            {
                Header = AssortmentMinorRevisionListAction.CommentsProperty.FriendlyName,
                Binding = new Binding(MinorRevisionActionWrapperViewModel.CommentsProperty.Path) { Mode = BindingMode.TwoWay, UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged },
                IsReadOnly = false,
                TextInputType = Galleria.Framework.Controls.Wpf.InputType.Alphanumeric,
                MaxLength = 1000
            });

            //Add 'Remove action' column
            DataGridExtendedTemplateColumn removeActionCol = new DataGridExtendedTemplateColumn();
            removeActionCol.Header = String.Empty;
            removeActionCol.CanUserFilter = false;
            removeActionCol.CellTemplate = (DataTemplate)this.Resources["ActionsOrganiser_DTRemoveActionCol"];
            ExtendedDataGrid.SetColumnCellAlignment(removeActionCol, System.Windows.HorizontalAlignment.Center);
            this.xActionsGrid.Columns.Add(removeActionCol);
        }

        /// <summary>
        /// Loads in the columnset for the action locations grid
        /// </summary>
        private void LoadActionLocationsGridColumnSet()
        {
            if (this.xActionLocationsGrid != null
                && this.ViewModel != null)
            {
                //only bother column swapping if the grid is actually visible
                if (this.ViewModel.ShowLocations)
                {
                    //for performance, do a quick check to see if a swap is actually required
                    Int32 expectedColCount =
                        (this.ViewModel.SelectedActionType == AssortmentMinorRevisionActionType.DeList) ? 3 : 5;

                    if (this.xActionLocationsGrid.Columns.Count != expectedColCount)
                    {
                        List<DataGridColumn> columns = new List<DataGridColumn>();

                        //loc code column
                        columns.Add(new DataGridExtendedTextColumn()
                        {
                            Header = Model.Location.CodeProperty.FriendlyName,
                            IsReadOnly = true,
                            Binding = new Binding(MinorRevisionActionLocationWrapperViewModel.LocationCodeProperty.Path) { Mode = BindingMode.OneWay },
                            SortDirection = ListSortDirection.Ascending,
                            Width = 80
                        });

                        //loc name column
                        columns.Add(new DataGridExtendedTextColumn()
                        {
                            Header = Model.Location.NameProperty.FriendlyName,
                            IsReadOnly = true,
                            Binding = new Binding(MinorRevisionActionLocationWrapperViewModel.LocationNameProperty.Path) { Mode = BindingMode.OneWay },
                            Width = new DataGridLength(1, DataGridLengthUnitType.Star)
                        });

                        //add facings and units columns if action is not delist
                        if (this.ViewModel.SelectedActionType != AssortmentMinorRevisionActionType.DeList)
                        {
                            //units column
                            columns.Add(new DataGridExtendedTextColumn()
                            {
                                Header = Message.Actions_ActionLocationsGrid_Units,
                                Binding = new Binding(MinorRevisionActionLocationWrapperViewModel.UnitsProperty.Path)
                                {
                                    Mode = BindingMode.TwoWay,
                                    UpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged,
                                    ValidatesOnDataErrors = true
                                },
                                TextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer,
                            });

                            //facings column
                            columns.Add(new DataGridExtendedTextColumn()
                            {
                                Header = Message.Actions_ActionLocationsGrid_Facings,
                                Binding = new Binding(MinorRevisionActionLocationWrapperViewModel.FacingsProperty.Path)
                                {
                                    Mode = BindingMode.TwoWay,
                                    UpdateSourceTrigger = System.Windows.Data.UpdateSourceTrigger.PropertyChanged,
                                    ValidatesOnDataErrors = true
                                },
                                TextInputType = Galleria.Framework.Controls.Wpf.InputType.Integer,
                            });

                        }

                        //add a remove row column
                        columns.Add(new DataGridExtendedTemplateColumn()
                        {
                            Header = null,
                            Width = 40,
                            ColumnCellAlignment = System.Windows.HorizontalAlignment.Center,
                            CanUserFilter = false,
                            CanUserHide = false,
                            CellTemplate = this.Resources["ActionsOrganiser_DTRemoveActionLocationCol"] as DataTemplate
                        });


                        //clear out the existing columns
                        if (this.xActionLocationsGrid.Columns.Count > 0)
                        {
                            this.xActionLocationsGrid.Columns.Clear();
                        }

                        //add the new ones
                        columns.ForEach(c => this.xActionLocationsGrid.Columns.Add(c));
                    }
                }
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);
        }

        #endregion
    
    }
}

