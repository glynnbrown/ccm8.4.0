﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25455 : J.Pickup
//  Created (Copied over from GFS).
// CCM-26303 : J.Pickup
//  fixed bug with save as. Should now save with db constraints applied.
// CCM-26286 : J.Pickup
//  Added additional save & new, save and close ribbon buttons
// V8-26301 : J.Pickup
//  Removed financial hierarhcy as a search option (doesn't exist in CCM). 
// CCM-26140 : I.George
//  Added FetchByEntityIncludingDeleted to the save Command
// CCM-27720 : I.George
//  Made ProductGroupId not nullable 
// CCM-27861 : J.Pickup
//  Location Bar Button now resets during new / open command.
#endregion
#region Version History: (CCM 8.0.3)
// CCM-29214 : D.Pleasance
//  Amended reference to FetchByEntityId from FetchByEntityIdIncludingDeleted since we now deleted outright
#endregion
#region Version History: (CCM 8.1.0)
// V8-30252 : N.Haywood
//  Removed limit on backstage open load
#endregion
#region Version History: (CCM 8.1.1)
// V8-30325 : I.George
//  Added friendly description to delete command
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    public enum MinorRevisionProductSearchType
    {
        Criteria,
        ProductUniverse,
        ConsumerDecisionTree
    }
    public static class MinorRevisionProductSearchTypeHelper
    {
        public static readonly Dictionary<MinorRevisionProductSearchType, String> FriendlyNames =
            new Dictionary<MinorRevisionProductSearchType, String>()
            {
                {MinorRevisionProductSearchType.Criteria, Message.Enum_MinorRevisionProductSearchType_Criteria },
                {MinorRevisionProductSearchType.ProductUniverse, Message.Enum_MinorRevisionProductSearchType_ProductUniverse },
                {MinorRevisionProductSearchType.ConsumerDecisionTree, Message.Enum_MinorRevisionProductSearchType_ConsumerDecisionTree },
            };
    }

    public sealed class AssortmentMinorRevisionSetupViewModel : ViewModelAttachedControlObject<AssortmentMinorRevisionSetupOrganiser>
    {
        #region Constants
        private const String _gibraltarCategory = "AssortmentMinorRevision";
        private const Int32 MaxRetryCount = 3;
        private const Int32 MaxBackstageItemsFetch = 1001;
        #endregion

        #region Fields

        private AssortmentMinorRevision _selectedAssortmentMinorRevision = null;
        private BulkObservableCollection<ActionProduct> _actionProducts = new BulkObservableCollection<ActionProduct>();

        private readonly AssortmentMinorRevisionInfoListViewModel _masterAssortmentMinorRevisionInfoListView = new AssortmentMinorRevisionInfoListViewModel(); // the master list of current assortments
        private BulkObservableCollection<AssortmentMinorRevisionInfo> _availableAssortmentMinorRevisions =
                    new BulkObservableCollection<AssortmentMinorRevisionInfo>(); // the list of available assortments
        private ReadOnlyBulkObservableCollection<AssortmentMinorRevisionInfo> _availableAssortmentsMinorRevisionRO;

        private readonly ConsumerDecisionTreeInfoListViewModel _consumerDecisionTreeInfoListView = new ConsumerDecisionTreeInfoListViewModel();
        private ConsumerDecisionTreeInfo _selectedConsumerDecisionTree;

        private BulkObservableCollection<MinorRevisionActionWrapperViewModel> _actions = new BulkObservableCollection<MinorRevisionActionWrapperViewModel>();
        private ReadOnlyBulkObservableCollection<MinorRevisionActionWrapperViewModel> _actionsRO;
        private BulkObservableCollection<MinorRevisionActionLocationWrapperViewModel> _actionLocations = new BulkObservableCollection<MinorRevisionActionLocationWrapperViewModel>();
        private ReadOnlyBulkObservableCollection<MinorRevisionActionLocationWrapperViewModel> _actionLocationsRO;

        private MinorRevisionActionWrapperViewModel _selectedAction;
        private AssortmentMinorRevisionActionType? _selectedActionType = null;

        private Int32 _numberOfLocationsAffected = 0;
        private ModalBusy _busyDialog;

        //cdt nodes
        private BulkObservableCollection<ConsumerDecisionTreeNode> _availableCDTNodes = new BulkObservableCollection<ConsumerDecisionTreeNode>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> _availableCDTNodesRO;

        private Boolean _showLocations = false;
        private String _filterItemsKey;
        private String _filterItemsCountMessage;
        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath SelectedAssortmentMinorRevisionProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SelectedAssortmentMinorRevision);
        public static readonly PropertyPath AvailableAssortmentsProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.AvailableAssortmentMinorRevisionInfos);
        public static readonly PropertyPath SelectedMerchandisingGroupProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SelectedMerchandisingGroup);
        public static readonly PropertyPath SelectedConsumerDecisonTreeProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SelectedConsumerDecisonTree);
        public static readonly PropertyPath IsReadOnlyProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.IsReadOnly);
        public static readonly PropertyPath ActionsProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.Actions);
        public static readonly PropertyPath ActionLocationsProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.ActionLocations);
        public static readonly PropertyPath SelectedActionProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SelectedAction);
        public static readonly PropertyPath SelectedActionTypeProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SelectedActionType);
        public static readonly PropertyPath NumberOfLocationsAffectedProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.NumberOfLocationsAffected);
        public static readonly PropertyPath ShowLocationsProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.ShowLocations);
        public static readonly PropertyPath FilterItemsKeyProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.FilterItemsKey);
        public static readonly PropertyPath FilterItemsCountMessageProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.FilterItemsCountMessage);
        public static readonly PropertyPath AvailableCDTNodesProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.AvailableCDTNodes);
        
        
        //commands
        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(P => P.DeleteCommand);
        public static readonly PropertyPath IncreaseActionPriorityCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.IncreaseActionPriorityCommand);
        public static readonly PropertyPath DecreaseActionPriorityCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.DecreaseActionPriorityCommand);
        public static readonly PropertyPath RemoveActionCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.RemoveActionCommand);
        public static readonly PropertyPath RemoveActionLocationCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.RemoveActionLocationCommand);
        public static readonly PropertyPath ExportActionsCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.ExportActionsCommand);
        public static readonly PropertyPath AdvancedExportActionsCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.AdvancedExportActionsCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<AssortmentMinorRevisionSetupViewModel>(p => p.SaveAndNewCommand);

        #endregion

        #region Properties

        /// <summary>
        /// The currently selected assortment minor revision
        /// </summary>
        public AssortmentMinorRevision SelectedAssortmentMinorRevision
        {
            get
            {
                return _selectedAssortmentMinorRevision;
            }
            set
            {
                AssortmentMinorRevision oldValue = _selectedAssortmentMinorRevision;
                _selectedAssortmentMinorRevision = value;
                OnPropertyChanged(SelectedAssortmentMinorRevisionProperty);
                OnPropertyChanged(IsReadOnlyProperty);

                OnSelectedAssortmentChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Determines Read Only criteria based upon state of model
        /// </summary>
        public Boolean IsReadOnly
        {
            get
            {//Galleria.Framework.Controls.Wpf.ResourceKeys.ThemeColour_BrushBorder
                if (this.SelectedAssortmentMinorRevision == null || this.SelectedAssortmentMinorRevision.IsNew)
                {
                    return !_userHasAssortmentCreatePerm;
                }
                else
                {
                    return !_userHasAssortmentEditPerm;
                }
            }
        }

        /// <summary>
        /// Returns the collection of available assortments
        /// </summary>
        public ReadOnlyBulkObservableCollection<AssortmentMinorRevisionInfo> AvailableAssortmentMinorRevisionInfos
        {
            get
            {
                if (_availableAssortmentsMinorRevisionRO == null)
                {
                    _availableAssortmentsMinorRevisionRO =
                        new ReadOnlyBulkObservableCollection<AssortmentMinorRevisionInfo>(_availableAssortmentMinorRevisions);

                }
                return _availableAssortmentsMinorRevisionRO;
            }
        }

        /// <summary>
        /// Returns the merchandising group to which this assortment is assigned
        /// </summary>
        public ProductGroupInfo SelectedMerchandisingGroup
        {
            get
            {
                return _selectedMerchandisingGroup;
            }
            set
            {
                _selectedMerchandisingGroup = value;
                OnPropertyChanged(SelectedMerchandisingGroupProperty);

                if (value != null)
                {
                    this.SelectedAssortmentMinorRevision.ProductGroupId = _selectedMerchandisingGroup.Id;
                }
            }
        }

        /// <summary>
        /// Returns the consumer decision tree to which this assortment is assigned
        /// </summary>
        public ConsumerDecisionTreeInfo SelectedConsumerDecisonTree
        {
            get { return _selectedConsumerDecisionTree; }
            set
            {
                _selectedConsumerDecisionTree = value;

                Int32? newId = (value != null) ? (Int32?)value.Id : null;

                if (this.SelectedAssortmentMinorRevision != null
                    && this.SelectedAssortmentMinorRevision.ConsumerDecisionTreeId != newId)
                {
                    this.SelectedAssortmentMinorRevision.ConsumerDecisionTreeId = newId;
                }

                OnPropertyChanged(SelectedConsumerDecisonTreeProperty);
            }
        }

        /// <summary>
        /// Returns the collection of scenario actions
        /// </summary>
        public ReadOnlyBulkObservableCollection<MinorRevisionActionWrapperViewModel> Actions
        {
            get
            {
                if (_actionsRO == null)
                {
                    _actionsRO = new ReadOnlyBulkObservableCollection<MinorRevisionActionWrapperViewModel>(_actions);
                }
                return _actionsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected action
        /// </summary>
        public MinorRevisionActionWrapperViewModel SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                MinorRevisionActionWrapperViewModel oldValue = _selectedAction;
                _selectedAction = value;
                OnSelectedActionChanged(_selectedAction, oldValue);
                OnPropertyChanged(SelectedActionProperty);
            }
        }

        /// <summary>
        /// Returns the current selection action type
        /// </summary>
        public AssortmentMinorRevisionActionType? SelectedActionType
        {
            get { return _selectedActionType; }
        }

        /// <summary>
        /// Returns the collection of scenario action locations
        /// </summary>
        public ReadOnlyBulkObservableCollection<MinorRevisionActionLocationWrapperViewModel> ActionLocations
        {
            get
            {
                if (_actionLocationsRO == null)
                {
                    _actionLocationsRO = new ReadOnlyBulkObservableCollection<MinorRevisionActionLocationWrapperViewModel>(_actionLocations);
                }
                return _actionLocationsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the number of locations affected
        /// </summary>
        public Int32 NumberOfLocationsAffected
        {
            get { return _numberOfLocationsAffected; }
            set
            {
                _numberOfLocationsAffected = value;
                OnPropertyChanged(NumberOfLocationsAffectedProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available CDT Nodes
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> AvailableCDTNodes
        {
            get
            {
                if (_availableCDTNodesRO == null)
                {
                    _availableCDTNodesRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode>(_availableCDTNodes);
                }
                return _availableCDTNodesRO;
            }
        }

        /// <summary>
        /// Boolean to determine if the location pannel should be shown
        /// </summary>
        public Boolean ShowLocations
        {
            get
            {
                return _showLocations && this.SelectedAction != null;
            }
            set
            {
                _showLocations = value;
                OnPropertyChanged(ShowLocationsProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the fetch criteria for backstage item fetch
        /// </summary>
        public String FilterItemsKey
        {
            get { return _filterItemsKey; }
            set
            {
                _filterItemsKey = value;
                OnPropertyChanged(FilterItemsKeyProperty);

                UpdateAvailableAssortmentMinorRevisionItems();
            }
        }

        /// <summary>
        /// Gets/Sets the filtered grid items count message
        /// </summary>
        public String FilterItemsCountMessage
        {
            get { return _filterItemsCountMessage; }
            private set
            {
                _filterItemsCountMessage = value;
                OnPropertyChanged(FilterItemsCountMessageProperty);
            }
        }

       
        #endregion

        #region Constructors

        /// <summary>
        /// Testing Constructor
        /// </summary>
        public AssortmentMinorRevisionSetupViewModel()
        {
            UpdatePermissionFlags();

            //assortment info list view           
            _masterAssortmentMinorRevisionInfoListView.ModelChanged += MasterAssortmentMinorRevisionInfoListView_ModelChanged;
            _masterAssortmentMinorRevisionInfoListView.FetchForEntity(App.ViewState.EntityId);

            //consumer decision tree info list view
            _consumerDecisionTreeInfoListView.ModelChanged += ConsumerDecisionTreeInfoListView_ModelChanged;
            _consumerDecisionTreeInfoListView.FetchForCurrentEntity();

            //create a new Assortment Minor Revision
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new item
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.AssortmentSetup_NewCommand_Tooltip,
                        InputGestureKey = Key.N,
                        InputGestureModifiers = ModifierKeys.Control,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16
                    };
                    base.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userHasAssortmentCreatePerm)
            {
                this.NewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                this.SelectedAssortmentMinorRevision = AssortmentMinorRevision.NewAssortmentMinorRevision(App.ViewState.EntityId);
                this.SelectedAssortmentMinorRevision.MarkGraphAsInitialized();

                ShowLocations = false;

                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Opens assortment for the given id
        /// param: Int32 the content id to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                        DisabledReason = Message.Assortment_OpenCommand_DisabledReason
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? AssortmentMinorRevisonId)
        {
            //id must not be null
            if (!AssortmentMinorRevisonId.HasValue)
            {
                return false;
            }

            //user must have fetch permission
            if (!_userHasAssortmentFetchPerm)
            {
                this.OpenCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? AssortmentMinorRevisonId)
        {
            if (this.ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //fetch the item
                    this.SelectedAssortmentMinorRevision = AssortmentMinorRevision.GetById(AssortmentMinorRevisonId.Value);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _gibraltarCategory);
                    if (this.AttachedControl != null)
                    {
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            String.Empty, OperationType.Open,
                            this.AttachedControl);
                    }

                    base.ShowWaitCursor(true);
                }

                ShowLocations = false;

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region Save

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.SelectedAssortmentMinorRevision == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            // Product group Id may not be null
            if (this.SelectedAssortmentMinorRevision.ProductGroupId == null)
            {
                this.SaveCommand.DisabledReason = Message.Assortment_SaveDisabled_NoCategory;
                return false;
            }

            //user must have edit permission
            if (!_userHasAssortmentEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.SelectedAssortmentMinorRevision.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }
            return true;
        }

        private void Save_Executed()
        {
            base.ShowWaitCursor(true);

            if (SaveCurrentItem())
            {
                attemptSave();
            }

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Calls a save on the current item
        /// </summary>
        /// <returns>true of successful, false if the save was cancelled</returns>
        private Boolean SaveCurrentItem()
        {
            String newName;
            Boolean nameAccepted = true;
            Int32 id = this.SelectedAssortmentMinorRevision.Id;

            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck = (s) =>
                    {
                        Boolean returnValue = true;
                        base.ShowWaitCursor(true);

                        foreach (AssortmentMinorRevisionInfo AssoInfo in AssortmentMinorRevisionInfoList.FetchByEntityId(App.ViewState.EntityId))
                        {
                            if (AssoInfo.Id != id && AssoInfo.Name.ToLowerInvariant() == s.ToLowerInvariant())
                            {
                                returnValue = false;
                                break;
                            }
                        }
                        base.ShowWaitCursor(false);
                        return returnValue;
                    };
                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptUserForValue(String.Empty, String.Empty,
                    Message.AssortmentMinorRevision_CodeAlreadyInUse,
                    Assortment.NameProperty.FriendlyName, 50, false, isUniqueCheck, this.SelectedAssortmentMinorRevision.Name, out newName);
            }
            else
            {
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedAssortmentMinorRevision.Name,
                   this.AvailableAssortmentMinorRevisionInfos.Where(p => p.Id != id).Select(p => p.Name));
            }

            if (nameAccepted)
            {
                if (this.SelectedAssortmentMinorRevision.Name != newName)
                {
                    SelectedAssortmentMinorRevision.Name = newName;
                }
            }

            return nameAccepted;
        }

        private void attemptSave()
        {
            try
            {
                //Save_CanExecute Current Item
                this.SelectedAssortmentMinorRevision = this.SelectedAssortmentMinorRevision.Save();

            }
            catch (DataPortalException ex)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(ex, _gibraltarCategory);
                Exception rootException = ex.GetBaseException();

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedAssortmentMinorRevision.Name);
                    if (itemReloadRequired)
                    {
                        this.SelectedAssortmentMinorRevision = AssortmentMinorRevision.GetById(this.SelectedAssortmentMinorRevision.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    if (this.AttachedControl != null)
                    {
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                        this.SelectedAssortmentMinorRevision.Name, OperationType.Save,
                        this.AttachedControl);
                    }
                }
            }
            //Update backstage list
            _masterAssortmentMinorRevisionInfoListView.FetchForEntity(App.ViewState.EntityId);

        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a completely new one
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureKey = Key.F12,
                        InputGestureModifiers = ModifierKeys.None
                    };
                    base.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //may not be null
            if (this.SelectedAssortmentMinorRevision == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have create permission
            if (!_userHasAssortmentCreatePerm)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            if (this.SelectedAssortmentMinorRevision.ProductGroupId == null)
            {
                this.SaveAsCommand.DisabledReason = Message.Assortment_SaveDisabled_NoCategory;
                return false;
            }

            //must be valid
            if (!this.SelectedAssortmentMinorRevision.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }
            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the name to save as
            String copyName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                (s) =>
                {
                    return !this.AvailableAssortmentMinorRevisionInfos.Select(p => p.Name).Contains(s);
                };

                nameAccepted = Galleria.Framework.Controls.Wpf.Helpers.PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            }
            else
            {
                //force a unique value for unit testing
                copyName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedAssortmentMinorRevision.Name, this.AvailableAssortmentMinorRevisionInfos.Select(p => p.Name));
            }

            if (nameAccepted)
            {
                base.ShowWaitCursor(true);

                //copy the item and save
                AssortmentMinorRevision itemCopy = this.SelectedAssortmentMinorRevision.Copy();
                itemCopy.Name = copyName;
                itemCopy.UniqueContentReference = Guid.NewGuid();

                itemCopy = itemCopy.Save();

                //set the copy as the new selected item
                this.SelectedAssortmentMinorRevision = itemCopy;

                //update the available infos
                _masterAssortmentMinorRevisionInfoListView.FetchForEntity(App.ViewState.EntityId);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Deletes the item
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(),
                        p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            //must not be null
            if (this.SelectedAssortmentMinorRevision == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have delete permission
            if (!_userHasAssortmentDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be new
            if (this.SelectedAssortmentMinorRevision.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(
                    this.SelectedAssortmentMinorRevision.ToString());
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the scheme as deleted
                AssortmentMinorRevision itemToDelete = this.SelectedAssortmentMinorRevision;
                itemToDelete.Delete();

                //load a new item
                NewCommand.Execute();

                //commit the delete
                try
                {
                    //commit the delete
                    AssortmentMinorRevision.DeleteById(itemToDelete.Id);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _gibraltarCategory);
                    if (this.AttachedControl != null)
                    {
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            itemToDelete.Name, OperationType.Delete,
                            this.AttachedControl);
                    }

                    base.ShowWaitCursor(true);
                }

                ShowLocations = false;

                _masterAssortmentMinorRevisionInfoListView.FetchForEntity(App.ViewState.EntityId);

                base.ShowWaitCursor(false);
            }


        }

        #endregion

        #region SaveAndNew

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Saves the current item and creates a new one
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16,
                        DisabledReason = SaveCommand.DisabledReason
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                attemptSave();
                NewCommand.Execute();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveAndClose

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16,
                        DisabledReason = SaveCommand.DisabledReason
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean itemSaved = SaveCurrentItem();

            base.ShowWaitCursor(false);

            //if save completed
            if (itemSaved)
            {
                attemptSave();

                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }

        }

        #endregion

        #region AddLinkedProductHierarchyCommand

        private RelayCommand _addLinkedProductHierarchyCommand;

        /// <summary>
        /// Add related hierarchy product
        /// </summary>
        public RelayCommand AddLinkedProductHierarchyCommand
        {
            get
            {
                if (_addLinkedProductHierarchyCommand == null)
                {
                    _addLinkedProductHierarchyCommand =
                        new RelayCommand(p => LinkedProductHierarchy_Executed(), p => LinkedProductHierarchy_CanExecute())
                        {
                            FriendlyName = Message.Assortment_AddLinkedLocationHierarchyCommand
                        };
                    this.ViewModelCommands.Add(_addLinkedProductHierarchyCommand);
                }
                return _addLinkedProductHierarchyCommand;
            }
        }

        private Boolean LinkedProductHierarchy_CanExecute()
        {
            //user must have fetch permission
            if (!Csla.Rules.BusinessRules.HasPermission
                (Csla.Rules.AuthorizationActions.GetObject, typeof(ProductHierarchy)))
            {
                this.AddLinkedProductHierarchyCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            if (this.SelectedAssortmentMinorRevision != null)
            {
                if (this.SelectedAssortmentMinorRevision.IsNew)
                {
                    //user must have create permission
                    if (!_userHasAssortmentCreatePerm)
                    {
                        this.AddLinkedProductHierarchyCommand.DisabledReason = Message.Generic_NoCreatePermission;
                        return false;
                    }
                }
                else
                {
                    //user must have edit permission
                    if (!_userHasAssortmentEditPerm)
                    {
                        this.AddLinkedProductHierarchyCommand.DisabledReason = Message.Generic_NoEditPermission;
                        return false;
                    }
                }
            }

            return true;
        }

        private void LinkedProductHierarchy_Executed()
        {
            if (this.AttachedControl != null)
            {
                MerchGroupSelectionWindow merchandisingHierarchyWindow = new MerchGroupSelectionWindow();
                merchandisingHierarchyWindow.SelectionMode = DataGridSelectionMode.Single;

                App.ShowWindow(merchandisingHierarchyWindow, this.AttachedControl, true);

                if (merchandisingHierarchyWindow.DialogResult == true)
                {
                    this.SelectedMerchandisingGroup = ProductGroupInfo.NewProductGroupInfo(merchandisingHierarchyWindow.SelectionResult[0]);
                }
            }
        }

        #endregion

        #region AddLinkedConsumerDecisionTreeCommand

        private RelayCommand _addLinkedConsumerDecisionTreeCommand;

        /// <summary>
        /// Add related hierarchy product
        /// </summary>
        public RelayCommand AddLinkedConsumerDecisionTreeCommand
        {
            get
            {
                if (_addLinkedConsumerDecisionTreeCommand == null)
                {
                    _addLinkedConsumerDecisionTreeCommand =
                        new RelayCommand(p => LinkedConsumerDecisionTree_Executed(), p => LinkedConsumerDecisionTree_CanExecute())
                        {
                            FriendlyName = Message.Assortment_AddLinkedLocationHierarchyCommand
                        };
                    this.ViewModelCommands.Add(_addLinkedConsumerDecisionTreeCommand);
                }
                return _addLinkedConsumerDecisionTreeCommand;
            }
        }

        private Boolean LinkedConsumerDecisionTree_CanExecute()
        {
            //user must have fetch permission
            if (!Csla.Rules.BusinessRules.HasPermission
                (Csla.Rules.AuthorizationActions.GetObject, typeof(ConsumerDecisionTreeInfo)))
            {
                this.AddLinkedConsumerDecisionTreeCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            if (this.SelectedAssortmentMinorRevision != null)
            {
                if (this.SelectedAssortmentMinorRevision.IsNew)
                {
                    //user must have create permission
                    if (!_userHasAssortmentCreatePerm)
                    {
                        this.AddLinkedConsumerDecisionTreeCommand.DisabledReason = Message.Generic_NoCreatePermission;
                        return false;
                    }
                }
                else
                {
                    //user must have edit permission
                    if (!_userHasAssortmentEditPerm)
                    {
                        this.AddLinkedConsumerDecisionTreeCommand.DisabledReason = Message.Generic_NoEditPermission;
                        return false;
                    }
                }
            }

            return true;
        }

        private void LinkedConsumerDecisionTree_Executed()
        {
            if (this.AttachedControl != null)
            {
                ConsumerDecisionTreeWindow consumerDecisionTreeWindow = new ConsumerDecisionTreeWindow();
                App.ShowWindow(consumerDecisionTreeWindow, this.AttachedControl, true);

               // throw new NotImplementedException();

                if (consumerDecisionTreeWindow.DialogResult == true)
                {
                    this.SelectedConsumerDecisonTree = consumerDecisionTreeWindow.SelectionResult;
                }
            }
        }

        #endregion

        #region ListProductMinorReviewCommand

        private void OpenListProductMinorReviewWindow(ConsumerDecisionTreeInfo consumerDecisionTree)
        {
            if (this.AttachedControl != null)
            {
                Window existingWin = Galleria.Framework.Controls.Wpf.Helpers.GetWindow<ListProductWindow>();
                if (existingWin == null)
                {
                    //show a new window
                    ListProductWindow window = new ListProductWindow(SelectedAssortmentMinorRevision, consumerDecisionTree);
                    App.ShowWindow(window, this.AttachedControl, true);
                }
                else
                {
                    //activate the existign window
                    if (existingWin.WindowState == WindowState.Minimized)
                    {
                        existingWin.WindowState = WindowState.Normal;
                    }
                    existingWin.Activate();
                }
            }
        }

        private RelayCommand _listProductMinorReviewCommand;

        /// <summary>
        /// Add Product via the wizard
        /// </summary>
        public RelayCommand ListProductMinorReviewCommand
        {
            get
            {
                if (_listProductMinorReviewCommand == null)
                {
                    _listProductMinorReviewCommand = new RelayCommand(
                        p => ListProductMinorReview_Executed(),
                        p => ListProductMinorReview_CanExecute())
                    {
                        FriendlyName = Message.MinorReview_ListProduct,
                        FriendlyDescription = Message.MinorReview_ListProduct_Description,
                        Icon = ImageResources.MinorReview_ListProduct
                    };
                    base.ViewModelCommands.Add(_listProductMinorReviewCommand);
                }
                return _listProductMinorReviewCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ListProductMinorReview_CanExecute()
        {
            //must have an item selected
            if (this.SelectedAssortmentMinorRevision == null)
            {
                this.ListProductMinorReviewCommand.DisabledReason = Message.AssortmentMinorRevisionSetup_NoAssortmentSelectedWarning;
                return false;
            }

            return true;
        }

        private void ListProductMinorReview_Executed()
        {
            if (this.AttachedControl != null)
            {
                if (this.SelectedAssortmentMinorRevision.ConsumerDecisionTreeId.HasValue)
                {
                    OpenListProductMinorReviewWindow(SelectedConsumerDecisonTree);
                }
                else
                {
                    OpenListProductMinorReviewWindow(null);
                }
            }

            LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
        }

        #endregion

        #region DeListProductMinorReviewCommand

        private RelayCommand _deListProductMinorReviewCommand;

        /// <summary>
        /// Delist Product via the wizard
        /// </summary>
        public RelayCommand DeListProductMinorReviewCommand
        {
            get
            {
                if (_deListProductMinorReviewCommand == null)
                {
                    _deListProductMinorReviewCommand = new RelayCommand(
                        p => DeListProductMinorReview_Executed(),
                        p => DeListProductMinorReview_CanExecute())
                    {
                        FriendlyName = Message.MinorReview_DeListProduct,
                        FriendlyDescription = Message.MinorReview_DeListProduct_Description,
                        Icon = ImageResources.MinorReview_DeListProduct
                    };
                    base.ViewModelCommands.Remove(_deListProductMinorReviewCommand);
                }
                return _deListProductMinorReviewCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean DeListProductMinorReview_CanExecute()
        {
            //must have an item selected
            if (this.SelectedAssortmentMinorRevision == null)
            {
                this.DeListProductMinorReviewCommand.DisabledReason = Message.AssortmentMinorRevisionSetup_NoAssortmentSelectedWarning;
                return false;
            }

            return true;
        }

        private void DeListProductMinorReview_Executed()
        {
            if (this.AttachedControl != null)
            {
                Window existingWin = Galleria.Framework.Controls.Wpf.Helpers.GetWindow<DeListProductWindow>();
                if (existingWin == null)
                {
                    //show a new window
                    DeListProductWindow window = new DeListProductWindow(SelectedAssortmentMinorRevision, this.SelectedConsumerDecisonTree);
                    App.ShowWindow(window, this.AttachedControl, true);
                }
                else
                {
                    //activate the existign window
                    if (existingWin.WindowState == WindowState.Minimized)
                    {
                        existingWin.WindowState = WindowState.Normal;
                    }
                    existingWin.Activate();
                }
            }
        }

        #endregion

        #region AmendDistributionMinorReviewCommand

        private RelayCommand _amendDistributionMinorReviewCommand;

        /// <summary>
        /// Amend distribution via the wizard
        /// </summary>
        public RelayCommand AmendDistributionMinorReviewCommand
        {
            get
            {
                if (_amendDistributionMinorReviewCommand == null)
                {
                    _amendDistributionMinorReviewCommand = new RelayCommand(
                        p => AmendDistributonMinorReview_Executed(),
                        p => AmendDistributonMinorReview_CanExecute())
                    {
                        FriendlyName = Message.MinorReview_AmendDistribution,
                        FriendlyDescription = Message.MinorReview_AmendDistribution_Description,
                        Icon = ImageResources.MinorReview_AmendDistribution
                    };
                    base.ViewModelCommands.Add(_amendDistributionMinorReviewCommand);
                }
                return _amendDistributionMinorReviewCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AmendDistributonMinorReview_CanExecute()
        {
            //must have an item selected
            if (this.SelectedAssortmentMinorRevision == null)
            {
                this.AmendDistributionMinorReviewCommand.DisabledReason = Message.AssortmentMinorRevisionSetup_NoAssortmentSelectedWarning;
                return false;
            }

            return true;
        }

        private void AmendDistributonMinorReview_Executed()
        {
            if (this.AttachedControl != null)
            {
                Window existingWin = Galleria.Framework.Controls.Wpf.Helpers.GetWindow<AmendDistributionWindow>();
                if (existingWin == null)
                {
                    //show a new window
                    AmendDistributionWindow window = new AmendDistributionWindow(SelectedAssortmentMinorRevision, this.SelectedConsumerDecisonTree);
                    App.ShowWindow(window, this.AttachedControl, true);
                }
                else
                {
                    //activate the existign window
                    if (existingWin.WindowState == WindowState.Minimized)
                    {
                        existingWin.WindowState = WindowState.Normal;
                    }
                    existingWin.Activate();
                }
            }
        }

        #endregion

        #region ReplaceProductMinorReviewCommand

        private RelayCommand _replaceProductMinorReviewCommand;

        /// <summary>
        /// Amend distribution via the wizard
        /// </summary>
        public RelayCommand ReplaceProductMinorReviewCommand
        {
            get
            {
                if (_replaceProductMinorReviewCommand == null)
                {
                    _replaceProductMinorReviewCommand = new RelayCommand(
                        p => ReplaceProductMinorReview_Executed(),
                        p => ReplaceProductMinorReview_CanExecute())
                    {
                        FriendlyName = Message.MinorReview_ReplaceProduct,
                        FriendlyDescription = Message.MinorReview_ReplaceProduct_Description,
                        Icon = ImageResources.MinorReview_ReplaceProduct
                    };
                    base.ViewModelCommands.Add(_replaceProductMinorReviewCommand);
                }
                return _replaceProductMinorReviewCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ReplaceProductMinorReview_CanExecute()
        {
            //must have an item selected
            if (this.SelectedAssortmentMinorRevision == null)
            {
                this.ReplaceProductMinorReviewCommand.DisabledReason = Message.AssortmentMinorRevisionSetup_NoAssortmentSelectedWarning;
                return false;
            }

            return true;
        }

        private void ReplaceProductMinorReview_Executed()
        {
            if (this.AttachedControl != null)
            {
                Window existingWin = Galleria.Framework.Controls.Wpf.Helpers.GetWindow<AmendDistributionWindow>();
                if (existingWin == null)
                {
                    //show a new window
                    ReplaceProductWindow window = new ReplaceProductWindow(SelectedAssortmentMinorRevision, this.SelectedConsumerDecisonTree);
                    App.ShowWindow(window, this.AttachedControl, true);
                }
                else
                {
                    //activate the existign window
                    if (existingWin.WindowState == WindowState.Minimized)
                    {
                        existingWin.WindowState = WindowState.Normal;
                    }
                    existingWin.Activate();
                }
            }
        }

        #endregion

        #region RemoveActionCommand

        private RelayCommand _removeActionCommand;

        /// <summary>
        /// Remove the action
        /// </summary>
        public RelayCommand RemoveActionCommand
        {
            get
            {
                if (_removeActionCommand == null)
                {
                    _removeActionCommand = new RelayCommand(
                        p => RemoveAction_Executed(),
                        p => RemoveAction_CanExecute())
                    {
                        FriendlyName = Message.Actions_RemoveAction,
                        FriendlyDescription = Message.Actions_RemoveAction_Description,
                        SmallIcon = ImageResources.Actions_RemoveAction
                    };
                    base.ViewModelCommands.Add(_removeActionCommand);
                }
                return _removeActionCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RemoveAction_CanExecute()
        {
            return (this.SelectedAction != null);
        }

        private void RemoveAction_Executed()
        {
            //Remove action
            if (this.SelectedAction != null)
            {
                if (this.SelectedActionType.HasValue)
                {
                    if (this.SelectedActionType.Value == AssortmentMinorRevisionActionType.AmendDistribution)
                    {
                        //Try and cast to correct type
                        AssortmentMinorRevisionAmendDistributionAction distAction = this.SelectedAction.Action as AssortmentMinorRevisionAmendDistributionAction;

                        //If cast passed
                        if (distAction != null)
                        {
                            this.SelectedAssortmentMinorRevision.RemoveAction(distAction);
                        }
                    }
                    else if (this.SelectedActionType.Value == AssortmentMinorRevisionActionType.List)
                    {
                        //Try and cast to correct type
                        AssortmentMinorRevisionListAction listAction = this.SelectedAction.Action as AssortmentMinorRevisionListAction;

                        //If cast passed
                        if (listAction != null)
                        {
                            this.SelectedAssortmentMinorRevision.RemoveAction(listAction);
                        }
                    }
                    else if (this.SelectedActionType.Value == AssortmentMinorRevisionActionType.DeList)
                    {
                        //Try and cast to correct type
                        AssortmentMinorRevisionDeListAction delistAction = this.SelectedAction.Action as AssortmentMinorRevisionDeListAction;

                        //If cast passed
                        if (delistAction != null)
                        {
                            this.SelectedAssortmentMinorRevision.RemoveAction(delistAction);
                        }
                    }
                    else if (this.SelectedActionType.Value == AssortmentMinorRevisionActionType.Replace)
                    {
                        //Try and cast to correct type
                        AssortmentMinorRevisionReplaceAction replaceAction = this.SelectedAction.Action as AssortmentMinorRevisionReplaceAction;

                        //If cast passed
                        if (replaceAction != null)
                        {
                            this.SelectedAssortmentMinorRevision.RemoveAction(replaceAction);
                        }
                    }
                }

                //Reorder priorities now one has been removed
                Int32 actionPriorityNum = 1;
                foreach (MinorRevisionActionWrapperViewModel action in this._actions.OrderBy(p => p.Priority))
                {
                    action.Priority = actionPriorityNum;
                    actionPriorityNum++;
                }
            }
        }

        #endregion

        #region RemoveActionLocationCommand

        private RelayCommand _removeActionLocationCommand;

        /// <summary>
        /// Remove the Action Location
        /// </summary>
        public RelayCommand RemoveActionLocationCommand
        {
            get
            {
                if (_removeActionLocationCommand == null)
                {
                    _removeActionLocationCommand = new RelayCommand(
                        p => RemoveActionLocation_Executed(p),
                        p => RemoveActionLocation_CanExecute(p))
                    {
                        FriendlyName = Message.Actions_RemoveActionLocation,
                        FriendlyDescription = Message.Actions_RemoveActionLocation_Description,
                        SmallIcon = ImageResources.Actions_RemoveActionLocation
                    };
                    base.ViewModelCommands.Add(_removeActionLocationCommand);
                }
                return _removeActionLocationCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RemoveActionLocation_CanExecute(Object p)
        {
            return this.SelectedAction != null && p as MinorRevisionActionLocationWrapperViewModel != null;
        }

        private void RemoveActionLocation_Executed(Object p)
        {
            //Cast parameter to location
            MinorRevisionActionLocationWrapperViewModel location = p as MinorRevisionActionLocationWrapperViewModel;

            if (location != null)
            {
                if (this.SelectedActionType.HasValue)
                {
                    if (this.SelectedActionType.Value == AssortmentMinorRevisionActionType.AmendDistribution)
                    {
                        AssortmentMinorRevisionAmendDistributionActionLocation distAction = location.ActionLocation as AssortmentMinorRevisionAmendDistributionActionLocation;

                        if (distAction != null)
                        {
                            //Remove action location from action
                            this.SelectedAction.Action.GetIAssortmentMinorRevisionActionLocations().Remove(distAction);
                        }
                    }
                    else if (this.SelectedActionType.Value == AssortmentMinorRevisionActionType.List)
                    {
                        AssortmentMinorRevisionListActionLocation listAction = location.ActionLocation as AssortmentMinorRevisionListActionLocation;

                        if (listAction != null)
                        {
                            //Remove action location from action
                            this.SelectedAction.Action.GetIAssortmentMinorRevisionActionLocations().Remove(listAction);
                        }
                    }
                    else if (this.SelectedActionType.Value == AssortmentMinorRevisionActionType.DeList)
                    {
                        AssortmentMinorRevisionDeListActionLocation delistAction = location.ActionLocation as AssortmentMinorRevisionDeListActionLocation;

                        if (delistAction != null)
                        {
                            //Remove action location from action
                            this.SelectedAction.Action.GetIAssortmentMinorRevisionActionLocations().Remove(delistAction);
                        }
                    }
                    else if (this.SelectedActionType.Value == AssortmentMinorRevisionActionType.Replace)
                    {
                        AssortmentMinorRevisionReplaceActionLocation replaceAction = location.ActionLocation as AssortmentMinorRevisionReplaceActionLocation;

                        if (replaceAction != null)
                        {
                            //Remove action location from action
                            this.SelectedAction.Action.GetIAssortmentMinorRevisionActionLocations().Remove(replaceAction);
                        }
                    }

                    //Update actions location count
                    this.SelectedAction.TriggerLocationCountUpdate();
                }

                //Trigger update of
                UpdateTotalLocationsAffected();
            }
        }

        #endregion

        #region IncreaseActionPriorityCommand

        private RelayCommand _increaseActionPriorityCommand;

        /// <summary>
        /// Increase the actions priority
        /// </summary>
        public RelayCommand IncreaseActionPriorityCommand
        {
            get
            {
                if (_increaseActionPriorityCommand == null)
                {
                    _increaseActionPriorityCommand = new RelayCommand(
                        p => IncreaseActionPriority_Executed(),
                        p => IncreaseActionPriority_CanExecute())
                    {
                        FriendlyName = Message.Actions_IncreaseActionPriority,
                        FriendlyDescription = Message.Actions_IncreaseActionPriority_Description,
                        SmallIcon = ImageResources.Actions_IncreaseActionPriority
                    };
                    base.ViewModelCommands.Add(_increaseActionPriorityCommand);
                }
                return _increaseActionPriorityCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean IncreaseActionPriority_CanExecute()
        {
            if (this.SelectedAction != null)
            {
                return (this.SelectedAction.Priority != 1);
            }
            return false;
        }

        private void IncreaseActionPriority_Executed()
        {
            if (this.SelectedAction != null)
            {
                //get previous rule
                MinorRevisionActionWrapperViewModel previousSelectedAction = this.SelectedAction;

                if (this.SelectedAction.Priority > 0)
                {
                    //switch selected rule with the one above
                    MovePriority(this.Actions[this.SelectedAction.Priority - 1], this.Actions[this.SelectedAction.Priority - 2]);
                }

                //reselect the rule
                this.SelectedAction = previousSelectedAction;
            }
        }

        #endregion

        #region DecreaseActionPriorityCommand

        private RelayCommand _decreaseActionPriorityCommand;

        /// <summary>
        /// Decrease the actions priority
        /// </summary>
        public RelayCommand DecreaseActionPriorityCommand
        {
            get
            {
                if (_decreaseActionPriorityCommand == null)
                {
                    _decreaseActionPriorityCommand = new RelayCommand(
                        p => DecreaseActionPriority_Executed(),
                        p => DecreaseActionPriority_CanExecute())
                    {
                        FriendlyName = Message.Actions_DecreaseActionPriority,
                        FriendlyDescription = Message.Actions_DecreaseActionPriority_Description,
                        SmallIcon = ImageResources.Actions_DecreaseActionPriority
                    };
                    base.ViewModelCommands.Add(_decreaseActionPriorityCommand);
                }
                return _decreaseActionPriorityCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean DecreaseActionPriority_CanExecute()
        {
            if (this.SelectedAction != null)
            {
                return (this.SelectedAction.Priority != this.Actions.Count);
            }
            else
            {
                return false;
            }
        }

        private void DecreaseActionPriority_Executed()
        {
            if (this.SelectedAction != null)
            {
                //get previous rule
                MinorRevisionActionWrapperViewModel previousSelectedAction = this.SelectedAction;

                if (this.SelectedAction.Priority > 0)
                {
                    //switch selected rule with the one below
                    MovePriority(this._actions[this.SelectedAction.Priority - 1], this._actions[this.SelectedAction.Priority]);
                }

                //reselect the rule
                this.SelectedAction = previousSelectedAction;
            }
        }

        #endregion

        #region ExportActionsCommand

        private RelayCommand _exportActionsCommand;

        /// <summary>
        /// Export the minor revision actions
        /// </summary>
        public RelayCommand ExportActionsCommand
        {
            get
            {
                if (_exportActionsCommand == null)
                {
                    _exportActionsCommand = new RelayCommand(
                        p => ExportActions_Executed(),
                        p => ExportActions_CanExecute())
                    {
                        FriendlyName = Message.Actions_ExportActions,
                        FriendlyDescription = Message.Actions_ExportActions_Description,
                        Icon = ImageResources.BackstageDataManagement_ExportData,
                        DisabledReason = Message.Actions_ExportActions_DisabledReason
                    };
                    base.ViewModelCommands.Add(_exportActionsCommand);
                }
                return _exportActionsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean ExportActions_CanExecute()
        {
            return this.Actions.Count > 0;
        }

        private void ExportActions_Executed()
        {
            if (this.Actions.Count > 0)
            {
                //Construct default file name
                String defaultFileName = String.Empty;
                if (this.SelectedAssortmentMinorRevision != null && this.SelectedAssortmentMinorRevision.Name != null)
                {
                    defaultFileName = this.SelectedAssortmentMinorRevision.Name;

                    //Replace invalid chars with uderscore.
                    foreach (Char invalidChar in Path.GetInvalidFileNameChars())
                    {
                        defaultFileName = defaultFileName.Replace(invalidChar, '_');
                    }
                    defaultFileName = String.Format("{0} {1}", defaultFileName, Message.Actions_ExportAction_FilePostfix);
                }
                else
                {
                    defaultFileName = String.Format("{0} {1}", Message.Actions_ExportActions, Message.Actions_ExportAction_FilePostfix);
                }

                //get the filename to save as
                Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
                saveDialog.CreatePrompt = false;
                saveDialog.Filter = Message.AssortmentMinorRevision_ExportFilter;
                saveDialog.FilterIndex = 3;
                saveDialog.FileName = defaultFileName;
                saveDialog.OverwritePrompt = true;

                if (saveDialog.ShowDialog() == true)
                {
                    //establish the required save format
                    Aspose.Cells.SaveFormat saveFormat;
                    switch (saveDialog.FilterIndex)
                    {
                        case 1:
                            saveFormat = Aspose.Cells.SaveFormat.CSV;
                            break;
                        case 2:
                            saveFormat = Aspose.Cells.SaveFormat.Excel97To2003;
                            break;
                        case 3:
                            saveFormat = Aspose.Cells.SaveFormat.Xlsx;
                            break;
                        default:
                            saveFormat = Aspose.Cells.SaveFormat.Xlsx;
                            break;
                    }

                    Boolean canContinue = true;

                    //If the selected file type is XLS
                    if (saveDialog.FilterIndex == 1)
                    {
                        //Show warning dialog informing user that if > 65k rows, they will not be outputted
                        //as it exceeds this file type row limit.
                        ModalMessage modelMessageWindow = new ModalMessage();
                        modelMessageWindow.Description = Message.DataManagement_RowNumberExceedsMaxLimitWarning;
                        modelMessageWindow.ButtonCount = 2;
                        modelMessageWindow.Button1Content = Message.Generic_Ok;
                        modelMessageWindow.Button2Content = Message.Generic_Cancel;
                        modelMessageWindow.Icon = ImageResources.Warning_32;
                        App.ShowWindow(modelMessageWindow, this.AttachedControl, true);

                        //set the result
                        canContinue = (modelMessageWindow.Result == ModalMessageResult.Button1);
                    }

                    if (canContinue)
                    {
                        //Get fileName
                        String fileName = saveDialog.FileName;

                        //Call method to export actions
                        ExportActions(this.Actions, fileName, saveFormat, false);
                    }
                }
            }
        }

        #endregion

        #region AdvancedExportActionsCommand

        private RelayCommand _advancedExportActionsCommand;
        private ProductGroupInfo _selectedMerchandisingGroup;

        /// <summary>
        /// Export the minor revision actions - advanced version
        /// </summary>
        public RelayCommand AdvancedExportActionsCommand
        {
            get
            {
                if (_advancedExportActionsCommand == null)
                {
                    _advancedExportActionsCommand = new RelayCommand(
                        p => AdvancedExportActions_Executed(),
                        p => AdvancedExportActions_CanExecute())
                    {
                        FriendlyName = Message.Actions_AdvancedExportActions,
                        FriendlyDescription = Message.Actions_AdvancedExportActions_Description,
                        Icon = ImageResources.BackstageDataManagement_ExportData,
                        DisabledReason = Message.Actions_ExportActions_DisabledReason
                    };
                    base.ViewModelCommands.Add(_advancedExportActionsCommand);
                }
                return _advancedExportActionsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean AdvancedExportActions_CanExecute()
        {
            return this.Actions.Count > 0;
        }

        private void AdvancedExportActions_Executed()
        {
            if (this.Actions.Count > 0)
            {
                //Construct default file name
                String defaultFileName = String.Empty;

                if (this.SelectedAssortmentMinorRevision != null && this.SelectedAssortmentMinorRevision.Name != null)
                {
                    defaultFileName = this.SelectedAssortmentMinorRevision.Name;

                    //Replace invalid chars with uderscore.
                    foreach (Char invalidChar in Path.GetInvalidFileNameChars())
                    {
                        defaultFileName = defaultFileName.Replace(invalidChar, '_');
                    }
                    defaultFileName = String.Format("{0} {1}", defaultFileName, Message.Actions_ExportAction_FilePostfix);
                }
                else
                {
                    defaultFileName = String.Format("{0} {1}", Message.Actions_AdvancedExportActions, Message.Actions_ExportAction_FilePostfix);
                }

                //get the filename to save as
                Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();
                saveDialog.CreatePrompt = false;
                saveDialog.Filter = Message.AssortmentMinorRevision_ExportFilter;
                saveDialog.FilterIndex = 3;
                saveDialog.FileName = defaultFileName;
                saveDialog.OverwritePrompt = true;

                if (saveDialog.ShowDialog() == true)
                {
                    //establish the required save format
                    Aspose.Cells.SaveFormat saveFormat;
                    switch (saveDialog.FilterIndex)
                    {
                        case 1:
                            saveFormat = Aspose.Cells.SaveFormat.CSV;
                            break;
                        case 2:
                            saveFormat = Aspose.Cells.SaveFormat.Excel97To2003;
                            break;
                        case 3:
                            saveFormat = Aspose.Cells.SaveFormat.Xlsx;
                            break;
                        default:
                            saveFormat = Aspose.Cells.SaveFormat.Xlsx;
                            break;
                    }

                    Boolean canContinue = true;

                    //If the selected file type is XLS
                    if (saveDialog.FilterIndex == 1)
                    {
                        //Show warning dialog informing user that if > 65k rows, they will not be outputted
                        //as it exceeds this file type row limit.
                        ModalMessage modelMessageWindow = new ModalMessage();
                        modelMessageWindow.Description = Message.DataManagement_RowNumberExceedsMaxLimitWarning;
                        modelMessageWindow.ButtonCount = 2;
                        modelMessageWindow.Button1Content = Message.Generic_Ok;
                        modelMessageWindow.Button2Content = Message.Generic_Cancel;
                        modelMessageWindow.Icon = ImageResources.Warning_32;
                        App.ShowWindow(modelMessageWindow, this.AttachedControl, true);

                        //set the result
                        canContinue = (modelMessageWindow.Result == ModalMessageResult.Button1);
                    }

                    if (canContinue)
                    {
                        //Get fileName
                        String fileName = saveDialog.FileName;

                        //Call method to export actions
                        ExportActions(this.Actions, fileName, saveFormat, true);
                    }
                }
            }
        }

        #endregion

        #region ShowHideLocationsCommand
        private RelayCommand _showHideLocationsCommand;

        /// <summary>
        /// Export the minor revision actions
        /// </summary>
        public RelayCommand ShowHideLocationsCommand
        {
            get
            {
                if (_showHideLocationsCommand == null)
                {
                    _showHideLocationsCommand = new RelayCommand(
                        p => ShowHideLocations_Executed(),
                        p => ShowHideLocations_CanExecute())
                    {
                        FriendlyName = Message.AssortmentMinorRevisionSetup_ShowLocationsButton_Caption,
                        FriendlyDescription = Message.AssortmentMinorRevisionSetup_ShowLocationsButton_Description,
                        Icon = ImageResources.AssortmentSetup_ShowLocations,
                        DisabledReason = Message.AssortmentMinorRevisionSetup_ShowLocationsButton_NoActionSelected
                    };
                    base.ViewModelCommands.Add(_showHideLocationsCommand);
                }
                return _showHideLocationsCommand;
            }
        }

        //[DebuggerStepThrough]
        private Boolean ShowHideLocations_CanExecute()
        {
            if (this.SelectedAction == null)
            {
                _showHideLocationsCommand.DisabledReason = Message.AssortmentMinorRevisionSetup_ShowLocationsButton_NoActionSelected;
                return false;
            }
            return true;
        }

        private void ShowHideLocations_Executed()
        {
            this.ShowLocations = !_showLocations;
        }
        #endregion
        #endregion

        #region Methods

        /// <summary>
        /// Updates the available content items collection
        /// 
        /// </summary>
        /// update request
        /// </param>
        private void UpdateAvailableAssortmentMinorRevisionItems()
        {

            base.ShowWaitCursor(true);

            //make note of the original content count
            Int32 originalContentCount = _availableAssortmentMinorRevisions.Count;

            //update the available content collection
            if (originalContentCount > 0) { _availableAssortmentMinorRevisions.Clear(); }

            AssortmentMinorRevisionInfoList backstageAssortmentMinorRevisionInfos; 

            //if (this.FilterItemsKey != null && this.FilterItemsKey != String.Empty)
            //{
            //    //Need to fetch based on search value
            //    backstageAssortmentMinorRevisionInfos = AssortmentMinorRevisionInfoList.FetchByEntityIdAssortmentMinorRevisionSearchCriteria(App.ViewState.EntityId, this.FilterItemsKey);
            //}
            //else
            //{
                backstageAssortmentMinorRevisionInfos = AssortmentMinorRevisionInfoList.FetchByEntityId(App.ViewState.EntityId);
            //}

            //if fetched items
            //if (backstageAssortmentMinorRevisionInfos != null && backstageAssortmentMinorRevisionInfos.Count() < MaxBackstageItemsFetch)
            //{
                _availableAssortmentMinorRevisions.AddRange(backstageAssortmentMinorRevisionInfos);
                this.FilterItemsCountMessage = String.Format(Message.AssortmentMinorRevision_FilterItemsCountMessage, backstageAssortmentMinorRevisionInfos.Count);
            //}
            //else
            //{
            //    this.FilterItemsCountMessage = String.Format(Message.AssortmentMinorRevision_FilterItemsCountMaxExceededMessage);
            //}

            base.ShowWaitCursor(false);
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedAssortmentMinorRevision, this.SaveCommand);
            }
            return continueExecute;
        }

        /// <summary>
        /// Method to swap the priorities of 2 actions
        /// </summary>
        /// <param name="droppedData"></param>
        /// <param name="target"></param>
        public void MovePriority(MinorRevisionActionWrapperViewModel droppedData, MinorRevisionActionWrapperViewModel target)
        {
            //Update priorities
            Int32 originalPriority = droppedData.Priority;
            droppedData.Priority = target.Priority;
            target.Priority = originalPriority;

            this._actions.Move(droppedData.Priority - 1, target.Priority - 1);
            this._actions.Reset();
            //Reorder actions
            //IEnumerable<MinorRevisionActionWrapperViewModel> actions = this._actions.OrderBy(p => p.Priority).ToList();
            //this._actions.Clear();
            //this._actions.AddRange(actions);
        }

        /// <summary>
        /// Method to update the total locations affected count
        /// </summary>
        private void UpdateTotalLocationsAffected()
        {
            Int32 total = 0;

            if (this.Actions.Count > 0)
            {
                //Colection of action locations
                List<IAssortmentMinorRevisionActionLocation> locations = new List<IAssortmentMinorRevisionActionLocation>();

                //Enumerate throug actions
                foreach (IAssortmentMinorRevisionAction action in this.Actions.Select(p => p.Action))
                {
                    foreach (IAssortmentMinorRevisionActionLocation location in action.GetIAssortmentMinorRevisionActionLocations())
                    {
                        //Add location
                        locations.Add(location);
                    }
                }

                //Get distinct count
                total = locations.Select(p => p.LocationCode).Distinct().Count();
            }

            this.NumberOfLocationsAffected = total;
        }

        /// <summary>
        /// Method to create a new action wrapper view model
        /// </summary>
        /// <returns></returns>
        public MinorRevisionActionWrapperViewModel CreateNewActionWrapper(IAssortmentMinorRevisionAction action)
        {
            //Amended location count
            Int32? amendedLocationCount = action.GetIAssortmentMinorRevisionActionLocations().Count;

            //Get replace product details for replace action
            String replacementProductGtin = String.Empty;
            String replacementProductName = String.Empty;

            //Lookup CDT Node
            ConsumerDecisionTreeNode cdtNode = null;
            if (action.Type == AssortmentMinorRevisionActionType.List)
            {
                AssortmentMinorRevisionListAction listAction = action as AssortmentMinorRevisionListAction;

                if (listAction != null)
                {
                    //Use value from the action
                    cdtNode = this.AvailableCDTNodes.FirstOrDefault(p => p.Id.Equals(listAction.AssortmentConsumerDecisionTreeNodeId));
                }
            }
            else
            {
                if (action.ProductId.HasValue)
                {
                    cdtNode = this.AvailableCDTNodes.FirstOrDefault(n => n.Products.Select(p => p.ProductId).Contains(action.ProductId.Value));
                }

                if (action.Type == AssortmentMinorRevisionActionType.Replace)
                {
                    AssortmentMinorRevisionReplaceAction replaceAction = action as AssortmentMinorRevisionReplaceAction;

                    if (replaceAction != null)
                    {
                        //Set value
                        replacementProductGtin = replaceAction.ReplacementProductGtin;
                        replacementProductName = replaceAction.ReplacementProductName;
                    }
                }
            }

            //Create new wrapper
            MinorRevisionActionWrapperViewModel wrapperViewModel = new MinorRevisionActionWrapperViewModel(action, cdtNode, replacementProductGtin, replacementProductName);
            return wrapperViewModel;
        }

        /// <summary>
        /// Method to create the action rows from the scenario
        /// </summary>
        private void CreateActionRows()
        {
            if (this.SelectedAssortmentMinorRevision != null)
            {
                //Clear any existing actions & performance
                _actions.Clear();

                //Compile all actions
                List<IAssortmentMinorRevisionAction> actions = new List<IAssortmentMinorRevisionAction>();
                actions.AddRange(this.SelectedAssortmentMinorRevision.ListActions);
                actions.AddRange(this.SelectedAssortmentMinorRevision.DeListActions);
                actions.AddRange(this.SelectedAssortmentMinorRevision.AmendDistributionActions);
                actions.AddRange(this.SelectedAssortmentMinorRevision.ReplaceActions);

                //Enumerate  through actions
                foreach (IAssortmentMinorRevisionAction action in actions.OrderBy(p => p.Priority))
                {
                    //Create and add action
                    _actions.Add(CreateNewActionWrapper(action));
                }

                //Trigger update of
                UpdateTotalLocationsAffected();
            }
        }

        private void UpdateAvailableCDTNodes()
        {
            //Update available cdt nodes
            if (this.SelectedAssortmentMinorRevision.ConsumerDecisionTreeId != null)
            {
                //Get CDT
                ConsumerDecisionTree tree = ConsumerDecisionTree.FetchById(this.SelectedAssortmentMinorRevision.ConsumerDecisionTreeId.Value);
                //Get available nodes
                IEnumerable<ConsumerDecisionTreeNode> nodes = tree.GetAllNodes();

                //Clear any existing just in case
                this._availableCDTNodes.Clear();

                //Add nodes to the collection
                this._availableCDTNodes.AddRange(nodes);

                //Update none list actions with current cdt info unless
                //the list action has no cdt info available.
                foreach (var wrappedAction in this.Actions.Where(a =>
                    a.Action.Type != AssortmentMinorRevisionActionType.List ||
                    String.IsNullOrEmpty(a.CDTNodeFullName)))
                {
                    if (wrappedAction.Action.ProductId.HasValue)
                    {
                        ConsumerDecisionTreeNode calcNode = ListProductViewModel.CalculateNewProductsCDTNode(wrappedAction.Action.ProductId.Value, tree);

                        if (calcNode != null)
                        {
                            wrappedAction.TriggerCDTNodeUpdate(calcNode);
                        }
                    }

                }
            }
        }


        /// <summary>
        /// Method to handle exporting the actions
        /// </summary>
        private void ExportActions(IEnumerable<MinorRevisionActionWrapperViewModel> actions, String fileName, Aspose.Cells.SaveFormat saveFormat, Boolean includeLocations)
        {
            //show a busy dialog
            _busyDialog = new ModalBusy();
            if (includeLocations)
            {
                _busyDialog.Header = Message.Actions_AdvancedExportActions;
                _busyDialog.Description = Message.Actions_AdvancedExportActions_Description;
            }
            else
            {
                _busyDialog.Header = Message.Actions_ExportActions;
                _busyDialog.Description = Message.Actions_ExportActions_Description;
            }
            _busyDialog.IsDeterminate = false;
            _busyDialog.Owner = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ExtendedRibbonWindow>(this.AttachedControl);

            //create a new background worker to perform the export.
            BackgroundWorker exportExcel = new BackgroundWorker();
            exportExcel.DoWork += new DoWorkEventHandler(ActionExport_DoWork);
            exportExcel.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ActionExport_RunWorkerCompleted);
            exportExcel.RunWorkerAsync(new Object[] { actions, fileName, saveFormat, includeLocations });

            //Show the busy modal window
            App.ShowWindow(_busyDialog, this.AttachedControl, true);
        }

        /// <summary>
        /// Export actions worker thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks>requires non async GetChachedData to work better</remarks>
        private void ActionExport_DoWork(object sender, DoWorkEventArgs e)
        {
            //Get arguments
            Object[] args = (Object[])e.Argument;
            IEnumerable<MinorRevisionActionWrapperViewModel> actions = (IEnumerable<MinorRevisionActionWrapperViewModel>)args[0];
            String fileName = (String)args[1];
            Aspose.Cells.SaveFormat saveFormat = (Aspose.Cells.SaveFormat)args[2];
            Boolean includeLocations = (Boolean)args[3];

            // create a new workbook to hold the export
            Aspose.Cells.Workbook workbook = new Aspose.Cells.Workbook();
            workbook.Worksheets.Clear();

            #region List Actions

            //Create a new worksheet for list items first
            Aspose.Cells.Worksheet listWorksheet = workbook.Worksheets.Add(Message.Actions_ExportActions_ListActionWorksheet);

            //Add headers
            AddActionExportHeaders(listWorksheet, includeLocations, true, false);

            //Start at row index 1
            Int32 rowIndex = 1;

            foreach (MinorRevisionActionWrapperViewModel listAction in this.Actions.Where(p => p.Action.Type == AssortmentMinorRevisionActionType.List).OrderBy(p => p.Priority))
            {
                //Start at column index 0
                Int32 colIndex = 0;

                listWorksheet.Cells[rowIndex, colIndex].Value = listAction.ProductGtin;
                colIndex++;
                listWorksheet.Cells[rowIndex, colIndex].Value = listAction.ProductName;
                colIndex++;
                listWorksheet.Cells[rowIndex, colIndex].Value = listAction.CDTNodeFullName;
                colIndex++;
                listWorksheet.Cells[rowIndex, colIndex].Value = listAction.Priority;
                colIndex++;
                listWorksheet.Cells[rowIndex, colIndex].Value = listAction.Comments;
                colIndex++;

                if (includeLocations)
                {
                    foreach (IAssortmentMinorRevisionActionLocation actionLocation in listAction.Action.GetIAssortmentMinorRevisionActionLocations())
                    {
                        //Start at column index 0
                        Int32 locColIndex = 0;

                        listWorksheet.Cells[rowIndex, locColIndex].Value = listAction.ProductGtin;
                        locColIndex++;
                        listWorksheet.Cells[rowIndex, locColIndex].Value = listAction.ProductName;
                        locColIndex++;
                        listWorksheet.Cells[rowIndex, locColIndex].Value = listAction.CDTNodeFullName;
                        locColIndex++;
                        listWorksheet.Cells[rowIndex, locColIndex].Value = listAction.Priority;
                        locColIndex++;
                        listWorksheet.Cells[rowIndex, locColIndex].Value = listAction.Comments;
                        locColIndex++;

                        //Cast to correct type
                        AssortmentMinorRevisionListActionLocation listActionLocation = actionLocation as AssortmentMinorRevisionListActionLocation;

                        listWorksheet.Cells[rowIndex, locColIndex].Value = listActionLocation.LocationCode;
                        locColIndex++;
                        listWorksheet.Cells[rowIndex, locColIndex].Value = listActionLocation.LocationName;
                        locColIndex++;
                        listWorksheet.Cells[rowIndex, locColIndex].Value = listActionLocation.Units;
                        locColIndex++;
                        listWorksheet.Cells[rowIndex, locColIndex].Value = listActionLocation.Facings;
                        locColIndex++;

                        //Increase row index
                        rowIndex++;
                    }
                }
                else
                {
                    //Increase row index
                    rowIndex++;
                }
            }

            #endregion

            #region DeList Actions

            //Create a new worksheet for deList items first
            Aspose.Cells.Worksheet deListworksheet = workbook.Worksheets.Add(Message.Actions_ExportActions_DeListActionWorksheet);

            //Add headers
            AddActionExportHeaders(deListworksheet, includeLocations, false, false);

            //Reset row index
            rowIndex = 1;

            foreach (MinorRevisionActionWrapperViewModel deListAction in this.Actions.Where(p => p.Action.Type == AssortmentMinorRevisionActionType.DeList).OrderBy(p => p.Priority))
            {
                //Start at column index 0
                Int32 colIndex = 0;

                deListworksheet.Cells[rowIndex, colIndex].Value = deListAction.ProductGtin;
                colIndex++;
                deListworksheet.Cells[rowIndex, colIndex].Value = deListAction.ProductName;
                colIndex++;
                deListworksheet.Cells[rowIndex, colIndex].Value = deListAction.CDTNodeFullName;
                colIndex++;
                deListworksheet.Cells[rowIndex, colIndex].Value = deListAction.Priority;
                colIndex++;
                deListworksheet.Cells[rowIndex, colIndex].Value = deListAction.Comments;
                colIndex++;

                if (includeLocations)
                {
                    foreach (IAssortmentMinorRevisionActionLocation actionLocation in deListAction.Action.GetIAssortmentMinorRevisionActionLocations())
                    {
                        //Start at column index 0
                        Int32 locColIndex = 0;

                        deListworksheet.Cells[rowIndex, locColIndex].Value = deListAction.ProductGtin;
                        locColIndex++;
                        deListworksheet.Cells[rowIndex, locColIndex].Value = deListAction.ProductName;
                        locColIndex++;
                        deListworksheet.Cells[rowIndex, locColIndex].Value = deListAction.CDTNodeFullName;
                        locColIndex++;
                        deListworksheet.Cells[rowIndex, locColIndex].Value = deListAction.Priority;
                        locColIndex++;
                        deListworksheet.Cells[rowIndex, locColIndex].Value = deListAction.Comments;
                        locColIndex++;

                        //Cast to correct type
                        AssortmentMinorRevisionDeListActionLocation delistActionLocation = actionLocation as AssortmentMinorRevisionDeListActionLocation;

                        deListworksheet.Cells[rowIndex, locColIndex].Value = delistActionLocation.LocationCode;
                        locColIndex++;
                        deListworksheet.Cells[rowIndex, locColIndex].Value = delistActionLocation.LocationName;
                        locColIndex++;

                        //Increase row index
                        rowIndex++;
                    }
                }
                else
                {
                    //Increase row index
                    rowIndex++;
                }
            }

            #endregion

            #region Amend Distribution Actions

            //Create a new worksheet for amendDist items first
            Aspose.Cells.Worksheet amendDistWorksheet = workbook.Worksheets.Add(Message.Actions_ExportActions_AmendDistActionWorksheet);

            //Add headers
            AddActionExportHeaders(amendDistWorksheet, includeLocations, true, false);

            //Reset row index
            rowIndex = 1;

            foreach (MinorRevisionActionWrapperViewModel distAction in this.Actions.Where(p => p.Action.Type == AssortmentMinorRevisionActionType.AmendDistribution).OrderBy(p => p.Priority))
            {
                //Start at column index 0
                Int32 colIndex = 0;

                amendDistWorksheet.Cells[rowIndex, colIndex].Value = distAction.ProductGtin;
                colIndex++;
                amendDistWorksheet.Cells[rowIndex, colIndex].Value = distAction.ProductName;
                colIndex++;
                amendDistWorksheet.Cells[rowIndex, colIndex].Value = distAction.CDTNodeFullName;
                colIndex++;
                amendDistWorksheet.Cells[rowIndex, colIndex].Value = distAction.Priority;
                colIndex++;
                amendDistWorksheet.Cells[rowIndex, colIndex].Value = distAction.Comments;
                colIndex++;

                if (includeLocations)
                {
                    foreach (IAssortmentMinorRevisionActionLocation distLocation in distAction.Action.GetIAssortmentMinorRevisionActionLocations())
                    {
                        //Start at column index 0
                        Int32 locColIndex = 0;

                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distAction.ProductGtin;
                        locColIndex++;
                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distAction.ProductName;
                        locColIndex++;
                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distAction.CDTNodeFullName;
                        locColIndex++;
                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distAction.Priority;
                        locColIndex++;
                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distAction.Comments;
                        locColIndex++;

                        //Cast to correct type
                        AssortmentMinorRevisionAmendDistributionActionLocation distActionLocation = distLocation as AssortmentMinorRevisionAmendDistributionActionLocation;

                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distActionLocation.LocationCode;
                        locColIndex++;
                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distActionLocation.LocationName;
                        locColIndex++;
                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distActionLocation.Units;
                        locColIndex++;

                        amendDistWorksheet.Cells[rowIndex, locColIndex].Value = distActionLocation.Facings;
                        locColIndex++;

                        //Increase row index
                        rowIndex++;
                    }
                }
                else
                {
                    //Increase row index
                    rowIndex++;
                }
            }

            #endregion

            #region Replace Actions

            //Create a new worksheet for amendDist items first
            Aspose.Cells.Worksheet replaceWorksheet = workbook.Worksheets.Add(Message.Actions_ExportActions_ReplaceWorksheet);

            //Add headers
            AddActionExportHeaders(replaceWorksheet, includeLocations, true, true);

            //Reset row index
            rowIndex = 1;

            foreach (MinorRevisionActionWrapperViewModel replaceAction in this.Actions.Where(p => p.Action.Type == AssortmentMinorRevisionActionType.Replace).OrderBy(p => p.Priority))
            {
                //Start at column index 0
                Int32 colIndex = 0;

                replaceWorksheet.Cells[rowIndex, colIndex].Value = replaceAction.ProductGtin;
                colIndex++;
                replaceWorksheet.Cells[rowIndex, colIndex].Value = replaceAction.ProductName;
                colIndex++;
                replaceWorksheet.Cells[rowIndex, colIndex].Value = replaceAction.CDTNodeFullName;
                colIndex++;
                replaceWorksheet.Cells[rowIndex, colIndex].Value = replaceAction.ReplacementProductGtin;
                colIndex++;
                replaceWorksheet.Cells[rowIndex, colIndex].Value = replaceAction.ReplacementProductName;
                colIndex++;
                replaceWorksheet.Cells[rowIndex, colIndex].Value = replaceAction.Priority;
                colIndex++;
                replaceWorksheet.Cells[rowIndex, colIndex].Value = replaceAction.Comments;
                colIndex++;

                if (includeLocations)
                {
                    foreach (IAssortmentMinorRevisionActionLocation replaceLocation in replaceAction.Action.GetIAssortmentMinorRevisionActionLocations())
                    {
                        //Start at column index 0
                        Int32 locColIndex = 0;

                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceAction.ProductGtin;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceAction.ProductName;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceAction.CDTNodeFullName;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceAction.ReplacementProductGtin;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceAction.ReplacementProductName;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceAction.Priority;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceAction.Comments;
                        locColIndex++;

                        //Cast to correct type
                        AssortmentMinorRevisionReplaceActionLocation replaceActionLocation = replaceLocation as AssortmentMinorRevisionReplaceActionLocation;

                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceActionLocation.LocationCode;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceActionLocation.LocationName;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceActionLocation.Units;
                        locColIndex++;
                        replaceWorksheet.Cells[rowIndex, locColIndex].Value = replaceActionLocation.Facings;
                        locColIndex++;

                        //Increase row index
                        rowIndex++;
                    }
                }
                else
                {
                    //Increase row index
                    rowIndex++;
                }
            }

            #endregion

            foreach (Aspose.Cells.Worksheet worksheet in workbook.Worksheets)
            {
                //Auto fit all worksheets
                worksheet.AutoFitColumns();
            }

            workbook.Save(fileName, saveFormat);
        }

        /// <summary>
        /// Method to add the action headers for a worksheet
        /// </summary>
        /// <param name="worksheet"></param>
        private void AddActionExportHeaders(Aspose.Cells.Worksheet worksheet, Boolean includeLocations, Boolean includeUnitsFacings, Boolean includeReplaced)
        {
            //Start at column 0;
            Int32 columnIndex = 0;

            //Add header for each data type
            worksheet.Cells[0, columnIndex].Value = Product.GtinProperty.FriendlyName;
            columnIndex++;
            worksheet.Cells[0, columnIndex].Value = Product.NameProperty.FriendlyName;
            columnIndex++;
            worksheet.Cells[0, columnIndex].Value = Message.Actions_ActionsGrid_CDTNode;
            columnIndex++;
            if (includeReplaced)
            {
                worksheet.Cells[0, columnIndex].Value = Message.Actions_ActionsGrid_ReplacementProductGtin;
                columnIndex++;
                worksheet.Cells[0, columnIndex].Value = Message.Actions_ActionsGrid_ReplacementProductName;
                columnIndex++;
            }
            worksheet.Cells[0, columnIndex].Value = AssortmentMinorRevisionListAction.PriorityProperty.FriendlyName;
            columnIndex++;
            worksheet.Cells[0, columnIndex].Value = AssortmentMinorRevisionListAction.CommentsProperty.FriendlyName;
            columnIndex++;

            if (includeLocations)
            {
                worksheet.Cells[0, columnIndex].Value = Location.CodeProperty.FriendlyName;
                columnIndex++;
                worksheet.Cells[0, columnIndex].Value = Location.NameProperty.FriendlyName;
                columnIndex++;
                if (includeUnitsFacings)
                {
                    worksheet.Cells[0, columnIndex].Value = Message.Actions_ActionLocationsGrid_Units;
                    columnIndex++;
                    worksheet.Cells[0, columnIndex].Value = Message.Actions_ActionLocationsGrid_Facings;
                    columnIndex++;
                }
            }
        }

        /// <summary>
        /// Export complete actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ActionExport_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //hide busy
            if (_busyDialog != null)
            {
                _busyDialog.Close();
                _busyDialog = null;
            }

            //show a dialog window displaying the result
            String description = null;
            String header = null;

            if (e.Error == null)
            {
                header = Message.BackstageDataManagement_ExportSuccess;
            }
            else
            {
                header = Message.BackstageDataManagement_ExportFailed;
                description = e.Error.Message;
            }


            if (this.AttachedControl != null)
            {
                ModalMessage errorWindow = new ModalMessage();
                errorWindow.Description = description;
                errorWindow.Header = header;
                errorWindow.ButtonCount = 1;
                errorWindow.Button1Content = Message.Generic_Ok;
                App.ShowWindow(errorWindow, this.AttachedControl, /*isModal*/true);
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of the master assortment info list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MasterAssortmentMinorRevisionInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<AssortmentMinorRevisionInfoList> e)
        {
            OnMasterAssortmentMinorRevisionInfoListViewModelChanged(e.NewModel);
        }
        private void OnMasterAssortmentMinorRevisionInfoListViewModelChanged(AssortmentMinorRevisionInfoList newModel)
        {
            //create the available assortment minor revision viewmodels
            if (this._availableAssortmentMinorRevisions.Count > 0)
            {
                _availableAssortmentMinorRevisions.Clear();
            }

            if (newModel != null)
            {
                UpdateAvailableAssortmentMinorRevisionItems();
            }
        }

        /// <summary>
        /// Responds to changes to the master merch hierarchy view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MerchandisingHierarchyView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductHierarchy> e)
        {
            //notify of the possible property change
            OnPropertyChanged(SelectedMerchandisingGroupProperty);
        }

        /// <summary>
        /// Responds to changes to the master CDT list view
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConsumerDecisionTreeInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ConsumerDecisionTreeInfoList> e)
        {
            //notify of the possible property change
            //OnPropertyChanged(SelectedLocationProperty);
        }


        /// <summary>
        /// Responds to a change of selected assormtent
        /// </summary>
        /// <param name="oldValue"></param>
        /// <param name="newValue"></param>
        private void OnSelectedAssortmentChanged(AssortmentMinorRevision oldValue, AssortmentMinorRevision newValue)
        {
            if (oldValue != null)
            {
                //Detach
                oldValue.PropertyChanged -= SelectedAssortmentMinorRevision_PropertyChanged;
                oldValue.DeListActions.BulkCollectionChanged -= MinorRevisionActions_BulkCollectionChanged;
                oldValue.ListActions.BulkCollectionChanged -= MinorRevisionActions_BulkCollectionChanged;
                oldValue.AmendDistributionActions.BulkCollectionChanged -= MinorRevisionActions_BulkCollectionChanged;
                oldValue.ReplaceActions.BulkCollectionChanged -= MinorRevisionActions_BulkCollectionChanged;

            }

            if (newValue != null)
            {
                newValue.PropertyChanged += SelectedAssortmentMinorRevision_PropertyChanged;
                newValue.DeListActions.BulkCollectionChanged += MinorRevisionActions_BulkCollectionChanged;
                newValue.ListActions.BulkCollectionChanged += MinorRevisionActions_BulkCollectionChanged;
                newValue.AmendDistributionActions.BulkCollectionChanged += MinorRevisionActions_BulkCollectionChanged;
                newValue.ReplaceActions.BulkCollectionChanged += MinorRevisionActions_BulkCollectionChanged;

                //update the selected product group
                ProductGroupInfo newSelectedProductGroup = null;
                if (newValue.ProductGroupId != null)
                {
                    newSelectedProductGroup = ProductGroupInfo.FetchById(newValue.ProductGroupId);
                }
                this.SelectedMerchandisingGroup = newSelectedProductGroup;

                //update the selected cdt
                ConsumerDecisionTreeInfo newSelectedCdt = null;
                if (newValue.ConsumerDecisionTreeId.HasValue)
                {
                    newSelectedCdt = ConsumerDecisionTreeInfo.FetchById(Convert.ToInt32(newValue.ConsumerDecisionTreeId));
                }
                this.SelectedConsumerDecisonTree = newSelectedCdt;

                //Update available cdt nodes
                UpdateAvailableCDTNodes();

                //Update action rows
                CreateActionRows();
            }

        }

        /// <summary>
        /// Responds to property changes on the selected assortment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedAssortmentMinorRevision_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == AssortmentMinorRevision.ProductGroupIdProperty.Name)
            {
                OnPropertyChanged(SelectedMerchandisingGroupProperty);
            }
            if (e.PropertyName == AssortmentMinorRevision.ConsumerDecisionTreeIdProperty.Name)
            {
                //Update available cdt nodes
                UpdateAvailableCDTNodes();
                OnPropertyChanged(SelectedConsumerDecisonTreeProperty);
            }
        }

        /// <summary>
        /// Method to handle the selected action changing
        /// </summary>
        /// <param name="action"></param>
        private void OnSelectedActionChanged(MinorRevisionActionWrapperViewModel actionRow, MinorRevisionActionWrapperViewModel oldRow)
        {
            base.ShowWaitCursor(true);

            if (oldRow != null)
            {
                //Detach from locations handler
                if (oldRow.Action.Type == AssortmentMinorRevisionActionType.List)
                {
                    AssortmentMinorRevisionListActionLocationList locationList = oldRow.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionListActionLocationList;

                    if (locationList != null)
                    {
                        locationList.BulkCollectionChanged -= MinorRevisionActionLocations_BulkCollectionChanged;
                    }
                }
                else if (oldRow.Action.Type == AssortmentMinorRevisionActionType.AmendDistribution)
                {
                    AssortmentMinorRevisionAmendDistributionActionLocationList locationList = oldRow.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionAmendDistributionActionLocationList;
                    if (locationList != null)
                    {
                        locationList.BulkCollectionChanged -= MinorRevisionActionLocations_BulkCollectionChanged;
                    }
                }
                else if (oldRow.Action.Type == AssortmentMinorRevisionActionType.DeList)
                {
                    AssortmentMinorRevisionDeListActionLocationList locationList = oldRow.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionDeListActionLocationList;
                    if (locationList != null)
                    {
                        locationList.BulkCollectionChanged -= MinorRevisionActionLocations_BulkCollectionChanged;
                    }
                }
                else if (oldRow.Action.Type == AssortmentMinorRevisionActionType.Replace)
                {
                    AssortmentMinorRevisionReplaceActionLocationList locationList = oldRow.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionReplaceActionLocationList;
                    if (locationList != null)
                    {
                        locationList.BulkCollectionChanged -= MinorRevisionActionLocations_BulkCollectionChanged;
                    }
                }
            }

            //If selected item has changed, no need to reload
            if (oldRow != actionRow)
            {
                //Clear location rows
                this._actionLocations.Clear();

                if (actionRow != null)
                {
                    //Update action type
                    this._selectedActionType = actionRow.Action.Type;
                    OnPropertyChanged(SelectedActionTypeProperty);

                    //Get units/facings value from live range cache
                    //Dictionary<Int16, Range> rangeLookup = null;
                    //if (actionRow.Action.Type == AssortmentMinorRevisionActionType.AmendDistribution)
                    //{
                    //    rangeLookup = _rangeDataCache.GetCachedItem(actionRow.Action.ProductId.Value);
                    //}

                    foreach (IAssortmentMinorRevisionActionLocation location in actionRow.Action.GetIAssortmentMinorRevisionActionLocations())
                    {
                        if (actionRow.Action.Type == AssortmentMinorRevisionActionType.List)
                        {
                            //Cast to list action location
                            AssortmentMinorRevisionListActionLocation listActionLocation = location as AssortmentMinorRevisionListActionLocation;

                            if (listActionLocation != null)
                            {
                                //Create and add location row
                                this._actionLocations.Add(new MinorRevisionActionLocationWrapperViewModel(listActionLocation));
                            }
                        }
                        else if (actionRow.Action.Type == AssortmentMinorRevisionActionType.AmendDistribution)
                        {
                            //Cast to list action location
                            AssortmentMinorRevisionAmendDistributionActionLocation amendDistActionLocation = location as AssortmentMinorRevisionAmendDistributionActionLocation;

                            if (amendDistActionLocation != null)
                            {
                                //Try and find corresponding range values
                                //Int16? units = null;
                                //Int16? facings = null;
                                //Range range = null;
                                ////If range data exists for this product and the location has an id
                                //if (rangeLookup != null && amendDistActionLocation.LocationId.HasValue)
                                //{
                                //    //If range data can be found for this location
                                //    if (rangeLookup.TryGetValue(amendDistActionLocation.LocationId.Value, out range))
                                //    {
                                //        units = range.Units;
                                //        facings = range.Facings;
                                //    }
                                //}

                                //Create and add location row
                                this._actionLocations.Add(new MinorRevisionActionLocationWrapperViewModel(amendDistActionLocation));
                            }
                        }
                        else
                        {
                            //Create and add location row
                            this._actionLocations.Add(new MinorRevisionActionLocationWrapperViewModel(location));
                        }
                    }

                    //Attach to locations handler
                    if (actionRow.Action.Type == AssortmentMinorRevisionActionType.List)
                    {
                        AssortmentMinorRevisionListActionLocationList locationList = actionRow.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionListActionLocationList;
                        if (locationList != null)
                        {
                            locationList.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(MinorRevisionActionLocations_BulkCollectionChanged);
                        }
                    }
                    else if (actionRow.Action.Type == AssortmentMinorRevisionActionType.AmendDistribution)
                    {
                        AssortmentMinorRevisionAmendDistributionActionLocationList locationList = actionRow.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionAmendDistributionActionLocationList;
                        if (locationList != null)
                        {
                            locationList.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(MinorRevisionActionLocations_BulkCollectionChanged);
                        }
                    }
                    else if (actionRow.Action.Type == AssortmentMinorRevisionActionType.DeList)
                    {
                        AssortmentMinorRevisionDeListActionLocationList locationList = actionRow.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionDeListActionLocationList;
                        if (locationList != null)
                        {
                            locationList.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(MinorRevisionActionLocations_BulkCollectionChanged);
                        }
                    }
                    else if (actionRow.Action.Type == AssortmentMinorRevisionActionType.Replace)
                    {
                        AssortmentMinorRevisionReplaceActionLocationList locationList = actionRow.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionReplaceActionLocationList;
                        if (locationList != null)
                        {
                            locationList.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(MinorRevisionActionLocations_BulkCollectionChanged);
                        }
                    }
                }
                else
                {
                    //If new row is null clear locations
                    this._actionLocations.Clear();
                }
            }

            base.ShowWaitCursor(false);
        }


        /// <summary>
        /// Handler for the minor revision actions bulk collection changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MinorRevisionActions_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case (NotifyCollectionChangedAction.Add):
                    //Store new wrappers
                    List<MinorRevisionActionWrapperViewModel> newWrappers = new List<MinorRevisionActionWrapperViewModel>();

                    //Enumerate throug new actions
                    foreach (IAssortmentMinorRevisionAction action in e.ChangedItems)
                    {
                        //Create new action
                        MinorRevisionActionWrapperViewModel actionWrapper = CreateNewActionWrapper(action);

                        //Create and add action
                        _actions.Add(actionWrapper);

                        //Add to wrappers
                        newWrappers.Add(actionWrapper);
                    }

                    //Trigger update of 
                    UpdateTotalLocationsAffected();
                    break;
                case (NotifyCollectionChangedAction.Remove):
                    foreach (IAssortmentMinorRevisionAction action in e.ChangedItems)
                    {
                        //Locate wrapper
                        MinorRevisionActionWrapperViewModel wrapper = this.Actions.FirstOrDefault(p => p.Action.Equals(action));

                        if (wrapper != null)
                        {
                            _actions.Remove(wrapper);
                        }
                    }

                    //Trigger update of 
                    UpdateTotalLocationsAffected();

                    break;
                case (NotifyCollectionChangedAction.Reset):
                    foreach (IAssortmentMinorRevisionAction action in e.ChangedItems)
                    {
                        //Clear all
                        _actions.Clear();
                    }

                    //Trigger update of 
                    UpdateTotalLocationsAffected();
                    break;
            }
        }

        /// <summary>
        /// Handler for the minor revision actions bulk collection changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MinorRevisionActionLocations_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case (NotifyCollectionChangedAction.Add):
                    foreach (IAssortmentMinorRevisionActionLocation location in e.ChangedItems)
                    {
                        //Create and add action location
                        _actionLocations.Add(new MinorRevisionActionLocationWrapperViewModel(location));
                    }
                    break;
                case (NotifyCollectionChangedAction.Remove):
                    foreach (IAssortmentMinorRevisionActionLocation actionLocation in e.ChangedItems)
                    {
                        //Locate wrapper
                        MinorRevisionActionLocationWrapperViewModel wrapper = this.ActionLocations.FirstOrDefault(p => p.ActionLocation.Equals(actionLocation));

                        if (wrapper != null)
                        {
                            _actionLocations.Remove(wrapper);
                        }
                    }
                    break;
                case (NotifyCollectionChangedAction.Reset):
                    foreach (IAssortmentMinorRevisionActionLocation action in e.ChangedItems)
                    {
                        //Clear all
                        _actionLocations.Clear();
                    }
                    break;
            }
        }


        #endregion

        #region Screen Permissions

        private Boolean _userHasAssortmentCreatePerm;
        private Boolean _userHasAssortmentFetchPerm;
        private Boolean _userHasAssortmentEditPerm;
        private Boolean _userHasAssortmentDeletePerm;

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //Assortment premissions
            //create
            _userHasAssortmentCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(AssortmentMinorRevision));

            //fetch
            _userHasAssortmentFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(AssortmentMinorRevision));

            //edit
            _userHasAssortmentEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(AssortmentMinorRevision));

            //delete
            _userHasAssortmentDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(AssortmentMinorRevision));
        }

        /// <summary>
        /// Returns true if the user has sufficient permissions to launch this screen.
        /// </summary>
        /// <returns></returns>
        internal static Boolean UserHasRequiredAccessPerms()
        {
            return

                //assortment minor revision fetch perm
                Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(AssortmentMinorRevision));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterAssortmentMinorRevisionInfoListView.ModelChanged -= MasterAssortmentMinorRevisionInfoListView_ModelChanged;
                    _consumerDecisionTreeInfoListView.ModelChanged -= ConsumerDecisionTreeInfoListView_ModelChanged;


                    OnSelectedAssortmentChanged(this.SelectedAssortmentMinorRevision, null);
                    _selectedAssortmentMinorRevision = null;


                    //Detach from action location collection changed
                    if (this.SelectedAction != null)
                    {
                        //Detach from locations handler
                        if (this.SelectedAction.Action.Type == AssortmentMinorRevisionActionType.List)
                        {
                            AssortmentMinorRevisionListActionLocationList locationList = this.SelectedAction.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionListActionLocationList;
                            if (locationList != null)
                            {
                                locationList.BulkCollectionChanged -= MinorRevisionActionLocations_BulkCollectionChanged;
                            }
                        }
                        else if (this.SelectedAction.Action.Type == AssortmentMinorRevisionActionType.AmendDistribution)
                        {
                            AssortmentMinorRevisionAmendDistributionActionLocationList locationList = this.SelectedAction.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionAmendDistributionActionLocationList;
                            if (locationList != null)
                            {
                                locationList.BulkCollectionChanged -= MinorRevisionActionLocations_BulkCollectionChanged;
                            }
                        }
                        else if (this.SelectedAction.Action.Type == AssortmentMinorRevisionActionType.DeList)
                        {
                            AssortmentMinorRevisionDeListActionLocationList locationList = this.SelectedAction.Action.GetIAssortmentMinorRevisionActionLocations() as AssortmentMinorRevisionDeListActionLocationList;
                            if (locationList != null)
                            {
                                locationList.BulkCollectionChanged -= MinorRevisionActionLocations_BulkCollectionChanged;
                            }
                        }
                    }

                    this.SelectedAction = null;
                    _actionLocations.Clear();
                    _actions.Clear();

                    _masterAssortmentMinorRevisionInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Minor Revision Action Wrapper View Model
    /// </summary>
    public sealed class MinorRevisionActionWrapperViewModel : INotifyPropertyChanged
    {
        #region Fields

        private IAssortmentMinorRevisionAction _minorRevisionAction;
        private ConsumerDecisionTreeNode _cdtNode;
        private String _replacementProductGtin;
        private String _replacementProductName;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath TypeProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.Type);
        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.ProductName);
        public static readonly PropertyPath LocationsAffectedCountProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.LocationsAffectedCount);
        public static readonly PropertyPath PriorityProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.Priority);
        public static readonly PropertyPath CommentsProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.Comments);
        public static readonly PropertyPath CDTNodeFullNameProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.CDTNodeFullName);
        public static readonly PropertyPath ReplacementProductGtinProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.ReplacementProductGtin);
        public static readonly PropertyPath ReplacementProductNameProperty = WpfHelper.GetPropertyPath<MinorRevisionActionWrapperViewModel>(p => p.ReplacementProductName);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the minor revision action
        /// </summary>
        public IAssortmentMinorRevisionAction Action
        {
            get { return _minorRevisionAction; }
        }

        /// <summary>
        /// Gets the minor revision action location count
        /// </summary>
        public Int32 LocationsAffectedCount
        {
            get { return _minorRevisionAction.GetIAssortmentMinorRevisionActionLocations().Count; }
        }

        /// <summary>
        /// Returns the review type
        /// </summary>
        public String Type
        {
            get { return AssortmentMinorRevisionActionTypeHelper.FriendlyNames[_minorRevisionAction.Type]; }
        }

        /// <summary>
        /// Returns the product gtin
        /// </summary>
        public String ProductGtin
        {
            get { return _minorRevisionAction.ProductGtin; }
        }

        /// <summary>
        /// Returns the product name
        /// </summary>
        public String ProductName
        {
            get { return _minorRevisionAction.ProductName; }
        }

        /// <summary>
        /// Returns the action change priority
        /// </summary>
        public Int32 Priority
        {
            get { return _minorRevisionAction.Priority; }
            set
            {
                _minorRevisionAction.Priority = value;
                OnPropertyChanged(PriorityProperty);
            }
        }

        /// <summary>
        /// Returns the action change comments
        /// </summary>
        public String Comments
        {
            get { return _minorRevisionAction.Comments; }
            set
            {
                _minorRevisionAction.Comments = value;
                OnPropertyChanged(CommentsProperty);
            }
        }

        /// <summary>
        /// Returns which product the cdt lies within
        /// </summary>
        public String CDTNodeFullName
        {
            get
            {
                if (_cdtNode != null)
                {
                    return _cdtNode.FullName;
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns the gtin of the replacement product
        /// </summary>
        public String ReplacementProductGtin
        {
            get
            {
                if (_replacementProductGtin != null)
                {
                    return _replacementProductGtin;
                }
                return String.Empty;
            }
        }

        /// <summary>
        /// Returns the name of the replacement product
        /// </summary>
        public String ReplacementProductName
        {
            get
            {
                if (_replacementProductName != null)
                {
                    return _replacementProductName;
                }
                return String.Empty;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public MinorRevisionActionWrapperViewModel(IAssortmentMinorRevisionAction action, ConsumerDecisionTreeNode cdtNode, String replacementProductGtin, String replacementProductName)
        {
            //Set the properties
            _minorRevisionAction = action;
            _cdtNode = cdtNode;
            _replacementProductGtin = replacementProductGtin;
            _replacementProductName = replacementProductName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Helper method to trigger location count update
        /// </summary>
        /// <remarks>This is here so that all of the events can be kept in the main view model, as Locations collection
        /// doesn't support collection changed events easily and updating this value is insignificant in 
        /// the scheme of things.</remarks>
        public void TriggerLocationCountUpdate()
        {
            OnPropertyChanged(LocationsAffectedCountProperty);
        }

        public void TriggerCDTNodeUpdate(ConsumerDecisionTreeNode cdtNode)
        {
            _cdtNode = cdtNode;

            if (Action is AssortmentMinorRevisionListAction)
            {
                (Action as AssortmentMinorRevisionListAction).AssortmentConsumerDecisionTreeNodeId = cdtNode.Id;
            }

            OnPropertyChanged(CDTNodeFullNameProperty);
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
        }


        #endregion
    }

    /// <summary>
    /// Minor Revision Action Location Wrapper View Model
    /// </summary>
    public sealed class MinorRevisionActionLocationWrapperViewModel : INotifyPropertyChanged
    {
        #region Fields

        private IAssortmentMinorRevisionActionLocation _minorRevisionActionLocation;
        private Int32? _units = 0;
        private Int32? _facings = 0;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath LocationCodeProperty = WpfHelper.GetPropertyPath<MinorRevisionActionLocationWrapperViewModel>(p => p.LocationCode);
        public static readonly PropertyPath LocationNameProperty = WpfHelper.GetPropertyPath<MinorRevisionActionLocationWrapperViewModel>(p => p.LocationName);
        public static readonly PropertyPath UnitsProperty = WpfHelper.GetPropertyPath<MinorRevisionActionLocationWrapperViewModel>(p => p.Units);
        public static readonly PropertyPath FacingsProperty = WpfHelper.GetPropertyPath<MinorRevisionActionLocationWrapperViewModel>(p => p.Facings);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the minor revision action location
        /// </summary>
        public IAssortmentMinorRevisionActionLocation ActionLocation
        {
            get { return _minorRevisionActionLocation; }
        }

        /// <summary>
        /// Returns the location code
        /// </summary>
        public String LocationCode
        {
            get { return _minorRevisionActionLocation.LocationCode; }
        }

        /// <summary>
        /// Returns the location name
        /// </summary>
        public String LocationName
        {
            get { return _minorRevisionActionLocation.LocationName; }
        }

        /// <summary>
        /// Returns the location units
        /// </summary>
        public Int32? Units
        {
            get { return _units; }
            set
            {
                _units = value;

                if (_units.HasValue)
                {
                    if (_minorRevisionActionLocation is AssortmentMinorRevisionListActionLocation)
                    {
                        (_minorRevisionActionLocation as AssortmentMinorRevisionListActionLocation).Units = _units.Value;
                    }
                    else if (_minorRevisionActionLocation is AssortmentMinorRevisionAmendDistributionActionLocation)
                    {
                        (_minorRevisionActionLocation as AssortmentMinorRevisionAmendDistributionActionLocation).Units = _units.Value;
                    }
                }

                OnPropertyChanged(UnitsProperty);
            }
        }

        /// <summary>
        /// Returns the location facings
        /// </summary>
        public Int32? Facings
        {
            get { return _facings; }
            set
            {
                _facings = value;
                if (_facings.HasValue)
                {
                    if (_minorRevisionActionLocation is AssortmentMinorRevisionListActionLocation)
                    {
                        (_minorRevisionActionLocation as AssortmentMinorRevisionListActionLocation).Facings = _facings.Value;
                    }
                    else if (_minorRevisionActionLocation is AssortmentMinorRevisionAmendDistributionActionLocation)
                    {
                        (_minorRevisionActionLocation as AssortmentMinorRevisionAmendDistributionActionLocation).Facings = _facings.Value;
                    }
                }
                OnPropertyChanged(FacingsProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MinorRevisionActionLocationWrapperViewModel(IAssortmentMinorRevisionActionLocation actionLocation)
        {
            //Set the properties
            _minorRevisionActionLocation = actionLocation;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MinorRevisionActionLocationWrapperViewModel(AssortmentMinorRevisionListActionLocation listActionLocation)
        {
            //Set the properties
            _minorRevisionActionLocation = listActionLocation;
            _units = listActionLocation.Units;
            _facings = listActionLocation.Facings;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MinorRevisionActionLocationWrapperViewModel(AssortmentMinorRevisionAmendDistributionActionLocation amendDistributionActionLocation)
        {
            //Set the properties
            _minorRevisionActionLocation = amendDistributionActionLocation;
            _units = amendDistributionActionLocation.Units;
            _facings = amendDistributionActionLocation.Facings;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
        }


        #endregion
    }

    /// <summary>
    /// Struct to hold action product data
    /// </summary>
    public struct ActionProduct
    {
        #region Fields
        private Int32 _productId;
        private String _gtin;
        private String _name;
        #endregion

        #region Properties

        public Int32 ProductId
        {
            get { return _productId; }
        }
        public String Gtin
        {
            get { return _gtin; }
        }
        public String Name
        {
            get { return _name; }
        }

        #endregion

        #region Constructors
        public ActionProduct(ProductUniverseProduct product)
            : this(product.ProductId, product.Gtin, product.Name)
        {
        }

        //public ActionProduct(ConsumerDecisionProduct product)
        //    : this(product.ProductId, product.GTIN, product.Name)
        //{
        //}

        public ActionProduct(Int32 productId, String gtin, String name)
        {
            _productId = productId;
            _gtin = gtin;
            _name = name;
        }
        #endregion

    }
}
