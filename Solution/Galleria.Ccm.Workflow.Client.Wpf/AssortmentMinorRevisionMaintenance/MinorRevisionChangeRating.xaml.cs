﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Interaction logic for MinorRevisionChangeRating.xaml
    /// </summary>
    public partial class MinorRevisionChangeRating : UserControl
    {
        public MinorRevisionChangeRating()
        {
            InitializeComponent();
        }

        #region Properties

        #region ChangeRatingProperty

        public static readonly DependencyProperty ChangeRatingProperty =
            DependencyProperty.Register("ChangeRating", typeof(Int32),
            typeof(MinorRevisionChangeRating));

        /// <summary>
        /// Gets/Sets the change rating value
        /// </summary>
        public Int32 ChangeRating
        {
            get { return (Int32)GetValue(ChangeRatingProperty); }
            set { SetValue(ChangeRatingProperty, value); }
        }

        #endregion

        #endregion
    }
}
