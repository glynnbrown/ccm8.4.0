﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Interaction logic for AssortmentMinorRevisionSetupHomeTab.xaml
    /// </summary>
    public sealed partial class AssortmentMinorRevisionSetupHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(AssortmentMinorRevisionSetupViewModel), typeof(AssortmentMinorRevisionSetupHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public AssortmentMinorRevisionSetupViewModel ViewModel
        {
            get { return (AssortmentMinorRevisionSetupViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public AssortmentMinorRevisionSetupHomeTab()
        {
            InitializeComponent();
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// Called when the print preview split button is ready to be populated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrintPreviewButton_Loaded(object sender, RoutedEventArgs e)
        {
            //Fluent.SplitButton senderControl = (Fluent.SplitButton)sender;
            //senderControl.Loaded -= PrintPreviewButton_Loaded;

            //if (senderControl.Items.Count == 0)
            //{
            //    PrintOptionInfoList printOptions =
            //    PrintOptionInfoList.FetchByEntityIdContentType(App.ViewState.EntityId, PrintOptionContentType.AssortmentMinorRevision);

            //    foreach (PrintOptionInfo option in printOptions.OrderBy(p => p.Name))
            //    {
            //        Fluent.MenuItem optionItem = new MenuItem();
            //        optionItem.Header = option.Name;
            //        optionItem.CommandParameter = option.Name;

            //        if (option.IsDefault)
            //        {
            //            optionItem.Header = String.Format(CultureInfo.CurrentCulture,
            //                "{0} {1}", option.Name, Message.Generic_PrintPreview_IsDefault);

            //            //set the default against the main button
            //            senderControl.CommandParameter = option.Name;
            //        }

            //        BindingOperations.SetBinding(optionItem, Fluent.MenuItem.CommandProperty,
            //            new Binding(ViewModelProperty.Name + "." + AssortmentMinorRevisionSetupViewModel.PrintPreviewCommandProperty.Path) { Source = this });


            //        senderControl.Items.Add(optionItem);
            //    }

            //}
        }

        #endregion
    }
}
