﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Minor Revision Product Row for Selecting Products
    /// </summary>
    public class MinorRevisionProductRow
    {
        #region Fields

        private ProductInfo _productInfo;
        //private Boolean _includedInUniverse;
        private Boolean _isActionProductValid;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath IsActionProductValidProperty = WpfHelper.GetPropertyPath<MinorRevisionProductRow>(p => p.IsActionProductValid);
        public static readonly PropertyPath ProductInfoProperty = WpfHelper.GetPropertyPath<MinorRevisionProductRow>(p => p.ProductInfo);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the product info
        /// </summary>
        public ProductInfo ProductInfo
        {
            get { return _productInfo; }
        }

        /// <summary>
        /// Returns whether the product is valid for a new action
        /// </summary>
        public Boolean IsActionProductValid
        {
            get { return _isActionProductValid; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public MinorRevisionProductRow(ProductInfo productInfo, Boolean isActionProductValid)
        {
            //Set the properties
            _productInfo = productInfo;
            _isActionProductValid = isActionProductValid;
        }

        #endregion
    }
}