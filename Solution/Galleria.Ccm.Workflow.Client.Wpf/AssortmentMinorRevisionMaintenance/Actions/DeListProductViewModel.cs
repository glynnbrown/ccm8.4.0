﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
// V8-26301 : J.Pickup
//  Removed fincial hierarhcy as a search option (doesn't exist in CCM). 

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// View Model class for the DelistProduct window
    /// </summary>
    public class DeListProductViewModel : ViewModelAttachedControlObject<DeListProductWindow>
    {
        #region Fields

        //Fields
        private Int32 _entityId;
        private AssortmentMinorRevision _minorRevision;
        private LocationInfoListViewModel _locationInfoListViewModel;
        private BulkObservableCollection<Int32> _currentActionsProductIds;

        //Products
        private ObservableCollection<ProductInfo> _selectedUnassignedProducts = new ObservableCollection<ProductInfo>();
        private BulkObservableCollection<ProductInfo> _unassignedProducts = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _unassignedProductsRO;
        private ObservableCollection<MinorRevisionProductRow> _selectedAssignedProducts = new ObservableCollection<MinorRevisionProductRow>();
        private BulkObservableCollection<MinorRevisionProductRow> _assignedProducts = new BulkObservableCollection<MinorRevisionProductRow>();
        private ReadOnlyBulkObservableCollection<MinorRevisionProductRow> _assignedProductsRO;

        #region Selection Fields
        private MinorRevisionProductSearchType _searchType = MinorRevisionProductSearchType.Criteria;
        private DataGridSelectionMode _selectionMode = DataGridSelectionMode.Extended;
        private ProductInfoListViewModel _productInfoListView = new ProductInfoListViewModel();
        private ObservableCollection<ProductInfo> _selectedProducts = new ObservableCollection<ProductInfo>();
        private Boolean _isSearching;
        private Boolean _isSearchQueued;

        private String _searchCriteria;

        private ProductHierarchy _financialHierarchy;
        private ProductGroup _selectedFinancialGroup;

        private ProductUniverseInfo _selectedProductUniverse;
        private ConsumerDecisionTreeInfo _selectedConsumerDecisionTreeInfo;

        private Boolean _containsRecords;
        #endregion
        #endregion

        #region Property Paths

        public static readonly PropertyPath SelectedUnassignedProductsProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectedUnassignedProducts);
        public static readonly PropertyPath SelectedAssignedProductsProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectedAssignedProducts);
        public static readonly PropertyPath UnassignedProductsProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.UnassignedProducts);
        public static readonly PropertyPath AssignedProductsProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.AssignedProducts);
        public static readonly PropertyPath CurrentActionsProductIdsProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.CurrentActionsProductIds);

        public static readonly PropertyPath AddSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.AddSelectedProductsCommand);
        public static readonly PropertyPath RemoveSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.RemoveSelectedProductsCommand);
        public static readonly PropertyPath AddAllProductsCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.AddAllProductsCommand);
        public static readonly PropertyPath RemoveAllProductsCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.RemoveAllProductsCommand);
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.OkCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.CancelCommand);

        #region Selection Property Paths

        //properties
        public static readonly PropertyPath SearchTypeProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SearchType);
        public static readonly PropertyPath SelectionModeProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectionMode);
        public static readonly PropertyPath IsSearchingProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.IsSearching);
        public static readonly PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SearchResults);
        public static readonly PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectedProducts);
        public static readonly PropertyPath SearchCriteriaProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SearchCriteria);
        public static readonly PropertyPath SelectedFinancialGroupProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectedFinancialGroup);
        public static readonly PropertyPath SelectedProductUniverseProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectedProductUniverse);
        public static readonly PropertyPath ContainsRecordsProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.ContainsRecords);
        public static readonly PropertyPath SelectedConsumerDecisionTreeProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectedConsumerDecisionTree);

        //commands
        public static readonly PropertyPath SelectFinancialGroupCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectFinancialGroupCommand);
        public static readonly PropertyPath SelectProductUniverseCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectProductUniverseCommand);
        public static readonly PropertyPath SelectConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<DeListProductViewModel>(p => p.SelectConsumerDecisionTreeCommand);

        #endregion
        #endregion

        #region Properties

        /// <summary>
        /// Returns the current AssortmentMinorRevision
        /// </summary>
        public AssortmentMinorRevision MinorRevision
        {
            get
            {
                return _minorRevision;
            }
        }

        /// <summary>
        /// Returns the current location list
        /// </summary>
        public LocationInfoList MasterLocationInfoList
        {
            get
            {
                return _locationInfoListViewModel.Model;
            }
        }

        /// <summary>
        /// Returns the collection of selected unassigned products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedUnassignedProducts
        {
            get { return _selectedUnassignedProducts; }
        }

        /// <summary>
        /// Returns the collection of unassigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> UnassignedProducts
        {
            get
            {
                if (_unassignedProductsRO == null)
                {
                    _unassignedProductsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_unassignedProducts);
                }
                return _unassignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected assigned products
        /// </summary>
        public ObservableCollection<MinorRevisionProductRow> SelectedAssignedProducts
        {
            get { return _selectedAssignedProducts; }
        }

        /// <summary>
        /// Returns the collection of Assigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<MinorRevisionProductRow> AssignedProducts
        {
            get
            {
                if (_assignedProductsRO == null)
                {
                    _assignedProductsRO = new ReadOnlyBulkObservableCollection<MinorRevisionProductRow>(_assignedProducts);
                }
                return _assignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the current list of actions product ids 
        /// </summary>
        public BulkObservableCollection<Int32> CurrentActionsProductIds
        {
            get { return _currentActionsProductIds; }
        }

        #region Selection Properties

        /// <summary>
        /// Gets/Sets the selection type
        /// </summary>
        public MinorRevisionProductSearchType SearchType
        {
            get { return _searchType; }
            set
            {
                _searchType = value;
                OnPropertyChanged(SearchTypeProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Returns the selection mode for this instance
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return _selectionMode; }
        }

        /// <summary>
        /// Returns true if a search is currently being performed
        /// </summary>
        public Boolean IsSearching
        {
            get { return _isSearching; }
            private set
            {
                _isSearching = value;
                OnPropertyChanged(IsSearchingProperty);
            }
        }

        /// <summary>
        /// Returns the readonly collection of search results
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> SearchResults
        {
            get
            {
                return _productInfoListView.BindingView;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedProducts
        {
            get { return _selectedProducts; }
        }

        /// <summary>
        /// Gets/Sets the search criteria value.
        /// </summary>
        public String SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                _searchCriteria = value;
                OnPropertyChanged(SearchCriteriaProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Gets/Sets the selected financial group search criteria
        /// </summary>
        public ProductGroup SelectedFinancialGroup
        {
            get { return _selectedFinancialGroup; }
            set
            {
                _selectedFinancialGroup = value;
                OnPropertyChanged(SelectedFinancialGroupProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ProductUniverseInfo SelectedProductUniverse
        {
            get { return _selectedProductUniverse; }
            set
            {
                _selectedProductUniverse = value;
                OnPropertyChanged(SelectedProductUniverseProperty);
                PerformSearch();
            }
        }


        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ConsumerDecisionTreeInfo SelectedConsumerDecisionTree
        {
            get { return _selectedConsumerDecisionTreeInfo; }
            set
            {
                _selectedConsumerDecisionTreeInfo = value;
                OnPropertyChanged(SelectedConsumerDecisionTreeProperty);
                PerformSearch();
            }
        }


        /// <summary>
        /// Returns true if the search results grid contains records
        /// </summary>
        public Boolean ContainsRecords
        {
            get { return _containsRecords; }
            private set
            {
                _containsRecords = value;
                OnPropertyChanged(ContainsRecordsProperty);
            }
        }

        #endregion
        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public DeListProductViewModel(AssortmentMinorRevision assortmentMinorRevision, ConsumerDecisionTreeInfo consumerDecisionTreeInfo)
            : this(
            assortmentMinorRevision,
            App.ViewState.EntityId,
            consumerDecisionTreeInfo)
        { }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public DeListProductViewModel(AssortmentMinorRevision assortmentMinorRevision,
                                      Int32 entityId, ConsumerDecisionTreeInfo consumerDecisionTreeInfo)
        {
            //Set minor revision
            _minorRevision = assortmentMinorRevision;
            _entityId = entityId;

            //Set location list
            _locationInfoListViewModel = new LocationInfoListViewModel();
            _locationInfoListViewModel.FetchAllForEntity();

            //Set CDT
            if (consumerDecisionTreeInfo != null)
            {
                this.SearchType = MinorRevisionProductSearchType.ConsumerDecisionTree;
                this.SelectedConsumerDecisionTree = consumerDecisionTreeInfo;
            }

            _productInfoListView.ModelChanged += ProductInfoListView_ModelChanged;
            if (_productInfoListView.Model != null)
            {
                _productInfoListView.Model.AddedNew += ProductInfoListView_AddedNew;
            }

            //Construct taken products ids for validation purposes, only 1 action per product
            BulkObservableCollection<Int32> actionProductIds = new BulkObservableCollection<Int32>();
            //List actions
            if (this.MinorRevision.ListActions != null
                && this.MinorRevision.ListActions.Count > 0)
            {
                actionProductIds.AddRange(this.MinorRevision.ListActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //DeList actions
            if (this.MinorRevision.DeListActions != null
                && this.MinorRevision.DeListActions.Count > 0)
            {
                actionProductIds.AddRange(this.MinorRevision.DeListActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //Amend Dist actions
            if (this.MinorRevision.AmendDistributionActions != null
                && this.MinorRevision.AmendDistributionActions.Count > 0)
            {
                actionProductIds.AddRange(this.MinorRevision.AmendDistributionActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //Assign to field
            _currentActionsProductIds = actionProductIds;
        }

        #endregion

        #region Commands

        #region Ok

        private RelayCommand _okCommand;

        /// <summary>
        /// Advances the process to the next step
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => Ok_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean OK_CanExecute()
        {
            if (this.AssignedProducts.Count <= 0)
            {
                this.OkCommand.DisabledReason = Message.DeListProduct_Ok_NoProductsSelected;
                return false;
            }
            else if (!this.AssignedProducts.All(p => p.IsActionProductValid))
            {
                this.OkCommand.DisabledReason = Message.DeListProduct_Next_InvalidProducts;
                return false;
            }
            return true;
        }

        private void Ok_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.AssignedProducts.Count > 0)
            {
                //Create location dictionary
                Dictionary<Int16, LocationInfo> locationInfoLookup = this.MasterLocationInfoList.ToDictionary(p => p.Id);

                //Fetch full fat products
                ProductList products = ProductList.FetchByProductIds(this.AssignedProducts.Select(p => p.ProductInfo.Id));
                Dictionary<Int32, Product> productLookup = products.ToDictionary(p => p.Id);

                //Enumerate through selected products
                foreach (MinorRevisionProductRow productRow in this.AssignedProducts)
                {
                    #region Create Action
                    //Calculate new priority number
                    Int32 newPriority = this.MinorRevision.TotalActionsCount + 1;

                    //Create action
                    AssortmentMinorRevisionDeListAction action = AssortmentMinorRevisionDeListAction.NewAssortmentMinorRevisionDeListAction(productRow.ProductInfo.Id, productRow.ProductInfo.Gtin, productRow.ProductInfo.Name, newPriority);

                    //Add all locations to action
                    foreach (LocationInfo locationInfo in _locationInfoListViewModel.Model)
                    {
                        action.Locations.Add(AssortmentMinorRevisionDeListActionLocation.NewAssortmentMinorRevisionDeListActionLocation(locationInfo.Id, locationInfo.Code, locationInfo.Name));
                    }

                    //Add Action
                    this.MinorRevision.DeListActions.Add(action);

                    #endregion
                }
            }

            if (SearchType == MinorRevisionProductSearchType.ConsumerDecisionTree &&
                SelectedConsumerDecisionTree != null && !_minorRevision.ConsumerDecisionTreeId.HasValue)
            {
                _minorRevision.ConsumerDecisionTreeId = SelectedConsumerDecisionTree.Id;
            }

            if (this.AttachedControl != null)
            {
                //Close window
                this.AttachedControl.Close();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancel the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.AttachedControl != null)
            {
                //Close window
                this.AttachedControl.Close();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_AddSelectedProducts
                    };
                    base.ViewModelCommands.Add(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            //must have some unassigned products selected
            return (this.SelectedUnassignedProducts.Count > 0);
        }

        private void AddSelectedProducts_Executed()
        {
            //Get list to add
            List<ProductInfo> addList = this.SelectedUnassignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (ProductInfo productInfo in addList)
            {
                if (!currentProductIds.Contains(productInfo.Id))
                {
                    addToSelectedProducts.Add(productInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo productInfo in addToSelectedProducts)
            {
                //Check if action product is valid
                Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //add the product to the list of assigned products
                _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
            }

            //clear the selected available products
            this.SelectedUnassignedProducts.Clear();

            //remove from the available
            this._unassignedProducts.RemoveRange(addList);
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_RemoveSelectedProducts
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        private Boolean RemoveSelectedProducts_CanExecute()
        {
            if (this.SelectedAssignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            //Get list to remove
            List<MinorRevisionProductRow> addList = this.SelectedAssignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.UnassignedProducts.Select(p => p.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (MinorRevisionProductRow productRow in addList)
            {
                if (!currentProductIds.Contains(productRow.ProductInfo.Id))
                {
                    addToSelectedProducts.Add(productRow.ProductInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo delistProduct in addToSelectedProducts)
            {
                //add the product to the list of unassigned products
                _unassignedProducts.Add(delistProduct);
            }

            //clear the selected assigned products
            this.SelectedAssignedProducts.Clear();

            //remove from the assigned
            this._assignedProducts.RemoveRange(addList);
        }

        #endregion

        #region AddAllProductsCommand

        private RelayCommand _addAllProductsCommand;

        public RelayCommand AddAllProductsCommand
        {
            get
            {
                if (_addAllProductsCommand == null)
                {
                    _addAllProductsCommand = new RelayCommand(
                        p => AddAllProducts_Executed(),
                        p => AddAllProducts_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_AddAllProducts
                    };
                    base.ViewModelCommands.Add(_addAllProductsCommand);
                }
                return _addAllProductsCommand;
            }
        }

        private Boolean AddAllProducts_CanExecute()
        {
            if (this.UnassignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddAllProducts_Executed()
        {
            //Get list to add
            List<ProductInfo> addList = this.UnassignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (ProductInfo productInfo in addList)
            {
                if (!currentProductIds.Contains(productInfo.Id))
                {
                    addToSelectedProducts.Add(productInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo productInfo in addToSelectedProducts)
            {
                //Check if action product is valid
                Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //add the product to the list of assigned products
                _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
            }

            //clear the selected available products
            this.SelectedUnassignedProducts.Clear();

            //remove from the available
            this._unassignedProducts.RemoveRange(addList);
        }

        #endregion

        #region RemoveAllProductsCommand

        private RelayCommand _removeAllProductsCommand;

        public RelayCommand RemoveAllProductsCommand
        {
            get
            {
                if (_removeAllProductsCommand == null)
                {
                    _removeAllProductsCommand = new RelayCommand(
                        p => RemoveAllProducts_Executed(),
                        p => RemoveAllProducts_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_RemoveAllProducts
                    };
                    base.ViewModelCommands.Add(_removeAllProductsCommand);
                }
                return _removeAllProductsCommand;
            }
        }

        private Boolean RemoveAllProducts_CanExecute()
        {
            if (this.AssignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveAllProducts_Executed()
        {
            //Get list to add
            List<MinorRevisionProductRow> addList = this.AssignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.UnassignedProducts.Select(p => p.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (MinorRevisionProductRow productRow in addList)
            {
                if (!currentProductIds.Contains(productRow.ProductInfo.Id))
                {
                    addToSelectedProducts.Add(productRow.ProductInfo);
                }
            }

            //for each product in the list of selected assigned products
            foreach (ProductInfo delistProduct in addToSelectedProducts)
            {
                //add the product to the list of unassigned products
                _unassignedProducts.Add(delistProduct);
            }

            //clear the selected assigned products
            this.SelectedAssignedProducts.Clear();

            //remove from the assigned
            this._assignedProducts.RemoveRange(addList);
        }

        #endregion

        #region Selection Commands

        #region SelectFinancialGroup

        private RelayCommand _selectFinancialGroupCommand;

        /// <summary>
        /// Opens the financial group selection window.
        /// </summary>
        public RelayCommand SelectFinancialGroupCommand
        {
            get
            {
                if (_selectFinancialGroupCommand == null)
                {
                    _selectFinancialGroupCommand = new RelayCommand(
                        p => SelectFinancialGroup_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    //base.ViewModelCommands.Add(_selectFinancialGroupCommand);
                }
                return _selectFinancialGroupCommand;
            }
        }


        private void SelectFinancialGroup_Executed()
        {
            //if (this.AttachedControl != null)
            //{
            //launch the selection window in single mode.
            MerchGroupSelectionWindow win = new MerchGroupSelectionWindow();

            win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
            App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

            if (win.DialogResult == true)
            {
                this.SelectedFinancialGroup = win.SelectionResult.First();
            }
            //}
        }

        #endregion

        #region SelectProductUniverse

        private RelayCommand _selectProductUniverseCommand;

        public RelayCommand SelectProductUniverseCommand
        {
            get
            {
                if (_selectProductUniverseCommand == null)
                {
                    _selectProductUniverseCommand = new RelayCommand(
                        p => SelectProductUniverse_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                }
                return _selectProductUniverseCommand;
            }
        }

        private void SelectProductUniverse_Executed()
        {
            ProductUniverseSelectionWindow productUniverseWindow = new ProductUniverseSelectionWindow();

            App.ShowWindow(productUniverseWindow, this.AttachedControl, true);

            if (productUniverseWindow.SelectedProductUniverse != null)
            {
                this.SelectedProductUniverse = productUniverseWindow.SelectedProductUniverse;
            }
        }

        #endregion

        #region SelectConsumerDecisionTree

        private RelayCommand _selectConsumerDecisionTreeCommand;

        public RelayCommand SelectConsumerDecisionTreeCommand
        {
            get
            {
                if (_selectConsumerDecisionTreeCommand == null)
                {
                    _selectConsumerDecisionTreeCommand = new RelayCommand(
                        p => SelectConsumerDecisionTree_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                }
                return _selectConsumerDecisionTreeCommand;
            }
        }

        private void SelectConsumerDecisionTree_Executed()
        {
            ConsumerDecisionTreeWindow consumerDecisionTreeWindow = new ConsumerDecisionTreeWindow();
            App.ShowWindow(consumerDecisionTreeWindow, this.AttachedControl, true);

            if (consumerDecisionTreeWindow.SelectionResult != null)
            {
                this.SelectedConsumerDecisionTree = consumerDecisionTreeWindow.SelectionResult;
            }
        }

        #endregion
        #endregion
        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change to the product info list model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
        {

            if (e.OldModel != null)
            {
                ProductInfoList oldList = (ProductInfoList)e.NewModel;
                oldList.AddedNew -= ProductInfoListView_AddedNew;
            }
            if (e.NewModel != null)
            {
                ProductInfoList newList = (ProductInfoList)e.NewModel;
                newList.AddedNew += ProductInfoListView_AddedNew;
                this._unassignedProducts.AddRange(newList);
            }

            //mark as finished searching
            this.IsSearching = false;

            //If the productInfoListView doesn't contain any results the "No Records" message should be displayed
            this.ContainsRecords = (_productInfoListView.Model.Count > 0);

            //check if another search is queued
            if (_isSearchQueued)
            {
                PerformSearch();
            }
        }

        /// <summary>
        /// Responds ProductInfoListView adding items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_AddedNew(object sender, Csla.Core.AddedNewEventArgs<ProductInfo> e)
        {
            this._unassignedProducts.Add(e.NewObject);
        }

        #endregion

        #region Methods

        #region Selection Methods

        /// <summary>
        /// Performs the search operation
        /// </summary>
        private void PerformSearch()
        {
            this._unassignedProducts.Clear();

            Int32 entityId = App.ViewState.EntityId;

            if (!this.IsSearching)
            {
                _isSearchQueued = false;

                this.IsSearching = true;

                //search asyncronously
                switch (this.SearchType)
                {
                    case MinorRevisionProductSearchType.Criteria:
                        _productInfoListView.BeginFetchByEntityIdSearchCriteria(entityId, this.SearchCriteria != null ? this.SearchCriteria : "");
                        break;

                    case MinorRevisionProductSearchType.ProductUniverse:
                        _productInfoListView.BeginFetchByProductUniverseId((this.SelectedProductUniverse != null) ? this.SelectedProductUniverse.Id : 0);
                        break;
                    case MinorRevisionProductSearchType.ConsumerDecisionTree:
                        _productInfoListView.BeginFetchByConsumerDecisionTree((this.SelectedConsumerDecisionTree != null) ? this.SelectedConsumerDecisionTree.Id : 0);
                        break;
                    default: throw new NotImplementedException();
                }
            }
            else
            {
                _isSearchQueued = true;
            }

        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _minorRevision = null;
                    _selectedAssignedProducts.Clear();
                    _selectedUnassignedProducts.Clear();
                    _unassignedProducts.Clear();
                    _assignedProducts.Clear();
                    _productInfoListView.ModelChanged -= ProductInfoListView_ModelChanged;
                    if (_productInfoListView.Model != null)
                    {
                        _productInfoListView.Model.AddedNew -= ProductInfoListView_AddedNew;
                    }
                    _productInfoListView.Dispose();
                    _productInfoListView = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}