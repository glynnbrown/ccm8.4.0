﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Interaction logic for EditDistributionLocationsWindow.xaml
    /// </summary>
    public partial class EditDistributionLocationsWindow : ExtendedRibbonWindow
    {
        #region Fields

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(EditDistributionLocationsViewModel), typeof(EditDistributionLocationsWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public EditDistributionLocationsViewModel ViewModel
        {
            get { return (EditDistributionLocationsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            EditDistributionLocationsWindow senderControl = (EditDistributionLocationsWindow)obj;

            if (e.OldValue != null)
            {
                EditDistributionLocationsViewModel oldModel = (EditDistributionLocationsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                EditDistributionLocationsViewModel newModel = (EditDistributionLocationsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public EditDistributionLocationsWindow(AssortmentMinorRevision assortmentMinorRevision, ProductLocationLinkRowViewModel selectedLinkRow)
        {
            InitializeComponent();

            this.ViewModel = new EditDistributionLocationsViewModel(assortmentMinorRevision, selectedLinkRow);

            this.xAvailableLocationsGrid.SortByDescriptions.Add(new System.ComponentModel.SortDescription("Rank", System.ComponentModel.ListSortDirection.Ascending));
            this.xRangedLocationsGrid.SortByDescriptions.Add(new System.ComponentModel.SortDescription("Rank", System.ComponentModel.ListSortDirection.Ascending));
        }

        #endregion

        #region Methods

        #endregion

        #region Event Handlers

        #region Available Grid

        /// <summary>
        /// Adds the doubleclicked row to the current Locations
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableLocationsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.AddSelectedLocationsCommand.Execute();
        }

        /// <summary>
        /// Removes the passed Locations from the current sublevel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            this.ViewModel.RemoveSelectedLocationsCommand.Execute();
        }

        /// <summary>
        /// Clears the selected items of the assigned grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableLocationsGrid_ItemsSelected(object sender, ExtendedDataGridItemSelectedEventArgs e)
        {
            this.xAvailableLocationsGrid.SelectedItems.Clear();
        }

        /// <summary>
        /// Makes an add request for all selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableLocationsRowContext_AddSelected(object sender, RoutedEventArgs e)
        {
            this.ViewModel.AddSelectedLocationsCommand.Execute();
        }

        /// <summary>
        /// Makes an add request for all selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAvailableLocationsRowContext_RemoveSelected(object sender, RoutedEventArgs e)
        {
            this.ViewModel.RemoveSelectedLocationsCommand.Execute();
        }

        #endregion

        #region Ranged Grid

        /// <summary>
        /// Removes the location from the current cluster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRangedLocationsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.RemoveSelectedLocationsCommand.Execute();
        }

        /// <summary>
        /// Adds the dropped locations to the cluster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRangedLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            this.ViewModel.AddSelectedLocationsCommand.Execute();
        }

        /// <summary>
        /// Makes a remove request for all selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRangedLocationsRowContext_RemoveSelected(object sender, RoutedEventArgs e)
        {
            this.ViewModel.RemoveSelectedLocationsCommand.Execute();
        }

        #endregion

        /// <summary>
        /// Close button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        /// <summary>
        /// section 508 grid changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationsGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAssigned = senderControl?.Name == xRangedLocationsGrid.Name;

            if (e.Key == Key.Return)
            {
                if (!isAssigned) this.ViewModel?.AddSelectedLocationsCommand.Execute();
                else this.ViewModel?.RemoveSelectedLocationsCommand.Execute();
                Keyboard.Focus(isAssigned ? xRangedLocationsGrid : xAvailableLocationsGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAssigned ? xAvailableLocationsGrid : xRangedLocationsGrid);
                e.Handled = true;
            }
        }

        #endregion

        #region Commands

        #endregion

        #region Window Close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    
    }
}