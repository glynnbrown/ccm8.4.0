﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Denotes the steps available in the list product wizard
    /// </summary>
    public enum ListProductWizardStep
    {
        SelectProducts,
        SelectTargets,
        SelectCDTNode
    }

    /// <summary>
    /// Denotes the type of target selection
    /// </summary>
    public enum TargetSelectionType
    {
        AllLocations,
        SpecificRanges,
        SpecificDistribution,
        LocationSubset
    }

    /// <summary>
    /// View model class for the ListProductWindow
    /// </summary>
    public class ListProductViewModel : ViewModelAttachedControlObject<ListProductWindow>
    {
        #region Fields

        //Wizard related
        private Int32 _entityId;
        private ListProductWizardStep _currentStep = ListProductWizardStep.SelectProducts;
        private TargetSelectionType _selectedTargetSelectionType = TargetSelectionType.AllLocations;
        private Int16 _currentStepNumber = 1;
        private AssortmentMinorRevision _assortmentMinorRevision = null;
        private Boolean _isCDTSetup = false;
        private BulkObservableCollection<Int32> _currentActionsProductIds;
        private BulkObservableCollection<Int32> _startingUniverseProductIds;

        //Selected data
        private ObservableCollection<LocationInfo> _selectedLocations = new ObservableCollection<LocationInfo>();
        private BulkObservableCollection<ListProductRow> _cdtProducts = new BulkObservableCollection<ListProductRow>();
        private ReadOnlyBulkObservableCollection<ListProductRow> _cdtProductsRO;
        private Single _selectedDistributionPercentage = 0;

        //Products
        private ObservableCollection<ProductInfo> _selectedUnassignedProducts = new ObservableCollection<ProductInfo>();
        private BulkObservableCollection<ProductInfo> _unassignedProducts = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _unassignedProductsRO;
        private ObservableCollection<MinorRevisionProductRow> _selectedAssignedProducts = new ObservableCollection<MinorRevisionProductRow>();
        private BulkObservableCollection<MinorRevisionProductRow> _assignedProducts = new BulkObservableCollection<MinorRevisionProductRow>();
        private ReadOnlyBulkObservableCollection<MinorRevisionProductRow> _assignedProductsRO;

        //locations
        private LocationInfoListViewModel _locationInfoListView;

        //ConsumerDecisionTree
        private Model.ConsumerDecisionTree _currentConsumerDecisionTree;

        //cdt nodes
        private BulkObservableCollection<ConsumerDecisionTreeNode> _availableCDTNodes = new BulkObservableCollection<ConsumerDecisionTreeNode>();
        private ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> _availableCDTNodesRO;

        #region Selection Fields
        private MinorRevisionProductSearchType _searchType = MinorRevisionProductSearchType.Criteria;
        private DataGridSelectionMode _selectionMode = DataGridSelectionMode.Extended;
        private ProductInfoListViewModel _productInfoListView = new ProductInfoListViewModel();
        private ObservableCollection<Galleria.Ccm.Model.ProductInfo> _selectedProducts = new ObservableCollection<Galleria.Ccm.Model.ProductInfo>();
        private Boolean _isSearching;
        private Boolean _isSearchQueued;

        private String _searchCriteria;

        private ProductHierarchy _financialHierarchy;
        private ProductGroup _selectedFinancialGroup;

        private ProductUniverseInfo _selectedProductUniverse;
        private Model.ConsumerDecisionTreeInfo _selectedConsumerDecisionTreeInfo;

        private Boolean _containsRecords;
        #endregion
        #endregion

        #region Property Paths

        //Wizard related
        public static readonly PropertyPath IsPreviousCommandVisibleProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.IsPreviousCommandVisible);
        public static readonly PropertyPath IsNextCommandVisibleProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.IsNextCommandVisible);
        public static readonly PropertyPath IsFinishCommandVisibleProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.IsFinishCommandVisible);
        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath CurrentStepNumberProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.CurrentStepNumber);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.NextCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.PreviousCommand);
        public static readonly PropertyPath FinishCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.FinishCommand);
        public static readonly PropertyPath NumberOfStagesProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.NumberOfStages);
        public static readonly PropertyPath SelectedTargetSelectionTypeProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedTargetSelectionType);
        public static readonly PropertyPath IsCDTSetupProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.IsCDTSetup);
        public static readonly PropertyPath AvailableCDTNodesProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.AvailableCDTNodes);
        public static readonly PropertyPath CDTProductsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.CDTProducts);
        public static readonly PropertyPath CurrentActionsProductIdsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.CurrentActionsProductIds);
        public static readonly PropertyPath SelectedDistributionPercentageProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedDistributionPercentage);

        //Selected Data
        public static readonly PropertyPath SelectedUnassignedProductsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedUnassignedProducts);
        public static readonly PropertyPath SelectedAssignedProductsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedAssignedProducts);
        public static readonly PropertyPath UnassignedProductsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.UnassignedProducts);
        public static readonly PropertyPath AssignedProductsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.AssignedProducts);
        public static readonly PropertyPath SelectedLocationsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedLocations);
        //public static readonly PropertyPath SelectedRangesProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedRanges); 

        //Commands
        public static readonly PropertyPath AddSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.AddSelectedProductsCommand);
        public static readonly PropertyPath RemoveSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.RemoveSelectedProductsCommand);
        public static readonly PropertyPath AddAllProductsCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.AddAllProductsCommand);
        public static readonly PropertyPath RemoveAllProductsCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.RemoveAllProductsCommand);

        #region Selection Property Paths

        //properties
        public static readonly PropertyPath SearchTypeProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SearchType);
        public static readonly PropertyPath SelectionModeProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectionMode);
        public static readonly PropertyPath IsSearchingProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.IsSearching);
        public static readonly PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SearchResults);
        public static readonly PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedProducts);
        public static readonly PropertyPath SearchCriteriaProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SearchCriteria);
        public static readonly PropertyPath SelectedFinancialGroupProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedFinancialGroup);
        public static readonly PropertyPath SelectedProductUniverseProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedProductUniverse);
        public static readonly PropertyPath ContainsRecordsProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.ContainsRecords);
        public static readonly PropertyPath SelectedConsumerDecisionTreeProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectedConsumerDecisionTree);

        //commands
        public static readonly PropertyPath SelectFinancialGroupCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectFinancialGroupCommand);
        public static readonly PropertyPath SelectProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectProductUniverseCommand);
        public static readonly PropertyPath SelectConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<ListProductViewModel>(p => p.SelectConsumerDecisionTreeCommand);

        #endregion
        #endregion

        #region Properties

        /// <summary>
        /// Returns whether the timeline has a cdt setup
        /// </summary>
        public Boolean IsCDTSetup
        {
            get { return _isCDTSetup; }
        }

        /// <summary>
        /// Returns the current AssortmentMinorRevision
        /// </summary>
        public AssortmentMinorRevision AssortmentMinorRevision
        {
            get
            {
                return _assortmentMinorRevision;
            }
        }

        /// <summary>
        /// Returns the master location info list
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationInfo> MasterLocationInfoList
        {
            get
            {
                return _locationInfoListView.BindingView;
            }
        }

        /// <summary>
        /// Returns true if the cancel command should be available on the current step
        /// </summary>
        public Boolean IsPreviousCommandVisible
        {
            get
            {
                if (this.CurrentStep == ListProductWizardStep.SelectTargets ||
                    this.CurrentStep == ListProductWizardStep.SelectCDTNode)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns true if the next command should be available
        /// for the current step
        /// </summary>
        public Boolean IsNextCommandVisible
        {
            get
            {
                if (this.CurrentStep == ListProductWizardStep.SelectProducts ||
                    (this.CurrentStep == ListProductWizardStep.SelectTargets && this.IsCDTSetup))
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Returns true if the finish command should be available
        /// for the current step
        /// </summary>
        public Boolean IsFinishCommandVisible
        {
            get
            {
                if (this.CurrentStep == ListProductWizardStep.SelectCDTNode
                    || (this.CurrentStep == ListProductWizardStep.SelectTargets && !this.IsCDTSetup))
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// The current step that the data selection wizard is at
        /// </summary>
        public ListProductWizardStep CurrentStep
        {
            get { return _currentStep; }
            set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                //Refresh button bindings
                OnPropertyChanged(IsNextCommandVisibleProperty);
                OnPropertyChanged(IsPreviousCommandVisibleProperty);
                OnPropertyChanged(IsFinishCommandVisibleProperty);
            }
        }

        /// <summary>
        /// The current step number that the data selection wizard is at
        /// </summary>
        public Int16 CurrentStepNumber
        {
            get { return _currentStepNumber; }
        }

        /// <summary>
        /// Returns the number of stages
        /// </summary>
        public Int32 NumberOfStages
        {
            get { return (this.IsCDTSetup) ? 3 : 2; }
        }

        /// <summary>
        /// Returns the collection of selected unassigned products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedUnassignedProducts
        {
            get { return _selectedUnassignedProducts; }
        }

        /// <summary>
        /// Returns the collection of unassigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> UnassignedProducts
        {
            get
            {
                if (_unassignedProductsRO == null)
                {
                    _unassignedProductsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_unassignedProducts);
                }
                return _unassignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected assigned products
        /// </summary>
        public ObservableCollection<MinorRevisionProductRow> SelectedAssignedProducts
        {
            get { return _selectedAssignedProducts; }
        }

        /// <summary>
        /// Returns the collection of Assigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<MinorRevisionProductRow> AssignedProducts
        {
            get
            {
                if (_assignedProductsRO == null)
                {
                    _assignedProductsRO = new ReadOnlyBulkObservableCollection<MinorRevisionProductRow>(_assignedProducts);
                }
                return _assignedProductsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected target selection type
        /// </summary>
        public TargetSelectionType SelectedTargetSelectionType
        {
            get { return _selectedTargetSelectionType; }
            set
            {
                _selectedTargetSelectionType = value;
                OnSelectedTargetSelectionTypeChanged(_selectedTargetSelectionType);
                OnPropertyChanged(SelectedTargetSelectionTypeProperty);
            }
        }

        /// <summary>
        /// The selected locations
        /// </summary>
        public ObservableCollection<LocationInfo> SelectedLocations
        {
            get { return _selectedLocations; }
            set
            {
                _selectedLocations = value;
                OnPropertyChanged(SelectedLocationsProperty);
            }
        }

        /// <summary>
        /// The selected ranges
        /// </summary>
        //public ObservableCollection<ScenarioModelNode> SelectedRanges
        //{
        //    get { return _selectedRanges; }
        //    set
        //    {
        //        _selectedRanges = value;
        //        OnPropertyChanged(SelectedRangesProperty);
        //    }
        //}

        /// <summary>
        /// Returns the collection of available CDT Nodes
        /// </summary>
        public ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode> AvailableCDTNodes
        {
            get
            {
                if (_availableCDTNodesRO == null)
                {
                    _availableCDTNodesRO = new ReadOnlyBulkObservableCollection<ConsumerDecisionTreeNode>(_availableCDTNodes);
                }
                return _availableCDTNodesRO;
            }
        }

        /// <summary>
        /// Returns the collection of cdt product assignment
        /// </summary>
        public ReadOnlyBulkObservableCollection<ListProductRow> CDTProducts
        {
            get
            {
                if (_cdtProductsRO == null)
                {
                    _cdtProductsRO = new ReadOnlyBulkObservableCollection<ListProductRow>(_cdtProducts);
                }
                return _cdtProductsRO;
            }
        }

        /// <summary>
        /// Returns the current list of actions product ids 
        /// </summary>
        public BulkObservableCollection<Int32> CurrentActionsProductIds
        {
            get { return _currentActionsProductIds; }
        }

        /// <summary>
        /// Gets/Sets the selected distribution percentage
        /// </summary>
        public Single SelectedDistributionPercentage
        {
            get { return _selectedDistributionPercentage; }
            set
            {
                _selectedDistributionPercentage = value;
                OnPropertyChanged(SelectedDistributionPercentageProperty);
            }
        }

        #region Selection Properties

        /// <summary>
        /// Gets/Sets the selection type
        /// </summary>
        public MinorRevisionProductSearchType SearchType
        {
            get { return _searchType; }
            set
            {
                _searchType = value;
                OnPropertyChanged(SearchTypeProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Returns the selection mode for this instance
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return _selectionMode; }
        }

        /// <summary>
        /// Returns true if a search is currently being performed
        /// </summary>
        public Boolean IsSearching
        {
            get { return _isSearching; }
            private set
            {
                _isSearching = value;
                OnPropertyChanged(IsSearchingProperty);
            }
        }

        /// <summary>
        /// Returns the readonly collection of search results
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> SearchResults
        {
            get
            {
                return _productInfoListView.BindingView;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedProducts
        {
            get { return _selectedProducts; }
        }

        /// <summary>
        /// Gets/Sets the search criteria value.
        /// </summary>
        public String SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                _searchCriteria = value;
                OnPropertyChanged(SearchCriteriaProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Gets/Sets the selected financial group search criteria
        /// </summary>
        public ProductGroup SelectedFinancialGroup
        {
            get { return _selectedFinancialGroup; }
            set
            {
                _selectedFinancialGroup = value;
                OnPropertyChanged(SelectedFinancialGroupProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ProductUniverseInfo SelectedProductUniverse
        {
            get { return _selectedProductUniverse; }
            set
            {
                _selectedProductUniverse = value;
                OnPropertyChanged(SelectedProductUniverseProperty);
                PerformSearch();
            }
        }


        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ConsumerDecisionTreeInfo SelectedConsumerDecisionTree
        {
            get { return _selectedConsumerDecisionTreeInfo; }
            set
            {
                _selectedConsumerDecisionTreeInfo = value;
                if (_selectedConsumerDecisionTreeInfo != null && _selectedConsumerDecisionTreeInfo.Id > 0)
                {
                    _currentConsumerDecisionTree = Model.ConsumerDecisionTree.FetchById(_selectedConsumerDecisionTreeInfo.Id);
                    _isCDTSetup = true;
                    //Get available nodes
                    IEnumerable<ConsumerDecisionTreeNode> nodes = _currentConsumerDecisionTree.GetAllNodes();

                    //Clear any existing just in case
                    this._availableCDTNodes.Clear();

                    //Add nodes to the collection
                    this._availableCDTNodes.AddRange(nodes);
                }
                OnPropertyChanged(SelectedConsumerDecisionTreeProperty);
                PerformSearch();
            }
        }


        /// <summary>
        /// Returns true if the search results grid contains records
        /// </summary>
        public Boolean ContainsRecords
        {
            get { return _containsRecords; }
            private set
            {
                _containsRecords = value;
                OnPropertyChanged(ContainsRecordsProperty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ListProductViewModel(AssortmentMinorRevision assortmentMinorRevision, Model.ConsumerDecisionTreeInfo consumerDecisionTreeInfo)
            : this(App.ViewState.EntityId, assortmentMinorRevision, consumerDecisionTreeInfo)
        { }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        public ListProductViewModel(Int32 entityId, AssortmentMinorRevision assortmentMinorRevision,
            Model.ConsumerDecisionTreeInfo consumerDecisionTreeInfo)
        {
            _assortmentMinorRevision = assortmentMinorRevision;
            _locationInfoListView = new LocationInfoListViewModel();
            _locationInfoListView.FetchAllForEntity();
            _entityId = entityId;

            _productInfoListView.ModelChanged += ProductInfoListView_ModelChanged;
            if (_productInfoListView.Model != null)
            {
                _productInfoListView.Model.AddedNew += ProductInfoListView_AddedNew;
            }

            ProductHierarchyViewModel financialHierarchyView = new ProductHierarchyViewModel();
            financialHierarchyView.FetchMerchandisingHierarchyForCurrentEntity();
            _financialHierarchy = financialHierarchyView.Model;
            _selectedFinancialGroup = _financialHierarchy.FetchAllGroups().First(g => g.ChildList.Count == 0);

            if (consumerDecisionTreeInfo != null)
            {
                SelectedConsumerDecisionTree = consumerDecisionTreeInfo;
                SearchType = MinorRevisionProductSearchType.ConsumerDecisionTree;
                _isCDTSetup = true;
            }

            //Fetch product infos
            //ProductInfoList productInfoList = ProductInfoList.FetchByEntityIdProductIds(entityId, availableProductIds);

            //Add items
            //this._unassignedProducts.Clear();
            //this._unassignedProducts.AddRange(productInfoList);


            //Construct taken products ids for validation purposes, only 1 action per product
            BulkObservableCollection<Int32> actionProductIds = new BulkObservableCollection<Int32>();
            //List actions
            if (this.AssortmentMinorRevision.ListActions != null
                && this.AssortmentMinorRevision.ListActions.Count > 0)
            {
                actionProductIds.AddRange(this.AssortmentMinorRevision.ListActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //DeList actions
            if (this.AssortmentMinorRevision.DeListActions != null
                && this.AssortmentMinorRevision.DeListActions.Count > 0)
            {
                actionProductIds.AddRange(this.AssortmentMinorRevision.DeListActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //Amend Dist actions
            if (this.AssortmentMinorRevision.AmendDistributionActions != null
                && this.AssortmentMinorRevision.AmendDistributionActions.Count > 0)
            {
                actionProductIds.AddRange(this.AssortmentMinorRevision.AmendDistributionActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //Assign to field
            _currentActionsProductIds = actionProductIds;

            if (this.IsCDTSetup)
            {
                //Get available nodes
                IEnumerable<ConsumerDecisionTreeNode> nodes = _currentConsumerDecisionTree.GetAllNodes();

                //Clear any existing just in case
                this._availableCDTNodes.Clear();

                //Add nodes to the collection
                this._availableCDTNodes.AddRange(nodes);
            }

        }

        #endregion

        #region Commands

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// Advances the process to the next step
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Next_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case ListProductWizardStep.SelectProducts:
                    if (this.AssignedProducts.Count <= 0)
                    {
                        this.NextCommand.DisabledReason = Message.ListProduct_Next_NoAssignedProducts;
                        return false;
                    }
                    else if (!this.AssignedProducts.All(p => p.IsActionProductValid))
                    {
                        this.NextCommand.DisabledReason = Message.ListProduct_Next_InvalidProducts;
                        return false;
                    }
                    return true;
                case ListProductWizardStep.SelectTargets:
                    if (this.SelectedTargetSelectionType == TargetSelectionType.AllLocations)
                    {
                        if (this.MasterLocationInfoList.Count > 0)
                        {
                            return true;
                        }
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.LocationSubset)
                    {
                        if (this.SelectedLocations.Count <= 0)
                        {
                            this.NextCommand.DisabledReason = Message.ListProduct_Selection_NextDisabledReason2;
                            return false;
                        }
                        return true;
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.SpecificRanges)
                    {
                        //if (this.SelectedRanges.Count <= 0)
                        //{
                        //    this.NextCommand.DisabledReason = Message.ListProduct_Selection_NextDisabledReason3;
                        //    return false;
                        //}
                        return true;
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.SpecificDistribution)
                    {
                        if (this.SelectedDistributionPercentage <= 0 || this.SelectedDistributionPercentage > 100)
                        {
                            this.NextCommand.DisabledReason = Message.ListProduct_Selection_NextDisabledReason4;
                            return false;
                        }
                        return true;
                    }

                    return false;
            }
            return false;
        }

        private void Next_Executed()
        {
            base.ShowWaitCursor(true);
            _currentStepNumber++;
            OnPropertyChanged(CurrentStepNumberProperty);
            switch (this.CurrentStep)
            {
                case ListProductWizardStep.SelectProducts:
                    this.CurrentStep = ListProductWizardStep.SelectTargets;
                    break;
                case ListProductWizardStep.SelectTargets:
                    //Clear collection just incase
                    _cdtProducts.Clear();

                    //IEnumerable<ScenarioConsumerDecisionTreeNode> leafNodes = this.CurrentScenario.ConsumerDecisionTree.FetchAllLeafNodes();

                    //Create product cdt selection rows
                    foreach (MinorRevisionProductRow productRow in this.AssignedProducts)
                    {
                        //Auto select cdt node
                        ConsumerDecisionTreeNode node = CalculateNewProductsCDTNode(productRow.ProductInfo.Id, _currentConsumerDecisionTree);
                        Int32? consumerDecisionTreeNodeId = null;
                        if (node != null)
                        {
                            consumerDecisionTreeNodeId = node.Id;
                        }

                        //Add to the rows
                        this._cdtProducts.Add(new ListProductRow(productRow, consumerDecisionTreeNodeId));
                    }

                    this.CurrentStep = ListProductWizardStep.SelectCDTNode;
                    break;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Previous

        private RelayCommand _previousCommand;

        /// <summary>
        /// Moves the process back a step
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Previous_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case ListProductWizardStep.SelectTargets:
                case ListProductWizardStep.SelectCDTNode:
                    return true;
            }
            return false;
        }

        private void Previous_Executed()
        {
            base.ShowWaitCursor(true);
            _currentStepNumber--;
            OnPropertyChanged(CurrentStepNumberProperty);
            switch (this.CurrentStep)
            {
                case ListProductWizardStep.SelectTargets:
                    this.CurrentStep = ListProductWizardStep.SelectProducts;
                    break;
                case ListProductWizardStep.SelectCDTNode:
                    this.CurrentStep = ListProductWizardStep.SelectTargets;
                    break;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Finish

        private RelayCommand _finishCommand;

        /// <summary>
        /// Ends the process back a step
        /// </summary>
        public RelayCommand FinishCommand
        {
            get
            {
                if (_finishCommand == null)
                {
                    _finishCommand = new RelayCommand(
                        p => Finish_Executed(),
                        p => Finish_CanExecute())
                    {
                        FriendlyName = Message.ListProduct_Finish
                    };
                    base.ViewModelCommands.Add(_finishCommand);
                }
                return _finishCommand;
            }
        }

        private Boolean Finish_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case ListProductWizardStep.SelectTargets:
                    if (this.SelectedTargetSelectionType == TargetSelectionType.AllLocations)
                    {
                        if (this.MasterLocationInfoList.Count > 0)
                        {
                            return true;
                        }
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.LocationSubset)
                    {
                        if (this.SelectedLocations.Count <= 0)
                        {
                            this.FinishCommand.DisabledReason = Message.ListProduct_Selection_NextDisabledReason2;
                            return false;
                        }
                        return true;
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.SpecificRanges)
                    {
                        //if (this.SelectedRanges.Count <= 0)
                        //{
                        //    this.FinishCommand.DisabledReason = Message.ListProduct_Selection_NextDisabledReason3;
                        //    return false;
                        //}
                        return true;
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.SpecificDistribution)
                    {
                        if (this.SelectedDistributionPercentage <= 0 || this.SelectedDistributionPercentage > 100)
                        {
                            this.FinishCommand.DisabledReason = Message.ListProduct_Selection_NextDisabledReason4;
                            return false;
                        }
                        return true;
                    }
                    break;
                case ListProductWizardStep.SelectCDTNode:
                    //Should be nullabe
                    //if (this.CDTProducts.Any(p => p.CDTNode == null))
                    //{
                    //    this.FinishCommand.DisabledReason = Message.ListProduct_Selection_FinishDisabledReason;
                    //    return false;
                    //}
                    return true;
            }
            return false;
        }

        private void Finish_Executed()
        {
            base.ShowWaitCursor(true);

            if (SearchType == MinorRevisionProductSearchType.ConsumerDecisionTree &&
                !AssortmentMinorRevision.ConsumerDecisionTreeId.HasValue &&
                SelectedConsumerDecisionTree != null)
            {
                AssortmentMinorRevision.ConsumerDecisionTreeId = SelectedConsumerDecisionTree.Id;
            }

            //If assigned products are populated
            if (this.AssignedProducts.Count > 0)
            {
                //Fetch full fat products
                ProductList products = ProductList.FetchByProductIds(this.AssignedProducts.Select(p => p.ProductInfo.Id));
                Dictionary<Int32, Product> productLookup = products.ToDictionary(p => p.Id);

                //Enumerate through selected products
                foreach (MinorRevisionProductRow productRow in this.AssignedProducts)
                {
                    //Get product
                    Product product = productLookup[productRow.ProductInfo.Id];

                    #region Create Action

                    Int32? cdtNodeId = null;
                    if (this.IsCDTSetup)
                    {
                        //Look cdt node
                        ListProductRow row = this.CDTProducts.FirstOrDefault(p => p.ProductRow.Equals(productRow));
                        if (row != null && row.ConsumerDecisionNodeId != null)
                        {
                            cdtNodeId = row.ConsumerDecisionNodeId;
                        }
                    }

                    //Calculate new priority number
                    Int32 newPriority = this.AssortmentMinorRevision.TotalActionsCount + 1;

                    //Create action
                    AssortmentMinorRevisionListAction action = AssortmentMinorRevisionListAction.NewAssortmentMinorRevisionListAction(
                        productRow.ProductInfo.Id, productRow.ProductInfo.Gtin, productRow.ProductInfo.Name, cdtNodeId, newPriority);

                    //Add locations
                    if (this.SelectedTargetSelectionType == TargetSelectionType.AllLocations)
                    {
                        //Enumerate throgh the master location info list
                        foreach (LocationInfo locationInfo in this.MasterLocationInfoList)
                        {
                            Int32 units = 0;// this.CurrentScenario.CalculateMinorRevisionProductUnits(product, locationInfo.Id);
                            Int32 facings = 0;//this.CurrentScenario.CalculateMinorRevisionProductFacings(product);

                            //Add location to the action
                            action.Locations.Add(AssortmentMinorRevisionListActionLocation.NewAssortmentMinorRevisionListActionLocation(locationInfo.Id, locationInfo.Code, locationInfo.Name, units, facings));
                        }
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.LocationSubset)
                    {
                        //Enumerate through the selected locations
                        foreach (LocationInfo locationInfo in this.SelectedLocations)
                        {
                            Int32 units = 0;// this.CurrentScenario.CalculateMinorRevisionProductUnits(product, locationInfo.Id);
                            Int32 facings = 0;// this.CurrentScenario.CalculateMinorRevisionProductFacings(product);

                            //Add location to the action
                            action.Locations.Add(AssortmentMinorRevisionListActionLocation.NewAssortmentMinorRevisionListActionLocation(locationInfo.Id, locationInfo.Code, locationInfo.Name, units, facings));
                        }
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.SpecificRanges)
                    {
                        ////Store scenario locations
                        //List<ScenarioLocation> locations = new List<ScenarioLocation>();

                        ////Get locations from ranges
                        //foreach (ScenarioModelNode range in this.SelectedRanges)
                        //{
                        //    locations.AddRange(range.FetchAllChildScenarioLocations(false));
                        //}

                        ////Enumerate through the selected locations
                        //foreach (ScenarioLocation location in locations.Distinct())
                        //{
                        //    //Only add locations with master data reference
                        //    if (location.LocationId.HasValue)
                        //    {
                        //        Int32 units = this.CurrentScenario.CalculateMinorRevisionProductUnits(product, location.LocationId.Value);
                        //        Int32 facings = this.CurrentScenario.CalculateMinorRevisionProductFacings(product);

                        //        //Add location to the action
                        //        action.Locations.Add(ScenarioMinorRevisionListActionLocation.NewScenarioMinorRevisionListActionLocation(location.LocationId.Value, location.Code, location.Name, units, facings));
                        //    }
                        //}
                    }
                    else if (this.SelectedTargetSelectionType == TargetSelectionType.SpecificDistribution)
                    {
                        //Calculate optimal distribution
                        //List<KeyValuePair<Int32?, ScenarioLocation>> locations = this.CurrentScenario.CalculateMinorReviewDistribution(productRow.ProductInfo, cdtNodeId, new List<Int16>(), this.SelectedDistributionPercentage).ToList();

                        ////Enumerate through the selected locations
                        //foreach (ScenarioLocation location in locations.Select(p => p.Value).Distinct())
                        //{
                        //    //Only add locations with master data reference
                        //    if (location.LocationId.HasValue)
                        //    {
                        //        Int32 units = this.CurrentScenario.CalculateMinorRevisionProductUnits(product, location.LocationId.Value);
                        //        Int32 facings = this.CurrentScenario.CalculateMinorRevisionProductFacings(product);

                        //        //Add location to the action
                        //        action.Locations.Add(ScenarioMinorRevisionListActionLocation.NewScenarioMinorRevisionListActionLocation(location.LocationId.Value, location.Code, location.Name, units, facings));
                        //    }
                        //}
                    }

                    //Add Action
                    this.AssortmentMinorRevision.ListActions.Add(action);

                    #endregion
                }
            }

            if (this.AttachedControl != null)
            {
                //Close window
                this.AttachedControl.Close();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_AddSelectedProducts
                    };
                    base.ViewModelCommands.Add(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            //must have some unassigned products selected
            return (this.SelectedUnassignedProducts.Count > 0);
        }

        private void AddSelectedProducts_Executed()
        {
            //Get list to add
            List<ProductInfo> addList = this.SelectedUnassignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (ProductInfo productInfo in addList)
            {
                if (!currentProductIds.Contains(productInfo.Id))
                {
                    addToSelectedProducts.Add(productInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo productInfo in addToSelectedProducts)
            {
                //Check if action product is valid
                Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //add the product to the list of assigned products
                _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
            }

            //clear the selected available products
            this.SelectedUnassignedProducts.Clear();

            //remove from the available
            this._unassignedProducts.RemoveRange(addList);
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_RemoveSelectedProducts
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        private Boolean RemoveSelectedProducts_CanExecute()
        {
            if (this.SelectedAssignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            //Get list to remove
            List<MinorRevisionProductRow> addList = this.SelectedAssignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.UnassignedProducts.Select(p => p.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (MinorRevisionProductRow productRow in addList)
            {
                if (!currentProductIds.Contains(productRow.ProductInfo.Id))
                {
                    addToSelectedProducts.Add(productRow.ProductInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo delistProduct in addToSelectedProducts)
            {
                //add the product to the list of unassigned products
                _unassignedProducts.Add(delistProduct);
            }

            //clear the selected assigned products
            this.SelectedAssignedProducts.Clear();

            //remove from the assigned
            this._assignedProducts.RemoveRange(addList);
        }

        #endregion

        #region AddAllProductsCommand

        private RelayCommand _addAllProductsCommand;

        public RelayCommand AddAllProductsCommand
        {
            get
            {
                if (_addAllProductsCommand == null)
                {
                    _addAllProductsCommand = new RelayCommand(
                        p => AddAllProducts_Executed(),
                        p => AddAllProducts_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_AddAllProducts
                    };
                    base.ViewModelCommands.Add(_addAllProductsCommand);
                }
                return _addAllProductsCommand;
            }
        }

        private Boolean AddAllProducts_CanExecute()
        {
            if (this.UnassignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddAllProducts_Executed()
        {
            //Get list to add
            List<ProductInfo> addList = this.UnassignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (ProductInfo productInfo in addList)
            {
                if (!currentProductIds.Contains(productInfo.Id))
                {
                    addToSelectedProducts.Add(productInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo productInfo in addToSelectedProducts)
            {
                //Check if action product is valid
                Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //add the product to the list of assigned products
                _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
            }

            //clear the selected available products
            this.SelectedUnassignedProducts.Clear();

            //remove from the available
            this._unassignedProducts.RemoveRange(addList);
        }

        #endregion

        #region RemoveAllProductsCommand

        private RelayCommand _removeAllProductsCommand;

        public RelayCommand RemoveAllProductsCommand
        {
            get
            {
                if (_removeAllProductsCommand == null)
                {
                    _removeAllProductsCommand = new RelayCommand(
                        p => RemoveAllProducts_Executed(),
                        p => RemoveAllProducts_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_RemoveAllProducts
                    };
                    base.ViewModelCommands.Add(_removeAllProductsCommand);
                }
                return _removeAllProductsCommand;
            }
        }

        private Boolean RemoveAllProducts_CanExecute()
        {
            if (this.AssignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveAllProducts_Executed()
        {
            //Get list to add
            List<MinorRevisionProductRow> addList = this.AssignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.UnassignedProducts.Select(p => p.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (MinorRevisionProductRow productRow in addList)
            {
                if (!currentProductIds.Contains(productRow.ProductInfo.Id))
                {
                    addToSelectedProducts.Add(productRow.ProductInfo);
                }
            }

            //for each product in the list of selected assigned products
            foreach (ProductInfo delistProduct in addToSelectedProducts)
            {
                //add the product to the list of unassigned products
                _unassignedProducts.Add(delistProduct);
            }

            //clear the selected assigned products
            this.SelectedAssignedProducts.Clear();

            //remove from the assigned
            this._assignedProducts.RemoveRange(addList);
        }

        #endregion

        #region Selection Commands

        #region SelectFinancialGroup

        private RelayCommand _selectFinancialGroupCommand;

        /// <summary>
        /// Opens the financial group selection window.
        /// </summary>
        public RelayCommand SelectFinancialGroupCommand
        {
            get
            {
                if (_selectFinancialGroupCommand == null)
                {
                    _selectFinancialGroupCommand = new RelayCommand(
                        p => SelectFinancialGroup_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    //base.ViewModelCommands.Add(_selectFinancialGroupCommand);
                }
                return _selectFinancialGroupCommand;
            }
        }


        private void SelectFinancialGroup_Executed()
        {
            //if (this.AttachedControl != null)
            //{
            //launch the selection window in single mode.
            MerchGroupSelectionWindow win = new MerchGroupSelectionWindow();

            win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
            App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

            if (win.DialogResult == true)
            {
                this.SelectedFinancialGroup = win.SelectionResult.First();
            }
            //}
        }

        #endregion

        #region SelectProductUniverse

        private RelayCommand _selectProductUniverseCommand;

        public RelayCommand SelectProductUniverseCommand
        {
            get
            {
                if (_selectProductUniverseCommand == null)
                {
                    _selectProductUniverseCommand = new RelayCommand(
                        p => SelectProductUniverse_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                }
                return _selectProductUniverseCommand;
            }
        }

        private void SelectProductUniverse_Executed()
        {
            ProductUniverseSelectionWindow productUniverseWindow = new ProductUniverseSelectionWindow();
            //productUniverseWindow.SelectionMode = DataGridSelectionMode.Single;

            App.ShowWindow(productUniverseWindow, this.AttachedControl, true);

            if (productUniverseWindow.SelectedProductUniverse != null)
            {
                this.SelectedProductUniverse = productUniverseWindow.SelectedProductUniverse;
            }

        }

        #endregion

        #region SelectConsumerDecisionTree

        private RelayCommand _selectConsumerDecisionTreeCommand;

        public RelayCommand SelectConsumerDecisionTreeCommand
        {
            get
            {
                if (_selectConsumerDecisionTreeCommand == null)
                {
                    _selectConsumerDecisionTreeCommand = new RelayCommand(
                        p => SelectConsumerDecisionTree_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                }
                return _selectConsumerDecisionTreeCommand;
            }
        }

        private void SelectConsumerDecisionTree_Executed()
        {
            ConsumerDecisionTreeWindow consumerDecisionTreeWindow = new ConsumerDecisionTreeWindow();
            App.ShowWindow(consumerDecisionTreeWindow, this.AttachedControl, true);

            if (consumerDecisionTreeWindow.SelectionResult != null)
            {
                this.SelectedConsumerDecisionTree = consumerDecisionTreeWindow.SelectionResult;
            }
        }

        #endregion
        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change to the product info list model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
        {

            if (e.OldModel != null)
            {
                ProductInfoList oldList = (ProductInfoList)e.NewModel;
                oldList.AddedNew -= ProductInfoListView_AddedNew;
            }
            if (e.NewModel != null)
            {
                ProductInfoList newList = (ProductInfoList)e.NewModel;
                newList.AddedNew += ProductInfoListView_AddedNew;
                this._unassignedProducts.AddRange(newList);
            }

            //mark as finished searching
            this.IsSearching = false;

            //If the productInfoListView doesn't contain any results the "No Records" message should be displayed
            this.ContainsRecords = (_productInfoListView.Model.Count > 0);

            //check if another search is queued
            if (_isSearchQueued)
            {
                PerformSearch();
            }
        }

        /// <summary>
        /// Responds ProductInfoListView adding items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_AddedNew(object sender, Csla.Core.AddedNewEventArgs<ProductInfo> e)
        {
            this._unassignedProducts.Add(e.NewObject);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Method to handle the target selection type changing
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedTargetSelectionTypeChanged(TargetSelectionType newValue)
        {
            if (newValue == TargetSelectionType.LocationSubset)
            {
                //Show location selection window
                LocationInfoListViewModel locationInfoListViewModel = new LocationInfoListViewModel();
                locationInfoListViewModel.FetchAllForEntity();
                LocationHierarchyViewModel locationHierarchyViewModel = new LocationHierarchyViewModel();
                locationHierarchyViewModel.FetchForCurrentEntity();
                Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors.LocationSelectionWindow win = new Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors.LocationSelectionWindow(this.MasterLocationInfoList.ToObservableCollection(), this.SelectedLocations, locationHierarchyViewModel.Model);
                App.ShowWindow(win, this.AttachedControl, true);

                //Get the taken locations
                if (win.DialogResult.HasValue)
                {
                    if (win.DialogResult.Value)
                    {
                        this.SelectedLocations = win.ViewModel.TakenLocations.Select(p => p.Location).ToObservableCollection();
                    }
                }
            }
            else if (newValue == TargetSelectionType.SpecificRanges)
            {
                ////Show location selection window
                //Common.ScenarioModelNodeSelectionWindow win = new Common.ScenarioModelNodeSelectionWindow(this.SelectedRanges, true);
                //App.ShowWindow(win, true);

                ////Get the taken locations
                //if (win.DialogResult.HasValue)
                //{
                //    if (win.DialogResult.Value)
                //    {
                //        this.SelectedRanges = win.SelectionResult.ToObservableCollection();
                //    }
                //}
            }
        }

        /// <summary>
        /// Method to calculate new products position in the cdt
        /// </summary>
        /// <param name="productInfo"></param>
        /// <returns></returns>
        public static ConsumerDecisionTreeNode CalculateNewProductsCDTNode(Int32 productId, Model.ConsumerDecisionTree tree)
        {
            ConsumerDecisionTreeNode currentNode = null;

            //Check to see if the product exists in a node.
            if (tree != null)
            {
                currentNode = tree.GetAllNodes().FirstOrDefault(n => n.Products.Select(p => p.ProductId).Contains(productId));

                //Return the current node if it contains the product.
                if (currentNode != null) return currentNode;
            }


            //Fetch all levels
            IEnumerable<Model.ConsumerDecisionTreeLevel> levels = tree.FetchAllLevels();

            //Fetch full product details
            Product product = Product.FetchById(productId);

            //Set current node to be the root
            currentNode = tree.RootNode;

            //Enumerate through levels in order
            foreach (Model.ConsumerDecisionTreeLevel level in levels.OrderBy(p => p.Id))
            {
                //Products value
                Object value = null;



                if (currentNode != null)
                {
                    //Create property info
                    PropertyInfo propertyInfo = null;

                    //Try and get property from product object
                    propertyInfo = typeof(Product).GetProperty(level.Name);

                    if (propertyInfo != null)
                    {
                        //Get products value for this property
                        Object propertyValue = propertyInfo.GetValue(product, null);
                        if (propertyValue != null)
                        {
                            value = propertyValue;
                        }
                    }

                    //Incase property is attribute data
                    //if (propertyInfo == null)
                    //{
                    //    //Try and get property from product attribute
                    //    propertyInfo = typeof(ProductAttributeData).GetProperty(level.Name);

                    //    if (propertyInfo != null)
                    //    {
                    //        //Get products attribute data value for this property
                    //        Object propertyValue = propertyInfo.GetValue(product.AttributeData, null);

                    //        if (propertyValue != null)
                    //        {
                    //            value = propertyValue;
                    //        }
                    //    }
                    //}


                    //If value is null/empty, convert to null node name
                    if (value != null)
                    {
                        //Fetch all nodes at this level
                        IEnumerable<ConsumerDecisionTreeNode> nodes = currentNode.ChildList;

                        //Try and locate node which matching property and set this to be the 
                        //current node ready for the next enumerate through the levels
                        currentNode = nodes.FirstOrDefault(p => p.Name.Equals(value.ToString()));
                    }
                }

                //If this is lowest level
                if (currentNode != null && currentNode.ChildList.Count <= 0)
                {
                    //Return the current node
                    return currentNode;
                }
            }
            return null;
        }

        #region Selection Methods

        /// <summary>
        /// Performs the search operation
        /// </summary>
        private void PerformSearch()
        {
            this._unassignedProducts.Clear();

            Int32 entityId = App.ViewState.EntityId;

            if (!this.IsSearching)
            {
                _isSearchQueued = false;

                this.IsSearching = true;

                //search asyncronously
                switch (this.SearchType)
                {
                    case MinorRevisionProductSearchType.Criteria:
                        _productInfoListView.BeginFetchByEntityIdSearchCriteria(entityId, this.SearchCriteria != null ? this.SearchCriteria : "");
                        break;

                    case MinorRevisionProductSearchType.ProductUniverse:
                        _productInfoListView.BeginFetchByProductUniverseId((this.SelectedProductUniverse != null) ? this.SelectedProductUniverse.Id : 0);
                        break;
                    case MinorRevisionProductSearchType.ConsumerDecisionTree:
                        _productInfoListView.BeginFetchByConsumerDecisionTree((this.SelectedConsumerDecisionTree != null) ? this.SelectedConsumerDecisionTree.Id : 0);
                        break;
                    default: throw new NotImplementedException();
                }
            }
            else
            {
                _isSearchQueued = true;
            }

        }

        #endregion
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedAssignedProducts.Clear();
                    _selectedUnassignedProducts.Clear();
                    _unassignedProducts.Clear();
                    _assignedProducts.Clear();
                    _locationInfoListView.Dispose();
                    _productInfoListView.ModelChanged -= ProductInfoListView_ModelChanged;
                    if (_productInfoListView.Model != null)
                    {
                        _productInfoListView.Model.AddedNew -= ProductInfoListView_AddedNew;
                    }
                    _productInfoListView.Dispose();
                    _productInfoListView = null;
                    _financialHierarchy = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// List Product Row for Selecting Product CDT
    /// </summary>
    public class ListProductRow : INotifyPropertyChanged
    {
        #region Fields

        private MinorRevisionProductRow _productRow;
        private Int32? _consumerDecisionNodeId;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ConsumerDecisionNodeIdProperty = WpfHelper.GetPropertyPath<ListProductRow>(p => p.ConsumerDecisionNodeId);
        public static readonly PropertyPath ProductRowProperty = WpfHelper.GetPropertyPath<ListProductRow>(p => p.ProductRow);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the product info
        /// </summary>
        public MinorRevisionProductRow ProductRow
        {
            get { return _productRow; }
        }

        /// <summary>
        /// Returns the product gtin
        /// </summary>
        public String Gtin
        {
            get { return _productRow.ProductInfo.Gtin; }
        }

        /// <summary>
        /// Returns the product name
        /// </summary>
        public String Name
        {
            get { return _productRow.ProductInfo.Name; }
        }

        /// <summary>
        /// Returns whether the product is in the product universe
        /// </summary>
        public Int32? ConsumerDecisionNodeId
        {
            get { return _consumerDecisionNodeId; }
            set
            {
                _consumerDecisionNodeId = value;
                OnPropertyChanged(ConsumerDecisionNodeIdProperty);
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public ListProductRow(MinorRevisionProductRow productRow, Int32? cdtNodeId)
        {
            //Set the properties
            _productRow = productRow;
            _consumerDecisionNodeId = cdtNodeId;
        }

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(PropertyPath property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property.Path));
            }
        }

        #endregion
    }
}