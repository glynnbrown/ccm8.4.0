﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
// V8-26301 : J.Pickup
//  Removed fincial hierarhcy as a search option (doesn't exist in CCM). 
// V8-26705 : A.Kuszyk
//  Changed ProductSelectionWindow to Common.Wpf version.
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Diagnostics;
using System.ComponentModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Denotes the steps available in the replace product wizard
    /// </summary>
    public enum ReplaceProductWizardStep
    {
        SelectProducts,
        SelectReplacementProducts
    }

    public class ReplaceProductViewModel : ViewModelAttachedControlObject<ReplaceProductWindow>
    {
        #region Fields

        private Int32 _entityId;
        private AssortmentMinorRevision _assortmentMinorRevision;
        private Boolean _isWizardComplete = false;

        //Wizard related
        private ReplaceProductWizardStep _currentStep = ReplaceProductWizardStep.SelectProducts;
        private Int16 _currentStepNumber = 1;
        private BulkObservableCollection<Int32> _currentActionsProductIds = new BulkObservableCollection<Int32>();
        private LocationInfoListViewModel _locationInfoListViewModel;

        //Selected data
        private ObservableCollection<ProductReplacementLinkRowViewModel> _availableLinkRows = new ObservableCollection<ProductReplacementLinkRowViewModel>();
        private ProductReplacementLinkRowViewModel _selectedLinkRow;

        //Products
        private ObservableCollection<ProductInfo> _selectedUnassignedProducts = new ObservableCollection<ProductInfo>();
        private BulkObservableCollection<ProductInfo> _unassignedProducts = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _unassignedProductsRO;

        private ObservableCollection<MinorRevisionProductRow> _selectedAssignedProducts = new ObservableCollection<MinorRevisionProductRow>();
        private BulkObservableCollection<MinorRevisionProductRow> _assignedProducts = new BulkObservableCollection<MinorRevisionProductRow>();
        private ReadOnlyBulkObservableCollection<MinorRevisionProductRow> _assignedProductsRO;

        #region Selection Fields

        private MinorRevisionProductSearchType _searchType = MinorRevisionProductSearchType.Criteria;
        private DataGridSelectionMode _selectionMode = DataGridSelectionMode.Extended;
        private ProductInfoListViewModel _productInfoListView = new ProductInfoListViewModel();
        private ObservableCollection<ProductInfo> _selectedProducts = new ObservableCollection<ProductInfo>();
        private Boolean _isSearching;
        private Boolean _isSearchQueued;

        private String _searchCriteria;

        private ProductUniverseInfo _selectedProductUniverse;
        private ConsumerDecisionTreeInfo _selectedConsumerDecisionTreeInfo;

        private Boolean _containsRecords;
        #endregion
        #endregion

        #region Property Paths

        public static readonly PropertyPath IsWizardCompleteProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.IsWizardComplete);
        public static readonly PropertyPath IsPreviousCommandVisibleProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.IsPreviousCommandVisible);
        public static readonly PropertyPath IsNextCommandVisibleProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.IsNextCommandVisible);
        public static readonly PropertyPath IsFinishCommandVisibleProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.IsFinishCommandVisible);
        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath CurrentStepNumberProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.CurrentStepNumber);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.NextCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.PreviousCommand);
        public static readonly PropertyPath FinishCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.FinishCommand);
        public static readonly PropertyPath NumberOfStagesProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.NumberOfStages);
        public static readonly PropertyPath AvailableLinkRowsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.AvailableLinkRows);
        public static readonly PropertyPath SelectedLinkRowProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectedLinkRow);
        public static readonly PropertyPath CurrentActionsProductIdsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.CurrentActionsProductIds);

        public static readonly PropertyPath SelectedUnassignedProductsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectedUnassignedProducts);
        public static readonly PropertyPath SelectedAssignedProductsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectedAssignedProducts);
        public static readonly PropertyPath UnassignedProductsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.UnassignedProducts);
        public static readonly PropertyPath AssignedProductsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.AssignedProducts);

        public static readonly PropertyPath AddSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.AddSelectedProductsCommand);
        public static readonly PropertyPath RemoveSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.RemoveSelectedProductsCommand);
        public static readonly PropertyPath AddAllProductsCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.AddAllProductsCommand);
        public static readonly PropertyPath RemoveAllProductsCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.RemoveAllProductsCommand);

        #region Selection Property Paths

        //properties
        public static readonly PropertyPath SearchTypeProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SearchType);
        public static readonly PropertyPath SelectionModeProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectionMode);
        public static readonly PropertyPath IsSearchingProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.IsSearching);
        public static readonly PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SearchResults);
        public static readonly PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectedProducts);
        public static readonly PropertyPath SearchCriteriaProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SearchCriteria);
        public static readonly PropertyPath SelectedProductUniverseProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectedProductUniverse);
        public static readonly PropertyPath ContainsRecordsProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.ContainsRecords);
        public static readonly PropertyPath SelectedConsumerDecisionTreeProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectedConsumerDecisionTree);

        //commands
        public static readonly PropertyPath SelectProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectProductUniverseCommand);
        public static readonly PropertyPath SelectConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<ReplaceProductViewModel>(p => p.SelectConsumerDecisionTreeCommand);

        #endregion
        #endregion

        #region Properties

        /// <summary>
        /// Returns the current scenario
        /// </summary>
        public AssortmentMinorRevision CurrentMinorRevision
        {
            get
            {
                return _assortmentMinorRevision;
            }
        }

        /// <summary>
        /// Returns true if the cancel command should be available on the current step
        /// </summary>
        public Boolean IsPreviousCommandVisible
        {
            get
            {
                if (this.CurrentStep == ReplaceProductWizardStep.SelectReplacementProducts)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns true if the next command should be available
        /// for the current step
        /// </summary>
        public Boolean IsNextCommandVisible
        {
            get
            {
                if (this.CurrentStep == ReplaceProductWizardStep.SelectProducts)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Returns true if the finish command should be available
        /// for the current step
        /// </summary>
        public Boolean IsFinishCommandVisible
        {
            get
            {
                if (this.CurrentStep == ReplaceProductWizardStep.SelectReplacementProducts)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// The current step that the data selection wizard is at
        /// </summary>
        public ReplaceProductWizardStep CurrentStep
        {
            get { return _currentStep; }
            set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                //Refresh button bindings
                OnPropertyChanged(IsNextCommandVisibleProperty);
                OnPropertyChanged(IsPreviousCommandVisibleProperty);
                OnPropertyChanged(IsFinishCommandVisibleProperty);
            }
        }

        /// <summary>
        /// The current step number that the data selection wizard is at
        /// </summary>
        public Int16 CurrentStepNumber
        {
            get { return _currentStepNumber; }
        }

        /// <summary>
        /// Returns the number of stages
        /// </summary>
        public Int32 NumberOfStages
        {
            get { return 2; }
        }

        /// <summary>
        /// Returns the collection of selected unassigned products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedUnassignedProducts
        {
            get { return _selectedUnassignedProducts; }
        }

        /// <summary>
        /// Returns the collection of unassigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> UnassignedProducts
        {
            get
            {
                if (_unassignedProductsRO == null)
                {
                    _unassignedProductsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_unassignedProducts);
                }
                return _unassignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected assigned products
        /// </summary>
        public ObservableCollection<MinorRevisionProductRow> SelectedAssignedProducts
        {
            get { return _selectedAssignedProducts; }
        }

        /// <summary>
        /// Returns the collection of Assigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<MinorRevisionProductRow> AssignedProducts
        {
            get
            {
                if (_assignedProductsRO == null)
                {
                    _assignedProductsRO = new ReadOnlyBulkObservableCollection<MinorRevisionProductRow>(_assignedProducts);
                }
                return _assignedProductsRO;
            }
        }

        /// <summary>
        /// Represents all available link rows for the current scenario
        /// </summary>
        public ObservableCollection<ProductReplacementLinkRowViewModel> AvailableLinkRows
        {
            get { return _availableLinkRows; }
        }

        /// <summary>
        /// Gets/sets the selected link row
        /// </summary>
        public ProductReplacementLinkRowViewModel SelectedLinkRow
        {
            get { return _selectedLinkRow; }
            set
            {
                _selectedLinkRow = value;
                OnPropertyChanged(SelectedLinkRowProperty);
            }
        }

        /// <summary>
        /// Returns the current list of actions product ids 
        /// </summary>
        public BulkObservableCollection<Int32> CurrentActionsProductIds
        {
            get { return _currentActionsProductIds; }
        }

        /// <summary>
        /// Returns the current list of actions product ids 
        /// </summary>
        public Boolean IsWizardComplete
        {
            get { return _isWizardComplete; }
            set
            {
                _isWizardComplete = value;
                OnPropertyChanged(IsWizardCompleteProperty);
            }
        }

        /// <summary>
        /// Returns the current location list
        /// </summary>
        public LocationInfoList MasterLocationInfoList
        {
            get
            {
                return _locationInfoListViewModel.Model;
            }
        }

        #region Selection Properties

        /// <summary>
        /// Gets/Sets the selection type
        /// </summary>
        public MinorRevisionProductSearchType SearchType
        {
            get { return _searchType; }
            set
            {
                _searchType = value;
                OnPropertyChanged(SearchTypeProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Returns the selection mode for this instance
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return _selectionMode; }
        }

        /// <summary>
        /// Returns true if a search is currently being performed
        /// </summary>
        public Boolean IsSearching
        {
            get { return _isSearching; }
            private set
            {
                _isSearching = value;
                OnPropertyChanged(IsSearchingProperty);
            }
        }

        /// <summary>
        /// Returns the readonly collection of search results
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> SearchResults
        {
            get
            {
                return _productInfoListView.BindingView;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedProducts
        {
            get { return _selectedProducts; }
        }

        /// <summary>
        /// Gets/Sets the search criteria value.
        /// </summary>
        public String SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                _searchCriteria = value;
                OnPropertyChanged(SearchCriteriaProperty);
                PerformSearch();
            }
        }



        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ProductUniverseInfo SelectedProductUniverse
        {
            get { return _selectedProductUniverse; }
            set
            {
                _selectedProductUniverse = value;
                OnPropertyChanged(SelectedProductUniverseProperty);
                PerformSearch();
            }
        }


        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ConsumerDecisionTreeInfo SelectedConsumerDecisionTree
        {
            get { return _selectedConsumerDecisionTreeInfo; }
            set
            {
                _selectedConsumerDecisionTreeInfo = value;
                OnPropertyChanged(SelectedConsumerDecisionTreeProperty);
                PerformSearch();
            }
        }


        /// <summary>
        /// Returns true if the search results grid contains records
        /// </summary>
        public Boolean ContainsRecords
        {
            get { return _containsRecords; }
            private set
            {
                _containsRecords = value;
                OnPropertyChanged(ContainsRecordsProperty);
            }
        }

        #endregion
        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public ReplaceProductViewModel(AssortmentMinorRevision assortmentMinorRevision, ConsumerDecisionTreeInfo consumerDecisionTreeInfo)
            : this(
            assortmentMinorRevision,
            App.ViewState.EntityId,
            consumerDecisionTreeInfo)
        { }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ReplaceProductViewModel(AssortmentMinorRevision assortmentMinorRevision,
            Int32 entityId, ConsumerDecisionTreeInfo consumerDecisionTreeInfo)
        {
            //Set scenario
            _assortmentMinorRevision = assortmentMinorRevision;
            _entityId = entityId;

            //Set location list
            _locationInfoListViewModel = new LocationInfoListViewModel();
            _locationInfoListViewModel.FetchAllForEntity();

            //Set CDT
            if (consumerDecisionTreeInfo != null)
            {
                this.SearchType = MinorRevisionProductSearchType.ConsumerDecisionTree;
                this.SelectedConsumerDecisionTree = consumerDecisionTreeInfo;
            }

            _productInfoListView.ModelChanged += ProductInfoListView_ModelChanged;
            if (_productInfoListView.Model != null)
            {
                _productInfoListView.Model.AddedNew += ProductInfoListView_AddedNew;
            }

            //if (this.CurrentMinorRevision != null)
            //{
            //    if (this.CurrentMinorRevision.ScenarioProductUniverse != null)
            //    {
            //        //Create dictionary
            //        _scenarioProductUniverseProductLookup = _scenarioViewModel.Model.ScenarioProductUniverse.Products.Where(p => p.ProductId.HasValue).ToDictionary(p => p.ProductId.Value);

            //        //Prepopulate all scenario product universe products into unassigned
            //        if (scenario.Model.ScenarioProductUniverse != null)
            //        {
            //            //Fetch product infos
            //            ProductInfoList productInfoList = ProductInfoList.FetchByEntityIdProductIds(entityId, scenario.Model.ScenarioProductUniverse.Products.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));

            //            //Add items
            //            this._unassignedProducts.AddRange(productInfoList);
            //        }
            //    }
            //}

            ////Construct taken products ids for validation purposes, only 1 action per product
            //IEnumerable<Int32> actionProductIds = this.MinorRevision.GetAllScenarioActionsUsedProductIds();

            //Construct taken products ids for validation purposes, only 1 action per product
            BulkObservableCollection<Int32> actionProductIds = new BulkObservableCollection<Int32>();
            //List actions
            if (this.CurrentMinorRevision.ListActions != null
                && this.CurrentMinorRevision.ListActions.Count > 0)
            {
                actionProductIds.AddRange(this.CurrentMinorRevision.ListActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //DeList actions
            if (this.CurrentMinorRevision.DeListActions != null
                && this.CurrentMinorRevision.DeListActions.Count > 0)
            {
                actionProductIds.AddRange(this.CurrentMinorRevision.DeListActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //Amend Dist actions
            if (this.CurrentMinorRevision.AmendDistributionActions != null
                && this.CurrentMinorRevision.AmendDistributionActions.Count > 0)
            {
                actionProductIds.AddRange(this.CurrentMinorRevision.AmendDistributionActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }

            //Assign to field
            _currentActionsProductIds.Clear();
            _currentActionsProductIds.AddRange(actionProductIds);

        }

        #endregion

        #region Commands

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// Advances the process to the next step
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Next_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case ReplaceProductWizardStep.SelectProducts:
                    if (this.AssignedProducts.Count <= 0)
                    {
                        this.NextCommand.DisabledReason = Message.ReplaceProduct_Next_NoAssignedProducts;
                        return false;
                    }
                    else if (!this.AssignedProducts.All(p => p.IsActionProductValid))
                    {
                        this.NextCommand.DisabledReason = Message.ReplaceProduct_Next_InvalidProducts;
                        return false;
                    }
                    return true;
            }
            return false;
        }

        private void Next_Executed()
        {
            base.ShowWaitCursor(true);
            _currentStepNumber++;
            OnPropertyChanged(CurrentStepNumberProperty);
            switch (this.CurrentStep)
            {
                case ReplaceProductWizardStep.SelectProducts:

                    //Create link rows for product selection
                    UpdateLinkRows();

                    this.CurrentStep = ReplaceProductWizardStep.SelectReplacementProducts;
                    break;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Previous

        private RelayCommand _previousCommand;

        /// <summary>
        /// Moves the process back a step
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Previous_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case ReplaceProductWizardStep.SelectReplacementProducts:
                    return true;
            }
            return false;
        }

        private void Previous_Executed()
        {
            base.ShowWaitCursor(true);
            _currentStepNumber--;
            OnPropertyChanged(CurrentStepNumberProperty);
            switch (this.CurrentStep)
            {
                case ReplaceProductWizardStep.SelectReplacementProducts:
                    this.CurrentStep = ReplaceProductWizardStep.SelectProducts;
                    break;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Finish

        private RelayCommand _finishCommand;

        /// <summary>
        /// Ends the process back a step
        /// </summary>
        public RelayCommand FinishCommand
        {
            get
            {
                if (_finishCommand == null)
                {
                    _finishCommand = new RelayCommand(
                        p => Finish_Executed(),
                        p => Finish_CanExecute())
                    {
                        FriendlyName = Message.ReplaceProduct_Finish,
                        DisabledReason = Message.ReplaceProduct_Finish_DisabledReason
                    };
                    base.ViewModelCommands.Add(_finishCommand);
                }
                return _finishCommand;
            }
        }

        private Boolean Finish_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case ReplaceProductWizardStep.SelectReplacementProducts:
                    //Check all rows are valid
                    return this.AvailableLinkRows.All(p => p.IsValid);
            }
            return false;
        }

        private void Finish_Executed()
        {
            base.ShowWaitCursor(true);

            //Set wizard flag
            this.IsWizardComplete = true;

            //If assigned products are populated
            if (this.AvailableLinkRows.Count > 0)
            {
                //Fetch full fat products
                List<Int32> productIds = new List<Int32>();
                productIds.AddRange(this.AvailableLinkRows.Select(p => p.ReplacementProductInfo.Id));
                productIds.AddRange(this.AvailableLinkRows.Select(p => p.ProductInfo.Id));

                ProductList products = ProductList.FetchByProductIds(productIds.Distinct());
                Dictionary<Int32, Product> productLookup = products.ToDictionary(p => p.Id);

                //Enumerate through selected products
                foreach (ProductReplacementLinkRowViewModel productLinkRow in this.AvailableLinkRows)
                {
                    //Get product
                    if (productLinkRow.ReplacementProductInfo != null)
                    {
                        //Ensure we have product data loaded
                        if (productLookup.ContainsKey(productLinkRow.ReplacementProductInfo.Id) &&
                             productLookup.ContainsKey(productLinkRow.ProductInfo.Id))
                        {

                            //Lookup original product
                            Product originalProduct = productLookup[productLinkRow.ProductInfo.Id];

                            #region Create Action

                            //Calculate new priority number
                            Int32 newPriority = this.CurrentMinorRevision.TotalActionsCount + 1;

                            //Create action
                            AssortmentMinorRevisionReplaceAction action
                                = AssortmentMinorRevisionReplaceAction.NewAssortmentMinorRevisionReplaceAction(productLinkRow.ProductInfo.Id, productLinkRow.ProductInfo.Gtin, productLinkRow.ProductInfo.Name, productLinkRow.ReplacementProductInfo.Id, productLinkRow.ReplacementProductInfo.Gtin, productLinkRow.ReplacementProductInfo.Name, newPriority);

                            //Add all locations to action
                            foreach (LocationInfo locationInfo in MasterLocationInfoList)
                            {
                                action.Locations.Add(AssortmentMinorRevisionReplaceActionLocation.NewAssortmentMinorRevisionReplaceActionLocation(locationInfo.Id, locationInfo.Code, locationInfo.Name, 0, 0));
                            }

                            //Add Action
                            this.CurrentMinorRevision.ReplaceActions.Add(action);

                            #endregion
                        }
                    }
                }
            }

            if (SearchType == MinorRevisionProductSearchType.ConsumerDecisionTree &&
                !this.CurrentMinorRevision.ConsumerDecisionTreeId.HasValue &&
                SelectedConsumerDecisionTree != null)
            {
                this.CurrentMinorRevision.ConsumerDecisionTreeId = SelectedConsumerDecisionTree.Id;
            }

            if (this.AttachedControl != null)
            {
                //Close window
                this.AttachedControl.Close();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        //FriendlyName = Message.ReplaceProduct_AddSelectedProducts,
                        Icon = ImageResources.MinorRevision_AddSelectedProducts
                    };
                    base.ViewModelCommands.Add(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            //must have some unassigned products selected
            return (this.SelectedUnassignedProducts.Count > 0);
        }

        private void AddSelectedProducts_Executed()
        {
            //Get list to add
            List<ProductInfo> addList = this.SelectedUnassignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (ProductInfo productInfo in addList)
            {
                if (!currentProductIds.Contains(productInfo.Id))
                {
                    addToSelectedProducts.Add(productInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo productInfo in addToSelectedProducts)
            {
                //Check if action product is valid
                Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //add the product to the list of assigned products
                _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
            }

            //clear the selected available products
            this.SelectedUnassignedProducts.Clear();

            //remove from the available
            this._unassignedProducts.RemoveRange(addList);
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.ReplaceProduct_RemoveSelectedProducts,
                        Icon = ImageResources.MinorRevision_RemoveSelectedProducts
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        private Boolean RemoveSelectedProducts_CanExecute()
        {
            if (this.SelectedAssignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            //Get list to remove
            List<MinorRevisionProductRow> addList = this.SelectedAssignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.UnassignedProducts.Select(p => p.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (MinorRevisionProductRow productRow in addList)
            {
                if (!currentProductIds.Contains(productRow.ProductInfo.Id))
                {
                    addToSelectedProducts.Add(productRow.ProductInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo delistProduct in addToSelectedProducts)
            {
                //add the product to the list of unassigned products
                _unassignedProducts.Add(delistProduct);
            }

            //clear the selected assigned products
            this.SelectedAssignedProducts.Clear();

            //remove from the assigned
            this._assignedProducts.RemoveRange(addList);
        }

        #endregion

        #region AddAllProductsCommand

        private RelayCommand _addAllProductsCommand;

        public RelayCommand AddAllProductsCommand
        {
            get
            {
                if (_addAllProductsCommand == null)
                {
                    _addAllProductsCommand = new RelayCommand(
                        p => AddAllProducts_Executed(),
                        p => AddAllProducts_CanExecute())
                    {
                        //FriendlyName = Message.ReplaceProduct_AddAllProducts,
                        Icon = ImageResources.MinorRevision_AddAllProducts
                    };
                    base.ViewModelCommands.Add(_addAllProductsCommand);
                }
                return _addAllProductsCommand;
            }
        }

        private Boolean AddAllProducts_CanExecute()
        {
            if (this.UnassignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddAllProducts_Executed()
        {
            //Get list to add
            List<ProductInfo> addList = this.UnassignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (ProductInfo productInfo in addList)
            {
                if (!currentProductIds.Contains(productInfo.Id))
                {
                    addToSelectedProducts.Add(productInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo productInfo in addToSelectedProducts)
            {
                //Check if action product is valid
                Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //add the product to the list of assigned products
                _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
            }

            //clear the selected available products
            this.SelectedUnassignedProducts.Clear();

            //remove from the available
            this._unassignedProducts.RemoveRange(addList);
        }

        #endregion

        #region RemoveAllProductsCommand

        private RelayCommand _removeAllProductsCommand;

        public RelayCommand RemoveAllProductsCommand
        {
            get
            {
                if (_removeAllProductsCommand == null)
                {
                    _removeAllProductsCommand = new RelayCommand(
                        p => RemoveAllProducts_Executed(),
                        p => RemoveAllProducts_CanExecute())
                    {
                        FriendlyName = Message.ReplaceProduct_RemoveAllProducts,
                        Icon = ImageResources.MinorRevision_RemoveAllProducts
                    };
                    base.ViewModelCommands.Add(_removeAllProductsCommand);
                }
                return _removeAllProductsCommand;
            }
        }

        private Boolean RemoveAllProducts_CanExecute()
        {
            if (this.AssignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveAllProducts_Executed()
        {
            //Get list to add
            List<MinorRevisionProductRow> addList = this.AssignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.UnassignedProducts.Select(p => p.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (MinorRevisionProductRow productRow in addList)
            {
                if (!currentProductIds.Contains(productRow.ProductInfo.Id))
                {
                    addToSelectedProducts.Add(productRow.ProductInfo);
                }
            }

            //for each product in the list of selected assigned products
            foreach (ProductInfo delistProduct in addToSelectedProducts)
            {
                //add the product to the list of unassigned products
                _unassignedProducts.Add(delistProduct);
            }

            //clear the selected assigned products
            this.SelectedAssignedProducts.Clear();

            //remove from the assigned
            this._assignedProducts.RemoveRange(addList);
        }

        #endregion

        #region SelectReplacementProductCommand

        private RelayCommand _selectReplacementProductCommand;

        /// <summary>
        /// Select the replacement product for the row in question
        /// </summary>
        public RelayCommand SelectReplacementProductCommand
        {
            get
            {
                if (_selectReplacementProductCommand == null)
                {
                    _selectReplacementProductCommand = new RelayCommand(
                        p => SelectReplacementProduct_Executed(),
                        p => SelectReplacementProduct_CanExecute())
                    {
                        FriendlyName = Message.ReplaceProduct_SelectReplacementProduct,
                        FriendlyDescription = Message.ReplaceProduct_SelectReplacementProduct_Description
                    };
                    base.ViewModelCommands.Add(_selectReplacementProductCommand);
                }
                return _selectReplacementProductCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SelectReplacementProduct_CanExecute()
        {
            return (this.SelectedLinkRow != null);
        }

        private void SelectReplacementProduct_Executed()
        {
            if (this.AttachedControl != null)
            {
                var winModel = new ProductSelectorViewModel();
                winModel.ShowDialog(this.AttachedControl);

                if (winModel.DialogResult == true)
                {
                    this.SelectedLinkRow.ReplacementProductInfo = ProductInfo.NewProductInfo(winModel.SelectedProduct);

                    //Get list of used product ids
                    List<Int32> invalidProductIds = this.CurrentActionsProductIds.ToList();
                    invalidProductIds.AddRange(this.AvailableLinkRows.Select(p => p.ProductInfo.Id));
                    invalidProductIds.AddRange(this.AvailableLinkRows.Where(p => p.ReplacementProductInfo != null && p != this.SelectedLinkRow).Select(p => p.ReplacementProductInfo.Id));

                    //Validate row
                    this.SelectedLinkRow.Validate(invalidProductIds.Distinct());
                }
            }
        }

        #endregion

        #region Selection Commands

        #region SelectProductUniverse

        private RelayCommand _selectProductUniverseCommand;

        public RelayCommand SelectProductUniverseCommand
        {
            get
            {
                if (_selectProductUniverseCommand == null)
                {
                    _selectProductUniverseCommand = new RelayCommand(
                        p => SelectProductUniverse_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                }
                return _selectProductUniverseCommand;
            }
        }

        private void SelectProductUniverse_Executed()
        {
            ProductUniverseSelectionWindow productUniverseWindow = new ProductUniverseSelectionWindow();

            App.ShowWindow(productUniverseWindow, this.AttachedControl, true);

            if (productUniverseWindow.SelectedProductUniverse != null)
            {
                this.SelectedProductUniverse = productUniverseWindow.SelectedProductUniverse;
            }
        }

        #endregion

        #region SelectConsumerDecisionTree

        private RelayCommand _selectConsumerDecisionTreeCommand;

        public RelayCommand SelectConsumerDecisionTreeCommand
        {
            get
            {
                if (_selectConsumerDecisionTreeCommand == null)
                {
                    _selectConsumerDecisionTreeCommand = new RelayCommand(
                        p => SelectConsumerDecisionTree_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                }
                return _selectConsumerDecisionTreeCommand;
            }
        }

        private void SelectConsumerDecisionTree_Executed()
        {
            ConsumerDecisionTreeWindow consumerDecisionTreeWindow = new ConsumerDecisionTreeWindow();
            //consumerDecisionTreeWindow.SelectionMode = DataGridSelectionMode.Single;
            App.ShowWindow(consumerDecisionTreeWindow, this.AttachedControl, true);

            if (consumerDecisionTreeWindow.SelectionResult != null)
            {
                this.SelectedConsumerDecisionTree = consumerDecisionTreeWindow.SelectionResult;
            }
        }

        #endregion

        #endregion

        #endregion

        #region Event Handlers
        /// <summary>
        /// Responds to a change to the product info list model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
        {

            if (e.OldModel != null)
            {
                ProductInfoList oldList = (ProductInfoList)e.NewModel;
                oldList.AddedNew -= ProductInfoListView_AddedNew;
            }
            if (e.NewModel != null)
            {
                ProductInfoList newList = (ProductInfoList)e.NewModel;
                newList.AddedNew += ProductInfoListView_AddedNew;
                this._unassignedProducts.AddRange(newList);
            }

            //mark as finished searching
            this.IsSearching = false;

            //If the productInfoListView doesn't contain any results the "No Records" message should be displayed
            this.ContainsRecords = (_productInfoListView.Model.Count > 0);

            //check if another search is queued
            if (_isSearchQueued)
            {
                PerformSearch();
            }
        }

        /// <summary>
        /// Responds ProductInfoListView adding items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_AddedNew(object sender, Csla.Core.AddedNewEventArgs<ProductInfo> e)
        {
            this._unassignedProducts.Add(e.NewObject);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Method to update the link rows
        /// </summary>
        public void UpdateLinkRows()
        {
            //Create selected products dictionary
            Dictionary<Int32, MinorRevisionProductRow> selectedProducts = this.AssignedProducts.ToDictionary(p => p.ProductInfo.Id);

            //Remove any unselected product's from the link collection
            foreach (ProductReplacementLinkRowViewModel linkRow in this._availableLinkRows.ToList())
            {
                //If product is no longer selected
                if (!selectedProducts.ContainsKey(linkRow.ProductInfo.Id))
                {
                    //Remove link
                    this._availableLinkRows.Remove(linkRow);
                }
            }

            //Get dictionary of existing link rows
            Dictionary<Int32, ProductReplacementLinkRowViewModel> existingLinks = this._availableLinkRows.ToDictionary(p => p.ProductInfo.Id);

            //Enumerate through the selected products
            foreach (MinorRevisionProductRow product in this.AssignedProducts)
            {
                //If doesn't already exist
                if (!existingLinks.ContainsKey(product.ProductInfo.Id))
                {
                    //Create and add row
                    this._availableLinkRows.Add(new ProductReplacementLinkRowViewModel(product.ProductInfo));
                }
            }
        }

        /// <summary>
        /// Method to undo the effects of the wizard if its not completed
        /// </summary>
        public void CancelWizard()
        {
            //Remove products to trigger event
            foreach (MinorRevisionProductRow productRow in this.AssignedProducts.ToList())
            {
                this._assignedProducts.Remove(productRow);
            }
        }


        #region Selection Methods

        /// <summary>
        /// Performs the search operation
        /// </summary>
        private void PerformSearch()
        {
            this._unassignedProducts.Clear();

            Int32 entityId = App.ViewState.EntityId;

            if (!this.IsSearching)
            {
                _isSearchQueued = false;

                this.IsSearching = true;

                //search asyncronously
                switch (this.SearchType)
                {
                    case MinorRevisionProductSearchType.Criteria:
                        _productInfoListView.BeginFetchByEntityIdSearchCriteria(entityId, this.SearchCriteria != null ? this.SearchCriteria : "");
                        break;

                    case MinorRevisionProductSearchType.ProductUniverse:
                        _productInfoListView.BeginFetchByProductUniverseId((this.SelectedProductUniverse != null) ? this.SelectedProductUniverse.Id : 0);
                        break;

                    case MinorRevisionProductSearchType.ConsumerDecisionTree:
                        _productInfoListView.BeginFetchByConsumerDecisionTree((this.SelectedConsumerDecisionTree != null) ? this.SelectedConsumerDecisionTree.Id : 0);
                        break;
                    default: throw new NotImplementedException();
                }
            }
            else
            {
                _isSearchQueued = true;
            }

        }

        #endregion
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedAssignedProducts.Clear();
                    _selectedUnassignedProducts.Clear();
                    _unassignedProducts.Clear();
                    _assignedProducts.Clear();
                    //_scenarioProductUniverseProductLookup.Clear();

                    _productInfoListView.ModelChanged -= ProductInfoListView_ModelChanged;
                    if (_productInfoListView.Model != null)
                    {
                        _productInfoListView.Model.AddedNew -= ProductInfoListView_AddedNew;
                    }
                    _productInfoListView.Dispose();
                    _productInfoListView = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Row view model for the product to be replaced
    /// </summary>
    public class ProductReplacementLinkRowViewModel : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private ProductInfo _productInfo;
        private Boolean _isValid;
        private ProductInfo _replacementProductInfo = null;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductInfoProperty = WpfHelper.GetPropertyPath<ProductReplacementLinkRowViewModel>(p => p.ProductInfo);
        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<ProductReplacementLinkRowViewModel>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<ProductReplacementLinkRowViewModel>(p => p.ProductName);
        public static readonly PropertyPath ReplacementProductInfoProperty = WpfHelper.GetPropertyPath<ProductReplacementLinkRowViewModel>(p => p.ReplacementProductInfo);
        public static readonly PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<ProductReplacementLinkRowViewModel>(p => p.IsValid);


        #endregion

        #region Properties

        /// <summary>
        /// Returns the product info
        /// </summary>
        public ProductInfo ProductInfo
        {
            get { return _productInfo; }
        }

        /// <summary>
        /// Returns the product gtin
        /// </summary>
        public String ProductGtin
        {
            get { return _productInfo.Gtin; }
        }

        /// <summary>
        /// Returns the product name
        /// </summary>
        public String ProductName
        {
            get { return _productInfo.Name; }
        }

        /// <summary>
        /// Returns the product info
        /// </summary>
        public ProductInfo ReplacementProductInfo
        {
            get { return _replacementProductInfo; }
            set
            {
                _replacementProductInfo = value;
                OnPropertyChanged(ReplacementProductInfoProperty);
            }
        }

        /// <summary>
        /// Returns whether the row is valid
        /// </summary>
        public Boolean IsValid
        {
            get
            {
                return _isValid;
            }
            set
            {
                _isValid = value;
                OnPropertyChanged(IsValidProperty);
                OnPropertyChanged(ReplacementProductInfoProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="productInfo"></param>
        /// <param name="locationInfoList"></param>
        /// <param name="initialLinkDictionary"></param>
        public ProductReplacementLinkRowViewModel(ProductInfo productInfo)
        {
            _productInfo = productInfo;
        }


        #endregion

        #region Methods

        /// <summary>
        /// Method to validate the replace actions from main view model
        /// </summary>
        /// <param name="invalidProductIds"></param>
        public void Validate(IEnumerable<Int32> invalidProductIds)
        {
            Boolean isValid = true;

            if (this.ReplacementProductInfo == null)
            {
                isValid = false;
            }
            else
            {
                if (invalidProductIds.Contains(this.ReplacementProductInfo.Id))
                {
                    isValid = false;
                }
            }

            this.IsValid = isValid;
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this._productInfo = null;
                    this._replacementProductInfo = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo


        string IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        string IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                if (columnName == ReplacementProductInfoProperty.Path
                    || columnName == IsValidProperty.Path)
                {
                    if (this.ReplacementProductInfo == null)
                        result = Message.ReplaceProduct_ProductReplacementRequired;
                    else if (this.ReplacementProductInfo != null
                                && !this.IsValid)
                        result = Message.ReplaceProduct_InvalidProductSelected;
                }

                return result; // return result
            }
        }
        #endregion
    }
}
