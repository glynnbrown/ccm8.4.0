﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Interaction logic for ReplaceProductWindow.xaml
    /// </summary>
    public partial class ReplaceProductWindow : ExtendedRibbonWindow
    {
        #region Constants

        const String SelectReplacementProductCommandKey = "SelectReplacementProductCommand";

        #endregion

        #region Fields

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ReplaceProductViewModel), typeof(ReplaceProductWindow),
                new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public ReplaceProductViewModel ViewModel
        {
            get { return (ReplaceProductViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ReplaceProductWindow senderControl = (ReplaceProductWindow) obj;

            if (e.OldValue != null)
            {
                ReplaceProductViewModel oldModel = (ReplaceProductViewModel) e.OldValue;
                oldModel.AttachedControl = null;
                senderControl.Resources.Remove(SelectReplacementProductCommandKey);
            }

            if (e.NewValue != null)
            {
                ReplaceProductViewModel newModel = (ReplaceProductViewModel) e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(SelectReplacementProductCommandKey, newModel.SelectReplacementProductCommand);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ReplaceProductWindow(AssortmentMinorRevision assortmentMinorRevision,
            ConsumerDecisionTreeInfo consumerDecisioTreeInfo)
        {
            InitializeComponent();
            this.ViewModel = new ReplaceProductViewModel(assortmentMinorRevision, consumerDecisioTreeInfo);
        }

        #endregion

        #region Methods

        #endregion

        #region Event Handlers

        #region Unassigned Grid

        /// <summary>
        /// Adds the doubleclicked row to the current products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedProductsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.AddSelectedProductsCommand.Execute();
        }

        /// <summary>
        /// Removes the passed products from the current sublevel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedProductsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            this.ViewModel.RemoveSelectedProductsCommand.Execute();
        }

        /// <summary>
        /// Clears the selected items of the assigned grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xUnassignedProductsGrid_ItemsSelected(object sender, ExtendedDataGridItemSelectedEventArgs e)
        {
            this.xUnassignedProductsGrid.SelectedItems.Clear();
        }

        /// <summary>
        /// Makes an add request for all selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnassignedGridRowContext_AddSelected(object sender, RoutedEventArgs e)
        {
            this.ViewModel.AddSelectedProductsCommand.Execute();
        }

        #endregion

        #region Assigned Grid

        /// <summary>
        /// Removes the location from the current cluster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedProductsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.RemoveSelectedProductsCommand.Execute();
        }

        /// <summary>
        /// Adds the dropped locations to the cluster
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedProductsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            this.ViewModel.AddSelectedProductsCommand.Execute();
        }

        /// <summary>
        /// Makes a remove request for all selected items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xAssignedProductsRowContext_RemoveSelected(object sender, RoutedEventArgs e)
        {
            this.ViewModel.RemoveSelectedProductsCommand.Execute();
        }

        #endregion

        /// <summary>
        /// Close button click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        #endregion

        #region Commands

        #endregion

        #region Window Close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            //Ensure wizard is complete on closing
            if (!this.ViewModel.IsWizardComplete)
            {
                //Undo action so far as product & performance had to be loaded half way through the wizard
                //to allow auto distribution ranking to take place.
                this.ViewModel.CancelWizard();
            }
            base.OnClosing(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
                (Action) (() =>
                {
                    IDisposable disposableViewModel = this.ViewModel as IDisposable;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }
                }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

        private void ProductGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            Boolean isAssigned = senderControl?.Name == xAssignedProductsGrid.Name;

            if (e.Key == Key.Return)
            {
                if (!isAssigned) this.ViewModel?.AddSelectedProductsCommand.Execute();
                else this.ViewModel?.RemoveSelectedProductsCommand.Execute();
                Keyboard.Focus(isAssigned ? xAssignedProductsGrid : xUnassignedProductsGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isAssigned ? xUnassignedProductsGrid : xAssignedProductsGrid);
                e.Handled = true;
            }
        }
    }
}