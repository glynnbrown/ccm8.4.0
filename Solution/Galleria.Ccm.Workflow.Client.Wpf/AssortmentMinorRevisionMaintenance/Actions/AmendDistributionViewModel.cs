﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
// V8-26705 : A.Kuszyk
//  Changed ProductSelectionWindow to Common.Wpf version.
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    /// <summary>
    /// Denotes the steps available in the list product wizard
    /// </summary>
    public enum AmendDistributionWizardStep
    {
        SelectProducts,
        UpdateDistribution
    }

    public class AmendDistributionViewModel : ViewModelAttachedControlObject<AmendDistributionWindow>
    {
        #region Fields

        private Int32 _entityId;
        private AssortmentMinorRevision _minorRevision;
        private Boolean _isWizardComplete = false;

        //Wizard related
        private AmendDistributionWizardStep _currentStep = AmendDistributionWizardStep.SelectProducts;
        private Int16 _currentStepNumber = 1;
        private BulkObservableCollection<Int32> _currentActionsProductIds;

        //Selected data
        private ObservableCollection<ProductLocationLinkRowViewModel> _availableLinkRows = new ObservableCollection<ProductLocationLinkRowViewModel>();
        private ProductLocationLinkRowViewModel _selectedLinkRow;

        //Products
        private ObservableCollection<ProductInfo> _selectedUnassignedProducts = new ObservableCollection<ProductInfo>();
        private BulkObservableCollection<ProductInfo> _unassignedProducts = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _unassignedProductsRO;

        private ObservableCollection<MinorRevisionProductRow> _selectedAssignedProducts = new ObservableCollection<MinorRevisionProductRow>();
        private BulkObservableCollection<MinorRevisionProductRow> _assignedProducts = new BulkObservableCollection<MinorRevisionProductRow>();
        private ReadOnlyBulkObservableCollection<MinorRevisionProductRow> _assignedProductsRO;

        #region Selection Fields
        private MinorRevisionProductSearchType _searchType = MinorRevisionProductSearchType.Criteria;
        private DataGridSelectionMode _selectionMode = DataGridSelectionMode.Extended;
        private ProductInfoListViewModel _productInfoListView = new ProductInfoListViewModel();
        private ObservableCollection<ProductInfo> _selectedProducts = new ObservableCollection<ProductInfo>();
        private Boolean _isSearching;
        private Boolean _isSearchQueued;

        private String _searchCriteria;

        private ProductGroup _selectedFinancialGroup;

        private ProductUniverseInfo _selectedProductUniverse;
        private ConsumerDecisionTreeInfo _selectedConsumerDecisionTreeInfo;

        private Boolean _containsRecords;
        #endregion
        #endregion

        #region Property Paths

        public static readonly PropertyPath IsWizardCompleteProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.IsWizardComplete);
        public static readonly PropertyPath IsPreviousCommandVisibleProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.IsPreviousCommandVisible);
        public static readonly PropertyPath IsNextCommandVisibleProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.IsNextCommandVisible);
        public static readonly PropertyPath IsFinishCommandVisibleProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.IsFinishCommandVisible);
        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath CurrentStepNumberProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.CurrentStepNumber);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.NextCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.PreviousCommand);
        public static readonly PropertyPath FinishCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.FinishCommand);
        public static readonly PropertyPath NumberOfStagesProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.NumberOfStages);
        public static readonly PropertyPath AvailableLinkRowsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.AvailableLinkRows);
        public static readonly PropertyPath SelectedLinkRowProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectedLinkRow);
        public static readonly PropertyPath CurrentActionsProductIdsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.CurrentActionsProductIds);

        public static readonly PropertyPath SelectedUnassignedProductsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectedUnassignedProducts);
        public static readonly PropertyPath SelectedAssignedProductsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectedAssignedProducts);
        public static readonly PropertyPath UnassignedProductsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.UnassignedProducts);
        public static readonly PropertyPath AssignedProductsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.AssignedProducts);

        public static readonly PropertyPath AddSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.AddSelectedProductsCommand);
        public static readonly PropertyPath RemoveSelectedProductsCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.RemoveSelectedProductsCommand);
        public static readonly PropertyPath AddAllProductsCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.AddAllProductsCommand);
        public static readonly PropertyPath RemoveAllProductsCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.RemoveAllProductsCommand);

        #region Selection Property Paths

        //properties
        public static readonly PropertyPath SearchTypeProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SearchType);
        public static readonly PropertyPath SelectionModeProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectionMode);
        public static readonly PropertyPath IsSearchingProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.IsSearching);
        public static readonly PropertyPath SearchResultsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SearchResults);
        public static readonly PropertyPath SelectedProductsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectedProducts);
        public static readonly PropertyPath SearchCriteriaProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SearchCriteria);
        public static readonly PropertyPath SelectedFinancialGroupProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectedFinancialGroup);
        public static readonly PropertyPath SelectedProductUniverseProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectedProductUniverse);
        public static readonly PropertyPath ContainsRecordsProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.ContainsRecords);
        public static readonly PropertyPath SelectedConsumerDecisionTreeProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectedConsumerDecisionTree);

        //commands
        public static readonly PropertyPath SelectFinancialGroupCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectFinancialGroupCommand);
        public static readonly PropertyPath SelectProductUniverseCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectProductUniverseCommand);
        public static readonly PropertyPath SelectConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<AmendDistributionViewModel>(p => p.SelectConsumerDecisionTreeCommand);

        #endregion
        #endregion

        #region Properties

        /// <summary>
        /// Returns the current scenario
        /// </summary>
        public AssortmentMinorRevision CurrentMinorRevision
        {
            get
            {
                return _minorRevision;
            }
        }

        /// <summary>
        /// Returns true if the cancel command should be available on the current step
        /// </summary>
        public Boolean IsPreviousCommandVisible
        {
            get
            {
                if (this.CurrentStep == AmendDistributionWizardStep.UpdateDistribution)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Returns true if the next command should be available
        /// for the current step
        /// </summary>
        public Boolean IsNextCommandVisible
        {
            get
            {
                if (this.CurrentStep == AmendDistributionWizardStep.SelectProducts)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Returns true if the finish command should be available
        /// for the current step
        /// </summary>
        public Boolean IsFinishCommandVisible
        {
            get
            {
                if (this.CurrentStep == AmendDistributionWizardStep.UpdateDistribution)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// The current step that the data selection wizard is at
        /// </summary>
        public AmendDistributionWizardStep CurrentStep
        {
            get { return _currentStep; }
            set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                //Refresh button bindings
                OnPropertyChanged(IsNextCommandVisibleProperty);
                OnPropertyChanged(IsPreviousCommandVisibleProperty);
                OnPropertyChanged(IsFinishCommandVisibleProperty);
            }
        }

        /// <summary>
        /// The current step number that the data selection wizard is at
        /// </summary>
        public Int16 CurrentStepNumber
        {
            get { return _currentStepNumber; }
        }

        /// <summary>
        /// Returns the number of stages
        /// </summary>
        public Int32 NumberOfStages
        {
            get { return 2; }
        }

        /// <summary>
        /// Returns the collection of selected unassigned products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedUnassignedProducts
        {
            get { return _selectedUnassignedProducts; }
        }

        /// <summary>
        /// Returns the collection of unassigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> UnassignedProducts
        {
            get
            {
                if (_unassignedProductsRO == null)
                {
                    _unassignedProductsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_unassignedProducts);
                }
                return _unassignedProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected assigned products
        /// </summary>
        public ObservableCollection<MinorRevisionProductRow> SelectedAssignedProducts
        {
            get { return _selectedAssignedProducts; }
        }

        /// <summary>
        /// Returns the collection of Assigned products
        /// </summary>
        public ReadOnlyBulkObservableCollection<MinorRevisionProductRow> AssignedProducts
        {
            get
            {
                if (_assignedProductsRO == null)
                {
                    _assignedProductsRO = new ReadOnlyBulkObservableCollection<MinorRevisionProductRow>(_assignedProducts);
                }
                return _assignedProductsRO;
            }
        }

        /// <summary>
        /// Represents all available link rows for the current scenario
        /// </summary>
        public ObservableCollection<ProductLocationLinkRowViewModel> AvailableLinkRows
        {
            get { return _availableLinkRows; }
        }

        /// <summary>
        /// Gets/sets the selected link row
        /// </summary>
        public ProductLocationLinkRowViewModel SelectedLinkRow
        {
            get { return _selectedLinkRow; }
            set
            {
                _selectedLinkRow = value;
                OnPropertyChanged(SelectedLinkRowProperty);
            }
        }

        /// <summary>
        /// Returns the current list of actions product ids 
        /// </summary>
        public BulkObservableCollection<Int32> CurrentActionsProductIds
        {
            get { return _currentActionsProductIds; }
        }

        /// <summary>
        /// Returns the current list of actions product ids 
        /// </summary>
        public Boolean IsWizardComplete
        {
            get { return _isWizardComplete; }
            set
            {
                _isWizardComplete = value;
                OnPropertyChanged(IsWizardCompleteProperty);
            }
        }
        #region Selection Properties

        /// <summary>
        /// Gets/Sets the selection type
        /// </summary>
        public MinorRevisionProductSearchType SearchType
        {
            get { return _searchType; }
            set
            {
                _searchType = value;
                OnPropertyChanged(SearchTypeProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Returns the selection mode for this instance
        /// </summary>
        public DataGridSelectionMode SelectionMode
        {
            get { return _selectionMode; }
        }

        /// <summary>
        /// Returns true if a search is currently being performed
        /// </summary>
        public Boolean IsSearching
        {
            get { return _isSearching; }
            private set
            {
                _isSearching = value;
                OnPropertyChanged(IsSearchingProperty);
            }
        }

        /// <summary>
        /// Returns the readonly collection of search results
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> SearchResults
        {
            get
            {
                return _productInfoListView.BindingView;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedProducts
        {
            get { return _selectedProducts; }
        }

        /// <summary>
        /// Gets/Sets the search criteria value.
        /// </summary>
        public String SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                _searchCriteria = value;
                OnPropertyChanged(SearchCriteriaProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Gets/Sets the selected financial group search criteria
        /// </summary>
        public ProductGroup SelectedFinancialGroup
        {
            get { return _selectedFinancialGroup; }
            set
            {
                _selectedFinancialGroup = value;
                OnPropertyChanged(SelectedFinancialGroupProperty);
                PerformSearch();
            }
        }

        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ProductUniverseInfo SelectedProductUniverse
        {
            get { return _selectedProductUniverse; }
            set
            {
                _selectedProductUniverse = value;
                OnPropertyChanged(SelectedProductUniverseProperty);
                PerformSearch();
            }
        }


        /// <summary>
        /// Gets/Sets the selected product universe search criteria
        /// </summary>
        public ConsumerDecisionTreeInfo SelectedConsumerDecisionTree
        {
            get { return _selectedConsumerDecisionTreeInfo; }
            set
            {
                _selectedConsumerDecisionTreeInfo = value;
                OnPropertyChanged(SelectedConsumerDecisionTreeProperty);
                PerformSearch();
            }
        }


        /// <summary>
        /// Returns true if the search results grid contains records
        /// </summary>
        public Boolean ContainsRecords
        {
            get { return _containsRecords; }
            private set
            {
                _containsRecords = value;
                OnPropertyChanged(ContainsRecordsProperty);
            }
        }

        #endregion
        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public AmendDistributionViewModel(AssortmentMinorRevision assortmentMinorRevision, ConsumerDecisionTreeInfo consumerDecisionTreeInfo)
            : this(
            assortmentMinorRevision,
            App.ViewState.EntityId,
            consumerDecisionTreeInfo)
        { }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public AmendDistributionViewModel(AssortmentMinorRevision assortmentMinorRevision,
            Int32 entityId, ConsumerDecisionTreeInfo consumerDecisionTreeInfo)
        {
            //Set minor revision
            _minorRevision = assortmentMinorRevision;
            _entityId = entityId;

            //Set CDT
            if (consumerDecisionTreeInfo != null)
            {
                this.SearchType = MinorRevisionProductSearchType.ConsumerDecisionTree;
                this.SelectedConsumerDecisionTree = consumerDecisionTreeInfo;
            }

            _productInfoListView.ModelChanged += ProductInfoListView_ModelChanged;
            if (_productInfoListView.Model != null)
            {
                _productInfoListView.Model.AddedNew += ProductInfoListView_AddedNew;
            }

            //Construct taken products ids for validation purposes, only 1 action per product
            BulkObservableCollection<Int32> actionProductIds = new BulkObservableCollection<Int32>();
            //List actions
            if (this.CurrentMinorRevision.ListActions != null
                && this.CurrentMinorRevision.ListActions.Count > 0)
            {
                actionProductIds.AddRange(this.CurrentMinorRevision.ListActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //DeList actions
            if (this.CurrentMinorRevision.DeListActions != null
                && this.CurrentMinorRevision.DeListActions.Count > 0)
            {
                actionProductIds.AddRange(this.CurrentMinorRevision.DeListActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //Amend Dist actions
            if (this.CurrentMinorRevision.AmendDistributionActions != null
                && this.CurrentMinorRevision.AmendDistributionActions.Count > 0)
            {
                actionProductIds.AddRange(this.CurrentMinorRevision.AmendDistributionActions.Where(p => p.ProductId.HasValue).Select(p => p.ProductId.Value));
            }
            //Assign to field
            _currentActionsProductIds = actionProductIds;

            //Attach to the assigned products collection changed event
            this.AssignedProducts.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(AssignedProducts_BulkCollectionChanged);
        }

        #endregion

        #region Commands

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// Advances the process to the next step
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Next_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case AmendDistributionWizardStep.SelectProducts:
                    if (this.AssignedProducts.Count <= 0)
                    {
                        this.NextCommand.DisabledReason = Message.AmendDistribution_Next_NoAssignedProducts;
                        return false;
                    }
                    else if (!this.AssignedProducts.All(p => p.IsActionProductValid))
                    {
                        this.NextCommand.DisabledReason = Message.AmendDistribution_Next_InvalidProducts;
                        return false;
                    }
                    return true;
            }
            return false;
        }

        private void Next_Executed()
        {
            base.ShowWaitCursor(true);
            _currentStepNumber++;
            OnPropertyChanged(CurrentStepNumberProperty);
            switch (this.CurrentStep)
            {
                case AmendDistributionWizardStep.SelectProducts:

                    //Create link rows for product selection
                    UpdateLinkRows();

                    this.CurrentStep = AmendDistributionWizardStep.UpdateDistribution;
                    break;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Previous

        private RelayCommand _previousCommand;

        /// <summary>
        /// Moves the process back a step
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Previous_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case AmendDistributionWizardStep.UpdateDistribution:
                    return true;
            }
            return false;
        }

        private void Previous_Executed()
        {
            base.ShowWaitCursor(true);
            _currentStepNumber--;
            OnPropertyChanged(CurrentStepNumberProperty);
            switch (this.CurrentStep)
            {
                case AmendDistributionWizardStep.UpdateDistribution:
                    this.CurrentStep = AmendDistributionWizardStep.SelectProducts;
                    break;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Finish

        private RelayCommand _finishCommand;

        /// <summary>
        /// Ends the process back a step
        /// </summary>
        public RelayCommand FinishCommand
        {
            get
            {
                if (_finishCommand == null)
                {
                    _finishCommand = new RelayCommand(
                        p => Finish_Executed(),
                        p => Finish_CanExecute())
                    {
                        FriendlyName = Message.AmendDistribution_Finish,
                        DisabledReason = Message.AmendDistribution_Finish_DisabledReason
                    };
                    base.ViewModelCommands.Add(_finishCommand);
                }
                return _finishCommand;
            }
        }

        private Boolean Finish_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case AmendDistributionWizardStep.UpdateDistribution:
                    //Check all rows are valid
                    return this.AvailableLinkRows.All(p => p.IsValid);
            }
            return false;
        }

        private void Finish_Executed()
        {
            base.ShowWaitCursor(true);

            //Set wizard flag
            this.IsWizardComplete = true;

            //If assigned products are populated
            if (this.AvailableLinkRows.Count > 0)
            {
                //Enumerate through selected products
                foreach (ProductLocationLinkRowViewModel productLinkRow in this.AvailableLinkRows)
                {
                    #region Create Action

                    //Calculate new priority number
                    Int32 newPriority = this.CurrentMinorRevision.TotalActionsCount + 1;

                    //Create action
                    AssortmentMinorRevisionAmendDistributionAction action
                        = AssortmentMinorRevisionAmendDistributionAction.NewAssortmentMinorRevisionAmendDistributionAction(productLinkRow.ProductInfo.Id, productLinkRow.ProductInfo.Gtin, productLinkRow.ProductInfo.Name, newPriority);

                    //Enumerate through locations in product link
                    foreach (LocationInfo location in productLinkRow.Locations)
                    {
                        //Add location to the action
                        action.Locations.Add(AssortmentMinorRevisionAmendDistributionActionLocation.NewAssortmentMinorRevisionAmendDistributionActionLocation(location.Id, location.Code, location.Name, 0, 0));
                    }

                    //Add Action
                    this.CurrentMinorRevision.AmendDistributionActions.Add(action);

                    #endregion
                }
            }

            if (SearchType == MinorRevisionProductSearchType.ConsumerDecisionTree &&
                !_minorRevision.ConsumerDecisionTreeId.HasValue &&
                SelectedConsumerDecisionTree != null)
            {
                _minorRevision.ConsumerDecisionTreeId = SelectedConsumerDecisionTree.Id;
            }

            if (this.AttachedControl != null)
            {
                //Close window
                this.AttachedControl.Close();
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region SelectAvailableProductsCommand

        private RelayCommand _selectAvailableProductsCommand;

        /// <summary>
        /// Launches the product search and loads selected products into the 
        /// available products list
        /// </summary>
        public RelayCommand SelectAvailableProductsCommand
        {
            get
            {
                if (_selectAvailableProductsCommand == null)
                {
                    _selectAvailableProductsCommand = new RelayCommand(
                        p => SelectAvailableProducts_Executed(),
                        p => SelectAvailableProducts_CanExecute())
                    {
                        FriendlyName = Message.AmendDistribution_SelectAvailableProducts,
                        FriendlyDescription = Message.AmendDistribution_SelectAvailableProducts_Description,
                        Icon = ImageResources.AmendDistribution_SelectAvailableProducts
                    };
                    base.ViewModelCommands.Add(_selectAvailableProductsCommand);
                }
                return _selectAvailableProductsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SelectAvailableProducts_CanExecute()
        {
            //user must have fetch permission

            return true;
        }

        private void SelectAvailableProducts_Executed()
        {
            if (this.AttachedControl != null)
            {
                IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

                var winModel = new ProductSelectorViewModel(currentProductIds);
                winModel.ShowDialog(this.AttachedControl);

                if (winModel.DialogResult == true)
                {
                    List<Product> selectedProducts = winModel.AssignedProducts.ToList();

                    //remove any old products
                    List<MinorRevisionProductRow> productsToRemove =
                       _assignedProducts.Where(p => !selectedProducts.Any(a => a.Id == p.ProductInfo.Id)).ToList();
                    _assignedProducts.RemoveRange(productsToRemove);

                    //add any new products
                    List<MinorRevisionProductRow> productsToAdd = new List<MinorRevisionProductRow>();
                    foreach (Product product in selectedProducts)
                    {
                        if (!currentProductIds.Contains(product.Id))
                        {
                            //Check if action product is valid
                            Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(product.Id);

                            //add the product to the list of assigned products
                            productsToAdd.Add(new MinorRevisionProductRow(ProductInfo.NewProductInfo(product), isActionProductValid));
                        }
                    }
                    _assignedProducts.AddRange(productsToAdd);
                }

               
                
                //ProductSelectionWindow win = new ProductSelectionWindow(DataGridSelectionMode.Extended);
                //App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

                

                ////add all selected products not already taken.
                //List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
                //foreach (ProductInfo productInfo in win.ViewModel.SelectedProducts)
                //{
                //    if (!currentProductIds.Contains(productInfo.Id))
                //    {
                //        addToSelectedProducts.Add(productInfo);
                //    }
                //}

                ////Create and add rows for selected
                //foreach (ProductInfo productInfo in addToSelectedProducts)
                //{
                //    //Check if action product is valid
                //    Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //    //add the product to the list of assigned products
                //    _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
                //}

            }
            else
            {
                //for unit testing only
                _assignedProducts.Clear();
                ProductInfoList productInfoList = ProductInfoList.FetchByEntityId(App.ViewState.EntityId);
                foreach (ProductInfo productInfo in productInfoList)
                {
                    //add the product to the list of assigned products
                    _assignedProducts.Add(new MinorRevisionProductRow(productInfo, true));
                }
            }
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.AmendDistribution_AddSelectedProducts,
                        Icon = ImageResources.MinorRevision_AddSelectedProducts
                    };
                    base.ViewModelCommands.Add(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            //must have some unassigned products selected
            return (this.SelectedUnassignedProducts.Count > 0);
        }

        private void AddSelectedProducts_Executed()
        {
            //Get list to add
            List<ProductInfo> addList = this.SelectedUnassignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (ProductInfo productInfo in addList)
            {
                if (!currentProductIds.Contains(productInfo.Id))
                {
                    addToSelectedProducts.Add(productInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo productInfo in addToSelectedProducts)
            {
                //Check if action product is valid
                Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //add the product to the list of assigned products
                _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
            }

            //clear the selected available products
            this.SelectedUnassignedProducts.Clear();

            //remove from the available
            this._unassignedProducts.RemoveRange(addList);
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyName = Message.AmendDistribution_RemoveSelectedProducts,
                        Icon = ImageResources.MinorRevision_RemoveSelectedProducts
                    };
                    base.ViewModelCommands.Add(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        private Boolean RemoveSelectedProducts_CanExecute()
        {
            if (this.SelectedAssignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            //Get list to remove
            List<MinorRevisionProductRow> addList = this.SelectedAssignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.UnassignedProducts.Select(p => p.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (MinorRevisionProductRow productRow in addList)
            {
                if (!currentProductIds.Contains(productRow.ProductInfo.Id))
                {
                    addToSelectedProducts.Add(productRow.ProductInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo delistProduct in addToSelectedProducts)
            {
                //add the product to the list of unassigned products
                _unassignedProducts.Add(delistProduct);
            }

            //clear the selected assigned products
            this.SelectedAssignedProducts.Clear();

            //remove from the assigned
            this._assignedProducts.RemoveRange(addList);
        }

        #endregion

        #region AddAllProductsCommand

        private RelayCommand _addAllProductsCommand;

        public RelayCommand AddAllProductsCommand
        {
            get
            {
                if (_addAllProductsCommand == null)
                {
                    _addAllProductsCommand = new RelayCommand(
                        p => AddAllProducts_Executed(),
                        p => AddAllProducts_CanExecute())
                    {
                        FriendlyName = Message.AmendDistribution_AddAllProducts,
                        Icon = ImageResources.MinorRevision_AddAllProducts
                    };
                    base.ViewModelCommands.Add(_addAllProductsCommand);
                }
                return _addAllProductsCommand;
            }
        }

        private Boolean AddAllProducts_CanExecute()
        {
            if (this.UnassignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddAllProducts_Executed()
        {
            //Get list to add
            List<ProductInfo> addList = this.UnassignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.AssignedProducts.Select(p => p.ProductInfo.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (ProductInfo productInfo in addList)
            {
                if (!currentProductIds.Contains(productInfo.Id))
                {
                    addToSelectedProducts.Add(productInfo);
                }
            }

            //for each product in the list of selected available products
            foreach (ProductInfo productInfo in addToSelectedProducts)
            {
                //Check if action product is valid
                Boolean isActionProductValid = !this.CurrentActionsProductIds.Contains(productInfo.Id);

                //add the product to the list of assigned products
                _assignedProducts.Add(new MinorRevisionProductRow(productInfo, isActionProductValid));
            }

            //clear the selected available products
            this.SelectedUnassignedProducts.Clear();

            //remove from the available
            this._unassignedProducts.RemoveRange(addList);
        }

        #endregion

        #region RemoveAllProductsCommand

        private RelayCommand _removeAllProductsCommand;

        public RelayCommand RemoveAllProductsCommand
        {
            get
            {
                if (_removeAllProductsCommand == null)
                {
                    _removeAllProductsCommand = new RelayCommand(
                        p => RemoveAllProducts_Executed(),
                        p => RemoveAllProducts_CanExecute())
                    {
                        FriendlyName = Message.AmendDistribution_RemoveAllProducts,
                        Icon = ImageResources.MinorRevision_RemoveAllProducts
                    };
                    base.ViewModelCommands.Add(_removeAllProductsCommand);
                }
                return _removeAllProductsCommand;
            }
        }

        private Boolean RemoveAllProducts_CanExecute()
        {
            if (this.AssignedProducts.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveAllProducts_Executed()
        {
            //Get list to add
            List<MinorRevisionProductRow> addList = this.AssignedProducts.ToList();

            //get current idds
            IEnumerable<Int32> currentProductIds = this.UnassignedProducts.Select(p => p.Id);

            //add all selected products not already taken.
            List<ProductInfo> addToSelectedProducts = new List<ProductInfo>();
            foreach (MinorRevisionProductRow productRow in addList)
            {
                if (!currentProductIds.Contains(productRow.ProductInfo.Id))
                {
                    addToSelectedProducts.Add(productRow.ProductInfo);
                }
            }

            //for each product in the list of selected assigned products
            foreach (ProductInfo delistProduct in addToSelectedProducts)
            {
                //add the product to the list of unassigned products
                _unassignedProducts.Add(delistProduct);
            }

            //clear the selected assigned products
            this.SelectedAssignedProducts.Clear();

            //remove from the assigned
            this._assignedProducts.RemoveRange(addList);
        }

        #endregion

        #region EditDistributionCommand

        private RelayCommand _editDistributionCommand;

        /// <summary>
        /// Edit the distribution for the selected product the action
        /// </summary>
        public RelayCommand EditDistributionCommand
        {
            get
            {
                if (_editDistributionCommand == null)
                {
                    _editDistributionCommand = new RelayCommand(
                        p => EditDistribution_Executed(),
                        p => EditDistribution_CanExecute())
                    {
                        FriendlyName = Message.AmendDistribution_EditDistribution,
                        FriendlyDescription = Message.AmendDistribution_EditDistribution_Description
                    };
                    base.ViewModelCommands.Add(_editDistributionCommand);
                }
                return _editDistributionCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean EditDistribution_CanExecute()
        {
            return (this.SelectedLinkRow != null);
        }

        private void EditDistribution_Executed()
        {
            if (this.AttachedControl != null)
            {
                //Create and display window to edit distribtion
                EditDistributionLocationsWindow win = new EditDistributionLocationsWindow(this.CurrentMinorRevision, this.SelectedLinkRow);
                App.ShowWindow(win, this.AttachedControl, true);

                IEnumerable<DistributionLocationRow> rangedLocations = win.ViewModel.RangedLocations;

                //Clear any existing locations
                this.SelectedLinkRow.Locations.Clear();

                //Assign new locations
                if (rangedLocations.Any())
                {
                    this.SelectedLinkRow.Locations.AddRange(rangedLocations.Select(p => p.Location));
                }
            }
        }

        #endregion

        #region Selection Commands

        #region SelectFinancialGroup

        private RelayCommand _selectFinancialGroupCommand;

        /// <summary>
        /// Opens the financial group selection window.
        /// </summary>
        public RelayCommand SelectFinancialGroupCommand
        {
            get
            {
                if (_selectFinancialGroupCommand == null)
                {
                    _selectFinancialGroupCommand = new RelayCommand(
                        p => SelectFinancialGroup_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                    //base.ViewModelCommands.Add(_selectFinancialGroupCommand);
                }
                return _selectFinancialGroupCommand;
            }
        }


        private void SelectFinancialGroup_Executed()
        {
            //if (this.AttachedControl != null)
            //{
            //launch the selection window in single mode.
            MerchGroupSelectionWindow win = new MerchGroupSelectionWindow();

            win.SelectionMode = System.Windows.Controls.DataGridSelectionMode.Single;
            App.ShowWindow(win, this.AttachedControl, /*isModal*/true);

            if (win.DialogResult == true)
            {
                this.SelectedFinancialGroup = win.SelectionResult.First();
            }
            //}
        }

        #endregion

        #region SelectProductUniverse

        private RelayCommand _selectProductUniverseCommand;

        public RelayCommand SelectProductUniverseCommand
        {
            get
            {
                if (_selectProductUniverseCommand == null)
                {
                    _selectProductUniverseCommand = new RelayCommand(
                        p => SelectProductUniverse_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                }
                return _selectProductUniverseCommand;
            }
        }

        private void SelectProductUniverse_Executed()
        {
            ProductUniverseSelectionWindow productUniverseWindow = new ProductUniverseSelectionWindow();

            App.ShowWindow(productUniverseWindow, this.AttachedControl, true);

            if (productUniverseWindow.SelectedProductUniverse != null)
            {
                this.SelectedProductUniverse = productUniverseWindow.SelectedProductUniverse;
            }
        }

        #endregion

        #region SelectConsumerDecisionTree

        private RelayCommand _selectConsumerDecisionTreeCommand;

        public RelayCommand SelectConsumerDecisionTreeCommand
        {
            get
            {
                if (_selectConsumerDecisionTreeCommand == null)
                {
                    _selectConsumerDecisionTreeCommand = new RelayCommand(
                        p => SelectConsumerDecisionTree_Executed())
                    {
                        FriendlyName = Message.Generic_Ellipsis
                    };
                }
                return _selectConsumerDecisionTreeCommand;
            }
        }

        private void SelectConsumerDecisionTree_Executed()
        {
            ConsumerDecisionTreeWindow consumerDecisionTreeWindow = new ConsumerDecisionTreeWindow();
            App.ShowWindow(consumerDecisionTreeWindow, this.AttachedControl, true);

            if (consumerDecisionTreeWindow.SelectionResult != null)
            {
                this.SelectedConsumerDecisionTree = consumerDecisionTreeWindow.SelectionResult;
            }
        }

        #endregion
        #endregion
        #endregion

        #region Event Handlers

        /// <summary>
        /// Method to handle the assigned products collection changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssignedProducts_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //TODO
            //if (e.Action == NotifyCollectionChangedAction.Add)
            //{
            //    foreach (Object item in e.ChangedItems)
            //    {
            //        MinorRevisionProductRow row = item as MinorRevisionProductRow;

            //        //If product is not in the product universe
            //        if (row != null && !row.IncludedInUniverse)
            //        {
            //            //Create minor review product
            //            ScenarioMinorRevisionProduct minorRevisionProduct = ScenarioMinorRevisionProduct.NewScenarioMinorRevisionProduct(row.ProductInfo);

            //            //TODO
            //            //Add to scenario
            //            //this.CurrentMinorRevision.MinorRevisionProducts.Add(minorRevisionProduct);

            //            //Populate performance data for this product
            //            this.CurrentMinorRevision.PopulateMinorRevisionPerformanceData(minorRevisionProduct);
            //        }
            //    }
            //}
            //else if (e.Action == NotifyCollectionChangedAction.Remove)
            //{
            //    foreach (Object item in e.ChangedItems)
            //    {
            //        MinorRevisionProductRow row = item as MinorRevisionProductRow;
            //        //If product is not in the product universe
            //        if (row != null && !row.IncludedInUniverse)
            //        {
            //            //Remove minor review product
            //            ScenarioMinorRevisionProduct productToRemove = this.CurrentMinorRevision.MinorRevisionProducts.FirstOrDefault(p => p.ProductId == row.ProductInfo.Id);

            //            if (productToRemove != null)
            //            {
            //                //remove product
            //                this.CurrentMinorRevision.MinorRevisionProducts.Remove(productToRemove);

            //                //unload performance data
            //                this.CurrentMinorRevision.UnloadMinorRevisionPerformanceData(productToRemove);
            //            }
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Responds to a change to the product info list model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
        {

            if (e.OldModel != null)
            {
                ProductInfoList oldList = (ProductInfoList)e.NewModel;
                oldList.AddedNew -= ProductInfoListView_AddedNew;
            }
            if (e.NewModel != null)
            {
                ProductInfoList newList = (ProductInfoList)e.NewModel;
                newList.AddedNew += ProductInfoListView_AddedNew;
                this._unassignedProducts.AddRange(newList);
            }

            //mark as finished searching
            this.IsSearching = false;

            //If the productInfoListView doesn't contain any results the "No Records" message should be displayed
            this.ContainsRecords = (_productInfoListView.Model.Count > 0);

            //check if another search is queued
            if (_isSearchQueued)
            {
                PerformSearch();
            }
        }

        /// <summary>
        /// Responds ProductInfoListView adding items
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductInfoListView_AddedNew(object sender, Csla.Core.AddedNewEventArgs<ProductInfo> e)
        {
            this._unassignedProducts.Add(e.NewObject);
        }
        #endregion

        #region Methods

        /// <summary>
        /// Method to update the link rows
        /// </summary>
        public void UpdateLinkRows()
        {
            //Create selected products dictionary
            Dictionary<Int32, MinorRevisionProductRow> selectedProducts = this.AssignedProducts.ToDictionary(p => p.ProductInfo.Id);

            //Remove any unselected product's from the link collection
            foreach (ProductLocationLinkRowViewModel linkRow in this._availableLinkRows.ToList())
            {
                //If product is no longer selected
                if (!selectedProducts.ContainsKey(linkRow.ProductInfo.Id))
                {
                    //Remove link
                    this._availableLinkRows.Remove(linkRow);
                }
            }

            //Get dictionary of existing link rows
            Dictionary<Int32, ProductLocationLinkRowViewModel> existingLinks = this._availableLinkRows.ToDictionary(p => p.ProductInfo.Id);

            LocationInfoListViewModel locationInfoListViewModel = new LocationInfoListViewModel();
            locationInfoListViewModel.FetchAllForEntity();

            //Enumerate through the selected products
            foreach (MinorRevisionProductRow product in this.AssignedProducts)
            {
                //If doesn't already exist
                if (!existingLinks.ContainsKey(product.ProductInfo.Id))
                {
                    IEnumerable<LocationInfo> rangedLocations = new List<LocationInfo>();

                    //Create and add row
                    this._availableLinkRows.Add(new ProductLocationLinkRowViewModel(product.ProductInfo, rangedLocations, locationInfoListViewModel.Model.Count));
                }
            }
        }

        /// <summary>
        /// Method to update the new distribution value
        /// </summary>
        public static Single CalculateDistributionValue(Int32 newRangeLocationCount, Int32 totalLocationCount)
        {
            //Calculate the distribution value
            Double value = (Double)newRangeLocationCount / (Double)totalLocationCount;
            Single newDistrbutionPercentage = Convert.ToSingle(Math.Round(value * 100, 2, MidpointRounding.AwayFromZero));
            newDistrbutionPercentage = Double.IsNaN(newDistrbutionPercentage) ? 0 : newDistrbutionPercentage;

            //return value
            return newDistrbutionPercentage;
        }

        /// <summary>
        /// Method to undo the effects of the wizard if its not completed
        /// </summary>
        public void CancelWizard()
        {
            //Remove products to trigger event
            foreach (MinorRevisionProductRow productRow in this.AssignedProducts.ToList())
            {
                this._assignedProducts.Remove(productRow);
            }
        }

        #region Selection Methods

        /// <summary>
        /// Performs the search operation
        /// </summary>
        private void PerformSearch()
        {
            this._unassignedProducts.Clear();

            Int32 entityId = App.ViewState.EntityId;

            if (!this.IsSearching)
            {
                _isSearchQueued = false;

                this.IsSearching = true;

                //search asyncronously
                switch (this.SearchType)
                {
                    case MinorRevisionProductSearchType.Criteria:
                        _productInfoListView.BeginFetchByEntityIdSearchCriteria(entityId, this.SearchCriteria != null ? this.SearchCriteria : "");
                        break;

                    case MinorRevisionProductSearchType.ProductUniverse:
                        _productInfoListView.BeginFetchByProductUniverseId((this.SelectedProductUniverse != null) ? this.SelectedProductUniverse.Id : 0);
                        break;

                    case MinorRevisionProductSearchType.ConsumerDecisionTree:
                        _productInfoListView.BeginFetchByConsumerDecisionTree((this.SelectedConsumerDecisionTree != null) ? this.SelectedConsumerDecisionTree.Id : 0);
                        break;

                    default: throw new NotImplementedException();
                }
            }
            else
            {
                _isSearchQueued = true;
            }

        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedAssignedProducts.Clear();
                    _selectedUnassignedProducts.Clear();
                    _unassignedProducts.Clear();
                    _assignedProducts.Clear();
                    _productInfoListView.ModelChanged -= ProductInfoListView_ModelChanged;
                    if (_productInfoListView.Model != null)
                    {
                        _productInfoListView.Model.AddedNew -= ProductInfoListView_AddedNew;
                    }
                    _productInfoListView.Dispose();
                    _productInfoListView = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Row view model for the product to ranged locations link
    /// </summary>
    public class ProductLocationLinkRowViewModel : ViewModelObject, IDataErrorInfo
    {
        #region Fields

        private ProductInfo _productInfo;
        private BulkObservableCollection<LocationInfo> _locations = new BulkObservableCollection<LocationInfo>();
        private Int32 _currentDistributionNumber;
        private Single _currentDistributionPercentage;
        private Single _newDistributionPercentage;
        private Int32 _totalLocationCount;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ProductInfoProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.ProductInfo);
        public static readonly PropertyPath ProductGtinProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.ProductGtin);
        public static readonly PropertyPath ProductNameProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.ProductName);
        public static readonly PropertyPath LocationsProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.Locations);
        public static readonly PropertyPath CurrentDistributionNumberProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.CurrentDistributionNumber);
        public static readonly PropertyPath CurrentDistributionPercentageProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.CurrentDistributionPercentage);
        public static readonly PropertyPath NewDistributionNumberProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.NewDistributionNumber);
        public static readonly PropertyPath NewDistributionPercentageProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.NewDistributionPercentage);
        public static readonly PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<ProductLocationLinkRowViewModel>(p => p.IsValid);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the product info
        /// </summary>
        public ProductInfo ProductInfo
        {
            get { return _productInfo; }
        }

        /// <summary>
        /// Returns the product gtin
        /// </summary>
        public String ProductGtin
        {
            get { return _productInfo.Gtin; }
        }

        /// <summary>
        /// Returns the product name
        /// </summary>
        public String ProductName
        {
            get { return _productInfo.Name; }
        }

        /// <summary>
        /// Returns the product to ranged locations links
        /// </summary>
        public BulkObservableCollection<LocationInfo> Locations
        {
            get
            {
                return _locations;
            }
        }

        /// <summary>
        /// Returns the current distribution number
        /// </summary>
        public Int32 CurrentDistributionNumber
        {
            get { return _currentDistributionNumber; }
        }

        /// <summary>
        /// Returns the current distribution percentage
        /// </summary>
        public Single CurrentDistributionPercentage
        {
            get { return _currentDistributionPercentage; }
        }

        /// <summary>
        /// Returns the new distribution number
        /// </summary>
        public Int32 NewDistributionNumber
        {
            get { return this.Locations.Count; }
        }

        /// <summary>
        /// Returns the new distribution percentage
        /// </summary>
        public Single NewDistributionPercentage
        {
            get { return _newDistributionPercentage; }
            set
            {
                _newDistributionPercentage = value;
                OnPropertyChanged(NewDistributionPercentageProperty);
            }
        }

        /// <summary>
        /// Returns whether the row is valid
        /// </summary>
        public Boolean IsValid
        {
            get { return _locations.Count > 0; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main constructor
        /// </summary>
        /// <param name="productInfo"></param>
        /// <param name="locationInfoList"></param>
        /// <param name="initialLinkDictionary"></param>
        public ProductLocationLinkRowViewModel(ProductInfo productInfo, IEnumerable<LocationInfo> initialLocations, Int32 totalLocationCount)
        {
            _productInfo = productInfo;
            _locations.AddRange(initialLocations);
            _totalLocationCount = totalLocationCount;

            //Calculate current distribution values
            _currentDistributionNumber = initialLocations.Count();
            _currentDistributionPercentage = AmendDistributionViewModel.CalculateDistributionValue(_currentDistributionNumber, _totalLocationCount);

            //Set new values to match the current
            _newDistributionPercentage = _currentDistributionPercentage;

            //Attach to location bulk collection changed
            this.Locations.BulkCollectionChanged += new EventHandler<BulkCollectionChangedEventArgs>(Locations_BulkCollectionChanged);
        }

        /// <summary>
        /// Locations collection changed event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Locations_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //Update new number
            OnPropertyChanged(NewDistributionNumberProperty);

            //Update new distribution percentage
            this.NewDistributionPercentage = AmendDistributionViewModel.CalculateDistributionValue(this.NewDistributionNumber, _totalLocationCount);

            OnPropertyChanged(IsValidProperty);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.Locations.BulkCollectionChanged -= Locations_BulkCollectionChanged;
                    this._productInfo = null;
                    this._locations.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region IDataErrorInfo


        string IDataErrorInfo.Error
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Implementation of the IDataErrorInfo interface
        /// </summary>
        /// <param name="columnName">Property name</param>
        /// <returns>String containing error</returns>
        string IDataErrorInfo.this[String columnName]
        {
            get
            {
                String result = null;

                if (columnName == NewDistributionNumberProperty.Path)
                {
                    if (this.NewDistributionNumber <= 0)
                        result = Message.AmendDistribution_NewDistributionNumberInvalid;
                }
                else if (columnName == NewDistributionPercentageProperty.Path)
                {
                    if (this.NewDistributionPercentage <= 0)
                        result = Message.AmendDistribution_NewDistributionPercentageInvalid;
                }

                return result; // return result
            }
        }
        #endregion
    }
}