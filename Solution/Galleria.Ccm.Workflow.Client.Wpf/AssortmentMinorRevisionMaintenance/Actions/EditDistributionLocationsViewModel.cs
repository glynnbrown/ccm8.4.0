﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;
using Galleria.Framework.Collections;
using System.Windows;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.AssortmentMinorRevisionMaintenance
{
    public class EditDistributionLocationsViewModel : ViewModelAttachedControlObject<EditDistributionLocationsWindow>
    {
        #region Fields

        private LocationInfoListViewModel _locationInfoListViewModel;
        private AssortmentMinorRevision _minorRevision;
        private Single _currentDistributionPercentage;
        private Single _newDistributionPercentage;
        private ProductInfo _selectedProductInfo;

        //Locations
        private ObservableCollection<DistributionLocationRow> _selectedAvailableLocations = new ObservableCollection<DistributionLocationRow>();
        private BulkObservableCollection<DistributionLocationRow> _availableLocations = new BulkObservableCollection<DistributionLocationRow>();
        private ReadOnlyBulkObservableCollection<DistributionLocationRow> _availableLocationsRO;

        private ObservableCollection<DistributionLocationRow> _selectedRangedLocations = new ObservableCollection<DistributionLocationRow>();
        private BulkObservableCollection<DistributionLocationRow> _rangedLocations = new BulkObservableCollection<DistributionLocationRow>();
        private ReadOnlyBulkObservableCollection<DistributionLocationRow> _rangedLocationsRO;

        #endregion

        #region Property Paths

        public static readonly PropertyPath CurrentDistributionPercentageProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.CurrentDistributionPercentage);
        public static readonly PropertyPath NewDistributionPercentageProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.NewDistributionPercentage);
        public static readonly PropertyPath SelectedAvailableLocationsProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.SelectedAvailableLocations);
        public static readonly PropertyPath SelectedRangedLocationsProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.SelectedRangedLocations);
        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath RangedLocationsProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.RangedLocations);

        public static readonly PropertyPath AddSelectedLocationsCommandProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.AddSelectedLocationsCommand);
        public static readonly PropertyPath RemoveSelectedLocationsCommandProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.RemoveSelectedLocationsCommand);
        public static readonly PropertyPath AddAllLocationsCommandProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.AddAllLocationsCommand);
        public static readonly PropertyPath RemoveAllLocationsCommandProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.RemoveAllLocationsCommand);
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.OkCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<EditDistributionLocationsViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current scenario
        /// </summary>
        public AssortmentMinorRevision MinorRevision
        {
            get
            {
                return _minorRevision;
            }
        }

        /// <summary>
        /// Returns the selected product info
        /// </summary>
        public ProductInfo SelectedProductInfo
        {
            get
            {
                return _selectedProductInfo;
            }
        }

        /// <summary>
        /// Returns the current distribution percentage
        /// </summary>
        public Single CurrentDistributionPercentage
        {
            get { return _currentDistributionPercentage; }
        }

        /// <summary>
        /// Returns the new distribution percentage
        /// </summary>
        public Single NewDistributionPercentage
        {
            get { return _newDistributionPercentage; }
            set
            {
                _newDistributionPercentage = value;
                OnPropertyChanged(NewDistributionPercentageProperty);
            }
        }

        /// <summary>
        /// Returns the collection of selected available Locations
        /// </summary>
        public ObservableCollection<DistributionLocationRow> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
        }

        /// <summary>
        /// Returns the collection of available Locations
        /// </summary>
        public ReadOnlyBulkObservableCollection<DistributionLocationRow> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyBulkObservableCollection<DistributionLocationRow>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected ranged Locations
        /// </summary>
        public ObservableCollection<DistributionLocationRow> SelectedRangedLocations
        {
            get { return _selectedRangedLocations; }
        }

        /// <summary>
        /// Returns the collection of Ranged Locations
        /// </summary>
        public ReadOnlyBulkObservableCollection<DistributionLocationRow> RangedLocations
        {
            get
            {
                if (_rangedLocationsRO == null)
                {
                    _rangedLocationsRO = new ReadOnlyBulkObservableCollection<DistributionLocationRow>(_rangedLocations);
                }
                return _rangedLocationsRO;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        public EditDistributionLocationsViewModel(AssortmentMinorRevision assortmentMinorRevision, ProductLocationLinkRowViewModel selectedLinkRow)
            : this(
            assortmentMinorRevision,
            App.ViewState.EntityId,
            selectedLinkRow)
        { }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public EditDistributionLocationsViewModel(AssortmentMinorRevision assortmentMinorRevision,
            Int32 entityId,
            ProductLocationLinkRowViewModel selectedLinkRow)
        {
            //Set fields
            this._minorRevision = assortmentMinorRevision;
            this._currentDistributionPercentage = selectedLinkRow.NewDistributionPercentage;
            this._newDistributionPercentage = selectedLinkRow.NewDistributionPercentage;
            this._selectedProductInfo = selectedLinkRow.ProductInfo;
            //Set location list
            _locationInfoListViewModel = new LocationInfoListViewModel();
            _locationInfoListViewModel.FetchAllForEntity();

            //Setup available/ranged locations
            foreach (LocationInfo location in selectedLinkRow.Locations)
            {
                this._rangedLocations.Add(new DistributionLocationRow(location));
            }

            //Get all ranged location ids
            List<Int16> rangedLocationIds = this.RangedLocations.Select(p => p.Location.Id).ToList();

            //Enumerate through master list of locations
            foreach (LocationInfo location in _locationInfoListViewModel.Model)
            {
                if (!rangedLocationIds.Contains(location.Id))
                {
                    this._availableLocations.Add(new DistributionLocationRow(location));
                }
            }

            //Attach to ranged locations collection changed event
            this.RangedLocations.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(RangedLocations_BulkCollectionChanged);
        }

        /// <summary>
        /// Handler for the ranged locations bulk collection changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RangedLocations_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //Update new distribution value
            UpdateNewDistributionValue();
        }

        #endregion

        #region Commands

        #region AddSelectedLocationsCommand

        private RelayCommand _addSelectedLocationsCommand;

        public RelayCommand AddSelectedLocationsCommand
        {
            get
            {
                if (_addSelectedLocationsCommand == null)
                {
                    _addSelectedLocationsCommand = new RelayCommand(
                        p => AddSelectedLocations_Executed(),
                        p => AddSelectedLocations_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_AddSelectedLocations
                    };
                    base.ViewModelCommands.Add(_addSelectedLocationsCommand);
                }
                return _addSelectedLocationsCommand;
            }
        }

        private Boolean AddSelectedLocations_CanExecute()
        {
            //must have some available locations selected
            return (this.SelectedAvailableLocations.Count > 0);
        }

        private void AddSelectedLocations_Executed()
        {
            //Get list to add
            List<DistributionLocationRow> addList = this.SelectedAvailableLocations.ToList();

            //get current idds
            IEnumerable<Int16> currentLocationIds = this.RangedLocations.Select(p => p.Location.Id);

            //add all location not already taken.
            List<DistributionLocationRow> addToRangedLocations = new List<DistributionLocationRow>();
            foreach (DistributionLocationRow locationRow in addList)
            {
                if (!currentLocationIds.Contains(locationRow.Location.Id))
                {
                    addToRangedLocations.Add(locationRow);
                }
            }

            //for each location in the list of selected available products
            foreach (DistributionLocationRow locationInfo in addToRangedLocations)
            {
                //add the location to the list of assigned products
                _rangedLocations.Add(locationInfo);
            }

            //clear the selected available products
            this.SelectedAvailableLocations.Clear();

            //remove from the available
            this._availableLocations.RemoveRange(addList);
        }

        #endregion

        #region RemoveSelectedLocationsCommand

        private RelayCommand _removeSelectedLocationsCommand;

        public RelayCommand RemoveSelectedLocationsCommand
        {
            get
            {
                if (_removeSelectedLocationsCommand == null)
                {
                    _removeSelectedLocationsCommand = new RelayCommand(
                        p => RemoveSelectedLocations_Executed(),
                        p => RemoveSelectedLocations_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_RemoveSelectedLocations
                    };
                    base.ViewModelCommands.Add(_removeSelectedLocationsCommand);
                }
                return _removeSelectedLocationsCommand;
            }
        }

        private Boolean RemoveSelectedLocations_CanExecute()
        {
            if (this.SelectedRangedLocations.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveSelectedLocations_Executed()
        {
            //Get list to remove
            List<DistributionLocationRow> addList = this.SelectedRangedLocations.ToList();

            //get current ids
            IEnumerable<Int16> currentLocationIds = this.AvailableLocations.Select(p => p.Location.Id);

            //add all selected Locations not already taken.
            List<DistributionLocationRow> addToAvailableLocations = new List<DistributionLocationRow>();
            foreach (DistributionLocationRow locationRow in addList)
            {
                if (!currentLocationIds.Contains(locationRow.Location.Id))
                {
                    addToAvailableLocations.Add(locationRow);
                }
            }

            //for each product in the list of selected available Locations
            foreach (DistributionLocationRow scenarioLocationRow in addToAvailableLocations)
            {
                //add the product to the list of unassigned Locations
                _availableLocations.Add(scenarioLocationRow);
            }

            //clear the selected assigned Locations
            this.SelectedRangedLocations.Clear();

            //remove from the assigned
            this._rangedLocations.RemoveRange(addList);
        }

        #endregion

        #region AddAllLocationsCommand

        private RelayCommand _addAllLocationsCommand;

        public RelayCommand AddAllLocationsCommand
        {
            get
            {
                if (_addAllLocationsCommand == null)
                {
                    _addAllLocationsCommand = new RelayCommand(
                        p => AddAllLocations_Executed(),
                        p => AddAllLocations_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_AddAllLocations
                    };
                    base.ViewModelCommands.Add(_addAllLocationsCommand);
                }
                return _addAllLocationsCommand;
            }
        }

        private Boolean AddAllLocations_CanExecute()
        {
            if (this.AvailableLocations.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddAllLocations_Executed()
        {
            //Get list to add
            List<DistributionLocationRow> addList = this.AvailableLocations.ToList();

            //get current idds
            IEnumerable<Int16> currentLocationIds = this.RangedLocations.Select(p => p.Location.Id);

            //add all selected Locations not already taken.
            List<DistributionLocationRow> addToRangedLocations = new List<DistributionLocationRow>();
            foreach (DistributionLocationRow scenarioLocationRow in addList)
            {
                if (!currentLocationIds.Contains(scenarioLocationRow.Location.Id))
                {
                    addToRangedLocations.Add(scenarioLocationRow);
                }
            }

            //for each product in the list of selected available Locations
            foreach (DistributionLocationRow scenarioLocationRow in addToRangedLocations)
            {
                //add the location to the list of ranged Locations
                _rangedLocations.Add(scenarioLocationRow);
            }

            //clear the selected available Locations
            this.SelectedAvailableLocations.Clear();

            //remove from the available
            this._availableLocations.RemoveRange(addList);
        }

        #endregion

        #region RemoveAllLocationsCommand

        private RelayCommand _removeAllLocationsCommand;

        public RelayCommand RemoveAllLocationsCommand
        {
            get
            {
                if (_removeAllLocationsCommand == null)
                {
                    _removeAllLocationsCommand = new RelayCommand(
                        p => RemoveAllLocations_Executed(),
                        p => RemoveAllLocations_CanExecute())
                    {
                        Icon = ImageResources.MinorRevision_RemoveAllLocations
                    };
                    base.ViewModelCommands.Add(_removeAllLocationsCommand);
                }
                return _removeAllLocationsCommand;
            }
        }

        private Boolean RemoveAllLocations_CanExecute()
        {
            if (this.RangedLocations.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void RemoveAllLocations_Executed()
        {
            //Get list to add
            List<DistributionLocationRow> addList = this.RangedLocations.ToList();

            //get current idds
            IEnumerable<Int16> currentLocationIds = this.AvailableLocations.Select(p => p.Location.Id);

            //add all selected Locations not already taken.
            List<DistributionLocationRow> addToAvailableLocations = new List<DistributionLocationRow>();
            foreach (DistributionLocationRow scenarioLocationRow in addList)
            {
                if (!currentLocationIds.Contains(scenarioLocationRow.Location.Id))
                {
                    addToAvailableLocations.Add(scenarioLocationRow);
                }
            }

            //for each product in the list of selected ranged Locations
            foreach (DistributionLocationRow scenarioLocationRow in addToAvailableLocations)
            {
                //add the location to the list of available Locations
                _availableLocations.Add(scenarioLocationRow);
            }

            //clear the selected ranged Locations
            this.SelectedRangedLocations.Clear();

            //remove from the assigned
            this._rangedLocations.RemoveRange(addList);
        }

        #endregion

        #region Ok

        private RelayCommand _okCommand;

        /// <summary>
        /// Advances the process to the next step
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => Ok_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean OK_CanExecute()
        {
            if (this.RangedLocations.Count <= 0)
            {
                //New distribution percentage must be between 1 and 100.
                this.OkCommand.DisabledReason = Message.EditDistributionLocations_OkCommandDisabledReason;

                return false;
            }
            return true;
        }

        private void Ok_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.AttachedControl != null)
            {
                //Close window by returning true
                this.AttachedControl.DialogResult = true;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancel the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            base.ShowWaitCursor(true);

            if (this.AttachedControl != null)
            {
                //Close window by returning true
                this.AttachedControl.DialogResult = false;
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Event Handlers

        #endregion

        #region Methods

        /// <summary>
        /// Method to update the new distribution value
        /// </summary>
        private void UpdateNewDistributionValue()
        {
            //Get new distribution percentage
            this.NewDistributionPercentage = AmendDistributionViewModel.CalculateDistributionValue(this.RangedLocations.Count, _locationInfoListViewModel.Model.Count);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this._selectedAvailableLocations.Clear();
                    this._selectedRangedLocations.Clear();
                    this.RangedLocations.BulkCollectionChanged -= RangedLocations_BulkCollectionChanged;
                    this._availableLocations.Clear();
                    this._rangedLocations.Clear();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }

    /// <summary>
    /// Distribution Location Row
    /// </summary>
    public class DistributionLocationRow
    {
        #region Fields

        private LocationInfo _location;
        private Int32? _rank;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath LocationProperty = WpfHelper.GetPropertyPath<DistributionLocationRow>(p => p.Location);
        public static readonly PropertyPath RankProperty = WpfHelper.GetPropertyPath<DistributionLocationRow>(p => p.Rank);
        public static readonly PropertyPath LocationCodeProperty = WpfHelper.GetPropertyPath<DistributionLocationRow>(p => p.LocationCode);
        public static readonly PropertyPath LocationNameProperty = WpfHelper.GetPropertyPath<DistributionLocationRow>(p => p.LocationName);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the location
        /// </summary>
        public LocationInfo Location
        {
            get { return _location; }
        }

        /// <summary>
        /// Gets the scenario location code
        /// </summary>
        public String LocationCode
        {
            get { return _location.Code; }
        }

        /// <summary>
        /// Gets the scenario location name
        /// </summary>
        public String LocationName
        {
            get { return _location.Name; }
        }

        /// <summary>
        /// Returns whether the product is valid for a new action
        /// </summary>
        public Int32? Rank
        {
            get { return _rank; }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public DistributionLocationRow(LocationInfo location)
        {
            //Set the properties
            _location = location;
        }

        #endregion
    }
}

