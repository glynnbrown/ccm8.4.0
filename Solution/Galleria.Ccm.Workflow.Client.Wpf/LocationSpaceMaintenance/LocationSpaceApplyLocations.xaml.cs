﻿using Galleria.Ccm.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpaceApplyLocations.xaml
    /// </summary>
    public partial class LocationSpaceApplyLocations : UserControl
    {
        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationSpaceApplyLocationsViewModel),
            typeof(LocationSpaceApplyLocations),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets the window viewmodel context
        /// </summary>
        public LocationSpaceApplyLocationsViewModel ViewModel
        {
            get { return (LocationSpaceApplyLocationsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceApplyLocations senderControl = (LocationSpaceApplyLocations)obj;

            if (e.OldValue != null)
            {
                LocationSpaceApplyLocationsViewModel oldModel = (LocationSpaceApplyLocationsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationSpaceApplyLocationsViewModel newModel = (LocationSpaceApplyLocationsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assortmentModel"></param>
        public LocationSpaceApplyLocations(ObservableCollection<LocationSpaceProductGroupRowViewModel> locationSpaceCategory, LocationList masterLocationList, ObservableCollection<Location> selectedLocations)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = new LocationSpaceApplyLocationsViewModel(locationSpaceCategory, masterLocationList, selectedLocations);

            this.Loaded += LocationSpaceAddLocations_Loaded;

        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationSpaceAddLocations_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationSpaceAddLocations_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion
    }
}
