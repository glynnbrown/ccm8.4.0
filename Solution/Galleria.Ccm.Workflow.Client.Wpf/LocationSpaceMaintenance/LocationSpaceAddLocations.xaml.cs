﻿using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Controls.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpaceAddLocations.xaml
    /// </summary>
    public partial class LocationSpaceAddLocations : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationSpaceAddLocationsViewModel),
            typeof(LocationSpaceAddLocations),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets the window viewmodel context
        /// </summary>
        public LocationSpaceAddLocationsViewModel ViewModel
        {
            get { return (LocationSpaceAddLocationsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceAddLocations senderControl = (LocationSpaceAddLocations)obj;

            if (e.OldValue != null)
            {
                LocationSpaceAddLocationsViewModel oldModel = (LocationSpaceAddLocationsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                LocationSpaceAddLocationsViewModel newModel = (LocationSpaceAddLocationsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assortmentModel"></param>
        public LocationSpaceAddLocations(ObservableCollection<LocationSpaceProductGroupRowViewModel> locationSpaceCategory, LocationList masterLocationList, ObservableCollection<Location> selectedLocations)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = new LocationSpaceAddLocationsViewModel(locationSpaceCategory, masterLocationList, selectedLocations);
            this.Loaded += LocationSpaceAddLocations_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationSpaceAddLocations_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationSpaceAddLocations_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Int32[] initialVisibleMapIds = new Int32[]
            {
                LocationImportMappingList.CodeMapId,
                LocationImportMappingList.NameMapId,
                LocationImportMappingList.Address1MapId,
                LocationImportMappingList.Address2MapId,
                LocationImportMappingList.RegionMapId,
                LocationImportMappingList.CountyMapId,
                LocationImportMappingList.PostalCodeMapId,
                LocationImportMappingList.CityMapId,
                LocationImportMappingList.CountyMapId
            };

            //nb - we are removing the location group code and type name cols
            // as we are only using the location object as the source for this.
            Dictionary<Int32, String> specialCases = new Dictionary<Int32, String>();
            specialCases.Add(LocationImportMappingList.GroupCodeMapId, null);
          //  specialCases.Add(LocationImportMappingList.LocationTypeMapId, null);

            //load the unassigned locs list
            DataGridColumnCollection unassignedLocationsColumnSet = new DataGridColumnCollection();
            List<DataGridColumn> unassignedLocationsCols = DataObjectViewHelper.GetBoundLocationInfoColumnSet();
            foreach (DataGridColumn col in unassignedLocationsCols)
            {
                unassignedLocationsColumnSet.Add(col);
            }

            AllLocationsGrid.ColumnSet = unassignedLocationsColumnSet;

            //load the assigned locs list
            DataGridColumnCollection assignedLocationsColumnSet = new DataGridColumnCollection();
            List<DataGridColumn> assignedLocationsCols = DataObjectViewHelper.GetBoundLocationInfoColumnSet();
            foreach (DataGridColumn col in assignedLocationsCols)
            {
                assignedLocationsColumnSet.Add(col);
            }

            AddedLocationsGrid.ColumnSet = assignedLocationsColumnSet;
        }

        private void AllLocationsGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            this.ViewModel.AddLocationsCommand.Execute();
        }

        private void AllLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.AddedLocationsGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveLocationCommand.Execute();
                }
            }
        }

        private void AddedLocationsGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.AllLocationsGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.AddLocationsCommand.Execute();
                }
            }
        }

        #endregion
    }
}
