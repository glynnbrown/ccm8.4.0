﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    public class LocationSpaceApplyLocationsViewModel : ViewModelAttachedControlObject<LocationSpaceApplyLocations>, IWizardStepViewModel
    {
        #region Constants
        private const String _exceptionCategory = "LocationSpaceSetup";
        private const Int32 MaxRetryCount = 3;
        private const Int32 MaxBackstageItemsFetch = 1001;
        #endregion

        #region Fields
        public BulkObservableCollection<StoreFixtureRelationship> _storeFixtureCreateAsNew = new BulkObservableCollection<StoreFixtureRelationship>();
        public BulkObservableCollection<StoreFixtureRelationship> _storeFixtureOverwrite = new BulkObservableCollection<StoreFixtureRelationship>();
        private LocationList _masterLocationList;

        private int _categoryCount;
        private int _storeFixtureCreateAsNewCount;
        private int _storeFixtureOverwriteCount;

        private BulkObservableCollection<LocationSpace> _allLocationSpaceToBeCreatedAsNew;
        private BulkObservableCollection<LocationSpace> _allLocationSpaceToBeOverwritten;

        private ObservableCollection<Location> _addedLocations = new ObservableCollection<Location>();
        private ObservableCollection<LocationSpaceProductGroupRowViewModel> _selectedLocationSpaceCategory = new ObservableCollection<LocationSpaceProductGroupRowViewModel>();

        #endregion

        #region Binding properties

        //properties
        public static readonly PropertyPath SelectedLocationSpaceCategoryProperty = WpfHelper.GetPropertyPath<LocationSpaceApplyLocationsViewModel>(p => p.SelectedLocationSpaceCategory);
        public static readonly PropertyPath CategoryCountProperty = WpfHelper.GetPropertyPath<LocationSpaceApplyLocationsViewModel>(p => p.CategoryCount);
        public static readonly PropertyPath StoreFixtureOverwriteProperty = WpfHelper.GetPropertyPath<LocationSpaceApplyLocationsViewModel>(p => p.StoreFixtureOverwrite);
        public static readonly PropertyPath StoreFixtureCreateAsNewProperty = WpfHelper.GetPropertyPath<LocationSpaceApplyLocationsViewModel>(p => p.StoreFixtureCreateAsNew);
        public static readonly PropertyPath StoreFixtureCreateAsNewCountProperty = WpfHelper.GetPropertyPath<LocationSpaceApplyLocationsViewModel>(p => p.StoreFixtureCreateAsNewCount);
        public static readonly PropertyPath StoreFixtureOverwriteCountProperty = WpfHelper.GetPropertyPath<LocationSpaceApplyLocationsViewModel>(p => p.StoreFixtureOverwriteCount);
        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return "Apply Locations"; }
        }

        public String StepDescription
        {
            get { return "Apply categories to other locations"; }
        }

        /// <summary>
        /// Carries out actions to finalize 
        /// the selections made in this step
        /// </summary>
        public void CompleteStep()
        {
            ApplyStoreFixtureRelationships();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Get/Sets the category count
        /// </summary>
        public Int32 CategoryCount
        {
            get { return _categoryCount; }
            set
            {
                _categoryCount = value;
                OnPropertyChanged(CategoryCountProperty);
            }
        }

        /// <summary>
        /// Get/Sets the StoreFixtureCreateAsNewCount count
        /// </summary>
        public Int32 StoreFixtureCreateAsNewCount
        {
            get { return _storeFixtureCreateAsNewCount; }
            set
            {
                _storeFixtureCreateAsNewCount = value;
                OnPropertyChanged(CategoryCountProperty);
            }
        }

        public BulkObservableCollection<LocationSpace> AllLocationSpaceToBeCreatedAsNew
        {
            get { return _allLocationSpaceToBeCreatedAsNew; }
            set
            {
                _allLocationSpaceToBeCreatedAsNew = value;
            }
        }

        /// <summary>
        /// Property for the store fixture create as new
        /// </summary>
        public BulkObservableCollection<StoreFixtureRelationship> StoreFixtureCreateAsNew
        {
            get { return _storeFixtureCreateAsNew; }
            private set
            {
                _storeFixtureCreateAsNew = value;
                OnPropertyChanged(StoreFixtureCreateAsNewProperty);
            }
        }

        /// <summary>
        /// Get/Sets the StoreFixtureCreateAsNewCount count
        /// </summary>
        public Int32 StoreFixtureOverwriteCount
        {
            get { return _storeFixtureOverwriteCount; }
            set
            {
                _storeFixtureOverwriteCount = value;
                OnPropertyChanged(CategoryCountProperty);
            }
        }

        public BulkObservableCollection<LocationSpace> AllLocationSpaceToBeOverwritten
        {
            get { return _allLocationSpaceToBeOverwritten; }
            set
            {
                _allLocationSpaceToBeOverwritten = value;
            }
        }

        /// <summary>
        /// Property for the store fixture overwrite
        /// </summary>
        public BulkObservableCollection<StoreFixtureRelationship> StoreFixtureOverwrite
        {
            get { return _storeFixtureOverwrite; }
            private set
            {
                _storeFixtureOverwrite = value;
                OnPropertyChanged(StoreFixtureOverwriteProperty);
            }
        }

        public LocationSpaceInfoList AllLocationSpaceInfo
        {
            get { return LocationSpaceInfoList.FetchByEntityId(App.ViewState.EntityId); }

        }

        /// <summary>
        /// Property for the selected category
        /// </summary>
        public ObservableCollection<LocationSpaceProductGroupRowViewModel> SelectedLocationSpaceCategory
        {
            get { return _selectedLocationSpaceCategory; }
            private set
            {
                _selectedLocationSpaceCategory = value;
                OnPropertyChanged(SelectedLocationSpaceCategoryProperty);
            }
        }

        public LocationList MasterLocationList
        {
            get { return _masterLocationList; }
            private set { _masterLocationList = value; }
        }

        /// <summary>
        /// Property for the selected locations
        /// </summary>
        public ObservableCollection<Location> AddedLocations
        {
            get { return _addedLocations; }
            set
            {
                _addedLocations = value;
                OnPropertyChanged(SelectedLocationSpaceCategoryProperty);
            }
        }

        /// <summary>
        /// Returns true if this step is valid
        /// and ready to proceed
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get
            {
                return true;
            }
        }

        public String StepDisabledReason
        {
            get { return "Step not completed"; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="assortment"></param>
        public LocationSpaceApplyLocationsViewModel(ObservableCollection<LocationSpaceProductGroupRowViewModel> locationSpaceCategory, LocationList masterLocationList, ObservableCollection<Location> selectedLocations)
        {
            //locations
            _masterLocationList = masterLocationList;

            if (_masterLocationList == null)
            {
                _masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);
            }

            //locationspace
            this.SelectedLocationSpaceCategory = locationSpaceCategory;

            _addedLocations = selectedLocations;

            _categoryCount = this.SelectedLocationSpaceCategory.Count;

            this.CreateStoreFixtureRelationships();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Iterate through each category and check if it exists in any the addedLocations. If yes it will be overwritten and if not it will be created as new.
        /// </summary>
        public void CreateStoreFixtureRelationships()
        {
            _storeFixtureCreateAsNew = new BulkObservableCollection<StoreFixtureRelationship>();
            _storeFixtureOverwrite = new BulkObservableCollection<StoreFixtureRelationship>();

            _allLocationSpaceToBeCreatedAsNew = new BulkObservableCollection<LocationSpace>();
            _allLocationSpaceToBeOverwritten = new BulkObservableCollection<LocationSpace>();

            foreach (LocationSpaceProductGroupRowViewModel category in SelectedLocationSpaceCategory)
            {
                foreach (Location location in AddedLocations)
                {
                    StoreFixtureRelationship storeFixtureRelationship = new StoreFixtureRelationship();

                    LocationSpaceInfo lsi = AllLocationSpaceInfo.Where(w => w.LocationId == location.Id).FirstOrDefault();

                    LocationSpace currentLocationSpace;

                    if (lsi != null)
                    {
                        currentLocationSpace = LocationSpace.GetLocationSpaceById(lsi.Id);

                        Boolean locationSpaceExist = false;

                        if (currentLocationSpace == null)
                        {
                            storeFixtureRelationship.CreateAsNew = true;

                            if (!_allLocationSpaceToBeCreatedAsNew.Any(p => p.LocationId == currentLocationSpace.LocationId))
                            {
                                _allLocationSpaceToBeCreatedAsNew.Add(currentLocationSpace);
                            }
                        }
                        else
                        {
                            foreach (var item in currentLocationSpace.LocationSpaceProductGroups.Where(w => w.ProductGroupId == category.LocationSpaceProductGroup.ProductGroupId))
                            {
                                storeFixtureRelationship.Overwrite = true;
                                storeFixtureRelationship.OldBayCount = item.BayCount;
                                storeFixtureRelationship.OldTotalWidth = item.Bays.Sum(s => s.Width);
                                locationSpaceExist = true;

                                if (!_allLocationSpaceToBeOverwritten.Any(p => p.LocationId == currentLocationSpace.LocationId))
                                {
                                    _allLocationSpaceToBeOverwritten.Add(currentLocationSpace);
                                }
                            }

                            if (locationSpaceExist == false)
                            {
                                storeFixtureRelationship.CreateAsNew = true;

                                if (!_allLocationSpaceToBeCreatedAsNew.Any(p => p.LocationId == currentLocationSpace.LocationId))
                                {
                                    _allLocationSpaceToBeCreatedAsNew.Add(currentLocationSpace);
                                }
                            }
                        }
                    }

                    storeFixtureRelationship.Category = category;
                    storeFixtureRelationship.Location = location;
                    storeFixtureRelationship.NewBayCount = category.BayCount;
                    storeFixtureRelationship.NewTotalWidth = category.LocationSpaceProductGroup.Bays.Sum(s => s.Width);
                    string[] parts = category.FriendlyName.Split(':');
                    storeFixtureRelationship.CategoryCode = parts[0];
                    storeFixtureRelationship.CategoryName = parts[1];

                    if (storeFixtureRelationship.CreateAsNew)
                    {
                        _storeFixtureCreateAsNew.Add(storeFixtureRelationship);
                    }

                    if (storeFixtureRelationship.Overwrite)
                    {
                        _storeFixtureOverwrite.Add(storeFixtureRelationship);
                    }
                }

            }
            _storeFixtureOverwriteCount = _storeFixtureOverwrite.Count();
            _storeFixtureCreateAsNewCount = _storeFixtureCreateAsNew.Count();

            
        }

        /// <summary>
        /// Iterate through each locationSpace to be created as new and apply the correct category to the locationSpace productGroups then save as new version.
        /// Iterate through each locationSpace to be overwritten and remove the category and add the new one to the locationSpace productGroups then save as same version.
        /// </summary>
        public void ApplyStoreFixtureRelationships()
        {
            foreach (LocationSpace lp in AllLocationSpaceToBeCreatedAsNew)
            {
                ApplyNewChanges(lp);
            }

            foreach (LocationSpace lp in AllLocationSpaceToBeOverwritten)
            {
                LocationSpace ln = AllLocationSpaceToBeCreatedAsNew.Where(w => w.LocationId == lp.LocationId).FirstOrDefault();
                
                if (ln != null)
                {
                    LocationSpace newlp = LocationSpace.GetLocationSpaceById(lp.Id);
                    ApplyOverwriteChanges(newlp);
                }
                else
                {
                    ApplyOverwriteChanges(lp);
                }
            }
        }


        public void ApplyNewChanges(LocationSpace lp)
        {
            foreach (LocationSpaceProductGroupRowViewModel category in _selectedLocationSpaceCategory)
            {
                LocationSpaceProductGroup ItemExist = null;

                foreach (Location location in _addedLocations)
                {

                    if (location.Id == lp.LocationId)
                    {
                        ItemExist = lp.LocationSpaceProductGroups.Where(x => x.ProductGroupId == category.LocationSpaceProductGroup.ProductGroupId).FirstOrDefault();

                        if (ItemExist == null)
                        {
                            lp.LocationSpaceProductGroups.Add(category.LocationSpaceProductGroup.Copy());
                        }
                        if (lp.LocationSpaceProductGroups.Count == 0)
                        {
                            lp.LocationSpaceProductGroups.Add(category.LocationSpaceProductGroup.Copy());
                        }

                    }
                }
            }

            lp.Save();
        }

        public void ApplyOverwriteChanges(LocationSpace lp)
        {
            foreach (LocationSpaceProductGroupRowViewModel category in _selectedLocationSpaceCategory)
            {
                LocationSpaceProductGroup itemToRemove = null;

                foreach (Location location in _addedLocations)
                {
                    if (location.Id == lp.LocationId)
                    {
                        itemToRemove = lp.LocationSpaceProductGroups.Where(x => x.ProductGroupId == category.LocationSpaceProductGroup.ProductGroupId).FirstOrDefault();

                        if (itemToRemove != null)
                        {
                            lp.LocationSpaceProductGroups.Remove(itemToRemove);
                            lp.LocationSpaceProductGroups.Add(category.LocationSpaceProductGroup.Copy());
                        }

                    }

                }
            }
            lp.Save();
        }
        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterLocationList = null;
                    _addedLocations.Clear();
                    _selectedLocationSpaceCategory = null;

                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
    #region helper class
    public class StoreFixtureRelationship
    {
        #region Properties

        public LocationSpaceProductGroupRowViewModel Category { get; set; }
        public Location Location { get; set; }
        public Double NewTotalWidth { get; set; }
        public Double NewBayCount { get; set; }
        public Double OldTotalWidth { get; set; }
        public Double OldBayCount { get; set; }
        public Boolean CreateAsNew { get; set; }
        public Boolean Overwrite { get; set; }
        public String CategoryCode { get; set; }
        public String CategoryName { get; set; }

        #endregion

    }
    #endregion
}
