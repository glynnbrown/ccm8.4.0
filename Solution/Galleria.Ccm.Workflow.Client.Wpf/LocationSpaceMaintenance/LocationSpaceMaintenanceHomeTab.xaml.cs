﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpacesHomeTab.xaml
    /// </summary>
    public partial class LocationSpaceMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationSpaceMaintenanceViewModel), typeof(LocationSpaceMaintenanceHomeTab), new PropertyMetadata(null));

        public LocationSpaceMaintenanceViewModel ViewModel
        {
            get { return (LocationSpaceMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public LocationSpaceMaintenanceHomeTab()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(LocationSpaceHomeTab_Loaded);
        }

        void LocationSpaceHomeTab_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationSpaceHomeTab_Loaded;
            LoadGroupByDropDown();
        }

        #endregion

        #region Methods

        private void LoadGroupByDropDown()
        {
            RelayCommand<String> groupByCommand = (this.DataContext != null) ? ((LocationSpaceMaintenanceViewModel)this.DataContext).GroupByCommand : null;

            //Create None Item
            Fluent.MenuItem noneItem = new Fluent.MenuItem()
            {
                Header = EnumHelpers.LocationSpaceGroupByTypeHelper.LocationSpaceGroupByTypeFriendlyNames[LocationSpaceGroupByType.None],
                Command = groupByCommand,
                CommandParameter = LocationSpaceGroupByType.None.ToString(),
                CanAddToQuickAccessToolBar = false
            };
            KeyTip.SetKeys(noneItem, KeyTips.LocationSpaceHomeTab_noneItem);
            KeyTip.SetAutoPlacement(noneItem, false);
            KeyTip.SetHorizontalAlignment(noneItem, HorizontalAlignment.Left);
            drpdwnGroupByValues.Items.Add(noneItem);

            #region Hierarchy Menu
            //Create Menu Group
            Fluent.MenuItem hierarchy = new Fluent.MenuItem()
            {
                Header = EnumHelpers.LocationSpaceGroupByTypeHelper.LocationSpaceGroupByTypeFriendlyNames[LocationSpaceGroupByType.Hierarchy],
                Command = groupByCommand,
                CanAddToQuickAccessToolBar = false
            };
            KeyTip.SetKeys(hierarchy, KeyTips.LocationSpaceHomeTab_hierarchy);
            KeyTip.SetAutoPlacement(hierarchy, false);
            KeyTip.SetHorizontalAlignment(hierarchy, HorizontalAlignment.Left);

            //Get hierarchy
            List<ProductLevel> levels = this.ViewModel.MasterProductHierarchyView.Model.EnumerateAllLevels().Where(p => p.IsRoot == false).ToList();
            if (levels.Count > 0)
            {
                //Remove lowest level
                levels.RemoveAt(levels.Count - 1);
            }

            //Add items to group
            foreach (ProductLevel level in levels)
            {
                hierarchy.Items.Add(new Fluent.MenuItem()
                {
                    Header = level.Name,
                    Command = groupByCommand,
                    CommandParameter = level.Name,
                    CanAddToQuickAccessToolBar = false
                    
                });

                hierarchy.CommandParameter = level.Name;
            }
            Int32 fluentKeyNum = 0;
            foreach (Fluent.MenuItem item in hierarchy.Items)
            {
                KeyTip.SetKeys(item, fluentKeyNum.ToString());
                KeyTip.SetAutoPlacement(item, false);
                KeyTip.SetHorizontalAlignment(item, HorizontalAlignment.Left);
                fluentKeyNum++;
            }
            
            //Add menu group to drop down button
            if (hierarchy.Items.Count > 0)
            {
               drpdwnGroupByValues.Items.Add
                (
                    hierarchy
                );
            }
            #endregion
            
            //bind the button isenabled state to the isenabled state of the first item
            BindingOperations.SetBinding(drpdwnGroupByValues, Fluent.DropDownButton.IsEnabledProperty, new Binding("Items[0].IsEnabled"));
        }

        #endregion
    }
}
