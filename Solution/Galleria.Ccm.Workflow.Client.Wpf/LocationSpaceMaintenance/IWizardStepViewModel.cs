﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    public interface IWizardStepViewModel
    {
        /// <summary>
        /// Returns the step header text
        /// </summary>
        String StepHeader { get; }

        /// <summary>
        /// Returns the step description text
        /// </summary>
        String StepDescription { get; }

        /// <summary>
        /// Returns true if the step is valid and 
        /// complete and we can move on.
        /// </summary>
        Boolean IsValidAndComplete { get; }

        /// <summary>
        /// Returns the disabled reason for the current step (Default is blank)
        /// </summary>
        /// <remarks>Overwrite where needed</remarks>
        String StepDisabledReason { get; }

        /// <summary>
        /// Carries out actions to complete this step.
        /// </summary>
        void CompleteStep();
    }
}
