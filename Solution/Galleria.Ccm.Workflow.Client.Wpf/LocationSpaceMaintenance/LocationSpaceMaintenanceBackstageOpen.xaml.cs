﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;


namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpaceMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class LocationSpaceMaintenanceBackstageOpen : UserControl
    {
        #region Fields
        private ObservableCollection<LocationSpaceInfo> _searchResults = new ObservableCollection<LocationSpaceInfo>();
        #endregion

        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationSpaceMaintenanceViewModel), typeof(LocationSpaceMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationSpaceMaintenanceViewModel ViewModel
        {
            get { return (LocationSpaceMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceMaintenanceBackstageOpen senderControl = (LocationSpaceMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                LocationSpaceMaintenanceViewModel oldModel = (LocationSpaceMaintenanceViewModel)e.OldValue;
                oldModel.AvailableLocationSpaceInfos.BulkCollectionChanged -= senderControl.ViewModel_AvailableLocationSpacesBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                LocationSpaceMaintenanceViewModel newModel = (LocationSpaceMaintenanceViewModel)e.NewValue;
                newModel.AvailableLocationSpaceInfos.BulkCollectionChanged += senderControl.ViewModel_AvailableLocationSpacesBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(LocationSpaceMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceMaintenanceBackstageOpen senderControl = (LocationSpaceMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyObservableCollection<LocationSpaceInfo>),
            typeof(LocationSpaceMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyObservableCollection<LocationSpaceInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<LocationSpaceInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationSpaceMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<LocationSpaceInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                IEnumerable<LocationSpaceInfo> results = this.ViewModel.GetMatchingLocationSpaces(this.SearchText);

                foreach (LocationSpaceInfo locInfo in results.OrderBy(l => l.ToString()))
                {
                    _searchResults.Add(locInfo);
                }
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Updates the search results when the available locations list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableLocationSpacesBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// Opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox senderControl = (ListBox)sender;
            ListBoxItem clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (clickedItem != null)
            {
                LocationSpaceInfo itemToOpen = (LocationSpaceInfo)senderControl.SelectedItem;

                if (itemToOpen != null)
                {
                    //send the location space to be opened
                    this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                }
            }
        }

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var itemToOpen = searchResultsListBox.SelectedItem as LocationSpaceInfo;
                if(itemToOpen != null)
                    this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
            }
        }
        #endregion
    }
}
