﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
// V8-28269 : A.Kuszyk
//  Optimised CreateLocationSpaceObjects method (renamed to CreateLocationSpaceRow).
// V8-28482 : N.Haywood
//  Changed CreateLocationSpaceRow to actually add to the display list
// V8-28444 : M.Shelley
//  Removed above change to CreateLocationSpaceRow as it broke the unit tests due to inconsistent usage 
//  (that created a duplicate entry) between 2 different calls to the method. The two calls now 
//  use CreateLocationSpaceRow to create the item  and this is then added separately.
#endregion
#region Version History: (CCM 8.1.1)
// CCM:30325 : I.George
// Added shortcut to Save command and friendly description to SaveAs and Delete commands
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.Selectors;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Common.Wpf.Selectors;
using System.Timers;
using Galleria.Ccm.Common.Wpf;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    public sealed class LocationSpaceMaintenanceViewModel : ViewModelAttachedControlObject<LocationSpaceMaintenanceOrganiser>
    {
        #region Constants
        private const String _exceptionCategory = "LocationSpaceSetup";
        private const Int32 MaxRetryCount = 3;
        private const Int32 MaxBackstageItemsFetch = 1001;
        #endregion


        #region Fields

        const String _exCategory = "LocationSpaceSetup";
        private LocationList _masterLocationList;

        private Boolean _userHasLocationSpaceCreatePerm;
        private Boolean _userHasLocationSpaceFetchPerm;
        private Boolean _userHasLocationSpaceEditPerm;
        private Boolean _userHasLocationSpaceDeletePerm;

        private readonly LocationSpaceInfoListViewModel _masterLocationSpaceInfoListView = new LocationSpaceInfoListViewModel();
        private BulkObservableCollection<LocationSpaceInfo> _availableLocationSpaceInfos = new BulkObservableCollection<LocationSpaceInfo>();
        private ReadOnlyBulkObservableCollection<LocationSpaceInfo> _availableLocationSpaceInfosRO;

        private readonly ProductHierarchyViewModel _masterProductHierarchyView = new ProductHierarchyViewModel();
        private readonly LocationInfoListViewModel _masterLocationInfoListView = new LocationInfoListViewModel();

        private BulkObservableCollection<ProductGroup> _availableProductGroups = new BulkObservableCollection<ProductGroup>();
        private ReadOnlyBulkObservableCollection<ProductGroup> _availableProductGroupsRO;

        private BulkObservableCollection<LocationSpaceProductGroupRowViewModel> _availableLocationSpaceRows = new BulkObservableCollection<LocationSpaceProductGroupRowViewModel>();

        private LocationSpace _selectedLocationSpace = null;
        private LocationSpaceProductGroupRowViewModel _selectedLocationSpaceProductGroupRow;
        private LocationSpaceProductGroup _currentLocationSpaceProductGroup;
        private LocationSpaceViewType _viewType = LocationSpaceViewType.Grid;
        private LocationSpaceBay _selectedLocationSpaceBay;
        private LocationSpaceElement _selectedLocationSpaceElement;
        private ProductGroup _selectedProductGroup;

        private Boolean _isDuplicateProductGroup = false;
        private Boolean _showLatestVersionOnly = true;
        private String _filterItemsKey;
        private String _filterItemsCountMessage;
        private LocationInfo _selectedLocationSpaceLocation = null;
        public Double _totalBayWidth;
        private Double _averageBayWidth;

        #endregion

        #region Property Paths
        public static readonly PropertyPath SelectedLocationSpaceProductGroupRowsProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SelectedLocationSpaceProductGroupRows);
        public static readonly PropertyPath TotalBayWidthProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.TotalBayWidth);
        public static readonly PropertyPath AverageBayWidthProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.AverageBayWidth);
        public static readonly PropertyPath MasterLocationInfoListProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.MasterLocationInfoList);
        public static readonly PropertyPath SelectedLocationInfoProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SelectedLocationInfo);
        public static readonly PropertyPath SelectedLocationSpaceProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SelectedLocationSpace);
        public static readonly PropertyPath SelectedLocationSpaceTitleProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SelectedLocationSpaceTitle);
        public static readonly PropertyPath SelectedLocationSpaceProductGroupRowProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SelectedLocationSpaceProductGroupRow);
        public static readonly PropertyPath CurrentLocationSpaceProductGroupProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.CurrentLocationSpaceProductGroup);
        public static readonly PropertyPath AvailableLocationSpaceInfosProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.AvailableLocationSpaceInfos);
        public static readonly PropertyPath AvailableProductGroupsProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.AvailableProductGroups);
        public static readonly PropertyPath AvailableLocationSpaceRowsProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.AvailableLocationSpaceRows);
        public static readonly PropertyPath ViewTypeProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.ViewType);
        public static readonly PropertyPath SelectedProductGroupProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SelectedProductGroup);
        public static readonly PropertyPath LocationSpacePropertiesTitleProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.LocationSpacePropertiesTitle);

        public static readonly PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.OkCommand);
        public static readonly PropertyPath EditCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.EditCommand);
        public static readonly PropertyPath AddCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.AddCommand);
        public static readonly PropertyPath RemoveCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.RemoveCommand);
        public static readonly PropertyPath GroupByCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.GroupByCommand);
        public static readonly PropertyPath IncreaseBaysSelectedElementProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.IncreaseBaysSelectedElement);
        public static readonly PropertyPath DecreaseBaysSelectedElementProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.DecreaseBaysSelectedElement);

        public static readonly PropertyPath AddBayCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.AddBayCommand);
        public static readonly PropertyPath CopyBayCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.CopyBayCommand);
        public static readonly PropertyPath RemoveBayCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.RemoveBayCommand);
        public static readonly PropertyPath AddElementCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.AddElementCommand);
        public static readonly PropertyPath CopyElementCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.CopyElementCommand);
        public static readonly PropertyPath RemoveElementCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.RemoveElementCommand);
        public static readonly PropertyPath SelectedLocationSpaceBayProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.SelectedLocationSpaceBay);
        public static readonly PropertyPath SelectedLocationSpaceElementProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.SelectedLocationSpaceElement);
        public static readonly PropertyPath SelectProductGroupCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.SelectProductGroupCommand);
        public static readonly PropertyPath IsDuplicateProductGroupProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(c => c.IsDuplicateProductGroup);
        public static readonly PropertyPath AddBayAfterCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.AddBayAfterCommand);
        public static readonly PropertyPath AddBayBeforeCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(m => m.AddBayBeforeCommand);
        public static readonly PropertyPath MoveBayLeftCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.MoveBayLeftCommand);
        public static readonly PropertyPath MoveBayRightCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.MoveBayRightCommand);
        public static readonly PropertyPath DuplicateBayCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceMaintenanceViewModel>(p => p.DuplicateBayCommand);


        #endregion

        #region Properties


        public Double TotalBayWidth
        {
            get { return _totalBayWidth; }
            set
            {
                _totalBayWidth = value;
                OnPropertyChanged(TotalBayWidthProperty);
            }
        }

        public Double AverageBayWidth
        {
            get { return _averageBayWidth; }
            set
            {
                _averageBayWidth = value;
                OnPropertyChanged(AverageBayWidthProperty);
            }
        }

        /// <summary>
        /// Returns the master product hierarchy
        /// </summary>
        public ProductHierarchyViewModel MasterProductHierarchyView
        {
            get { return _masterProductHierarchyView; }
        }

        /// <summary>
        /// Returns the master list of location space infos
        /// </summary>
        private ReadOnlyBulkObservableCollection<LocationSpaceInfo> MasterLocationSpaceInfoList
        {
            get { return _masterLocationSpaceInfoListView.BindingView; }
        }

        /// <summary>
        /// Returns the master list of location infos
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationInfo> MasterLocationInfoList
        {
            get { return _masterLocationInfoListView.BindingView; }
        }

        /// <summary>
        /// Gets/Sets the location info for the current SelectedLocationSpace
        /// </summary>
        public LocationInfo SelectedLocationInfo 
        {
            get
            {
                if (this.SelectedLocationSpace != null)
                {
                    Int16 locId = this.SelectedLocationSpace.LocationId;
                    return this.MasterLocationInfoList.FirstOrDefault(l => l.Id == locId);
                }
                return null;
            }

            set
            {
                if (this.SelectedLocationSpace != null)
                {
                    this.SelectedLocationSpace.LocationId = value.Id;
                }
                OnPropertyChanged(SelectedLocationInfoProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available location spaces
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationSpaceInfo> AvailableLocationSpaceInfos
        {
            get
            {
                if (_availableLocationSpaceInfosRO == null)
                {
                    _availableLocationSpaceInfosRO = new ReadOnlyBulkObservableCollection<LocationSpaceInfo>(_availableLocationSpaceInfos);
                }
                return _availableLocationSpaceInfosRO;
            }
        }

        /// <summary>
        /// Returns the collection of product groups available for selection
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductGroup> AvailableProductGroups
        {
            get
            {
                if (_availableProductGroupsRO == null)
                {
                    _availableProductGroupsRO = new ReadOnlyBulkObservableCollection<ProductGroup>(_availableProductGroups);
                }
                return _availableProductGroupsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected location
        /// </summary>
        public LocationSpace SelectedLocationSpace
        {
            get { return _selectedLocationSpace; }
            set
            {
                LocationSpace oldModel = _selectedLocationSpace;
                _selectedLocationSpace = value;
                OnSelectedLocationSpaceChanged(oldModel, value);
                OnPropertyChanged(SelectedLocationSpaceProperty);
                OnPropertyChanged(SelectedLocationSpaceTitleProperty);
            }
        }

        /// <summary>
        /// The window title
        /// </summary>
        public String SelectedLocationSpaceTitle
        {
            get
            {
                if (this.SelectedLocationSpace != null)
                {
                    //Find matching location
                    LocationInfo locInfo = this.MasterLocationInfoList.FirstOrDefault(p => p.Id == this.SelectedLocationSpace.LocationId);

                    if (locInfo != null)
                    {
                        return String.Format(CultureInfo.CurrentCulture, "[ {0} {1} ] - {2}", locInfo.Code, locInfo.Name, Message.Setup_LocationSpaceMaintenance);
                    }
                }
                return String.Format(CultureInfo.CurrentCulture, "[ ] - {0}", Message.Setup_LocationSpaceMaintenance);
            }
        }

        /// <summary>
        /// Gets the collection of location space row records
        /// </summary>
        public BulkObservableCollection<LocationSpaceProductGroupRowViewModel> AvailableLocationSpaceRows
        {
            get { return _availableLocationSpaceRows; }
        }

        /// <summary>
        /// Gets/Sets the selected location space product group
        /// </summary>
        public LocationSpaceProductGroupRowViewModel SelectedLocationSpaceProductGroupRow
        {
            get
            {
                return _selectedLocationSpaceProductGroupRow = SelectedLocationSpaceProductGroupRows.FirstOrDefault();
            }
            set
            {
                _selectedLocationSpaceProductGroupRow = value;
                OnSelectedLocationSpaceProductGroupRowChanged();
            }
        }

        private ObservableCollection<LocationSpaceProductGroupRowViewModel> _SelectedLocationSpaceProductGroupRows = new ObservableCollection<LocationSpaceProductGroupRowViewModel>();

        public ObservableCollection<LocationSpaceProductGroupRowViewModel> SelectedLocationSpaceProductGroupRows
        {
            get { return _SelectedLocationSpaceProductGroupRows; }
            set
            {
                _SelectedLocationSpaceProductGroupRows = value;

            }

        }

        /// <summary>
        /// Gets/Sets the selected location space product group
        /// </summary>
        public LocationSpaceProductGroup CurrentLocationSpaceProductGroup
        {
            get { return _currentLocationSpaceProductGroup; }
            set
            {
                _currentLocationSpaceProductGroup = value;
                OnPropertyChanged(CurrentLocationSpaceProductGroupProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected location space bay
        /// </summary>
        public LocationSpaceBay SelectedLocationSpaceBay
        {
            get { return _selectedLocationSpaceBay; }
            set
            {
                LocationSpaceBay oldValue = _selectedLocationSpaceBay;

                _selectedLocationSpaceBay = value;
                OnSelectedLocationSpaceBayChanged(oldValue, value);
                OnPropertyChanged(SelectedLocationSpaceBayProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected location space element
        /// </summary>
        public LocationSpaceElement SelectedLocationSpaceElement
        {
            get { return _selectedLocationSpaceElement; }
            set
            {
                LocationSpaceElement oldValue = _selectedLocationSpaceElement;

                _selectedLocationSpaceElement = value;
                OnSelectedLocationSpaceElementChanged(oldValue, value);
                OnPropertyChanged(SelectedLocationSpaceElementProperty);
                CalculateTotalBayWidthAndAverageBayWidth();
            }
        }

        /// <summary>
        /// View Location Space View is selected.
        /// </summary>
        public LocationSpaceViewType ViewType
        {
            get { return _viewType; }
            set
            {
                _viewType = value;
                OnPropertyChanged(ViewTypeProperty);
            }
        }

        /// <summary>
        /// Returns title for the location space properties window
        /// </summary>
        public String LocationSpacePropertiesTitle
        {
            get
            {
                //Lookup location name
                String locationName = String.Empty;
                if (this.SelectedLocationSpace != null)
                {
                    LocationInfo locInfo = this.MasterLocationInfoList.FirstOrDefault(p => p.Id == this.SelectedLocationSpace.LocationId);
                    locationName = (locInfo != null) ? locInfo.ToString() : String.Empty;
                }
                return String.Format(CultureInfo.CurrentCulture, Message.LocationSpace_PropertiesTitle, locationName);
            }
        }

        /// <summary>
        /// Gets/sets the selected Product Group (Assgined Category)
        /// </summary>
        public ProductGroup SelectedProductGroup
        {
            get { return _selectedProductGroup; }
            set
            {
                _selectedProductGroup = value;
                OnPropertyChanged(SelectedProductGroupProperty);
                if (value != null && _currentLocationSpaceProductGroup != null)
                {
                    CurrentLocationSpaceProductGroup.ProductGroupId = value.Id;
                }
            }
        }

        /// <summary>
        /// Returns true if a LocationSpaceProductGroup using the selected group id exists, otherwise false
        /// </summary>
        public Boolean IsDuplicateProductGroup
        {
            get { return _isDuplicateProductGroup; }
        }

        #endregion

        #region Events

        /// <summary>
        /// Delclaration of CloseWindow event
        /// </summary>
        public event EventHandler CloseWindow;
        /// <summary>
        /// Method for event to fire
        /// </summary>
        public void OnCloseWindow()
        {
            if (this.CloseWindow != null)
            {
                this.CloseWindow(this, EventArgs.Empty);
            }
        }

        public event EventHandler BaysUpdated;

        public void OnBaysUpdated() => BaysUpdated?.Invoke(this, EventArgs.Empty);

        #endregion

        #region Constructor


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="?"></param>
        public LocationSpaceMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            //location info view
            _masterLocationInfoListView.FetchAllForEntity();

            //merch hierarchy view
            _masterProductHierarchyView.ModelChanged += MasterProductHierarchyViewModel_ModelChanged;
            _masterProductHierarchyView.FetchMerchandisingHierarchyForCurrentEntity();

            //_masterProductHierarchyNonDeleted = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);
            //location space list view
            _masterLocationSpaceInfoListView.ModelChanged += MasterLocationSpaceInfoListView_ModelChanged;
            _masterLocationSpaceInfoListView.FetchForCurrentEntity();

            //create a new location space
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;
        /// <summary>
        /// Create a new location space record
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand =
                        new RelayCommand(p => New_Executed(), p => New_CanExecute())
                        {
                            FriendlyName = Message.Generic_New,
                            FriendlyDescription = Message.Generic_New_Tooltip,
                            Icon = ImageResources.New_32,
                            SmallIcon = ImageResources.New_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.N
                        };
                    this.ViewModelCommands.Add(_newCommand);
                }
                return _newCommand;
            }
        }

        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_userHasLocationSpaceCreatePerm)
            {
                this.NewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                //Create new item
                this.SelectedLocationSpace = LocationSpace.NewLocationSpace(App.ViewState.EntityId);
                this.SelectedLocationSpace.MarkGraphAsInitialized();

                if (this.AttachedControl != null)
                {
                    //close the backstage
                    LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
                }
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;
        /// <summary>
        /// Loads the requested store
        /// parameter = the id of the store to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p.Value),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    this.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? locationSpaceId)
        {
            //user must have get permission
            if (!_userHasLocationSpaceFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (locationSpaceId == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32 locationSpaceId)
        {
            //var openTimer = new Stopwatch();
            //openTimer.Start();
            //Stopwatch timer = new Stopwatch();

            //check if current item requires save first
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    //timer.Start();
                    var locationSpace = LocationSpace.GetLocationSpaceById(locationSpaceId);
                    //timer.Stop();
                    //Console.WriteLine("Loading LocationSpace from DB took {0} milliseconds", timer.ElapsedMilliseconds);
                    //timer.Restart();
                    this.SelectedLocationSpace = locationSpace;
                    //timer.Stop();
                    //Console.WriteLine("Assigning LocationSpace to SelectedLocationSpace took {0} milliseconds", timer.ElapsedMilliseconds);
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);
                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                    base.ShowWaitCursor(true);
                }

                //close the attached organiser backstage
                //timer.Restart();
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*IsOpen*/false);
                //timer.Stop();
                //Console.WriteLine("SetRibbonBackstageState took {0} milliseconds to execute", timer.ElapsedMilliseconds);

                base.ShowWaitCursor(false);
            }

            //openTimer.Stop();
            //Console.WriteLine("Open Command took {0} milliseconds to execute.", openTimer.ElapsedMilliseconds);
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// Saves the current version of location space
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                        new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                        {
                            FriendlyName = Message.Generic_Save,
                            FriendlyDescription = Message.Generic_Save_Tooltip,
                            Icon = ImageResources.Save_32,
                            SmallIcon = ImageResources.Save_16,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.S
                        };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //model must exist
            if (this.SelectedLocationSpace == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have edit permission
            if (!_userHasLocationSpaceEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //the model must be unique location id must be valid if new, otherwise user cannot change location id anyway
            if (this.SelectedLocationSpace.IsNew)
            {
                if (this.AvailableLocationSpaceInfos.Count(p => p.LocationId == this.SelectedLocationSpace.LocationId) > 0)
                {
                    this.SaveCommand.DisabledReason = Message.LocationSpace_SaveDisabledReasonDuplicateLocation;
                    this.SaveAndCloseCommand.DisabledReason = Message.LocationSpace_SaveDisabledReasonDuplicateLocation;
                    this.SaveAndNewCommand.DisabledReason = Message.LocationSpace_SaveDisabledReasonDuplicateLocation;
                    return false;
                }
            }

            //ensure all location space product groups have a valid bay count
            foreach (LocationSpaceProductGroup productGroup in this.SelectedLocationSpace.LocationSpaceProductGroups)
            {
                if ((productGroup.BayCount % 0.5) > 0)
                {
                    this.SaveCommand.DisabledReason = Message.LocationSpace_SaveDisabledReasonInvalidBayCounts;
                    this.SaveAndCloseCommand.DisabledReason = Message.LocationSpace_SaveDisabledReasonInvalidBayCounts;
                    this.SaveAndNewCommand.DisabledReason = Message.LocationSpace_SaveDisabledReasonInvalidBayCounts;
                    return false;
                }
            }

            //the model must be valid
            if (!this.SelectedLocationSpace.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //the model must be amended
            if (!this.SelectedLocationSpace.IsDirty)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Calls a save on the current item
        /// </summary>
        /// <returns>true of successful, false if the save was cancelled</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.SelectedLocationSpace = this.SelectedLocationSpace.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //[SA-17668] set return flag to false as save was not completed.
                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.MasterLocationInfoList.First(p => p.Id == this.SelectedLocationSpace.LocationId).ToString());
                        if (itemReloadRequired)
                        {
                            this.SelectedLocationSpace = LocationSpace.GetLocationSpaceById(this.SelectedLocationSpace.Id);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.MasterLocationInfoList.First(p => p.Id == this.SelectedLocationSpace.LocationId).ToString(), OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                //update the infos list
                this._masterLocationSpaceInfoListView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }

            return continueWithSave;
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    base.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;
        /// <summary>
        /// Saves the new version tree of the location space
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand =
                        new RelayCommand(p => SaveAs_Executed(), p => SaveAs_CanExecute())
                        {
                            FriendlyName = Message.Generic_SaveAs,
                            Icon = ImageResources.SaveAs_32,
                            SmallIcon = ImageResources.SaveAs_16,
                            FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                            InputGestureKey = Key.F12,
                            InputGestureModifiers = ModifierKeys.None
                        };
                    this.ViewModelCommands.Add(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //model must exist
            if (this.SelectedLocationSpace == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have edit permission
            if (!_userHasLocationSpaceEditPerm)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //may not be new
            if (this.SelectedLocationSpace.IsNew)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_SaveAs_DisabledReason;
                return false;
            }

            //the model must be valid
            if (!this.SelectedLocationSpace.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            //Generate generate a locations which dont have location space setup yet
            ObservableCollection<LocationInfo> availableLocationInfos = this.MasterLocationInfoList.ToObservableCollection();
            foreach (LocationSpaceInfo locSpace in this.MasterLocationSpaceInfoList)
            {
                LocationInfo info = availableLocationInfos.FirstOrDefault(p => p.Id == locSpace.LocationId);
                if (info != null)
                {
                    availableLocationInfos.Remove(info);
                }
            }

            //Prompt which location to create for
            LocationSpaceMaintenanceSaveAsWindow win = new LocationSpaceMaintenanceSaveAsWindow(availableLocationInfos);
            App.ShowWindow(win, true);

            //If save ok button was pressed, otherwise cancelled
            if (win.DialogResult.Value)
            {
                base.ShowWaitCursor(true);

                //copy the item and save
                LocationSpace itemCopy = this.SelectedLocationSpace.Copy();
                itemCopy.LocationId = win.SelectedLocation.Id; ;

                //set the copy as the new selected item
                this.SelectedLocationSpace = itemCopy;

                try
                {
                    //Save new location space
                    this.SelectedLocationSpace = this.SelectedLocationSpace.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //otherwise just show the user an error has occurred.
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.MasterLocationInfoList.First(p => p.Id == this.SelectedLocationSpace.LocationId).ToString(), OperationType.Save);

                    base.ShowWaitCursor(true);
                }

                //update the infos list
                this._masterLocationSpaceInfoListView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;
        /// <summary>
        /// Deletes the current location space
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_userHasLocationSpaceDeletePerm)
            {
                this.DeleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.SelectedLocationSpace == null)
            {
                this.DeleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.SelectedLocationSpace.IsNew)
            {
                this.DeleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            //Find matching location
            LocationInfo locInfo = this.MasterLocationInfoList.First(p => p.Id == this.SelectedLocationSpace.LocationId);

            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                //Lookup location info based on id and use as defining attribute

                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(locInfo.Name);
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //mark the location space as deleted
                LocationSpace locSpaceToDelete = this.SelectedLocationSpace;
                locSpaceToDelete.Delete();

                //load a new item
                this.SelectedLocationSpace = LocationSpace.NewLocationSpace(App.ViewState.EntityId);

                //commit the delete
                try
                {
                    //commit the delete
                    locSpaceToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(locInfo.Name.ToString(), OperationType.Delete);

                    base.ShowWaitCursor(true);
                }

                //update the available items
                this._masterLocationSpaceInfoListView.FetchForCurrentEntity();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region EditCommand

        private RelayCommand _editCommand;
        /// <summary>
        /// Edit the selected location space
        /// </summary>
        public RelayCommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                {
                    _editCommand =
                        new RelayCommand(p => Edit_Executed(), p => Edit_CanExecute())
                        {
                            FriendlyName = Message.LocationSpace_Edit,
                            Icon = ImageResources.LocationSpace_Edit
                        };
                    this.ViewModelCommands.Add(_editCommand);
                }
                return _editCommand;
            }
        }

        private Boolean Edit_CanExecute()
        {
            return OnSelectedLocationSpaceProductGroupRowChanged() && this.SelectedLocationSpaceProductGroupRows.Count == 1 && this.SelectedLocationSpace != null;
        }

        private void Edit_Executed()
        {
            //Ensure duplicate flag is off
            this._isDuplicateProductGroup = false;

            //Temporarily subscribe to the property changed event
            this.CurrentLocationSpaceProductGroup.PropertyChanged += new PropertyChangedEventHandler(SelectedLocationSpaceProductGroup_PropertyChanged);

            this.CurrentLocationSpaceProductGroup.BeginEdit();

            //Select first bay
            this.SelectedLocationSpaceBay = this.CurrentLocationSpaceProductGroup.Bays.FirstOrDefault();

            //Select first element, if available or reset
            this.SelectedLocationSpaceElement = (this.SelectedLocationSpaceBay != null) ? this.SelectedLocationSpaceBay.Elements.FirstOrDefault() : null;

            //Find product group from Id
            foreach (ProductGroup productGroup in _masterProductHierarchyView.Model.EnumerateAllGroups())
            {
                if (productGroup.Id == CurrentLocationSpaceProductGroup.ProductGroupId)
                {
                    this.SelectedProductGroup = productGroup;
                    break;
                }
            }

            //Show editting window
            LocationSpaceMaintenanceProperties win = new LocationSpaceMaintenanceProperties(this);
            App.ShowWindow(win, this.AttachedControl, true);

            if (!win.DialogResult.Value)
            {
                //Cancel edit if cancel button has been clicked
                this.CurrentLocationSpaceProductGroup.CancelEdit();
            }
            else
            {
                //Apply edit if result was returned
                this.CurrentLocationSpaceProductGroup.ApplyEdit();
            }

            //Unsubscribe from the property changed event
            this.CurrentLocationSpaceProductGroup.PropertyChanged -= SelectedLocationSpaceProductGroup_PropertyChanged;
        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;
        /// <summary>
        /// Ok command for the properties window
        /// </summary>
        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand =
                        new RelayCommand(p => Ok_Executed(), p => Ok_CanExecute())
                        {
                            FriendlyName = Message.Generic_Ok
                        };
                    this.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        private Boolean Ok_CanExecute()
        {
            return (this.CurrentLocationSpaceProductGroup != null && this.CurrentLocationSpaceProductGroup.IsValid);
        }

        private void Ok_Executed()
        {
            //Nothing to do, window handles effect
        }

        #endregion

        #region AddCommand

        private RelayCommand _addCommand;

        /// <summary>
        /// Create a new location space record
        /// </summary>
        public RelayCommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand =
                        new RelayCommand(
                            p => Add_Executed(),
                            p => Add_CanExecute())
                        {
                            FriendlyName = Message.Generic_Add,
                            FriendlyDescription = Message.LocationSpace_AddLocationSpaceGroup_Description,
                            Icon = ImageResources.LocationSpace_Add,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.A
                        };
                    base.ViewModelCommands.Add(_addCommand);
                }
                return _addCommand;
            }
        }

        private Boolean Add_CanExecute()
        {
            return (this.SelectedLocationSpace != null);
        }

        private void Add_Executed()
        {
            if (this.SelectedLocationSpace != null)
            {
                //Create new record
                this.CurrentLocationSpaceProductGroup = LocationSpaceProductGroup.NewLocationSpaceProductGroup();

                //Select first bay
                this.SelectedLocationSpaceBay = this.CurrentLocationSpaceProductGroup.Bays.FirstOrDefault();

                //Select first element, if available or reset
                this.SelectedLocationSpaceElement = (this.SelectedLocationSpaceBay != null) ? this.SelectedLocationSpaceBay.Elements.FirstOrDefault() : null;

                //Make product group null
                this.SelectedProductGroup = null;

                //Temporarily subscribe to property changed event
                this.CurrentLocationSpaceProductGroup.PropertyChanged += SelectedLocationSpaceProductGroupAdd_PropertyChanged;

                //Show editting window
                LocationSpaceMaintenanceProperties win = new LocationSpaceMaintenanceProperties(this);
                App.ShowWindow(win, this.AttachedControl, true);

                //If complete add to list
                if (win.DialogResult.Value)
                {
                    if (this.CurrentLocationSpaceProductGroup.IsNew) { this.SelectedLocationSpace.LocationSpaceProductGroups.Add(this.CurrentLocationSpaceProductGroup); }
                }



                //Unsubscribe from property changed
                this.CurrentLocationSpaceProductGroup.PropertyChanged -= SelectedLocationSpaceProductGroupAdd_PropertyChanged;

                //Reset the flag for next time add is called
                _isDuplicateProductGroup = false;
            }
        }

        #endregion

        #region RemoveCommand

        private RelayCommand _removeCommand;

        /// <summary>
        /// remove a location space record
        /// </summary>
        public RelayCommand RemoveCommand
        {
            get
            {
                if (_removeCommand == null)
                {
                    _removeCommand =
                        new RelayCommand(
                            p => Remove_Executed(),
                            p => Remove_CanExecute())
                        {
                            FriendlyName = Message.Generic_Remove,
                            FriendlyDescription = Message.LocationSpace_RemoveLocationSpaceGroup_Description,
                            Icon = ImageResources.LocationSpace_Remove,
                            InputGestureModifiers = ModifierKeys.Control,
                            InputGestureKey = Key.R
                        };
                    base.ViewModelCommands.Add(_removeCommand);
                }
                return _removeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Remove_CanExecute()
        {
            if (this.SelectedLocationSpaceProductGroupRows.Count > 0)
            {
                return true;
            }
            this.RemoveCommand.DisabledReason = Message.LocationSpaceMaintenance_Remove_NoProductGroupSelected;
            return false;
        }

        private void Remove_Executed()
        {
            List<LocationSpaceProductGroup> itemsToRemove = new List<LocationSpaceProductGroup>();

            foreach (LocationSpaceProductGroupRowViewModel item in this.SelectedLocationSpaceProductGroupRows)
            {

                if (item != null)
                {
                    itemsToRemove.Add(item.LocationSpaceProductGroup);
                }
            }

            //Remove groups
            foreach (LocationSpaceProductGroup item in itemsToRemove)
            {
                this.CurrentLocationSpaceProductGroup = item;
                this.SelectedLocationSpace.LocationSpaceProductGroups.Remove(this.CurrentLocationSpaceProductGroup);
                this.CurrentLocationSpaceProductGroup = null;
            }
        }

        #endregion

        #region GroupByCommand

        private RelayCommand<String> _groupByCommand;
        /// <summary>
        /// Saves the current locationCluster
        /// </summary>
        public RelayCommand<String> GroupByCommand
        {
            get
            {
                if (_groupByCommand == null)
                {
                    _groupByCommand =
                        new RelayCommand<String>(p => GroupBy_Executed(p), p => GroupBy_CanExecute())
                        {
                            FriendlyName = Message.LocationSpace_Group,
                            Icon = ImageResources.LocationSpace_Group,
                            SmallIcon = ImageResources.LocationSpace_Group
                        };
                    this.ViewModelCommands.Add(_groupByCommand);
                }
                return _groupByCommand;
            }
        }

        private Boolean GroupBy_CanExecute()
        {
            return true;
        }

        private void GroupBy_Executed(String groupByValue)
        {
            //Handled by organiser.xaml.cs code
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(),
                        p => Close_CanExecute(),
                        attachToCommandManager: false)
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region Fixture Bay Element Commands


        #region AddBayAfterCommand

        private RelayCommand _addBayAfterCommand;

        /// <summary>
        /// Adds a new fixture bay
        /// </summary>
        public RelayCommand AddBayAfterCommand
        {
            get
            {
                if (_addBayAfterCommand == null)
                {
                    _addBayAfterCommand = new RelayCommand(p => AddBayAfter_Executed(), p => AddBayAfter_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_AddBay,
                        FriendlyDescription = Message.LocationSpace_AddBayAfter_Tooltip,
                        Icon = ImageResources.LocationSpace_AddFixtureAfter_16
                    };
                    this.ViewModelCommands.Add(_addBayAfterCommand);
                }
                return _addBayAfterCommand;
            }
        }

        private bool AddBayAfter_CanExecute()
        {
            return ((this.CurrentLocationSpaceProductGroup != null) ? true : false);
        }

        private void AddBayAfter_Executed()
        {
            InsertNewBay();

            //Update selected elements
            SelectedLocationSpaceElement = SelectedLocationSpaceBay.Elements.FirstOrDefault();

            OnBaysUpdated();
        }

        #endregion

        #region AddBayBeforeCommand

        private RelayCommand _addBayBeforeCommand;

        /// <summary>
        /// Adds a new fixture bay
        /// </summary>
        public RelayCommand AddBayBeforeCommand
        {
            get
            {
                if (_addBayBeforeCommand == null)
                {
                    _addBayBeforeCommand = new RelayCommand(p => AddBayBefore_Executed(), p => AddBayBefore_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_AddBay,
                        FriendlyDescription = Message.LocationSpace_AddBayBefore_Tooltip,
                        Icon = ImageResources.LocationSpace_AddFixtureBefore_16
                    };
                    this.ViewModelCommands.Add(_addBayBeforeCommand);
                }
                return _addBayBeforeCommand;
            }
        }

        private bool AddBayBefore_CanExecute()
        {
            return ((this.CurrentLocationSpaceProductGroup != null) ? true : false);
        }

        private void AddBayBefore_Executed()
        {
            InsertNewBay(before: true);

            //Update selected elements
            this.SelectedLocationSpaceElement = this.SelectedLocationSpaceBay.Elements.FirstOrDefault();

            OnBaysUpdated();
        }

        private void InsertNewBay(Boolean before = false)
        {
            ShowWaitCursor(true);
            LocationSpaceBay newFixtureBay = SelectedLocationSpaceBay.Copy();
            Int32 replacedIndex = newFixtureBay.Order + (before ? -1 : 1);
            newFixtureBay.Order = (Byte)replacedIndex;

            CurrentLocationSpaceProductGroup.Bays.Add(newFixtureBay);
            SelectedLocationSpaceBay = newFixtureBay;
            ShowWaitCursor(false);
        }

        #endregion

        #region MoveBayLeftCommand

        private RelayCommand _moveBayLeftCommand;

        /// <summary>
        /// Moves Selected Bay Left
        /// </summary>
        public RelayCommand MoveBayLeftCommand
        {
            get
            {
                if (_moveBayLeftCommand == null)
                {
                    _moveBayLeftCommand = new RelayCommand(
                        p => MoveBayLeft_Executed(),
                        p => MoveBayLeft_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_MoveBayLeft,
                        FriendlyDescription = Message.LocationSpace_MoveBayLeft_Desc,
                        SmallIcon = ImageResources.LocationSpace_MoveBayLeft_16,
                    };
                    base.ViewModelCommands.Add(_moveBayLeftCommand);
                }
                return _moveBayLeftCommand;
            }
        }

        private Boolean MoveBayLeft_CanExecute()
        {
            if (SelectedLocationSpaceBay != null && SelectedLocationSpaceBay.Order <= 1)
            {
                // disabled if there is not Bay to the left
                _moveBayLeftCommand.DisabledReason = Message.LocationSpace_MoveBayLeft_Disabled_NoBayLeft;
                return false;
            }

            return true;
        }

        public void MoveBayLeft_Executed()
        {
            ShowWaitCursor(true);

            SelectedLocationSpaceBay.Order--;

            OnBaysUpdated();

            ShowWaitCursor(false);
        }

        #endregion

        #region MoveBayRightCommand

        private RelayCommand _moveBayRightCommand;

        /// <summary>
        /// Moves Selected Bay Right
        /// </summary>
        public RelayCommand MoveBayRightCommand
        {
            get
            {
                if (_moveBayRightCommand == null)
                {
                    _moveBayRightCommand = new RelayCommand(
                        p => MoveBayRight_Executed(),
                        p => MoveBayRight_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_MoveBayRight,
                        FriendlyDescription = Message.LocationSpace_MoveBayRight_Desc,
                        SmallIcon = ImageResources.LocationSpace_MoveBayRight_16,
                    };
                    base.ViewModelCommands.Add(_moveBayRightCommand);
                }
                return _moveBayRightCommand;
            }
        }

        private Boolean MoveBayRight_CanExecute()
        {
            if (SelectedLocationSpaceBay != null && CurrentLocationSpaceProductGroup.Bays != null && SelectedLocationSpaceBay.Order == CurrentLocationSpaceProductGroup.Bays.Count())
            {
                // disabled if there is not Bay to the Right
                _moveBayRightCommand.DisabledReason = Message.LocationSpace_MoveBayRight_Disabled_NoBayRight;
                return false;
            }

            return true;
        }

        public void MoveBayRight_Executed()
        {
            ShowWaitCursor(true);

            SelectedLocationSpaceBay.Order++;

            OnBaysUpdated(); ;

            ShowWaitCursor(false);
        }

        #endregion

        #region DuplicateBayCommand

        private RelayCommand _duplicateBayCommand;

        /// <summary>
        ///  Creates a new copy of the currently selected bay and places it after the existing one.
        /// </summary>
        public RelayCommand DuplicateBayCommand
        {
            get
            {
                if (_duplicateBayCommand == null)
                {
                    _duplicateBayCommand = new RelayCommand(
                        p => DuplicateBay_Executed(),
                        p => DuplicateBay_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_DuplicateBay,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_duplicateBayCommand);
                }
                return _duplicateBayCommand;
            }
        }

        private Boolean DuplicateBay_CanExecute()
        {
            if (this.SelectedLocationSpaceBay == null) return false;

            return true;
        }

        private void DuplicateBay_Executed()
        {
            ShowWaitCursor(true);

            CopyBay_Executed();

            ShowWaitCursor(false);
        }

        #endregion

        #region AddElementCommand

        private RelayCommand _addElementCommand;

        /// <summary>
        /// Adds a new element
        /// </summary>
        public RelayCommand AddElementCommand
        {
            get
            {
                if (_addElementCommand == null)
                {
                    _addElementCommand = new RelayCommand(p => AddElement_Executed(), p => AddElement_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_AddElement,
                        FriendlyDescription = Message.LocationSpace_AddElement_Tooltip,
                        Icon = ImageResources.LocationSpace_AddElement,
                        DisabledReason = Message.LocationSpace_AddElement_Invalid
                    };
                    this.ViewModelCommands.Add(_addElementCommand);
                }
                return _addElementCommand;
            }
        }


        private bool AddElement_CanExecute()
        {
            if (this.SelectedLocationSpaceBay != null)
            {
                if (SelectedLocationSpaceBay.Height <= 0 || SelectedLocationSpaceBay.Width <= 0)
                {
                    this.AddElementCommand.DisabledReason = Message.LocationSpace_ElementCommandInvalid;
                    return false;
                }
                //if the bay already has elements check that another will fit
                if (this.SelectedLocationSpaceBay.Elements.Count > 0)
                {
                    LocationSpaceElement maxElement = this.SelectedLocationSpaceBay.Elements.OrderBy(e => e.Y).Last();

                    float maxElementTotalHeight = maxElement.FaceThickness + maxElement.Height;

                    //If bay height is greater than the full height of 
                    //the highest element in the bay then a new element is viable. 
                    if ((maxElement.Y + maxElementTotalHeight) < (this.SelectedLocationSpaceBay.Height - this.SelectedLocationSpaceBay.BaseHeight))
                    {
                        return true;
                    }
                    else
                    {
                        this.AddElementCommand.DisabledReason = Message.LocationSpace_ElementCommandInvalid;
                        return false;
                    }

                }
                else
                {
                    //if no elements at all allow one to be added
                    return true;
                }
            }
            else
            {
                this.AddElementCommand.DisabledReason = Message.LocationSpace_AddElement_Invalid;
                return false;
            }
        }


        private void AddElement_Executed()
        {
            float newElementStartingYPosition = 0;
            IEnumerable<LocationSpaceElement> tempElementList = new List<LocationSpaceElement>();

            //if an element is selected then update with its details
            if (this.SelectedLocationSpaceElement != null)
            {
                //Calculate new elements starting position ABOVE the selected
                newElementStartingYPosition = this.SelectedLocationSpaceElement.Y + this.SelectedLocationSpaceElement.Height + this.SelectedLocationSpaceElement.FaceThickness + this.SelectedLocationSpaceElement.MerchandisableHeight;

                //Create list of any elements higher than current
                tempElementList = this.SelectedLocationSpaceBay.Elements.Where(e => e.Y > this.SelectedLocationSpaceElement.Y);

            }

            //Add new element
            LocationSpaceElement newElement = LocationSpaceElement.NewLocationSpaceElement();

            //Set initial width to bay width
            newElement.Width = this.SelectedLocationSpaceBay.Width;

            //Increase new elements Y position to its new starting position
            newElement.Y = newElementStartingYPosition;

            //Calculate new elements total height
            float newElementsTotalHeight = (newElement.Height + newElement.FaceThickness);

            //Add this height to elements
            foreach (LocationSpaceElement elementItem in tempElementList)
            {
                elementItem.Y += newElementsTotalHeight;
            }

            //Add element
            this.SelectedLocationSpaceBay.Elements.Add(newElement);
            this.SelectedLocationSpaceElement = newElement;
            ValidateLocationSpaceElementNumbers();
        }

        #endregion

        #region CopyElementCommand

        private RelayCommand _copyElementCommand;

        /// <summary>
        /// Copies element
        /// </summary>
        public RelayCommand CopyElementCommand
        {
            get
            {
                if (_copyElementCommand == null)
                {
                    _copyElementCommand = new RelayCommand(p => CopyElement_Executed(), p => CopyElement_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_CopyElement,
                        FriendlyDescription = Message.LocationSpace_CopyElement_Tooltip,
                        Icon = ImageResources.LocationSpace_CopyElement,
                        DisabledReason = Message.LocationSpace_CopyElement_Invalid
                    };
                    this.ViewModelCommands.Add(_copyElementCommand);
                }
                return _copyElementCommand;
            }
        }

        private bool CopyElement_CanExecute()
        {
            if (this.SelectedLocationSpaceBay != null
                && this.CurrentLocationSpaceProductGroup != null
                && this.SelectedLocationSpaceElement != null
                && this.SelectedLocationSpaceBay.Elements.Count > 0)
            {
                //Get max fixture element
                LocationSpaceElement maxElement = this.SelectedLocationSpaceBay.Elements.OrderBy(e => e.Y).Last();
                float maxElementTotalHeight = (maxElement.FaceThickness + maxElement.Height);
                //If bay height is greater than the full height of 
                //the highest element in the bay then a new element is viable. 
                if ((maxElement.Y + maxElementTotalHeight) < (this.SelectedLocationSpaceBay.Height - this.SelectedLocationSpaceBay.BaseHeight))
                {
                    return true;
                }
                else
                {
                    this.CopyElementCommand.DisabledReason = Message.LocationSpace_ElementCommandInvalid;
                    return false;
                }
            }
            else
            {
                this.CopyElementCommand.DisabledReason = Message.LocationSpace_CopyElement_Invalid;
                return false;
            }
        }

        private void CopyElement_Executed()
        {
            //Take copy of current selected element
            LocationSpaceElement currentSelectedElement = this.SelectedLocationSpaceElement;

            //Calculate new elements starting position ABOVE the selected
            float newElementStartingYPosition = currentSelectedElement.Y + currentSelectedElement.Height + currentSelectedElement.FaceThickness;

            //Create list of any elements higher than current
            List<LocationSpaceElement> tempElementList = new List<LocationSpaceElement>();
            tempElementList = this.SelectedLocationSpaceBay.Elements.Where(e => e.Y > currentSelectedElement.Y).ToList();

            //Copy element
            LocationSpaceElement copyElement = this.SelectedLocationSpaceElement.Copy();

            //Increase new elements Y position to its new starting position
            copyElement.Y = newElementStartingYPosition;

            //Calculate new elements total height
            float copyElementsTotalHeight = (copyElement.Height + copyElement.FaceThickness);

            //Add this height to elements
            foreach (LocationSpaceElement elementItem in tempElementList)
            {
                elementItem.Y += copyElementsTotalHeight;
            }

            //Add to fixture bay
            this.SelectedLocationSpaceBay.Elements.Add(copyElement);

            //Select copy fixture element
            this.SelectedLocationSpaceElement = copyElement;

            //Revalidate element numbers
            ValidateLocationSpaceElementNumbers();
        }

        #endregion

        #region RemoveElementCommand

        private RelayCommand _removeElementCommand;

        /// <summary>
        /// Removes a element
        /// </summary>
        public RelayCommand RemoveElementCommand
        {
            get
            {
                if (_removeElementCommand == null)
                {
                    _removeElementCommand = new RelayCommand(p => RemoveElement_Executed(), p => RemoveElement_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_RemoveElement,
                        FriendlyDescription = Message.LocationSpace_RemoveElement_Tooltip,
                        Icon = ImageResources.LocationSpace_RemoveElement,
                        DisabledReason = Message.LocationSpace_RemoveElement_Invalid
                    };
                    this.ViewModelCommands.Add(_removeElementCommand);
                }
                return _removeElementCommand;
            }
        }

        private bool RemoveElement_CanExecute()
        {
            if (this.CurrentLocationSpaceProductGroup != null && this.SelectedLocationSpaceBay != null && this.SelectedLocationSpaceElement != null)
            {
                return (this.SelectedLocationSpaceBay.Elements.Count > 1 ? true : false);
            }
            return false;
        }

        private void RemoveElement_Executed()
        {
            //take copy of fixture element to remove
            LocationSpaceElement elementToRemove = this.SelectedLocationSpaceElement;

            //remove the requested fixture element
            this.SelectedLocationSpaceBay.Elements.Remove(elementToRemove);

            //select new fixture element if possible
            this.SelectedLocationSpaceElement = this.SelectedLocationSpaceBay.Elements.FirstOrDefault();

            ValidateLocationSpaceElementNumbers();
        }

        #endregion

        #region AddBayCommand

        private RelayCommand _addBayCommand;

        /// <summary>
        /// Adds a new fixture bay
        /// </summary>
        public RelayCommand AddBayCommand
        {
            get
            {
                if (_addBayCommand == null)
                {
                    _addBayCommand = new RelayCommand(p => AddBay_Executed(), p => AddBay_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_AddBay,
                        FriendlyDescription = Message.LocationSpace_AddBay_Tooltip,
                        Icon = ImageResources.LocationSpace_AddFixtureBay
                    };
                    this.ViewModelCommands.Add(_addBayCommand);
                }
                return _addBayCommand;
            }
        }

        private bool AddBay_CanExecute()
        {
            return ((this.CurrentLocationSpaceProductGroup != null) ? true : false);
        }

        private void AddBay_Executed()
        {
            //Create new bay number
            LocationSpaceBay newFixtureBay = null;
            if (this.CurrentLocationSpaceProductGroup.Bays.Count > 0)
            {
                //Copy last bay 
                newFixtureBay = this.CurrentLocationSpaceProductGroup.Bays.OrderByDescending(b => b.Order).First().Copy();
            }
            else
            {
                //Create new bay with bay order 1
                newFixtureBay = LocationSpaceBay.NewLocationSpaceBay();
            }

            //Add new location space bay
            this.CurrentLocationSpaceProductGroup.Bays.Add(newFixtureBay);

            //Select new location space bay
            this.SelectedLocationSpaceBay = this.CurrentLocationSpaceProductGroup.Bays.Last();

            //Update new bayNumber to a valid number which continues the sequence
            byte newBayNum = (this.CurrentLocationSpaceProductGroup.Bays.Select(f => f.Order).Max());
            newBayNum++;
            this.SelectedLocationSpaceBay.Order = newBayNum;

            //Update selected elements
            this.SelectedLocationSpaceElement = this.SelectedLocationSpaceBay.Elements.FirstOrDefault();

            OnBaysUpdated();
        }

        #endregion

        #region CopyBayCommand

        private RelayCommand _copyBayCommand;

        /// <summary>
        /// Copies fixture bay
        /// </summary>
        public RelayCommand CopyBayCommand
        {
            get
            {
                if (_copyBayCommand == null)
                {
                    _copyBayCommand = new RelayCommand(p => CopyBay_Executed(), p => CopyBay_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_CopyBay,
                        FriendlyDescription = Message.LocationSpace_CopyBay_Tooltip,
                        Icon = ImageResources.LocationSpace_CopyFixtureBay,
                        DisabledReason = Message.LocationSpace_CopyBay_Invalid
                    };
                    this.ViewModelCommands.Add(_copyBayCommand);
                }
                return _copyBayCommand;
            }
        }

        private bool CopyBay_CanExecute()
        {
            return this.CurrentLocationSpaceProductGroup != null && this.SelectedLocationSpaceBay != null && (this.CurrentLocationSpaceProductGroup.Bays.Count > 0);
        }

        private void CopyBay_Executed()
        {
            //Copy bay
            LocationSpaceBay copyBay = this.SelectedLocationSpaceBay.Copy();

            //Add to fixture option
            this.CurrentLocationSpaceProductGroup.Bays.Add(copyBay);

            //Select copy fixture bay
            this.SelectedLocationSpaceBay = copyBay;

            //Update new bayNumber to a valid number which continues the sequence
            byte newBayNum = (this.CurrentLocationSpaceProductGroup.Bays.Select(f => f.Order).Max());
            newBayNum++;
            this.SelectedLocationSpaceBay.Order = newBayNum;

            OnBaysUpdated();
        }

        #endregion

        #region RemoveBayCommand

        private RelayCommand _removeBayCommand;

        /// <summary>
        /// Removes a bay
        /// </summary>
        public RelayCommand RemoveBayCommand
        {
            get
            {
                if (_removeBayCommand == null)
                {
                    _removeBayCommand = new RelayCommand(p => RemoveBay_Executed(), p => RemoveBay_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_RemoveBay,
                        FriendlyDescription = Message.LocationSpace_RemoveBay_Tooltip,
                        Icon = ImageResources.LocationSpace_RemoveFixtureBay,
                        DisabledReason = Message.LocationSpace_RemoveBay_Invalid
                    };
                    this.ViewModelCommands.Add(_removeBayCommand);
                }
                return _removeBayCommand;
            }
        }

        private bool RemoveBay_CanExecute()
        {
            if (SelectedLocationSpaceBay == null) return false;
            return CurrentLocationSpaceProductGroup?.Bays?.Count > 1;
        }

        private void RemoveBay_Executed()
        {
            //remove the requested bay
            this.CurrentLocationSpaceProductGroup.Bays.Remove(SelectedLocationSpaceBay);

            OnBaysUpdated();

            this.SelectedLocationSpaceBay = this.CurrentLocationSpaceProductGroup.Bays.FirstOrDefault(x => x.Order == 1);

            //refresh the UI and set the first bay as selected
            OnBaysUpdated();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region IncreaseBaysSelectedElement

        private RelayCommand _increaseBaysSelectedElement;

        /// <summary>
        /// Changes the selected element to the next in the list
        /// </summary>
        public RelayCommand IncreaseBaysSelectedElement
        {
            get
            {
                if (_increaseBaysSelectedElement == null)
                {
                    _increaseBaysSelectedElement = new RelayCommand(p => IncreaseBaysSelectedElement_Executed(), p => IncreaseBaysSelectedElement_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_IncreaseBaysSelectedElement,
                        FriendlyDescription = Message.LocationSpace_IncreaseBaysSelectedElement_Tooltip,
                        Icon = ImageResources.LocationSpace_IncreaseBaysSelectedElement,
                        DisabledReason = Message.LocationSpace_IncreaseBaysSelectedElement_Invalid
                    };
                    this.ViewModelCommands.Add(_increaseBaysSelectedElement);
                }
                return _increaseBaysSelectedElement;
            }
        }

        private bool IncreaseBaysSelectedElement_CanExecute()
        {
            return (this.SelectedLocationSpaceElement != null
                && this.SelectedLocationSpaceBay != null
                && this.SelectedLocationSpaceBay.Elements.Count > 0
                && this.SelectedLocationSpaceElement.Order != this.SelectedLocationSpaceBay.Elements.OrderBy(e => e.Order).Last().Order ? true : false);
        }

        private void IncreaseBaysSelectedElement_Executed()
        {
            //Get current selected elements number
            byte currentNum = this.SelectedLocationSpaceElement.Order;

            //Increase current number
            currentNum++;

            //Select new element
            this.SelectedLocationSpaceElement = this.SelectedLocationSpaceBay.Elements.Where(e => e.Order == currentNum).First();
        }

        #endregion

        #region DecreaseBaysSelectedElement

        private RelayCommand _decreaseBaysSelectedElement;

        /// <summary>
        /// Changes the selected element to the previous in the list
        /// </summary>
        public RelayCommand DecreaseBaysSelectedElement
        {
            get
            {
                if (_decreaseBaysSelectedElement == null)
                {
                    _decreaseBaysSelectedElement = new RelayCommand(p => DecreaseBaysSelectedElement_Executed(), p => DecreaseBaysSelectedElement_CanExecute())
                    {
                        FriendlyName = Message.LocationSpace_DecreaseBaysSelectedElement,
                        FriendlyDescription = Message.LocationSpace_DecreaseBaysSelectedElement_Tooltip,
                        Icon = ImageResources.LocationSpace_DecreaseBaysSelectedElement,
                        DisabledReason = Message.LocationSpace_DecreaseBaysSelectedElement_Invalid
                    };
                    this.ViewModelCommands.Add(_decreaseBaysSelectedElement);
                }
                return _decreaseBaysSelectedElement;
            }
        }

        private bool DecreaseBaysSelectedElement_CanExecute()
        {
            return (this.SelectedLocationSpaceElement != null
                && this.SelectedLocationSpaceBay != null
                && this.SelectedLocationSpaceBay.Elements.Count > 0
                && this.SelectedLocationSpaceElement.Order != this.SelectedLocationSpaceBay.Elements.OrderBy(e => e.Order).First().Order ? true : false);
        }

        private void DecreaseBaysSelectedElement_Executed()
        {
            //Get current selected elements number
            byte currentNum = this.SelectedLocationSpaceElement.Order;

            //Increase current number
            currentNum--;

            //Select new element
            this.SelectedLocationSpaceElement = this.SelectedLocationSpaceBay.Elements.Where(e => e.Order == currentNum).First();
        }


        #endregion

        #region SelectProductGroupCommand

        private RelayCommand _selectProductGroupCommand;

        /// <summary>
        /// Shows the select product group dialog
        /// </summary>
        public RelayCommand SelectProductGroupCommand
        {
            get
            {
                if (_selectProductGroupCommand == null)
                {
                    _selectProductGroupCommand = new RelayCommand(
                        p => SelectProductGroup_Executed(),
                        p => SelectProductGroup_CanExecute())
                    {
                    };
                    base.ViewModelCommands.Add(_selectProductGroupCommand);
                }
                return _selectProductGroupCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SelectProductGroup_CanExecute()
        {
            //user must have get permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(ProductGroup)))
            {
                this.SelectProductGroupCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SelectProductGroup_Executed()
        {
            if (this.AttachedControl != null)
            {
                MerchGroupSelectionWindow dialog = new MerchGroupSelectionWindow(_masterProductHierarchyView.Model, null);
                dialog.SelectionMode = DataGridSelectionMode.Single;

                //create the columnset
                DataGridColumnCollection columnSet = new DataGridColumnCollection();

                columnSet.Add(
                    ExtendedDataGrid.CreateReadOnlyTextColumn(ProductGroup.CodeProperty.FriendlyName, ProductGroup.CodeProperty.Name, HorizontalAlignment.Left));

                columnSet.Add(
                    ExtendedDataGrid.CreateReadOnlyTextColumn(ProductGroup.NameProperty.FriendlyName, ProductGroup.NameProperty.Name, HorizontalAlignment.Left));

                //show the dialog
                App.ShowWindow(dialog, Galleria.Framework.Controls.Wpf.Helpers.GetWindow<LocationSpaceMaintenanceProperties>(), true);

                if (dialog.DialogResult == true)
                {
                    this.SelectedProductGroup = dialog.SelectionResult.Cast<ProductGroup>().FirstOrDefault();
                }
            }
            else
            {
                //For testing purposes
                this.SelectedProductGroup = _availableProductGroups.Last();
            }
        }

        #endregion

        #endregion

        #region AddLocationsCommand

        private RelayCommand _addLocationsCommand;

        public RelayCommand AddLocationsCommand
        {
            get
            {
                if (_addLocationsCommand == null)
                {
                    _addLocationsCommand = new RelayCommand(
                        p => AddLocations_Executed(),
                        p => AddLocations_CanExecute())
                    {
                        FriendlyName = Message.LocationSpaceMaintenance_CopyToOtherLocations,
                        Icon = ImageResources.LocationSpaceMaintenance_Copy_32
                    };
                    base.ViewModelCommands.Add(_addLocationsCommand);
                }
                return _addLocationsCommand;
            }
        }

        private Boolean AddLocations_CanExecute()
        {
            //user must have edit permission
            if (!_userHasLocationSpaceEditPerm)
            {
                this.AddLocationsCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (this.SelectedLocationSpaceProductGroupRows.Count == 0)
            {
                this.AddLocationsCommand.DisabledReason = Message.AssortmentSetup_NoAssortmentSelected;
                return false;
            }

            return true;

        }

        private void AddLocations_Executed()
        {

            if(LocationSpaceContinueWithItemChange())
            {

                if (this.AttachedControl != null && SelectedLocationSpaceProductGroupRows.Count>0)
                    {

                        //load the loc list if it does not already exist

                        if (_masterLocationList == null)
                        {
                            base.ShowWaitCursor(true);

                            try
                            {
                                _masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);
                            }
                            catch (DataPortalException ex)
                            {
                                base.ShowWaitCursor(false);

                                if (this.AttachedControl != null)
                                {
                                    Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                        String.Empty, OperationType.Open,
                                        this.AttachedControl);
                                }

                                base.ShowWaitCursor(true);
                            }
                            base.ShowWaitCursor(false);
                        }

                        LocationList MasterLocationList = new LocationList();
                        LocationSpaceInfoList AllLocationSpaceInfo = LocationSpaceInfoList.FetchByEntityId(App.ViewState.EntityId);
                        foreach (var item in _masterLocationList)
                        {
                            LocationSpaceInfo lsi = AllLocationSpaceInfo.Where(w => w.LocationId == item.Id).FirstOrDefault();
                            if (lsi != null)
                            {
                                MasterLocationList.Add(item);
                            }
                        }

                        LocationList AllLocationsWithLocationSpace = new LocationList();
                        AllLocationsWithLocationSpace = GetAllLocationsWithLocationSpace(_masterLocationList);

                        //Open the Window
                        LocationSpaceAddLocationsWizardWindowViewModel lpviewmodel = new LocationSpaceAddLocationsWizardWindowViewModel(this.SelectedLocationSpaceProductGroupRows, AllLocationsWithLocationSpace);

                        LocationSpaceAddLocationsWizardWindow addLocationsWindow =
                            new LocationSpaceAddLocationsWizardWindow(lpviewmodel);

                        App.ShowWindow(addLocationsWindow, this.AttachedControl, /*isModal*/true);

                        this.SaveCommand.Execute();

                    }

            }
        }

        #endregion

        #endregion

        #region Methods

        public LocationList GetAllLocationsWithLocationSpace(LocationList masterLocationList)
        {
            LocationList MasterLocationList = new LocationList();
            LocationSpaceInfoList AllLocationSpaceInfo = LocationSpaceInfoList.FetchByEntityId(App.ViewState.EntityId);
            foreach (var item in masterLocationList)
            {
                LocationSpaceInfo lsi = AllLocationSpaceInfo.Where(w => w.LocationId == item.Id).FirstOrDefault();
                if (lsi != null)
                {
                    MasterLocationList.Add(item);
                }
            }
            return MasterLocationList;
        }



        /// <summary>
        /// Returns a list of location spaces that match the given criteria
        /// </summary>
        /// <param name="codeOrNameCriteria"></param>
        /// <returns></returns>
        public IEnumerable<LocationSpaceInfo> GetMatchingLocationSpaces(String codeOrNameCriteria)
        {
            String invariantCriteria = codeOrNameCriteria.ToLowerInvariant();

            return this.AvailableLocationSpaceInfos.Where(
                p => p.LocationName.ToLowerInvariant().Contains(invariantCriteria) ||
                    p.LocationCode.ToLowerInvariant().Contains(invariantCriteria));
        }

        public void ReOrderBayNumbers()
        {
            Int32 updatedNumBays = CurrentLocationSpaceProductGroup.Bays.Count;
            foreach (LocationSpaceBay bayItem in this.CurrentLocationSpaceProductGroup.Bays.OrderByDescending(b => b.Order))
            {
                bayItem.Order = (Byte)updatedNumBays;
                updatedNumBays--;
            }

        }


        public void CalculateTotalBayWidthAndAverageBayWidth()
        {
            if (CurrentLocationSpaceProductGroup == null) return;

            TotalBayWidth = this.CurrentLocationSpaceProductGroup.Bays.Sum(s => s.Width);
            AverageBayWidth = TotalBayWidth / CurrentLocationSpaceProductGroup.Bays.Count;
        }

        /// <summary>
        /// On selected location space product group changing
        /// </summary>
        private Boolean OnSelectedLocationSpaceProductGroupRowChanged()
        {
            if (this.SelectedLocationSpaceProductGroupRow != null)
            {
                this.CurrentLocationSpaceProductGroup =
                    this.SelectedLocationSpaceProductGroupRow.LocationSpaceProductGroup;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// On selected location space changing
        /// </summary>
        private void OnSelectedLocationSpaceChanged(LocationSpace oldModel, LocationSpace newModel)
        {
            if (oldModel != null)
            {
                //Unsubscribe from old collection cahnged
                oldModel.LocationSpaceProductGroups.BulkCollectionChanged -= LocationSpaceProductGroupList_BulkCollectionChanged;
            }

            if (newModel != null)
            {
                //Clear any old data
                if (_availableLocationSpaceRows.Count > 0)
                {
                    _availableLocationSpaceRows.Clear();
                }
                //Load any groups into memory
                Dictionary<ProductGroup, IEnumerable<ProductGroup>> parentPathsByGroup = _masterProductHierarchyView.Model.
                    EnumerateAllGroups().
                    ToDictionary(g => g, g => g.GetParentPath().Where(p => !p.IsRoot));
                Int32 numLevels = _masterProductHierarchyView.Model.EnumerateAllLevels().Count(p => p.IsRoot == false);
                List<LocationSpaceProductGroupRowViewModel> list =
                    new List<LocationSpaceProductGroupRowViewModel>(newModel.LocationSpaceProductGroups.Count);
                foreach (LocationSpaceProductGroup productGroup in newModel.LocationSpaceProductGroups)
                {
                    var locSpaceRowItem = CreateLocationSpaceRow(productGroup, parentPathsByGroup, numLevels);
                    list.Add(locSpaceRowItem);
                }
                _availableLocationSpaceRows.AddRange(list);

                //Resubscribe to new
                newModel.LocationSpaceProductGroups.BulkCollectionChanged += new EventHandler<Galleria.Framework.Model.BulkCollectionChangedEventArgs>(LocationSpaceProductGroupList_BulkCollectionChanged);
            }

            //OnPropertyChanged(AvailableLocationSpaceRowsProperty);
            OnPropertyChanged(SelectedLocationInfoProperty);
        }

        /// <summary>
        /// Method to create required objects based on location space
        /// </summary>
        /// <param name="locSpace"></param>
        private LocationSpaceProductGroupRowViewModel CreateLocationSpaceRow(
            LocationSpaceProductGroup locSpaceProdGroup,
            Dictionary<ProductGroup, IEnumerable<ProductGroup>> parentPathsByGroup,
            Int32 numLevels)
        {
            //Find group names
            BulkObservableCollection<String> groupNames = new BulkObservableCollection<String>();

            //Find assigned group
            ProductGroup group = parentPathsByGroup.Keys.FirstOrDefault(p => p.Id == locSpaceProdGroup.ProductGroupId);

            //If group can be located
            if (group != null)
            {
                if (!group.IsRoot)
                {
                    //Add group code/names
                    foreach (ProductGroup parentGroup in parentPathsByGroup[group])
                    {
                        groupNames.Add(String.Format("{0} : {1}", parentGroup.Code, parentGroup.Name));
                    }
                    //If assigned group is not root as these details too
                    if (!group.IsRoot)
                    {
                        groupNames.Add(String.Format("{0} : {1}", group.Code, group.Name));
                    }

                    //Add empty string to resolve binding exceptions for location spaces assigned higher up the hierarchy
                    Int32 numGroups = groupNames.Count;
                    if (numGroups < numLevels)
                    {
                        for (int i = 0; i < (numLevels - numGroups); i++)
                        {
                            groupNames.Add(String.Empty);
                        }
                    }
                }
            }
            else
            {
                //Reset product group id to 0 as it no longer exists
                locSpaceProdGroup.ProductGroupId = 0;
            }

            LocationSpaceProductGroupRowViewModel row =
                new LocationSpaceProductGroupRowViewModel(locSpaceProdGroup, groupNames);

            return row;
        }

        /// <summary>
        /// Method to generate column set based on hierarchy
        /// </summary>
        /// <returns></returns>
        public DataGridColumnCollection GenerateColumnSet()
        {
            //Create collection
            DataGridColumnCollection columnSet = new DataGridColumnCollection();

            //Find level details
            IEnumerable<ProductLevel> levels = this._masterProductHierarchyView.Model.EnumerateAllLevels().Where(p => p.IsRoot != true);
            Int32 levelIndex = 0;
            foreach (ProductLevel level in levels)
            {
                //Create columns
                columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(level.Name, String.Format("GroupNames[{0}]", levelIndex), HorizontalAlignment.Left));
                levelIndex++;
            }

            columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationSpaceProductGroup.BayCountProperty.FriendlyName, "LocationSpaceProductGroup.BayCount", HorizontalAlignment.Right, Galleria.Framework.Controls.Wpf.InputType.Decimal));
            columnSet.Add(ExtendedDataGrid.CreateReadOnlyTextColumn(LocationSpaceProductGroup.ProductCountProperty.FriendlyName, "LocationSpaceProductGroup.ProductCount", HorizontalAlignment.Right, Galleria.Framework.Controls.Wpf.InputType.Integer));

            //average bay width
            var uomValues = Galleria.Ccm.Common.Wpf.Helpers.DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.CurrentEntityView.Model);
            columnSet.Add(Galleria.Ccm.Common.Wpf.Helpers.CommonHelper.CreateReadOnlyColumn(LocationSpaceProductGroup.AverageBayWidthProperty,
               uomValues, null, true, "LocationSpaceProductGroup"));
            return columnSet;
        }

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //Location Space premissions
            //create
            _userHasLocationSpaceCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(LocationSpace));

            //fetch
            _userHasLocationSpaceFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(LocationSpace));

            //edit
            _userHasLocationSpaceEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(LocationSpace));

            //delete
            _userHasLocationSpaceDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(LocationSpace));
        }

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                //NB - Had to implement this as LocationSpace model object doesn't have user friendly
                //String override, and we agreed not to have a name in the Schema.
                if (this.SelectedLocationSpace.IsDirty
                    && !this.SelectedLocationSpace.IsDeleted
                        && !this.SelectedLocationSpace.IsInitialized)
                {
                    //Find matching location
                    LocationInfo locInfo = this.MasterLocationInfoList.FirstOrDefault(p => p.Id == this.SelectedLocationSpace.LocationId);

                    if (locInfo != null)
                    {
                        continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ShowContinueWithItemChangeDialog(locInfo.Name, this.SelectedLocationSpace.IsValid, this.SaveCommand);
                    }
                }
            }
            return continueExecute;
        }

        public Boolean LocationSpaceContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                //NB - Had to implement this as LocationSpace model object doesn't have user friendly
                //String override, and we agreed not to have a name in the Schema.
                if (this.SelectedLocationSpace.IsDirty
                    && !this.SelectedLocationSpace.IsDeleted
                        && !this.SelectedLocationSpace.IsInitialized)
                {
                    //Find matching location
                    LocationInfo locInfo = this.MasterLocationInfoList.FirstOrDefault(p => p.Id == this.SelectedLocationSpace.LocationId);

                    if (locInfo != null)
                    {
                        continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ShowItemChangeDialogSaveAndCancel(locInfo.Name, this.SelectedLocationSpace.IsValid, this.SaveCommand);
                    }
                }
            }
            return continueExecute;
        }



        /// <summary>
        /// On selected location space bay changing
        /// </summary>
        private void OnSelectedLocationSpaceBayChanged(LocationSpaceBay oldValue, LocationSpaceBay newValue)
        {
            if (oldValue != null)
            {
                //unsubscribe from events
                oldValue.PropertyChanged -= SelectedLocationSpaceBay_PropertyChanged;
            }

            if (newValue != null)
            {

                //subscribe to required events
                newValue.PropertyChanged += new PropertyChangedEventHandler(SelectedLocationSpaceBay_PropertyChanged);

                //Select first element if a valid element for this bay is not already selected
                if (this.SelectedLocationSpaceElement != null)
                {
                    if (this.SelectedLocationSpaceElement.Parent != newValue)
                    {
                        this.SelectedLocationSpaceElement = newValue.Elements.OrderBy(e => e.Order).FirstOrDefault();
                    }
                }
                else
                {
                    this.SelectedLocationSpaceElement = newValue.Elements.OrderBy(e => e.Order).FirstOrDefault();
                }

            }
        }

        /// <summary>
        /// Method to handle updates required upon changing fixture element selection
        /// </summary>
        private void OnSelectedLocationSpaceElementChanged(LocationSpaceElement oldValue, LocationSpaceElement newValue)
        {
            //clear down from the old value
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedLocationSpaceElement_PropertyChanged;
            }

            if (newValue != null)
            {
                //Subscribe to the event for when the loation space element properties change
                newValue.PropertyChanged += new PropertyChangedEventHandler(SelectedLocationSpaceElement_PropertyChanged);
            }
            CalculateTotalBayWidthAndAverageBayWidth();
        }

        /// <summary>
        /// Validates the location space bay numbers ensuring valid and in a sequencel order
        /// </summary>
        private void ValidateLocationSpaceBayNumbers()
        {
            //Get new bayNumber
            byte newBayNumber = this.SelectedLocationSpaceBay.Order;

            //Get number of bays for this option
            int numBays = this.CurrentLocationSpaceProductGroup.Bays.Count;

            //Check bay number is valid
            bool validBaynumber = (newBayNumber <= numBays) ? true : false;

            if (validBaynumber)
            {
                //Check for existing bay with this number
                bool duplicateBaysExist = (this.CurrentLocationSpaceProductGroup.Bays.Where(f => f.Order == newBayNumber).Count() > 1);

                //If duplicate exists
                if (duplicateBaysExist)
                {
                    //Generate list of numbers
                    List<byte> numberList = new List<byte>();
                    for (int i = 1; i <= numBays; i++)
                    {
                        if (i != (byte)newBayNumber)
                        {
                            numberList.Add((byte)i);
                        }
                    }
                    //Update the bay numbers accordingly
                    foreach (LocationSpaceBay bay in this.CurrentLocationSpaceProductGroup.Bays)
                    {
                        if (bay != this.SelectedLocationSpaceBay)
                        {
                            bay.Order = numberList.First();
                            numberList.Remove(numberList.First());
                        }
                    }
                }
            }
            //If not valid number i.e larger than number of bays
            else
            {
                //Set bay number to first valid number --> at the end
                //Generate list of numbers
                List<byte> numberList = new List<byte>();
                for (int i = 1; i <= numBays; i++)
                {
                    //Find where no bayNumber exists and use this value for the fixture bay's number
                    if (!(this.CurrentLocationSpaceProductGroup.Bays.Any(f => f.Order == (byte)i)))
                    {
                        numberList.Add((byte)i);
                    }
                }

                //Set to location space bay property
                this.SelectedLocationSpaceBay.Order = numberList.First();
                OnPropertyChanged(SelectedLocationSpaceBayProperty);
            }
        }

        /// <summary>
        /// Validates the location space elements numbers ensuring valid and in a sequencel order
        /// </summary>
        private void ValidateLocationSpaceElementNumbers()
        {

            //Get new y position for element
            float newYPositionValue = this.SelectedLocationSpaceElement.Y;

            //Check for duplicates
            bool duplicateYPositions = this.SelectedLocationSpaceBay.Elements.Where(e => e.Y == newYPositionValue).Count() > 1;

            //If exists - alter the value by 0.1
            if (duplicateYPositions)
            {
                this.SelectedLocationSpaceElement.Y = (this.SelectedLocationSpaceElement.Y + 0.5F);
            }

            LocationSpaceElement LocationSpaceSelectedELement = LocationSpaceElement.NewLocationSpaceElement();

            LocationSpaceSelectedELement = this.SelectedLocationSpaceElement;

            //Get number of elements for this option
            int numElements = this.SelectedLocationSpaceBay.Elements.Count;

            LocationSpaceElementList tempElementList = new LocationSpaceElementList();

            //Reorder element numbers based on the Yposition where y is not 0
            //y position will usually be 0 for the first element and cannot be less than 0
            //this is so that if the user has imported with more than one element at 0, it won't change the element order
            foreach (LocationSpaceElement elementItem in this.SelectedLocationSpaceBay.Elements.Where(l => l.Y != 0).OrderByDescending(e => e.Y))
            {
                elementItem.Order = (byte)numElements;
                numElements--;
            }

            foreach (LocationSpaceElement elementItem in this.SelectedLocationSpaceBay.Elements.Reverse())
            {
                if (elementItem.Y == 0)
                {
                    elementItem.Order = (byte)numElements;
                    numElements--;
                }
                tempElementList.Add(elementItem);
            }

            //Apply re-order and refresh as Elements is not an observable collection
            this.SelectedLocationSpaceBay.Elements.Clear();
            LocationSpaceElement selectedTemp = LocationSpaceElement.NewLocationSpaceElement();

            foreach (LocationSpaceElement elementItem in tempElementList.OrderBy(x => x.Order))
            {
                this.SelectedLocationSpaceBay.Elements.Add(elementItem);

                if (elementItem.Y == LocationSpaceSelectedELement.Y)
                {
                    selectedTemp = elementItem;
                }
            }

            this.SelectedLocationSpaceElement = this.SelectedLocationSpaceBay.Elements.Where(x => x.Y == selectedTemp.Y).FirstOrDefault();

        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Responds to a change of the master hierarchy model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MasterProductHierarchyViewModel_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductHierarchy> e)
        {
            //create the available product groups
            if (_availableProductGroups.Count > 0)
            {
                _availableProductGroups.Clear();
            }

            if (e.NewModel != null)
            {
                _availableProductGroups.AddRange(e.NewModel.EnumerateAllGroups().ToList());
            }
        }


        /// <summary>
        /// Responds to changes to the master location space infos list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MasterLocationSpaceInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<LocationSpaceInfoList> e)
        {
            if (_availableLocationSpaceInfos.Count > 0)
            {
                _availableLocationSpaceInfos.Clear();
            }

            if (e.NewModel != null)
            {
                //add all infos for which there is a master locationinfo
                // as we don't want to show items where the equivalent location has been deleted.
                Int16[] locationIds = this.MasterLocationInfoList.Select(l => l.Id).ToArray();

                List<LocationSpaceInfo> availableInfos = new List<LocationSpaceInfo>();
                foreach (LocationSpaceInfo info in e.NewModel)
                {
                    if (locationIds.Contains(info.LocationId))
                    {
                        availableInfos.Add(info);
                    }
                }
                _availableLocationSpaceInfos.AddRange(availableInfos);
            }
        }

        /// <summary>
        /// Location Space's product group collection changing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationSpaceProductGroupList_BulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                Dictionary<ProductGroup, IEnumerable<ProductGroup>> parentPathsByGroup = _masterProductHierarchyView.Model.
                    EnumerateAllGroups().
                    ToDictionary(g => g, g => g.GetParentPath().Where(p => !p.IsRoot));
                Int32 numLevels = _masterProductHierarchyView.Model.EnumerateAllLevels().Count(p => p.IsRoot == false);
                foreach (LocationSpaceProductGroup locationSpaceProdGroup in e.ChangedItems)
                {
                    //Create wrapper objects
                    var newLocSpaceRowItem = CreateLocationSpaceRow(locationSpaceProdGroup, parentPathsByGroup, numLevels);
                    _availableLocationSpaceRows.Add(newLocSpaceRowItem);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (LocationSpaceProductGroup locationSpaceProdGroup in e.ChangedItems)
                {
                    //Find corresponding row
                    LocationSpaceProductGroupRowViewModel rowObj = this._availableLocationSpaceRows.FirstOrDefault(p => p.LocationSpaceProductGroup == locationSpaceProdGroup);
                    if (rowObj != null)
                    {
                        //Remove
                        this._availableLocationSpaceRows.Remove(rowObj);
                    }
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Reset)
            {
                //Clear existing location space
                foreach (LocationSpaceProductGroupRowViewModel row in this._availableLocationSpaceRows)
                {
                    row.Dispose();
                }
                this._availableLocationSpaceRows.Clear();
            }
            OnPropertyChanged(AvailableLocationSpaceRowsProperty);
        }

        /// <summary>
        /// Property changed event to update the group names collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLocationSpaceProductGroup_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationSpaceProductGroup.ProductGroupIdProperty.Name || e.PropertyName == String.Empty)
            {
                LocationSpaceProductGroup locSpaceProdGroup = (LocationSpaceProductGroup)sender;

                //Check for duplicate product group ids, exlcuding the record in question
                List<Int32> takenIds = this.AvailableLocationSpaceRows.Where(p => p.LocationSpaceProductGroup != locSpaceProdGroup).Select(p => p.LocationSpaceProductGroup.ProductGroupId).ToList();
                if (takenIds.Contains(locSpaceProdGroup.ProductGroupId))
                {
                    _isDuplicateProductGroup = true;
                }
                else
                {
                    _isDuplicateProductGroup = false;
                }
                OnPropertyChanged(IsDuplicateProductGroupProperty);

                //Find relevant location space row
                LocationSpaceProductGroupRowViewModel row = this.AvailableLocationSpaceRows.FirstOrDefault(p => p.LocationSpaceProductGroup == locSpaceProdGroup);

                //Find group names
                BulkObservableCollection<String> groupNames = new BulkObservableCollection<String>();

                IEnumerable<ProductGroup> groups = this._masterProductHierarchyView.Model.EnumerateAllGroups().ToList();

                //Find assigned group
                ProductGroup group = groups.FirstOrDefault(p => p.Id == locSpaceProdGroup.ProductGroupId);

                if (group != null)
                {
                    //Add group code/names
                    foreach (ProductGroup parentGroup in group.GetParentPath().Where(p => p.IsRoot != true))
                    {
                        groupNames.Add(String.Format(CultureInfo.CurrentCulture, "{0} : {1}", parentGroup.Code, parentGroup.Name));
                    }
                    //If assigned group is not root as these details too
                    if (!group.IsRoot)
                    {
                        groupNames.Add(String.Format(CultureInfo.CurrentCulture, "{0} : {1}", group.Code, group.Name));
                    }
                }

                row.GroupNames = groupNames;
            }
        }

        /// <summary>
        /// Handles the selected fixture bay property changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLocationSpaceBay_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //Only concerned if bay order number changes
            if (e.PropertyName == LocationSpaceBay.OrderProperty.Name)
            {
                ValidateLocationSpaceBayNumbers();
            }
        }

        /// <summary>
        /// Handles the selected location space element property changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLocationSpaceElement_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //Only concerned if element y position value changes
            if (e.PropertyName == LocationSpaceElement.YProperty.Name)
            {
                ValidateLocationSpaceElementNumbers();
            }
        }

        /// <summary>
        /// Responds to a LocationSpaceProductGroup property changed event while adding to update the group names collection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedLocationSpaceProductGroupAdd_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationSpaceProductGroup.ProductGroupIdProperty.Name)
            {
                LocationSpaceProductGroup locSpaceProdGroup = (LocationSpaceProductGroup)sender;

                //Check for duplicate product group assignments
                List<Int32> takenIds = this.AvailableLocationSpaceRows.Select(p => p.LocationSpaceProductGroup.ProductGroupId).ToList();

                if (takenIds.Contains(locSpaceProdGroup.ProductGroupId))
                {
                    _isDuplicateProductGroup = true;
                }
                else
                {
                    _isDuplicateProductGroup = false;
                }
                OnPropertyChanged(IsDuplicateProductGroupProperty);
            }
        }

        #endregion

        #region IDisposableMembers

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterLocationSpaceInfoListView.ModelChanged -= MasterLocationSpaceInfoListView_ModelChanged;
                    _masterProductHierarchyView.ModelChanged -= MasterProductHierarchyViewModel_ModelChanged;

                    _selectedLocationSpaceProductGroupRow = null;
                    _currentLocationSpaceProductGroup = null;

                    this.SelectedLocationSpace.LocationSpaceProductGroups.BulkCollectionChanged -= LocationSpaceProductGroupList_BulkCollectionChanged;
                    _selectedLocationSpace = null;

                    //Clear existing location space
                    _availableLocationSpaceInfos.Clear();
                    foreach (LocationSpaceProductGroupRowViewModel row in _availableLocationSpaceRows)
                    {
                        row.Dispose();
                    }

                    _availableLocationSpaceRows.Clear();


                    if (_selectedLocationSpaceBay != null)
                    {
                        _selectedLocationSpaceBay.PropertyChanged -= SelectedLocationSpaceBay_PropertyChanged;
                        _selectedLocationSpaceBay = null;
                    }

                    if (_selectedLocationSpaceElement != null)
                    {
                        _selectedLocationSpaceElement.PropertyChanged -= SelectedLocationSpaceElement_PropertyChanged;
                        _selectedLocationSpaceElement = null;
                    }


                    _masterLocationSpaceInfoListView.Dispose();
                    _masterLocationInfoListView.Dispose();
                }
                base.IsDisposed = true;
            }
        }


        #endregion
    }

    /// <summary>
    /// Location Space Product Group Row Wrapper Object
    /// </summary>
    public sealed class LocationSpaceProductGroupRowViewModel : Galleria.Framework.ViewModel.ViewModelObject
    {
        #region Field Names

        private LocationSpaceProductGroup _locationSpaceProductGroup;
        private BulkObservableCollection<String> _groupNames;
        private Single _maxDrawSpacebreak = 100F;

        #endregion

        #region Property Paths

        public static readonly PropertyPath MaxDrawSpacebreakProperty = Galleria.Framework.Helpers.WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.MaxDrawSpacebreak);
        public static readonly PropertyPath HeaderProperty = Galleria.Framework.Helpers.WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.Header);
        public static readonly PropertyPath GroupNamesProperty = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.GroupNames);
        public static readonly PropertyPath FriendlyNameProperty = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.FriendlyName);

        public static readonly PropertyPath BayCountProperty = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.BayCount);
        public static readonly PropertyPath ProductCountProperty = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.ProductCount);
        public static readonly PropertyPath AverageBayWidthProperty = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.AverageBayWidth);
        public static readonly PropertyPath AisleNameProperty = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.AisleName);
        public static readonly PropertyPath ValleyNameProperty = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.ValleyName);
        public static readonly PropertyPath ZoneNameProperty = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.ZoneName);
        public static readonly PropertyPath CustomAttribute01Property = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.CustomAttribute01);
        public static readonly PropertyPath CustomAttribute02Property = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.CustomAttribute02);
        public static readonly PropertyPath CustomAttribute03Property = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.CustomAttribute03);
        public static readonly PropertyPath CustomAttribute04Property = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.CustomAttribute04);
        public static readonly PropertyPath CustomAttribute05Property = WpfHelper.GetPropertyPath<LocationSpaceProductGroupRowViewModel>(p => p.CustomAttribute05);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the parent location space product group
        /// </summary>
        public LocationSpaceProductGroup LocationSpaceProductGroup
        {
            get { return _locationSpaceProductGroup; }
        }

        /// <summary>
        /// Gets the records group name path collection
        /// </summary>
        public BulkObservableCollection<String> GroupNames
        {
            get { return _groupNames; }
            set
            {
                _groupNames = value;
                OnPropertyChanged(GroupNamesProperty);
                OnPropertyChanged(HeaderProperty);
                OnPropertyChanged(FriendlyNameProperty);
            }
        }

        /// <summary>
        /// Returns friendly name
        /// </summary>
        public String FriendlyName
        {
            get { return _groupNames.Where(p => p != String.Empty).Last(); }
        }

        /// <summary>
        /// Returns the space changer header
        /// </summary>
        public String Header
        {
            get { return (this.GroupNames.Count > 0) ? this.GroupNames.Where(p => p != String.Empty).Last() : String.Empty; }
        }

        /// <summary>
        /// Returns the max spacebreak the control should draw to
        /// </summary>
        public Single MaxDrawSpacebreak
        {
            get { return _maxDrawSpacebreak; }
        }

        /// <summary>
        /// Gets/Sets the bay count
        /// </summary>
        public Single BayCount
        {
            get { return _locationSpaceProductGroup.BayCount; }
            set
            {
                _locationSpaceProductGroup.BayCount = value;
                OnPropertyChanged(BayCountProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the product count
        /// </summary>
        public Int32? ProductCount
        {
            get { return _locationSpaceProductGroup.ProductCount; }
            set
            {
                _locationSpaceProductGroup.ProductCount = value;
                OnPropertyChanged(ProductCountProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the average bay width
        /// </summary>
        public Single AverageBayWidth
        {
            get { return _locationSpaceProductGroup.AverageBayWidth; }
            set
            {
                _locationSpaceProductGroup.AverageBayWidth = value;
                OnPropertyChanged(AverageBayWidthProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the Aisle Name
        /// </summary>
        public String AisleName
        {
            get { return _locationSpaceProductGroup.AisleName; }
            set
            {
                _locationSpaceProductGroup.AisleName = value;
                OnPropertyChanged(AisleNameProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the Valley Name
        /// </summary>
        public String ValleyName
        {
            get { return _locationSpaceProductGroup.ValleyName; }
            set
            {
                _locationSpaceProductGroup.ValleyName = value;
                OnPropertyChanged(ValleyNameProperty);
            }
        }


        /// <summary>
        /// Gets/Sets the Zone Name
        /// </summary>
        public String ZoneName
        {
            get { return _locationSpaceProductGroup.ZoneName; }
            set
            {
                _locationSpaceProductGroup.ZoneName = value;
                OnPropertyChanged(ZoneNameProperty);
            }
        }
        /// <summary>
        /// Gets/Sets the CustomAttribute01
        /// </summary>
        public String CustomAttribute01
        {
            get { return _locationSpaceProductGroup.CustomAttribute01; }
            set
            {
                _locationSpaceProductGroup.CustomAttribute01 = value;
                OnPropertyChanged(CustomAttribute01Property);
            }
        }
        /// <summary>
        /// Gets/Sets the CustomAttribute02
        /// </summary>
        public String CustomAttribute02
        {
            get { return _locationSpaceProductGroup.CustomAttribute02; }
            set
            {
                _locationSpaceProductGroup.CustomAttribute02 = value;
                OnPropertyChanged(CustomAttribute02Property);
            }
        }
        /// <summary>
        /// Gets/Sets the CustomAttribute03
        /// </summary>
        public String CustomAttribute03
        {
            get { return _locationSpaceProductGroup.CustomAttribute03; }
            set
            {
                _locationSpaceProductGroup.CustomAttribute03 = value;
                OnPropertyChanged(CustomAttribute03Property);
            }
        }

        /// <summary>
        /// Gets/Sets the CustomAttribute04
        /// </summary>
        public String CustomAttribute04
        {
            get { return _locationSpaceProductGroup.CustomAttribute04; }
            set
            {
                _locationSpaceProductGroup.CustomAttribute04 = value;
                OnPropertyChanged(CustomAttribute04Property);
            }
        }
        /// <summary>
        /// Gets/Sets the CustomAttribute05
        /// </summary>
        public String CustomAttribute05
        {
            get { return _locationSpaceProductGroup.CustomAttribute05; }
            set
            {
                _locationSpaceProductGroup.CustomAttribute05 = value;
                OnPropertyChanged(CustomAttribute05Property);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="locationSpace"></param>
        public LocationSpaceProductGroupRowViewModel(LocationSpaceProductGroup locationSpaceProductGroup, BulkObservableCollection<String> groupNames)
        {
            //Load properties
            this._locationSpaceProductGroup = locationSpaceProductGroup;

            //Work out group names from the merch hierarchy
            this._groupNames = groupNames;

            //Subscribe to property changed event
            this._locationSpaceProductGroup.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(LocationSpace_PropertyChanged);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Property chagned event on the location space object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationSpace_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationSpaceProductGroup.BayCountProperty.Name)
            {
                OnPropertyChanged(BayCountProperty);
            }
            else if (e.PropertyName == LocationSpaceProductGroup.ProductCountProperty.Name)
            {
                OnPropertyChanged(ProductCountProperty);
            }
            else if (e.PropertyName == LocationSpaceProductGroup.AverageBayWidthProperty.Name)
            {
                OnPropertyChanged(AverageBayWidthProperty);
            }
            else if (e.PropertyName == LocationSpaceProductGroup.ProductGroupIdProperty.Name)
            {
                OnPropertyChanged(GroupNamesProperty);
                OnPropertyChanged(HeaderProperty);
            }
            else if (e.PropertyName == String.Empty)
            {
                OnPropertyChanged(GroupNamesProperty);
                OnPropertyChanged(BayCountProperty);
                OnPropertyChanged(AverageBayWidthProperty);
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (this._locationSpaceProductGroup != null)
                    {
                        this._locationSpaceProductGroup.PropertyChanged -= LocationSpace_PropertyChanged;
                    }
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
