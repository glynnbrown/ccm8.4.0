﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpaceMaintenanceSaveAsWindow.xaml
    /// </summary>
    public partial class LocationSpaceMaintenanceSaveAsWindow : ExtendedRibbonWindow
    {
        #region Properties

        public static readonly DependencyProperty SelectedLocationProperty =
            DependencyProperty.Register("SelectedLocation", typeof(LocationInfo), typeof(LocationSpaceMaintenanceSaveAsWindow));
        public LocationInfo SelectedLocation
        {
            get { return (LocationInfo)GetValue(SelectedLocationProperty); }
            private set { SetValue(SelectedLocationProperty, value); }
        }

        public static readonly DependencyProperty AvailableLocationsProperty =
            DependencyProperty.Register("AvailableLocations", typeof(ObservableCollection<LocationInfo>), typeof(LocationSpaceMaintenanceSaveAsWindow));
        public ObservableCollection<LocationInfo> AvailableLocations
        {
            get { return (ObservableCollection<LocationInfo>)GetValue(AvailableLocationsProperty); }
            private set { SetValue(AvailableLocationsProperty, value); }
        }

        #endregion

        #region Constructor

        public LocationSpaceMaintenanceSaveAsWindow(ObservableCollection<LocationInfo> availableLocations)
        {
            //Pass available locations into the collection
            this.AvailableLocations = availableLocations;

            //Set initial selected location
            this.SelectedLocation = this.AvailableLocations.FirstOrDefault();

            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Save button click 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Save button click 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        #endregion
    }
}
