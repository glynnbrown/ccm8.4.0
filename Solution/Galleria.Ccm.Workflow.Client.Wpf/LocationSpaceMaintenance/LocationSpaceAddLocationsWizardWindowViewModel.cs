﻿using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    public class LocationSpaceAddLocationsWizardWindowViewModel : ViewModelAttachedControlObject<LocationSpaceAddLocationsWizardWindow>
    {
        #region Fields

        private Boolean? _dialogResult;
        private Int32? _createdPackageId;
        private WizardStep _currentWizardStep = WizardStep.AddLocations;
        private IWizardStepViewModel _currentStepViewModel;

        private LocationList _masterLocationList;
        private ObservableCollection<LocationSpaceProductGroupRowViewModel> _selectedLocationSpaceCategory = new ObservableCollection<LocationSpaceProductGroupRowViewModel>();
        private ObservableCollection<Location> _selectedLocations = new ObservableCollection<Location>();

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath CurrentWizardStepProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsWizardWindowViewModel>(p => p.CurrentWizardStep);
        public static readonly PropertyPath CurrentStepViewModelProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsWizardWindowViewModel>(p => p.CurrentStepViewModel);

        //Commands
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsWizardWindowViewModel>(p => p.NextCommand);
        public static readonly PropertyPath FinishCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsWizardWindowViewModel>(p => p.FinishCommand);
        public static readonly PropertyPath PreviousCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsWizardWindowViewModel>(p => p.PreviousCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsWizardWindowViewModel>(p => p.CancelCommand);

        #endregion

        #region Properties

        public ObservableCollection<LocationSpaceProductGroupRowViewModel> SelectedLocationSpaceCategory
        {
            get { return _selectedLocationSpaceCategory; }
            private set
            {
                _selectedLocationSpaceCategory = value;
            }
        }

        public LocationList MasterLocationList
        {
            get { return _masterLocationList; }
            private set
            {
                _masterLocationList = value;
            }
        }

        public ObservableCollection<Location> SelectedLocations
        {
            get { return _selectedLocations; }
            private set
            {
                _selectedLocations = value;
            }
        }

        /// <summary>
        /// Returns the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            private set
            {
                _dialogResult = value;

                if (this.AttachedControl != null)
                {
                    this.AttachedControl.DialogResult = value;
                }
            }
        }

        /// <summary>
        /// Gets/Sets the created package id.
        /// </summary>
        public Int32? CreatedPackageId
        {
            get { return _createdPackageId; }
            private set { _createdPackageId = value; }
        }

        /// <summary>
        /// Returns the current step of the wizard
        /// </summary>
        public WizardStep CurrentWizardStep
        {
            get { return _currentWizardStep; }
            private set
            {
                _currentWizardStep = value;
                //    OnPropertyChanged(CurrentWizardStepProperty);

                OnCurrentWizardStepChanged(value);
            }
        }

        /// <summary>
        /// Returns the viewmodel for the current wizard step
        /// </summary>
        public IWizardStepViewModel CurrentStepViewModel
        {
            get { return _currentStepViewModel; }
            private set
            {
                IWizardStepViewModel oldValue = _currentStepViewModel;

                _currentStepViewModel = value;
                OnPropertyChanged(CurrentStepViewModelProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public LocationSpaceAddLocationsWizardWindowViewModel(ObservableCollection<LocationSpaceProductGroupRowViewModel> locationSpaceCategory, LocationList masterLocationList)
        {
            _masterLocationList = masterLocationList;

            if (_masterLocationList == null)
            {
                _masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);
            }

            //locationspace
            this.SelectedLocationSpaceCategory = locationSpaceCategory;

            //First step
            OnCurrentWizardStepChanged(WizardStep.AddLocations);
        }

        #endregion

        #region Commands

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// Advances the wizard to the next step.
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        private Boolean Next_CanExecute()
        {
            //must not be on the last step
            if (this.CurrentWizardStep == WizardStep.ApplyLocationsAdded || this.SelectedLocations.Count == 0)
            {
                this.NextCommand.DisabledReason = null;
                return false;
            }

            //the current step must be valid
            if (!this.CurrentStepViewModel.IsValidAndComplete)
            {
                this.NextCommand.DisabledReason = this.CurrentStepViewModel.StepDisabledReason;
                return false;
            }

            return true;
        }

        private void Next_Executed()
        {
            base.ShowWaitCursor(true);

            //Complete the previous step.
            this.CurrentStepViewModel.CompleteStep();

            //select the next one.
            WizardStep nextWizardStep = WizardStep.ApplyLocationsAdded;

            this.CurrentWizardStep = nextWizardStep;

            //force a requery.
            this.NextCommand.RaiseCanExecuteChanged();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Finish

        private RelayCommand _finishCommand;

        /// <summary>
        /// Advances the wizard to the finish step.
        /// </summary>
        public RelayCommand FinishCommand
        {
            get
            {
                if (_finishCommand == null)
                {
                    _finishCommand = new RelayCommand(
                        p => Finish_Executed(),
                        p => Finish_CanExecute())
                    {
                        FriendlyName = Message.Generic_Finish,
                    };
                    base.ViewModelCommands.Add(_finishCommand);
                }
                return _finishCommand;
            }
        }

        private Boolean Finish_CanExecute()
        {
            //must be on the last step
            if (this.CurrentWizardStep != WizardStep.ApplyLocationsAdded)
            {
                return false;
            }

            //must be complete
            if (!this.CurrentStepViewModel.IsValidAndComplete)
            {
                return false;
            }

            return true;
        }

        private void Finish_Executed()
        {
            base.ShowWaitCursor(true);

            //complete the step
            this.CurrentStepViewModel.CompleteStep();

            base.ShowWaitCursor(false);

            //close this window
            this.DialogResult = true;

        }

        #endregion

        #region Previous

        private RelayCommand _previousCommand;

        /// <summary>
        /// Advances the wizard to the previous step.
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                    {
                        FriendlyName = Message.Generic_Previous,
                    };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        private Boolean Previous_CanExecute()
        {
            //must not be on the first step.
            if (this.CurrentWizardStep == WizardStep.AddLocations)
            {
                return false;
            }

            return true;
        }

        private void Previous_Executed()
        {
            WizardStep prevStep = WizardStep.AddLocations;
            this.CurrentWizardStep = prevStep;

        }

        #endregion

        #region Cancel

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Advances the wizard to the cancel step.
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed())
                    {
                        FriendlyName = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            this.DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods
        public new void ShowWaitCursor(bool show)
        {
            base.ShowWaitCursor(show);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever the current wizard step changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnCurrentWizardStepChanged(WizardStep newValue)
        {
            base.ShowWaitCursor(true);

            //load the correct viewmodel
            switch (newValue)
            {
                case WizardStep.AddLocations:
                    this.CurrentStepViewModel = new LocationSpaceAddLocationsViewModel(this.SelectedLocationSpaceCategory, this.MasterLocationList, this.SelectedLocations);
                    break;

                case WizardStep.ApplyLocationsAdded:
                    this.CurrentStepViewModel = new LocationSpaceApplyLocationsViewModel(this.SelectedLocationSpaceCategory, this.MasterLocationList, this.SelectedLocations);
                    break;

                default: throw new NotImplementedException();

            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region IDisposable

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.CurrentStepViewModel = null;
                }

                base.IsDisposed = true;
            }
        }

        #endregion
    }
    #region Suppporting Classes

    /// <summary>
    /// Denotes the different steps that the 
    /// wizard may go through.
    /// </summary>
    public enum WizardStep
    {
        AddLocations = 0,
        ApplyLocationsAdded = 1,

    }

    #endregion
}
