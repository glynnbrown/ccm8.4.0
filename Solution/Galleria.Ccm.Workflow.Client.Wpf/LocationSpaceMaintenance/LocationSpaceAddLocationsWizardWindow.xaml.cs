﻿using Galleria.Framework.Controls.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.WorkPackages.WorkpackageWizard;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpaceAddLocationsWizardWindow.xaml
    /// </summary>
    public partial class LocationSpaceAddLocationsWizardWindow : ExtendedRibbonWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationSpaceAddLocationsWizardWindowViewModel), typeof(LocationSpaceAddLocationsWizardWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceAddLocationsWizardWindow senderControl = (LocationSpaceAddLocationsWizardWindow)obj;

            if (e.OldValue != null)
            {
                LocationSpaceAddLocationsWizardWindowViewModel oldModel = (LocationSpaceAddLocationsWizardWindowViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                LocationSpaceAddLocationsWizardWindowViewModel newModel = (LocationSpaceAddLocationsWizardWindowViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }

            senderControl.SetContentPresenterTemplate();
        }

        /// <summary>
        /// Gets/Sets the viewmodel context
        /// </summary>
        public LocationSpaceAddLocationsWizardWindowViewModel ViewModel
        {
            get { return (LocationSpaceAddLocationsWizardWindowViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out initial load actions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationSpaceWizard_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationSpaceWizard_Loaded;
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }), priority: DispatcherPriority.Background);
        }

        /// <summary>
        /// Called whenever a property changes on the viewmodel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationSpaceAddLocationsWizardWindowViewModel.CurrentStepViewModelProperty.Path)
            {
                SetContentPresenterTemplate();
            }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public LocationSpaceAddLocationsWizardWindow(LocationSpaceAddLocationsWizardWindowViewModel viewModel)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = viewModel;

            this.Loaded += LocationSpaceWizard_Loaded;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Updates the template used by the step content presenter
        /// </summary>
        private void SetContentPresenterTemplate()
        {
            if (this.StepContentPresenter != null)
            {
                IDisposable oldContent = this.StepContentPresenter.Content as IDisposable;

                FrameworkElement newContent = null;

                if (this.ViewModel != null)
                {

                    switch (this.ViewModel.CurrentWizardStep)
                    {
                        case WizardStep.AddLocations:
                            newContent = new LocationSpaceAddLocations(this.ViewModel.SelectedLocationSpaceCategory, this.ViewModel.MasterLocationList, this.ViewModel.SelectedLocations);
                            break;

                        case WizardStep.ApplyLocationsAdded:
                            newContent = new LocationSpaceApplyLocations(this.ViewModel.SelectedLocationSpaceCategory, this.ViewModel.MasterLocationList, this.ViewModel.SelectedLocations);
                            break;
                    }
                }

                this.StepContentPresenter.Content = newContent;

                //clear off the old content.
                if (oldContent != null)
                {
                    oldContent.Dispose();
                }
            }
        }

        #endregion

        #region Window close

        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    IDisposable disposableViewModel = (IDisposable)this.ViewModel;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }
        }

        #endregion
    }
}
