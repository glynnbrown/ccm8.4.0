﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.ComponentModel;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using System.Windows.Threading;
using System.Diagnostics;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpaceMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class LocationSpaceMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(LocationSpaceMaintenanceViewModel), typeof(LocationSpaceMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public LocationSpaceMaintenanceViewModel ViewModel
        {
            get { return (LocationSpaceMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceMaintenanceOrganiser senderControl = (LocationSpaceMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                LocationSpaceMaintenanceViewModel oldModel = (LocationSpaceMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.GroupByCommand.Executed -= senderControl.GroupByCommand_Executed;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
            }

            if (e.NewValue != null)
            {
                LocationSpaceMaintenanceViewModel newModel = (LocationSpaceMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.GroupByCommand.Executed += senderControl.GroupByCommand_Executed;
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationSpaceMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.LocationSpaceMaintenance);

            this.ViewModel = new LocationSpaceMaintenanceViewModel();

            this.Loaded += new RoutedEventHandler(LocationSpaceMaintenanceOrganiser_Loaded);
        }



        /// <summary>
        /// Loaded event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationSpaceMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationSpaceMaintenanceOrganiser_Loaded;

            ResetLocationSpaceGrid();


            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }



        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);


            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.backstageOpenTab.IsSelected = true;
            }

        }

        /// <summary>
        /// Responds to viewmodel property changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationSpaceMaintenanceViewModel.SelectedLocationSpaceProperty.Path)
            {
                ResetLocationSpaceGrid();
            }
        }



        /// <summary>
        /// Double click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExtendedDataGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.EditCommand.Execute();
            }
        }

        private void xLocationSpaceGrid_PrefilterItem(object sender, ExtendedDataGridFilterEventArgs e)
        {
            if (this.xLocationSpaceGrid != null)
            {
                String prefilter = this.xLocationSpaceGrid.PrefilterText;
                if (!String.IsNullOrEmpty(prefilter))
                {
                    LocationSpaceProductGroupRowViewModel row = e.Item as LocationSpaceProductGroupRowViewModel;
                    if (row != null && row.LocationSpaceProductGroup != null)
                    {
                        if (row.GroupNames.Count > 0)
                        {
                            String productGroupDesc = row.GroupNames.Where(s => !String.IsNullOrEmpty(s)).Last();

                            productGroupDesc = productGroupDesc.ToLowerInvariant();

                            if (!productGroupDesc.Contains(prefilter.ToLowerInvariant()))
                            {
                                e.Accepted = false;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// GroupBy command executed event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GroupByCommand_Executed(object sender, EventArgs e)
        {
            RelayCommand<String> senderCommand = (RelayCommand<String>)sender;
            String parameter = (String)senderCommand.Parameter;

            //Clear any old descriptions
            xLocationSpaceGrid.GroupByDescriptions.Clear();
            xLocationSpaceSliderGrid.GroupByDescriptions.Clear();

            if (parameter != LocationSpaceGroupByType.None.ToString())
            {
                //Lookup hierarchy level selected and find in group names collection
                List<ProductLevel> levels = this.ViewModel.MasterProductHierarchyView.Model.EnumerateAllLevels().Where(p => p.IsRoot == false).ToList();
                //Selected level
                ProductLevel selectedLevel = levels.FirstOrDefault(p => p.Name == parameter);
                //Get index of chosen level
                Int32 index = (selectedLevel != null) ? levels.IndexOf(selectedLevel) : 0;

                //Create group by descriptions
                xLocationSpaceGrid.GroupByDescriptions.Add(new PropertyGroupDescription(String.Format("{0}[{1}]", LocationSpaceProductGroupRowViewModel.GroupNamesProperty.Path, index)));
                xLocationSpaceSliderGrid.GroupByDescriptions.Add(new PropertyGroupDescription(String.Format("{0}[{1}]", LocationSpaceProductGroupRowViewModel.GroupNamesProperty.Path, index)));
            }

        }
        private void XLocationSpaceGrid_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                this.ViewModel.EditCommand.Execute();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Resets the filters and sorts on the main grid
        /// </summary>
        private void ResetLocationSpaceGrid()
        {
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();

            ExtendedDataGrid locSpaceGrid = this.xLocationSpaceGrid;

            if (locSpaceGrid != null
                && this.ViewModel != null)
            {
                if (locSpaceGrid.Columns.Count == 0)
                {
                    //Create column set
                    foreach (DataGridColumn col in this.ViewModel.GenerateColumnSet())
                    {
                        xLocationSpaceGrid.Columns.Add(col);
                    }
                }

                //clear all filters
                locSpaceGrid.ClearFilterValues();

                //clear all group by
                if (locSpaceGrid.GroupByDescriptions.Count > 0)
                {
                    locSpaceGrid.GroupByDescriptions.Clear();
                }

                //clear all sorts
                if (locSpaceGrid.SortByDescriptions.Count > 0)
                {
                    //locSpaceGrid.CommitEdit();
                    locSpaceGrid.SortByDescriptions.Clear();
                }

                foreach (DataGridTextColumn col in locSpaceGrid.Columns)
                {
                    //all columns should be visible
                    if (col.Visibility != System.Windows.Visibility.Visible)
                    {
                        col.Visibility = System.Windows.Visibility.Visible;
                    }

                    //apply sorts to level columns
                    String bindingPath = ExtendedDataGrid.GetBindingProperty(col);
                    if (bindingPath.Contains(LocationSpaceProductGroupRowViewModel.GroupNamesProperty.Path))
                    {
                        locSpaceGrid.SortByDescriptions.Add(new SortDescription(bindingPath, ListSortDirection.Ascending));
                        //col.SortDirection = ListSortDirection.Ascending;
                    }
                }
            }

            //stopwatch.Stop();
            //Console.WriteLine("ResetLocationSpaceGrid took {0} milliseconds to execute", stopwatch.ElapsedMilliseconds);
        }

        #endregion

        #region Window close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion

       
    }
}
