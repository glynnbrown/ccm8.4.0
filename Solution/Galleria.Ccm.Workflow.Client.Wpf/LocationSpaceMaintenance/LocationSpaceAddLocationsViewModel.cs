﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
   public class LocationSpaceAddLocationsViewModel : ViewModelAttachedControlObject<LocationSpaceAddLocations>, IWizardStepViewModel
    {
        #region Fields

        private ObservableCollection<LocationSpaceProductGroupRowViewModel> _selectedLocationSpaceCategory = new ObservableCollection<LocationSpaceProductGroupRowViewModel>();
        private ObservableCollection<Location> _selectedLocations = new ObservableCollection<Location>();

        private LocationList _masterLocationList;
        private int _categoryCount;
        private BulkObservableCollection<Location> _availableLocations = new BulkObservableCollection<Location>();
        private ReadOnlyBulkObservableCollection<Location> _availableLocationsRO;
        private ObservableCollection<Location> _selectedAvailableLocations = new ObservableCollection<Location>();

        private ObservableCollection<Location> _selectedLocationsToRemove = new ObservableCollection<Location>();


        #endregion

        #region Binding properties

        //properties
        public static readonly PropertyPath SelectedLocationSpaceCategoryProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsViewModel>(p => p.SelectedLocationSpaceCategory);
        public static readonly PropertyPath SelectedLocationsProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsViewModel>(p => p.SelectedLocations);

        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath SelectedAvailableLocationsProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsViewModel>(p => p.SelectedAvailableLocations);
        public static readonly PropertyPath SelectedLocationsToRemoveProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsViewModel>(p => p.SelectedLocationsToRemove);

        //commands
        public static readonly PropertyPath AddLocationsCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsViewModel>(p => p.AddLocationsCommand);
        public static readonly PropertyPath RemoveLocationCommandProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsViewModel>(p => p.RemoveLocationCommand);


        public static readonly PropertyPath CategoryCountProperty = WpfHelper.GetPropertyPath<LocationSpaceAddLocationsViewModel>(p => p.CategoryCount);
        #endregion

        #region Properties

        /// <summary>
        /// Get/Sets the category count
        /// </summary>
        public Int32 CategoryCount
        {
            get { return _categoryCount; }
            set
            {
                _categoryCount = value;
                OnPropertyChanged(CategoryCountProperty);
            }
        }

        /// <summary>
        /// Property for the selected Loca object
        /// </summary>
        public ObservableCollection<LocationSpaceProductGroupRowViewModel> SelectedLocationSpaceCategory
        {
            get { return _selectedLocationSpaceCategory; }
            private set
            {
                _selectedLocationSpaceCategory = value;
                OnPropertyChanged(SelectedLocationSpaceCategoryProperty);

                OnSelectedLocationSpaceChanged();
            }
        }

        public ObservableCollection<Location> SelectedLocations
        {
            get { return _selectedLocations; }
            private set
            {
                _selectedLocations = value;
                OnPropertyChanged(SelectedLocationsProperty);
            }
        }

        public LocationList MasterLocationList
        {
            get { return _masterLocationList; }
            private set { _masterLocationList = value; }
        }

        /// <summary>
        /// Gets the list of all locations
        /// </summary>
        public ReadOnlyBulkObservableCollection<Location> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyBulkObservableCollection<Location>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected locations
        /// </summary>
        public ObservableCollection<Location> SelectedAvailableLocations
        {
            get { return _selectedAvailableLocations; }
            set
            {
                _selectedAvailableLocations = value;
            }
        }

        /// <summary>
        /// Returns the editable collection of selected locations
        /// </summary>
        public ObservableCollection<Location> SelectedLocationsToRemove
        {
            get { return _selectedLocationsToRemove; }
            set { _selectedLocationsToRemove = value; }
        }

        /// <summary>
        /// Returns true if this step is valid
        /// and ready to proceed
        /// </summary>
        public Boolean IsValidAndComplete
        {
            get
            {
                //No action required here

                return true;
            }
        }

        public String StepDisabledReason
        {
            get { return "Step not completed"; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="LocationSpace"></param>
        public LocationSpaceAddLocationsViewModel(ObservableCollection<LocationSpaceProductGroupRowViewModel> locationSpaceCategory, LocationList masterLocationList, ObservableCollection<Location> selectedLocations)
        {
            //locations
            _masterLocationList = masterLocationList;

            if (_masterLocationList == null)
            {
                _masterLocationList = LocationList.FetchByEntityId(App.ViewState.EntityId);
            }

            //locationspace
            this.SelectedLocationSpaceCategory = locationSpaceCategory;
            this.SelectedLocations = selectedLocations;

            _categoryCount = this.SelectedLocationSpaceCategory.Count;
        }

        #endregion

        #region Commands

        #region AddLocationsCommand

        private RelayCommand _addLocationsCommand;

        /// <summary>
        /// adds the specified locations to the content
        /// </summary>
        public RelayCommand AddLocationsCommand
        {
            get
            {
                if (_addLocationsCommand == null)
                {
                    _addLocationsCommand = new RelayCommand(
                        p => AddLocations_Executed(),
                        p => AddLocations_CanExecute())
                    {
                        FriendlyName = Message.AssortmentSetup_AddLocationsButton_Caption,
                        SmallIcon = ImageResources.Add_16
                    };
                    base.ViewModelCommands.Add(_addLocationsCommand);
                }
                return _addLocationsCommand;
            }
        }

        private Boolean AddLocations_CanExecute()
        {
            if (this.SelectedAvailableLocations.Count == 0)
            {
                return false;
            }

            return true;
        }

        private void AddLocations_Executed()
        {
            base.ShowWaitCursor(true);

            List<Location> addList = this.SelectedAvailableLocations.ToList();

            //clear for speed
            this.SelectedAvailableLocations.Clear();

            //remove from the available
            _availableLocations.RemoveRange(addList);

            foreach (Location location in addList)
            {
                this.SelectedLocations.Add(location);
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region RemoveLocationCommand

        private RelayCommand _removeLocationCommand;

        /// <summary>
        /// Removes a location from list to add to project
        /// </summary>
        public RelayCommand RemoveLocationCommand
        {
            get
            {
                if (_removeLocationCommand == null)
                {
                    _removeLocationCommand = new RelayCommand(
                        p => RemoveLocation_Executed(),
                        p => RemoveLocation_CanExecute())
                    {
                        FriendlyName = Message.ProjectContentManagement_AddLocations_RemoveSelection,
                        SmallIcon = ImageResources.Delete_16
                    };
                    this.ViewModelCommands.Add(_removeLocationCommand);
                }
                return _removeLocationCommand;
            }
        }

        private Boolean RemoveLocation_CanExecute()
        {
            if (this.SelectedLocationsToRemove.Count == 0)
            {
                return false;
            }
            return true;
        }

        private void RemoveLocation_Executed()
        {
            base.ShowWaitCursor(true);

            List<Location> removeList = this.SelectedLocationsToRemove.ToList();

            //clear out the selection for speed
            this.SelectedLocationsToRemove.Clear();

            //remove from assigned
            foreach (Location location in removeList)
            {
                this.SelectedLocations.Remove(location);
            }

            //add back to the available
            _availableLocations.AddRange(removeList);

            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Event handlers

        /// <summary>
        /// Responds to a change of selected assortment
        /// </summary>
        private void OnSelectedLocationSpaceChanged()
        {
            ResetLocationsLists();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Resets the added locaiotns list to hold the ones already in the current project content's 
        /// locations list - all newly added items not saved will be removed
        /// </summary>
        private void ResetLocationsLists()
        {
            if (_selectedAvailableLocations.Count > 0) { _selectedAvailableLocations.Clear(); }
            if (_availableLocations.Count > 0) { _availableLocations.Clear(); }
            if (_selectedLocationsToRemove.Count > 0) { _selectedLocationsToRemove.Clear(); }

            foreach (Location loc in _masterLocationList)
            {
                _availableLocations.Add(loc);
            }

        }

        /// <summary>
        /// Carries out actions to finalize 
        /// the selections made in this step
        /// </summary>
        public void CompleteStep()
        {
            //No work required here, we are moving to the next screen
        }

        #endregion

        #region IWizardStepViewModel Members

        public String StepHeader
        {
            get { return "Add Locations"; }
        }

        public String StepDescription
        {
            get { return "Add categories to other locations"; }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _masterLocationList = null;
                    _selectedAvailableLocations.Clear();
                    _selectedLocationsToRemove.Clear();
                    _selectedLocationSpaceCategory = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
