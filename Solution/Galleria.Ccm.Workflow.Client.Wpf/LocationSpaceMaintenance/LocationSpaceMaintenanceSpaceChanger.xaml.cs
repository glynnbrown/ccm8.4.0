﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using System.Globalization;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpaceMaintenanceSpaceChanger.xaml
    /// </summary>
    public partial class LocationSpaceMaintenanceSpaceChanger : UserControl
    {
        #region Resource Name Keys

        public static readonly String StyNumberText = "LocationSpaceChanger_StyNumberText"; //NumberTextStyle
        public static readonly String StySpacebar = "LocationSpaceChanger_StySpacebar"; //SpaceBarStyle
        public static readonly String TmpHeaderToolTip = "LocationSpaceChanger_TmpHeaderToolTip";//Template_HeaderToolTip

        #endregion

        #region Fields
        private const int _columnWidth = 13;
        private const int _dragChangeBuffer = 25;
        private const Int32 _maxSliderValue = 50;
        private Control _spaceBar; //the space bar visual control to be resized
        private Thumb _spaceBarThumb; //the move thumb
        private List<TextBlock> _numberLabels = new List<TextBlock>();
        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationSpaceProductGroupRowViewModel), typeof(LocationSpaceMaintenanceSpaceChanger),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceMaintenanceSpaceChanger senderControl = (LocationSpaceMaintenanceSpaceChanger)obj;

            if (e.OldValue != null)
            {
                LocationSpaceProductGroupRowViewModel oldModel = (LocationSpaceProductGroupRowViewModel)e.OldValue;
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.Disposed -= senderControl.ViewModel_Disposed;

                senderControl.Span = 0;
            }

            if (e.NewValue != null)
            {
                LocationSpaceProductGroupRowViewModel newModel = (LocationSpaceProductGroupRowViewModel)e.NewValue;
                newModel.PropertyChanged += new PropertyChangedEventHandler(senderControl.ViewModel_PropertyChanged);
                newModel.Disposed += new EventHandler(senderControl.ViewModel_Disposed);

                //initialise the control span property
                senderControl.Span = (int)(newModel.BayCount * 2) + 1;
            }

            senderControl.Draw();
        }

        /// <summary>
        /// Gets/Sets the control viewmodel
        /// </summary>
        public LocationSpaceProductGroupRowViewModel ViewModel
        {
            get { return (LocationSpaceProductGroupRowViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Span Property

        public static readonly DependencyProperty SpanProperty =
            DependencyProperty.Register("Span", typeof(int), typeof(LocationSpaceMaintenanceSpaceChanger),
            new PropertyMetadata(1, OnSpanPropertyChanged, new CoerceValueCallback(OnSpanPropertyCoerce)));

        private static object OnSpanPropertyCoerce(DependencyObject obj, object value)
        {
            LocationSpaceMaintenanceSpaceChanger senderControl = (LocationSpaceMaintenanceSpaceChanger)obj;
            int currentSpan = senderControl.Span;
            int requestedSpan = (int)value;


            //if the span has not changed just return it back out
            if (currentSpan == requestedSpan) { return value; }


            //get the bayValue the new span represents
            float requestedNewBayValue = (float)((requestedSpan - 1) / 2.0);

            if (senderControl.ViewModel != null)
            {
                //correct the span if its below 0 or above the max
                int maxAllowedSpan = (int)(senderControl.ViewModel.MaxDrawSpacebreak * 2) + 1;
                if (requestedSpan < 1) { requestedSpan = 1; }
                else if (requestedSpan > maxAllowedSpan) { requestedSpan = maxAllowedSpan; }
            }

            //correct the requested span
            if (requestedSpan < 1)
            {
                requestedSpan = 1;
            }

            //return the requested span
            return requestedSpan;
        }

        private static void OnSpanPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationSpaceMaintenanceSpaceChanger senderControl = (LocationSpaceMaintenanceSpaceChanger)obj;

            Int32 newValue = (Int32)e.NewValue;

            //calculate the new break value
            Single newBayValue = (Single)((newValue - 1) / 2.0);

            if (senderControl.ViewModel != null)
            {
                if (senderControl.ViewModel.BayCount != newBayValue)
                {
                    senderControl.ViewModel.BayCount = newBayValue;
                }
            }
        }

        public int Span
        {
            get { return (int)GetValue(SpanProperty); }
            set { SetValue(SpanProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        ///<summary>
        ///Constructor
        ///</summary>
        public LocationSpaceMaintenanceSpaceChanger()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to property changes on the view model
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == LocationSpaceProductGroupRowViewModel.MaxDrawSpacebreakProperty.Path)
            {
                //redraw
                Draw();
            }
            else if (e.PropertyName == LocationSpaceProductGroupRowViewModel.BayCountProperty.Path)
            {
                this.Span = (int)(this.ViewModel.BayCount * 2) + 1;
            }
        }

        /// <summary>
        /// Dettaches from the viewmodel when it is disposed of.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_Disposed(object sender, EventArgs e)
        {
            //dettach from the view model.
            this.ViewModel = null;
        }


        /// <summary>
        /// Handles the load complete of the spacebar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _spaceBar_Loaded(object sender, RoutedEventArgs e)
        {
            _spaceBar.Loaded -= _spaceBar_Loaded;

            _spaceBar.ApplyTemplate();

            //attach the thumb events
            _spaceBarThumb = _spaceBar.Template.FindName("PART_BarThumb", _spaceBar) as Thumb;
            if (_spaceBarThumb != null)
            {
                _spaceBarThumb.DragDelta += new DragDeltaEventHandler(_spaceBarThumb_DragDelta);
            }

        }

        /// <summary>
        /// Handles the drag event of the space bar thumb
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _spaceBarThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            int currentSpan = this.Span;

            //get the requested new span
            int newSpan = currentSpan;

            if (e.HorizontalChange < 0 && currentSpan > 50)
            {
                this.Span = 50;
            }
            else if (newSpan <= 50)
            {
                if (e.HorizontalChange > _dragChangeBuffer)
                {
                    newSpan = currentSpan + 1;
                }
                else if (e.HorizontalChange < -_dragChangeBuffer)
                {
                    newSpan = currentSpan - 1;
                }

                //set the new span value
                this.Span = newSpan;
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Draws the space changer
        /// </summary>
        private void Draw()
        {
            //clear the layout area
            LayoutArea.Children.Clear();
            LayoutArea.ColumnDefinitions.Clear();
            LayoutArea.RowDefinitions.Clear();


            if (this.ViewModel != null)
            {
                //create 2 rows (top = labels, bottom = bar)
                for (int i = 0; i < 2; i++)
                {
                    RowDefinition rd = new RowDefinition();

                    rd.Height = (i == 0) ?
                        new GridLength(1, GridUnitType.Auto) : new GridLength(1, GridUnitType.Star);

                    LayoutArea.RowDefinitions.Add(rd);
                }

                //add a 0 column
                ColumnDefinition autoCol = new ColumnDefinition();
                autoCol.Width = new GridLength(1, GridUnitType.Auto);
                LayoutArea.ColumnDefinitions.Add(autoCol);

                //add (_maxSpaceBreak) columns * 1
                int requiredCols = Math.Min((int)(this.ViewModel.MaxDrawSpacebreak * 2), 50);
                for (int i = 0; i < requiredCols; i++)
                {
                    ColumnDefinition cd = new ColumnDefinition();
                    cd.Width = new GridLength(_columnWidth);
                    LayoutArea.ColumnDefinitions.Add(cd);
                }


                //for every second column in row 0 add a number label
                _numberLabels.Clear();
                for (int i = 0; i < LayoutArea.ColumnDefinitions.Count; i += 2)
                {
                    TextBlock numTxt = new TextBlock();
                    numTxt.Style = (Style)this.Resources[StyNumberText];
                    numTxt.Text = (i / 2).ToString(CultureInfo.InvariantCulture);
                    Grid.SetColumn(numTxt, i);
                    LayoutArea.Children.Add(numTxt);
                    _numberLabels.Add(numTxt);
                }


                //create the drag thumb
                _spaceBar = new Control();
                _spaceBar.Style = (Style)this.Resources[StySpacebar];
                _spaceBar.HorizontalAlignment = HorizontalAlignment.Stretch;
                _spaceBar.VerticalAlignment = VerticalAlignment.Stretch;
                Grid.SetRow(_spaceBar, 1);
                Grid.SetColumn(_spaceBar, 0);
                _spaceBar.Loaded += new RoutedEventHandler(_spaceBar_Loaded);
                LayoutArea.Children.Add(_spaceBar);


                //set the bar colspan to the proposed bays value
                _spaceBar.SetBinding(Grid.ColumnSpanProperty, new Binding(SpanProperty.Name) { Source = this });
            }

        }

        /// <summary>
        /// Places the specified marker on the space changer
        /// </summary>
        /// <param name="marker"></param>
        /// <param name="markerValue"></param>
        /// <param name="verticalAlign"></param>
        private void PlaceMarker(Image marker, float markerValue, VerticalAlignment verticalAlign)
        {
            //set the tooltip to show the value for testing
            marker.ToolTip = markerValue.ToString(CultureInfo.InvariantCulture);

            //place the marker on the grid using the value as the column

            //remove it from the grid first to avoid issues
            LayoutCanvas.Children.Remove(marker);

            marker.IsHitTestVisible = false;
            marker.Margin = new Thickness(0, 0, -4, 0);
            marker.HorizontalAlignment = HorizontalAlignment.Center;
            marker.VerticalAlignment = verticalAlign;

            Canvas.SetLeft(marker, (markerValue * (_columnWidth * 2)));
            Canvas.SetBottom(marker, 5);
            LayoutCanvas.Children.Add(marker);
        }

        #endregion
    }
}
