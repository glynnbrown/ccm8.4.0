﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common.LocationSpaceUserControls;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationSpaceMaintenance
{
    /// <summary>
    /// Interaction logic for LocationSpaceMaintenanceProperties.xaml
    /// </summary>
    public partial class LocationSpaceMaintenanceProperties : ExtendedRibbonWindow
    {

        public LocationSpaceBayVisual SelectedLocationSpaceBayVisual { get; set; }

        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationSpaceMaintenanceViewModel), typeof(LocationSpaceMaintenanceProperties),
                            new PropertyMetadata(null, OnViewModelPropertyChanged));
        public LocationSpaceMaintenanceViewModel ViewModel
        {
            get { return (LocationSpaceMaintenanceViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = obj as LocationSpaceMaintenanceProperties;
            if (senderControl == null) return;

            var oldViewModel = e.OldValue as LocationSpaceMaintenanceViewModel;
            if (oldViewModel != null) oldViewModel.AttachedControl = null;

            var newViewModel = e.NewValue as LocationSpaceMaintenanceViewModel;
            if (newViewModel != null) newViewModel.AttachedControl = senderControl.ViewModel.AttachedControl;
            if (newViewModel != null) newViewModel.BaysUpdated += senderControl.ViewModelOnBaysUpdated;

            senderControl.UpdateFixtureStack();
        }

        #endregion

        #region BayVisualsProperty

        public static readonly DependencyProperty BayVisualsProperty = DependencyProperty.Register(nameof(BayVisuals),
            typeof(List<LocationSpaceBayVisual>), typeof(LocationSpaceMaintenanceProperties),
            new PropertyMetadata(null));

        public List<LocationSpaceBayVisual> BayVisuals
        {
            get { return (List<LocationSpaceBayVisual>)GetValue(BayVisualsProperty); }
            set { SetValue(BayVisualsProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///// Constructor
        /// </summary>
        /// <param name="viewModel"></param>
        public LocationSpaceMaintenanceProperties(LocationSpaceMaintenanceViewModel viewModel)
        {
            InitializeComponent();
            BayVisuals = new List<LocationSpaceBayVisual>();

            //ViewModel gets re-initialsed onclosed
            this.ViewModel = viewModel;

            this.AddHandler(LocationSpaceBayVisual.SelectedEvent, (RoutedEventHandler)LocationSpaceBayVisualOnSelected);

            //Subscribe to the ok command being executed
            this.ViewModel.OkCommand.Executed += new EventHandler(OkCommand_Executed);

            advancedLocationSpacePositionDetailPanel.ViewModel = ViewModel;

            advancedLocationSpacePropertiesPanel.ViewModel = ViewModel;

            advancedLocationSpaceFixtureDetailPanel.ViewModel = ViewModel;

            this.Loaded += new RoutedEventHandler(LocationSpaceProperties_Loaded);
        }

        public void LocationSpaceBayVisualOnSelected(Object sender, RoutedEventArgs e)
        {
            var visual = e.OriginalSource as LocationSpaceBayVisual;
            if (visual == null) return;

            if (!Object.Equals(SelectedLocationSpaceBayVisual, visual)) SelectedLocationSpaceBayVisual.IsSelected = false;
            SelectedLocationSpaceBayVisual = visual;
            ViewModel.SelectedLocationSpaceBay = SelectedLocationSpaceBayVisual.LocationSpaceBay;
        }

        void LocationSpaceProperties_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationSpaceProperties_Loaded;
        }

        /// <summary>
        /// OK Command executed event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkCommand_Executed(object sender, EventArgs e)
        {
            //set the result (will close the window)
            this.DialogResult = true;
        }

        #endregion


        #region Methods

        /// <summary>
        /// Shows the context menu for the given fixture package view
        /// </summary>
        /// <param name="fixturePackageView"></param>
        private void ShowContextMenu(LocationSpaceBay fixtureView)
        {
            Fluent.ContextMenu contextMenu = new Fluent.ContextMenu();
            RelayCommand cmd;

            cmd = this.ViewModel.MoveBayLeftCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            cmd = this.ViewModel.MoveBayRightCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            contextMenu.Items.Add(new Separator());

            cmd = this.ViewModel.DuplicateBayCommand;
            contextMenu.Items.Add(new Fluent.MenuItem() { Command = cmd, Header = cmd.FriendlyName, Icon = cmd.SmallIcon, });

            contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint;
            contextMenu.IsOpen = true;
        }


        public void ViewModelOnBaysUpdated(Object sender, EventArgs eventArgs)
        {
            UpdateFixtureStack();
        }

        private void UpdateFixtureStack()
        {
            ClearOldVisuals();

            if (ViewModel?.CurrentLocationSpaceProductGroup == null) return;

            ViewModel.ReOrderBayNumbers();

            IEnumerable<LocationSpaceBay> orderedLocationSpaceBays =
                 ViewModel.CurrentLocationSpaceProductGroup.Bays.OrderBy(v => v.Order);
            BayVisuals = orderedLocationSpaceBays.Select(i => new LocationSpaceBayVisual(i)).ToList();
            Byte selectedIndex = ViewModel.SelectedLocationSpaceBay.Order;

            LocationSpaceBayVisual selectedVisual =
                BayVisuals.FirstOrDefault(visual => visual.LocationSpaceBay.Order == selectedIndex);
            if (selectedVisual == null) return;

            selectedVisual.IsSelected = true;
            SelectedLocationSpaceBayVisual = selectedVisual;
        }

        public void ClearOldVisuals()
        {

            List<LocationSpaceBayVisual> oldVisuals = BayVisuals.ToList();

            foreach (var item in BayVisuals.ToList())
            {
                if (BayVisuals.Contains(item))
                {
                    BayVisuals.Remove(item);
                }
            }

            foreach (LocationSpaceBayVisual old in oldVisuals)
            {
                old.Dispose();
            }
        }

        #endregion

        #region Event Handlers

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                this.ViewModel.OkCommand.Executed -= OkCommand_Executed;
            }

        }

        /// <summary>
        /// Cancel view model changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            //set the result (will close the window)
            this.DialogResult = false;
        }

        /// <summary>
        /// Called when a preview mouse right button down on the viewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xFixturesStack_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            LocationSpaceBayVisual senderControl = ((DependencyObject)e.OriginalSource).FindVisualAncestor<LocationSpaceBayVisual>();

            if (this.ViewModel != null)
            {
                this.ViewModel.SelectedLocationSpaceBay = senderControl.LocationSpaceBay;

                if (Keyboard.Modifiers == ModifierKeys.None)
                {
                    ShowContextMenu(this.ViewModel.SelectedLocationSpaceBay);
                }

                e.Handled = true;
            }
        }

        #endregion


        /// <summary>
        /// Re-initialse viewmodel onclosed
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            ClearOldVisuals();

            base.OnClosed(e);
        }
    }
}
