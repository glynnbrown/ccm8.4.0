﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26158 : I.George
//	Initial version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.MetricMaintenance
{
    /// <summary>
    /// Interaction logic for MetricWindow.xaml
    /// </summary>
    public partial class MetricWindow : ExtendedRibbonWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MetricMaintenanceViewModel), typeof(MetricWindow),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        public static void ViewModel_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MetricWindow senderControl = (MetricWindow)obj;
            if (e.OldValue != null)
            {
                MetricMaintenanceViewModel oldModel = (MetricMaintenanceViewModel)e.OldValue;

                //if the old model is in edit mode - cancel
                if (oldModel.IsInMetricEditMode)
                {
                    oldModel.CancelExternalLinkSetupEdit();
                }

                oldModel.CloseWindow -= senderControl.ViewModel_CloseWindow;
            }

            if(e.NewValue != null)
            {
                MetricMaintenanceViewModel newModel = (MetricMaintenanceViewModel)e.NewValue;

                //begin an edit on the new item
                newModel.BeginMetricEdit();

                newModel.CloseWindow += senderControl.ViewModel_CloseWindow;
            }
        }

        public MetricMaintenanceViewModel ViewModel
        {
            get { return (MetricMaintenanceViewModel)GetValue(ViewModelProperty);}
            private set { SetValue(ViewModelProperty, value); }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="metricMaintenanceViewModel"></param>
        public MetricWindow(MetricMaintenanceViewModel metricMaintenanceViewModel)
        {
            InitializeComponent();

            //set the view model
            this.ViewModel = metricMaintenanceViewModel;
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Subscribe to method to close the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ViewModel_CloseWindow(object sender, EventArgs e)
        {
            //Close the window
            this.Close();

            this.ViewModel = null;
        }

        /// <summary>
        /// Applies changes to the viewmodel and closes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCancelButtonClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;

            //close the window
            this.Close();
        }

        /// <summary>
        /// Clears down the control safely
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Set view model to null to cause edit mode to be canceled whenever the window does close
            MetricMaintenanceViewModel oldViewModel = this.ViewModel;
            this.ViewModel = null;

            Dispatcher.BeginInvoke(
           (Action)(() =>
           {
               if (this.ViewModel != null)
               {
                   IDisposable dis = this.ViewModel;
                   this.ViewModel = null;

                   if (dis != null)
                   {
                       dis.Dispose();
                   }
               }

           }), priority: DispatcherPriority.Background);
        }
        #endregion

       
    }
}
