﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26158 : I.George
//	Initial version
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.MetricMaintenance
{
    /// <summary>
    /// Interaction logic for MetricMaintenanceOrganiser.xaml
    /// </summary>
    public partial class MetricMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region View Model Property

        public static readonly DependencyProperty ViewModelProperty
            = DependencyProperty.Register("ViewModel", typeof(MetricMaintenanceViewModel), typeof(MetricMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public MetricMaintenanceViewModel ViewModel
        {
            get { return (MetricMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MetricMaintenanceOrganiser senderControl = (MetricMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                MetricMaintenanceViewModel oldModel = (MetricMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                MetricMaintenanceViewModel newModel = (MetricMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor
       
        public MetricMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;
            InitializeComponent();

            //Add help file link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.MetricMaintenance);

            this.ViewModel = new MetricMaintenanceViewModel();
            this.Loaded += new RoutedEventHandler(MetricMaintenanceOrganiser_Loaded);
        }

      

        private void MetricMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= new RoutedEventHandler(MetricMaintenanceOrganiser_Loaded);

            //cancel the busy cursor 
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                // this.xBackstageOpen.IsSelected = true;
            }
        }

        private void UIElement_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selected = DataGrid.CurrentItem;
                if(selected != null)
                    this.ViewModel.EditCommand.Execute(selected);
            }
            else if (e.Key == Key.Delete)
            {
                var selected = DataGrid.CurrentItem;
                if(selected != null)
                    this.ViewModel.RemoveCommand.Execute(selected);
            }
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Disposes of the viewmodel when the window has closed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;
                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }
                }

            }), priority: DispatcherPriority.Background);
        }
        #endregion
    }
}
