﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-26158 : I.George
//	Initial version
#endregion
#region Version History: (CCM 820)
// CCM-30752 : J.Pickup
//	New Promotional Sales units exists check for save can execute
// CCM-30755  : J.Pickup
//	Fixed a but that allowed multiple metrics with special types regular sales.
#endregion

#endregion

using System.Linq;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Collections.ObjectModel;
using Galleria.Framework.Enums;
using Galleria.Framework.Collections;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Helpers;
using System.Collections.Generic;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Workflow.Client.Wpf.MetricMaintenance
{
    public sealed class MetricMaintenanceViewModel : ViewModelAttachedControlObject<MetricMaintenanceOrganiser>
    {
        #region Fields

        const String _exCategory = "MetricMaintenace"; //category name for exception in gibraltar from here

        private Boolean _userHasMetricCreatePerm;
        private Boolean _userHasMetricFetchPerm;
        private Boolean _userHasMetricEditPerm;
        private Boolean _userHasMetricDeletePerm;

        private readonly MetricListViewModel _masterMetricListView = new MetricListViewModel();
        private Metric _selectedMetric; //selected metric
        #endregion

        #region Property Binding Path
        public static PropertyPath SelectedMetricProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.SelectedMetric);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath SavaAndCloseCommandProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath AddCommandProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.AddCommand);
        public static PropertyPath EditCommandProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.EditCommand);
        public static PropertyPath RemoveCommandProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.RemoveCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.CloseCommand);
        public static PropertyPath MasterMetricListProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.MasterMetricList);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<MetricMaintenanceViewModel>(p => p.SaveCommand);
        #endregion

        #region Properties
        /// <summary>
        /// Gets/Sets the selected metric
        /// </summary>
        public Metric SelectedMetric
        {
            get { return _selectedMetric; }
            set
            {
                _selectedMetric = value;
                OnPropertyChanged(SelectedMetricProperty);
            }
        }

        public ReadOnlyBulkObservableCollection<Metric> MasterMetricList
        {
            get
            {
                return _masterMetricListView.BindingView;
            }
        }

        #endregion

        #region Constructor

        public MetricMaintenanceViewModel()
        {
            UpdatePermissionFlags();
            //metric list
            _masterMetricListView.FetchForEntity();
            _masterMetricListView.ModelChanged += MasterMetricListView_ModelChanged;
        }
        #endregion

        #region Commands

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand =
                   new RelayCommand(p => SaveAndNew_Executed(), p => SaveAndNew_CanExecute())
                   {
                       FriendlyName = Message.Ribbon_SaveAndNew,
                       FriendlyDescription = Message.Generic_Save_Tooltip,
                       Icon = ImageResources.SaveAndNew_16
                   };
                    this.ViewModelCommands.Add(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private Boolean SaveAndNew_CanExecute()
        {
            //must have create permission
            if (!_userHasMetricCreatePerm)
            {
                SaveAndNewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //must be able to save
            if (!SaveCommand.CanExecute())
            {
                SaveAndNewCommand.DisabledReason = SaveCommand.DisabledReason;
                return false;
            }
            return true;
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //Put back into edit mode for new metric
                this.BeginMetricEdit();

                this.SelectedMetric = Metric.NewMetric(App.ViewState.EntityId);
            }


        }
        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(p => SaveAndClose_Executed(), p => SaveAndClose_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }
        private Boolean SaveAndClose_CanExecute()
        {
            //must be able to save
            if (!SaveCommand.CanExecute())
            {
                SaveAndCloseCommand.DisabledReason = SaveCommand.DisabledReason;
                return false;
            }

            return true;
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                OnCloseWindow();
            }
        }
        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;
        /// <summary>
        /// saves the current metric list
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand =
                   new RelayCommand(p => Save_Executed(), p => Save_CanExecute())
                   {
                       FriendlyName = Message.Generic_Save,
                       FriendlyDescription = Message.Generic_Save_Tooltip,
                       Icon = ImageResources.Save_32,
                       SmallIcon = ImageResources.Save_16,
                       InputGestureModifiers = ModifierKeys.Control,
                       DisabledReason = Message.MetricMaintenance_InvalidMetrics,
                       InputGestureKey = Key.S

                   };
                    this.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasMetricEditPerm)
            {
                SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be set
            if (this.SelectedMetric == null)
            {
                SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedMetric.IsValid)
            {
                SaveCommand.DisabledReason = Message.MetricMaintenance_InvalidMetric;
                return false;
            }

            // special type regular sales unit already applied
            if (SelectedMetric.SpecialType == MetricSpecialType.RegularSalesUnits)
            {
                if (MasterMetricList.Any(p =>
                    p != SelectedMetric && p.SpecialType == MetricSpecialType.RegularSalesUnits))
                {
                    SaveCommand.DisabledReason = Message.MetricMaintenance_InvalidMetric_RegularSalesUnits;
                    return false;
                }
            }

            // replenishment days already applied
            if (SelectedMetric.SpecialType == MetricSpecialType.ReplenishmentDays)
            {
                if (MasterMetricList.Any(p => p != SelectedMetric && p.SpecialType == MetricSpecialType.ReplenishmentDays))
                {
                    SaveCommand.DisabledReason = Message.MetricMaintenance_InvalidSpecialType_ReplenishmentDays;
                    return false;
                }
            }

            // promotional sales units already applied
            if (SelectedMetric.SpecialType == MetricSpecialType.PromotionalSalesUnits)
            {
                if (MasterMetricList.Any(p => p != SelectedMetric && p.SpecialType == MetricSpecialType.PromotionalSalesUnits))
                {
                    SaveCommand.DisabledReason = Message.MetricMaintenance_InvalidSpecialType_PromotionalSalesUnits;
                    return false;
                }
            }
            return true;
        }

        public void Save_Executed()
        {
            if (this.Save_CanExecute())
            {
                SaveCurrentItem();
            }
        }

        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //** check the item unique value
            String newName;

            Boolean nameAccepted = true;
            if (this.AttachedControl != null)
            {
                Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Int32 id = this.SelectedMetric.Id;
                       IEnumerable<String> takenValues =
                           this.MasterMetricList.Where(p => p.Id != id).Select(p => p.Name);

                       return !takenValues.Contains(s);
                   };

                nameAccepted =
                   Galleria.Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(isUniqueCheck, this.SelectedMetric.Name, out newName);

            }
            else
            {
                //force a unique value for unit testing
                Int32 id = this.SelectedMetric.Id;
                newName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(this.SelectedMetric.Name,
                    this.MasterMetricList.Where(p => p.Id != id).Select(p => p.Name));

            }

            //set the code
            if (nameAccepted)
            {
                if (this.SelectedMetric.Name != newName)
                {
                    this.SelectedMetric.Name = newName;
                }
            }
            else
            {
                continueWithSave = false;
            }

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    if (this.SelectedMetric.IsNew) { _masterMetricListView.Model.Add(this.SelectedMetric); }

                    ApplyMetricEdit();

                    //save synchronously
                    _masterMetricListView.Model = _masterMetricListView.Model.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    //[SA-17668] set return flag to false as save was not completed.
                    continueWithSave = false;

                    Galleria.Ccm.Workflow.Client.Wpf.Common.LocalHelper.RecordException(ex, _exCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedMetric.Name);
                        if (itemReloadRequired)
                        {
                            //take copy of id
                            Int32 selectedMetricId = this.SelectedMetric.Id;
                            //refresh the metric list
                            _masterMetricListView.FetchForEntity();
                            //reselect metric
                            this.SelectedMetric = this.MasterMetricList.First(p => p.Id == selectedMetricId);
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(this.SelectedMetric.Name, OperationType.Save);
                    }

                    base.ShowWaitCursor(true);
                }

                base.ShowWaitCursor(false);
            }
            return continueWithSave;
        }
        #endregion

        #region AddCommand

        private RelayCommand _addCommand;
        /// <summary>
        /// executes the add command
        /// </summary>
        public RelayCommand AddCommand
        {
            get
            {
                if (_addCommand == null)
                {
                    _addCommand = new RelayCommand(p => Add_Executed(), p => Add_CanExecute())
                    {
                        FriendlyName = Message.MetricMaintenance_AddMetric,
                        FriendlyDescription = Message.MetricMaintenance_AddMetric_Tooltip,
                        SmallIcon = ImageResources.MetricMaintenance_AddMetric,
                        DisabledReason = Message.MetricMaintenance_AddMetric_Disabled
                    };
                    this.ViewModelCommands.Add(_addCommand);
                }
                return _addCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Add_CanExecute()
        {
           // user must have create permission
            if (!_userHasMetricCreatePerm)
            {
                _addCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }
            return true;
        }

        private void Add_Executed()
        {
            //create new Metric
            this.SelectedMetric = Metric.NewMetric(App.ViewState.EntityId);

            //if view model is attached allowing show dialogue method
            if (this.AttachedControl != null)
            {
                //show dialog
                MetricWindow window = new MetricWindow(this);
                App.ShowWindow(window,AttachedControl, true);
            }
        }
        #endregion

        #region EditCommand

        private RelayCommand _editCommand;

        public RelayCommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                {
                    _editCommand = new RelayCommand(p => Edit_Executed(), p => Edit_CanExecute())
                    {
                        FriendlyName = Message.MetricMaintenance_EditMetric,
                        FriendlyDescription = Message.MetricMaintenance_EditMetric_Tooltip,
                        DisabledReason = Message.MetricMaintenance_EditMetric_Disabled,
                        SmallIcon = ImageResources.MetricMaintenance_EditMetric
                    };
                    this.ViewModelCommands.Add(_editCommand);
                }
                return _editCommand;
            }
        }
        [DebuggerStepThrough]
        private Boolean Edit_CanExecute()
        {
            //user must have get permission
            if (!_userHasMetricFetchPerm)
            {
                this.EditCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //selected metric must not be null
            if (this.SelectedMetric == null)
            {
                this.EditCommand.DisabledReason = Message.MetricMaintenance_View_NoMetric;
                return false;
            }

            return true;
        }

        private void Edit_Executed()
        {
            if (this.AttachedControl != null)
            {
                //Show dialog
                MetricWindow window = new MetricWindow(this);
                App.ShowWindow(window, AttachedControl, true);
            }
        }
        #endregion

        #region Remove Command
        private RelayCommand _removeComand;

        public RelayCommand RemoveCommand
        {
            get
            {
                if (_removeComand == null)
                {
                    _removeComand = new RelayCommand(p => Remove_Executed(), p => Remove_CanExecute())
                     {
                         FriendlyName = Message.MetricMaintenance_RemoveMetric,
                         FriendlyDescription = Message.MetricMaintenance_RemoveMetric_Tooltip,
                         DisabledReason = Message.MetricMaintenance_RemoveMetric_Disabled,
                         SmallIcon = ImageResources.MetricMaintenance_RemoveMetric
                     };
                    this.ViewModelCommands.Add(_removeComand);
                }
                return _removeComand;
            }
        }

        public bool Remove_CanExecute()
        {
            // User must have delete permission
            if (!_userHasMetricDeletePerm)
            {
                _removeComand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

             //must not be null
            if (this.SelectedMetric == null)
            {
                _removeComand.DisabledReason = Message.MetricMaintenance_Remove_NoMetric;
                return false;
            }
            //must not be new
            if (this.SelectedMetric.IsNew)
            {
                _removeComand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }


            return true;
        }

        private void Remove_Executed()
        {
            Boolean deleteConfirmed = true;
            String deleteFriendlyName = String.Empty;

            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedMetric.ToString());
            }

            if (deleteConfirmed)
            {
                ShowWaitCursor(true);

                
                try
                {
                    //mark the item as deleted so item change does not show
                    Metric itemToDelete = this.SelectedMetric;

                    _masterMetricListView.Model.Remove(itemToDelete);
                    
                    //commit the delete
                    _masterMetricListView.Save();
                }

                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exCategory);
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(deleteFriendlyName, OperationType.Delete);
                    base.ShowWaitCursor(true);
                }

                //update the available items
                _masterMetricListView.FetchForEntity();

                base.ShowWaitCursor(false);
            }
        }
        #endregion

        #region Close Command
        private RelayCommand _closeComand;

        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeComand == null)
                {
                    _closeComand = new RelayCommand(p => Close_Executed(), p => Close_CanExecute())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    this.ViewModelCommands.Add(_closeComand);
                }
                return _closeComand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }
        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //save the metric type list so that any edits are committed
                    _masterMetricListView.ModelChanged -= MasterMetricListView_ModelChanged;
                    _selectedMetric = null;
                    this.AttachedControl = null;
                }
                base.IsDisposed = true;
            }
        }

        #endregion

        #region Event Handlers

        void MasterMetricListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<MetricList> e)
        {
            //Force rebind
            OnPropertyChanged(MasterMetricListProperty);
        }

        #endregion

        #region Editing Control

        #region Metric Edit Control

        public bool IsInMetricEditMode { get; private set; }

        public void BeginMetricEdit()
        {
            Debug.Assert(!this.IsInMetricEditMode, "Warning - MetricMaintenanceViewModel.cs is already in edit mode");

            //only begin the edit if we are not already editing 
            //or we will end up with edit level exceptions later on
            if (!this.IsInMetricEditMode)
            {
                this._masterMetricListView.Model.BeginEdit();
                IsInMetricEditMode = true;
            }
        }

        /// <summary>
        /// Applies the current edit transaction
        /// </summary>
        public void ApplyMetricEdit()
        {
            Debug.Assert(this.IsInMetricEditMode, "Warning - MetricMaintenanceViewModel.cs cannot apply as not in edit mode");

            if (this.IsInMetricEditMode)
            {
                this._masterMetricListView.Model.ApplyEdit();
                IsInMetricEditMode = false;
            }
        }

        /// <summary>
        /// Cancels the current edit transaction
        /// </summary>
        public void CancelExternalLinkSetupEdit()
        {
            Debug.Assert(this.IsInMetricEditMode, "Warning - MetricMaintenanceViewModel.cs cannot cancel as not in edit mode");

            if (this.IsInMetricEditMode)
            {
                this._masterMetricListView.Model.CancelEdit();
                IsInMetricEditMode = false;
            }
            else
            {
                Debug.WriteLine("Warning - MetricMaintenanceViewModel.cs cannot cancel as not in edit mode");
            }
        }
        #endregion
        #endregion

        #region Events

        /// <summary>
        /// Delclaration of CloseWindow event
        /// </summary>
        public event EventHandler CloseWindow;
        /// <summary>
        /// Method for event to fire
        /// </summary>
        public void OnCloseWindow()
        {
            if (this.CloseWindow != null)
            {
                this.CloseWindow(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //metric premissions
            //create
            _userHasMetricCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(Metric));

            //fetch
            _userHasMetricFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(Metric));

            //edit
            _userHasMetricEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(Metric));

            //delete
            _userHasMetricDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(Metric));
        }

        public Boolean ContinueWithItemChange()
        {
            /// <summary>
            /// Shows a warning requesting user ok to continue if current
            /// item is dirty
            /// </summary>
            /// <returns>true if the action may continue</returns>
            Boolean continueExecute = true; ;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedMetric, this.SaveCommand);
            }
            return continueExecute;
        }
        #endregion

    }
}
