﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.MetricMaintenance
{
    /// <summary>
    /// Interaction logic for MetricMaintenanceHomeTab.xaml
    /// </summary>
    public partial class MetricMaintenanceHomeTab : RibbonTabItem
    {
        #region properties
        #region ViewModelproperty

        public static readonly DependencyProperty ViewModelProperty
           = DependencyProperty.Register("ViewModel", typeof(MetricMaintenanceViewModel), typeof(MetricMaintenanceHomeTab),
           new PropertyMetadata(null, OnViewModelPropertyChanged));

        public MetricMaintenanceViewModel ViewModel
        {
            get { return (MetricMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MetricMaintenanceHomeTab senderControl = (MetricMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                MetricMaintenanceViewModel oldModel = (MetricMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                MetricMaintenanceViewModel newModel = (MetricMaintenanceViewModel)e.NewValue;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MetricMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
