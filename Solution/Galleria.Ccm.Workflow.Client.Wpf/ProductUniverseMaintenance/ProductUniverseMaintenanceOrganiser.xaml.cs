﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM 8.0.0
// CCM-25452 : N.Haywood
//	Added from SA
#endregion

#region Version History: CCM 8.1.1
// V8-30516 : M.Shelley
//  Added mouse double click behaviour to both the unassigned product and assigned product grids.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductUniverseMaintenance
{
    /// <summary>
    /// Interaction logic for ProductToCategoryOrganiser.xaml
    /// </summary>
    public sealed partial class ProductUniverseMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductUniverseMaintenanceViewModel), typeof(ProductUniverseMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ProductUniverseMaintenanceViewModel ViewModel
        {
            get { return (ProductUniverseMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductUniverseMaintenanceOrganiser senderControl = (ProductUniverseMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                ProductUniverseMaintenanceViewModel oldModel = (ProductUniverseMaintenanceViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                ProductUniverseMaintenanceViewModel newModel = (ProductUniverseMaintenanceViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProductUniverseMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ProductUniverseMaintenance);

            this.ViewModel = new ProductUniverseMaintenanceViewModel();

            this.Loaded += ProductUniverseSetupOrganiser_Loaded;
        }

        /// <summary>
        /// On Loaded event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductUniverseSetupOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProductUniverseSetupOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }


        /// <summary>
        /// Performs a RemoveSelectedProducts for rows dropped on the unassigned grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UnassignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.assignedGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.RemoveSelectedProductsCommand.Execute();
                }
            }
        }

        private void unassignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.unassignedGrid)) return;
            if (!ViewModel.SelectedAvailableProducts.Any()) return;

            ViewModel.AddSelectedProductsCommand.Execute();
        }

        /// <summary>
        /// Performs an AddSelectedProducts for rows dropped on the assigned grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AssignedGrid_RowDropCaught(object sender, ExtendedDataGridRowDropEventArgs e)
        {
            if (e.SourceGrid == this.unassignedGrid)
            {
                if (this.ViewModel != null)
                {
                    this.ViewModel.AddSelectedProductsCommand.Execute();
                }
            }
        }

        private void assignedGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (ViewModel == null || !Equals(sender, this.assignedGrid )) return;
            if (!ViewModel.SelectedTakenProducts.Any()) return;

            ViewModel.RemoveSelectedProductsCommand.Execute();
        }

        /// <summary>
        /// Imitates the double-click events on return key press for 508 compliance
        /// additional functionality with space bar selecting adjacent grid 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void assignmentGrids_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            var currentGrid = sender as ExtendedDataGrid;
            Boolean isUnassigned = currentGrid?.Name == unassignedGrid.Name;

            if (e.Key == Key.Return)
            {
                if (isUnassigned) ViewModel?.AddSelectedProductsCommand?.Execute();
                else ViewModel?.RemoveSelectedProductsCommand?.Execute();
                Keyboard.Focus(isUnassigned ? unassignedGrid : assignedGrid);
                e.Handled = true;
            }

            if (e.Key == Key.Space)
            {
                Keyboard.Focus(isUnassigned ? assignedGrid : unassignedGrid);
                e.Handled = true;
            }
        }

        #endregion

        #region Window close


        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
