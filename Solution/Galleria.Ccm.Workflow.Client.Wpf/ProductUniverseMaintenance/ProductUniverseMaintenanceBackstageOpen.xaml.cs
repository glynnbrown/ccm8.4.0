﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductUniverseMaintenance
{
    /// <summary>
    /// Interaction logic for ProductUniverseMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class ProductUniverseMaintenanceBackstageOpen : UserControl
    {
        #region Fields
        private BulkObservableCollection<ProductUniverseInfo> _searchResults = new BulkObservableCollection<ProductUniverseInfo>();
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductUniverseMaintenanceViewModel), typeof(ProductUniverseMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ProductUniverseMaintenanceViewModel ViewModel
        {
            get { return (ProductUniverseMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductUniverseMaintenanceBackstageOpen senderControl = (ProductUniverseMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                ProductUniverseMaintenanceViewModel oldModel = (ProductUniverseMaintenanceViewModel)e.OldValue;
                oldModel.AvailableProductUniverses.BulkCollectionChanged -= senderControl.ViewModel_AvailableProductUniversesBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                ProductUniverseMaintenanceViewModel newModel = (ProductUniverseMaintenanceViewModel)e.NewValue;
                newModel.AvailableProductUniverses.BulkCollectionChanged += senderControl.ViewModel_AvailableProductUniversesBulkCollectionChanged;
            }

            senderControl.UpdateSearchResults();
        }


        #endregion

        #region SearchTextProperty

        public static readonly DependencyProperty SearchTextProperty =
            DependencyProperty.Register("SearchText", typeof(String), typeof(ProductUniverseMaintenanceBackstageOpen),
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        /// <summary>
        /// Gets/Sets the criteria to search products for
        /// </summary>
        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ProductUniverseMaintenanceBackstageOpen senderControl = (ProductUniverseMaintenanceBackstageOpen)obj;
            senderControl.UpdateSearchResults();
        }

        #endregion

        #region SearchResultsProperty

        public static readonly DependencyProperty SearchResultsProperty =
            DependencyProperty.Register("SearchResults", typeof(ReadOnlyBulkObservableCollection<ProductUniverseInfo>),
            typeof(ProductUniverseMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Returns the collection of search results
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductUniverseInfo> SearchResults
        {
            get { return (ReadOnlyBulkObservableCollection<ProductUniverseInfo>)GetValue(SearchResultsProperty); }
            private set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ProductUniverseMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyBulkObservableCollection<ProductUniverseInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Finds results matching the given criteria from the viewmodel and updates the search collection
        /// </summary>
        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (this.ViewModel != null)
            {
                IEnumerable<ProductUniverseInfo> results = this.ViewModel.GetMatchingProductUniverses(this.SearchText);
                _searchResults.AddRange(results.OrderBy(l => l.ToString()));
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Updates the search results when the available universes list changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_AvailableProductUniversesBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }

        /// <summary>
        /// Opens the double clicked item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void searchResultsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ExtendedDataGrid senderControl = (ExtendedDataGrid)sender;
            DataGridRow clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<DataGridRow>();
            if (clickedItem != null)
            {
                ProductUniverseInfo itemToOpen = (ProductUniverseInfo)senderControl.SelectedItem;

                if (itemToOpen != null)
                {
                    //send the store to be opened
                    this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                }
            }
        }

        private void SearchResultsListBox_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                ExtendedDataGrid senderControl = (ExtendedDataGrid)sender;
                DataGridRow clickedItem = ((DependencyObject)e.OriginalSource).FindVisualAncestor<DataGridRow>();
                if (clickedItem != null)
                {
                    ProductUniverseInfo itemToOpen = (ProductUniverseInfo)senderControl.SelectedItem;

                    if (itemToOpen != null)
                    {
                        //send the store to be opened
                        this.ViewModel.OpenCommand.Execute(itemToOpen.Id);
                    }
                }
                e.Handled = true;
            }
        }
        #endregion
    }
}
