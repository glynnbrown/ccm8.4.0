﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductUniverseMaintenance
{
    /// <summary>
    /// Interaction logic for ProductToCategoryHomeTab.xaml
    /// </summary>
    public partial class ProductUniverseMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ProductUniverseMaintenanceViewModel), typeof(ProductUniverseMaintenanceHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ProductUniverseMaintenanceViewModel ViewModel
        {
            get { return (ProductUniverseMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ProductUniverseMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
