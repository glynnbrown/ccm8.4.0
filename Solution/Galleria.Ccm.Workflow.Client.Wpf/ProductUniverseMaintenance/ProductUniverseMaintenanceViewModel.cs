﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#region Version History: (CCM 801)
//V8-28922 : L.Ineson
//  Refactored to use new WindowService and WindowViewModelBase
#endregion
#region Version History: (CCM 8.1.1)
// V8-30325 : I.George
//  Added friendly description to Delete Command
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ProductUniverseMaintenance
{
    public sealed class ProductUniverseMaintenanceViewModel : WindowViewModelBase
    {
        #region Fields

        const String _exCategory = "ProductUniverseMaintenance";
        private ModelPermission<ProductUniverse> _itemPermissions = new ModelPermission<ProductUniverse>();
        private ModelPermission<Product> _productPermissions = new ModelPermission<Product>();

        private readonly ProductUniverseInfoListViewModel _masterUniverseInfoListView = new ProductUniverseInfoListViewModel();
        private ProductUniverse _selectedProductUniverse;

        private ProductGroupInfo _selectedProductGroup;

        private BulkObservableCollection<ProductInfo> _masterProductsCollection = new BulkObservableCollection<ProductInfo>();
        private BulkObservableCollection<ProductInfo> _availableProducts = new BulkObservableCollection<ProductInfo>();
        private ReadOnlyBulkObservableCollection<ProductInfo> _availableProductsRO;
        private ObservableCollection<ProductInfo> _selectedAvailableProducts = new ObservableCollection<ProductInfo>();
        private ObservableCollection<ProductUniverseProduct> _selectedTakenProducts = new ObservableCollection<ProductUniverseProduct>();

        private Boolean _isSearching = false;
        private String _searchCriteria = String.Empty;
        private Boolean _isProductSearchQueued;
        private readonly ProductInfoListViewModel _searchProductInfoListView = new ProductInfoListViewModel();
        private String _selectedProductGroupName = "";

        #endregion

        #region Binding Property Paths

        //properties
        public static readonly PropertyPath AvailableProductUniversesProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.AvailableProductUniverses);
        public static readonly PropertyPath SelectedProductUniverseProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SelectedProductUniverse);
        public static readonly PropertyPath AvailableProductsProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.AvailableProducts);
        public static readonly PropertyPath SelectedAvailableProductsProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SelectedAvailableProducts);
        public static readonly PropertyPath SelectedTakenProductsProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SelectedTakenProducts);
        public static readonly PropertyPath SelectedProductGroupProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SelectedProductGroup);
        public static readonly PropertyPath IsSearchingProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.IsSearching);
        public static readonly PropertyPath SearchCriteriaProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SearchCriteria);
        public static readonly PropertyPath SelectedProductGroupNameProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SelectedProductGroupName);

        //commands
        public static readonly PropertyPath NewCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath DeleteCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath AddSelectedProductsCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.AddSelectedProductsCommand);
        public static readonly PropertyPath AddAllProductsCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.AddAllProductsCommand);
        public static readonly PropertyPath RemoveSelectedProductsCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.RemoveSelectedProductsCommand);
        public static readonly PropertyPath RemoveAllProductsCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.RemoveAllProductsCommand);
        public static readonly PropertyPath CloseCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath SelectProductGroupCommandProperty = GetPropertyPath<ProductUniverseMaintenanceViewModel>(p => p.SelectProductGroupCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available product universes
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductUniverseInfo> AvailableProductUniverses
        {
            get { return _masterUniverseInfoListView.BindableCollection; }
        }

        /// <summary>
        /// Returns the current ProductUniverse item
        /// </summary>
        public ProductUniverse SelectedProductUniverse
        {
            get { return _selectedProductUniverse; }
            private set
            {
                ProductUniverse oldModel = _selectedProductUniverse;
                _selectedProductUniverse = value;
                OnPropertyChanged(SelectedProductUniverseProperty);
                OnSelectedProductUniverseChanged(oldModel, value);
            }
        }

        /// <summary>
        /// Returns the readonly collection of products still available for adding to the category
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> AvailableProducts
        {
            get
            {
                if (_availableProductsRO == null)
                {
                    _availableProductsRO = new ReadOnlyBulkObservableCollection<ProductInfo>(_availableProducts);
                }
                return _availableProductsRO;
            }
        }

        /// <summary>
        /// Returns the collection of selected available products
        /// </summary>
        public ObservableCollection<ProductInfo> SelectedAvailableProducts
        {
            get { return _selectedAvailableProducts; }
        }

        /// <summary>
        /// Returns the selected productgroup
        /// </summary>
        public ProductGroupInfo SelectedProductGroup
        {
            get { return _selectedProductGroup; }
            set
            {
                _selectedProductGroup = value;
                OnPropertyChanged(SelectedProductGroupProperty);

                if (value != null)
                {
                    _selectedProductUniverse.ProductGroupId = _selectedProductGroup.Id;
                    _selectedProductGroupName = value.ToString();
                }
                else
                {
                    _selectedProductGroupName = "";
                }

                OnPropertyChanged(SelectedProductGroupNameProperty);
            }
        }

        /// <summary>
        /// Returns the name of the selected product group
        /// </summary>
        public String SelectedProductGroupName
        {
            get { return _selectedProductGroupName; }
        }

        /// <summary>
        /// Returns the collection of selected taken products
        /// </summary>
        public ObservableCollection<ProductUniverseProduct> SelectedTakenProducts
        {
            get { return _selectedTakenProducts; }
        }

        /// <summary>
        /// Is a search being performed for products?
        /// </summary>
        public Boolean IsSearching
        {
            get { return _isSearching; }
            set
            {
                _isSearching = value;
                OnPropertyChanged(IsSearchingProperty);
            }
        }

        /// <summary>
        /// The search text the user has entered to search by
        /// </summary>
        public String SearchCriteria
        {
            get { return _searchCriteria; }
            set
            {
                _searchCriteria = value;

                OnPropertyChanged(SearchCriteriaProperty);
                OnSearchCriteriaChanged();
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ProductUniverseMaintenanceViewModel()
        {
            _selectedAvailableProducts.CollectionChanged += SelectedAvailableProducts_CollectionChanged;
            _selectedTakenProducts.CollectionChanged += SelectedTakenProducts_CollectionChanged;

            //master info list
            _masterUniverseInfoListView.FetchAllForEntity();

            //search product info list view
            _searchProductInfoListView.ModelChanged += SearchProductInfoListView_ModelChanged;


            //create a new universe
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        /// Creates a new item
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                                p => New_Executed(),
                                p => New_CanExecute(),
                                attachToCommandManager: false)
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    RegisterCommand(_newCommand);
                }
                return _newCommand;
            }
        }

        private Boolean New_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            return true;
        }

        private void New_Executed()
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;

            //create a new item
            this.SelectedProductUniverse = ProductUniverse.NewProductUniverse(App.ViewState.EntityId);

            //close the backstage
            CloseRibbonBackstage();
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        /// Opens the items for the category with the given id
        /// param: Int32 the content id to open
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        private Boolean Open_CanExecute(Int32? id)
        {
            //user must have get permission
            if (!_itemPermissions.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //id must not be null
            if (id == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? id)
        {
            //Confirm change with user.
            if (!ContinueWithItemChange()) return;


            ShowWaitCursor(true);

            try
            {
                //fetch the requested item
                this.SelectedProductUniverse = ProductUniverse.FetchById(id.Value);
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            //close the backstage
            CloseRibbonBackstage();

            ShowWaitCursor(false);

        }

        #endregion

        #region Save

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    RegisterCommand(_saveCommand);
                }
                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.SelectedProductUniverse == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //if item is new, must have create
            if (this.SelectedProductUniverse.IsNew && !_itemPermissions.CanCreate)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //if item is old, user must have edit permission
            if (!this.SelectedProductUniverse.IsNew && !_itemPermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.SelectedProductUniverse.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            //must have a category assigned.
            if (this.SelectedProductGroup == null)
            {
                this.SaveCommand.DisabledReason = Message.ProductUniverseMaintenance_NoProductGroup;
                this.SaveAndNewCommand.DisabledReason = Message.ProductUniverseMaintenance_NoProductGroup;
                this.SaveAndCloseCommand.DisabledReason = Message.ProductUniverseMaintenance_NoProductGroup;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        private Boolean SaveCurrentItem()
        {
            Int32 itemId = this.SelectedProductUniverse.Id;

            //** check the item unique value
            String newName;

            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       IEnumerable<String> takenValues = this.AvailableProductUniverses.Where(p => p.Id != itemId).Select(p => p.Name);
                       return !takenValues.Contains(s);
                   };

            Boolean nameAccepted =
                GetWindowService().PromptIfNameIsNotUnique(isUniqueCheck, this.SelectedProductUniverse.Name, out newName);
            if (!nameAccepted) return false;

            //set the name
            if (this.SelectedProductUniverse.Name != newName) this.SelectedProductUniverse.Name = newName;



            //** save the item
            ShowWaitCursor(true);

            try
            {
                this.SelectedProductUniverse = this.SelectedProductUniverse.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);

                RecordException(ex, _exCategory);
                Exception rootException = ex.GetBaseException();

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(this.SelectedProductUniverse.Name);
                    if (itemReloadRequired)
                    {
                        this.SelectedProductUniverse = ProductUniverse.FetchById(this.SelectedProductUniverse.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(this.SelectedProductUniverse.Name, OperationType.Save);
                }
                return false;
            }

            //refresh the info list
            _masterUniverseInfoListView.FetchAllForEntity();

            ShowWaitCursor(false);
            return true;
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        /// Saves the current item as a completely new one
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    RegisterCommand(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_itemPermissions.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.SelectedProductUniverse == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have a category assigned.
            if (this.SelectedProductGroup == null)
            {
                this.SaveAsCommand.DisabledReason = Message.ProductUniverseMaintenance_NoProductGroup;
                return false;
            }

            //must be valid
            if (!this.SelectedProductUniverse.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }


            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the name to save as
            String copyName = null;

            Predicate<String> isUniqueCheck =
               (s) => { return !this.AvailableProductUniverses.Select(p => p.Name).Contains(s, StringComparer.OrdinalIgnoreCase); };

            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            ShowWaitCursor(true);

            //copy the item and rename
            ProductUniverse itemCopy = this.SelectedProductUniverse.Copy();
            itemCopy.Name = copyName;
            this.SelectedProductUniverse = itemCopy;

            ShowWaitCursor(false);

            //save it.
            SaveCurrentItem();
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        /// Executes a save then the new command
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    RegisterCommand(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            //request a save of the current item
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                //load a new item
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;
        /// <summary>
        /// Executes the save command then closes the attached window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    RegisterCommand(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }


        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                CloseWindow();
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Deletes the item
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(),
                        p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    RegisterCommand(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_itemPermissions.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.SelectedProductUniverse == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.SelectedProductUniverse.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            //confirm with user
            if (!GetWindowService().ConfirmDeleteWithUser(this.SelectedProductUniverse.Name)) return;


            ShowWaitCursor(true);

            //mark the item as deleted so item changed does not show.
            ProductUniverse itemToDelete = this.SelectedProductUniverse;
            itemToDelete.Delete();

            //load a new item
            NewCommand.Execute();

            //commit the delete
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex, _exCategory);
                GetWindowService().ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);
                return;
            }

            //update the available items
            _masterUniverseInfoListView.FetchAllForEntity();

            ShowWaitCursor(false);
        }

        #endregion

        #region AddSelectedProductsCommand

        private RelayCommand _addSelectedProductsCommand;

        /// <summary>
        /// Adds any selected available products to the current product universe
        /// </summary>
        public RelayCommand AddSelectedProductsCommand
        {
            get
            {
                if (_addSelectedProductsCommand == null)
                {
                    _addSelectedProductsCommand = new RelayCommand(
                        p => AddSelectedProducts_Executed(),
                        p => AddSelectedProducts_CanExecute())
                    {
                        FriendlyDescription = Message.ProductUniverseMaintenance_AddSelectedProducts,
                        SmallIcon = ImageResources.ProductUniverseMaintenance_AddSelectedProducts,
                    };
                    RegisterCommand(_addSelectedProductsCommand);
                }
                return _addSelectedProductsCommand;
            }
        }

        private Boolean AddSelectedProducts_CanExecute()
        {
            //must have products selected
            if (this.SelectedAvailableProducts.Count == 0)
            {
                this.AddSelectedProductsCommand.DisabledReason = Message.ProductUniverseMaintenance_AddSelectedProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void AddSelectedProducts_Executed()
        {
            this.SelectedProductUniverse.Products.AddList(this.SelectedAvailableProducts.ToList());
            _selectedAvailableProducts.Clear();
        }

        #endregion

        #region AddAllProductsCommand

        private RelayCommand _addAllProductsCommand;

        /// <summary>
        /// Adds all available products to the selected product universe
        /// </summary>
        public RelayCommand AddAllProductsCommand
        {
            get
            {
                if (_addAllProductsCommand == null)
                {
                    _addAllProductsCommand = new RelayCommand(
                        p => AddAllProducts_Executed(),
                        p => AddAllProducts_CanExecute())
                    {
                        FriendlyDescription = Message.ProductUniverseMaintenance_AddAllProducts,
                        SmallIcon = ImageResources.ProductUniverseMaintenance_AddAllProducts,
                    };
                    RegisterCommand(_addAllProductsCommand);
                }
                return _addAllProductsCommand;
            }
        }

        private Boolean AddAllProducts_CanExecute()
        {
            //must have products available
            if (this.AvailableProducts.Count == 0)
            {
                this.AddAllProductsCommand.DisabledReason = Message.ProductUniverseMaintenance_AddAllProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void AddAllProducts_Executed()
        {
            this.SelectedProductUniverse.Products.AddList(this.AvailableProducts.ToList());
            _selectedAvailableProducts.Clear();
        }

        #endregion

        #region RemoveSelectedProductsCommand

        private RelayCommand _removeSelectedProductsCommand;

        /// <summary>
        /// Removes the selected products from the current product universe
        /// </summary>
        public RelayCommand RemoveSelectedProductsCommand
        {
            get
            {
                if (_removeSelectedProductsCommand == null)
                {
                    _removeSelectedProductsCommand = new RelayCommand(
                        p => RemoveSelectedProducts_Executed(),
                        p => RemoveSelectedProducts_CanExecute())
                    {
                        FriendlyDescription = Message.ProductUniverseMaintenance_RemoveSelectedProducts,
                        SmallIcon = ImageResources.ProductUniverseMaintenance_RemoveSelectedProducts,
                    };
                    RegisterCommand(_removeSelectedProductsCommand);
                }
                return _removeSelectedProductsCommand;
            }
        }

        private Boolean RemoveSelectedProducts_CanExecute()
        {
            //must have taken products selected
            if (this.SelectedTakenProducts.Count == 0)
            {
                this.RemoveSelectedProductsCommand.DisabledReason = Message.ProductUniverseMaintenance_RemoveSelectedProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void RemoveSelectedProducts_Executed()
        {
            this.SelectedProductUniverse.Products.RemoveList(this.SelectedTakenProducts.ToList());
            this.SelectedTakenProducts.Clear();
        }

        #endregion

        #region RemoveAllProductsCommand

        private RelayCommand _removeAllProductsCommand;

        /// <summary>
        /// Removes all products from the current product universe
        /// </summary>
        public RelayCommand RemoveAllProductsCommand
        {
            get
            {
                if (_removeAllProductsCommand == null)
                {
                    _removeAllProductsCommand = new RelayCommand(
                        p => RemoveAllProducts_Executed(),
                        p => RemoveAllProducts_CanExecute())
                    {
                        FriendlyDescription = Message.ProductUniverseMaintenance_RemoveAllProducts,
                        SmallIcon = ImageResources.ProductUniverseMaintenance_RemoveAllProducts,
                    };
                    RegisterCommand(_removeAllProductsCommand);
                }
                return _removeAllProductsCommand;
            }
        }

        private Boolean RemoveAllProducts_CanExecute()
        {
            //must have products taken
            if (this.SelectedProductUniverse.Products.Count == 0)
            {
                this.RemoveAllProductsCommand.DisabledReason = Message.ProductUniverseMaintenance_RemoveAllProducts_DisabledReason;
                return false;
            }

            return true;
        }

        private void RemoveAllProducts_Executed()
        {
            this.SelectedProductUniverse.Products.RemoveList(this.SelectedProductUniverse.Products.ToList());
        }


        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    RegisterCommand(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            CloseWindow();
        }

        #endregion

        #region SelectProductGroupCommand

        private RelayCommand _selectProductGroupCommand;

        /// <summary>
        /// Shows the select product group dialog
        /// </summary>
        public RelayCommand SelectProductGroupCommand
        {
            get
            {
                if (_selectProductGroupCommand == null)
                {
                    _selectProductGroupCommand = new RelayCommand(
                        p => SelectProductGroup_Executed(),
                        p => SelectProductGroup_CanExecute())
                    {
                    };
                    RegisterCommand(_selectProductGroupCommand);
                }
                return _selectProductGroupCommand;
            }
        }

        private Boolean SelectProductGroup_CanExecute()
        {
            //user must have get permission
            if (!Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(ProductGroup)))
            {
                this.SelectProductGroupCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            return true;
        }

        private void SelectProductGroup_Executed()
        {
            //TODO: Refactor this out.
            if (!HasAttachedControl()) return;


            MerchGroupSelectionWindow dialog = new MerchGroupSelectionWindow();
            dialog.SelectionMode = DataGridSelectionMode.Single;
            GetWindowService().ShowDialog<MerchGroupSelectionWindow>(dialog);

            if (dialog.DialogResult == true)
            {
                ProductGroup selectedGroup = dialog.SelectionResult.FirstOrDefault();
                if (selectedGroup != null)
                {
                    this.SelectedProductGroup = ProductGroupInfo.NewProductGroupInfo(selectedGroup);
                }
            }
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        /// Responds to a change of selected product universe
        /// </summary>
        /// <param name="oldModel"></param>
        /// <param name="newModel"></param>
        private void OnSelectedProductUniverseChanged(ProductUniverse oldModel, ProductUniverse newModel)
        {
            ProductGroupInfo newSelectedProductGroup = null;

            if (oldModel != null)
            {
                oldModel.Products.BulkCollectionChanged -= ProductUniverse_ProductsBulkCollectionChanged;
            }

            if (newModel != null)
            {
                newModel.Products.BulkCollectionChanged += ProductUniverse_ProductsBulkCollectionChanged;

                if (newModel.ProductGroupId.HasValue)
                {
                    newSelectedProductGroup = ProductGroupInfo.FetchById(newModel.ProductGroupId.Value);
                }
            }

            //update the available products collectin
            ResetAvailableProducts();

            //update the selected product group
            this.SelectedProductGroup = newSelectedProductGroup;
        }

        /// <summary>
        /// Responds to changes in the product collection of the currently selected product universe
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductUniverse_ProductsBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:

                    //make a list of products to be removed from the available list
                    List<ProductInfo> removeFromAvailableList = new List<ProductInfo>();
                    foreach (ProductUniverseProduct item in e.ChangedItems)
                    {
                        Int32 itemId = item.ProductId;
                        ProductInfo availableInfo = _availableProducts.FirstOrDefault(p => p.Id == itemId);
                        removeFromAvailableList.Add(availableInfo);
                    }

                    _availableProducts.RemoveRange(removeFromAvailableList);
                    break;

                case NotifyCollectionChangedAction.Remove:

                    //make a list of products to add to the available list
                    List<ProductInfo> addToAvailableList = new List<ProductInfo>();
                    foreach (ProductUniverseProduct item in e.ChangedItems)
                    {
                        Int32 itemId = item.ProductId;
                        ProductInfo availableInfo = _masterProductsCollection.FirstOrDefault(p => p.Id == itemId);
                        if (availableInfo != null)
                        {
                            addToAvailableList.Add(availableInfo);
                        }
                    }

                    _availableProducts.AddRange(addToAvailableList);
                    break;

                case NotifyCollectionChangedAction.Reset:
                    ResetAvailableProducts();
                    break;
            }

        }

        /// <summary>
        /// Reponds to changes to the selected avaialble products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedAvailableProducts_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //clear the selected taken products if an item has been added
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (this.SelectedTakenProducts.Count > 0)
                {
                    this.SelectedTakenProducts.Clear();
                }
            }
        }

        /// <summary>
        /// Reponds to changes to the selected taken products
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedTakenProducts_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //clear the selected available products if an item has been added
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                if (this.SelectedAvailableProducts.Count > 0)
                {
                    this.SelectedAvailableProducts.Clear();
                }
            }
        }

        /// <summary>
        /// Responds to a change of product search criteria
        /// </summary>
        private void OnSearchCriteriaChanged()
        {
            RefreshProductSearchResults();
        }

        /// <summary>
        /// Reponds the the completion of a product info list search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchProductInfoListView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
        {
            //mark as finished searching
            this.IsSearching = false;
            _masterProductsCollection.Clear();

            //check if another search is queued
            if (_isProductSearchQueued)
            {
                RefreshProductSearchResults();
            }
            else
            {
                _masterProductsCollection.AddRange(e.NewModel);
            }

            ResetAvailableProducts();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(this.SelectedProductUniverse, this.SaveCommand);
        }

        /// <summary>
        /// Corrects the list of available products
        /// </summary>
        private void ResetAvailableProducts()
        {
            if (_availableProducts.Count > 0)
            {
                _availableProducts.Clear();
            }

            if (this.SelectedProductUniverse != null)
            {
                IEnumerable<Int32> selectedIds = this.SelectedProductUniverse.Products.Select(p => p.ProductId);

                IEnumerable<ProductInfo> availableProducts =
                    _masterProductsCollection.Where(p => !selectedIds.Contains(p.Id));

                _availableProducts.AddRange(availableProducts);
            }
        }

        /// <summary>
        /// Researches for products based on the product search criteria
        /// </summary>
        private void RefreshProductSearchResults()
        {
            if (!this.IsSearching)
            {
                _isProductSearchQueued = false;

                this.IsSearching = true;

                //search asyncronously
                _searchProductInfoListView.BeginFetchForCurrentEntityBySearchCriteria(_searchCriteria);
            }
            else
            {
                _isProductSearchQueued = true;
            }
        }

        /// <summary>
        /// Returns a list of matching infos based on the given critera
        /// </summary>
        /// <param name="searchCriteria"></param>
        /// <returns></returns>
        public IEnumerable<ProductUniverseInfo> GetMatchingProductUniverses(String searchCriteria)
        {
            if (!String.IsNullOrEmpty(searchCriteria))
            {
                String criteria = searchCriteria.ToLowerInvariant();
                return this.AvailableProductUniverses.Where(p => p.Name.ToLowerInvariant().Contains(criteria));
            }
            else
            {
                return this.AvailableProductUniverses;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.IsSearching = false;

                    //unsubscribe from events
                    _selectedAvailableProducts.CollectionChanged -= SelectedAvailableProducts_CollectionChanged;
                    _selectedTakenProducts.CollectionChanged -= SelectedTakenProducts_CollectionChanged;
                    this.SelectedProductUniverse.Products.BulkCollectionChanged -= ProductUniverse_ProductsBulkCollectionChanged;
                    _searchProductInfoListView.ModelChanged -= SearchProductInfoListView_ModelChanged;

                    //dispose viewmodels
                    _searchProductInfoListView.Dispose();
                    _masterUniverseInfoListView.Dispose();

                    DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
