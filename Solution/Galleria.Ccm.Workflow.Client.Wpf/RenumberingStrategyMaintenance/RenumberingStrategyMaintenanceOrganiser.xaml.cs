﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.
 
#endregion

#region Version History: CCM801

// V8-28923 : A.Silva
//      Added OnClosed override to dispose of the ViewModel.
//      Some Tidying up.

#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;

namespace Galleria.Ccm.Workflow.Client.Wpf.RenumberingStrategyMaintenance
{
    /// <summary>
    /// Interaction logic for RenumberingStrategyMaintenanceOrganiser.xaml
    /// </summary>
    public partial class RenumberingStrategyMaintenanceOrganiser
    {
        #region Properties

        #region ViewModel

        /// <summary>
        ///     Static <see cref="DependencyProperty"/> for the <see cref="ViewModel"/>.
        /// </summary>
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(RenumberingStrategyMaintenanceViewModel), typeof(RenumberingStrategyMaintenanceOrganiser),
            new PropertyMetadata(null, ViewModelOnPropertyChanged));

        /// <summary>
        ///     Gets or sets the value of the <see cref="ViewModel"/> property, notifying when the value changes.
        /// </summary>
        public RenumberingStrategyMaintenanceViewModel ViewModel
        {
            get { return (RenumberingStrategyMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="RenumberingStrategyMaintenanceOrganiser"/>.
        /// </summary>
        public RenumberingStrategyMaintenanceOrganiser()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            // Add helpfile.
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.RenumberingStrategyMaintenance);

            this.ViewModel = new RenumberingStrategyMaintenanceViewModel();

            Loaded += RenumberingStrategyMaintenanceOrganiser_Loaded;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Handles the PropertyChanged event raised on the <see cref="ViewModel"/>.
        /// </summary>
        private static void ViewModelOnPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            RenumberingStrategyMaintenanceOrganiser senderControl = (RenumberingStrategyMaintenanceOrganiser) sender;

            RenumberingStrategyMaintenanceViewModel oldValue = e.OldValue as RenumberingStrategyMaintenanceViewModel;
            if (oldValue != null)
            {
                oldValue.AttachedControl = null;
            }

            RenumberingStrategyMaintenanceViewModel newValue = e.NewValue as RenumberingStrategyMaintenanceViewModel;
            if (newValue != null)
            {
                newValue.AttachedControl = senderControl;
            }
        }

        private void RenumberingStrategyMaintenanceOrganiser_Loaded(Object sender, RoutedEventArgs e)
        {
            Loaded -= RenumberingStrategyMaintenanceOrganiser_Loaded;

            Dispatcher.BeginInvoke((Action) (() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Window.Closing"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.ComponentModel.CancelEventArgs"/> that contains the event data.</param>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel && this.ViewModel != null) e.Cancel = !this.ViewModel.ContinueWithItemChange();
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Window.Closed"/> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> that contains the event data.</param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke((Action) (() =>
            {
                IDisposable disposableViewModel = this.ViewModel;
                this.ViewModel = null;

                if (disposableViewModel != null) disposableViewModel.Dispose();

            }), DispatcherPriority.Background);
        }

        #endregion
    }
}