﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Reporting.Controls.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.RenumberingStrategyMaintenance
{
    /// <summary>
    /// Interaction logic for RenumberingStrategyMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class RenumberingStrategyMaintenanceBackstageOpen
    {
        #region Fields

        private readonly ObservableCollection<RenumberingStrategyInfo> _searchResults = new ObservableCollection<RenumberingStrategyInfo>();

        #endregion

        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof (RenumberingStrategyMaintenanceViewModel), typeof (RenumberingStrategyMaintenanceBackstageOpen),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        public RenumberingStrategyMaintenanceViewModel ViewModel
        {
            get { return (RenumberingStrategyMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SearchResults

        public static readonly DependencyProperty SearchResultsProperty = DependencyProperty.Register(
            "SearchResults", typeof (ReadOnlyObservableCollection<RenumberingStrategyInfo>),
            typeof (RenumberingStrategyMaintenanceBackstageOpen), new PropertyMetadata(null));

        public ReadOnlyObservableCollection<RenumberingStrategyInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<RenumberingStrategyInfo>) GetValue(SearchResultsProperty); }
            set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #region SearchText

        public static readonly DependencyProperty SearchTextProperty = DependencyProperty.Register(
            "SearchText", typeof (String), typeof (RenumberingStrategyMaintenanceBackstageOpen), new PropertyMetadata(String.Empty, SearchText_PropertyChanged));

        public String SearchText
        {
            get { return (String) GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }
        #endregion

        #endregion

        #region Constructor

        public RenumberingStrategyMaintenanceBackstageOpen()
        {
            SearchResults = new ReadOnlyObservableCollection<RenumberingStrategyInfo>(_searchResults);
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked when the collection of available validation templates changes.
        /// </summary>
        private void AvailableRenumberingStrategies_BulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            RefreshAvailableRenumberingStrategies();
        }

        /// <summary>
        ///     Invoked when the view model attached to this view changes.
        /// </summary>
        private static void ViewModel_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as RenumberingStrategyMaintenanceBackstageOpen;
            if (senderControl == null) return;

            var oldModel = e.OldValue as RenumberingStrategyMaintenanceViewModel;
            if (oldModel != null)
                oldModel.AvailableRenumberingStrategies.BulkCollectionChanged -=
                    senderControl.AvailableRenumberingStrategies_BulkCollectionChanged;
            var newModel = e.NewValue as RenumberingStrategyMaintenanceViewModel;
            if (newModel != null)
                newModel.AvailableRenumberingStrategies.BulkCollectionChanged +=
                    senderControl.AvailableRenumberingStrategies_BulkCollectionChanged;
            senderControl.RefreshAvailableRenumberingStrategies();
        }

        /// <summary>
        ///     Invoked when the search text changes.
        /// </summary>
        private static void SearchText_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as RenumberingStrategyMaintenanceBackstageOpen;
            if (senderControl != null) senderControl.RefreshAvailableRenumberingStrategies();
        }

        /// <summary>
        ///     Invoked when the user double clicks on the search list.
        /// </summary>
        private void XSearchResults_OnMouseDoubleClick(Object sender, MouseButtonEventArgs e)
        {
            var senderControl = sender as ListBox;
            if (senderControl == null) return;

            var item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (item == null) return;

            var target = senderControl.SelectedItem as RenumberingStrategyInfo;
            if (target == null) return;

            ViewModel.OpenCommand.Execute(target.Id);
        }

        #endregion

        #region Methods

        private void RefreshAvailableRenumberingStrategies()
        {
            _searchResults.Clear();

            if (ViewModel == null) return;

            foreach (var match in ViewModel.GetRenumberingStrategiesMatches(SearchText))
            {
                _searchResults.Add(match);
            }
        }

        #endregion

        private void XSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ListBox;
                if (senderControl == null) return;

                var item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
                if (item == null) return;

                var target = senderControl.SelectedItem as RenumberingStrategyInfo;
                if (target == null) return;

                ViewModel.OpenCommand.Execute(target.Id);

                e.Handled = true;
            }
        }
    }
}
