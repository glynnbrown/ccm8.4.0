﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.RenumberingStrategyMaintenance
{
    /// <summary>
    /// Interaction logic for RenumberingStrategyMaintenanceHomeTab.xaml
    /// </summary>
    public partial class RenumberingStrategyMaintenanceHomeTab
    {
        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof (RenumberingStrategyMaintenanceViewModel), typeof (RenumberingStrategyMaintenanceHomeTab),
            new PropertyMetadata(null));

        public RenumberingStrategyMaintenanceViewModel ViewModel
        {
            get { return (RenumberingStrategyMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public RenumberingStrategyMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}