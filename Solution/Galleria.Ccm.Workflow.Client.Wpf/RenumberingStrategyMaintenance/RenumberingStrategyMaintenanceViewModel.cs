﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#region Version History: CCM 801

// V8-28923 : A.Silva
//      Localized strings.
//      Tidied up.

#endregion
#region Version History: (CCM 8.1.1)
// V8-30270 : I.George
//  Removed IsDirty check in save can execute command
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows;
using System.Windows.Input;
using Csla;
using Csla.Rules;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using FrameworkHelpers = Galleria.Framework.Controls.Wpf.Helpers;

namespace Galleria.Ccm.Workflow.Client.Wpf.RenumberingStrategyMaintenance
{
    /// <summary>
    ///     Viewmodel implementation for the <see cref="RenumberingStrategyMaintenanceOrganiser"/>.
    /// </summary>
    public sealed class RenumberingStrategyMaintenanceViewModel : ViewModelAttachedControlObject<RenumberingStrategyMaintenanceOrganiser>
    {
        #region Constants

        /// <summary>
        ///     Category name for any exceptions raised to Gibraltar from here.
        /// </summary>
        private const String ExceptionCategory = "RenumberingStrategyMaintenance";

        #endregion

        #region Fields

        /// <summary>
        ///     Model permission instance.
        /// </summary>
        private ModelPermission<RenumberingStrategy> _itemPermission =  new ModelPermission<RenumberingStrategy>();

        /// <summary>
        ///     View of all the existing <see cref="RenumberingStrategyInfo"/> for the current entity.
        /// </summary>
        private readonly RenumberingStrategyInfoListView _masterRenumberingStrategyInfoListView = new RenumberingStrategyInfoListView();

        #endregion

        #region Binding Paths

        #region Properties

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="AvailableRenumberingStrategies"/>.
        /// </summary>
        public static readonly PropertyPath AvailableRenumberingStrategiesProperty =
            GetPropertyPath(o => o.AvailableRenumberingStrategies);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="CurrentRenumberingStrategy"/>.
        /// </summary>
        public static readonly PropertyPath CurrentRenumberingStrategyProperty =
            GetPropertyPath(o => o.CurrentRenumberingStrategy);

        #endregion

        #region Commands

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="NewCommand"/>.
        /// </summary>
        public static readonly PropertyPath NewCommandProperty = GetPropertyPath(o => o.NewCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="OpenCommand"/>.
        /// </summary>
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath(o => o.OpenCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="SaveCommand"/>.
        /// </summary>
        public static readonly PropertyPath SaveCommandProperty = GetPropertyPath(o => o.SaveCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="SaveAsCommand"/>.
        /// </summary>
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath(o => o.SaveAsCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="SaveAndNewCommand"/>.
        /// </summary>
        public static readonly PropertyPath SaveAndNewCommandProperty = GetPropertyPath(o => o.SaveAndNewCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="SaveAndCloseCommand"/>.
        /// </summary>
        public static readonly PropertyPath SaveAndCloseCommandProperty = GetPropertyPath(o => o.SaveAndCloseCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="DeleteCommand"/>.
        /// </summary>
        public static readonly PropertyPath DeleteCommandProperty = GetPropertyPath(o => o.DeleteCommand);

        /// <summary>
        ///     Static <see cref="PropertyPath"/> for the <see cref="CloseCommand"/>.
        /// </summary>
        public static readonly PropertyPath CloseCommandProperty = GetPropertyPath(o => o.CloseCommand);

        #endregion

        #endregion

        #region Properties

        #region AvailableRenumberingStrategies

        /// <summary>
        ///     Gets the value of the <see cref="AvailableRenumberingStrategies"/> property.
        /// </summary>
        public ReadOnlyBulkObservableCollection<RenumberingStrategyInfo> AvailableRenumberingStrategies
        {
            get { return _masterRenumberingStrategyInfoListView.BindableCollection; }
        }

        #endregion

        #region CurrentRenumberingStrategy

        /// <summary>
        ///     Holds the value for the <see cref="CurrentRenumberingStrategy"/> property.
        /// </summary>
        private RenumberingStrategy _currentRenumberingStrategy;

        /// <summary>
        ///     Gets or sets the value of the <see cref="CurrentRenumberingStrategy"/> property, notifying when the value changes.
        /// </summary>
        public RenumberingStrategy CurrentRenumberingStrategy
        {
            get { return _currentRenumberingStrategy; }
            set
            {
                _currentRenumberingStrategy = value;
                OnPropertyChanged(CurrentRenumberingStrategyProperty);
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="RenumberingStrategyMaintenanceViewModel" />.
        /// </summary>
        public RenumberingStrategyMaintenanceViewModel()
        {
            _masterRenumberingStrategyInfoListView.FetchAllForEntity();
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region New

        /// <summary>
        ///     Reference to the current instance for <see cref="NewCommand"/>.
        /// </summary>
        private RelayCommand _newCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="NewCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand != null) return _newCommand;

                _newCommand = new RelayCommand(o => New_Executed(), o => New_CanExecute(), false)
                {
                    FriendlyName = Message.Generic_New,
                    FriendlyDescription = Message.Generic_New_Tooltip,
                    Icon = ImageResources.New_32,
                    SmallIcon = ImageResources.New_16,
                    InputGestureModifiers = ModifierKeys.Control,
                    InputGestureKey = Key.N
                };
                ViewModelCommands.Add(_newCommand);
                return _newCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="NewCommand"/>.
        /// </summary>
        private Boolean New_CanExecute()
        {
            if (!_itemPermission.CanCreate) 
            {
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }
            
            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="NewCommand"/> is executed.
        /// </summary>
        private void New_Executed()
        {
            if(!ContinueWithItemChange()) return;

            // Create new item and begin editing.
            this.CurrentRenumberingStrategy = RenumberingStrategy.NewRenumberingStrategy(App.ViewState.EntityId);

            // Close the Backstage if open.
            LocalHelper.SetRibbonBackstageState(AttachedControl, false);
        }

        #endregion

        #region Open

        /// <summary>
        ///     Reference to the current instance for <see cref="OpenCommand"/>.
        /// </summary>
        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="OpenCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand != null) return _openCommand;

                _openCommand = new RelayCommand<Int32?>(Open_Executed, Open_CanExecute)
                {
                    FriendlyName = Message.Generic_Open
                };
                ViewModelCommands.Add(_openCommand);
                return _openCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="OpenCommand"/>.
        /// </summary>
        private Boolean Open_CanExecute(Int32? itemId)
        {
            if (!_itemPermission.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }
            
            if (itemId == null)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="OpenCommand"/> is executed.
        /// </summary>
        private void Open_Executed(Int32? itemId)
        {
            if (!ContinueWithItemChange()) return;

            ShowWaitCursor(true);

            try
            {
                if (itemId != null) this.CurrentRenumberingStrategy = RenumberingStrategy.FetchById(itemId.Value);
            }
            catch (DataPortalException e)
            {
                ShowWaitCursor(false);

                LocalHelper.RecordException(e, ExceptionCategory);
                FrameworkHelpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                ShowWaitCursor(true);
            }

            // Close the backstage.
            LocalHelper.SetRibbonBackstageState(AttachedControl, false);

            ShowWaitCursor(false);
        }

        #endregion

        #region Save

        /// <summary>
        ///     Reference to the current instance for <see cref="SaveCommand"/>.
        /// </summary>
        private RelayCommand _saveCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="SaveCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand != null) return _saveCommand;

                _saveCommand = new RelayCommand(o => Save_Executed(), o => Save_CanExecute())
                {
                    FriendlyName = Message.Generic_Save,
                    FriendlyDescription = Message.Generic_Save_Tooltip,
                    Icon = ImageResources.Save_32,
                    SmallIcon = ImageResources.Save_16,
                    InputGestureModifiers = ModifierKeys.Control,
                    InputGestureKey = Key.S
                };
                ViewModelCommands.Add(_saveCommand);
                return _saveCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="SaveCommand"/>.
        /// </summary>
        private Boolean Save_CanExecute()
        {
            if (this.CurrentRenumberingStrategy == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            if (this.CurrentRenumberingStrategy.IsNew && !_itemPermission.CanCreate)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            if (!this.CurrentRenumberingStrategy.IsNew && !_itemPermission.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (!this.CurrentRenumberingStrategy.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            if ((this.AttachedControl != null && !LocalHelper.AreBindingsValid(this.AttachedControl)))
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }
            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="SaveCommand"/> is executed.
        /// </summary>
        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        #endregion

        #region SaveAs

        /// <summary>
        ///     Reference to the current instance for <see cref="SaveAsCommand"/>.
        /// </summary>
        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="SaveAsCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand != null) return _saveAsCommand;

                _saveAsCommand = new RelayCommand(o => SaveAs_Executed(), o => SaveAs_CanExecute())
                {
                    FriendlyName = Message.Generic_SaveAs,
                    FriendlyDescription = Message.Generic_Save_Tooltip,
                    Icon = ImageResources.SaveAs_32,
                    SmallIcon = ImageResources.SaveAs_16,
                    InputGestureModifiers = ModifierKeys.Control,
                    InputGestureKey = Key.F12
                };
                ViewModelCommands.Add(_saveAsCommand);
                return _saveAsCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="SaveAsCommand"/>.
        /// </summary>
        private Boolean SaveAs_CanExecute()
        {
            if (!_itemPermission.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            if (this.CurrentRenumberingStrategy == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            if (!this.CurrentRenumberingStrategy.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }
                
            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="SaveAsCommand"/> is executed.
        /// </summary>
        private void SaveAs_Executed()
        {
            String copyName;

            Predicate<String> isUniqueCheck = s =>
            {
                ShowWaitCursor(true);

                Boolean returnValue = _masterRenumberingStrategyInfoListView.Model
                    .All(info => !String.Equals(info.Name, s, StringComparison.InvariantCultureIgnoreCase));

                ShowWaitCursor(false);

                return returnValue;
            };

            Boolean nameAccepted = CommonHelper.GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;

            ShowWaitCursor(true);

            RenumberingStrategy itemCopy = CurrentRenumberingStrategy.Copy();
            itemCopy.Name = copyName;
            CurrentRenumberingStrategy = itemCopy;

            ShowWaitCursor(false);

            SaveCurrentItem();
        }

        #endregion

        #region SaveAndNew

        /// <summary>
        ///     Reference to the current instance for <see cref="SaveAndNewCommand"/>.
        /// </summary>
        private RelayCommand _saveAndNewCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="SaveAndNewCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand != null) return _saveAndNewCommand;

                _saveAndNewCommand = new RelayCommand(o => SaveAndNew_Executed(), o => Save_CanExecute())
                {
                    FriendlyName = Message.Generic_SaveAndNew,
                    FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                    SmallIcon = ImageResources.SaveAndNew_16
                };
                ViewModelCommands.Add(_saveAndNewCommand);
                return _saveAndNewCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the <see cref="SaveAndNewCommand"/> is executed.
        /// </summary>
        private void SaveAndNew_Executed()
        {
            if (SaveCurrentItem()) this.NewCommand.Execute();
        }

        #endregion

        #region SaveAndClose

        /// <summary>
        ///     Reference to the current instance for <see cref="SaveAndCloseCommand"/>.
        /// </summary>
        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="SaveAndCloseCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand != null) return _saveAndCloseCommand;

                _saveAndCloseCommand = new RelayCommand(o => SaveAndClose_Executed(), o => Save_CanExecute())
                {
                    FriendlyName = Message.Generic_SaveAndClose,
                    FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                    SmallIcon = ImageResources.SaveAndClose_16
                };
                ViewModelCommands.Add(_saveAndCloseCommand);
                return _saveAndCloseCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the <see cref="SaveAndCloseCommand"/> is executed.
        /// </summary>
        private void SaveAndClose_Executed()
        {
            if (SaveCurrentItem()) CommonHelper.GetWindowService().CloseWindow(AttachedControl);
        }

        #endregion

        #region Delete

        /// <summary>
        ///     Reference to the current instance for <see cref="DeleteCommand"/>.
        /// </summary>
        private RelayCommand _deleteCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="DeleteCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand != null) return _deleteCommand;

                _deleteCommand = new RelayCommand(o => Delete_Executed(), o => Delete_CanExecute())
                {
                    FriendlyName = Message.Generic_Delete,
                    FriendlyDescription = Message.Generic_Delete_Tooltip,
                    SmallIcon = ImageResources.Delete_16
                };
                ViewModelCommands.Add(_deleteCommand);
                return _deleteCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the UI needs to assess the availability of the <see cref="DeleteCommand"/>.
        /// </summary>
        private Boolean Delete_CanExecute()
        {
            if (!_itemPermission.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }
            
            if (this.CurrentRenumberingStrategy == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }
            
            if (this.CurrentRenumberingStrategy.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }
                
            return true;
        }

        /// <summary>
        ///		Invoked whenever the <see cref="DeleteCommand"/> is executed.
        /// </summary>
        private void Delete_Executed()
        {
            if (!CommonHelper.GetWindowService().ConfirmDeleteWithUser(this.CurrentRenumberingStrategy.Name)) return;

            ShowWaitCursor(true);

            RenumberingStrategy itemToDelete = this.CurrentRenumberingStrategy;
            itemToDelete.Delete();

            this.NewCommand.Execute();

            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException e)
            {
                DeleteOnDataPortalException(e, itemToDelete.Name);
                return;
            }

            _masterRenumberingStrategyInfoListView.FetchAllForEntity();

            ShowWaitCursor(false);
        }

        #endregion

        #region Close

        /// <summary>
        ///     Reference to the current instance for <see cref="CloseCommand"/>.
        /// </summary>
        private RelayCommand _closeCommand;

        /// <summary>
        ///     Gets the reference to the <see cref="CloseCommand"/>.
        /// </summary>
        /// <remarks>Lazy instantiates a new command if there was not an instance already.</remarks>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand != null) return _closeCommand;

                _closeCommand = new RelayCommand(o => Close_Executed())
                {
                    FriendlyName = Message.Backstage_Close,
                    SmallIcon = ImageResources.Backstage_Close,
                    InputGestureModifiers = ModifierKeys.Control,
                    InputGestureKey = Key.F4
                };
                ViewModelCommands.Add(_closeCommand);
                return _closeCommand;
            }
        }

        /// <summary>
        ///		Invoked whenever the <see cref="CloseCommand"/> is executed.
        /// </summary>
        private void Close_Executed()
        {
            CommonHelper.GetWindowService().CloseWindow(AttachedControl);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        ///     Shows a warning requesting the user ok to continue if the current item is dirty.
        /// </summary>
        /// <returns><c>True</c> if the action should continue, <c>false</c> otherwise.</returns>
        public Boolean ContinueWithItemChange()
        {
            return CommonHelper.GetWindowService()
                .ContinueWithItemChange(this.CurrentRenumberingStrategy, this.SaveCommand);
        }

        private Boolean SaveCurrentItem()
        {
            Int32? itemId = CurrentRenumberingStrategy.Id;

            // Check the item unique value.
            String newName;

            //  Prompt for a new name if the current one is not unique.
            //  Return without saving if the user cancels.
            Predicate<string> isUniqueCheck = name =>
            {
                ShowWaitCursor(true);

                Boolean duplicateFound = _masterRenumberingStrategyInfoListView
                    .Model.Where(info => info.Id != itemId)
                          .Any(info => String.Equals(info.Name, name, StringComparison.InvariantCultureIgnoreCase));

                ShowWaitCursor(false);

                return !duplicateFound;
            };

            Boolean nameAccepted = CommonHelper
                .GetWindowService().PromptIfNameIsNotUnique(isUniqueCheck, CurrentRenumberingStrategy.Name, out newName);
            if (!nameAccepted)
            {
                return false;
            }
            if (CurrentRenumberingStrategy.Name != newName) CurrentRenumberingStrategy.Name = newName;

            ShowWaitCursor(true);

            try
            {
                CurrentRenumberingStrategy = CurrentRenumberingStrategy.Save();
            }
            catch (DataPortalException e)
            {
                SaveCurrentItemOnDataPortalException(e);
                return false;
            }

            _masterRenumberingStrategyInfoListView.FetchAllForEntity();

            ShowWaitCursor(false);

            return true;
        }

        public IEnumerable<RenumberingStrategyInfo> GetRenumberingStrategiesMatches(String nameCriteria)
        {
            String invariantCriteria = nameCriteria.ToLowerInvariant();
            return this.AvailableRenumberingStrategies
                .Where(info => info.Name.ToLowerInvariant().Contains(invariantCriteria))
                .OrderBy(info => info.ToString());
        }

        /// <summary>
        ///     Handles the <see cref="DataPortalException"/> for the <see cref="SaveCurrentItem"/> method.
        /// </summary>
        private void SaveCurrentItemOnDataPortalException(DataPortalException e)
        {
            ShowWaitCursor(false);

            Exception rootException = e.GetBaseException();
            LocalHelper.RecordException(rootException, ExceptionCategory);

            if (rootException is ConcurrencyException)
            {
                if (CommonHelper.GetWindowService()
                                .ShowConcurrencyReloadPrompt(this.CurrentRenumberingStrategy.Name))
                    this.CurrentRenumberingStrategy = RenumberingStrategy.FetchById(this.CurrentRenumberingStrategy.Id);
            }
            else
            {
                CommonHelper.GetWindowService()
                            .ShowErrorOccurredMessage(this.CurrentRenumberingStrategy.Name, OperationType.Save);
            }
        }

        /// <summary>
        ///     Handles the <see cref="DataPortalException"/> for the <see cref="Delete_Executed"/> method.
        /// </summary>
        private void DeleteOnDataPortalException(DataPortalException e, String itemName)
        {
            ShowWaitCursor(false);
            LocalHelper.RecordException(e, ExceptionCategory);
            CommonHelper.GetWindowService().ShowErrorOccurredMessage(itemName, OperationType.Delete);
        }

        #endregion

        #region Static Helpers

        /// <summary>
        ///     Helper method to get the property path for the <see cref="RenumberingStrategyMaintenanceViewModel" /> property indicated by the
        ///     <paramref name="expression" />.
        /// </summary>
        /// <param name="expression">Lambda expression to access the property.</param>
        /// <returns>
        ///     Returns a <see cref="PropertyPath" /> Object with the path to the property in the
        ///     <paramref name="expression" />.
        /// </returns>
        private static PropertyPath GetPropertyPath(
            Expression<Func<RenumberingStrategyMaintenanceViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        /// <summary>
        ///     Checks whether the user has the provided <paramref name="type" /> of permission for the objects of the given type.
        /// </summary>
        /// <typeparam name="T">Type of the objects involved in the permission.</typeparam>
        /// <param name="type">Type of permission.</param>
        /// <returns><c>True</c> if the user has permission for the action on the type, <c>false</c> otherwise.</returns>
        private static Boolean HasPermission<T>(AuthorizationActions type) where T : BusinessBase<T>
        {
            return BusinessRules.HasPermission(type, typeof(T));
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                AttachedControl = null;
                this.CurrentRenumberingStrategy = null;
                _masterRenumberingStrategyInfoListView.Dispose();
            }

            IsDisposed = true;
        }

        #endregion
    }
}