﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0.0)
// CCM-26520 : J.Pickup
//	Created
// CCM-27891 : J.Pickup
//	Validation and renumbering added
#endregion

#region Version History: (CCM 8.0.2)
//V8-29255 : L.Ineson
//  Removed old sequences code.
#endregion

#region Version History: (CCM 8.2.0)
// V8-30461 : M.Shelley
//  Restrict the assortment list to those assortments that match the current selected product group
#endregion

#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
// V8-32810 : M.Pettit
//  Added Print templates
// CCM-18624 : D.Pleasance
//  Added missing IsPrintTemplate property. Also removed unused property IsSequence.
#endregion

#endregion

using System;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using System.Windows;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    public class ContentLookupManualAssignViewModel : ViewModelAttachedControlObject<ContentLookupManualAssign>
    {
        #region Fields

        private Boolean? _dialogResult;
        private Boolean _showNext = false;
        private Boolean _showBack = false;

        private ContentLookupItemType _currentItemType;
        private String _friendlyDescription;

        private ProductUniverseInfoList _productUniverses;
        private AssortmentInfoList _assortments;
        private MetricProfileInfoList _metricProfiles;
        private InventoryProfileInfoList _inventoryProfiles;
        private ConsumerDecisionTreeInfoList _cdts;
        private AssortmentMinorRevisionInfoList _assortmentMinorRevisons;
        private PerformanceSelectionInfoList _performanceSelections;
        private ClusterSchemeInfoList _clusterSchemes;
        private ClusterScheme _detailedClusterScheme;
        private BlockingInfoList _blockings;
        private RenumberingStrategyInfoList _renumberingStrategies;
        private ValidationTemplateInfoList _validationTemplates;
        private PlanogramNameTemplateInfoList _planogramNameTemplates;
        private PrintTemplateInfoList _printTemplates;

        private Boolean _isPerformanceSelection = false;
        private Boolean _isProductUniverse = false;
        private Boolean _isMetricProfile = false;
        private Boolean _isInventoryProfile = false;
        private Boolean _isAssortment = false;
        private Boolean _isCdt = false;
        private Boolean _isAssortmentMinorRevision = false;
        private Boolean _isBlocking = false;
        private Boolean _isCluster = false;
        private Boolean _isDetailedClusterScheme = false;
        private Boolean _isRenumberingStrategy = false;
        private Boolean _isValidationTemplate = false;
        private Boolean _isPlanogramNameTemplate = false;
        private Boolean _isPrintTemplate = false;

        private Object _selectedItem;
        private Object _secondSelectedItem;

        #endregion

        #region Properties

        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (AttachedControl != null) AttachedControl.DialogResult = value;
            }
        }

        public String FriendlyDescription
        {
            get { return _friendlyDescription; }
            set 
            { 
                _friendlyDescription = value;
                OnPropertyChanged(FriendlyDescriptionProperty);
            }
        }

        public Boolean ShowNext
        {
            get { return _showNext; }
            set 
            {
                _showNext = value;
                OnPropertyChanged(ShowNextProperty);
            }
        }

        public Boolean ShowBack
        {
            get { return _showBack; }
            set
            {
                _showBack = value;
                OnPropertyChanged(ShowBackProperty);
            }
        }

        public String WindowTitle
        {
            get { return String.Format(Message.ContentLookup_ManualAssign_Title, ContentLookupItemTypeHelper.FriendlyNames[_currentItemType]); }
        }

        public Object SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }

        public Object SecondSelectedItem
        {
            get { return _secondSelectedItem; }
            set { _secondSelectedItem = value; }
        }

        public ProductUniverseInfoList ProductUniverses
        {
            get { return _productUniverses; }
            set { _productUniverses = value; }
        }

        public AssortmentInfoList Assortments
        {
            get { return _assortments; }
            set { _assortments = value; }
        }

        public MetricProfileInfoList MetricProfiles
        {
            get { return _metricProfiles; }
            set { _metricProfiles = value; }
        }

        public InventoryProfileInfoList InventoryProfiles
        {
            get { return _inventoryProfiles; }
            set { _inventoryProfiles = value; }
        }

        public ConsumerDecisionTreeInfoList Cdts
        {
            get { return _cdts; }
            set { _cdts = value; }
        }

        public AssortmentMinorRevisionInfoList AssortmentMinorRevisions
        {
            get { return _assortmentMinorRevisons; }
            set { _assortmentMinorRevisons = value; }
        }

        public PerformanceSelectionInfoList PerformanceSelections
        {
            get { return _performanceSelections; }
            set { _performanceSelections = value; }
        }

        public BlockingInfoList Blockings
        {
            get { return _blockings; }
            set { _blockings = value; }
        }

        public RenumberingStrategyInfoList RenumberingStrategies
        {
            get { return _renumberingStrategies; }
            set { _renumberingStrategies = value; }
        }

        public ValidationTemplateInfoList ValidationTemplates
        {
            get { return _validationTemplates; }
            set { _validationTemplates = value; }
        }

        public PlanogramNameTemplateInfoList PlanogramNameTemplates
        {
            get { return _planogramNameTemplates; }
            set { _planogramNameTemplates = value; }
        }

        public PrintTemplateInfoList PrintTemplates
        {
            get { return _printTemplates; }
            set { _printTemplates = value; }
        }

        public ClusterSchemeInfoList ClusterSchemes
        {
            get { return _clusterSchemes; }
            set { _clusterSchemes = value; }
        }

        public ClusterScheme DetailedClusterScheme
        {
            get { return _detailedClusterScheme; }
            set 
            { 
                _detailedClusterScheme = value;
                OnPropertyChanged(DetailedClusterSchemeProperty);
            }
        }

        public Boolean IsProductUniverse
        {
            get { return _isProductUniverse; }
            set { _isProductUniverse = value; }
        }

        public Boolean IsAssortment
        {
            get { return _isAssortment; }
            set { _isAssortment = value; }
        }

        public Boolean IsMetricProfile
        {
            get { return _isMetricProfile; }
            set { _isMetricProfile = value; }
        }

        public Boolean IsInventoryProfile
        {
            get { return _isInventoryProfile; }
            set { _isInventoryProfile = value; }
        }
        
        public Boolean IsCdt
        {
            get { return _isCdt; }
            set { _isCdt = value; }
        }

        public Boolean IsAssortmentMinorRevision
        {
            get { return _isAssortmentMinorRevision; }
            set { _isAssortmentMinorRevision = value; }
        }

        public Boolean IsPerformanceSelection
        {
            get { return _isPerformanceSelection; }
            set { _isPerformanceSelection = value; }
        }

        public Boolean IsBlocking
        {
            get { return _isBlocking; }
            set { _isBlocking = value; }
        }

        public Boolean IsClusterScheme
        {
            get { return _isCluster; }
            set { _isCluster = value; }
        }

        public Boolean IsDetailedClusterScheme
        {
            get { return _isDetailedClusterScheme; }
            set 
            { 
                _isDetailedClusterScheme = value;
                OnPropertyChanged(IsDetailedClusterSchemeProperty);
            }
        }

        public Boolean IsRenumberingStrategy
        {
            get { return _isRenumberingStrategy; }
            set { _isRenumberingStrategy = value; }
        }

        public Boolean IsValidationTemplate
        {
            get { return _isValidationTemplate; }
            set { _isValidationTemplate = value; }
        }

        public Boolean IsPlanogramNameTemplate
        {
            get { return _isPlanogramNameTemplate; }
            set { _isPlanogramNameTemplate = value; }
        }

        public Boolean IsPrintTemplate
        {
            get { return _isPrintTemplate; }
            set { _isPrintTemplate = value; }
        }

        #endregion

        #region PropertyPaths

        //Properties
        public static readonly PropertyPath ShowNextProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.ShowNext);
        public static readonly PropertyPath ShowBackProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.ShowBack);
        public static readonly PropertyPath IsDetailedClusterSchemeProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.IsDetailedClusterScheme);
        public static readonly PropertyPath DetailedClusterSchemeProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.DetailedClusterScheme);
        public static readonly PropertyPath FriendlyDescriptionProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.FriendlyDescription);

        //Commands
        public static readonly PropertyPath OkPressedCommandProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.OkPressedCommand);
        public static readonly PropertyPath CancelPressedCommandProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.CancelPressedCommand);
        public static readonly PropertyPath NextCommandProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.NextCommand);
        public static readonly PropertyPath BackCommandProperty = WpfHelper.GetPropertyPath<ContentLookupManualAssignViewModel>(p => p.BackCommand);

        #endregion

        #region Constructor

        public ContentLookupManualAssignViewModel(ContentLookupItemType itemType, ProductGroupViewModel productGroupVm = null)
        {
            _currentItemType = itemType;
            FriendlyDescription = String.Format(Message.ContentLookup_ManualAssign_Description, ContentLookupItemTypeHelper.FriendlyNames[_currentItemType]);

            switch (itemType)
            {
                case ContentLookupItemType.MetricProfile:
                    MetricProfiles = MetricProfileInfoList.FetchByEntityId(App.ViewState.EntityId);
                    IsMetricProfile  = true;
                    break;
                case ContentLookupItemType.InventoryProfile:
                    InventoryProfiles = InventoryProfileInfoList.FetchByEntityId(App.ViewState.EntityId);
                    IsInventoryProfile = true;
                    break;
                case ContentLookupItemType.ProductUniverse:
                    ProductUniverses = ProductUniverseInfoList.FetchByEntityId(App.ViewState.EntityId);
                    IsProductUniverse = true;
                    break;
                case ContentLookupItemType.Assortment:
                    if (productGroupVm != null && productGroupVm.ProductGroup != null)
                    {
                        Int32 productGroupId = productGroupVm.ProductGroup.Id;
                        Assortments = AssortmentInfoList.FetchByProductGroupId(App.ViewState.EntityId, productGroupId);
                    }
                    else
                    {
                        Assortments = AssortmentInfoList.FetchByEntityId(App.ViewState.EntityId);
                    }

                    IsAssortment = true;
                    break;
                case ContentLookupItemType.ConsumerDecisionTree:
                    Cdts = ConsumerDecisionTreeInfoList.FetchByEntityId(App.ViewState.EntityId);
                    IsCdt = true;
                    break;
                case ContentLookupItemType.MinorAssortment:
                    AssortmentMinorRevisions = AssortmentMinorRevisionInfoList.FetchByEntityId(App.ViewState.EntityId);
                    IsAssortmentMinorRevision = true;
                    break;
                case ContentLookupItemType.PerformanceSelections:
                    PerformanceSelections = PerformanceSelectionInfoList.FetchByEntityId(App.ViewState.EntityId);
                    _isPerformanceSelection = true;
                    break;
                case ContentLookupItemType.Blocking:
                    Blockings = BlockingInfoList.FetchByEntityId(App.ViewState.EntityId);
                    _isBlocking = true;
                    break;
                case ContentLookupItemType.Cluster:
                    ClusterSchemes = ClusterSchemeInfoList.FetchByEntityId(App.ViewState.EntityId);
                    _isCluster = true;
                    ShowNext = true;
                    break;
                case ContentLookupItemType.RenumberingStrategy:
                    RenumberingStrategies = RenumberingStrategyInfoList.FetchByEntityId(App.ViewState.EntityId);
                    _isRenumberingStrategy = true;
                    break;
                case ContentLookupItemType.ValidationTemplate:
                    ValidationTemplates = ValidationTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
                    _isValidationTemplate = true;
                    break;
                case ContentLookupItemType.PlanogramNameTemplate:
                    PlanogramNameTemplates = PlanogramNameTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
                    _isPlanogramNameTemplate = true;
                    break;
                case ContentLookupItemType.PrintTemplate:
                    PrintTemplates = PrintTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);
                    _isPrintTemplate = true;
                    break;

                default: 
                    Debug.Fail("Type not handled");
                    break;
            }
        }

        #endregion

        #region Commands

        #region OkPressed

        private RelayCommand _okPressedCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand OkPressedCommand
        {
            get
            {
                if (_okPressedCommand == null)
                {
                    _okPressedCommand = new RelayCommand(
                        p => OkPressedCommand_Executed(), p => OkPressedCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Ok,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okPressedCommand);
                }
                return _okPressedCommand;
            }
        }

        private Boolean OkPressedCommand_CanExecute()
        {
            if (!IsClusterScheme )
            {
                if (this.SelectedItem == null)
                {
                    _okPressedCommand.DisabledReason = Message.LocationProductAttributeMaintenance_NoItemSelected;
                    return false;
                }
            }
            else
            {
                if (IsDetailedClusterScheme && SecondSelectedItem == null)
                {
                    _okPressedCommand.DisabledReason = Message.LocationProductAttributeMaintenance_NoItemSelected;
                    return false;
                }
            }
            
            return true;
        }

        private void OkPressedCommand_Executed()
        {
            DialogResult = true;
        }

        #endregion

        #region Next

        private RelayCommand _nextCommand;

        /// <summary>
        /// We proceed next
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => NextCommand_Executed(), p => NextCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Next,
                        FriendlyDescription = Message.Generic_Next,
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        private Boolean NextCommand_CanExecute()
        {
            if (this.SelectedItem == null)
            {
                _nextCommand.DisabledReason = Message.LocationProductAttributeMaintenance_NoItemSelected;
                return false;
            }

            return true;
        }

        private void NextCommand_Executed()
        {
            DetailedClusterScheme = ClusterScheme.FetchById(((ClusterSchemeInfo)SelectedItem).Id);
            FriendlyDescription = String.Format(Message.ContentLookup_ManualAssign_Description, Message.ContentLookup_ManualAssign_ClusterDescription);

            IsDetailedClusterScheme = true;
            ShowNext = false;
            ShowBack = true;
        }

        #endregion

        #region Back

        private RelayCommand _backCommand;

        /// <summary>
        /// We proceed back
        /// </summary>
        public RelayCommand BackCommand
        {
            get
            {
                if (_backCommand == null)
                {
                    _backCommand = new RelayCommand(
                        p => BackCommand_Executed(), p => BackCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Previous,
                        FriendlyDescription = Message.Generic_Previous,
                    };
                    base.ViewModelCommands.Add(_backCommand);
                }
                return _backCommand;
            }
        }

        private Boolean BackCommand_CanExecute()
        {
            return true;
        }

        private void BackCommand_Executed()
        {
            FriendlyDescription = String.Format(Message.ContentLookup_ManualAssign_Description, ContentLookupItemTypeHelper.FriendlyNames[_currentItemType]);

            IsDetailedClusterScheme = false;
            ShowBack = false;
            ShowNext = true;
        }

        #endregion

        #region CancelPressed

        private RelayCommand _cancelPressedCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand CancelPressedCommand
        {
            get
            {
                if (_cancelPressedCommand == null)
                {
                    _cancelPressedCommand = new RelayCommand(
                        p => CancelPressedCommand_Executed(), p => CancelPressedCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Cancel,
                        FriendlyDescription = Message.Generic_Cancel,
                    };
                    base.ViewModelCommands.Add(_cancelPressedCommand);
                }
                return _cancelPressedCommand;
            }
        }

        private Boolean CancelPressedCommand_CanExecute()
        {
            return true;
        }

        private void CancelPressedCommand_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #endregion
    
        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //Remove event handlers
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
