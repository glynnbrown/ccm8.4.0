﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Threading;
using Galleria.Ccm.Model;
using Galleria.Ccm.Common.Wpf.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    /// <summary>
    /// Interaction logic for ContentLookupManualAssign.xaml
    /// </summary>
    public partial class ContentLookupManualAssign : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ContentLookupManualAssignViewModel), typeof(ContentLookupManualAssign),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ContentLookupManualAssignViewModel ViewModel
        {
            get { return (ContentLookupManualAssignViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ContentLookupManualAssign senderControl = (ContentLookupManualAssign)obj;

            if (e.OldValue != null)
            {
                ContentLookupManualAssignViewModel oldModel = (ContentLookupManualAssignViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ContentLookupManualAssignViewModel newModel = (ContentLookupManualAssignViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ContentLookupManualAssign(ContentLookupItemType itemType, ProductGroupViewModel productGroupVm = null)
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = new ContentLookupManualAssignViewModel(itemType, productGroupVm);

            this.Loaded += new RoutedEventHandler(ContentLookupManualAssign_Loaded);

        }

        private void ContentLookupManualAssign_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ContentLookupManualAssign_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        private void xAvailableItemsDisplayProductUniverse_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel.IsClusterScheme)
            {
                this.ViewModel.NextCommand.Execute();
            }
            else
            {
                this.ViewModel.OkPressedCommand.Execute();
            }
        }

        private void xAvailableItemsDisplayProductUniverse_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (this.ViewModel.IsClusterScheme)
                {
                    this.ViewModel.NextCommand.Execute();
                }
                else
                {
                    this.ViewModel.OkPressedCommand.Execute();
                }
                e.Handled = true;
            }
        }

        #endregion

        #region Window Close


        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //Handle closing if neccessary
            }

        }


        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion


        
    }
}
