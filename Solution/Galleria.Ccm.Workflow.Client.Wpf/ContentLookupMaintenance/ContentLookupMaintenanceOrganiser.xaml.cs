﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
// CCM-27891 : J.Pickup
//	Validation and renumbering added
#endregion
#region Version History: (CCM 810)
// V8-29752 : A.Probyn
//  Updated column set
#endregion
#region Version History : CCM820
//  V8-30958 : L.Ineson
//      Updated after column manager changes.
//  V8-30850 : A.Probyn
//      Updated column order to be alphabetical.   
#endregion
#region Version History: (CCM 830)
// V8-31832 : L.Ineson
//  Added support for calculated columns.
// V8-31831 : A.Probyn
//  Added Planogram Info column for CustomText 1-50 as users may now be using these
//  to identify planogram names.
// V8-32810 : M.Pettit
//  Added Print Template column
#endregion
#endregion

using System;
using System.Linq;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.ModelObjectViewModels;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using System.Collections.Generic;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    /// <summary>
    /// Interaction logic for ContentLookupMaintenanceOrganiser.xaml
    /// </summary>
    public partial class ContentLookupMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Fields
        private ColumnLayoutManager _planogramColumnLayoutManager;
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ContentLookupMaintenanceViewModel), typeof(ContentLookupMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ContentLookupMaintenanceViewModel ViewModel
        {
            get { return (ContentLookupMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ContentLookupMaintenanceOrganiser senderControl = (ContentLookupMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                ContentLookupMaintenanceViewModel oldModel = (ContentLookupMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ContentLookupMaintenanceViewModel newModel = (ContentLookupMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ContentLookupMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramContentLinkMaintenance);

            this.ViewModel = new ContentLookupMaintenanceViewModel();

            this.Loaded += new RoutedEventHandler(ProductMaintenanceOrganiser_Loaded);

        }

        #endregion

        #region Event Handlers

        private void ProductMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ProductMaintenanceOrganiser_Loaded;

            //create the layout factory:
            PlanogramInfoColumnLayoutFactory factory = new PlanogramInfoColumnLayoutFactory(typeof(ContentLookupRow));

            //exclude name and plan group properties as they will be appended automatically.
            factory.ExcludedProperties.Clear();
            factory.ExcludedProperties.Add(PlanogramInfo.NameProperty);
            factory.ExcludedProperties.Add(PlanogramInfo.PlanogramGroupNameProperty);

            //clear all visible properties
            factory.SetVisibleFields(new List<String>());

            //create planogram grid column manager
            _planogramColumnLayoutManager =
                new ColumnLayoutManager(factory,
                    DisplayUnitOfMeasureCollection.NewDisplayUnitOfMeasureCollection(App.ViewState.CurrentEntityView.Model),
                    "ContentLookup", "PlanogramInfo.{0}");
            _planogramColumnLayoutManager.IncludeUnseenColumns = false;
            _planogramColumnLayoutManager.CalculatedColumnPath = "CalculatedValueResolver";
            _planogramColumnLayoutManager.ColumnSetChanging += PlanogramColumnLayoutManager_ColumnSetChanging;
            _planogramColumnLayoutManager.AttachDataGrid(xAvailableContentLookupItemsDisplay);

            

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        /// <summary>
        /// Called when the column manager is generating a new columnset.
        /// </summary>
        private void PlanogramColumnLayoutManager_ColumnSetChanging(object sender, ColumnLayoutManager.ColumnLayoutColumnSetEventArgs e)
        {
            DataGridColumnCollection columnSet = e.ColumnSet;

            //Add default columns
            Int32 colIdx = 0;

            String contentLookupGroup = Message.ContentLookup_AvailableContentLookupItemsDisplay_ContentLookupGroupName;
            String customValuesGroup = Message.ContentLookup_AvailableContentLookupItemsDisplay_CustomGroupName;

            DataGridExtendedTextColumn nameGridCol = new DataGridExtendedTextColumn
            {
                Header = Galleria.Ccm.Common.Wpf.Resources.Language.Message.PlanRepository_PlanogramNameColumnHeader,
                Binding = new Binding("PlanogramInfo.Name"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = "",
                MinWidth = 50,
                IsReadOnly = true,
                SortDirection = ListSortDirection.Ascending,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++,nameGridCol);

            DataGridExtendedTextColumn planGroupGridCol = new DataGridExtendedTextColumn
            {
                Header = PlanogramInfo.PlanogramGroupNameProperty.FriendlyName,
                Binding = new Binding("PlanogramInfo.PlanogramGroupName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = "",
                MinWidth = 50,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++,planGroupGridCol);

            Dictionary<String, IModelPropertyInfo> customValueFriendlyNameLookup = PlanogramInfo.EnumerateDisplayablePlanogramCustomInfos().ToDictionary(p => p.Name);
            for(int i=1; i < 51; i++)
            {
                DataGridExtendedTextColumn customTextValueCol = new DataGridExtendedTextColumn
                {
                    Header = customValueFriendlyNameLookup.ContainsKey(String.Format("Text{0}", i)) ? customValueFriendlyNameLookup[String.Format("Text{0}", i)].FriendlyName : String.Empty,
                    Binding = new Binding(String.Format("PlanogramInfo.Text{0}", i)),
                    CanUserFilter = true,
                    CanUserSort = true,
                    CanUserReorder = false,
                    CanUserResize = true,
                    CanUserHide = true,
                    ColumnGroupName = customValuesGroup,
                    MinWidth = 50,
                    IsReadOnly = true,
                    Visibility = System.Windows.Visibility.Collapsed,
                    ColumnCellAlignment = HorizontalAlignment.Stretch
                };
                columnSet.Insert(colIdx++, customTextValueCol);
            }

            DataGridExtendedTextColumn assortmentNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_Assortment,
                Binding = new Binding("FriendlyContentLookup.AssortmentName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++,assortmentNameGridCol);

            DataGridExtendedTextColumn blockingNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_Blocking,
                Binding = new Binding("FriendlyContentLookup.BlockingName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++,blockingNameGridCol);

            DataGridExtendedTextColumn clusterSchemeNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_ClusterSchemeName,
                Binding = new Binding("FriendlyContentLookup.ClusterSchemeName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, clusterSchemeNameGridCol);

            DataGridExtendedTextColumn clusterNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_ClusterName,
                Binding = new Binding("FriendlyContentLookup.ClusterName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, clusterNameGridCol);

            DataGridExtendedTextColumn cdtNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_ConsumerDecisionTree,
                Binding = new Binding("FriendlyContentLookup.ConsumerDecisionTreeName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, cdtNameGridCol);

            DataGridExtendedTextColumn inventoryProfileNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_InventoryProfile,
                Binding = new Binding("FriendlyContentLookup.InventoryProfileName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, inventoryProfileNameGridCol);

            DataGridExtendedTextColumn metricProfileNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_MetricProfile,
                Binding = new Binding("FriendlyContentLookup.MetricProfileName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, metricProfileNameGridCol);

            DataGridExtendedTextColumn minorRevisionNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_MinorAssortment,
                Binding = new Binding("FriendlyContentLookup.AssortmentMinorRevisionName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, minorRevisionNameGridCol);

            DataGridExtendedTextColumn performanceNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_PerformanceSelection,
                Binding = new Binding("FriendlyContentLookup.PerformanceSelectionName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, performanceNameGridCol);

            DataGridExtendedTextColumn planogramNameTemplateGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_PlanogramNameTemplate,
                Binding = new Binding("FriendlyContentLookup.PlanogramNameTemplateName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, planogramNameTemplateGridCol);

            DataGridExtendedTextColumn printTemplateGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_PrintTemplate,
                Binding = new Binding("FriendlyContentLookup.PrintTemplateName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, printTemplateGridCol);

            DataGridExtendedTextColumn prodUniverseNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_ProductUniverse,
                Binding = new Binding("FriendlyContentLookup.ProductUniverseName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, prodUniverseNameGridCol);

            DataGridExtendedTextColumn renumberingStrategyNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_RenumberingStrategyName,
                Binding = new Binding("FriendlyContentLookup.RenumberingStrategyName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++, renumberingStrategyNameGridCol);

            DataGridExtendedTextColumn sequenceNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_Sequence,
                Binding = new Binding("FriendlyContentLookup.SequenceName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++,sequenceNameGridCol);

            DataGridExtendedTextColumn validationTemplateNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_ValidationTemplate,
                Binding = new Binding("FriendlyContentLookup.ValidationTemplateName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++,validationTemplateNameGridCol);

            DataGridExtendedTextColumn usersNameGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_User,
                Binding = new Binding("UsersFriendlyName"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++,usersNameGridCol);

            DataGridExtendedTextColumn dateLastModifiedGridCol = new DataGridExtendedTextColumn
            {
                Header = Message.ContentLookup_AvailableContentLookupItemsDisplay_ContentLookupDateModified,
                Binding = new Binding("FriendlyContentLookup.DateLastModified"),
                CanUserFilter = true,
                CanUserSort = true,
                CanUserReorder = false,
                CanUserResize = true,
                CanUserHide = true,
                ColumnGroupName = contentLookupGroup,
                Width = DataGridLength.Auto,
                MinWidth = 100,
                IsReadOnly = true,
                ColumnCellAlignment = HorizontalAlignment.Stretch
            };
            columnSet.Insert(colIdx++,dateLastModifiedGridCol);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        #endregion


        #region Window Close


        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }


        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (_planogramColumnLayoutManager != null)
            {
                _planogramColumnLayoutManager.ColumnSetChanging -= PlanogramColumnLayoutManager_ColumnSetChanging;
                _planogramColumnLayoutManager.Dispose();
            }

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {
                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
