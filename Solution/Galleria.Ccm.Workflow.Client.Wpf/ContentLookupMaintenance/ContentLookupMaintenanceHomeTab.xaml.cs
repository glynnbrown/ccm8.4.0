﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 :J.Pickup
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    /// <summary>
    /// Interaction logic for ContentLookupMaintenanceHomeTab.xaml
    /// </summary>
    public partial class ContentLookupMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ContentLookupMaintenanceViewModel), typeof(ContentLookupMaintenanceHomeTab),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ContentLookupMaintenanceViewModel ViewModel
        {
            get { return (ContentLookupMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ContentLookupMaintenanceHomeTab senderControl = (ContentLookupMaintenanceHomeTab)obj;

            if (e.OldValue != null)
            {
                ContentLookupMaintenanceViewModel oldModel = (ContentLookupMaintenanceViewModel)e.OldValue;

            }

            if (e.NewValue != null)
            {
                ContentLookupMaintenanceViewModel newModel = (ContentLookupMaintenanceViewModel)e.NewValue;


            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ContentLookupMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
