﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using System.Windows.Threading;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    /// <summary>
    /// Interaction logic for ContentLookupGroupClear.xaml
    /// </summary>
    public partial class ContentLookupGroupClear : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ContentLookupGroupClearViewModel), typeof(ContentLookupGroupClear),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ContentLookupGroupClearViewModel ViewModel
        {
            get { return (ContentLookupGroupClearViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ContentLookupGroupClear senderControl = (ContentLookupGroupClear)obj;

            if (e.OldValue != null)
            {
                ContentLookupGroupClearViewModel oldModel = (ContentLookupGroupClearViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ContentLookupGroupClearViewModel newModel = (ContentLookupGroupClearViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ContentLookupGroupClear()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            this.ViewModel = new ContentLookupGroupClearViewModel();

            this.Loaded += new RoutedEventHandler(ContentLookupGroupClear_Loaded);

        }

        private void ContentLookupGroupClear_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= ContentLookupGroupClear_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        #endregion

        #region Window Close


        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //Handle closing if neccessary
            }

        }


        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion
    }
}
