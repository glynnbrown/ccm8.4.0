﻿
#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
// CCM-28025 : J.Pickup
//	_autoAssignSequence set to false for BETA release. (Sequencing hidden).

#endregion
#region Version History: (CCM 802)
// V8-29255 : L.Ineson
//  Removed sequencing.
#endregion
#region Version History: (CCM 8.1.1)
// V8-30236 : I.George
// OkPressedCommand_CanExecute now returns false if no field is set to true
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    public class ContentLookupAutoAssignViewModel : ViewModelAttachedControlObject<ContentLookupAutoAssign>
    {
        #region Fields

        private Boolean _autoAssignProductUniverse = true;
        private Boolean _autoAssignAssortment = true;
        private Boolean _autoAssignSequence = false;
        private Boolean _autoAssignCDT = true;
        private Boolean _autoAssignAssortmentMinorRevision = true;

        private bool? _dialogResult;

        #endregion

        #region PropertyPaths

        //Properties
        public static readonly PropertyPath AutoAssignProductUniverseProperty = WpfHelper.GetPropertyPath<ContentLookupAutoAssignViewModel>(p => p.AutoAssignProductUniverse);
        public static readonly PropertyPath AutoAssignAssortmentProperty = WpfHelper.GetPropertyPath<ContentLookupAutoAssignViewModel>(p => p.AutoAssignAssortment);
        public static readonly PropertyPath AutoAssignConsumerDecisionTreeProperty = WpfHelper.GetPropertyPath<ContentLookupAutoAssignViewModel>(p => p.AutoAssignCDT);
        public static readonly PropertyPath AutoAssignAssortmentMinorRevisionProperty = WpfHelper.GetPropertyPath<ContentLookupAutoAssignViewModel>(p => p.AutoAssignAssortmentMinorRevision);
        
        //Commands
        public static readonly PropertyPath OkPressedCommandProperty = WpfHelper.GetPropertyPath<ContentLookupAutoAssignViewModel>(p => p.OkPressedCommand);
        public static readonly PropertyPath CancelPressedCommandProperty = WpfHelper.GetPropertyPath<ContentLookupAutoAssignViewModel>(p => p.CancelPressedCommand);

        #endregion

        #region Properties

        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (AttachedControl != null) AttachedControl.DialogResult = value;
            }
        }

        public Boolean AutoAssignProductUniverse
        {
            get { return _autoAssignProductUniverse; }
            set { _autoAssignProductUniverse = value; }
        }

        public Boolean AutoAssignAssortment
        {
            get { return _autoAssignAssortment; }
            set { _autoAssignAssortment = value; }
        }

        public Boolean AutoAssignCDT
        {
            get { return _autoAssignCDT; }
            set { _autoAssignCDT = value; }
        }

        public Boolean AutoAssignAssortmentMinorRevision
        {
            get { return _autoAssignAssortmentMinorRevision; }
            set { _autoAssignAssortmentMinorRevision = value; }
        }

        #endregion

        #region Constructor

        public ContentLookupAutoAssignViewModel()
        {

        }

        #endregion

        #region Commands

        #region OkPressed

        private RelayCommand _okPressedCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand OkPressedCommand
        {
            get
            {
                if (_okPressedCommand == null)
                {
                    _okPressedCommand = new RelayCommand(
                        p => OkPressedCommand_Executed(), p => OkPressedCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Ok,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_AutoAssign_OkDescription
                    };
                    base.ViewModelCommands.Add(_okPressedCommand);
                }
                return _okPressedCommand;
            }
        }

        private Boolean OkPressedCommand_CanExecute()
        {
            if (!_autoAssignProductUniverse && !_autoAssignAssortment 
                && !_autoAssignCDT && !_autoAssignAssortmentMinorRevision)
            {
                _okPressedCommand.DisabledReason = Message.ContentLookup_AutoAssign_OkPressed_DisabledReason;
                return false;
            }

            return true;
        }

        private void OkPressedCommand_Executed()
        {
            DialogResult = true;
        }

        #endregion

        #region CancelPressed

        private RelayCommand _cancelPressedCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand CancelPressedCommand
        {
            get
            {
                if (_cancelPressedCommand == null)
                {
                    _cancelPressedCommand = new RelayCommand(
                        p => CancelPressedCommand_Executed(), p => CancelPressedCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Cancel,
                        FriendlyDescription = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelPressedCommand);
                }
                return _cancelPressedCommand;
            }
        }

        private Boolean CancelPressedCommand_CanExecute()
        {
            return true;
        }

        private void CancelPressedCommand_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //Remove event handlers
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
