﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26520 : J.Pickup
//	Created
// CCM-28025 : J.Pickup
//	_clearSequence set to false for BETA release. (Sequencing hidden).
// CCM-27891 : J.Pickup
//	Validation and renumbering added
#endregion
#region Version History: (CCM 802)
// V8-29202 : I.George
//	_ClearSequence set back to true. Sequence is no longer hidden
#endregion
#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
// V8-32810 : M.Pettit
//  Added Print templates
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    public class ContentLookupGroupClearViewModel : ViewModelAttachedControlObject<ContentLookupGroupClear>
    {
        #region Fields

        private Boolean _clearProductUniverse = true;
        private Boolean _clearAssortment = true;
        private Boolean _clearAssortmentMinorRevision = true;
        private Boolean _clearSequence = true;
        private Boolean _clearCDT = true;
        private Boolean _clearMetricProfile = true;
        private Boolean _clearInventoryProfile = true;
        private Boolean _clearPerformanceSelections = true;
        private Boolean _clearPlanogramNameTemplate = true;
        private Boolean _clearBlocking = true;
        private Boolean _clearCluster = true;
        private Boolean _clearRenumberingStrategy = true;
        private Boolean _clearValidationTemplate = true;
        private Boolean _clearPrintTemplate = true;

        private bool? _dialogResult;

        #endregion

        #region PropertyPaths

        //Properties
        public static readonly PropertyPath ClearProductUniverseProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearProductUniverse);
        public static readonly PropertyPath ClearAssortmentProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearAssortment);
        public static readonly PropertyPath ClearAssortmentMinorRevisionProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearAssortmentMinorRevision);
        public static readonly PropertyPath ClearSequenceProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearSequence);
        public static readonly PropertyPath ClearConsumerDecisionTreeProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearCDT);
        public static readonly PropertyPath MetricProfileProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearMetricProfile);
        public static readonly PropertyPath ClearMetricProfileProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearMetricProfile);
        public static readonly PropertyPath ClearInventoryProfileProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearInventoryProfile);
        public static readonly PropertyPath ClearPerformanceSelectionsProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearPerformanceSelections);
        public static readonly PropertyPath ClearBlockingProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearBlocking);
        public static readonly PropertyPath ClearClusterProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearCluster);
        public static readonly PropertyPath ClearRenumberingStrategyProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearRenumberingStrategy);
        public static readonly PropertyPath ClearValidationTemplateProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearValidationTemplate);
        public static readonly PropertyPath ClearPlanogramNameTemplateProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearPlanogramNameTemplate);
        public static readonly PropertyPath ClearPrintTemplateProperty = WpfHelper.GetPropertyPath<ContentLookupGroupClearViewModel>(p => p.ClearPrintTemplate);

        //Commands
        public static readonly PropertyPath OkPressedCommandProperty = WpfHelper.GetPropertyPath<ContentLookupAutoAssignViewModel>(p => p.OkPressedCommand);
        public static readonly PropertyPath CancelPressedCommandProperty = WpfHelper.GetPropertyPath<ContentLookupAutoAssignViewModel>(p => p.CancelPressedCommand);

        #endregion

        #region Properties

        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (AttachedControl != null) AttachedControl.DialogResult = value;
            }
        }

        public Boolean ClearProductUniverse
        {
            get { return _clearProductUniverse; }
            set { _clearProductUniverse = value; }
        }

        public Boolean ClearAssortment
        {
            get { return _clearAssortment; }
            set { _clearAssortment = value; }
        }

        public Boolean ClearAssortmentMinorRevision
        {
            get { return _clearAssortmentMinorRevision; }
            set { _clearAssortmentMinorRevision = value; }
        }

        public Boolean ClearSequence
        {
            get { return _clearSequence; }
            set { _clearSequence = value; }
        }

        public Boolean ClearCDT
        {
            get { return _clearCDT; }
            set { _clearCDT = value; }
        }

        public Boolean ClearMetricProfile
        {
            get { return _clearMetricProfile; }
            set { _clearMetricProfile = value; }
        }

        public Boolean ClearInventoryProfile
        {
            get { return _clearInventoryProfile; }
            set { _clearInventoryProfile = value; }
        }

        public Boolean ClearPerformanceSelections
        {
            get { return _clearPerformanceSelections; }
            set { _clearPerformanceSelections = value; }
        }

        public Boolean ClearPlanogramNameTemplate
        {
            get { return _clearPlanogramNameTemplate; }
            set { _clearPlanogramNameTemplate = value; }
        }

        public Boolean ClearPrintTemplate
        {
            get { return _clearPrintTemplate; }
            set { _clearPrintTemplate = value; }
        }

        public Boolean ClearBlocking
        {
            get { return _clearBlocking; }
            set { _clearBlocking = value; }
        }

        public Boolean ClearCluster
        {
            get { return _clearCluster; }
            set { _clearCluster = value; }
        }

        public Boolean ClearRenumberingStrategy
        {
            get { return _clearRenumberingStrategy; }
            set { _clearRenumberingStrategy = value; }
        }

        public Boolean ClearValidationTemplate
        {
            get { return _clearValidationTemplate; }
            set { _clearValidationTemplate = value; }
        }
        #endregion

        #region Constructor

        public ContentLookupGroupClearViewModel()
        {

        }

        #endregion

        #region Commands

        #region OkPressed

        private RelayCommand _okPressedCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand OkPressedCommand
        {
            get
            {
                if (_okPressedCommand == null)
                {
                    _okPressedCommand = new RelayCommand(
                        p => OkPressedCommand_Executed(), p => OkPressedCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Ok,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_AutoAssign_OkDescription
                    };
                    base.ViewModelCommands.Add(_okPressedCommand);
                }
                return _okPressedCommand;
            }
        }

        private Boolean OkPressedCommand_CanExecute()
        {
            if (!_clearProductUniverse && !_clearAssortment  && !_clearAssortmentMinorRevision && !_clearBlocking 
                && !_clearCDT && !_clearCluster && !_clearInventoryProfile && !_clearMetricProfile 
                && !_clearPerformanceSelections && !_clearSequence && !_clearRenumberingStrategy && !_clearValidationTemplate)
            {
                _okPressedCommand.DisabledReason = Message.ContentLookup_AutoAssign_OkPressed_DisabledReason;
                return false;
            }

            return true;
        }

        private void OkPressedCommand_Executed()
        {
            DialogResult = true;
        }

        #endregion

        #region CancelPressed

        private RelayCommand _cancelPressedCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand CancelPressedCommand
        {
            get
            {
                if (_cancelPressedCommand == null)
                {
                    _cancelPressedCommand = new RelayCommand(
                        p => CancelPressedCommand_Executed(), p => CancelPressedCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_Cancel,
                        FriendlyDescription = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelPressedCommand);
                }
                return _cancelPressedCommand;
            }
        }

        private Boolean CancelPressedCommand_CanExecute()
        {
            return true;
        }

        private void CancelPressedCommand_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //Remove event handlers
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
