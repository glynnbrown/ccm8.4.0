﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : CCM800
// CCM-26520 :J.Pickup
//	Created
// V8-27227 : L.Luong
//  Fixed the dialogs for autoAssign methods
// V8-27411 : M.Pettit
//  Set Package.FetchById() to be a readonly fetch, to avoid placing a lock on the package
// CCM-27891 : J.Pickup
//	Validation and renumbering added
#endregion
#region Version History : CCM 802
// V8-29002 : A.Kuszyk
//  Amended manual assign for Sequence and Blocking to use Planogram selector.
// V8-29212 : I.George
//  Added Close Command
// V8-29255 : L.Ineson
//  Commented out old auto assign sequence command.
#endregion
#region Version History : CCM 810
// V8-29746 : A.Probyn
//  Updated inline with changes to replace Names with Ids on ContentLookup
// V8-30049 : N.Foster
//  Improved performance of auto-assign assortments
// V8-29211 : M.Pettit
//  Auto Assign CDT failed to show a message if autoassign did not work
#endregion
#region Version History: (CCM 830)
// V8-31831 : A.Probyn
//  Updated to include PlanogramNameTemplate
// V8-32810 : M.Pettit
//  Updated to include PrintTemplate
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Common.ModelObjectViewModels;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    public class ContentLookupMaintenanceViewModel : ViewModelAttachedControlObject<ContentLookupMaintenanceOrganiser>
    {
        #region Fields

        const String _exCategory = "ProductMaintenance";

        //Permissions
        private Boolean _userHasContentLookupCreatePerm;
        private Boolean _userHasContentLookupFetchPerm;
        private Boolean _userHasContentLookupEditPerm;
        private Boolean _userHasContentLookupDeletePerm;

        //Instance Variables
        private ReadOnlyCollection<ProductGroup> _productGroups;
        private ObservableCollection<ProductGroupViewModel> _productGroupViewModels = new ObservableCollection<ProductGroupViewModel>();
        private ProductGroupViewModel _selectedProductGroupViewModel;
        private ProductGroupViewModel _currentProductGroupViewModel = new ProductGroupViewModel(null);
        private ContentLookupRowViewModel _contentLookupRows = new ContentLookupRowViewModel();
        private BulkObservableCollection<ContentLookupRow> _selectedContentLookupRows = new BulkObservableCollection<ContentLookupRow>();

        private Boolean _autoAssignAllFail;
        private List<String> _autoAsignFailList = new List<String>();

        #endregion

        #region Property Paths

        //Properties
        public static readonly PropertyPath ProductGroupsProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ProductGroups);
        public static readonly PropertyPath ProductGroupViewModelsProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ProductGroupViewModels);
        public static readonly PropertyPath SelectedProductGroupViewModelProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.SelectedProductGroupViewModel);
        public static readonly PropertyPath CurrentProductGroupViewModelProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.CurrentProductGroupViewModel);
        public static readonly PropertyPath SelectedContentLookupRowsProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.SelectedContentLookupRows);
        public static readonly PropertyPath ContentLookupRowsProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ContentLookupRows);

        //Commands
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath OpenUnassociatedCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.OpenUnassociatedCommand);
        public static PropertyPath ClearAllCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearAllCommand);
        public static PropertyPath AutoAssignProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.AutoAssignProductUniverseCommand);
        public static PropertyPath ManualAssignProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManualAssignProductUniverseCommand);
        public static PropertyPath ClearProductUniverseCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearProductUniverseCommand);
        public static PropertyPath AutoAssignAssortmentCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.AutoAssignAssortmentCommand);
        public static PropertyPath ManuallyAssignAssortmentCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignAssortmentCommand);
        public static PropertyPath ClearAssortmentCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearAssortmentCommand);
        public static PropertyPath ManuallyAssignMetricProfileCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignMetricProfileCommand);
        public static PropertyPath ClearMetricProfileCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearMetricProfileCommand);
        public static PropertyPath ManuallyAssignInventoryProfileCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignInventoryProfileCommand);
        public static PropertyPath ClearInventoryProfileCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearInventoryProfileCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath AutoAssignAllCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.AutoAssignAllCommand);
        //public static PropertyPath AutoAssignSequenceCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.AutoAssignSequenceCommand);
        public static PropertyPath ManuallyAssignSequenceCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignSequenceCommand);
        public static PropertyPath ClearSequenceCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearSequenceCommand);
        public static PropertyPath AutoAssignConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.AutoAssignConsumerDecisionTreeCommand);
        public static PropertyPath ManuallyAssignConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignConsumerDecisionTreeCommand);
        public static PropertyPath ClearConsumerDecisionTreeCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearConsumerDecisionTreeCommand);
        public static PropertyPath AutoAssignAssortmentMinorRevisionCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.AutoAssignMinorAssortmentCommand);
        public static PropertyPath ManuallyAssignAssortmentMinorRevisionCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignMinorAssortmentCommand);
        public static PropertyPath ClearAssortmentMinorRevisionCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearMinorAssortmentCommand);
        public static PropertyPath ManuallyAssignPerformanceSelectionCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignPerformanceSelectionCommand);
        public static PropertyPath ClearPerformanceSelectionCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearPerformanceSelectionCommand);
        public static PropertyPath ManuallyAssignBlockingCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignBlockingCommand);
        public static PropertyPath ClearBlockingCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearBlockingCommand);
        public static PropertyPath ManuallyAssignClusterCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignClusterCommand);
        public static PropertyPath ClearClusterCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearClusterCommand);
        public static PropertyPath ManuallyAssignRenumberingStrategyCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignRenumberingStrategyCommand);
        public static PropertyPath ClearRenumberingStrategyCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearRenumberingStrategyCommand);
        public static PropertyPath ManuallyAssignValidationTemplateCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignValidationTemplateCommand);
        public static PropertyPath ClearValidationTemplateCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearValidationTemplateCommand);
        public static PropertyPath ManuallyAssignPlanogramNameTemplateCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignPlanogramNameTemplateCommand);
        public static PropertyPath ClearPlanogramNameTemplateCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearPlanogramNameTemplateCommand);
        public static PropertyPath ManuallyAssignPrintTemplateCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ManuallyAssignPrintTemplateCommand);
        public static PropertyPath ClearPrintTemplateCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.ClearPrintTemplateCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<ContentLookupMaintenanceViewModel>(p => p.CloseCommand);


        #endregion

        #region Properties

        public ReadOnlyCollection<ProductGroup> ProductGroups
        {
            get { return _productGroups; }
        }

        public ObservableCollection<ProductGroupViewModel> ProductGroupViewModels
        {
            get { return _productGroupViewModels; }
            set
            {
                _productGroupViewModels = value;
                OnPropertyChanged(ProductGroupViewModelsProperty);
            }
        }

        public ProductGroupViewModel SelectedProductGroupViewModel
        {
            get { return _selectedProductGroupViewModel; }
            set
            {
                _selectedProductGroupViewModel = value;
                OnPropertyChanged(SelectedProductGroupViewModelProperty);
            }
        }

        public ProductGroupViewModel CurrentProductGroupViewModel
        {
            get { return _currentProductGroupViewModel; }
            set
            {
                _currentProductGroupViewModel = value;
                OnPropertyChanged(CurrentProductGroupViewModelProperty);
            }
        }

        public ContentLookupRowViewModel ContentLookupRows
        {
            get { return _contentLookupRows; }
            set { _contentLookupRows = value; }
        }

        public BulkObservableCollection<ContentLookupRow> SelectedContentLookupRows
        {
            get { return _selectedContentLookupRows; }
            set { _selectedContentLookupRows = value; }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Content Lookup Maintenance View Model Contstructor
        /// </summary>
        public ContentLookupMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            UpdateAvailableProductGroups();
        }

        #endregion

        #region Commands

        #region Save

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //user must have edit permission
            if (!_userHasContentLookupEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            if (!ContentLookupRows.IsDirty() == true)
            {
                _saveCommand.DisabledReason = Message.Generic_Save_DisabledReasonNotDirty;
                return false;
            }

            if (this.AttachedControl != null)
            {
                //bindings must be valid
                if (!LocalHelper.AreBindingsValid(this.AttachedControl))
                {
                    this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                    return false;
                }
            }

            return true;
        }

        private void Save_Executed()
        {
            base.ShowWaitCursor(true);

            if (!SaveContentLookupItems())
            {
                throw new Exception();
            }

            base.ShowWaitCursor(false);
        }

        private Boolean SaveContentLookupItems()
        {
            Boolean saved = true;

            try
            {
                _contentLookupRows.SaveContentLookupItems();
            }
            catch (Exception ex)
            {
                ShowWaitCursor(false);

                LocalHelper.RecordException(ex, _exCategory);
                Exception rootException = ex.GetBaseException();

                //If it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(CurrentProductGroupViewModel.ProductGroup.Name);
                    if (itemReloadRequired)
                    {
                        this.ContentLookupRows.UpdateContentLookupRows(_currentProductGroupViewModel.ProductGroup.Code);
                    }
                }
                else
                {
                    //Otherwise just show the user an error has occurred.
                    Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(CurrentProductGroupViewModel.ProductGroup.Name, OperationType.Save);
                }

                ShowWaitCursor(true);
            }

            return saved;
        }

        #endregion

        #region SaveAndClose

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16,
                        DisabledReason = SaveCommand.DisabledReason,
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean saved = SaveContentLookupItems();

            if (saved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
            else
            {
                //throw error
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Delete

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_MaintenanceViewModel_DeleteSelected,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_MaintenanceViewModel_DeleteSelected_Description,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _deleteCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            if (this.SelectedContentLookupRows.Where(c => c.FriendlyContentLookup == null).Count() == SelectedContentLookupRows.Count())
            {
                _deleteCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            if (this.Save_CanExecute())
            {
                _deleteCommand.DisabledReason = Message.ContentLookup_DeleteDisabledReason_MustSave;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
             Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(String.Format(Message.ContentLookup_Delete_Title, CurrentProductGroupViewModel.ProductGroupLabel));
            }

            //confirm with user
            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                //Delete
                foreach (ContentLookupRow currentLookupRowToDelete in SelectedContentLookupRows)
                {
                    if (currentLookupRowToDelete.FriendlyContentLookup != null)
                    {
                        ContentLookup.DeleteById(currentLookupRowToDelete.FriendlyContentLookup.Id);
                    }
                }

                //Update our collection
                if (_currentProductGroupViewModel.ProductGroup != null)
                {
                    ContentLookupRows.UpdateContentLookupRows(_currentProductGroupViewModel.ProductGroup.Code);
                }
                else
                {
                    OpenUnassociatedCommand.Execute();
                }

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region Open

        private RelayCommand _openCommand;

        /// <summary>
        /// Opens the current Merchandising hierrarchy with the given id
        /// param: Int32 merchHierrarchyId
        /// </summary>
        public RelayCommand OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand(p =>
                        Open_Executed(), p => Open_CanExecute())
                    {
                        FriendlyName = Message.Generic_Open,
                        FriendlyDescription = Message.Generic_Open,
                    };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute()
        {
            //must not be null
            if (this.SelectedProductGroupViewModel == null)
            {
                _openCommand.DisabledReason = Message.ContentLookup_BackstageOpen_NoProductGroupSelected;
                return false;
            }

            return true;
        }

        private void Open_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean continueWithOpen = true;

            if (this.Save_CanExecute())
            {
                continueWithOpen = ContinueWithItemChange();
            }

            if (continueWithOpen)
            {
                CurrentProductGroupViewModel = this._selectedProductGroupViewModel;
                CurrentProductGroupViewModel_CategoryChanged();

                //Auto select first
                if (ContentLookupRows.ContentLookupRows != null && ContentLookupRows.ContentLookupRows.Count > 0)
                {
                    SelectedContentLookupRows.Add(ContentLookupRows.ContentLookupRows.FirstOrDefault());
                }

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);
            }

            base.ShowWaitCursor(false);            
        }

        #endregion

        #region OpenUnassociated

        private RelayCommand _openUnassociatedCommand;

        /// <summary>
        /// Opens the content lookup screen for planograms that have no associated category id (null).
        /// </summary>
        public RelayCommand OpenUnassociatedCommand
        {
            get
            {
                if (_openUnassociatedCommand == null)
                {
                    _openUnassociatedCommand = new RelayCommand(p =>
                        OpenUnassociated_Executed(), p => OpenUnassociated_CanExecute())
                    {
                        FriendlyName = Message.ContentLookup_OpenUnassociatedCommand_FriendlyName,
                        FriendlyDescription = Message.ContentLookup_OpenUnassociatedCommand_FriendlyDescription,
                    };
                    base.ViewModelCommands.Add(_openUnassociatedCommand);
                }
                return _openUnassociatedCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean OpenUnassociated_CanExecute()
        {
            //Unconditional
            return true;
        }

        private void OpenUnassociated_Executed()
        {
            base.ShowWaitCursor(true);

            Boolean continueWithOpen = true;

            if (this.Save_CanExecute())
            {
                continueWithOpen = ContinueWithItemChange();
            }

            if (continueWithOpen)
            {
                //Loads planograms without a Category association
                _contentLookupRows.UpdateContentLookupRowsForUnassociated();

                //Auto select first
                if (ContentLookupRows.ContentLookupRows != null && ContentLookupRows.ContentLookupRows.Count > 0)
                {
                    SelectedContentLookupRows.Add(ContentLookupRows.ContentLookupRows.FirstOrDefault());
                }

                //There should be no ProductGroup selected for planograms without a CategoryCode
                CurrentProductGroupViewModel = new ProductGroupViewModel(null);

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpenUnassociated*/false);
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region Actions

        #region AutoAssignAll

        private RelayCommand _autoAssignAllCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand AutoAssignAllCommand
        {
            get
            {
                if (_autoAssignAllCommand == null)
                {
                    _autoAssignAllCommand = new RelayCommand(
                        p => AutoAssignAllCommand_Executed(), p => AutoAssignAllCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignAllCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignAllCommand_Description,
                        Icon = ImageResources.ContentLookup_AutoAssign32
                    };
                    base.ViewModelCommands.Add(_autoAssignAllCommand);
                }
                return _autoAssignAllCommand;
            }
        }

        private Boolean AutoAssignAllCommand_CanExecute()
        {
            if (_currentProductGroupViewModel.ProductGroup == null) 
            {
                //Cannot auto assign without a product group as there is a lack of data available to make assumptions from
                _autoAssignAllCommand.DisabledReason = Message.ContentLookup_AutoAssignUnavailableForNullProductGroup;
                return false;
            }

            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignAllCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignAllCommand_Executed()
        {
            if (AutoAssignAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupAutoAssign newAutoAssignment = new ContentLookupAutoAssign();
                newAutoAssignment.Owner = this.AttachedControl;
                newAutoAssignment.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;

                newAutoAssignment.ShowDialog();

                if (newAutoAssignment.DialogResult == true)
                {
                    //Important! Must keep the booleans otherwise the viewmodel will have chance to run through garabge collection 
                    //if a command cannot associate a reference to. 
                    Boolean _assignProductUniverse = newAutoAssignment.ViewModel.AutoAssignProductUniverse;
                    Boolean _assignAutoAssignAssortment = newAutoAssignment.ViewModel.AutoAssignAssortment;
                    Boolean _assignProductCDT = newAutoAssignment.ViewModel.AutoAssignCDT;
                    Boolean _assignAssortmentMinorRevision = newAutoAssignment.ViewModel.AutoAssignAssortmentMinorRevision;
                    _autoAssignAllFail = true;

                    if (_assignProductUniverse)
                    {
                        this.AutoAssignProductUniverseCommand.Execute();
                    }

                    if (_assignAutoAssignAssortment)
                    {
                        this.AutoAssignAssortmentCommand.Execute();
                    }

                    if (_assignProductCDT)
                    {
                        this.AutoAssignConsumerDecisionTreeCommand.Execute();
                    }

                    if (_assignAssortmentMinorRevision)
                    {
                        this._autoAssignMinorAssortmentCommand.Execute();
                    }

                    if (_autoAsignFailList.Any())
                    {
                        ShowCouldNotAutoAssignAll(_autoAsignFailList);
                    }

                    _autoAsignFailList.Clear();
                    _autoAssignAllFail = false;
                }

                ShowWaitCursor(false);
            }
        }

        #endregion

        #region ClearAll

        private RelayCommand _clearAllCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand ClearAllCommand
        {
            get
            {
                if (_clearAllCommand == null)
                {
                    _clearAllCommand = new RelayCommand(
                        p => ClearAllCommand_Executed(), p => ClearAllCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearAllCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearAllCommand_Description,
                        Icon = ImageResources.ContentLookup_AutoUnassign32
                    };
                    base.ViewModelCommands.Add(_clearAllCommand);
                }
                return _clearAllCommand;
            }
        }

        private Boolean ClearAllCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearAllCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearAllCommand_Executed()
        {
            if (ClearAllCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupGroupClear newGroupClear = new ContentLookupGroupClear();
                newGroupClear.Owner = this.AttachedControl;
                newGroupClear.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;

                newGroupClear.ShowDialog();

                if (newGroupClear.DialogResult == true)
                {

                    if (newGroupClear.ViewModel.ClearProductUniverse)
                    {
                        this.ClearProductUniverseCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearAssortment)
                    {
                        this.ClearAssortmentCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearAssortmentMinorRevision)
                    {
                        this.ClearMinorAssortmentCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearSequence)
                    {
                        this.ClearSequenceCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearCDT)
                    {
                        this.ClearConsumerDecisionTreeCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearMetricProfile)
                    {
                        this.ClearMetricProfileCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearInventoryProfile)
                    {
                        this.ClearInventoryProfileCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearPerformanceSelections)
                    {
                        this.ClearPerformanceSelectionCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearPlanogramNameTemplate)
                    {
                        this.ClearPlanogramNameTemplateCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearBlocking)
                    {
                        this.ClearBlockingCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearCluster)
                    {
                        this.ClearClusterCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearRenumberingStrategy)
                    {
                        this.ClearRenumberingStrategyCommand.Execute();
                    }

                    if (newGroupClear.ViewModel.ClearValidationTemplate);
                    {
                        this.ClearValidationTemplateCommand.Execute();
                    }

                }

                ShowWaitCursor(false);
            }
        }

        #endregion

        #region Product Universe Assignments

        #region AutoAssignProductUniverse

        private RelayCommand _autoAssignProductUniverseCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand AutoAssignProductUniverseCommand
        {
            get
            {
                if (_autoAssignProductUniverseCommand == null)
                {
                    _autoAssignProductUniverseCommand = new RelayCommand(
                        p => AutoAssignProductUniverseCommand_Executed(), p => AutoAssignProductUniverseCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignProductUniverseCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignProductUniverseCommand_Description,
                        Icon = ImageResources.ContentLookup_ProductUniverse,
                        SmallIcon = ImageResources.ContentLookup_ProductUniverse16,
                    };
                    base.ViewModelCommands.Add(_autoAssignProductUniverseCommand);
                }
                return _autoAssignProductUniverseCommand;
            }
        }

        private Boolean AutoAssignProductUniverseCommand_CanExecute()
        {
            if (_currentProductGroupViewModel.ProductGroup == null)
            {
                //Cannot auto assign without a product group as there is a lack of data available to make assumptions from
                _autoAssignProductUniverseCommand.DisabledReason = Message.ContentLookup_AutoAssignUnavailableForNullProductGroup;
                return false;
            }

            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignProductUniverseCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignProductUniverseCommand_Executed()
        {
            if (AutoAssignAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                Boolean failedToAssignToAll = false;

                try
                {
                    //Get all product universes for current category
                    ProductUniverseInfoList associatedProductUniverses = ProductUniverseInfoList.FetchByProductGroupId(_selectedProductGroupViewModel.ProductGroup.Id);

                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        if (associatedProductUniverses.Count == 0)
                        {
                            failedToAssignToAll = true; //for message when come out of foreach
                        }
                        else if (associatedProductUniverses.Count == 1)
                        {
                            //Only the one product universe so this is the most 'appropriate to add'.
                            currentRow.ContentLookup.ProductUniverseName = associatedProductUniverses.First().Name;
                            currentRow.ContentLookup.ProductUniverseId = associatedProductUniverses.First().Id;
                        }
                        else
                        {
                            //If more than one how many products in product universe have the same products as in planogram.
                            Package currentPackage = Package.FetchById(currentRow.PlanogramInfo.Id);

                            if (currentPackage.Planograms != null && currentPackage.Planograms.Count > 0)
                            {
                                Planogram myPlanogram = currentPackage.Planograms.FirstOrDefault();
                                Dictionary<ProductUniverse, Int32> productCountDict = new Dictionary<ProductUniverse, Int32>();


                                //For each product universe we want to get a count of matching products
                                foreach (ProductUniverseInfo currentUniverse in associatedProductUniverses)
                                {
                                    //At this point we now know we want full productUniverse (We may not always need this before now). 
                                    ProductUniverse currentUniverseToAssess = ProductUniverse.FetchById(currentUniverse.Id);
                                    Int32 countForUniverse = 0;

                                    //Go through each product in planogram and count the matching products
                                    foreach (PlanogramProduct planoProduct in myPlanogram.Products)
                                    {
                                        Int32 cnt = currentUniverseToAssess.Products.Where(o => o.Gtin == planoProduct.Gtin).Count();
                                        countForUniverse += cnt;
                                    }

                                    productCountDict.Add(currentUniverseToAssess, countForUniverse);
                                }

                                //Highest key
                                var dictEntry = productCountDict.Aggregate((l, r) => l.Value > r.Value ? l : r);
                                Int32 max = dictEntry.Value;

                                //Is Distinct?
                                Int32 distinctCount = productCountDict.Values.Where(k => k == max).Count();

                                if (distinctCount == 1)
                                {
                                    //When rating is distinct this is our appropriate universe.
                                    currentRow.ContentLookup.ProductUniverseName = dictEntry.Key.Name;
                                    currentRow.ContentLookup.ProductUniverseId = dictEntry.Key.Id;
                                }
                                else
                                {
                                    //If rating not unqiue then we fall back to getting the most recently modified of the several non distinct.
                                    List<ProductUniverse> universes = new List<ProductUniverse>();

                                    foreach (KeyValuePair<ProductUniverse, Int32> entry in productCountDict)
                                    {
                                        if (entry.Value == max)
                                        {
                                            universes.Add(entry.Key);
                                        }
                                    }

                                    //Unverses now is from relevant universes
                                    DateTime latest = universes.Max(u => u.DateLastModified);
                                    ProductUniverse appropriateUniverse = universes.Where(u => u.DateLastModified == latest).First();

                                    //set name
                                    currentRow.ContentLookup.ProductUniverseName = appropriateUniverse.Name;
                                    currentRow.ContentLookup.ProductUniverseId = appropriateUniverse.Id;
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                { 
                    //Something went wrong...
                    failedToAssignToAll = true;
                }

                if (failedToAssignToAll)
                {
                    if (_autoAssignAllFail)
                    {
                        // if the AutoAssignAllCommand is called
                        _autoAsignFailList.Add(Message.ContentLookup_ShowCouldNotAutoAssign_ProductUniverse);
                    }
                    else
                    {
                        ShowWaitCursor(false);
                        //If the category has no associated product universes create info message
                        ShowCouldNotAutoAssign(Message.ContentLookup_ShowCouldNotAutoAssign_ProductUniverse);
                        ShowWaitCursor(true);
                    }
                }


                ShowWaitCursor(false);
            }


        }

        #endregion

        #region ManualAssignProductUniverse

        private RelayCommand _manualAssignProductUniverseCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand ManualAssignProductUniverseCommand
        {
            get
            {
                if (_manualAssignProductUniverseCommand == null)
                {
                    _manualAssignProductUniverseCommand = new RelayCommand(
                        p => ManualAssignProductUniverseCommand_Executed(), p => ManualAssignProductUniverseCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManualAssignProductUniverseCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManualAssignProductUniverseCommand_Description,
                        Icon = ImageResources.ContentLookup_ProductUniverse,
                        SmallIcon = ImageResources.ContentLookup_ProductUniverse16,
                    };
                    base.ViewModelCommands.Add(_manualAssignProductUniverseCommand);
                }
                return _manualAssignProductUniverseCommand;
            }
        }

        private Boolean ManualAssignProductUniverseCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manualAssignProductUniverseCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManualAssignProductUniverseCommand_Executed()
        {
            if (ManualAssignProductUniverseCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.ProductUniverse);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.ProductUniverseName = ((ProductUniverseInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.ProductUniverseId = ((ProductUniverseInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }

        #endregion

        #region ClearProductUniverse

        private RelayCommand _clearProductUniverseCommand;

        /// <summary>
        /// Handles Auto Assignment with product universe
        /// </summary>
        public RelayCommand ClearProductUniverseCommand
        {
            get
            {
                if (_clearProductUniverseCommand == null)
                {
                    _clearProductUniverseCommand = new RelayCommand(
                        p => ClearProductUniverseCommand_Executed(), p => ClearProductUniverseCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearProductUniverseCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearProductUniverseCommand_Description,
                        Icon = ImageResources.ContentLookup_ProductUniverse,
                        SmallIcon = ImageResources.ContentLookup_ProductUniverse16,
                    };
                    base.ViewModelCommands.Add(_clearProductUniverseCommand);
                }
                return _clearProductUniverseCommand;
            }
        }

        private Boolean ClearProductUniverseCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearProductUniverseCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearProductUniverseCommand_Executed()
        {
            if (ClearProductUniverseCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Row:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.ProductUniverseName = String.Empty;
                    currentRow.ContentLookup.ProductUniverseId = null;
                }

                ShowWaitCursor(false);
            }
        }

        #endregion

        #endregion

        #region Assortment Assignments

        #region AutoAssignAssortment

        private RelayCommand _autoAssignAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with Assortment
        /// </summary>
        public RelayCommand AutoAssignAssortmentCommand
        {
            get
            {
                if (_autoAssignAssortmentCommand == null)
                {
                    _autoAssignAssortmentCommand = new RelayCommand(
                        p => AutoAssignAssortmentCommand_Executed(), p => AutoAssignAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignAssortmentCommand_Description,
                        Icon = ImageResources.ContentLookup_Assortment,
                        SmallIcon = ImageResources.ContentLookup_Assortment16,
                    };
                    base.ViewModelCommands.Add(_autoAssignAssortmentCommand);
                }
                return _autoAssignAssortmentCommand;
            }
        }

        private Boolean AutoAssignAssortmentCommand_CanExecute()
        {
            if (_currentProductGroupViewModel.ProductGroup == null)
            {
                //Cannot auto assign without a product group as there is a lack of data available to make assumptions from
                _autoAssignAssortmentCommand.DisabledReason = Message.ContentLookup_AutoAssignUnavailableForNullProductGroup;
                return false;
            }

            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignAssortmentCommand_Executed()
        {
            if (AutoAssignAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                Boolean failedToAssignToAll = false;

                try
                {

                    // get all Assortments for current category
                    AssortmentInfoList associatedAssortments = AssortmentInfoList.FetchByProductGroupId(App.ViewState.EntityId, _selectedProductGroupViewModel.ProductGroup.Id);
                    
                    // build a lookup of assortments should they be required
                    Dictionary<Assortment, HashSet<String>> assortments = new Dictionary<Assortment, HashSet<String>>();
                    if (associatedAssortments.Count > 1)
                    {
                        foreach (AssortmentInfo currentInfo in associatedAssortments)
                        {
                            // fetch the assortment details
                            Assortment assortment = Assortment.GetById(currentInfo.Id);

                            // build a hash set of the assortment product codes for fast lookup
                            HashSet<String> assortmentProductCodes = new HashSet<String>();
                            foreach (AssortmentProduct assortmentProduct in assortment.Products)
                            {
                                assortmentProductCodes.Add(assortmentProduct.Gtin);
                            }

                            // add to the assortments lookup
                            assortments.Add(assortment, assortmentProductCodes);
                        }
                    }

                    // for each planogram:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        if (associatedAssortments.Count == 0)
                        {
                            failedToAssignToAll = true;
                        }
                        else if (associatedAssortments.Count == 1)
                        {
                            // only one assortment found so assign
                            currentRow.ContentLookup.AssortmentName = associatedAssortments.First().Name;
                            currentRow.ContentLookup.AssortmentId = associatedAssortments.First().Id;
                        }
                        else
                        {
                            // more than one assortment found - analyse further

                            // get full planogram
                            Package currentPackage = Package.FetchById(currentRow.PlanogramInfo.Id);
                            if (currentPackage.Planograms != null && currentPackage.Planograms.Count > 0)
                            {
                                // get the first planogram in the package
                                Planogram myPlanogram = currentPackage.Planograms.FirstOrDefault();

                                // now find those assortments that
                                // have the maximum number of products
                                // matching the planogram
                                Int32 maxProductCount = 0;
                                List<Assortment> matchedAssortments = new List<Assortment>();

                                // evaluate each assortment and determine how many products match
                                foreach (KeyValuePair<Assortment, HashSet<String>> currentAssortment in assortments)
                                {
                                    // go through each product in planogram and count the matching products
                                    Int32 matchedProductCount = 0;
                                    foreach (PlanogramProduct planoProduct in myPlanogram.Products)
                                    {
                                        if (currentAssortment.Value.Contains(planoProduct.Gtin))
                                            matchedProductCount++;
                                    }

                                    // add to the matched products if required
                                    if (matchedProductCount > maxProductCount)
                                    {
                                        // the number of matched products exceeds the current
                                        // number of matched products
                                        matchedAssortments.Clear();
                                        matchedAssortments.Add(currentAssortment.Key);
                                        maxProductCount = matchedProductCount;
                                    }
                                    else if (matchedProductCount == maxProductCount)
                                    {
                                        matchedAssortments.Add(currentAssortment.Key);
                                    }
                                }

                                // find the most recent modified assortment
                                // out of all thoses that matched with the greatest
                                // number of matched products. Choose the assortment
                                // that contains the greatest number of overall products
                                // then order by name
                                Assortment matchedAssortment = null;
                                foreach (Assortment currentAssortment in matchedAssortments.OrderByDescending(a => a.Products.Count).ThenBy(a => a.Name))
                                {
                                    if ((matchedAssortment == null) || (currentAssortment.DateLastModified > matchedAssortment.DateLastModified))
                                    {
                                        matchedAssortment = currentAssortment;
                                    }
                                }

                                // assign the matched assortment
                                currentRow.ContentLookup.AssortmentName = matchedAssortment.Name;
                                currentRow.ContentLookup.AssortmentId = matchedAssortment.Id;
                            }
                        }
                    }

                }
                catch (Exception)
                { 
                    //Something went wrong
                    failedToAssignToAll = true;
                }

                if (failedToAssignToAll)
                {
                    ShowWaitCursor(false);
                    if (_autoAssignAllFail)
                    {
                        // if the AutoAssignAllCommand is called
                        _autoAsignFailList.Add(Message.ContentLookup_ShowCouldNotAutoAssign_Assortment);
                    }
                    else
                    {
                        ShowWaitCursor(false);
                        //No associated assortments found so tell user
                        ShowCouldNotAutoAssign(Message.ContentLookup_ShowCouldNotAutoAssign_Assortment);
                        ShowWaitCursor(true);
                    }
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ClearAssortment

        private RelayCommand _clearAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with Assortment
        /// </summary>
        public RelayCommand ClearAssortmentCommand
        {
            get
            {
                if (_clearAssortmentCommand == null)
                {
                    _clearAssortmentCommand = new RelayCommand(
                        p => ClearAssortmentCommand_Executed(), p => ClearAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearAssortmentCommand_Description,
                        Icon = ImageResources.ContentLookup_Assortment,
                        SmallIcon = ImageResources.ContentLookup_Assortment16,
                    };
                    base.ViewModelCommands.Add(_clearAssortmentCommand);
                }
                return _clearAssortmentCommand;
            }
        }

        private Boolean ClearAssortmentCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearAssortmentCommand_Executed()
        {
            if (ClearAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.AssortmentName = String.Empty;
                    currentRow.ContentLookup.AssortmentId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignAssortment

        private RelayCommand _manuallyAssignAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with Assortment
        /// </summary>
        public RelayCommand ManuallyAssignAssortmentCommand
        {
            get
            {
                if (_manuallyAssignAssortmentCommand == null)
                {
                    _manuallyAssignAssortmentCommand = new RelayCommand(
                        p => ManuallyAssignAssortmentCommand_Executed(), p => ManuallyAssignAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignAssortmentCommand_Description,
                        Icon = ImageResources.ContentLookup_Assortment16,
                        SmallIcon = ImageResources.ContentLookup_Assortment16
                    };
                    base.ViewModelCommands.Add(_manuallyAssignAssortmentCommand);
                }
                return _manuallyAssignAssortmentCommand;
            }
        }

        private Boolean ManuallyAssignAssortmentCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignAssortmentCommand_Executed()
        {
            if (ManuallyAssignAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.Assortment, _currentProductGroupViewModel);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.AssortmentName = ((AssortmentInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.AssortmentId = ((AssortmentInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region Metric Profile

        #region ManuallyAssignMetricProfile

        private RelayCommand _ManuallyAssignMetricProfileCommand;

        /// <summary>
        /// Handles Manually Assignment with MetricProfile
        /// </summary>
        public RelayCommand ManuallyAssignMetricProfileCommand
        {
            get
            {
                if (_ManuallyAssignMetricProfileCommand == null)
                {
                    _ManuallyAssignMetricProfileCommand = new RelayCommand(
                        p => ManuallyAssignMetricProfileCommand_Executed(), p => ManuallyAssignMetricProfileCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignMetricProfileCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignMetricProfileCommand_Description,
                        Icon = ImageResources.ContentLookup_MetricProfile,
                        SmallIcon = ImageResources.ContentLookup_MetricProfile16,
                    };
                    base.ViewModelCommands.Add(_ManuallyAssignMetricProfileCommand);
                }
                return _ManuallyAssignMetricProfileCommand;
            }
        }

        private Boolean ManuallyAssignMetricProfileCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ManuallyAssignMetricProfileCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignMetricProfileCommand_Executed()
        {
            if (ManuallyAssignMetricProfileCommand.CanExecute())
            {
                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.MetricProfile);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.MetricProfileName = ((MetricProfileInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.MetricProfileId = ((MetricProfileInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }
            }
        }

        #endregion

        #region ClearMetricProfile

        private RelayCommand _ClearMetricProfileCommand;

        /// <summary>
        /// Handles Manually Assignment with MetricProfile
        /// </summary>
        public RelayCommand ClearMetricProfileCommand
        {
            get
            {
                if (_ClearMetricProfileCommand == null)
                {
                    _ClearMetricProfileCommand = new RelayCommand(
                        p => ClearMetricProfileCommand_Executed(), p => ClearMetricProfileCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearMetricProfileCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearMetricProfileCommand_Description,
                        Icon = ImageResources.ContentLookup_MetricProfile,
                        SmallIcon = ImageResources.ContentLookup_MetricProfile16,
                    };
                    base.ViewModelCommands.Add(_ClearMetricProfileCommand);
                }
                return _ClearMetricProfileCommand;
            }
        }

        private Boolean ClearMetricProfileCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ClearMetricProfileCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearMetricProfileCommand_Executed()
        {
            //Foreach Planogram:
            foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
            {
                currentRow.ContentLookup.MetricProfileName = String.Empty;
                currentRow.ContentLookup.MetricProfileId = null;
            }
        }

        #endregion

        #endregion

        #region Inventory Profile

        #region ManuallyAssignInventoryProfile

        private RelayCommand _ManuallyAssignInventoryProfileCommand;

        /// <summary>
        /// Handles Manually Assignment with InventoryProfile
        /// </summary>
        public RelayCommand ManuallyAssignInventoryProfileCommand
        {
            get
            {
                if (_ManuallyAssignInventoryProfileCommand == null)
                {
                    _ManuallyAssignInventoryProfileCommand = new RelayCommand(
                        p => ManuallyAssignInventoryProfileCommand_Executed(), p => ManuallyAssignInventoryProfileCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignInventoryProfileCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignInventoryProfileCommand_Description,
                        Icon = ImageResources.ContentLookup_InventoryProfile,
                        SmallIcon = ImageResources.ContentLookup_InventoryProfile16,
                    };
                    base.ViewModelCommands.Add(_ManuallyAssignInventoryProfileCommand);
                }
                return _ManuallyAssignInventoryProfileCommand;
            }
        }

        private Boolean ManuallyAssignInventoryProfileCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ManuallyAssignInventoryProfileCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignInventoryProfileCommand_Executed()
        {
            if (ManuallyAssignInventoryProfileCommand.CanExecute())
            {
                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.InventoryProfile);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.InventoryProfileName = ((InventoryProfileInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.InventoryProfileId = ((InventoryProfileInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }
            }
        }

        #endregion

        #region ClearInventoryProfile

        private RelayCommand _ClearInventoryProfileCommand;

        /// <summary>
        /// Handles Manually Assignment with InventoryProfile
        /// </summary>
        public RelayCommand ClearInventoryProfileCommand
        {
            get
            {
                if (_ClearInventoryProfileCommand == null)
                {
                    _ClearInventoryProfileCommand = new RelayCommand(
                        p => ClearInventoryProfileCommand_Executed(), p => ClearInventoryProfileCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearInventoryProfileCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearInventoryProfileCommand_Description,
                        Icon = ImageResources.ContentLookup_InventoryProfile,
                        SmallIcon = ImageResources.ContentLookup_InventoryProfile16,
                    };
                    base.ViewModelCommands.Add(_ClearInventoryProfileCommand);
                }
                return _ClearInventoryProfileCommand;
            }
        }

        private Boolean ClearInventoryProfileCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ClearInventoryProfileCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearInventoryProfileCommand_Executed()
        {
            //Foreach Planogram:
            foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
            {
                currentRow.ContentLookup.InventoryProfileName = String.Empty;
                currentRow.ContentLookup.InventoryProfileId = null;
            }
        }

        #endregion

        #endregion

        #region Sequence Assignments

        #region AutoAssignSequence

        //private RelayCommand _autoAssignSequenceCommand;

        ///// <summary>
        ///// Handles Auto Assignment with Sequence
        ///// </summary>
        //public RelayCommand AutoAssignSequenceCommand
        //{
        //    get
        //    {
        //        if (_autoAssignSequenceCommand == null)
        //        {
        //            _autoAssignSequenceCommand = new RelayCommand(
        //                p => AutoAssignSequenceCommand_Executed(), p => AutoAssignSequenceCommand_CanExecute())
        //            {
        //                FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignSequenceCommand_FriendlyName,
        //                FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignSequenceCommand_Description,
        //                SmallIcon = ImageResources.ContentLookup_ProductSequence16,
        //            };
        //            base.ViewModelCommands.Add(_autoAssignSequenceCommand);
        //        }
        //        return _autoAssignSequenceCommand;
        //    }
        //}

        //private Boolean AutoAssignSequenceCommand_CanExecute()
        //{
        //    if (_currentProductGroupViewModel.ProductGroup == null)
        //    {
        //        //Cannot auto assign without a product group as there is a lack of data available to make assumptions from
        //        _autoAssignSequenceCommand.DisabledReason = Message.ContentLookup_AutoAssignUnavailableForNullProductGroup;
        //        return false;
        //    }

        //    if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
        //    {
        //        _autoAssignSequenceCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
        //        return false;
        //    }

        //    return true;
        //}

        //private void AutoAssignSequenceCommand_Executed()
        //{
        //    if (AutoAssignSequenceCommand_CanExecute())
        //    {
        //        ShowWaitCursor(true);

        //        Boolean failedToAssignToAll = false;

        //        try
        //        {

        //            //Get all sequences for current category
        //            SequenceInfoList associatedSequences = SequenceInfoList.FetchByProductGroupId(_selectedProductGroupViewModel.ProductGroup.Id);

        //            //Foreach Row:
        //            foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
        //            {
        //                if (associatedSequences.Count == 0)
        //                {
        //                    failedToAssignToAll = true;
        //                }
        //                else if (associatedSequences.Count == 1)
        //                {
        //                    //Only the one product sequence so this is the most 'appropriate to add'.
        //                    currentRow.ContentLookup.SequenceName = associatedSequences.First().Name;
        //                }
        //                else
        //                {
        //                    //If more than one how many products in product sequence have the same products as in planogram.
        //                    Package currentPackage = Package.FetchById(currentRow.PlanogramInfo.Id);

        //                    if (currentPackage.Planograms != null && currentPackage.Planograms.Count > 0)
        //                    {
        //                        Planogram myPlanogram = currentPackage.Planograms.FirstOrDefault();
        //                        Dictionary<Sequence, Int32> productCountDict = new Dictionary<Sequence, Int32>();


        //                        //For each product sequence we want to get a count of matching products
        //                        foreach (SequenceInfo currentSequence in associatedSequences)
        //                        {
        //                            //At this point we now know we want full sequence (We may not always need this before now). 
        //                            Sequence currentSequenceToAssess = Sequence.FetchById(currentSequence.Id);
        //                            Int32 countForSequence = 0;

        //                            //Because SequenceProduct does not have Gtin bring back a list of gtins from products for the product ids 
        //                            List<String> gtinList = new List<String>();
        //                            foreach (SequenceProduct currentSequenceProduct in currentSequenceToAssess.Products)
        //                            {
        //                                gtinList.Add(Product.FetchById(currentSequenceProduct.ProductId).Gtin);
        //                            }

        //                            //Go through each product in planogram and count the matching products
        //                            foreach (PlanogramProduct planoProduct in myPlanogram.Products)
        //                            {

        //                                Int32 cnt = gtinList.Where(o => o == planoProduct.Gtin).Count();
        //                                countForSequence += cnt;
        //                            }

        //                            productCountDict.Add(currentSequenceToAssess, countForSequence);
        //                        }

        //                        //Highest key
        //                        var dictEntry = productCountDict.Aggregate((l, r) => l.Value > r.Value ? l : r);
        //                        Int32 max = dictEntry.Value;

        //                        //Is Distinct?
        //                        Int32 distinctCount = productCountDict.Values.Where(k => k == max).Count();

        //                        if (distinctCount == 1)
        //                        {
        //                            //When rating is distinct this is our appropriate sequence.
        //                            currentRow.ContentLookup.SequenceName = dictEntry.Key.Name;
        //                        }
        //                        else
        //                        {
        //                            //If rating not unqiue then we fall back to getting the most recently modified of the several non distinct.
        //                            List<Sequence> sequences = new List<Sequence>();

        //                            foreach (KeyValuePair<Sequence, Int32> entry in productCountDict)
        //                            {
        //                                if (entry.Value == max)
        //                                {
        //                                    sequences.Add(entry.Key);
        //                                }
        //                            }

        //                            //Unverses now is from relevant sequences
        //                            DateTime latest = sequences.Max(u => u.DateLastModified);
        //                            Sequence appropriateSequence = sequences.Where(u => u.DateLastModified == latest).First();

        //                            //set name
        //                            currentRow.ContentLookup.SequenceName = appropriateSequence.Name;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            //Something went wrong...
        //            failedToAssignToAll = true;
        //        }

        //        if (failedToAssignToAll)
        //        {
        //            if (_autoAssignAllFail)
        //            {
        //                // if the AutoAssignAllCommand is called
        //                _autoAsignFailList.Add(Message.ContentLookup_ShowCouldNotAutoAssign_Sequence);
        //            }
        //            else
        //            {
        //                ShowWaitCursor(false);
        //                //If the category has no associated product sequences create info message
        //                ShowCouldNotAutoAssign(Message.ContentLookup_ShowCouldNotAutoAssign_Sequence);
        //                ShowWaitCursor(true);
        //            }
        //        }

        //        ShowWaitCursor(false);
        //    }
        //}


        #endregion

        #region ClearSequence

        private RelayCommand _clearSequenceCommand;

        /// <summary>
        /// Handles Auto Assignment with Sequence
        /// </summary>
        public RelayCommand ClearSequenceCommand
        {
            get
            {
                if (_clearSequenceCommand == null)
                {
                    _clearSequenceCommand = new RelayCommand(
                        p => ClearSequenceCommand_Executed(), p => ClearSequenceCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearSequenceCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearSequenceCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ProductSequence16,
                    };
                    base.ViewModelCommands.Add(_clearSequenceCommand);
                }
                return _clearSequenceCommand;
            }
        }

        private Boolean ClearSequenceCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearSequenceCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearSequenceCommand_Executed()
        {
            if (ClearSequenceCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.SequenceName = String.Empty;
                    currentRow.ContentLookup.SequenceId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignSequence

        private RelayCommand _manuallyAssignSequenceCommand;

        /// <summary>
        /// Handles Auto Assignment with Sequence
        /// </summary>
        public RelayCommand ManuallyAssignSequenceCommand
        {
            get
            {
                if (_manuallyAssignSequenceCommand == null)
                {
                    _manuallyAssignSequenceCommand = new RelayCommand(
                        p => ManuallyAssignSequenceCommand_Executed(), p => ManuallyAssignSequenceCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignSequenceCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignSequenceCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ProductSequence16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignSequenceCommand);
                }
                return _manuallyAssignSequenceCommand;
            }
        }

        private Boolean ManuallyAssignSequenceCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignSequenceCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignSequenceCommand_Executed()
        {
            if (ManuallyAssignSequenceCommand_CanExecute())
            {
                ShowWaitCursor(true);

                PlanogramSelectorViewModel viewModel = new PlanogramSelectorViewModel(
                    App.ViewState.CustomColumnLayoutFolderPath, App.ViewState.EntityId);

                Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>().ShowDialog<PlanogramSelectorWindow>(viewModel);

                if (viewModel.DialogResult == true)
                {
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.SequenceName = viewModel.SelectedPlanogramInfo.Name;
                        currentRow.ContentLookup.SequenceId = viewModel.SelectedPlanogramInfo.Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region ConsumerDecisionTree Assignments

        #region AutoAssignConsumerDecisionTree

        private RelayCommand _autoAssignConsumerDecisionTreeCommand;

        /// <summary>
        /// Handles Auto Assignment with ConsumerDecisionTree
        /// </summary>
        public RelayCommand AutoAssignConsumerDecisionTreeCommand
        {
            get
            {
                if (_autoAssignConsumerDecisionTreeCommand == null)
                {
                    _autoAssignConsumerDecisionTreeCommand = new RelayCommand(
                        p => AutoAssignConsumerDecisionTreeCommand_Executed(), p => AutoAssignConsumerDecisionTreeCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignConsumerDecisionTreeCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.AutoAssignConsumerDecisionTreeCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ConsumerDecisionTree16,
                    };
                    base.ViewModelCommands.Add(_autoAssignConsumerDecisionTreeCommand);
                }
                return _autoAssignConsumerDecisionTreeCommand;
            }
        }

        private Boolean AutoAssignConsumerDecisionTreeCommand_CanExecute()
        {
            if (_currentProductGroupViewModel.ProductGroup == null)
            {
                //Cannot auto assign without a product group as there is a lack of data available to make assumptions from
                _autoAssignConsumerDecisionTreeCommand.DisabledReason = Message.ContentLookup_AutoAssignUnavailableForNullProductGroup;
                return false;
            }

            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignConsumerDecisionTreeCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignConsumerDecisionTreeCommand_Executed()
        {
            if (AutoAssignConsumerDecisionTreeCommand_CanExecute())
            {
                ShowWaitCursor(true);

                Boolean failedToAssignToAll = false;

                try
                {

                    //Get all sequences for current category
                    ConsumerDecisionTreeInfoList associatedConsumerDecisionTrees = ConsumerDecisionTreeInfoList.FetchByProductGroupId(_selectedProductGroupViewModel.ProductGroup.Id);

                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        if (associatedConsumerDecisionTrees.Count == 0)
                        {
                            //there are no cdts for this category so nothing we can assign to
                            failedToAssignToAll = true;
                        }
                        else if (associatedConsumerDecisionTrees.Count == 1)
                        {
                            //Only the one product sequence so this is the most 'appropriate to add'.
                            currentRow.ContentLookup.ConsumerDecisionTreeName = associatedConsumerDecisionTrees.First().Name;
                            currentRow.ContentLookup.ConsumerDecisionTreeId = associatedConsumerDecisionTrees.First().Id;
                        }
                        else
                        {
                            //If more than one how many products in CDT have the same products as in planogram.
                            Package currentPackage = Package.FetchById(currentRow.PlanogramInfo.Id);

                            if (currentPackage.Planograms != null && currentPackage.Planograms.Count > 0)
                            {
                                Planogram myPlanogram = currentPackage.Planograms.FirstOrDefault();
                                Dictionary<ConsumerDecisionTree, Int32> productCountDict = new Dictionary<ConsumerDecisionTree, Int32>();


                                //For each product sequence we want to get a count of matching products
                                foreach (ConsumerDecisionTreeInfo currentCDT in associatedConsumerDecisionTrees)
                                {
                                    //At this point we now know we want full sequence (We may not always need this before now). 
                                    ConsumerDecisionTree currentCDTToAssess = ConsumerDecisionTree.FetchById(currentCDT.Id);
                                    Int32 countForCdt = 0;

                                    ////Because CDT does not have Gtin bring back a list of gtins from products for the product ids 
                                    List<String> gtinList = new List<String>();

                                    foreach (Int32 currentProductId in currentCDTToAssess.GetAssignedProductIds())
                                    {
                                        try
                                        {
                                            gtinList.Add(Product.FetchById(currentProductId).Gtin);
                                        }
                                        catch (Exception)
                                        {
                                            //Shouldn't reach here but if we do just ignore and don't add to rating as cannot be important in decision.
                                        }
                                    }

                                    //Go through each product in planogram and count the matching products
                                    foreach (PlanogramProduct planoProduct in myPlanogram.Products)
                                    {
                                        Int32 cnt = gtinList.Where(o => o == planoProduct.Gtin).Count();
                                        countForCdt += cnt;
                                    }

                                    productCountDict.Add(currentCDTToAssess, countForCdt);
                                }

                                //Highest key
                                var dictEntry = productCountDict.Aggregate((l, r) => l.Value > r.Value ? l : r);
                                Int32 max = dictEntry.Value;

                                //Is Distinct?
                                Int32 distinctCount = productCountDict.Values.Where(k => k == max).Count();

                                if (distinctCount == 1)
                                {
                                    //When rating is distinct this is our appropriate sequence.
                                    currentRow.ContentLookup.SequenceName = dictEntry.Key.Name;
                                    currentRow.ContentLookup.SequenceId = dictEntry.Key.Id;
                                }
                                else
                                {
                                    //If rating not unqiue then we fall back to getting the most recently modified of the several non distinct.
                                    List<ConsumerDecisionTree> cdts = new List<ConsumerDecisionTree>();

                                    foreach (KeyValuePair<ConsumerDecisionTree, Int32> entry in productCountDict)
                                    {
                                        if (entry.Value == max)
                                        {
                                            cdts.Add(entry.Key);
                                        }
                                    }

                                    //CDT does not have date values : we assume that they are both 'appropriate'. 
                                    //So we just choose first for now. 
                                    ConsumerDecisionTree appropriateCDT = cdts.First();

                                    //set name
                                    currentRow.ContentLookup.ConsumerDecisionTreeName = appropriateCDT.Name;
                                    currentRow.ContentLookup.ConsumerDecisionTreeId = appropriateCDT.Id;
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                { 
                    //Something went wrong...
                    failedToAssignToAll = true;
                }

                if (failedToAssignToAll)
                {
                    if (_autoAssignAllFail)
                    {
                        // if the AutoAssignAllCommand is called
                        _autoAsignFailList.Add(Message.ContentLookup_ShowCouldNotAutoAssign_ConsumerDecisionTree);
                    }
                    else
                    {
                        ShowWaitCursor(false);
                        //If the category has no associated product sequences create info message
                        ShowCouldNotAutoAssign(Message.ContentLookup_ShowCouldNotAutoAssign_ConsumerDecisionTree);
                        ShowWaitCursor(true);
                    }
                }
           
                ShowWaitCursor(false);
            }
        }


        #endregion

        #region ClearConsumerDecisionTree

        private RelayCommand _clearConsumerDecisionTreeCommand;

        /// <summary>
        /// Handles Auto Assignment with ConsumerDecisionTree
        /// </summary>
        public RelayCommand ClearConsumerDecisionTreeCommand
        {
            get
            {
                if (_clearConsumerDecisionTreeCommand == null)
                {
                    _clearConsumerDecisionTreeCommand = new RelayCommand(
                        p => ClearConsumerDecisionTreeCommand_Executed(), p => ClearConsumerDecisionTreeCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearConsumerDecisionTreeCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearConsumerDecisionTreeCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ConsumerDecisionTree16,
                    };
                    base.ViewModelCommands.Add(_clearConsumerDecisionTreeCommand);
                }
                return _clearConsumerDecisionTreeCommand;
            }
        }

        private Boolean ClearConsumerDecisionTreeCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearConsumerDecisionTreeCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearConsumerDecisionTreeCommand_Executed()
        {
            if (ClearConsumerDecisionTreeCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.ConsumerDecisionTreeName = String.Empty;
                    currentRow.ContentLookup.ConsumerDecisionTreeId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignConsumerDecisionTree

        private RelayCommand _manuallyAssignConsumerDecisionTreeCommand;

        /// <summary>
        /// Handles Auto Assignment with ConsumerDecisionTree
        /// </summary>
        public RelayCommand ManuallyAssignConsumerDecisionTreeCommand
        {
            get
            {
                if (_manuallyAssignConsumerDecisionTreeCommand == null)
                {
                    _manuallyAssignConsumerDecisionTreeCommand = new RelayCommand(
                        p => ManuallyAssignConsumerDecisionTreeCommand_Executed(), p => ManuallyAssignConsumerDecisionTreeCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignConsumerDecisionTreeCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignConsumerDecisionTreeCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ConsumerDecisionTree16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignConsumerDecisionTreeCommand);
                }
                return _manuallyAssignConsumerDecisionTreeCommand;
            }
        }

        private Boolean ManuallyAssignConsumerDecisionTreeCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignConsumerDecisionTreeCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignConsumerDecisionTreeCommand_Executed()
        {
            if (ManuallyAssignConsumerDecisionTreeCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.ConsumerDecisionTree);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.ConsumerDecisionTreeName = ((ConsumerDecisionTreeInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.ConsumerDecisionTreeId = ((ConsumerDecisionTreeInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region MinorAssortment Assignments

        #region AutoAssignMinorAssortment

        private RelayCommand _autoAssignMinorAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with MinorAssortment
        /// </summary>
        public RelayCommand AutoAssignMinorAssortmentCommand
        {
            get
            {
                if (_autoAssignMinorAssortmentCommand == null)
                {
                    _autoAssignMinorAssortmentCommand = new RelayCommand(
                        p => AutoAssignMinorAssortmentCommand_Executed(), p => AutoAssignMinorAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_AutoAssignMinorAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_AutoAssignMinorAssortmentCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_AssortmentMinorRevision16,
                    };
                    base.ViewModelCommands.Add(_autoAssignMinorAssortmentCommand);
                }
                return _autoAssignMinorAssortmentCommand;
            }
        }

        private Boolean AutoAssignMinorAssortmentCommand_CanExecute()
        {
            if (_currentProductGroupViewModel.ProductGroup == null)
            {
                //Cannot auto assign without a product group as there is a lack of data available to make assumptions from
                _autoAssignMinorAssortmentCommand.DisabledReason = Message.ContentLookup_AutoAssignUnavailableForNullProductGroup;
                return false;
            }

            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _autoAssignMinorAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void AutoAssignMinorAssortmentCommand_Executed()
        {
            if (AutoAssignMinorAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                Boolean failedToAssignToAll = false;

                try
                {

                    //Get all AssortmentMinorRevisions for current category
                    AssortmentMinorRevisionInfoList associatedAssortmentMinorRevisions = AssortmentMinorRevisionInfoList.FetchByProductGroupId(_selectedProductGroupViewModel.ProductGroup.Id);

                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        if (associatedAssortmentMinorRevisions.Count == 0)
                        {
                            failedToAssignToAll = true;
                        }
                        else if (associatedAssortmentMinorRevisions.Count == 1)
                        {
                            //Only the one product universe so this is the most 'appropriate to add'.
                            currentRow.ContentLookup.AssortmentMinorRevisionName = associatedAssortmentMinorRevisions.First().Name;
                            currentRow.ContentLookup.AssortmentMinorRevisionId = associatedAssortmentMinorRevisions.First().Id;
                        }
                        else
                        {
                            //If more than one how many products in product universe have the same products as in planogram.
                            Package currentPackage = Package.FetchById(currentRow.PlanogramInfo.Id);

                            if (currentPackage.Planograms != null && currentPackage.Planograms.Count > 0)
                            {
                                Planogram myPlanogram = currentPackage.Planograms.FirstOrDefault();
                                Dictionary<AssortmentMinorRevision, Int32> productCountDict = new Dictionary<AssortmentMinorRevision, Int32>();


                                //For each product universe we want to get a count of matching products
                                foreach (AssortmentMinorRevisionInfo currentMinorAssortment in associatedAssortmentMinorRevisions)
                                {
                                    //At this point we now know we want full productUniverse (We may not always need this before now). 
                                    AssortmentMinorRevision currentAssortmentMinorRevisionToAssess = AssortmentMinorRevision.GetById(currentMinorAssortment.Id);
                                    Int32 countForMinorAssortment = 0;

                                    //Minor assortment contains actions. Lets go and go through each action and retrieve a list of nique GTINs from
                                    //the products that are associated to those actions.
                                    List<String> GtinList = new List<String>();

                                    //List actions
                                    foreach (AssortmentMinorRevisionListAction currentListAction in currentAssortmentMinorRevisionToAssess.ListActions)
                                    {
                                        if (!GtinList.Contains(currentListAction.ProductGtin))
                                        {
                                            GtinList.Add(currentListAction.ProductGtin);
                                        }
                                    }

                                    //DeList actions
                                    foreach (AssortmentMinorRevisionDeListAction currentDeListAction in currentAssortmentMinorRevisionToAssess.DeListActions)
                                    {
                                        if (!GtinList.Contains(currentDeListAction.ProductGtin))
                                        {
                                            GtinList.Add(currentDeListAction.ProductGtin);
                                        }
                                    }

                                    //AmmendDistributionActions
                                    foreach (AssortmentMinorRevisionAmendDistributionAction amendAction in currentAssortmentMinorRevisionToAssess.AmendDistributionActions)
                                    {
                                        if (!GtinList.Contains(amendAction.ProductGtin))
                                        {
                                            GtinList.Add(amendAction.ProductGtin);
                                        }
                                    }

                                    //ReplaceActions?
                                    foreach (AssortmentMinorRevisionReplaceAction replaceAction in currentAssortmentMinorRevisionToAssess.ReplaceActions)
                                    {
                                        if (!GtinList.Contains(replaceAction.ProductGtin))
                                        {
                                            GtinList.Add(replaceAction.ProductGtin);
                                        }
                                    }

                                    //Go through each product in planogram and count the matching products
                                    foreach (PlanogramProduct planoProduct in myPlanogram.Products)
                                    {
                                        Int32 cnt = GtinList.Where(o => o == planoProduct.Gtin).Count();
                                        countForMinorAssortment += cnt;
                                    }

                                    productCountDict.Add(currentAssortmentMinorRevisionToAssess, countForMinorAssortment);
                                }

                                //Highest key
                                var dictEntry = productCountDict.Aggregate((l, r) => l.Value > r.Value ? l : r);
                                Int32 max = dictEntry.Value;

                                //Is Distinct?
                                Int32 distinctCount = productCountDict.Values.Where(k => k == max).Count();

                                if (distinctCount == 1)
                                {
                                    //When rating is distinct this is our appropriate universe.
                                    currentRow.ContentLookup.AssortmentMinorRevisionName = dictEntry.Key.Name;
                                    currentRow.ContentLookup.AssortmentMinorRevisionId = dictEntry.Key.Id;
                                }
                                else
                                {
                                    //If rating not unqiue then we fall back to getting the most recently modified of the several non distinct.
                                    List<AssortmentMinorRevision> MinorAssortments = new List<AssortmentMinorRevision>();

                                    foreach (KeyValuePair<AssortmentMinorRevision, Int32> entry in productCountDict)
                                    {
                                        if (entry.Value == max)
                                        {
                                            MinorAssortments.Add(entry.Key);
                                        }
                                    }

                                    //Unverses now is from relevant universes
                                    DateTime latest = MinorAssortments.Max(u => u.DateLastModified);
                                    AssortmentMinorRevision appropriateMinorRevision = MinorAssortments.Where(u => u.DateLastModified == latest).First();

                                    //set name
                                    currentRow.ContentLookup.AssortmentMinorRevisionName = appropriateMinorRevision.Name;
                                    currentRow.ContentLookup.AssortmentMinorRevisionId = appropriateMinorRevision.Id;
                                }
                            }
                        }
                    }

                }
                catch (Exception)
                { 
                    //Something went wrong...
                    failedToAssignToAll = true;
                }

                if (failedToAssignToAll)
                {
                    if (_autoAssignAllFail)
                    {
                        // if the AutoAssignAllCommand is called
                        _autoAsignFailList.Add(Message.ContentLookup_ShowCouldNotAutoAssign_AssortmentMinorRevision);
                    }
                    else
                    {
                        ShowWaitCursor(false);
                        //If the category has no associated product universes create info message
                        ShowCouldNotAutoAssign(Message.ContentLookup_ShowCouldNotAutoAssign_AssortmentMinorRevision);
                        ShowWaitCursor(true);
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #region ClearMinorAssortment

        private RelayCommand _clearMinorAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with MinorAssortment
        /// </summary>
        public RelayCommand ClearMinorAssortmentCommand
        {
            get
            {
                if (_clearMinorAssortmentCommand == null)
                {
                    _clearMinorAssortmentCommand = new RelayCommand(
                        p => ClearMinorAssortmentCommand_Executed(), p => ClearMinorAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_ClearMinorAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_ClearMinorAssortmentCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_AssortmentMinorRevision16,
                    };
                    base.ViewModelCommands.Add(_clearMinorAssortmentCommand);
                }
                return _clearMinorAssortmentCommand;
            }
        }

        private Boolean ClearMinorAssortmentCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearMinorAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearMinorAssortmentCommand_Executed()
        {
            if (ClearMinorAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.AssortmentMinorRevisionName = String.Empty;
                    currentRow.ContentLookup.AssortmentMinorRevisionId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignMinorAssortment

        private RelayCommand _manuallyAssignMinorAssortmentCommand;

        /// <summary>
        /// Handles Auto Assignment with MinorAssortment
        /// </summary>
        public RelayCommand ManuallyAssignMinorAssortmentCommand
        {
            get
            {
                if (_manuallyAssignMinorAssortmentCommand == null)
                {
                    _manuallyAssignMinorAssortmentCommand = new RelayCommand(
                        p => ManuallyAssignMinorAssortmentCommand_Executed(), p => ManuallyAssignMinorAssortmentCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_ManuallyAssignMinorAssortmentCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_ManuallyAssignMinorAssortmentCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_AssortmentMinorRevision16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignMinorAssortmentCommand);
                }
                return _manuallyAssignMinorAssortmentCommand;
            }
        }

        private Boolean ManuallyAssignMinorAssortmentCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignMinorAssortmentCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignMinorAssortmentCommand_Executed()
        {
            if (ManuallyAssignMinorAssortmentCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.MinorAssortment);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.AssortmentMinorRevisionName = ((AssortmentMinorRevisionInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.AssortmentMinorRevisionId = ((AssortmentMinorRevisionInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region Performance Selection

        #region ManuallyAssignPerformanceSelection

        private RelayCommand _ManuallyAssignPerformanceSelectionCommand;

        /// <summary>
        /// Handles Manually Assignment with PerformanceSelection
        /// </summary>
        public RelayCommand ManuallyAssignPerformanceSelectionCommand
        {
            get
            {
                if (_ManuallyAssignPerformanceSelectionCommand == null)
                {
                    _ManuallyAssignPerformanceSelectionCommand = new RelayCommand(
                        p => ManuallyAssignPerformanceSelectionCommand_Executed(), p => ManuallyAssignPerformanceSelectionCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_ManuallyAssignPerformanceSelectionCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_ManuallyAssignPerformanceSelectionCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_PerformanceSelectionMaintenance_16,
                    };
                    base.ViewModelCommands.Add(_ManuallyAssignPerformanceSelectionCommand);
                }
                return _ManuallyAssignPerformanceSelectionCommand;
            }
        }

        private Boolean ManuallyAssignPerformanceSelectionCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ManuallyAssignPerformanceSelectionCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignPerformanceSelectionCommand_Executed()
        {
            if (ManuallyAssignPerformanceSelectionCommand.CanExecute())
            {
                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.PerformanceSelections);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.PerformanceSelectionName = ((PerformanceSelectionInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.PerformanceSelectionId = ((PerformanceSelectionInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }
            }
        }

        #endregion

        #region ClearPerformanceSelection

        private RelayCommand _ClearPerformanceSelectionCommand;

        /// <summary>
        /// Handles Manually Assignment with PerformanceSelection
        /// </summary>
        public RelayCommand ClearPerformanceSelectionCommand
        {
            get
            {
                if (_ClearPerformanceSelectionCommand == null)
                {
                    _ClearPerformanceSelectionCommand = new RelayCommand(
                        p => ClearPerformanceSelectionCommand_Executed(), p => ClearPerformanceSelectionCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_ClearPerformanceSelectionCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ContentLookup_ClearPerformanceSelectionCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_PerformanceSelectionMaintenance_16,
                    };
                    base.ViewModelCommands.Add(_ClearPerformanceSelectionCommand);
                }
                return _ClearPerformanceSelectionCommand;
            }
        }

        private Boolean ClearPerformanceSelectionCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _ClearPerformanceSelectionCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearPerformanceSelectionCommand_Executed()
        {
            //Foreach Planogram:
            foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
            {
                currentRow.ContentLookup.PerformanceSelectionName = String.Empty;
                currentRow.ContentLookup.PerformanceSelectionId = null;
            }
        }

        #endregion

        #endregion

        #region Blocking Assignments

        #region ClearBlocking

        private RelayCommand _clearBlockingCommand;

        /// <summary>
        /// Handles Auto Assignment with Blocking
        /// </summary>
        public RelayCommand ClearBlockingCommand
        {
            get
            {
                if (_clearBlockingCommand == null)
                {
                    _clearBlockingCommand = new RelayCommand(
                        p => ClearBlockingCommand_Executed(), p => ClearBlockingCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearBlockingCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearBlockingCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_Blocking_16,
                    };
                    base.ViewModelCommands.Add(_clearBlockingCommand);
                }
                return _clearBlockingCommand;
            }
        }

        private Boolean ClearBlockingCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearBlockingCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearBlockingCommand_Executed()
        {
            if (ClearBlockingCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.BlockingName = String.Empty;
                    currentRow.ContentLookup.BlockingId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignBlocking

        private RelayCommand _manuallyAssignBlockingCommand;

        /// <summary>
        /// Handles Auto Assignment with Blocking
        /// </summary>
        public RelayCommand ManuallyAssignBlockingCommand
        {
            get
            {
                if (_manuallyAssignBlockingCommand == null)
                {
                    _manuallyAssignBlockingCommand = new RelayCommand(
                        p => ManuallyAssignBlockingCommand_Executed(), p => ManuallyAssignBlockingCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignBlockingCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignBlockingCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_Blocking_16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignBlockingCommand);
                }
                return _manuallyAssignBlockingCommand;
            }
        }

        private Boolean ManuallyAssignBlockingCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignBlockingCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignBlockingCommand_Executed()
        {
            if (ManuallyAssignBlockingCommand_CanExecute())
            {
                ShowWaitCursor(true);

                PlanogramSelectorViewModel viewModel = new PlanogramSelectorViewModel(
                    App.ViewState.CustomColumnLayoutFolderPath, App.ViewState.EntityId);

                Galleria.Ccm.Services.ServiceContainer.GetService<IWindowService>().ShowDialog<PlanogramSelectorWindow>(viewModel);

                if (viewModel.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.BlockingName = viewModel.SelectedPlanogramInfo.Name;
                        currentRow.ContentLookup.BlockingId = viewModel.SelectedPlanogramInfo.Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region Cluster Assignments

        #region ClearCluster

        private RelayCommand _clearClusterCommand;

        /// <summary>
        /// Handles Auto Assignment with Cluster
        /// </summary>
        public RelayCommand ClearClusterCommand
        {
            get
            {
                if (_clearClusterCommand == null)
                {
                    _clearClusterCommand = new RelayCommand(
                        p => ClearClusterCommand_Executed(), p => ClearClusterCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearClusterCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearClusterCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_Clusters_16,
                    };
                    base.ViewModelCommands.Add(_clearClusterCommand);
                }
                return _clearClusterCommand;
            }
        }

        private Boolean ClearClusterCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearClusterCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearClusterCommand_Executed()
        {
            if (ClearClusterCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.ClusterName = String.Empty;
                    currentRow.ContentLookup.ClusterId = null;
                    currentRow.ContentLookup.ClusterSchemeName = String.Empty;
                    currentRow.ContentLookup.ClusterSchemeId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignCluster

        private RelayCommand _manuallyAssignClusterCommand;

        /// <summary>
        /// Handles Auto Assignment with Cluster
        /// </summary>
        public RelayCommand ManuallyAssignClusterCommand
        {
            get
            {
                if (_manuallyAssignClusterCommand == null)
                {
                    _manuallyAssignClusterCommand = new RelayCommand(
                        p => ManuallyAssignClusterCommand_Executed(), p => ManuallyAssignClusterCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignClusterCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignClusterCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_Clusters_16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignClusterCommand);
                }
                return _manuallyAssignClusterCommand;
            }
        }

        private Boolean ManuallyAssignClusterCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignClusterCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignClusterCommand_Executed()
        {
            if (ManuallyAssignClusterCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.Cluster);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.ClusterSchemeName = ((ClusterSchemeInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.ClusterSchemeId = ((ClusterSchemeInfo)newManualAssign.ViewModel.SelectedItem).Id;
                        currentRow.ContentLookup.ClusterName = ((Cluster)newManualAssign.ViewModel.SecondSelectedItem).Name;
                        currentRow.ContentLookup.ClusterId = ((Cluster)newManualAssign.ViewModel.SecondSelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region RenumberingStrategy Assignments

        #region RenumberingStrategy

        private RelayCommand _clearRenumberingStrategyCommand;

        /// <summary>
        /// Handles Auto Assignment with Renumbering Strategy
        /// </summary>
        public RelayCommand ClearRenumberingStrategyCommand
        {
            get
            {
                if (_clearRenumberingStrategyCommand == null)
                {
                    _clearRenumberingStrategyCommand = new RelayCommand(
                        p => ClearRenumberingStrategyCommand_Executed(), p => ClearRenumberingStrategyCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearRenumberingStrategyCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearRenumberingStrategyCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_RenumberingStrategy_16,
                    };
                    base.ViewModelCommands.Add(_clearRenumberingStrategyCommand);
                }
                return _clearRenumberingStrategyCommand;
            }
        }

        private Boolean ClearRenumberingStrategyCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearRenumberingStrategyCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearRenumberingStrategyCommand_Executed()
        {
            if (ClearRenumberingStrategyCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.RenumberingStrategyName = String.Empty;
                    currentRow.ContentLookup.RenumberingStrategyId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignRenumberingStrategy

        private RelayCommand _manuallyAssignRenumberingStrategyCommand;

        /// <summary>
        /// Handles Auto Assignment with renumbering strategy
        /// </summary>
        public RelayCommand ManuallyAssignRenumberingStrategyCommand
        {
            get
            {
                if (_manuallyAssignRenumberingStrategyCommand == null)
                {
                    _manuallyAssignRenumberingStrategyCommand = new RelayCommand(
                        p => ManuallyAssignRenumberingStrategyCommand_Executed(), p => ManuallyAssignRenumberingStrategyCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignRenumberingStrategyCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignRenumberingStrategyCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_RenumberingStrategy_16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignRenumberingStrategyCommand);
                }
                return _manuallyAssignRenumberingStrategyCommand;
            }
        }

        private Boolean ManuallyAssignRenumberingStrategyCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignRenumberingStrategyCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignRenumberingStrategyCommand_Executed()
        {
            if (ManuallyAssignRenumberingStrategyCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.RenumberingStrategy);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.RenumberingStrategyName = ((RenumberingStrategyInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.RenumberingStrategyId = ((RenumberingStrategyInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region ValidationTemplate Assignments

        #region ValidationTemplate

        private RelayCommand _clearValidationTemplateCommand;

        /// <summary>
        /// Handles Auto Assignment with ValidationTemplate
        /// </summary>
        public RelayCommand ClearValidationTemplateCommand
        {
            get
            {
                if (_clearValidationTemplateCommand == null)
                {
                    _clearValidationTemplateCommand = new RelayCommand(
                        p => ClearValidationTemplateCommand_Executed(), p => ClearValidationTemplateCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearValidationTemplateCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearValidationTemplateCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ValidationTemplate_16,
                    };
                    base.ViewModelCommands.Add(_clearValidationTemplateCommand);
                }
                return _clearValidationTemplateCommand;
            }
        }

        private Boolean ClearValidationTemplateCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearValidationTemplateCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearValidationTemplateCommand_Executed()
        {
            if (ClearValidationTemplateCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.ValidationTemplateName = String.Empty;
                    currentRow.ContentLookup.ValidationTemplateId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignValidationTemplate

        private RelayCommand _manuallyAssignValidationTemplateCommand;

        /// <summary>
        /// Handles Auto Assignment with validation templates
        /// </summary>
        public RelayCommand ManuallyAssignValidationTemplateCommand
        {
            get
            {
                if (_manuallyAssignValidationTemplateCommand == null)
                {
                    _manuallyAssignValidationTemplateCommand = new RelayCommand(
                        p => ManuallyAssignValidationTemplateCommand_Executed(), p => ManuallyAssignValidationTemplateCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignValidationTemplateCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignValidationTemplateCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_ValidationTemplate_16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignValidationTemplateCommand);
                }
                return _manuallyAssignValidationTemplateCommand;
            }
        }

        private Boolean ManuallyAssignValidationTemplateCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignValidationTemplateCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignValidationTemplateCommand_Executed()
        {
            if (ManuallyAssignValidationTemplateCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.ValidationTemplate);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.ValidationTemplateName = ((ValidationTemplateInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.ValidationTemplateId = (Int32)((ValidationTemplateInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #region PlanogramNameTemplate Assignments

        #region PlanogramNameTemplate

        private RelayCommand _clearPlanogramNameTemplateCommand;

        /// <summary>
        /// Handles Auto Assignment with PlanogramNameTemplate
        /// </summary>
        public RelayCommand ClearPlanogramNameTemplateCommand
        {
            get
            {
                if (_clearPlanogramNameTemplateCommand == null)
                {
                    _clearPlanogramNameTemplateCommand = new RelayCommand(
                        p => ClearPlanogramNameTemplateCommand_Executed(), p => ClearPlanogramNameTemplateCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearPlanogramNameTemplateCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearPlanogramNameTemplateCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_PlanogramNameTemplate_16,
                    };
                    base.ViewModelCommands.Add(_clearPlanogramNameTemplateCommand);
                }
                return _clearPlanogramNameTemplateCommand;
            }
        }

        private Boolean ClearPlanogramNameTemplateCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearPlanogramNameTemplateCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearPlanogramNameTemplateCommand_Executed()
        {
            if (ClearPlanogramNameTemplateCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.PlanogramNameTemplateName = String.Empty;
                    currentRow.ContentLookup.PlanogramNameTemplateId = null;
                }

                ShowWaitCursor(false);
            }

        }


        #endregion

        #region ManuallyAssignPlanogramNameTemplate

        private RelayCommand _manuallyAssignPlanogramNameTemplateCommand;

        /// <summary>
        /// Handles Auto Assignment with planogram name template
        /// </summary>
        public RelayCommand ManuallyAssignPlanogramNameTemplateCommand
        {
            get
            {
                if (_manuallyAssignPlanogramNameTemplateCommand == null)
                {
                    _manuallyAssignPlanogramNameTemplateCommand = new RelayCommand(
                        p => ManuallyAssignPlanogramNameTemplateCommand_Executed(), p => ManuallyAssignPlanogramNameTemplateCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignPlanogramNameTemplateCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignPlanogramNameTemplateCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_PlanogramNameTemplate_16,
                    };
                    base.ViewModelCommands.Add(_manuallyAssignPlanogramNameTemplateCommand);
                }
                return _manuallyAssignPlanogramNameTemplateCommand;
            }
        }

        private Boolean ManuallyAssignPlanogramNameTemplateCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignPlanogramNameTemplateCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignPlanogramNameTemplateCommand_Executed()
        {
            if (ManuallyAssignPlanogramNameTemplateCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.PlanogramNameTemplate);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.PlanogramNameTemplateName = ((PlanogramNameTemplateInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.PlanogramNameTemplateId = (Int32)((PlanogramNameTemplateInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }
        
        #endregion

        #endregion

        #region PrintTemplate Assignments

        #region PrintTemplate

        private RelayCommand _clearPrintTemplateCommand;

        /// <summary>
        /// Handles Auto Assignment with PrintTemplate
        /// </summary>
        public RelayCommand ClearPrintTemplateCommand
        {
            get
            {
                if (_clearPrintTemplateCommand == null)
                {
                    _clearPrintTemplateCommand = new RelayCommand(
                        p => ClearPrintTemplateCommand_Executed(), p => ClearPrintTemplateCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearPrintTemplateCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ClearPrintTemplateCommand_Description,
                        SmallIcon = ImageResources.Delete_16
                    };
                    base.ViewModelCommands.Add(_clearPrintTemplateCommand);
                }
                return _clearPrintTemplateCommand;
            }
        }

        private Boolean ClearPrintTemplateCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _clearPrintTemplateCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ClearPrintTemplateCommand_Executed()
        {
            if (ClearPrintTemplateCommand_CanExecute())
            {
                ShowWaitCursor(true);

                //Foreach Planogram:
                foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                {
                    currentRow.ContentLookup.PrintTemplateName = String.Empty;
                    currentRow.ContentLookup.PrintTemplateId = null;
                }

                ShowWaitCursor(false);
            }

        }

        #endregion

        #region ManuallyAssignPlanogramNameTemplate

        private RelayCommand _manuallyAssignPrintTemplateCommand;

        /// <summary>
        /// Handles Auto Assignment with planogram name template
        /// </summary>
        public RelayCommand ManuallyAssignPrintTemplateCommand
        {
            get
            {
                if (_manuallyAssignPrintTemplateCommand == null)
                {
                    _manuallyAssignPrintTemplateCommand = new RelayCommand(
                        p => ManuallyAssignPrintTemplateCommand_Executed(), p => ManuallyAssignPrintTemplateCommand_CanExecute())
                    {
                        FriendlyName = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignPrintTemplateCommand_FriendlyName,
                        FriendlyDescription = Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.ManuallyAssignPrintTemplateCommand_Description,
                        SmallIcon = ImageResources.ContentLookup_PrintTemplate_16
                    };
                    base.ViewModelCommands.Add(_manuallyAssignPrintTemplateCommand);
                }
                return _manuallyAssignPrintTemplateCommand;
            }
        }

        private Boolean ManuallyAssignPrintTemplateCommand_CanExecute()
        {
            if (this.SelectedContentLookupRows == null || this._selectedContentLookupRows.Count == 0)
            {
                _manuallyAssignPrintTemplateCommand.DisabledReason = Message.ContentLookup_NoContentLookupsSelected;
                return false;
            }

            return true;
        }

        private void ManuallyAssignPrintTemplateCommand_Executed()
        {
            if (ManuallyAssignPrintTemplateCommand_CanExecute())
            {
                ShowWaitCursor(true);

                ContentLookupManualAssign newManualAssign = new ContentLookupManualAssign(ContentLookupItemType.PrintTemplate);
                newManualAssign.Owner = this.AttachedControl;
                newManualAssign.WindowStartupLocation = WindowStartupLocation.CenterOwner;

                newManualAssign.ShowDialog();

                if (newManualAssign.DialogResult == true)
                {
                    //Foreach Row:
                    foreach (ContentLookupRow currentRow in _selectedContentLookupRows)
                    {
                        currentRow.ContentLookup.PrintTemplateName = ((PrintTemplateInfo)newManualAssign.ViewModel.SelectedItem).Name;
                        currentRow.ContentLookup.PrintTemplateId = (Int32)((PrintTemplateInfo)newManualAssign.ViewModel.SelectedItem).Id;
                    }
                }

                ShowWaitCursor(false);
            }
        }


        #endregion

        #endregion

        #endregion

        #region Close

        private RelayCommand _closeCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(
                        p => Close_Executed(), p => Close_CanExecute())
                    {
                        FriendlyName = Message.Generic_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }
        #endregion

        #endregion

        #region Methods

        #region UpdatePermissonFlags

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //Product premissions
            //create
            _userHasContentLookupCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(ContentLookup));

            //fetch
            _userHasContentLookupFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(ContentLookup));

            //edit
            _userHasContentLookupEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(ContentLookup));

            //delete
            _userHasContentLookupDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(ContentLookup));
        }

        #endregion

        #region UpdateAvailableProductGroups

        private void UpdateAvailableProductGroups()
        {
            base.ShowWaitCursor(true);

            //Reload Merch hierarchy from data store
            ProductHierarchy merchHierarchy = ProductHierarchy.FetchByEntityId(App.ViewState.EntityId);
            _productGroups = new ReadOnlyCollection<ProductGroup>(merchHierarchy.FetchAllGroups());

            foreach (ProductGroup group in _productGroups)
            {
                ProductGroupViewModels.Add(new ProductGroupViewModel(group));
            }

            base.ShowWaitCursor(false);
        }

        #endregion

        #region ContinueWithItemChange

        /// <summary>
        /// Shows a warning requesting user ok to continue if current item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                if (this.ContentLookupRows.IsDirty())
                {
                    ModalMessage ShouldOverwritePrompt = new ModalMessage();
                    ShouldOverwritePrompt.Title = Message.ContentLookup_ContinueWithChange_Title;
                    ShouldOverwritePrompt.MessageIcon = ImageResources.DialogInformation;
                    ShouldOverwritePrompt.Header = String.Format(Message.ContentLookup_ContinueWithChange_Header, CurrentProductGroupViewModel.ProductGroupLabel);
                    ShouldOverwritePrompt.Description = Message.ContentLookup_ContinueWithChange_Description;
                    ShouldOverwritePrompt.ButtonCount = 3;
                    ShouldOverwritePrompt.Button1Content = Message.Generic_Save;
                    ShouldOverwritePrompt.Button2Content = Message.Generic_Continue;
                    ShouldOverwritePrompt.Button3Content = Message.Generic_Cancel;
                    ShouldOverwritePrompt.Owner = this.AttachedControl;
                    ShouldOverwritePrompt.WindowStartupLocation = WindowStartupLocation.CenterOwner;


                    ShouldOverwritePrompt.ShowDialog();

                    if (ShouldOverwritePrompt.Result == ModalMessageResult.Button1)
                    {
                        //Save
                        this.SaveCommand.Execute();
                        continueExecute = true;
                    }

                    if (ShouldOverwritePrompt.Result == ModalMessageResult.Button2)
                    {
                        //Don't save just quit
                        continueExecute = true;
                    }

                    if (ShouldOverwritePrompt.Result == ModalMessageResult.Button3)
                    {
                        //Cancel
                        continueExecute = false;
                    }
                }
            }
            return continueExecute;
        }

        #endregion

        #region ShowCouldNotAutoAssign

        private void ShowCouldNotAutoAssign(String itemName)
        {
            ModalMessage dialog =
                       new ModalMessage()
                       {
                           Title = Message.ContentLookup_ShowCouldNotAutoAssign_Title,
                           Header = Message.ContentLookup_ShowCouldNotAutoAssign_Header,
                           Description = String.Format(Message.ContentLookup_ShowCouldNotAutoAssign_Description, itemName),
                           ButtonCount = 1,
                           Button1Content = Message.Generic_Ok,
                           DefaultButton = ModalMessageButton.Button1,
                           CancelButton = ModalMessageButton.Button1,
                           MessageIcon = ImageResources.Dialog_Information
                       };

            if (this.AttachedControl != null)
            {
                dialog.Owner = this.AttachedControl;
                WindowStartupLocation startupLocation = WindowStartupLocation.CenterOwner;
                dialog.WindowStartupLocation = startupLocation;
            }

            dialog.ShowDialog();

        }

        private void ShowCouldNotAutoAssignAll(List<String> itemName)
        {
            ModalMessage dialog =
                       new ModalMessage()
                       {
                           Title = Message.ContentLookup_ShowCouldNotAutoAssign_Title,
                           Header = Message.ContentLookup_ShowCouldNotAutoAssign_Header,
                           Description = String.Format("{0} {1}", Message.ContentLookup_ShowCouldNotAutoAssignAll_Description, String.Join(Environment.NewLine, itemName)),
                           ButtonCount = 1,
                           Button1Content = Message.Generic_Ok,
                           DefaultButton = ModalMessageButton.Button1,
                           CancelButton = ModalMessageButton.Button1,
                           MessageIcon = ImageResources.Dialog_Information
                       };

            if (this.AttachedControl != null)
            {
                dialog.Owner = this.AttachedControl;
                WindowStartupLocation startupLocation = WindowStartupLocation.CenterOwner;
                dialog.WindowStartupLocation = startupLocation;
            }

            dialog.ShowDialog();

        }

        #endregion

        #endregion

        #region Event Handlers

        void CurrentProductGroupViewModel_CategoryChanged()
        {
            //Logic for when a new Product Group is Openened
            _contentLookupRows.UpdateContentLookupRows(_currentProductGroupViewModel.ProductGroup.Code);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    //Remove event handlers
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}
