﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.ContentLookupMaintenance
{
    /// <summary>
    /// Interaction logic for ContentLookupMaintenanceBackstageOpen.xaml
    /// </summary>
    public sealed partial class ContentLookupMaintenanceBackstageOpen : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ContentLookupMaintenanceViewModel), typeof(ContentLookupMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public ContentLookupMaintenanceViewModel ViewModel
        {
            get { return (ContentLookupMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ContentLookupMaintenanceBackstageOpen()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        private void xAvailableItemsDisplay_RowItemMouseDoubleClick(object sender, Framework.Controls.Wpf.ExtendedDataGridItemEventArgs e)
        {
            if (this.ViewModel.OpenCommand.CanExecute())
            {
                this.ViewModel.OpenCommand.Execute();
            }
        }

        #endregion

        private void XAvailableItemsDisplay_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && xAvailableItemsDisplay.SelectedItem != null)
            {
                if (this.ViewModel.OpenCommand.CanExecute())
                    this.ViewModel.OpenCommand.Execute();

                e.Handled = true;
            }
        }
    }
}
