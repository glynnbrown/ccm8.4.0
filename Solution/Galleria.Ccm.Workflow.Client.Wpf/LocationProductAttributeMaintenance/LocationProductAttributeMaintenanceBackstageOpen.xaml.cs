﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductAttributeMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class LocationProductAttributeMaintenanceBackstageOpen : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationProductAttributeMaintenanceViewModel),
            typeof(LocationProductAttributeMaintenanceBackstageOpen),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public LocationProductAttributeMaintenanceViewModel ViewModel
        {
            get { return (LocationProductAttributeMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationProductAttributeMaintenanceBackstageOpen()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the click of the open button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            existingSearchResults_MouseDoubleClick(existingSearchResults, null);
            e.Handled = true;
        }

        private void existingSearchResults_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ListBox senderControl = sender as ListBox;

            if (this.ViewModel != null)
            {
                LocationProductAttributeInfo existingItem = (LocationProductAttributeInfo)senderControl.SelectedItem;
                this.ViewModel.SelectedInfoItem = existingItem; // 

                if (this.ViewModel.OpenCommand.CanExecute(new Tuple<Int16?, Int32?>(existingItem.LocationId, existingItem.ProductId)))
                {
                    this.ViewModel.OpenCommand.Execute(new Tuple<Int16?, Int32?>(existingItem.LocationId, existingItem.ProductId));
                }
            }

            if (e != null)
            {
                e.Handled = true;
            }
        }

        #endregion

        private void ExistingSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var searchResults = existingSearchResults.SelectedItem as LocationProductAttributeInfo;
                if (searchResults != null)
                {
                    this.ViewModel.OpenCommand.Execute(new Tuple<Int16?,Int32?>(searchResults.LocationId,searchResults.ProductId));
                }
            }
            e.Handled = true;
        }
    }
}
