﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#region Version History: (CCM 8.1.1)
// CCM:30325 : I.George
// Added friendly description to Delete command
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Csla.Server;
using Csla.Threading;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Imports;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance
{
    public sealed class LocationProductAttributeMaintenanceViewModel : ViewModelAttachedControlObject<LocationProductAttributeMaintenanceOrganiser>//, IFollowProvider
    {
        /*[TODO]
         * Awaiting info on how multiselect is to work in this as 
         * editing one item at a time is not realistic.
         * Will then need to consider how open is to work as there will
         * be too many records to display all.
         */

        #region Fields

        const String _exceptionCategory = "LocationProductAttributeMaintenance";
        private const Int32 _batchSize = 5; // the number of locations for which custom attribute values are deleted in one go
        private ModalBusy _busyDialog; // the dialog with a deterministic progress bar to display progress of deleting custom attribute values
        private Boolean _canRemoveLocationProductAttribute = false; // flag used to indicate if the custom attribute values were deleted successfully
        private BackgroundWorker _customAttributeValueDeleteWorker;

        private List<Int16> _locationIds = new List<Int16>();
        private String _attributeColumnNames;

        //Location related
        private readonly LocationInfoListViewModel _masterAvailableLocationsView = new LocationInfoListViewModel();
        private BulkObservableCollection<LocationInfo> _availableLocations = new BulkObservableCollection<LocationInfo>();
        private ReadOnlyBulkObservableCollection<LocationInfo> _availableLocationsRO;

        //Product related
        private readonly ProductInfoListViewModel _masterAvailableProductsView = new ProductInfoListViewModel();
        private String _productSearchCriteria;
        private Boolean _isProductSearchQueued;
        private Boolean _isProcessingProductSearch;

        private readonly LocationProductAttributeInfoListViewModel _masterAvailableLocationProductAttributeInfosView = new LocationProductAttributeInfoListViewModel();

        private String _LocationProductAttributeMaintenanceSearchCriteria;
        private Boolean _isLocationProductAttributeMaintenanceSearchQueued;
        private Boolean _isProcessingLocationProductAttributeMaintenanceSearch;
        private LocationProductAttributeInfo _selectedInfoItem;

        private LocationProductAttribute _selectedItem;
        private Product _selectedLinkedProduct;

        private LocationInfo _selectedItemLocation;
        private ProductInfo _selectedItemProduct;

        private Boolean _freezeSelectedItemPropertyChanged = false;// freezes property changes within SelectedItem_PropertyChange, to avoid property value getting overriden 
        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AvailableLocationsProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.AvailableLocations);
        public static readonly PropertyPath AvailableProductsProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.ProductSearchResults);

        public static readonly PropertyPath SelectedInfoItemProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.SelectedInfoItem);
        public static readonly PropertyPath SelectedItemProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.SelectedItem);
        public static readonly PropertyPath SelectedLinkedProductProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(c => c.SelectedLinkedProduct);
        public static readonly PropertyPath SelectedItemLocationProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.SelectedItemLocation);
        public static readonly PropertyPath SelectedItemProductProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.SelectedItemProduct);
        public static readonly PropertyPath SelectedItemTitleProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.SelectedItemTitle);

        public static readonly PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.SaveCommand);
        public static readonly PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(v => v.CloseCommand);

        public static readonly PropertyPath ProductSearchCriteriaProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(v => v.ProductSearchCriteria);
        public static readonly PropertyPath IsProcessingProductSearchProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(v => v.IsProcessingProductSearch);


        public static readonly PropertyPath LocationProductAttributeMaintenanceSearchCriteriaProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.LocationProductAttributeMaintenanceSearchCriteria);
        public static readonly PropertyPath IsProcessingLocationProductAttributeMaintenanceSearchProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.IsProcessingLocationProductAttributeMaintenanceSearch);
        public static readonly PropertyPath LocationProductAttributeMaintenanceAttributeSearchResultsProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceViewModel>(p => p.LocationProductAttributeMaintenanceAttributeSearchResults);
        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available locations
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationInfo> AvailableLocations
        {
            get
            {
                if (_availableLocationsRO == null)
                {
                    _availableLocationsRO = new ReadOnlyBulkObservableCollection<LocationInfo>(_availableLocations);
                }
                return _availableLocationsRO;
            }
        }

        /// <summary>
        /// Returns the collection of available products
        /// </summary>
        public ReadOnlyBulkObservableCollection<ProductInfo> ProductSearchResults
        {
            get { return _masterAvailableProductsView.BindingView; }
        }

        /// <summary>
        /// Gets/Sets the criteria to use when searching for products
        /// </summary>
        public String ProductSearchCriteria
        {
            get { return _productSearchCriteria; }
            set
            {
                _productSearchCriteria = value;
                OnPropertyChanged(ProductSearchCriteriaProperty);
                OnProductSearchCriteriaChanged();
            }
        }

        /// <summary>
        /// Returns true if a product search is currently being processed.
        /// </summary>
        public Boolean IsProcessingProductSearch
        {
            get { return _isProcessingProductSearch; }
            private set
            {
                _isProcessingProductSearch = value;
                OnPropertyChanged(IsProcessingProductSearchProperty);
            }
        }

         /// <summary>
        /// Gets/Sets the criteria to use when searching for products
        /// </summary>
        public String LocationProductAttributeMaintenanceSearchCriteria
        {
            get { return _LocationProductAttributeMaintenanceSearchCriteria; }
            set
            {
                _LocationProductAttributeMaintenanceSearchCriteria = value;
                OnPropertyChanged(LocationProductAttributeMaintenanceSearchCriteriaProperty);
                OnLocationProductAttributeMaintenanceSearchCriteriaChanged();
            }
        }

        /// <summary>
        /// Return the collection of available location to product attributes based on the search criteria
        /// </summary>
        public ReadOnlyBulkObservableCollection<LocationProductAttributeInfo> LocationProductAttributeMaintenanceAttributeSearchResults
        {
            get { return _masterAvailableLocationProductAttributeInfosView.BindingView; }
        }

        /// <summary>
        /// Returns true if a Location to product search is currently being processed.
        /// </summary>
        public Boolean IsProcessingLocationProductAttributeMaintenanceSearch
        {
            get { return _isProcessingLocationProductAttributeMaintenanceSearch; }
            private set
            {
                _isProcessingLocationProductAttributeMaintenanceSearch = value;
                OnPropertyChanged(IsProcessingLocationProductAttributeMaintenanceSearchProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected item
        /// </summary>
        public LocationProductAttributeInfo SelectedInfoItem
        {
            get { return _selectedInfoItem; }
            set
            {
                _selectedInfoItem = value;
                OnPropertyChanged(SelectedInfoItemProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the selected item
        /// </summary>
        public LocationProductAttribute SelectedItem
        {
            get { return _selectedItem; }
            private set
            {
                LocationProductAttribute oldValue = _selectedItem;
                _selectedItem = value;
                OnPropertyChanged(SelectedItemProperty);
                OnSelectedItemChanged(oldValue, _selectedItem);
            }
        }

        /// <summary>
        /// Returns a title for the window based on the selected item
        /// </summary>
        public String SelectedItemTitle
        {
            get
            {
                if (this.SelectedItem != null)
                {
                    return String.Format("[ {0} ] - {1}", this.SelectedItem, Message.MasterDataManagement_LocationProductAttributeMaintenance);
                }
                return Message.MasterDataManagement_LocationProductAttributeMaintenance;
            }
        }

        /// <summary>
        /// Get/Sets the Product linked to the selected item
        /// </summary>
        public Product SelectedLinkedProduct
        {
            get { return _selectedLinkedProduct; }
            private set
            {
                _selectedLinkedProduct = value;
                OnPropertyChanged(SelectedLinkedProductProperty);
            }
        }

        /// <summary>
        /// Returns the location info for the selected item
        /// </summary>
        public LocationInfo SelectedItemLocation
        {
            get { return _selectedItemLocation; }
            private set
            {
                _selectedItemLocation = value;
                OnPropertyChanged(SelectedItemLocationProperty);
            }
        }

        /// <summary>
        /// Returns the product info for the selected item
        /// </summary>
        public ProductInfo SelectedItemProduct
        {
            get { return _selectedItemProduct; }
            private set
            {
                _selectedItemProduct = value;
                OnPropertyChanged(SelectedItemProductProperty);
            }
        }

        #region Following

        /// <summary>
        /// Gets the selected location's id
        /// </summary>
        public Int32 FollowItemIdentifier
        {
            get
            {
                if (this.SelectedItem != null)
                {
                    return (Int32)this.SelectedItem.LocationId;
                }
                else return 0;
            }
        }

        /// <summary>
        /// Gets the secondary identifier for the item to follow
        /// This item must be identifed using two identifiers
        /// </summary>
        public Int32? FollowItemSecondaryIdentifier
        {
            get
            {
                if (this.SelectedItem != null)
                {
                    // GFS-25254 - applying a bit of a cheat so that if the SelectedItem is not an existing record
                    //  the secondary identifier will be passed as 0 resulting in the SelectedItem being treated as new
                    //  from the follow control perspective. 
                    //  This type is an exception where it does not have its own it but always relies on IDs of existing 
                    //  Location and Product
                    if (this.SelectedItem.IsNew)
                    {
                        return 0;
                    }
                    else
                    {
                        return this.SelectedItem.ProductId;
                    }
                }
                else return 0;
            }
        }

        /// <summary>
        /// Gets the selected type as location
        /// </summary>
        public CCMDataItemType FollowItemType
        {
            get
            {
                return Galleria.Ccm.Imports.CCMDataItemType.LocationProductAttributes;
            }
        }

        /// <summary>
        /// Gets the name of the selected item
        /// </summary>
        public String FollowParentItemName
        {
            get
            {
                if (this.SelectedItem != null)
                {
                    return this.SelectedItem.ToString();
                }
                else return null;
            }
        }

        #endregion
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationProductAttributeMaintenanceViewModel()
        {
            //Update permissions
            UpdatePermissionFlags();

            //locations view
            _masterAvailableLocationsView.ModelChanged += MasterAvailableLocationsView_ModelChanged;
            _masterAvailableLocationsView.FetchAllForEntity();

            //product view
            _masterAvailableProductsView.ModelChanged += MasterAvailableProductsView_ModelChanged;

            //locationproductattribute info list
            _masterAvailableLocationProductAttributeInfosView.ModelChanged += MasterAvailableLocationProductAttributeInfosView_ModelChanged;
            this.SelectedInfoItem = null;
        }

        #endregion

        #region Commands

        #region OpenCommand

        private RelayCommand<Tuple<Int16?, Int32?>> _openCommand;

        /// <summary>
        /// Opens the item with the given id
        /// param: locationid & productid
        /// </summary>
        public RelayCommand<Tuple<Int16?, Int32?>> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand =
                        new RelayCommand<Tuple<Int16?, Int32?>>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                        {
                            FriendlyName = Message.Generic_Open,
                            DisabledReason = Message.LocationProductAttributeMaintenance_NoLocationProductSelectedDisabledReason
                        };
                    base.ViewModelCommands.Add(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Open_CanExecute(Tuple<Int16?, Int32?> ids)
        {

            //user must have fetch permission
            if (!_userHasLocationProductAttributeFetchPerm)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }

            //must not be null
            if (ids == null && this.AttachedControl != null && this.AttachedControl.Ribbon.SelectedTabItem != null
                && this.AttachedControl.Ribbon.SelectedTabItem.Name == this.AttachedControl.xBackstageNew.Name)
            {
                return false;
            }

            //both ids must be provided
            if (ids != null && (!ids.Item1.HasValue || !ids.Item2.HasValue) &&
                this.AttachedControl != null && this.AttachedControl.Ribbon.SelectedTabItem != null
                && this.AttachedControl.Ribbon.SelectedTabItem.Name == this.AttachedControl.xBackstageNew.Name
                )
            {
                return false;
            }

            if (this.SelectedInfoItem == null && this.AttachedControl != null && this.AttachedControl.Ribbon.SelectedTabItem != null
                && this.AttachedControl.Ribbon.SelectedTabItem.Name == this.AttachedControl.xBackstageOpen.Name)
            {
                return false;
            }


            return true;
        }

        private void Open_Executed(Tuple<Int16?, Int32?> ids)
        {
            if (ContinueWithItemChange())
            {
                base.ShowWaitCursor(true);

                try
                {
                    LocationProductAttribute existingItem = null;
                    //if the item already exists in the list open it
                    try
                    {
                        if (ids != null && ids.Item1.HasValue && ids.Item2.HasValue)
                        {
                            existingItem = LocationProductAttribute.FetchByLocationIdProductId(ids.Item1.Value, ids.Item2.Value);
                        }
                        else if (this.SelectedInfoItem != null)
                        {
                            existingItem = LocationProductAttribute.FetchByLocationIdProductId(this.SelectedInfoItem.LocationId, this.SelectedInfoItem.ProductId);
                        }
                        //if (existingItem != null && existingItem.DateDeleted != null)
                        //{
                        //    //it was an item that was previously deleted. Create a new clean object
                        //    LocationProductAttribute newItem =
                        //           LocationProductAttribute.NewLocationProductAttribute(ids.Item1.Value, ids.Item2.Value, App.ViewState.EntityId);
                        //    this.SelectedItem = newItem;
                        //    OnPropertyChanged(SelectedItemTitleProperty);
                        //}
                        //else
                        //{
                            this.SelectedItem = existingItem;
                        //}
                    }
                    catch (Exception e) // dto does not exist
                    {
                        //check the user has create permissions
                        if (_userHasLocationProductAttributeCreatePerm)
                        {
                            LocationProductAttribute newItem =
                                   LocationProductAttribute.NewLocationProductAttribute(ids.Item1.Value, ids.Item2.Value, App.ViewState.EntityId);
                            this.SelectedItem = newItem;
                            OnPropertyChanged(SelectedItemTitleProperty);
                        }
                        else
                        {
                            base.ShowWaitCursor(false);

                            //does not exist and user does not have permission to create - show error.
                            ModalMessage errorMessage = new ModalMessage();
                            errorMessage.Description = Message.LocationProductAttributeMaintenance_Open_NoCreatePermission;
                            errorMessage.ButtonCount = 1;
                            errorMessage.MessageIcon = ImageResources.TODO;//.Dialog_Error;
                            errorMessage.Button1Content = Message.Generic_Ok;
                            errorMessage.DefaultButton = ModalMessageButton.Button1;
                            App.ShowWindow(errorMessage, this.AttachedControl, /*isModal*/true);

                            return;
                        }
                    }
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exceptionCategory);
                    if (this.AttachedControl != null)
                    {
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            String.Empty, OperationType.Open,
                            this.AttachedControl);
                    }

                    base.ShowWaitCursor(true);
                }

                //close the backstage
                LocalHelper.SetRibbonBackstageState(this.AttachedControl, /*isOpen*/false);

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region Save Command

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(), p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    base.ViewModelCommands.Add(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Save_CanExecute()
        {
            //may not be null
            if (this.SelectedItem == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have edit permission
            if (!_userHasLocationProductAttributeEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.SelectedItem.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        /// <summary>
        /// Saves the current item.
        /// </summary>
        /// <returns>Returns true if the save was sucessful. False if cancelled.</returns>
        private Boolean SaveCurrentItem()
        {
            Boolean continueWithSave = true;

            //** save the item
            if (continueWithSave)
            {
                base.ShowWaitCursor(true);

                try
                {
                    this.SelectedItem = this.SelectedItem.Save();
                    //update available items
                    //Not sure whetrher to do this - may have a time cost to its operation
                    RefreshLocationProductAttributeMaintenanceSearchResults();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    continueWithSave = false;

                    LocalHelper.RecordException(ex, _exceptionCategory);
                    Exception rootException = ex.GetBaseException();

                    //if it is a concurrency exception check if the user wants to reload
                    if (rootException is ConcurrencyException)
                    {
                        Boolean itemReloadRequired = Galleria.Framework.Controls.Wpf.Helpers.ShowConcurrencyReloadPrompt(this.SelectedItem.ToString());
                        if (itemReloadRequired)
                        {
                            LocationProductAttribute existingItem = null;
                            //if the item already exists in the list open it
                            try
                            {
                                existingItem = LocationProductAttribute.FetchByLocationIdProductId(this.SelectedItem.LocationId, this.SelectedItem.ProductId);
                                this.SelectedItem = existingItem;
                            }
                            catch (DataPortalException) // dto does not exist
                            {
                                LocationProductAttribute newItem =
                                        LocationProductAttribute.NewLocationProductAttribute(this.SelectedItem.LocationId, this.SelectedItem.ProductId, App.ViewState.EntityId);
                                this.SelectedItem = newItem;

                                newItem.Description = String.Format("{0} : {1}", this.SelectedItemLocation.Name, this.SelectedItemProduct.Name);
                                //newItem.MarkGraphAsInitialized();
                            }
                        }
                    }
                    else
                    {
                        //otherwise just show the user an error has occurred.
                        if (this.AttachedControl != null)
                        {
                            Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                                this.SelectedItem.ToString(), OperationType.Save,
                                this.AttachedControl);
                        }
                    }

                    base.ShowWaitCursor(true);
                }

                //nb can't update available infos as don't know what search criteria was.

                base.ShowWaitCursor(false);
            }

            return continueWithSave;
        }

        #endregion

        #region SaveAndClose Command

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        /// Saves the current item and closes the window
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    base.ViewModelCommands.Add(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }


        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed
            if (itemSaved)
            {
                //close the attached window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }
        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        /// Deletes the current item
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                       p => Delete_Executed(), p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        SmallIcon = ImageResources.Delete_16,
                        FriendlyDescription = Message.Generic_Delete_Tooltip
                    };
                    base.ViewModelCommands.Add(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        private bool Delete_CanExecute()
        {
            //must not be null
            if (this.SelectedItem == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //user must have delete permission
            if (!_userHasLocationProductAttributeDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be new
            if (this.SelectedItem.IsNew)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            Boolean deleteConfirmed = true;
            if (this.AttachedControl != null)
            {
                //confirm the delete with the user
                deleteConfirmed = Galleria.Framework.Controls.Wpf.Helpers.ConfirmDeleteWithUser(this.SelectedItem.ToString());
            }

            if (deleteConfirmed)
            {
                base.ShowWaitCursor(true);

                LocationProductAttribute itemToDelete = this.SelectedItem;

                //remove the item from the list and save
                itemToDelete.Delete();

                //set the selected item to null
                this.SelectedItem = null;

                //commit the delete
                try
                {
                    itemToDelete.Save();
                }
                catch (DataPortalException ex)
                {
                    base.ShowWaitCursor(false);

                    LocalHelper.RecordException(ex, _exceptionCategory);
                    if (this.AttachedControl != null)
                    {
                        Galleria.Framework.Controls.Wpf.Helpers.ShowErrorOccurredMessage(
                            itemToDelete.ToString(), OperationType.Delete,
                            this.AttachedControl);
                    }

                    base.ShowWaitCursor(true);
                }

                //Not sure whetrher to do this - may have a time cost to its operation
                RefreshLocationProductAttributeMaintenanceSearchResults();

                base.ShowWaitCursor(false);
            }
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;
        /// <summary>
        /// Closes the current maintenance form
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed(), p => Close_CanExecute())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    base.ViewModelCommands.Add(_closeCommand);
                }
                return _closeCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Close_CanExecute()
        {
            return true;
        }

        private void Close_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        //#region AddLocationProductCustomAttribute

        //private RelayCommand _addLocationProductCustomAttributeCommand;

        ///// <summary>
        ///// Adds the current editing attribute to the attribute list
        ///// </summary>
        //public RelayCommand AddLocationProductCustomAttributeCommand
        //{
        //    get
        //    {
        //        if (_addLocationProductCustomAttributeCommand == null)
        //        {
        //            _addLocationProductCustomAttributeCommand = new RelayCommand(
        //                p => AddLocationProductCustomAttribute_Executed(),
        //                p => AddLocationProductCustomAttribute_CanExecute())
        //            {
        //                FriendlyName = Message.LocationProductAttributeMaintenance_AddLocationProductCustomAttribute,
        //                FriendlyDescription = Message.LocationProductAttributeMaintenance_AddLocationProductCustomAttribute_Description,
        //                SmallIcon = ImageResources.LocationProductAttributeMaintenance_AddLocationProductCustomAttribute
        //            };
        //            base.ViewModelCommands.Add(_addLocationProductCustomAttributeCommand);
        //        }
        //        return _addLocationProductCustomAttributeCommand;
        //    }
        //}

        //[DebuggerStepThrough]
        //private Boolean AddLocationProductCustomAttribute_CanExecute()
        //{
        //    //user must have create permission
        //    if (!_userHasLocationProductAttributeCreatePerm)
        //    {
        //        _addLocationProductCustomAttributeCommand.DisabledReason = Message.Generic_NoCreatePermission;
        //        return false;
        //    }

        //    //max 50 attributes
        //    if (this.AvailableAttributes.Count() >= 50)
        //    {
        //        _addLocationProductCustomAttributeCommand.DisabledReason = String.Empty;
        //        return false;
        //    }

        //    //name must be populated
        //    if (String.IsNullOrEmpty(this.CurrentEditingAttribute.Name))
        //    {
        //        _addLocationProductCustomAttributeCommand.DisabledReason = Message.LocationProductAttributeMaintenance_AddLocationProductCustomAttribute_DisabledNameIsEmpty;
        //        return false;
        //    }

        //    //name must be no longer than 50 characters
        //    if (this.CurrentEditingAttribute.Name.Length > 50)
        //    {
        //        _addLocationProductCustomAttributeCommand.DisabledReason = Message.LocationProductAttributeMaintenance_AddLocationProductCustomAttribute_DisabledNameIsTooLong;
        //        return false;
        //    }

        //    //name must be unique
        //    IEnumerable<String> attributeNames = this.AvailableAttributes.Select(a => a.Name.TrimEnd());
        //    if (attributeNames.Contains(this.CurrentEditingAttribute.Name.TrimEnd()))
        //    {
        //        _addLocationProductCustomAttributeCommand.DisabledReason = Message.LocationProductAttributeMaintenance_AddLocationProductCustomAttribute_DisabledNonUniqueName;
        //        return false;
        //    }

        //    return true;
        //}

        //private void AddLocationProductCustomAttribute_Executed()
        //{
        //    //Pre-save the list in case any names have been updated
        //    SaveLocationProductCustomAttributes();

        //    Boolean continueWithAdd = true;

        //    //** check the item unique value
        //    String newName;

        //    #region Set the columnnumber

        //    //set the column number
        //    IEnumerable<Byte> takenColumnNumbers = this.AvailableAttributes.Select(a => a.ColumnNumber);
        //    Byte nextColumnNumber = 1;
        //    while (nextColumnNumber < 51)
        //    {
        //        if (takenColumnNumbers.Contains(nextColumnNumber))
        //        {
        //            nextColumnNumber++;
        //        }
        //        else
        //        {
        //            break;
        //        }
        //    }
        //    Debug.Assert(nextColumnNumber != 51, "Suitable product attribute column number not found");
        //    this.CurrentEditingAttribute.ColumnNumber = nextColumnNumber;

        //    #endregion

        //    #region Check the name is unique

        //    List<LocationProductCustomAttributeInfo> existingAttributesIncludingDeleted =
        //        LocationProductCustomAttributeInfoList.FetchByEntityIdIncludingDeleted(App.ViewState.EntityId).ToList();

        //    Boolean nameAccepted = true;

        //    LocationProductCustomAttribute addItem = this.CurrentEditingAttribute;


        //    IEnumerable<String> currentNames = null;
        //    currentNames = existingAttributesIncludingDeleted.
        //        Where(p => p.Id != addItem.Id).Select(q => q.Name.ToUpperInvariant().TrimEnd());

        //    nameAccepted = Framework.Controls.Wpf.Helpers.PromptIfNameIsNotUnique(
        //        p => !currentNames.Contains(p.ToUpperInvariant().TrimEnd()),
        //        addItem.Name, out newName);

        //    if (nameAccepted)
        //    {
        //        if (addItem.Name != newName)
        //        {
        //            addItem.Name = newName;
        //        }
        //    }
        //    else
        //    {
        //        continueWithAdd = false;
        //    }

        //    #endregion

        //    //** save the item
        //    if (continueWithAdd)
        //    {
        //        base.ShowWaitCursor(true);

        //        //add to the master list
        //        _masterCustomAttributeListView.Model.Add(addItem);

        //        //load a new editing item
        //        this.CurrentEditingAttribute = LocationProductCustomAttribute.NewLocationProductCustomAttribute(App.ViewState.EntityId);

        //        //save straight away as we need the id
        //        SaveLocationProductCustomAttributes();
        //    }

        //    base.ShowWaitCursor(false);
        //}

        //#endregion

        //#region RemoveLocationProductCustomAttribute

        //private RelayCommand<LocationProductCustomAttribute> _removeLocationProductCustomAttributeCommand;

        ///// <summary>
        ///// Removes the given attribute from the list.
        ///// </summary>
        //public RelayCommand<LocationProductCustomAttribute> RemoveLocationProductCustomAttributeCommand
        //{
        //    get
        //    {
        //        if (_removeLocationProductCustomAttributeCommand == null)
        //        {
        //            _removeLocationProductCustomAttributeCommand = new RelayCommand<LocationProductCustomAttribute>(
        //                p => RemoveLocationProductCustomAttribute_Executed(p),
        //                p => RemoveLocationProductCustomAttribute_CanExecute(p))
        //            {
        //                FriendlyDescription = Message.LocationProductAttributeMaintenance_RemoveLocationProductCustomAttribute,
        //                SmallIcon = ImageResources.LocationProductAttributeMaintenance_RemoveLocationProductCustomAttribute
        //            };
        //            base.ViewModelCommands.Add(_removeLocationProductCustomAttributeCommand);
        //        }
        //        return _removeLocationProductCustomAttributeCommand;
        //    }
        //}

        //[DebuggerStepThrough]
        //private Boolean RemoveLocationProductCustomAttribute_CanExecute(LocationProductCustomAttribute attributeToRemove)
        //{
        //    //user must have delete permission
        //    if (!_userHasLocationProductAttributeDeletePerm)
        //    {
        //        _addLocationProductCustomAttributeCommand.DisabledReason = Message.Generic_NoDeletePermission;
        //        return false;
        //    }

        //    //must not be null
        //    if (attributeToRemove == null)
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //private void RemoveLocationProductCustomAttribute_Executed(LocationProductCustomAttribute attributeToRemove)
        //{
        //    //Pre-save the list in case any names have been updated
        //    SaveLocationProductCustomAttributes();

        //    Int32 selectedProductId = 0;
        //    Int16 selectedLocationId = 0;
        //    if (this.SelectedItem != null)
        //    {
        //        selectedProductId = this.SelectedItem.ProductId;
        //        selectedLocationId = this.SelectedItem.LocationId;
        //    }

        //    if (this.AttachedControl != null)
        //    {
        //        // confirm deletion with user
        //        ModalMessage confirmationPrompt = new ModalMessage()
        //        {
        //            Header = Message.RemoveLocationProductCustomAttribute_Confirm,
        //            Description = String.Format(
        //                CultureInfo.CurrentCulture, Message.RemoveLocationProductCustomAttribute_Confirm_Description, attributeToRemove.Name),
        //            ButtonCount = 2,
        //            MessageIcon = ImageResources.Dialog_Warning,
        //            Button1Content = Message.Generic_Ok,
        //            DefaultButton = ModalMessageButton.Button1,
        //            Button2Content = Message.Generic_Cancel,
        //            CancelButton = ModalMessageButton.Button2
        //        };
        //        App.ShowWindow(confirmationPrompt, this.AttachedControl, /*isModal*/true);

        //        // if user confirmed 
        //        if (confirmationPrompt.Result == ModalMessageResult.Button1)
        //        {
        //            _canRemoveLocationProductAttribute = false; //reset canRemoveAttribute flag

        //            // attempt to delete custom attribute values for the selected attribute to remove
        //            DeleteRelatedCustomAttributeValues(attributeToRemove);

        //            // if ok to do so, remove the actual attribute
        //            if (_canRemoveLocationProductAttribute)
        //            {
        //                base.ShowWaitCursor(true);

        //                //remove from the master list
        //                _masterCustomAttributeListView.Model.Remove(attributeToRemove);

        //                //save immediately
        //                SaveLocationProductCustomAttributes();

        //                //if a product was selected, refresh it as well
        //                if (selectedProductId != 0 && selectedLocationId != 0)
        //                {
        //                    // GFS-24727 Try to open instead of fetching - will create a new item if previously opened was never saved
        //                    //  and will prompt to save current changes before reloading the attribute
        //                    this.OpenCommand.Execute(new Tuple<short?, int?>(selectedLocationId, selectedProductId));
        //                }

        //                base.ShowWaitCursor(false);
        //            }
        //        }
        //    }
        //    else // running through nunit
        //    {
        //        _canRemoveLocationProductAttribute = false; //reset canRemoveAttribute flag

        //        // attempt to delete custom attribute values for the selected attribute to remove
        //        DeleteRelatedCustomAttributeValues(attributeToRemove);

        //        while (_customAttributeValueDeleteWorker.IsBusy) // force wait until delete worker completes
        //        {
        //            System.Threading.Thread.Sleep(500);
        //        }

        //        // if ok to do so, remove the actual attribute
        //        if (_canRemoveLocationProductAttribute)
        //        {
        //            //remove from the master list
        //            _masterCustomAttributeListView.Model.Remove(attributeToRemove);

        //            //save immediately
        //            SaveLocationProductCustomAttributes();
        //        }
        //    }
        //}

        ///// <summary>
        ///// Performs a batch delete of custom attribute values using a backgroud worker. 
        ///// Reports progress of the operation to the user. Can be canceled.
        ///// </summary>
        ///// <param name="customAttribtueToDelete">The custom attribute for which we are erasing the values</param>
        //private void DeleteRelatedCustomAttributeValues(LocationProductCustomAttribute customAttribtueToDelete)
        //{

        //    //create loctation ids list
        //    _locationIds = LocationProductAttributeList.FetchByEntityId(App.ViewState.EntityId).Select(l => l.LocationId).Distinct().ToList();

        //    //create custom attribute column names list
        //    //at the moment values for only one attribute can be deleted at the time, however the procedure suports a number of custom attributes 
        //    _attributeColumnNames = String.Empty;
        //    String colNumber = (customAttribtueToDelete.ColumnNumber < 10) ?
        //            String.Format(CultureInfo.InvariantCulture, "0{0}", customAttribtueToDelete.ColumnNumber)
        //                : customAttribtueToDelete.ColumnNumber.ToString(CultureInfo.InvariantCulture);
        //    _attributeColumnNames += String.Format(CultureInfo.InvariantCulture, "LocationProductAttribute_CustomAttribute{0}", colNumber);
        //    if (this.AttachedControl != null)
        //    {
        //        //create a busy dialog and the delete values worker
        //        _busyDialog = new ModalBusy();
        //        _busyDialog.Header = Message.BackstageDataManagement_BusyDeletingHeader;
        //        _busyDialog.Description = Message.BackstageDataManagement_BusyDeletingDescription;
        //        _busyDialog.IsDeterminate = true;
        //        _busyDialog.WindowStartupLocation = WindowStartupLocation.CenterOwner;
        //        _busyDialog.Owner = this.AttachedControl;
        //        _busyDialog.Closing += new CancelEventHandler(_busyDialog_Closing);
        //    }

        //    _customAttributeValueDeleteWorker = new BackgroundWorker();
        //    _customAttributeValueDeleteWorker.WorkerReportsProgress = true;
        //    _customAttributeValueDeleteWorker.WorkerSupportsCancellation = true;
        //    _customAttributeValueDeleteWorker.DoWork += new DoWorkEventHandler(CustomAttributeValueDeleteWorker_DoWork);
        //    _customAttributeValueDeleteWorker.ProgressChanged += new ProgressChangedEventHandler(CustomAttributeValueDeleteWorker_ProgressChanged);
        //    _customAttributeValueDeleteWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CustomAttributeValueDeleteWorker_RunWorkerCompleted);

        //    // run the worker and show the busy dialog
        //    Dispatcher dispatcher = this.AttachedControl != null ? this.AttachedControl.Dispatcher : Dispatcher.CurrentDispatcher;
        //    _customAttributeValueDeleteWorker.RunWorkerAsync(dispatcher);
        //    if (_busyDialog != null)
        //        App.ShowWindow(_busyDialog, this.AttachedControl, /*isModal*/true);
        //}

        //void _busyDialog_Closing(object sender, CancelEventArgs e)
        //{
        //    // the only way for the user to cancel the delete is to click the 'X' on the busy dialog
        //    if (_customAttributeValueDeleteWorker != null && _customAttributeValueDeleteWorker.IsBusy)
        //    {
        //        _busyDialog.Closing -= _busyDialog_Closing;
        //        _customAttributeValueDeleteWorker.CancelAsync();
        //    }
        //}

        //#region Remove Location Product Custom Attribute Values Worker

        ///// <summary>
        ///// Does the actual work to delete the custom attribute values
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void CustomAttributeValueDeleteWorker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    for (int i = 0; i < _locationIds.Count(); i += _batchSize)
        //    {
        //        //check for cancelation
        //        if (_customAttributeValueDeleteWorker.CancellationPending)
        //        {
        //            e.Cancel = true;
        //            return;
        //        }
        //        else
        //        {
        //            List<Int16> locationIdsBatch = _locationIds.Skip(i).Take(_batchSize).ToList();

        //            //delete a batch of attribute values
        //            // the changes are committed in batches to avoid the transaction log growing too large
        //            //   when millions of rows are being processed. This may lead to some of the attribute values being deleted, 
        //            //      even though the operation as a whole was canceled or unsuccessful, however this in intentional as per CK
        //            LocationProductAttribute.ExecuteDeleteCustomAttributeValueByLocationIds(locationIdsBatch, _attributeColumnNames);


        //            //Report progress
        //            Int32 progressPercentage = Math.Min(100, i * 100 / _locationIds.Count());
        //            _customAttributeValueDeleteWorker.ReportProgress(progressPercentage);
        //        }
        //    }
        //}

        ///// <summary>
        ///// Updates progress
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //private void CustomAttributeValueDeleteWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //{
        //    if (this.AttachedControl != null)
        //    {
        //        UpdateBusyModalProgressBar(e.ProgressPercentage);
        //    }
        //}

        ///// <summary>
        ///// Sets the ProgressPercentage property value for _busyDialog
        ///// </summary>
        ///// <param name="progress"></param>
        //private void UpdateBusyModalProgressBar(Int32 progress)
        //{
        //    if (_busyDialog != null)
        //        _busyDialog.SetValue(ModalBusy.ProgressPercentageProperty, progress);
        //}

        //private void CustomAttributeValueDeleteWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    //show dialog at least for a sec
        //    if (_busyDialog != null)
        //        _busyDialog.Close();

        //    if (e.Error != null)
        //    {
        //        ModalMessage canceledMessage = new ModalMessage();
        //        canceledMessage.Header = String.Format(
        //            CultureInfo.InvariantCulture, Message.LocationProductAttributeMaintenance_RemoveLocationProductCustomAttribute_Error);
        //        canceledMessage.Description = String.Format(
        //            CultureInfo.CurrentCulture, Message.LocationProductAttributeMaintenance_RemoveLocationProductCustomAttribute_Error_Description, e.Error);
        //        canceledMessage.ButtonCount = 1;
        //        canceledMessage.MessageIcon = ImageResources.Dialog_Error;
        //        canceledMessage.Button1Content = Message.Generic_Ok;
        //        canceledMessage.DefaultButton = ModalMessageButton.Button1;
        //        App.ShowWindow(canceledMessage, this.AttachedControl, /*isModal*/true);
        //        return;
        //    }
        //    else if (e.Cancelled)
        //    {
        //        ModalMessage canceledMessage = new ModalMessage();
        //        canceledMessage.Header = String.Format(
        //            CultureInfo.InvariantCulture, Message.LocationProductAttributeMaintenance_RemoveLocationProductCustomAttribute_Canceled);
        //        canceledMessage.Description = String.Format(
        //            CultureInfo.InvariantCulture, Message.LocationProductAttributeMaintenance_RemoveLocationProductCustomAttribute_Canceled_Description);
        //        canceledMessage.ButtonCount = 1;
        //        canceledMessage.MessageIcon = ImageResources.Dialog_Information;
        //        canceledMessage.Button1Content = Message.Generic_Ok;
        //        canceledMessage.DefaultButton = ModalMessageButton.Button1;
        //        App.ShowWindow(canceledMessage, this.AttachedControl, /*isModal*/true);
        //        return;
        //    }
        //    else
        //    {
        //        //if there was no errors and the delete wasn't canceled
        //        _canRemoveLocationProductAttribute = true;
        //    }
        //}

        //#endregion
        //#endregion

        #endregion

        #region Event Handlers

        private void MasterAvailableLocationsView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<LocationInfoList> e)
        {
            OnMasterAvailableLocationsViewModelChanged();
        }

        private void OnMasterAvailableLocationsViewModelChanged()
        {
            _availableLocations.Clear();
            _availableLocations.AddRange(_masterAvailableLocationsView.Model);
        }

        private void MasterAvailableProductsView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<ProductInfoList> e)
        {
            //mark as finished searching
            this.IsProcessingProductSearch = false;

            //check if another search is queued
            if (_isProductSearchQueued)
            {
                RefreshProductSearchResults();
            }
        }

        private void MasterAvailableLocationProductAttributeInfosView_ModelChanged(object sender, ViewStateObjectModelChangedEventArgs<LocationProductAttributeInfoList> e)
        {
            //mark as finished searching
            this.IsProcessingLocationProductAttributeMaintenanceSearch = false;

            //check if another search is queued
            if (_isLocationProductAttributeMaintenanceSearchQueued)
            {
                RefreshLocationProductAttributeMaintenanceSearchResults();
            }
        }

        /// <summary>
        /// Responds to a change of selected item
        /// </summary>
        private void OnSelectedItemChanged(LocationProductAttribute oldValue, LocationProductAttribute newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= SelectedItem_PropertyChanged;
            }

            if (newValue != null)
            {
                newValue.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(SelectedItem_PropertyChanged);

                //get the related loc info
                Int16 locId = this.SelectedItem.LocationId;
                this.SelectedItemLocation = this.AvailableLocations.FirstOrDefault(l => l.Id == locId);
                Debug.Assert(this.SelectedItemLocation != null, "Related location info not found");

                //get the linked product
                Int32 prodId = this.SelectedItem.ProductId;
                SelectedLinkedProduct = Product.FetchById(prodId);

                //get the related product info
                _productSearchCriteria = SelectedLinkedProduct.Gtin;
                ProductInfoList productInfoList = ProductInfoList.FetchByEntityIdSearchCriteria(App.ViewState.EntityId, _productSearchCriteria);
                this.SelectedItemProduct = productInfoList.FirstOrDefault(p => p.Id == prodId);
                Debug.Assert(this.SelectedItemProduct != null, "Related product info not found");

                // deal with following
                if (this.AttachedControl != null)
                {
                    OnRefreshFollowingRequested();
                }
            }
            else
            {
                this.SelectedItemLocation = null;
                this.SelectedItemProduct = null;
                this.SelectedLinkedProduct = null;
            }

            //Update title
            OnPropertyChanged(SelectedItemTitleProperty);
        }

        #region Following

        /// <summary>
        /// The RefreshFollowingRequested event. 
        /// Request to fetch the following value for a given item from the database.
        /// </summary>
        public event EventHandler RefreshFollowingRequested;
        /// <summary>
        /// Fires the RefreshFollowingRequested event
        /// </summary>
        private void OnRefreshFollowingRequested()
        {
            if (RefreshFollowingRequested != null)
            {
                RefreshFollowingRequested(this, EventArgs.Empty);
            }
        }
        #endregion

        /// <summary>
        /// Handles property changes of Selected Item
        /// </summary>
        void SelectedItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (this.SelectedItem != null && !_freezeSelectedItemPropertyChanged)
            {
                if (e.PropertyName == LocationProductAttribute.CasePackUnitsProperty.Name
                    )
                {
                    //when user set CasePackUnits value manually, set high, wide and deep values to null
                    if (this.SelectedItem.CasePackUnits != null)
                    {
                        _freezeSelectedItemPropertyChanged = true;
                        this.SelectedItem.CaseHigh = null;
                        this.SelectedItem.CaseWide = null;
                        this.SelectedItem.CaseDeep = null;
                        _freezeSelectedItemPropertyChanged = false;
                    }
                }
                else if (e.PropertyName == LocationProductAttribute.CaseHighProperty.Name
                    || e.PropertyName == LocationProductAttribute.CaseWideProperty.Name
                    || e.PropertyName == LocationProductAttribute.CaseDeepProperty.Name)
                {
                    //On manual change of high, wide or deep values, set CasePackUnits, but do not go 
                    //through property changed as would zero out high, wide and deep values
                    _freezeSelectedItemPropertyChanged = true;
                    if (this.SelectedItem.CaseHigh.HasValue && this.SelectedItem.CaseHigh.Value != 0
                        && this.SelectedItem.CaseWide.HasValue && this.SelectedItem.CaseWide.Value != 0
                        && this.SelectedItem.CaseDeep.HasValue && this.SelectedItem.CaseDeep.Value != 0)
                    {
                        this.SelectedItem.CasePackUnits
                            = (Int16)(this.SelectedItem.CaseHigh * this.SelectedItem.CaseWide * this.SelectedItem.CaseDeep);
                    }
                    else
                    {
                        this.SelectedItem.CasePackUnits = null;
                    }
                    _freezeSelectedItemPropertyChanged = false;
                }
                else if (e.PropertyName == LocationProductAttribute.TrayPackUnitsProperty.Name
                    )
                {
                    //when user set TrayPackUnits value manually, set high, wide and deep values to null
                    if (this.SelectedItem.TrayPackUnits != null)
                    {
                        _freezeSelectedItemPropertyChanged = true;
                        this.SelectedItem.TrayHigh = null;
                        this.SelectedItem.TrayWide = null;
                        this.SelectedItem.TrayDeep = null;
                        _freezeSelectedItemPropertyChanged = false;
                    }
                }
                else if (e.PropertyName == LocationProductAttribute.TrayHighProperty.Name
                    || e.PropertyName == LocationProductAttribute.TrayWideProperty.Name
                    || e.PropertyName == LocationProductAttribute.TrayDeepProperty.Name)
                {
                    //On manual change of high, wide or deep values, set TrayPackUnits, but do not go 
                    //through property changed as would zero out high, wide and deep values
                    _freezeSelectedItemPropertyChanged = true;
                    if (this.SelectedItem.TrayHigh.HasValue && this.SelectedItem.TrayHigh.Value != 0
                        && this.SelectedItem.TrayWide.HasValue && this.SelectedItem.TrayWide.Value != 0
                        && this.SelectedItem.TrayDeep.HasValue && this.SelectedItem.TrayDeep.Value != 0)
                    {
                        this.SelectedItem.TrayPackUnits
                            = (Int16)(this.SelectedItem.TrayHigh * this.SelectedItem.TrayWide * this.SelectedItem.TrayDeep);
                    }
                    else
                    {
                        this.SelectedItem.TrayPackUnits = null;
                    }
                    _freezeSelectedItemPropertyChanged = false;
                }
            }
        }

        /// <summary>
        /// Responds to a change of product search criteria
        /// </summary>
        private void OnProductSearchCriteriaChanged()
        {
            RefreshProductSearchResults();
        }

        /// <summary>
        /// Responds to a change of location to product attribute search criteria
        /// </summary>
        private void OnLocationProductAttributeMaintenanceSearchCriteriaChanged()
        {
            RefreshLocationProductAttributeMaintenanceSearchResults();
        }

         #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            Boolean continueExecute = true;
            if (this.AttachedControl != null)
            {
                continueExecute = Galleria.Framework.Controls.Wpf.Helpers.ContinueWithItemChange(this.SelectedItem, this.SaveCommand);
            }
            return continueExecute;
        }

        /// <summary>
        /// Researches for products based on the product search criteria
        /// </summary>
        private void RefreshProductSearchResults()
        {
            _isProductSearchQueued = false;

            if (!this.IsProcessingProductSearch)
            {
                this.IsProcessingProductSearch = true;

                //search asyncronously
                _masterAvailableProductsView.BeginFetchByEntityIdSearchCriteria(App.ViewState.EntityId, _productSearchCriteria);
            }
        }

        /// <summary>
        /// Researches for location to product attributes based on the search criteria
        /// </summary>
        private void RefreshLocationProductAttributeMaintenanceSearchResults()
        {
            _isLocationProductAttributeMaintenanceSearchQueued = false;

            if (!this.IsProcessingLocationProductAttributeMaintenanceSearch)
            {
                this.IsProcessingLocationProductAttributeMaintenanceSearch = true;

                //search asyncronously
                _masterAvailableLocationProductAttributeInfosView.BeginFetchByEntityIdSearchCriteria(App.ViewState.EntityId, _LocationProductAttributeMaintenanceSearchCriteria);
            }
        }

        #endregion

        #region Screen Permissions

        private Boolean _userHasLocationProductAttributeCreatePerm;
        private Boolean _userHasLocationProductAttributeFetchPerm;
        private Boolean _userHasLocationProductAttributeEditPerm;
        private Boolean _userHasLocationProductAttributeDeletePerm;

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //Product premissions
            //create
            _userHasLocationProductAttributeCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(LocationProductAttribute));

            //fetch
            _userHasLocationProductAttributeFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(LocationProductAttribute));

            //edit
            _userHasLocationProductAttributeEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(LocationProductAttribute));

            //delete
            _userHasLocationProductAttributeDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(LocationProductAttribute));
        }

        /// <summary>
        /// Returns true if the current user has sufficient permissions to access this screen.
        /// </summary>
        /// <returns></returns>
        internal static Boolean UserHasRequiredAccessPerms()
        {
            return

                //location attribute fetch
                Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(LocationProductAttribute))

                //product fetch
                && Csla.Rules.BusinessRules.HasPermission(Csla.Rules.AuthorizationActions.GetObject, typeof(Product));
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    if (this.SelectedItem != null)
                    {
                        _selectedItem.PropertyChanged -= SelectedItem_PropertyChanged;
                    }
                    _masterAvailableLocationsView.ModelChanged -= MasterAvailableLocationsView_ModelChanged;
                    _masterAvailableProductsView.ModelChanged -= MasterAvailableProductsView_ModelChanged;
                    _masterAvailableLocationProductAttributeInfosView.ModelChanged -= MasterAvailableLocationProductAttributeInfosView_ModelChanged;
                    _masterAvailableLocationsView.Dispose();
                    _masterAvailableProductsView.Dispose();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }


    /// <summary>
    /// Binding validation rule to ensure assortment product rank is unique:
    /// </summary>
    public class LocationProductCustomAttributeValueRule : ValidationRule
    {
        #region Constructor
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public LocationProductCustomAttributeValueRule() : base(ValidationStep.CommittedValue, true) { }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the validation of our business rules
        /// </summary>
        /// <param name="value">The value to validate</param>
        /// <param name="cultureInfo">The current culture</param>
        /// <returns>The validation results</returns>
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            // assume success
            ValidationResult validationResult = ValidationResult.ValidResult;
            return validationResult;
        }
        #endregion

    }

}
