﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fluent;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductAttributeMaintenanceHomeTab.xaml
    /// </summary>
    public sealed partial class LocationProductAttributeMaintenanceHomeTab : RibbonTabItem
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationProductAttributeMaintenanceViewModel), typeof(LocationProductAttributeMaintenanceHomeTab),
            new PropertyMetadata(null));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public LocationProductAttributeMaintenanceViewModel ViewModel
        {
            get { return (LocationProductAttributeMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion


        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationProductAttributeMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
