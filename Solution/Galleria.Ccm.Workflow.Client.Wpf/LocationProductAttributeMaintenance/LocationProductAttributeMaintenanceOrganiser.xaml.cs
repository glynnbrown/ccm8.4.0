﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Workflow.Client.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductAttributeMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class LocationProductAttributeMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationProductAttributeMaintenanceViewModel), typeof(LocationProductAttributeMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public LocationProductAttributeMaintenanceViewModel ViewModel
        {
            get { return (LocationProductAttributeMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            LocationProductAttributeMaintenanceOrganiser senderControl = (LocationProductAttributeMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                LocationProductAttributeMaintenanceViewModel oldModel = (LocationProductAttributeMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;

            }

            if (e.NewValue != null)
            {
                LocationProductAttributeMaintenanceViewModel newModel = (LocationProductAttributeMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;



            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public LocationProductAttributeMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.LocationProductAttributeMaintenance);

            this.ViewModel = new LocationProductAttributeMaintenanceViewModel();
            LocalHelper.SetRibbonBackstageState(this.ViewModel.AttachedControl, true);

            //Subscribe to key down event
            this.KeyDown += new KeyEventHandler(LocationProductAttributeMaintenanceOrganiser_KeyDown);

            this.Loaded += new RoutedEventHandler(LocationProductAttributeMaintenanceOrganiser_Loaded);
        }

        private void LocationProductAttributeMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= LocationProductAttributeMaintenanceOrganiser_Loaded;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Key down event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationProductAttributeMaintenanceOrganiser_KeyDown(object sender, KeyEventArgs e)
        {
            //If Ctrl+O selected, set backstage to true and select open tab
            if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
            {
                //Open backstage
                LocalHelper.SetRibbonBackstageState(this, true);
                //Select open tab
                this.xBackstageOpen.IsSelected = true;
            }
        }

        #endregion

        #region Window close

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }

            }
        }

        /// <summary>
        /// Carries out window on closed actions
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            if (this.ViewModel != null)
            {
                this.Dispatcher.Invoke((Action)(() =>
                {
                    this.KeyDown -= LocationProductAttributeMaintenanceOrganiser_KeyDown;

                    IDisposable disposableViewModel = (IDisposable)this.ViewModel;

                    //this.unitsDisplay.RootUnitView = null;
                    this.ViewModel = null;

                    if (disposableViewModel != null)
                    {
                        disposableViewModel.Dispose();
                    }

                }), DispatcherPriority.Background);
            }

        }


        #endregion
    }

    /// <summary>
    /// Selects the template to be used against custom attribute item depending on its data type
    /// </summary>
    public class CustomAttributeDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;

            //if (element != null && item != null)
            //{
            //    if (item is LocationProductAttributeMaintenanceCustomAttribute)
            //    {
            //        LocationProductAttributeMaintenanceCustomAttribute attribute = item as LocationProductAttributeMaintenanceCustomAttribute;

            //        switch (attribute.AttributeDataType)
            //        {
            //            case LocationProductCustomAttributeDataType.Boolean:
            //                return element.FindResource("LocationProductAttributeMaintenance_DTCustomAttributeBoolean") as DataTemplate;
            //            case LocationProductCustomAttributeDataType.Currency:
            //                return element.FindResource("LocationProductAttributeMaintenance_DTCustomAttributeCurrency") as DataTemplate;
            //            case LocationProductCustomAttributeDataType.Decimal:
            //                return element.FindResource("LocationProductAttributeMaintenance_DTCustomAttributeDecimal") as DataTemplate;
            //            case LocationProductCustomAttributeDataType.Integer:
            //                return element.FindResource("LocationProductAttributeMaintenance_DTCustomAttributeInteger") as DataTemplate;
            //            case LocationProductCustomAttributeDataType.Text:
            //                return element.FindResource("LocationProductAttributeMaintenance_DTCustomAttributeText") as DataTemplate;
            //        }
            //    }
            //}

            return null;
        }
    }
}
