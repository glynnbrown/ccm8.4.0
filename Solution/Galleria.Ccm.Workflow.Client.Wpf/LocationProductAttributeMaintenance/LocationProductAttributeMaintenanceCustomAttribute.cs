﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using System.Reflection;
using Galleria.Ccm.Model;
using System.Windows;
using Galleria.Framework.Helpers;
using System.Globalization;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance
{
    /// <summary>
    /// Holds details for the location product cusstom attribute
    /// </summary>
    public class LocationProductAttributeMaintenanceCustomAttribute : ViewModelObject
    {
        //#region Fields
        //private PropertyInfo _attributePropertyInfo;
        //private LocationProductAttribute _boundAttributeData;
        //private LocationProductCustomAttribute _sourceAttribute;
        //#endregion

        //#region Binding Property Paths

        //public static readonly PropertyPath AttributeNameProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceCustomAttribute>(p => p.AttributeName);
        //public static readonly PropertyPath AttributeDataTypeProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceCustomAttribute>(p => p.AttributeDataType);
        //public static readonly PropertyPath LocationToProductCustomAttributeValueProperty = WpfHelper.GetPropertyPath<LocationProductAttributeMaintenanceCustomAttribute>(p => p.LocationToProductCustomAttributeValue);

        //#endregion

        //#region Properties

        ///// <summary>
        ///// Gets the name of the attribute
        ///// </summary>
        //public String AttributeName
        //{
        //    get { return _sourceAttribute.Name; }
        //}

        ///// <summary>
        ///// Gets the type of the attribute
        ///// </summary>
        //public LocationProductCustomAttributeDataType AttributeDataType
        //{
        //    get { return _sourceAttribute.DataType; }
        //}

        ///// <summary>
        ///// Gets the attribute value based on the source attribute column number
        ///// </summary>
        //public Object LocationToProductCustomAttributeValue
        //{
        //    get
        //    {
        //        if (_boundAttributeData != null)
        //        {
        //            return _attributePropertyInfo.GetValue(_boundAttributeData, null);
        //        }
        //        else { return null; }

        //    }
        //    set
        //    {
        //        //the setter needs to convert to value to bytes
        //        Object setValue = value;
        //        _attributePropertyInfo.SetValue(_boundAttributeData, setValue, null);
        //    }
        //}

        //#endregion


        //#region Constructor

        ///// <summary>
        ///// constructor
        ///// </summary>
        ///// <param name="attribute"></param>
        ///// <param name="boundProduct"></param>
        //public LocationProductAttributeMaintenanceCustomAttribute(LocationProductCustomAttribute attribute, LocationProductAttribute boundLocationProduct)
        //{
        //    _boundAttributeData = boundLocationProduct;
        //    _sourceAttribute = attribute;

        //    _sourceAttribute.PropertyChanged += Attribute_PropertyChanged;
        //    _boundAttributeData.PropertyChanged += BoundAttributeData_PropertyChanged;

        //    UpdateAttributePropertyInfo();
        //}

        ///// <summary>
        ///// constructor used when no bound attribute data is to be provided
        ///// </summary>
        ///// <param name="attribute"></param>
        //public LocationProductAttributeMaintenanceCustomAttribute(LocationProductCustomAttribute attribute)
        //{
        //    _sourceAttribute = attribute;
        //    _sourceAttribute.PropertyChanged += Attribute_PropertyChanged;

        //    UpdateAttributePropertyInfo();
        //}

        //#endregion

        //#region Event Handlers

        ///// <summary>
        ///// Responds to property changes on the source attribute
        ///// </summary>
        //private void Attribute_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == ProductAttribute.NameProperty.Name)
        //    {
        //        OnPropertyChanged(AttributeNameProperty);
        //    }
        //    else if (e.PropertyName == ProductAttribute.DataTypeProperty.Name)
        //    {
        //        OnPropertyChanged(AttributeDataTypeProperty);
        //    }
        //    else if (e.PropertyName == ProductAttribute.ColumnNumberProperty.Name)
        //    {
        //        UpdateAttributePropertyInfo();

        //    }

        //}

        ///// <summary>
        ///// Responds to property changes on the bound attribute data
        ///// </summary>
        //private void BoundAttributeData_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    if (_attributePropertyInfo != null)
        //    {
        //        if (e.PropertyName == _attributePropertyInfo.Name)
        //        {
        //            OnPropertyChanged(LocationToProductCustomAttributeValueProperty);
        //        }
        //    }
        //}


        //#endregion

        //#region Methods

        //private void UpdateAttributePropertyInfo()
        //{
        //    String colNumber = (_sourceAttribute.ColumnNumber < 10) ?
        //           String.Format(CultureInfo.InvariantCulture, "0{0}", _sourceAttribute.ColumnNumber) : _sourceAttribute.ColumnNumber.ToString(CultureInfo.InvariantCulture);
        //    _attributePropertyInfo = typeof(LocationProductAttribute).GetProperty(String.Format(CultureInfo.InvariantCulture, "CustomAttribute{0}", colNumber));
        //    OnPropertyChanged(LocationToProductCustomAttributeValueProperty);
        //}

        //#endregion

        //#region IDisposableMembers

        protected override void Dispose(bool disposing)
        {
        //    if (!base.IsDisposed)
        //    {
        //        if (disposing)
        //        {
        //            _sourceAttribute.PropertyChanged -= Attribute_PropertyChanged;
        //            if (_boundAttributeData != null)
        //            {
        //                _boundAttributeData.PropertyChanged -= BoundAttributeData_PropertyChanged;
        //            }

        //            _sourceAttribute = null;
        //            _boundAttributeData = null;
        //            _attributePropertyInfo = null;
        //        }
        //        base.IsDisposed = true;
        //    }
        }

        //#endregion
    }
}
