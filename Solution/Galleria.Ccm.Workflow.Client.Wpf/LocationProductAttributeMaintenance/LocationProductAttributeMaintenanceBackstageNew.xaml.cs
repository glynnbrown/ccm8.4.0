﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.LocationProductAttributeMaintenance
{
    /// <summary>
    /// Interaction logic for LocationProductAttributeMaintenanceBackstageNew.xaml
    /// </summary>
    public partial class LocationProductAttributeMaintenanceBackstageNew : UserControl
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(LocationProductAttributeMaintenanceViewModel),
            typeof(LocationProductAttributeMaintenanceBackstageNew),
            new PropertyMetadata(null));

        public LocationProductAttributeMaintenanceViewModel ViewModel
        {
            get { return (LocationProductAttributeMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public LocationProductAttributeMaintenanceBackstageNew()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Calls for a refilter on text changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void productSearchInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionView view = productSearchResults.ItemsSource as CollectionView;
            if (view != null)
            {
                view.Refresh();
            }
        }

        /// <summary>
        /// Filters the sent item against the product criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProductSearch_Filter(object sender, System.Windows.Data.FilterEventArgs e)
        {
            ProductInfo info = (ProductInfo)e.Item;
            e.Accepted = info.ToString().ToLowerInvariant().Contains(productSearchInput.Text.ToLowerInvariant());
        }

        /// <summary>
        /// Calls for a refresh on the location filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void locationSearchInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionView view = locationSearchResults.ItemsSource as CollectionView;
            if (view != null)
            {
                view.Refresh();
            }
        }

        /// <summary>
        /// Filters the sent item against the location criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LocationSearch_Filter(object sender, System.Windows.Data.FilterEventArgs e)
        {
            LocationInfo info = (LocationInfo)e.Item;
            e.Accepted = info.ToString().ToLowerInvariant().Contains(locationSearchInput.Text.ToLowerInvariant());
        }

        /// <summary>
        /// Handles the click of the open button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenButton_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.OpenCommand.Execute(
                new Tuple<Int16?, Int32?>(
                    ((LocationInfo)locationSearchResults.SelectedItem).Id,
                ((ProductInfo)productSearchResults.SelectedItem).Id));
        }

        private void LocationSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && locationSearchResults.SelectedItem != null)
            {
                productSearchInput.Focus();
            }
        }

        private void ProductSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var location = (locationSearchResults.SelectedItem as LocationInfo);
                var product = (productSearchResults.SelectedItem as ProductInfo);
                if (location != null && product != null)
                {
                   var locationProductIDSet = new Tuple<Int16?, Int32?>(location.Id, product.Id);
                   if(this.ViewModel.OpenCommand.CanExecute(locationProductIDSet))
                         this.ViewModel.OpenCommand.Execute(locationProductIDSet);
                }
            }
        }
        #endregion

      
    }
}
