﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.Model;
using Galleria.Reporting.Controls.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for ValidateTemplateMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class ValidationTemplateMaintenanceBackstageOpen
    {
        #region Fields
        
        private readonly ObservableCollection<ValidationTemplateInfo> _searchResults = new ObservableCollection<ValidationTemplateInfo>();
        
        #endregion
        
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = 
            DependencyProperty.Register("ViewModel", typeof (ValidationTemplateMaintenanceViewModel), typeof (ValidationTemplateMaintenanceBackstageOpen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ValidationTemplateMaintenanceBackstageOpen senderControl = (ValidationTemplateMaintenanceBackstageOpen)obj;

            if (e.OldValue != null)
            {
                ValidationTemplateMaintenanceViewModel oldModel = (ValidationTemplateMaintenanceViewModel)e.OldValue;
                oldModel.AvailableValidationTemplates.BulkCollectionChanged -= senderControl.ViewModel_AvailableValidationTemplatesBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                ValidationTemplateMaintenanceViewModel newModel = (ValidationTemplateMaintenanceViewModel)e.NewValue;
                newModel.AvailableValidationTemplates.BulkCollectionChanged += senderControl.ViewModel_AvailableValidationTemplatesBulkCollectionChanged;
            }
            senderControl.UpdateSearchResults();
        }

        public ValidationTemplateMaintenanceViewModel ViewModel
        {
            get { return (ValidationTemplateMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SearchResults Property

        public static readonly DependencyProperty SearchResultsProperty = 
            DependencyProperty.Register("SearchResults", typeof (ReadOnlyObservableCollection<ValidationTemplateInfo>),
            typeof (ValidationTemplateMaintenanceBackstageOpen),  
            new PropertyMetadata(default(ReadOnlyObservableCollection<ValidationTemplateInfo>)));

        public ReadOnlyObservableCollection<ValidationTemplateInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<ValidationTemplateInfo>) GetValue(SearchResultsProperty); }
            set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #region SearchText Property

        public static readonly DependencyProperty SearchTextProperty = 
            DependencyProperty.Register("SearchText", typeof (String), typeof (ValidationTemplateMaintenanceBackstageOpen), 
            new PropertyMetadata(String.Empty, OnSearchTextPropertyChanged));

        private static void OnSearchTextPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((ValidationTemplateMaintenanceBackstageOpen)obj).UpdateSearchResults();
        }

        /// <summary>
        /// Gets/Sets the criteria to search items for
        /// </summary>
        public String SearchText
        {
            get { return (String) GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ValidationTemplateMaintenanceBackstageOpen()
        {
            this.SearchResults = new ReadOnlyObservableCollection<ValidationTemplateInfo>(_searchResults);

            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked when the collection of available validation templates changes.
        /// </summary>
        private void ViewModel_AvailableValidationTemplatesBulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            UpdateSearchResults();
        }


        /// <summary>
        ///     Invoked when the user double clicks on the search list.
        /// </summary>
        private void XSearchResults_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var senderControl = sender as ListBox;
            if (senderControl == null) return;

            var item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
            if (item == null) return;

            var target = senderControl.SelectedItem as ValidationTemplateInfo;
            if (target == null) return;

            ViewModel.OpenCommand.Execute(target.Id);
        }

        #endregion

        #region Methods

        private void UpdateSearchResults()
        {
            _searchResults.Clear();

            if (ViewModel == null) return;

            String searchPattern = LocalHelper.GetRexexKeywordPattern(this.SearchText);

            var results =
                this.ViewModel.AvailableValidationTemplates
                .Where(w => Regex.IsMatch(w.Name, searchPattern, RegexOptions.IgnoreCase))
                .OrderBy(l => l.ToString());

            foreach (var info in results.OrderBy(l => l.ToString()))
            {
                _searchResults.Add(info);
            }

        }

        #endregion

        private void XSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                var senderControl = sender as ListBox;
                if (senderControl == null) return;

                var item = ((DependencyObject)e.OriginalSource).FindVisualAncestor<ListBoxItem>();
                if (item == null) return;

                var target = senderControl.SelectedItem as ValidationTemplateInfo;
                if (target == null) return;

                ViewModel.OpenCommand.Execute(target.Id);

                e.Handled = true;
            }
        }
    }
}
