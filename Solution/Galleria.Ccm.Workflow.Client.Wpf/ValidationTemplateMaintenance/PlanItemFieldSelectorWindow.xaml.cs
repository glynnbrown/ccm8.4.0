﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#endregion

using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance
{
    /// <summary>
    ///     Interaction logic for PlanItemFieldSelectorWindow.xaml
    /// </summary>
    public partial class PlanItemFieldSelectorWindow
    {
        #region Properties

        #region ViewModel

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(
            "ViewModel", typeof (PlanItemFieldSelectorViewModel), typeof (PlanItemFieldSelectorWindow),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        public PlanItemFieldSelectorViewModel ViewModel
        {
            get { return (PlanItemFieldSelectorViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void ViewModel_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as PlanItemFieldSelectorWindow;
            if (senderControl == null) return;

            var oldModel = e.OldValue as PlanItemFieldSelectorViewModel;
            if (oldModel != null)
            {
                oldModel.AttachedControl = null;
                oldModel.PlanItemFields.BulkCollectionChanged -=
                    senderControl.ViewModelPlanItemFields_BulkCollectionChanged;
            }

            var newModel = e.NewValue as PlanItemFieldSelectorViewModel;
            if (newModel == null) return;

            newModel.AttachedControl = senderControl;
            newModel.PlanItemFields.BulkCollectionChanged +=
                senderControl.ViewModelPlanItemFields_BulkCollectionChanged;
        }

        private void ViewModelPlanItemFields_BulkCollectionChanged(object sender, BulkCollectionChangedEventArgs e)
        {
            ReapplyFilter();
        }

        #endregion

        #endregion

        #region Constructor

        public PlanItemFieldSelectorWindow(PlanItemFieldSelectorViewModel viewModel)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            ViewModel = viewModel;
            Loaded += PlanItemFieldSelectorWindow_Loaded;
        }

        #endregion

        #region Event Handlers

        private void XPlanItemFieldGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            var planItemField = e.RowItem as PlanItemField;
            if (planItemField == null) return;

            ViewModel.AddField(planItemField, xFieldTextBox.CaretIndex);
            ViewModel.OkCommand.Execute(null);
        }

        private void PlanItemFieldSelectorWindow_Loaded(object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= PlanItemFieldSelectorWindow_Loaded;
            Dispatcher.BeginInvoke((Action)(() => Mouse.OverrideCursor = null));
        }

        #endregion

        #region Methods

        private void ReapplyFilter()
        {
            var text = xFieldTextBox.Text;
            if (String.IsNullOrEmpty(text)) return;

            xFieldTextBox.Text = null;
            xFieldTextBox.Text = text;
        }

        #endregion

        #region Window Close

        /// <summary>
        ///     Raises the <see cref="E:System.Windows.Window.Closed" /> event.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs" /> that contains the event data.</param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke((Action) (() =>
            {
                if (ViewModel == null) return;

                var disposable = ViewModel as IDisposable;
                if (disposable == null) return;

                disposable.Dispose();
            }), DispatcherPriority.Background);
        }

        #endregion
    }
}