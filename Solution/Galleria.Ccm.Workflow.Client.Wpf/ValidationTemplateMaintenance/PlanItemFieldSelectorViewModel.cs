﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Some refactoring and localization.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Aspose.Pdf;
using Csla.Core;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance
{
    /// <summary>
    ///     Implements the ViewModel for a <see cref="PlanItemFieldSelectorWindow" /> View.
    /// </summary>
    public sealed class PlanItemFieldSelectorViewModel :
        ViewModelAttachedControlObject<PlanItemFieldSelectorWindow>, IDataErrorInfo
    {
        #region Constants

        /// <summary>
        ///     The string format mask to use when adding a new field to the field text. Similar to " {0} ".
        /// </summary>
        private const string NewFieldMask = " {0} ";

        #endregion

        #region Fields

        /// <summary>
        ///     Observable collection of <see cref="PlanItemField" /> items, filtered by <see cref="SelectedPlanItemTypeView" />.
        /// </summary>
        private readonly BulkObservableCollection<PlanItemField> _planItemFields =
            new BulkObservableCollection<PlanItemField>();

        /// <summary>
        ///     The collection of <see cref="PlanItemTypeView" /> items to filter the plan item fields by.
        /// </summary>
        private readonly ReadOnlyCollection<IPlanItemTypeView> _planItemTypeViews;

        /// <summary>
        ///     The read only observable collection of available <see cref="PlanItemField" /> items to display on the UI.
        /// </summary>
        private ReadOnlyBulkObservableCollection<PlanItemField> _availablePlanItemFieldsRo;

        /// <summary>
        ///     The result for this dialog (either <c>true</c> if the user accepts the selection, or <c>false</c> if not.
        /// </summary>
        private bool? _dialogResult;

        /// <summary>
        ///     The current expression to use as the field or formula.
        /// </summary>
        private String _fieldText;

        /// <summary>
        ///     The registered error, if any, after validating the field text expression.
        /// </summary>
        private String _fieldTextError;

        /// <summary>
        ///     Currently selected <see cref="PlanItemField" />.
        /// </summary>
        private PlanItemField _selectedPlanItemField;

        /// <summary>
        ///     Currently selected <see cref="PlanItemTypeView" />.
        /// </summary>
        private IPlanItemTypeView _selectedPlanItemTypeView;

        #endregion

        #region Binding Property Paths

        /// <summary>
        ///     <see cref="PropertyPath" /> for the <see cref="FieldText" /> property.
        /// </summary>
        public static readonly PropertyPath FieldTextProperty = GetPropertyPath(o => o.FieldText);

        /// <summary>
        ///     <see cref="PropertyPath" /> for the <see cref="PlanItemFields" /> property.
        /// </summary>
        public static readonly PropertyPath PlanItemFieldsProperty =
            GetPropertyPath(o => o.PlanItemFields);

        /// <summary>
        ///     <see cref="PropertyPath" /> for the <see cref="PlanItemTypeViews" /> property.
        /// </summary>
        public static readonly PropertyPath PlanItemTypeViewsProperty =
            GetPropertyPath(o => o.PlanItemTypeViews);

        /// <summary>
        ///     <see cref="PropertyPath" /> for the <see cref="SelectedPlanItemField" /> property.
        /// </summary>
        public static readonly PropertyPath SelectedPlanItemFieldProperty = GetPropertyPath(o => o.SelectedPlanItemField);

        /// <summary>
        ///     <see cref="PropertyPath" /> for the <see cref="SelectedPlanItemTypeView" /> property.
        /// </summary>
        public static readonly PropertyPath SelectedPlanItemTypeViewProperty =
            GetPropertyPath(o => o.SelectedPlanItemTypeView);

        #region Commands

        /// <summary>
        ///     <see cref="PropertyPath" /> for the <see cref="CancelCommand" />.
        /// </summary>
        public static readonly PropertyPath CancelCommandProperty = GetPropertyPath(o => o.CancelCommand);

        /// <summary>
        ///     <see cref="PropertyPath" /> for the <see cref="ClearCommand" />.
        /// </summary>
        public static readonly PropertyPath ClearCommandProperty = GetPropertyPath(o => o.ClearCommand);

        /// <summary>
        ///     <see cref="PropertyPath" /> for the <see cref="OkCommand" />.
        /// </summary>
        public static readonly PropertyPath OkCommandProperty = GetPropertyPath(o => o.OkCommand);

        #endregion

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the result for this dialog.
        /// </summary>
        private Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (AttachedControl != null) AttachedControl.DialogResult = value;
            }
        }

        /// <summary>
        ///     Gets or sets the expression to use as field or formula.
        /// </summary>
        public String FieldText
        {
            get { return _fieldText; }
            set
            {
                _fieldText = value;
                OnPropertyChanged(FieldTextProperty);

                OnFieldTextChanged();
            }
        }

        /// <summary>
        ///     Gets the available <see cref="PlanItemField" /> items filtered by the <see cref="SelectedPlanItemTypeView" />.
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanItemField> PlanItemFields
        {
            get
            {
                return _availablePlanItemFieldsRo
                       ??
                       (_availablePlanItemFieldsRo =
                           new ReadOnlyBulkObservableCollection<PlanItemField>(_planItemFields));
            }
        }

        /// <summary>
        ///     Gets the available <see cref="PlanItemTypeView" /> items.
        /// </summary>
        public ReadOnlyCollection<IPlanItemTypeView> PlanItemTypeViews
        {
            get { return _planItemTypeViews; }
        }

        /// <summary>
        ///     Gets or sets the currently selected <see cref="PlanItemField" />.
        /// </summary>
        public PlanItemField SelectedPlanItemField
        {
            get { return _selectedPlanItemField; }
            set
            {
                _selectedPlanItemField = value;
                OnPropertyChanged(SelectedPlanItemFieldProperty);
            }
        }

        /// <summary>
        ///     Gets or sets the currently selected <see cref="PlanItemTypeView" />.
        /// </summary>
        public IPlanItemTypeView SelectedPlanItemTypeView
        {
            get { return _selectedPlanItemTypeView; }
            set
            {
                _selectedPlanItemTypeView = value;
                OnPropertyChanged(SelectedPlanItemTypeViewProperty);

                OnSelectedPlanItemTypeChanged();
            }
        }

        private void OnSelectedPlanItemTypeChanged()
        {
            _planItemFields.Clear();
            _planItemFields.AddRange(SelectedPlanItemTypeView.EnumeratePlanItemFields(ExistingPlanItemFields).OrderBy(field => field.DisplayName));
        }

        private IEnumerable<String> ExistingPlanItemFields { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of the <see cref="PlanItemFieldSelectorViewModel" /> class.
        /// </summary>
        /// <param name="fieldText">Initial value of the field text.</param>
        /// <param name="existingPlanItemFields"></param>
        public PlanItemFieldSelectorViewModel(string fieldText, IEnumerable<string> existingPlanItemFields)
        {
            Error = String.Empty;
            _planItemTypeViews = new ReadOnlyCollection<IPlanItemTypeView>(new IPlanItemTypeView[]
            {
                PlanItemTypeView.AllTypes,
                new PlanItemTypeView(PlanItemType.Planogram),
                new PlanItemTypeView(PlanItemType.Product),
                new PlanItemTypeView(PlanItemType.Position),
                new PlanItemTypeView(PlanItemType.SubComponent),
                new PlanItemTypeView(PlanItemType.Component),
                new PlanItemTypeView(PlanItemType.Fixture)
            });

            ExistingPlanItemFields = existingPlanItemFields;

            SelectedPlanItemTypeView = _planItemTypeViews.First();

            FieldText = fieldText;
        }

        public PlanItemFieldSelectorViewModel(string fieldText) : this(fieldText,null)
        {
            
        }
        #endregion

        #region Commands

        #region Cancel

        private RelayCommand _cancelCommand;

        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand != null) return _cancelCommand;

                _cancelCommand = new RelayCommand(o => Cancel_Executed())
                {
                    FriendlyName = Message.Generic_Cancel
                };
                ViewModelCommands.Add(_cancelCommand);
                return _cancelCommand;
            }
        }

        private void Cancel_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #region Clear

        private RelayCommand _clearCommand;

        public RelayCommand ClearCommand
        {
            get
            {
                if (_clearCommand != null) return _clearCommand;

                _clearCommand = new RelayCommand(o => Clear_Executed())
                {
                    FriendlyName = "Clear",
                    SmallIcon = ImageResources.TODO
                };
                ViewModelCommands.Add(_clearCommand);
                return _clearCommand;
            }
        }

        private void Clear_Executed()
        {
            FieldText = null;
        }

        #endregion

        #region Ok

        private RelayCommand _okCommand;

        public RelayCommand OkCommand
        {
            get
            {
                if (_okCommand != null) return _okCommand;

                _okCommand = new RelayCommand(o => Ok_Executed(), o => Ok_CanExecute())
                {
                    FriendlyName = Message.Generic_Ok,
                    InputGestureKey = Key.Enter
                };
                ViewModelCommands.Add(_okCommand);
                return _okCommand;
            }
        }

        private Boolean Ok_CanExecute()
        {
            return (!String.IsNullOrEmpty(FieldText) || SelectedPlanItemField != null)
                   && String.IsNullOrEmpty(_fieldTextError);
        }

        private void Ok_Executed()
        {
            FieldText = SelectedPlanItemField.FieldText;
            DialogResult = true;
        }

        #endregion

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked when the <see cref="FieldText" /> value changes.
        /// </summary>
        private void OnFieldTextChanged()
        {
            ValidateFieldTextValue();
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Validates the text entered for the field.
        /// </summary>
        private void ValidateFieldTextValue()
        {
            //TODO Implement validation of Field text.
            _fieldTextError = String.Empty;
        }

        #endregion

        #region IDataErrorInfo Members

        /// <summary>
        ///     Gets an error message indicating what is wrong with this object.
        /// </summary>
        /// <returns>
        ///     An error message indicating what is wrong with this object. The default is an empty string ("").
        /// </returns>
        public String Error { get; private set; }

        /// <summary>
        ///     Gets the error message for the property with the given name.
        /// </summary>
        /// <returns>
        ///     The error message for the property. The default is an empty string ("").
        /// </returns>
        /// <param name="columnName">The name of the property whose error message to get. </param>
        public String this[String columnName]
        {
            get
            {
                String error = null;

                if (columnName == FieldTextProperty.Path)
                {
                    error = _fieldTextError;
                }

                return error;
            }
        }

        #endregion

        public void AddField(PlanItemField field, Int32 caretIndex)
        {
            var sb = new StringBuilder(FieldText);
            var fieldText = String.Format(NewFieldMask, field.FieldText);
            sb.Insert(caretIndex, fieldText);
            if (AttachedControl != null) AttachedControl.xFieldTextBox.CaretIndex += fieldText.Length;
        }

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                // Dispose resources here.
            }

            IsDisposed = true;
        }

        #region Nested Class: PlanItemTypeView

        #region Nested type: IPlanItemTypeView

        public interface IPlanItemTypeView
        {
            /// <summary>
            ///     Gets or sets the text to display as human readable name for the type in this instance.
            /// </summary>
            String DisplayName { get; }

            /// <summary>
            ///     Gets or sets the value for the type in this instance.
            /// </summary>
            PlanItemType? Value { get; }

            IEnumerable<PlanItemField> EnumeratePlanItemFields();

            IEnumerable<PlanItemField> EnumeratePlanItemFields(IEnumerable<String> exceptionItemFields);
        }

        #endregion

        #region Nested type: PlanItemTypeView

        /// <summary>
        ///     View to display <see cref="PlanItemType" /> values.
        /// </summary>
        public sealed class PlanItemTypeView : IPlanItemTypeView
        {
            #region Properties

            /// <summary>
            ///     Gets or sets the text to display as human readable name for the type in this instance.
            /// </summary>
            public String DisplayName { get; private set; }

            /// <summary>
            ///     Gets or sets the value for the type in this instance.
            /// </summary>
            public PlanItemType? Value { get; private set; }

            #endregion

            #region Constructor

            /// <summary>
            ///     Initializes a new instance of <see cref="PlanItemTypeView" />.
            /// </summary>
            /// <remarks>Used only internally.</remarks>
            private PlanItemTypeView()
            {
            }

            /// <summary>
            ///     Initializes a new instance of <see cref="PlanItemTypeView" /> using the provided <see cref="PlanItemType" /> value
            ///     to configure the view.
            /// </summary>
            /// <param name="type">The <see cref="PlanItemType" /> value to be represented by this instance.</param>
            public PlanItemTypeView(PlanItemType type)
            {
                DisplayName = PlanItemTypeHelper.FriendlyNames[type];
                Value = type;
            }

            #endregion

            #region Factory Methods

            /// <summary>
            ///     Gets a new instance of <see cref="PlanItemTypeView" /> to represent all possible values at once.
            /// </summary>
            public static PlanItemTypeView AllTypes
            {
                get
                {
                    return new PlanItemTypeView
                    {
                        DisplayName = Message.PlanItemFieldSelector_AllPlanItemTypes,
                        Value = null
                    };
                }
            }

            #endregion

            #region Overrides

            /// <summary>
            ///     Returns a <see cref="T:System.String" /> that represents the current <see cref="PlanItemTypeView" />.
            /// </summary>
            /// <returns>
            ///     A <see cref="T:System.String" /> that represents the current <see cref="PlanItemTypeView" />.
            /// </returns>
            public override string ToString()
            {
                return DisplayName;
            }

            #endregion

            #region IPlanItemTypeView Members

            public IEnumerable<PlanItemField> EnumeratePlanItemFields()
            {
                return EnumeratePlanItemFields(null);
            }

            public IEnumerable<PlanItemField> EnumeratePlanItemFields(IEnumerable<String> exceptionItemFields)
            {
                if (exceptionItemFields == null) exceptionItemFields = new List<String>();
                if (Value == null || Value == PlanItemType.Planogram)
                {
                    foreach (
                        var planItemField in
                            PlanItemField.EnumeratePlanogramPlanItemFields()
                                .Where(
                                    planItemField => exceptionItemFields.All(field => field != planItemField.FieldText))
                        )
                        yield return planItemField;
                }
                if (Value == null || Value == PlanItemType.Product)
                {
                    foreach (var planItemField in PlanItemField.EnumeratePlanogramProductPlanItemFields().Where(planItemField => exceptionItemFields.All(field => field != planItemField.FieldText)))
                        yield return planItemField;
                }
                if (Value == null || Value == PlanItemType.Position)
                {
                    foreach (var planItemField in PlanItemField.EnumeratePlanogramPositionPlanItemFields().Where(planItemField => exceptionItemFields.All(field => field != planItemField.FieldText)))
                        yield return planItemField;
                }
                if (Value == null || Value == PlanItemType.SubComponent)
                {
                    foreach (var planItemField in PlanItemField.EnumeratePlanogramSubComponentPlanItemFields().Where(planItemField => exceptionItemFields.All(field => field != planItemField.FieldText)))
                        yield return planItemField;
                }
                if (Value == null || Value == PlanItemType.Component)
                {
                    foreach (var planItemField in PlanItemField.EnumeratePlanogramComponentPlanItemFields().Where(planItemField => exceptionItemFields.All(field => field != planItemField.FieldText)))
                        yield return planItemField;
                }
                if (Value == null || Value == PlanItemType.Fixture)
                {
                    foreach (var planItemField in PlanItemField.EnumeratePlanogramFixturePlanItemFields().Where(planItemField => exceptionItemFields.All(field => field != planItemField.FieldText)))
                        yield return planItemField;
                }
            }

            #endregion
        }

        #endregion

        #endregion

        #region Static Private Helpers

        private static PropertyPath GetPropertyPath(
            Expression<Func<PlanItemFieldSelectorViewModel, Object>> expression)
        {
            return WpfHelper.GetPropertyPath(expression);
        }

        #endregion
    }

    public enum PlanItemType
    {
        Fixture,
        Assembly,
        Component,
        SubComponent,
        Position,
        Annotation,
        Product,
        Planogram
    }

    public static class PlanItemTypeHelper
    {
        public static readonly Dictionary<PlanItemType, String> FriendlyNames = new Dictionary<PlanItemType, String>
        {
            {PlanItemType.Fixture, Message.Generic_Fixture},
            {PlanItemType.Assembly, Message.Generic_Assembly},
            {PlanItemType.Component, Message.Generic_Component},
            {PlanItemType.SubComponent, Message.Generic_SubComponent},
            {PlanItemType.Position, Message.Generic_Position},
            {PlanItemType.Product, Message.Generic_Product},
            {PlanItemType.Annotation, Message.Generic_Annotation},
            {PlanItemType.Planogram, Message.Generic_Planogram}
        };
    }

    public class PlanItemField
    {
        private const string DisplayNameFormat = "{0} {1}";
        private const string FieldFormat = "<{0}>";
        private const string PathFormat = "{0}.{1}";

        private PlanItemField(PlanItemType itemType, IPropertyInfo propertyInfo)
            : this(itemType, propertyInfo.Name, propertyInfo.FriendlyName, propertyInfo.Type)
        {
        }

        private PlanItemField(PlanItemType itemType, String propertyName, String friendlyName, Type propertyType)
        {
            ItemType = itemType;
            PropertyName = propertyName;
            FriendlyName = friendlyName;
            PropertyType = propertyType;
        }

        public String DisplayName
        {
            get { return String.Format(DisplayNameFormat, PlanItemTypeHelper.FriendlyNames[ItemType], FriendlyName); }
        }

        public string FieldText
        {
            get { return String.Format(FieldFormat, Path); }
        }

        private String FriendlyName { get; set; }
        private PlanItemType ItemType { get; set; }

        private String Path
        {
            get { return String.Format(PathFormat, ItemType, PropertyName); }
        }

        private String PropertyName { get; set; }
        private Type PropertyType { get; set; }

        #region Static Helper Methods

        public static String ConvertFieldToDisplay(String field)
        {
            var displayText = field;

            return !String.IsNullOrEmpty(displayText)
                ? EnumeratePlanItemFields(null)
                    .Aggregate(displayText,
                        (current, itemField) => current.Replace(itemField.FieldText, itemField.DisplayName))
                : displayText;
        }

        public static Boolean IsAggregable(String field)
        {
            return !String.IsNullOrEmpty(field)
                   && EnumeratePlanItemFields(PlanItemType.Planogram)
                       .All(itemField => !itemField.FieldText.Equals(field));
        }

        internal static IEnumerable<PlanItemField> EnumeratePlanogramComponentPlanItemFields()
        {
            const PlanItemType planItemType = PlanItemType.Component;

            yield return new PlanItemField(planItemType, PlanogramComponent.HeightProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.WidthProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.DepthProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.SupplierCostPriceProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.SupplierDiscountProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.SupplierLeadTimeProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.MinPurchaseQtyProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.WeightLimitProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.WeightProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.VolumeProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.DiameterProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.CapacityProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.MetaNumberOfSubComponentsProperty);
            yield return
                new PlanItemField(planItemType, PlanogramComponent.MetaNumberOfMerchandisedSubComponentsProperty);
            yield return new PlanItemField(planItemType, PlanogramComponent.MetaNumberOfShelfEdgeLabelsProperty);
        }

        internal static IEnumerable<PlanItemField> EnumeratePlanogramFixturePlanItemFields()
        {
            const PlanItemType planItemType = PlanItemType.Fixture;

            yield return new PlanItemField(planItemType, PlanogramFixture.HeightProperty);
            yield return new PlanItemField(planItemType, PlanogramFixture.WidthProperty);
            yield return new PlanItemField(planItemType, PlanogramFixture.DepthProperty);
            yield return new PlanItemField(planItemType, PlanogramFixture.NumberOfAssembliesProperty);
            yield return new PlanItemField(planItemType, PlanogramFixture.NumberOfMerchandisedSubComponentsProperty);
            yield return new PlanItemField(planItemType, PlanogramFixture.TotalFixtureCostProperty);
            yield return new PlanItemField(planItemType, PlanogramFixture.UniqueProductCountProperty);
        }

        internal static IEnumerable<PlanItemField> EnumeratePlanogramPlanItemFields()
        {
            const PlanItemType planItemType = PlanItemType.Planogram;

            yield return new PlanItemField(planItemType, Planogram.HeightProperty);
            yield return new PlanItemField(planItemType, Planogram.WidthProperty);
            yield return new PlanItemField(planItemType, Planogram.DepthProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaBayCountProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaUniqueProductCountProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalPositionCountProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalMerchandisableLinearSpaceProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalMerchandisableAreaSpaceProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalMerchandisableVolumetricSpaceProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalLinearWhiteSpaceProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalAreaWhiteSpaceProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalVolumetricWhiteSpaceProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaProductsPlacedProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaNewProductsProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaChangesFromPreviousCountProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaChangeFromPreviousStarRatingProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaBlocksDroppedProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaNotAchievedInventoryProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalFacingsProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaAverageFacingsProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaTotalUnitsProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaMinDosProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaMaxDosProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaAverageDosProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaMaxCasesProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaMinCasesProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaAverageCasesProperty);
            yield return new PlanItemField(planItemType, Planogram.MetaSpaceToUnitsIndexProperty);
        }

        internal static IEnumerable<PlanItemField> EnumeratePlanogramPositionPlanItemFields()
        {
            const PlanItemType planItemType = PlanItemType.Position;

            yield return new PlanItemField(planItemType, PlanogramPosition.XProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.YProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.ZProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.SlopeProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.AngleProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.RollProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsHighProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsWideProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsXHighProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsXWideProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsXDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsYHighProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsYDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsZHighProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsZWideProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.FacingsZDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.SequenceProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.SequenceXProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.SequenceYProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.SequenceZProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.UnitsHighProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.UnitsWideProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.UnitsDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramPosition.TotalUnitsProperty);
        }

        internal static IEnumerable<PlanItemField> EnumeratePlanogramProductPlanItemFields()
        {
            const PlanItemType planItemType = PlanItemType.Product;

            yield return new PlanItemField(planItemType, PlanogramProduct.HeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.WidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.DepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.DisplayHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.DisplayWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.DisplayDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.AlternateHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.AlternateWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.AlternateDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.PointOfPurchaseHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.PointOfPurchaseWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.PointOfPurchaseDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.NumberOfPegHolesProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.PegXProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.PegX2Property);
            yield return new PlanItemField(planItemType, PlanogramProduct.PegX3Property);
            yield return new PlanItemField(planItemType, PlanogramProduct.PegYProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.PegY2Property);
            yield return new PlanItemField(planItemType, PlanogramProduct.PegY3Property);
            yield return new PlanItemField(planItemType, PlanogramProduct.PegProngOffsetProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.PegDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.SqueezeHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.SqueezeWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.SqueezeDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.NestingHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.NestingWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.NestingDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CasePackUnitsProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CaseHighProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CaseWideProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CaseDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CaseHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CaseWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CaseDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.MaxStackProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.MaxTopCapProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.MaxRightCapProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.MinDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.MaxDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.MaxNestingHighProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.MaxNestingDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.MaxTopCapProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayPackUnitsProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayHighProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayWideProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayDeepProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayThickHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayThickWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TrayThickDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.FrontOverhangProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.FingerSpaceAboveProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.FingerSpaceToTheSideProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.ShelfLifeProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.DeliveryFrequencyDaysProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.SellPriceProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.SellPackCountProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.RecommendedRetailPriceProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.ManufacturerRecommendedRetailPriceProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CostPriceProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.CaseCostProperty);
            yield return new PlanItemField(planItemType, PlanogramProduct.TaxRateProperty);
        }

        internal static IEnumerable<PlanItemField> EnumeratePlanogramSubComponentPlanItemFields()
        {
            const PlanItemType planItemType = PlanItemType.SubComponent;

            yield return new PlanItemField(planItemType, PlanogramSubComponent.HeightProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.WidthProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DepthProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.XProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.YProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.ZProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.SlopeProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.AngleProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.RollProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.TransparencyPercentFrontProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.TransparencyPercentBackProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.TransparencyPercentTopProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.TransparencyPercentLeftProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.TransparencyPercentRightProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.FaceThicknessBackProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.FaceThicknessBottomProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.FaceThicknessFrontProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.FaceThicknessLeftProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.FaceThicknessRightProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.FaceThicknessTopProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.RiserHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.RiserThicknessProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.RiserTransparencyPercentProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.NotchStartXProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.NotchStartYProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.NotchSpacingXProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.NotchSpacingYProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.NotchHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.NotchWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionHeightProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionWidthProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionDepthProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionStartXProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionStartYProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionStartZProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionSpacingXProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionSpacingYProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.DividerObstructionSpacingZProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow1StartXProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow1SpacingXProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow1StartYProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow1SpacingYProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow1HeightProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow1WidthProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow2StartXProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow2StartYProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow2SpacingXProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow2SpacingYProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow2HeightProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.MerchConstraintRow2WidthProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.LineThicknessProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.LeftOverhangProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.RightOverhangProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.FrontOverhangProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.BackOverhangProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.TopOverhangProperty);
            yield return new PlanItemField(planItemType, PlanogramSubComponent.BottomOverhangProperty);
        }

        private static IEnumerable<PlanItemField> EnumeratePlanItemFields(PlanItemType? type)
        {
            if (type == null || type == PlanItemType.Planogram)
            {
                foreach (var planItemField in EnumeratePlanogramPlanItemFields()) yield return planItemField;
            }
            if (type == null || type == PlanItemType.Product)
            {
                foreach (var planItemField in EnumeratePlanogramProductPlanItemFields()) yield return planItemField;
            }
            if (type == null || type == PlanItemType.Position)
            {
                foreach (var planItemField in EnumeratePlanogramPositionPlanItemFields()) yield return planItemField;
            }
            if (type == null || type == PlanItemType.SubComponent)
            {
                foreach (var planItemField in EnumeratePlanogramSubComponentPlanItemFields())
                    yield return planItemField;
            }
            if (type == null || type == PlanItemType.Component)
            {
                foreach (var planItemField in EnumeratePlanogramComponentPlanItemFields()) yield return planItemField;
            }
            if (type == null || type == PlanItemType.Fixture)
            {
                foreach (var planItemField in EnumeratePlanogramFixturePlanItemFields()) yield return planItemField;
            }
        }

        #endregion
    }
}