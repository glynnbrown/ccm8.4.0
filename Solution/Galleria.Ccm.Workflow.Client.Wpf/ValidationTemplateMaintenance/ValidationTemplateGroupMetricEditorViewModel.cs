﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26401 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Tidy and localization.
// V8-26721 : A.Silva ~ Refactored to use the Validation Template interfaces.

#endregion

#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance
{
    public class ValidationTemplateGroupMetricEditorViewModel : ViewModelAttachedControlObject<ValidationTemplateGroupMetricEditorWindow>
    {
        #region Fields

        private Boolean? _dialogResult;

        private IValidationTemplateGroupMetric _validationTemplateGroupMetric;

        private PlanogramValidationAggregationType _aggregationType;

        private String _title;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath ValidationTemplateGroupMetricProperty =
            WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.ValidationTemplateGroupMetric);

        public static readonly PropertyPath TitleProperty =
            WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.Title);

        public static readonly PropertyPath AggregationTypeProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.AggregationType);

        public static readonly PropertyPath OkCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.OkCommand);
        public static readonly PropertyPath CancelCommandProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.CancelCommand);

        public static readonly PropertyPath IsAggregableProperty = WpfHelper.GetPropertyPath<ValidationTemplateGroupMetricEditorViewModel>(o => o.IsAggregable);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the dialog result.
        /// </summary>
        public Boolean? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                if (AttachedControl != null) AttachedControl.DialogResult = value;
            }
        }

        public IValidationTemplateGroupMetric ValidationTemplateGroupMetric
        {
            get { return _validationTemplateGroupMetric; }
            set
            {
                _validationTemplateGroupMetric = value;
                OnPropertyChanged(ValidationTemplateGroupMetricProperty);
                OnPropertyChanged(IsAggregableProperty);
            }
        }

        public String Title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged(TitleProperty);
            }
        }

        public PlanogramValidationAggregationType AggregationType
        {
            get { return _aggregationType; }
            set
            {
                _aggregationType = value;
                OnPropertyChanged(AggregationTypeProperty);
            }
        }

        public Boolean IsAggregable
        {
            get { return PlanItemField.IsAggregable(ValidationTemplateGroupMetric.Field); }
        }
        #endregion

        #region Constructor

        public ValidationTemplateGroupMetricEditorViewModel(ValidationTemplateGroupMetricEditorWindow validationTemplateGroupMetricEditorWindow, String title, ValidationTemplateGroupMetric validationTemplateGroupMetric)
        {
            AttachedControl = validationTemplateGroupMetricEditorWindow;
            Title = title;
            ValidationTemplateGroupMetric = validationTemplateGroupMetric;
            AggregationType = ValidationTemplateGroupMetric.AggregationType;
        }

        #endregion

        #region Commands

        #region OkCommand

        private IRelayCommand _okCommand;

        /// <summary>
        ///     Accepts the changes and closes the window.
        /// </summary>
        public IRelayCommand OkCommand
        {
            get
            {
                if (_okCommand != null) return _okCommand;

                _okCommand = new RelayCommand(o => OkCommand_Executed(), o => OkCommand_CanExecute())
                {
                    FriendlyName = Message.Generic_Ok,
                    InputGestureKey = Key.Enter
                };
                ViewModelCommands.Add(_okCommand);

                return _okCommand;
            }
        }

        private Boolean OkCommand_CanExecute()
        {
            return ValidationTemplateGroupMetric.IsDirty;
        }

        private void OkCommand_Executed()
        {
            DialogResult = true;
        }

        #endregion

        #region CancelCommand

        private IRelayCommand _cancelCommand;

        /// <summary>
        ///     Cancels the change and closes the window.
        /// </summary>
        public IRelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand != null) return _cancelCommand;

                _cancelCommand = new RelayCommand(o => CancelCommand_Executed())
                {
                    FriendlyName = Message.Generic_Cancel
                };
                ViewModelCommands.Add(_cancelCommand);

                return _cancelCommand;
            }
        }

        private void CancelCommand_Executed()
        {
            DialogResult = false;
        }

        #endregion

        #endregion

        #region Methods

        public void EditValidationTemplateGroupMetricField()
        {
            if (AttachedControl == null) return;

            var planItemFieldSelectorViewModel = new PlanItemFieldSelectorViewModel(ValidationTemplateGroupMetric.Field, ValidationTemplateGroupMetric.Parent.Metrics.Select(metric => metric.Field));
            var win = new PlanItemFieldSelectorWindow(planItemFieldSelectorViewModel);
            App.ShowWindow(win, AttachedControl, true);
            if (win.DialogResult != true) return;

            ValidationTemplateGroupMetric.Field = planItemFieldSelectorViewModel.FieldText;
            OnPropertyChanged(IsAggregableProperty);
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (IsDisposed) return;

            if (disposing)
            {
                // dispose here of any resources.
            }

            IsDisposed = true;
        }

        #endregion
    }
}