﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Tidy up and localization.

#endregion

#endregion

using System.Windows;

namespace Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for ValidateTemplateMaintenanceHomeTab.xaml
    /// </summary>
    public partial class ValidationTemplateMaintenanceHomeTab
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof (ValidationTemplateMaintenanceViewModel), typeof (ValidationTemplateMaintenanceHomeTab),
            new PropertyMetadata(null));

        public ValidationTemplateMaintenanceViewModel ViewModel
        {
            get { return (ValidationTemplateMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public ValidationTemplateMaintenanceHomeTab()
        {
            InitializeComponent();
        }

        #endregion
    }
}
