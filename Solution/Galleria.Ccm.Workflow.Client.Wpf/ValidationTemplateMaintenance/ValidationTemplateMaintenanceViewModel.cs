﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26400 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Implemented backstage commands. Tidy up and localization.
// V8-26634 : A.Silva ~ Added delete functionality for group metric fields.
// V8-26809 : A.Silva ~ Corrected implementation of SaveAs, so that it does save as new, and not just updates.
// V8-26817 : A.Silva ~ Updated usage of IValidationTemplateEditorViewModel.
// V8-26812 : A.Silva ~ Added Validation error descriptions for ValidationTemplate.
// V8-27321 : A.Silva
//      Amended IsUniqueCheck to correctly find duplicate matches.
// V8-28553 : A.Silva
//      Amended Save_CanExecute() so that only a data is invalid message is displayed instead of individual broken rules.

#endregion

#region Version History: (CCM 8.01)
// V8-28023 : L.Ineson
//  Refactored completely to bring up to standard with other screens.
// V8-28922 : L.Ineson
//  Updated to use new window service window viewmodel base.
#endregion

#region Version History: (CCM 8.10)
// V8-30265 : A.Probyn
//  Updated save to force unique names on validation groups (inline with other screens)
#endregion
#endregion

using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Galleria.Ccm.Common.Wpf.ValidationTemplateEditor;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance
{
    /// <summary>
    /// Viewmodel controller for ValidationTemplateMaintenanceOrganiser
    /// </summary>
    public sealed class ValidationTemplateMaintenanceViewModel : WindowViewModelBase, IValidationTemplateEditorViewModel
    {
        #region Constants

        private const String _exceptionCategory = "ValidationTemplateMaintenance";

        #endregion

        #region Fields

        private readonly ModelPermission<ValidationTemplate> _itemPermissions;

        private readonly ValidationTemplateInfoListView _masterValidationTemplateInfoListView = new ValidationTemplateInfoListView();

        private ValidationTemplate _editingValidationTemplate;

        #endregion

        #region Binding Property Paths

        //Properties
        public static readonly PropertyPath AvailableValidationTemplatesProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.AvailableValidationTemplates);
        public static readonly PropertyPath ValidationTemplateEditProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.ValidationTemplateEdit);

        //Commands
        public static readonly PropertyPath CloseCommandProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.CloseCommand);
        public static readonly PropertyPath DeleteCommandProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.DeleteCommand);
        public static readonly PropertyPath NewCommandProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.NewCommand);
        public static readonly PropertyPath OpenCommandProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.OpenCommand);
        public static readonly PropertyPath SaveAndCloseCommandProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static readonly PropertyPath SaveAndNewCommandProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static readonly PropertyPath SaveAsCommandProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.SaveAsCommand);
        public static readonly PropertyPath SaveCommandProperty = GetPropertyPath<ValidationTemplateMaintenanceViewModel>(p => p.SaveCommand);

        #endregion

        #region Properties

        /// <summary>
        ///     Gets the list of available <see cref="ValidationTemplateInfo" /> items.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ValidationTemplateInfo> AvailableValidationTemplates
        {
            get { return _masterValidationTemplateInfoListView.BindableCollection; }
        }

        /// <summary>
        ///     Currently selected validation template.
        /// </summary>
        public ValidationTemplate ValidationTemplateEdit
        {
            get { return _editingValidationTemplate; }
            private set
            {
                _editingValidationTemplate = value;
                OnPropertyChanged(ValidationTemplateEditProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="ValidationTemplateMaintenanceViewModel" />.
        /// </summary>
        public ValidationTemplateMaintenanceViewModel()
        {
            //set perms
            _itemPermissions = new ModelPermission<ValidationTemplate>(ValidationTemplate.GetUserPermissions());

            //fetch
            _masterValidationTemplateInfoListView.FetchAllForEntity();

            //create a new item.
            this.NewCommand.Execute();
        }

        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        ///  Creates a new item.
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(o => New_Executed(), o => New_CanExecute())
                    {
                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    RegisterCommand(_newCommand);
                }

                return _newCommand;
            }
        }

        private Boolean New_CanExecute()
        {
            //nb -check for create permission will be performed on save.

            return true;
        }

        private void New_Executed()
        {
            if (!ContinueWithItemChange()) return;

            // Create a new item.
            ValidationTemplate newValidationTemplate = ValidationTemplate.NewValidationTemplate(App.ViewState.EntityId);
            newValidationTemplate.PrepTemplateForEdit();
            newValidationTemplate.MarkGraphAsInitialized();

            this.ValidationTemplateEdit = newValidationTemplate;

            // Close the Backstage.
            CloseRibbonBackstage();
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        ///  Opens an existing validation template by id.
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {
                    _openCommand = new RelayCommand<Int32?>(Open_Executed, Open_CanExecute)
                    {
                        FriendlyName = Message.Generic_Open,
                    };
                    RegisterCommand(_openCommand);
                }

                return _openCommand;
            }
        }

        private Boolean Open_CanExecute(Int32? itemId)
        {
            if (!_itemPermissions.CanFetch)
            {
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
                return false;
            }
            if (!itemId.HasValue)
            {
                _openCommand.DisabledReason = String.Empty;
                return false;
            }

            return true;
        }

        private void Open_Executed(Int32? itemId)
        {
            if (!ContinueWithItemChange()) return;

            ShowWaitCursor(true);

            try
            {
                this.ValidationTemplateEdit = ValidationTemplate.FetchById(itemId.Value);
            }
            catch (DataPortalException e)
            {
                ShowWaitCursor(false);
                RecordException(e, _exceptionCategory);
                GetWindowService().ShowErrorOccurredMessage(String.Empty, OperationType.Open);
                return;
            }

            // Close the backstage.
            CloseRibbonBackstage();

            ShowWaitCursor(false);
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        ///     Gets the save command.
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(o => Save_Executed(), o => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    RegisterCommand(_saveCommand);
                }

                return _saveCommand;
            }
        }

        private Boolean Save_CanExecute()
        {
            //must have an item
            if (ValidationTemplateEdit == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have correct permission
            if (this.ValidationTemplateEdit.IsNew && !_itemPermissions.CanCreate)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }
            if (!this.ValidationTemplateEdit.IsNew && !_itemPermissions.CanEdit)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //item must be valid.
            if (!this.ValidationTemplateEdit.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        private Boolean SaveCurrentItem()
        {
            //** check the item unique value
            Object itemId = this.ValidationTemplateEdit.Id;
            String newName;
            ValidationTemplateInfoList existingItems = ValidationTemplateInfoList.FetchByEntityId(App.ViewState.EntityId);

            Boolean nameAccepted = GetWindowService().PromptIfNameIsNotUnique(
                (s) => { return !existingItems.Any(p => !Object.Equals(p.Id, itemId) && p.Name.ToLowerInvariant() == s.ToLowerInvariant()); },
                this.ValidationTemplateEdit.Name, out newName);
            if (!nameAccepted) return false;  

            //set the name
            if (this.ValidationTemplateEdit.Name != newName) this.ValidationTemplateEdit.Name = newName;

            //force all validation groups names to be unique
            foreach (ValidationTemplateGroup group in this.ValidationTemplateEdit.Groups.Reverse())
            {
                String validName = Galleria.Framework.Controls.Wpf.Helpers.GenerateUniqueString(group.Name, this.ValidationTemplateEdit.Groups.Where(r => r != group).Select(p => p.Name));
                if (group.Name != validName)
                {
                    group.Name = validName;
                }
            }

            //Perform the save
            ShowWaitCursor(true);
            try
            {
                this.ValidationTemplateEdit = this.ValidationTemplateEdit.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);

                RecordException(ex);
                Exception rootException = ex.GetBaseException();

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(this.ValidationTemplateEdit.Name);
                    if (itemReloadRequired)
                    {
                        this.ValidationTemplateEdit = ValidationTemplate.FetchById(itemId);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(this.ValidationTemplateEdit.Name, OperationType.Save);
                }
                return false;
            }

            //refresh the info list
            _masterValidationTemplateInfoListView.FetchAllForEntity();


            ShowWaitCursor(false);

            return true;
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     Gets the SaveAs command.
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(), 
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F12
                    };
                    RegisterCommand(_saveAsCommand);
                }

                return _saveAsCommand;
            }
        }

        private Boolean SaveAs_CanExecute()
        {
            //must have an item
            if (ValidationTemplateEdit == null)
            {
                this.SaveAsCommand.DisabledReason = String.Empty;
                return false;
            }

            //must have correct permission
            if (!_itemPermissions.CanCreate)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //item must be valid.
            if (!this.ValidationTemplateEdit.IsValid)
            {
                this.SaveAsCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the name to save as
            String copyName;


            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(
                (s) =>{ return !this.AvailableValidationTemplates.Any(p => p.Name.ToLowerInvariant() == s.ToLowerInvariant()); }, 
                String.Empty, out copyName);
            
            if (!nameAccepted) return;


            //Copy the item and rename
            ShowWaitCursor(true);

            ValidationTemplate itemCopy = this.ValidationTemplateEdit.Copy();
            itemCopy.Name = copyName;
            this.ValidationTemplateEdit = itemCopy;

            ShowWaitCursor(false);


            //save it.
            SaveCurrentItem();
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        ///     Gets the SaveAndNew command.
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(o => SaveAndNew_Executed(), o => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    RegisterCommand(_saveAndNewCommand);
                }

                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            Boolean itemSaved = SaveCurrentItem();

            if (itemSaved)
            {
                NewCommand.Execute();
            }
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        ///     Gets the SaveAndClose command.
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(o => SaveAndClose_Executed(), o => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    RegisterCommand(_saveAndCloseCommand);
                }

                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed close the window.
            if (itemSaved)
            {
                CloseWindow();
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        ///     Gets the Delete command.
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(o => Delete_Executed(), o => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_deleteCommand);
                }

                return _deleteCommand;
            }
        }

        private Boolean Delete_CanExecute()
        {
            //user must have delete permission
            if (!_itemPermissions.CanDelete)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
                return false;
            }

            //must not be null
            if (this.ValidationTemplateEdit == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
                return false;
            }

            //must not be new
            if (this.ValidationTemplateEdit.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
                return false;
            }

            return true;
        }

        private void Delete_Executed()
        {
            //confirm with user
            if (!GetWindowService().ConfirmDeleteWithUser(this.ValidationTemplateEdit.Name)) return;

            ShowWaitCursor(true);

            //mark the item as deleted so item changed does not show.
            ValidationTemplate itemToDelete = this.ValidationTemplateEdit;
            itemToDelete.Delete();

            //load a new item
            NewCommand.Execute();

            //commit the delete
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);
                RecordException(ex);
                GetWindowService().ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);
                return;
            }

            //update the available items
            _masterValidationTemplateInfoListView.FetchAllForEntity();

            ShowWaitCursor(false);
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        ///     Gets the Close command.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(o => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    RegisterCommand(_closeCommand);
                }

                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            CloseWindow();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Shows a warning requesting user ok to continue if current
        /// item is dirty
        /// </summary>
        /// <returns>true if the action may continue</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(this.ValidationTemplateEdit, this.SaveCommand);
        }

        #endregion

        #region IValidationTemplateEditorViewModel Members

        void IValidationTemplateEditorViewModel.EditGroupMetric(IValidationTemplateGroup validationTemplateGroup,
            IValidationTemplateGroupMetric metric, String metricName)
        {
            if (!HasAttachedControl()) return;

            ValidationTemplateEdit.BeginEdit();

            //if (validationTemplateGroup == null)
            //{
            //    validationTemplateGroup = ValidationTemplateEdit.AddNewGroup();
            //}

            if (metric == null)
            {
                metric = validationTemplateGroup.AddNewMetric();
            }


            //TODO: Refactor to not create window control.
            ValidationTemplateGroupMetricEditorWindow editorWindow = new ValidationTemplateGroupMetricEditorWindow(metricName, metric);
            GetWindowService().ShowDialog<ValidationTemplateGroupMetricEditorWindow>(editorWindow);

            if (editorWindow.DialogResult != true)
            {
                ValidationTemplateEdit.CancelEdit();
            }
            else
            {
                ValidationTemplateEdit.ApplyEdit();
            }
        }

        void IValidationTemplateEditorViewModel.DeleteGroupMetric(IValidationTemplateGroup validationTemplateGroup,
            IValidationTemplateGroupMetric metric)
        {
            validationTemplateGroup.RemoveMetric(metric);
        }

        IValidationTemplate IValidationTemplateEditorViewModel.ValidationTemplateEdit
        {
            get { return this.ValidationTemplateEdit; }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    this.ValidationTemplateEdit = null;
                    _masterValidationTemplateInfoListView.Dispose();

                    DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion

    }
}