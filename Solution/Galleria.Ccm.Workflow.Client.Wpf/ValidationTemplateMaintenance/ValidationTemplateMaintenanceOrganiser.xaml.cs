﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26400 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Tidy up and localization.
// V8-26634 : A.Silva ~ Added delete functionality for group metric fields.

#endregion

#endregion

using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Galleria.Ccm.Workflow.Client.Wpf.ValidationTemplateMaintenance
{
    /// <summary>
    ///     Interaction logic for ValidationTemplateMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class ValidationTemplateMaintenanceOrganiser
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = 
            DependencyProperty.Register("ViewModel", typeof (ValidationTemplateMaintenanceViewModel), typeof (ValidationTemplateMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ValidationTemplateMaintenanceOrganiser senderControl = sender as ValidationTemplateMaintenanceOrganiser;

            if (e.OldValue != null)
            {
                ValidationTemplateMaintenanceViewModel oldModel = (ValidationTemplateMaintenanceViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
            }

            if (e.NewValue != null)
            {
                ValidationTemplateMaintenanceViewModel newModel = (ValidationTemplateMaintenanceViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
            }
        }


        public ValidationTemplateMaintenanceViewModel ViewModel
        {
            get { return (ValidationTemplateMaintenanceViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ValidationTemplateMaintenanceOrganiser()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Set help key
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.ValidationTemplateMaintenance);

            ViewModel = new ValidationTemplateMaintenanceViewModel();

            Loaded += ValidationTemplateMaintenanceOrganiser_Loaded;
        }

        /// <summary>
        /// Carries out initial load actions.
        /// </summary>
        private void ValidationTemplateMaintenanceOrganiser_Loaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            Loaded -= ValidationTemplateMaintenanceOrganiser_Loaded;

            Dispatcher.BeginInvoke((Action) (() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Window Close

        /// <summary>
        /// On the window being closed, checked if changes may require saving
        /// </summary>
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                if (!this.ViewModel.ContinueWithItemChange())
                {
                    e.Cancel = true;
                }

            }

        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
               (Action)(() =>
               {

                   IDisposable disposableViewModel = this.ViewModel as IDisposable;
                   this.ViewModel = null;

                   if (disposableViewModel != null)
                   {
                       disposableViewModel.Dispose();
                   }

               }), priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}