﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25438 : L.Hodson
//  Created.
// V8-25787 : N.Foster
//  Added workflow engine
// V8-26159 : L.Ineson
//  Added gfs service types registration
// V8-26159 : L.Ineson
//  Added gfs service types registration
// V8-26969 : J.Pickup
//  Added Aspose PDF Licensing for Reporting.
// V8-27528 : M.Shelley
//  Added Desaware product licensing
// V8-27732 : L.Ineson
//  Added fetch of systemsettings, ceip window and amended how db setup window loads.
#endregion

#region Version History: CCM 8.01
// CCM-28755 : L.Ineson
//  Amended to use new window service
#endregion

#region Version History: CCM 8.1.0
// V8-30243 : L.Ineson
// Added try catch around the check for existing process.
#endregion

#region Version History: CCM 8.1.1
// V8-30327 : I.George
// Now opens databaseSetupwindow if user does not exist in the database
#endregion
#region Version History: CCM 8.3.0
// V8-32191 : A.Probyn
//  Added handling in the App_DispatcherUnhandledException for clipboard exception
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using System.Xml;
using Csla;
using Galleria.Ccm.Common.Wpf;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Licensing;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Ccm.Services;
using Galleria.Ccm.Workflow.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Ccm.Workflow.Client.Wpf.Startup;
using Galleria.Framework.Planograms.Helpers;
using Gibraltar.Agent;
using Galleria.Ccm.Common.Wpf.Services;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Common.Wpf.Converters;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Converters;
using Galleria.Reporting.Controls.Wpf;
using Galleria.Framework.Controls.Wpf.Converters;
using Galleria.Framework.Controls.Wpf.Themes.Colours;

namespace Galleria.Ccm.Workflow.Client.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Nested Classes

        /// <summary>
        /// The SessionException class allows an unhandled exception to be wrapped in such a way that
        /// its message includes the unique Session ID for this ISO session, a property that can be used to 
        /// find this specific session in a Gibraltar repository.
        /// </summary>
        private class SessionException : Exception
        {
            public string SessionId { get; set; }

            public SessionException(Exception innerException) :
                base(string.Format(
                CultureInfo.InvariantCulture,
                Galleria.Ccm.Workflow.Client.Wpf.Resources.Language.Message.UnhandledException_Message,
                Log.SessionSummary.Properties[_sessionIdName],
                Environment.NewLine,
                innerException.Message),
                innerException)
            {
                SessionId = Log.SessionSummary.Properties[_sessionIdName];
            }
        }

        #endregion

        #region Constants

        private const Int32 _secondsToWaitForProcessShutdown = 5;
        private const String _sessionIdName = "Session ID";

        #endregion

        #region Fields

        private static MainPageViewModel _unitTestingViewModel; // the mainpage viewmodel to return when unit testing.
        private static ViewState _viewState = new ViewState();
        private static Engine.Engine _engine = new Engine.Engine(); // instance of the ccm workflow engine
        private static LicenseState _licenseState;

        #endregion

        #region Static Properties

        /// <summary>
        ///     Gets/Sets whether we are currently unit testing.
        /// </summary>
        public static Boolean IsUnitTesting
        {
            get { return CCMClient.IsUnitTesting; }
        }

        /// <summary>
        /// The ccm workflow engine
        /// </summary>
        public static Engine.Engine Engine
        {
            get { return _engine; }
        }

        /// <summary>
        /// The application view state
        /// </summary>
        public static ViewState ViewState
        {
            get { return _viewState; }
        }

        /// <summary>
        /// Returns the mainpage viewmodel for this app.
        /// </summary>
        public static MainPageViewModel MainPageViewModel
        {
            get
            {
                if (IsUnitTesting)
                {
                    if (_unitTestingViewModel == null)
                    {
                        _unitTestingViewModel = new MainPageViewModel();
                    }
                    return _unitTestingViewModel;
                }
                else
                {
                    MainPageOrganiser org = App.Current.MainWindow as MainPageOrganiser;
                    if (org != null)
                    {
                        return org.ViewModel;
                    }
                    return null;
                }
            }
        }

        /// <summary>
        /// The Application LicenseState
        /// </summary>
        public static LicenseState LicenseState
        {
            get
            {
                if (_licenseState == null)
                {
                    _licenseState = new LicenseState();
                }
                return _licenseState;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public App()
            : this(false)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="unitTesting">Indicates if we are loading this app for unit testing purposes</param>
        public App(Boolean unitTesting)
        {
            this.Startup += new StartupEventHandler(App_Startup);
            this.Exit += new ExitEventHandler(App_Exit);

            // Attach to the event to globally hand exceptions before they hit the os
            this.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(App_DispatcherUnhandledException);

            //Register the viewstate.
            CCMClient.Register(ViewState);
            if (unitTesting) CCMClient.MarkAsUnitTesting();
        }

        #endregion

        #region App Event Handlers

        /// <summary>
        /// Called once the application has been started up
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        public void App_Startup(object sender, StartupEventArgs e)
        {
            this.Startup -= App_Startup;

            UserSystemSetting systemSettings = GetSystemSettings();

            //Override the current culture info
            // This determines the numberformats/currency etc that will be used by the app.
            // The following line ensures that it will be picked up from the pc ControlPanel-> Region and Language -> Format setting.
            FrameworkElement.LanguageProperty.OverrideMetadata(
            typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            //set the display language
            //This is the language that the application will be displayed in.
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(systemSettings.DisplayLanguage);

            //Check that an instance is not already running
            try
            {
                Process thisProc = Process.GetCurrentProcess();
                String thisProcOwner = GetProcessOwner(thisProc.Id);
                for (int i = 0; i <= _secondsToWaitForProcessShutdown; i++)
                {
                    //get all processes with the same name
                    Process[] matchingProcesses = Process.GetProcessesByName(thisProc.ProcessName);

                    //if there is more than one check if any has the same owner
                    if (matchingProcesses.Length > 1)
                    {
                        if (matchingProcesses.Where(p => GetProcessOwner(p.Id) == thisProcOwner).Count() > 1)
                        {
                            if (i < _secondsToWaitForProcessShutdown)
                            {
                                Thread.Sleep(1000);
                            }
                            else
                            {
                                MessageBox.Show(Message.Startup_CCMAlreadyRunning);
                                App.Current.Shutdown();
                                return;
                            }

                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception)
            {
                //The pc WMI service failed to start. Not our problem.
            }
            //end 



            // if we are unit testing, then we do not perform
            // authentication or load the main page organiser
            if (!IsUnitTesting)
            {

                //Load any required theme and colour dictionaries.
                LoadThemeResources();

                try
                {
                    // set aspose cells license
                    Aspose.Cells.License cellsLicense = new Aspose.Cells.License();
                    cellsLicense.SetLicense("Aspose.Total.10.lic");

                    // set aspose pdf license
                    Aspose.Pdf.License pdfLicense = new Aspose.Pdf.License();
                    pdfLicense.SetLicense("Aspose.Total.10.lic");
                }
                catch (Exception)
                {
                    // License doesn't exist for developer, GABS will swap in on release build
                }

                //Have to set the below because of the dialog win
                this.ShutdownMode = ShutdownMode.OnExplicitShutdown;

                //CEIP gilbraltar integration
                CEIPViewModel ceipViewModel = new CEIPViewModel(systemSettings);
                if (ceipViewModel.CEIPWindowRequired())
                {
                    CEIPWindow win = new CEIPWindow(ceipViewModel);
                    win.ShowDialog();
                }
                ceipViewModel = null;

                // initialize and start the logging session
                Log.Initializing += new Log.InitializingEventHandler(Log_Initializing);
                Log.StartSession();

                //register gfs service types
                FoundationServiceClient.RegisterGFSServiceClientTypes();

                //register the image renderer
                PlanogramImagesHelper.RegisterPlanogramImageRenderer<Galleria.Framework.Planograms.Controls.Wpf.Helpers.PlanogramImageRenderer>();

                //register the window and modal busy service
                ServiceContainer.RegisterServiceObject<IWindowService>(new WpfWindowService());
                ServiceContainer.RegisterServiceObject<IModalBusyWorkerService>(new WpfModalBusyWorkerService());

                // Database setup view model
                DatabaseSetupViewModel databaseSetupViewModel = new DatabaseSetupViewModel();
                if (databaseSetupViewModel.DatabaseSetupWindowRequired())
                {
                    DatabaseSetupWindow win = new DatabaseSetupWindow(databaseSetupViewModel);
                    win.ShowDialog();

                    //shut down the app if no connection was set.
                    if (databaseSetupViewModel.DialogResult != true)
                    {
                        App.Current.Shutdown();
                        return;
                    }

                    databaseSetupViewModel = null;

                    //start the session 
                    //(setup window will have already performed the logon authentication.)
                    BeginSession();
                }
                else
                {
                    databaseSetupViewModel.Dispose();
                    databaseSetupViewModel = null;

                    // Logon
                    DomainPrincipal.BeginAuthentication(AuthenticationComplete);
                }
            }
        }

        /// <summary>
        /// Called when the application is exiting
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        public void App_Exit(object sender, ExitEventArgs e)
        {
            try
            {
                // unsubscribe from the exit event
                this.Exit -= App_Exit;

                // shutdown the engine
                Engine.StopEngine();

                // end the logging session
                Log.EndSession();
            }
            catch (Exception)
            {
                // if something happened during shutdown
                // then there is not much we can do about it now
            }
        }

        /// <summary>
        /// Handler to catch global exceptions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            // indicate that this exception has been handled
            e.Handled = true;

            // get the base exception
            Exception baseException = e.Exception.GetBaseException();

            //// if a security exception has been thrown
            //// then display a standard dialog box to the
            //// user and allow them to continue
            //// otherwise log the exception using gibralter
            //// and shut down the application
            //// we only show the dialog if the main window has
            //// been loaded, as any security issue before that
            //// point means that the application cannot start
            //if ((App.Current.MainWindow != null) &&
            //    (App.Current.MainWindow is MainPageOrganiser) &&
            //    (baseException is SecurityException))
            //{
            //    //turn off busy cursor
            //    Dispatcher.BeginInvoke((Action)(() => { System.Windows.Input.Mouse.OverrideCursor = null; }));

            //    ModalMessage message = new ModalMessage();
            //    message.Title = App.Current.MainWindow.GetType().Assembly.GetName().Name;
            //    message.Header = Message.SecurityDialog_Exception;
            //    message.Description = baseException.Message;
            //    message.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            //    message.ButtonCount = 1;
            //    message.Button1Content = Message.Generic_Ok;
            //    message.DefaultButton = ModalMessageButton.Button1;
            //    message.MessageIcon = ImageResources.Dialog_Error;
            //    App.ShowWindow(message, true);
            //    return;
            //}

            //V8-32191 - Clipboard exception
            var comException = baseException as System.Runtime.InteropServices.COMException;
            if (comException != null && comException.ErrorCode == -2147221040)
            {
                //just ignore.
                Debug.Fail(baseException.Message);
                return;
            }

            // log the exception to gibralter and shutdown the application
            try
            {
                Log.ReportException(new SessionException(e.Exception), "Unhandled Exception", false, true); // GFS-15573
            }
            catch (Exception)
            {
                // If we get an exception at this point, there is not a lot more we can do
                // but at least prevent a system exception
            }

            Application.Current.Shutdown();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the ownername of the given process
        /// </summary>
        /// <param name="processId"></param>
        /// <returns></returns>
        private String GetProcessOwner(Int32 processId)
        {
            String query = "Select * From Win32_Process Where ProcessID = " + processId;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection processList = searcher.Get();

            foreach (ManagementObject obj in processList)
            {
                String[] argList = new String[] { String.Empty, String.Empty };
                Int32 returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                if (returnVal == 0)
                {
                    // return DOMAIN\user
                    return argList[1] + "\\" + argList[0];
                }
            }

            return "NO OWNER";
        }

        /// <summary>
        /// Check that the app is correctly licensed
        /// </summary>
        public Boolean CheckLicense()
        {
            // Setup / verify licensing
            var licenceObject = new LicenceSetup(false);

            // shut down the app if we are not licensed.
            if (licenceObject.ApplicationShutdown)
            {
                this.Shutdown();
                return false;
            }

            _licenseState = licenceObject.LicenseState;

            // Check if the license setup has set the Application LicenseState property
            if (this.Properties["LicenseState"] == null)
            {
                // There is no current App LicenseState property, so set a reference in the App properties for use by the Help page
                this.Properties["LicenseState"] = _licenseState;
            }

            return true;
        }

        /// <summary>
        /// Returns the system settings model for this app.
        /// </summary>
        public static UserSystemSetting GetSystemSettings()
        {
            UserSystemSetting settings;

            try
            {
                settings = UserSystemSetting.FetchUserSettings(IsUnitTesting);
            }
            catch (Exception)
            {
                settings = UserSystemSetting.NewUserSystemSetting();
            }
            return settings;
        }

        /// <summary>
        /// Loads resource dictionaries required for themes and styling.
        /// </summary>
        private void LoadThemeResources()
        {
            //load converters
            ResourceDictionary convertersDictionary = new ResourceDictionary();
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterStringFormat, new StringFormatConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterUTCToDate, new UTCToLocalDateConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterUTCToDateTime, new UTCToLocalDateTimeConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterUTCToLocalTime, new UTCToLocalTimeConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterConditionToBool, new ConditionToBoolConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterConditionToVisibility, new ConditionToVisibilityConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterDictionaryKeyToValue, new DictionaryKeyToValueConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterCollectionToView, new CollectionToViewConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterMultiConditionToBool, new MultiConditionToBoolConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterValueToPercentage, new ValueToPercentageConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterMultiConditionToVisibility, new MultiConditionToVisibilityConverter());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterTrueFalseValueProvider, new TrueFalseValueProvider());
            convertersDictionary.Add(Framework.Controls.Wpf.ResourceKeys.ConverterIntToColor, new IntToColorConverter());
            convertersDictionary.Add(ResourceKeys.ConverterAreEqual, new AreEqualConverter());
            convertersDictionary.Add(ResourceKeys.ConverterUnitDisplay, new UnitDisplayConverter());
            convertersDictionary.Add(ResourceKeys.ConverterBytesToImageSource, new BytesToImageSourceConverter());
            convertersDictionary.Add(ResourceKeys.ConverterEnumTypeToFriendlyNames, new EnumTypeToFriendlyNamesConverter());
            convertersDictionary.Add(ResourceKeys.ConverterEnumValueToFriendlyName, new EnumValueToFriendlyNameConverter());
            convertersDictionary.Add(ResourceKeys.ConverterToPercentage, new ToPercentageConverter());
            convertersDictionary.Add(ResourceKeys.ConverterFieldTextToDisplay, new FieldTextToDisplayConverter());
            convertersDictionary.Add(ResourceKeys.ConverterSourceToDisplayUnitOfMeasure, new SourceToDisplayUnitOfMeasure());
            convertersDictionary.Add(Galleria.Reporting.Controls.Wpf.ResourceKeys.ConverterReportPageWidthToDouble, new ConverterReportPageWidthToDouble());
            Application.Current.Resources.MergedDictionaries.Add(convertersDictionary);


            //Colours
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Fluent;Component/Themes/Office2010/Blue.xaml", UriKind.Relative)));
            
            if (SystemParameters.HighContrast) Application.Current.Resources.MergedDictionaries.Add(HighContrastHelper.GetFluentHighContrastResourceDictionary());

            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Framework.Controls.Wpf;component/Themes/Colours/ColorsBlue.xaml", UriKind.Relative)));
            
            if (SystemParameters.HighContrast) Application.Current.Resources.MergedDictionaries.Add(HighContrastHelper.GetGalleriaHighContrastResourceDictionary());

            //framework resources
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Framework.Controls.Wpf;component/Themes/Theme_Common.xaml", UriKind.Relative)));
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Framework.Controls.Wpf;component/Themes/Colours_Common.xaml", UriKind.Relative)));

            //reporting resources
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Reporting.Controls.Wpf;component/Themes/Theme_Common.xaml", UriKind.Relative)));

            //planogram framework resources
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Framework.Planograms.Controls.Wpf;component/Themes/Theme_Common.xaml", UriKind.Relative)));

            //local resources
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("/Galleria.Ccm.Common.Wpf;component/Themes/Res_Main.xaml", UriKind.Relative)));
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("Resources/Theme/Res_Main.xaml", UriKind.Relative)));
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("Resources/Theme/Res_Diagram.xaml", UriKind.Relative)));
            Application.Current.Resources.MergedDictionaries.Add((ResourceDictionary)Application.LoadComponent(new Uri("Common/EngineTasks/TaskTemplates.xaml", UriKind.Relative)));

            //Set the application colour brush
            Application.Current.Resources.Add("ApplicationColourBrush",
                //(SystemParameters.HighContrast) ? SystemColors.HighlightBrush :
                new System.Windows.Media.SolidColorBrush((System.Windows.Media.Color)System.Windows.Media.ColorConverter.ConvertFromString("#FF027EB3")));

            SetupColourPicker();

        }
        // this is to added selected color to the recent color box
        private static void SetupColourPicker()
        {
            Style currentColorPickerStyle = (Application.Current.Resources[typeof(ColorPickerBox)] as Style);
            if (currentColorPickerStyle == null) return;

            Style colorPickerBoxStyle = new Style(typeof(ColorPickerBox), currentColorPickerStyle);
            colorPickerBoxStyle.Setters.Add(new EventSetter(ColorPickerBox.SelectedColorChangedEvent, (RoutedEventHandler)(OnSelectedColorChanged)));
            colorPickerBoxStyle.Seal();
            Application.Current.Resources[typeof(ColorPickerBox)] = colorPickerBoxStyle;
        }

        public static void OnSelectedColorChanged(object sender, RoutedEventArgs e)
        {
            var colorPicker = e.OriginalSource as ColorPickerBox;
            if (colorPicker != null)
            {
                App.ViewState.ApplyRecentColor(colorPicker.SelectedColor);
            }
        }

        #endregion

        #region Authentication

        /// <summary>
        /// Called when the authentication process is complete
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void AuthenticationComplete(object sender, DomainAuthenticationResultEventArgs e)
        {
            if (e.Exception == null)
            {
                Boolean isAuthenticated = ApplicationContext.User.Identity.IsAuthenticated;
                if (isAuthenticated)
                {
                    BeginSession();
                }
                else
                {
                    MessageBox.Show(Message.Error_UnauthorisedUser);
                }
            }
            else
            {
                //Database setup view model
                DatabaseSetupViewModel databaseSetupViewModel = new DatabaseSetupViewModel();
                DatabaseSetupWindow win = new DatabaseSetupWindow(databaseSetupViewModel);
                win.ShowDialog();

                //shut down the app if no connection was set.
                if (databaseSetupViewModel.DialogResult != true)
                {
                    App.Current.Shutdown();
                    return;
                }

                databaseSetupViewModel = null;
            }

        }

        /// <summary>
        /// Starts the app session
        /// </summary>
        private void BeginSession()
        {
            LoadingWindow loadingWin = new LoadingWindow();
            loadingWin.Show();
        }

        #endregion

        #region Logging

        /// <summary>
        /// Called when the log is initialized
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void Log_Initializing(object sender, LogInitializingEventArgs e)
        {
            // configure the application to log to the service
            e.Configuration.Server.UseGibraltarService = true;
            e.Configuration.Server.CustomerName = "galleria";
            e.Configuration.Server.SendAllApplications = true;
            e.Configuration.Server.PurgeSentSessions = true;
            e.Configuration.Server.UseSsl = true;
            e.Configuration.Properties[_sessionIdName] = Guid.NewGuid().ToString();
            e.Configuration.SessionFile.EnableFilePruning = true; // ISO-13436
            e.Configuration.SessionFile.MaxLocalDiskUsage = 200; // ISO-13436
            e.Configuration.Publisher.EnableAnonymousMode = false; // GFS-14882
#if !DEBUG
            // now determine if we are running within the IDE.
            // normally, you shouldn't change the behaviour of
            // an application depending on whether a debugger
            // is attached or not, however, we want to be able
            // to still use the logging facilities in debug
            // builds when they are distrubuted to people such
            // as qa and consultancy. Therefore we have used
            // this method instead of conditional compilation
            //if (!Debugger.IsAttached) // GFS-14882
            //{
            //    // set auto-sending of debug information to what the user has selected
            //    e.Configuration.Server.AutoSendSessions = _CEIPViewModel.IsParticipate();
            //}
#endif
        }

        #endregion

        #region Static Helpers

        /// <summary>
        ///     Initializes for a new unit test.
        /// </summary>
        public static void InitialiseForUnitTest()
        {
            _viewState = new ViewState();

            _unitTestingViewModel = null;

            CCMClient.Register(ViewState);
            CCMClient.MarkAsUnitTesting();
        }

        [Obsolete]
        public static void ShowWindow(Window window, Boolean isModal)
        {
            WindowHelper.ShowWindow(window, isModal);
        }

        [Obsolete]
        public static void ShowWindow(Window window, Window parentWindow, Boolean isModal)
        {
            WindowHelper.ShowWindow(window, parentWindow, isModal);
        }

        [Obsolete]
        public static void ShowWindow(ShowWindowRequestArgs args)
        {
            WindowHelper.ShowWindow(args);
        }


        #endregion
    }

    public class ShowWindowRequestArgs : WindowHelper.ShowWindowRequestArgs
    { }

}
