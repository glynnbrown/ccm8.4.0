﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//	Created
#endregion

#region Version History: (CCM 8.0.3)
// V8-29479 : N.Haywood
//  Added up/down buttons for field positions
#endregion

#region Version History: CCM811
// V8-30261 : M.Pettit
//  Renamed PlanogramNameTemplateSeparatorTypeHelper Underline to Underscore
#endregion

#region Version History: CCM830
// V8-31831 : A.Probyn
//  Added new Planogram Name Template PlanogramAttributeType
//  Reworked for formulae.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.Windows.Threading;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramNameTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramNameTemplateMaintenanceOrganiser.xaml
    /// </summary>
    public sealed partial class PlanogramNameTemplateMaintenanceOrganiser : ExtendedRibbonWindow
    {
        #region Constants
        
        public const String EditCalculatedColumnCommandKey = "EditCalculatedColumnCommand";

        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(PlanogramNameTemplateMaintenanceViewModel), typeof(PlanogramNameTemplateMaintenanceOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public PlanogramNameTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramNameTemplateMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            PlanogramNameTemplateMaintenanceOrganiser senderControl = (PlanogramNameTemplateMaintenanceOrganiser)obj;

            if (e.OldValue != null)
            {
                PlanogramNameTemplateMaintenanceViewModel oldModel = (PlanogramNameTemplateMaintenanceViewModel)e.OldValue;
                oldModel.UnregisterWindowControl();
                oldModel.PropertyChanged -= senderControl.ViewModel_PropertyChanged;
                oldModel.SelectedGroupFields.BulkCollectionChanged -= senderControl.ViewModel_ItemTypeFieldsBulkCollectionChanged;
            }

            if (e.NewValue != null)
            {
                PlanogramNameTemplateMaintenanceViewModel newModel = (PlanogramNameTemplateMaintenanceViewModel)e.NewValue;
                newModel.RegisterWindowControl(senderControl);
                newModel.PropertyChanged += senderControl.ViewModel_PropertyChanged;
                newModel.SelectedGroupFields.BulkCollectionChanged += senderControl.ViewModel_ItemTypeFieldsBulkCollectionChanged;
            }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramNameTemplateMaintenanceOrganiser()
        {
            //show the busy cursor
            Mouse.OverrideCursor = Cursors.Wait;

            InitializeComponent();

            //Add helpfile link
            HelpFileKeys.SetHelpFile(this, HelpFileKeys.PlanogramNameTemplateMaintenance);

            this.ViewModel = new PlanogramNameTemplateMaintenanceViewModel();

            this.Loaded += new RoutedEventHandler(PlanogramNameTemplateMaintenanceOrganiser_Loaded);
        }

        private void PlanogramNameTemplateMaintenanceOrganiser_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= PlanogramNameTemplateMaintenanceOrganiser_Loaded;

            //initialize the caret to the end
            this.ViewModel.CaretIndex = this.ViewModel.FieldText.Length;

            //cancel the busy cursor
            Dispatcher.BeginInvoke((Action)(() => { Mouse.OverrideCursor = null; }));
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Called whenever a property changes on the viewmodel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PlanogramNameTemplateMaintenanceViewModel.CaretIndexProperty.Path)
            {
                if (this.xFormulaTextBox != null)
                {
                    this.xFormulaTextBox.CaretIndex = this.ViewModel.CaretIndex;
                }
            }
        }
        
        /// <summary>
        /// Called whenever the item type fields collection changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_ItemTypeFieldsBulkCollectionChanged(object sender, Galleria.Framework.Model.BulkCollectionChangedEventArgs e)
        {
            //Reapply the prefilter
            String val = this.xFieldGridFilter.Text;
            if (!String.IsNullOrEmpty(val))
            {
                this.xFieldGridFilter.Text = null;
                this.xFieldGridFilter.Text = val;
            }
        }
        
        /// <summary>
        /// Called whenever the user double clicks on the field grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FieldGrid_RowItemMouseDoubleClick(object sender, ExtendedDataGridItemEventArgs e)
        {
            if (e.RowItem != null)
            {
                this.ViewModel.AddFieldToText((ObjectFieldInfo)e.RowItem);
            }
        }

        /// <summary>
        /// Called whenever the user double clicks on the operators list box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OperatorsListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBoxItem item = Galleria.Framework.Controls.Wpf.Helpers.FindVisualAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
            if (item != null)
            {
                this.ViewModel.AddOperator((ObjectFieldExpressionOperator)item.Content);
            }
        }

        private void xFormulaTextBox_TextInput(object sender, TextCompositionEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.CaretIndex = this.xFormulaTextBox.CaretIndex;
            }
        }

        private void xFormulaTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.CaretIndex = this.xFormulaTextBox.CaretIndex;
            }
        }
        #endregion

        #region Window Close

        /// <summary>
        /// Responds to the click of the backstage close button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            if (!e.Cancel)
            {
                //check if we need to save
                if (this.ViewModel != null)
                {
                    //e.Cancel = !this.ViewModel.ContinueWithItemChange();
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Dispatcher.BeginInvoke(
            (Action)(() =>
            {

                if (this.ViewModel != null)
                {
                    IDisposable dis = this.ViewModel;

                    this.ViewModel = null;

                    if (dis != null)
                    {
                        dis.Dispose();
                    }

                }

            }), priority: DispatcherPriority.Background);

        }

        #endregion

        private void UIElement_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                this.ViewModel.AddFieldToText((ObjectFieldInfo)lblgrid1.CurrentItem);
        }

        private void OpporatorsList_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.ViewModel.AddOperator((ObjectFieldExpressionOperator)OpporatorsList.SelectedItem);
            }
        }
    }
}