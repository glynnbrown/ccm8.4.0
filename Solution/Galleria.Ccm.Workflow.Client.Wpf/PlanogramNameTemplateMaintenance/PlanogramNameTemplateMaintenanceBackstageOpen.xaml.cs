﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//  Created.
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Galleria.Ccm.Model;
using Galleria.Framework.Model;
using Galleria.Framework.Controls.Wpf;
using Galleria.Reporting.Controls.Wpf.Common;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramNameTemplateMaintenance
{
    /// <summary>
    /// Interaction logic for PlanogramNameTemplateMaintenanceBackstageOpen.xaml
    /// </summary>
    public partial class PlanogramNameTemplateMaintenanceBackstageOpen
    {
        #region Fields

        private readonly ObservableCollection<PlanogramNameTemplateInfo> _searchResults = new ObservableCollection<PlanogramNameTemplateInfo>();

        #endregion

        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel",
            typeof(PlanogramNameTemplateMaintenanceViewModel), typeof(PlanogramNameTemplateMaintenanceBackstageOpen),
            new PropertyMetadata(null, ViewModel_PropertyChanged));

        public PlanogramNameTemplateMaintenanceViewModel ViewModel
        {
            get { return (PlanogramNameTemplateMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region SearchResults Property

        public static readonly DependencyProperty SearchResultsProperty = DependencyProperty.Register("SearchResults",
            typeof(ReadOnlyObservableCollection<PlanogramNameTemplateInfo>),
            typeof(PlanogramNameTemplateMaintenanceBackstageOpen),
            new PropertyMetadata(default(ReadOnlyObservableCollection<PlanogramNameTemplateInfo>)));

        public ReadOnlyObservableCollection<PlanogramNameTemplateInfo> SearchResults
        {
            get { return (ReadOnlyObservableCollection<PlanogramNameTemplateInfo>)GetValue(SearchResultsProperty); }
            set { SetValue(SearchResultsProperty, value); }
        }

        #endregion

        #region SearchText Property

        public static readonly DependencyProperty SearchTextProperty = DependencyProperty.Register("SearchText",
            typeof(String), typeof(PlanogramNameTemplateMaintenanceBackstageOpen), new PropertyMetadata(String.Empty, SearchText_PropertyChanged));

        public String SearchText
        {
            get { return (String)GetValue(SearchTextProperty); }
            set { SetValue(SearchTextProperty, value); }
        }

        #endregion

        #endregion

        #region Constructor

        public PlanogramNameTemplateMaintenanceBackstageOpen()
        {
            SearchResults = new ReadOnlyObservableCollection<PlanogramNameTemplateInfo>(_searchResults);
            InitializeComponent();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        ///     Invoked when the collection of available planogram name template changes.
        /// </summary>
        private void AvailablePlanogramNameTemplates_BulkCollectionChanged(Object sender, BulkCollectionChangedEventArgs e)
        {
            RefreshAvailablePlanogramNameTemplates();
        }

        /// <summary>
        ///     Invoked when the search text changes.
        /// </summary>
        private static void SearchText_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as PlanogramNameTemplateMaintenanceBackstageOpen;
            if (senderControl != null) senderControl.RefreshAvailablePlanogramNameTemplates();
        }

        /// <summary>
        ///     Invoked when the view model attached to this view changes.
        /// </summary>
        private static void ViewModel_PropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var senderControl = sender as PlanogramNameTemplateMaintenanceBackstageOpen;
            if (senderControl == null) return;

            var oldModel = e.OldValue as PlanogramNameTemplateMaintenanceViewModel;
            if (oldModel != null)
                oldModel.AvailablePlanogramNameTemplates.BulkCollectionChanged -=
                    senderControl.AvailablePlanogramNameTemplates_BulkCollectionChanged;
            var newModel = e.NewValue as PlanogramNameTemplateMaintenanceViewModel;
            if (newModel != null)
                newModel.AvailablePlanogramNameTemplates.BulkCollectionChanged +=
                    senderControl.AvailablePlanogramNameTemplates_BulkCollectionChanged;
            senderControl.RefreshAvailablePlanogramNameTemplates();
        }

        /// <summary>
        ///     Invoked when the user double clicks on the search list.
        /// </summary>
        private void XSearchResults_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var senderControl = sender as ExtendedDataGrid;
            if (senderControl == null) return;

            var target = senderControl.SelectedItem as PlanogramNameTemplateInfo;
            if (target == null) return;

            ViewModel.OpenCommand.Execute(target.Id);
        }

        #endregion

        #region Methods

        private void RefreshAvailablePlanogramNameTemplates()
        {
            _searchResults.Clear();

            if (ViewModel == null) return;

            foreach (var match in ViewModel.GetPlanogramNameTemplateMatches(SearchText))
            {
                _searchResults.Add(match);
            }
        }

        #endregion

        private void XSearchResults_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var selected = xSearchResults.SelectedItem as PlanogramNameTemplateInfo;
                if(selected != null)
                    ViewModel.OpenCommand.Execute(selected.Id);
            }
        }
    }
}