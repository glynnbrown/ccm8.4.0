﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0.2)
// V8-29010 : D.Pleasance
//	Created
#endregion

#region Version History: (CCM 8.0.3)
// V8-29479 : N.Haywood
//  Added up/down buttons for field positions
#endregion

#region Version History: CCM811
// V8-30261 : M.Pettit
//  Renamed PlanogramNameTemplateSeparatorTypeHelper Underline to Underscore
#endregion

#region Version History: CCM830
// V8-31831 : A.Probyn
//  Added new Planogram Name Template PlanogramAttributeType
//  Reworked for formulae.
// V8-32829 : A.Kuszyk
//  Added metadata fields to available Planogram fields.
// V8-32919 : A.Probyn
//  Added defensive code to AddOperator and fixed
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Csla;
using Csla.Core;
using Galleria.Ccm.Common.Wpf.CustomColumnLayouts;
using Galleria.Ccm.Common.Wpf.Helpers;
using Galleria.Ccm.Common.Wpf.Selectors;
using Galleria.Ccm.Common.Wpf.ViewModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Workflow.Client.Wpf.Common;
using Galleria.Ccm.Workflow.Client.Wpf.Resources.Language;
using Galleria.Framework.Collections;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.ViewModel;
using FrameworkHelpers = Galleria.Framework.Controls.Wpf.Helpers;
using Galleria.Framework.Planograms.Model;
using System.Text.RegularExpressions;
using System.Text;

namespace Galleria.Ccm.Workflow.Client.Wpf.PlanogramNameTemplateMaintenance
{
    public sealed class PlanogramNameTemplateMaintenanceViewModel : WindowViewModelBase
    {
        #region Constants

        private const Boolean DoNotAttachToCommandManager = false;
        private const String ExceptionCategory = "PlanogramNameTemplateMaintenance";
        private const Boolean SaveAsCopy = true;

        #endregion

        #region Fields

        const String _exCategory = "PlanogramNameTemplateMaintenance";
        private Boolean _userHasPlanogramNameTemplateCreatePerm;
        private Boolean _userHasPlanogramNameTemplateFetchPerm;
        private Boolean _userHasPlanogramNameTemplateEditPerm;
        private Boolean _userHasPlanogramNameTemplateDeletePerm;

        private readonly PlanogramNameTemplateInfoListViewModel _planogramNameTemplateInfoListViewModel = new PlanogramNameTemplateInfoListViewModel();

        private PlanogramNameTemplate _selectedPlanogramNameTemplate;
        private ReadOnlyCollection<FieldSelectorGroup> _fieldGroups;
        private FieldSelectorGroup _selectedFieldGroup;
        private FieldSelectorGroup _allFieldsGroup;

        private readonly BulkObservableCollection<ObjectFieldInfo> _selectedGroupFields = new BulkObservableCollection<ObjectFieldInfo>();
        private ReadOnlyBulkObservableCollection<ObjectFieldInfo> _selectedGroupFieldsRO;
        private ObjectFieldInfo _selectedField;

        private ReadOnlyCollection<ObjectFieldExpressionOperator> _availableOperators;

        private String _fieldFormula;
        private String _fieldFormulaError;
        private Int32 _caretIndex; //the position of the caret

        #endregion

        #region Binding Property paths

        // Properties
        public static readonly PropertyPath SelectedPlanogramNameTemplateProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.SelectedPlanogramNameTemplate);
        public static readonly PropertyPath AvailableFieldGroupsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.AvailableFieldGroups);
        public static readonly PropertyPath SelectedFieldGroupProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedFieldGroup);
        public static readonly PropertyPath SelectedGroupFieldsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedGroupFields);
        public static readonly PropertyPath SelectedFieldProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.SelectedField);
        public static readonly PropertyPath AvailableOperatorsProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.AvailableOperators);
        public static readonly PropertyPath BuilderFormulaProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.BuilderFormula);
        public static readonly PropertyPath FieldTextProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.FieldText);
        public static readonly PropertyPath SelectedAttributeProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.SelectedAttribute);
        public static readonly PropertyPath CaretIndexProperty = WpfHelper.GetPropertyPath<FieldSelectorViewModel>(p => p.CaretIndex);

        // Commands
        public static PropertyPath NewCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.NewCommand);
        public static PropertyPath OpenCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.OpenCommand);
        public static PropertyPath SaveCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.SaveCommand);
        public static PropertyPath SaveAsCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.SaveAsCommand);
        public static PropertyPath SaveAndNewCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.SaveAndNewCommand);
        public static PropertyPath SaveAndCloseCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.SaveAndCloseCommand);
        public static PropertyPath DeleteCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.DeleteCommand);
        public static PropertyPath CloseCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.CloseCommand);
        public static PropertyPath ClearCommandProperty = WpfHelper.GetPropertyPath<PlanogramNameTemplateMaintenanceViewModel>(p => p.ClearCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Return the collection of available Planogram Name Templates
        /// </summary>
        public ReadOnlyBulkObservableCollection<PlanogramNameTemplateInfo> AvailablePlanogramNameTemplates
        {
            get { return _planogramNameTemplateInfoListViewModel.BindableCollection; }
        }

        /// <summary>
        /// Returns the selected PlanogramNameTemplate model.
        /// </summary>
        public PlanogramNameTemplate SelectedPlanogramNameTemplate
        {
            get { return _selectedPlanogramNameTemplate; }
            private set
            {
                _selectedPlanogramNameTemplate = value;
                OnPropertyChanged(SelectedPlanogramNameTemplateProperty);
                OnPropertyChanged(SelectedAttributeProperty);
                OnPropertyChanged(BuilderFormulaProperty);
                OnSelectedPlanogramNameTemplateChanged();
            }
        }

        /// <summary>
        /// Gets the list of available field groups
        /// </summary>
        public ReadOnlyCollection<FieldSelectorGroup> AvailableFieldGroups
        {
            get { return _fieldGroups; }
        }

        /// <summary>
        /// Gets/Sets the selected field group
        /// </summary>
        public FieldSelectorGroup SelectedFieldGroup
        {
            get { return _selectedFieldGroup; }
            set
            {
                _selectedFieldGroup = value;
                OnPropertyChanged(SelectedFieldGroupProperty);

                OnSelectedFieldGroupChanged(value);
            }
        }

        /// <summary>
        /// Returns the collection of fields available for the selected item type.
        /// </summary>
        public ReadOnlyBulkObservableCollection<ObjectFieldInfo> SelectedGroupFields
        {
            get
            {
                if (_selectedGroupFieldsRO == null)
                {
                    _selectedGroupFieldsRO = new ReadOnlyBulkObservableCollection<ObjectFieldInfo>(_selectedGroupFields);
                }
                return _selectedGroupFieldsRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected field
        /// </summary>
        public ObjectFieldInfo SelectedField
        {
            get { return _selectedField; }
            set
            {
                _selectedField = value;
                OnPropertyChanged(SelectedFieldProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available operators.
        /// </summary>
        public ReadOnlyCollection<ObjectFieldExpressionOperator> AvailableOperators
        {
            get { return _availableOperators; }
        }

        /// <summary>
        /// Gets/Sets the text being edited by the user.
        /// </summary>
        public String FieldText
        {
            get { return _fieldFormula; }
            set
            {
                _fieldFormula = value;
                OnFieldTextChanged(value);
                OnPropertyChanged(FieldTextProperty);
            }
        }

        /// <summary>
        /// Gets the final text that will assigned.
        /// </summary>
        public String BuilderFormula
        {
            get { return _selectedPlanogramNameTemplate.BuilderFormula; }
            private set
            {
                _selectedPlanogramNameTemplate.BuilderFormula = value;
                OnPropertyChanged(BuilderFormulaProperty);
            }
        }

        public PlanogramNameTemplatePlanogramAttributeType SelectedAttribute
        {
            get { return _selectedPlanogramNameTemplate.PlanogramAttribute; }
            set
            {
                _selectedPlanogramNameTemplate.PlanogramAttribute = value;
                OnPropertyChanged(SelectedAttributeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the position of the caret on the text.
        /// </summary>
        public Int32 CaretIndex
        {
            get { return _caretIndex; }
            set
            {
                if (_caretIndex != value)
                {
                    _caretIndex = value;
                    OnPropertyChanged(CaretIndexProperty);
                }
            }
        }
        
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public PlanogramNameTemplateMaintenanceViewModel()
        {
            UpdatePermissionFlags();

            List<FieldSelectorGroup> availableFieldGroups = PlanogramNameFieldSelectorGroups();
            _allFieldsGroup = new FieldSelectorGroup(Message.PlanogramNameTemplateMaintenance_AllFields, availableFieldGroups.SelectMany(f => f.Fields).Distinct());

            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();
            groups.Add(_allFieldsGroup);
            groups.AddRange(availableFieldGroups);
            _fieldGroups = groups.AsReadOnly();

            this.SelectedFieldGroup = this.AvailableFieldGroups.First();

            _availableOperators = ObjectFieldExpression.GetSupportedOperators().AsReadOnly();

            _planogramNameTemplateInfoListViewModel.FetchAllForEntity();
            this.NewCommand.Execute();

            OnSelectedPlanogramNameTemplateChanged();
        }
        
        #endregion

        #region Commands

        #region NewCommand

        private RelayCommand _newCommand;

        /// <summary>
        ///     Gets the New command.
        /// </summary>
        public RelayCommand NewCommand
        {
            get
            {
                if (_newCommand == null)
                {
                    _newCommand = new RelayCommand(
                        p => New_Executed(),
                        p => New_CanExecute(),
                        DoNotAttachToCommandManager)
                    {

                        FriendlyName = Message.Generic_New,
                        FriendlyDescription = Message.Generic_New_Tooltip,
                        Icon = ImageResources.New_32,
                        SmallIcon = ImageResources.New_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.N
                    };
                    RegisterCommand(_newCommand);
                }
                return _newCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean New_CanExecute()
        {
            if (!_userHasPlanogramNameTemplateCreatePerm)
                _newCommand.DisabledReason = Message.Generic_NoCreatePermission;
            else
                return true;

            return false;
        }

        private void New_Executed()
        {
            if (ContinueWithItemChange())
            {
                // Create a new item.
                var newPlanogramNameTemplate = PlanogramNameTemplate.NewPlanogramNameTemplate(App.ViewState.EntityId);

                // Re-initialize as property was changed.
                newPlanogramNameTemplate.MarkGraphAsInitialized();

                this.SelectedPlanogramNameTemplate = newPlanogramNameTemplate;
                this.SelectedFieldGroup = _fieldGroups[0];

                //close the backstage
                CloseRibbonBackstage();
            }
        }

        #endregion

        #region OpenCommand

        private RelayCommand<Int32?> _openCommand;

        /// <summary>
        ///     Gets the Open command.
        /// </summary>
        public RelayCommand<Int32?> OpenCommand
        {
            get
            {
                if (_openCommand == null)
                {

                    _openCommand = new RelayCommand<Int32?>(
                        p => Open_Executed(p),
                        p => Open_CanExecute(p))
                    {
                        FriendlyName = Message.Generic_Open,
                    };
                    RegisterCommand(_openCommand);
                }
                return _openCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Open_CanExecute(Int32? itemId)
        {
            if (!_userHasPlanogramNameTemplateFetchPerm)
                _openCommand.DisabledReason = Message.Generic_NoFetchPermission;
            else if (itemId == null)
                _openCommand.DisabledReason = String.Empty;
            else
                return true;

            return false;
        }

        private void Open_Executed(Int32? itemId)
        {
            if (!ContinueWithItemChange()) return;

            base.ShowWaitCursor(true);

            try
            {
                if (itemId != null) this.SelectedPlanogramNameTemplate = PlanogramNameTemplate.FetchById(itemId.Value);
            }
            catch (DataPortalException e)
            {
                base.ShowWaitCursor(false);

                LocalHelper.RecordException(e, ExceptionCategory);
                FrameworkHelpers.ShowErrorOccurredMessage(String.Empty, OperationType.Open);

                base.ShowWaitCursor(true);
            }

            this.SelectedFieldGroup = _fieldGroups[0];

            // Close the backstage.
            CloseRibbonBackstage();

            base.ShowWaitCursor(false);
        }

        #endregion

        #region SaveCommand

        private RelayCommand _saveCommand;

        /// <summary>
        /// Saves the current item
        /// </summary>
        public RelayCommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                {
                    _saveCommand = new RelayCommand(
                        p => Save_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_Save,
                        FriendlyDescription = Message.Generic_Save_Tooltip,
                        Icon = ImageResources.Save_32,
                        SmallIcon = ImageResources.Save_16,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.S
                    };
                    RegisterCommand(_saveCommand);
                }
                return _saveCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Save_CanExecute()
        {
            //may not be null
            if (this.SelectedPlanogramNameTemplate == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                this.SaveAndNewCommand.DisabledReason = String.Empty;
                this.SaveAndCloseCommand.DisabledReason = String.Empty;
                return false;
            }

            //if item is new, must have create
            if (this.SelectedPlanogramNameTemplate.IsNew && !_userHasPlanogramNameTemplateCreatePerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoCreatePermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //if item is old, user must have edit permission
            if (!this.SelectedPlanogramNameTemplate.IsNew && !_userHasPlanogramNameTemplateEditPerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_NoEditPermission;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_NoEditPermission;
                return false;
            }

            //must be valid
            if (!this.SelectedPlanogramNameTemplate.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndNewCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                this.SaveAndCloseCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void Save_Executed()
        {
            SaveCurrentItem();
        }

        #endregion

        #region SaveAsCommand

        private RelayCommand _saveAsCommand;

        /// <summary>
        ///     Gets the SaveAs command.
        /// </summary>
        public RelayCommand SaveAsCommand
        {
            get
            {
                if (_saveAsCommand == null)
                {
                    _saveAsCommand = new RelayCommand(
                        p => SaveAs_Executed(),
                        p => SaveAs_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAs,
                        FriendlyDescription = Message.Generic_SaveAs_Tooltip,
                        Icon = ImageResources.SaveAs_32,
                        SmallIcon = ImageResources.SaveAs_16,
                        InputGestureModifiers = ModifierKeys.None,
                        InputGestureKey = Key.F12
                    };
                    RegisterCommand(_saveAsCommand);
                }
                return _saveAsCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean SaveAs_CanExecute()
        {
            //user must have create permission
            if (!_userHasPlanogramNameTemplateCreatePerm)
            {
                this.SaveCommand.DisabledReason = Message.Generic_NoCreatePermission;
                return false;
            }

            //may not be null
            if (this.SelectedPlanogramNameTemplate == null)
            {
                this.SaveCommand.DisabledReason = String.Empty;
                return false;
            }

            //must be valid
            if (!this.SelectedPlanogramNameTemplate.IsValid)
            {
                this.SaveCommand.DisabledReason = Message.Generic_Save_DisabledReasonInvalidData;
                return false;
            }

            return true;
        }

        private void SaveAs_Executed()
        {
            //** confirm the name to save as
            String copyName = null;

            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       base.ShowWaitCursor(true);

                       foreach (PlanogramNameTemplateInfo info in _planogramNameTemplateInfoListViewModel.Model)
                       {
                           if (info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       base.ShowWaitCursor(false);

                       return returnValue;
                   };


            Boolean nameAccepted = GetWindowService().PromptForSaveAsName(isUniqueCheck, String.Empty, out copyName);
            if (!nameAccepted) return;


            //Copy the item and rename
            base.ShowWaitCursor(true);

            Model.PlanogramNameTemplate itemCopy = this.SelectedPlanogramNameTemplate.Copy();
            itemCopy.Name = copyName;
            this.SelectedPlanogramNameTemplate = itemCopy;

            base.ShowWaitCursor(false);

            //save it.
            SaveCurrentItem();
        }

        #endregion

        #region SaveAndNewCommand

        private RelayCommand _saveAndNewCommand;

        /// <summary>
        ///     Gets the SaveAndNew command.
        /// </summary>
        public RelayCommand SaveAndNewCommand
        {
            get
            {
                if (_saveAndNewCommand == null)
                {
                    _saveAndNewCommand = new RelayCommand(
                        p => SaveAndNew_Executed(), 
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndNew,
                        FriendlyDescription = Message.Generic_SaveAndNew_Tooltip,
                        SmallIcon = ImageResources.SaveAndNew_16
                    };
                    RegisterCommand(_saveAndNewCommand);
                }
                return _saveAndNewCommand;
            }
        }

        private void SaveAndNew_Executed()
        {
            if (SaveCurrentItem()) NewCommand.Execute();
        }

        #endregion

        #region SaveAndCloseCommand

        private RelayCommand _saveAndCloseCommand;

        /// <summary>
        ///     Gets the SaveAndClose command.
        /// </summary>
        public RelayCommand SaveAndCloseCommand
        {
            get
            {
                if (_saveAndCloseCommand == null)
                {
                    _saveAndCloseCommand = new RelayCommand(
                        p => SaveAndClose_Executed(),
                        p => Save_CanExecute())
                    {
                        FriendlyName = Message.Generic_SaveAndClose,
                        FriendlyDescription = Message.Generic_SaveAndClose_Tooltip,
                        SmallIcon = ImageResources.SaveAndClose_16
                    };
                    RegisterCommand(_saveAndCloseCommand);
                }
                return _saveAndCloseCommand;
            }
        }

        private void SaveAndClose_Executed()
        {
            //request a save
            Boolean itemSaved = SaveCurrentItem();

            //if save completed close the window.
            if (itemSaved)
            {
                CloseWindow();
            }
        }

        #endregion

        #region DeleteCommand

        private RelayCommand _deleteCommand;

        /// <summary>
        ///     Gets the Delete command.
        /// </summary>
        public RelayCommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                {
                    _deleteCommand = new RelayCommand(
                        p => Delete_Executed(),
                        p => Delete_CanExecute())
                    {
                        FriendlyName = Message.Generic_Delete,
                        FriendlyDescription = Message.Generic_Delete_Tooltip,
                        SmallIcon = ImageResources.Delete_16
                    };
                    RegisterCommand(_deleteCommand);
                }
                return _deleteCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Delete_CanExecute()
        {
            if (!(this.SelectedPlanogramNameTemplate is PlanogramNameTemplate))
            {
                _deleteCommand.DisabledReason = String.Empty;
            }
            if (!_userHasPlanogramNameTemplateDeletePerm)
            {
                _deleteCommand.DisabledReason = Message.Generic_NoDeletePermission;
            }
            else if (this.SelectedPlanogramNameTemplate == null)
            {
                _deleteCommand.DisabledReason = String.Empty;
            }
            else if (this.SelectedPlanogramNameTemplate.IsNew)
            {
                _deleteCommand.DisabledReason = Message.Generic_Delete_DisabledIsNew;
            }
            else
            {
                return true;
            }

            return false;
        }

        private void Delete_Executed()
        {
            // Make sure the item can be deleted (only pure templates can be deleted).
            var itemToDelete = this.SelectedPlanogramNameTemplate as PlanogramNameTemplate;
            if (itemToDelete == null) return;

            //confirm with user
            if (!GetWindowService().ConfirmDeleteWithUser(this.SelectedPlanogramNameTemplate.ToString())) return;

            ShowWaitCursor(true);

            // Mark the item as deleted, so the item does not show on the list anymore.
            itemToDelete.Delete();

            // Create a new item.
            NewCommand.Execute();

            // Commit the deletion.
            try
            {
                itemToDelete.Save();
            }
            catch (DataPortalException e)
            {
                ShowWaitCursor(false);

                LocalHelper.RecordException(e, ExceptionCategory);
                FrameworkHelpers.ShowErrorOccurredMessage(itemToDelete.Name, OperationType.Delete);

                ShowWaitCursor(true);
            }

            // Update the available items.
            _planogramNameTemplateInfoListViewModel.FetchAllForEntity();

            ShowWaitCursor(false);
        }

        #endregion

        #region CloseCommand

        private RelayCommand _closeCommand;

        /// <summary>
        ///     Gets the Close command.
        /// </summary>
        public RelayCommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(p => Close_Executed())
                    {
                        FriendlyName = Message.Backstage_Close,
                        SmallIcon = ImageResources.Backstage_Close,
                        InputGestureModifiers = ModifierKeys.Control,
                        InputGestureKey = Key.F4
                    };
                    RegisterCommand(_closeCommand);
                }
                return _closeCommand;
            }
        }

        private void Close_Executed()
        {
            //close the window
            CloseWindow();
        }

        #endregion
        
        #region Clear

        private RelayCommand _clearCommand;

        /// <summary>
        /// Clears out the current formula
        /// </summary>
        public RelayCommand ClearCommand
        {
            get
            {
                if (_clearCommand == null)
                {
                    _clearCommand = new RelayCommand(
                        p => Clear_Executed())
                    {
                        FriendlyName = Message.PlanogramNameTemplateMaintenance_ClearFormula,
                        SmallIcon = ImageResources.PlanogramNameTemplateMaintenance_ClearFormula
                    };
                    RegisterCommand(_clearCommand);
                }
                return _clearCommand;
            }
        }

        private void Clear_Executed()
        {
            this.FieldText = null;
        }

        #endregion


        #endregion

        #region Methods

        public IEnumerable<PlanogramNameTemplateInfo> GetPlanogramNameTemplateMatches(String nameCriteria)
        {
            var invariantCriteria = nameCriteria.ToLowerInvariant();
            return AvailablePlanogramNameTemplates
                .Where(info => info.Name.ToLowerInvariant().Contains(invariantCriteria))
                .OrderBy(info => info.ToString());
        }

        private Boolean SaveCurrentItem(Boolean saveAsCopy = false)
        {
            Int32 itemId = this.SelectedPlanogramNameTemplate.Id;

            //** check the item unique value
            String newName;


            Predicate<String> isUniqueCheck =
                   (s) =>
                   {
                       Boolean returnValue = true;

                       ShowWaitCursor(true);

                       foreach (PlanogramNameTemplateInfo info in _planogramNameTemplateInfoListViewModel.Model)
                       {
                           if (info.Id != itemId
                               && info.Name.ToLowerInvariant() == s.ToLowerInvariant())
                           {
                               returnValue = false;
                               break;
                           }
                       }

                       ShowWaitCursor(false);

                       return returnValue;
                   };


            Boolean nameAccepted = GetWindowService().PromptIfNameIsNotUnique(isUniqueCheck, this.SelectedPlanogramNameTemplate.Name, out newName);
            if (!nameAccepted) return false;

            //set the name
            if (this.SelectedPlanogramNameTemplate.Name != newName) this.SelectedPlanogramNameTemplate.Name = newName;

            //** save the item
            ShowWaitCursor(true);

            try
            {
                this.SelectedPlanogramNameTemplate = this.SelectedPlanogramNameTemplate.Save();
            }
            catch (DataPortalException ex)
            {
                ShowWaitCursor(false);

                Exception rootException = ex.GetBaseException();
                RecordException(rootException, _exCategory);

                //if it is a concurrency exception check if the user wants to reload
                if (rootException is ConcurrencyException)
                {
                    Boolean itemReloadRequired = GetWindowService().ShowConcurrencyReloadPrompt(this.SelectedPlanogramNameTemplate.Name);
                    if (itemReloadRequired)
                    {
                        this.SelectedPlanogramNameTemplate = Model.PlanogramNameTemplate.FetchById(this.SelectedPlanogramNameTemplate.Id);
                    }
                }
                else
                {
                    //otherwise just show the user an error has occurred.
                    GetWindowService().ShowErrorOccurredMessage(this.SelectedPlanogramNameTemplate.Name, OperationType.Save);
                }

                return false;
            }

            //refresh the info list
            _planogramNameTemplateInfoListViewModel.FetchAllForEntity();

            ShowWaitCursor(false);

            return true;
        }

        /// <summary>
        ///     Shows a warning requesting the user ok to continue if the current item is dirty.
        /// </summary>
        /// <returns><c>True</c> if the action should continue, <c>false</c> otherwise.</returns>
        public Boolean ContinueWithItemChange()
        {
            return GetWindowService().ContinueWithItemChange(this.SelectedPlanogramNameTemplate, this.SaveCommand);
        }

        private Predicate<String> IsUniqueCheck(Object currentId, IEnumerable<PlanogramNameTemplateInfo> existingItems)
        {
            return newName =>
            {
                ShowWaitCursor(true);
                Boolean isDuplicateMatch = existingItems.Any(info => info.Name == newName && !currentId.Equals(info.Id));
                ShowWaitCursor(false);
                return !isDuplicateMatch;
            };
        }

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            //create
            _userHasPlanogramNameTemplateCreatePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.CreateObject, typeof(PlanogramNameTemplate));

            //fetch
            _userHasPlanogramNameTemplateFetchPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.GetObject, typeof(PlanogramNameTemplate));

            //edit
            _userHasPlanogramNameTemplateEditPerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.EditObject, typeof(PlanogramNameTemplate));

            //delete
            _userHasPlanogramNameTemplateDeletePerm = Csla.Rules.BusinessRules.HasPermission(
                Csla.Rules.AuthorizationActions.DeleteObject, typeof(PlanogramNameTemplate));

        }

        public static List<FieldSelectorGroup> PlanogramNameFieldSelectorGroups()
        {
            List<FieldSelectorGroup> groups = new List<FieldSelectorGroup>();

            // Metadata fields should be included for Planograms.
            groups.Add(new FieldSelectorGroup("Planogram", Planogram.EnumerateDisplayableFieldInfos(includeMetadata:true)));
            groups.Add(new FieldSelectorGroup("Location", Location.EnumerateDisplayableFieldInfos()));
            groups.Add(new FieldSelectorGroup("Location Space", LocationSpaceProductGroup.EnumerateDisplayableFieldInfos()));

            return groups;
        }
        
        /// <summary>
        /// Adds the given field to the text.
        /// </summary>
        /// <param name="field"></param>
        public void AddFieldToText(ObjectFieldInfo field)
        {
            String addText = field.FieldFriendlyPlaceholder;
            Int32 caretIdx = Math.Max(0, this.CaretIndex);

            StringBuilder sb = new StringBuilder(this.FieldText);

            Int32 initialSize = sb.Length;

            //When caret is precluding an operator by either 1 or 2 indexes then we increase caret to insert next field in the correct position. 
            //(Fields always succeed Operands).
            if (caretIdx > 0 && caretIdx < sb.Length && (AvailableOperators.Any(t => t.Symbol == sb[caretIdx].ToString())))
            {
                caretIdx++;
            }
            else if (caretIdx > 0 && caretIdx + 1 < sb.Length && AvailableOperators.Any(t => t.Symbol == sb[caretIdx + 1].ToString()))
            {
                caretIdx++;
                caretIdx++;
            }

            if (caretIdx >= sb.Length)
            {
                sb.Append(" ");
                sb.Append(addText);
            }
            else
            {
                sb.Insert(caretIdx, " ");
                sb.Insert(caretIdx + 1, addText);
            }

            this.FieldText = sb.ToString().Trim();

            Int32 finalSize = sb.Length;
            this.CaretIndex = caretIdx + (finalSize - initialSize);
        }

        /// <summary>
        /// Adds the given operator to the end of the field text
        /// </summary>
        /// <param name="fieldOperator"></param>
        public void AddOperator(ObjectFieldExpressionOperator fieldOperator)
        {
            if (fieldOperator.IsFunction)
            {
                if (this.FieldText == null)
                {
                    this.FieldText = string.Empty;
                }

                //add the function name plus an opening brace to the caret position
                String functionText = fieldOperator.Symbol + "(";
                //Work out index
                Int32 index = Math.Max(0, this.CaretIndex);
                //If not valid
                if (index >= this.FieldText.Length)
                {
                    //Set to max position
                    index = this.FieldText.Length;
                }
                this.FieldText = this.FieldText.Insert(index, functionText);
                this.CaretIndex = Math.Max(0, index) + functionText.Length;
                return;
            }



            String addText = fieldOperator.Symbol;
            Int32 origCaretIdx = Math.Max(0, this.CaretIndex);
            Int32 caretIdx = Math.Max(0, this.CaretIndex);
            StringBuilder sb = new StringBuilder(this.FieldText);

            Boolean addBraces = (fieldOperator.Symbol != "(" && fieldOperator.Symbol != ")");

            //Add the start brace:
            if (addBraces)
            {
                String beforeCaret = (caretIdx > 0) ? this.FieldText.Substring(0, caretIdx - 1) : String.Empty;

                //get the index of the last sq bracket
                Int32 startBraceIdx = 0;
                if (beforeCaret.Contains(ObjectFieldInfo.FieldStart))
                {
                    startBraceIdx = Math.Max(0, beforeCaret.LastIndexOf(ObjectFieldInfo.FieldStart) - 1);
                }

                //If we are not in {} allready then lets go and add them else we dont need to.
                Int32 openCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaStart);
                Int32 closeCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaEnd);

                //-1 for covering when not found (frst time only).
                if (closeCurlyIndex == -1)
                {
                    sb.Insert(startBraceIdx, ObjectFieldInfo.FormulaStart + " ");
                    caretIdx += 2;
                }
                else
                {
                    //HERE THE must be some check about ptr being after that last } else add
                    if (closeCurlyIndex < openCurlyIndex || caretIdx > closeCurlyIndex)
                    {
                        sb.Insert(startBraceIdx, ObjectFieldInfo.FormulaStart + " ");
                        caretIdx += 2;
                    }
                }

                //What is this doing and do we need it? probably wants moving into the conditions above
                if (startBraceIdx != 0)
                {
                    sb.Insert(startBraceIdx, " ");
                    caretIdx += 1;
                };
            }


            Int32 initialSize = sb.Length;

            if (caretIdx >= sb.Length)
            {
                sb.Append(" ");
                sb.Append(addText);

                //Add the end brace:
                if (addBraces)
                {
                    sb.Append(" " + ObjectFieldInfo.FormulaEnd);
                }

            }
            else
            {
                sb.Insert(caretIdx, " ");
                sb.Insert(caretIdx + 1, addText);
                sb.Insert(caretIdx + 1 + addText.Length, " ");

                //Add the end brace:
                if (addBraces)
                {
                    //Again, we only add the braces when we aren't already in a set {}.
                    Int32 openCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaStart);
                    Int32 closeCurlyIndex = sb.ToString().LastIndexOf(ObjectFieldInfo.FormulaEnd);

                    if (closeCurlyIndex < openCurlyIndex)
                    {
                        sb.Insert(caretIdx + 1 + addText.Length + 1, " " + ObjectFieldInfo.FormulaEnd);
                    }
                }
            }


            this.FieldText = sb.ToString().Trim();

            Int32 finalSize = sb.Length;

            if (addBraces)
            {
                //if braces were added place the caret just within the closing one.
                this.CaretIndex = origCaretIdx + (finalSize - initialSize) - 1;
            }
            else
            {
                this.CaretIndex = origCaretIdx + (finalSize - initialSize);
            }
        }

        private void OnSelectedPlanogramNameTemplateChanged()
        {
            if (this.SelectedPlanogramNameTemplate != null)
            {
                //set the text value
                this.FieldText = ObjectFieldInfo.ReplaceFieldsWithFriendlyPlaceholders(this.BuilderFormula, _allFieldsGroup.Fields);

                //initialize the caret to the end
                this.CaretIndex =
                    (!String.IsNullOrWhiteSpace(this.FieldText)) ?
                    this.FieldText.Length
                    : 0;
            }
        }

        
        #endregion

        #region Event Handler

        /// <summary>
        /// Called when the selected item type property value changes.
        /// </summary>
        /// <param name="newValue"></param>
        private void OnSelectedFieldGroupChanged(FieldSelectorGroup newValue)
        {
            _selectedGroupFields.Clear();

            _selectedGroupFields.AddRange(newValue.Fields);
        }

        /// <summary>
        /// Called whenever the FieldText property value changes
        /// </summary>
        /// <param name="newValue"></param>
        private void OnFieldTextChanged(String newValue)
        {
            //update the field text
            this.BuilderFormula = ObjectFieldInfo.ReplaceFriendlyPlaceholdersWithFields(this.FieldText, _allFieldsGroup.Fields);
            
            _fieldFormulaError = ObjectFieldInfo.ValidateFieldText(newValue, _allFieldsGroup.Fields, true);
        }

        #endregion


        #region IDataErrorInfo Members

        private String _error;
        public String Error
        {
            get { return _error; }
            private set
            {
                _error = value;
            }
        }

        public String this[String columnName]
        {
            get
            {
                String error = null;

                if (columnName == BuilderFormulaProperty.Path)
                {
                    error = _fieldFormulaError;
                }


                return error;
            }
        }

        #endregion


        #region IDisposable Members

        protected override void Dispose(Boolean disposing)
        {
            if (!base.IsDisposed)
            {
                if (disposing)
                {
                    _selectedPlanogramNameTemplate = null;
                    _selectedField = null;
                    _selectedFieldGroup = null;
                    _availableOperators = null;
                    _allFieldsGroup = null;
                    _fieldGroups = null;
                    _selectedGroupFields.Clear();
                    _planogramNameTemplateInfoListViewModel.Dispose();

                    DisposeBase();
                }
                base.IsDisposed = true;
            }
        }

        #endregion
    }
}