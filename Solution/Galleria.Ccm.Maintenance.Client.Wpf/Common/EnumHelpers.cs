﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace Galleria.Ccm.Maintenance.Client.Wpf
{
    /// <summary>
    /// Provides a method of retrieving the language toString overrides for enums
    /// </summary>
    public static class EnumHelpers
    {

        #region UILanguage

        private static Dictionary<String, String> _uiLanguageCodeFriendlyNames;
        /// <summary>
        /// Dictionary to map language codes to friendly names
        /// </summary>
        public static Dictionary<String, String> UILanguageCodeFriendlyNames
        {
            get
            {
                if (_uiLanguageCodeFriendlyNames == null)
                {
                    _uiLanguageCodeFriendlyNames =
                    new Dictionary<String, String>()
                        {
                            {"en-gb", new CultureInfo("en-us").NativeName}
                            //,{"en-us", new CultureInfo("en-us").NativeName},
                            //{"zh-CN",new CultureInfo("zh-CN").NativeName},
                            //{"es-MX",new CultureInfo("es-MX").NativeName}
                        };

                }
                return _uiLanguageCodeFriendlyNames;
            }
        }

        #endregion
    }
}
