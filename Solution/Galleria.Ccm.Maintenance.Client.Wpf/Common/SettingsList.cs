﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
// V8-28215 : A.Probyn
//  Updated FileName to use Constants.AppDataFolderName for app data folder.
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Common
{
    /// <summary>
    /// An object that contains the information that is saved to/from the application's
    /// user-specific xml config file. This stores details of display language, central
    /// file backup location as well as the list of databases the user has connected to
    /// in previous sessions.
    /// </summary>
    [Serializable()]
    public class SettingsList //!Singleton pattern used
    {
        #region Constants

        private const String _cConnectionsTag = "Connections";

        #endregion

        #region Fields

        //Lazy instantation of the SettingsList singleton
        private static readonly Lazy<SettingsList> lazySettings = new Lazy<SettingsList>(() => new SettingsList());
        private List<Connection> _connections = new List<Connection>(); // The list of current connections
        private String _fileName = "CCMDBUtilUsers.xml"; // the default settings file name
        private String _displayLanguage = "en-gb"; // the user's language preference
        private String _backupLocation = "C:\\"; //The user's preferred backup location
        private Boolean _displayCEIPWindow = true; // The user selects the display behaviour of the customer enhancment improvment program
        private Boolean _sendCEIPInformation = true; // The user can choose if they want to send the gibraltar information back to Galleria
        private Boolean _displayExclusiveDBUseRequiredDialogNextTime = true; // The user selects the display behaviour of the exclusive database access required dialog
        private Boolean _unitTesting = false; // indicates if we are loading the application for unit testing purposes
        #endregion

        #region Properties

        /// <summary>
        /// Instance of Settings List (Singleton)
        /// </summary>
        public static SettingsList Instance
        {
            get
            {
                return lazySettings.Value;
            }
        }

        /// <summary>
        /// Returns the full name and path of the user file
        /// </summary>
        public String Filename
        {
            get
            {
                if (_unitTesting)
                {
                    return _fileName;
                }
                else
                {
                    return Path.Combine(
                        Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                        Constants.AppDataFolderName, _fileName);
                }
            }
        }

        /// <summary>
        /// The list of connections
        /// </summary>
        public List<Connection> Connections
        {
            get { return this._connections; }
            set { this._connections = value; }
        }

        /// <summary>
        /// The users language preference
        /// </summary>
        public String DisplayLanguage
        {
            get { return _displayLanguage; }
            set
            {
                _displayLanguage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// The users preferred backup location for all databases
        /// </summary>
        public String BackupLocation
        {
            get { return _backupLocation; }
            set
            {
                _backupLocation = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// The user selects the display behaviour of the customer enhancment improvment program
        /// </summary>
        public bool DisplayCEIPWindow
        {
            get { return _displayCEIPWindow; }
            set { _displayCEIPWindow = value; }
        }

        /// <summary>
        /// The user can choose if they want to send the gibraltar information back to Galleria
        /// </summary>
        public bool SendCEIPInformation
        {
            get { return _sendCEIPInformation; }
            set { _sendCEIPInformation = value; }
        }

        /// <summary>
        /// The user selects the display behaviour of the exclusive database access required dialog
        /// </summary>
        public bool DisplayExclusiveDBUseRequiredDialogNextTime
        {
            get { return _displayExclusiveDBUseRequiredDialogNextTime; }
            set { _displayExclusiveDBUseRequiredDialogNextTime = value; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Private constructor used with Lazy instantiation of SettingsList
        /// </summary>
        private SettingsList() { }

        /// <summary>
        /// Testing constructor
        /// </summary>
        public SettingsList(Boolean IsUnitTesting, String fileName)
        {
            _unitTesting = IsUnitTesting;
            _fileName = fileName;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Loads the current users settings list
        /// </summary>
        public void LoadSettingsList()
        {
            SettingsList settingsList;
            if (_unitTesting)
            {
                settingsList = new SettingsList(true, _fileName);
            }
            else
            {
                settingsList = new SettingsList();
            }
            try
            {
                Stream stream = File.Open(this.Filename, FileMode.Open);

                XmlSerializer x = new XmlSerializer(settingsList.GetType());
                settingsList = (SettingsList)x.Deserialize(stream);

                stream.Close();
            }
            catch
            {
                //user files doesnt exist
                settingsList.SaveSettings();
            }

            _displayLanguage = settingsList._displayLanguage;
            _backupLocation = settingsList._backupLocation;
            _connections = settingsList._connections;

            _displayCEIPWindow = settingsList._displayCEIPWindow;
            _sendCEIPInformation = settingsList._sendCEIPInformation;

            _displayExclusiveDBUseRequiredDialogNextTime = settingsList._displayExclusiveDBUseRequiredDialogNextTime;
        }

        /// <summary>
        /// Saves the current users settings list
        /// </summary>
        public void SaveSettings()
        {
            try
            {
                // ensure that the database folder exists
                Directory.CreateDirectory(Path.GetDirectoryName(this.Filename));

                Stream stream = File.Open(this.Filename, FileMode.Create);
                XmlSerializer x = new XmlSerializer(this.GetType());
                x.Serialize(stream, this);

                stream.Close();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Resets the active current connection list
        /// </summary>
        public void ResetDefault()
        {
            this.DisplayCEIPWindow = true;
            this.SendCEIPInformation = true;
            this.DisplayExclusiveDBUseRequiredDialogNextTime = true;

            this.SaveSettings();
        }

        #endregion

        #region Event Handlers

        private void OnPropertyChanged()
        {
            this.SaveSettings();
        }

        #endregion
    }
}
