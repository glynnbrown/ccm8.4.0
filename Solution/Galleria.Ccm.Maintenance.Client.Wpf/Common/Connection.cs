﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Common
{
    /// <summary>
    /// A public enumeration dictating the Connection Type
    /// </summary>
    public enum ConnectionType
    {
        //LocalDatabase = 0,
        SqlDatabase = 1
        //Webservice = 2
    }

    /// <summary>
    /// Describes a serializable connection object for connecting to a Local or SQL-type database.
    /// This object is read from/written to the utility's xml file connections list.
    /// </summary>
    [Serializable()]
    public class Connection : ISerializable, INotifyPropertyChanged
    {
        #region Constants

        private const String _cTypeTag = "Type";
        private const String _cServerNameTag = "ServerName";
        private const String _cDatabaseNameTag = "DatabaseName";
        private const String _cHostNameTag = "HostName";
        private const String _cPortNumberTag = "PortNumber";
        private const String _cWebserviceNameTag = "WebserviceName";
        private const String _cFilePathTag = "FilePath";

        #endregion

        #region Fields

        private ConnectionType _type;
        private String _serverName;
        private String _databaseName;
        private String _hostName;
        private Byte _portNumber = 80;
        private String _webserviceName;
        private String _filePath;

        #endregion

        #region Properties

        /// <summary>
        /// The connection Type
        /// </summary>
        public ConnectionType Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged("Type");
            }
        }

        /// <summary>
        /// The connection server name
        /// </summary>
        public String ServerName
        {
            get { return _serverName; }
            set
            {
                _serverName = value;
                OnPropertyChanged("ServerName");
            }
        }

        /// <summary>
        /// The connection database name
        /// </summary>
        public String DatabaseName
        {
            get { return _databaseName; }
            set
            {
                _databaseName = value;
                OnPropertyChanged("DatabaseName");
            }
        }

        /// <summary>
        /// The connection host name
        /// </summary>
        public string HostName
        {
            get { return _hostName; }
            set
            {
                _hostName = value;
                OnPropertyChanged("HostName");
            }
        }

        /// <summary>
        /// The connection port number
        /// </summary>
        public Byte PortNumber
        {
            get { return _portNumber; }
            set
            {
                _portNumber = value;
                OnPropertyChanged("PortNumber");
            }
        }

        /// <summary>
        /// The connection webservice name
        /// </summary>
        public String WebserviceName
        {
            get { return _webserviceName; }
            set
            {
                _webserviceName = value;
                OnPropertyChanged("WebserviceName");
            }
        }

        /// <summary>
        /// The local db file path
        /// </summary>
        public String FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                OnPropertyChanged("FilePath");
            }
        }

        #endregion

        #region Constructor

        public Connection() { }

        public Connection(SerializationInfo info, StreamingContext context)
        {
            _type = (ConnectionType)info.GetValue(_cTypeTag, typeof(ConnectionType));
            _serverName = (String)info.GetValue(_cServerNameTag, typeof(String));
            _databaseName = (String)info.GetValue(_cDatabaseNameTag, typeof(String));
            _hostName = (String)info.GetValue(_cHostNameTag, typeof(String));
            _portNumber = (Byte)info.GetValue(_cPortNumberTag, typeof(Byte));
            _webserviceName = (String)info.GetValue(_cWebserviceNameTag, typeof(String));
            _filePath = (String)info.GetValue(_cFilePathTag, typeof(String));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Serialize this connection
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(_cTypeTag, this.Type);
            info.AddValue(_cServerNameTag, this.ServerName);
            info.AddValue(_cDatabaseNameTag, this.DatabaseName);
            info.AddValue(_cHostNameTag, this.HostName);
            info.AddValue(_cPortNumberTag, this.PortNumber);
            info.AddValue(_cWebserviceNameTag, this.WebserviceName);
            info.AddValue(_cFilePathTag, this.FilePath);
        }

        public override bool Equals(object obj)
        {
            Connection other = obj as Connection;
            if (other != null)
            {
                if ((other.Type != this.Type) || (other.ServerName != this.ServerName)
                    || (other.DatabaseName != this.DatabaseName) || (other.HostName != this.HostName)
                    || (other.PortNumber != this.PortNumber) || (other.WebserviceName != this.WebserviceName)
                    || (other.FilePath != this.FilePath))
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        public override Int32 GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
