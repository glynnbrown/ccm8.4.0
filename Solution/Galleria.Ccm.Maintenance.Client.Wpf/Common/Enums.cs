﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Common
{
    /// <summary>
    /// Enum to indicate different dialog screens
    /// </summary>
    public enum DialogScreen
    {
        Options
    }

    /// <summary>
    /// Enum to indicate which screen of a maintenance process the user is currently on
    /// </summary>
    public enum MaintenanceProcessStep
    {
        StartInfo,
        SelectLocation,
        Confirm,
        InProgress,
        Complete,
        Error,
        Cancel
    }
    /// <summary>
    /// Enum to indicate which error type is linked with the MaintenanceProcessStep Error
    /// </summary>
    public enum MaintenanceProcessErrorType
    {
        None,
        Unknown,
        IncompatibleDatabase,
        InsufficientPermissions,
        Concurrency,
        Duplicate,
        Deadlock,
        Timeout
    }
}
