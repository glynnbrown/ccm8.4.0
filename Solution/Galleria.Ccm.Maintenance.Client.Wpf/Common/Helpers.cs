﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Common
{
    /// <summary>
    /// Class containing helper methods
    /// </summary>
    public static class Helpers
    {
        /// <summary>
        /// Generates a backup filename
        /// </summary>
        /// <param name="isAutomatedBackup">If the file is auto-generrated 
        ///     (i.e. not created from a backup operation,filename should be tagged with suffix "AUTO"</param>
        /// <returns>The generated filename String</returns>
        public static String CreateBackupFileName(Boolean isAutomatedBackup)
        {
            String isAutoGenString = isAutomatedBackup ? "-AUTO" : String.Empty;
            //String hms = DateTime.Now.ToShortTimeString().Remove(2, 1);

            return String.Format("ccmdb-{0}-{1}{2}{3}-{4}{5}{6}{7}.sabak",
                /*machineName*/Environment.MachineName,
                /*year(4)*/DateTime.Now.Year,
                /*month(2)*/DateTime.Now.Month.ToString("00"),
                /*day(2)*/DateTime.Now.Day.ToString("00"),
                /*hour(2)*/DateTime.Now.Hour.ToString("00"),
                /*min(2)*/DateTime.Now.Minute.ToString("00"),
                /*sec(2)*/DateTime.Now.Second.ToString("00"),
                isAutoGenString);
        }

        /// <summary>
        /// Returns the open window of the given type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetWindow<T>()
        {
            IEnumerable<T> openWindows = App.Current.Windows.OfType<T>();
            if (openWindows.Any())
            {
                return openWindows.First();
            }
            else
            {
                return default(T);
            }
        }

    }
}
