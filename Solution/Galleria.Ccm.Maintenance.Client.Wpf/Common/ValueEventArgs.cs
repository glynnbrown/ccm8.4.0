﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Common
{
    /// <summary>
    /// Event args allowing value return
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ValueEventArgs<T> : EventArgs
    {
        private T _returnObject;

        public T ReturnObject
        {
            get { return _returnObject; }
        }

        public ValueEventArgs(T returnObject)
        {
            _returnObject = returnObject;
        }
    }
}
