﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows;
using System.IO;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using System.Globalization;
using Galleria.Framework.Helpers;
using System.Threading;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using System.Windows.Threading;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Common
{
    /// <summary>
    /// Describes a valid database that the DBMaintenance Utility can connect to.
    /// Handles the retrieval of database properties from the relevant Dal, such
    /// as version, last backup dates etc
    /// </summary>
    public class DatabaseInfo : System.ComponentModel.INotifyPropertyChanged
    {
        #region Fields
        private String _serverName;
        private String _databaseName;
        private String _databaseVersion = Message.Generic_NotAvailable;
        private String _backupLocation = Message.Generic_NotAvailable;
        private String _lastBackupDate = Message.Generic_NotAvailable;
        private String _lastMaintenenceDate = Message.Generic_NotAvailable;
        private String _localeCompliantLastBackupDate = Message.Generic_NotAvailable; //Locale aware version of the backup Date
        private String _localeCompliantLastMaintenanceDate = Message.Generic_NotAvailable; //Locale aware version of the maintenance Date
        private ObservableCollection<String> _availableDatabaseVersions = new ObservableCollection<String>();
        private ReadOnlyObservableCollection<String> _availableDatabaseVersionsRO; //readonly version to be exposed
        private DalFactoryConfigElement _dalFactoryConfig = new DalFactoryConfigElement();
        private Boolean _isAvailable;

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Property Paths
        public static readonly PropertyPath IsAvailableProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.IsAvailable);
        public static readonly PropertyPath LastBackupDateProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.LastBackupDate);
        public static readonly PropertyPath LastMaintenanceDateProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.LastMaintenanceDate);
        public static readonly PropertyPath VersionProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.Version);
        public static readonly PropertyPath FileSizeProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.FileSize);
        public static readonly PropertyPath BackupLocationProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.BackupLocation);
        public static readonly PropertyPath TypeProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.Type);
        public static readonly PropertyPath SourceConnectionProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.SourceConnection);
        public static readonly PropertyPath FileLocationProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.FileLocation);
        public static readonly PropertyPath FileNameProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.FileName);
        public static readonly PropertyPath AvailableDatabaseVersionsROProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.AvailableDatabaseVersionsRO);
        public static readonly PropertyPath ServerNameProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.ServerName);
        public static readonly PropertyPath ServerDisplayNameProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.ServerDisplayName);
        public static readonly PropertyPath DatabaseNameProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.DatabaseName);
        public static readonly PropertyPath DisplayNameProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.DisplayName);
        public static readonly PropertyPath DalFactoryConfigProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.DalFactoryConfig);
        public static readonly PropertyPath LocaleCompliantLastBackupDateProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.LocaleCompliantLastBackupDate);
        public static readonly PropertyPath LocaleCompliantLastMaintenanceDateProperty = WpfHelper.GetPropertyPath<DatabaseInfo>(p => p.LocaleCompliantLastMaintenanceDate);
        #endregion

        #region Properties
        /// <summary>
        /// The connection details of this database
        /// </summary>
        public Maintenance.Client.Wpf.Common.Connection SourceConnection { get; set; }

        /// <summary>
        /// The Type (Local, SQl etc) of this database
        /// </summary>
        public Maintenance.Client.Wpf.Common.ConnectionType Type
        {
            get { return this.SourceConnection.Type; }
        }

        /// <summary>
        /// The physical location of this database if it is a local type
        /// </summary>
        public String FileLocation
        {
            get
            {
                if (!String.IsNullOrEmpty(this.SourceConnection.FilePath))
                {
                    return Path.GetDirectoryName(this.SourceConnection.FilePath);
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        /// <summary>
        /// The name of the file containing this database, if it is a local type
        /// </summary>
        public String FileName
        {
            get
            {
                if (!String.IsNullOrEmpty(this.SourceConnection.FilePath))
                {
                    return Path.GetFileName(this.SourceConnection.FilePath);
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        /// <summary>
        /// The size of this database file in Mb, if it is a local type
        /// </summary>
        public Double? FileSize
        {
            get
            {
                Double? fileSize = null;
                if (!String.IsNullOrEmpty(this.SourceConnection.FilePath))
                {
                    System.IO.FileInfo file = new System.IO.FileInfo(this.SourceConnection.FilePath);
                    if (file.Exists)
                    {
                        fileSize = (file.Length / 1048576D);
                    }
                }
                return fileSize;
            }
        }

        /// <summary>
        /// The current schema version of this database
        /// </summary>
        public String Version
        {
            get
            {
                return _databaseVersion;
            }
            set
            {
                _databaseVersion = value;
                OnPropertyChanged(VersionProperty);
                OnDatabaseVersionChanged();
            }
        }

        /// <summary>
        /// Gives the list of available upgrade database versions for this database
        /// </summary>
        public ReadOnlyObservableCollection<String> AvailableDatabaseVersionsRO
        {
            get
            {
                if (_availableDatabaseVersionsRO == null)
                {
                    _availableDatabaseVersionsRO = new ReadOnlyObservableCollection<String>(_availableDatabaseVersions);
                }
                return _availableDatabaseVersionsRO;
            }
        }

        /// <summary>
        /// The name of the server containing this database (SQL only)
        /// </summary>
        public String ServerName
        {
            get { return _serverName; }
        }

        /// <summary>
        /// The displayed name of the server containing this database (SQL only)
        /// </summary>
        public String ServerDisplayName
        {
            get
            {
                if (!String.IsNullOrEmpty(_serverName))
                {
                    if (_serverName.Equals("."))
                    {
                        //Local SQl server shortcut used
                        return Message.Main_Status_LocalServerName;
                    }
                    else if (_serverName.StartsWith(".\\"))
                    {
                        //Local SQl Instance declared with local SQL name shortcut
                        return String.Format("{0}{1}", Message.Main_Status_LocalServerName, _serverName.Remove(0, 1));

                    }
                    else
                    {
                        return _serverName;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// The name of the database (SQL only)
        /// </summary>
        public String DatabaseName
        {
            get { return _databaseName; }
        }

        /// <summary>
        /// The name to be displayed to the user to describe this database
        /// </summary>
        public String DisplayName
        {
            get
            {
                switch (this.Type)
                {
                    //case Maintenance.Client.Wpf.Common.ConnectionType.LocalDatabase:
                    //    return this.FileName;
                    case Maintenance.Client.Wpf.Common.ConnectionType.SqlDatabase:
                        return String.Format("{0}\\{1}", ServerDisplayName, DatabaseName);
                    default: return null;
                }
            }
        }

        /// <summary>
        /// A DalFactory config element object, containing the parameters required to create a DalFactory
        /// for this database
        /// </summary>
        public DalFactoryConfigElement DalFactoryConfig
        {
            get
            {
                return _dalFactoryConfig;
            }
            private set
            {
                _dalFactoryConfig = value;
                OnDalFactoryConfigChanged();
            }
        }

        /// <summary>
        /// The user's preferred backup location for this database (Local Vista only). This is stored
        /// in the ISODBMUtilUsers.xml file
        /// </summary>
        public String BackupLocation
        {
            get
            {
                return _backupLocation;
            }
            set
            {
                _backupLocation = value;
                OnPropertyChanged(BackupLocationProperty);
            }
        }

        /// <summary>
        /// The data of the last backup of the database
        /// </summary>
        public String LastBackupDate
        {
            get
            {
                return _lastBackupDate;
            }
            set
            {
                _lastBackupDate = value;
                OnPropertyChanged(LastBackupDateProperty);
                OnLastBackupPropertyChanged();
                SaveBackupDate();
            }
        }

        /// <summary>
        /// The data of the the last compact and repair operation on this database
        /// </summary>
        public String LastMaintenanceDate
        {
            get
            {
                return _lastMaintenenceDate;
            }
            set
            {
                _lastMaintenenceDate = value;
                OnPropertyChanged(LastMaintenanceDateProperty);
                OnLastMaintenancePropertyChanged();
                SaveMaintenanceDate();
            }
        }

        /// <summary>
        /// Determines whether the database exists and is available for maintenance
        /// </summary>
        public Boolean IsAvailable
        {
            get
            {
                return _isAvailable;
            }
            private set
            {
                if (value != _isAvailable)
                {
                    _isAvailable = value;
                    OnPropertyChanged(IsAvailableProperty);
                    //If availability has changed so may have these properties
                    OnPropertyChanged(FileSizeProperty);
                }
            }
        }

        /// <summary>
        /// Locale compliant version of the last backup date for display purposes
        /// </summary>
        public String LocaleCompliantLastBackupDate
        {
            get
            {
                return _localeCompliantLastBackupDate;
            }
        }

        /// <summary>
        /// Locale compliant version of the last maintenance date for display purposes
        /// </summary>
        public String LocaleCompliantLastMaintenanceDate
        {
            get
            {
                return _localeCompliantLastMaintenanceDate;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="source"></param>
        public DatabaseInfo(Ccm.Maintenance.Client.Wpf.Common.Connection source)
        {
            this.SourceConnection = source;

            // set the SQL connection details
            if (this.Type == Maintenance.Client.Wpf.Common.ConnectionType.SqlDatabase)
            {
                _serverName = source.ServerName;
                _databaseName = source.DatabaseName;
            }
            GetDalFactoryConfig();

            CheckAvailability(true); //check if database is available
            GetBackupAndMaintenanceDates();

            GetDatabaseVersion();
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="source"></param>
        /// <param name="initialAvailability"></param>
        /// <param name="backupLocation"></param>
        public DatabaseInfo(Maintenance.Client.Wpf.Common.Connection source, Boolean initialAvailability, String backupLocation)
        {
            this.SourceConnection = source;

            // set the SQL connection details
            if (this.Type == Maintenance.Client.Wpf.Common.ConnectionType.SqlDatabase)
            {
                _serverName = source.ServerName;
                _databaseName = source.DatabaseName;
            }
            GetDalFactoryConfig();

            CheckAvailability(true); //check if database is available
            this.BackupLocation = backupLocation;

            if (initialAvailability)
            {
                GetBackupAndMaintenanceDates();
                GetDatabaseVersion();
            }
        }

        /// <summary>
        /// Test constructor
        /// </summary>
        /// <param name="source"></param>
        /// <param name="initialAvailability"></param>
        public DatabaseInfo()
        {

        }

        #endregion

        #region Methods

        /// <summary>
        /// Fetches the current version of this database
        /// </summary>
        /// <returns></returns>
        public void GetDatabaseVersion()
        {
            // Only try to get version if database is available, otherwise
            // we will create a local db instance by creating a DalFactory
            if (this.IsAvailable)
            {
                //We are creating a bespoke Dalfactory for the process
                using (IDalFactory dalFactory = DalContainer.CreateFactory(this.DalFactoryConfig))
                {
                    this.Version = dalFactory.GetDatabaseVersion();
                }
            }
        }

        /// <summary>
        /// Fetches the list of available databses for our database type
        /// </summary>
        private void UpdateAvailableDatabaseVersions()
        {
            List<String> allVersionsList = null;
            _availableDatabaseVersions.Clear();

            //Fetch a new list of available databases
            if (this.IsAvailable)
            {
                // create a bespoke Dalfactory for the process
                using (IDalFactory dalFactory = DalContainer.CreateFactory(this.DalFactoryConfig))
                {
                    allVersionsList = dalFactory.GetAvailableDatabaseScripts();
                }
            }

            //Limits the list to only versions higher then the current version
            if (allVersionsList != null)
            {
                foreach (String version in allVersionsList)
                {
                    if (Double.Parse(version, CultureInfo.InvariantCulture.NumberFormat) > Double.Parse(this._databaseVersion, CultureInfo.InvariantCulture.NumberFormat))
                    {
                        _availableDatabaseVersions.Add(version);
                    }
                }
            }
        }

        public DatabaseInfo Save()
        {
            return this;
        }

        /// <summary>
        /// Gets a correct DalFactoryConfig depending on the connection type
        /// </summary>
        /// <returns></returns>
        private DalFactoryConfigElement GetDalFactoryConfig()
        {
            switch (this.Type)
            {
                //case Maintenance.Client.Wpf.Common.ConnectionType.LocalDatabase:
                //    _dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Location", this.BackupLocation));
                //    _dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("File", Path.Combine(this.FileLocation, this.FileName)));
                //    _dalFactoryConfig.Name = "VistaDb";
                //    _dalFactoryConfig.AssemblyName = String.Format(CultureInfo.InvariantCulture, "Galleria.Ccm.Dal.{0}.dll", _dalFactoryConfig.Name);
                //    return _dalFactoryConfig;

                case Maintenance.Client.Wpf.Common.ConnectionType.SqlDatabase:
                    _dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", this.ServerName));
                    _dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", this.DatabaseName));
                    _dalFactoryConfig.Name = "Mssql";
                    _dalFactoryConfig.AssemblyName = String.Format(CultureInfo.InvariantCulture, "Galleria.Ccm.Dal.{0}.dll", _dalFactoryConfig.Name);
                    return _dalFactoryConfig;

                default: return null;
            }
        }

        /// <summary>
        /// Updates VistaDB DalFactoryConfig. (Used when file location changed)
        /// </summary>
        public void UpdateDalFactoryConfig()
        {
            //if (this.Type == Maintenance.Client.Wpf.Common.ConnectionType.LocalDatabase)
            //{
            //    //recreate the element and add new values
            //    _dalFactoryConfig = new DalFactoryConfigElement();
            //    _dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Location", this.BackupLocation));
            //    _dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("File", Path.Combine(this.FileLocation, this.FileName)));
            //    _dalFactoryConfig.Name = "VistaDb";
            //    _dalFactoryConfig.AssemblyName = String.Format(CultureInfo.InvariantCulture, "Galleria.Ccm.Dal.{0}.dll", _dalFactoryConfig.Name);
            //}
        }

        /// <summary>
        /// Gets latest backup and maintenance dates
        /// </summary>
        public void GetBackupAndMaintenanceDates()
        {
            //if (this.IsAvailable && this.Type == Maintenance.Client.Wpf.Common.ConnectionType.LocalDatabase) //at the moment this makes sense only for vistadb
            //{
            //    using (IDalFactory dalFactory = DalContainer.CreateFactory(this.DalFactoryConfig))
            //    {
            //        dalFactory.GetMaintenanceDates(out _lastBackupDate, out _lastMaintenenceDate);
            //    }
            //}
            OnLastBackupPropertyChanged();
            OnLastMaintenancePropertyChanged();
        }

        /// <summary>
        /// Save latest backup date in SystemSettings table
        /// </summary>
        private void SaveBackupDate()
        {
            //if (this.IsAvailable && this.Type == Maintenance.Client.Wpf.Common.ConnectionType.LocalDatabase) //at the moment this makes sense only for vistadb
            //{
            //    using (IDalFactory dalFactory = DalContainer.CreateFactory(this.DalFactoryConfig))
            //    {
            //        dalFactory.PersistBackupDate(_lastBackupDate);
            //    }
            //}
        }

        /// <summary>
        /// Save latest maintenance date in SystemSettings table
        /// </summary>
        private void SaveMaintenanceDate()
        {
            //if (this.IsAvailable && this.Type == Maintenance.Client.Wpf.Common.ConnectionType.LocalDatabase) //at the moment this makes sense only for vistadb
            //{
            //    using (IDalFactory dalFactory = DalContainer.CreateFactory(this.DalFactoryConfig))
            //    {
            //        dalFactory.PersistMaintenanceDate(_lastMaintenenceDate);
            //    }
            //}
        }


        /// <summary>
        /// Checks Availability of a database
        /// </summary>
        /// <param name="runSynch">If true the method runs synchronously; runs asynchronously with a priority "Background" otherwise</param>
        public Boolean CheckAvailability(Boolean runSynch)
        {
            if (runSynch)
            {
                CheckAvailabilitySynch();
            }
            else
            {
                Dispatcher.CurrentDispatcher.BeginInvoke(
                    (Action)(() =>
                    {
                        CheckAvailabilitySynch();

                    }), priority: DispatcherPriority.Background);
            }
            return this.IsAvailable;
        }

        private void CheckAvailabilitySynch()
        {
            bool result = false;

            //if (this.Type == Maintenance.Client.Wpf.Common.ConnectionType.LocalDatabase)
            //{
            //    //Check if file exists
            //    if (!String.IsNullOrEmpty(this.SourceConnection.FilePath))
            //    {
            //        System.IO.FileInfo file = new System.IO.FileInfo(this.SourceConnection.FilePath);
            //        if (!file.Exists)
            //        {
            //            result = false;
            //        }
            //    }
            //}
            //We are creating a bespoke Dalfactory for the process
            using (IDalFactory dalFactory = DalContainer.CreateFactory(this.DalFactoryConfig))
            {
                result = dalFactory.IsDatabaseAvailable();
            }

            this.IsAvailable = result;
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Triggers update of available update versions
        /// </summary>
        private void OnDatabaseVersionChanged()
        {
            UpdateAvailableDatabaseVersions();
        }

        /// <summary>
        /// Update DalFactoryConfig
        /// </summary>
        private void OnDalFactoryConfigChanged()
        {
            UpdateDalFactoryConfig();
        }

        /// <summary>
        /// INotify interface member
        /// </summary>
        /// <param name="propertyPath"></param>
        public void OnPropertyChanged(System.Windows.PropertyPath propertyPath)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyPath.Path));
            }
        }

        /// <summary>
        /// Update LocaleCompliant LastBackupDate and display to the user
        /// </summary>
        private void OnLastBackupPropertyChanged()
        {
            if (LastBackupDate != Message.Generic_NotAvailable)
            {
                Thread.CurrentThread.CurrentCulture.ClearCachedData();
                //String tempDate = DateTime.ParseExact(LastBackupDate, Constants.VistaDateFormat, Thread.CurrentThread.CurrentCulture).ToString();
                //_localeCompliantLastBackupDate = tempDate;
            }
            else
            {
                _localeCompliantLastBackupDate = LastBackupDate;
            }
            OnPropertyChanged(LocaleCompliantLastBackupDateProperty);
        }

        /// <summary>
        /// Update LocaleCompliant Last Maintenance date and display to the user
        /// </summary>
        private void OnLastMaintenancePropertyChanged()
        {
            if (LastMaintenanceDate != Message.Generic_NotAvailable)
            {
                Thread.CurrentThread.CurrentCulture.ClearCachedData();
                //String tempDate = DateTime.ParseExact(LastMaintenanceDate, Constants.VistaDateFormat, Thread.CurrentThread.CurrentCulture).ToString();
                //_localeCompliantLastMaintenanceDate = tempDate;
            }
            else
            {
                _localeCompliantLastMaintenanceDate = Message.Generic_NotAvailable;
            }
            OnPropertyChanged(LocaleCompliantLastMaintenanceDateProperty);
        }
        #endregion

        #region ToString method
        /// <summary>
        /// Returns a String describing our database
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            switch (this.Type)
            {
                //case Maintenance.Client.Wpf.Common.ConnectionType.LocalDatabase:
                //    return FileName;
                case Maintenance.Client.Wpf.Common.ConnectionType.SqlDatabase:
                    return DatabaseName;
                default: return null;
            }
        }
        #endregion
    }
}
