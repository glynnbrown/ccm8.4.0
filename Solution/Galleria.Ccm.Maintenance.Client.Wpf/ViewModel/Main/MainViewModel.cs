﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using Galleria.Ccm.Maintenance.Client.Wpf.ViewModel.Main.Commands;

namespace Galleria.Ccm.Maintenance.Client.Wpf.ViewModel.Main
{
    public class MainViewModel : DependencyObject
    {
        #region Properties
        /// <summary>
        /// The server name
        /// </summary>
        public static readonly DependencyProperty ServerNameProperty =
            DependencyProperty.Register("ServerName", typeof(String), typeof(MainViewModel));
        public String ServerName
        {
            get { return (String)GetValue(ServerNameProperty); }
            set { SetValue(ServerNameProperty, value); }
        }

        /// <summary>
        /// The database name
        /// </summary>
        public static readonly DependencyProperty DatabaseNameProperty =
            DependencyProperty.Register("DatabaseName", typeof(String), typeof(MainViewModel));
        public String DatabaseName
        {
            get { return (String)GetValue(DatabaseNameProperty); }
            set { SetValue(DatabaseNameProperty, value); }
        }

        /// <summary>
        /// The execute command
        /// </summary>
        public static readonly DependencyProperty ExecuteCommandProperty =
            DependencyProperty.Register("ExecuteCommand", typeof(ICommand), typeof(MainViewModel),
            new PropertyMetadata(new ExecuteCommand()));
        public ICommand ExecuteCommand
        {
            get { return (ICommand)GetValue(ExecuteCommandProperty); }
        }
        #endregion
    }
}
