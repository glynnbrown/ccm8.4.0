﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using System.Windows;
using Galleria.Framework.Dal.Configuration;
using Galleria.Ccm.Dal.Mssql;
using System.Windows.Input;

namespace Galleria.Ccm.Maintenance.Client.Wpf.ViewModel.Main.Commands
{
    public class ExecuteCommand : ICommand
    {
        #region Event Handlers
        /// <summary>
        /// Event raised when the state of the execute command is changed
        /// </summary>
        public event EventHandler CanExecuteChanged;
        #endregion

        #region Methods
        /// <summary>
        /// Indicates if this command can be executed
        /// </summary>
        /// <param name="parameter">An execution parameter</param>
        /// <returns>True if the command can be executed, else false</returns>
        public bool CanExecute(object parameter)
        {
            return true;
        }

        /// <summary>
        /// Executes the command
        /// </summary>
        /// <param name="parameter">The command parameter</param>
        public void Execute(object parameter)
        {
            MainViewModel viewModel = parameter as MainViewModel;
            if (viewModel != null)
            {
                this.Execute(viewModel.ServerName, viewModel.DatabaseName);
            }
        }

        /// <summary>
        /// Executes the command
        /// </summary>
        /// <param name="serverName">The database server name</param>
        /// <param name="databaseName">The database name</param>
        public void Execute(String serverName, String databaseName)
        {
            // validate
            if (serverName == null)
            {
                throw new Exception("Server name cannot be null");
            }
            if (databaseName == null)
            {
                throw new Exception("Database name cannot be null");
            }

            // create a new config element for the dal
            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement();
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", serverName));
            dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", databaseName));

            // now create a new instance of the Mssql dal
            IDalFactory dalFactory = new DalFactory(dalFactoryConfig);

            // and call the upgrade process
            dalFactory.Upgrade();

            // display a message
            MessageBox.Show("Done");
        }
        #endregion
    }
}
