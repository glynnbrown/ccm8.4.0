﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Allow the user to change application settings
    /// </summary>
    public partial class OptionsScreen : ExtendedRibbonWindow
    {
        #region ViewModel property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(OptionsViewModel), typeof(OptionsScreen),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            OptionsScreen senderControl = (OptionsScreen)obj;

            if (e.OldValue != null)
            {
                OptionsViewModel oldModel = (OptionsViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                OptionsViewModel newModel = (OptionsViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }

        }

        /// <summary>
        /// Returns the viewmodel controller for this screen
        /// </summary>
        public OptionsViewModel ViewModel
        {
            get { return (OptionsViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public OptionsScreen(Int32 initialTab)
        {
            InitializeComponent();
            this.ViewModel = new OptionsViewModel();
            this.xOptionsTabControl.SelectedIndex = initialTab;
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// User has clicked on the text to change the database's backup location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DatabaseMaintenanceBackupFolder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.ViewModel.SelectBackupFolderCommand.Execute();
        }


        /// <summary>
        /// Clears down the control safely
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //get a reference to the old model before disposing.
            IDisposable oldModel = this.ViewModel;

            //null off the viewmodel property
            //we dont want to dispose before this or we could get screen binding issues
            // plus we would end up reloading the screen twice just to close it.
            this.ViewModel = null;

            //dispose of the model
            oldModel.Dispose();

        }

        #endregion


    }
}
