﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for MoveDatabaseWindow.xaml
    /// </summary>
    public partial class MoveDatabaseWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MoveDatabaseViewModel), typeof(MoveDatabaseWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public MoveDatabaseViewModel ViewModel
        {
            get { return (MoveDatabaseViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MoveDatabaseWindow senderControl = (MoveDatabaseWindow)obj;

            if (e.OldValue != null)
            {
                MoveDatabaseViewModel oldModel = (MoveDatabaseViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                MoveDatabaseViewModel newModel = (MoveDatabaseViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public MoveDatabaseWindow(DatabaseInfo info)
        {
            InitializeComponent();

            this.ViewModel = new MoveDatabaseViewModel(info);
        }

        #endregion

        #region Window close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            //cancel the simple close
            e.Cancel = true;

            if (this.ViewModel != null)
            {
                //go through the viewmodel commands instead
                if (this.ViewModel.CancelCommand.CanExecute())
                {
                    this.ViewModel.CancelCommand.Execute();
                }
                else
                {
                    if (this.ViewModel.CurrentStep == MaintenanceProcessStep.Complete)
                    {
                        this.ViewModel.OKCommand.Execute();
                    }
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
