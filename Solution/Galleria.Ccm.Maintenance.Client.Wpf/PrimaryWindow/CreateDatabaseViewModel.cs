﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion
#region Version History: CCM802
// V8-29230 : N.Foster
//  Added option to include sample data when creating a database
#endregion
#endregion

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Windows;
using Galleria.Ccm.Dal.Mssql;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Enum to hold possible create database screens
    /// </summary>
    public enum CreateDatabaseStep
    {
        SelectDbType = 1,
        LocalEnterDetails = 2,
        LocalInProgress = 3,
        LocalComplete = 4,
        SqlEnterDetails = 5,
        SqlCreateSampleData = 6,
        SqlInProgress = 7,
        CreationFailed = 8,
        SqlComplete = 9
    }

    /// <summary>
    /// A viewModel that handles the creation of a database and adds it to the list fo available databases.
    /// This class interfaces with the relevant Dal code to allow it to manage its own databse creation.
    /// </summary>
    public sealed class CreateDatabaseViewModel : ViewModelAttachedControlObject<CreateDatabaseWindow>
    {
        #region Constants
        private const String _vistaDb = "VistaDb"; //Default vista dal name
        private const String _mssql = "Mssql"; // default sql dal name
        private const String _defaultFileName = "LocalDatabase"; // the default local database filename
        private const String _defaultFileExtension = ".sadb"; // the default local database file extension
        private const String _parameterCreateSampleData = "CreateSampleData"; // upgrade parameter to inform dal to create sample data
        #endregion

        #region Fields
        private CreateDatabaseStep _currentStep = CreateDatabaseStep.SqlEnterDetails;
        private ReadOnlyObservableCollection<ConnectionType> _availbleConnectionTypesRO;
        private ConnectionType _selectedConnectionType = ConnectionType.SqlDatabase;
        private String _locationInput1;
        private String _locationInput2;
        private bool _isCancelling;
        private BackgroundWorker _backgroundWorker;
        private String _creationFailedDetail;
        private String _defaultFileLocation = Path.Combine(
                                                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                                "CCM"); // The default file location
        private String _combinedVistaDbPath = String.Empty;
        private String _combinedSqlPath = String.Empty;
        private Boolean _createSampleData = false;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath WindowTitleProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.WindowTitle);
        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath SelectedConnectionTypeProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.SelectedConnectionType);
        public static readonly PropertyPath AvailableConnectionTypesProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.AvailableConnectionTypes);
        public static readonly PropertyPath LocationInput1Property = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.LocationInput1);
        public static readonly PropertyPath LocationInput1FriendlyNameProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.LocationInput1FriendlyName);
        public static readonly PropertyPath LocationInput2Property = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.LocationInput2);
        public static readonly PropertyPath IsPreviousCommandVisibleProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.IsPreviousCommandVisible);
        public static readonly PropertyPath IsNextCommandVisibleProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.IsNextCommandVisible);
        public static readonly PropertyPath IsCancelCommandVisibleProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.IsCancelCommandVisible);
        public static readonly PropertyPath CreationFailedDetailProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.CreationFailedDetail);
        public static readonly PropertyPath CombinedVistaDbPathProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.CombinedVistaDbPath);
        public static readonly PropertyPath CombinedSqlPathProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.CombinedSqlPath);
        public static readonly PropertyPath CreateSampleDataProperty = WpfHelper.GetPropertyPath<CreateDatabaseViewModel>(p => p.CreateSampleData);

        #endregion

        #region Properties

        /// <summary>
        /// Gets the current window title
        /// </summary>
        public String WindowTitle
        {
            get
            {
                switch (this.CurrentStep)
                {
                    case CreateDatabaseStep.SelectDbType:
                        return Message.CreateDatabase_TitleSelectDbType;

                    case CreateDatabaseStep.LocalEnterDetails:
                        return Message.CreateDatabase_TitleLocalEnterDetails;

                    case CreateDatabaseStep.SqlEnterDetails:
                        return Message.CreateDatabase_TitleSqlEnterDetails;

                    case CreateDatabaseStep.SqlCreateSampleData:
                        return Message.CreateDatabase_TitleCreateSampleData;

                    case CreateDatabaseStep.LocalInProgress:
                    case CreateDatabaseStep.SqlInProgress:
                        return Message.CreateDatabase_TitleInProgress;

                    case CreateDatabaseStep.CreationFailed:
                        return Message.CreateDatabase_TitleError;

                    case CreateDatabaseStep.LocalComplete:
                    case CreateDatabaseStep.SqlComplete:
                        return Message.CreateDatabase_TitleCompleted;

                    default:
                        throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// Gets the current screen step
        /// </summary>
        public CreateDatabaseStep CurrentStep
        {
            get { return _currentStep; }
            private set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                OnPropertyChanged(WindowTitleProperty);
                OnPropertyChanged(IsPreviousCommandVisibleProperty);
                OnPropertyChanged(IsNextCommandVisibleProperty);
                OnPropertyChanged(IsCancelCommandVisibleProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available database types
        /// </summary>
        public ReadOnlyObservableCollection<ConnectionType> AvailableConnectionTypes
        {
            get
            {
                if (_availbleConnectionTypesRO == null)
                {
                    _availbleConnectionTypesRO = new ReadOnlyObservableCollection<ConnectionType>(
                        new ObservableCollection<ConnectionType>()
                        {
                            ConnectionType.SqlDatabase
                        });
                }
                return _availbleConnectionTypesRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected database type
        /// </summary>
        public ConnectionType SelectedConnectionType
        {
            get { return _selectedConnectionType; }
            set
            {
                _selectedConnectionType = value;
                OnPropertyChanged(SelectedConnectionTypeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the first location input value
        /// </summary>
        public String LocationInput1
        {
            get { return _locationInput1; }
            set
            {
                _locationInput1 = value;
                OnPropertyChanged(LocationInput1Property);
                OnPropertyChanged(LocationInput1FriendlyNameProperty);
            }

        }

        /// <summary>
        /// Gets/Sets the second location input value
        /// </summary>
        public String LocationInput2
        {
            get { return _locationInput2; }
            set
            {
                _locationInput2 = value;
                OnPropertyChanged(LocationInput2Property);
            }
        }

        /// <summary>
        /// Friendly name conversion for UI to display (Local) instead of "." for local servers
        /// </summary>
        public String LocationInput1FriendlyName
        {
            get
            {
                if (_locationInput1 != ".")
                {
                    return _locationInput1;
                }
                else
                {
                    return Message.Main_Status_LocalServerName;
                }
            }
        }

        /// <summary>
        /// The creation failed description
        /// </summary>
        public String CreationFailedDetail
        {
            get { return _creationFailedDetail; }
            set
            {
                _creationFailedDetail = value;
                OnPropertyChanged(CreationFailedDetailProperty);
            }
        }

        /// <summary>
        /// The full location path of a VistaDB database
        /// </summary>
        public String CombinedVistaDbPath
        {
            get { return _combinedVistaDbPath; }
            set
            {
                _combinedVistaDbPath = value;
                OnPropertyChanged(CombinedVistaDbPathProperty);
            }
        }
        /// <summary>
        /// The full location path of a SQL database
        /// </summary>
        public String CombinedSqlPath
        {
            get { return _combinedSqlPath; }
            set
            {
                _combinedSqlPath = value;
                OnPropertyChanged(CombinedSqlPathProperty);
            }
        }

        /// <summary>
        /// Indicates if sample data should be created
        /// </summary>
        public Boolean CreateSampleData
        {
            get { return _createSampleData; }
            set
            {
                _createSampleData = value;
                OnPropertyChanged(CreateSampleDataProperty);
            }
        }

        #endregion

        #region Events

        public event EventHandler<ValueEventArgs<Connection>> DatabaseCreated;
        private void RaiseDatabaseCreated(Connection con)
        {
            if (this.DatabaseCreated != null)
            {
                this.DatabaseCreated(this, new ValueEventArgs<Connection>(con));
            }
        }
        #endregion

        #region Constructor

        public CreateDatabaseViewModel()
        { }

        #endregion

        #region Commands

        #region PreviousCommand

        private RelayCommand _previousCommand;

        /// <summary>
        /// Moves to the previous step
        /// </summary>
        public RelayCommand PreviousCommand
        {
            get
            {
                if (_previousCommand == null)
                {
                    _previousCommand = new RelayCommand(
                        p => Previous_Executed(),
                        p => Previous_CanExecute())
                        {
                            FriendlyName = Message.Generic_Previous
                        };
                    base.ViewModelCommands.Add(_previousCommand);
                }
                return _previousCommand;
            }
        }

        public Boolean IsPreviousCommandVisible
        {
            get { return this.Previous_CanExecute(); }
        }

        [DebuggerStepThrough]
        private Boolean Previous_CanExecute()
        {
            switch (this.CurrentStep)
            {
                case CreateDatabaseStep.SqlCreateSampleData:
                    return true;
                default:
                    return false;
            }
        }

        [DebuggerStepThrough]
        private void Previous_Executed()
        {
            this.CurrentStep = this.GetPreviousStepNumber();
        }

        #endregion


        #region NextCommand

        private RelayCommand _nextCommand;

        /// <summary>
        /// Moves to the next step
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        public bool IsNextCommandVisible
        {
            get
            {
                return
                !(this.CurrentStep == CreateDatabaseStep.LocalInProgress ||
                    this.CurrentStep == CreateDatabaseStep.SqlInProgress ||
                    this.CurrentStep == CreateDatabaseStep.CreationFailed);
            }
        }

        [DebuggerStepThrough]
        private bool Next_CanExecute()
        {
            //if on an enter details step check that they have indeed been filled
            if (this.CurrentStep == CreateDatabaseStep.LocalEnterDetails)
            {
                return
                    !String.IsNullOrEmpty(this.LocationInput1);
            }
            else if (this.CurrentStep == CreateDatabaseStep.SqlEnterDetails)
            {
                return !String.IsNullOrEmpty(this.LocationInput1) && !String.IsNullOrEmpty(this.LocationInput2);
            }

            //change the text if we are on the complete screen
            else if (this.CurrentStep == CreateDatabaseStep.LocalComplete ||
                this.CurrentStep == CreateDatabaseStep.SqlComplete)
            {
                _nextCommand.FriendlyName = Message.Generic_Ok;
            }

            return true;
        }

        private void Next_Executed()
        {
            this.CurrentStep = GetNextStepNumber();

            //if we are on the local enter step then pre-populate thelocation
            if (this.CurrentStep == CreateDatabaseStep.LocalEnterDetails)
            {
                this.LocationInput1 = _defaultFileLocation;
            }

            //if we are on an in progress set then start the process
            if (this.CurrentStep == CreateDatabaseStep.SqlInProgress)
            {
                CreateSQLDatabase();
            }
            else if (this.CurrentStep == CreateDatabaseStep.LocalInProgress)
            {
                CreateLocalDatabase();
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the create database operation
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute(), false)
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Cancel_CanExecute()
        {
            return !_isCancelling;
        }

        private void Cancel_Executed()
        {
            if (this.CurrentStep == CreateDatabaseStep.LocalInProgress ||
                this.CurrentStep == CreateDatabaseStep.SqlInProgress)
            {
                //disable the button
                _isCancelling = true;
                _cancelCommand.RaiseCanExecuteChanged();

                //start rolling back
                //[TODO]
            }
            else
            {
                //close the window
                this.AttachedControl.Close();
            }
        }

        public Boolean IsCancelCommandVisible
        {
            get
            {
                return this.CurrentStep != CreateDatabaseStep.LocalComplete &&
                        this.CurrentStep != CreateDatabaseStep.SqlComplete &&
                        this.CurrentStep != CreateDatabaseStep.SqlInProgress &&
                        this.CurrentStep != CreateDatabaseStep.LocalInProgress;
            }
        }

        #endregion

        #region BrowseForLocationFolder

        private RelayCommand _browseForLocationFolderCommand;

        /// <summary>
        /// Opens the browse folder dialog to select the location path
        /// </summary>
        public RelayCommand BrowseForLocationFolderCommand
        {
            get
            {
                if (_browseForLocationFolderCommand == null)
                {
                    _browseForLocationFolderCommand = new RelayCommand(
                        p => BrowseForLocationFolder_Executed())
                    {
                        FriendlyName = Message.CreateDatabase_BrowseForLocationFolder,
                        SmallIcon = Wpf.Resources.ImageResources.Browse_16
                    };
                    base.ViewModelCommands.Add(_browseForLocationFolderCommand);
                }
                return _browseForLocationFolderCommand;
            }
        }

        private void BrowseForLocationFolder_Executed()
        {
            System.Windows.Forms.FolderBrowserDialog diag = new System.Windows.Forms.FolderBrowserDialog();
            diag.SelectedPath = this.LocationInput1;

            System.Windows.Forms.DialogResult result = diag.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.LocationInput1 = diag.SelectedPath;
            }
        }

        #endregion

        #region OpenLocationCommand

        private RelayCommand _openLocationCommand;

        /// <summary>
        /// Shows the location in the file explorer
        /// </summary>
        public RelayCommand OpenLocationCommand
        {
            get
            {
                if (_openLocationCommand == null)
                {
                    _openLocationCommand = new RelayCommand(
                        p => OpenLocation_Executed());
                    base.ViewModelCommands.Add(_openLocationCommand);
                }
                return _openLocationCommand;
            }
        }

        private void OpenLocation_Executed()
        {
            //open the folder using windows file explorer
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "explorer.exe";
            String pathToOpen = Path.GetFullPath(this.LocationInput1);
            startInfo.Arguments = pathToOpen;
            System.Diagnostics.Process.Start(startInfo);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the number of the next step screen
        /// </summary>
        /// <returns></returns>
        private CreateDatabaseStep GetNextStepNumber()
        {
            switch (this.CurrentStep)
            {
                // database type select screen
                case CreateDatabaseStep.SelectDbType:
                    switch (this.SelectedConnectionType)
                    {
                        //case ConnectionType.LocalDatabase: return CreateDatabaseStep.LocalEnterDetails;
                        case ConnectionType.SqlDatabase: return CreateDatabaseStep.SqlEnterDetails;
                    }
                    break;

                // enter details
                case CreateDatabaseStep.LocalEnterDetails:
                case CreateDatabaseStep.SqlEnterDetails:
                    switch (this.SelectedConnectionType)
                    {
                        //case ConnectionType.LocalDatabase: return CreateDatabaseStep.LocalInProgress;
                        case ConnectionType.SqlDatabase: return CreateDatabaseStep.SqlCreateSampleData;
                    }
                    break;

                 // create sample data
                case CreateDatabaseStep.SqlCreateSampleData:
                    return CreateDatabaseStep.SqlInProgress;

                //In progress
                case CreateDatabaseStep.LocalInProgress:
                case CreateDatabaseStep.SqlInProgress:
                    switch (this.SelectedConnectionType)
                    {
                        //case ConnectionType.LocalDatabase: return CreateDatabaseStep.LocalComplete;
                        case ConnectionType.SqlDatabase: return CreateDatabaseStep.SqlComplete;
                    }
                    break;

                //completed
                case CreateDatabaseStep.LocalComplete:
                case CreateDatabaseStep.SqlComplete:
                    //close the window as have completed
                    this.AttachedControl.Close();
                    return this.CurrentStep;
            }

            //should have returned out by now
            throw new NotImplementedException();
        }

        /// <summary>
        /// Returns the number of the previous step screen
        /// </summary>
        private CreateDatabaseStep GetPreviousStepNumber()
        {
            switch (this.CurrentStep)
            {
                case CreateDatabaseStep.SqlCreateSampleData:
                    return CreateDatabaseStep.SqlEnterDetails;

                default:
                    return this.CurrentStep;
            }
        }
        #endregion

        #region Create database

        #region Create Sql database

        /// <summary>
        /// Starts off the process
        /// </summary>
        private void CreateSQLDatabase()
        {
            CombinePathForSql();

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(CreateSQLDatabase_DoWork);
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(CreateDatabase_ProgressChanged);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CreateDatabase_RunWorkerCompleted);
            _backgroundWorker.RunWorkerAsync(new String[] { this.LocationInput1, this.LocationInput2 });
        }

        /// <summary>
        /// Carries out the work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateSQLDatabase_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;
            String[] args = (String[])e.Argument;

            String serverName = args[0];
            String databaseName = args[1];

            // validate
            if (serverName == null)
            {
                throw new Exception(Message.CreateDatabase_ServerNameNotSpecifiedError);
            }
            if (databaseName == null)
            {
                throw new Exception(Message.CreateDatabase_DatabaseNameNotSpecifiedError);
            }

            try
            {
                // create a new config element for the dal
                DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement();
                dalFactoryConfig.Name = "Mssql";
                dalFactoryConfig.AssemblyName = String.Format(CultureInfo.InvariantCulture, "Galleria.Ccm.Dal.{0}.dll", dalFactoryConfig.Name);
                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", serverName));
                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", databaseName));
                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("DefaultUser", System.Security.Principal.WindowsIdentity.GetCurrent().Name));

                // now create a new instance of the Mssql dal
                using (IDalFactory dalFactory = new DalFactory(dalFactoryConfig))
                {
                    // check database is empty and can be created as a Gfs database
                    if (dalFactory.IsEmptyDatabase())
                    {
                        // and call the upgrade process
                        dalFactory.Upgrade();
                    }
                    else
                    {
                        throw new Exception(Message.CreateDatabase_DatabaseAlreadyPopulatedError);
                    }

                    // database creation has been successful
                    // now determine if we are creating sample data
                    if (this.CreateSampleData)
                    {
                        // record the current dal name
                        String previousDalName = DalContainer.DalName;

                        // register the dal factory as the default dal
                        // in the dal container, under a temporary name
                        String dalName = Guid.NewGuid().ToString();
                        DalContainer.RegisterFactory(dalName, dalFactory);
                        DalContainer.DalName = dalName;

                        try
                        {
                            // now perform authentication
                            if (!DomainPrincipal.Authenticate()) throw new DomainAuthenticationException();

                            // now we are authenticated, we can execute
                            // the create sample data process
                            IProcess createSampleDataProcess = new Galleria.Ccm.Processes.DatabaseMaintenance.CreateSampleData();

                            // execute this process
                            createSampleDataProcess.Execute();
                        }
                        catch
                        {
                            throw;
                        }
                        finally
                        {
                            // revert the dal container
                            DalContainer.RemoveFactory(dalName);
                            DalContainer.DalName = previousDalName;
                        }
                    }
                };

                // return success
                e.Result = true;
            }
            catch (Exception except)
            {
                //report out the exception
                _backgroundWorker.ReportProgress(0, except);
                e.Result = false;
            }
        }

        #endregion

        #region Create local database

        private void CreateLocalDatabase()
        {
            #region Check file exists
            Boolean fileSelected = false;

            while (!fileSelected)
            {
                if (String.IsNullOrEmpty(this.LocationInput2))
                {
                    //we're dealing with default database name
                    this._locationInput2 = _defaultFileName;
                }

                if (DatabaseAlreadyExists()) //test if db exists
                {
                    this.CurrentStep = CreateDatabaseStep.LocalEnterDetails;
                    return;
                }
                fileSelected = true;
            }

            CombinePathForVistaDb();

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(CreateLocalDatabase_DoWork);
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(CreateDatabase_ProgressChanged);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CreateDatabase_RunWorkerCompleted);
            _backgroundWorker.RunWorkerAsync(new String[] { this.LocationInput1, this.LocationInput2 });
        }

        /// <summary>
        /// Carries out the work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateLocalDatabase_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;
            String[] args = (String[])e.Argument;

            String locationName = args[0];
            String fileName = args[1];

            try
            {
                // validate
                if (String.IsNullOrEmpty(locationName))
                {
                    throw new Exception(Message.CreateDatabase_LocalEnterDetails_Description);
                }

                // create a new config element for the dal
                DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(_vistaDb);

                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("File", Path.Combine(locationName, fileName))); //fileName already contains file extension

                e.Result = true;
            }
            catch (Exception except)
            {
                //report out the exception
                _backgroundWorker.ReportProgress(0, except);
                e.Result = false;
            }
        }

            #endregion

        private void CreateDatabase_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is Exception)
            {
                if (this.AttachedControl != null)
                {
                    Exception except = (Exception)e.UserState;

                    this.CreationFailedDetail = except.Message;
                    this.CancelCommand.FriendlyName = Message.Generic_Close;
                    this.CurrentStep = CreateDatabaseStep.CreationFailed;
                }
            }
        }

        /// <summary>
        /// Handles the worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateDatabase_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            if (this.CurrentStep == CreateDatabaseStep.LocalInProgress)
            {
                worker.DoWork -= CreateLocalDatabase_DoWork;
            }
            else
            {
                worker.DoWork -= CreateSQLDatabase_DoWork;
            }
            _backgroundWorker.ProgressChanged -= CreateDatabase_ProgressChanged;
            worker.RunWorkerCompleted -= CreateDatabase_RunWorkerCompleted;

            if ((bool)e.Result)
            {
                //create the connection object
                Connection con = new Connection();

                if (this.CurrentStep == CreateDatabaseStep.LocalInProgress)
                {
                    //con.Type = ConnectionType.LocalDatabase;
                    con.FilePath = Path.Combine(this.LocationInput1, this.LocationInput2);
                }
                else
                {
                    con.Type = ConnectionType.SqlDatabase;
                    con.ServerName = this.LocationInput1;
                    con.DatabaseName = this.LocationInput2;
                }

                RaiseDatabaseCreated(con);

                //change to the next screen
                this.NextCommand.FriendlyName = Message.Generic_Ok;
                this.NextCommand.Execute();
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Checks if the current database details entered by the user match a database
        /// that already exists in the chosen location
        /// </summary>
        /// <returns></returns>
        private Boolean DatabaseAlreadyExists()
        {
            Boolean databaseAlreadyExists = false;
            String tempName = this.LocationInput2;

            AppendFileExtension(ref tempName);

            //create search path
            String path = Path.Combine(this.LocationInput1, tempName);

            //look for the file
            //if (this.SelectedConnectionType == ConnectionType.LocalDatabase)
            //{
            //    if (File.Exists(path))
            //    {
            //        databaseAlreadyExists = true;
            //    }
            //}

            //if true, inform user that the database exists
            if (databaseAlreadyExists)
            {
                Framework.Controls.Wpf.ModalMessage dialog = new Framework.Controls.Wpf.ModalMessage()
                {
                    Title = Message.Application_Title,
                    Header = Message.CreateDatabase_Dialog_DBAlreadyExistsHeader,
                    Description = String.Format(Message.CreateDatabase_Dialog_DBAlreadyExistsMessage,
                        tempName),
                    IsIconVisible = true,
                    Owner = this.AttachedControl.FindVisualAncestor<CreateDatabaseWindow>(),
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    MessageIcon = Resources.ImageResources.Dialog_Warning_24,
                    ButtonCount = 1,
                    Button1Content = Message.DialogButton_Ok,
                    Height = 200,
                    Width = 400
                };
                App.ShowWindow(dialog, true);
            }
            else
            {
                this.LocationInput2 = tempName;// if db with proposed name does not exist yet use full name (with extension if did not have one)
            }

            return databaseAlreadyExists;
        }

        /// <summary>
        /// Appends default file extension to a filename if the file already doesn't have one
        /// </summary>
        /// <param name="fileName"></param>
        private void AppendFileExtension(ref String fileName)
        {
            if (!fileName.Contains(_defaultFileExtension))
            {
                fileName += _defaultFileExtension;
            }
        }

        /// <summary>
        /// Combines LocationInput1 and LocationInput2 in a path for VistaDB databases
        /// </summary>
        private void CombinePathForVistaDb()
        {
            CombinedVistaDbPath = Path.Combine(LocationInput1, LocationInput2);
        }

        /// <summary>
        /// Combines LocationInput1FriendlyNameProperty and LocationInput2 in a path for SQL databases
        /// </summary>
        private void CombinePathForSql()
        {
            CombinedSqlPath = Path.Combine(LocationInput1FriendlyName, LocationInput2);
        }

        #endregion

        #endregion

        #region IDisposable members

        protected override void Dispose(bool disposing)
        {
            //nothing to be disposed
        }

        #endregion
    }
}
