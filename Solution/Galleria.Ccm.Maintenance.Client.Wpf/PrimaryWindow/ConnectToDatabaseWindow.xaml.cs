﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for ConnectToDatabaseWindow.xaml
    /// </summary>
    public partial class ConnectToDatabaseWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ConnectToDatabaseViewModel), typeof(ConnectToDatabaseWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public ConnectToDatabaseViewModel ViewModel
        {
            get { return (ConnectToDatabaseViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ConnectToDatabaseWindow senderControl = (ConnectToDatabaseWindow)obj;

            if (e.OldValue != null)
            {
                ConnectToDatabaseViewModel oldModel = (ConnectToDatabaseViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.DatabaseConnectionCreated -= senderControl.ViewModel_DatabaseConnectionCreated;
            }

            if (e.NewValue != null)
            {
                ConnectToDatabaseViewModel newModel = (ConnectToDatabaseViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.DatabaseConnectionCreated += senderControl.ViewModel_DatabaseConnectionCreated;
            }
        }


        #endregion

        #endregion

        #region Events

        public event EventHandler<ValueEventArgs<Connection>> DatabaseConnectionCreated;
        private void RaiseDatabaseConnectionCreated(Connection con)
        {
            if (this.DatabaseConnectionCreated != null)
            {
                this.DatabaseConnectionCreated(this, new ValueEventArgs<Connection>(con));
            }
        }

        #endregion

        #region Constructor

        public ConnectToDatabaseWindow()
        {
            InitializeComponent();

            this.ViewModel = new ConnectToDatabaseViewModel();
        }

        public ConnectToDatabaseWindow(ConnectionType startType)
        {
            InitializeComponent();

            this.ViewModel = new ConnectToDatabaseViewModel(startType);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Relays the viewmodel event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_DatabaseConnectionCreated(object sender, ValueEventArgs<Connection> e)
        {
            RaiseDatabaseConnectionCreated(e.ReturnObject);
        }


        #endregion

        #region window close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            //cancel the simple close
            e.Cancel = true;

            //execute the ok command instead
            if (this.ViewModel.CancelCommand.CanExecute())
            {
                this.ViewModel.CancelCommand.Execute();
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }



        #endregion
    }
}
