﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using System.ComponentModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Processes;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using System.IO;
using System.Diagnostics;
using Galleria.Ccm.Processes.DatabaseMaintenance;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// A viewModel that handles the physical move the files of a local (Vista) type database.
    /// It can be extended for other database types by implementing the required move methods in
    /// the releavant Dal.
    /// </summary>
    public sealed class MoveDatabaseViewModel : ViewModelAttachedControlObject<MoveDatabaseWindow>
    {
        #region Fields

        private DatabaseInfo _dbInfo;
        private BackgroundWorker _backgroundWorker;
        private MaintenanceProcessStep _currentStep = MaintenanceProcessStep.SelectLocation;
        private String _sourceFilePath;
        private String _destinationFolderPath;
        private Boolean _isProcessing;
        private Int32 _percentageComplete;
        private TimeSpan _estimatedTimeLeft;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath SourceFilePathProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.SourceFilePath);
        public static readonly PropertyPath DestinationFolderPathProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.DestinationFolderPath);
        public static readonly PropertyPath IsProcessingProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.IsProcessing);
        public static readonly PropertyPath PercentageCompleteProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.PercentageComplete);
        public static readonly PropertyPath EstimatedTimeLeftProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.EstimatedTimeLeft);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current step shown by the window
        /// </summary>
        public MaintenanceProcessStep CurrentStep
        {
            get { return _currentStep; }
            private set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
            }
        }

        /// <summary>
        /// Returns the source file path
        /// </summary>
        public String SourceFilePath
        {
            get { return _sourceFilePath; }
        }

        /// <summary>
        /// Gets/Sets the destination folder of the move
        /// </summary>
        public String DestinationFolderPath
        {
            get { return _destinationFolderPath; }
            set
            {
                _destinationFolderPath = value;
                OnPropertyChanged(DestinationFolderPathProperty);
            }
        }

        /// <summary>
        /// Returns true if the process has been started
        /// </summary>
        public Boolean IsProcessing
        {
            get { return _isProcessing; }
            private set
            {
                _isProcessing = value;
                OnPropertyChanged(IsProcessingProperty);
            }
        }

        /// <summary>
        /// Returns the percentage completion value for the process
        /// </summary>
        public Int32 PercentageComplete
        {
            get { return _percentageComplete; }
            private set
            {
                _percentageComplete = value;
                OnPropertyChanged(PercentageCompleteProperty);
            }
        }

        /// <summary>
        /// Returns an estimated time left for the process
        /// </summary>
        public TimeSpan EstimatedTimeLeft
        {
            get { return _estimatedTimeLeft; }
            private set
            {
                _estimatedTimeLeft = value;
                OnPropertyChanged(EstimatedTimeLeftProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        ///<param name="dbInfo">the info object for the db to move</param>
        public MoveDatabaseViewModel(DatabaseInfo dbInfo)
        {
            _dbInfo = dbInfo;
            _sourceFilePath = dbInfo.SourceConnection.FilePath;
        }

        #endregion

        #region Command

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Starts the move and closes the window when complete
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OKCommand_Executed(),
                        p => OKCommand_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);

                }
                return _okCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean OKCommand_CanExecute()
        {
            //only enable if the move destination is populated 
            //and we are not already processing
            return !this.IsProcessing &&
                !String.IsNullOrEmpty(this.DestinationFolderPath)
                || this.CurrentStep == MaintenanceProcessStep.Cancel || this.CurrentStep == MaintenanceProcessStep.Error;
        }

        private void OKCommand_Executed()
        {
            if (this.CurrentStep == MaintenanceProcessStep.SelectLocation)
            {
                this.IsProcessing = true;
                MoveDatabase(this.SourceFilePath, this.DestinationFolderPath);
            }
            else //if (this.CurrentStep == MaintenanceProcessStep.Complete || this.CurrentStep == MaintenanceProcessStep.Cancel)
            {
                //close the window as we are done
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the process and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Cancel_CanExecute()
        {
            //enable if we are not processing and are on the 
            //select location screen
            return (this.CurrentStep == MaintenanceProcessStep.SelectLocation &&
                    !this.IsProcessing);
        }

        private void Cancel_Executed()
        {
            //close the window
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region SelectMoveDestinationCommand

        private RelayCommand _selectMoveDestinationCommand;

        /// <summary>
        /// Shows the open folder dialog so that the destination location
        /// may be selected
        /// </summary>
        public RelayCommand SelectMoveDestinationCommand
        {
            get
            {
                if (_selectMoveDestinationCommand == null)
                {
                    _selectMoveDestinationCommand = new RelayCommand(
                        p => SelectMoveDestination_Executed())
                    {
                        FriendlyName = Message.Generic_Browse
                    };
                    base.ViewModelCommands.Add(_selectMoveDestinationCommand);
                }
                return _selectMoveDestinationCommand;
            }
        }

        private void SelectMoveDestination_Executed()
        {
            Boolean canExitMethod = false;

            while (!canExitMethod)
            {
                //show the open folder dialog
                System.Windows.Forms.FolderBrowserDialog fileSelectDialog = new System.Windows.Forms.FolderBrowserDialog();
                fileSelectDialog.SelectedPath =
                    (!String.IsNullOrEmpty(this.DestinationFolderPath)) ? this.DestinationFolderPath : this.SourceFilePath;

                System.Windows.Forms.DialogResult result = fileSelectDialog.ShowDialog();

                //if a folder was selected then set as the move destination
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    //Check directory exists
                    Boolean pathIsValid = Directory.Exists(fileSelectDialog.SelectedPath);
                    canExitMethod = pathIsValid;
                    if (!pathIsValid)
                    {
                        //Inform User that directory does not exist
                        //Inform user path is same as current database path
                        Framework.Controls.Wpf.ModalMessage dialog = new Framework.Controls.Wpf.ModalMessage()
                        {
                            Title = Message.Application_Title,
                            Header = Message.MoveDatabase_Dialog_DestinationPathDoesNotExistHeader,
                            Description = String.Format(Message.MoveDatabase_Dialog_DestinationPathDoesNotExistMessage,
                                fileSelectDialog.SelectedPath),
                            IsIconVisible = true,
                            WindowStartupLocation = WindowStartupLocation.CenterScreen,
                            MessageIcon = Resources.ImageResources.Dialog_Warning_24,
                            ButtonCount = 1,
                            Button1Content = Message.DialogButton_Ok,
                            Height = 200,
                            Width = 400
                        };
                        App.ShowWindow(dialog, true);
                    }

                    //Check path is not the same as current location
                    if (pathIsValid)
                    {
                        pathIsValid = String.Compare(fileSelectDialog.SelectedPath, Path.GetDirectoryName(this.SourceFilePath), true) != 0;
                        canExitMethod = pathIsValid;
                        if (!pathIsValid)
                        {
                            //Inform user path is same as current database path
                            Framework.Controls.Wpf.ModalMessage dialog = new Framework.Controls.Wpf.ModalMessage()
                            {
                                Title = Message.Application_Title,
                                Header = Message.MoveDatabase_Dialog_DestinationPathSameAsSourceHeader,
                                Description = String.Format(Message.MoveDatabase_Dialog_DestinationPathSameAsSourceMessage,
                                    fileSelectDialog.SelectedPath),
                                IsIconVisible = true,
                                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                                MessageIcon = Resources.ImageResources.Dialog_Warning_24,
                                ButtonCount = 1,
                                Button1Content = Message.DialogButton_Ok,
                                Height = 200,
                                Width = 400
                            };
                            App.ShowWindow(dialog, true);
                        }
                    }

                    //Check file does not already exist in destination location
                    if (pathIsValid)
                    {
                        String destinationFilePath = Path.Combine(fileSelectDialog.SelectedPath, Path.GetFileName(this.SourceFilePath));
                        pathIsValid = !File.Exists(destinationFilePath);
                        canExitMethod = pathIsValid;
                        if (!pathIsValid)
                        {
                            //Inform user path is same as current database path
                            Framework.Controls.Wpf.ModalMessage dialog = new Framework.Controls.Wpf.ModalMessage()
                            {
                                Title = Message.Application_Title,
                                Header = Message.MoveDatabase_Dialog_DestinationFileAlreadyExistsHeader,
                                Description = String.Format(Message.MoveDatabase_Dialog_DestinationFileAlreadyExistsMessage,
                                    fileSelectDialog.SelectedPath, Path.GetFileName(this.SourceFilePath)),
                                IsIconVisible = true,
                                WindowStartupLocation = WindowStartupLocation.CenterScreen,
                                MessageIcon = Resources.ImageResources.Dialog_Warning_24,
                                ButtonCount = 1,
                                Button1Content = Message.DialogButton_Ok,
                                Height = 200,
                                Width = 400
                            };
                            App.ShowWindow(dialog, true);
                        }
                    }
                    //if all is well, set the destination path
                    if (pathIsValid)
                    {
                        this.DestinationFolderPath = fileSelectDialog.SelectedPath;
                    }
                }
                else
                {
                    //user cancelled out of file selection
                    canExitMethod = true;
                    this.DestinationFolderPath = null;
                }
            }
        }

        #endregion

        #region ShowLocationCommand

        private RelayCommand _showLocationCommand;

        /// <summary>
        /// Opens the given folder location in file explorer
        /// </summary>
        public RelayCommand ShowLocationCommand
        {
            get
            {
                if (_showLocationCommand == null)
                {
                    _showLocationCommand = new RelayCommand
                        (p => ShowLocation_Executed(p as String));
                    base.ViewModelCommands.Add(_showLocationCommand);
                }
                return _showLocationCommand;
            }
        }

        /// <summary>
        /// Open the location given by path
        /// </summary>
        /// <param name="path"></param>
        private void ShowLocation_Executed(String path)
        {
            String folder = path;
            if (File.Exists(path))
            {
                folder = Path.GetDirectoryName(path);
            }
            System.Diagnostics.Process.Start("explorer.exe", folder);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Starts the database move process
        /// </summary>
        /// <param name="source">the source file path</param>
        /// <param name="destination">the destination folder path</param>
        private void MoveDatabase(String source, String destination)
        {
            this.EstimatedTimeLeft = new TimeSpan(10000);

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            _backgroundWorker.RunWorkerAsync();
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;

            //create the process to run
            MoveDatabaseProcess moveProcess = new MoveDatabaseProcess();
            moveProcess.DesinationFolderPath = this.DestinationFolderPath;
            //Set the DalFactoryconfig to match the setup decribed in our DatabaseInfo object - this
            //will ensure that the process is run against the correct database
            moveProcess.DalFactoryConfig = _dbInfo.DalFactoryConfig;

            moveProcess.OperationCompleted += new EventHandler<ProcessProgressEventArgs>(process_OperationCompleted);

            //Check if the database is still available
            if (this._dbInfo.CheckAvailability(true))
            {
                //sent an initial status report
                _backgroundWorker.ReportProgress(0, moveProcess.StepDescriptions[1]);
                //start the execution
                ProcessFactory.Execute(moveProcess);
            }
            else
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Responds to process progress notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void process_OperationCompleted(object sender, ProcessProgressEventArgs e)
        {
            MoveDatabaseProcess moveProcess = (MoveDatabaseProcess)sender;

            if (this._dbInfo.IsAvailable)
            {
                //get the percentage completed & the next step description
                Double percentage = ((Double)e.CurrentProgress / (Double)e.MaxProgress) * 100;
                String nextStepStatus =
                    (e.CurrentProgress != e.MaxProgress) ?
                    moveProcess.StepDescriptions[e.CurrentProgress + 1] : String.Empty;

                //notify out the progress
                _backgroundWorker.ReportProgress((Int32)percentage, nextStepStatus);

                //unsubscribe the handler if this was the last step
                if (e.CurrentProgress == e.MaxProgress)
                {
                    moveProcess.OperationCompleted -= process_OperationCompleted;
                }
            }
            else
            {
                moveProcess.OperationCompleted -= process_OperationCompleted;
            }
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.PercentageComplete += 10;
            this.EstimatedTimeLeft = new TimeSpan(1000);
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= worker_DoWork;
            worker.RunWorkerCompleted -= worker_RunWorkerCompleted;
            worker.ProgressChanged -= worker_ProgressChanged;

            if (e.Cancelled)
            {
                this.CurrentStep = MaintenanceProcessStep.Cancel;
            }
            else if (e.Error != null)
            {
                this.CurrentStep = MaintenanceProcessStep.Error;
            }
            else
            {
                //Update the connection details to the new settings
                //Get the id of the old connection first - once we have changed its path it will be 
                //hard to find
                SettingsList _settingsList = SettingsList.Instance;
                _settingsList.LoadSettingsList();

                Int32 connectionId = _settingsList.Connections.IndexOf(_dbInfo.SourceConnection);

                //update the dbinfo object
                _dbInfo.SourceConnection.FilePath =
                    Path.Combine(this.DestinationFolderPath, Path.GetFileName(this.SourceFilePath));

                //Update the connection details to the new settings
                _settingsList.Connections[connectionId].FilePath = _dbInfo.SourceConnection.FilePath;
                //Save the changes
                _settingsList.SaveSettings();

                //move to the completed screen
                this.IsProcessing = false;
                //change to completed screen if not cancelled

                this.CurrentStep = MaintenanceProcessStep.Complete;
                this._dbInfo.UpdateDalFactoryConfig();
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            //nothing to unsubscribe
        }

        #endregion
    }
}
