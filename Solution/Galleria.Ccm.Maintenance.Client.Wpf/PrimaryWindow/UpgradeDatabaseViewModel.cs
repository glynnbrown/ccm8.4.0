﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using System.ComponentModel;
using Galleria.Framework.Processes;
using Galleria.Framework.Dal;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using Galleria.Ccm.Processes.DatabaseMaintenance;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// A viewModel that handles the upgrade of an available database.
    /// </summary>
    public sealed class UpgradeDatabaseViewModel : ViewModelAttachedControlObject<UpgradeDatabaseWindow>
    {
        #region Constants

        private const String _upgradeDalFactoryName = "UpgradeDalFactory";

        #endregion

        #region Fields
        private BackgroundWorker _backgroundWorker;
        private DatabaseInfo _dbInfo;
        private MaintenanceProcessStep _currentStep = MaintenanceProcessStep.StartInfo;
        private String _currentVersion;
        private String _futureVersion;
        private Int32 _percentageComplete;
        private TimeSpan _estimatedTimeLeft;
        private CultureInfo _invariantCultureProvider = CultureInfo.InvariantCulture;
        private MaintenanceProcessErrorType _errorType = MaintenanceProcessErrorType.None;
        private String _databaseUpgradeErrorHeader = Message.UpgradeDatabase_Error_DefaultHeader;
        private String _databaseUpgradeErrorDescription = Message.UpgradeDatabase_Error_DefaultDescription;
        private String _databaseUpgradeErrorDetails = String.Empty;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<UpgradeDatabaseViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath WindowTitleProperty = WpfHelper.GetPropertyPath<UpgradeDatabaseViewModel>(p => p.WindowTitle);
        public static readonly PropertyPath CurrentVersionProperty = WpfHelper.GetPropertyPath<UpgradeDatabaseViewModel>(p => p.CurrentVersion);
        public static readonly PropertyPath FutureVersionProperty = WpfHelper.GetPropertyPath<UpgradeDatabaseViewModel>(p => p.FutureVersion);
        public static readonly PropertyPath IsProcessingProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.IsProcessing);
        public static readonly PropertyPath PercentageCompleteProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.PercentageComplete);
        public static readonly PropertyPath EstimatedTimeLeftProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.EstimatedTimeLeft);
        public static readonly PropertyPath AvailableVersionsROProperty = WpfHelper.GetPropertyPath<UpgradeDatabaseViewModel>(p => p.AvailableVersionsRO);
        public static readonly PropertyPath IsUpgradePossibleProperty = WpfHelper.GetPropertyPath<UpgradeDatabaseViewModel>(p => p.IsUpgradePossible);
        public static readonly PropertyPath UpgradeErrorHeaderProperty = WpfHelper.GetPropertyPath<UpgradeDatabaseViewModel>(p => p.UpgradeErrorHeader);
        public static readonly PropertyPath UpgradeErrorDescriptionProperty = WpfHelper.GetPropertyPath<UpgradeDatabaseViewModel>(p => p.UpgradeErrorDescription);
        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the current step to be displayed
        /// </summary>
        public MaintenanceProcessStep CurrentStep
        {
            get { return _currentStep; }
            private set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                OnPropertyChanged(WindowTitleProperty);
                OnPropertyChanged(UpgradeErrorHeaderProperty);
                OnPropertyChanged(UpgradeErrorDescriptionProperty);
                PromptButtonsCanExecuteRefresh();
            }
        }

        /// <summary>
        /// Returns the window title based on the current step
        /// </summary>
        public String WindowTitle
        {
            get
            {
                switch (this.CurrentStep)
                {
                    case MaintenanceProcessStep.StartInfo:
                        return Message.UpgradeDatabase_StartInfo_Title;
                    case MaintenanceProcessStep.Error:
                        return Message.UpgradeDatabase_Error_Title;
                    case MaintenanceProcessStep.InProgress:
                        return Message.UpgradeDatabase_InProgress_Title;
                    case MaintenanceProcessStep.Complete:
                        return Message.UpgradeDatabase_Completed_Title;
                    default: throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// Returns the error window header based on the error type
        /// </summary>
        public String UpgradeErrorHeader
        {
            get
            {
                switch (_errorType)
                {
                    case MaintenanceProcessErrorType.None:
                        return String.Empty;
                    case MaintenanceProcessErrorType.InsufficientPermissions:
                        return Message.UpgradeDatabase_Error_InsufficientPermissions_Header;
                    case MaintenanceProcessErrorType.Timeout:
                        return Message.UpgradeDatabase_Error_Timeout_Header;
                    case MaintenanceProcessErrorType.Concurrency:
                        return Message.UpgradeDatabase_Error_Concurrency_Header;
                    case MaintenanceProcessErrorType.Deadlock:
                        return Message.UpgradeDatabase_Error_Deadlock_Header;
                    case MaintenanceProcessErrorType.Duplicate:
                        return Message.UpgradeDatabase_Error_Duplicate_Header;
                    case MaintenanceProcessErrorType.IncompatibleDatabase:
                        return Message.UpgradeDatabase_Error_IncompatibleDatabase_Header;
                    case MaintenanceProcessErrorType.Unknown:
                        return Message.UpgradeDatabase_Error_DefaultHeader;

                    default: throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// Returns the error window description based on the error type
        /// </summary>
        public String UpgradeErrorDescription
        {
            get
            {
                switch (_errorType)
                {
                    case MaintenanceProcessErrorType.None:
                        return String.Empty;
                    case MaintenanceProcessErrorType.InsufficientPermissions:
                        return String.Format(_invariantCultureProvider, Message.UpgradeDatabase_Error_InsufficientPermissions_Description,
                            this.CurrentVersion, this.FutureVersion);
                    case MaintenanceProcessErrorType.Timeout:
                        return String.Format(_invariantCultureProvider, Message.UpgradeDatabase_Error_Timeout_Description,
                           this.CurrentVersion, this.FutureVersion);
                    case MaintenanceProcessErrorType.Concurrency:
                        return String.Format(_invariantCultureProvider, Message.UpgradeDatabase_Error_Concurrency_Description,
                           this.CurrentVersion, this.FutureVersion);
                    case MaintenanceProcessErrorType.Deadlock:
                        return String.Format(_invariantCultureProvider, Message.UpgradeDatabase_Error_Deadlock_Description,
                           this.CurrentVersion, this.FutureVersion);
                    case MaintenanceProcessErrorType.Duplicate:
                        return String.Format(_invariantCultureProvider, Message.UpgradeDatabase_Error_Duplicate_Description,
                           this.CurrentVersion, this.FutureVersion);
                    case MaintenanceProcessErrorType.IncompatibleDatabase:
                        return String.Format(_invariantCultureProvider, Message.UpgradeDatabase_Error_IncompatibleDatabase_Description,
                          this.CurrentVersion, this.FutureVersion);
                    case MaintenanceProcessErrorType.Unknown:
                        return String.Format(_invariantCultureProvider, Message.UpgradeDatabase_Error_DefaultDescription,
                           this.CurrentVersion, this.FutureVersion, _databaseUpgradeErrorDetails);

                    default: throw new NotImplementedException();
                }
            }
        }

        /// <summary>
        /// Returns the current version of the database to be upgraded
        /// </summary>
        public String CurrentVersion
        {
            get { return _currentVersion; }
            private set
            {
                _currentVersion = value;
                OnPropertyChanged(CurrentVersionProperty);
            }
        }

        /// <summary>
        /// Returns the version that the database will be upgraded to
        /// </summary>
        public String FutureVersion
        {
            get { return _futureVersion; }
            set
            {
                _futureVersion = value;
                OnPropertyChanged(FutureVersionProperty);
            }
        }


        /// <summary>
        /// Returns collection of update to versions
        /// </summary>
        public ReadOnlyObservableCollection<String> AvailableVersionsRO
        {
            get { return _dbInfo.AvailableDatabaseVersionsRO; }
        }

        /// <summary>
        /// Returns the version that the database will be upgraded to
        /// </summary>
        public Boolean IsUpgradePossible
        {
            get { return _dbInfo.AvailableDatabaseVersionsRO.Count != 0; }
        }

        /// <summary>
        /// Returns the percentage completion value for the process
        /// </summary>
        public Int32 PercentageComplete
        {
            get { return _percentageComplete; }
            private set
            {
                _percentageComplete = value;
                OnPropertyChanged(PercentageCompleteProperty);
            }
        }

        /// <summary>
        /// Returns an estimated time left for the process
        /// </summary>
        public TimeSpan EstimatedTimeLeft
        {
            get { return _estimatedTimeLeft; }
            private set
            {
                _estimatedTimeLeft = value;
                OnPropertyChanged(EstimatedTimeLeftProperty);
            }
        }

        #endregion

        #region Constructor

        public UpgradeDatabaseViewModel(DatabaseInfo databaseInfo)
        {
            _dbInfo = databaseInfo;

            this.CurrentVersion = _dbInfo.Version;
            if (AvailableVersionsRO != null && AvailableVersionsRO.Count != 0)
            {
                this.FutureVersion = AvailableVersionsRO[0];
            }

            this.CurrentStep = MaintenanceProcessStep.StartInfo;
        }

        /// <summary>
        /// test constructor
        /// </summary>
        /// <param name="databaseInfo"></param>
        public UpgradeDatabaseViewModel()
        {

        }

        #endregion

        #region Commands

        #region UpgradeCommand

        private RelayCommand _upgradeCommand;

        /// <summary>
        /// Starts the upgrade process
        /// </summary>
        public RelayCommand UpgradeCommand
        {
            get
            {
                if (_upgradeCommand == null)
                {
                    _upgradeCommand = new RelayCommand(
                        p => Upgrade_Executed(),
                        p => Upgrade_CanExecute())
                    {
                        FriendlyName = Message.UpgradeDatabase_Upgrade
                    };
                    base.ViewModelCommands.Add(_upgradeCommand);
                }
                return _upgradeCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Upgrade_CanExecute()
        {
            return (this.CurrentStep == MaintenanceProcessStep.StartInfo && IsUpgradePossible);
        }

        private void Upgrade_Executed()
        {
            this.CurrentStep = MaintenanceProcessStep.InProgress;

            this.EstimatedTimeLeft = new TimeSpan(10000);

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            _backgroundWorker.RunWorkerAsync();
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels this window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Cancel_CanExecute()
        {
            return this.CurrentStep != MaintenanceProcessStep.InProgress &&
                this.CurrentStep != MaintenanceProcessStep.Complete && this.CurrentStep != MaintenanceProcessStep.Error;
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region OKCommand

        private RelayCommand _okCommand;

        /// <summary>
        /// Closes the window on complete
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_okCommand == null)
                {
                    _okCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_okCommand);
                }
                return _okCommand;
            }
        }

        [DebuggerStepThrough]
        private bool OK_CanExecute()
        {
            return this.CurrentStep == MaintenanceProcessStep.Complete || this.CurrentStep == MaintenanceProcessStep.Cancel
                || this.CurrentStep == MaintenanceProcessStep.Error;
        }

        private void OK_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Starts off the process
        /// </summary>
        public void BeginProcess()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            _backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Forces CanExecute checks for Ok and Cancel commands as statuses weren't refreshing on time
        /// </summary>
        private void PromptButtonsCanExecuteRefresh()
        {
            if (_okCommand != null)
            {
                _okCommand.RaiseCanExecuteChanged();
            }
            if (_cancelCommand != null)
            {
                _cancelCommand.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out the work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;

            //create the process to run
            UpgradeDatabaseProcess upgradeProcess = new UpgradeDatabaseProcess();
            upgradeProcess.upgradeToVersion = _futureVersion;
            //Set the DalFactoryconfig to match the setup decribed in our DatabaseInfo object - this
            //will ensure that the process is run against the correct database
            upgradeProcess.DalFactoryConfig = _dbInfo.DalFactoryConfig;

            upgradeProcess.OperationCompleted += new EventHandler<ProcessProgressEventArgs>(process_OperationCompleted);

            //ISO-13490 check if the database is still available
            if (this._dbInfo.CheckAvailability(true))
            {
                //sent an initial status report
                _backgroundWorker.ReportProgress(0, upgradeProcess.StepDescriptions[1]);

                //create the DalFactory instance for the database we will execute against
                IDalFactory dalFactory = DalContainer.CreateFactory(upgradeProcess.DalFactoryConfig);

                // register this factory within the dal container
                DalContainer.RegisterFactory(_upgradeDalFactoryName, dalFactory);

                // set this factory to be the default of the container
                DalContainer.DalName = _upgradeDalFactoryName;

                //Authenticate the user - this authenticates against the default database
                Galleria.Ccm.Security.DomainPrincipal.Authenticate();

                //start the execution
                ProcessFactory.Execute(upgradeProcess);
            }
            else
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Responds to process progress notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void process_OperationCompleted(object sender, ProcessProgressEventArgs e)
        {
            UpgradeDatabaseProcess upgradeProcess = (UpgradeDatabaseProcess)sender;

            if (this._dbInfo.IsAvailable)
            {
                //get the percentage completed & the next step description
                Double percentage = ((Double)e.CurrentProgress / (Double)e.MaxProgress) * 100;
                String nextStepStatus =
                    (e.CurrentProgress != e.MaxProgress) ?
                    upgradeProcess.StepDescriptions[e.CurrentProgress + 1] : String.Empty;

                //notify out the progress
                _backgroundWorker.ReportProgress((Int32)percentage, nextStepStatus);

                //unsubscribe the handler if this was the last step
                if (e.CurrentProgress == e.MaxProgress)
                {
                    upgradeProcess.OperationCompleted -= process_OperationCompleted;
                }

                //_dbInfo.LastBackupDate = DateTime.Now.ToString(Constants.VistaDateFormat, _invariantCultureProvider);
            }
            else
            {
                upgradeProcess.OperationCompleted -= process_OperationCompleted;
            }
        }


        /// <summary>
        /// Handles the worker progress changed notifications
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.PercentageComplete = e.ProgressPercentage;
            this.EstimatedTimeLeft = new TimeSpan(1000);
            //this.CurrentStatusDescription = (String)e.UserState;
        }

        /// <summary>
        /// Handles the worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= worker_DoWork;
            worker.RunWorkerCompleted -= worker_RunWorkerCompleted;
            worker.ProgressChanged -= worker_ProgressChanged;

            _dbInfo = _dbInfo.Save();

            //reset error
            _errorType = MaintenanceProcessErrorType.None;
            _databaseUpgradeErrorDetails = String.Empty;

            if (e.Error != null)
            {
                // determine error type
                if (e.Error is Galleria.Framework.Dal.SqlPermissionException)
                {
                    _errorType = MaintenanceProcessErrorType.InsufficientPermissions;
                }
                else if (e.Error is Galleria.Framework.Dal.ConcurrencyException)
                {
                    _errorType = MaintenanceProcessErrorType.Concurrency;
                }
                else if (e.Error is Galleria.Framework.Dal.DeadlockException)
                {
                    _errorType = MaintenanceProcessErrorType.Deadlock;
                }
                else if (e.Error is Galleria.Framework.Dal.DuplicateException)
                {
                    _errorType = MaintenanceProcessErrorType.Duplicate;
                }
                else if (e.Error is Galleria.Framework.Dal.TimeoutException)
                {
                    _errorType = MaintenanceProcessErrorType.Timeout;
                }
                else if ((e.Error as System.Data.SqlClient.SqlException) != null
                    && (e.Error as System.Data.SqlClient.SqlException).Number == 1088)
                {
                    //  originally the incompatible database was the default type
                    _errorType = MaintenanceProcessErrorType.IncompatibleDatabase;
                }
                else
                {
                    _errorType = MaintenanceProcessErrorType.Unknown;
                }
                _databaseUpgradeErrorDetails = e.Error.Message;
                // set current step to error
                this.CurrentStep = MaintenanceProcessStep.Error;
            }
            else if (e.Cancelled)
            {
                this.CurrentStep = MaintenanceProcessStep.Cancel;
            }
            else
            {
                this.CurrentStep = MaintenanceProcessStep.Complete;
                _dbInfo.GetDatabaseVersion();
            }

            //remove connection from the DalContainer
            IDalFactory dalFactory;
            dalFactory = DalContainer.GetDalFactory(_upgradeDalFactoryName);
            if (dalFactory != null)
            {
                DalContainer.RemoveFactory(_upgradeDalFactoryName);
            }
        }

        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            this.AttachedControl = null;
        }
        #endregion
    }
}
