﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for MainPageOrganiser.xaml
    /// </summary>
    public partial class MainPageOrganiser : ExtendedRibbonWindow
    {
        #region Constants
        const String SelectBackupFolderCommandKey = "SelectBackupFolderCommand";
        #endregion

        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(MainPageViewModel), typeof(MainPageOrganiser),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public MainPageViewModel ViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            MainPageOrganiser senderControl = (MainPageOrganiser)obj;

            object oldCmd = senderControl.Resources[SelectBackupFolderCommandKey];
            if (oldCmd != null)
            {
                senderControl.Resources.Remove(SelectBackupFolderCommandKey);
            }

            if (e.OldValue != null)
            {
                MainPageViewModel oldModel = (MainPageViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                MainPageViewModel newModel = (MainPageViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(SelectBackupFolderCommandKey, newModel.SelectBackupFolderCommand);
            }
        }

        #endregion

        #endregion


        #region Constructor

        public MainPageOrganiser()
        {
            InitializeComponent();

            this.ViewModel = new MainPageViewModel();

            this.SizeChanged += new SizeChangedEventHandler(MainPageOrganiser_SizeChanged);
        }
        #endregion


        #region Event Handlers

        /// <summary>
        /// User has clicked on the text to change the database's backup location
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DatabaseMaintenanceBackupFolder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //this.ViewModel.SelectBackupFolderCommand.Execute();
            this.ViewModel.ShowOptionsCommand.Execute(this);
        }

        /// <summary>
        /// Ogranizer's size changed event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainPageOrganiser_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            InvalidateMeasure();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Prepares and opens the given window
        /// </summary>
        /// <param name="window">The window to open as a child of this window</param>
        /// <param name="isModal">Is it a Modal form, if so open in the centre of the screen</param>
        public void OpenChildWindow(Window window, bool isModal)
        {
            //prep the window
            if (window.Owner == null)
            {
                window.Owner = this;
            }

            window.Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.ApplicationIcon;

            //Set start location
            if (isModal)
            {
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                //System.Drawing.Size screenSize = System.Windows.Forms.SystemInformation.PrimaryMonitorSize;
                //window.Top = (screenSize.Height - window.Height) / 2;
                //window.Left = (screenSize.Width - window.Width) / 2;
                window.ShowDialog();
            }
            else
            {
                window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                window.Show();
            }
        }

        #endregion

        /// <summary>
        /// We do not use the Ribbon's main toolbar, so always hide it despite any user's best efforts
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xRibbon_IsMinimizedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (xRibbon.IsMinimized)
            {
                xRibbon.IsMinimized = false;
            }
        }
    }
}
