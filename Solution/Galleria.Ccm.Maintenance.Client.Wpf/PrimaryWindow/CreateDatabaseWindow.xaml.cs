﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for CreateDatabaseWindow.xaml
    /// </summary>
    public partial class CreateDatabaseWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(CreateDatabaseViewModel), typeof(CreateDatabaseWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public CreateDatabaseViewModel ViewModel
        {
            get { return (CreateDatabaseViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CreateDatabaseWindow senderControl = (CreateDatabaseWindow)obj;

            if (e.OldValue != null)
            {
                CreateDatabaseViewModel oldModel = (CreateDatabaseViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.DatabaseCreated -= senderControl.ViewModel_DatabaseCreated;

            }

            if (e.NewValue != null)
            {
                CreateDatabaseViewModel newModel = (CreateDatabaseViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.DatabaseCreated += senderControl.ViewModel_DatabaseCreated;

            }
        }

        #endregion

        #endregion

        #region Events

        public event EventHandler<ValueEventArgs<Connection>> DatabaseCreated;
        private void RaiseDatabaseCreated(Connection con)
        {
            if (this.DatabaseCreated != null)
            {
                this.DatabaseCreated(this, new ValueEventArgs<Connection>(con));
            }
        }

        #endregion

        #region Constructor

        public CreateDatabaseWindow()
        {
            InitializeComponent();

            this.ViewModel = new CreateDatabaseViewModel();
        }

        #endregion

        #region Event Handlers

        private void ViewModel_DatabaseCreated(object sender, ValueEventArgs<Connection> e)
        {
            //Relay the event
            RaiseDatabaseCreated(e.ReturnObject);
        }

        #endregion

        #region window close

        protected override void OnCrossCloseRequested(System.ComponentModel.CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);
            e.Cancel = true;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
