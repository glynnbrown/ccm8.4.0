﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using Galleria.Framework.Helpers;
using System.Windows;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// A ViewModel that handles the viewing and changin go the application-wide 
    /// options available to the user. These include display language, back location 
    /// etc
    /// </summary>
    public class OptionsViewModel : ViewModelAttachedControlObject<OptionsScreen>
    {
        #region Fields
        private SettingsList _settingsList;
        private String _backupLocation = String.Empty;
        private String _displayLanguage = String.Empty;

        #endregion

        #region Property Paths

        public static PropertyPath DisplayLanguageProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(o => o.DisplayLanguage);
        public static PropertyPath BackupLocationProperty = WpfHelper.GetPropertyPath<OptionsViewModel>(o => o.BackupLocation);

        #endregion

        #region Properties

        /// <summary>
        /// The currently selected backup location
        /// </summary>
        public String BackupLocation
        {
            get { return _backupLocation; }
            set
            {
                _backupLocation = value;
                OnPropertyChanged(BackupLocationProperty);
            }
        }

        /// <summary>
        /// The currently selected display language
        /// </summary>
        public String DisplayLanguage
        {
            get { return _displayLanguage; }
            set
            {
                _displayLanguage = value;
                OnPropertyChanged(DisplayLanguageProperty);
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="entity"></param>
        public OptionsViewModel()
        {
            _settingsList = SettingsList.Instance;
            _backupLocation = _settingsList.BackupLocation;
            _displayLanguage = _settingsList.DisplayLanguage;
        }

        #endregion

        #region Commands

        #region Save Settings Command

        private RelayCommand _saveSettingsCommand;

        /// <summary>
        /// Saves the settings down
        /// </summary>
        public RelayCommand SaveSettingsCommand
        {
            get
            {
                if (_saveSettingsCommand == null)
                {
                    _saveSettingsCommand =
                        new RelayCommand(p => SaveSettings_Executed())
                        {
                            FriendlyName = Message.Generic_Save
                        };
                    this.ViewModelCommands.Add(_saveSettingsCommand);
                }
                return _saveSettingsCommand;
            }
        }

        private void SaveSettings_Executed()
        {
            base.ShowWaitCursor(true);

            //Call save on settings state
            this._settingsList.BackupLocation = _backupLocation; //permanent saves
            this._settingsList.DisplayLanguage = _displayLanguage;

            base.ShowWaitCursor(false);

            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region SelectBackupFolderCommand

        private RelayCommand _selectBackupFolderCommand;

        /// <summary>
        /// Shows the open folder dialog to select the 
        /// database backup folder
        /// </summary>
        public RelayCommand SelectBackupFolderCommand
        {
            get
            {
                if (_selectBackupFolderCommand == null)
                {
                    _selectBackupFolderCommand = new RelayCommand
                    (p => SelectBackupFolderCommand_Executed());
                }
                return _selectBackupFolderCommand;
            }
        }

        private void SelectBackupFolderCommand_Executed()
        {
            System.Windows.Forms.FolderBrowserDialog diag = new System.Windows.Forms.FolderBrowserDialog();
            diag.SelectedPath = this._settingsList.BackupLocation;

            System.Windows.Forms.DialogResult result = diag.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.BackupLocation = diag.SelectedPath; //saving backup location to the local var
            }
        }

        #endregion

        #region Reset Defaults Command

        private RelayCommand _resetDefaultCommand;

        /// <summary>
        /// Calls reset to defaults on settings list
        /// </summary>
        public RelayCommand ResetDefaultCommand
        {
            get
            {
                if (_resetDefaultCommand == null)
                {
                    _resetDefaultCommand =
                        new RelayCommand(p => ResetDefault_Executed())
                        {
                            FriendlyName = Message.Options_Settings_Reset
                        };
                    this.ViewModelCommands.Add(_resetDefaultCommand);
                }
                return _resetDefaultCommand;
            }
        }

        private void ResetDefault_Executed()
        {
            base.ShowWaitCursor(true);

            // Reset user connection list
            SettingsList connList = SettingsList.Instance;
            connList.ResetDefault();

            base.ShowWaitCursor(false);
        }

        #endregion

        #endregion

        #region Dispose

        /// <summary>
        /// Handles an IDisposable dispose call
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    //dispose control
                    this.AttachedControl = null;
                }
                this.IsDisposed = true;
            }
        }

        #endregion
    }
}
