﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using System.ComponentModel;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for UpgradeDatabaseWindow.xaml
    /// </summary>
    public partial class UpgradeDatabaseWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(UpgradeDatabaseViewModel), typeof(UpgradeDatabaseWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public UpgradeDatabaseViewModel ViewModel
        {
            get { return (UpgradeDatabaseViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            UpgradeDatabaseWindow senderControl = (UpgradeDatabaseWindow)obj;

            if (e.OldValue != null)
            {
                UpgradeDatabaseViewModel oldModel = (UpgradeDatabaseViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                UpgradeDatabaseViewModel newModel = (UpgradeDatabaseViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

            }
        }

        #endregion

        #endregion

        #region Constructor

        public UpgradeDatabaseWindow(DatabaseInfo databaseToUpgrade)
        {
            InitializeComponent();

            this.ViewModel = new UpgradeDatabaseViewModel(databaseToUpgrade);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// S
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void xFutureVersion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ViewModel != null && xFutureVersion.SelectedItem != null)
            {
                this.ViewModel.FutureVersion = xFutureVersion.SelectedItem.ToString();
            }
        }

        #endregion

        #region window close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            e.Cancel = true;

            if (this.ViewModel != null)
            {
                if (this.ViewModel.CurrentStep == MaintenanceProcessStep.StartInfo ||
                    this.ViewModel.CurrentStep == MaintenanceProcessStep.Error)
                {
                    this.ViewModel.CancelCommand.Execute();
                }
                else if (this.ViewModel.CurrentStep == MaintenanceProcessStep.Complete)
                {
                    this.ViewModel.OKCommand.Execute();
                }
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
