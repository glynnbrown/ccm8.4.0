﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using System.Globalization;
using System.Diagnostics;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using System.IO;
using System.Windows;
using Galleria.Framework.Helpers;
using System.ComponentModel;
using Galleria.Framework.Processes;
using Galleria.Ccm.Processes.DatabaseMaintenance;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// A viewModel that performs the backup of the selected database to the cetnral file location.
    /// </summary>
    public sealed class BackupDatabaseViewModel : ViewModelAttachedControlObject<BackupDatabaseWindow>
    {
        #region Fields

        private BackgroundWorker _backgroundWorker;
        private Double _percentageComplete;
        private String _currentStatusDescription;
        private MaintenanceProcessStep _currentStep = MaintenanceProcessStep.Confirm;
        private String _backupLocationFolder;
        private String _backupFileName;
        private DatabaseInfo _dbInfo;
        private String _errorDescription; //holds any detailed information on errors that have occurred
        private CultureInfo _invariantCultureProvider = CultureInfo.InvariantCulture;

        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath PercentageCompleteProperty = WpfHelper.GetPropertyPath<BackupDatabaseViewModel>(p => p.PercentageComplete);
        public static readonly PropertyPath CurrentStatusDescriptionProperty = WpfHelper.GetPropertyPath<BackupDatabaseViewModel>(p => p.CurrentStatusDescription);
        public static readonly PropertyPath CurrentScreenProperty = WpfHelper.GetPropertyPath<BackupDatabaseViewModel>(p => p.CurrentScreen);
        public static readonly PropertyPath BackupLocationFolderProperty = WpfHelper.GetPropertyPath<BackupDatabaseViewModel>(p => p.BackupLocationFolder);
        public static readonly PropertyPath BackupFileNameProperty = WpfHelper.GetPropertyPath<BackupDatabaseViewModel>(p => p.BackupFileName);
        public static readonly PropertyPath ErrorDescriptionProperty = WpfHelper.GetPropertyPath<BackupDatabaseViewModel>(p => p.ErrorDescription);
        public static readonly PropertyPath BackupFileNameAndPathProperty = WpfHelper.GetPropertyPath<BackupDatabaseViewModel>(p => p.BackupFileNameAndPath);
        #endregion

        #region Properties

        /// <summary>
        /// Returns the current screen number
        /// </summary>
        public MaintenanceProcessStep CurrentScreen
        {
            get { return _currentStep; }
            private set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentScreenProperty);
            }
        }

        /// <summary>
        /// Returns the percentage progress so far
        /// </summary>
        public Double PercentageComplete
        {
            get { return _percentageComplete; }
            private set
            {
                _percentageComplete = value;
                OnPropertyChanged(PercentageCompleteProperty);
            }
        }

        /// <summary>
        /// Returns the friendly description for current step being processed
        /// </summary>
        public String CurrentStatusDescription
        {
            get { return _currentStatusDescription; }
            private set
            {
                _currentStatusDescription = value;
                OnPropertyChanged(CurrentStatusDescriptionProperty);
            }
        }

        /// <summary>
        /// Returns the backup folder location path
        /// </summary>
        public String BackupLocationFolder
        {
            get { return _backupLocationFolder; }
            set
            {
                _backupLocationFolder = value;
                OnPropertyChanged(BackupLocationFolderProperty);
            }
        }

        /// <summary>
        /// Returns the file name of the backup
        /// </summary>
        public String BackupFileName
        {
            get
            {
                if (_backupFileName == null)
                {
                    //Ensure backup is NOT tagged as auto-generated
                    _backupFileName = Common.Helpers.CreateBackupFileName(false);
                }
                return _backupFileName;
            }
        }

        /// <summary>
        /// Gives the full path and file name of the backup file - used
        /// for displaying to user when complete
        /// </summary>
        public String BackupFileNameAndPath
        {
            get
            {
                return Path.Combine(BackupLocationFolder, BackupFileName);
            }
        }

        /// <summary>
        /// The details of the error that has occurred if any
        /// </summary>
        public String ErrorDescription
        {
            get { return _errorDescription; }
            set
            {
                _errorDescription = value;
                OnPropertyChanged(ErrorDescriptionProperty);
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="info">the info for the db to be backed up</param>
        public BackupDatabaseViewModel(DatabaseInfo info)
        {
            _dbInfo = info;
            _backupLocationFolder = info.BackupLocation;
        }

        #endregion

        #region Commands

        #region OpenBackupFolderLocationCommand

        private RelayCommand _openBackupFolderLocationCommand;

        /// <summary>
        /// Opens the backupfolder location in the file explorer
        /// </summary>
        public RelayCommand OpenBackupFolderLocationCommand
        {
            get
            {
                if (_openBackupFolderLocationCommand == null)
                {
                    _openBackupFolderLocationCommand = new RelayCommand
                    (p => OpenBackupFolderLocationCommand_Executed());
                    base.ViewModelCommands.Add(_openBackupFolderLocationCommand);
                }
                return _openBackupFolderLocationCommand;
            }
        }

        private void OpenBackupFolderLocationCommand_Executed()
        {
            //open the backup folder using windows file explorer
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "explorer.exe";
            String pathToOpen = this.BackupLocationFolder;
            startInfo.Arguments = pathToOpen;
            System.Diagnostics.Process.Start(startInfo);
        }

        #endregion

        #region OKCommand

        private RelayCommand _OKCommand;

        /// <summary>
        /// Closes the backup window on complete
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_OKCommand == null)
                {
                    _OKCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute(), false)
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_OKCommand);

                    this.PropertyChanged += OKCommand_CanExecuteRequeryRequired;
                }
                return _OKCommand;
            }
        }

        private void OKCommand_CanExecuteRequeryRequired(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == CurrentScreenProperty.Path)
            {
                OKCommand.RaiseCanExecuteChanged();
            }
        }

        [DebuggerStepThrough]
        private bool OK_CanExecute()
        {
            return this.CurrentScreen == MaintenanceProcessStep.Complete ||
                this.CurrentScreen == MaintenanceProcessStep.Cancel || this.CurrentScreen == MaintenanceProcessStep.Error;
        }

        private void OK_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Starts off the process
        /// </summary>
        public void BeginProcess()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            _backgroundWorker.RunWorkerAsync();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out the work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;

            //Clear any error descriptions
            this.ErrorDescription = null;

            //create the process to run
            BackupProcess backupProcess = new BackupProcess();
            backupProcess.BackupLocation = String.Format("{0}\\{1}", this.BackupLocationFolder, this.BackupFileName);
            //Set the DalFactoryconfig to match the setup decribed in our DatabaseInfo object - this
            //will ensure that the process is run against the correct database
            backupProcess.DalFactoryConfig = _dbInfo.DalFactoryConfig;

            backupProcess.OperationCompleted += new EventHandler<ProcessProgressEventArgs>(process_OperationCompleted);

            //check if the database is still available
            if (this._dbInfo.CheckAvailability(true))
            {
                //sent an initial status report
                _backgroundWorker.ReportProgress(0, backupProcess.StepDescriptions[1]);
                //start the execution
                ProcessFactory.Execute(backupProcess);
            }
            else
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Responds to process progress notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void process_OperationCompleted(object sender, ProcessProgressEventArgs e)
        {
            BackupProcess backupProcess = (BackupProcess)sender;

            if (this._dbInfo.IsAvailable)
            {
                //get the percentage completed & the next step description
                Double percentage = ((Double)e.CurrentProgress / (Double)e.MaxProgress) * 100;
                String nextStepStatus =
                    (e.CurrentProgress != e.MaxProgress) ?
                    backupProcess.StepDescriptions[e.CurrentProgress + 1] : String.Empty;

                //notify out the progress
                _backgroundWorker.ReportProgress((Int32)percentage, nextStepStatus);

                //unsubscribe the handler if this was the last step
                if (e.CurrentProgress == e.MaxProgress)
                {
                    backupProcess.OperationCompleted -= process_OperationCompleted;
                }

                // Update LastBackup Date
                //_dbInfo.LastBackupDate = DateTime.Now.ToString(Constants.VistaDateFormat, _invariantCultureProvider);
            }
            else
            {
                backupProcess.OperationCompleted -= process_OperationCompleted;
            }
        }

        /// <summary>
        /// Handles the worker progress changed notifications
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.PercentageComplete = e.ProgressPercentage;
            this.CurrentStatusDescription = (String)e.UserState;
        }

        /// <summary>
        /// Handles the worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= worker_DoWork;
            worker.RunWorkerCompleted -= worker_RunWorkerCompleted;
            worker.ProgressChanged -= worker_ProgressChanged;

            //change to completed screen if not cancelled
            if (e.Cancelled)
            {
                this.CurrentScreen = MaintenanceProcessStep.Cancel;
            }
            else if (e.Error != null)
            {
                this.ErrorDescription = e.Error.Message;
                this.CurrentScreen = MaintenanceProcessStep.Error;
            }
            else
            {
                this.CurrentScreen = MaintenanceProcessStep.Complete;
            }
            //Update running result
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = (e.Error == null);
            }
        }

        #endregion

        #region IDisposable Members
        protected override void Dispose(bool disposing)
        {
            //nothing to cleanup
        }
        #endregion
    }
}
