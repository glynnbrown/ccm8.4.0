﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using System.Windows;
using System.ComponentModel;
using Galleria.Framework.Processes;
using Microsoft.Win32;
using System.IO;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using System.Diagnostics;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Processes.DatabaseMaintenance;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// A viewModel that handles the retore process of a local (Vista) type database.
    /// It can be extended for other database types by implementing the required restore methods in
    /// the releavant Dal.
    /// </summary>
    public sealed class RestoreDatabaseViewModel : ViewModelAttachedControlObject<RestoreDatabaseWindow>
    {
        #region Fields
        private DatabaseInfo _dbInfo;
        private BackgroundWorker _backgroundWorker;
        private Double _percentageComplete;
        private String _currentStepDescription;
        private String _backupLocationFolder;
        private String _backupFileName;
        private String _restoreFilePath;
        private Int32 _currentScreen = 1;
        #endregion

        #region Binding Property Paths
        public static readonly PropertyPath PercentageCompleteProperty = WpfHelper.GetPropertyPath<RestoreDatabaseViewModel>(p => p.PercentageComplete);
        public static readonly PropertyPath CurrentStepDescriptionProperty = WpfHelper.GetPropertyPath<RestoreDatabaseViewModel>(p => p.CurrentStepDescription);
        public static readonly PropertyPath BackupLocationFolderProperty = WpfHelper.GetPropertyPath<RestoreDatabaseViewModel>(p => p.BackupLocationFolder);
        public static readonly PropertyPath RestoreFilePathProperty = WpfHelper.GetPropertyPath<RestoreDatabaseViewModel>(p => p.RestoreFilePath);
        public static readonly PropertyPath BackupFileNameProperty = WpfHelper.GetPropertyPath<RestoreDatabaseViewModel>(p => p.BackupFileName);
        public static readonly PropertyPath CurrentScreenProperty = WpfHelper.GetPropertyPath<RestoreDatabaseViewModel>(p => p.CurrentScreen);
        #endregion

        #region Properties

        /// <summary>
        /// Returns the percentage of the process complete
        /// </summary>
        public Double PercentageComplete
        {
            get { return _percentageComplete; }
            private set
            {
                _percentageComplete = value;
                OnPropertyChanged(PercentageCompleteProperty);
            }
        }

        /// <summary>
        /// Returns the friendly description for the steop currently being processed
        /// </summary>
        public String CurrentStepDescription
        {
            get { return _currentStepDescription; }
            private set
            {
                _currentStepDescription = value;
                OnPropertyChanged(CurrentStepDescriptionProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the folder to save the backup to
        /// </summary>
        public String BackupLocationFolder
        {
            get { return _backupLocationFolder; }
            set
            {
                _backupLocationFolder = value;
                OnPropertyChanged(BackupLocationFolderProperty);
            }
        }

        /// <summary>
        /// Returns the file name of the backup to be produced
        /// </summary>
        public String BackupFileName
        {
            get
            {
                if (_backupFileName == null)
                {
                    //Ensure backup is tagged as auto-generated
                    _backupFileName = Galleria.Ccm.Maintenance.Client.Wpf.Common.Helpers.CreateBackupFileName(true);
                }
                return _backupFileName;
            }

        }

        /// <summary>
        /// Gets/Sets the path of the file to restore from
        /// </summary>
        public String RestoreFilePath
        {
            get { return _restoreFilePath; }
            set
            {
                _restoreFilePath = value;
                OnPropertyChanged(RestoreFilePathProperty);
            }
        }

        /// <summary>
        /// Returns the current window screen
        /// </summary>
        public Int32 CurrentScreen
        {
            get { return _currentScreen; }
            private set
            {
                _currentScreen = value;
                OnPropertyChanged(CurrentScreenProperty);
                _OKCommand.RaiseCanExecuteChanged(); //forces OKCommand.CanExecute() check as it wasn't doing it in time for the OK button
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="info">the info for the db to be backed up</param>
        public RestoreDatabaseViewModel(DatabaseInfo info)
        {
            _dbInfo = info;
            _backupLocationFolder = info.BackupLocation;
        }

        #endregion

        #region Commands

        #region Restore Command

        private RelayCommand _restoreCommand;

        /// <summary>
        /// Starts the restore process
        /// </summary>
        public RelayCommand RestoreCommand
        {
            get
            {
                if (_restoreCommand == null)
                {
                    _restoreCommand = new RelayCommand(
                        p => RestoreCommand_Executed(),
                        p => RestoreCommand_CanExecute())
                    {
                        FriendlyName = Message.DatabaseRestore_Restore
                    };
                    base.ViewModelCommands.Add(_restoreCommand);
                }
                return _restoreCommand;
            }
        }

        [DebuggerStepThrough]
        private bool RestoreCommand_CanExecute()
        {
            return (this.CurrentScreen == 1) && (File.Exists(this.RestoreFilePath));
        }

        private void RestoreCommand_Executed()
        {
            //change to the next screen
            this.CurrentScreen++;

            //start the process
            BeginProcess();
        }

        #endregion

        #region Cancel Command

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the operation and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => CancelCommand_Executed(),
                        p => Cancel_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private bool Cancel_CanExecute()
        {
            return this.CurrentScreen == 1;
        }


        private void CancelCommand_Executed()
        {
            if (this.CurrentScreen == 1)
            {
                //if we are on the first screen the process hasnt started yet so just close
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region OKCommand
        private RelayCommand _OKCommand;
        /// <summary>
        /// Closes the window on complete
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_OKCommand == null)
                {
                    _OKCommand = new RelayCommand(
                        p => OK_Executed(),
                        p => OK_CanExecute())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_OKCommand);

                }
                return _OKCommand;
            }
        }

        [DebuggerStepThrough]
        private bool OK_CanExecute()
        {
            return (this.CurrentScreen == 3 || this.CurrentScreen == 0 || this.CurrentScreen == -1);
        }

        private void OK_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region SelectRestoreFileCommand

        private RelayCommand _selectRestoreFileCommand;

        /// <summary>
        /// Opens the file dialog to select the file to be restored
        /// </summary>
        public RelayCommand SelectRestoreFileCommand
        {
            get
            {
                if (_selectRestoreFileCommand == null)
                {
                    _selectRestoreFileCommand = new RelayCommand(
                        p => SelectRestoreFileCommand_Executed())
                    {
                        FriendlyName = Message.RestoreDatabase_BrowseForLocationButton
                    };
                    base.ViewModelCommands.Add(_selectRestoreFileCommand);
                }
                return _selectRestoreFileCommand;
            }
        }

        private void SelectRestoreFileCommand_Executed()
        {
            if (this.AttachedControl != null)
            {
                OpenFileDialog ofd = new OpenFileDialog();

                if (!String.IsNullOrEmpty(this.RestoreFilePath))
                {
                    ofd.InitialDirectory = Path.GetDirectoryName(this.RestoreFilePath);
                    ofd.FileName = this.RestoreFilePath;
                }
                else
                {
                    ofd.InitialDirectory = this.BackupLocationFolder;
                }

                ofd.Filter = Message.Generic_SelectBackupFilter;
                ofd.Multiselect = false;

                if ((bool)ofd.ShowDialog())
                {
                    this.RestoreFilePath = ofd.FileName;
                }
                else if (String.IsNullOrEmpty(ofd.FileName))
                {
                    this.RestoreFilePath = null;
                    this.CancelCommand_Executed(); //cancel RestoreDatabase dialog if no file name selected
                }
            }
        }

        #endregion

        #region OpenBackupFolderCommand

        private RelayCommand _openBackupFolderCommand;

        /// <summary>
        /// Opens the backup folder location in file explorer
        /// </summary>
        public RelayCommand OpenBackupFolderCommand
        {
            get
            {
                if (_openBackupFolderCommand == null)
                {
                    _openBackupFolderCommand = new RelayCommand(
                        p => OpenBackupFolderCommand_Executed());
                    base.ViewModelCommands.Add(_openBackupFolderCommand);
                }
                return _openBackupFolderCommand;
            }
        }

        private void OpenBackupFolderCommand_Executed()
        {
            //open the backup folder using windows file explorer
            System.Diagnostics.Process.Start("explorer.exe", this.BackupLocationFolder);
        }

        #endregion

        #endregion

        #region Methods

        private void BeginProcess()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            _backgroundWorker.RunWorkerAsync();
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Carries out the work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;

            //create the process to run
            RestoreProcess restoreProcess = new RestoreProcess();
            restoreProcess.BackupLocation = Path.Combine(this.BackupLocationFolder, this.BackupFileName);
            restoreProcess.RestoreFilePath = _restoreFilePath;
            //Set the DalFactoryconfig to match the setup decribed in our DatabaseInfo object - this
            //will ensure that the process is run against the correct database
            restoreProcess.DalFactoryConfig = _dbInfo.DalFactoryConfig;

            restoreProcess.OperationCompleted += new EventHandler<ProcessProgressEventArgs>(process_OperationCompleted);

            //check if the database is still available
            if (this._dbInfo.CheckAvailability(true))
            {
                //sent an initial status report
                _backgroundWorker.ReportProgress(0, restoreProcess.StepDescriptions[1]);
                //start the execution
                ProcessFactory.Execute(restoreProcess);
            }
            else
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Responds to process progress notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void process_OperationCompleted(object sender, ProcessProgressEventArgs e)
        {
            RestoreProcess restoreProcess = (RestoreProcess)sender;

            if (this._dbInfo.IsAvailable)
            {
                //get the percentage completed & the next step description
                Double percentage = ((Double)e.CurrentProgress / (Double)e.MaxProgress) * 100;
                String nextStepStatus =
                    (e.CurrentProgress != e.MaxProgress) ?
                    restoreProcess.StepDescriptions[e.CurrentProgress + 1] : String.Empty;

                //notify out the progress
                _backgroundWorker.ReportProgress((Int32)percentage, nextStepStatus);

                //unsubscribe the handler if this was the last step
                if (e.CurrentProgress == e.MaxProgress)
                {
                    restoreProcess.OperationCompleted -= process_OperationCompleted;
                }

                //  [TODO] mainenance takes a backup so possibly that backup should be persisted (?)
            }
            else
            {
                restoreProcess.OperationCompleted -= process_OperationCompleted;
            }
        }


        /// <summary>
        /// Handles the worker progress changed notifications
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.PercentageComplete = e.ProgressPercentage;
            this.CurrentStepDescription = (String)e.UserState;
        }

        /// <summary>
        /// Handles the worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= worker_DoWork;
            worker.RunWorkerCompleted -= worker_RunWorkerCompleted;
            worker.ProgressChanged -= worker_ProgressChanged;
            _backgroundWorker = null;

            //switch to the next screen
            if (e.Cancelled)
            {
                this.CurrentScreen = 0;
            }
            else if (e.Error != null)
            {
                this.CurrentScreen = -1;
            }
            else
            {
                this.CurrentScreen = 3;
            }
            //Update running result
            if (this.AttachedControl != null)
            {
                this.AttachedControl.DialogResult = (e.Error == null);
            }
        }


        #endregion

        #region IDisposable

        protected override void Dispose(bool disposing)
        {
            //nothing to clear
        }

        #endregion
    }
}
