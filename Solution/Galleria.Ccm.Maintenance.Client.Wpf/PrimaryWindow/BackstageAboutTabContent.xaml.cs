﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Displays the properties of the current review
    /// </summary>
    public partial class BackstageAboutTabContent : UserControl
    {
        #region Constants
        const String SelectBackupFolderCommandKey = "SelectBackupFolderCommand";
        #endregion

        #region Properties

        #region Build Version Property
        public static readonly DependencyProperty BuildVersionProperty =
            DependencyProperty.Register("BuildVersion", typeof(String), typeof(BackstageAboutTabContent));

        public String BuildVersion
        {
            get { return (String)GetValue(BuildVersionProperty); }
            private set { SetValue(BuildVersionProperty, value); }
        }
        #endregion

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("MainViewModel", typeof(MainPageViewModel), typeof(BackstageAboutTabContent),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the window viewmodel context
        /// </summary>
        public MainPageViewModel MainViewModel
        {
            get { return (MainPageViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackstageAboutTabContent senderControl = (BackstageAboutTabContent)obj;

            object oldCmd = senderControl.Resources[SelectBackupFolderCommandKey];
            if (oldCmd != null)
            {
                senderControl.Resources.Remove(SelectBackupFolderCommandKey);
            }

            if (e.OldValue != null)
            {
                MainPageViewModel oldModel = (MainPageViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                MainPageViewModel newModel = (MainPageViewModel)e.NewValue;
                //newModel.AttachedControl = senderControl;
                senderControl.Resources.Add(SelectBackupFolderCommandKey, newModel.SelectBackupFolderCommand);
            }
        }

        #endregion

        #region Is CEIP Participate Property
        public static readonly DependencyProperty IsCEIPParticipateProperty =
            DependencyProperty.Register("IsCEIPParticipate", typeof(Boolean), typeof(BackstageAboutTabContent));

        /// <summary>
        /// Is the user participating in CEIP?
        /// </summary>
        public Boolean IsCEIPParticipate
        {
            get { return (Boolean)GetValue(IsCEIPParticipateProperty); }
            set { SetValue(IsCEIPParticipateProperty, value); }
        }
        #endregion



        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BackstageAboutTabContent()
        {
            InitializeComponent();

            //Set Build Version
            this.BuildVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        #endregion

    }
}
