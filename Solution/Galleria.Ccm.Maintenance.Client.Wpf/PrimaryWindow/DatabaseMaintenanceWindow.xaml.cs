﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for DatabaseMaintenanceWindow.xaml
    /// </summary>
    public partial class DatabaseMaintenanceWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(DatabaseMaintenanceViewModel),
            typeof(DatabaseMaintenanceWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public DatabaseMaintenanceViewModel ViewModel
        {
            get { return (DatabaseMaintenanceViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            DatabaseMaintenanceWindow senderControl = (DatabaseMaintenanceWindow)obj;

            if (e.OldValue != null)
            {
                DatabaseMaintenanceViewModel oldModel = (DatabaseMaintenanceViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                DatabaseMaintenanceViewModel newModel = (DatabaseMaintenanceViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseMaintenanceWindow(DatabaseInfo info)
        {
            this.ViewModel = new DatabaseMaintenanceViewModel(info);
            this.ViewModel.BackupLocationFolder = info.BackupLocation;

            InitializeComponent();

            this.Loaded += new RoutedEventHandler(CompactRepairDatabaseWindow_Loaded);
        }

        private void CompactRepairDatabaseWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= CompactRepairDatabaseWindow_Loaded;

            //queue the process on a delay
            Dispatcher.BeginInvoke(
                (Action)(() => this.ViewModel.BeginProcess()),
               priority: System.Windows.Threading.DispatcherPriority.ApplicationIdle);
        }

        #endregion

        #region Event Handlers

        private void FolderLocation_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.OpenBackupFolderLocationCommand.Execute();
            }
        }

        #endregion

        #region Window close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            //suppress the close request from the cross
            e.Cancel = true;

            //if ok can be executed use this instead
            if (this.ViewModel.OKCommand.CanExecute())
            {
                this.ViewModel.OKCommand.Execute();
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion

    }
}
