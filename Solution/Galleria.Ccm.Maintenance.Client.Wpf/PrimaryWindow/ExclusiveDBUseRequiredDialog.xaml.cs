﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for ExclusiveDBUseRequiredDialog.xaml
    /// </summary>
    public partial class ExclusiveDBUseRequiredDialog : ExtendedRibbonWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(ExclusiveDBUseRequiredDialogViewModel), typeof(ExclusiveDBUseRequiredDialog),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the attached viewmodel
        /// </summary>
        public ExclusiveDBUseRequiredDialogViewModel ViewModel
        {
            get { return (ExclusiveDBUseRequiredDialogViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ExclusiveDBUseRequiredDialog senderControl = (ExclusiveDBUseRequiredDialog)obj;

            if (e.OldValue != null)
            {
                ExclusiveDBUseRequiredDialogViewModel oldModel = (ExclusiveDBUseRequiredDialogViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                ExclusiveDBUseRequiredDialogViewModel newModel = (ExclusiveDBUseRequiredDialogViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion


        public ExclusiveDBUseRequiredDialog(ExclusiveDBUseRequiredDialogViewModel viewmodel)
        {
            InitializeComponent();

            //set the view model
            this.ViewModel = viewmodel;
        }

        #region Event Handlers

        /// <summary>
        /// Handles the retry click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SaveUserSettings();
            this.DialogResult = true;
        }

        void cmdCancel_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SaveUserSettings();
            this.DialogResult = false;
        }
        #endregion
    }
}
