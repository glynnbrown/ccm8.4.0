﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using System.ComponentModel;
using System.Windows;
using Galleria.Framework.Helpers;
using Galleria.Framework.Processes;
using System.Diagnostics;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using Galleria.Ccm.Processes.DatabaseMaintenance;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// A viewModel that handles the deletion of a local (Vista) type database.
    /// It can be extended for other database types by implementing the required delete methods in
    /// the releavant Dal.
    /// </summary>
    public sealed class DeleteDatabaseViewModel : ViewModelAttachedControlObject<DeleteDatabaseWindow>
    {
        #region Fields

        private DatabaseInfo _dbInfo;
        private MaintenanceProcessStep _currentStep = MaintenanceProcessStep.Confirm;
        private BackgroundWorker _backgroundWorker;
        private Boolean _isProcessing;
        private Int32 _percentageComplete;
        private TimeSpan _estimatedTimeLeft;

        #endregion

        #region Binding Property Path

        public static readonly PropertyPath DatabasePathProperty = WpfHelper.GetPropertyPath<DeleteDatabaseViewModel>(p => p.DatabasePath);
        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<DeleteDatabaseViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath DestinationFolderPathProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.DestinationFolderPath);
        public static readonly PropertyPath IsProcessingProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.IsProcessing);
        public static readonly PropertyPath PercentageCompleteProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.PercentageComplete);
        public static readonly PropertyPath EstimatedTimeLeftProperty = WpfHelper.GetPropertyPath<MoveDatabaseViewModel>(p => p.EstimatedTimeLeft);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current step shown by the window
        /// </summary>
        public MaintenanceProcessStep CurrentStep
        {
            get { return _currentStep; }
            private set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
            }
        }

        /// <summary>
        /// Returns the path of the database to be deleted
        /// </summary>
        public String DatabasePath
        {
            get { return (_dbInfo != null) ? _dbInfo.SourceConnection.FilePath : String.Empty; }
        }

        /// <summary>
        /// Returns true if the process has been started
        /// </summary>
        public Boolean IsProcessing
        {
            get { return _isProcessing; }
            private set
            {
                _isProcessing = value;
                OnPropertyChanged(IsProcessingProperty);
            }
        }

        /// <summary>
        /// Returns the percentage completion value for the process
        /// </summary>
        public Int32 PercentageComplete
        {
            get { return _percentageComplete; }
            private set
            {
                _percentageComplete = value;
                OnPropertyChanged(PercentageCompleteProperty);
            }
        }

        /// <summary>
        /// Returns an estimated time left for the process
        /// </summary>
        public TimeSpan EstimatedTimeLeft
        {
            get { return _estimatedTimeLeft; }
            private set
            {
                _estimatedTimeLeft = value;
                OnPropertyChanged(EstimatedTimeLeftProperty);
            }
        }

        #endregion

        #region Event

        /// <summary>
        /// Notifies that the database has been deleted
        /// </summary>
        public event EventHandler<ValueEventArgs<DatabaseInfo>> DatabaseDeleted;
        private void RaiseDatabaseDeleted(DatabaseInfo info)
        {
            if (this.DatabaseDeleted != null)
            {
                this.DatabaseDeleted(this, new ValueEventArgs<DatabaseInfo>(info));
            }
        }

        #endregion

        #region Constructor
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="dbInfo"></param>
        public DeleteDatabaseViewModel(DatabaseInfo dbInfo)
        {
            _dbInfo = dbInfo;
        }

        #endregion

        #region Commands

        #region OKCommand

        private RelayCommand _OKCommand;

        /// <summary>
        /// Proceeds to the next step
        /// </summary>
        public RelayCommand OKCommand
        {
            get
            {
                if (_OKCommand == null)
                {
                    _OKCommand = new RelayCommand(
                        p => OK_Executed())
                    {
                        FriendlyName = Message.Generic_Ok
                    };
                    base.ViewModelCommands.Add(_OKCommand);
                }
                return _OKCommand;
            }
        }

        private void OK_Executed()
        {
            if (this.CurrentStep == MaintenanceProcessStep.Confirm)
            {
                //start the delete process
                base.ShowWaitCursor(true);

                this.CurrentStep = MaintenanceProcessStep.InProgress;
                DeleteDatabase(this.DatabasePath);


                base.ShowWaitCursor(false);
            }
            else if (this.CurrentStep == MaintenanceProcessStep.Complete || this.CurrentStep == MaintenanceProcessStep.Cancel
                || this.CurrentStep == MaintenanceProcessStep.Error)
            {
                //close the window
                if (this.AttachedControl != null)
                {
                    this.AttachedControl.Close();
                }
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the opertation and closes the window
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Cancel_CanExecute()
        {
            return this.CurrentStep == MaintenanceProcessStep.Confirm;
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Starts the delete process
        /// </summary>
        /// <param name="filePath"></param>
        private void DeleteDatabase(String filePath)
        {
            this.EstimatedTimeLeft = new TimeSpan(10000);

            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            _backgroundWorker.RunWorkerAsync();
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;

            //create the process to run
            DeleteDatabaseProcess deleteProcess = new DeleteDatabaseProcess();
            //Set the DalFactoryconfig to match the setup decribed in our DatabaseInfo object - this
            //will ensure that the process is run against the correct database
            deleteProcess.DalFactoryConfig = _dbInfo.DalFactoryConfig;

            deleteProcess.OperationCompleted += new EventHandler<ProcessProgressEventArgs>(process_OperationCompleted);

            //check if the database is still available
            if (this._dbInfo.CheckAvailability(true))
            {
                //sent an initial status report
                _backgroundWorker.ReportProgress(0, deleteProcess.StepDescriptions[1]);
                //start the execution
                ProcessFactory.Execute(deleteProcess);
            }
            else
            {
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Update relevant properties for UI when progress has changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.PercentageComplete += 10;
            this.EstimatedTimeLeft = new TimeSpan(1000);
        }

        /// <summary>
        /// Responds to process progress notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void process_OperationCompleted(object sender, ProcessProgressEventArgs e)
        {
            DeleteDatabaseProcess deleteProcess = (DeleteDatabaseProcess)sender;

            if (this._dbInfo.IsAvailable)
            {
                //get the percentage completed & the next step description
                Double percentage = ((Double)e.CurrentProgress / (Double)e.MaxProgress) * 100;
                String nextStepStatus =
                    (e.CurrentProgress != e.MaxProgress) ?
                    deleteProcess.StepDescriptions[e.CurrentProgress + 1] : String.Empty;

                //notify out the progress
                _backgroundWorker.ReportProgress((Int32)percentage, nextStepStatus);

                //unsubscribe the handler if this was the last step
                if (e.CurrentProgress == e.MaxProgress)
                {
                    deleteProcess.OperationCompleted -= process_OperationCompleted;
                }
            }
            else
            {
                deleteProcess.OperationCompleted -= process_OperationCompleted;
            }
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= worker_DoWork;
            worker.RunWorkerCompleted -= worker_RunWorkerCompleted;
            worker.ProgressChanged -= worker_ProgressChanged;

            //move to the completed screen
            this.IsProcessing = false;

            if (e.Cancelled)
            {
                this.CurrentStep = MaintenanceProcessStep.Cancel;
            }
            else if (e.Error != null)
            {
                this.CurrentStep = MaintenanceProcessStep.Error;
            }
            else
            {
                this.CurrentStep = MaintenanceProcessStep.Complete;
                RaiseDatabaseDeleted(_dbInfo);
            }
        }
        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {
            //nothing to dispose
        }

        #endregion
    }
}
