﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using System.Windows;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// A viewModel that handles the messaging to the user to inform them that before 
    /// any operation can be performed on a database they must have exclusive use of
    /// the database being queried.
    /// </summary>
    public class ExclusiveDBUseRequiredDialogViewModel : ViewModelAttachedControlObject<ExclusiveDBUseRequiredDialog>
    {
        #region Fields

        private SettingsList _currentSettings = SettingsList.Instance;
        private bool _isExclusiveDBUseRequiredDialogNextTime = false;

        #endregion

        #region Binding PropertyPaths

        public static PropertyPath IsExclusiveDBUseRequiredDialogNextTimeProperty = WpfHelper.GetPropertyPath<ExclusiveDBUseRequiredDialogViewModel>(p => p.StopDisplayingWarning);

        #endregion

        #region Properties

        /// <summary>
        /// Returns/Sets the user's choice for showing CEIP window next time.
        /// </summary>
        public bool StopDisplayingWarning
        {
            get { return _isExclusiveDBUseRequiredDialogNextTime; }
            set
            {
                _isExclusiveDBUseRequiredDialogNextTime = value;
                OnPropertyChanged(IsExclusiveDBUseRequiredDialogNextTimeProperty);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Validates if Exclusive DB Use Dialog required
        /// </summary>
        /// <returns>True if CEIP window required, else false</returns>
        public Boolean ExclusiveDBUseRequiredDialogRequired()
        {
            _currentSettings.LoadSettingsList();
            return _currentSettings.DisplayExclusiveDBUseRequiredDialogNextTime;
        }

        /// <summary>
        /// Save the user settings to the ISODBMUtilUsers.xml file
        /// </summary>
        /// <returns></returns>
        public void SaveUserSettings()
        {
            _currentSettings.DisplayExclusiveDBUseRequiredDialogNextTime = !StopDisplayingWarning;
            _currentSettings.SaveSettings();
        }

        #endregion

        #region IDisposable

        /// <summary>
        /// Clears up on dispose.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (!this.IsDisposed)
            {
                if (disposing)
                {
                    this.AttachedControl = null;
                }
                this.IsDisposed = true;
            }
        }

        #endregion
    }
}
