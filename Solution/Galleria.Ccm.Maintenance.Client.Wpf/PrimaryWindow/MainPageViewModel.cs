﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
// V8-27544 : M.Shelley
//  Added licence details to the Help page
// V8-28039 : J.Pickup
//  Adde missing Main_Connect image to showConnectToSql Command
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows;
using Galleria.Ccm.Common.Wpf.Licensing;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using Galleria.Framework.Controls.Wpf;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;
using Csla;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// The Viewmodel of the main page of the application.
    /// </summary>
    public class MainPageViewModel : ViewModelAttachedControlObject<MainPageOrganiser>, INotifyPropertyChanged
    {
        #region Fields
        private SettingsList _settingsList = SettingsList.Instance;
        private ObservableCollection<DatabaseInfo> _availableDatabases = new ObservableCollection<DatabaseInfo>();
        private ReadOnlyObservableCollection<DatabaseInfo> _availableDatabasesRO;
        private DatabaseInfo _selectedDatabase;
        private LicenseState _state;
        private Boolean _userHasLicensingPerm;

        private ExclusiveDBUseRequiredDialogViewModel _ExclusiveDBUseRequiredDialogViewModel; //The exclusive database use dialog view model
        private ExclusiveDBUseRequiredDialog _ExclusiveDBUseRequiredDialog; //The exclusive database use dialog if required

        #region Background worker related
        private BackgroundWorker _backgroundWorker;
        // NOTE: _reportProgress should be dividable by _checkForCancellation
        private static Int32 _reportProgress = 30000; // (ms) defines how much time should elapse before consecutive progress reports 
        private static Int32 _checkForCancellation = 1000; // (ms) defines how much time should elapse before consecutive CancellationPending checks        
        #endregion

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath AvailableDatabasesProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.AvailableDatabases);
        public static readonly PropertyPath SelectedDatabaseProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.SelectedDatabase);
        public static readonly PropertyPath IsCEIPParticipateProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.IsCEIPParticipate);
        public static readonly PropertyPath StateProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.State);
        public static readonly PropertyPath OpenLicenseScreenCommandProperty = WpfHelper.GetPropertyPath<MainPageViewModel>(p => p.OpenLicenseScreenCommand);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the collection of available databases
        /// </summary>
        public ReadOnlyObservableCollection<DatabaseInfo> AvailableDatabases
        {
            get
            {
                if (_availableDatabasesRO == null)
                {
                    _availableDatabasesRO =
                        new ReadOnlyObservableCollection<DatabaseInfo>(_availableDatabases);
                }
                return _availableDatabasesRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected database
        /// </summary>
        public DatabaseInfo SelectedDatabase
        {
            get { return _selectedDatabase; }
            set
            {
                DatabaseInfo oldValue = _selectedDatabase;

                _selectedDatabase = value;

                OnPropertyChanged(SelectedDatabaseProperty);
                //Raise property changed event
                OnSelectedDatabaseChanged(oldValue, value);
            }
        }

        /// <summary>
        /// Gets/Sets whether the user is participating in CEIP
        /// </summary>
        public Boolean IsCEIPParticipate
        {
            get { return _settingsList.SendCEIPInformation; }
        }

        /// <summary>
        /// Returns the license state object
        /// </summary>
        public LicenseState State
        {
            get { return _state; }
        }

        /// <summary>
        /// Returns whether relicensing the application should be visible
        /// </summary>
        public Boolean IsRelicenseVisible
        {
            get { return _userHasLicensingPerm; }
        }

        #endregion

        #region Constructor

        public MainPageViewModel()
        {
            //refresh the connections
            this.RefreshAllCommand.Execute();

            //used to check physical availability of our "AvailableDatabases"
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _backgroundWorker.WorkerReportsProgress = true;
            _backgroundWorker.WorkerSupportsCancellation = true;
            _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(_backgroundWorker_ProgressChanged);
            _backgroundWorker.RunWorkerAsync();

            //_state = App.LicenseState;

            // Get the LicenseState reference from the Application Properties
            var stateProp = Application.Current.Properties["LicenseState"];

            // Get the LicenseState object
            var licenceState = stateProp as LicenseState;
            if (licenceState != null)
            {
                _state = licenceState;
            }

            UpdatePermissionFlags();
        }

        /// <summary>
        /// Testing Constructor
        /// </summary>
        /// <param name="_databases"></param>
        public MainPageViewModel(ObservableCollection<DatabaseInfo> _databases)
        {
            this._availableDatabases = _databases;

            if (this.AttachedControl != null)
            {
                //refresh the connections
                this.RefreshAllCommand.Execute();

                //used to check physical availability of our "AvailableDatabases"
                _backgroundWorker = new BackgroundWorker();
                _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
                _backgroundWorker.WorkerReportsProgress = true;
                _backgroundWorker.WorkerSupportsCancellation = true;
                _backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(_backgroundWorker_ProgressChanged);
                _backgroundWorker.RunWorkerAsync();

                //_state = App.LicenseState;
            }
        }

        #endregion

        #region Commands

        #region ShowConnect

        private RelayCommand _showConnectCommand;

        /// <summary>
        /// Opens the connect to database window
        /// </summary>
        public RelayCommand ShowConnectCommand
        {
            get
            {
                if (_showConnectCommand == null)
                {
                    _showConnectCommand = new RelayCommand(
                        p => ShowConnect_Executed())
                    {
                        FriendlyName = Message.MainPage_Menu_Connect,
                        SmallIcon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_Connect
                    };
                    base.ViewModelCommands.Add(_showConnectCommand);
                }
                return _showConnectCommand;
            }
        }

        private void ShowConnect_Executed()
        {
            if (this.AttachedControl != null)
            {
                ConnectToDatabaseWindow win = new ConnectToDatabaseWindow();
                win.DatabaseConnectionCreated += ConnectToDatabase_DatabaseConnectionCreated;

                App.ShowWindow(win, true);

                win.DatabaseConnectionCreated -= ConnectToDatabase_DatabaseConnectionCreated;
            }
        }

        /// <summary>
        /// Adds the new connection to the settings list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectToDatabase_DatabaseConnectionCreated(object sender, ValueEventArgs<Connection> e)
        {
            AddNewConnection(e.ReturnObject);
        }


        #endregion

        #region ShowConnectToLocal

        private RelayCommand _showConnectToLocalCommand;

        /// <summary>
        /// Shows the connect to database window from the 
        /// enter local details screen
        /// </summary>
        public RelayCommand ShowConnectToLocalCommand
        {
            get
            {
                if (_showConnectToLocalCommand == null)
                {
                    _showConnectToLocalCommand = new RelayCommand(
                        p => ShowConnectToLocal_Executed())
                    {
                        FriendlyName = Message.MainPage_Menu_ConnectToLocalDb
                    };
                    base.ViewModelCommands.Add(_showConnectToLocalCommand);
                }
                return _showConnectToLocalCommand;
            }
        }

        private void ShowConnectToLocal_Executed()
        {
            if (this.AttachedControl != null)
            {
                //ConnectToDatabaseWindow win = new ConnectToDatabaseWindow(ConnectionType.LocalDatabase);
                //win.DatabaseConnectionCreated += ConnectToDatabase_DatabaseConnectionCreated;

                //App.ShowWindow(win, true);

                //win.DatabaseConnectionCreated -= ConnectToDatabase_DatabaseConnectionCreated;
            }
        }

        #endregion

        #region ShowConnectToSQL

        private RelayCommand _showConnectToSQLCommand;

        /// <summary>
        /// Shows the connect to database window from
        /// the ente sql details screen
        /// </summary>
        public RelayCommand ShowConnectToSQLCommand
        {
            get
            {
                if (_showConnectToSQLCommand == null)
                {
                    _showConnectToSQLCommand = new RelayCommand(
                        p => ShowConnectToSQL_Executed())
                    {
                        FriendlyName = Message.MainPage_Menu_ConnectToSqlServerDb,
                        SmallIcon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_Connect
                    };
                    base.ViewModelCommands.Add(_showConnectToSQLCommand);
                }
                return _showConnectToSQLCommand;
            }
        }

        private void ShowConnectToSQL_Executed()
        {
            if (this.AttachedControl != null)
            {
                ConnectToDatabaseWindow win = new ConnectToDatabaseWindow(ConnectionType.SqlDatabase);
                win.DatabaseConnectionCreated += ConnectToDatabase_DatabaseConnectionCreated;

                App.ShowWindow(win, true);

                win.DatabaseConnectionCreated -= ConnectToDatabase_DatabaseConnectionCreated;
            }
        }

        #endregion

        #region RefreshAll

        private RelayCommand _refreshAllCommand;

        /// <summary>
        /// Refreshes and attempts to connect to all databases in the list
        /// </summary>
        public RelayCommand RefreshAllCommand
        {
            get
            {
                if (_refreshAllCommand == null)
                {
                    _refreshAllCommand = new RelayCommand(
                        p => RefreshAll_Executed())
                    {
                        FriendlyName = Message.Main_RefreshAll,
                        SmallIcon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_RefreshAll
                    };
                    base.ViewModelCommands.Add(_refreshAllCommand);
                }
                return _refreshAllCommand;
            }
        }

        private void RefreshAll_Executed()
        {
            //get the current selected index
            Int32? currentSelectedIndex = null;
            if (this.SelectedDatabase != null)
            {
                currentSelectedIndex = _availableDatabases.IndexOf(this.SelectedDatabase);
            }

            //clear out the existing dijsplay list
            _availableDatabases.Clear();

            //get a list of the saved connections
            _settingsList.LoadSettingsList();

            foreach (Connection con in _settingsList.Connections)
            {
                DatabaseInfo info;

                //for VistaDb check if file exists
                //if (con.Type == ConnectionType.LocalDatabase)
                //{
                //    //Check if file exists
                //    if (!String.IsNullOrEmpty(con.FilePath))
                //    {
                //        FileInfo file = new FileInfo(con.FilePath);
                //        if (file.Exists)
                //        {
                //            info = new DatabaseInfo(con, true, _settingsList.BackupLocation);
                //            info.SourceConnection = con;

                //        }
                //        else
                //        {
                //            info = new DatabaseInfo(con, false, _settingsList.BackupLocation);
                //            info.SourceConnection = con;
                //        }
                //        _availableDatabases.Add(info);
                //    }
                //}
                //else
                //{
                    info = new DatabaseInfo(con);
                    info.SourceConnection = con;
                    _availableDatabases.Add(info);
                //}
            }

            //reselect the selected database if it is available
            if (currentSelectedIndex.HasValue)
            {
                this.SelectedDatabase = _availableDatabases.ElementAt(currentSelectedIndex.Value);
            }
            else
            {
                this.SelectedDatabase = _availableDatabases.FirstOrDefault();
            }
        }

        #endregion

        #region RemoveSelectedDatabase

        private RelayCommand _removeSelectedDatabaseCommand;

        /// <summary>
        /// Removes the selected database from the connection list
        /// </summary>
        public RelayCommand RemoveSelectedDatabaseCommand
        {
            get
            {
                if (_removeSelectedDatabaseCommand == null)
                {
                    _removeSelectedDatabaseCommand = new RelayCommand(
                        p => RemoveSelectedDatabase_Executed(),
                        p => RemoveSelectedDatabase_CanExecute())
                    {
                        FriendlyName = Message.Main_RemoveSelectedDatabase,
                        SmallIcon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_Disconnect
                    };
                    base.ViewModelCommands.Add(_removeSelectedDatabaseCommand);
                }
                return _removeSelectedDatabaseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RemoveSelectedDatabase_CanExecute()
        {
            return this.SelectedDatabase != null;
        }

        private void RemoveSelectedDatabase_Executed()
        {
            RemoveConnection(this.SelectedDatabase);
        }

        #endregion

        #region CreateNewDatabase

        private RelayCommand _createNewDatabaseCommand;

        /// <summary>
        /// Shows the wizard to add a new database
        /// </summary>
        public RelayCommand CreateNewDatabaseCommand
        {
            get
            {
                if (_createNewDatabaseCommand == null)
                {
                    _createNewDatabaseCommand = new RelayCommand(
                        p => CreateNewDatabase_Executed())
                    {
                        FriendlyDescription = Message.Main_CreateNewDatabase
                    };
                    base.ViewModelCommands.Add(_createNewDatabaseCommand);

                }
                return _createNewDatabaseCommand;
            }
        }

        private void CreateNewDatabase_Executed()
        {
            if (this.AttachedControl != null)
            {
                CreateDatabaseWindow win = new CreateDatabaseWindow();
                win.DatabaseCreated += CreateDatabaseWindow_DatabaseCreated;
                App.ShowWindow(win, true);
                win.DatabaseCreated -= CreateDatabaseWindow_DatabaseCreated;
            }
        }

        /// <summary>
        /// Adds the created connection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateDatabaseWindow_DatabaseCreated(object sender, ValueEventArgs<Connection> e)
        {
            AddNewConnection(e.ReturnObject);
        }

        #endregion

        #region BackupDatabase

        private RelayCommand _backupDatabaseCommand;

        /// <summary>
        /// Shows the window to start the database backup process
        /// </summary>
        public RelayCommand BackupDatabaseCommand
        {
            get
            {
                if (_backupDatabaseCommand == null)
                {
                    _backupDatabaseCommand = new RelayCommand(
                        p => BackupDatabase_Executed(),
                        p => BackupDatabase_CanExecute())
                    {
                        FriendlyName = Message.Main_BackupDatabase,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_BackupDatabase
                    };
                    base.ViewModelCommands.Add(_backupDatabaseCommand);

                    PropertyChanged += BackupDatabaseCommand_CanExecuteRequeryRequired;
                }
                return _backupDatabaseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean BackupDatabase_CanExecute()
        {
            //Check a database has been selected
            Boolean canExecute = this.SelectedDatabase != null;
            //Check it is a Vista type
            if (canExecute)
            {
                canExecute = false;//canExecute = this.SelectedDatabase.Type == ConnectionType.LocalDatabase;
            }
            //Check it is available
            if (canExecute)
            {
                canExecute = this.SelectedDatabase.IsAvailable;
            }

            return canExecute;
        }

        private void BackupDatabase_Executed()
        {
            if (this.AttachedControl != null)
            {
                Boolean startOperation = DisplayExclusiveDBUseRequiredDialog();

                if (startOperation)
                {
                    _backgroundWorker.CancelAsync();

                    //this.SelectedDatabase.BackupLocation = this._settingsList.BackupLocation;
                    BackupDatabaseWindow win = new BackupDatabaseWindow(this.SelectedDatabase);
                    App.ShowWindow(win, true);

                    if (win.DialogResult == true)
                    {
                        //Get the latest backup and maintenance dates from the db file
                        this.SelectedDatabase.GetBackupAndMaintenanceDates();
                    }

                    if (!_backgroundWorker.IsBusy)
                    {
                        _backgroundWorker.RunWorkerAsync();
                    }
                }
            }
        }

        private Boolean DisplayExclusiveDBUseRequiredDialog()
        {
            _ExclusiveDBUseRequiredDialogViewModel = new ExclusiveDBUseRequiredDialogViewModel();

            if (_ExclusiveDBUseRequiredDialogViewModel.ExclusiveDBUseRequiredDialogRequired())
            {
                return ShowExclusiveDBUseRequiredDialog();
            }
            return true;
        }

        /// <summary>
        /// Force a refresh of command enabled state if selected database availability changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackupDatabaseCommand_CanExecuteRequeryRequired(object sender, PropertyChangedEventArgs e)
        {
            if (_backupDatabaseCommand != null)
            {
                if (e.PropertyName == "IsAvailable")
                {
                    BackupDatabaseCommand.RaiseCanExecuteChanged();
                }
            }
        }
        #endregion

        #region SelectBackupFolderCommand

        private RelayCommand _selectBackupFolderCommand;

        /// <summary>
        /// Shows the open folder dialog to select the 
        /// database backup folder
        /// </summary>
        public RelayCommand SelectBackupFolderCommand
        {
            get
            {
                if (_selectBackupFolderCommand == null)
                {
                    _selectBackupFolderCommand = new RelayCommand
                    (p => SelectBackupFolderCommand_Executed());
                    base.ViewModelCommands.Add(_selectBackupFolderCommand);
                }
                return _selectBackupFolderCommand;

            }
        }

        private void SelectBackupFolderCommand_Executed()
        {
            System.Windows.Forms.FolderBrowserDialog diag = new System.Windows.Forms.FolderBrowserDialog();
            diag.SelectedPath = this.SelectedDatabase.BackupLocation;

            System.Windows.Forms.DialogResult result = diag.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)
            {
                this.SelectedDatabase.BackupLocation = diag.SelectedPath;
                this.SelectedDatabase = this.SelectedDatabase.Save();

                //Save the value into the SettingsList
                Int32 sourceConnectionId = _settingsList.Connections.IndexOf(this.SelectedDatabase.SourceConnection);

                _settingsList.BackupLocation = diag.SelectedPath;
                //_settingsList.SaveSettings(); Backup location assignment should take care of this

                //refresh the connections
                this.RefreshAllCommand.Execute();

            }
        }

        #endregion

        #region UpgradeDatabase

        private RelayCommand _upgradeDatabaseCommand;

        /// <summary>
        /// Shows the window to start the upgrade process
        /// </summary>
        public RelayCommand UpgradeDatabaseCommand
        {
            get
            {
                if (_upgradeDatabaseCommand == null)
                {
                    _upgradeDatabaseCommand = new RelayCommand(
                        p => UpgradeDatabase_Executed(),
                        p => UpgradeDatabase_CanExecute())
                    {
                        FriendlyName = Message.Main_UpgradeDatabase,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_UpgradeDatabase
                    };
                    base.ViewModelCommands.Add(_upgradeDatabaseCommand);
                }
                return _upgradeDatabaseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean UpgradeDatabase_CanExecute()
        {
            //Check a database has been selected
            Boolean canExecute = this.SelectedDatabase != null;
            //Check it is available
            if (canExecute)
            {
                canExecute = this.SelectedDatabase.IsAvailable;
            }

            return canExecute;
        }

        private void UpgradeDatabase_Executed()
        {
            Boolean startOperation = DisplayExclusiveDBUseRequiredDialog();

            if (startOperation)
            {
                _backgroundWorker.CancelAsync();
                if (this.AttachedControl != null)
                {
                    UpgradeDatabaseWindow win = new UpgradeDatabaseWindow(this.SelectedDatabase);
                    App.ShowWindow(win, true);
                }
                if (!_backgroundWorker.IsBusy)
                {
                    _backgroundWorker.RunWorkerAsync();
                }
            }
        }

        /// <summary>
        /// Force a refresh of command enabled state if selected database availability changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpgradeDatabaseCommand_CanExecuteRequeryRequired(object sender, PropertyChangedEventArgs e)
        {
            if (_upgradeDatabaseCommand != null)
            {
                if (e.PropertyName == "IsAvailable")
                {
                    UpgradeDatabaseCommand.RaiseCanExecuteChanged();
                }
            }
        }
        #endregion

        #region DatabaseMaintenance

        private RelayCommand _databaseMaintenanceCommand;

        /// <summary>
        /// Shows the window to start the maintenance process
        /// </summary>
        public RelayCommand DatabaseMaintenanceCommand
        {
            get
            {
                if (_databaseMaintenanceCommand == null)
                {
                    _databaseMaintenanceCommand = new RelayCommand(
                        p => DatabaseMaintenance_Executed(),
                        p => DatabaseMaintenance_CanExecute())
                    {
                        FriendlyName = Message.Main_DatabaseMaintenance,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_DatabaseMaintenance
                    };
                    base.ViewModelCommands.Add(_databaseMaintenanceCommand);
                }
                return _databaseMaintenanceCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean DatabaseMaintenance_CanExecute()
        {
            //Check a database has been selected
            Boolean canExecute = this.SelectedDatabase != null;
            //Check it is a Vista type
            if (canExecute)
            {
                canExecute = false; // canExecute = this.SelectedDatabase.Type == ConnectionType.LocalDatabase;
            }
            //Check it is available
            if (canExecute)
            {
                canExecute = this.SelectedDatabase.IsAvailable;
            }

            return canExecute;
        }

        private void DatabaseMaintenance_Executed()
        {
            Boolean startOperation = DisplayExclusiveDBUseRequiredDialog();

            if (startOperation)
            {
                _backgroundWorker.CancelAsync();
                //if no backup location is selected then show the select window
                if (String.IsNullOrEmpty(this.SelectedDatabase.BackupLocation))
                {
                    this.SelectBackupFolderCommand.Execute();
                }

                //proceed unless user canceled Backup Location selection
                if (!String.IsNullOrEmpty(this.SelectedDatabase.BackupLocation))
                {
                    DatabaseMaintenanceWindow win = new DatabaseMaintenanceWindow(this.SelectedDatabase);
                    App.ShowWindow(win, true);

                    if (win.DialogResult == true)
                    {
                        //Get the latest backup and maintenance dates from the db file
                        this.SelectedDatabase.GetBackupAndMaintenanceDates();
                    }
                }
                if (!_backgroundWorker.IsBusy)
                {
                    _backgroundWorker.RunWorkerAsync();
                }
            }
        }

        /// <summary>
        /// Force a refresh of command enabled state if selected database availability changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DatabaseMaintenanceCommand_CanExecuteRequeryRequired(object sender, PropertyChangedEventArgs e)
        {
            if (_databaseMaintenanceCommand != null)
            {
                if (e.PropertyName == "IsAvailable")
                {
                    DatabaseMaintenanceCommand.RaiseCanExecuteChanged();
                }
            }
        }
        #endregion

        #region RestoreDatabase

        private RelayCommand _restoreDatabaseCommand;

        /// <summary>
        /// Shows the window to start the database restore process
        /// </summary>
        public RelayCommand RestoreDatabaseCommand
        {
            get
            {
                if (_restoreDatabaseCommand == null)
                {
                    _restoreDatabaseCommand = new RelayCommand(
                        p => RestoreDatabase_Executed(),
                        p => RestoreDatabase_CanExecute())
                    {
                        FriendlyName = Message.Main_RestoreDatabase,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_RestoreDatabase
                    };
                    base.ViewModelCommands.Add(_restoreDatabaseCommand);
                }
                return _restoreDatabaseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean RestoreDatabase_CanExecute()
        {
            //Check a database has been selected
            Boolean canExecute = this.SelectedDatabase != null;
            //Check it is a Vista type
            if (canExecute)
            {
                canExecute = false;// canExecute = this.SelectedDatabase.Type == ConnectionType.LocalDatabase;
            }
            //Check it is available
            if (canExecute)
            {
                canExecute = this.SelectedDatabase.IsAvailable;
            }

            return canExecute;
        }

        private void RestoreDatabase_Executed()
        {
            Boolean startOperation = DisplayExclusiveDBUseRequiredDialog();

            if (startOperation)
            {
                _backgroundWorker.CancelAsync();

                //if no backup location is selected then show the select window
                if (String.IsNullOrEmpty(this.SelectedDatabase.BackupLocation))
                {
                    this.SelectBackupFolderCommand.Execute();
                }

                //proceed unless user canceled Backup Location selection
                if (!String.IsNullOrEmpty(this.SelectedDatabase.BackupLocation))
                {
                    RestoreDatabaseWindow win = new RestoreDatabaseWindow(this.SelectedDatabase);
                    App.ShowWindow(win, true);

                    if (win.DialogResult == true)
                    {
                        //Get the latest backup and maintenance dates from the restored file
                        this.SelectedDatabase.GetBackupAndMaintenanceDates();
                    }
                }
                if (!_backgroundWorker.IsBusy)
                {
                    _backgroundWorker.RunWorkerAsync();
                }
            }
        }

        /// <summary>
        /// Force a refresh of command enabled state if selected database availability changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RestoreDatabaseCommand_CanExecuteRequeryRequired(object sender, PropertyChangedEventArgs e)
        {
            if (_restoreDatabaseCommand != null)
            {
                if (e.PropertyName == "IsAvailable")
                {
                    RestoreDatabaseCommand.RaiseCanExecuteChanged();
                }
            }
        }
        #endregion

        #region MoveDatabase

        private RelayCommand _moveDatabaseCommand;

        /// <summary>
        /// Shows the window to start the database move process
        /// </summary>
        public RelayCommand MoveDatabaseCommand
        {
            get
            {
                if (_moveDatabaseCommand == null)
                {
                    _moveDatabaseCommand = new RelayCommand(
                        p => MoveDatabase_Executed(),
                        p => MoveDatabase_CanExecute())
                    {
                        FriendlyName = Message.Main_MoveDatabase,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_MoveDatabase
                    };
                    base.ViewModelCommands.Add(_moveDatabaseCommand);
                }
                return _moveDatabaseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean MoveDatabase_CanExecute()
        {
            //Check a database has been selected
            Boolean canExecute = this.SelectedDatabase != null;
            //Check it is a Vista type
            if (canExecute)
            {
                canExecute = false; // canExecute = this.SelectedDatabase.Type == ConnectionType.LocalDatabase;
            }
            //Check it is available
            if (canExecute)
            {
                canExecute = this.SelectedDatabase.IsAvailable;
            }

            return canExecute;
        }

        private void MoveDatabase_Executed()
        {
            Boolean startOperation = DisplayExclusiveDBUseRequiredDialog();

            if (startOperation)
            {
                _backgroundWorker.CancelAsync();

                if (this.AttachedControl != null)
                {
                    //show the window as a dialog
                    MoveDatabaseWindow win = new MoveDatabaseWindow(this.SelectedDatabase);
                    App.ShowWindow(win, true);

                    this.RefreshAllCommand.Execute();
                }
                if (!_backgroundWorker.IsBusy)
                {
                    _backgroundWorker.RunWorkerAsync();
                }
            }
        }

        /// <summary>
        /// Force a refresh of command enabled state if selected database availability changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveDatabaseCommand_CanExecuteRequeryRequired(object sender, PropertyChangedEventArgs e)
        {
            if (_moveDatabaseCommand != null)
            {
                if (e.PropertyName == "IsAvailable")
                {
                    MoveDatabaseCommand.RaiseCanExecuteChanged();
                }
            }
        }
        #endregion

        #region TransferDatabase

        private RelayCommand _transferDatabaseCommand;

        /// <summary>
        /// Opens the window to transfer the database
        /// </summary>
        public RelayCommand TransferDatabaseCommand
        {
            get
            {
                if (_transferDatabaseCommand == null)
                {
                    _transferDatabaseCommand = new RelayCommand(
                        p => TransferDatabase_Executed())
                    {
                        FriendlyName = Message.Main_TransferDatabase,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_TransferDatabase
                    };
                    base.ViewModelCommands.Add(_transferDatabaseCommand);
                }
                return _transferDatabaseCommand;
            }
        }

        private void TransferDatabase_Executed()
        {
            _backgroundWorker.CancelAsync();

            //[TODO]
            if (!_backgroundWorker.IsBusy)
            {
                _backgroundWorker.RunWorkerAsync();
            }
        }

        #endregion

        #region DeleteDatabase

        private RelayCommand _deleteDatabaseCommand;

        /// <summary>
        /// Opens the window to start the delete database process
        /// </summary>
        public RelayCommand DeleteDatabaseCommand
        {
            get
            {
                if (_deleteDatabaseCommand == null)
                {
                    _deleteDatabaseCommand = new RelayCommand(
                        p => DeleteDatabase_Executed(),
                        p => DeleteDatabase_CanExecute())
                    {
                        FriendlyName = Message.Main_DeleteDatabase,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Main_DeleteDatabase
                    };
                    base.ViewModelCommands.Add(_deleteDatabaseCommand);
                }
                return _deleteDatabaseCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean DeleteDatabase_CanExecute()
        {
            //Check a database has been selected
            Boolean canExecute = this.SelectedDatabase != null;
            //Check it is a Vista type
            if (canExecute)
            {
                canExecute = false;//canExecute = this.SelectedDatabase.Type == ConnectionType.LocalDatabase;
            }
            //Check it is available
            if (canExecute)
            {
                canExecute = this.SelectedDatabase.IsAvailable;
            }

            return canExecute;
        }

        private void DeleteDatabase_Executed()
        {
            Boolean startOperation = DisplayExclusiveDBUseRequiredDialog();

            if (startOperation)
            {
                _backgroundWorker.CancelAsync();

                if (this.AttachedControl != null)
                {
                    DeleteDatabaseWindow win = new DeleteDatabaseWindow(this.SelectedDatabase);

                    //Subscribe to database deleted event so we can update the connections lists when database deleted
                    win.DatabaseDeleted += DeleteDatabase_DatabaseDeleted;

                    //show as dialog
                    App.ShowWindow(win, true);

                    win.DatabaseDeleted -= DeleteDatabase_DatabaseDeleted;
                }
                if (!_backgroundWorker.IsBusy)
                {
                    _backgroundWorker.RunWorkerAsync();
                }
            }
        }

        /// <summary>
        /// Once database is deleted, we need to update list of databases
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteDatabase_DatabaseDeleted(object sender, ValueEventArgs<DatabaseInfo> e)
        {
            //remove the database from the settings and available databases
            RemoveConnection(e.ReturnObject);
        }

        /// <summary>
        /// Force a refresh of command enabled state if selected database availability changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteDatabaseCommand_CanExecuteRequeryRequired(object sender, PropertyChangedEventArgs e)
        {
            if (_deleteDatabaseCommand != null)
            {
                if (e.PropertyName == "IsAvailable")
                {
                    DeleteDatabaseCommand.RaiseCanExecuteChanged();
                }
            }
        }
        #endregion

        #region ShowOptionsCommand
        private RelayCommand _showOptionsCommand;
        public RelayCommand ShowOptionsCommand
        {
            get
            {
                if (_showOptionsCommand == null)
                {
                    _showOptionsCommand = new RelayCommand(
                        p => ShowOptions_Executed((String)p))
                    {
                        FriendlyName = Message.Backstage_Options,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Options_16
                    };
                    this.ViewModelCommands.Add(_showOptionsCommand);
                }
                return _showOptionsCommand;
            }
        }


        private void ShowOptions_Executed(String initialTabName)
        {
            //check organiser is attached - this indicates we are running design code
            if (this.AttachedControl != null)
            {
                IEnumerable<OptionsScreen> openWindows = Application.Current.Windows.OfType<OptionsScreen>();
                OptionsScreen optionsWindow;
                //Get the tab to select first
                Int32 initialTab = 0;
                if (initialTabName.Equals(Message.Options_PageButtons_Backup))
                {
                    initialTab = 1;
                }


                if (openWindows.Any() && openWindows.First() != null)
                {
                    optionsWindow = (OptionsScreen)openWindows.First();

                    if (optionsWindow.WindowState == WindowState.Minimized)
                    {
                        optionsWindow.WindowState = WindowState.Normal;
                    }
                    optionsWindow.Activate();
                    //Set the initial tab
                    optionsWindow.xOptionsTabControl.SelectedIndex = initialTab;
                }
                else
                {
                    //Set the initial tab on creation
                    optionsWindow = new OptionsScreen(initialTab);

                    optionsWindow.Owner = this.AttachedControl.FindVisualAncestor<ExtendedRibbonWindow>();
                    optionsWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    App.ShowWindow(optionsWindow, true);
                }

                //Update the backup locations in each database info object
                foreach (DatabaseInfo info in this._availableDatabases)
                {
                    info.BackupLocation = this._settingsList.BackupLocation;
                }
            }
        }

        #endregion

        #region ExitCommand

        private RelayCommand _exitCommand;

        /// <summary>
        /// Exits the application
        /// </summary>
        public RelayCommand ExitCommand
        {
            get
            {
                if (_exitCommand == null)
                {
                    _exitCommand = new RelayCommand(
                        p => Exit_Executed())
                    {
                        FriendlyName = Message.Backstage_Exit,
                        Icon = Galleria.Ccm.Maintenance.Client.Wpf.Resources.ImageResources.Exit
                    };
                    base.ViewModelCommands.Add(_exitCommand);
                }
                return _exitCommand;
            }
        }

        private void Exit_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }

            if (!_backgroundWorker.IsBusy)
            {
                _backgroundWorker.CancelAsync(); //not sure if needed, added just in case; What happens if you are trying to exit application without cancelling the thread?
            }
        }

        #endregion

        #region OpenLicenseScreenCommand

        private RelayCommand _openLicenseScreenCommand;

        /// <summary>
        /// Shows the licensing screen
        /// </summary>
        public RelayCommand OpenLicenseScreenCommand
        {
            get
            {
                if (_openLicenseScreenCommand == null)
                {
                    _openLicenseScreenCommand = new RelayCommand(
                        p => OpenLicenseScreen_Executed())
                    {
                        FriendlyName = Message.Generic_Open
                    };
                    base.ViewModelCommands.Add(_openLicenseScreenCommand);
                }
                return _openLicenseScreenCommand;
            }
        }

        private void OpenLicenseScreen_Executed()
        {
            var license = SecurityDialogViewModel.OpenLicenceScreen();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Adds the new connection to the settingslist &
        /// available databases list
        /// </summary>
        /// <param name="con"></param>
        private void AddNewConnection(Connection con)
        {
            //add the connection to the settings list
            _settingsList.Connections.Add(con);

            //save
            _settingsList.SaveSettings();

            //refresh the connections
            this.RefreshAllCommand.Execute();
        }

        /// <summary>
        /// Remvoes the connection from the settingslist and
        /// the available databases list
        /// </summary>
        /// <param name="info"></param>
        private void RemoveConnection(DatabaseInfo info)
        {
            if (this.AttachedControl != null)
            {
                //remove the source connection from the settings list
                _settingsList.Connections.Remove(this.SelectedDatabase.SourceConnection);
                _settingsList.SaveSettings();
            }

            //remove the selected db from the available list
            _availableDatabases.Remove(this.SelectedDatabase);

            //select the next available database
            this.SelectedDatabase = _availableDatabases.FirstOrDefault();
        }

        /// <summary>
        /// Updates the permission flags
        /// </summary>
        private void UpdatePermissionFlags()
        {
            // licensing permissions - does this user have the permission to relicence the application
            // ToDo: Enable this when permission have been configured
            //_userHasLicensingPerm = ApplicationContext.User.IsInRole(DomainPermission.License.ToString());

            _userHasLicensingPerm = true;
        }

        #endregion

        #region Event Handlers


        //Subscribe/Unsubscribe command button requery events to the SelectedDatabase change
        private void OnSelectedDatabaseChanged(DatabaseInfo oldValue, DatabaseInfo newValue)
        {
            if (oldValue != null)
            {
                oldValue.PropertyChanged -= BackupDatabaseCommand_CanExecuteRequeryRequired;
                oldValue.PropertyChanged -= UpgradeDatabaseCommand_CanExecuteRequeryRequired;
                oldValue.PropertyChanged -= DatabaseMaintenanceCommand_CanExecuteRequeryRequired;
                oldValue.PropertyChanged -= RestoreDatabaseCommand_CanExecuteRequeryRequired;
                oldValue.PropertyChanged -= MoveDatabaseCommand_CanExecuteRequeryRequired;
                oldValue.PropertyChanged -= DeleteDatabaseCommand_CanExecuteRequeryRequired;
            }

            if (newValue != null)
            {
                newValue.PropertyChanged += BackupDatabaseCommand_CanExecuteRequeryRequired;
                newValue.PropertyChanged += UpgradeDatabaseCommand_CanExecuteRequeryRequired;
                newValue.PropertyChanged += DatabaseMaintenanceCommand_CanExecuteRequeryRequired;
                newValue.PropertyChanged += RestoreDatabaseCommand_CanExecuteRequeryRequired;
                newValue.PropertyChanged += MoveDatabaseCommand_CanExecuteRequeryRequired;
                newValue.PropertyChanged += DeleteDatabaseCommand_CanExecuteRequeryRequired;
            }
        }

        /// <summary>
        /// Show the Customer Experiance Improvment program window
        /// </summary>
        private Boolean ShowExclusiveDBUseRequiredDialog()
        {
            _ExclusiveDBUseRequiredDialog = new ExclusiveDBUseRequiredDialog(_ExclusiveDBUseRequiredDialogViewModel);
            _ExclusiveDBUseRequiredDialog.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Boolean? result = _ExclusiveDBUseRequiredDialog.ShowDialog();

            return (Boolean)(result != null ? result : true);
        }

        /// <summary>
        /// Carries out the work
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker senderWorker = (BackgroundWorker)sender;

            while (!senderWorker.CancellationPending)
            {
                senderWorker.ReportProgress(0);
                //start reporting every 5 minutes, test for cancellation every sec
                for (Int32 i = 1; i <= _reportProgress; i += _checkForCancellation)
                {
                    if (senderWorker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }
                    Thread.Sleep(_checkForCancellation); //1 sec
                }
            }
        }

        /// <summary>
        /// Checks availability of each of the available databases
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            foreach (DatabaseInfo info in this.AvailableDatabases)
            {
                info.CheckAvailability(false); // run with a dispatcher priority : backgroud
            }
        }
        #endregion

        #region IDisposable members

        protected override void Dispose(bool disposing)
        {
            //unsubscribe the worker
            _backgroundWorker.CancelAsync();
            _backgroundWorker.DoWork -= worker_DoWork;
            _backgroundWorker.ProgressChanged -= _backgroundWorker_ProgressChanged;
            _selectedDatabase.PropertyChanged -= BackupDatabaseCommand_CanExecuteRequeryRequired;
            _selectedDatabase.PropertyChanged -= UpgradeDatabaseCommand_CanExecuteRequeryRequired;
            _selectedDatabase.PropertyChanged -= DatabaseMaintenanceCommand_CanExecuteRequeryRequired;
            _selectedDatabase.PropertyChanged -= RestoreDatabaseCommand_CanExecuteRequeryRequired;
            _selectedDatabase.PropertyChanged -= MoveDatabaseCommand_CanExecuteRequeryRequired;
            _selectedDatabase.PropertyChanged -= DeleteDatabaseCommand_CanExecuteRequeryRequired;
            this._state = null;
        }

        #endregion
    }
}
