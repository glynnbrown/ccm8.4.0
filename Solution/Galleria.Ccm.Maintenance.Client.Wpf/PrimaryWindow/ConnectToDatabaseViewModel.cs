﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using System.Collections.ObjectModel;
using System.Windows;
using Galleria.Framework.Helpers;
using System.ComponentModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using System.Diagnostics;
using Galleria.Framework.Dal.Configuration;
using System.IO;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    public enum ConnectToDatabaseStep
    {
        SelectDbType,
        LocalEnterDetails,
        SqlEnterDetails,
        AttemptingConnection,
        Complete,
        ConnectionFailed
    }

    /// <summary>
    /// A viewModel that handles the process of connecting to a database and adding 
    /// it to the list of available databases. Handles cases where database is already
    /// in the list/ database is not a valid Gfs database etc
    /// </summary>
    public sealed class ConnectToDatabaseViewModel : ViewModelAttachedControlObject<ConnectToDatabaseWindow>
    {
        #region Constants
        const String cVistaDb = "VistaDb"; //Default vista dal name
        const String cMssql = "Mssql"; // default sql dal name
        #endregion

        #region Fields
        private ConnectToDatabaseStep _currentStep = ConnectToDatabaseStep.SelectDbType;
        private ConnectionType _selectedConnectionType = ConnectionType.SqlDatabase;
        private ReadOnlyObservableCollection<ConnectionType> _availableConnectionTypesRO;
        private String _locationInput1;
        private String _locationInput2;
        private BackgroundWorker _backgroundWorker;

        #endregion

        #region Binding Property Paths

        public static readonly PropertyPath CurrentStepProperty = WpfHelper.GetPropertyPath<ConnectToDatabaseViewModel>(p => p.CurrentStep);
        public static readonly PropertyPath SelectedConnectionTypeProperty = WpfHelper.GetPropertyPath<ConnectToDatabaseViewModel>(p => p.SelectedConnectionType);
        public static readonly PropertyPath AvailableConnectionTypesProperty = WpfHelper.GetPropertyPath<ConnectToDatabaseViewModel>(p => p.AvailableConnectionTypes);
        public static readonly PropertyPath LocationInput1Property = WpfHelper.GetPropertyPath<ConnectToDatabaseViewModel>(p => p.LocationInput1);
        public static readonly PropertyPath LocationInput2Property = WpfHelper.GetPropertyPath<ConnectToDatabaseViewModel>(p => p.LocationInput2);
        public static readonly PropertyPath IsNextCommandVisibleProperty = WpfHelper.GetPropertyPath<ConnectToDatabaseViewModel>(p => p.IsNextCommandVisible);

        #endregion

        #region Properties

        /// <summary>
        /// Returns the current step
        /// </summary>
        public ConnectToDatabaseStep CurrentStep
        {
            get { return _currentStep; }
            private set
            {
                _currentStep = value;
                OnPropertyChanged(CurrentStepProperty);
                OnPropertyChanged(IsNextCommandVisibleProperty);
            }
        }

        /// <summary>
        /// Returns the collection of available connection types
        /// </summary>
        public ReadOnlyObservableCollection<ConnectionType> AvailableConnectionTypes
        {
            get
            {
                if (_availableConnectionTypesRO == null)
                {
                    _availableConnectionTypesRO = new ReadOnlyObservableCollection<ConnectionType>(
                        new ObservableCollection<ConnectionType>()
                        {
                            ConnectionType.SqlDatabase
                        });
                }
                return _availableConnectionTypesRO;
            }
        }

        /// <summary>
        /// Gets/Sets the selected connection type
        /// </summary>
        public ConnectionType SelectedConnectionType
        {
            get { return _selectedConnectionType; }
            set
            {
                _selectedConnectionType = value;
                OnPropertyChanged(SelectedConnectionTypeProperty);
            }
        }

        /// <summary>
        /// Gets/Sets the location 1 value
        /// </summary>
        public String LocationInput1
        {
            get { return _locationInput1; }
            set
            {
                _locationInput1 = value;
                OnPropertyChanged(LocationInput1Property);
            }
        }

        /// <summary>
        /// Gets/Sets the location 2 value
        /// </summary>
        public String LocationInput2
        {
            get { return _locationInput2; }
            set
            {
                _locationInput2 = value;
                OnPropertyChanged(LocationInput2Property);
            }
        }

        #endregion

        #region Events

        public event EventHandler<ValueEventArgs<Connection>> DatabaseConnectionCreated;
        private void RaiseDatabaseConnectionCreated(Connection con)
        {
            if (this.DatabaseConnectionCreated != null)
            {
                this.DatabaseConnectionCreated(this, new ValueEventArgs<Connection>(con));
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ConnectToDatabaseViewModel()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="selectedType">The connection type to start with</param>
        public ConnectToDatabaseViewModel(ConnectionType selectedType)
        {
            this.SelectedConnectionType = selectedType;
            this.NextCommand.Execute();
        }

        #endregion

        #region Commands

        #region BackCommand

        private RelayCommand _backCommand;

        /// <summary>
        /// Returns to the previous step screen
        /// </summary>
        public RelayCommand BackCommand
        {
            get
            {
                if (_backCommand == null)
                {
                    _backCommand = new RelayCommand(
                        p => Back_Executed(),
                        p => Back_CanExecute())
                    {
                        FriendlyName = Message.Generic_Back
                    };
                    base.ViewModelCommands.Add(_backCommand);
                }
                return _backCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Back_CanExecute()
        {
            return this.CurrentStep == ConnectToDatabaseStep.LocalEnterDetails ||
                    this.CurrentStep == ConnectToDatabaseStep.SqlEnterDetails ||
                    this.CurrentStep == ConnectToDatabaseStep.ConnectionFailed;
        }

        private void Back_Executed()
        {
            switch (this.CurrentStep)
            {
                case ConnectToDatabaseStep.LocalEnterDetails:
                case ConnectToDatabaseStep.SqlEnterDetails:
                    this.CurrentStep = ConnectToDatabaseStep.SelectDbType;
                    break;

                case ConnectToDatabaseStep.ConnectionFailed:
                    switch (this.SelectedConnectionType)
                    {
                        //case ConnectionType.LocalDatabase:
                        //    this.CurrentStep = ConnectToDatabaseStep.LocalEnterDetails;
                        //    break;
                        case ConnectionType.SqlDatabase:
                            this.CurrentStep = ConnectToDatabaseStep.SqlEnterDetails;
                            break;
                        default: throw new NotImplementedException();
                    }
                    break;

                default: throw new NotImplementedException();
            }

        }

        #endregion

        #region NextCommand

        private RelayCommand _nextCommand;

        /// <summary>
        /// Moves to the next step
        /// </summary>
        public RelayCommand NextCommand
        {
            get
            {
                if (_nextCommand == null)
                {
                    _nextCommand = new RelayCommand(
                        p => Next_Executed(),
                        p => Next_CanExecute())
                    {
                        FriendlyName = Message.Generic_Next
                    };
                    base.ViewModelCommands.Add(_nextCommand);
                }
                return _nextCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Next_CanExecute()
        {
            if (this.CurrentStep == ConnectToDatabaseStep.LocalEnterDetails)
            {
                return !String.IsNullOrEmpty(this.LocationInput1);
            }
            else if (this.CurrentStep == ConnectToDatabaseStep.SqlEnterDetails)
            {
                return !String.IsNullOrEmpty(this.LocationInput1) && !String.IsNullOrEmpty(this.LocationInput2);
            }
            else if (this.CurrentStep == ConnectToDatabaseStep.ConnectionFailed)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void Next_Executed()
        {
            switch (this.CurrentStep)
            {
                case ConnectToDatabaseStep.SelectDbType:
                    switch (this.SelectedConnectionType)
                    {
                        //case ConnectionType.LocalDatabase:
                        //    this.CurrentStep = ConnectToDatabaseStep.LocalEnterDetails;
                        //    break;
                        case ConnectionType.SqlDatabase:
                            this.CurrentStep = ConnectToDatabaseStep.SqlEnterDetails;
                            break;
                        default: throw new NotImplementedException();
                    }
                    break;

                case ConnectToDatabaseStep.LocalEnterDetails:
                case ConnectToDatabaseStep.SqlEnterDetails:

                    //Check not already in list. If so inform user and prevent
                    //next step
                    if (!DatabaseAlreadySelected())
                    {
                        //test the connection
                        this.CurrentStep = ConnectToDatabaseStep.AttemptingConnection;
                        TestConnection();
                    }
                    break;

                case ConnectToDatabaseStep.Complete:
                    //process is complete so close the window
                    if (this.AttachedControl != null)
                    {
                        this.AttachedControl.Close();
                    }
                    break;

            }


        }

        /// <summary>
        /// Returns true if the next command button should be shown
        /// </summary>
        public Boolean IsNextCommandVisible
        {
            get
            {
                return this.CurrentStep != ConnectToDatabaseStep.AttemptingConnection;
            }
        }

        #endregion

        #region CancelCommand

        private RelayCommand _cancelCommand;

        /// <summary>
        /// Cancels the connection
        /// </summary>
        public RelayCommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                {
                    _cancelCommand = new RelayCommand(
                        p => Cancel_Executed(),
                        p => Cancel_CanExecute())
                    {
                        FriendlyName = Message.Generic_Cancel
                    };
                    base.ViewModelCommands.Add(_cancelCommand);
                }
                return _cancelCommand;
            }
        }

        [DebuggerStepThrough]
        private Boolean Cancel_CanExecute()
        {
            return
                this.CurrentStep == ConnectToDatabaseStep.SelectDbType ||
                this.CurrentStep == ConnectToDatabaseStep.LocalEnterDetails ||
                this.CurrentStep == ConnectToDatabaseStep.SqlEnterDetails ||
                this.CurrentStep == ConnectToDatabaseStep.ConnectionFailed;
        }

        private void Cancel_Executed()
        {
            if (this.AttachedControl != null)
            {
                this.AttachedControl.Close();
            }
        }

        #endregion

        #region BrowseForLocalDatabase

        private RelayCommand _browseForLocalDatabaseCommand;

        /// <summary>
        /// Shows the open file dialog to select a local database file
        /// </summary>
        public RelayCommand BrowseForLocalDatabaseCommand
        {
            get
            {
                if (_browseForLocalDatabaseCommand == null)
                {
                    _browseForLocalDatabaseCommand = new RelayCommand(
                        p => BrowseForLocalDatabase_Executed())
                    {
                        FriendlyName = Message.ConnectToDatabase_BrowseForLocalDatabaseCommand
                    };
                    base.ViewModelCommands.Add(_browseForLocalDatabaseCommand);
                }
                return _browseForLocalDatabaseCommand;
            }
        }

        private void BrowseForLocalDatabase_Executed()
        {
            Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
            dialog.Filter = Message.ConnectToDatabase_OpenFileFilter;
            dialog.CheckFileExists = true;
            dialog.CheckPathExists = true;
            dialog.Multiselect = false;

            //Allow user to re-select if database already in list or cancel
            Boolean fileSelected = false;

            while (!fileSelected)
            {
                if (dialog.ShowDialog() == true)
                {
                    //check database is not already in the list
                    fileSelected = true;
                    this.LocationInput1 = dialog.FileName;

                    if (DatabaseAlreadySelected())
                    {
                        fileSelected = false;
                    }
                }
                else
                {
                    fileSelected = true;
                    this.LocationInput1 = null;
                }
            }
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Checks if the current database details entered by the user match a database
        /// that is already in the list
        /// </summary>
        /// <returns></returns>
        private Boolean DatabaseAlreadySelected()
        {
            Boolean databaseAlreadyInList = false;

            SettingsList settingsList = SettingsList.Instance;
            settingsList.LoadSettingsList();

            switch (this.SelectedConnectionType)
            {
                case ConnectionType.SqlDatabase:
                    foreach (Connection con in settingsList.Connections)
                    {
                        if (con.Type == this.SelectedConnectionType)
                        {
                            if (con.ServerName.Equals(this.LocationInput1))
                            {
                                //perform case insensitive comparison
                                if (String.Compare(con.DatabaseName, this.LocationInput2, true) == 0)
                                {
                                    databaseAlreadyInList = true;
                                    break;
                                }
                            }
                        }
                    }
                    break;

                //case ConnectionType.LocalDatabase:
                //    {
                //        foreach (Connection con in settingsList.Connections)
                //        {
                //            if (con.Type == this.SelectedConnectionType)
                //            {
                //                //perform case insensitive comparison
                //                if (String.Compare(con.FilePath, this.LocationInput1, true) == 0)
                //                {
                //                    databaseAlreadyInList = true;
                //                    break;
                //                }
                //            }
                //        }
                //    }
                //    break;
            }

            //inform user that the databsae 
            if (databaseAlreadyInList)
            {
                Framework.Controls.Wpf.ModalMessage dialog = new Framework.Controls.Wpf.ModalMessage()
                {
                    Title = Message.Application_Title,
                    Header = Message.ConnectToDatabase_Dialog_DBAlreadyExistsHeader,
                    Description = String.Format(Message.ConnectToDatabase_Dialog_DBAlreadyExistsMessage,
                        this.LocationInput2),
                    IsIconVisible = true,
                    WindowStartupLocation = WindowStartupLocation.CenterScreen,
                    MessageIcon = Resources.ImageResources.Dialog_Warning_24,
                    ButtonCount = 1,
                    Button1Content = Message.DialogButton_Ok,
                    Height = 200,
                    Width = 400
                };
                App.ShowWindow(dialog, true);
            }
            return databaseAlreadyInList;
        }

        /// <summary>
        /// Test the connection details
        /// </summary>
        private void TestConnection()
        {
            _backgroundWorker = new BackgroundWorker();
            _backgroundWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            _backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            _backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Carries out the work of testing the connection to ensure that a connection
        /// can be made and that the database is a valid Gfs database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _backgroundWorker = (BackgroundWorker)sender;

            switch (this.SelectedConnectionType)
            {
                //case ConnectionType.LocalDatabase:
                //    {
                //        //check file exists
                //        if (this.LocationInput1 != null)
                //        {
                //            e.Result = false;

                //            FileInfo fileInfo = new FileInfo(this.LocationInput1);

                //            if (fileInfo.Exists)
                //            {
                //                //Check if database is an GFS database
                //                //[TODO] Would be nice to separate out the checks so we can have:
                //                // Failure type 1 - file not found - show appropriate message
                //                // Failure type 2 - Not GFS database - show appropriate messsage

                //                // create a new config element for the dal
                //                DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(cVistaDb);
                //                dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("File", this.LocationInput1));

                //                // now create a new instance of the VistaDb dal to check if Gfs db
                //                //using (IDalFactory dalFactory = new Galleria.Ccm.Dal.VistaDb.DalFactory(dalFactoryConfig))
                //                //{
                //                //    String databaseType = dalFactory.GetDatabaseType();
                //                //    e.Result = (databaseType == Constants.SaDatabaseType);
                //                //};
                //            }
                //            return;
                //        }
                //        else
                //        {
                //            e.Result = true;
                //            return;
                //        }

                //    }

                case ConnectionType.SqlDatabase:
                    {
                        //Check if database is correct type
                        e.Result = false;

                        //[TODO] Would be nice to separate out failed connections and incorrect database types
                        // create a new config element for the dal
                        DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement();
                        dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Server", this.LocationInput1));
                        dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement("Database", this.LocationInput2));

                        // now create a new instance of the MSSql dal to check if Gfs db
                        using (IDalFactory dalFactory = new Galleria.Ccm.Dal.Mssql.DalFactory(dalFactoryConfig))
                        {
                            String databaseType = dalFactory.GetDatabaseType();
                            e.Result = (databaseType == Constants.CcmDatabaseType);
                        };
                        break;
                    }

            }
        }

        /// <summary>
        /// Handles the test connection worker completed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //unsubscribe the worker
            BackgroundWorker worker = (BackgroundWorker)sender;
            worker.DoWork -= worker_DoWork;
            worker.RunWorkerCompleted -= worker_RunWorkerCompleted;

            if ((bool)e.Result)
            {
                //create the database info object 
                Connection con = new Connection();
                con.Type = this.SelectedConnectionType;

                switch (con.Type)
                {
                    //case ConnectionType.LocalDatabase:
                    //    con.FilePath = this.LocationInput1;
                    //    break;

                    case ConnectionType.SqlDatabase:
                        con.ServerName = this.LocationInput1;
                        con.DatabaseName = this.LocationInput2;
                        break;
                }

                //raise out the object in the event
                RaiseDatabaseConnectionCreated(con);

                //move to the completed step
                this.NextCommand.FriendlyName = Message.Generic_Ok;
                this.CurrentStep = ConnectToDatabaseStep.Complete;

            }
            else
            {
                this.CurrentStep = ConnectToDatabaseStep.ConnectionFailed;
            }
        }

        #endregion

        #region IDisposable Members

        protected override void Dispose(bool disposing)
        {

        }

        #endregion
    }
}
