﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for BackupDatabaseWindow.xaml
    /// </summary>
    public partial class BackupDatabaseWindow : ExtendedRibbonWindow
    {
        #region Properties
        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(BackupDatabaseViewModel), typeof(BackupDatabaseWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public BackupDatabaseViewModel ViewModel
        {
            get { return (BackupDatabaseViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            BackupDatabaseWindow senderControl = (BackupDatabaseWindow)obj;

            if (e.OldValue != null)
            {
                BackupDatabaseViewModel oldModel = (BackupDatabaseViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                BackupDatabaseViewModel newModel = (BackupDatabaseViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;

            }
        }

        #endregion
        #endregion

        #region Constructor
        public BackupDatabaseWindow(DatabaseInfo info)
        {
            this.ViewModel = new BackupDatabaseViewModel(info);

            InitializeComponent();

            this.Loaded += new RoutedEventHandler(BackupDatabaseWindow_Loaded);
        }

        private void BackupDatabaseWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= BackupDatabaseWindow_Loaded;

            //queue the process on a delay
            Dispatcher.BeginInvoke(
                (Action)(() => this.ViewModel.BeginProcess()),
               priority: System.Windows.Threading.DispatcherPriority.ApplicationIdle);
        }
        #endregion

        #region Event Handlers

        private void FolderLocation_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.OpenBackupFolderLocationCommand.Execute();
            }
        }

        #endregion

        #region Window close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            //cancel the simple close
            e.Cancel = true;

            //execute the ok command instead
            if (this.ViewModel.OKCommand.CanExecute())
            {
                this.ViewModel.OKCommand.Execute();
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
