﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Galleria.Framework.Controls.Wpf;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for RestoreDatabaseWindow.xaml
    /// </summary>
    public partial class RestoreDatabaseWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(RestoreDatabaseViewModel), typeof(RestoreDatabaseWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        public RestoreDatabaseViewModel ViewModel
        {
            get { return (RestoreDatabaseViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelProperty, value); }
        }


        private static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            RestoreDatabaseWindow senderControl = (RestoreDatabaseWindow)obj;

            if (e.OldValue != null)
            {
                RestoreDatabaseViewModel oldModel = (RestoreDatabaseViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                RestoreDatabaseViewModel newModel = (RestoreDatabaseViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }


        }

        #endregion

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public RestoreDatabaseWindow(DatabaseInfo info)
        {
            this.ViewModel = new RestoreDatabaseViewModel(info);

            InitializeComponent();

            this.Loaded += new RoutedEventHandler(RestoreDatabaseWindow_Loaded);
        }

        private void RestoreDatabaseWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.Loaded -= RestoreDatabaseWindow_Loaded;
            this.ViewModel.SelectRestoreFileCommand.Execute();
        }


        #endregion

        #region Event Handlers

        private void FolderLocation_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (this.ViewModel != null)
            {
                this.ViewModel.OpenBackupFolderCommand.Execute();
            }
        }
        #endregion

        #region window close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            //cancel the simple close
            e.Cancel = true;

            //go through the viewmodel commands instead
            if (this.ViewModel.CancelCommand.CanExecute())
            {
                this.ViewModel.CancelCommand.Execute();
            }
            else
            {
                if (this.ViewModel.OKCommand.CanExecute())
                {
                    this.ViewModel.OKCommand.Execute();
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }

        #endregion
    }
}
