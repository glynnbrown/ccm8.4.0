﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using Galleria.Framework.Controls.Wpf;
using System.ComponentModel;

namespace Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow
{
    /// <summary>
    /// Interaction logic for DeleteDatabaseWindow.xaml
    /// </summary>
    public partial class DeleteDatabaseWindow : ExtendedRibbonWindow
    {
        #region Properties

        #region ViewModelProperty

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(DeleteDatabaseViewModel), typeof(DeleteDatabaseWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// 
        /// Gets/Sets the window viewmodel context
        /// 
        public DeleteDatabaseViewModel ViewModel
        {
            get { return (DeleteDatabaseViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }


        public static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            DeleteDatabaseWindow senderControl = (DeleteDatabaseWindow)obj;

            if (e.OldValue != null)
            {
                DeleteDatabaseViewModel oldModel = (DeleteDatabaseViewModel)e.OldValue;
                oldModel.AttachedControl = null;
                oldModel.DatabaseDeleted -= senderControl.ViewModel_DatabaseDeleted;
            }

            if (e.NewValue != null)
            {
                DeleteDatabaseViewModel newModel = (DeleteDatabaseViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
                newModel.DatabaseDeleted += senderControl.ViewModel_DatabaseDeleted;

            }
        }

        #endregion

        #endregion

        #region Event

        /// <summary>
        /// Notifies that the database has been deleted
        /// </summary>
        public event EventHandler<ValueEventArgs<DatabaseInfo>> DatabaseDeleted;
        private void RaiseDatabaseDeleted(DatabaseInfo info)
        {
            if (this.DatabaseDeleted != null)
            {
                this.DatabaseDeleted(this, new ValueEventArgs<DatabaseInfo>(info));
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="info">info for the database to be deleted</param>
        public DeleteDatabaseWindow(DatabaseInfo info)
        {
            InitializeComponent();

            this.ViewModel = new DeleteDatabaseViewModel(info);
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Relays the event back out
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewModel_DatabaseDeleted(object sender, Common.ValueEventArgs<DatabaseInfo> e)
        {
            RaiseDatabaseDeleted(e.ReturnObject);
        }

        #endregion

        #region window close

        protected override void OnCrossCloseRequested(CancelEventArgs e)
        {
            base.OnCrossCloseRequested(e);

            //cancel the simple close
            e.Cancel = true;

            //execute the ok command instead
            if (this.ViewModel.OKCommand.CanExecute())
            {
                this.ViewModel.OKCommand.Execute();
            }

        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            //Dispose of the viewmodel
            Dispatcher.BeginInvoke(
                (Action)(() =>
                {

                    if (this.ViewModel != null)
                    {
                        IDisposable oldModel = this.ViewModel;
                        this.ViewModel = null;
                        oldModel.Dispose();
                    }
                }),
                    priority: System.Windows.Threading.DispatcherPriority.Background);
        }


        #endregion
    }
}
