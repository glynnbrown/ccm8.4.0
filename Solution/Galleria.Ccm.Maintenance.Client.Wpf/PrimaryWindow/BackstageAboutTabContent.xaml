﻿<!--Header Information-->
<!--Copyright © Galleria RTS Ltd 2014.-->
<!--Version History : (CCM 8.0)-->
<!--
    CCM-25903 : N.Haywood
      Copied from SA
    V8-27544 : M.Shelley
        Added licence details to the Help page
-->
<UserControl
    x:Class="Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow.BackstageAboutTabContent"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:lang="clr-namespace:Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language"
    xmlns:common="clr-namespace:Galleria.Ccm.Maintenance.Client.Wpf.Common"
    xmlns:root="clr-namespace:Galleria.Ccm.Maintenance.Client.Wpf"
	xmlns:res="clr-namespace:Galleria.Ccm.Maintenance.Client.Wpf.Resources"
	xmlns:g="clr-namespace:Galleria.Framework.Controls.Wpf;assembly=Galleria.Framework.Controls.Wpf"
    xmlns:commonlg="clr-namespace:Galleria.Ccm.Common.Wpf.Resources.Language;assembly=Galleria.Ccm.Common.Wpf"
	xmlns:local="clr-namespace:Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow"
    xmlns:license="clr-namespace:Galleria.Ccm.Common.Wpf.Licensing;assembly=Galleria.Ccm.Common.Wpf"
    x:Name="rootControl">

    <Grid MaxWidth="400"
          DataContext="{Binding Path=MainViewModel, ElementName=rootControl}">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
        </Grid.RowDefinitions>

        <!-- Header -->
        <StackPanel Grid.Row="0" Orientation="Horizontal">
            <!-- Use image of icon, not the icon itself for sharp rendering, and only use image that is the same size as the one shown-->
            <!--<Image Source="{x:Static res:ImageResources.SAMaintenance_About_96}" Height="96" Width="96" Margin="5"
				   VerticalAlignment="Bottom"></Image>-->
            <TextBlock Text="{x:Static lang:Message.Application_Title}" Margin="5,5,5,0"
					   HorizontalAlignment="Left" VerticalAlignment="Bottom" FontSize="18" FontWeight="SemiBold"
					   Foreground="{StaticResource {x:Static g:ResourceKeys.Main_BrushMidMain}}"
                       />
        </StackPanel>

        <!-- Horizontal Line-->
        <Border Grid.Row="1" HorizontalAlignment="Stretch" BorderThickness="0,1,0,0"
				Margin="0,10,0,10" BorderBrush="{StaticResource {x:Static g:ResourceKeys.ThemeColour_BrushBorder}}"/>

        <!-- Version Information Header -->
        <TextBlock Grid.Row="2" Margin="5,10,5,0" FontWeight="SemiBold"
				   HorizontalAlignment="Left" VerticalAlignment="Center"
				   Text="{x:Static lang:Message.About_VersionInformation}"/>

        <!-- Version Information-->
        <StackPanel Grid.Row="3" Orientation="Horizontal">
            <TextBlock Text="{x:Static lang:Message.Generic_Version}" Margin="5"
					   HorizontalAlignment="Left" VerticalAlignment="Center"/>
            <TextBlock Text="{Binding Path=BuildVersion, 
					   RelativeSource={RelativeSource Mode=FindAncestor, AncestorType={x:Type local:BackstageAboutTabContent}}}"
					   Margin="5" HorizontalAlignment="Left" VerticalAlignment="Center"/>
        </StackPanel>

        <!-- Copyright Information Header -->
        <TextBlock Grid.Row="4" Margin="5,20,5,0" FontWeight="SemiBold"
				   HorizontalAlignment="Left" VerticalAlignment="Center"
				   Text="{x:Static lang:Message.About_CopyrightInformation}"/>

        <!-- Copyright Information -->
        <TextBlock Grid.Row="5" Margin="5" Text="{x:Static lang:Message.SplashCopyright}"/>

        <!-- Licensing information -->
        <Grid 
            Grid.Row="6" 
            DataContext="{Binding Path={x:Static local:MainPageViewModel.StateProperty}}"
            Visibility="{Binding Path={x:Static license:LicenseState.IsLoadedProperty}, 
                    Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, 
                    ConverterParameter=equals:True}">

            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
            </Grid.RowDefinitions>

            <!-- Activation Information Header -->
            <TextBlock 
                Grid.Row="0" Margin="5,10,5,0" FontWeight="SemiBold"
                HorizontalAlignment="Left" VerticalAlignment="Center"
                Text="{x:Static commonlg:Message.BackstageHelp_Activation_StatusHeader}"/>

            <!-- Licence Information -->
            <StackPanel 
                Grid.Row="1" Orientation="Horizontal">

                <TextBlock 
                    Text="{x:Static commonlg:Message.BackstageHelp_Activation_StatusText}" Margin="5,3,5,3" 
                    HorizontalAlignment="Left" VerticalAlignment="Center"/>
                <TextBlock 
                    Text="{Binding Path={x:Static license:LicenseState.LicenceStatusProperty}}"
                    Margin="5,3,5,3" HorizontalAlignment="Left" VerticalAlignment="Center" />
            </StackPanel>

            <!--License details-->
            <StackPanel 
                Grid.Row="2" Orientation="Vertical" 
                Visibility="{Binding Path=IsFullLicense, 
				Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}},
                    ConverterParameter=equals:True}">

                <!-- Activation Days Remaining Information -->
                <StackPanel Orientation="Horizontal">
                    <TextBlock 
                        Text="{x:Static commonlg:Message.BackstageHelp_Activation_StatusActivationDaysText}" 
                        Margin="5,3,5,3" HorizontalAlignment="Left" VerticalAlignment="Center"/>
                    <TextBlock 
                        Text="{Binding Path={x:Static license:LicenseState.ActivationDaysRemainingProperty}}"
                        Margin="5,3,5,3" HorizontalAlignment="Left" VerticalAlignment="Center"/>
                </StackPanel>

                <!-- Activation Key Information -->
                <StackPanel Orientation="Horizontal">
                    <TextBlock 
                        Text="{x:Static commonlg:Message.BackstageHelp_Activation_ActivationGUIDText}" 
                        Margin="5,3,5,3" HorizontalAlignment="Left" VerticalAlignment="Center"/>
                    <TextBlock 
                        Text="{Binding Path={x:Static license:LicenseState.LicenceGUIDProperty}}"
                        Margin="5,3,5,3" HorizontalAlignment="Left" VerticalAlignment="Center"/>
                </StackPanel>
            </StackPanel>

            <!-- Demo Days Remaining Information -->
            <StackPanel 
                Grid.Row="3" Orientation="Horizontal" 
                Visibility="{Binding Path=IsInDemoMode, 
                        Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}},
						ConverterParameter=equals:True}">
                <TextBlock 
                    Text="{x:Static commonlg:Message.BackstageHelp_Activation_StatusDemoDaysText}" 
                    Margin="5,3,5,3" HorizontalAlignment="Left" VerticalAlignment="Center"/>
                <TextBlock 
                    Text="{Binding Path={x:Static license:LicenseState.DemoDaysRemainingProperty}}"
                    Margin="5,3,5,3" HorizontalAlignment="Left" VerticalAlignment="Center"/>
            </StackPanel>

            <!-- Relicense Area-->
            <StackPanel 
                Grid.Row="4" Orientation="Horizontal"
                Visibility="{Binding Path=MainViewModel.IsRelicenseVisible, 
                        ElementName=rootControl,
                        Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, 
                        ConverterParameter=equals:True}">

                <TextBlock x:Name="mainTextBox" Margin="5,3,5,3">

                    <Run Text="{x:Static commonlg:Message.SecurityDialog_Relicense_Part1}"/>
                    <Hyperlink 
                        Foreground="Red" 
						Command="{Binding Path=MainViewModel.OpenLicenseScreenCommand, ElementName=rootControl}"
                        CommandTarget="{Binding ElementName=mainTextBox}">
                        <Run Text="{x:Static commonlg:Message.SecurityDialog_Relicense_Part2}"/>
                    </Hyperlink>
                    <Run Text="{x:Static commonlg:Message.SecurityDialog_Relicense_Part3}"/>

                </TextBlock>
            </StackPanel>

        </Grid>

        <!-- CEIP Information Header -->
        <TextBlock Grid.Row="7" Margin="5,20,5,0" FontWeight="SemiBold"
           HorizontalAlignment="Left" VerticalAlignment="Center"
           Text="{x:Static lang:Message.About_CEIPInformation}"/>

        <!-- CEIP Information -->
        <StackPanel Grid.Row="8" Orientation="Horizontal">
            <TextBlock Text="{x:Static lang:Message.About_CEIPParticipationStatus}" Margin="5"
             HorizontalAlignment="Left" VerticalAlignment="Center"/>
            <TextBlock Text="{x:Static lang:Message.Generic_Participating}"
             Visibility="{Binding Path=IsCEIPParticipate, 
						Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, 
						ConverterParameter=equals:True}"
             Margin="5" HorizontalAlignment="Left" VerticalAlignment="Center"/>
            <TextBlock Text="{x:Static lang:Message.Generic_NotParticipating}"
             Visibility="{Binding Path=IsCEIPParticipate, 
						Converter={StaticResource {x:Static g:ResourceKeys.ConverterConditionToVisibility}}, 
						ConverterParameter=equals:False}"
             Margin="5" HorizontalAlignment="Left" VerticalAlignment="Center"/>
        </StackPanel>
    </Grid>

</UserControl>
