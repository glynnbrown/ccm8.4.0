﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Reflection;
using System.Windows.Media;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Resources
{
    /// <summary>
    /// Holds static references to all image sources required for the solution.
    /// </summary>
    /// <remarks>Need to lazy load all the main resources</remarks>
    [SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Justification = "NamingConvention ok here.")]
    public static class ImageResources
    {
        #region Helper Methods
        /// <summary>
        /// Helper method to create a frozen bitmap image source
        /// This prevents issue where static sources can memory leak
        /// </summary>
        /// <param name="imagePath"></param>
        /// <returns></returns>
        private static BitmapImage CreateAsFrozen(String imagePath)
        {
            //Unit test requirement as the pack header is not registered until after the WPF application is loaded.
            //This code should never execute when the main app is run.
            if (!UriParser.IsKnownScheme("pack"))
            {
                System.Windows.Application app = new System.Windows.Application();
            }

            BitmapImage src = new BitmapImage(new Uri(imagePath, UriKind.RelativeOrAbsolute));
            src.Freeze();
            return src;
        }
        #endregion

        #region Icon Constant image Paths
        // The following commented out code works just fine when executing the client directly, but doesn't work
        // for the NUnit tests, because it causes an implicit call to Assembly.GetEntryAssembly(), which isn't
        // this assembly for the NUnit tests.
        //private const String _iconFolderPath = "pack://application:,,,/Resources/Icons/{0}";
        private static readonly String _iconFolderPath =
            String.Format(CultureInfo.InvariantCulture,
            "pack://application:,,,/{0};component/Resources/Icons/{{0}}",
            Assembly.GetExecutingAssembly().GetName().Name);

        private static readonly ImageSource _ccmIcon =
            CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _iconFolderPath, "CCMMaintenance.ico"));


        #endregion

        #region Images Constant Image Paths
        // The following commented out code works just fine when executing the client directly, but doesn't work
        // for the NUnit tests, because it causes an implicit call to Assembly.GetEntryAssembly(), which isn't
        // this assembly for the NUnit tests.
        //private const String _imagesFolderPath = "pack://application:,,,/Resources/Images/{0}";
        private static readonly String _imagesFolderPath =
            String.Format(CultureInfo.InvariantCulture,
            "pack://application:,,,/{0};component/Resources/Images/{{0}}",
            Assembly.GetExecutingAssembly().GetName().Name);

        //Splash screen Images
        private static readonly ImageSource _ccmSplashScreen = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "CCM-Splashscreen.png"));

        //Dialog icons
        private static readonly ImageSource _error_24_Dialog = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Error-24-Dialog.png"));
        private static readonly ImageSource _question_24_Dialog = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Question-24-Dialog.png"));
        private static readonly ImageSource _warning_24_Dialog = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Warning-24-Dialog.png"));
        private static readonly ImageSource _information_24_Dialog = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Info-24-Dialog.png"));
        //private static readonly ImageSource _SideBar_Dialog = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "DialogSideBar.png"));
        //private static readonly ImageSource _TopBar_Dialog = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "DialogTopBar.png"));

        //Buttons
        private static readonly ImageSource _backupDatabase_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "BackupDatabase-32-Database.png"));
        private static readonly ImageSource _connectDatabase_16_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "ConnectDatabase-16-Database.png"));
        private static readonly ImageSource _copyDatabase_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "CopyDatabase-32-Database.png"));
        private static readonly ImageSource _database_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Database-32-Database.png"));
        private static readonly ImageSource _database_32_File = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Database-32-File.png"));
        private static readonly ImageSource _dataServer_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "DataServer-32-Database.png"));
        private static readonly ImageSource _deleteDatabase_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "DeleteDatabase-32-Database.png"));
        private static readonly ImageSource _disconnectDatabase_16_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "DisconnectDatabase-16-Database.png"));
        private static readonly ImageSource _folder_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Folder-16-Toolbar.png"));
        private static readonly ImageSource _localAvailable_24 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "LocalAvailable-24.png"));
        private static readonly ImageSource _localUnavailable_24 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "LocalUnavailable-24.png"));
        private static readonly ImageSource _redoChanges_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "RedoChanges-32-Database.png"));
        private static readonly ImageSource _refresh_16_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Refresh-16-Database.png"));
        private static readonly ImageSource _remove_16_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Remove-16-Database.png"));
        private static readonly ImageSource _repairDatabase_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "RepairDatabase-32-Database.png"));
        private static readonly ImageSource _restoreDatabase_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "RestoreDatabase-32-Database.png"));
        private static readonly ImageSource _sqlAvailable_24 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "SqlAvailable-24.png"));
        private static readonly ImageSource _sqlUnavailable_24 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "SqlUnavailable-24.png"));
        private static readonly ImageSource _uploadDatabase_32_Database = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "UploadDatabase-32-Database.png"));
        private static readonly ImageSource _OpenFile_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Open-16-Toolbar.png"));
        private static readonly ImageSource _World_32 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "World-32-Toolbar.png"));
        private static readonly ImageSource _Options_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Options-16-Toolbar.png"));
        private static readonly ImageSource _Options_32 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Options-32-Toolbar.png"));
        private static readonly ImageSource _Warning_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Warning-16-Toolbar.png"));
        private static readonly ImageSource _Exit_16 = CreateAsFrozen(String.Format(CultureInfo.InvariantCulture, _imagesFolderPath, "Exit-16-Toolbar.png"));

        #endregion

        #region Appwide
        //Splash screen
        public static ImageSource CCMSplashscreen { get { return _ccmSplashScreen; } }
        //App Icon
        public static ImageSource ApplicationIcon { get { return _ccmIcon; } }
        //App icon png image - note do NOT use this for form icons, use the correct SAMaintenance.ico file
        //public static ImageSource SAMaintenance_About_96 { get { return _saMaintenance_About_96; } }
        //public static ImageSource SAMaintenance_About_512 { get { return _saMaintenance_About_512; } }

        public static ImageSource Browse_16 { get { return _folder_16; } }
        public static ImageSource Open_16 { get { return _OpenFile_16; } }
        public static ImageSource LicenseWindowIcon { get { return _World_32; } }
        public static ImageSource Options_16 { get { return _Options_16; } }
        public static ImageSource Options_32 { get { return _Options_32; } }
        public static ImageSource Exit { get { return _Exit_16; } }

        //Dialogs
        public static ImageSource Dialog_Error_24 { get { return _error_24_Dialog; } }
        public static ImageSource Dialog_Question_24 { get { return _question_24_Dialog; } }
        public static ImageSource Dialog_Warning_24 { get { return _warning_24_Dialog; } }
        public static ImageSource Dialog_Information_24 { get { return _information_24_Dialog; } }
        //public static ImageSource Dialog_SideBar { get { return _SideBar_Dialog; } }
        //public static ImageSource Dialog_TopBar { get { return _TopBar_Dialog; } }

        //status bar
        public static ImageSource Warning { get { return _Warning_16; } }

        #endregion

        #region Main
        public static ImageSource Main_Connect { get { return _connectDatabase_16_Database; } }
        public static ImageSource Main_Disconnect { get { return _disconnectDatabase_16_Database; } }
        public static ImageSource Main_RefreshAll { get { return _refresh_16_Database; } }
        public static ImageSource Main_RemoveSelectedDatabase { get { return _remove_16_Database; } }
        public static ImageSource Main_BackupDatabase { get { return _backupDatabase_32_Database; } }
        public static ImageSource Main_UpgradeDatabase { get { return _uploadDatabase_32_Database; } }
        public static ImageSource Main_DatabaseMaintenance { get { return _repairDatabase_32_Database; } }
        public static ImageSource Main_RestoreDatabase { get { return _restoreDatabase_32_Database; } }
        public static ImageSource Main_MoveDatabase { get { return _redoChanges_32_Database; } }
        public static ImageSource Main_TransferDatabase { get { return _copyDatabase_32_Database; } }
        public static ImageSource Main_DeleteDatabase { get { return _deleteDatabase_32_Database; } }
        public static ImageSource Main_SqlAvailable { get { return _sqlAvailable_24; } }
        public static ImageSource Main_SqlUnavailable { get { return _sqlUnavailable_24; } }
        public static ImageSource Main_LocalAvailable { get { return _localAvailable_24; } }
        public static ImageSource Main_LocalUnavailable { get { return _localUnavailable_24; } }
        #endregion

        #region CreateDatabaseWindow
        public static ImageSource CreateDatabase_Local { get { return _database_32_File; } }
        public static ImageSource CreateDatabase_SQL { get { return _dataServer_32_Database; } }
        #endregion
    }
}
