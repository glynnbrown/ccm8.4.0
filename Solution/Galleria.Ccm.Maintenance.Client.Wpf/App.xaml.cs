﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25903 : N.Haywood
//  Copied from SA
// V8-27528 : M.Shelley
//  Added Desaware product licensing
#endregion
#region Version Histry: CCM802
// V8-29230 : N.Foster
//  Added Aspose licensing
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Management;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using System.Xml;
using Galleria.Ccm.Common.Wpf.Licensing;
using Galleria.Ccm.Maintenance.Client.Wpf.Common;
using Galleria.Ccm.Maintenance.Client.Wpf.PrimaryWindow;
using Galleria.Ccm.Maintenance.Client.Wpf.Resources.Language;
using Galleria.Ccm.Maintenance.Client.Wpf.Startup;
using Gibraltar.Agent;

namespace Galleria.Ccm.Maintenance.Client.Wpf
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        #region Constants

        private const string _sessionIdName = "Session ID";
        private const Int32 _secondsToWaitForProcessShutdown = 5;

        #endregion

        #region Fields

        private Boolean _unitTesting; // indicates if we are loading the application for unit testing purposes
        private StartupSplash _splashScreen;
        private CEIPViewModel _CEIPViewModel; //The customer experience improvment program view model
        private CEIPWindow _CEIPWindow; //The customer experience improvment program window if required
        private Boolean _showDialog; //Has the demo dialog already been displayed
        private static LicenseState _licenseState;

        #endregion

        #region Properties

        /// <summary>
        /// The Application LicenseState
        /// </summary>
        public static LicenseState LicenseState
        {
            get
            {
                if (_licenseState == null)
                {
                    _licenseState = new LicenseState();
                }
                return _licenseState;
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public App()
            : this(false)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="unitTesting">Indicates if we are loading this app for unit testing purposes</param>
        public App(bool unitTesting)
        {
            _unitTesting = unitTesting;
            this.Startup += new StartupEventHandler(App_Startup);
            this.Exit += new ExitEventHandler(App_Exit);
        }
        #endregion

        #region Event Handlers

        /// <summary>
        /// The main Application Startup code
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void App_Startup(object sender, StartupEventArgs e)
        {
            // unsubscribe from the startup event
            this.Startup -= App_Startup;

            //Override the current culture info
            // This determines the numberformats/currency etc that will be used by the app.
            // The following line ensures that it will be picked up from the pc ControlPanel-> Region and Language -> Format setting.
            FrameworkElement.LanguageProperty.OverrideMetadata(
            typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            //if we are not unit testing then pull back the display language
            if (!_unitTesting)
            {
                try
                {
                    // set aspose cells license
                    Aspose.Cells.License cellsLicense = new Aspose.Cells.License();
                    cellsLicense.SetLicense("Aspose.Total.10.lic");
                }
                catch (XmlException)
                {
                    // License doesn't exist for developer, GABS will swap in on release build
                }

                //stop this shutting down as each dialog closes
                this.ShutdownMode = ShutdownMode.OnExplicitShutdown;

                //CEIP gilbraltar integration
                _CEIPViewModel = new CEIPViewModel();

                if (_CEIPViewModel.CEIPWindowRequired())
                {
                    ShowCEIPWindow();
                }

                // Setup / verify licensing
                var licenceObject = new LicenceSetup(false);

                // If the application is shutting down, exit the startup
                if (licenceObject.ApplicationShutdown)
                {
                    return;
                }
                _licenseState = licenceObject.LicenseState;

                // Check if the license setup has set the Application LicenseState property
                if (this.Properties["LicenseState"] == null)
                {
                    // There is no current App LicenseState property, so set a reference in the App properties for use by the Help page
                    this.Properties["LicenseState"] = _licenseState;
                }

                //Get the culture to use for the UI
                SettingsList userSettings = SettingsList.Instance;
                userSettings.LoadSettingsList();

                //set the display language
                //This is the language that the application will be displayed in.
                String cultureCode = userSettings.DisplayLanguage;
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(cultureCode);

                //Check that an instance is not already running
                try
                {
                    Process thisProc = Process.GetCurrentProcess();
                    String thisProcOwner = GetProcessOwner(thisProc.Id);
                    for (int i = 0; i <= _secondsToWaitForProcessShutdown; i++)
                    {
                        //get all processes with the same name
                        Process[] matchingProcesses = Process.GetProcessesByName(thisProc.ProcessName);

                        //if there is more than one check if any has the same owner
                        if (matchingProcesses.Length > 1)
                        {
                            if (matchingProcesses.Where(p => GetProcessOwner(p.Id) == thisProcOwner).Count() > 1)
                            {
                                if (i < _secondsToWaitForProcessShutdown)
                                {
                                    Thread.Sleep(1000);
                                }
                                else
                                {
                                    MessageBox.Show(Message.Application_AlreadyRunning);
                                    App.Current.Shutdown();
                                    return;
                                }
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                catch (Exception)
                {
                    //The os wmi service has failed to start.
                    // This is a windows problem. Not ours.
                }
            }

            // initialize and start the logging session
            Log.Initializing += new Log.InitializingEventHandler(Log_Initializing);
            Log.StartSession();

            //attach to the event to globally hand exceptions before they hit the os
            this.DispatcherUnhandledException += new System.Windows.Threading.DispatcherUnhandledExceptionEventHandler(App_DispatcherUnhandledException);

            //show the loading window
            if (!_unitTesting)
            {
                LoadApplication();

                //if (!App.LicenseState.IsLoaded)
                //{
                    //App.LicenseState.Refresh(_securityLicense);
                //}
            }
        }

        /// <summary>
        /// Called when the application is closing down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void App_Exit(object sender, ExitEventArgs e)
        {
            //unsubscribe from the exit event
            this.Exit -= App_Exit;

            //End the logging session
            Log.EndSession();
        }

        /// <summary>
        /// Forces to close the application when ShutdownMode is set to ExplicitShutdown
        /// </summary>
        private void ApplicationForceExit()
        {
            this.Startup -= App_Startup;
            this.Exit -= App_Exit;

            System.Environment.Exit(0);
        }

        /// <summary>
        /// performs tasks once the main window has finished loading,
        /// such as closing the splashscreen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainWindow_Activated(object sender, EventArgs e)
        {
            MainPageOrganiser mainWindow = (MainPageOrganiser)sender;
            mainWindow.Activated -= mainWindow_Activated;
            App.Current.MainWindow = mainWindow;
            //Close down the splashscreen
            if (_splashScreen != null)
            {
                _splashScreen.Close();
            }
        }
        #endregion

        #region CEIP Checking
        /// <summary>
        /// Show the Customer Experiance Improvment program window
        /// </summary>
        private void ShowCEIPWindow()
        {
            _CEIPWindow = new CEIPWindow(_CEIPViewModel);
            _CEIPWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _CEIPWindow.ShowDialog();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Go forth and load the splashscreen and the main application window
        /// </summary>
        private void LoadApplication()
        {
            //Show the splash screen and then main page
            _splashScreen = new StartupSplash();
            _splashScreen.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            _splashScreen.Show();
            _splashScreen.StatusText = "Loading..";

            MainPageOrganiser mainWindow = new MainPageOrganiser();
            mainWindow.Activated += new EventHandler(mainWindow_Activated);
            mainWindow.Show();

            //set the shutdown to when the main window closes.
            this.ShutdownMode = System.Windows.ShutdownMode.OnLastWindowClose;
        }

        /// <summary>
        /// Opens the given window as a child of the primary.
        /// </summary>
        /// <param name="window"></param>
        /// <param name="isModal"></param>
        public static void ShowWindow(Window window, bool isModal)
        {
            ((MainPageOrganiser)App.Current.MainWindow).OpenChildWindow(window, isModal);
        }

        /// <summary>
        /// Returns the ownername of the given process
        /// </summary>
        /// <param name="processId"></param>
        /// <returns></returns>
        public String GetProcessOwner(Int32 processId)
        {
            String query = "Select * From Win32_Process Where ProcessID = " + processId;
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection processList = searcher.Get();

            foreach (ManagementObject obj in processList)
            {
                String[] argList = new String[] { String.Empty, String.Empty };
                Int32 returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
                if (returnVal == 0)
                {
                    // return DOMAIN\user
                    return argList[1] + "\\" + argList[0];
                }
            }

            return "NO OWNER";
        }

        #endregion

        #region Gibraltar Logging
        /// <summary>
        /// Called when the log is initialized
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void Log_Initializing(object sender, LogInitializingEventArgs e)
        {
            // configure the application to log to the service
            e.Configuration.Server.UseGibraltarService = true;
            e.Configuration.Server.CustomerName = "galleria";
            e.Configuration.Server.SendAllApplications = true;
            e.Configuration.Server.PurgeSentSessions = true;
            e.Configuration.Server.UseSsl = true;
            e.Configuration.Properties[_sessionIdName] = Guid.NewGuid().ToString();
            e.Configuration.SessionFile.EnableFilePruning = true; // ISO-13436
            e.Configuration.SessionFile.MaxLocalDiskUsage = 200; // ISO-13436
            e.Configuration.Publisher.EnableAnonymousMode = false; // GFS-14882
#if !DEBUG
            // now determine if we are running within the IDE.
            // normally, you shouldn't change the behaviour of
            // an application depending on whether a debugger
            // is attached or not, however, we want to be able
            // to still use the logging facilities in debug
            // builds when they are distrubuted to people such
            // as qa and consultancy. Therefore we have used
            // this method instead of conditional compilation
            if (!Debugger.IsAttached) // GFS-14882
            {
                // set auto-sending of debug information to what the user has selected
                e.Configuration.Server.AutoSendSessions = _CEIPViewModel.IsParticipate();
            }
#endif
        }
        #endregion Gibraltar Logging

        #region Exception handling
        /// <summary>
        /// Handler to catch global exceptions.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            //show the exception has been handled
            e.Handled = true;

            //Log the exception
            Log.ReportException(e.Exception, "Unhandled Exception", false, true);

            //Close 
            Application.Current.Shutdown();
        }
        #endregion
    }
}
