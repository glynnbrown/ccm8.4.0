﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Framework.ViewModel;
//using System.ComponentModel;
//using System.Windows;
//using Galleria.Framework.Helpers;

//namespace Galleria.Ccm.Maintenance.Client.Wpf.Startup
//{
//    public class SecurityViewModel : ViewModelAttachedControlObject<SecurityWindow>, IDataErrorInfo
//    {
//        #region Fields

//        private DomainLicense _currentSecurity; //The current security settings

//        private String _licenseMsg;

//        #endregion

//        #region Binding PropertyPaths

//        public static PropertyPath securityCompanyEmailProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.SecurityCompanyEmail);
//        public static PropertyPath SecurityInputKeyProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.SecurityInputKey);
//        public static PropertyPath SecurityUserNameProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.SecurityUserName);
//        public static PropertyPath SecurityCompanyNameProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.SecurityCompanyName);
//        public static PropertyPath SecurityCompanyAddressProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.SecurityCompanyAddress);
//        public static PropertyPath SecurityCompanyPhoneProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.SecurityCompanyPhone);
//        public static PropertyPath SecurityInstallTypeProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.SecurityInstallType);
//        public static PropertyPath LicenseMsgProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.LicenseMsg);
//        public static PropertyPath IsValidProperty = WpfHelper.GetPropertyPath<SecurityViewModel>(p => p.IsValid);

//        #endregion

//        #region Properties

//        /// <summary>
//        /// The input key entered
//        /// </summary>
//        public String SecurityInputKey
//        {
//            get { return _currentSecurity.inputKey; }
//            set
//            {
//                _currentSecurity.inputKey = (value != String.Empty) ? value : null;
//            }
//        }

//        /// <summary>
//        /// The name of the person registering the license
//        /// </summary>
//        public String SecurityUserName
//        {
//            get { return _currentSecurity.userName; }
//            set
//            {
//                _currentSecurity.userName = (value != String.Empty) ? value : null;
//                SaveCommand.RaiseCanExecuteChanged();
//            }
//        }

//        /// <summary>
//        /// The name of the company registering the license
//        /// </summary>
//        public String SecurityCompanyName
//        {
//            get { return _currentSecurity.companyName; }
//            set
//            {
//                _currentSecurity.companyName = (value != String.Empty) ? value : null;
//                SaveCommand.RaiseCanExecuteChanged();
//            }
//        }

//        /// <summary>
//        /// The address of the company registering the license
//        /// </summary>
//        public String SecurityCompanyAddress
//        {
//            get { return _currentSecurity.companyAddress; }
//            set
//            {
//                _currentSecurity.companyAddress = (value != String.Empty) ? value : null;
//                SaveCommand.RaiseCanExecuteChanged();
//            }
//        }

//        /// <summary>
//        /// The phone number of the company registering the license
//        /// </summary>
//        public String SecurityCompanyPhone
//        {
//            get { return _currentSecurity.companyPhone; }
//            set
//            {
//                _currentSecurity.companyPhone = (value != String.Empty) ? value : null;
//                SaveCommand.RaiseCanExecuteChanged();
//            }
//        }

//        /// <summary>
//        /// The email address of the company registering the license
//        /// </summary>
//        public String SecurityCompanyEmail
//        {
//            get { return _currentSecurity.companyEmail; }
//            set
//            {
//                _currentSecurity.companyEmail = (value != String.Empty) ? value : null;
//                OnPropertyChanged(securityCompanyEmailProperty);
//                SaveCommand.RaiseCanExecuteChanged();
//            }
//        }

//        /// <summary>
//        /// The Install Type
//        /// </summary>
//        public InstallType SecurityInstallType
//        {
//            get { return _currentSecurity.securityInstallType; }
//            set { _currentSecurity.securityInstallType = value; }
//        }

//        /// <summary>
//        /// The license message. The status of license and demo days
//        /// </summary>
//        public String LicenseMsg
//        {
//            get { return _licenseMsg; }
//            set
//            {
//                _licenseMsg = (value != String.Empty) ? value : null;
//            }
//        }

//        /// <summary>
//        /// Returns if the object is valid
//        /// </summary>
//        public bool IsValid
//        {
//            get
//            {
//                return ValidateProperties();
//            }
//        }

//        #endregion

//        #region Constructor

//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        public SecurityViewModel(DomainLicense License)
//            : this(false)
//        {
//            _currentSecurity = License;
//        }

//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        /// <param name="unitTesting">Indicates if we are loading this app for unit testing purposes</param>
//        public SecurityViewModel(bool unitTesting)
//        {
//            if (!unitTesting)
//            {

//            }
//        }

//        #endregion

//        #region Methods

//        /// <summary>
//        /// Force a cycle through of the data error info interface
//        /// </summary>
//        /// <returns>Is the object valid or not</returns>
//        private bool ValidateProperties()
//        {
//            bool isValid = true;

//            IDataErrorInfo sourceItem = this as IDataErrorInfo;
//            String result = null;

//            //Check name
//            result = sourceItem[securityCompanyEmailProperty.Path];
//            if (result != null)
//            {
//                isValid = false;
//            }

//            return isValid;
//        }

//        /// <summary>
//        /// Validates if security window required
//        /// </summary>
//        /// <returns>True if security window required, else false</returns>
//        public Boolean SecurityWindowRequired()
//        {
//            return _currentSecurity.IsFullLicense();
//        }

//        /// <summary>
//        /// Load all the information to be displayed on the registration form
//        /// </summary>
//        public void LoadRegistrationDetails()
//        {
//            _currentSecurity.LicenseLoadCustomData();

//            SecurityUserName = _currentSecurity.userName;
//            SecurityCompanyName = _currentSecurity.companyName;
//            SecurityCompanyAddress = _currentSecurity.companyAddress;
//            SecurityCompanyPhone = _currentSecurity.companyPhone;
//            SecurityCompanyEmail = _currentSecurity.companyEmail;

//        }

//        /// <summary>
//        /// Check that the Email Address is valid or Not?
//        /// </summary>
//        /// <param name="emailAddress">Validate the email address string inputted by the user</param>
//        /// <returns>True/False Dependant on the email address</returns>
//        private bool IsEmailValid(String emailAddress)
//        {
//            bool isValid = true;

//            if (emailAddress != null)
//            {
//                // Return true if strIn is in valid e-mail format.
//                isValid = Regex.IsMatch(emailAddress,
//                           @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
//                           @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
//            }

//            return isValid;
//        }

//        #endregion

//        #region SaveCommand

//        private RelayCommand _saveCommand;

//        /// <summary>
//        /// Saves the current license registration details
//        /// </summary>
//        public RelayCommand SaveCommand
//        {
//            get
//            {
//                if (_saveCommand == null)
//                {
//                    _saveCommand = new RelayCommand(
//                    p => SaveCommand_Executed(), p => SaveCommand_CanExecute(), false)
//                    {
//                        FriendlyName = Message.SecurityWindow_InstallButton,
//                        Icon = ImageResources.Open_16
//                    };
//                    this.ViewModelCommands.Add(_saveCommand);
//                }
//                return _saveCommand;
//            }
//        }

//        private bool SaveCommand_CanExecute()
//        {
//            Boolean returnVal = true;

//            returnVal = (((_currentSecurity.userName != null) && (_currentSecurity.userName.Length > 0))) &&
//                        (((_currentSecurity.companyName != null) && (_currentSecurity.companyName.Length > 0))) &&
//                        (((_currentSecurity.companyAddress != null) && (_currentSecurity.companyAddress.Length > 0))) &&
//                        (((_currentSecurity.companyPhone != null) && (_currentSecurity.companyPhone.Length > 0))) &&
//                        (((_currentSecurity.companyEmail != null) && (_currentSecurity.companyEmail.Length > 0)));

//            return returnVal;
//        }

//        private void SaveCommand_Executed()
//        {

//            //Get the install key from the form
//            String installCode = this.AttachedControl.searialKey.InstallCode;

//            if ((installCode != null) && (installCode.Length > 0))
//            {
//                SecurityInstallType = InstallType.Full;
//                _currentSecurity.inputKey = installCode;
//            }
//            else
//            {
//                SecurityInstallType = InstallType.Demo;
//            }

//            //Set the return result of the dialog
//            this.AttachedControl.DialogResult = true;
//        }

//        #endregion

//        #region Events

//        #endregion

//        #region IDataErrorInfo

//        String IDataErrorInfo.Error
//        {
//            get { throw new NotImplementedException(); }
//        }

//        /// <summary>
//        /// Implementation of the IDataErrorInfo interface
//        /// </summary>
//        /// <param name="columnName">Property name</param>
//        /// <returns>String containing error</returns>
//        String IDataErrorInfo.this[String columnName]
//        {
//            get
//            {
//                String result = null;

//                if (columnName == securityCompanyEmailProperty.Path)
//                {
//                    if (!IsEmailValid(SecurityCompanyEmail))
//                        result = String.Format(CultureInfo.CurrentCulture, Message.SecurityWindow_EmailValidation);
//                }

//                return result; // return result
//            }
//        }
//        #endregion

//        #region IDisposable

//        /// <summary>
//        /// Clears up on dispose.
//        /// </summary>
//        /// <param name="disposing"></param>
//        protected override void Dispose(bool disposing)
//        {
//            if (!this.IsDisposed)
//            {
//                if (disposing)
//                {
//                    this.AttachedControl = null;
//                }
//                this.IsDisposed = true;
//            }
//        }

//        #endregion
//    }
//}
