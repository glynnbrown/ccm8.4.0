﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Galleria.Framework.Controls.Wpf;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Startup
{
    /// <summary>
    /// Interaction logic for CEIP.xaml
    /// </summary>
    public partial class CEIPWindow : ExtendedRibbonWindow
    {
        #region ViewModel Property

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof(CEIPViewModel), typeof(CEIPWindow),
            new PropertyMetadata(null, OnViewModelPropertyChanged));

        /// <summary>
        /// Gets/Sets the attached viewmodel
        /// </summary>
        public CEIPViewModel ViewModel
        {
            get { return (CEIPViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        static void OnViewModelPropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            CEIPWindow senderControl = (CEIPWindow)obj;

            if (e.OldValue != null)
            {
                CEIPViewModel oldModel = (CEIPViewModel)e.OldValue;
                oldModel.AttachedControl = null;
            }

            if (e.NewValue != null)
            {
                CEIPViewModel newModel = (CEIPViewModel)e.NewValue;
                newModel.AttachedControl = senderControl;
            }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public CEIPWindow(CEIPViewModel viewModel)
        {
            InitializeComponent();

            //Set the viewModel
            this.ViewModel = viewModel;
        }

        protected override void OnClosed(System.EventArgs e)
        {
            base.OnClosed(e);

            IDisposable disposableViewModel = this.ViewModel as IDisposable;
            this.ViewModel = null;

            if (disposableViewModel != null)
            {
                disposableViewModel.Dispose();
            }
        }

        #endregion

        #region Methods

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handles the retry click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void cmdOk_Click(object sender, RoutedEventArgs e)
        {
            this.ViewModel.SaveUserSettings();
            this.DialogResult = true;
        }

        #endregion
    }
}
