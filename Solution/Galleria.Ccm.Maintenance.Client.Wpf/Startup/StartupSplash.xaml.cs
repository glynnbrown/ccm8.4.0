﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
// V8-26410 : J.Pickup
//      The product date is now rendered in code as opposed to being provided on the background graphic
#endregion
#region Version History: (CCM 8.1.0)
// V8-29953 : M.Pettit
//      Updated product date
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;

namespace Galleria.Ccm.Maintenance.Client.Wpf.Startup
{
    /// <summary>
    /// Interaction logic for StartupSplash.xaml
    /// </summary>
    public partial class StartupSplash : Window
    {
        #region Fields

        private const String _productDate = "2016";

        #endregion

        #region Properties

        #region Status Text Property
        public static DependencyProperty StatusTextProperty =
            DependencyProperty.Register("StatusText", typeof(String), typeof(StartupSplash));

        public String StatusText
        {
            get { return (String)GetValue(StatusTextProperty); }
            set { SetValue(StatusTextProperty, value); }
        }
        #endregion

        #region Build Version Property
        public static readonly DependencyProperty BuildVersionProperty =
            DependencyProperty.Register("BuildVersion", typeof(String), typeof(StartupSplash));

        public String BuildVersion
        {
            get { return (String)GetValue(BuildVersionProperty); }
            private set { SetValue(BuildVersionProperty, value); }
        }
        #endregion

        #region Product Date Property

        public String ProductDate
        {
            get { return _productDate; }
        }

        #endregion

        #endregion

        #region Constructor
        public StartupSplash()
        {
            InitializeComponent();

            // stop this window showing in the taskbar
            this.ShowInTaskbar = false;

            //Set Build Version
            this.BuildVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();

            // start the loading process
            this.Loaded += new RoutedEventHandler(StartupSplash_Loaded);
        }

        private void StartupSplash_Loaded(object sender, RoutedEventArgs e)
        {
            this.StatusText = "Initialising...";
            //Check Licensing Setup
            //TODO Licensing

            //Check Gibralter Acceptance
            //TODO Gibraltar

            //Load the main form if all is ok
            //LoadApplication();
        }
        #endregion

        #region Methods

        //private void LoadApplication()
        //{
        //    MainPageOrganiser mainWindow = new MainPageOrganiser();
        //    mainWindow.Activated += new EventHandler(mainWindow_Activated);
        //    mainWindow.Show();
        //}

        //private void mainWindow_Activated(object sender, EventArgs e)
        //{
        //    MainPageOrganiser mainWindow = (MainPageOrganiser)sender;
        //    mainWindow.Activated -= mainWindow_Activated;
        //    App.Current.MainWindow = mainWindow;
        //    //Close down the splashscreen
        //    this.Close();
        //}
        #endregion
    }
}
