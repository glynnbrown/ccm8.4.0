﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Data;
//using System.Windows.Documents;
//using System.Windows.Input;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
//using System.Windows.Shapes;

//namespace Galleria.Ccm.Maintenance.Client.Wpf.Startup
//{
//    /// <summary>
//    /// Interaction logic for SecurityDialog.xaml
//    /// </summary>
//    public partial class SecurityDialog : ExtendedRibbonWindow
//    {

//        #region ViewModel Property

//        public static readonly DependencyProperty ViewModelProperty =
//            DependencyProperty.Register("ViewModel", typeof(SecurityDialogViewModel), typeof(SecurityDialog),
//            new PropertyMetadata(null, ViewModel_PropertyChanged));

//        private static void ViewModel_PropertyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
//        {
//            SecurityDialog senderControl = (SecurityDialog)obj;

//            if (e.OldValue != null)
//            {
//                SecurityDialogViewModel oldModel = (SecurityDialogViewModel)e.OldValue;
//                oldModel.CloseWindow -= senderControl.ViewModel_CloseWindow;
//            }

//            if (e.NewValue != null)
//            {
//                SecurityDialogViewModel newModel = (SecurityDialogViewModel)e.NewValue;
//                newModel.CloseWindow += senderControl.ViewModel_CloseWindow;
//            }
//        }

//        /// <summary>
//        /// Gets/Sets the attached viewmodel
//        /// </summary>
//        public SecurityDialogViewModel ViewModel
//        {
//            get { return (SecurityDialogViewModel)GetValue(ViewModelProperty); }
//            set { SetValue(ViewModelProperty, value); }
//        }

//        #endregion

//        #region Constructor

//        /// <summary>
//        /// Constructor
//        /// </summary>
//        public SecurityDialog(SecurityDialogViewModel viewModel)
//        {
//            InitializeComponent();

//            //Set the viewModel
//            this.ViewModel = viewModel;

//            this.Loaded += new RoutedEventHandler(SecurityDialog_Loaded);
//        }

//        protected override void OnClosed(System.EventArgs e)
//        {
//            base.OnClosed(e);

//            IDisposable disposableViewModel = this.ViewModel as IDisposable;
//            this.ViewModel = null;

//            if (disposableViewModel != null)
//            {
//                disposableViewModel.Dispose();
//            }
//        }

//        #endregion

//        #region Methods

//        #endregion

//        #region Event Handlers

//        /// <summary>
//        /// Handles the load event of the window
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        private void SecurityDialog_Loaded(object sender, RoutedEventArgs e)
//        {
//            this.ViewModel.LoadDialog();
//        }

//        /// <summary>
//        /// Subscribe to method to close the window
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        void ViewModel_CloseWindow(object sender, EventArgs e)
//        {
//            this.DialogResult = true;

//            //Close the window
//            this.Close();

//            this.ViewModel = null;
//        }

//        /// <summary>
//        /// Handles the ok\continue click
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        void cmdContinue_Click(object sender, RoutedEventArgs e)
//        {
//            this.DialogResult = true;
//        }

//        /// <summary>
//        /// Handles the ok\return click
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        void cmdReturn_Click(object sender, RoutedEventArgs e)
//        {
//            this.ViewModel.IsReturnButtonPressed = true;
//            this.DialogResult = false;
//        }

//        /// <summary>
//        /// Handles the retry click
//        /// </summary>
//        /// <param name="sender"></param>
//        /// <param name="e"></param>
//        void cmdRetry_Click(object sender, RoutedEventArgs e)
//        {
//            this.ViewModel.LoadDialog();
//        }

//        #endregion
//    }
//}
