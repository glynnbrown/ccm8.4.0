﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (%VERSION%)
// %GEMINI% : %DEVELOPER%
//		Created (Auto-generated)
#endregion
#endregion

using System;

using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;

using %NAMESPACE%.Dal.Interfaces;

namespace %NAMESPACE%.Dal.DataTransferObjects
{
    /// <summary>
    /// %OBJECT% Data Transfer Object
    /// </summary>
    [Serializable]
    public class %OBJECT%Dto
    {
        #region Properties
        public Object Id { get; set; }%DTOKEY%%PROPERTIES%
        #endregion

        #region Methods
        /// <summary>
        /// Returns a hash code for this object
        /// </summary>
        /// <returns>The object hash code</returns>
        public override Int32 GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Check to see if two dtos are the same
        /// </summary>
        /// <param name="obj">The object to compare against</param>
        /// <returns>true if objects are equal</returns>
        public override Boolean Equals(Object obj)
        {
            %OBJECT%Dto other = obj as %OBJECT%Dto;
            if (other != null)
            {
                if (other.Id != this.Id) { return false; }%EQUALS%
            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion 
    }
	%DTOKEYCLASS%
	%DTOISSETCLASS%
}
