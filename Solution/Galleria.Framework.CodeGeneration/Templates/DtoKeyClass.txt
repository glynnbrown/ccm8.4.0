﻿
public class %OBJECT%DtoKey {
	
    #region Properties
    %KEYPROPERTIES%
    #endregion

    #region Methods
    /// <summary>
    /// Returns a hash code for this object
    /// </summary>
    /// <returns>The object hash code</returns>
    public override Int32 GetHashCode()
    {
        return%KEYHASH%;
    }

    /// <summary>
    /// Check to see if two keys are the ISOme
    /// </summary>
    /// <param name="obj">The object to compare against</param>
    /// <returns>true if objects are equal</returns>
    public override Boolean Equals(Object obj)
    {
        %OBJECT%DtoKey other = obj as %OBJECT%DtoKey;
        if (other != null)
        {
            %KEYEQUALS%
        }
        else
        {
            return false;
        }
        return true;
    }
    #endregion 
}