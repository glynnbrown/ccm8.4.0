﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-24290 : K.Pickup
//      Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Galleria.Framework.CodeGeneration
{
    public class CodeGenerator
    {
        #region Nested Classes

        private class ObjectProperty
        {
            public String CodeName { get; set; }

            public String CodeType { get; set; }

            public String BaseCodeType { get; set; }

            public String DbName { get; set; }

            public String MssqlDbType { get; set; }

            public String MssqlAdoType { get; set; }

            public String Nullability { get; set; }

            public Boolean IsKey { get; set; }
        }

        #endregion

        #region Constants

        private const String _templatePath = "Templates";
        private const String _dtoTemplate = "Dto.txt";
        private const String _dtoKeyTemplate = "DtoKey.txt";
        private const String _dtoKeyClassTemplate = "DtoKeyClass.txt";
        private const String _dtoIsSetClassTemplate = "DtoIsSetClass.txt";
        private const String _iDalTemplate = "IDal.txt";
        private const String _modelRootObjectTemplate = "ModelRootObject.txt";
        private const String _modelRootObjectServerTemplate = "ModelRootObjectServer.txt";
        private const String _modelChildObjectTemplate = "ModelChildObject.txt";
        private const String _modelChildObjectServerTemplate = "ModelChildObjectServer.txt";

        #endregion

        public static void Generate(String[] args)
        {
            if (args.Length != 7)
            {
                Console.WriteLine("Please call this app using the following arguments:");
                Console.WriteLine("ALL {object def path} {namespace} {object name} {ISO version} {gemini ticket #} {developer}");
            }

            String objectDefinitionPath = args[1];
            String objectNamespace = args[2];
            String inputObjectName = args[3];
            Dictionary<String, String> objectDefinitions = new Dictionary<String, String>();
            if (!String.IsNullOrEmpty(inputObjectName))
            {
                objectDefinitions[inputObjectName] = Path.Combine(objectDefinitionPath, inputObjectName + ".txt");
            }
            else
            {
                foreach (String path in Directory.GetFiles(objectDefinitionPath))
                {
                    String file = Path.GetFileName(path);
                    objectDefinitions[file.Substring(0, file.Length - 4)] = path;
                }
            }
            String version = args[4];
            String gemini = args[5];
            String developer = args[6];
            String output = null;

            String outputTypes = args[0];
            if (outputTypes == "ALL")
            {
                outputTypes = "DTO|IDAL|MSSQLSCHEMA|MSSQLTABLE|MSSQLDAL|MSSQLSTOREDPROCS|MSSQLKEYS|MODELROOTOBJECT|MODELROOTOBJECTSERVER|MODELCHILDOBJECT|MODELCHILDOBJECTSERVER";
            }
            Char[] delim = { '|' };
            String[] tokens = outputTypes.ToUpperInvariant().Split(delim, StringSplitOptions.RemoveEmptyEntries);

            foreach (String outputType in tokens)
            {
                File.WriteAllText(String.Format("{0}Output.txt", outputType), "");
            }

            foreach (String objectName in objectDefinitions.Keys)
            {
                String objectDefinition = objectDefinitions[objectName];

                List<ObjectProperty> properties = new List<ObjectProperty>(
                    GetObjectProperties(objectName, objectDefinition));
                List<String> storedProcedures = new List<String>(
                    GetStoredProcedures(objectDefinition));
                String schema = GetObjectSchema(objectDefinition);

                foreach (String outputType in tokens)
                {
                    String fetchByCriteriaMethods = "";
                    String deleteByCriteriaMethods = "";
                    switch (outputType)
                    {
                        #region DTO
                        case "DTO":
                            output = File.ReadAllText(Path.Combine(_templatePath, _dtoTemplate));
                            output = output.Replace("%PROPERTIES%", GetDtoProperties(schema, properties, false));
                            output = output.Replace("%EQUALS%", GetDtoEqualsOverrides(schema, properties, false));
                            if (properties.Count(p => p.IsKey) > 0)
                            {
                                output = output.Replace("%DTOKEY%", File.ReadAllText(Path.Combine(_templatePath, _dtoKeyTemplate)));
                                output = output.Replace("%DTOKEYCLASS%", File.ReadAllText(Path.Combine(_templatePath, _dtoKeyClassTemplate)));
                                output = output.Replace("%KEYINIT%", GetDtoKeyInit(properties));
                                output = output.Replace("%KEYPROPERTIES%", GetDtoProperties(schema, properties, true));
                                output = output.Replace("%KEYHASH%", GetDtoKeyHash(properties));
                                output = output.Replace("%KEYEQUALS%", GetDtoEqualsOverrides(schema, properties, true));
                            }
                            else
                            {
                                output = output.Replace("%DTOKEY%", "");
                                output = output.Replace("%DTOKEYCLASS%", "");
                            }
                            if (schema == "MasterData")
                            {
                                output = output.Replace("%DTOISSETCLASS%", File.ReadAllText(Path.Combine(_templatePath, _dtoIsSetClassTemplate)));
                                output = output.Replace("%ISSETPROPERTIES%", GetIsSetProperties(properties));
                                output = output.Replace("%ISSETHASH%", GetIsSetHash(properties));
                                output = output.Replace("%ISSETEQUALS%", GetIsSetEqualsOverrides(properties));
                                output = output.Replace("%ISSETSET%", GetIsSetSet(properties));
                            }
                            else
                            {
                                output = output.Replace("%DTOISSETCLASS%", "");
                            }
                            break;
                        #endregion

                        #region IDAL
                        case "IDAL":
                            output = File.ReadAllText(Path.Combine(_templatePath, _iDalTemplate));
                            output = output.Replace("%METHODS%", GetIDalMethods(objectName, storedProcedures, properties));
                            break;
                        #endregion

                        //#region MODELROOTOBJECT
                        //case "MODELROOTOBJECT":
                        //    output = "";
                        //    output = File.ReadAllText(Path.Combine(_templatePath, _modelRootObjectTemplate));
                        //    output = output.Replace("%MODELPROPERTIES%", GetModelProperties(properties));
                        //    output = output.Replace("%MODELBUSINESSRULES%", GetModelBusinessRules(properties));
                        //    break;
                        //#endregion

                        //#region MODELROOTOBJECTSERVER
                        //case "MODELROOTOBJECTSERVER":
                        //    output = "";
                        //    output = File.ReadAllText(Path.Combine(_templatePath, _modelRootObjectServerTemplate));
                        //    output = output.Replace("%MODELGETDTOPROPERTIES%", GetModelGetDtoProperties(properties));
                        //    output = output.Replace("%MODELLOADDTOPROPERTIES%", GetModelLoadDtoProperties(properties));
                        //    break;
                        //#endregion

                        #region MODELCHILDOBJECT
                        case "MODELCHILDOBJECT":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _modelChildObjectTemplate));
                            output = output.Replace("%MODELPROPERTIES%", GetModelProperties(properties));
                            output = output.Replace("%MODELBUSINESSRULES%", GetModelBusinessRules(properties));
                            break;
                        #endregion

                        #region MODELCHILDOBJECTSERVER
                        case "MODELCHILDOBJECTSERVER":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _modelChildObjectServerTemplate));
                            output = output.Replace("%MODELGETDTOPROPERTIES%", GetModelGetDtoProperties(properties));
                            output = output.Replace("%MODELLOADDTOPROPERTIES%", GetModelLoadDtoProperties(properties));
                            break;
                        #endregion

                        default:
                            break;
                    }

                    output = output.Replace("%SCHEMA%", schema);
                    output = output.Replace("%VERSION%", version);
                    output = output.Replace("%GEMINI%", gemini);
                    output = output.Replace("%DEVELOPER%", developer);
                    output = output.Replace("%OBJECT%", objectName);
                    output = output.Replace("%NAMESPACE%", objectNamespace);

                    File.AppendAllText(String.Format("{0}Output.txt", outputType), output);
                }
            }
        }

        private static IEnumerable<ObjectProperty> GetObjectProperties(String objectName, String objectDefinitionFile)
        {
            bool start = false;
            foreach (String line in File.ReadAllLines(objectDefinitionFile))
            {
                if (start && line != "TODO")
                {
                    Char[] delim = { ',' };
                    String[] tokens = line.Split(delim);
                    ObjectProperty returnValue = new ObjectProperty();
                    returnValue.CodeName = tokens[0].Replace("_", "").Trim();
                    if (tokens[0].Contains('_'))
                    {
                        returnValue.DbName = tokens[0].Trim();
                    }
                    else
                    {
                        returnValue.DbName = String.Format("{0}_{1}", objectName, tokens[0].Trim());
                    }
                    returnValue.MssqlDbType = tokens[1].ToUpperInvariant().Trim();
                    returnValue.Nullability = tokens[2].ToUpperInvariant().Trim();
                    returnValue.MssqlAdoType = GetAdoType(returnValue.MssqlDbType, false);
                    if (tokens[0] == "Id" || tokens[0].Contains('_'))
                    {
                        returnValue.CodeType = "Object";
                        returnValue.BaseCodeType = "Object";
                    }
                    else
                    {
                        returnValue.CodeType = GetCodeType(returnValue.MssqlDbType, returnValue.Nullability);
                        returnValue.BaseCodeType = GetBaseCodeType(returnValue.MssqlDbType);
                    }
                    if (tokens.Length > 3)
                    {
                        switch (tokens[3].ToUpperInvariant().Trim())
                        {
                            case "KEY":
                                returnValue.IsKey = true;
                                break;
                            case "FIXEDKEY":
                                returnValue.IsKey = true;
                                //returnValue.IsFixedKey = true;
                                break;
                        }
                    }
                    yield return returnValue;
                }
                else if (line.Trim() == "Properties")
                {
                    start = true;
                }
            }
        }

        #region Object Definition Interpretation

        private static String GetObjectSchema(String objectDefinitionFile)
        {
            String returnValue = null;
            bool start = false;
            foreach (String line in File.ReadAllLines(objectDefinitionFile))
            {
                if (start)
                {
                    returnValue = line.Trim();
                    break;
                }
                if (line.Trim() == "Schema")
                {
                    start = true;
                }
            }
            return returnValue;
        }

        private static IEnumerable<String> GetStoredProcedures(String objectDefinitionFile)
        {
            bool start = false;
            foreach (String line in File.ReadAllLines(objectDefinitionFile))
            {
                if (start)
                {
                    if (String.IsNullOrWhiteSpace(line))
                    {
                        break;
                    }
                    yield return line.Trim();
                }
                else if (line.Trim() == "Stored Procedures")
                {
                    start = true;
                }
            }
        }

        private static String GetCodeType(String type, String nullability)
        {
            String returnValue = "";
            String trimmedType = type.Replace("[", "");
            trimmedType = trimmedType.Replace("]", "");
            if (trimmedType.Contains("CHAR"))
            {
                returnValue = "String";
            }
            else if (trimmedType == "SQL_VARIANT")
            {
                returnValue = "Byte[]";
            }
            else
            {
                switch (trimmedType)
                {
                    case "TINYINT":
                        returnValue = "Byte";
                        break;
                    case "SMALLINT":
                        returnValue = "Int16";
                        break;
                    case "INT":
                        returnValue = "Int32";
                        break;
                    case "BIGINT":
                        returnValue = "Int64";
                        break;
                    case "BIT":
                        returnValue = "Boolean";
                        break;
                    case "REAL":
                        returnValue = "Single";
                        break;
                    case "FLOAT":
                        returnValue = "Double";
                        break;
                    case "SMALLDATETIME":
                    case "DATETIME":
                    case "DATE":
                        returnValue = "DateTime";
                        break;
                    case "UNIQUEIDENTIFIER":
                        returnValue = "Guid";
                        break;
                }
                if (nullability == "NULL")
                {
                    returnValue += "?";
                }
            }
            return returnValue;
        }

        private static String GetBaseCodeType(String type)
        {
            String returnValue = "";
            String trimmedType = type.Replace("[", "");
            trimmedType = trimmedType.Replace("]", "");
            if (trimmedType.Contains("CHAR"))
            {
                returnValue = "String";
            }
            else if (trimmedType == "SQL_VARIANT")
            {
                returnValue = "Byte[]";
            }
            else
            {
                switch (trimmedType)
                {
                    case "TINYINT":
                        returnValue = "Byte";
                        break;
                    case "SMALLINT":
                        returnValue = "Int16";
                        break;
                    case "INT":
                        returnValue = "Int32";
                        break;
                    case "BIGINT":
                        returnValue = "Int64";
                        break;
                    case "BIT":
                        returnValue = "Boolean";
                        break;
                    case "REAL":
                        returnValue = "Single";
                        break;
                    case "FLOAT":
                        returnValue = "Double";
                        break;
                    case "SMALLDATETIME":
                    case "DATETIME":
                    case "DATE":
                        returnValue = "DateTime";
                        break;
                    case "UNIQUEIDENTIFIER":
                        returnValue = "Guid";
                        break;
                }
            }
            return returnValue;
        }

        private static String GetAdoType(String type, Boolean vistaDb)
        {
            String returnValue = "";
            String trimmedType = type.Replace("[", "");
            trimmedType = trimmedType.Replace("]", "");
            if (trimmedType.Contains("NVARCHAR"))
            {
                returnValue = "NVarChar";
            }
            else if (trimmedType.Contains("VARCHAR"))
            {
                returnValue = "VarChar";
            }
            else
            {
                switch (trimmedType)
                {
                    case "TINYINT":
                        returnValue = "TinyInt";
                        break;
                    case "SMALLINT":
                        returnValue = "SmallInt";
                        break;
                    case "INT":
                        returnValue = "Int";
                        break;
                    case "BIGINT":
                        returnValue = "BigInt";
                        break;
                    case "BIT":
                        returnValue = "Bit";
                        break;
                    case "REAL":
                        returnValue = "Real";
                        break;
                    case "FLOAT":
                        returnValue = "Float";
                        break;
                    case "SMALLDATETIME":
                        returnValue = "SmallDateTime";
                        break;
                    case "DATETIME":
                        returnValue = "DateTime";
                        break;
                    case "DATE":
                        returnValue = "Date";
                        break;
                    case "SQL_VARIANT":
                        returnValue = "Variant";
                        break;
                    case "VARBINARY":
                        returnValue = "VarBinary";
                        break;
                    case "UNIQUEIDENTIFIER":
                        returnValue = "UniqueIdentifier";
                        break;
                }
            }
            if (vistaDb)
            {
                returnValue = String.Format("VistaDBType.{0}", returnValue);
            }
            else
            {
                returnValue = String.Format("SqlDbType.{0}", returnValue);
            }
            return returnValue;
        }

        private static List<String> GetFetchCriteria(String fetchSignature)
        {
            List<String> returnValue = new List<String>();
            Char[] delim = { '_' };
            String[] tokens = fetchSignature.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            for (Int32 i = 1; i < tokens.Length; i++)
            {
                returnValue.Add(tokens[i]);
            }
            return returnValue;
        }

        private static String GetFetchMethodName(List<String> fetchCriteria)
        {
            StringBuilder returnValue = new StringBuilder();
            returnValue.Append("FetchBy");
            foreach (String criterion in fetchCriteria)
            {
                returnValue.Append(criterion);
            }
            return returnValue.ToString();
        }

        private static String GetFetchMethodArguments(List<String> fetchCriteria, List<ObjectProperty> properties)
        {
            StringBuilder returnValue = new StringBuilder();
            for (Int32 i = 0; i < fetchCriteria.Count; i++)
            {
                if (i > 0)
                {
                    returnValue.Append(", ");
                }
                ObjectProperty property = properties.First(p => p.CodeName == fetchCriteria[i]);
                returnValue.Append(property.CodeType);
                returnValue.Append(" ");
                returnValue.Append(property.CodeName.Substring(0, 1).ToLowerInvariant());
                returnValue.Append(property.CodeName.Substring(1));
            }
            return returnValue.ToString();
        }

        private static List<String> GetDeleteCriteria(String deleteSignature)
        {
            List<String> returnValue = new List<String>();
            Char[] delim = { '_' };
            String[] tokens = deleteSignature.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            for (Int32 i = 1; i < tokens.Length; i++)
            {
                returnValue.Add(tokens[i]);
            }
            return returnValue;
        }

        private static String GetDeleteMethodName(List<String> deleteCriteria)
        {
            StringBuilder returnValue = new StringBuilder();
            returnValue.Append("DeleteBy");
            foreach (String criterion in deleteCriteria)
            {
                returnValue.Append(criterion);
            }
            return returnValue.ToString();
        }

        #endregion

        #region Dto-Related Methods

        private static String GetDtoProperties(
            String schema,
            IEnumerable<ObjectProperty> properties,
            Boolean keyPropertiesOnly)
        {
            String normalPattern = "public {0} {1} {{ get; set; }}";
            String foreignKeyPatternNonMasterData = "[ForeignKey(typeof({2}Dto), typeof(I{2}Dal))]" +
                Environment.NewLine + "public {0} {1} {{ get; set; }}";
            String foreignKeyPatternMasterData = "[ForeignKey(typeof({2}Dto), typeof(I{2}Dal), DeleteBehavior.Leave)]" +
                Environment.NewLine + "public {0} {1} {{ get; set; }}";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                if ((!keyPropertiesOnly) || (property.IsKey))
                {
                    if (property.DbName.EndsWith("_Id") && (!keyPropertiesOnly))
                    {
                        String pattern = foreignKeyPatternNonMasterData;
                        if (schema == "MasterData")
                        {
                            pattern = foreignKeyPatternMasterData;
                        }
                        resolvedPattern = String.Format(
                            pattern,
                            property.CodeType,
                            property.CodeName,
                            property.DbName.Substring(0, property.DbName.Length - 3));
                    }
                    else
                    {
                        resolvedPattern = String.Format(normalPattern, property.CodeType, property.CodeName);
                    }
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }
            if ((schema == "MasterData") && (!keyPropertiesOnly))
            {
                resolvedPattern = String.Format(normalPattern, "DateTime", "DateCreated");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(normalPattern, "DateTime", "DateLastModified");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(normalPattern, "DateTime?", "DateDeleted");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetIsSetProperties(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "public Boolean Is{0}Set {{ get; set; }}";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDtoEqualsOverrides(
            String schema,
            IEnumerable<ObjectProperty> properties,
            Boolean keyPropertiesOnly)
        {
            String pattern = "if(other.{0} != this.{0}) {{ return false; }}";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                if ((!keyPropertiesOnly) || (property.IsKey))
                {
                    resolvedPattern = String.Format(pattern, property.CodeName);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            if ((schema == "MasterData") && (!keyPropertiesOnly))
            {
                resolvedPattern = String.Format(pattern, "DateCreated");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateLastModified");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateDeleted");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetIsSetEqualsOverrides(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "if(other.Is{0}Set != this.Is{0}Set) {{ return false; }}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetIsSetSet(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "Is{0}Set = isSet;";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDtoKeyInit(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "{0} = this.{0},";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeName);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString.Substring(0, finalString.Length - 1); // Remove trailing comma.
        }

        private static String GetDtoKeyHash(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "{0}.GetHashCode() + ";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeName);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString.Substring(0, finalString.Length - 3); // Remove trailing " + ".
        }

        private static String GetIsSetHash(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "Is{0}Set.GetHashCode() + ";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 3); // Remove trailing " + ".
        }

        #endregion

        #region Dal-Related Methods

        private static String GetColumnConstants(
            String schema,
            String objectName,
            IEnumerable<ObjectProperty> properties)
        {
            String pattern = "public const String {0}{1} = \"{2}\";";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                resolvedPattern = String.Format(pattern, objectName, property.CodeName, property.DbName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }
            if (schema == "MasterData")
            {
                resolvedPattern = String.Format(pattern, objectName, "DateCreated", objectName + "_DateCreated");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName, "DateLastModified", objectName + "_DateLastModified");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName, "DateDeleted", objectName + "_DateDeleted");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName, "SetProperties", objectName + "_SetProperties");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetStoredProcedureConstants(String objectName, String schema, IEnumerable<String> storedProcedures, bool includeSchema)
        {
            String pattern = "public const String {0}{1} = \"{0}_{1}\";";
            if (includeSchema)
            {
                pattern = "public const String {0}{1} = \"{2}.{0}_{1}\";";
            }

            String finalString = String.Empty;
            foreach (String storedProcedure in storedProcedures)
            {
                String resolvedStoredProcedure = storedProcedure.Replace("FetchBy_", "FetchBy");
                resolvedStoredProcedure = resolvedStoredProcedure.Replace("DeleteBy_", "DeleteBy");
                String resolvedPattern = String.Format(pattern, objectName, resolvedStoredProcedure, schema);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetIDalMethods(String objectName, IEnumerable<String> storedProcedures, List<ObjectProperty> properties)
        {
            String fetchAllPattern = "IEnumerable<{0}Dto> FetchAll();";
            String fetchByIdPattern = "{0}Dto FetchById(Object id);";
            String fetchByEntityIdPattern = "IEnumerable<{0}Dto> FetchByEntityId(Int32 entityId);";
            String fetchByCriteriaPattern = "IEnumerable<{0}Dto> {1}({2});";
            String insertPattern = "void Insert({0}Dto dto);";
            String bulkInsertPattern = "void Insert(IEnumerable<{0}Dto> dtoList);";
            String updateByIdPattern = "void Update({0}Dto dto);";
            String bulkUpsertPattern = "void Upsert(IEnumerable<{0}Dto> dtoList, {0}IsSetDto isSetDto, Boolean updateDeleted);";
            String deleteByIdPattern = "void DeleteById(Object id);";
            String deleteByEntityIdPattern = "void DeleteByEntityId(Int32 entityId);";
            String deleteByCriteriaPattern = "void {3}({4});";

            String finalString = String.Empty;
            foreach (String storedProcedure in storedProcedures)
            {
                List<String> fetchCriteria = new List<String>();
                List<String> deleteCriteria = new List<String>();
                String resolvedPattern = storedProcedure;
                if (resolvedPattern.StartsWith("FetchBy_"))
                {
                    fetchCriteria = GetFetchCriteria(resolvedPattern);
                    resolvedPattern = fetchByCriteriaPattern;
                }
                else if (resolvedPattern.StartsWith("DeleteBy_"))
                {
                    deleteCriteria = GetDeleteCriteria(resolvedPattern);
                    resolvedPattern = deleteByCriteriaPattern;
                }
                switch (storedProcedure)
                {
                    case "FetchAll":
                        resolvedPattern = fetchAllPattern;
                        break;
                    case "FetchById":
                        resolvedPattern = fetchByIdPattern;
                        break;
                    case "FetchByEntityId":
                        resolvedPattern = fetchByEntityIdPattern;
                        break;
                    case "Insert":
                        resolvedPattern = insertPattern;
                        break;
                    case "BulkInsert":
                        resolvedPattern = bulkInsertPattern;
                        break;
                    case "UpdateById":
                        resolvedPattern = updateByIdPattern;
                        break;
                    case "BulkUpsert":
                        resolvedPattern = bulkUpsertPattern;
                        break;
                    case "DeleteById":
                        resolvedPattern = deleteByIdPattern;
                        break;
                    case "DeleteByEntityId":
                        resolvedPattern = deleteByEntityIdPattern;
                        break;
                }
                resolvedPattern = String.Format(resolvedPattern, objectName,
                    GetFetchMethodName(fetchCriteria), GetFetchMethodArguments(fetchCriteria, properties),
                    GetDeleteMethodName(deleteCriteria), GetFetchMethodArguments(deleteCriteria, properties));
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDalDtoProperties(
            String schema,
            String objectName,
            IEnumerable<ObjectProperty> properties)
        {
            String pattern = "{1} = ({0})GetValue(dr[FieldNames.{2}{1}])";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName, objectName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }
            if (schema == "MasterData")
            {
                resolvedPattern = String.Format(pattern, "DateTime", "DateCreated", objectName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateTime", "DateLastModified", objectName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateTime?", "DateDeleted", objectName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDalInsertUpdateParameters(String objectName, IEnumerable<ObjectProperty> properties, Boolean vistaDb)
        {
            String pattern = "CreateParameter(command, FieldNames.{0}{1}, {2}, dto.{1});";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, objectName, property.CodeName, property.MssqlAdoType);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDalUpdateDda(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String nonNullablePattern = "if (isInsert || isSetDto.Is{2}Set || (!table.Get(FieldNames.{1}DateDeleted).IsNull)){0}" +
                "{{{0}" +
                "table.Put{3}(FieldNames.{1}{2}, dto.{2});" +
                "}}";
            String nullablePattern = "if (isInsert || isSetDto.Is{2}Set || (!table.Get(FieldNames.{1}DateDeleted).IsNull)){0}" +
                "if (dto.{3} != null){0}" +
                "{{{0}" +
                "table.Put{4}(FieldNames.{2}{3}, ({4})dto.{3});{0}" +
                "}}" +
                "else{0}" +
                "{{{0}" +
                "table.PutNull(FieldNames.{2}{3});{0}" +
                "}}" +
                "}}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = "";
                if (property.Nullability == "NOT NULL")
                {
                    resolvedPattern = String.Format(nonNullablePattern, property.CodeType, objectName, property.CodeName, property.BaseCodeType);
                }
                else
                {
                    resolvedPattern = String.Format(nullablePattern, Environment.NewLine, property.CodeType, objectName, property.CodeName, property.BaseCodeType);
                }
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDalTempTableColumns(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		\"[{0}] {1}, \" +";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.DbName, property.MssqlDbType);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString = finalString.Substring(0, finalString.Length - 5) +
                finalString.Substring(finalString.Length - 4); // Remove trailing comma.
        }

        private static String GetDalBulkCopyCases(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "case {1}:{0}value = _enumerator.Current.{2};{0}break;";

            String finalString = String.Empty;
            Int32 i = 0;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, Environment.NewLine, i, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                i++;
            }

            return finalString;
        }

        private static String GetMssqlDalDtoKey(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "key.{0} = ({1})GetValue(dr[FieldNames.%OBJECT%{0}]);";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeName, property.CodeType);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString;
        }

        private static String GetSetPropertiesCode(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern1 = "if (isSetDto.Is{0}Set)";
            String pattern2 = "{";
            String pattern3 = "setProperties = String.Format(\"{{0}}#{{1}}#\", setProperties, FieldNames.{1}{0});";
            String pattern4 = "}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {

                String resolvedPattern1 = String.Format(pattern1, property.CodeName);
                String resolvedPattern3 = String.Format(pattern3, property.CodeName, objectName);
                finalString = String.Format(
                    "{0} {1} {2} {3} {2} {4} {2} {5} {2}",
                    finalString,
                    resolvedPattern1,
                    Environment.NewLine,
                    pattern2,
                    resolvedPattern3,
                    pattern4);

            }

            return finalString;
        }

        private static String GetVistaDbDalDtoKey(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "key.{0} = ({1})table.Get(FieldNames.{2}{0}).Value;";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeName, property.CodeType, objectName);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString;
        }

        private static String GetDalFetchParameters(
            String objectName,
            IEnumerable<ObjectProperty> properties,
            List<String> fetchCriteria,
            Boolean vistaDb)
        {
            String pattern = "CreateParameter(command, FieldNames.{0}{1}, {2}, {3});";

            String finalString = String.Empty;
            foreach (String criterion in fetchCriteria)
            {
                ObjectProperty property = properties.First(p => p.CodeName == criterion);
                String resolvedPattern = String.Format(pattern, objectName, property.CodeName, property.MssqlAdoType, FirstLetterToLowercase(property.CodeName));
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String FirstLetterToLowercase(String input)
        {
            return String.Format("{0}{1}", input.Substring(0, 1).ToLowerInvariant(), input.Substring(1));
        }

        #endregion

        #region ModelRelated Methods

        private static String GetModelProperties(IEnumerable<ObjectProperty> properties)
        {
            String pattern =
            "public static readonly ModelPropertyInfo<{0}> {1}Property =" + Environment.NewLine +
          "RegisterModelProperty<{0}>(c => c.{1});" + Environment.NewLine +
        "public {0} {1}" + Environment.NewLine +
        "{{" + Environment.NewLine +
            "get {{ return GetProperty<{0}>({1}Property); }}" + Environment.NewLine +
            "set {{ SetProperty<{0}>({1}Property, value); }}" + Environment.NewLine +
            "}}" + Environment.NewLine + "" + Environment.NewLine;

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);
            }

            return finalString;
        }

        private static String GetModelGetDtoProperties(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "{1} = ReadProperty<{0}>({1}Property),";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);
            }

            return finalString;
        }

        private static String GetModelLoadDtoProperties(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "LoadProperty<{0}>({1}Property, dto.{1});";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);
            }

            return finalString;
        }

        private static String GetModelBusinessRules(IEnumerable<ObjectProperty> properties)
        {

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {

                //if the property is a string add a max length rule
                if (property.CodeType == "String")
                {
                    //if property is a string and cannot be null add a required rule
                    if (property.Nullability == "NOT NULL")
                    {
                        String requiredPattern =
                            String.Format("BusinessRules.AddRule(new Required({0}Property));", property.CodeName);
                        finalString = String.Format("{0} {1} {2}", finalString, requiredPattern, Environment.NewLine);
                    }


                    String maxLength = property.MssqlDbType.Substring(11, property.MssqlDbType.IndexOf(')') - 11);

                    String maxLengthPattern =
                        String.Format("BusinessRules.AddRule(new MaxLength({0}Property, {1}));", property.CodeName, maxLength);
                    finalString = String.Format("{0} {1} {2}", finalString, maxLengthPattern, Environment.NewLine);
                }

                //if the property is an int key then set a min val of 1.
                else if (property.CodeType == "Int32" && property.IsKey)
                {
                    String requiredPattern =
                            String.Format("BusinessRules.AddRule(new MinValue<Int32>({0}Property, 1));", property.CodeName);
                    finalString = String.Format("{0} {1} {2}", finalString, requiredPattern, Environment.NewLine);
                }
            }

            return finalString;
        }

        #endregion
    }
}
