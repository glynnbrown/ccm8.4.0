﻿Schema
MasterData

Stored Procedures
FetchById
FetchByEntityId
Insert
UpdateById
DeleteById

Properties
Entity_Id, [INT], NOT NULL, KEY
Name, [NVARCHAR](50), NOT NULL