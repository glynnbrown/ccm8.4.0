﻿Schema
Content

Stored Procedures
FetchBy_PlanogramId
Insert
UpdateById
DeleteById

Properties
Planogram_Id, [INT], NOT NULL, KEY
Gtin, [nvarchar](14), NOT NULL
Name, [nvarchar](100), NOT NULL
Brand, [nvarchar](20), NULL
Height, [real], NOT NULL
Width, [real], NOT NULL
Depth, [real], NOT NULL
DisplayHeight, [real], NOT NULL
DisplayWidth, [real], NOT NULL
DisplayDepth, [real], NOT NULL
AlternateHeight, [real], NOT NULL
AlternateWidth, [real], NOT NULL
AlternateDepth, [real], NOT NULL
PointOfPurchaseHeight, [real], NOT NULL
PointOfPurchaseWidth, [real], NOT NULL
PointOfPurchaseDepth, [real], NOT NULL
NumberOfPegHoles, [tinyint], NOT NULL
PegX, [real], NOT NULL
PegX2, [real], NOT NULL
PegX3, [real], NOT NULL
PegY, [real], NOT NULL
PegY2, [real], NOT NULL
PegY3, [real], NOT NULL
PegProngOffset, [real], NOT NULL
PegDepth, [real], NOT NULL
SqueezeHeight, [real], NOT NULL
SqueezeWidth, [real], NOT NULL
SqueezeDepth, [real], NOT NULL
NestingHeight, [real], NOT NULL
NestingWidth, [real], NOT NULL
NestingDepth, [real], NOT NULL
CasePackUnits, [smallint], NOT NULL
CaseHigh, [tinyint], NOT NULL
CaseWide, [tinyint], NOT NULL
CaseDeep, [tinyint], NOT NULL
CaseHeight, [real], NOT NULL
CaseWidth, [real], NOT NULL
CaseDepth, [real], NOT NULL
MaxStack, [tinyint], NOT NULL
MaxTopCap, [tinyint], NOT NULL
MaxRightCap, [tinyint], NOT NULL
MinDeep, [tinyint], NOT NULL
MaxDeep, [tinyint], NOT NULL
TrayPackUnits, [smallint], NOT NULL
TrayHigh, [tinyint], NOT NULL
TrayWide, [tinyint], NOT NULL
TrayDeep, [tinyint], NOT NULL
TrayHeight, [real], NOT NULL
TrayWidth, [real], NOT NULL
TrayDepth, [real], NOT NULL
TrayThickHeight, [real], NOT NULL
TrayThickWidth, [real], NOT NULL
TrayThickDepth, [real], NOT NULL
FrontOverhang, [real], NOT NULL
FingerSpaceAbove, [real], NOT NULL
FingerSpaceToTheSide, [real], NOT NULL
StatusType, [tinyint], NOT NULL
OrientationType, [tinyint], NOT NULL
MerchandisingStyle, [tinyint], NOT NULL
IsFrontOnly, [bit], NOT NULL
IsTrayProduct, [bit], NOT NULL
IsPlaceHolderProduct, [bit], NOT NULL
IsActive, [bit], NOT NULL
CanBreakTrayUp, [bit], NOT NULL
CanBreakTrayDown, [bit], NOT NULL
CanBreakTrayBack, [bit], NOT NULL
CanBreakTrayTop, [bit], NOT NULL
ForceMiddleCap, [bit], NOT NULL
ForceBottomCap, [bit], NOT NULL
PlacementStyle, [SMALLINT], NOT NULL
PlanogramProduct_PlanogramImageIdFront, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdBack, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdTop, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdBottom, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdLeft, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdRight, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdDisplayFront, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdDisplayBack, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdDisplayTop, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdDisplayBottom, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdDisplayLeft, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdDisplayRight, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdTrayFront, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdTrayBack, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdTrayTop, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdTrayBottom, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdTrayLeft, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdTrayRight, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdPointOfPurchaseFront, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdPointOfPurchaseBack, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdPointOfPurchaseTop, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdPointOfPurchaseBottom, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdPointOfPurchaseLeft, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdPointOfPurchaseRight, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdAlternateFront, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdAlternateBack, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdAlternateTop, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdAlternateBottom, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdAlternateLeft, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdAlternateRight, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdCaseFront, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdCaseBack, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdCaseTop, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdCaseBottom, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdCaseLeft, [INT], NOT NULL, KEY
PlanogramProduct_PlanogramImageIdCaseRight, [INT], NOT NULL, KEY
ShapeType, [TINYINT], NOT NULL
FillPatternType, [TINYINT], NOT NULL
FillColour, [INT], NOT NULL