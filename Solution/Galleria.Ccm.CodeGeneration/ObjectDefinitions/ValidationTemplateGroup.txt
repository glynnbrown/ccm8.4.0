﻿Schema
MasterData

Stored Procedures
FetchBy_ValidationTemplateId
Insert
UpdateById
DeleteById

Properties
ValidationTemplate_Id, [INT], NOT NULL, KEY, PARENTID
Name, [NVARCHAR](50), NOT NULL, KEY
Threshold1, [REAL], NOT NULL
Threshold2, [REAL], NOT NULL