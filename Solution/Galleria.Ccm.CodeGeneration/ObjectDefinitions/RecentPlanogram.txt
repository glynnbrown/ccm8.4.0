﻿Schema
Content

Stored Procedures
FetchAll
FetchById
Insert
UpdateById
DeleteById

Properties
Name, [NVARCHAR](100), NOT NULL
Type, [TINYINT], NOT NULL
DateLastAccessed, [DATETIME], NOT NULL
IsPinned, [BIT], NOT NULL
Location, [NVARCHAR](MAX), NULL