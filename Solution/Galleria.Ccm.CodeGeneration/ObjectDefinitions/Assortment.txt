﻿Schema
MasterData

Stored Procedures
FetchById
Insert
UpdateById
DeleteById

Properties
Entity_Id, [INT], NOT NULL, KEY
ProductGroup_Id, [INT], NOT NULL, KEY
ConsumerDecisionTree_Id, [INT], NULL, KEY
Name, [NVARCHAR](255), NOT NULL
UniqueContentReference, [UNIQUEIDENTIFIER], NOT NULL 


