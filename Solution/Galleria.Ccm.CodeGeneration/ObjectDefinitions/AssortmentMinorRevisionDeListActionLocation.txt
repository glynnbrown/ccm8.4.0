﻿Schema
MasterData

Stored Procedures
FetchById
Insert
UpdateById
DeleteById

Properties
AssortmentMinorRevisionDeListAction_Id, [INT], NOT NULL, PARENTID
Location_Id, [SMALLINT], NULL, KEY
LocationCode, [NVARCHAR](50), NOT NULL 
LocationName, [NVARCHAR](50), NOT NULL  
