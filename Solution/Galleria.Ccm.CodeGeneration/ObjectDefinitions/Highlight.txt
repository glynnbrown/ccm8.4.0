﻿Schema
Content

Stored Procedures
FetchAll
Insert
UpdateById
DeleteById

Properties
Name, [NVARCHAR](50), NOT NULL
Type, [TINYINT], NOT NULL
MethodType, [TINYINT], NOT NULL
IsFilterEnabled, [BIT], NOT NULL
IsPreFilter, [BIT], NOT NULL
IsAndFilter, [BIT], NOT NULL
Field1, [NVARCHAR](100), NOT NULL, KEY
Field2, [NVARCHAR](100), NOT NULL, KEY
QuadrantXType, [TINYINT], NOT NULL
QuadrantXConstant, [REAL], NOT NULL
QuadrantYType, [TINYINT], NOT NULL
QuadrantYConstant, [REAL], NOT NULL