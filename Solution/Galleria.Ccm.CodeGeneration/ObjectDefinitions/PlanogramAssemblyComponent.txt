﻿Schema
Content

Stored Procedures
FetchById
Insert
UpdateById
DeleteById

Properties
PlanogramAssembly_Id, [INT], NOT NULL, KEY
PlanogramComponent_Id, [INT], NOT NULL, KEY
X, [REAL], NOT NULL
Y, [REAL], NOT NULL
Z, [REAL], NOT NULL
Slope, [REAL], NOT NULL
Angle, [REAL], NOT NULL
Roll, [REAL], NOT NULL