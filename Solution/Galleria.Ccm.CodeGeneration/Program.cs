﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (SA 1.0)
// SA-15877 : K.Pickup
//      Added support for floats.
// SA-15609 : K.Pickup
//      Brought up to date with latest DAL conventions.
// SA-17779 : L.Hodson
//      Added generation for model objects.
// SA-17728 : L.Hodson
//      Added guid to GetCodeType.
// SA-17752 : L.Hodson
//      Corrected fetchbycriteria template for masterdata
// SA-11745 : L.Hodson
//      Added MssqlUpdateByIdStoredProcMasterData template.
//      Added support for delete by criteria.
// SA-17748 : L.Hodson
//      Corrected GetSetPropertiesCode() as it was only outputting for key properties
// SA-18077 : K.Pickup
//      Changed data type of DateCreated, DateDeleted, and DateLastModified to SMALLDATETIME. 
// SA-18118 : K.Pickup
//      Various additions/fixes.
#endregion
#region Version History: (SA 2.0)
// SA-19856 : L.Hodson
//      Updated GetSetPropertiesCode and GetSetPropertiesScripts with end hash.
#endregion
#region Version History: (CCM 8.0)
// CCM v8 : L.Hodson
//      Stolen from SA :)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.CodeGeneration
{
    public class Program
    {
        #region Nested Classes

        private class ObjectProperty
        {
            public String CodeName { get; set; }

            public String CodeType { get; set; }

            public String BaseCodeType { get; set; }

            public String DbName { get; set; }

            public String MssqlDbType { get; set; }

            public String VistaDbDbType { get; set; }

            public String MssqlAdoType { get; set; }

            public String VistaDbAdoType { get; set; }

            public String Nullability { get; set; }

            public Boolean IsKey { get; set; }

            public Boolean IsParentId { get; set; }

            public Boolean IsInInfo { get; set; }

            //public Boolean IsFixedKey { get; set; }
        }

        #endregion

        #region Constants

        private const String _templatePath = "Templates";
        private const String _dtoTemplate = "Dto.txt";
        private const String _dtoKeyTemplate = "DtoKey.txt";
        private const String _dtoKeyClassTemplate = "DtoKeyClass.txt";
        private const String _dtoIsSetClassTemplate = "DtoIsSetClass.txt";
        private const String _iDalTemplate = "IDal.txt";
        private const String _vistaDbSchemaTemplate = "VistaDbSchema.txt";
        private const String _mssqlSchemaTemplate = "MssqlSchema.txt";
        private const String _vistaDbTableTemplate = "VistaDbTable.txt";
        private const String _mssqlTableTemplate = "MssqlTable.txt";
        private const String _vistaDbFetchAllStoredProcTemplate = "VistaDbFetchAllStoredProc.txt";
        private const String _vistaDbFetchAllStoredProcMasterDataTemplate = "VistaDbFetchAllStoredProcMasterData.txt";
        private const String _vistaDbFetchByIdStoredProcTemplate = "VistaDbFetchByIdStoredProc.txt";
        private const String _vistaDbFetchByIdStoredProcMasterDataTemplate = "VistaDbFetchByIdStoredProcMasterData.txt";
        private const String _vistaDbFetchByEntityIdStoredProcTemplate = "VistaDbFetchByEntityIdStoredProc.txt";
        private const String _vistaDbFetchByEntityIdStoredProcMasterDataTemplate = "VistaDbFetchByEntityIdStoredProcMasterData.txt";
        private const String _vistaDbFetchByCriteriaStoredProcTemplate = "VistaDbFetchByCriteriaStoredProc.txt";
        private const String _vistaDbFetchByCriteriaStoredProcMasterDataTemplate = "VistaDbFetchByCriteriaStoredProcMasterData.txt";
        private const String _vistaDbInsertStoredProcTemplate = "VistaDbInsertStoredProc.txt";
        private const String _vistaDbInsertStoredProcMasterDataTemplate = "VistaDbInsertStoredProcMasterData.txt";
        private const String _vistaDbUpdateByIdStoredProcTemplate = "VistaDbUpdateByIdStoredProc.txt";
        private const String _vistaDbDeleteByIdStoredProcTemplate = "VistaDbDeleteByIdStoredProc.txt";
        private const String _vistaDbDeleteByIdStoredProcMasterDataTemplate = "VistaDbDeleteByIdStoredProcMasterData.txt";
        private const String _vistaDbDeleteByEntityIdStoredProcTemplate = "VistaDbDeleteByEntityIdStoredProc.txt";
        private const String _vistaDbDeleteByEntityIdStoredProcMasterDataTemplate = "VistaDbDeleteByEntityIdStoredProcMasterData.txt";
        private const String _mssqlFetchAllStoredProcTemplate = "MssqlFetchAllStoredProc.txt";
        private const String _mssqlFetchAllStoredProcMasterDataTemplate = "MssqlFetchAllStoredProcMasterData.txt";
        private const String _mssqlFetchByIdStoredProcTemplate = "MssqlFetchByIdStoredProc.txt";
        private const String _mssqlFetchByIdStoredProcMasterDataTemplate = "MssqlFetchByIdStoredProcMasterData.txt";
        private const String _mssqlFetchByEntityIdStoredProcTemplate = "MssqlFetchByEntityIdStoredProc.txt";
        private const String _mssqlFetchByEntityIdStoredProcMasterDataTemplate = "MssqlFetchByEntityIdStoredProcMasterData.txt";
        private const String _mssqlFetchByCriteriaStoredProcTemplate = "MssqlFetchByCriteriaStoredProc.txt";
        private const String _mssqlFetchByCriteriaStoredProcMasterDataTemplate = "MssqlFetchByCriteriaStoredProcMasterData.txt";
        private const String _mssqlInsertStoredProcTemplate = "MssqlInsertStoredProc.txt";
        private const String _mssqlBulkInsertStoredProcTemplate = "MssqlBulkInsertStoredProc.txt";
        private const String _mssqlInsertStoredProcMasterDataTemplate = "MssqlInsertStoredProcMasterData.txt";
        private const String _mssqlUpdateByIdStoredProcTemplate = "MssqlUpdateByIdStoredProc.txt";
        private const String _mssqlUpdateByIdStoredProcMasterDataTemplate = "MssqlUpdateByIdStoredProcMasterData.txt";
        private const String _mssqlBulkUpsertStoredProcTemplate = "MssqlBulkUpsertStoredProc.txt";
        private const String _mssqlDeleteByIdStoredProcTemplate = "MssqlDeleteByIdStoredProc.txt";
        private const String _mssqlDeleteByIdStoredProcMasterDataTemplate = "MssqlDeleteByIdStoredProcMasterData.txt";
        private const String _mssqlDeleteByEntityIdStoredProcTemplate = "MssqlDeleteByEntityIdStoredProc.txt";
        private const String _mssqlDeleteByEntityIdStoredProcMasterDataTemplate = "MssqlDeleteByEntityIdStoredProcMasterData.txt";
        private const String _mssqlDeleteByCriteriaStoredProcTemplate = "MssqlDeleteByCriteriaStoredProc.txt";
        private const String _mssqlDeleteByCriteriaStoredProcMasterDataTemplate = "MssqlDeleteByCriteriaStoredProcMasterData.txt";
        private const String _mssqlUniqueIndexTemplate = "MssqlUniqueIndex.txt";
        private const String _mssqlForeignKeyTemplate = "MssqlForeignKey.txt";
        private const String _mssqlForeignKeyIndexTemplate = "MssqlForeignKeyIndex.txt";
        private const String _vistaDbDalTemplate = "VistaDbDal.txt";
        private const String _vistaDbFetchAllMethodTemplate = "VistaDbFetchAllMethod.txt";
        private const String _vistaDbFetchByIdMethodTemplate = "VistaDbFetchByIdMethod.txt";
        private const String _vistaDbFetchByEntityIdMethodTemplate = "VistaDbFetchByEntityIdMethod.txt";
        private const String _vistaDbFetchByCriteriaMethodTemplate = "VistaDbFetchByCriteriaMethod.txt";
        private const String _vistaDbInsertMethodTemplate = "VistaDbInsertMethod.txt";
        private const String _vistaDbUpdateMethodTemplate = "VistaDbUpdateMethod.txt";
        private const String _vistaDbBulkUpsertMethodTemplate = "VistaDbBulkUpsertMethod.txt";
        private const String _vistaDbDeleteByIdMethodTemplate = "VistaDbDeleteByIdMethod.txt";
        private const String _vistaDbDeleteByEntityIdMethodTemplate = "VistaDbDeleteByEntityIdMethod.txt";
        private const String _vistaDbUniqueIndexTemplate = "VistaDbUniqueIndex.txt";
        private const String _vistaDbForeignKeyTemplate = "VistaDbForeignKey.txt";
        private const String _vistaDbForeignKeyIndexTemplate = "VistaDbForeignKeyIndex.txt";
        private const String _mssqlDalTemplate = "MssqlDal.txt";
        private const String _mssqlFetchAllMethodTemplate = "MssqlFetchAllMethod.txt";
        private const String _mssqlFetchByIdMethodTemplate = "MssqlFetchByIdMethod.txt";
        private const String _mssqlFetchByEntityIdMethodTemplate = "MssqlFetchByEntityIdMethod.txt";
        private const String _mssqlFetchByCriteriaMethodTemplate = "MssqlFetchByCriteriaMethod.txt";
        private const String _mssqlInsertMethodTemplate = "MssqlInsertMethod.txt";
        private const String _mssqlBulkInsertMethodTemplate = "MssqlBulkInsertMethod.txt";
        private const String _mssqlUpdateMethodTemplate = "MssqlUpdateMethod.txt";
        private const String _mssqlDeleteByCriteriaMethodTemplate = "MssqlDeleteByCriteriaMethod.txt";
        private const String _mssqlDalConstantsTemplate = "MssqlDalConstants.txt";
        private const String _mssqlDalNestedClassesTemplate = "MssqlDalNestedClasses.txt";
        private const String _mssqlBulkUpsertMethodTemplate = "MssqlBulkUpsertMethod.txt";
        private const String _mssqlDeleteByIdMethodTemplate = "MssqlDeleteByIdMethod.txt";
        private const String _mssqlDeleteByEntityIdMethodTemplate = "MssqlDeleteByEntityIdMethod.txt";
        private const String _modelRootObjectTemplate = "ModelRootObject.txt";
        private const String _modelRootObjectServerTemplate = "ModelRootObjectServer.txt";
        private const String _modelChildObjectTemplate = "ModelChildObject.txt";
        private const String _modelChildObjectServerTemplate = "ModelChildObjectServer.txt";
        private const String _modelPropertyTemplate = "ModelProperty.txt";
        private const String _modelChildListTemplate = "ModelChildList.txt";
        private const String _modelChildListServerTemplate = "ModelChildListServer.txt";
        private const String _modelInfoObjectTemplate = "ModelInfoObject.txt";
        private const String _modelInfoObjectServerTemplate = "ModelInfoObjectServer.txt";
        private const String _modelInfoListTemplate = "ModelInfoList.txt";
        private const String _modelInfoListServerTemplate = "ModelInfoListServer.txt";
        private const String _modelInfoPropertyTemplate = "ModelInfoProperty.txt";
        private const String _maintenanceViewModelTemplate = "MaintenanceViewModel.txt";


        private const String _objectDefinitionPath = "ObjectDefinitions";

        #endregion

        public static void Main(String[] args)
        {
            if (args.Length != 5)
            {
                Console.WriteLine("Please call this app using the following arguments:");
                Console.WriteLine("ALL {object name} {CCM version} {gemini ticket #} {developer}");

                Console.WriteLine();
                Console.WriteLine("Press Enter to exit.");
                Console.ReadLine();
                return;
            }

            String[] objectNames;
            if (args[1] == "ALL")
            {
                objectNames =
                    Directory.GetFiles(_objectDefinitionPath)
                    .Select(f=> Path.GetFileNameWithoutExtension(f)).ToArray();
                    
            }
            else
            {
                objectNames = new String[] { args[1] };
            }

            foreach (String objectName in objectNames)
            {

                //String objectName = args[1];
                String objectDefinition = Path.Combine(_objectDefinitionPath, objectName + ".txt");
                String version = args[2];
                String gemini = args[3];
                String developer = args[4];
                String output = null;

                List<ObjectProperty> properties = new List<ObjectProperty>(GetObjectProperties(objectName, objectDefinition));

                List<String> storedProcedures = new List<String>(GetStoredProcedures(objectDefinition));
                String schema = GetObjectSchema(objectDefinition);

                ObjectProperty parentIdProperty = properties.FirstOrDefault(p => p.IsParentId);
                String parentName = "TODO";
                if (parentIdProperty != null)
                {
                    parentName = parentIdProperty.CodeName.Replace("Id", "");
                }



                String outputTypes = args[0];
                if (outputTypes == "ALL")
                {
                    outputTypes =
                        "DTO|IDAL|MSSQLSCHEMA|MSSQLTABLE|MSSQLDAL|MSSQLSTOREDPROCS|MSSQLKEYS|" +
                        "MODELROOTOBJECT|MODELROOTOBJECTSERVER|MODELCHILDOBJECT|MODELCHILDOBJECTSERVER|MODELCHILDLIST|MODELCHILDLISTSERVER|" + 
                        "MODELINFOOBJECT|MODELINFOOBJECTSERVER|MODELINFOLIST|MODELINFOLISTSERVER|MAINTENANCEVIEWMODEL";
                }
                Char[] delim = { '|' };
                String[] tokens = outputTypes.ToUpperInvariant().Split(delim, StringSplitOptions.RemoveEmptyEntries);

                foreach (String outputType in tokens)
                {
                    String fetchByCriteriaMethods = "";
                    String deleteByCriteriaMethods = "";
                    switch (outputType)
                    {
                        #region DTO
                        case "DTO":
                            output = File.ReadAllText(Path.Combine(_templatePath, _dtoTemplate));
                            output = output.Replace("%PROPERTIES%", GetDtoProperties(schema, properties, false));
                            output = output.Replace("%EQUALS%", GetDtoEqualsOverrides(schema, properties, false));
                            if (properties.Count(p => p.IsKey) > 0)
                            {
                                output = output.Replace("%DTOKEY%", File.ReadAllText(Path.Combine(_templatePath, _dtoKeyTemplate)));
                                output = output.Replace("%DTOKEYCLASS%", File.ReadAllText(Path.Combine(_templatePath, _dtoKeyClassTemplate)));
                                output = output.Replace("%KEYINIT%", GetDtoKeyInit(properties));
                                output = output.Replace("%KEYPROPERTIES%", GetDtoProperties(schema, properties, true));
                                output = output.Replace("%KEYHASH%", GetDtoKeyHash(properties));
                                output = output.Replace("%KEYEQUALS%", GetDtoEqualsOverrides(schema, properties, true));
                            }
                            else
                            {
                                output = output.Replace("%DTOKEY%", File.ReadAllText(Path.Combine(_templatePath, _dtoKeyTemplate)));
                                output = output.Replace("%DTOKEYCLASS%", File.ReadAllText(Path.Combine(_templatePath, _dtoKeyClassTemplate)));
                                output = output.Replace("%KEYINIT%", GetDtoKeyInit(null));
                            }
                            if (schema == "MasterData" && storedProcedures.Contains("BulkUpsert"))
                            {
                                output = output.Replace("%DTOISSETCLASS%", File.ReadAllText(Path.Combine(_templatePath, _dtoIsSetClassTemplate)));
                                output = output.Replace("%ISSETPROPERTIES%", GetIsSetProperties(properties));
                                output = output.Replace("%ISSETHASH%", GetIsSetHash(properties));
                                output = output.Replace("%ISSETEQUALS%", GetIsSetEqualsOverrides(properties));
                                output = output.Replace("%ISSETSET%", GetIsSetSet(properties));
                            }
                            else
                            {
                                output = output.Replace("%DTOISSETCLASS%", "");
                            }
                            break;
                        #endregion

                        #region IDAL
                        case "IDAL":
                            output = File.ReadAllText(Path.Combine(_templatePath, _iDalTemplate));
                            output = output.Replace("%METHODS%", GetIDalMethods(objectName, storedProcedures, properties));
                            break;
                        #endregion

                        #region VISTADBSCHEMA
                        //case "VISTADBSCHEMA":
                        //    output = File.ReadAllText(Path.Combine(_templatePath, _vistaDbSchemaTemplate));
                        //    output = output.Replace("%COLUMNS%", GetColumnConstants(schema, objectName, properties));
                        //    output = output.Replace("%STOREDPROCS%", GetStoredProcedureConstants(objectName, schema, storedProcedures, false));
                        //    break;
                        #endregion

                        #region MSSQLSCHEMA
                        case "MSSQLSCHEMA":
                            output = File.ReadAllText(Path.Combine(_templatePath, _mssqlSchemaTemplate));
                            output = output.Replace("%COLUMNS%", GetColumnConstants(schema, objectName, properties));
                            output = output.Replace("%STOREDPROCS%", GetStoredProcedureConstants(objectName, schema, storedProcedures, true));
                            break;
                        #endregion

                        #region VISTADBDAL
                        //case "VISTADBDAL":
                        //    output = File.ReadAllText(Path.Combine(_templatePath, _vistaDbDalTemplate));
                        //    foreach (String storedProcedure in storedProcedures)
                        //    {
                        //        switch (storedProcedure)
                        //        {
                        //            case "FetchAll":
                        //                output = output.Replace("%FETCHALL%", File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchAllMethodTemplate)));
                        //                break;
                        //            case "FetchById":
                        //                output = output.Replace("%FETCHBYID%", File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByIdMethodTemplate)));
                        //                break;
                        //            case "FetchByEntityId":
                        //                output = output.Replace("%FETCHBYENTITYID%", File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByEntityIdMethodTemplate)));
                        //                break;
                        //            case "Insert":
                        //                output = output.Replace("%INSERT%", File.ReadAllText(Path.Combine(_templatePath, _vistaDbInsertMethodTemplate)));
                        //                break;
                        //            case "UpdateById":
                        //                output = output.Replace("%UPDATE%", File.ReadAllText(Path.Combine(_templatePath, _vistaDbUpdateMethodTemplate)));
                        //                break;
                        //            case "BulkUpsert":
                        //                output = output.Replace("%UPSERT%", File.ReadAllText(Path.Combine(_templatePath, _vistaDbBulkUpsertMethodTemplate)));
                        //                break;
                        //            case "DeleteById":
                        //                output = output.Replace("%DELETEBYID%", File.ReadAllText(Path.Combine(_templatePath, _vistaDbDeleteByIdMethodTemplate)));
                        //                break;
                        //            case "DeleteByEntityId":
                        //                output = output.Replace("%DELETEBYENTITYID%", File.ReadAllText(Path.Combine(_templatePath, _vistaDbDeleteByEntityIdMethodTemplate)));
                        //                break;
                        //            default:
                        //                if (storedProcedure.StartsWith("FetchBy_"))
                        //                {
                        //                    fetchByCriteriaMethods = String.Format("{0} {1} {1} {2}", fetchByCriteriaMethods, Environment.NewLine, File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByCriteriaMethodTemplate)));
                        //                    List<String> fetchCriteria = GetFetchCriteria(storedProcedure);
                        //                    fetchByCriteriaMethods = fetchByCriteriaMethods.Replace("%METHODNAME%", GetFetchMethodName(fetchCriteria));
                        //                    fetchByCriteriaMethods = fetchByCriteriaMethods.Replace("%ARGUMENTS%", GetFetchMethodArguments(fetchCriteria, properties));
                        //                    fetchByCriteriaMethods = fetchByCriteriaMethods.Replace("%PARAMETERS%", GetDalFetchParameters(objectName, properties, fetchCriteria, true));
                        //                }
                        //                break;
                        //        }
                        //    }
                        //    output = output.Replace("%FETCHALL%", "");
                        //    output = output.Replace("%FETCHBYID%", "");
                        //    output = output.Replace("%FETCHBYENTITYID%", "");
                        //    output = output.Replace("%FETCHBYCRITERIA%", fetchByCriteriaMethods);
                        //    output = output.Replace("%INSERT%", "");
                        //    output = output.Replace("%UPDATE%", "");
                        //    output = output.Replace("%UPSERT%", "");
                        //    output = output.Replace("%DELETEBYID%", "");
                        //    output = output.Replace("%DELETEBYENTITYID%", "");
                        //    output = output.Replace("%GETDTO%", GetDalDtoProperties(schema, objectName, properties));
                        //    output = output.Replace("%PARAMETERS%", GetDalInsertUpdateParameters(objectName, properties, true));
                        //    output = output.Replace("%KEY%", GetVistaDbDalDtoKey(objectName, properties));
                        //    output = output.Replace("%UPDATEDDA%", GetDalUpdateDda(objectName, properties));
                        //    break;
                        #endregion

                        #region MSSQLDAL
                        case "MSSQLDAL":
                            output = File.ReadAllText(Path.Combine(_templatePath, _mssqlDalTemplate));
                            foreach (String storedProcedure in storedProcedures)
                            {
                                switch (storedProcedure)
                                {
                                    case "FetchAll":
                                        output = output.Replace("%FETCHALL%", File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchAllMethodTemplate)));
                                        break;
                                    case "FetchById":
                                        output = output.Replace("%FETCHBYID%", File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByIdMethodTemplate)));
                                        break;
                                    case "FetchByEntityId":
                                        output = output.Replace("%FETCHBYENTITYID%", File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByEntityIdMethodTemplate)));
                                        break;
                                    case "Insert":
                                        output = output.Replace("%INSERT%", File.ReadAllText(Path.Combine(_templatePath, _mssqlInsertMethodTemplate)));
                                        break;
                                    case "BulkInsert":
                                        output = output.Replace("%CONSTANTS%", File.ReadAllText(Path.Combine(_templatePath, _mssqlDalConstantsTemplate)));
                                        output = output.Replace("%NESTEDCLASSES%", File.ReadAllText(Path.Combine(_templatePath, _mssqlDalNestedClassesTemplate)));
                                        output = output.Replace("%BULKINSERT%", File.ReadAllText(Path.Combine(_templatePath, _mssqlBulkInsertMethodTemplate)));
                                        break;
                                    case "UpdateById":
                                        output = output.Replace("%UPDATE%", File.ReadAllText(Path.Combine(_templatePath, _mssqlUpdateMethodTemplate)));
                                        break;
                                    case "BulkUpsert":
                                        output = output.Replace("%CONSTANTS%", File.ReadAllText(Path.Combine(_templatePath, _mssqlDalConstantsTemplate)));
                                        output = output.Replace("%NESTEDCLASSES%", File.ReadAllText(Path.Combine(_templatePath, _mssqlDalNestedClassesTemplate)));
                                        output = output.Replace("%UPSERT%", File.ReadAllText(Path.Combine(_templatePath, _mssqlBulkUpsertMethodTemplate)));
                                        break;
                                    case "DeleteById":
                                        output = output.Replace("%DELETEBYID%", File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByIdMethodTemplate)));
                                        break;
                                    case "DeleteByEntityId":
                                        output = output.Replace("%DELETEBYENTITYID%", File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByEntityIdMethodTemplate)));
                                        break;
                                    default:
                                        if (storedProcedure.StartsWith("FetchBy_"))
                                        {
                                            fetchByCriteriaMethods = String.Format("{0} {1} {1} {2}", fetchByCriteriaMethods, Environment.NewLine, File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByCriteriaMethodTemplate)));
                                            List<String> fetchCriteria = GetFetchCriteria(storedProcedure);
                                            fetchByCriteriaMethods = fetchByCriteriaMethods.Replace("%METHODNAME%", GetFetchMethodName(fetchCriteria));
                                            fetchByCriteriaMethods = fetchByCriteriaMethods.Replace("%ARGUMENTS%", GetFetchMethodArguments(fetchCriteria, properties));
                                            fetchByCriteriaMethods = fetchByCriteriaMethods.Replace("%PARAMETERS%", GetDalFetchParameters(objectName, properties, fetchCriteria, false));
                                        }
                                        else if (storedProcedure.StartsWith("DeleteBy_"))
                                        {
                                            deleteByCriteriaMethods = String.Format("{0} {1} {1} {2}", deleteByCriteriaMethods, Environment.NewLine, File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByCriteriaMethodTemplate)));
                                            List<String> deleteCriteria = GetDeleteCriteria(storedProcedure);
                                            deleteByCriteriaMethods = deleteByCriteriaMethods.Replace("%METHODNAME%", GetDeleteMethodName(deleteCriteria));
                                            deleteByCriteriaMethods = deleteByCriteriaMethods.Replace("%ARGUMENTS%", GetFetchMethodArguments(deleteCriteria, properties));
                                            deleteByCriteriaMethods = deleteByCriteriaMethods.Replace("%PARAMETERS%", GetDalFetchParameters(objectName, properties, deleteCriteria, false));
                                        }
                                        break;
                                }
                            }
                            output = output.Replace("%FETCHALL%", "");
                            output = output.Replace("%FETCHBYID%", "");
                            output = output.Replace("%FETCHBYENTITYID%", "");
                            output = output.Replace("%FETCHBYCRITERIA%", fetchByCriteriaMethods);
                            output = output.Replace("%INSERT%", "");
                            output = output.Replace("%UPDATE%", "");
                            output = output.Replace("%CONSTANTS%", "");
                            output = output.Replace("%NESTEDCLASSES%", "");
                            output = output.Replace("%BULKINSERT%", "");
                            output = output.Replace("%UPSERT%", "");
                            output = output.Replace("%DELETEBYID%", "");
                            output = output.Replace("%DELETEBYENTITYID%", "");
                            output = output.Replace("%DELETEBYCRITERIA%", deleteByCriteriaMethods);
                            output = output.Replace("%GETDTO%", GetDalDtoProperties(schema, objectName, properties));
                            output = output.Replace("%PARAMETERS%", GetDalInsertUpdateParameters(objectName, properties, false));
                            output = output.Replace("%COLUMNS%", GetDalTempTableColumns(properties));
                            output = output.Replace("%FIELDCOUNT%", properties.Count.ToString());
                            output = output.Replace("%BULKCASES%", GetDalBulkCopyCases(properties));
                            output = output.Replace("%KEY%", GetMssqlDalDtoKey(properties));
                            output = output.Replace("%SETPROPERTIES%", GetSetPropertiesCode(objectName, properties));
                            break;
                        #endregion

                        #region VISTADBTABLE
                        //case "VISTADBTABLE":
                        //    output = File.ReadAllText(Path.Combine(_templatePath, _vistaDbTableTemplate));
                        //    output = output.Replace("%COLUMNS%", GetTableColumnScripts(schema, objectName, properties, true));
                        //    break;
                        #endregion

                        #region MSSQLTABLE
                        case "MSSQLTABLE":
                            output = File.ReadAllText(Path.Combine(_templatePath, _mssqlTableTemplate));
                            output = output.Replace("%COLUMNS%", GetTableColumnScripts(schema, objectName, properties, false));
                            break;
                        #endregion

                        #region VISTADBSTOREDPROCS
                        //case "VISTADBSTOREDPROCS":
                        //    output = "";
                        //    foreach (String storedProcedure in storedProcedures)
                        //    {
                        //        String storedProcOutput = "";
                        //        switch (schema)
                        //        {
                        //            case "MasterData":
                        //                switch (storedProcedure)
                        //                {
                        //                    case "FetchAll":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchAllStoredProcMasterDataTemplate));
                        //                        break;
                        //                    case "FetchById":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByIdStoredProcMasterDataTemplate));
                        //                        break;
                        //                    case "FetchByEntityId":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByEntityIdStoredProcMasterDataTemplate));
                        //                        break;
                        //                    case "Insert":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbInsertStoredProcMasterDataTemplate));
                        //                        storedProcOutput = storedProcOutput.Replace("%WHERE%", GetInsertKeyWhereScripts(objectName, properties));
                        //                        break;
                        //                    case "UpdateById":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbUpdateByIdStoredProcTemplate));
                        //                        break;
                        //                    case "DeleteById":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbDeleteByIdStoredProcMasterDataTemplate));
                        //                        break;
                        //                    case "DeleteByEntityId":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbDeleteByEntityIdStoredProcMasterDataTemplate));
                        //                        break;
                        //                    default:
                        //                        if (storedProcedure.StartsWith("FetchBy_"))
                        //                        {
                        //                            storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByCriteriaStoredProcMasterDataTemplate));
                        //                            List<String> fetchCriteria = GetFetchCriteria(storedProcedure);
                        //                            storedProcOutput = storedProcOutput.Replace("%METHODNAME%", GetFetchMethodName(fetchCriteria));
                        //                            storedProcOutput = storedProcOutput.Replace("%PARAMETERS%", GetFetchCriteriaScripts(objectName, properties, fetchCriteria, true));
                        //                            storedProcOutput = storedProcOutput.Replace("%WHERE%", GetFetchWhereScripts(objectName, properties, fetchCriteria));
                        //                        }
                        //                        break;
                        //                }
                        //                break;
                        //            default:
                        //                switch (storedProcedure)
                        //                {
                        //                    case "FetchAll":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchAllStoredProcTemplate));
                        //                        break;
                        //                    case "FetchById":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByIdStoredProcTemplate));
                        //                        break;
                        //                    case "FetchByEntityId":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByEntityIdStoredProcTemplate));
                        //                        break;
                        //                    case "Insert":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbInsertStoredProcTemplate));
                        //                        break;
                        //                    case "UpdateById":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbUpdateByIdStoredProcTemplate));
                        //                        break;
                        //                    case "DeleteById":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbDeleteByIdStoredProcTemplate));
                        //                        break;
                        //                    case "DeleteByEntityId":
                        //                        storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbDeleteByEntityIdStoredProcTemplate));
                        //                        break;
                        //                    default:
                        //                        if (storedProcedure.StartsWith("FetchBy_"))
                        //                        {
                        //                            storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbFetchByCriteriaStoredProcTemplate));
                        //                            List<String> fetchCriteria = GetFetchCriteria(storedProcedure);
                        //                            storedProcOutput = storedProcOutput.Replace("%METHODNAME%", GetFetchMethodName(fetchCriteria));
                        //                            storedProcOutput = storedProcOutput.Replace("%PARAMETERS%", GetFetchCriteriaScripts(objectName, properties, fetchCriteria, true));
                        //                            storedProcOutput = storedProcOutput.Replace("%WHERE%", GetFetchWhereScripts(objectName, properties, fetchCriteria));
                        //                        }
                        //                        break;
                        //                }
                        //                break;
                        //        }
                        //        output = String.Format("{0}{1}{2}", output, Environment.NewLine, storedProcOutput);
                        //    }
                        //    output = output.Replace("%SELECT%", GetSelectColumnScripts(schema, objectName, properties));
                        //    output = output.Replace("%CRITERIA%", GetInsertUpdateCriteriaScripts(objectName, properties, true));
                        //    output = output.Replace("%INSERT%", GetInsertColumnScripts(objectName, properties));
                        //    output = output.Replace("%VALUES%", GetValuesScripts(objectName, properties));
                        //    output = output.Replace("%SET%", GetSetColumnScripts(objectName, properties));
                        //    break;
                        #endregion

                        #region MSSQLSTOREDPROCS
                        case "MSSQLSTOREDPROCS":
                            output = "";
                            foreach (String storedProcedure in storedProcedures)
                            {
                                String storedProcOutput = "";
                                switch (schema)
                                {
                                    case "MasterData":
                                        switch (storedProcedure)
                                        {
                                            case "FetchAll":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchAllStoredProcMasterDataTemplate));
                                                break;
                                            case "FetchById":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByIdStoredProcMasterDataTemplate));
                                                break;
                                            case "FetchByEntityId":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByEntityIdStoredProcMasterDataTemplate));
                                                break;
                                            case "Insert":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlInsertStoredProcMasterDataTemplate));
                                                storedProcOutput = storedProcOutput.Replace("%WHERE%", GetInsertKeyWhereScripts(objectName, properties));
                                                break;
                                            case "UpdateById":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlUpdateByIdStoredProcMasterDataTemplate));
                                                break;
                                            case "BulkUpsert":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlBulkUpsertStoredProcTemplate));
                                                break;
                                            case "DeleteById":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByIdStoredProcMasterDataTemplate));
                                                break;
                                            case "DeleteByEntityId":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByEntityIdStoredProcMasterDataTemplate));
                                                break;
                                            default:
                                                if (storedProcedure.StartsWith("FetchBy_"))
                                                {
                                                    storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByCriteriaStoredProcMasterDataTemplate));
                                                    List<String> fetchCriteria = GetFetchCriteria(storedProcedure);
                                                    storedProcOutput = storedProcOutput.Replace("%METHODNAME%", GetFetchMethodName(fetchCriteria));
                                                    storedProcOutput = storedProcOutput.Replace("%PARAMETERS%", GetFetchCriteriaScripts(objectName, properties, fetchCriteria, false));
                                                    storedProcOutput = storedProcOutput.Replace("%WHERE%", GetFetchWhereScripts(objectName, properties, fetchCriteria));
                                                }
                                                else if (storedProcedure.StartsWith("DeleteBy_"))
                                                {
                                                    storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByCriteriaStoredProcMasterDataTemplate));
                                                    List<String> deleteCriteria = GetDeleteCriteria(storedProcedure);
                                                    storedProcOutput = storedProcOutput.Replace("%METHODNAME%", GetDeleteMethodName(deleteCriteria));
                                                    storedProcOutput = storedProcOutput.Replace("%PARAMETERS%", GetFetchCriteriaScripts(objectName, properties, deleteCriteria, false));
                                                    storedProcOutput = storedProcOutput.Replace("%WHERE%", GetFetchWhereScripts(objectName, properties, deleteCriteria));
                                                }
                                                break;
                                        }
                                        break;
                                    default:
                                        switch (storedProcedure)
                                        {
                                            case "FetchAll":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchAllStoredProcTemplate));
                                                break;
                                            case "FetchById":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByIdStoredProcTemplate));
                                                break;
                                            case "FetchByEntityId":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByEntityIdStoredProcTemplate));
                                                break;
                                            case "Insert":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlInsertStoredProcTemplate));
                                                break;
                                            case "BulkInsert":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlBulkInsertStoredProcTemplate));
                                                break;
                                            case "UpdateById":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlUpdateByIdStoredProcTemplate));
                                                break;
                                            case "DeleteById":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByIdStoredProcTemplate));
                                                break;
                                            case "DeleteByEntityId":
                                                storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByEntityIdStoredProcTemplate));
                                                break;
                                            default:
                                                if (storedProcedure.StartsWith("FetchBy_"))
                                                {
                                                    storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlFetchByCriteriaStoredProcTemplate));
                                                    List<String> fetchCriteria = GetFetchCriteria(storedProcedure);
                                                    storedProcOutput = storedProcOutput.Replace("%METHODNAME%", GetFetchMethodName(fetchCriteria));
                                                    storedProcOutput = storedProcOutput.Replace("%PARAMETERS%", GetFetchCriteriaScripts(objectName, properties, fetchCriteria, false));
                                                    storedProcOutput = storedProcOutput.Replace("%WHERE%", GetFetchWhereScripts(objectName, properties, fetchCriteria));
                                                }
                                                else if (storedProcedure.StartsWith("DeleteBy_"))
                                                {
                                                    storedProcOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlDeleteByCriteriaStoredProcTemplate));
                                                    List<String> deleteCriteria = GetDeleteCriteria(storedProcedure);
                                                    storedProcOutput = storedProcOutput.Replace("%METHODNAME%", GetDeleteMethodName(deleteCriteria));
                                                    storedProcOutput = storedProcOutput.Replace("%PARAMETERS%", GetFetchCriteriaScripts(objectName, properties, deleteCriteria, false));
                                                    storedProcOutput = storedProcOutput.Replace("%WHERE%", GetFetchWhereScripts(objectName, properties, deleteCriteria));
                                                }
                                                break;
                                        }
                                        break;
                                }
                                output = String.Format("{0}{1}{2}", output, Environment.NewLine, storedProcOutput);
                            }
                            output = output.Replace("%SELECT%", GetSelectColumnScripts(schema, objectName, properties));
                            output = output.Replace("%CRITERIA%", GetInsertUpdateCriteriaScripts(objectName, properties, false));
                            output = output.Replace("%INSERT%", GetInsertColumnScripts(objectName, properties));
                            output = output.Replace("%VALUES%", GetValuesScripts(objectName, properties));
                            output = output.Replace("%SET%", GetSetColumnScripts(objectName, properties));
                            output = output.Replace("%SETPROPERTIES%", GetSetPropertiesScripts(objectName, properties));
                            output = output.Replace("%BULKSET%", GetBulkSetColumnScripts(objectName, properties));
                            output = output.Replace("%KEYDEF%", GetKeyDefScripts(objectName, properties));
                            output = output.Replace("%KEYINSERTED%", GetKeyInsertedScripts(objectName, properties));
                            if (properties.Count(p => p.IsKey) > 0)
                            {
                                output = output.Replace("%KEYJOIN%", GetKeyJoinScripts(schema, objectName, properties));
                                output = output.Replace("%KEYWHERE%", GetBulkUpsertKeyWhereScripts(objectName, properties));
                            }
                            //if (properties.Count(p => p.IsFixedKey) > 0)
                            //{
                            //    output = output.Replace("%FIXEDKEYJOIN%", GetFixedKeyJoinScripts(schema, objectName, properties));
                            //    output = output.Replace("%FIXEDKEYWHERE%", GetFixedKeyWhereScripts(objectName, properties));
                            //}
                            break;
                        #endregion

                        #region VISTADBKEYS
                        //case "VISTADBKEYS":
                        //    output = "";
                        //    if (properties.Count(p => p.IsKey) > 0)
                        //    {
                        //        output = File.ReadAllText(Path.Combine(_templatePath, _vistaDbUniqueIndexTemplate));
                        //        output = output.Replace("%COLUMNSNAME%", GetKeyColumnsNameScript(properties));
                        //        output = output.Replace("%COLUMNS%", GetKeyColumnsScript(properties));
                        //    }
                        //    foreach (ObjectProperty property in properties.Where(p => p.DbName.EndsWith("_Id")))
                        //    {
                        //        String foreignKeyIndexOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbForeignKeyIndexTemplate));
                        //        foreignKeyIndexOutput = foreignKeyIndexOutput.Replace("%PARENT%", property.DbName.Substring(0, property.DbName.IndexOf('_')));
                        //        output = String.Format("{0}{1}{2}", output, Environment.NewLine, foreignKeyIndexOutput);
                        //    }
                        //    foreach (ObjectProperty property in properties.Where(p => p.DbName.EndsWith("_Id")))
                        //    {
                        //        String foreignKeyOutput = File.ReadAllText(Path.Combine(_templatePath, _vistaDbForeignKeyTemplate));
                        //        foreignKeyOutput = foreignKeyOutput.Replace("%PARENT%", property.DbName.Substring(0, property.DbName.IndexOf('_')));
                        //        output = String.Format("{0}{1}{2}", output, Environment.NewLine, foreignKeyOutput);
                        //    }
                        //    break;
                        #endregion

                        #region MSSQLKEYS
                        case "MSSQLKEYS":
                            output = "";
                            if (properties.Count(p => p.IsKey) > 0)
                            {
                                output = File.ReadAllText(Path.Combine(_templatePath, _mssqlUniqueIndexTemplate));
                                output = output.Replace("%COLUMNSNAME%", GetKeyColumnsNameScript(properties));
                                output = output.Replace("%COLUMNS%", GetKeyColumnsScript(properties));
                            }
                            foreach (ObjectProperty property in properties.Where(p => p.DbName.EndsWith("_Id")))
                            {
                                String foreignKeyIndexOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlForeignKeyIndexTemplate));
                                foreignKeyIndexOutput = foreignKeyIndexOutput.Replace("%PARENT%", property.DbName.Substring(0, property.DbName.IndexOf('_')));
                                output = String.Format("{0}{1}{2}", output, Environment.NewLine, foreignKeyIndexOutput);
                            }
                            foreach (ObjectProperty property in properties.Where(p => p.DbName.EndsWith("_Id")))
                            {
                                String foreignKeyOutput = File.ReadAllText(Path.Combine(_templatePath, _mssqlForeignKeyTemplate));
                                foreignKeyOutput = foreignKeyOutput.Replace("%PARENT%", property.DbName.Substring(0, property.DbName.IndexOf('_')));
                                output = String.Format("{0}{1}{2}", output, Environment.NewLine, foreignKeyOutput);
                            }
                            break;
                        #endregion

                        #region MODELROOTOBJECT
                        case "MODELROOTOBJECT":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _modelRootObjectTemplate));
                            output = output.Replace("%MODELPROPERTIES%", GetModelProperties(schema, properties));
                            output = output.Replace("%MODELBUSINESSRULES%", GetModelBusinessRules(properties));
                            break;
                        #endregion

                        #region MODELROOTOBJECTSERVER
                        case "MODELROOTOBJECTSERVER":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _modelRootObjectServerTemplate));
                            output = output.Replace("%MODELGETDTOPROPERTIES%", GetModelGetDtoProperties(schema, properties));
                            output = output.Replace("%MODELLOADDTOPROPERTIES%", GetModelLoadDtoProperties(schema, properties));
                            break;
                        #endregion

                        #region MODELCHILDOBJECT
                        case "MODELCHILDOBJECT":
                            {
                                output = "";
                                output = File.ReadAllText(Path.Combine(_templatePath, _modelChildObjectTemplate));
                                output = output.Replace("%MODELPROPERTIES%", GetModelProperties(schema, properties));
                                output = output.Replace("%MODELBUSINESSRULES%", GetModelBusinessRules(properties));
                            }
                            break;
                        #endregion

                        #region MODELCHILDOBJECTSERVER
                        case "MODELCHILDOBJECTSERVER":
                            {
                                output = "";
                                output = File.ReadAllText(Path.Combine(_templatePath, _modelChildObjectServerTemplate));
                                output = output.Replace("%MODELGETDTOPROPERTIES%", GetModelGetDtoProperties(schema, properties));
                                output = output.Replace("%MODELLOADDTOPROPERTIES%", GetModelLoadDtoProperties(schema, properties));
                            }
                            break;
                        #endregion

                        #region MODELCHILDLIST
                        case "MODELCHILDLIST":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _modelChildListTemplate));
                            break;
                        #endregion

                        #region MODELCHILDLISTSERVER
                        case "MODELCHILDLISTSERVER":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _modelChildListServerTemplate));
                            break;
                        #endregion

                        #region MODELINFOOBJECT
                        case "MODELINFOOBJECT":
                            {
                                output = "";
                                output = File.ReadAllText(Path.Combine(_templatePath, _modelInfoObjectTemplate));
                                output = output.Replace("%MODELINFOPROPERTIES%", GetModelInfoProperties(schema, properties));
                                output = output.Replace("%MODELBUSINESSRULES%", GetModelBusinessRules(properties));
                            }
                            break;
                        #endregion

                        #region MODELINFOOBJECTSERVER
                        case "MODELINFOOBJECTSERVER":
                            {
                                output = "";
                                output = File.ReadAllText(Path.Combine(_templatePath, _modelInfoObjectServerTemplate));
                                output = output.Replace("%MODELLOADDTOPROPERTIES%", GetModelInfoLoadDtoProperties(schema, properties));
                            }
                            break;
                        #endregion

                        #region MODELINFOLIST
                        case "MODELINFOLIST":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _modelInfoListTemplate));
                            break;
                        #endregion

                        #region MODELINFOLISTSERVER
                        case "MODELINFOLISTSERVER":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _modelInfoListServerTemplate));
                            break;
                        #endregion

                        #region MAINTENANCEVIEWMODEL
                        case "MAINTENANCEVIEWMODEL":
                            output = "";
                            output = File.ReadAllText(Path.Combine(_templatePath, _maintenanceViewModelTemplate));
                            break;
                        #endregion

                        default:
                            break;
                    }

                    output = output.Replace("%SCHEMA%", schema);
                    output = output.Replace("%VERSION%", version);
                    output = output.Replace("%GEMINI%", gemini);
                    output = output.Replace("%DEVELOPER%", developer);
                    output = output.Replace("%OBJECT%", objectName);
                    output = output.Replace("%OBJECTPARENT%", parentName);

                    if (!Directory.Exists(objectName))
                    {
                        Directory.CreateDirectory(objectName);
                    }
                    File.WriteAllText(String.Format("{0}/{1}Output.txt", objectName, outputType), output);
                }
            }
        }

        private static List<String> GetFetchCriteria(String fetchSignature)
        {
            List<String> returnValue = new List<String>();
            Char[] delim = { '_' };
            String[] tokens = fetchSignature.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            for (Int32 i = 1; i < tokens.Length; i++)
            {
                returnValue.Add(tokens[i]);
            }
            return returnValue;
        }

        private static String GetFetchMethodName(List<String> fetchCriteria)
        {
            StringBuilder returnValue = new StringBuilder();
            returnValue.Append("FetchBy");
            foreach (String criterion in fetchCriteria)
            {
                returnValue.Append(criterion);
            }
            return returnValue.ToString();
        }

        private static String GetFetchMethodArguments(List<String> fetchCriteria, List<ObjectProperty> properties)
        {
            StringBuilder returnValue = new StringBuilder();
            for (Int32 i = 0; i < fetchCriteria.Count; i++)
            {
                if (i > 0)
                {
                    returnValue.Append(", ");
                }
                ObjectProperty property = properties.First(p => p.CodeName == fetchCriteria[i]);
                returnValue.Append(property.CodeType);
                returnValue.Append(" ");
                returnValue.Append(property.CodeName.Substring(0, 1).ToLowerInvariant());
                returnValue.Append(property.CodeName.Substring(1));
            }
            return returnValue.ToString();
        }

        private static List<String> GetDeleteCriteria(String deleteSignature)
        {
            List<String> returnValue = new List<String>();
            Char[] delim = { '_' };
            String[] tokens = deleteSignature.Split(delim, StringSplitOptions.RemoveEmptyEntries);
            for (Int32 i = 1; i < tokens.Length; i++)
            {
                returnValue.Add(tokens[i]);
            }
            return returnValue;
        }

        private static String GetDeleteMethodName(List<String> deleteCriteria)
        {
            StringBuilder returnValue = new StringBuilder();
            returnValue.Append("DeleteBy");
            foreach (String criterion in deleteCriteria)
            {
                returnValue.Append(criterion);
            }
            return returnValue.ToString();
        }

        #region Private Methods

        #region Object Definition Interpretation

        private static String GetObjectSchema(String objectDefinitionFile)
        {
            String returnValue = null;
            bool start = false;
            foreach (String line in File.ReadAllLines(objectDefinitionFile))
            {
                if (start)
                {
                    returnValue = line.Trim();
                    break;
                }
                if (line.Trim() == "Schema")
                {
                    start = true;
                }
            }
            return returnValue;
        }

        private static IEnumerable<String> GetStoredProcedures(String objectDefinitionFile)
        {
            bool start = false;
            foreach (String line in File.ReadAllLines(objectDefinitionFile))
            {
                if (start)
                {
                    if (String.IsNullOrWhiteSpace(line))
                    {
                        break;
                    }
                    yield return line.Trim();
                }
                else if (line.Trim() == "Stored Procedures")
                {
                    start = true;
                }
            }
        }

        private static IEnumerable<ObjectProperty> GetObjectProperties(String objectName, String objectDefinitionFile)
        {
            bool start = false;
            foreach (String line in File.ReadAllLines(objectDefinitionFile))
            {
                if (start)
                {
                    if (String.IsNullOrEmpty(line))
                    {
                        break;
                    }

                    Char[] delim = { ',' };
                    String[] tokens = line.Split(delim);
                    ObjectProperty returnValue = new ObjectProperty();
                    returnValue.CodeName = tokens[0].Replace("_", "").Trim();
                    if (tokens[0].Contains('_'))
                    {
                        returnValue.DbName = tokens[0].Trim();
                    }
                    else
                    {
                        returnValue.DbName = String.Format("{0}_{1}", objectName, tokens[0].Trim());
                    }
                    returnValue.MssqlDbType = tokens[1].ToUpperInvariant().Trim();
                    returnValue.VistaDbDbType = returnValue.MssqlDbType;
                    if (returnValue.VistaDbDbType == "[DATE]")
                    {
                        returnValue.VistaDbDbType = "[SMALLDATETIME]";
                    }
                    if (returnValue.VistaDbDbType == "[SQL_VARIANT]")
                    {
                        returnValue.VistaDbDbType = "[VARBINARY]";
                    }
                    returnValue.Nullability = tokens[2].ToUpperInvariant().Trim();
                    returnValue.MssqlAdoType = GetAdoType(returnValue.MssqlDbType, false);
                    returnValue.VistaDbAdoType = GetAdoType(returnValue.VistaDbDbType, true);
                    returnValue.CodeType = GetCodeType(returnValue.MssqlDbType, returnValue.Nullability);
                    returnValue.BaseCodeType = GetBaseCodeType(returnValue.MssqlDbType);


                    if (tokens.Length > 3)
                    {
                        for(Int32 i = 3; i<= tokens.Length-1; i++)
                        {
                            switch (tokens[i].ToUpperInvariant().Trim())
                            {
                                case "KEY":
                                    returnValue.IsKey = true;
                                    break;
                                case "PARENTID":
                                    returnValue.IsParentId = true;
                                    break;
                                case "FIXEDKEY":
                                    returnValue.IsKey = true;
                                    //returnValue.IsFixedKey = true;
                                    break;
                                case "INFO":
                                    returnValue.IsInInfo = true;
                                    break;
                            }
                        }
                    }
                    yield return returnValue;
                }
                else if (line.Trim() == "Properties")
                {
                    start = true;
                }
            }
        }

        private static String GetCodeType(String type, String nullability)
        {
            String returnValue = "";
            String trimmedType = type.Replace("[", "");
            trimmedType = trimmedType.Replace("]", "");
            if (trimmedType.Contains("CHAR"))
            {
                returnValue = "String";
            }
            else if (trimmedType == "SQL_VARIANT")
            {
                returnValue = "Byte[]";
            }
            else
            {
                switch (trimmedType)
                {
                    case "TINYINT":
                        returnValue = "Byte";
                        break;
                    case "SMALLINT":
                        returnValue = "Int16";
                        break;
                    case "INT":
                        returnValue = "Int32";
                        break;
                    case "BIGINT":
                        returnValue = "Int64";
                        break;
                    case "BIT":
                        returnValue = "Boolean";
                        break;
                    case "REAL":
                        returnValue = "Single";
                        break;
                    case "FLOAT":
                        returnValue = "Double";
                        break;
                    case "SMALLDATETIME":
                    case "DATETIME":
                    case "DATE":
                        returnValue = "DateTime";
                        break;
                    case "UNIQUEIDENTIFIER":
                        returnValue = "Guid";
                        break;
                    case "OBJECT":
                        returnValue = "Object";
                        break;
                }
                if (nullability == "NULL")
                {
                    returnValue += "?";
                }
            }
            return returnValue;
        }

        private static String GetBaseCodeType(String type)
        {
            String returnValue = "";
            String trimmedType = type.Replace("[", "");
            trimmedType = trimmedType.Replace("]", "");
            if (trimmedType.Contains("CHAR"))
            {
                returnValue = "String";
            }
            else if (trimmedType == "SQL_VARIANT")
            {
                returnValue = "Byte[]";
            }
            else
            {
                switch (trimmedType)
                {
                    case "TINYINT":
                        returnValue = "Byte";
                        break;
                    case "SMALLINT":
                        returnValue = "Int16";
                        break;
                    case "INT":
                        returnValue = "Int32";
                        break;
                    case "BIGINT":
                        returnValue = "Int64";
                        break;
                    case "BIT":
                        returnValue = "Boolean";
                        break;
                    case "REAL":
                        returnValue = "Single";
                        break;
                    case "FLOAT":
                        returnValue = "Double";
                        break;
                    case "SMALLDATETIME":
                    case "DATETIME":
                    case "DATE":
                        returnValue = "DateTime";
                        break;
                    case "UNIQUEIDENTIFIER":
                        returnValue = "Guid";
                        break;
                    case "OBJECT":
                        returnValue = "Object";
                        break;
                }
            }
            return returnValue;
        }

        private static String GetAdoType(String type, Boolean vistaDb)
        {
            String returnValue = "";
            String trimmedType = type.Replace("[", "");
            trimmedType = trimmedType.Replace("]", "");
            if (trimmedType.Contains("NVARCHAR"))
            {
                returnValue = "NVarChar";
            }
            else if (trimmedType.Contains("VARCHAR"))
            {
                returnValue = "VarChar";
            }
            else
            {
                switch (trimmedType)
                {
                    case "TINYINT":
                        returnValue = "TinyInt";
                        break;
                    case "SMALLINT":
                        returnValue = "SmallInt";
                        break;
                    case "INT":
                        returnValue = "Int";
                        break;
                    case "BIGINT":
                        returnValue = "BigInt";
                        break;
                    case "BIT":
                        returnValue = "Bit";
                        break;
                    case "REAL":
                        returnValue = "Real";
                        break;
                    case "FLOAT":
                        returnValue = "Float";
                        break;
                    case "SMALLDATETIME":
                        returnValue = "SmallDateTime";
                        break;
                    case "DATETIME":
                        returnValue = "DateTime";
                        break;
                    case "DATE":
                        returnValue = "Date";
                        break;
                    case "SQL_VARIANT":
                        returnValue = "Variant";
                        break;
                    case "VARBINARY":
                        returnValue = "VarBinary";
                        break;
                    case "UNIQUEIDENTIFIER":
                        returnValue = "UniqueIdentifier";
                        break;
                    case "OBJECT":
                        returnValue = "Object";
                        break;
                }
            }
            if (vistaDb)
            {
                returnValue = String.Format("VistaDBType.{0}", returnValue);
            }
            else
            {
                returnValue = String.Format("SqlDbType.{0}", returnValue);
            }
            return returnValue;
        }

        #endregion

        #region Dto-Related Methods

        private static String GetDtoProperties(
            String schema,
            IEnumerable<ObjectProperty> properties,
            Boolean keyPropertiesOnly)
        {
            String pattern = "public {0} {1} {{ get; set; }}";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                if ((!keyPropertiesOnly) || (property.IsKey))
                {
                    resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }
            if ((schema == "MasterData") && (!keyPropertiesOnly)
                && !properties.Any(p=> p.IsParentId))
            {
                resolvedPattern = String.Format(pattern, "DateTime", "DateCreated");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateTime", "DateLastModified");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateTime?", "DateDeleted");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetIsSetProperties(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "public Boolean Is{0}Set {{ get; set; }}";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDtoEqualsOverrides(
            String schema,
            IEnumerable<ObjectProperty> properties,
            Boolean keyPropertiesOnly)
        {
            String pattern = "if(other.{0} != this.{0}) {{ return false; }}";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                if ((!keyPropertiesOnly) || (property.IsKey))
                {
                    if (property.CodeType == "Object")
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine();
                        sb.AppendLine(String.Format("if ((other.{0} != null) && (this.{0} != null))", property.CodeName));
                        sb.AppendLine("{");
                        sb.AppendLine(String.Format("if (!other.{0}.Equals(this.{0})) {{ return false; }}", property.CodeName));
                        sb.AppendLine("}");
                        sb.AppendLine(String.Format("if ((other.{0} != null) && (this.{0} == null)) {{ return false; }}", property.CodeName));
                        sb.AppendLine(String.Format("if ((other.{0} == null) && (this.{0} != null)) {{ return false; }}", property.CodeName));

                        resolvedPattern = sb.ToString();
                    }
                    else
                    {
                        resolvedPattern = String.Format(pattern, property.CodeName);
                    }
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            if ((schema == "MasterData") && (!keyPropertiesOnly))
            {
                resolvedPattern = String.Format(pattern, "DateCreated");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateLastModified");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateDeleted");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetIsSetEqualsOverrides(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "if(other.Is{0}Set != this.Is{0}Set) {{ return false; }}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetIsSetSet(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "Is{0}Set = isSet;";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDtoKeyInit(IEnumerable<ObjectProperty> properties)
        {
            if (properties == null)
            {
                return "Id = this.Id,";
            }
            else
            {
                String pattern = "{0} = this.{0},";

                String finalString = String.Empty;
                foreach (ObjectProperty property in properties)
                {
                    if (property.IsKey)
                    {
                        String resolvedPattern = String.Format(pattern, property.CodeName);
                        finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                    }
                }

                return finalString.Substring(0, finalString.Length - 1); // Remove trailing comma.
            }
        }

        private static String GetDtoKeyHash(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "{0}.GetHashCode() + ";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeName);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString.Substring(0, finalString.Length - 3); // Remove trailing " + ".
        }

        private static String GetIsSetHash(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "Is{0}Set.GetHashCode() + ";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 3); // Remove trailing " + ".
        }

        #endregion

        #region Dal-Related Methods

        private static String GetColumnConstants(
            String schema,
            String objectName,
            IEnumerable<ObjectProperty> properties)
        {
            String pattern = "public const String {0}{1} = \"{2}\";";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                resolvedPattern = String.Format(pattern, objectName, property.CodeName, property.DbName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }
            if (schema == "MasterData")
            {
                resolvedPattern = String.Format(pattern, objectName, "DateCreated", objectName + "_DateCreated");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName, "DateLastModified", objectName + "_DateLastModified");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName, "DateDeleted", objectName + "_DateDeleted");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName, "SetProperties", objectName + "_SetProperties");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetStoredProcedureConstants(String objectName, String schema, IEnumerable<String> storedProcedures, bool includeSchema)
        {
            String pattern = "public const String {0}{1} = \"{0}_{1}\";";
            if (includeSchema)
            {
                pattern = "public const String {0}{1} = \"{2}.{0}_{1}\";";
            }

            String finalString = String.Empty;
            foreach (String storedProcedure in storedProcedures)
            {
                String resolvedStoredProcedure = storedProcedure.Replace("FetchBy_", "FetchBy");
                resolvedStoredProcedure = resolvedStoredProcedure.Replace("DeleteBy_", "DeleteBy");
                String resolvedPattern = String.Format(pattern, objectName, resolvedStoredProcedure, schema);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetIDalMethods(String objectName, IEnumerable<String> storedProcedures, List<ObjectProperty> properties)
        {
            String fetchAllPattern = "IEnumerable<{0}Dto> FetchAll();";
            String fetchByIdPattern = "{0}Dto FetchById(Int32 id);";
            String fetchByEntityIdPattern = "IEnumerable<{0}Dto> FetchByEntityId(Int32 entityId);";
            String fetchByCriteriaPattern = "IEnumerable<{0}Dto> {1}({2});";
            String insertPattern = "void Insert({0}Dto dto);";
            String bulkInsertPattern = "void Insert(IEnumerable<{0}Dto> dtoList);";
            String updateByIdPattern = "void Update({0}Dto dto);";
            String bulkUpsertPattern = "void Upsert(IEnumerable<{0}Dto> dtoList, {0}IsSetDto isSetDto);";
            String deleteByIdPattern = "void DeleteById(Int32 id);";
            String deleteByEntityIdPattern = "void DeleteByEntityId(Int32 entityId);";
            String deleteByCriteriaPattern = "void {3}({4});";

            String finalString = String.Empty;
            foreach (String storedProcedure in storedProcedures)
            {
                List<String> fetchCriteria = new List<String>();
                List<String> deleteCriteria = new List<String>();
                String resolvedPattern = storedProcedure;
                if (resolvedPattern.StartsWith("FetchBy_"))
                {
                    fetchCriteria = GetFetchCriteria(resolvedPattern);
                    resolvedPattern = fetchByCriteriaPattern;
                }
                else if (resolvedPattern.StartsWith("DeleteBy_"))
                {
                    deleteCriteria = GetDeleteCriteria(resolvedPattern);
                    resolvedPattern = deleteByCriteriaPattern;
                }
                switch (storedProcedure)
                {
                    case "FetchAll":
                        resolvedPattern = fetchAllPattern;
                        break;
                    case "FetchById":
                        resolvedPattern = fetchByIdPattern;
                        break;
                    case "FetchByEntityId":
                        resolvedPattern = fetchByEntityIdPattern;
                        break;
                    case "Insert":
                        resolvedPattern = insertPattern;
                        break;
                    case "BulkInsert":
                        resolvedPattern = bulkInsertPattern;
                        break;
                    case "UpdateById":
                        resolvedPattern = updateByIdPattern;
                        break;
                    case "BulkUpsert":
                        resolvedPattern = bulkUpsertPattern;
                        break;
                    case "DeleteById":
                        resolvedPattern = deleteByIdPattern;
                        break;
                    case "DeleteByEntityId":
                        resolvedPattern = deleteByEntityIdPattern;
                        break;
                }
                resolvedPattern = String.Format(resolvedPattern, objectName,
                    GetFetchMethodName(fetchCriteria), GetFetchMethodArguments(fetchCriteria, properties),
                    GetDeleteMethodName(deleteCriteria), GetFetchMethodArguments(deleteCriteria, properties));
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDalDtoProperties(
            String schema,
            String objectName,
            IEnumerable<ObjectProperty> properties)
        {
            String pattern = "{1} = ({0})GetValue(dr[FieldNames.{2}{1}])";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName, objectName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }
            if (schema == "MasterData")
            {
                resolvedPattern = String.Format(pattern, "DateTime", "DateCreated", objectName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateTime", "DateLastModified", objectName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, "DateTime?", "DateDeleted", objectName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDalInsertUpdateParameters(String objectName, IEnumerable<ObjectProperty> properties, Boolean vistaDb)
        {
            String pattern = "CreateParameter(command, FieldNames.{0}{1}, {2}, dto.{1});";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, objectName, property.CodeName, vistaDb ? property.VistaDbAdoType : property.MssqlAdoType);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDalUpdateDda(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String nonNullablePattern = "if (isInsert || isSetDto.Is{2}Set || (!table.Get(FieldNames.{1}DateDeleted).IsNull)){0}" +
                "{{{0}" +
                "table.Put{3}(FieldNames.{1}{2}, dto.{2});" +
                "}}";
            String nullablePattern = "if (isInsert || isSetDto.Is{2}Set || (!table.Get(FieldNames.{1}DateDeleted).IsNull)){0}" +
                "if (dto.{3} != null){0}" +
                "{{{0}" +
                "table.Put{4}(FieldNames.{2}{3}, ({4})dto.{3});{0}" +
                "}}" +
                "else{0}" +
                "{{{0}" +
                "table.PutNull(FieldNames.{2}{3});{0}" +
                "}}" +
                "}}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = "";
                if (property.Nullability == "NOT NULL")
                {
                    resolvedPattern = String.Format(nonNullablePattern, property.CodeType, objectName, property.CodeName, property.BaseCodeType);
                }
                else
                {
                    resolvedPattern = String.Format(nullablePattern, Environment.NewLine, property.CodeType, objectName, property.CodeName, property.BaseCodeType);
                }
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetDalTempTableColumns(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		\"[{0}] {1}, \" +";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.DbName, property.MssqlDbType);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString = finalString.Substring(0, finalString.Length - 5) +
                finalString.Substring(finalString.Length - 4); // Remove trailing comma.
        }

        private static String GetDalBulkCopyCases(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "case {1}:{0}value = _enumerator.Current.{2};{0}break;";

            String finalString = String.Empty;
            Int32 i = 0;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, Environment.NewLine, i, property.CodeName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                i++;
            }

            return finalString;
        }

        private static String GetMssqlDalDtoKey(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "key.{0} = ({1})GetValue(dr[FieldNames.%OBJECT%{0}]);";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeName, property.CodeType);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString;
        }

        private static String GetSetPropertiesCode(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern1 = "if (isSetDto.Is{0}Set)";
            String pattern2 = "{";
            String pattern3 = "setProperties = String.Format(\"{{0}}#{{1}}#\", setProperties, FieldNames.{1}{0});";
            String pattern4 = "}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {

                String resolvedPattern1 = String.Format(pattern1, property.CodeName);
                String resolvedPattern3 = String.Format(pattern3, property.CodeName, objectName);
                finalString = String.Format(
                    "{0} {1} {2} {3} {2} {4} {2} {5} {2}",
                    finalString,
                    resolvedPattern1,
                    Environment.NewLine,
                    pattern2,
                    resolvedPattern3,
                    pattern4);

            }

            return finalString;
        }

        private static String GetVistaDbDalDtoKey(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "key.{0} = ({1})table.Get(FieldNames.{2}{0}).Value;";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeName, property.CodeType, objectName);
                    finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString;
        }

        private static String GetDalFetchParameters(
            String objectName,
            IEnumerable<ObjectProperty> properties,
            List<String> fetchCriteria,
            Boolean vistaDb)
        {
            String pattern = "CreateParameter(command, FieldNames.{0}{1}, {2}, {3});";

            String finalString = String.Empty;
            foreach (String criterion in fetchCriteria)
            {
                ObjectProperty property = properties.First(p => p.CodeName == criterion);
                String resolvedPattern = String.Format(pattern, objectName, property.CodeName, vistaDb ? property.VistaDbAdoType : property.MssqlAdoType, FirstLetterToLowercase(property.CodeName));
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String FirstLetterToLowercase(String input)
        {
            return String.Format("{0}{1}", input.Substring(0, 1).ToLowerInvariant(), input.Substring(1));
        }

        #endregion

        #region DB Script-Related Methods

        #region Tables

        private static String GetTableColumnScripts(
            String schema,
            String objectName,
            IEnumerable<ObjectProperty> properties,
            Boolean vistaDb)
        {
            String pattern = "		[{0}] {1} {2},";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                resolvedPattern = String.Format(pattern, property.DbName, vistaDb ? property.VistaDbDbType : property.MssqlDbType, property.Nullability);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }
            if (schema == "MasterData")
            {
                resolvedPattern = String.Format(pattern, objectName + "_DateCreated", "[SMALLDATETIME]", "NOT NULL");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName + "_DateLastModified", "[SMALLDATETIME]", "NOT NULL");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName + "_DateDeleted", "[SMALLDATETIME]", "NULL");
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        #endregion

        #region Stored Procedures

        private static String GetSelectColumnScripts(
            String schema,
            String objectName,
            IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		[{0}].[{1}]";

            String finalString = String.Empty;
            String resolvedPattern = null;
            foreach (ObjectProperty property in properties)
            {
                resolvedPattern = String.Format(pattern, objectName, property.DbName);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }
            if (schema == "MasterData")
            {
                resolvedPattern = String.Format(pattern, objectName, objectName + "_DateCreated");
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName, objectName + "_DateLastModified");
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                resolvedPattern = String.Format(pattern, objectName, objectName + "_DateDeleted");
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetInsertUpdateCriteriaScripts(String objectName, IEnumerable<ObjectProperty> properties, Boolean vistaDb)
        {
            String pattern = "	@{0} {1}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.DbName, vistaDb ? property.VistaDbDbType : property.MssqlDbType);
                finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString;
        }

        private static String GetFetchCriteriaScripts(
            String objectName,
            IEnumerable<ObjectProperty> properties,
            List<String> fetchCriteria,
            Boolean vistaDb)
        {
            String pattern = "	@{0} {1},";

            String finalString = String.Empty;
            foreach (String criterion in fetchCriteria)
            {
                ObjectProperty property = properties.First(p => p.CodeName == criterion);
                String resolvedPattern = String.Format(pattern, property.DbName, vistaDb ? property.VistaDbDbType : property.VistaDbDbType);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 1);
        }

        private static String GetFetchWhereScripts(
            String objectName,
            IEnumerable<ObjectProperty> properties,
            List<String> fetchCriteria)
        {
            String pattern = "	        [{0}] = @{0} AND";

            String finalString = String.Empty;
            foreach (String criterion in fetchCriteria)
            {
                ObjectProperty property = properties.First(p => p.CodeName == criterion);
                String resolvedPattern = String.Format(pattern, property.DbName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 4);
        }

        private static String GetInsertColumnScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		[{0}]";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.DbName);
                finalString = String.Format("{0} {1} {2},", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 1); // (Remove trailing comma)
        }

        private static String GetValuesScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		@{0}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.DbName);
                finalString = String.Format("{0} {1} {2},", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 1); // (Remove trailing comma)
        }

        private static String GetSetColumnScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		[{0}] = @{0}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern = String.Format(pattern, property.DbName);
                finalString = String.Format("{0} {1} {2},", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 1); // (Remove trailing comma)
        }

        private static String GetBulkSetColumnScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern1 = "		{0} = CASE WHEN @{0} > 0 OR [{1}_DateDeleted] IS NOT NULL";
            String pattern2 = "         THEN [#tmp{0}].[{1}] ELSE [{0}].[{1}] END";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern1 = String.Format(pattern1, property.DbName, objectName);
                String resolvedPattern2 = String.Format(pattern2, objectName, property.DbName);
                finalString = String.Format(
                    "{0} {1} {2} {1} {3},",
                    finalString,
                    Environment.NewLine,
                    resolvedPattern1,
                    resolvedPattern2);
            }

            return finalString.Substring(0, finalString.Length - 1); // (Remove trailing comma)
        }

        private static String GetSetPropertiesScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern1 = "       DECLARE @{0} BIT;";
            String pattern2 = "       SET @{0} = CHARINDEX('{0}#', @{1}_SetProperties);";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern1 = String.Format(pattern1, property.DbName);
                String resolvedPattern2 = String.Format(pattern2, property.DbName, objectName);
                finalString = String.Format(
                    "{0} {1} {2} {3} {2}",
                    finalString,
                    resolvedPattern1,
                    Environment.NewLine,
                    resolvedPattern2);
            }

            return finalString;
        }

        private static String GetKeyDefScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		{0} {1}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.DbName, property.MssqlDbType);
                    finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString;
        }

        private static String GetKeyInsertedScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		INSERTED.{0}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.DbName);
                    finalString = String.Format("{0}, {1} {2}", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString;
        }

        private static String GetKeyJoinScripts(
            String schemaName,
            String objectName,
            IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		[#tmp{0}].{1} = {2}.[{0}].{1}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, objectName, property.DbName, schemaName);
                    finalString = String.Format("{0} {1} {2} AND", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString.Substring(0, finalString.Length - 4); // (Remove trailing AND)
        }

        //private static String GetFixedKeyJoinScripts(
        //    String schemaName,
        //    String objectName,
        //    IEnumerable<ObjectProperty> properties)
        //{
        //    String pattern = "		[#tmp{0}].{1} = {2}.[{0}].{1}";

        //    String finalString = String.Empty;
        //    foreach (ObjectProperty property in properties)
        //    {
        //        if (property.IsFixedKey)
        //        {
        //            String resolvedPattern = String.Format(pattern, objectName, property.DbName, schemaName);
        //            finalString = String.Format("{0} AND {1} {2}", finalString, Environment.NewLine, resolvedPattern);
        //        }
        //    }

        //    return finalString;
        //}

        private static String GetInsertKeyWhereScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "				{0} = @{0}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.DbName);
                    finalString = String.Format("{0} {1} {2} AND", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            if (!String.IsNullOrEmpty(finalString))
            {
                return finalString.Substring(0, finalString.Length - 4); // (Remove trailing AND)
            }
            else { return finalString; };
        }

        private static String GetBulkUpsertKeyWhereScripts(String objectName, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "				{0} = [#tmp{1}].{0}";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (property.IsKey)
                {
                    String resolvedPattern = String.Format(pattern, property.DbName, objectName);
                    finalString = String.Format("{0} {1} {2} AND", finalString, Environment.NewLine, resolvedPattern);
                }
            }

            return finalString.Substring(0, finalString.Length - 4); // (Remove trailing AND)
        }

        //private static String GetFixedKeyWhereScripts(String objectName, IEnumerable<ObjectProperty> properties)
        //{
        //    String pattern = "				{0} = [#tmp{1}].{0}";

        //    String finalString = String.Empty;
        //    foreach (ObjectProperty property in properties)
        //    {
        //        if (property.IsFixedKey)
        //        {
        //            String resolvedPattern = String.Format(pattern, property.DbName, objectName);
        //            finalString = String.Format("{0} AND {1} {2}", finalString, Environment.NewLine, resolvedPattern);
        //        }
        //    }

        //    return finalString;
        //}

        private static String GetKeyColumnsNameScript(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "{0}_";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties.Where(p => p.IsKey /*|| p.IsFixedKey*/))
            {
                String resolvedPattern = String.Format(pattern, property.CodeName);
                finalString = String.Format("{0}{1}", finalString, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 1); // Remove trailing underscore
        }

        private static String GetKeyColumnsScript(IEnumerable<ObjectProperty> properties)
        {
            String pattern = "		[{0}] ASC,";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties.Where(p => p.IsKey /*|| p.IsFixedKey*/))
            {
                String resolvedPattern = String.Format(pattern, property.DbName);
                finalString = String.Format("{0} {1} {2}", finalString, Environment.NewLine, resolvedPattern);
            }

            return finalString.Substring(0, finalString.Length - 1); // Remove trailing comma
        }

        #endregion

        #endregion

        #region Model-Related Methods

        private static String GetModelProperties(String schema, IEnumerable<ObjectProperty> properties)
        {
            String pattern = File.ReadAllText(Path.Combine(_templatePath, _modelPropertyTemplate));

            String output = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (!property.IsParentId)
                {
                    String resolvedPattern = pattern.Replace("%PROPERTYNAME%", property.CodeName);
                    resolvedPattern = resolvedPattern.Replace("%PROPERTYTYPE%", property.CodeType);

                    output = String.Format("{0} {1}{2}", output, resolvedPattern, Environment.NewLine);
                }
            }

            if ((schema == "MasterData") && !properties.Any(p=> p.IsParentId))
            {
                //Add on date info properties

                String resolvedPattern = pattern.Replace("%PROPERTYNAME%", "DateCreated");
                resolvedPattern = resolvedPattern.Replace("%PROPERTYTYPE%", "DateTime");
                output = String.Format("{0} {1}{2}", output, resolvedPattern, Environment.NewLine);

                resolvedPattern = pattern.Replace("%PROPERTYNAME%", "DateLastModified");
                resolvedPattern = resolvedPattern.Replace("%PROPERTYTYPE%", "DateTime");
                output = String.Format("{0} {1}{2}", output, resolvedPattern, Environment.NewLine);

                resolvedPattern = pattern.Replace("%PROPERTYNAME%", "DateDeleted");
                resolvedPattern = resolvedPattern.Replace("%PROPERTYTYPE%", "DateTime?");
                output = String.Format("{0} {1}{2}", output, resolvedPattern, Environment.NewLine);
            }

            return output;
        }

        private static String GetModelInfoProperties(String schema, IEnumerable<ObjectProperty> properties)
        {
            String pattern = File.ReadAllText(Path.Combine(_templatePath, _modelInfoPropertyTemplate));

            String output = String.Empty;
            foreach (ObjectProperty property in properties.Where(p=> p.IsInInfo))
            {
                if (!property.IsParentId)
                {
                    String resolvedPattern = pattern.Replace("%PROPERTYNAME%", property.CodeName);
                    resolvedPattern = resolvedPattern.Replace("%PROPERTYTYPE%", property.CodeType);

                    output = String.Format("{0} {1}{2}", output, resolvedPattern, Environment.NewLine);
                }
            }


            return output;
        }

        private static String GetModelGetDtoProperties(String schema, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "{1} = ReadProperty<{0}>({1}Property),";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                String resolvedPattern;
                if (property.IsParentId)
                {
                    resolvedPattern = String.Format("{0} = parent.Id,", property.CodeName);
                }
                else
                {
                    resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName);
                }
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);

            }

            if ((schema == "MasterData"))
            {
                //Add on date info properties

                String resolvedPattern = String.Format(pattern, "DateTime", "DateCreated");
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);

                resolvedPattern = String.Format(pattern, "DateTime", "DateLastModified");
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);

                resolvedPattern = String.Format(pattern, "DateTime?", "DateDeleted");
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);
            }

            return finalString;
        }

        private static String GetModelLoadDtoProperties(String schema, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "this.LoadProperty<{0}>({1}Property, dto.{1});";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (!property.IsParentId)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName);
                    finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);
                }
            }

            if ((schema == "MasterData"))
            {
                //Add on date info properties

                String resolvedPattern = String.Format(pattern, "DateTime", "DateCreated");
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);

                resolvedPattern = String.Format(pattern, "DateTime", "DateLastModified");
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);

                resolvedPattern = String.Format(pattern, "DateTime?", "DateDeleted");
                finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);
            }

            return finalString;
        }

        private static String GetModelInfoLoadDtoProperties(String schema, IEnumerable<ObjectProperty> properties)
        {
            String pattern = "this.LoadProperty<{0}>({1}Property, dto.{1});";

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties.Where(p=> p.IsInInfo))
            {
                if (!property.IsParentId)
                {
                    String resolvedPattern = String.Format(pattern, property.CodeType, property.CodeName);
                    finalString = String.Format("{0} {1} {2}", finalString, resolvedPattern, Environment.NewLine);
                }
            }

            return finalString;
        }

        private static String GetModelBusinessRules(IEnumerable<ObjectProperty> properties)
        {

            String finalString = String.Empty;
            foreach (ObjectProperty property in properties)
            {
                if (!property.IsParentId)
                {
                    //if the property is a string add a max length rule
                    if (property.CodeType == "String")
                    {
                        //if property is a string and cannot be null add a required rule
                        if (property.Nullability == "NOT NULL")
                        {
                            String requiredPattern =
                                String.Format("BusinessRules.AddRule(new Required({0}Property));", property.CodeName);
                            finalString = String.Format("{0} {1} {2}", finalString, requiredPattern, Environment.NewLine);
                        }


                        String maxLength = property.MssqlDbType.Substring(11, property.MssqlDbType.IndexOf(')') - 11);

                        String maxLengthPattern =
                            String.Format("BusinessRules.AddRule(new MaxLength({0}Property, {1}));", property.CodeName, maxLength);
                        finalString = String.Format("{0} {1} {2}", finalString, maxLengthPattern, Environment.NewLine);
                    }

                    //if the property is an int key then set a min val of 1.
                    else if (property.CodeType == "Int32" && property.IsKey)
                    {
                        String requiredPattern =
                                String.Format("BusinessRules.AddRule(new MinValue<Int32>({0}Property, 1));", property.CodeName);
                        finalString = String.Format("{0} {1} {2}", finalString, requiredPattern, Environment.NewLine);
                    }
                }
            }

            return finalString;
        }

        #endregion

        #endregion
    }
}
