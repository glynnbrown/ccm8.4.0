﻿
[Serializable]
public class %OBJECT%IsSetDto {
	
    #region Properties
    %ISSETPROPERTIES%
    #endregion

    #region Constructors

    /// <summary>
    /// Default
    /// </summary>
    public %OBJECT%IsSetDto() { }

    /// <summary>
    /// Create a new instance with all fields set to 
    /// the passed in Boolean value
    /// </summary>
    /// <param name="initialSet">Boolean value to set all fields</param>
    public %OBJECT%IsSetDto(Boolean initialSet)
    {
        SetAllFieldsToBoolean(initialSet);
    }
        
    #endregion

    #region Methods
    /// <summary>
    /// Returns a hash code for this object
    /// </summary>
    /// <returns>The object hash code</returns>
    public override Int32 GetHashCode()
    {
        return%ISSETHASH%;
    }

    /// <summary>
    /// Check to see if two isSet objects are the same
    /// </summary>
    /// <param name="obj">The object to compare against</param>
    /// <returns>true if objects are equal</returns>
    public override Boolean Equals(Object obj)
    {
        %OBJECT%IsSetDto other = obj as %OBJECT%IsSetDto;
        if (other != null)
        {
            %ISSETEQUALS%
        }
        else
        {
            return false;
        }
        return true;
    }

    /// <summary>
    /// Sets all fields to the passed in boolean value
    /// </summary>
    /// <param name="isSet">Boolean value to set all fields</param>
    public void SetAllFieldsToBoolean(Boolean isSet)
    {
		%ISSETSET%
    }
    #endregion 
}