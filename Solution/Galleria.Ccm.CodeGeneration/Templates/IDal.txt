﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM %VERSION%)
// CCM-%GEMINI% : %DEVELOPER%
//		Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Dal.Interfaces
{
    public interface I%OBJECT%Dal : IDal
    {%METHODS%
    }
}
