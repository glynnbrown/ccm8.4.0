﻿/****** [%SCHEMA%].[%OBJECT%_FetchAll] (Auto-generated) ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[%SCHEMA%].[%OBJECT%_FetchAll]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [%SCHEMA%].[%OBJECT%_FetchAll];
END
GO

-- Description: Fetch all %OBJECT% record(s)

-- Version History : CCM %VERSION%
--	CCM-%GEMINI% : %DEVELOPER%
--		Initial Version

CREATE PROCEDURE [%SCHEMA%].[%OBJECT%_FetchAll]
AS
BEGIN TRY
	SELECT
		[%OBJECT%].[%OBJECT%_Id],
		[%OBJECT%].[%OBJECT%_RowVersion]%SELECT%
	FROM
		[%SCHEMA%].[%OBJECT%];
END TRY

BEGIN CATCH
	DECLARE @ErrorProcedure NVARCHAR(126);
	SELECT @ErrorProcedure = ISNULL(ERROR_PROCEDURE(),N'[%SCHEMA%].[%OBJECT%_FetchAll]');
	EXEC System.RethrowError @ErrorProcedure;
END CATCH

GO

