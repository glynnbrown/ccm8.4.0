﻿/****** [%SCHEMA%].[%OBJECT%_Insert] (Auto-generated) ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[%SCHEMA%].[%OBJECT%_Insert]') AND type in (N'P', N'PC'))
BEGIN
	DROP PROCEDURE [%SCHEMA%].[%OBJECT%_Insert];
END
GO

-- Description: Create a %OBJECT% record

-- Version History : CCM %VERSION%
--	CCM-%GEMINI% : %DEVELOPER%
--		Initial Version

CREATE PROCEDURE [%SCHEMA%].[%OBJECT%_Insert]
	@%OBJECT%_Id [INT] OUT,
	@%OBJECT%_RowVersion [ROWVERSION] OUT%CRITERIA%
AS
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @Output TABLE ([%OBJECT%_Id] [INT], [%OBJECT%_RowVersion] [BINARY](8));

	INSERT [%SCHEMA%].[%OBJECT%]
	(%INSERT%
	)
	OUTPUT
		INSERTED.[%OBJECT%_Id],
		INSERTED.[%OBJECT%_RowVersion]
	INTO
		@Output
	VALUES
	(%VALUES%
	);

	SELECT
		@%OBJECT%_Id = [%OBJECT%_Id],
		@%OBJECT%_RowVersion = [%OBJECT%_RowVersion]
	FROM
		@Output;

END TRY

BEGIN CATCH
	DECLARE @ErrorProcedure [NVARCHAR](126);
	SELECT @ErrorProcedure = ISNULL(ERROR_PROCEDURE(),N'[%SCHEMA%].[%OBJECT%_Insert]');
	EXEC System.RethrowError @ErrorProcedure;
END CATCH

GO
