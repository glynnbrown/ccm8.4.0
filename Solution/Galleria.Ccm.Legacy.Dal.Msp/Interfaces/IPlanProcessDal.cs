﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Dal.Msp.Interfaces
{
    public interface IPlanProcessDal : IDal
    {
        List<Int32> FetchPlanProposalCodes();
    }
}
