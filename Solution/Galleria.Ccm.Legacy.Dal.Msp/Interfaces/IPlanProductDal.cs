﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Legacy.Dal.Msp.Interfaces
{
    public interface IPlanProductDal : IDal
    {
    }
}
