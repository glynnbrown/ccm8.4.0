﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using Galleria.Framework.Dal;
using System;

namespace Galleria.Ccm.Legacy.Dal.Msp
{
    public class DalBase : Galleria.Framework.Dal.DalBase
    {
        #region Methods

        internal Int32 GetInt32(Object value)
        {
            if (value == null || value is System.DBNull)
            {
                return default(Int32);
            }
            else
            {
                return Convert.ToInt32(value);
            }
        }
        internal Double GetDouble(Object value)
        {
            if (value == null || value is System.DBNull)
            {
                return default(Double);
            }
            else
            {
                return Convert.ToDouble(value);
            }
        }
        internal Byte GetByte(Object value)
        {
            if (value == null || value is System.DBNull)
            {
                return default(Byte);
            }
            else
            {
                return Convert.ToByte(value);
            }
        }

        internal String GetString(Object value)
        {
            if (value == null || value is System.DBNull)
            {
                return default(String);
            }
            else
            {
                return Convert.ToString(value);
            }
        }
        
        /// <summary>
        /// The context for this dal
        /// </summary>
        public DalContext DalContext
        {
            get { return (DalContext)((IDal)this).DalContext; }
            set { ((IDal)this).DalContext = value; }
        }

        /// <summary>
        /// Called when disposing of this instance
        /// </summary>
        public override void Dispose()
        {
            // Nothing to do
        }

        #endregion

    }
}
