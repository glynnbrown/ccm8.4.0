﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Dal.Msp.Scripts
{
    public class PlanPosition
    {
        public const String FetchByProposalCodeElementCode =
            @"SELECT 
                Position_.Position_Code, 
                Position_.Plan_Element_Code, 
                Product_.Product_Code, 
                Orientation_.Name_ AS Orientation,
                Orientation_.Forward_Face_,
                Orientation_.Rotation_,
                Position_.X_Pos_, 
                Position_.Y_Pos_, 
                Position_.No_High_, 
                Position_.No_Wide_, 
                Position_.No_Deep_, 
                Position_.Units_, 
                Product_.Height_, 
                Product_.Width_, 
                Product_.Depth_, 
                [Position_].[Squeeze_Height_] / [Position_].[Block_Height_] AS SqueezeHeight, 
                [Position_].[Squeeze_Width_] / [Position_].[Block_Width_] AS SqueezeWidth, 
                Position_.Block_Height_, 
                Position_.Block_Width_, 
                Position_.Block_Top_, 
                Position_.Leading_Divider_, 
                Position_.Leading_Gap_, 
                Position_.Leading_Gap_Vert_, 
                Position_.Cap_Bot_Deep_, 
                Position_.Cap_Bot_High_, 
                Position_.Cap_Lft_Deep_, 
                Position_.Cap_Lft_Wide_, 
                Position_.Cap_Rgt_Deep_, 
                Position_.Cap_Rgt_Wide_, 
                Position_.No_Capping_, 
                Position_.No_Capping_Deep_, 
                Tray_Pack_.Rotation_ AS Tray_Rotation, 
                Tray_Pack_.Trays_Only_, 
                Tray_Pack_.Tray_Height_, 
                Tray_Pack_.Tray_Thick_, 
                Tray_Pack_.Units_High_ AS Tray_Units_High, 
                Tray_Pack_.Units_Wide_ AS Tray_Units_Wide, 
                Tray_Pack_.Units_Deep_ AS Tray_Units_Deep,
                Position_.Manual_Merchandising_Styl
            FROM (((Product_ INNER JOIN (Position_ INNER JOIN Range_Matrix_ ON Position_.Range_Matrix_Code = Range_Matrix_.Range_Matrix_Code) ON Product_.Product_Code = Range_Matrix_.Product_Code) INNER JOIN Tray_Pack_ ON Product_.Tray_Pack_Code = Tray_Pack_.Tray_Pack_Code) INNER JOIN Orientation_ ON Position_.Orientation_Code = Orientation_.Orientation_Code) INNER JOIN Plan_Element_ AS PE ON Position_.Plan_Element_Code = PE.Plan_Element_Code 
            WHERE Position_.Plan_Element_Code = @PlanElementCode 
            ORDER BY Position_.X_Pos_, Position_.Y_Pos_";
    }
}
