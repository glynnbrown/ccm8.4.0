﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects
{
    public class PlanElementDto
    {
        public Int32 Plan_Element_Code { get; set; }
        public Int32 Active { get; set; }
        public Int32 Colour { get; set; }

        public String Combined_With { get; set; }

        public Double Dimension_0 { get; set; }
        public Double Dimension_1 { get; set; }
        public Double Dimension_2 { get; set; }
        public Double Dimension_3 { get; set; }
        public Double Dimension_4 { get; set; }
        public Double Dimension_5 { get; set; }
        public Double Dimension_6 { get; set; }
        public Double Dimension_7 { get; set; }
        public Double Dimension_8 { get; set; }
        public Double Finger_Space { get; set; }

        public String Name { get; set; }

        public Int32 Next_PE_Code { get; set; }
        public Int32 No_Shelves_Wide { get; set; }
        public Int32 Plan_Bay_Code { get; set; }
        public Int32 Prev_PE_Code { get; set; }
        public Int32 Prnt_PE_Code { get; set; }

        public Double Slope { get; set; }
        public Double Space_Remaining { get; set; }

        public Int32 SplitType { get; set; }
        public Int32 Sub_Type { get; set; }

        public Double Tallest_Height { get; set; }

        public String Type { get; set; }

        public Int32 Use_Finger_Space { get; set; }

        public Double X_Position { get; set; }
        public Double X_Scale { get; set; }
        public Double Y_Position { get; set; }
        public Double Y_Scale { get; set; }
        public Double Z_Position { get; set; }

        public Int32 OriginalPlanElementCode { get; set; }
        public Int32 PlanID { get; set; }

        public Byte Status { get; set; }
    }
}
