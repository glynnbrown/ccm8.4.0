﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects
{
    public class PlanBayDto
    {
        public Int32 Plan_Bay_Code { get; set; }
        public Int32 Base_Colour { get; set; }

        public Double Base_Depth { get; set; }
        public Double Base_Height { get; set; }
        public Double Base_Width { get; set; }
        public Double Bay_Position { get; set; }

        public String Bay_Units { get; set; }

        public Int32 Colour { get; set; }

        public String Continuous_Shelf { get; set; }

        public Double Depth { get; set; }
        public Double Height { get; set; }
        public Double Width { get; set; }
        public Double FirstNotch { get; set; }

        public String Name { get; set; }

        public Double Notch_Spacing { get; set; }

        public Int32 Number_Of_Bays { get; set; }
        public Int32 Plan_Proposal_Code { get; set; }
        public Int32 Position_Relative { get; set; }
        public Int32 Show_Peg_Holes { get; set; }

        public Double Take_Off { get; set; }

        public Int32 Use_Notch_Spacing { get; set; }
        public Int32 OriginalPlanBayCode { get; set; }
        public Int32 PlanId { get; set; }

        public Byte Status { get; set; }

    }
}
