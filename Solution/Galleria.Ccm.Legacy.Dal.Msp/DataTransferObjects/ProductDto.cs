﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects
{
    public class ProductDto
    {
        public Int32 Product_Code { get; set; }
        public Int32 Baskets_Code { get; set; }
        public Int32 Case_Pack_Code { get; set; }
        public Int32 Category_Code { get; set; }
        public Int32 Merchandising_Style_Code { get; set; }
        public Int32 Description_A_Code { get; set; }
        public Int32 Dimension_Units_Code { get; set; }
        public Int32 Group_Parent_Code { get; set; }
        public Int32 Inventory_Rules_Code { get; set; }
        public Int32 Key_Field_Code { get; set; }
        public Int32 Manufacturer_Code { get; set; }
        public Int32 Pallets_Code { get; set; }
        public Int32 Product_Group_Code { get; set; }
        public Int32 Size_Code { get; set; }
        public Int32 Squeeze_Factor_Code { get; set; }
        public Int32 Sub_Category_Code { get; set; }
        public Int32 Sub_Section_Code { get; set; }
        public Int32 Tray_Pack_Code { get; set; }
        public Int32 VAT_Code { get; set; }
        public Int32 Section_Code { get; set; }

        public String Prod_ID { get; set; }
        public String Name { get; set; }
        public String Code { get; set; }
        public String EAN { get; set; }

        public Int32 Colour { get; set; }
        public Int32 Pallets_Units_High { get; set; }

        public Double Cost_Price { get; set; }
        public Double Height { get; set; }
        public Double Width { get; set; }
        public Double Depth { get; set; }
        public Double G_Profit { get; set; }
        public Double Left_Nest_Height { get; set; }
        public Double Left_Nest_Top_Offset { get; set; }
        public Double Left_Nest_Width { get; set; }
        public Double Right_Nest_Height { get; set; }
        public Double Right_Nest_Top_Offset { get; set; }
        public Double Right_Nest_Width { get; set; }
        public Double Margin { get; set; }
        public Double Movement { get; set; }
        public Double Nesting_Depth { get; set; }
        public Double Nesting_Height { get; set; }
        public Double Nesting_Width { get; set; }
        public Double Peg_Centre_Left { get; set; }
        public Double Peg_Centre_Top { get; set; }
        public Double Price { get; set; }
        public Double Product_PSI { get; set; }
        public Double Rank_PSI { get; set; }
        public Double Sales { get; set; }
        public Double Section_PSI { get; set; }
        public Double Section_Rank { get; set; }


        public String TrayPackName { get; set; }
        public Int32 Rotation { get; set; }
        public Int32 Trays_Only { get; set; }

        public Double Tray_Height { get; set; }
        public Double Tray_Thick { get; set; }

        public Int32 Tray_Units_High { get; set; }
        public Int32 Tray_Units_Wide { get; set; }
        public Int32 Tray_Units_Deep { get; set; }
        public Double VatRate { get; set; }

        public Double User_Number_1 { get; set; }
        public Double User_Number_2 { get; set; }
        public Double User_Number_3 { get; set; }
        public Double User_Number_4 { get; set; }
        public Double User_Number_5 { get; set; }
        public Double User_Number_6 { get; set; }
        public Double User_Number_7 { get; set; }
        public Double User_Number_8 { get; set; }
        public Double User_Number_9 { get; set; }
        public Double User_Number_10 { get; set; }
        public Double User_Number_11 { get; set; }
        public Double User_Number_12 { get; set; }
        public Double User_Number_13 { get; set; }
        public Double User_Number_14 { get; set; }
        public Double User_Number_15 { get; set; }
        public Double User_Number_16 { get; set; }
        public Double User_Number_17 { get; set; }
        public Double User_Number_18 { get; set; }
        public Double User_Number_19 { get; set; }
        public Double User_Number_20 { get; set; }

        public String User_String_1 { get; set; }
        public String User_String_2 { get; set; }
        public String User_String_3 { get; set; }
        public String User_String_4 { get; set; }
        public String User_String_5 { get; set; }
        public String User_String_6 { get; set; }
        public String User_String_7 { get; set; }
        public String User_String_8 { get; set; }
        public String User_String_9 { get; set; }
        public String User_String_10 { get; set; }

        public String Sub_Category_Name { get; set; }
        public String Manufacturer_Name { get; set; }

        public Int32 Max_Cap { get; set; }
        public Int32 Max_Stack { get; set; }
        public Single Inventory_Cases { get; set; }

    }
}
