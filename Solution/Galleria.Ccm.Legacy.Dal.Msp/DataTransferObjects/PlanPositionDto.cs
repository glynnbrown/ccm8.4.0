﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects
{
    public class PlanPositionDto
    {
        public Int32 Position_Code { get; set; }
        public Int32 Plan_Element_Code { get; set; }
        public Int32 Product_Code { get; set; }
        public String OrientationID { get; set; }
        public Double X_Pos { get; set; }
        public Double Y_Pos { get; set; }
        public Int32 Forward_Face { get; set; }
        public Int32 Rotation { get; set; }
        public Double Height { get; set; }
        public Double Width { get; set; }
        public Double Depth { get; set; }
        public Double SqueezeHeight { get; set; }
        public Double SqueezeWidth { get; set; }
        public Double Block_Height { get; set; }
        public Double Block_Width { get; set; }
        public Double Block_Top { get; set; }
        public Double Leading_Divider { get; set; }
        public Double Leading_Gap { get; set; }
        public Double Leading_Gap_Vert { get; set; }

        public Int32 No_High { get; set; }
        public Int32 No_Wide { get; set; }
        public Int32 No_Deep { get; set; }
        public Int32 Units { get; set; }
        public Int32 Cap_Bot_Deep { get; set; }
        public Int32 Cap_Bot_High { get; set; }
        public Int32 Cap_Lft_Deep { get; set; }
        public Int32 Cap_Lft_Wide { get; set; }
        public Int32 Cap_Rgt_Deep { get; set; }
        public Int32 Cap_Rgt_Wide { get; set; }
        public Int32 No_Capping { get; set; }
        public Int32 No_Capping_Deep { get; set; }

        public Int32 Tray_Rotation { get; set; }
        public Int32 Trays_Only { get; set; }
        public Double Tray_Height { get; set; }
        public Double Tray_Thick { get; set; }
        public Int32 Tray_Units_High { get; set; }
        public Int32 Tray_Units_Wide { get; set; }
        public Int32 Tray_Units_Deep { get; set; }

        public Int32 IsManuallyPlaced { get; set; }
    }
}
