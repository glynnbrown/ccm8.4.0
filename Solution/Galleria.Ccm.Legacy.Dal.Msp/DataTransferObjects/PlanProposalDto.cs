﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects
{
    public class PlanProposalDto
    {
        public Int32 Plan_Proposal_Code { get; set; }
        public Int32 Category_Market_Code { get; set; }
        public Int32 Collisions{ get; set; }
        public Int32 DimUnits { get; set; }
        
        public String Fixture_Name { get; set; }
        public String FixtureElementEntry { get; set; }

        public Int32 Flags { get; set; }
        public Int32 FloatingLabelEntry { get; set; }
        public Double Footage { get; set; }

        public String InfoBoxEntry { get; set; }

        public Double Max_Height { get; set; }
        public Double Max_Width { get; set; }

        public String Name { get; set; }

        public Double No_Stores { get; set; }

        public String Nominal_Size { get; set; }
        public String Notes { get; set; }
        public String ProductEntry { get; set; }
        public String ProductListEntry { get; set; }

        public Double Shelf_Linear { get; set; }

        public String Title { get; set; }

        public Int32 Traffic_Flow { get; set; }
        public Double User_Number_1 { get; set; }
        public Double User_Number_2 { get; set; }
        public Double User_Number_3 { get; set; }
        public Double User_Number_4 { get; set; }
        public Double User_Number_5 { get; set; }

        public String User_String_1 { get; set; }
        public String User_String_2 { get; set; }

        public Int32 Vert_Traffic_Flow { get; set; }
        public Int32 PlanId { get; set; }
    }
}
