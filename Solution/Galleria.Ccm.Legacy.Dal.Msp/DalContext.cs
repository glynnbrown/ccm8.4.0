﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using Galleria.Framework.Dal;
using System.Data.OleDb;
using System;
using System.Reflection;
using Galleria.Framework.Dal.Configuration;
using System.Configuration;

namespace Galleria.Ccm.Legacy.Dal.Msp
{
    public class DalContext : DalContextBase
    {
        #region Fields
        private Assembly _assembly; // holds the assembly to load dals from
        private OleDbConnection _connection;
        private Boolean _transactionInProgress = false; // indicates if a transaction is in progress on this context
        #endregion

        #region Properties
        public OleDbConnection Connection
        {
            get
            {
                return _connection;
            }
            internal set
            {
                _connection = value;
            }
        }

        /// <summary>
        /// Indicates if a transaction is in progress on this context
        /// </summary>
        public override Boolean TransactionInProgress
        {
            get { return _transactionInProgress; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalContext(OleDbConnection connection)
            :base()
        {
            _connection = connection;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DalContext(Assembly assembly, DalFactoryConfigElement dalFactoryConfig)
        {
            _assembly = assembly;
            _connection = CreateConnection(dalFactoryConfig);
            Open();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a new connection from the specified connection string
        /// </summary>
        /// <param name="connectionString">The database connection string</param>
        /// <returns>A new database connection</returns>
        private OleDbConnection CreateConnection(DalFactoryConfigElement dalFactoryConfig)
        {
            // build the database connection string
            String connectionString = dalFactoryConfig.DalParameters["ConnectionString"].Value;
            
            
            // create a connection from our connection string
            OleDbConnection connection = new OleDbConnection(connectionString);

            // return the connection
            return connection;
        }


        ///// <summary>
        ///// Returns a dal object that supports the specified interface
        ///// </summary>
        ///// <typeparam name="TDalInterface">The interface to return a dal for</typeparam>
        ///// <returns>A dal object that supports the specified interface</returns>
        //public override TDalInterface GetDal<TDalInterface>()
        //{
        //    return this.GetDal<TDalInterface>(_assembly);
        //}

        public void Open()
        {
            _connection.Open();
        }
        /// <summary>
        /// Begins a transaction within this context
        /// </summary>
        public override void Begin()
        {
            _transactionInProgress = true;
        }

        /// <summary>
        /// Commits a transaction within this context
        /// </summary>
        public override void Commit()
        {
            _transactionInProgress = false;
        }

        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        protected override void OnDispose()
        {
            base.OnDispose();
            if (_connection != null) _connection.Close();
        }
        #endregion
    }
}
