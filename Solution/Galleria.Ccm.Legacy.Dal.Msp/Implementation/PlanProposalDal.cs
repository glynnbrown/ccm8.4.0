﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.Msp;

using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Dal.Msp.Implementation
{

    public class PlanProposalDal : DalBase, IPlanProposalDal
    {
        private PlanProposalDto GetDataTransferObject(OleDbDataReader dr)
        {
            return new PlanProposalDto()
            {
                Plan_Proposal_Code = GetInt32(dr["Plan_Proposal_Code"]),
                Category_Market_Code = GetInt32(dr["Category_Market_Code"]),
                Collisions = GetInt32(dr["Collisions_"]),
                DimUnits = GetInt32(dr["DimUnits_"]),

                Fixture_Name = Convert.ToString(dr["Fixture_Name_"]),
                FixtureElementEntry = Convert.ToString(dr["FixtureElementEntry_"]),

                Flags = GetInt32(dr["Flags_"]),
                FloatingLabelEntry = GetInt32(dr["FloatingLabelEntry_"]),
                Footage = GetDouble(dr["Footage_"]),

                InfoBoxEntry = Convert.ToString(dr["InfoBoxEntry_"]),

                Max_Height = GetDouble(dr["Max_Height_"]),
                Max_Width = GetDouble(dr["Max_Width_"]),

                Name = Convert.ToString(dr["Name_"]),

                No_Stores = GetDouble(dr["No_Stores_"]),

                Nominal_Size = Convert.ToString(dr["Nominal_Size_"]),
                Notes = Convert.ToString(dr["Notes_"]),
                ProductEntry = Convert.ToString(dr["ProductEntry_"]),
                ProductListEntry = Convert.ToString(dr["ProductListEntry_"]),

                Shelf_Linear = GetInt32(dr["Shelf_Linear_"]),

                Title = Convert.ToString(dr["Title_"]),

                Traffic_Flow = GetInt32(dr["Traffic_Flow_"]),
                User_Number_1 = GetDouble(dr["User_Number_1_"]),
                User_Number_2 = GetDouble(dr["User_Number_2_"]),
                User_Number_3 = GetDouble(dr["User_Number_3_"]),
                User_Number_4 = GetDouble(dr["User_Number_4_"]),
                User_Number_5 = GetDouble(dr["User_Number_5_"]),

                User_String_1 = Convert.ToString(dr["User_String_1_"]),
                User_String_2 = Convert.ToString(dr["User_String_2_"]),

                Vert_Traffic_Flow = GetInt32(dr["Vert_Traffic_Flow_"])
            };
        }
        public PlanProposalDto PlanProposal_FetchByCode(Int32 proposalCode)
        {
            PlanProposalDto output = null;
            try
            {
                using (OleDbCommand cmd = this.DalContext.Connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT * " +
                                        " FROM Plan_Proposal_ " +
                                        " WHERE Plan_Proposal_Code = @PlanProposalCode";
                    cmd.Parameters.Add("@PlanProposalCode", OleDbType.Integer).Value = proposalCode;

                    using (OleDbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            if (dr.Read())
                            {
                                output = GetDataTransferObject(dr);
                            }
                            else
                            {
                                throw new DtoDoesNotExistException();
                            }
                        }
                        else
                        {
                        }
                    }
                }
            }
            catch (OleDbException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return output;
        }
    }
}
