﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using System.Data.OleDb;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.Msp.Scripts;
namespace Galleria.Ccm.Legacy.Dal.Msp.Implementation
{
    public class ProductDal : DalBase, IProductDal
    {
        private ProductDto GetDataTransferObject(OleDbDataReader dr)
        {
            return new ProductDto() 
            {
                Product_Code = GetInt32(dr["Product_Code"]),
                Baskets_Code = GetInt32(dr["Baskets_Code"]),
                Case_Pack_Code = GetInt32(dr["Case_Pack_Code"]),
                Category_Code = GetInt32(dr["Category_Code"]),
                Merchandising_Style_Code = GetInt32(dr["Merchandising_Style_Code"]),
                Description_A_Code = GetInt32(dr["Description_A_Code"]),
                Dimension_Units_Code = GetInt32(dr["Dimension_Units_Code"]),
                Group_Parent_Code = GetInt32(dr["Group_Parent_Code"]),
                Inventory_Rules_Code = GetInt32(dr["Inventory_Rules_Code"]),
                Key_Field_Code = GetInt32(dr["Key_Field_Code"]),
                Manufacturer_Code = GetInt32(dr["Manufacturer_Code"]),
                Pallets_Code = GetInt32(dr["Pallets_Code"]),
                Product_Group_Code = GetInt32(dr["Product_Group_Code"]),
                Size_Code = GetInt32(dr["Size_Code"]),
                Squeeze_Factor_Code = GetInt32(dr["Squeeze_Factor_Code"]),
                Sub_Category_Code = GetInt32(dr["Sub_Category_Code"]),
                Sub_Section_Code = GetInt32(dr["Sub_Section_Code"]),
                Tray_Pack_Code = GetInt32(dr["Tray_Pack_Code"]),
                VAT_Code = GetInt32(dr["VAT_Code"]),
                Section_Code = GetInt32(dr["Section_Code"]),

                Prod_ID = GetString(dr["Prod_ID_"]),
                Name = GetString(dr["Name_"]),
                Code = GetString(dr["Code_"]),
                EAN = GetString(dr["EAN_"]),

                Colour = GetInt32(dr["Colour_"]),
                Pallets_Units_High = GetInt32(dr["Pallets_Units_High_"]),

                Cost_Price = GetDouble(dr["Cost_Price_"]),
                Height = GetDouble(dr["Height_"]),
                Width = GetDouble(dr["Width_"]),
                Depth = GetDouble(dr["Depth_"]),
                G_Profit = GetDouble(dr["G_Profit_"]),
                Left_Nest_Height = GetDouble(dr["Left_Nest_Height_"]),
                Left_Nest_Top_Offset = GetDouble(dr["Left_Nest_Top_Offset_"]),
                Left_Nest_Width = GetDouble(dr["Left_Nest_Width_"]),
                Right_Nest_Height = GetDouble(dr["Right_Nest_Height_"]),
                Right_Nest_Top_Offset = GetDouble(dr["Right_Nest_Top_Offset_"]),
                Right_Nest_Width = GetDouble(dr["Right_Nest_Width_"]),
                Margin = GetDouble(dr["Margin_"]),
                Movement = GetDouble(dr["Movement_"]),
                Nesting_Depth = GetDouble(dr["Nesting_Depth_"]),
                Nesting_Height = GetDouble(dr["Nesting_Height_"]),
                Nesting_Width = GetDouble(dr["Nesting_Width_"]),
                Peg_Centre_Left = GetDouble(dr["Peg_Centre_Left_"]),
                Peg_Centre_Top = GetDouble(dr["Peg_Centre_Top_"]),
                Price = GetDouble(dr["Price_"]),
                Product_PSI = GetDouble(dr["Product_PSI_"]),
                Rank_PSI = GetDouble(dr["Rank_PSI_"]),
                Sales = GetDouble(dr["Sales_"]),
                Section_PSI = GetDouble(dr["Section_PSI_"]),
                Section_Rank = GetDouble(dr["Section_Rank_"]),


                TrayPackName = GetString(dr["TrayPackName"]),
                Rotation = GetInt32(dr["Rotation_"]),
                Trays_Only = GetInt32(dr["Trays_Only_"]),

                Tray_Height = GetDouble(dr["Tray_Height_"]),
                Tray_Thick = GetDouble(dr["Tray_Thick_"]),

                Tray_Units_High = GetInt32(dr["Tray_Units_High"]),
                Tray_Units_Wide = GetInt32(dr["Tray_Units_Wide"]),
                Tray_Units_Deep = GetInt32(dr["Tray_Units_Deep"]),
                VatRate = GetDouble(dr["VatRate"]),
                User_Number_1 = GetDouble(dr["User_Number_1_"]),
                User_Number_2 = GetDouble(dr["User_Number_2_"]),
                User_Number_3 = GetDouble(dr["User_Number_3_"]),
                User_Number_4 = GetDouble(dr["User_Number_4_"]),
                User_Number_5 = GetDouble(dr["User_Number_5_"]),
                User_Number_6 = GetDouble(dr["User_Number_6_"]),
                User_Number_7 = GetDouble(dr["User_Number_7_"]),
                User_Number_8 = GetDouble(dr["User_Number_8_"]),
                User_Number_9 = GetDouble(dr["User_Number_9_"]),
                User_Number_10 = GetDouble(dr["User_Number_10_"]),
                User_Number_11 = GetDouble(dr["User_Number_11_"]),
                User_Number_12 = GetDouble(dr["User_Number_12_"]),
                User_Number_13 = GetDouble(dr["User_Number_13_"]),
                User_Number_14 = GetDouble(dr["User_Number_14_"]),
                User_Number_15 = GetDouble(dr["User_Number_15_"]),
                User_Number_16 = GetDouble(dr["User_Number_16_"]),
                User_Number_17 = GetDouble(dr["User_Number_17_"]),
                User_Number_18 = GetDouble(dr["User_Number_18_"]),
                User_Number_19 = GetDouble(dr["User_Number_19_"]),
                User_Number_20 = GetDouble(dr["User_Number_20_"]),
                User_String_1 = GetString(dr["User_String_1_"]),
                User_String_2 = GetString(dr["User_String_2_"]),
                User_String_3 = GetString(dr["User_String_3_"]),
                User_String_4 = GetString(dr["User_String_4_"]),
                User_String_5 = GetString(dr["User_String_5_"]),
                User_String_6 = GetString(dr["User_String_6_"]),
                User_String_7 = GetString(dr["User_String_7_"]),
                User_String_8 = GetString(dr["User_String_8_"]),
                User_String_9 = GetString(dr["User_String_9_"]),
                User_String_10 = GetString(dr["User_String_10_"]),
                Sub_Category_Name = GetString(dr["Sub_Category_Name"]),
                Manufacturer_Name = GetString(dr["Manufacturer_Name"]),
                Max_Cap = GetInt32(dr["Max_Cap"]),
                Max_Stack = GetInt32(dr["Max_Stack"]),
                Inventory_Cases = (Single)GetDouble(dr["Inventory_Cases"])
            };
        }
        public List<ProductDto> FetchAll()
        {
            List<ProductDto> output = new List<ProductDto>();
            try
            {
                using (OleDbCommand cmd = this.DalContext.Connection.CreateCommand())
                {
                    cmd.CommandText = Product.FetchAll;
                   
                    using (OleDbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                output.Add(GetDataTransferObject(dr));
                            }
                        }
                    }
                }
            }
            catch (OleDbException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return output;
        }

        public List<ProductDto> FetchByProposalCode(Int32 proposalCode)
        {
            List<ProductDto> output = new List<ProductDto>();
            try
            {
                using (OleDbCommand cmd = this.DalContext.Connection.CreateCommand())
                {
                    cmd.CommandText = Product.FetchByPlanProposalCode;
                    cmd.Parameters.Add("@PlanProposalCode", OleDbType.Integer).Value = proposalCode;

                    using (OleDbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                output.Add(GetDataTransferObject(dr));
                            }
                        }
                    }
                }
            }
            catch (OleDbException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return output;
        }

        public List<ProductDto> FetchPlacedByProposalCode(Int32 proposalCode)
        {
            List<ProductDto> output = new List<ProductDto>();
            try
            {
                using (OleDbCommand cmd = this.DalContext.Connection.CreateCommand())
                {
                    cmd.CommandText = Product.FetchPlacedByPlanProposalCode;
                    cmd.Parameters.Add("@PlanProposalCode", OleDbType.Integer).Value = proposalCode;

                    using (OleDbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                output.Add(GetDataTransferObject(dr));
                            }
                        }
                    }
                }
            }
            catch (OleDbException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return output;
        }
    }
}
