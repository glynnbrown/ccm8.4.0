﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using System.Data.OleDb;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.Msp.Scripts;

namespace Galleria.Ccm.Legacy.Dal.Msp.Implementation
{
    public class PlanPositionDal : DalBase, IPlanPositionDal
    {
        private PlanPositionDto GetDataTransferObject(OleDbDataReader dr)
        {
            return new PlanPositionDto()
            {
                Position_Code = GetInt32(dr["Position_Code"]),
                Plan_Element_Code = GetInt32(dr["Plan_Element_Code"]),
                Product_Code = GetInt32(dr["Product_Code"]),
                OrientationID = GetString(dr["Orientation"]),

                X_Pos = GetDouble(dr["X_Pos_"]),
                Y_Pos = GetDouble(dr["Y_Pos_"]),
                Forward_Face = GetInt32(dr["Forward_Face_"]),
                Rotation = GetInt32(dr["Rotation_"]),

                Height = GetDouble(dr["Height_"]),
                Width = GetDouble(dr["Width_"]),
                Depth = GetDouble(dr["Depth_"]),
                SqueezeHeight = GetDouble(dr["SqueezeHeight"]),
                SqueezeWidth = GetDouble(dr["SqueezeWidth"]),
                Block_Height = GetDouble(dr["Block_Height_"]),
                Block_Width = GetDouble(dr["Block_Width_"]),
                Block_Top = GetDouble(dr["Block_Top_"]),
                Leading_Divider = GetDouble(dr["Leading_Divider_"]),
                Leading_Gap = GetDouble(dr["Leading_Gap_"]),
                Leading_Gap_Vert = GetDouble(dr["Leading_Gap_Vert_"]),

                No_High = GetInt32(dr["No_High_"]),
                No_Wide = GetInt32(dr["No_Wide_"]),
                No_Deep = GetInt32(dr["No_Deep_"]),
                Units = GetInt32(dr["Units_"]),
                Cap_Bot_Deep = GetInt32(dr["Cap_Bot_Deep_"]),
                Cap_Bot_High = GetInt32(dr["Cap_Bot_High_"]),
                Cap_Lft_Deep = GetInt32(dr["Cap_Lft_Deep_"]),
                Cap_Lft_Wide = GetInt32(dr["Cap_Lft_Wide_"]),
                Cap_Rgt_Deep = GetInt32(dr["Cap_Rgt_Deep_"]),
                Cap_Rgt_Wide = GetInt32(dr["Cap_Rgt_Wide_"]),
                No_Capping = GetInt32(dr["No_Capping_"]),
                No_Capping_Deep = GetInt32(dr["No_Capping_Deep_"]),

                Tray_Rotation = GetInt32(dr["Tray_Rotation"]),
                Trays_Only = GetInt32(dr["Trays_Only_"]),
                Tray_Height = GetDouble(dr["Tray_Height_"]),
                Tray_Thick = GetDouble(dr["Tray_Thick_"]),
                Tray_Units_High = GetInt32(dr["Tray_Units_High"]),
                Tray_Units_Wide = GetInt32(dr["Tray_Units_Wide"]),
                Tray_Units_Deep = GetInt32(dr["Tray_Units_Deep"]),
                IsManuallyPlaced = GetInt32(dr["Manual_Merchandising_Styl"]),
            };
        }

        public List<PlanPositionDto> FetchByProposalCodeElementCode(Int32 proposalCode, Int32 elementCode)
        {
            List<PlanPositionDto> output = new List<PlanPositionDto>();
            try
            {
                using (OleDbCommand cmd = this.DalContext.Connection.CreateCommand())
                {
                    cmd.CommandText = PlanPosition.FetchByProposalCodeElementCode;
                   // cmd.Parameters.Add("@PlanProposalCode", OleDbType.Integer).Value = proposalCode;
                    cmd.Parameters.Add("@PlanElementCode", OleDbType.Integer).Value = elementCode;

                    using (OleDbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                output.Add(GetDataTransferObject(dr));
                            }
                        }
                    }
                }
            }
            catch (OleDbException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return output;
        }
    }
}
