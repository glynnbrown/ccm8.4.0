﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using System.Data.OleDb;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Legacy.Dal.Msp.Implementation
{
    public class PlanBayDal : DalBase, IPlanBayDal
    {

        private PlanBayDto GetDataTransferObject(OleDbDataReader dr)
        {
            return new PlanBayDto()
            {
                Plan_Bay_Code = GetInt32(dr["Plan_Bay_Code"]),
                Base_Colour = GetInt32(dr["Base_Colour_"]),

                Base_Depth = GetDouble(dr["Base_Depth_"]),
                Base_Height = GetDouble(dr["Base_Height_"]),
                Base_Width = GetDouble(dr["Base_Width_"]),
                Bay_Position = GetDouble(dr["Bay_Position_"]),

                Bay_Units = (String)(dr["Bay_Units_"]),

                Colour = GetInt32(dr["Colour_"]),

                Continuous_Shelf = (String)(dr["Continuous_Shelf_"]),

                Depth = GetDouble(dr["Depth_"]),
                Height = GetDouble(dr["Height_"]),
                Width = GetDouble(dr["Width_"]),
                FirstNotch = GetDouble(dr["First_Notch_"]),

                Name = (String)(dr["Name_"]),

                Notch_Spacing = GetDouble(dr["Notch_Spacing_"]),

                Number_Of_Bays = GetInt32(dr["Number_Of_Bays_"]),
                Plan_Proposal_Code = GetInt32(dr["Plan_Proposal_Code"]),
                Position_Relative = GetInt32(dr["Position_Relative_"]),
                Show_Peg_Holes = GetInt32(dr["Show_Peg_Holes_"]),

                Take_Off = GetDouble(dr["Take_Off_"]),

                Use_Notch_Spacing = GetInt32(dr["Use_Notch_Spacing_"]),

            };
        }

        public List<PlanBayDto> PlanBay_FetchByCode(Int32 proposalCode)
        {
            List<PlanBayDto> output = new List<PlanBayDto>();
            try
            {
                using (OleDbCommand cmd = this.DalContext.Connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT * " +
                                        " FROM Plan_Bay_ " +
                                        " WHERE Plan_Proposal_Code = @PlanProposalCode ORDER BY Bay_Position_ ASC";
                    cmd.Parameters.Add("@PlanProposalCode", OleDbType.Integer).Value = proposalCode;

                    using (OleDbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                output.Add(GetDataTransferObject(dr));
                            }
                        }
                    }
                }
            }
            catch (OleDbException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return output;
        }
    }
}
