﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.Msp;

using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Dal.Msp.Implementation
{
    public class PlanProcessDal : DalBase, IPlanProcessDal
    {
        public List<Int32> FetchPlanProposalCodes()
        {
            List<Int32> output = new List<Int32>();
            try
            {
                using (OleDbCommand cmd = this.DalContext.Connection.CreateCommand())
                {
                    cmd.CommandText = "SELECT Plan_Proposal_Code " +
                                        " FROM Plan_Proposal_ ";
                    
                    using (OleDbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                output.Add(Convert.ToInt32(dr["Plan_Proposal_Code"]));
                            }
                        }
                    }
                }
            }
            catch (OleDbException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return output;
        }
    }
}
