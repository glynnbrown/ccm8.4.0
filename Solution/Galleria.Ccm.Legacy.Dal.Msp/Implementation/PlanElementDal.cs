﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using System.Data.OleDb;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Legacy.Dal.Msp.Implementation
{
    class PlanElementDal : DalBase, IPlanElementDal
    {

        private PlanElementDto GetDataTransferObject(OleDbDataReader dr)
        {
            return new PlanElementDto()
            {
                Plan_Element_Code = GetInt32(dr["Plan_Element_Code"]),
                Active = GetInt32(dr["Active_"]),
                Colour = GetInt32(dr["Colour_"]),

                Combined_With = GetString(dr["Combined_With_"]),

                Dimension_0 = GetDouble(dr["Dimension_0_"]),
                Dimension_1 = GetDouble(dr["Dimension_1_"]),
                Dimension_2 = GetDouble(dr["Dimension_2_"]),
                Dimension_3 = GetDouble(dr["Dimension_3_"]),
                Dimension_4 = GetDouble(dr["Dimension_4_"]),
                Dimension_5 = GetDouble(dr["Dimension_5_"]),
                Dimension_6 = GetDouble(dr["Dimension_6_"]),
                Dimension_7 = GetDouble(dr["Dimension_7_"]),
                Dimension_8 = GetDouble(dr["Dimension_8_"]),
                Finger_Space = GetDouble(dr["Finger_Space_"]),

                Name = GetString(dr["Name_"]),

                Next_PE_Code = GetInt32(dr["Next_PE_Code"]),
                No_Shelves_Wide = GetInt32(dr["No_Shelves_Wide_"]),
                Plan_Bay_Code = GetInt32(dr["Plan_Bay_Code"]),
                Prev_PE_Code = GetInt32(dr["Prev_PE_Code"]),
                Prnt_PE_Code = GetInt32(dr["Prnt_PE_Code"]),

                Slope = GetDouble(dr["Slope_"]),
                Space_Remaining = GetDouble(dr["Space_Remaining_"]),

                SplitType = GetInt32(dr["SplitType_"]),
                Sub_Type = GetInt32(dr["Sub_Type_"]),

                Tallest_Height = GetDouble(dr["Tallest_Height_"]),

                Type = GetString(dr["Type_"]),

                Use_Finger_Space = GetInt32(dr["Use_Finger_Space_"]),

                X_Position = GetDouble(dr["X_Position_"]),
                X_Scale = GetDouble(dr["X_Scale_"]),
                Y_Position = GetDouble(dr["Y_Position_"]),
                Y_Scale = GetDouble(dr["Y_Scale_"]),
                Z_Position = GetDouble(dr["Z_Position_"])

            };
        }

        private const String getElements =
            @"SELECT 
                    IIF(Plan_Element__InsideWall.Plan_Element_Code IS NULL, Plan_Element_.Plan_Element_Code, Plan_Element__InsideWall.Plan_Element_Code) AS Plan_Element_Code,
                    Plan_Element_.Active_,
                    Plan_Element_.Colour_,
                    Plan_Element_.Combined_With_,
                    Plan_Element_.Dimension_0_,
                    Plan_Element_.Dimension_1_,
                    Plan_Element_.Dimension_2_,
                    Plan_Element_.Dimension_3_,
                    Plan_Element_.Dimension_4_,
                    Plan_Element_.Dimension_5_,
                    Plan_Element_.Dimension_6_,
                    Plan_Element_.Dimension_7_,
                    Plan_Element_.Dimension_8_,
                    Plan_Element_.Finger_Space_,
                    Plan_Element_.Name_,
                    Plan_Element_.Next_PE_Code,
                    Plan_Element_.No_Shelves_Wide_,
                    Plan_Element_.Plan_Bay_Code,
                    Plan_Element_.Prev_PE_Code,
                    Plan_Element_.Prnt_PE_Code,
                    Plan_Element_.Slope_,
                    Plan_Element_.Space_Remaining_,
                    Plan_Element_.SplitType_,
                    Plan_Element_.Sub_Type_,
                    Plan_Element_.Tallest_Height_,
                    Plan_Element_.Type_,
                    Plan_Element_.Use_Finger_Space_,
                    Plan_Element_.X_Position_,
                    Plan_Element_.X_Scale_,
                    Plan_Element_.Y_Position_,
                    Plan_Element_.Y_Scale_,
                    Plan_Element_.Z_Position_
                    FROM Plan_Element_  
                    LEFT JOIN Plan_Element_ AS Plan_Element__InsideWall 
	                    ON Plan_Element__InsideWall.Prnt_PE_Code = Plan_Element_.Plan_Element_Code
                    WHERE 
                    Plan_Element_.Prnt_PE_Code = 0 AND
                    Plan_Element_.Plan_Bay_Code = @PlanBayCode ";
        public List<PlanElementDto> PlanElement_FetchByBayCode(Int32 bayCode)
        {
            List<PlanElementDto> output = new List<PlanElementDto>();
            try
            {
                using (OleDbCommand cmd = this.DalContext.Connection.CreateCommand())
                {
                    cmd.CommandText = getElements;
                    cmd.Parameters.Add("@PlanBayCode", OleDbType.Integer).Value = bayCode;

                    using (OleDbDataReader dr = cmd.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                output.Add(GetDataTransferObject(dr));
                            }
                        }
                    }
                }
            }
            catch (OleDbException e)
            {
                DalExceptionHelper.ThrowException(e);
            }
            return output;
        }
    }
}
