﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion


using System;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using System.Data.OleDb;

namespace Galleria.Ccm.Legacy.Dal.Msp
{
    public class DalFactory : DalFactoryBase
    {
       
        #region Constructors
        public DalFactory() : base() { }
        public DalFactory(DalFactoryConfigElement dalFactoryConfig) : base(dalFactoryConfig) { }
        #endregion

        /// <summary>
        /// Creates a new dal context using the default configuration
        /// </summary>
        /// <returns>A new dal context</returns>
        public override IDalContext CreateContext()
        {
            return this.CreateContext(this.DalFactoryConfig);
        }

        /// <summary>
        /// Creates a new dal context using the provided configuration
        /// </summary>
        public override IDalContext CreateContext(DalFactoryConfigElement dalFactoryConfig)
        {
            return new DalContext(this.GetType().Assembly, dalFactoryConfig);
        }
    }
}