﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    [Serializable]
    public partial class PlanElementList : ModelList<PlanElementList, PlanElement>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //BusinessRules.AddRule(typeof(PlanElementList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.CategoryFetch.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class GetByProposalCodeFilePathCriteria : Csla.CriteriaBase<GetByProposalCodeFilePathCriteria>
        {
            public static readonly PropertyInfo<Int32> BayCodeProperty =
            RegisterProperty<Int32>(c => c.BayCode);
            public Int32 BayCode
            {
                get { return ReadProperty<Int32>(BayCodeProperty); }
            }

            public static readonly PropertyInfo<String> FilePathProperty =
            RegisterProperty<String>(c => c.FilePath);
            public String FilePath
            {
                get { return ReadProperty<String>(FilePathProperty); }
            }

            public GetByProposalCodeFilePathCriteria(Int32 bayCode, String elementCode)
            {
                LoadProperty<Int32>(BayCodeProperty, bayCode);
                LoadProperty<String>(FilePathProperty, elementCode);
            }
        }

        #endregion
    }
}
