﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.Msp;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class PlanPositionList
    {
        #region Constructors
        private PlanPositionList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanPositionList FetchByProposalCodeElementCode(Int32 proposalCode, Int32 elementCode,String filePath)
        {
            return DataPortal.Fetch<PlanPositionList>(new GetByProposalCodeElementCodeCriteria(proposalCode, elementCode, filePath));
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(GetByProposalCodeElementCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory(criteria.FilePath);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanPositionDal dal = dalContext.GetDal<IPlanPositionDal>())
                {
                    IEnumerable<PlanPositionDto> dtoList = dal.FetchByProposalCodeElementCode(criteria.ProposalCode, criteria.ElementCode);

                    if (dtoList != null)
                    {
                        foreach (PlanPositionDto dto in dtoList)
                        {
                            this.Add(PlanPosition.GetPlanPosition(dalContext, dto));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
