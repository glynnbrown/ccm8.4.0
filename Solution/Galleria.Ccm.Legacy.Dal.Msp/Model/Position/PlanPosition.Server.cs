﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class PlanPosition
    {
        #region Constructor
        private PlanPosition() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanPosition GetPlanPosition(IDalContext dalContext, PlanPositionDto dto)
        {
            return DataPortal.FetchChild<PlanPosition>(dalContext, dto);
        }
        #endregion
        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanPositionDto dto)
        {
            this.LoadProperty<Int32>(Position_CodeProperty, dto.Position_Code);
            this.LoadProperty<Int32>(Plan_Element_CodeProperty, dto.Plan_Element_Code);
            this.LoadProperty<Int32>(Product_CodeProperty, dto.Product_Code);
            this.LoadProperty<String>(OrientationIDProperty, dto.OrientationID);
            this.LoadProperty<Double>(X_PosProperty, dto.X_Pos);
            this.LoadProperty<Double>(Y_PosProperty, dto.Y_Pos);
            this.LoadProperty<Int32>(Forward_FaceProperty, dto.Forward_Face);
            this.LoadProperty<Int32>(RotationProperty, dto.Rotation);

            this.LoadProperty<Double>(HeightProperty, dto.Height);
            this.LoadProperty<Double>(WidthProperty, dto.Width);
            this.LoadProperty<Double>(DepthProperty, dto.Depth);
            this.LoadProperty<Double>(SqueezeHeightProperty, dto.SqueezeHeight);
            this.LoadProperty<Double>(SqueezeWidthProperty, dto.SqueezeWidth);
            this.LoadProperty<Double>(Block_HeightProperty, dto.Block_Height);
            this.LoadProperty<Double>(Block_WidthProperty, dto.Block_Width);
            this.LoadProperty<Double>(Block_TopProperty, dto.Block_Top);
            this.LoadProperty<Double>(Leading_DividerProperty, dto.Leading_Divider);
            this.LoadProperty<Double>(Leading_GapProperty, dto.Leading_Gap);
            this.LoadProperty<Double>(Leading_Gap_VertProperty, dto.Leading_Gap_Vert);

            this.LoadProperty<Int32>(No_HighProperty, dto.No_High);
            this.LoadProperty<Int32>(No_WideProperty, dto.No_Wide);
            this.LoadProperty<Int32>(No_DeepProperty, dto.No_Deep);
            this.LoadProperty<Int32>(UnitsProperty, dto.Units);
            this.LoadProperty<Int32>(Cap_Bot_DeepProperty, dto.Cap_Bot_Deep);
            this.LoadProperty<Int32>(Cap_Bot_HighProperty, dto.Cap_Bot_High);
            this.LoadProperty<Int32>(Cap_Lft_DeepProperty, dto.Cap_Lft_Deep);
            this.LoadProperty<Int32>(Cap_Lft_WideProperty, dto.Cap_Lft_Wide);
            this.LoadProperty<Int32>(Cap_Rgt_DeepProperty, dto.Cap_Rgt_Deep);
            this.LoadProperty<Int32>(Cap_Rgt_WideProperty, dto.Cap_Rgt_Wide);
            this.LoadProperty<Int32>(No_CappingProperty, dto.No_Capping);
            this.LoadProperty<Int32>(No_Capping_DeepProperty, dto.No_Capping_Deep);

            this.LoadProperty<Int32>(Tray_RotationProperty, dto.Tray_Rotation);
            this.LoadProperty<Int32>(Trays_OnlyProperty, dto.Trays_Only);
            this.LoadProperty<Double>(Tray_HeightProperty, dto.Tray_Height);
            this.LoadProperty<Double>(Tray_ThickProperty, dto.Tray_Thick);
            this.LoadProperty<Int32>(Tray_Units_HighProperty, dto.Tray_Units_High);
            this.LoadProperty<Int32>(Tray_Units_WideProperty, dto.Tray_Units_Wide);
            this.LoadProperty<Int32>(Tray_Units_DeepProperty, dto.Tray_Units_Deep);

            this.LoadProperty<Boolean>(IsManuallyPlacedProperty, dto.IsManuallyPlaced != 0);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanPositionDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion
    }
}
