﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    [Serializable]
    public partial class PlanPosition : ModelObject<PlanPosition>
    {

        #region properties
        /// <summary>
        /// Position_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Position_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Position_Code);
        public Int32 Position_Code
        {
            get { return GetProperty<Int32>(Position_CodeProperty); }
        }

        /// <summary>
        /// Plan_Element_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Plan_Element_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Plan_Element_Code);
        public Int32 Plan_Element_Code
        {
            get { return GetProperty<Int32>(Plan_Element_CodeProperty); }
        }

        /// <summary>
        /// Product_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Product_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Product_Code);
        public Int32 Product_Code
        {
            get { return GetProperty<Int32>(Product_CodeProperty); }
        }

        /// <summary>
        /// OrientationID
        /// </summary>
        public static readonly ModelPropertyInfo<String> OrientationIDProperty =
            RegisterModelProperty<String>(c => c.OrientationID);
        public String OrientationID
        {
            get { return GetProperty<String>(OrientationIDProperty); }
        }

        /// <summary>
        /// Forward_Face
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Forward_FaceProperty =
            RegisterModelProperty<Int32>(c => c.Forward_Face);
        public Int32 Forward_Face
        {
            get { return GetProperty<Int32>(Forward_FaceProperty); }
        }

        /// <summary>
        /// Rotation
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RotationProperty =
            RegisterModelProperty<Int32>(c => c.Rotation);
        public Int32 Rotation
        {
            get { return GetProperty<Int32>(RotationProperty); }
        }

        /// <summary>
        /// X_Pos
        /// </summary>
        public static readonly ModelPropertyInfo<Double> X_PosProperty =
            RegisterModelProperty<Double>(c => c.X_Pos);
        public Double X_Pos
        {
            get { return GetProperty<Double>(X_PosProperty); }
        }

        /// <summary>
        /// Y_Pos
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Y_PosProperty =
            RegisterModelProperty<Double>(c => c.Y_Pos);
        public Double Y_Pos
        {
            get { return GetProperty<Double>(Y_PosProperty); }
        }

        /// <summary>
        /// Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> HeightProperty =
            RegisterModelProperty<Double>(c => c.Height);
        public Double Height
        {
            get { return GetProperty<Double>(HeightProperty); }
        }

        /// <summary>
        /// Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> WidthProperty =
            RegisterModelProperty<Double>(c => c.Width);
        public Double Width
        {
            get { return GetProperty<Double>(WidthProperty); }
        }

        /// <summary>
        /// Depth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> DepthProperty =
            RegisterModelProperty<Double>(c => c.Depth);
        public Double Depth
        {
            get { return GetProperty<Double>(DepthProperty); }
        }

        /// <summary>
        /// SqueezeHeight
        /// </summary>
        public static readonly ModelPropertyInfo<Double> SqueezeHeightProperty =
            RegisterModelProperty<Double>(c => c.SqueezeHeight);
        public Double SqueezeHeight
        {
            get { return GetProperty<Double>(SqueezeHeightProperty); }
        }

        /// <summary>
        /// SqueezeWidth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> SqueezeWidthProperty =
            RegisterModelProperty<Double>(c => c.SqueezeWidth);
        public Double SqueezeWidth
        {
            get { return GetProperty<Double>(SqueezeWidthProperty); }
        }

        /// <summary>
        /// Block_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Block_HeightProperty =
            RegisterModelProperty<Double>(c => c.Block_Height);
        public Double Block_Height
        {
            get { return GetProperty<Double>(Block_HeightProperty); }
        }

        /// <summary>
        /// Block_Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Block_WidthProperty =
            RegisterModelProperty<Double>(c => c.Block_Width);
        public Double Block_Width
        {
            get { return GetProperty<Double>(Block_WidthProperty); }
        }

        /// <summary>
        /// Block_Top
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Block_TopProperty =
            RegisterModelProperty<Double>(c => c.Block_Top);
        public Double Block_Top
        {
            get { return GetProperty<Double>(Block_TopProperty); }
        }

        /// <summary>
        /// Leading_Divider
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Leading_DividerProperty =
            RegisterModelProperty<Double>(c => c.Leading_Divider);
        public Double Leading_Divider
        {
            get { return GetProperty<Double>(Leading_DividerProperty); }
        }

        /// <summary>
        /// Leading_Gap
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Leading_GapProperty =
            RegisterModelProperty<Double>(c => c.Leading_Gap);
        public Double Leading_Gap
        {
            get { return GetProperty<Double>(Leading_GapProperty); }
        }

        /// <summary>
        /// Leading_Gap_Vert
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Leading_Gap_VertProperty =
            RegisterModelProperty<Double>(c => c.Leading_Gap_Vert);
        public Double Leading_Gap_Vert
        {
            get { return GetProperty<Double>(Leading_Gap_VertProperty); }
        }


        /// <summary>
        /// No_High
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> No_HighProperty =
            RegisterModelProperty<Int32>(c => c.No_High);
        public Int32 No_High
        {
            get { return GetProperty<Int32>(No_HighProperty); }
        }

        /// <summary>
        /// No_Wide
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> No_WideProperty =
            RegisterModelProperty<Int32>(c => c.No_Wide);
        public Int32 No_Wide
        {
            get { return GetProperty<Int32>(No_WideProperty); }
        }

        /// <summary>
        /// No_Deep
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> No_DeepProperty =
            RegisterModelProperty<Int32>(c => c.No_Deep);
        public Int32 No_Deep
        {
            get { return GetProperty<Int32>(No_DeepProperty); }
        }

        /// <summary>
        /// Units
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> UnitsProperty =
            RegisterModelProperty<Int32>(c => c.Units);
        public Int32 Units
        {
            get { return GetProperty<Int32>(UnitsProperty); }
        }

        /// <summary>
        /// Cap_Bot_Deep
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Cap_Bot_DeepProperty =
            RegisterModelProperty<Int32>(c => c.Cap_Bot_Deep);
        public Int32 Cap_Bot_Deep
        {
            get { return GetProperty<Int32>(Cap_Bot_DeepProperty); }
        }

        /// <summary>
        /// Cap_Bot_High
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Cap_Bot_HighProperty =
            RegisterModelProperty<Int32>(c => c.Cap_Bot_High);
        public Int32 Cap_Bot_High
        {
            get { return GetProperty<Int32>(Cap_Bot_HighProperty); }
        }

        /// <summary>
        /// Cap_Lft_Deep
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Cap_Lft_DeepProperty =
            RegisterModelProperty<Int32>(c => c.Cap_Lft_Deep);
        public Int32 Cap_Lft_Deep
        {
            get { return GetProperty<Int32>(Cap_Lft_DeepProperty); }
        }

        /// <summary>
        /// Cap_Lft_Wide
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Cap_Lft_WideProperty =
            RegisterModelProperty<Int32>(c => c.Cap_Lft_Wide);
        public Int32 Cap_Lft_Wide
        {
            get { return GetProperty<Int32>(Cap_Lft_WideProperty); }
        }

        /// <summary>
        /// Cap_Rgt_Deep
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Cap_Rgt_DeepProperty =
            RegisterModelProperty<Int32>(c => c.Cap_Rgt_Deep);
        public Int32 Cap_Rgt_Deep
        {
            get { return GetProperty<Int32>(Cap_Rgt_DeepProperty); }
        }

        /// <summary>
        /// Cap_Rgt_Wide
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Cap_Rgt_WideProperty =
            RegisterModelProperty<Int32>(c => c.Cap_Rgt_Wide);
        public Int32 Cap_Rgt_Wide
        {
            get { return GetProperty<Int32>(Cap_Rgt_WideProperty); }
        }

        /// <summary>
        /// No_Capping
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> No_CappingProperty =
            RegisterModelProperty<Int32>(c => c.No_Capping);
        public Int32 No_Capping
        {
            get { return GetProperty<Int32>(No_CappingProperty); }
        }

        /// <summary>
        /// No_Capping_Deep
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> No_Capping_DeepProperty =
            RegisterModelProperty<Int32>(c => c.No_Capping_Deep);
        public Int32 No_Capping_Deep
        {
            get { return GetProperty<Int32>(No_Capping_DeepProperty); }
        }


        /// <summary>
        /// Tray_Rotation
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Tray_RotationProperty =
            RegisterModelProperty<Int32>(c => c.Tray_Rotation);
        public Int32 Tray_Rotation
        {
            get { return GetProperty<Int32>(Tray_RotationProperty); }
        }

        /// <summary>
        /// Trays_Only
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Trays_OnlyProperty =
            RegisterModelProperty<Int32>(c => c.Trays_Only);
        public Int32 Trays_Only
        {
            get { return GetProperty<Int32>(Trays_OnlyProperty); }
        }

        /// <summary>
        /// Tray_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Tray_HeightProperty =
            RegisterModelProperty<Double>(c => c.Tray_Height);
        public Double Tray_Height
        {
            get { return GetProperty<Double>(Tray_HeightProperty); }
        }

        /// <summary>
        /// Tray_Thick
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Tray_ThickProperty =
            RegisterModelProperty<Double>(c => c.Tray_Thick);
        public Double Tray_Thick
        {
            get { return GetProperty<Double>(Tray_ThickProperty); }
        }

        /// <summary>
        /// Tray_Units_High
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Tray_Units_HighProperty =
            RegisterModelProperty<Int32>(c => c.Tray_Units_High);
        public Int32 Tray_Units_High
        {
            get { return GetProperty<Int32>(Tray_Units_HighProperty); }
        }

        /// <summary>
        /// Tray_Units_Wide
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Tray_Units_WideProperty =
            RegisterModelProperty<Int32>(c => c.Tray_Units_Wide);
        public Int32 Tray_Units_Wide
        {
            get { return GetProperty<Int32>(Tray_Units_WideProperty); }
        }

        /// <summary>
        /// Tray_Units_Deep
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Tray_Units_DeepProperty =
            RegisterModelProperty<Int32>(c => c.Tray_Units_Deep);
        public Int32 Tray_Units_Deep
        {
            get { return GetProperty<Int32>(Tray_Units_DeepProperty); }
        }

        /// <summary>
        /// IsManuallyPlaced
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsManuallyPlacedProperty =
            RegisterModelProperty<Boolean>(c => c.IsManuallyPlaced);
        public Boolean IsManuallyPlaced
        {
            get { return GetProperty<Boolean>(IsManuallyPlacedProperty); }
        }
        #endregion

    }
}
