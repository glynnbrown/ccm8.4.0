﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    [Serializable]
    public partial class PlanPositionList : ModelList<PlanPositionList, PlanPosition>
    {
        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class GetByProposalCodeElementCodeCriteria : Csla.CriteriaBase<GetByProposalCodeElementCodeCriteria>
        {
            public static readonly PropertyInfo<Int32> ProposalCodeProperty =
            RegisterProperty<Int32>(c => c.ProposalCode);
            public Int32 ProposalCode
            {
                get { return ReadProperty<Int32>(ProposalCodeProperty); }
            }

            public static readonly PropertyInfo<Int32> ElementCodeProperty =
            RegisterProperty<Int32>(c => c.ElementCode);
            public Int32 ElementCode
            {
                get { return ReadProperty<Int32>(ElementCodeProperty); }
            }

            public static readonly PropertyInfo<String> FilePathProperty =
            RegisterProperty<String>(c => c.FilePath);
            public String FilePath
            {
                get { return ReadProperty<String>(FilePathProperty); }
            }

            public GetByProposalCodeElementCodeCriteria(Int32 proposalCode, Int32 elementCode, String filePath)
            {
                LoadProperty<Int32>(ProposalCodeProperty, proposalCode);
                LoadProperty<Int32>(ElementCodeProperty, elementCode);
                LoadProperty<String>(FilePathProperty, filePath);
            }
        }

        #endregion
    }
}
