﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.Msp;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class PlanElementList
    {
        #region Constructors
        private PlanElementList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanElementList GetPlanElementsByBayCode(Int32 bayCode, String filePath)
        {
            return DataPortal.Fetch<PlanElementList>(new GetByProposalCodeFilePathCriteria(bayCode, filePath));
        }
        #endregion

        #region Data Access


        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(GetByProposalCodeFilePathCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory(criteria.FilePath);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanElementDal dal = dalContext.GetDal<IPlanElementDal>())
                {
                    IEnumerable<PlanElementDto> dtoList = dal.PlanElement_FetchByBayCode(criteria.BayCode);
                    if (dtoList != null)
                    {
                        foreach (PlanElementDto dto in dtoList)
                        {
                            
                            this.Add(PlanElement.GetPlanElement(dalContext, dto, criteria.FilePath));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
