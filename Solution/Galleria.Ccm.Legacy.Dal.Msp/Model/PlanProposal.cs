﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    [Serializable]
    public partial class PlanProposal : ModelObject<PlanProposal>
    {
        #region Properties
        /// <summary>
        /// FielPath
        /// </summary>
        public static readonly ModelPropertyInfo<String> FilePathProperty =
            RegisterModelProperty<String>(c => c.FilePath);
        public String FilePath
        {
            get { return GetProperty<String>(FilePathProperty); }
        }

        /// <summary>
        /// Plan_Proposal_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Plan_Proposal_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Plan_Proposal_Code);
        public Int32 Plan_Proposal_Code
        {
            get { return GetProperty<Int32>(Plan_Proposal_CodeProperty); }
        }

        /// <summary>
        /// Category_Market_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Category_Market_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Category_Market_Code);
        public Int32 Category_Market_Code
        {
            get { return GetProperty<Int32>(Category_Market_CodeProperty); }
        }

        /// <summary>
        /// Collisions
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> CollisionsProperty =
            RegisterModelProperty<Int32>(c => c.Collisions);
        public Int32 Collisions
        {
            get { return GetProperty<Int32>(CollisionsProperty); }
        }

        /// <summary>
        /// DimUnits
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> DimUnitsProperty =
            RegisterModelProperty<Int32>(c => c.DimUnits);
        public Int32 DimUnits
        {
            get { return GetProperty<Int32>(DimUnitsProperty); }
        }

        /// <summary>
        /// Fixture_Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> Fixture_NameProperty =
            RegisterModelProperty<String>(c => c.Fixture_Name);
        public String Fixture_Name
        {
            get { return GetProperty<String>(Fixture_NameProperty); }
        }

        /// <summary>
        /// FixtureElementEntry
        /// </summary>
        public static readonly ModelPropertyInfo<String> FixtureElementEntryProperty =
            RegisterModelProperty<String>(c => c.FixtureElementEntry);
        public String FixtureElementEntry
        {
            get { return GetProperty<String>(FixtureElementEntryProperty); }
        }

        /// <summary>
        /// Flags
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FlagsProperty =
            RegisterModelProperty<Int32>(c => c.Flags);
        public Int32 Flags
        {
            get { return GetProperty<Int32>(FlagsProperty); }
        }

        /// <summary>
        /// FloatingLabelEntry
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FloatingLabelEntryProperty =
            RegisterModelProperty<Int32>(c => c.FloatingLabelEntry);
        public Int32 FloatingLabelEntry
        {
            get { return GetProperty<Int32>(FloatingLabelEntryProperty); }
        }

        /// <summary>
        /// Footage
        /// </summary>
        public static readonly ModelPropertyInfo<Double> FootageProperty =
            RegisterModelProperty<Double>(c => c.Footage);
        public Double Footage
        {
            get { return GetProperty<Double>(FootageProperty); }
        }

        /// <summary>
        /// InfoBoxEntry
        /// </summary>
        public static readonly ModelPropertyInfo<String> InfoBoxEntryProperty =
            RegisterModelProperty<String>(c => c.InfoBoxEntry);
        public String InfoBoxEntry
        {
            get { return GetProperty<String>(InfoBoxEntryProperty); }
        }

        /// <summary>
        /// Max_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Max_HeightProperty =
            RegisterModelProperty<Double>(c => c.Max_Height);
        public Double Max_Height
        {
            get { return GetProperty<Double>(Max_HeightProperty); }
        }

        /// <summary>
        /// Max_Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Max_WidthProperty =
            RegisterModelProperty<Double>(c => c.Max_Width);
        public Double Max_Width
        {
            get { return GetProperty<Double>(Max_WidthProperty); }
        }


        /// <summary>
        /// Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// No_Stores
        /// </summary>
        public static readonly ModelPropertyInfo<Double> No_StoresProperty =
            RegisterModelProperty<Double>(c => c.No_Stores);
        public Double No_Stores
        {
            get { return GetProperty<Double>(No_StoresProperty); }
        }

        /// <summary>
        /// Nominal_Size
        /// </summary>
        public static readonly ModelPropertyInfo<String> Nominal_SizeProperty =
            RegisterModelProperty<String>(c => c.Nominal_Size);
        public String Nominal_Size
        {
            get { return GetProperty<String>(Nominal_SizeProperty); }
        }

        /// <summary>
        /// Notes
        /// </summary>
        public static readonly ModelPropertyInfo<String> NotesProperty =
            RegisterModelProperty<String>(c => c.Notes);
        public String Notes
        {
            get { return GetProperty<String>(NotesProperty); }
        }
        
        /// <summary>
        /// ProductEntry
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductEntryProperty =
            RegisterModelProperty<String>(c => c.ProductEntry);
        public String ProductEntry
        {
            get { return GetProperty<String>(ProductEntryProperty); }
        }

        /// <summary>
        /// ProductListEntry
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductListEntryProperty =
            RegisterModelProperty<String>(c => c.ProductListEntry);
        public String ProductListEntry
        {
            get { return GetProperty<String>(ProductListEntryProperty); }
        }

        /// <summary>
        /// Shelf_Linear
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Shelf_LinearProperty =
            RegisterModelProperty<Double>(c => c.Shelf_Linear);
        public Double Shelf_Linear
        {
            get { return GetProperty<Double>(Shelf_LinearProperty); }
        }

        /// <summary>
        /// Title
        /// </summary>
        public static readonly ModelPropertyInfo<String> TitleProperty =
            RegisterModelProperty<String>(c => c.Title);
        public String Title
        {
            get { return GetProperty<String>(TitleProperty); }
        }

        /// <summary>
        /// Traffic_Flow
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Traffic_FlowProperty =
            RegisterModelProperty<Int32>(c => c.Traffic_Flow);
        public Int32 Traffic_Flow
        {
            get { return GetProperty<Int32>(Traffic_FlowProperty); }
        }

        /// <summary>
        /// User_Number_1
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_1Property =
            RegisterModelProperty<Double>(c => c.User_Number_1);
        public Double User_Number_1
        {
            get { return GetProperty<Double>(User_Number_1Property); }
        }

        /// <summary>
        /// User_Number_2
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_2Property =
            RegisterModelProperty<Double>(c => c.User_Number_2);
        public Double User_Number_2
        {
            get { return GetProperty<Double>(User_Number_2Property); }
        }

        /// <summary>
        /// User_Number_3
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_3Property =
            RegisterModelProperty<Double>(c => c.User_Number_3);
        public Double User_Number_3
        {
            get { return GetProperty<Double>(User_Number_3Property); }
        }

        /// <summary>
        /// User_Number_4
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_4Property =
            RegisterModelProperty<Double>(c => c.User_Number_4);
        public Double User_Number_4
        {
            get { return GetProperty<Double>(User_Number_4Property); }
        }

        /// <summary>
        /// User_Number_5
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_5Property =
            RegisterModelProperty<Double>(c => c.User_Number_5);
        public Double User_Number_5
        {
            get { return GetProperty<Double>(User_Number_5Property); }
        }

        /// <summary>
        /// User_String_1
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_1Property =
            RegisterModelProperty<String>(c => c.User_String_1);
        public String User_String_1
        {
            get { return GetProperty<String>(User_String_1Property); }
        }
        /// <summary>
        /// User_String_2
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_2Property =
            RegisterModelProperty<String>(c => c.User_String_2);
        public String User_String_2
        {
            get { return GetProperty<String>(User_String_2Property); }
        }

        /// <summary>
        /// Vert_Traffic_Flow
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Vert_Traffic_FlowProperty =
            RegisterModelProperty<Int32>(c => c.Vert_Traffic_Flow);
        public Int32 Vert_Traffic_Flow
        {
            get { return GetProperty<Int32>(Vert_Traffic_FlowProperty); }
        }
        /// <summary>
        /// PlanId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanId);
        public Int32 PlanId
        {
            get { return GetProperty<Int32>(PlanIdProperty); }
        }
        #endregion

        #region List Properties
        public static readonly ModelPropertyInfo<PlanBayList> PlanBaysProperty =
                RegisterModelProperty<PlanBayList>(c => c.PlanBays);
        public PlanBayList PlanBays
        {
            get
            {
                return GetProperty<PlanBayList>(PlanBaysProperty);
            }
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class GetByProposalCodeFilePathCriteria : Csla.CriteriaBase<GetByProposalCodeFilePathCriteria>
        {
            public static readonly PropertyInfo<Int32> ProposalCodeProperty =
            RegisterProperty<Int32>(c => c.ProposalCode);
            public Int32 ProposalCode
            {
                get { return ReadProperty<Int32>(ProposalCodeProperty); }
            }

            public static readonly PropertyInfo<String> FilePathProperty =
            RegisterProperty<String>(c => c.FilePath);
            public String FilePath
            {
                get { return ReadProperty<String>(FilePathProperty); }
            }

            public GetByProposalCodeFilePathCriteria(Int32 proposalCode, String elementCode)
            {
                LoadProperty<Int32>(ProposalCodeProperty, proposalCode);
                LoadProperty<String>(FilePathProperty, elementCode);
            }
        }

        #endregion
    }
}
