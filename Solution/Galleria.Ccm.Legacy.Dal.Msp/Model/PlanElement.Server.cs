﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class PlanElement
    {
        #region Constructor
        private PlanElement() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanElement GetPlanElement(IDalContext dalContext, PlanElementDto dto, String filePath)
        {
            return DataPortal.FetchChild<PlanElement>(dalContext, dto, filePath);
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanElementDto dto, String filePath)
        {
            this.LoadProperty<Int32>(Plan_Element_CodeProperty, dto.Plan_Element_Code);
            this.LoadProperty<Int32>(ActiveProperty, dto.Active);
            this.LoadProperty<Int32>(ColourProperty, dto.Colour);

            this.LoadProperty<String>(Combined_WithProperty, dto.Combined_With);

            this.LoadProperty<Double>(Dimension_0Property, dto.Dimension_0);
            this.LoadProperty<Double>(Dimension_1Property, dto.Dimension_1);
            this.LoadProperty<Double>(Dimension_2Property, dto.Dimension_2);
            this.LoadProperty<Double>(Dimension_3Property, dto.Dimension_3);
            this.LoadProperty<Double>(Dimension_4Property, dto.Dimension_4);
            this.LoadProperty<Double>(Dimension_5Property, dto.Dimension_5);
            this.LoadProperty<Double>(Dimension_6Property, dto.Dimension_6);
            this.LoadProperty<Double>(Dimension_7Property, dto.Dimension_7);
            this.LoadProperty<Double>(Dimension_8Property, dto.Dimension_8);
            this.LoadProperty<Double>(Finger_SpaceProperty, dto.Finger_Space);

            this.LoadProperty<String>(NameProperty, dto.Name);

            this.LoadProperty<Int32>(Next_PE_CodeProperty, dto.Next_PE_Code);
            this.LoadProperty<Int32>(No_Shelves_WideProperty, dto.No_Shelves_Wide);
            this.LoadProperty<Int32>(Plan_Bay_CodeProperty, dto.Plan_Bay_Code);
            this.LoadProperty<Int32>(Prev_PE_CodeProperty, dto.Prev_PE_Code);
            this.LoadProperty<Int32>(Prnt_PE_CodeProperty, dto.Prnt_PE_Code);

            this.LoadProperty<Double>(SlopeProperty, dto.Slope);
            this.LoadProperty<Double>(Space_RemainingProperty, dto.Space_Remaining);

            this.LoadProperty<Int32>(SplitTypeProperty, dto.SplitType);
            this.LoadProperty<Int32>(Sub_TypeProperty, dto.Sub_Type);

            this.LoadProperty<Double>(Tallest_HeightProperty, dto.Tallest_Height);

            this.LoadProperty<String>(TypeProperty, dto.Type);

            this.LoadProperty<Int32>(Use_Finger_SpaceProperty, dto.Use_Finger_Space);

            this.LoadProperty<Double>(X_PositionProperty, dto.X_Position);
            this.LoadProperty<Double>(X_ScaleProperty, dto.X_Scale);
            this.LoadProperty<Double>(Y_PositionProperty, dto.Y_Position);
            this.LoadProperty<Double>(Y_ScaleProperty, dto.Y_Scale);
            this.LoadProperty<Double>(Z_PositionProperty, dto.Z_Position);

            this.LoadProperty<Int32>(OriginalPlanElementCodeProperty, dto.OriginalPlanElementCode);
            this.LoadProperty<Int32>(PlanIDProperty, dto.PlanID);

            this.LoadProperty<Byte>(StatusProperty, dto.Status);

            this.LoadProperty<PlanPositionList>(PositionsProperty, PlanPositionList.FetchByProposalCodeElementCode(0, dto.Plan_Element_Code, filePath));
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanElementDto dto, String filePath)
        {
            this.LoadDataTransferObject(dalContext, dto, filePath);
        }
        #endregion
    }
}
