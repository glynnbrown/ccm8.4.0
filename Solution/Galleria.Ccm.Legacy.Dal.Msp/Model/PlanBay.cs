﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    [Serializable]
    public partial class PlanBay : ModelObject<PlanBay>
    {
        #region Properties
        /// <summary>
        /// Plan_Bay_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Plan_Bay_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Plan_Bay_Code);
        public Int32 Plan_Bay_Code
        {
            get { return GetProperty<Int32>(Plan_Bay_CodeProperty); }
        }

        /// <summary>
        /// Base_Colour
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Base_ColourProperty =
            RegisterModelProperty<Int32>(c => c.Base_Colour);
        public Int32 Base_Colour
        {
            get { return GetProperty<Int32>(Base_ColourProperty); }
        }

        /// <summary>
        /// Base_Depth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Base_DepthProperty =
            RegisterModelProperty<Double>(c => c.Base_Depth);
        public Double Base_Depth
        {
            get { return GetProperty<Double>(Base_DepthProperty); }
        }

        /// <summary>
        /// Base_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Base_HeightProperty =
            RegisterModelProperty<Double>(c => c.Base_Height);
        public Double Base_Height
        {
            get { return GetProperty<Double>(Base_HeightProperty); }
        }

        /// <summary>
        /// Base_Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Base_WidthProperty =
            RegisterModelProperty<Double>(c => c.Base_Width);
        public Double Base_Width
        {
            get { return GetProperty<Double>(Base_WidthProperty); }
        }

        /// <summary>
        /// Bay_Position
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Bay_PositionProperty =
            RegisterModelProperty<Double>(c => c.Bay_Position);
        public Double Bay_Position
        {
            get { return GetProperty<Double>(Bay_PositionProperty); }
        }

        /// <summary>
        /// Bay_Units
        /// </summary>
        public static readonly ModelPropertyInfo<String> Bay_UnitsProperty =
            RegisterModelProperty<String>(c => c.Bay_Units);
        public String Bay_Units
        {
            get { return GetProperty<String>(Bay_UnitsProperty); }
        }

        /// <summary>
        /// Colour
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ColourProperty =
            RegisterModelProperty<Int32>(c => c.Colour);
        public Int32 Colour
        {
            get { return GetProperty<Int32>(ColourProperty); }
        }

        /// <summary>
        /// Continuous_Shelf
        /// </summary>
        public static readonly ModelPropertyInfo<String> Continuous_ShelfProperty =
            RegisterModelProperty<String>(c => c.Continuous_Shelf);
        public String Continuous_Shelf
        {
            get { return GetProperty<String>(Continuous_ShelfProperty); }
        }

        /// <summary>
        /// Depth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> DepthProperty =
            RegisterModelProperty<Double>(c => c.Depth);
        public Double Depth
        {
            get { return GetProperty<Double>(DepthProperty); }
        }

        /// <summary>
        /// Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> HeightProperty =
            RegisterModelProperty<Double>(c => c.Height);
        public Double Height
        {
            get { return GetProperty<Double>(HeightProperty); }
        }
        
        /// <summary>
        /// Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> WidthProperty =
            RegisterModelProperty<Double>(c => c.Width);
        public Double Width
        {
            get { return GetProperty<Double>(WidthProperty); }
        }

        /// <summary>
        /// FirstNotch
        /// </summary>
        public static readonly ModelPropertyInfo<Double> FirstNotchProperty =
            RegisterModelProperty<Double>(c => c.FirstNotch);
        public Double FirstNotch
        {
            get { return GetProperty<Double>(FirstNotchProperty); }
        }

        /// <summary>
        /// Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// Notch_Spacing
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Notch_SpacingProperty =
            RegisterModelProperty<Double>(c => c.Notch_Spacing);
        public Double Notch_Spacing
        {
            get { return GetProperty<Double>(Notch_SpacingProperty); }
        }

        /// <summary>
        /// Number_Of_Bays
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Number_Of_BaysProperty =
            RegisterModelProperty<Int32>(c => c.Number_Of_Bays);
        public Int32 Number_Of_Bays
        {
            get { return GetProperty<Int32>(Number_Of_BaysProperty); }
        }

        /// <summary>
        /// Plan_Proposal_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Plan_Proposal_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Plan_Proposal_Code);
        public Int32 Plan_Proposal_Code
        {
            get { return GetProperty<Int32>(Plan_Proposal_CodeProperty); }
        }

        /// <summary>
        /// Position_Relative
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Position_RelativeProperty =
            RegisterModelProperty<Int32>(c => c.Position_Relative);
        public Int32 Position_Relative
        {
            get { return GetProperty<Int32>(Position_RelativeProperty); }
        }

        /// <summary>
        /// Show_Peg_Holes
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Show_Peg_HolesProperty =
            RegisterModelProperty<Int32>(c => c.Show_Peg_Holes);
        public Int32 Show_Peg_Holes
        {
            get { return GetProperty<Int32>(Show_Peg_HolesProperty); }
        }

        /// <summary>
        /// Take_Off
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Take_OffProperty =
            RegisterModelProperty<Double>(c => c.Take_Off);
        public Double Take_Off
        {
            get { return GetProperty<Double>(Take_OffProperty); }
        }

        /// <summary>
        /// Use_Notch_Spacing
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Use_Notch_SpacingProperty =
            RegisterModelProperty<Int32>(c => c.Use_Notch_Spacing);
        public Int32 Use_Notch_Spacing
        {
            get { return GetProperty<Int32>(Use_Notch_SpacingProperty); }
        }

        /// <summary>
        /// OriginalPlanBayCode
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> OriginalPlanBayCodeProperty =
            RegisterModelProperty<Int32>(c => c.OriginalPlanBayCode);
        public Int32 OriginalPlanBayCode
        {
            get { return GetProperty<Int32>(OriginalPlanBayCodeProperty); }
        }

        /// <summary>
        /// PlanId
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanId);
        public Int32 PlanId
        {
            get { return GetProperty<Int32>(PlanIdProperty); }
        }

        /// <summary>
        /// Status
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> StatusProperty =
            RegisterModelProperty<Byte>(c => c.Status);
        public Byte Status
        {
            get { return GetProperty<Byte>(StatusProperty); }
        }

        #endregion

        #region List Properties
        public static readonly ModelPropertyInfo<PlanElementList> PlanElementsProperty =
                RegisterModelProperty<PlanElementList>(c => c.PlanElements);
        public PlanElementList PlanElements
        {
            get
            {
                return GetProperty<PlanElementList>(PlanElementsProperty);
            }
        }
        #endregion
    }
}
