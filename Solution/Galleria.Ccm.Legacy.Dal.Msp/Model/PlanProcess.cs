﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    public class PlanProcess
    {
        #region Constructor
        private PlanProcess()
        {
        }
        #endregion

        public static List<Int32> FetchPlanProposalCodes(String filePath)
        {

            List<Int32> dtoList = new List<Int32>();
            IDalFactory dalFactory = DalContainer.GetDalFactory(filePath);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanProcessDal dal = dalContext.GetDal<IPlanProcessDal>())
                {
                    dtoList.AddRange(dal.FetchPlanProposalCodes());
                }
            }
            return dtoList;
        }
    }
}
