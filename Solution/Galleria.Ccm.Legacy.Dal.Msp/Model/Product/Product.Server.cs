﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class Product
    {
        #region Constructor
        private Product() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static Product GetProduct(IDalContext dalContext, ProductDto dto)
        {
            return DataPortal.FetchChild<Product>(dalContext, dto);
        }
        #endregion
        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, ProductDto dto)
        {
            this.LoadProperty<Int32>(Product_CodeProperty, dto.Product_Code);
            this.LoadProperty<Int32>(Baskets_CodeProperty, dto.Baskets_Code);
            this.LoadProperty<Int32>(Case_Pack_CodeProperty, dto.Case_Pack_Code);
            this.LoadProperty<Int32>(Category_CodeProperty, dto.Category_Code);
            this.LoadProperty<Int32>(Merchandising_Style_CodeProperty, dto.Merchandising_Style_Code);
            this.LoadProperty<Int32>(Description_A_CodeProperty, dto.Description_A_Code);
            this.LoadProperty<Int32>(Dimension_Units_CodeProperty, dto.Dimension_Units_Code);
            this.LoadProperty<Int32>(Group_Parent_CodeProperty, dto.Group_Parent_Code);
            this.LoadProperty<Int32>(Inventory_Rules_CodeProperty, dto.Inventory_Rules_Code);
            this.LoadProperty<Int32>(Key_Field_CodeProperty, dto.Key_Field_Code);
            this.LoadProperty<Int32>(Manufacturer_CodeProperty, dto.Manufacturer_Code);
            this.LoadProperty<Int32>(Pallets_CodeProperty, dto.Pallets_Code);
            this.LoadProperty<Int32>(Product_Group_CodeProperty, dto.Product_Group_Code);
            this.LoadProperty<Int32>(Size_CodeProperty, dto.Size_Code);
            this.LoadProperty<Int32>(Squeeze_Factor_CodeProperty, dto.Squeeze_Factor_Code);
            this.LoadProperty<Int32>(Sub_Category_CodeProperty, dto.Sub_Category_Code);
            this.LoadProperty<Int32>(Section_CodeProperty, dto.Section_Code);
            this.LoadProperty<Int32>(Sub_Section_CodeProperty, dto.Sub_Section_Code);
            this.LoadProperty<Int32>(Tray_Pack_CodeProperty, dto.Tray_Pack_Code);
            this.LoadProperty<Int32>(VAT_CodeProperty, dto.VAT_Code);


            this.LoadProperty<String>(Prod_IDProperty, dto.Prod_ID);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(CodeProperty, dto.Code);
            this.LoadProperty<String>(EANProperty, dto.EAN);

            this.LoadProperty<Int32>(ColourProperty, dto.Colour);
            this.LoadProperty<Int32>(Pallets_Units_HighProperty, dto.Pallets_Units_High);

            this.LoadProperty<Double>(Cost_PriceProperty, dto.Cost_Price);
            this.LoadProperty<Double>(HeightProperty, dto.Height);
            this.LoadProperty<Double>(WidthProperty, dto.Width);
            this.LoadProperty<Double>(DepthProperty, dto.Depth);
            this.LoadProperty<Double>(G_ProfitProperty, dto.G_Profit);
            this.LoadProperty<Double>(Left_Nest_HeightProperty, dto.Left_Nest_Height);
            this.LoadProperty<Double>(Left_Nest_Top_OffsetProperty, dto.Left_Nest_Top_Offset);
            this.LoadProperty<Double>(Left_Nest_WidthProperty, dto.Left_Nest_Width);
            this.LoadProperty<Double>(Right_Nest_HeightProperty, dto.Right_Nest_Height);
            this.LoadProperty<Double>(Right_Nest_Top_OffsetProperty, dto.Right_Nest_Top_Offset);
            this.LoadProperty<Double>(Right_Nest_WidthProperty, dto.Right_Nest_Width);
            this.LoadProperty<Double>(MarginProperty, dto.Margin);
            this.LoadProperty<Double>(MovementProperty, dto.Movement);
            this.LoadProperty<Double>(Nesting_DepthProperty, dto.Nesting_Depth);
            this.LoadProperty<Double>(Nesting_HeightProperty, dto.Nesting_Height);
            this.LoadProperty<Double>(Nesting_WidthProperty, dto.Nesting_Width);
            this.LoadProperty<Double>(Peg_Centre_LeftProperty, dto.Peg_Centre_Left);
            this.LoadProperty<Double>(Peg_Centre_TopProperty, dto.Peg_Centre_Top);
            this.LoadProperty<Double>(PriceProperty, dto.Price);
            this.LoadProperty<Double>(Product_PSIProperty, dto.Product_PSI);
            this.LoadProperty<Double>(Rank_PSIProperty, dto.Rank_PSI);
            this.LoadProperty<Double>(SalesProperty, dto.Sales);
            this.LoadProperty<Double>(Section_PSIProperty, dto.Section_PSI);
            this.LoadProperty<Double>(Section_RankProperty, dto.Section_Rank);


            this.LoadProperty<String>(TrayPackNameProperty, dto.TrayPackName);
            this.LoadProperty<Int32>(RotationProperty, dto.Rotation);
            this.LoadProperty<Int32>(Trays_OnlyProperty, dto.Trays_Only);

            this.LoadProperty<Double>(Tray_HeightProperty, dto.Tray_Height);
            this.LoadProperty<Double>(Tray_ThickProperty, dto.Tray_Thick);

            this.LoadProperty<Int32>(Tray_Units_HighProperty, dto.Tray_Units_High);
            this.LoadProperty<Int32>(Tray_Units_WideProperty, dto.Tray_Units_Wide);
            this.LoadProperty<Int32>(Tray_Units_DeepProperty, dto.Tray_Units_Deep);
            
            this.LoadProperty<Double>(VatRateProperty, dto.VatRate);


            this.LoadProperty<Double>(User_Number_1Property, dto.User_Number_1);
            this.LoadProperty<Double>(User_Number_2Property, dto.User_Number_2);
            this.LoadProperty<Double>(User_Number_3Property, dto.User_Number_3);
            this.LoadProperty<Double>(User_Number_4Property, dto.User_Number_4);
            this.LoadProperty<Double>(User_Number_5Property, dto.User_Number_5);
            this.LoadProperty<Double>(User_Number_6Property, dto.User_Number_6);
            this.LoadProperty<Double>(User_Number_7Property, dto.User_Number_7);
            this.LoadProperty<Double>(User_Number_8Property, dto.User_Number_8);
            this.LoadProperty<Double>(User_Number_9Property, dto.User_Number_9);
            this.LoadProperty<Double>(User_Number_10Property, dto.User_Number_10);
            this.LoadProperty<Double>(User_Number_11Property, dto.User_Number_11);
            this.LoadProperty<Double>(User_Number_12Property, dto.User_Number_12);
            this.LoadProperty<Double>(User_Number_13Property, dto.User_Number_13);
            this.LoadProperty<Double>(User_Number_14Property, dto.User_Number_14);
            this.LoadProperty<Double>(User_Number_15Property, dto.User_Number_15);
            this.LoadProperty<Double>(User_Number_16Property, dto.User_Number_16);
            this.LoadProperty<Double>(User_Number_17Property, dto.User_Number_17);
            this.LoadProperty<Double>(User_Number_18Property, dto.User_Number_18);
            this.LoadProperty<Double>(User_Number_19Property, dto.User_Number_19);
            this.LoadProperty<Double>(User_Number_20Property, dto.User_Number_20);

            this.LoadProperty<String>(User_String_1Property, dto.User_String_1);
            this.LoadProperty<String>(User_String_2Property, dto.User_String_2);
            this.LoadProperty<String>(User_String_3Property, dto.User_String_3);
            this.LoadProperty<String>(User_String_4Property, dto.User_String_4);
            this.LoadProperty<String>(User_String_5Property, dto.User_String_5);
            this.LoadProperty<String>(User_String_6Property, dto.User_String_6);
            this.LoadProperty<String>(User_String_7Property, dto.User_String_7);
            this.LoadProperty<String>(User_String_8Property, dto.User_String_8);
            this.LoadProperty<String>(User_String_9Property, dto.User_String_9);
            this.LoadProperty<String>(User_String_10Property, dto.User_String_10);

            this.LoadProperty<String>(Sub_Category_NameProperty, dto.Sub_Category_Name);
            this.LoadProperty<String>(Manufacturer_NameProperty, dto.Manufacturer_Name);

            this.LoadProperty<Int32>(Max_CapProperty, dto.Max_Cap);
            this.LoadProperty<Int32>(Max_StackProperty, dto.Max_Stack);

            this.LoadProperty<Single>(Inventory_CasesProperty, dto.Inventory_Cases);
            
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, ProductDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion
    }
}
