﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.Msp;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class ProductList
    {
        #region Constructors
        private ProductList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all poducts
        /// </summary>
        /// <returns>A list of plans</returns>
        public static ProductList FetchAll()
        {
            return DataPortal.Fetch<ProductList>();
        }

        public static ProductList GetProductsProposalByCode(Int32 proposalCode, Boolean placedOnly, String filePath)
        {
            return DataPortal.Fetch<ProductList>(new GetByProposalCodeCriteria(proposalCode, placedOnly, filePath));
        }        
        #endregion

        #region Data Access

        /// <summary>
        /// Called when retrieving a list of all products
        /// </summary>
        private void DataPortal_Fetch()
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    IEnumerable<ProductDto> dtoList = dal.FetchAll();

                    if (dtoList != null)
                    {
                        foreach (ProductDto dto in dtoList)
                        {
                            this.Add(Product.GetProduct(dalContext, dto));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when retrieving a list of products
        /// </summary>
        private void DataPortal_Fetch(GetByProposalCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory(criteria.FilePath);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    IEnumerable<ProductDto> dtoList = criteria.PlacedOnly ?
                            dal.FetchPlacedByProposalCode(criteria.ProposalCode) :
                            dal.FetchByProposalCode(criteria.ProposalCode);

                    if (dtoList != null)
                    {
                        foreach (ProductDto dto in dtoList)
                        {
                            this.Add(Product.GetProduct(dalContext, dto));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
