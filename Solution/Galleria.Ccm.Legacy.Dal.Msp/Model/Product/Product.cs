﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    [Serializable]
    public partial class Product : ModelObject<Product>
    {
        #region Properties
        /// <summary>
        /// Product_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Product_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Product_Code);
        public Int32 Product_Code
        {
            get { return GetProperty<Int32>(Product_CodeProperty); }
        }

        /// <summary>
        /// Baskets_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Baskets_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Baskets_Code);
        public Int32 Baskets_Code
        {
            get { return GetProperty<Int32>(Baskets_CodeProperty); }
        }

        /// <summary>
        /// Case_Pack_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Case_Pack_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Case_Pack_Code);
        public Int32 Case_Pack_Code
        {
            get { return GetProperty<Int32>(Case_Pack_CodeProperty); }
        }

        /// <summary>
        /// Category_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Category_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Category_Code);
        public Int32 Category_Code
        {
            get { return GetProperty<Int32>(Category_CodeProperty); }
        }

        /// <summary>
        /// Merchandising_Style_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Merchandising_Style_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Merchandising_Style_Code);
        public Int32 Merchandising_Style_Code
        {
            get { return GetProperty<Int32>(Merchandising_Style_CodeProperty); }
        }

        /// <summary>
        /// Description_A_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Description_A_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Description_A_Code);
        public Int32 Description_A_Code
        {
            get { return GetProperty<Int32>(Description_A_CodeProperty); }
        }

        /// <summary>
        /// Dimension_Units_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Dimension_Units_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Dimension_Units_Code);
        public Int32 Dimension_Units_Code
        {
            get { return GetProperty<Int32>(Dimension_Units_CodeProperty); }
        }

        /// <summary>
        /// Group_Parent_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Group_Parent_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Group_Parent_Code);
        public Int32 Group_Parent_Code
        {
            get { return GetProperty<Int32>(Group_Parent_CodeProperty); }
        }

        /// <summary>
        /// Inventory_Rules_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Inventory_Rules_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Inventory_Rules_Code);
        public Int32 Inventory_Rules_Code
        {
            get { return GetProperty<Int32>(Inventory_Rules_CodeProperty); }
        }

        /// <summary>
        /// Key_Field_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Key_Field_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Key_Field_Code);
        public Int32 Key_Field_Code
        {
            get { return GetProperty<Int32>(Key_Field_CodeProperty); }
        }

        /// <summary>
        /// Manufacturer_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Manufacturer_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Manufacturer_Code);
        public Int32 Manufacturer_Code
        {
            get { return GetProperty<Int32>(Manufacturer_CodeProperty); }
        }

        /// <summary>
        /// Pallets_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Pallets_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Pallets_Code);
        public Int32 Pallets_Code
        {
            get { return GetProperty<Int32>(Pallets_CodeProperty); }
        }

        /// <summary>
        /// Product_Group_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Product_Group_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Product_Group_Code);
        public Int32 Product_Group_Code
        {
            get { return GetProperty<Int32>(Product_Group_CodeProperty); }
        }

        /// <summary>
        /// Size_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Size_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Size_Code);
        public Int32 Size_Code
        {
            get { return GetProperty<Int32>(Size_CodeProperty); }
        }

        /// <summary>
        /// Squeeze_Factor_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Squeeze_Factor_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Squeeze_Factor_Code);
        public Int32 Squeeze_Factor_Code
        {
            get { return GetProperty<Int32>(Squeeze_Factor_CodeProperty); }
        }

        /// <summary>
        /// Sub_Category_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Sub_Category_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Sub_Category_Code);
        public Int32 Sub_Category_Code
        {
            get { return GetProperty<Int32>(Sub_Category_CodeProperty); }
        }


        /// <summary>
        /// Section_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Section_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Section_Code);
        public Int32 Section_Code
        {
            get { return GetProperty<Int32>(Section_CodeProperty); }
        }

        /// <summary>
        /// Sub_Section_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Sub_Section_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Sub_Section_Code);
        public Int32 Sub_Section_Code
        {
            get { return GetProperty<Int32>(Sub_Section_CodeProperty); }
        }

        /// <summary>
        /// Tray_Pack_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Tray_Pack_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Tray_Pack_Code);
        public Int32 Tray_Pack_Code
        {
            get { return GetProperty<Int32>(Tray_Pack_CodeProperty); }
        }

        /// <summary>
        /// VAT_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> VAT_CodeProperty =
            RegisterModelProperty<Int32>(c => c.VAT_Code);
        public Int32 VAT_Code
        {
            get { return GetProperty<Int32>(VAT_CodeProperty); }
        }

        /// <summary>
        /// Prod_ID
        /// </summary>
        public static readonly ModelPropertyInfo<String> Prod_IDProperty =
            RegisterModelProperty<String>(c => c.Prod_ID);
        public String Prod_ID
        {
            get { return GetProperty<String>(Prod_IDProperty); }
        }

        /// <summary>
        /// Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// Code
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }

        /// <summary>
        /// EAN
        /// </summary>
        public static readonly ModelPropertyInfo<String> EANProperty =
            RegisterModelProperty<String>(c => c.EAN);
        public String EAN
        {
            get { return GetProperty<String>(EANProperty); }
        }

        /// <summary>
        /// Colour
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ColourProperty =
            RegisterModelProperty<Int32>(c => c.Colour);
        public Int32 Colour
        {
            get { return GetProperty<Int32>(ColourProperty); }
        }

        /// <summary>
        /// Pallets_Units_High
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Pallets_Units_HighProperty =
            RegisterModelProperty<Int32>(c => c.Pallets_Units_High);
        public Int32 Pallets_Units_High
        {
            get { return GetProperty<Int32>(Pallets_Units_HighProperty); }
        }

        /// <summary>
        /// Cost_Price
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Cost_PriceProperty =
            RegisterModelProperty<Double>(c => c.Cost_Price);
        public Double Cost_Price
        {
            get { return GetProperty<Double>(Cost_PriceProperty); }
        }

        /// <summary>
        /// Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> HeightProperty =
            RegisterModelProperty<Double>(c => c.Height);
        public Double Height
        {
            get { return GetProperty<Double>(HeightProperty); }
        }

        /// <summary>
        /// Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> WidthProperty =
            RegisterModelProperty<Double>(c => c.Width);
        public Double Width
        {
            get { return GetProperty<Double>(WidthProperty); }
        }

        /// <summary>
        /// Depth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> DepthProperty =
            RegisterModelProperty<Double>(c => c.Depth);
        public Double Depth
        {
            get { return GetProperty<Double>(DepthProperty); }
        }

        /// <summary>
        /// G_Profit
        /// </summary>
        public static readonly ModelPropertyInfo<Double> G_ProfitProperty =
            RegisterModelProperty<Double>(c => c.G_Profit);
        public Double G_Profit
        {
            get { return GetProperty<Double>(G_ProfitProperty); }
        }

        /// <summary>
        /// Left_Nest_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Left_Nest_HeightProperty =
            RegisterModelProperty<Double>(c => c.Left_Nest_Height);
        public Double Left_Nest_Height
        {
            get { return GetProperty<Double>(Left_Nest_HeightProperty); }
        }

        /// <summary>
        /// Left_Nest_Top_Offset
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Left_Nest_Top_OffsetProperty =
            RegisterModelProperty<Double>(c => c.Left_Nest_Top_Offset);
        public Double Left_Nest_Top_Offset
        {
            get { return GetProperty<Double>(Left_Nest_Top_OffsetProperty); }
        }
        
        /// <summary>
        /// Left_Nest_Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Left_Nest_WidthProperty =
            RegisterModelProperty<Double>(c => c.Left_Nest_Width);
        public Double Left_Nest_Width
        {
            get { return GetProperty<Double>(Left_Nest_WidthProperty); }
        }

        /// <summary>
        /// Right_Nest_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Right_Nest_HeightProperty =
            RegisterModelProperty<Double>(c => c.Right_Nest_Height);
        public Double Right_Nest_Height
        {
            get { return GetProperty<Double>(Right_Nest_HeightProperty); }
        }

        /// <summary>
        /// Right_Nest_Top_Offset
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Right_Nest_Top_OffsetProperty =
            RegisterModelProperty<Double>(c => c.Right_Nest_Top_Offset);
        public Double Right_Nest_Top_Offset
        {
            get { return GetProperty<Double>(Right_Nest_Top_OffsetProperty); }
        }

        /// <summary>
        /// Right_Nest_Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Right_Nest_WidthProperty =
            RegisterModelProperty<Double>(c => c.Right_Nest_Width);
        public Double Right_Nest_Width
        {
            get { return GetProperty<Double>(Right_Nest_WidthProperty); }
        }

        /// <summary>
        /// Margin
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MarginProperty =
            RegisterModelProperty<Double>(c => c.Margin);
        public Double Margin
        {
            get { return GetProperty<Double>(MarginProperty); }
        }

        /// <summary>
        /// Movement
        /// </summary>
        public static readonly ModelPropertyInfo<Double> MovementProperty =
            RegisterModelProperty<Double>(c => c.Movement);
        public Double Movement
        {
            get { return GetProperty<Double>(MovementProperty); }
        }

        /// <summary>
        /// Nesting_Depth
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Nesting_DepthProperty =
            RegisterModelProperty<Double>(c => c.Nesting_Depth);
        public Double Nesting_Depth
        {
            get { return GetProperty<Double>(Nesting_DepthProperty); }
        }

        /// <summary>
        /// Nesting_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Nesting_HeightProperty =
            RegisterModelProperty<Double>(c => c.Nesting_Height);
        public Double Nesting_Height
        {
            get { return GetProperty<Double>(Nesting_HeightProperty); }
        }

        /// <summary>
        /// Nesting_Width
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Nesting_WidthProperty =
            RegisterModelProperty<Double>(c => c.Nesting_Width);
        public Double Nesting_Width
        {
            get { return GetProperty<Double>(Nesting_WidthProperty); }
        }

        /// <summary>
        /// Peg_Centre_Left
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Peg_Centre_LeftProperty =
            RegisterModelProperty<Double>(c => c.Peg_Centre_Left);
        public Double Peg_Centre_Left
        {
            get { return GetProperty<Double>(Peg_Centre_LeftProperty); }
        }

        /// <summary>
        /// Peg_Centre_Top
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Peg_Centre_TopProperty =
            RegisterModelProperty<Double>(c => c.Peg_Centre_Top);
        public Double Peg_Centre_Top
        {
            get { return GetProperty<Double>(Peg_Centre_TopProperty); }
        }
        /// <summary>
        /// Price
        /// </summary>
        public static readonly ModelPropertyInfo<Double> PriceProperty =
            RegisterModelProperty<Double>(c => c.Price);
        public Double Price
        {
            get { return GetProperty<Double>(PriceProperty); }
        }

        /// <summary>
        /// Product_PSI
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Product_PSIProperty =
            RegisterModelProperty<Double>(c => c.Product_PSI);
        public Double Product_PSI
        {
            get { return GetProperty<Double>(Product_PSIProperty); }
        }

        /// <summary>
        /// Rank_PSI
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Rank_PSIProperty =
            RegisterModelProperty<Double>(c => c.Rank_PSI);
        public Double Rank_PSI
        {
            get { return GetProperty<Double>(Rank_PSIProperty); }
        }

        /// <summary>
        /// Sales
        /// </summary>
        public static readonly ModelPropertyInfo<Double> SalesProperty =
            RegisterModelProperty<Double>(c => c.Sales);
        public Double Sales
        {
            get { return GetProperty<Double>(SalesProperty); }
        }

        /// <summary>
        /// Section_PSI
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Section_PSIProperty =
            RegisterModelProperty<Double>(c => c.Section_PSI);
        public Double Section_PSI
        {
            get { return GetProperty<Double>(Section_PSIProperty); }
        }

        /// <summary>
        /// Section_Rank
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Section_RankProperty =
            RegisterModelProperty<Double>(c => c.Section_Rank);
        public Double Section_Rank
        {
            get { return GetProperty<Double>(Section_RankProperty); }
        }


        /// <summary>
        /// TrayPackName
        /// </summary>
        public static readonly ModelPropertyInfo<String> TrayPackNameProperty =
            RegisterModelProperty<String>(c => c.TrayPackName);
        public String TrayPackName
        {
            get { return GetProperty<String>(TrayPackNameProperty); }
        }

        /// <summary>
        /// Rotation
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> RotationProperty =
            RegisterModelProperty<Int32>(c => c.Rotation);
        public Int32 Rotation
        {
            get { return GetProperty<Int32>(RotationProperty); }
        }

        /// <summary>
        /// Trays_Only
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Trays_OnlyProperty =
            RegisterModelProperty<Int32>(c => c.Trays_Only);
        public Int32 Trays_Only
        {
            get { return GetProperty<Int32>(Trays_OnlyProperty); }
        }


        /// <summary>
        /// Tray_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Tray_HeightProperty =
            RegisterModelProperty<Double>(c => c.Tray_Height);
        public Double Tray_Height
        {
            get { return GetProperty<Double>(Tray_HeightProperty); }
        }

        /// <summary>
        /// Tray_Thick
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Tray_ThickProperty =
            RegisterModelProperty<Double>(c => c.Tray_Thick);
        public Double Tray_Thick
        {
            get { return GetProperty<Double>(Tray_ThickProperty); }
        }

        /// <summary>
        /// Tray_Units_High
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Tray_Units_HighProperty =
            RegisterModelProperty<Int32>(c => c.Tray_Units_High);
        public Int32 Tray_Units_High
        {
            get { return GetProperty<Int32>(Tray_Units_HighProperty); }
        }

        /// <summary>
        /// Tray_Units_Wide
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Tray_Units_WideProperty =
            RegisterModelProperty<Int32>(c => c.Tray_Units_Wide);
        public Int32 Tray_Units_Wide
        {
            get { return GetProperty<Int32>(Tray_Units_WideProperty); }
        }

        /// <summary>
        /// Tray_Units_Deep
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Tray_Units_DeepProperty =
            RegisterModelProperty<Int32>(c => c.Tray_Units_Deep);
        public Int32 Tray_Units_Deep
        {
            get { return GetProperty<Int32>(Tray_Units_DeepProperty); }
        }

        /// <summary>
        /// VatRate
        /// </summary>
        public static readonly ModelPropertyInfo<Double> VatRateProperty =
            RegisterModelProperty<Double>(c => c.VatRate);
        public Double VatRate
        {
            get { return GetProperty<Double>(VatRateProperty); }
        }

        /// <summary>
        /// User_Number_1
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_1Property =
            RegisterModelProperty<Double>(c => c.User_Number_1);
        public Double User_Number_1
        {
            get { return GetProperty<Double>(User_Number_1Property); }
        }

        /// <summary>
        /// User_Number_2
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_2Property =
            RegisterModelProperty<Double>(c => c.User_Number_2);
        public Double User_Number_2
        {
            get { return GetProperty<Double>(User_Number_2Property); }
        }

        /// <summary>
        /// User_Number_3
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_3Property =
            RegisterModelProperty<Double>(c => c.User_Number_3);
        public Double User_Number_3
        {
            get { return GetProperty<Double>(User_Number_3Property); }
        }

        /// <summary>
        /// User_Number_4
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_4Property =
            RegisterModelProperty<Double>(c => c.User_Number_4);
        public Double User_Number_4
        {
            get { return GetProperty<Double>(User_Number_4Property); }
        }

        /// <summary>
        /// User_Number_5
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_5Property =
            RegisterModelProperty<Double>(c => c.User_Number_5);
        public Double User_Number_5
        {
            get { return GetProperty<Double>(User_Number_5Property); }
        }

        /// <summary>
        /// User_Number_6
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_6Property =
            RegisterModelProperty<Double>(c => c.User_Number_6);
        public Double User_Number_6
        {
            get { return GetProperty<Double>(User_Number_6Property); }
        }

        /// <summary>
        /// User_Number_7
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_7Property =
            RegisterModelProperty<Double>(c => c.User_Number_7);
        public Double User_Number_7
        {
            get { return GetProperty<Double>(User_Number_7Property); }
        }

        /// <summary>
        /// User_Number_8
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_8Property =
            RegisterModelProperty<Double>(c => c.User_Number_8);
        public Double User_Number_8
        {
            get { return GetProperty<Double>(User_Number_8Property); }
        }

        /// <summary>
        /// User_Number_9
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_9Property =
            RegisterModelProperty<Double>(c => c.User_Number_9);
        public Double User_Number_9
        {
            get { return GetProperty<Double>(User_Number_9Property); }
        }

        /// <summary>
        /// User_Number_10
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_10Property =
            RegisterModelProperty<Double>(c => c.User_Number_10);
        public Double User_Number_10
        {
            get { return GetProperty<Double>(User_Number_10Property); }
        }

        /// <summary>
        /// User_Number_11
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_11Property =
            RegisterModelProperty<Double>(c => c.User_Number_11);
        public Double User_Number_11
        {
            get { return GetProperty<Double>(User_Number_11Property); }
        }

        /// <summary>
        /// User_Number_12
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_12Property =
            RegisterModelProperty<Double>(c => c.User_Number_12);
        public Double User_Number_12
        {
            get { return GetProperty<Double>(User_Number_12Property); }
        }

        /// <summary>
        /// User_Number_13
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_13Property =
            RegisterModelProperty<Double>(c => c.User_Number_13);
        public Double User_Number_13
        {
            get { return GetProperty<Double>(User_Number_13Property); }
        }

        /// <summary>
        /// User_Number_14
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_14Property =
            RegisterModelProperty<Double>(c => c.User_Number_14);
        public Double User_Number_14
        {
            get { return GetProperty<Double>(User_Number_14Property); }
        }

        /// <summary>
        /// User_Number_15
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_15Property =
            RegisterModelProperty<Double>(c => c.User_Number_15);
        public Double User_Number_15
        {
            get { return GetProperty<Double>(User_Number_15Property); }
        }

        /// <summary>
        /// User_Number_16
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_16Property =
            RegisterModelProperty<Double>(c => c.User_Number_16);
        public Double User_Number_16
        {
            get { return GetProperty<Double>(User_Number_16Property); }
        }

        /// <summary>
        /// User_Number_17
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_17Property =
            RegisterModelProperty<Double>(c => c.User_Number_17);
        public Double User_Number_17
        {
            get { return GetProperty<Double>(User_Number_17Property); }
        }

        /// <summary>
        /// User_Number_18
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_18Property =
            RegisterModelProperty<Double>(c => c.User_Number_18);
        public Double User_Number_18
        {
            get { return GetProperty<Double>(User_Number_18Property); }
        }

        /// <summary>
        /// User_Number_19
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_19Property =
            RegisterModelProperty<Double>(c => c.User_Number_19);
        public Double User_Number_19
        {
            get { return GetProperty<Double>(User_Number_19Property); }
        }

        /// <summary>
        /// User_Number_20
        /// </summary>
        public static readonly ModelPropertyInfo<Double> User_Number_20Property =
            RegisterModelProperty<Double>(c => c.User_Number_20);
        public Double User_Number_20
        {
            get { return GetProperty<Double>(User_Number_20Property); }
        }

        /// <summary>
        /// User_String_1
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_1Property =
            RegisterModelProperty<String>(c => c.User_String_1);
        public String User_String_1
        {
            get { return GetProperty<String>(User_String_1Property); }
        }

        /// <summary>
        /// User_String_2
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_2Property =
            RegisterModelProperty<String>(c => c.User_String_2);
        public String User_String_2
        {
            get { return GetProperty<String>(User_String_2Property); }
        }

        /// <summary>
        /// User_String_3
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_3Property =
            RegisterModelProperty<String>(c => c.User_String_3);
        public String User_String_3
        {
            get { return GetProperty<String>(User_String_3Property); }
        }

        /// <summary>
        /// User_String_4
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_4Property =
            RegisterModelProperty<String>(c => c.User_String_4);
        public String User_String_4
        {
            get { return GetProperty<String>(User_String_4Property); }
        }

        /// <summary>
        /// User_String_5
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_5Property =
            RegisterModelProperty<String>(c => c.User_String_5);
        public String User_String_5
        {
            get { return GetProperty<String>(User_String_5Property); }
        }

        /// <summary>
        /// User_String_6
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_6Property =
            RegisterModelProperty<String>(c => c.User_String_6);
        public String User_String_6
        {
            get { return GetProperty<String>(User_String_6Property); }
        }

        /// <summary>
        /// User_String_7
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_7Property =
            RegisterModelProperty<String>(c => c.User_String_7);
        public String User_String_7
        {
            get { return GetProperty<String>(User_String_7Property); }
        }

        /// <summary>
        /// User_String_8
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_8Property =
            RegisterModelProperty<String>(c => c.User_String_8);
        public String User_String_8
        {
            get { return GetProperty<String>(User_String_8Property); }
        }

        /// <summary>
        /// User_String_9
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_9Property =
            RegisterModelProperty<String>(c => c.User_String_9);
        public String User_String_9
        {
            get { return GetProperty<String>(User_String_9Property); }
        }

        /// <summary>
        /// User_String_10
        /// </summary>
        public static readonly ModelPropertyInfo<String> User_String_10Property =
            RegisterModelProperty<String>(c => c.User_String_10);
        public String User_String_10
        {
            get { return GetProperty<String>(User_String_10Property); }
        }

        /// <summary>
        /// Sub_Category_Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> Sub_Category_NameProperty =
            RegisterModelProperty<String>(c => c.Sub_Category_Name);
        public String Sub_Category_Name
        {
            get { return GetProperty<String>(Sub_Category_NameProperty); }
        }

        /// <summary>
        /// Manufacturer_Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> Manufacturer_NameProperty =
            RegisterModelProperty<String>(c => c.Manufacturer_Name);
        public String Manufacturer_Name
        {
            get { return GetProperty<String>(Manufacturer_NameProperty); }
        }
        
        /// <summary>
        /// Max_Cap
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Max_CapProperty =
            RegisterModelProperty<Int32>(c => c.Max_Cap);
        public Int32 Max_Cap
        {
            get { return GetProperty<Int32>(Max_CapProperty); }
        }

        /// <summary>
        /// Max_Stack
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Max_StackProperty =
            RegisterModelProperty<Int32>(c => c.Max_Stack);
        public Int32 Max_Stack
        {
            get { return GetProperty<Int32>(Max_StackProperty); }
        }

        /// <summary>
        /// Inventory_Cases
        /// </summary>
        public static readonly ModelPropertyInfo<Single> Inventory_CasesProperty =
            RegisterModelProperty<Single>(c => c.Inventory_Cases);
        public Single Inventory_Cases
        {
            get { return GetProperty<Single>(Inventory_CasesProperty); }
        }
        
        #endregion
    }
}
