﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    [Serializable]
    public partial class PlanBayList : ModelList<PlanBayList, PlanBay>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //BusinessRules.AddRule(typeof(PlanBayList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.CategoryFetch.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByCategoryReviewId
        /// </summary>
        [Serializable]
        public class GetByProposalCodeFilePathCriteria : Csla.CriteriaBase<GetByProposalCodeFilePathCriteria>
        {
            public static readonly PropertyInfo<Int32> ProposalCodeProperty =
            RegisterProperty<Int32>(c => c.ProposalCode);
            public Int32 ProposalCode
            {
                get { return ReadProperty<Int32>(ProposalCodeProperty); }
            }

            public static readonly PropertyInfo<String> FilePathProperty =
            RegisterProperty<String>(c => c.FilePath);
            public String FilePath
            {
                get { return ReadProperty<String>(FilePathProperty); }
            }

            public GetByProposalCodeFilePathCriteria(Int32 proposalCode, String elementCode)
            {
                LoadProperty<Int32>(ProposalCodeProperty, proposalCode);
                LoadProperty<String>(FilePathProperty, elementCode);
            }
        }

        #endregion
    }
}
