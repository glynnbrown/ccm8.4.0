﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (CCM.Net 1.0)
// CCM-15604 : M.Brumby
//  Created
#endregion
#endregion
using System;
using System.Collections.Generic;

using Csla;
using Csla.Data;

using Galleria.Framework.Dal;

using Galleria.Ccm.Legacy.Dal.Msp;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class PlanBayList
    {
        #region Constructors
        private PlanBayList() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns all plans
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanBayList GetBaysByCode(Int32 proposalCode, String filePath)
        {
            return DataPortal.Fetch<PlanBayList>(new GetByProposalCodeFilePathCriteria(proposalCode, filePath));
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Called when retrieving a list of all category infos
        /// </summary>
        private void DataPortal_Fetch(GetByProposalCodeFilePathCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory(criteria.FilePath);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanBayDal dal = dalContext.GetDal<IPlanBayDal>())
                {
                    IEnumerable<PlanBayDto> dtoList = dal.PlanBay_FetchByCode(criteria.ProposalCode);

                    if (dtoList != null)
                    {
                        foreach (PlanBayDto dto in dtoList)
                        {
                            this.Add(PlanBay.GetPlanBay(dalContext, dto, criteria.FilePath));
                        }
                    }
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
