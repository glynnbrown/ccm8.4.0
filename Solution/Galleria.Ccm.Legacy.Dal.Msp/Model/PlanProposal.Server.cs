﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class PlanProposal
    {
        #region Constructor
        private PlanProposal() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing plan
        /// </summary>
        /// <returns>A list of plans</returns>
        public static PlanProposal GetPlanProposalByCode(Int32 proposalCode, String filePath)
        {
            return DataPortal.Fetch<PlanProposal>(new GetByProposalCodeFilePathCriteria(proposalCode, filePath));
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanProposalDto dto)
        {
            this.LoadProperty<Int32>(Plan_Proposal_CodeProperty, dto.Plan_Proposal_Code);
            this.LoadProperty<Int32>(Category_Market_CodeProperty, dto.Category_Market_Code);
            this.LoadProperty<Int32>(CollisionsProperty, dto.Collisions);
            this.LoadProperty<Int32>(DimUnitsProperty, dto.DimUnits);

            this.LoadProperty<String>(Fixture_NameProperty, dto.Fixture_Name);
            this.LoadProperty<String>(FixtureElementEntryProperty, dto.FixtureElementEntry);

            this.LoadProperty<Int32>(FlagsProperty, dto.Flags);
            this.LoadProperty<Int32>(FloatingLabelEntryProperty, dto.FloatingLabelEntry);
            this.LoadProperty<Double>(FootageProperty, dto.Footage);

            this.LoadProperty<String>(InfoBoxEntryProperty, dto.InfoBoxEntry);

            this.LoadProperty<Double>(Max_HeightProperty, dto.Max_Height);
            this.LoadProperty<Double>(Max_WidthProperty, dto.Max_Width);

            this.LoadProperty<String>(NameProperty, dto.Name);

            this.LoadProperty<Double>(No_StoresProperty, dto.No_Stores);

            this.LoadProperty<String>(Nominal_SizeProperty, dto.Nominal_Size);
            this.LoadProperty<String>(NotesProperty, dto.Notes);
            this.LoadProperty<String>(ProductEntryProperty, dto.ProductEntry);
            this.LoadProperty<String>(ProductListEntryProperty, dto.ProductListEntry);

            this.LoadProperty<Double>(Shelf_LinearProperty, dto.Shelf_Linear);

            this.LoadProperty<String>(TitleProperty, dto.Title);

            this.LoadProperty<Int32>(Traffic_FlowProperty, dto.Traffic_Flow);
            this.LoadProperty<Double>(User_Number_1Property, dto.User_Number_1);
            this.LoadProperty<Double>(User_Number_2Property, dto.User_Number_2);
            this.LoadProperty<Double>(User_Number_3Property, dto.User_Number_3);
            this.LoadProperty<Double>(User_Number_4Property, dto.User_Number_4);
            this.LoadProperty<Double>(User_Number_5Property, dto.User_Number_5);

            this.LoadProperty<String>(User_String_1Property, dto.User_String_1);
            this.LoadProperty<String>(User_String_2Property, dto.User_String_2);

            this.LoadProperty<Int32>(Vert_Traffic_FlowProperty, dto.Vert_Traffic_Flow);
            this.LoadProperty<Int32>(PlanIdProperty, dto.PlanId);


            this.LoadProperty<PlanBayList>(PlanBaysProperty, PlanBayList.GetBaysByCode(dto.Plan_Proposal_Code, this.FilePath));
        }
        #endregion


        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void DataPortal_Fetch(GetByProposalCodeFilePathCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory(criteria.FilePath);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IPlanProposalDal dal = dalContext.GetDal<IPlanProposalDal>())
                {
                    PlanProposalDto dto = dal.PlanProposal_FetchByCode(criteria.ProposalCode);
                    this.LoadProperty<String>(FilePathProperty, criteria.FilePath);
                    if (dto != null) LoadDataTransferObject(dalContext, dto);

                   
                }
            }
        }

        #endregion
    }
}
