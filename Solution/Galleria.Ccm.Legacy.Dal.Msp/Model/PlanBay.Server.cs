﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Legacy.Dal.Msp.DataTransferObjects;
using Galleria.Ccm.Legacy.Dal.Msp.Interfaces;
using Galleria.Framework.Dal;


namespace Galleria.Ccm.Legacy.Msp.Model
{
    public partial class PlanBay
    {
        #region Constructor
        private PlanBay() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new object</returns>
        internal static PlanBay GetPlanBay(IDalContext dalContext, PlanBayDto dto, String filePath)
        {
            return DataPortal.FetchChild<PlanBay>(dalContext, dto, filePath);
        }
        #endregion

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanBayDto dto, String filePath)
        {
            this.LoadProperty<Int32>(Plan_Bay_CodeProperty, dto.Plan_Bay_Code);
            this.LoadProperty<Int32>(Base_ColourProperty, dto.Base_Colour);

            this.LoadProperty<Double>(Base_DepthProperty, dto.Base_Depth);
            this.LoadProperty<Double>(Base_HeightProperty, dto.Base_Height);
            this.LoadProperty<Double>(Base_WidthProperty, dto.Base_Width);
            this.LoadProperty<Double>(Bay_PositionProperty, dto.Bay_Position);

            this.LoadProperty<String>(Bay_UnitsProperty, dto.Bay_Units);

            this.LoadProperty<Int32>(ColourProperty, dto.Colour);

            this.LoadProperty<String>(Continuous_ShelfProperty, dto.Continuous_Shelf);

            this.LoadProperty<Double>(DepthProperty, dto.Depth);
            this.LoadProperty<Double>(HeightProperty, dto.Height);
            this.LoadProperty<Double>(WidthProperty, dto.Width);
            this.LoadProperty<Double>(FirstNotchProperty, dto.FirstNotch);

            this.LoadProperty<String>(NameProperty, dto.Name);

            this.LoadProperty<Double>(Notch_SpacingProperty, dto.Notch_Spacing);

            this.LoadProperty<Int32>(Number_Of_BaysProperty, dto.Number_Of_Bays);
            this.LoadProperty<Int32>(Plan_Proposal_CodeProperty, dto.Plan_Proposal_Code);
            this.LoadProperty<Int32>(Position_RelativeProperty, dto.Position_Relative);
            this.LoadProperty<Int32>(Show_Peg_HolesProperty, dto.Show_Peg_Holes);

            this.LoadProperty<Double>(Take_OffProperty, dto.Take_Off);

            this.LoadProperty<Int32>(Use_Notch_SpacingProperty, dto.Use_Notch_Spacing);
            this.LoadProperty<Int32>(OriginalPlanBayCodeProperty, dto.OriginalPlanBayCode);
            this.LoadProperty<Int32>(PlanIdProperty, dto.PlanId);

            this.LoadProperty<Byte>(StatusProperty, dto.Status);

            this.LoadProperty<PlanElementList>(PlanElementsProperty, PlanElementList.GetPlanElementsByBayCode(dto.Plan_Bay_Code, filePath));
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, PlanBayDto dto, String filePath)
        {
            this.LoadDataTransferObject(dalContext, dto, filePath);
        }
        #endregion
    }
}
