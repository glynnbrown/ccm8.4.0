﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    public enum PlanElementType
    {
        Shelf,
        Bar,
        Peg,
        Chest,
        Pallet
    }

    public static class PlanElementTypeHelper
    {
        public static Dictionary<PlanElementType, String> InternalCCMNames = new Dictionary<PlanElementType, string>()
        {
            {PlanElementType.Shelf, "SHELF"},
            {PlanElementType.Bar, "BAR"},
            {PlanElementType.Peg, "PEG"},
            {PlanElementType.Chest, "CHEST"},
            {PlanElementType.Pallet, "PALLET"}
        };


        public static PlanElementType InternalNameToEnum(String InternalName)
        {
            foreach (KeyValuePair<PlanElementType, String> d in InternalCCMNames)
            {
                if (d.Value.ToUpperInvariant() == InternalName.ToUpperInvariant())
                {
                    return d.Key;
                }
            }

            return PlanElementType.Shelf;
        }
    }
}
