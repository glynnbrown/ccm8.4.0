﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM830)
// CCM-31850 : M.Brumby
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Legacy.Msp.Model
{
    [Serializable]
    public partial class PlanElement : ModelObject<PlanElement>
    {
        #region Properties
        /// <summary>
        ///Plan_Element_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Plan_Element_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Plan_Element_Code);
        public Int32 Plan_Element_Code
        {
            get { return GetProperty<Int32>(Plan_Element_CodeProperty); }
        }

        /// <summary>
        /// Active
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ActiveProperty =
            RegisterModelProperty<Int32>(c => c.Active);
        public Int32 Active
        {
            get { return GetProperty<Int32>(ActiveProperty); }
        }

        /// <summary>
        /// Colour
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ColourProperty =
            RegisterModelProperty<Int32>(c => c.Colour);
        public Int32 Colour
        {
            get { return GetProperty<Int32>(ColourProperty); }
        }
        
        /// <summary>
        /// Combined_With
        /// </summary>
        public static readonly ModelPropertyInfo<String> Combined_WithProperty =
            RegisterModelProperty<String>(c => c.Combined_With);
        public String Combined_With
        {
            get { return GetProperty<String>(Combined_WithProperty); }
        }

        /// <summary>
        /// Dimension_0
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_0Property =
            RegisterModelProperty<Double>(c => c.Dimension_0);
        public Double Dimension_0
        {
            get { return GetProperty<Double>(Dimension_0Property); }
        }

        /// <summary>
        /// Dimension_1
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_1Property =
            RegisterModelProperty<Double>(c => c.Dimension_1);
        public Double Dimension_1
        {
            get { return GetProperty<Double>(Dimension_1Property); }
        }

        /// <summary>
        /// Dimension_2
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_2Property =
            RegisterModelProperty<Double>(c => c.Dimension_2);
        public Double Dimension_2
        {
            get { return GetProperty<Double>(Dimension_2Property); }
        }

        /// <summary>
        /// Dimension_3
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_3Property =
            RegisterModelProperty<Double>(c => c.Dimension_3);
        public Double Dimension_3
        {
            get { return GetProperty<Double>(Dimension_3Property); }
        }

        /// <summary>
        /// Dimension_4
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_4Property =
            RegisterModelProperty<Double>(c => c.Dimension_4);
        public Double Dimension_4
        {
            get { return GetProperty<Double>(Dimension_4Property); }
        }

        /// <summary>
        /// Dimension_5
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_5Property =
            RegisterModelProperty<Double>(c => c.Dimension_5);
        public Double Dimension_5
        {
            get { return GetProperty<Double>(Dimension_5Property); }
        }

        /// <summary>
        /// Dimension_6
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_6Property =
            RegisterModelProperty<Double>(c => c.Dimension_6);
        public Double Dimension_6
        {
            get { return GetProperty<Double>(Dimension_6Property); }
        }

        /// <summary>
        /// Dimension_7
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_7Property =
            RegisterModelProperty<Double>(c => c.Dimension_7);
        public Double Dimension_7
        {
            get { return GetProperty<Double>(Dimension_7Property); }
        }

        /// <summary>
        /// Dimension_8
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Dimension_8Property =
            RegisterModelProperty<Double>(c => c.Dimension_8);
        public Double Dimension_8
        {
            get { return GetProperty<Double>(Dimension_8Property); }
        }
        
        /// <summary>
        /// Finger_Space
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Finger_SpaceProperty =
            RegisterModelProperty<Double>(c => c.Finger_Space);
        public Double Finger_Space
        {
            get { return GetProperty<Double>(Finger_SpaceProperty); }
        }
        
        /// <summary>
        /// Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// Next_PE_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Next_PE_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Next_PE_Code);
        public Int32 Next_PE_Code
        {
            get { return GetProperty<Int32>(Next_PE_CodeProperty); }
        }

        /// <summary>
        /// No_Shelves_Wide
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> No_Shelves_WideProperty =
            RegisterModelProperty<Int32>(c => c.No_Shelves_Wide);
        public Int32 No_Shelves_Wide
        {
            get { return GetProperty<Int32>(No_Shelves_WideProperty); }
        }

        /// <summary>
        /// Plan_Bay_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Plan_Bay_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Plan_Bay_Code);
        public Int32 Plan_Bay_Code
        {
            get { return GetProperty<Int32>(Plan_Bay_CodeProperty); }
        }

        /// <summary>
        /// Prev_PE_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Prev_PE_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Prev_PE_Code);
        public Int32 Prev_PE_Code
        {
            get { return GetProperty<Int32>(Prev_PE_CodeProperty); }
        }

        /// <summary>
        /// Prnt_PE_Code
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Prnt_PE_CodeProperty =
            RegisterModelProperty<Int32>(c => c.Prnt_PE_Code);
        public Int32 Prnt_PE_Code
        {
            get { return GetProperty<Int32>(Prnt_PE_CodeProperty); }
        }

        /// <summary>
        /// Slope
        /// </summary>
        public static readonly ModelPropertyInfo<Double> SlopeProperty =
            RegisterModelProperty<Double>(c => c.Slope);
        public Double Slope
        {
            get { return GetProperty<Double>(SlopeProperty); }
        }

        /// <summary>
        /// Space_Remaining
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Space_RemainingProperty =
            RegisterModelProperty<Double>(c => c.Space_Remaining);
        public Double Space_Remaining
        {
            get { return GetProperty<Double>(Space_RemainingProperty); }
        }

        /// <summary>
        /// SplitType
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> SplitTypeProperty =
            RegisterModelProperty<Int32>(c => c.SplitType);
        public Int32 SplitType
        {
            get { return GetProperty<Int32>(SplitTypeProperty); }
        }

        /// <summary>
        /// Sub_Type
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Sub_TypeProperty =
            RegisterModelProperty<Int32>(c => c.Sub_Type);
        public Int32 Sub_Type
        {
            get { return GetProperty<Int32>(Sub_TypeProperty); }
        }

        /// <summary>
        /// Tallest_Height
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Tallest_HeightProperty =
            RegisterModelProperty<Double>(c => c.Tallest_Height);
        public Double Tallest_Height
        {
            get { return GetProperty<Double>(Tallest_HeightProperty); }
        }

        /// <summary>
        /// Type
        /// </summary>
        public static readonly ModelPropertyInfo<String> TypeProperty =
            RegisterModelProperty<String>(c => c.Type);
        public String Type
        {
            get { return GetProperty<String>(TypeProperty); }
        }

        /// <summary>
        /// Use_Finger_Space
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> Use_Finger_SpaceProperty =
            RegisterModelProperty<Int32>(c => c.Use_Finger_Space);
        public Int32 Use_Finger_Space
        {
            get { return GetProperty<Int32>(Use_Finger_SpaceProperty); }
        }

        /// <summary>
        /// X_Position
        /// </summary>
        public static readonly ModelPropertyInfo<Double> X_PositionProperty =
            RegisterModelProperty<Double>(c => c.X_Position);
        public Double X_Position
        {
            get { return GetProperty<Double>(X_PositionProperty); }
        }

        /// <summary>
        /// X_Scale
        /// </summary>
        public static readonly ModelPropertyInfo<Double> X_ScaleProperty =
            RegisterModelProperty<Double>(c => c.X_Scale);
        public Double X_Scale
        {
            get { return GetProperty<Double>(X_ScaleProperty); }
        }

        /// <summary>
        /// Y_Position
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Y_PositionProperty =
            RegisterModelProperty<Double>(c => c.Y_Position);
        public Double Y_Position
        {
            get { return GetProperty<Double>(Y_PositionProperty); }
        }

        /// <summary>
        /// Y_Scale
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Y_ScaleProperty =
            RegisterModelProperty<Double>(c => c.Y_Scale);
        public Double Y_Scale
        {
            get { return GetProperty<Double>(Y_ScaleProperty); }
        }

        /// <summary>
        /// Z_Position
        /// </summary>
        public static readonly ModelPropertyInfo<Double> Z_PositionProperty =
            RegisterModelProperty<Double>(c => c.Z_Position);
        public Double Z_Position
        {
            get { return GetProperty<Double>(Z_PositionProperty); }
        }
        
        /// <summary>
        /// OriginalPlanElementCode
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> OriginalPlanElementCodeProperty =
            RegisterModelProperty<Int32>(c => c.OriginalPlanElementCode);
        public Int32 OriginalPlanElementCode
        {
            get { return GetProperty<Int32>(OriginalPlanElementCodeProperty); }
        }

        /// <summary>
        /// PlanID
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanIDProperty =
            RegisterModelProperty<Int32>(c => c.PlanID);
        public Int32 PlanID
        {
            get { return GetProperty<Int32>(PlanIDProperty); }
        }

        /// <summary>
        /// Status
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> StatusProperty =
            RegisterModelProperty<Byte>(c => c.Status);
        public Byte Status
        {
            get { return GetProperty<Byte>(StatusProperty); }
        }

        #endregion

        #region List Properties
        public static readonly ModelPropertyInfo<PlanPositionList> PositionsProperty =
                RegisterModelProperty<PlanPositionList>(c => c.Positions);
        public PlanPositionList Positions
        {
            get
            {
                return GetProperty<PlanPositionList>(PositionsProperty);
            }
        }
        #endregion
    }
}
