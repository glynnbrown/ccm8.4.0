﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Sync.Service
{
    /// <summary>
    /// Internal class used to hold global constants
    /// </summary>
    internal class Constants
    {
        public const String ServiceName = "CCM Sync Service";
    }
}