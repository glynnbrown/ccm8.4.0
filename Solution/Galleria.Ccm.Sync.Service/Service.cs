﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.ServiceProcess;
using System.Threading;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Sync.Service
{
    /// <summary>
    /// The application service
    /// </summary>
    public partial class Service : ServiceBase, IService
    {
        #region Fields
        private ServiceStatus _serviceStatus = ServiceStatus.Stopped; // the current service status
        private ManualResetEvent _shutdownEvent = new ManualResetEvent(false); // event used to notify service processes of shutdown
        private Thread _serviceThread; // the service thread
        #endregion

        #region Properties
        /// <summary>
        /// Indicates the services current status
        /// </summary>
        public ServiceStatus ServiceStatus
        {
            get { return _serviceStatus; }
            private set { _serviceStatus = value; }
        }

        /// <summary>
        /// Event used to notify services processes of shutdown
        /// </summary>
        public ManualResetEvent ShutdownEvent
        {
            get { return _shutdownEvent; }
        }

        /// <summary>
        /// The service thread
        /// </summary>
        private Thread ServiceThread
        {
            get { return _serviceThread; }
            set { _serviceThread = value; }
        }
        #endregion

        #region Constuctors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Service()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods

        #region Start
        /// <summary>
        /// Starts the service
        /// </summary>
        /// <param name="args">An array of command line parameters</param>
        public void StartService(String[] args)
        {
            if (this.ServiceStatus == ServiceStatus.Stopped)
            {
                // set the service status to running
                this.ServiceStatus = ServiceStatus.Running;

                // ensure that the shutdown event is reset
                this.ShutdownEvent.Reset();

                // create the service implementation on a new thread
                this.ServiceThread = new Thread(new ThreadStart(new ServiceProcess(this).Main));

                // start the thread
                this.ServiceThread.Start();
            }
        }

        /// <summary>
        /// Called when this service is being started
        /// </summary>
        /// <param name="args">An array of parameters passed on the command line</param>
        protected override void OnStart(string[] args)
        {
            this.StartService(args);
            base.OnStart(args);
        }
        #endregion

        #region Stop
        /// <summary>
        /// Stops the service
        /// </summary>
        public void StopService()
        {
            if ((this.ServiceStatus == ServiceStatus.Running) ||
                (this.ServiceStatus == ServiceStatus.Paused))
            {
                try
                {
                    if (GfsLoggingHelper.Logger == null) // in reality should have been created by now, but better to be on the safe side
                    {
                        GfsLoggingHelper.Logger = new GfsLoggingHelper(this.ServiceName, true);
                    }

                    //Log into windows event log
                    GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, EventLogEvent.WindowsServiceStopping, this.ServiceName);
                }
                catch
                {
                    //failed to log
                }

                // set the service status to stopped
                this.ServiceStatus = ServiceStatus.Stopped;

                // signal the service processes to stop
                this.ShutdownEvent.Set();

                // SA-18838: Removed a timeout from the following Join() call and a subsequent block of code 
                // designed to abort the ServiceThread if it's still running.

                // wait for the service to stop
                this.ServiceThread.Join();
            }
        }

        /// <summary>
        /// Called when this service is being stopped
        /// </summary>
        protected override void OnStop()
        {
            this.StopService();
            base.OnStop();
        }
        #endregion

        #region Pause
        /// <summary>
        /// Pauses this service
        /// </summary>
        public void PauseService()
        {
            if (this.ServiceStatus == ServiceStatus.Running)
            {
                this.ServiceStatus = ServiceStatus.Paused;
            }
        }

        /// <summary>
        /// Called when this service is paused
        /// </summary>
        protected override void OnPause()
        {
            this.PauseService();
            base.OnPause();
        }
        #endregion

        #region Continue
        /// <summary>
        /// Continues the service from paused
        /// </summary>
        public void ContinueService()
        {
            if (this.ServiceStatus == ServiceStatus.Paused)
            {
                this.ServiceStatus = ServiceStatus.Running;
            }
        }

        /// <summary>
        /// Called when continuing the service
        /// </summary>
        protected override void OnContinue()
        {
            this.ContinueService();
            base.OnContinue();
        }
        #endregion

        #region Install
        /// <summary>
        /// Installs this service on the local machine
        /// </summary>
        public void InstallService()
        {
            // TODO
        }
        #endregion

        #region Uninstall
        /// <summary>
        /// Uninstalls this service from the local machine
        /// </summary>
        public void UninstallService()
        {
            // TODO
        }
        #endregion

        #endregion
    }
}