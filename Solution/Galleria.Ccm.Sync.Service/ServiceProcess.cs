﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.ServiceModel;

using Galleria.Ccm.Processes;
using Galleria.Ccm.Processes.Synchronization;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;

using Gibraltar.Agent;

using Galleria.Framework.Dal;
using Galleria.Framework.Processes;
using Csla;

namespace Galleria.Ccm.Sync.Service
{
    /// <summary>
    /// Defines the process that is executed by the service
    /// </summary>
    public class ServiceProcess
    {
        #region Constants
        private const int _serviceDelay = 1000; // the amount of time in millisecond between each service poll
        private const Int32 _logExceptionDelay = 15; // the amount of time in minutes to wait before relogging the same exception
        private const String _sessionIdName = "Session ID";
        public const String _serviceName = "CCM Sync Service";
        #endregion

        #region Fields
        private IService _service; // hods a reference to the parent service
        private Exception _gibraltarStartupException; // the exception that occurred while initializing Gibraltar, if any
        private Dictionary<String, DateTime> _loggedExceptionTimes; // the time a particular exception was logged
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="service"></param>
        public ServiceProcess(IService service)
        {
            _service = service;
            _loggedExceptionTimes = new Dictionary<String, DateTime>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// The parent service
        /// </summary>
        private IService Service
        {
            get { return _service; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// The main 
        /// </summary>
        public void Main()
        {
            // Start logging.
            Log.Initializing += new Log.InitializingEventHandler(Log_Initializing);
            Log.StartSession();
            GfsLoggingHelper logger = new GfsLoggingHelper(Service.ServiceName, true);

            // If an exception occurred while initializing Gibraltar, log it.
            if (_gibraltarStartupException != null)
            {
                logger.Write(GfsLoggingHelper.SyncEventLogName, null, _gibraltarStartupException);
            }

            // Log that the service has started. 
            logger.Write(GfsLoggingHelper.SyncEventLogName, null, EventLogEvent.WindowsServiceStart, _serviceName);

            try
            {
                // Set all running Sync Targets to Idle
                foreach (SyncTarget syncTarget in SyncTargetList.GetAllSyncTargets(true))
                {
                    // it is impossible for any sync target to be running if the service 
                    // is just being turned on, so set them back to idle
                    if (syncTarget.Status == SyncTargetStatus.Running)
                    {
                        SyncTarget syncTargetUpdate = SyncTarget.GetSyncTargetById(syncTarget.Id);
                        syncTargetUpdate.Status = SyncTargetStatus.Idle;
                        syncTargetUpdate.Save();
                    }
                }
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }

            // execute the service
            while (true)
            {
                // perform the service processing
                // if the service status is paused
                // then perform no processing at this time
                if (this.Service.ServiceStatus == ServiceStatus.Running)
                {
                    try
                    {
                        // Process
                        Process();
                    }
                    catch (Exception e)
                    {
                        LogUnexpectedException(e);
                    }
                }

                // determine if the service has signalled a shutdown
                // this also causes the thread to wait so that we are
                // not continually polling the service event
                if (this.Service.ShutdownEvent.WaitOne(_serviceDelay, true))
                {
                    break;
                }
            }

            logger.Write(GfsLoggingHelper.SyncEventLogName, null, EventLogEvent.WindowsServiceStop, _serviceName);

            // Stop logging.
            Log.EndSession();
        }

        #region Logging

        /// <summary>
        /// Called when the log is initialized
        /// </summary>
        /// <param name="sender">The event source</param>
        /// <param name="e">The event arguments</param>
        private void Log_Initializing(object sender, LogInitializingEventArgs e)
        {
            try
            {
                // configure the application to log to the service
                e.Configuration.Server.UseGibraltarService = true;
                e.Configuration.Server.CustomerName = "galleria";
                e.Configuration.Server.SendAllApplications = true;
                e.Configuration.Server.PurgeSentSessions = true;
                e.Configuration.Server.UseSsl = true;
                e.Configuration.Properties[_sessionIdName] = Guid.NewGuid().ToString();
                e.Configuration.SessionFile.EnableFilePruning = true; // ISO-13436
                e.Configuration.SessionFile.MaxLocalDiskUsage = 200; // ISO-13436
                e.Configuration.Publisher.EnableAnonymousMode = false; // GFS-14882
#if !DEBUG
                // now determine if we are running within the IDE.
                // normally, you shouldn't change the behaviour of
                // an application depending on whether a debugger
                // is attached or not, however, we want to be able
                // to still use the logging facilities in debug
                // builds when they are distrubuted to people such
                // as qa and consultancy. Therefore we have used
                // this method instead of conditional compilation
                if (!Debugger.IsAttached) // GFS-14882
                {
                    // enable auto-sending of debug information
                    e.Configuration.Server.AutoSendSessions = true;
                }
#endif
            }
            catch (Exception ex)
            {
                _gibraltarStartupException = ex;
            }
        }

        private void LogAuthenticationFailure()
        {
            String exception = "Authentication failure";
            if (!_loggedExceptionTimes.ContainsKey(exception))
            {
                _loggedExceptionTimes[exception] = DateTime.UtcNow.AddMinutes(_logExceptionDelay * -1 - 1);
            }
            if ((DateTime.UtcNow - _loggedExceptionTimes[exception]).TotalMinutes > _logExceptionDelay)
            {
                GfsLoggingHelper.Logger.WriteAuthenticationFailure(GfsLoggingHelper.SyncEventLogName);
                _loggedExceptionTimes[exception] = DateTime.UtcNow;
            }
        }

        private void LogUnexpectedException(Exception e)
        {
            String exception = e.ToString();
            if (!_loggedExceptionTimes.ContainsKey(exception))
            {
                _loggedExceptionTimes[exception] = DateTime.UtcNow.AddMinutes(_logExceptionDelay * -1 - 1);
            }
            if ((DateTime.UtcNow - _loggedExceptionTimes[exception]).TotalMinutes > _logExceptionDelay)
            {
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, e);
                _loggedExceptionTimes[exception] = DateTime.UtcNow;
            }
        }

        #endregion

        /// <summary>
        /// Called when performing the processing of the service
        /// </summary>
        private void Process()
        {
            // execute the synchronisation process
            Framework.Processes.ProcessFactory.Execute<Galleria.Ccm.Processes.Synchronization.Process>(new Galleria.Ccm.Processes.Synchronization.Process());
        }
        #endregion
    }
}
