﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using Galleria.Ccm.Helpers;

namespace Galleria.Ccm.Sync.Service
{
    /// <summary>
    /// Installation class for the service
    /// </summary>
    [RunInstaller(true)]
    public partial class ServiceInstaller : Installer
    {
        #region Constants
        private const String _defaultEventLogEntry = "Event Source Registered";
        #endregion

        #region Methods
        /// <summary>
        /// Custom install method used to create event log entry
        /// </summary>
        /// <param name="stateSaver">Saved state dictionary reference</param>
        public override void Install(IDictionary stateSaver)
        {
            // call the base
            base.Install(stateSaver);

            // write default event log entry to the application event log
            using (EventLog eventLog = new EventLog(GfsLoggingHelper.ApplicationLogName, Environment.MachineName, Constants.ServiceName))
            {
                eventLog.WriteEntry(_defaultEventLogEntry);
            }
        }
        #endregion
    }
}
