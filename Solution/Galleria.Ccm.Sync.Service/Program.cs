﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26159 : L.Ineson
//  Added gfs service types registration
#endregion
#endregion

using System;
using System.ServiceProcess;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.Sync.Service
{
    /// <summary>
    /// The main application class
    /// </summary>
    public static class Program
    {
        #region Constants
        private const String _runInConsole = "-CONSOLE"; // the command line argument used to run the service as a service
        #endregion

        #region Methods
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main(params String[] args)
        {
            //register gfs service types
            FoundationServiceClient.RegisterGFSServiceClientTypes();

            // determine if we are going to run the service within the console
            Boolean runInConsole = false;
            if (args.Length > 0)
            {
                runInConsole = (String.Compare(args[0], _runInConsole, StringComparison.InvariantCultureIgnoreCase) == 0);
            }

            // attempt to run the service
            using (Service service = new Service())
            {
                if (runInConsole)
                {
                    service.StartService(args);
                    Console.WriteLine("Press [Enter] to stop the service");
                    Console.ReadLine();
                    service.StopService();
                }
                else
                {
                    ServiceBase.Run(service);
                }
            }
        }
        #endregion
    }
}
