﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Threading;

namespace Galleria.Ccm.Sync.Service
{
    /// <summary>
    /// Defines the interface used for calling back to the hosting service
    /// </summary>
    public interface IService
    {
        #region Properties
        /// <summary>
        /// Indicates the service status
        /// </summary>
        ServiceStatus ServiceStatus { get; }

        /// <summary>
        /// Event used to indicate that the service is shutting down
        /// </summary>
        ManualResetEvent ShutdownEvent { get; }

        /// <summary>
        /// The name of the service, for logging purposes.
        /// </summary>
        String ServiceName { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Starts the service
        /// </summary>
        /// <param name="args">An array of parameters passed on the command line</param>
        void StartService(String[] args);

        /// <summary>
        /// Stops the service
        /// </summary>
        void StopService();

        /// <summary>
        /// Pauses the service
        /// </summary>
        void PauseService();

        /// <summary>
        /// Continues the service from paused
        /// </summary>
        void ContinueService();

        /// <summary>
        /// Installs this service on the local machine
        /// </summary>
        void InstallService();

        /// <summary>
        /// Uninstalls this service from the local machine
        /// </summary>
        void UninstallService();
        #endregion
    }
}