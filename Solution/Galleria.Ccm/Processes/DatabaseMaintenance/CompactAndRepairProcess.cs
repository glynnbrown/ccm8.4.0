﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Processes;
using Galleria.Framework.Dal.Configuration;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Processes.DatabaseMaintenance
{
    [Serializable]
    public partial class CompactAndRepairProcess : ProcessBase<CompactAndRepairProcess>
    {
        #region Fields
        const Int32 _totalStepCount = 2;
        private Int32 _completedStepCount;
        private String _backUpFilePath;
        private DalFactoryConfigElement _dalFactoryConfig;

        private Dictionary<Int32, String> _stepDescriptions =
            new Dictionary<Int32, String>()
            {
                {1, Message.CompactAndRepairProcess_Step1},
                {2, Message.CompactAndRepairProcess_Step2}
            };
        #endregion

        #region Properties

        public String BackupLocation
        {
            get { return _backUpFilePath; }
            set { _backUpFilePath = value; }
        }

        /// <summary>
        /// Returns a dictionary of step numbers to description
        /// </summary>
        public Dictionary<Int32, String> StepDescriptions
        {
            get { return _stepDescriptions; }
        }

        /// <summary>
        /// The optional Dal config to use. If null, Process will
        /// use the default factory in the DalContainer
        /// </summary>
        public DalFactoryConfigElement DalFactoryConfig
        {
            get { return _dalFactoryConfig; }
            set { _dalFactoryConfig = value; }
        }

        #endregion

        #region Event Raising

        public event EventHandler<ProcessProgressEventArgs> OperationCompleted;
        private void RaiseOperationCompleted(Int32 completedStepNumber)
        {
            if (this.OperationCompleted != null)
            {
                ProcessProgressEventArgs args =
                    new ProcessProgressEventArgs(this.ProcessId, _totalStepCount, completedStepNumber, Message.Process_OperationCompleted);

                this.OperationCompleted(this, args);
            }
        }
        #endregion

        #region Constuctor

        /// <summary>
        /// Constructor
        /// </summary>
        public CompactAndRepairProcess()
        { }

        #endregion
    }
}
