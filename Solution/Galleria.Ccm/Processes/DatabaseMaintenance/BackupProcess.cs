﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Processes;
using Galleria.Framework.Dal.Configuration;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Processes.DatabaseMaintenance
{
    [Serializable]
    public partial class BackupProcess : ProcessBase<BackupProcess>
    {
        #region Fields
        const Int32 _totalStepCount = 1;
        private Int32 _completedStepCount;
        private String _backUpFilePath;
        private DalFactoryConfigElement _dalFactoryConfig;

        private Dictionary<Int32, String> _stepDescriptions =
            new Dictionary<Int32, String>()
            {
                {1, Message.DatabaseBackupProcess_Step1}
            };
        #endregion

        #region Properties
        /// <summary>
        /// The file path and name where the backup should be made
        /// </summary>
        public String BackupLocation
        {
            get { return _backUpFilePath; }
            set { _backUpFilePath = value; }
        }

        /// <summary>
        /// The optional Dal config to use. If null, Process will
        /// use the default factory in the DalContainer
        /// </summary>
        public DalFactoryConfigElement DalFactoryConfig
        {
            get { return _dalFactoryConfig; }
            set { _dalFactoryConfig = value; }
        }

        /// <summary>
        /// Returns a dictionary of step numbers to description
        /// </summary>
        public Dictionary<Int32, String> StepDescriptions
        {
            get { return _stepDescriptions; }
        }

        #endregion

        #region Event Raising

        public event EventHandler<ProcessProgressEventArgs> OperationCompleted;
        private void RaiseOperationCompleted(Int32 completedStepNumber)
        {
            if (this.OperationCompleted != null)
            {
                ProcessProgressEventArgs args = new ProcessProgressEventArgs
                    (this.ProcessId, _totalStepCount, completedStepNumber, Message.Process_OperationCompleted);

                this.OperationCompleted(this, args);
            }
        }
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public BackupProcess()
        { }

        #endregion
    }
}
