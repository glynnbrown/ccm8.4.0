﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM802
// V8-29230 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Framework.Processes;

namespace Galleria.Ccm.Processes.DatabaseMaintenance
{
    /// <summary>
    /// A process that is used to create sample data
    /// within the default dal
    /// </summary>
    public partial class CreateSampleData : ProcessBase<CreateSampleData>
    {
    }
}
