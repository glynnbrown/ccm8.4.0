﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25903 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Processes.DatabaseMaintenance
{
    public partial class BackupProcess
    {

        #region Execute
        protected override void OnExecute()
        {
            //call the dal factory method 
            IDalFactory dalFactory;
            if (this.DalFactoryConfig != null)
            {
                //We are creating a bespoke Dalfactory for the process
                dalFactory = DalContainer.CreateFactory(this.DalFactoryConfig);
            }
            else
            {
                //We are using the default DalFactory
                dalFactory = DalContainer.GetDalFactory();
            }
            dalFactory.OperationCompleted += new EventHandler(dalFactory_OperationCompleted);
            dalFactory.BackupDatabase(this.BackupLocation);
            dalFactory.OperationCompleted -= dalFactory_OperationCompleted;
        }

        #endregion


        #region Event Handlers

        private void dalFactory_OperationCompleted(object sender, EventArgs e)
        {
            _completedStepCount++;

            RaiseOperationCompleted(_completedStepCount);
        }

        #endregion

    }
}
