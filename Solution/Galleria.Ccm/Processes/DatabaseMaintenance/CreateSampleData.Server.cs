﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM802
// V8-29230 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-29780 : D.Pleasance
//  Added CreateAssortments()
// V8-29819 : D.Pleasance
//  Added CreateMetricsAndMetricProfile()
// V8-29833 : N.Foster
//  Included sample cluster schemes
// V8-29885 : N.Foster
//  Included content links
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using Aspose.Cells;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Imports.Processes;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Enums;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Processes.DatabaseMaintenance
{
    /// <summary>
    /// A process used to import sample data during database creation
    /// </summary>
    public partial class CreateSampleData
    {
        #region Constants
        // directories
        private const String _sampleDirectoryPlanograms = @"Sample Files\Planograms";
        private const String _sampleDirectoryAssortments = @"Sample Files\Assortments";
        private const String _sampleDirectoryProductUniverses = @"Sample Files\Product Universes";
        private const String _sampleDirectoryClusterSchemes = @"Sample Files\Cluster Schemes";
        // files
        private const String _sampleFileMerchandisingHierarchy = @"Sample Files\Master Data\Merchandising Hierarchy.xlsx";
        private const String _sampleFileLocationHierarchy = @"Sample Files\Master Data\Location Hierarchy.xlsx";
        private const String _sampleFileLocations = @"Sample Files\Master Data\Locations.xlsx";
        private const String _sampleFileLocationSpace = @"Sample Files\Master Data\Location Space.xlsx";
        private const String _sampleFileLocationSpaceBay = @"Sample Files\Master Data\Location Space Bay.xlsx";
        private const String _sampleFilePlanogramAssignment = @"Sample Files\Master Data\Planogram Assignment.xlsx";
        private const String _sampleFileMetricProfile = @"Sample Files\Master Data\Metric Profile.xlsx";
        private const String _sampleFileContentLinks = @"Sample Files\Master Data\Planogram Content Links.xlsx";
        // names
        private const String _sampleTemplates = "Templates";
        private const String _sampleLocationSpecific = "Location Specific";
        #endregion

        #region Nested Classes

        #region ProcessContext
        /// <summary>
        /// A process context class for this process
        /// </summary>
        private class ProcessContext : IDisposable
        {
            #region Fields
            private String _samplesPath;
            private User _user;
            private Entity _entity;
            private ProductHierarchy _merchandisingHierarchy;
            private LocationHierarchy _locationHierarchy;
            private PlanogramHierarchy _planogramHierarchy;
            private ProductUniverseInfoList _productUniverses;
            private LocationInfoList _locations;
            private LocationSpaceInfoList _locationSpace;
            private PlanogramInfoList _planograms;
            private AssortmentInfoList _assortments;
            private List<PlanogramPerformanceMetric> _planogramMetrics = new List<PlanogramPerformanceMetric>();
            private ClusterSchemeInfoList _clusterSchemes;
            private List<MetricProfile> _metricProfiles = new List<MetricProfile>();
            private InventoryProfileInfoList _inventoryProfiles;
            private RenumberingStrategyInfoList _renumberingStrategies;
            private ValidationTemplateInfoList _validationTemplates;
            private ContentLookupList _contentLookups;

            #endregion

            #region Properties
            /// <summary>
            /// Holds the full path to where all samples are extracted
            /// </summary>
            public String SamplesPath
            {
                get { return _samplesPath; }
                set { _samplesPath = value; }
            }

            /// <summary>
            /// Gets the current user
            /// </summary>
            public User User
            {
                get { return _user; }
            }

            /// <summary>
            /// Gets or sets the entity
            /// </summary>
            public Entity Entity
            {
                get { return _entity; }
                set { _entity = value; }
            }

            /// <summary>
            /// Gets or sets the merchandising hierarchy
            /// </summary>
            public ProductHierarchy MerchandisingHierarchy
            {
                get { return _merchandisingHierarchy; }
                set { _merchandisingHierarchy = value; }
            }

            /// <summary>
            /// Gets or sets the location hierarchy
            /// </summary>
            public LocationHierarchy LocationHierarchy
            {
                get { return _locationHierarchy; }
                set { _locationHierarchy = value; }
            }

            /// <summary>
            /// Gets or sets the planogram hierarchy
            /// </summary>
            public PlanogramHierarchy PlanogramHierarchy
            {
                get { return _planogramHierarchy; }
                set { _planogramHierarchy = value; }
            }

            /// <summary>
            /// Gets or set the list of available product universes
            /// </summary>
            public ProductUniverseInfoList ProductUniverses
            {
                get { return _productUniverses; }
                set { _productUniverses = value; }
            }

            /// <summary>
            /// Gets or sets the list of locations
            /// </summary>
            public LocationInfoList Locations
            {
                get { return _locations; }
                set { _locations = value; }
            }

            /// <summary>
            /// Gets or sets location space
            /// </summary>
            public LocationSpaceInfoList LocationSpace
            {
                get { return _locationSpace; }
                set { _locationSpace = value; }
            }

            /// <summary>
            /// Gets or sets the list of planograms
            /// </summary>
            public PlanogramInfoList Planograms
            {
                get { return _planograms; }
                set { _planograms = value; }
            }

            /// <summary>
            /// Gets or sets the list of assortments
            /// </summary>
            public AssortmentInfoList Assortments
            {
                get { return _assortments; }
                set { _assortments = value; }
            }

            /// <summary>
            /// Gets or sets the metrics
            /// </summary>
            public List<PlanogramPerformanceMetric> PlanogramMetrics
            {
                get { return _planogramMetrics; }
            }

            /// <summary>
            /// Gets or sets the list of imported cluster schemes
            /// </summary>
            public ClusterSchemeInfoList ClusterSchemes
            {
                get { return _clusterSchemes; }
                set { _clusterSchemes = value; }
            }

            /// <summary>
            /// Gets a list of all metric profiles created as part of the process
            /// </summary>
            public List<MetricProfile> MetricProfiles
            {
                get { return _metricProfiles; }
            }

            /// <summary>
            /// Gets or sets the list of available inventory profiles
            /// </summary>
            public InventoryProfileInfoList InventoryProfiles
            {
                get { return _inventoryProfiles; }
                set { _inventoryProfiles = value; }
            }

            /// <summary>
            /// Gets or sets the list of available renumbering strategies
            /// </summary>
            public RenumberingStrategyInfoList RenumberingStrategies
            {
                get { return _renumberingStrategies; }
                set { _renumberingStrategies = value; }
            }

            /// <summary>
            /// Gets or sets the list of validation templates
            /// </summary>
            public ValidationTemplateInfoList ValidationTemplates
            {
                get { return _validationTemplates; }
                set { _validationTemplates = value; }
            }

            /// <summary>
            /// Gets or sets the content lookup information
            /// </summary>
            public ContentLookupList ContentLookups
            {
                get { return _contentLookups; }
                set { _contentLookups = value; }
            }

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public ProcessContext()
            {
                _user = User.GetCurrentUser();
            }
            #endregion

            #region Methods
            /// <summary>
            /// Called when this instance is diposed
            /// </summary>
            public void Dispose()
            {
                // delete the samples path if it exists
                if (Directory.Exists(_samplesPath))
                {
                    Directory.Delete(_samplesPath, true);
                }
            }
            #endregion
        }
        #endregion

        #endregion

        #region Fields
        private static Assembly _samplesAssembly = null; // holds a reference to the samples assembly once loaded
        #endregion

        #region Execute
        /// <summary>
        /// Called when executing this process
        /// </summary>
        protected override void OnExecute()
        {
            using (ProcessContext context = new ProcessContext())
            {
                CreateSampleFiles(context);
                CreateEntity(context);
                CreateMerchandisingHierarchyLevels(context);
                CreateMerchandisingHierarchy(context);
                CreateLocationHierarchyLevels(context);
                CreateLocationHierarchy(context);
                CreateLocations(context);
                CreateLocationSpace(context);
                CreateLocationSpaceBay(context);
                CreateProductUniverses(context);
                CreateCombinedProductUnivereses(context);
                CreatePlanogramHierarchy(context);
                CreatePlanograms(context);
                CreatePlanogramAssignments(context);
                CreateMetricsAndMetricProfile(context);
                CreateAssortments(context);
                CreateClusterSchemes(context);
                CreatePlanogramContentLinks(context);
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Extracts all the sample files from the samples assembly
        /// into a temporary folder on disk, so that we can import
        /// them into the solution database
        /// </summary>
        private static void CreateSampleFiles(ProcessContext context)
        {
            // load the assembly if it has already been loaded
            if (_samplesAssembly == null)
            {
                // build the samples assembly path
                String samplesAssemblyPath = Assembly.GetExecutingAssembly().CodeBase.Replace(".DLL", ".Samples.DLL");
                if (samplesAssemblyPath.Substring(0, 7) == "file://")
                {
                    samplesAssemblyPath = samplesAssemblyPath.Substring(8);
                    samplesAssemblyPath = samplesAssemblyPath.Replace("/", "\\");
                }

                // load the assembly
                _samplesAssembly = Assembly.LoadFrom(samplesAssemblyPath);
            }

            // generate a temporary folder to which we will
            // extract all resources within the samples folder
            context.SamplesPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());

            // now iterate through all resources within the samples
            // assembly and extract them to the samples directory
            foreach (String resourceName in _samplesAssembly.GetManifestResourceNames())
            {
                // build the output path for the resource
                String resourcePath = Path.Combine(context.SamplesPath,
                    String.Format("{0}{1}",
                        Path.GetFileNameWithoutExtension(resourceName)
                            .Replace("Galleria.Ccm.Samples.", String.Empty)
                            .Replace("Customer_Centric_Merchandising.", String.Empty)
                            .Replace("Common.", String.Empty)
                            .Replace("Editor.", String.Empty)
                            .Replace("Workflow.", String.Empty)
                            .Replace("_", " ")
                            .Replace(".", "\\"),
                        Path.GetExtension(resourceName)));

                // create the output directory
                Directory.CreateDirectory(Path.GetDirectoryName(resourcePath));

                // write the file to disk
                using (Stream resourceStream = _samplesAssembly.GetManifestResourceStream(resourceName))
                {
                    using (Stream outputStream = System.IO.File.Create(resourcePath))
                    {
                        resourceStream.CopyTo(outputStream);
                    }
                }
            }
        }

        /// <summary>
        /// Creates an entity in the solution
        /// </summary>
        private static void CreateEntity(ProcessContext context)
        {
            // create the entity
            Entity entity = Entity.NewEntity();
            entity.Name = "Galleria RTS";

            // change any settings required by the sample data
            // NF - TODO

            // save the entity and store in the process context
            context.Entity = entity.Save();

            // fetch any items that were created when the entity was created
            context.InventoryProfiles = InventoryProfileInfoList.FetchByEntityId(context.Entity.Id);
            context.RenumberingStrategies = RenumberingStrategyInfoList.FetchByEntityId(context.Entity.Id);
            context.ValidationTemplates = ValidationTemplateInfoList.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Creates the merchandising hierarchy
        /// </summary>
        private static void CreateMerchandisingHierarchyLevels(ProcessContext context)
        {
            // get the current merchandising hierarchy
            ProductHierarchy merchandisingHierarchy = ProductHierarchy.FetchByEntityId(context.Entity.Id);

            // rename all levels to avoid duplicates when importing
            ProductLevel tempLevel = merchandisingHierarchy.RootLevel;
            while (tempLevel != null)
            {
                tempLevel.Name = tempLevel.Id.ToString();
                tempLevel.Colour = 0;
                tempLevel.ShapeNo = 0;
                tempLevel = tempLevel.ChildLevel;
            }

            // save the hierarchy to commit the rename
            merchandisingHierarchy = merchandisingHierarchy.Save();

            // get the root level
            ProductLevel parentLevel = merchandisingHierarchy.RootLevel;
            parentLevel.Name = "Galleria RTS";

            // generate the merchandising hierarchy file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFileMerchandisingHierarchy);

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);
            foreach (ImportFileDataColumn column in importData.Columns)
            {
                // get the column header
                String header = column.Header;
                if (header.Contains(" Code"))
                {
                    // get the level name
                    String levelName = header.Replace(" Code", String.Empty);

                    // get the product level if it already exists
                    ProductLevel productLevel = parentLevel.ChildLevel;
                    if (productLevel == null) productLevel = ProductLevel.NewProductLevel();

                    // set the product level properties
                    productLevel.Name = levelName;

                    // add to the merchandising hierarchy if required
                    if (parentLevel.ChildLevel == null)
                        merchandisingHierarchy.AddLevel(productLevel, parentLevel);

                    // and make the new level the parent level
                    parentLevel = productLevel;
                }
            }

            // save the merchandising hierarchy into the context
            context.MerchandisingHierarchy = merchandisingHierarchy.Save();
        }

        /// <summary>
        /// Imports the merchandising hierarchy file
        /// </summary>
        private static void CreateMerchandisingHierarchy(ProcessContext context)
        {
            // generate the merchandising hierarchy file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFileMerchandisingHierarchy);

            // build a list of all merch hierarchy levels
            List<String> merchHierarchylevels = new List<String>();
            ProductLevel level = context.MerchandisingHierarchy.RootLevel.ChildLevel;
            while (level != null)
            {
                merchHierarchylevels.Add(level.Name);
                level = level.ChildLevel;
            }

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);

            // set the mappings for the import
            importData.MappingList = MerchHierarchyImportMappingList.NewMerchHierarchyImportMappingList(merchHierarchylevels);
            foreach (ImportFileDataColumn column in importData.Columns)
            {
                ImportMapping mapping = importData.MappingList.FirstOrDefault(m => String.Compare(m.PropertyName.Trim(), column.Header.Trim(), true) == 0);
                if (mapping != null) mapping.ColumnReference = column.Header;
            }

            // execute the import process
            ProcessFactory.Execute<ImportMerchandisingHierarchyProcess>(
                new ImportMerchandisingHierarchyProcess(context.Entity.Id, importData).Execute());

            // retrieve the updated merchandising hiearchy
            context.MerchandisingHierarchy = ProductHierarchy.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Creates the merchandising hierarchy
        /// </summary>
        private static void CreateLocationHierarchyLevels(ProcessContext context)
        {
            // get the current location hierarchy
            LocationHierarchy locationHierarchy = LocationHierarchy.FetchByEntityId(context.Entity.Id);

            // rename all levels to avoid duplicates when importing
            LocationLevel tempLevel = locationHierarchy.RootLevel;
            while (tempLevel != null)
            {
                tempLevel.Name = tempLevel.Id.ToString();
                tempLevel.Colour = 0;
                tempLevel.ShapeNo = 0;
                tempLevel = tempLevel.ChildLevel;
            }

            // save the hierarchy to commit the rename
            locationHierarchy = locationHierarchy.Save();

            // get the root level
            LocationLevel parentLevel = locationHierarchy.RootLevel;
            parentLevel.Name = "Galleria RTS";

            // generate the merchandising hierarchy file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFileLocationHierarchy);

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);
            foreach (ImportFileDataColumn column in importData.Columns)
            {
                // get the column header
                String header = column.Header;
                if (header.Contains(" Code"))
                {
                    // get the level name
                    String levelName = header.Replace(" Code", String.Empty);

                    // get the product level if it already exists
                    LocationLevel locationLevel = parentLevel.ChildLevel;
                    if (locationLevel == null) locationLevel = LocationLevel.NewLocationLevel();

                    // set the product level properties
                    locationLevel.Name = levelName;

                    // add to the merchandising hierarchy if required
                    if (parentLevel.ChildLevel == null)
                        locationHierarchy.AddLevel(locationLevel, parentLevel);

                    // and make the new level the parent level
                    parentLevel = locationLevel;
                }
            }

            // save the merchandising hierarchy into the context
            context.LocationHierarchy = locationHierarchy.Save();
        }

        /// <summary>
        /// Imports the location hierarchy file
        /// </summary>
        private static void CreateLocationHierarchy(ProcessContext context)
        {
            // generate the location hierarchy file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFileLocationHierarchy);

            // build a list of all merch hierarchy levels
            List<String> locationHierarchylevels = new List<String>();
            LocationLevel level = context.LocationHierarchy.RootLevel.ChildLevel;
            while (level != null)
            {
                locationHierarchylevels.Add(level.Name);
                level = level.ChildLevel;
            }

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);

            // set the mappings for the import
            importData.MappingList = LocationHierarchyImportMappingList.NewList(locationHierarchylevels);
            foreach (ImportFileDataColumn column in importData.Columns)
            {
                ImportMapping mapping = importData.MappingList.FirstOrDefault(m => String.Compare(m.PropertyName.Trim(), column.Header.Trim(), true) == 0);
                if (mapping != null) mapping.ColumnReference = column.Header;
            }

            // execute the import process
            ProcessFactory.Execute<ImportLocationHierarchyProcess>(
                new ImportLocationHierarchyProcess(context.Entity.Id, importData).Execute());

            // retrieve the updated merchandising hiearchy
            context.LocationHierarchy = LocationHierarchy.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Imports the locations file
        /// </summary>
        private static void CreateLocations(ProcessContext context)
        {
            // generate the location hierarchy file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFileLocations);

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);

            // set the mappings for the import
            importData.MappingList = LocationImportMappingList.NewLocationImportMappingList();
            foreach (ImportFileDataColumn column in importData.Columns)
            {
                ImportMapping mapping = importData.MappingList.FirstOrDefault(m => String.Compare(m.PropertyName.Trim(), column.Header.Trim(), true) == 0);
                if (mapping != null) mapping.ColumnReference = column.Header;
            }

            // execute the process to import the products
            // into master data
            ProcessFactory.Execute<ImportLocationsProcess>(
                new ImportLocationsProcess(context.Entity.Id, importData, String.Format("New Group {0}", DateTime.Now)));

            // load the locations into the context
            context.Locations = LocationInfoList.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Imports the locations space file
        /// </summary>
        private static void CreateLocationSpace(ProcessContext context)
        {
            // generate the location hierarchy file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFileLocationSpace);

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);

            // set the mappings for the import
            importData.MappingList = LocationSpaceImportMappingList.NewLocationSpaceImportMappingList();
            foreach (ImportFileDataColumn column in importData.Columns)
            {
                ImportMapping mapping = importData.MappingList.FirstOrDefault(m => String.Compare(m.PropertyName.Trim(), column.Header.Trim(), true) == 0);
                if (mapping != null) mapping.ColumnReference = column.Header;
            }

            // execute the process to import the products
            // into master data
            ProcessFactory.Execute<ImportLocationSpaceProcess>(
                new ImportLocationSpaceProcess(context.Entity.Id, importData));

            // load location space into the process context
            context.LocationSpace = LocationSpaceInfoList.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Imports the locations space bay file
        /// </summary>
        private static void CreateLocationSpaceBay(ProcessContext context)
        {
            // generate the location hierarchy file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFileLocationSpaceBay);

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);

            // set the mappings for the import
            importData.MappingList = LocationSpaceBayImportMappingList.NewLocationSpaceBayImportMappingList();
            foreach (ImportFileDataColumn column in importData.Columns)
            {
                ImportMapping mapping = importData.MappingList.FirstOrDefault(m => String.Compare(m.PropertyName.Trim(), column.Header.Trim(), true) == 0);
                if (mapping != null) mapping.ColumnReference = column.Header;
            }

            // execute the process to import the products
            // into master data
            ProcessFactory.Execute<ImportLocationSpaceBayProcess>(
                new ImportLocationSpaceBayProcess(context.Entity.Id, importData));

            // load location space into the process context
            context.LocationSpace = LocationSpaceInfoList.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Creates the planogram hierarchy from the merch hierarchy
        /// </summary>
        private static void CreatePlanogramHierarchy(ProcessContext context)
        {
            // get the lowest level of the merchandising hierarchy
            ProductLevel productLevel = context.MerchandisingHierarchy.RootLevel;
            while (productLevel.ChildLevel != null)
            {
                productLevel = productLevel.ChildLevel;
            }

            // edit the entity to enable the plan hierarchy sync
            context.Entity.ProductLevelId = productLevel.Id;

            // run the plan hierarchy sync
            context.Entity.SyncPlanHierarchy();

            // retrieve the planogram hierarchy
            context.PlanogramHierarchy = PlanogramHierarchy.FetchByEntityId(context.Entity.Id);

            // add child groups to all required planogram groups
            foreach (PlanogramGroup planogramGroup in context.PlanogramHierarchy.EnumerateAllGroups().ToList())
            {
                if ((planogramGroup != context.PlanogramHierarchy.RootGroup) &&
                    (planogramGroup.ProductGroupId != null))
                {
                    // create a template group
                    PlanogramGroup templateGroup = PlanogramGroup.NewPlanogramGroup();
                    templateGroup.Name = _sampleTemplates;
                    planogramGroup.ChildList.Add(templateGroup);

                    // create a location specific group
                    PlanogramGroup locationSpecificGroup = PlanogramGroup.NewPlanogramGroup();
                    locationSpecificGroup.Name = _sampleLocationSpecific;
                    planogramGroup.ChildList.Add(locationSpecificGroup);
                }
            }

            // and save the planogram hierarchy
            context.PlanogramHierarchy = context.PlanogramHierarchy.Save();
        }

        /// <summary>
        /// Imports planogram into the database and saves
        /// them under the correct planogram hierarchy group
        /// </summary>
        private static void CreatePlanograms(ProcessContext context)
        {
            // build the path to the planograms folder
            String planogramsPath = Path.Combine(context.SamplesPath, _sampleDirectoryPlanograms);   

            // build a lookup of all the groups in the merchandising hierarchy
            Dictionary<Int32, ProductGroup> productGroups = new Dictionary<Int32, ProductGroup>();
            foreach (ProductGroup productGroup in context.MerchandisingHierarchy.RootGroup.EnumerateAllChildGroups())
            {
                productGroups.Add(productGroup.Id, productGroup);
            }

            // build a lookup of all the groups in the planogram hierachy
            Dictionary<String, PlanogramGroup> planogramGroups = new Dictionary<String, PlanogramGroup>();
            foreach (PlanogramGroup planogramGroup in context.PlanogramHierarchy.RootGroup.EnumerateAllChildGroups())
            {
                if (planogramGroup.ProductGroupId != null)
                {
                    planogramGroups.Add(productGroups[(Int32)planogramGroup.ProductGroupId].Code, planogramGroup);
                }
            }

            // import all planograms under this directory
            String[] planogramFileNames = Directory.GetFiles(planogramsPath, "*.pog", SearchOption.AllDirectories);
            List<Int32> planogramIds = new List<Int32>();
            foreach (String planogramFileName in planogramFileNames)
            {
                // load the planogram from the file
                Package filePackage = Package.FetchByFileName(planogramFileName);

                // create helper
                IPlanogramMetadataHelper metadataHelper = PlanogramMetadataHelper.NewPlanogramMetadataHelper(context.Entity.Id, filePackage);

                // ensure meta data is up-to-date for this package
                filePackage.CalculateMetadata(false, metadataHelper);

                // get the first planogram in the package
                Planogram filePlanogram = filePackage.Planograms[0];

                // add metrics to the context
                GetPlanogramMetrics(context, filePlanogram);

                // save the planogram to the database
                Package databasePackage = filePackage.SaveAs(DomainPrincipal.CurrentUserId, false, context.Entity.Id);

                // record the new planogram package id
                planogramIds.Add((Int32)databasePackage.Id);

                // determine if we can link this planogram to a planogram group
                if ((String.IsNullOrEmpty(filePlanogram.CategoryCode)) ||
                    (String.IsNullOrWhiteSpace(filePlanogram.CategoryCode)) ||
                    (!planogramGroups.ContainsKey(filePlanogram.CategoryCode)))
                {
                    // associate this package to the root group
                    context.PlanogramHierarchy.RootGroup.AssociatePlanograms(databasePackage.Planograms);
                }
                else
                {
                    // get the category planogram group
                    PlanogramGroup planogramGroup = planogramGroups[filePlanogram.CategoryCode];

                    // determine if the planogram is a template
                    // or location specific planogram
                    if ((filePlanogram.PlanogramType == PlanogramType.BlockingTemplate) || (filePlanogram.PlanogramType == PlanogramType.SequenceTemplate))
                    {
                        // determine if the planogram group has a 
                        // sub folder for location specific planograms
                        PlanogramGroup childPlanogramGroup = null;
                        foreach (PlanogramGroup item in planogramGroup.ChildList)
                        {
                            if (item.Name == _sampleTemplates)
                            {
                                childPlanogramGroup = item;
                                break;
                            }
                        }

                        // associate the planogram to this group, if one was located
                        if (childPlanogramGroup != null)
                            childPlanogramGroup.AssociatePlanograms(databasePackage.Planograms);
                    }
                    else if (!String.IsNullOrWhiteSpace(filePlanogram.LocationCode))
                    {
                        // determine if the planogram group has a 
                        // sub folder for location specific planograms
                        PlanogramGroup childPlanogramGroup = null;
                        foreach (PlanogramGroup item in planogramGroup.ChildList)
                        {
                            if (item.Name == _sampleLocationSpecific)
                            {
                                childPlanogramGroup = item;
                                break;
                            }
                        }

                        // associate the planogram to this group, if one was located
                        if (childPlanogramGroup != null)
                            childPlanogramGroup.AssociatePlanograms(databasePackage.Planograms);
                    }
                    else
                    {
                        // associate this package to the planogram group
                        planogramGroup.AssociatePlanograms(databasePackage.Planograms);
                    }
                }

                // dispose of the packages
                filePackage.Dispose();
                databasePackage.Dispose();
            }

            // load the list of planograms into the process context
            context.Planograms = PlanogramInfoList.FetchByIds(planogramIds);
        }

        /// <summary>
        /// Builds a list of metrics from all the imported planograms
        /// </summary>
        private static void GetPlanogramMetrics(ProcessContext context, Planogram filePlanogram )
        {
            if (filePlanogram.Performance != null)
            {
                foreach (PlanogramPerformanceMetric planogramPerformanceMetric in filePlanogram.Performance.Metrics)
                {
                    if (!context.PlanogramMetrics.Where(p => p.Name == planogramPerformanceMetric.Name).Any())
                    {
                        context.PlanogramMetrics.Add(planogramPerformanceMetric);
                    }
                }
            }
        }

        /// <summary>
        /// Imports product universes into the database
        /// </summary>
        private static void CreateProductUniverses(ProcessContext context)
        {
            // build the path to the planograms folder
            String productUniversesPath = Path.Combine(context.SamplesPath, _sampleDirectoryProductUniverses);

            // import all product univereses under this directory
            String[] productUniverseFileNames = Directory.GetFiles(productUniversesPath, "*.xlsx", SearchOption.AllDirectories);
            foreach (String productUniversefileName in productUniverseFileNames)
            {
                // import the file
                ImportFileData importData = ImportFromExcel(productUniversefileName);

                // set the mappings for the import
                importData.MappingList = ProductImportMappingList.NewProductImportMappingList(true);
                foreach (ImportFileDataColumn column in importData.Columns)
                {
                    ImportMapping mapping = importData.MappingList.FirstOrDefault(m => String.Compare(m.PropertyName.Trim(), column.Header.Trim(), true) == 0);
                    if (mapping != null) mapping.ColumnReference = column.Header;
                }

                // execute the process to import the products
                // into master data
                ProcessFactory.Execute<ImportProductsProcess>(
                    new ImportProductsProcess(context.Entity.Id, importData));
            }

            // retrieve the list of all product universes
            // that have been created within the solution
            context.ProductUniverses = ProductUniverseInfoList.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Recursively creates combined product univereses
        /// </summary>
        private static IEnumerable<ProductUniverseProduct> CreateCombinedProductUnivereses(ProcessContext context, ProductGroup productGroup = null)
        {
            // start with the root group if required
            if (productGroup == null) return CreateCombinedProductUnivereses(context, context.MerchandisingHierarchy.RootGroup);

            // locate the product universe info that matches the product group
            ProductUniverseInfo productUniverseInfo = context.ProductUniverses.FirstOrDefault(item => item.Id == productGroup.Id);

            // if a product universe cannot be located
            // then return an empty list
            if (productUniverseInfo == null) return new List<ProductUniverseProduct>();

            // fetch the full product universe
            ProductUniverse productUniverse = ProductUniverse.FetchById(productUniverseInfo.Id);

            // build a list of all product universe products
            // that exist within the current product universe
            List<String> productCodes = new List<String>();
            foreach (ProductUniverseProduct product in productUniverse.Products)
            {
                productCodes.Add(product.Gtin);
            }

            // add all child products to this product universe
            foreach (ProductGroup childProductGroup in productGroup.ChildList)
            {
                foreach (ProductUniverseProduct product in CreateCombinedProductUnivereses(context, childProductGroup))
                {
                    if (!productCodes.Contains(product.Gtin))
                    {
                        productUniverse.Products.Add(product.Copy());
                        productCodes.Add(product.Gtin);
                    }
                }
            }

            // save the product universe
            productUniverse = productUniverse.Save();

            // return a list of all product universe products
            return productUniverse.Products;
        }

        /// <summary>
        /// Creates plan assignment information
        /// </summary>
        private static void CreatePlanogramAssignments(ProcessContext context)
        {
            // build a lookup of product group codes to id
            Dictionary<String, Int32> productGroupIds = new Dictionary<String,Int32>();
            foreach (ProductGroup productGroup in context.MerchandisingHierarchy.RootGroup.EnumerateAllChildGroups())
            {
                productGroupIds.Add(productGroup.Code, productGroup.Id);
            }

            // build a lookup of all location codes to id
            Dictionary<String, Int16> locationIds = new Dictionary<String,Int16>();
            foreach (LocationInfo location in context.Locations)
            {
                locationIds.Add(location.Code, location.Id);
            }

            // build a lookup of all planogram names to id
            Dictionary<String, Int32> planogramIds = new Dictionary<String,Int32>();
            foreach (PlanogramInfo planogram in context.Planograms)
            {
                planogramIds.Add(planogram.Name, planogram.Id);
            }

            // create the empty list of location plan assignments
            LocationPlanAssignmentList planAssignments = LocationPlanAssignmentList.NewLocationPlanAssignmentList();

            // generate the planogram assigment file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFilePlanogramAssignment);

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);

            // enumerate through the import data
            // and create the plan assignments
            for (Int32 i = 1; i < importData.Rows.Count; i++)
            {
                // get the row we are interested in
                ImportFileDataRow row = importData.Rows[i];

                // get the row details
                String productGroupCode = row[1].OriginalValue.ToString();
                String locationCode = row[2].OriginalValue.ToString();
                String planogramName = row[3].OriginalValue.ToString();

                // verify we have data for this row
                if ((productGroupIds.ContainsKey(productGroupCode)) &&
                    (locationIds.ContainsKey(locationCode)) &&
                    (planogramIds.ContainsKey(planogramName)))
                {
                    // get the ids based on the row data
                    Int32 productGroupId = productGroupIds[productGroupCode];
                    Int16 locationId = locationIds[locationCode];
                    Int32 planogramId = planogramIds[planogramName];

                    // assign this planogram to this store category
                    planAssignments.Add(LocationPlanAssignment.NewLocationPlanAssignment(
                        productGroupId,
                        planogramId,
                        locationId,
                        context.User,
                        DateTime.UtcNow));
                }
            }

            // save the plan assignment list
            planAssignments.Save();
        }

        /// <summary>
        /// Creates distinct list of metrics from the imported planograms and creates default sample metric profile
        /// </summary>
        /// <param name="context"></param>
        private static void CreateMetricsAndMetricProfile(ProcessContext context)
        {
            #region Create Metrics

            MetricList metricList = MetricList.NewMetricList();

            foreach (PlanogramPerformanceMetric planogramMetric in context.PlanogramMetrics)
            {
                Metric metric = Metric.NewMetric(context.Entity.Id);
                metric.Name = planogramMetric.Name;
                metric.Description = planogramMetric.Description;
                metric.Direction = (EnumHelper.Parse<MetricDirectionType>(planogramMetric.Direction.ToString(), MetricDirectionType.MaximiseIncrease));
                metric.Type = (EnumHelper.Parse<MetricType>(planogramMetric.Type.ToString(), MetricType.Decimal));
                metric.SpecialType = (EnumHelper.Parse<MetricSpecialType>(planogramMetric.SpecialType.ToString(), MetricSpecialType.None));

                metricList.Add(metric);
            }

            metricList = metricList.Save();

            #endregion

            #region Create Metric Profile

            MetricProfile metricProfile = MetricProfile.NewMetricProfile(context.Entity.Id);
            metricProfile.Name = "Sample Metric Profile";

            // Get metric profile file
            String fileName = Path.Combine(context.SamplesPath, _sampleFileMetricProfile);

            // import the file data
            ImportFileData importData = ImportFromExcel(fileName);

            // create metric profile
            PropertyInfo property = null;
            Byte metricCount = 1;
            foreach (ImportFileDataRow row in importData.Rows)
            {
                String metricIdField = String.Format("Metric{0}MetricId", metricCount);
                String metricRatioField = String.Format("Metric{0}Ratio", metricCount);

                property = typeof(MetricProfile).GetProperty(metricIdField);
                if (property != null)
                {
                    String metricName = null;
                    Single metricRatio;
                    try
                    {
                        metricName = row.Cells[0].ToString();
                        metricRatio = Convert.ToSingle(row.Cells[1].ToString());
                    }
                    catch { }

                    if (metricName != null)
                    {
                        Metric metric = metricList.Where(p => p.Name == row.Cells[0].ToString()).FirstOrDefault();
                        if (metric != null)
                        {
                            property.SetValue(metricProfile, metric.Id, null);
                            property = typeof(MetricProfile).GetProperty(metricRatioField);
                            property.SetValue(metricProfile, Convert.ToSingle(row.Cells[1].ToString()), null);
                            metricCount++;
                        }
                    }
                }
            }

            // save and add the metric profile to the context
            if (metricCount > 1) context.MetricProfiles.Add(metricProfile.Save());

            #endregion
        }

        /// <summary>
        /// Creates the sample assortment data
        /// </summary>
        /// <param name="context"></param>
        private static void CreateAssortments(ProcessContext context)
        {
            // build the path to the assortment folder
            String assortmentsPath = Path.Combine(context.SamplesPath, _sampleDirectoryAssortments);

            // import all assortments under this directory
            String[] assortmentFileNames = Directory.GetFiles(assortmentsPath, "*.xlsx", SearchOption.AllDirectories);
            foreach (String assortmentFileName in assortmentFileNames)
            {
                // import the file
                ImportFileData importData = ImportFromExcel(assortmentFileName);

                // set the mappings for the import
                importData.MappingList = AssortmentImportMappingList.NewAssortmentImportMappingList();
                foreach (ImportFileDataColumn column in importData.Columns)
                {
                    ImportMapping mapping = importData.MappingList.FirstOrDefault(m => String.Compare(m.PropertyName.Trim(), column.Header.Trim(), true) == 0);
                    if (mapping != null) mapping.ColumnReference = column.Header;
                }

                // execute the process to import the assortment
                ProcessFactory.Execute<ImportAssortmentProcess>(
                    new ImportAssortmentProcess(context.Entity.Id, importData));
            }

            // load the assortments into the context
            context.Assortments = AssortmentInfoList.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Creates default cluster schemes
        /// </summary>
        private static void CreateClusterSchemes(ProcessContext context)
        {
            // build the path to the cluster scheme folder
            String clusterSchemesPath = Path.Combine(context.SamplesPath, _sampleDirectoryClusterSchemes);

            // import all assortments under this directory
            String[] clusterSchemeFileNames = Directory.GetFiles(clusterSchemesPath, "*.xlsx", SearchOption.AllDirectories);
            foreach (String clusterSchemeFileName in clusterSchemeFileNames)
            {
                // import the file
                ImportFileData importData = ImportFromExcel(clusterSchemeFileName);

                // set the mappings for the import
                importData.MappingList = LocationClusterImportMappingList.NewLocationClusterImportMappingList();
                foreach (ImportFileDataColumn column in importData.Columns)
                {
                    ImportMapping mapping = importData.MappingList.FirstOrDefault(m => String.Compare(m.PropertyName.Trim(), column.Header.Trim(), true) == 0);
                    if (mapping != null) mapping.ColumnReference = column.Header;
                }

                // execute the process to import the assortment
                ProcessFactory.Execute<ImportLocationClustersProcess>(
                    new ImportLocationClustersProcess(context.Entity.Id, importData));
            }

            // load the assortments into the context
            context.ClusterSchemes = ClusterSchemeInfoList.FetchByEntityId(context.Entity.Id);
        }

        /// <summary>
        /// Creates content links for the sample data
        /// </summary>
        private void CreatePlanogramContentLinks(ProcessContext context)
        {
            // build a dictionary of content lookups for all
            // planograms specified within the database
            // indexed by the planogram name
            Dictionary<String, ContentLookup> planogramLookups = new Dictionary<String,ContentLookup>();
            foreach (PlanogramInfo planogram in context.Planograms)
            {
                ContentLookup contentLookup = ContentLookup.NewContentLookup(context.Entity.Id);
                contentLookup.PlanogramId = planogram.Id;
                contentLookup.PlanogramName = planogram.Name;
                contentLookup.UserId = (Int32)DomainPrincipal.CurrentUserId;
                planogramLookups.Add(contentLookup.PlanogramName, contentLookup);
            }

            // generate the location hierarchy file name and path
            String fileName = Path.Combine(context.SamplesPath, _sampleFileContentLinks);

            // import the file
            ImportFileData importData = ImportFromExcel(fileName);

            // build a mapping of columns indexed by name
            Dictionary<String, ImportFileDataColumn> columnMappings = new Dictionary<String, ImportFileDataColumn>();
            foreach (ImportFileDataColumn column in importData.Columns)
            {
                columnMappings.Add(column.Header, column);
            }

            // enumerate through all records in the file and populate lookups
            foreach (ImportFileDataRow row in importData.Rows)
            {
                // get all details for the row
                String planogramName = columnMappings.ContainsKey("Planogram Name") ? Convert.ToString(row[columnMappings["Planogram Name"].ColumnNumber].Value) : null;
                String productUniverseName = columnMappings.ContainsKey("Product Universe") ? Convert.ToString(row[columnMappings["Product Universe"].ColumnNumber].Value) : null;
                String assortmentName = columnMappings.ContainsKey("Assortment") ? Convert.ToString(row[columnMappings["Assortment"].ColumnNumber].Value) : null;
                String blockingName = columnMappings.ContainsKey("Blocking") ? Convert.ToString(row[columnMappings["Blocking"].ColumnNumber].Value) : null;
                String sequenceName = columnMappings.ContainsKey("Sequence") ? Convert.ToString(row[columnMappings["Sequence"].ColumnNumber].Value) : null;
                String metricProfileName = columnMappings.ContainsKey("Metric Profile") ? Convert.ToString(row[columnMappings["Metric Profile"].ColumnNumber].Value) : null;
                String inventoryProfileName = columnMappings.ContainsKey("Inventory Profile") ? Convert.ToString(row[columnMappings["Inventory Profile"].ColumnNumber].Value) : null;
                String consumerDecisionTreeName = columnMappings.ContainsKey("Consumer Decision Tree") ? Convert.ToString(row[columnMappings["Consumer Decision Tree"].ColumnNumber].Value) : null;
                String minorAssortment = columnMappings.ContainsKey("Minor Assortment") ? Convert.ToString(row[columnMappings["Minor Assortment"].ColumnNumber].Value) : null;
                String performanceSelectionName = columnMappings.ContainsKey("Performance Selection") ? Convert.ToString(row[columnMappings["Performance Selection"].ColumnNumber].Value) : null;
                String clusterSchemeName = columnMappings.ContainsKey("Cluster Scheme") ? Convert.ToString(row[columnMappings["Cluster Scheme"].ColumnNumber].Value) : null;
                String clusterName = columnMappings.ContainsKey("Cluster") ? Convert.ToString(row[columnMappings["Cluster"].ColumnNumber].Value) : null;
                String renumberingStrategyName = columnMappings.ContainsKey("Renumbering Strategy") ? Convert.ToString(row[columnMappings["Renumbering Strategy"].ColumnNumber].Value) : null;
                String validationTemplateName = columnMappings.ContainsKey("Validation Template") ? Convert.ToString(row[columnMappings["Validation Template"].ColumnNumber].Value) : null;

                // populate the lookup details
                if (planogramLookups.ContainsKey(planogramName))
                {
                    // locate the lookup
                    ContentLookup planogramLookup = planogramLookups[planogramName];

                    #region Product Universe

                    ProductUniverseInfo productUniverse = context.ProductUniverses.FirstOrDefault(item => item.Name == productUniverseName);
                    if (productUniverse != null)
                    {
                        planogramLookup.ProductUniverseId = productUniverse.Id;
                        planogramLookup.ProductUniverseName = productUniverse.Name;
                    }

                    #endregion

                    #region Assortment

                    AssortmentInfo assortment = context.Assortments.FirstOrDefault(item => item.Name == assortmentName);
                    if (assortment != null)
                    {
                        planogramLookup.AssortmentId = assortment.Id;
                        planogramLookup.AssortmentName = assortment.Name;
                    }

                    #endregion

                    #region Blocking

                    PlanogramInfo blockingTemplate = context.Planograms.FirstOrDefault(item => item.Name == blockingName);
                    if (blockingTemplate != null)
                    {
                        planogramLookup.BlockingId = blockingTemplate.Id;
                        planogramLookup.BlockingName = blockingTemplate.Name;
                    }

                    #endregion

                    #region Sequence

                    PlanogramInfo sequenceTemplate = context.Planograms.FirstOrDefault(item => item.Name == sequenceName);
                    if (sequenceTemplate != null)
                    {
                        planogramLookup.SequenceId = sequenceTemplate.Id;
                        planogramLookup.SequenceName = sequenceTemplate.Name;
                    }

                    #endregion

                    #region Metric Profile

                    MetricProfile metricProfile = context.MetricProfiles.FirstOrDefault(item => item.Name == metricProfileName);
                    if (metricProfile != null)
                    {
                        planogramLookup.MetricProfileId = metricProfile.Id;
                        planogramLookup.MetricProfileName = metricProfile.Name;
                    }

                    #endregion

                    #region Inventory Profile

                    InventoryProfileInfo inventoryProfile = context.InventoryProfiles.FirstOrDefault(item => item.Name == inventoryProfileName);
                    if (inventoryProfile != null)
                    {
                        planogramLookup.InventoryProfileId = inventoryProfile.Id;
                        planogramLookup.InventoryProfileName = inventoryProfile.Name;
                    }


                    #endregion

                    #region Consumer Decision Tree

                    // no consumer decision trees at this time

                    #endregion

                    #region Minor Assortment

                    // no minor assortments at this time

                    #endregion

                    #region Performance Selection

                    // no performance selections at this time

                    #endregion

                    #region Cluster Scheme & Cluster

                    ClusterSchemeInfo clusterScheme = context.ClusterSchemes.FirstOrDefault(item => item.Name == clusterSchemeName);
                    if (clusterScheme != null)
                    {
                        planogramLookup.ClusterSchemeId = clusterScheme.Id;
                        planogramLookup.ClusterSchemeName = clusterScheme.Name;

                        Cluster cluster = ClusterScheme.FetchById(clusterScheme.Id).Clusters.FirstOrDefault(item => item.Name == clusterName);
                        if (cluster != null)
                        {
                            planogramLookup.ClusterId = cluster.Id;
                            planogramLookup.ClusterName = cluster.Name;
                        }
                    }

                    #endregion

                    #region Renumbering Strategy

                    RenumberingStrategyInfo renumberingStrategy = context.RenumberingStrategies.FirstOrDefault(item => item.Name == renumberingStrategyName);
                    if (renumberingStrategy != null)
                    {
                        planogramLookup.RenumberingStrategyId = renumberingStrategy.Id;
                        planogramLookup.RenumberingStrategyName = renumberingStrategy.Name;
                    }

                    #endregion

                    #region Validation Template

                    ValidationTemplateInfo validationTemplate = context.ValidationTemplates.FirstOrDefault(item => item.Name == validationTemplateName);
                    if (validationTemplate != null)
                    {
                        planogramLookup.ValidationTemplateId = (Int32?)validationTemplate.Id;
                        planogramLookup.ValidationTemplateName = validationTemplate.Name;
                    }

                    #endregion

                    // save the lookup
                    planogramLookup.Save();
                }
            }

            // retrieve the content lookups
            context.ContentLookups = ContentLookupList.FetchByEntityId(context.Entity.Id);
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Imports an excel spreadsheet into a file data structure
        /// </summary>
        private static ImportFileData ImportFromExcel(String fileName)
        {
            // load the workbook
            Workbook workbook = new Workbook(fileName, new LoadOptions(LoadFormat.Auto)
            {
                LoadDataOnly = true,
                LoadDataOptions = new LoadDataOption()
                {
                    SheetIndexes = new Int32[] { 0 },
                    ImportFormula = false
                }
            });

            // get the first worksheet
            Worksheet worksheet = workbook.Worksheets[0];

            // calculate how many rows exist
            DataTable dataTable = null;
            if ((worksheet != null) && (worksheet.Cells != null) && (worksheet.Cells.Count > 0))
            {
                Int32 rows = worksheet.Cells.MaxDataRow + 1;
                Int32 columns = worksheet.Cells.MaxDataColumn +1;
                dataTable = worksheet.Cells.ExportDataTableAsString(0, 0, rows, columns, false);
            }

            // if the data table was not created, then return null
            if (dataTable == null) return null;

            // create the import data
            ImportFileData importData = ImportFileData.NewImportFileData();

            // setup the columns
            for (Int32 columnIndex = 0; columnIndex < dataTable.Columns.Count; columnIndex++)
            {
                ImportFileDataColumn importColumn = ImportFileDataColumn.NewImportFileDataColumn(columnIndex + 1);
                importColumn.Header = dataTable.Rows[0].ItemArray[columnIndex].ToString();
                importColumn.CellAlignment = ImportFileDataHorizontalAlignment.Left;
                importData.Columns.Add(importColumn);
            }

            // setup the rows
            for (Int32 rowIndex = 1; rowIndex < dataTable.Rows.Count; rowIndex++)
            {
                ImportFileDataRow importRow = ImportFileDataRow.NewImportFileDataRow(rowIndex + 1);
                for (Int32 cellIndex = 0; cellIndex < dataTable.Rows[rowIndex].ItemArray.Count(); cellIndex++)
                {
                    ImportFileDataCell cell = ImportFileDataCell.NewImportFileDataCell(cellIndex + 1, dataTable.Rows[rowIndex].ItemArray[cellIndex]);
                    importRow.Cells.Add(cell);
                }
                importData.Rows.Add(importRow);
            }

            // dispose of the data table
            dataTable.Dispose();

            // return the import data
            return importData;
        }

        #endregion
    }
}
