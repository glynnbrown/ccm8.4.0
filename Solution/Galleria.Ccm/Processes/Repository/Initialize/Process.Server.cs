﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using Galleria.Ccm.Model;

namespace Galleria.Ccm.Processes.Repository.Initialize
{
    /// <summary>
    /// A process that performs a repository sync operation
    /// </summary>
    public partial class Process
    {
        #region Methods
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            // get the source repository information
            this.SourceInfo = this.SourceProxy.GetRepositoryInfo(this.RepositorySync.Source);

            // get the destination repository information
            this.DestinationInfo = this.DestinationProxy.GetRepositoryInfo(this.RepositorySync.Destination);

            // register the destination repository within the source
            this.SourceProxy.RegisterRepository(this.DestinationInfo);

            // if the sync operation is two way, then register the destination with the source
            if (this.RepositorySync.Direction == RepositorySyncDirection.TwoWay)
                this.DestinationProxy.RegisterRepository(this.SourceInfo);
        }
        #endregion
    }
}
