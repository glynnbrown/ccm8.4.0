﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services.Proxies;
using Galleria.Framework2.Repository.Services.DataContracts;

namespace Galleria.Ccm.Processes.Repository.Sync
{
    /// <summary>
    /// A process that performs a repository sync operation
    /// </summary>
    public partial class Process
    {
        #region Constants
        private const Int32 _itemsToProcess = 1; // indicates how many items to process in this single pass
        #endregion

        #region Methods
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            // at the point this process is to be executed
            // we have already initialized our source and
            // destination repository, plus registered
            // our respositories with each other

            // the process here is relatively straightforward
            // we simple read the next item that requires syncing
            // from our source repository and publish to our desintation
            // we also do this the other way around if the sync is two way

            // we limit the number of items that we single each time this
            // process is executed in order to ensure that our process
            // host remains responsive to any requests, such as shutting
            // down the windows service

            // set our initial counter
            Int32 itemsProcessed = 0;

            // sync source items to the destination
            while (itemsProcessed < _itemsToProcess)
            {
                // sync the next item from the source to destination
                Boolean itemProcssed = ProcessNextItem(this.SourceProxy, this.SourceInfo, this.DestinationProxy, this.DestinationInfo);

                // if an item was not processed, then exit this loop now
                if (!itemProcssed) break;

                // increase the process counter
                itemsProcessed++;
            }

            // sync destination items to the source
            if (this.RepositorySync.Direction == RepositorySyncDirection.TwoWay)
            {
                while (itemsProcessed < _itemsToProcess)
                {
                    // sync the next item from the destination to the source
                    Boolean itemProcessed = ProcessNextItem(this.DestinationProxy, this.DestinationInfo, this.SourceProxy, this.SourceInfo);
                    
                    // if an item was not processed, then exit this lop now
                    if (!itemProcessed) break;

                    // increase the process counter
                    itemsProcessed++;
                }
            }
        }

        /// <summary>
        /// Performs the process of syncing a single item from
        /// the source repository to the destination repository
        /// </summary>
        /// <param name="sourceProxy">The source repository proxy</param>
        /// <param name="sourceInfo">The source repository info</param>
        /// <param name="destinationProxy">The destination repository proxy</param>
        /// <param name="destinationInfo">The destination repository info</param>
        /// <returns>True if an item was processed</returns>
        private static Boolean ProcessNextItem(RepositoryServiceProxy sourceProxy, RepositoryInfoDc sourceInfo, RepositoryServiceProxy destinationProxy, RepositoryInfoDc destinationInfo)
        {
            // get the next item from the source repository
            RepositoryItemDc item = sourceProxy.GetNextRepositoryItem(destinationInfo.UniqueId);

            // if no item requires processing, then exit now
            if (item == null) return false;

            // based on the repository item type
            // take the appropriate action
            switch (item.ItemType)
            {
                case "Planogram":
                    return ProcesssRepositoryPlanogram(item, sourceProxy, sourceInfo, destinationProxy, destinationInfo);
                default:
                    throw new InvalidOperationException();
            }
        }

        /// <summary>
        /// Performs the process of syncing of a single repository planogram
        /// from the source repository to the destination repository
        /// </summary>
        /// <param name="item">The repository item details</param>
        /// <param name="sourceProxy">The source repository proxy</param>
        /// <param name="sourceInfo">The source repository info</param>
        /// <param name="destinationProxy">The destination repository proxy</param>
        /// <param name="destinationInfo">The destination repository info</param>
        /// <returns></returns>
        private static Boolean ProcesssRepositoryPlanogram(RepositoryItemDc item, RepositoryServiceProxy sourceProxy, RepositoryInfoDc sourceInfo, RepositoryServiceProxy destinationProxy, RepositoryInfoDc destinationInfo)
        {
            // retrieve the full planogram information from the source repository
            RepositoryPlanogramDc planogram = sourceProxy.GetRepositoryPlanogram(item.Id);

            // now publish this item to the destination repository
            destinationProxy.PublishRepositoryPlanogram(planogram);

            // and indicate that the operation was successful
            return true;
        }
        #endregion
    }
}
