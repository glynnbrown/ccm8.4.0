﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using Csla;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services.Proxies;
using Galleria.Framework.Processes;
using Galleria.Framework2.Repository.Services.DataContracts;

namespace Galleria.Ccm.Processes.Repository.Sync
{
    /// <summary>
    /// A process that performs a repository sync operation
    /// </summary>
    public partial class Process : ProcessBase<Process>
    {
        #region Properties

        #region Sync
        /// <summary>
        /// RepositorySync property definition
        /// </summary>
        public static PropertyInfo<RepositorySync> RepositorySyncProperty =
            RegisterProperty<RepositorySync>(c => c.RepositorySync);
        /// <summary>
        /// Returns the repository sync information
        /// </summary>
        public RepositorySync RepositorySync
        {
            get { return this.ReadProperty<RepositorySync>(RepositorySyncProperty); }
        }
        #endregion

        #region SourceProxy
        /// <summary>
        /// SourceProxy property definition
        /// </summary>
        public static readonly PropertyInfo<RepositoryServiceProxy> SourceProxyProperty =
            RegisterProperty<RepositoryServiceProxy>(c => c.SourceProxy);
        /// <summary>
        /// Returns the source proxy
        /// </summary>
        public RepositoryServiceProxy SourceProxy
        {
            get { return this.ReadProperty<RepositoryServiceProxy>(SourceProxyProperty); }
        }
        #endregion

        #region SourceInfo
        /// <summary>
        /// SourceInfo property definition
        /// </summary>
        public static readonly PropertyInfo<RepositoryInfoDc> SourceInfoProperty =
            RegisterProperty<RepositoryInfoDc>(c => c.SourceInfo);
        /// <summary>
        /// Returns or sets the source repository info
        /// </summary>
        public RepositoryInfoDc SourceInfo
        {
            get { return this.ReadProperty<RepositoryInfoDc>(SourceInfoProperty); }
        }
        #endregion

        #region DestinationProxy
        /// <summary>
        /// DesintationProxy property definition
        /// </summary>
        public static readonly PropertyInfo<RepositoryServiceProxy> DestinationProxyProperty =
            RegisterProperty<RepositoryServiceProxy>(c => c.DestinationProxy);
        /// <summary>
        /// Returns the destination repository proxy
        /// </summary>
        public RepositoryServiceProxy DestinationProxy
        {
            get { return this.ReadProperty<RepositoryServiceProxy>(DestinationProxyProperty); }
        }
        #endregion

        #region DestinationInfo
        /// <summary>
        /// DestinationInfo property definition
        /// </summary>
        public static readonly PropertyInfo<RepositoryInfoDc> DestinationInfoProperty =
            RegisterProperty<RepositoryInfoDc>(c => c.DestinationInfo);
        /// <summary>
        /// Returns or sets the Destination repository information
        /// </summary>
        public RepositoryInfoDc DestinationInfo
        {
            get { return this.ReadProperty<RepositoryInfoDc>(DestinationInfoProperty); }
        }
        #endregion

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Process(RepositorySync repositorySync, RepositoryServiceProxy sourceProxy, RepositoryInfoDc sourceInfo, RepositoryServiceProxy destinationProxy, RepositoryInfoDc destinationInfo)
        {
            this.LoadProperty<RepositorySync>(RepositorySyncProperty, repositorySync);
            this.LoadProperty<RepositoryServiceProxy>(SourceProxyProperty, sourceProxy);
            this.LoadProperty<RepositoryInfoDc>(SourceInfoProperty, sourceInfo);
            this.LoadProperty<RepositoryServiceProxy>(DestinationProxyProperty, destinationProxy);
            this.LoadProperty<RepositoryInfoDc>(DestinationInfoProperty, destinationInfo);
        }
        #endregion
    }
}
