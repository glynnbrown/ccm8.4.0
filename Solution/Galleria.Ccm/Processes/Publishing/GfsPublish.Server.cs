﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-27783 : M.Brumby
//      Created.
// V8-29987 : M.Brumby
//  Added flag to publish plan with a new UCR or not.
#endregion

#region Version History: (CCM 8.0)
// V8-29173 : M.Shelley
//  Just tidying the code a little
#endregion

#region Version History: (CCM 8.1.1)
// V8-30463 : L.Ineson
//  Publish user now set as id.
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Planogram;
using Galleria.Ccm.Services.Project;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Processes.Publishing
{
    public partial class GfsPublish
    {

        #region Factory Methods

        public static void ProcessLocally(GfsPublishContext context)
        {
            GfsPublish process = new GfsPublish(context);
            try
            {
                ProcessFactory.Execute<GfsPublish>(process);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        protected override void OnExecute()
        {
            List<ProjectContent> contentList = new List<ProjectContent>();
            try
            {
                WriteInformationToGFS(EventLogEvent.PublishStart, new String[] { "" });

                using (FoundationServiceClient proxy = new FoundationServiceClient(this.Context.Entity.EndpointRoot))//@"http://localhost:60099/GFS215Service"))
                {
                    if (this.Context.PlanAssignmentList != null)
                    {
                        foreach (GFSPublishPlanContext context in this.Context.PlanContexts)
                        {
                            Planogram dc = null;
                            try
                            {
                                dc = PublishPlanToGFS(proxy, this.Context, context);
                            }
                            catch (Exception ex)
                            {
                                WriteErrorToGFS(EventLogEvent.PublishErrorPlan, ex);
                            }

                            try
                            {
                                if (dc != null)
                                {
                                    //Link the plan to each store
                                    contentList.Add(CreateProjectContent(dc.UniqueContentReference, context, DateTime.UtcNow));//item.PublishedItemStores));
                                    //SaveToProject(dc.UniqueContentReference, item.PublishedItemStores, CategoryReview.LinkedProjectName);
                                }
                                else
                                {
                                    //Link the plan to each store
                                    // contentList.Add(CreateProjectContent(planToPublish.UniqueContentReference, publishedItemStoresToCycleThrough, itemStorePlanPublishedDate));// item.PublishedItemStores));
                                    // SaveToProject(planToPublish.UniqueContentReference, item.PublishedItemStores, CategoryReview.LinkedProjectName);
                                }
                            }
                            catch (Exception ex)
                            {
                                WriteErrorToGFS(EventLogEvent.PublishErrorLinkingToProject, ex);
                            }
                        }
                        SaveToProject(proxy, contentList, this.Context.Entity.Name, this.Context.ProjectName);
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorToGFS(EventLogEvent.UnexpectedException, ex);
            }

            WriteInformationToGFS(EventLogEvent.PublishEnd, new String[] { "" });
        }

        /// Publishes the supplied plan to GFS with a number of retries unless the
        /// error is a Planogram FaultException.
        /// </summary>
        /// <param name="item">oject containing details of the plan to be published</param>
        /// <param name="planToPublish">the ccm plan to be published</param>
        /// <param name="retries">the number of times to try publishing</param>
        /// <returns>the data contract created from the ccm plan</returns>
        public static Planogram PublishPlanToGFS(FoundationServiceClient proxy, GfsPublishContext publishContext, GFSPublishPlanContext planContext, Int32 retries = 3)
        {
            return PublishPlanToGFS(proxy, planContext.PlanogramDataContract, publishContext.Entity.Name, ProductGroupInfo.FetchById(planContext.ProductGroupId).Code);
        }

        public static Planogram PublishPlanToGFS(FoundationServiceClient proxy, Planogram planogramDataContract, String entityName, String productGroupCode, Boolean sendAsNewPlan = true, Int32 retries = 3)
        {
            Planogram dc = null;
            try
            {
                //map the plan data to the data contract format
                dc = planogramDataContract;

                //TEMP  
                dc.ProductName = "CCMv8";
                dc.ComputerName = System.Environment.MachineName;
                if(sendAsNewPlan) dc.UniqueContentReference = Guid.NewGuid();
                dc.ProductGroupCode = productGroupCode;
                //dc.Name = String.Format("{0}_{1}", dc.Name, dc.ProductGroupCode);
                //updat the entity name
                dc.EntityName = entityName;

                // WriteInformationToGFS(EventLogEvent.PublishInformation, new String[] { dc.Name, planContext.PlanAssignmentList.GroupBy(p => p.LocationId).Count().ToString(), dc.ProductGroupCode });

                //publish the plan to gfs
                dc = proxy.PlanogramServiceClient.PublishPlanogram(new PublishPlanogramRequest(dc)).Planogram;
            }
            catch (Exception ex)
            {
                if (retries > 0)
                {
                    retries--;
                    dc = PublishPlanToGFS(proxy, planogramDataContract, entityName, productGroupCode, sendAsNewPlan, retries);
                }
                else
                {
                    throw ex;
                }
            }

            return dc;
        }

        /// <summary>
        /// Create a project content object for a planogram that has been published.
        /// </summary>
        public ProjectContent CreateProjectContent(Guid uniqueContentReference, GFSPublishPlanContext context, DateTime itemStorePlanPublishedDate)
        {
            ProjectContent content = new ProjectContent();

            content.UniqueContentReference = uniqueContentReference;
            content.Locations = new List<ProjectContentLocation>();

            foreach (LocationPlanAssignment assignments in context.PlanAssignmentList)
            {
                Location location = Location.FetchById(assignments.LocationId);
                assignments.PublishedByUserId = this.Context.User.Id;
                //assignments.DatePublished = itemStorePlanPublishedDate;
                assignments.SetPublishedDate(itemStorePlanPublishedDate);
                assignments.PublishType = PlanPublishType.PublishGFS;

                content.Locations.Add(new ProjectContentLocation()
                {
                    LocationCode = location.Code,                    
                });
            }

            return content;
        }

        /// <summary>
        /// Publishes a planogram to a selection of locations in a Project. This publish action
        /// will retry a number of times on erroring unless the error is a Project FaultException
        /// </summary>
        /// <param name="contentList">content list to be published</param>
        /// <param name="ProjectName">The name of the project being pubished to.</param>
        /// <param name ="retries">The number of attempts ot publish</param>
        public static void SaveToProject(FoundationServiceClient proxy, List<ProjectContent> contentList, String entityName, String ProjectName, Int32 retries = 3)
        {
            try
            {
                if (contentList.Count > 0)
                {
                    proxy.ProjectServiceClient.AddProjectContentByEntityNameProjectName(new AddProjectContentByEntityNameProjectNameRequest(ProjectName, entityName, contentList));
                }
            }
            catch (Exception ex)
            {
                if (retries > 0)
                {
                    retries--;
                    SaveToProject(proxy, contentList, entityName, ProjectName, retries);
                }
                else
                {
                    throw ex;
                }
            }
        }

        #region WriteErrorToGFS

        /// <summary>
        /// Writes the error to the GFS eventlog
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="syncType">The synchronization type</param>
        private void WriteErrorToGFS(EventLogEvent eventLogEvent, Exception ex)
        {
            GfsLoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Error, "LG:Publish", this.Context.Entity.Id, eventLogEvent, ex.Message);
        }
        #endregion

        #region WriteInformationToGFS

        /// <summary>
        /// Writes the information to the GFS eventlog
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="syncType">The synchronization type</param>
        private void WriteInformationToGFS(EventLogEvent eventLogEvent, String[] args)
        {
            GfsLoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Information, "LG:Publish", this.Context.Entity.Id, eventLogEvent, args);
        }
        #endregion

    }
}
