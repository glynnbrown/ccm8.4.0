﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//  V8-27783 : M.Brumby
//      Created.
#endregion
#endregion

using System.Collections.Generic;
using Galleria.Ccm.Model;
using System;
using System.Linq;
using Galleria.Ccm.Services.Planogram;
using Galleria.Ccm.Helpers;

namespace Galleria.Ccm.Processes.Publishing
{

    public class GfsPublishContext
    {
        private IEnumerable<LocationPlanAssignment> _planAssignmentList;
        private List<GFSPublishPlanContext> _planContexts = new List<GFSPublishPlanContext>();
        private GFSModel.Entity _entity;
        private Int32 _singlePlanogramId = 0;
        private String _projectName = String.Empty;
        private User _user;
        
        public IEnumerable<LocationPlanAssignment> PlanAssignmentList
        {
            get
            {
                return _planAssignmentList;
            }
            set
            {
                _planAssignmentList = value;
                GeneratePlanContexts();
            }
        }

        public GFSModel.Entity Entity
        {
            get
            {
                return _entity;
            }
            set
            {
                _entity = value;
            }
        }
        public Int32 SinglePlanogramId
        {
            get
            {
                return _singlePlanogramId;
            }
            set
            {
                _singlePlanogramId = value;
            }
        }
        public String ProjectName
        {
            get
            {
                return _projectName;
            }
            set
            {
                _projectName = value;
            }
        }
        public List<GFSPublishPlanContext> PlanContexts
        {
            get
            {
                return _planContexts;
            }
        }
        public User User
        {
            get
            {
                return _user;
            }
        }

        private GfsPublishContext() { }
        public GfsPublishContext(GFSModel.Entity entity, User user)
        {
            _entity = entity;
            _user = user;
        }

        private void GeneratePlanContexts()
        {
            _planContexts.Clear();
            GFSPublishPlanContext currentContext = null;
            foreach (LocationPlanAssignment assignment in _planAssignmentList.OrderBy(a => a.ProductGroupId).ThenBy(a => a.PlanogramId))
            {
                if (currentContext == null)
                {
                    currentContext = new GFSPublishPlanContext(assignment);
                    _planContexts.Add(currentContext);
                }
                else if (assignment.PlanogramId != currentContext.PlanogramId || assignment.ProductGroupId != currentContext.ProductGroupId)
                {
                    currentContext = new GFSPublishPlanContext(assignment);
                    _planContexts.Add(currentContext);
                }
                else
                {
                    currentContext.PlanAssignmentList.Add(assignment);
                }
            }
        }
    }

    /// <summary>
    /// Grouping of LocationPlanAAssignments with PlanogramId and productGroupId
    /// as the key.
    /// </summary>
    public class GFSPublishPlanContext
    {        
        private Int32 _productGroupId = 0;
        private Int32 _planogramId = 0;
        private List<LocationPlanAssignment> _planAssignmentList = new List<LocationPlanAssignment>();
        private Planogram _planogramDataContract = null;

        public Int32 ProductGroupId
        {
            get
            {
                return _productGroupId;
            }
        }
        public Int32 PlanogramId
        {
            get
            {
                return _planogramId;
            }
        }
        public List<LocationPlanAssignment> PlanAssignmentList
        {
            get
            {
                return _planAssignmentList;
            }
        }
        public Planogram PlanogramDataContract
        {
            get
            {
                if (_planogramDataContract == null)
                {
                    _planogramDataContract = CcmToGfsPlanogramConverter.ConvertToPlanogramDc(PlanogramId);
                }
                return _planogramDataContract;
            }
        }

        public GFSPublishPlanContext(LocationPlanAssignment assignment)
        {
            _productGroupId = assignment.ProductGroupId;
            _planogramId = assignment.PlanogramId;
            _planAssignmentList.Add(assignment);
        }
    }
}
