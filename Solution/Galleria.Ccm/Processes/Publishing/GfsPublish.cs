﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//  V8-27783 : M.Brumby
//      Created.
#endregion
#endregion

using System;
using Galleria.Framework.Processes;
using Csla;
using Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Model;
using System.Collections.Generic;
namespace Galleria.Ccm.Processes.Publishing
{
    [Serializable]
    public partial class GfsPublish : ProcessBase<GfsPublish>
    {
        #region Properties
        /// <summary>
        /// The publishing context property
        /// </summary>
        private static readonly PropertyInfo<GfsPublishContext> ContextProperty =
            RegisterProperty<GfsPublishContext>(c => c.Context);
        public GfsPublishContext Context
        {
            get { return ReadProperty<GfsPublishContext>(ContextProperty); }
            set { LoadProperty<GfsPublishContext>(ContextProperty, value); }
        }
        #endregion

        #region Constructor
        public GfsPublish(GfsPublishContext context)
        {
            this.LoadProperty<GfsPublishContext>(ContextProperty, context);
        }
        #endregion
    }

}
