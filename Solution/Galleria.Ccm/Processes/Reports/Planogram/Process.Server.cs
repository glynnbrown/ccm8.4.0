﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created
// V8-31105 : L.Ineson
//  Added PlanogramIsProportional property
#endregion
#region Version History: (CCM 830)
// V8-31379 : L.Ineson
//  Now adds textbox if datasheet could not be loaded.
// V8-31768 : L.Ineson
//  Changed how datasheet rows are created to add support for aggregation.
// V8-32636 : A.Probyn
//  Implemented Annotations
// CCM-13566 : M.Brumby
//  Ensured correct order of print groups and sections
// CCM-18499 : L.Ineson
//  Corrected an issue where datasheet components contain all items from the plan even though display was for a specific bay.
// CCM-18579 : L.Ineson
//  Made sure that print template fetches as readonly.
#endregion
#endregion

using Aspose.Pdf.Generator;
using Csla;
using Galleria.Ccm.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Rendering;
using Galleria.Framework.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CcmPlanogram = Galleria.Framework.Planograms.Model.Planogram;
using CultureInfo = System.Globalization.CultureInfo;

namespace Galleria.Ccm.Processes.Reports.Planogram
{
    public partial class Process
    {
        private const Boolean AsReadOnly = true;

        #region Nested Classes
        private class PlanogramComponentImageSettings : IPlanogramImageSettings
        {
            #region Properties

            public PlanogramImageViewType ViewType { get; set; }

            public Boolean Risers { get; set; }
            public Boolean ChestWalls { get; set; }
            public Boolean FixtureImages { get; set; }
            public Boolean FixtureFillPatterns { get; set; }
            public Boolean Notches { get; set; }
            public Boolean PegHoles { get; set; }
            public Boolean DividerLines { get; set; }
            public Boolean Positions { get; set; }
            public Boolean PositionUnits { get; set; }
            public Boolean RotateTopDownComponents { get; set; }
            public Boolean ProductImages { get; set; }
            public Boolean ProductShapes { get; set; }
            public Boolean ProductFillColours { get; set; }
            public Boolean ProductFillPatterns { get; set; }
            public Boolean Pegs { get; set; }
            public Boolean Dividers { get; set; }
            public Boolean TextBoxes { get; set; }
            public Boolean Annotations { get; set; }

            public IPlanogramLabel ProductLabel { get; set; }
            public IPlanogramLabel FixtureLabel { get; set; }
            public PlanogramPositionHighlightColours PositionHighlights { get; set; }
            public PlanogramPositionLabelTexts PositionLabelText { get; set; }
            public PlanogramFixtureLabelTexts FixtureLabelText { get; set; }

            public VectorValue? CameraLookDirection { get; set; }
            public PointValue? CameraPosition { get; set; }

            #endregion

            #region Constructor

            public PlanogramComponentImageSettings() { }

            #endregion



        }

        private class PlanogramItem
        {
            public CcmPlanogram Planogram { get; set; }
            public PlanogramFixtureItem FixtureItem { get; set; }
            public PlanogramFixture Fixture { get; set; }
            public PlanogramFixtureAssembly FixtureAssembly { get; set; }
            public PlanogramAssembly Assembly { get; set; }
            public PlanogramAssemblyComponent AssemblyComponent { get; set; }
            public PlanogramFixtureComponent FixtureComponent { get; set; }
            public PlanogramComponent Component { get; set; }
            public PlanogramPosition Position { get; set; }
            public PlanogramProduct Product { get; set; }
        }
        #endregion

        #region Execute

        /// <summary>
        /// Called when executing this report
        /// </summary>
        /// <exception cref="ArgumentNullException">The method was unable to obtain a valid plan or printing template.</exception>
        protected override void OnExecuteReport()
        {
            Pdf pdfDoc = Report;
            //list to hold references to text components so we can resolve page numbers later.
            var textComponents = new List<Text>();

            CcmPlanogram plan = GetPlanogram();
            if (ProcessShouldAbort()) return;
            PrintTemplate printTemplate = GetPrintTemplate();
            if (ProcessShouldAbort()) return;
            ProcessContext context = GetProcessContext(printTemplate, plan);
            context.PreloadExistingPlanogramImages();

            List<PlanogramFixtureItem> allBays = context.PlanogramFixtureItems();

            //[CCM-13567] If any of the section groups are displaying bays per page then split components now.
            if (!context.PrintingTemplate.SectionGroups.All(sg => sg.BaysPerPage == 0 || sg.BaysPerPage == allBays.Count))
            {
                try
                {
                    SplitComponentsByBay(allBays);
                }
                catch (Exception ex)
                {
                    //if this fails for some reason then just continue regardless.
                    Gibraltar.Agent.Log.RecordException(ex, null, /*canContinue*/true);
                }
            }
            
            //cycle through creating each sectiongroup
            foreach (PrintTemplateSectionGroup printSectionGroup in context.PrintingTemplate.SectionGroups.OrderBy(s => s.Number))
            {
                if (this.ProcessShouldAbort()) return;

                //cycle through bay groupings based on the bays per page
                List<PlanogramFixtureItem> remainingBays = allBays.ToList();
                Int32 baysPerPage = (printSectionGroup.BaysPerPage != 0) ? printSectionGroup.BaysPerPage : remainingBays.Count;

                while (remainingBays.Count > 0)
                {
                    if (this.ProcessShouldAbort()) return;

                    Int32 groupCount = (baysPerPage <= remainingBays.Count) ? baysPerPage : remainingBays.Count;
                    List<PlanogramFixtureItem> currentBays = remainingBays.Take(groupCount).ToList();

                    //cycle through sections in order
                    PrintTemplateSection[] groupSections = printSectionGroup.Sections.OrderBy(p => p.Number).ToArray();
                    foreach (PrintTemplateSection printSection in groupSections.OrderBy(s => s.Number))
                    {
                        Section firstSection = new Section(pdfDoc);
                        pdfDoc.Sections.Add(firstSection);
                        Int32 startSectionIdx = pdfDoc.Sections.Count - 1;


                        //set the page size and orientation
                        ReportHelper.SetSectionPageSize(firstSection, context.PrintingTemplate.PaperSize,
                            (printSection.Orientation == PrintTemplateOrientationType.Landscape));


                        //non-repeatable components first.
                        //These are components that don't get repeated on every page but may span accross pages.
                        foreach (PrintTemplateComponent printComponent in printSection.Components)
                        {
                            switch (printComponent.Type)
                            {
                                case PrintTemplateComponentType.DataSheet:
                                    AddDataSheetComponent(context, pdfDoc, startSectionIdx, printComponent, currentBays);
                                    break;
                            }
                        }


                        //for all the created pdf sections
                        //cycle through repeatable components
                        foreach (PrintTemplateComponent printComponent in printSection.Components)
                        {
                            switch (printComponent.Type)
                            {
                                case PrintTemplateComponentType.Planogram:
                                    {
                                        if (currentBays.Count == 0) continue;

                                        Boolean b = baysPerPage > 0;
                                        Int32 curRangeStart = b ? allBays.IndexOf(currentBays.First()) + 1 : 0;
                                        Int32 curRangeEnd = b ? allBays.IndexOf(currentBays.Last()) + 1 : 0;

                                        Aspose.Pdf.Generator.Image componentImage =
                                            GetPlanogramComponentImage(context, printComponent, curRangeStart, curRangeEnd, this.IsSoftwareRender, this.PlanogramModelDataList);

                                        if (componentImage != null)
                                        {
                                            for (Int32 i = startSectionIdx; i < pdfDoc.Sections.Count; i++)
                                            {
                                                AddPlanogramComponent(pdfDoc.Sections[i], printComponent, componentImage);
                                            }
                                        }
                                    }
                                    break;

                                case PrintTemplateComponentType.TextBox:
                                    String resolvedText = context.ResolveTextBoxText(printComponent.ComponentDetails.TextBoxText);
                                    for (Int32 i = startSectionIdx; i < pdfDoc.Sections.Count; i++)
                                    {
                                        textComponents.Add(ReportHelper.AddTextBoxComponent(pdfDoc.Sections[i], printComponent, resolvedText));
                                    }
                                    break;

                                case PrintTemplateComponentType.Line:
                                    for (Int32 i = startSectionIdx; i < pdfDoc.Sections.Count; i++)
                                    {
                                        ReportHelper.AddLineComponent(pdfDoc.Sections[i], printComponent);
                                    }
                                    break;
                            }
                        }


                        //remove the processed bays.
                        currentBays.ForEach(b => remainingBays.Remove(b));
                    }
                }
            }

            pdfDoc.SetUnicode();

            //resolve total pages
            ReportHelper.ResolvePageNumbering(pdfDoc, textComponents);
        }

        private ProcessContext GetProcessContext(PrintTemplate printTemplate, CcmPlanogram plan)
        {
            var context = new ProcessContext(printTemplate, plan);
            if (IsOverridingPlanogramLabels)
                context.SetLabelOverrides(PlanogramProductLabelOverride, PlanogramFixtureLabelOverride);
            if (IsOverridingPlanogramHighlights)
                context.SetHiglightOverride(PlanogramHighlightOverride);
            return context;
        }

        /// <summary>
        /// Get the print template from either the <see cref="PrintingTemplate"/> or the <see cref="PrintTemplateId"/>.
        /// </summary>
        /// <exception cref="ArgumentNullException">The method was unable to obtain a valid printing template.</exception>
        /// <returns></returns>
        private PrintTemplate GetPrintTemplate()
        {
            PrintTemplate printTemplate = PrintingTemplate;
            if (printTemplate == null && PrintTemplateId != null)
            {
                var printTemplateId = PrintTemplateId as Int32?;
                if (printTemplateId.HasValue)
                {
                    printTemplate = PrintTemplate.FetchById(printTemplateId.Value);
                }
                else if (PrintTemplateId is String)
                {
                    printTemplate = PrintTemplate.FetchByFilename((String) PrintTemplateId, AsReadOnly);
                }
            }

            if (printTemplate == null)
                throw new ArgumentNullException(nameof(printTemplate), @"No printing template set.");

            return printTemplate;
        }

        /// <summary>
        /// Get the planogram from either the <see cref="PlanogramContext"/> or the <see cref="PlanogramId"/>.
        /// </summary>
        /// <exception cref="ArgumentNullException">The method was unable to obtain a valid plan.</exception>
        /// <returns></returns>
        private CcmPlanogram GetPlanogram()
        {
            CcmPlanogram plan = PlanogramContext?.Clone(false);

            if (plan == null && PlanogramId != null)
            {
                Package planogramPackage = null;

                var planRepositoryId = PlanogramId as Int32?;
                if (planRepositoryId.HasValue)
                {
                    planogramPackage = Package.FetchById(planRepositoryId.Value);
                }
                else if (PlanogramId is String)
                {
                    planogramPackage = Package.FetchByFileName((String) PlanogramId);
                }

                plan = planogramPackage?.Planograms.FirstOrDefault();
            }

            if (plan == null)
                throw new ArgumentNullException(nameof(plan), @"No planogram set.");

            return plan;
        }

        #endregion

        #region Planogram Component

        /// <summary>
        /// Gets the image data for the given component
        /// </summary>
        /// <param name="context"></param>
        /// <param name="bayContexts"></param>
        /// <returns></returns>
        private static Aspose.Pdf.Generator.Image GetPlanogramComponentImage(
            ProcessContext context,
            PrintTemplateComponent component,
            Int32 bayRangeStart, Int32 bayRangeEnd,
            Boolean isSoftwareRender,
            List<Plan3DData> planogramModelDataList)
        {
            Byte[] imgData = null;

            PrintTemplateComponentDetail componentDetail = component.ComponentDetails;
            FloatingBox positionalBox = ReportHelper.GetFloatingBox(component);

            //Get the render
            Int32 imgHeight = (Int32)positionalBox.BoxHeight;
            Int32 imgWidth = (Int32)positionalBox.BoxWidth;


            if (PlanogramImagesHelper.IsPlanogramImageRendererRegistered())
            {
                PlanogramComponentImageSettings settings = CreatePlanogramImageSettings(context, componentDetail);

                //Create thumbnail using process synchronously
                Framework.Planograms.Processes.RenderPlanogramImage.Process planogramRenderProcess =
                    new Framework.Planograms.Processes.RenderPlanogramImage.Process(context.Planogram, isSoftwareRender, settings);

                planogramRenderProcess.ImageHeight = imgHeight;
                planogramRenderProcess.ImageWidth = imgWidth;
                planogramRenderProcess.BayRangeStart = bayRangeStart;
                planogramRenderProcess.BayRangeEnd = bayRangeEnd;
                planogramRenderProcess.IsProportional = componentDetail.PlanogramIsProportional;
                planogramRenderProcess.PlanogramModelDataList = planogramModelDataList;
                planogramRenderProcess.Execute();

                imgData = planogramRenderProcess.ImageData.ToArray();
            }

            //draw the pdf image
            Aspose.Pdf.Generator.Image pdfImage = new Aspose.Pdf.Generator.Image();
            pdfImage.ImageInfo.ImageFileType = Aspose.Pdf.Generator.ImageFileType.Jpeg;
            if (imgData != null)
            {
                pdfImage.ImageInfo.ImageStream = new MemoryStream(imgData);
            }

            //force the img width and height otherwise it bugs and draws too small.
            pdfImage.ImageInfo.FixWidth = imgWidth;
            pdfImage.ImageInfo.FixHeight = imgHeight;
            pdfImage.ImageInfo.Alignment = AlignmentType.Center;


            // return the image
            return pdfImage;
        }

        /// <summary>
        /// Creates the planogram image settings based on the given print detail
        /// </summary>
        /// <param name="context">the print context</param>
        /// <param name="printDetail">the component detail to create the settings for.</param>
        /// <returns>PlanogramComponentImageSettings to apply to the render.</returns>
        private static PlanogramComponentImageSettings CreatePlanogramImageSettings(ProcessContext context, PrintTemplateComponentDetail printDetail)
        {
            PlanogramComponentImageSettings settings = new PlanogramComponentImageSettings();

            settings.ViewType = EnumHelper.Parse<PlanogramImageViewType>(printDetail.PlanogramViewType.ToString(), PlanogramImageViewType.Design);

            settings.Risers = printDetail.PlanogramShelfRisers;
            settings.ChestWalls = printDetail.PlanogramChestWalls;
            settings.FixtureImages = printDetail.PlanogramFixtureImages;
            settings.FixtureFillPatterns = printDetail.PlanogramFixtureFillPatterns;
            settings.Notches = printDetail.PlanogramNotches;
            settings.PegHoles = printDetail.PlanogramPegHoles;
            settings.DividerLines = printDetail.PlanogramDividerLines;
            settings.Positions = printDetail.PlanogramPositions;
            settings.PositionUnits = printDetail.PlanogramPositionUnits;
            settings.RotateTopDownComponents = printDetail.PlanogramRotateTopDownComponents;
            settings.ProductImages = printDetail.PlanogramProductImages;
            settings.ProductShapes = printDetail.PlanogramProductShapes;
            settings.ProductFillColours = printDetail.PlanogramProductFillColours;
            settings.ProductFillPatterns = printDetail.PlanogramProductFillPatterns;
            settings.Pegs = printDetail.PlanogramPegs;
            settings.Dividers = printDetail.PlanogramDividers;
            settings.TextBoxes = printDetail.PlanogramTextBoxes;
            settings.Annotations = printDetail.PlanogramAnnotations;

            if (printDetail.PlanogramViewType == PrintTemplatePlanogramViewType.Perspective)
            {
                settings.CameraLookDirection = ParseVectorValue(printDetail.PlanogramCameraLookDirection);
                settings.CameraPosition = ParsePointValue(printDetail.PlanogramCameraPosition);
            }


            //Product Label
            settings.ProductLabel = context.GetProductLabelById(printDetail.PlanogramProductLabelId);
            settings.PositionLabelText = context.GetPositionLabelTextsByLabelId(printDetail.PlanogramProductLabelId);

            //Fixture Label
            settings.FixtureLabel = context.GetFixtureLabelById(printDetail.PlanogramFixtureLabelId);
            settings.FixtureLabelText = context.GetFixtureLabelTextsByLabelId(printDetail.PlanogramFixtureLabelId);


            //Highlight
            settings.PositionHighlights = context.GetHighlightColoursByHighlightId(printDetail.PlanogramHighlightId);


            return settings;
        }

        /// <summary>
        /// Adds the planogram component to the given section
        /// </summary>
        /// <param name="pdfSection"></param>
        /// <param name="component"></param>
        /// <param name="pdfImage"></param>
        private static void AddPlanogramComponent(Section pdfSection, PrintTemplateComponent component, Aspose.Pdf.Generator.Image pdfImage)
        {
            //create the position box
            FloatingBox positionalBox = ReportHelper.GetFloatingBox(component);
            pdfSection.Paragraphs.Add(positionalBox);


            Int32 imgHeight = (Int32)pdfImage.ImageInfo.FixHeight;

            positionalBox.Paragraphs.Add(pdfImage);

            //pdfImage.ImageInfo.VerticalAlignment does not exist at present so instead
            // resize the the positional box and move its ypos.
            Double yChange = (positionalBox.BoxHeight - imgHeight) / 2.0;
            positionalBox.BoxHeight = imgHeight;
            positionalBox.Top += (Single)yChange;
        }

        /// <summary>
        /// Parses the given string to a VectorValue
        /// </summary>
        private static VectorValue? ParseVectorValue(String value)
        {
            if (String.IsNullOrWhiteSpace(value)) return null;

            String[] parts = value.Split(',');
            if (parts.Count() != 3) return null;

            Single val1;
            if (!Single.TryParse(parts[0], out val1)) return null;

            Single val2;
            if (!Single.TryParse(parts[1], out val2)) return null;

            Single val3;
            if (!Single.TryParse(parts[2], out val3)) return null;

            return new VectorValue(val1, val2, val3);
        }

        /// <summary>
        /// Parses the given string to a PointValue
        /// </summary>
        private static PointValue? ParsePointValue(String value)
        {
            if (String.IsNullOrWhiteSpace(value)) return null;

            String[] parts = value.Split(',');
            if (parts.Count() != 3) return null;

            Single val1;
            if (!Single.TryParse(parts[0], out val1)) return null;

            Single val2;
            if (!Single.TryParse(parts[1], out val2)) return null;

            Single val3;
            if (!Single.TryParse(parts[2], out val3)) return null;

            return new PointValue(val1, val2, val3);
        }


        private static void SplitComponentsByBay(List<PlanogramFixtureItem> orderedBays)
        {
            //dont bother if we only have one bay!
            if (orderedBays.Count <= 1) return;


            //start by checking for components which overhang bays
            // but not for the last one.
            for (Int32 i = 0; i < orderedBays.Count-1; i++)
            {

                PlanogramFixtureItem bay = orderedBays[i];

                PlanogramFixture fixture = bay.GetPlanogramFixture();
                if (fixture == null) continue;

                RectValue fixtureLocalBounds = new RectValue(0, 0, 0, fixture.Width, fixture.Height, fixture.Depth);
                var maxX = fixtureLocalBounds.X + fixtureLocalBounds.Width;
                foreach (PlanogramFixtureComponent fc in fixture.Components.ToArray())
                {
                    //check if the component overhangs the bay to the right
                    PlanogramComponent component = fc.GetPlanogramComponent();

                    RectValue componentBoundsInFixture =
                    MatrixValue.CreateTransformationMatrix(fc.X, fc.Y, fc.Z, fc.Angle, fc.Slope, fc.Roll)
                    .TransformBounds(new RectValue(0, 0, 0, component.Width, component.Height, component.Depth));

                    //if the component is fully outside of this bay then just move it along.
                    if(componentBoundsInFixture.X.GreaterOrEqualThan(maxX))
                    {
                        orderedBays[i + 1].MoveComponent(fc, bay);
                    }

                    //or if the component overhangs the fixture to the right then split it.
                    // but for now, only if we have no angle or roll applied.
                    else if((componentBoundsInFixture.X + componentBoundsInFixture.Width).GreaterThan(maxX)
                        && fc.Angle.EqualTo(0) && fc.Roll.EqualTo(0))
                    {
                        SplitComponent(fc, component, componentBoundsInFixture, orderedBays[i+1], fixtureLocalBounds.Width);
                    }

                    //should we also split up positions that overhang into the next bay??
                }

                //now check assemblies
                foreach(PlanogramFixtureAssembly fa in fixture.Assemblies.ToArray())
                {
                    PlanogramAssembly assembly = fa.GetPlanogramAssembly();
                    if (assembly == null) continue;

                    RectValue assemblyBoundsInFixture =
                     MatrixValue.CreateTransformationMatrix(fa.X, fa.Y, fa.Z, fa.Angle, fa.Slope, fa.Roll)
                    .TransformBounds(new RectValue(0, 0, 0, assembly.Width, assembly.Height, assembly.Depth));

                    //if the assembly is mainly outside of this bay then just move it along.
                    if ((assemblyBoundsInFixture.X +(assembly.Width/2F)).GreaterOrEqualThan(maxX))
                    {
                        orderedBays[i + 1].MoveAssembly(fa, bay);
                    }
                    //nb - not splitting assemblies atm.
                }
            }
        }

        /// <summary>
        /// Splits a component and its children accross 2 bays.
        /// </summary>
        /// <param name="fixtureComponent">the fixture component to be split</param>
        /// <param name="component">the component to be split</param>
        /// <param name="newTargetBay">the bay on which the new component should reside</param>
        /// <param name="origComponentBoundsInFixture">the bounds of the original component.</param>
        /// <param name="newComponentX">The x point at which we are to split the component, relative to the parent fixture.</param>
        private static void SplitComponent(
            PlanogramFixtureComponent origFC, PlanogramComponent origComponent,
            RectValue origComponentBoundsInFixture,
             PlanogramFixtureItem newTargetBay, Single newComponentX)
        {

            //Create the new component and fixture component
            PlanogramComponent newComponent = origComponent.Copy();
            origComponent.Parent.Components.Add(newComponent);
            Debug.Assert(newComponent.SubComponents.Count == origComponent.SubComponents.Count,
                "Working on the understanding that this is a deep copy.");

            PlanogramFixtureComponent newFc = origFC.Copy();
            newFc.PlanogramComponentId = newComponent.Id;
            newTargetBay.GetPlanogramFixture().Components.Add(newFc);

            //update placements and widths
            newFc.X = 0;
            newComponent.Width = (origComponentBoundsInFixture.X + origComponentBoundsInFixture.Width) - newComponentX;
            origComponent.Width = newComponentX - origComponentBoundsInFixture.X;


            //split subcomponents:
            List<PlanogramSubComponent> origSubs = origComponent.SubComponents.ToList();
            Single subComponentSplitX = newComponentX - origFC.X;
            for(Int32 i=0; i < origComponent.SubComponents.Count; i++)
            {
                PlanogramSubComponent origSub = origSubs[i];
                PlanogramSubComponent newSub = newComponent.SubComponents[i];

                RectValue subBoundsInComponent = 
                   MatrixValue.CreateTransformationMatrix(origSub.X, origSub.Y, origSub.Z, origSub.Angle, origSub.Slope, origSub.Roll)
                   .TransformBounds(new RectValue(0, 0, 0, origSub.Width, origSub.Height, origSub.Depth));

                //if the sub sits within the split orig component range then ignore
                if ((subBoundsInComponent.X + subBoundsInComponent.Width).LessOrEqualThan(subComponentSplitX)) continue;

                //move all positions accross which are on the right side of the split.
                var origSubPlacement = origSub.GetPlanogramSubComponentPlacement();
                var newSubPlacement = newSub.GetPlanogramSubComponentPlacement();

                foreach (var pos in origSubPlacement.GetPlanogramPositions().ToArray())
                {
                    if ((pos.X + (pos.GetPositionDetails(origSubPlacement).TotalSize.Width / 2F)).GreaterOrEqualThan(subComponentSplitX))
                    {
                        newSubPlacement.AssociatePosition(pos);
                        pos.X -= subComponentSplitX;
                    }
                }

                //do the same for annotations
                foreach(var anno in origSubPlacement.GetPlanogramAnnotations())
                {
                    if (anno.X.HasValue && anno.X.Value.GreaterOrEqualThan(subComponentSplitX))
                    {
                        newSubPlacement.AssociateAnnotation(anno);
                        anno.X -= subComponentSplitX;
                    }
                }


                //now update the placement and widths of both subs
                if (origSub.X.GreaterThan(subComponentSplitX))
                {
                    //the sub sits outside of the original range so just remove it.
                    origComponent.SubComponents.Remove(origSub);
                }

                //otherwise, we need to split it.
                else
                {
                    newSub.X = 0;
                    newSub.Width = (origSub.X + origSub.Width) - subComponentSplitX;
                    origSub.Width = subComponentSplitX - origSub.X;
                }
            }
            

            //if the component was a chest then drop the walls.
            if (origComponent.ComponentType == PlanogramComponentType.Chest)
            {
                origComponent.SubComponents[0].FaceThicknessRight = 0;
                newComponent.SubComponents[0].FaceThicknessLeft = 0;
            }
        }


        #endregion

        #region DataSheet Component

        /// <summary>
        /// Adds a datasheet component to the pdf.
        /// </summary>
        private static void AddDataSheetComponent(ProcessContext context, Pdf pdfDoc, Int32 startSectIdx,
            PrintTemplateComponent printComponent, List<PlanogramFixtureItem> bayContexts)
        {
            CcmPlanogram plan = context.Planogram;
            PrintTemplateComponentDetail componentDetail = printComponent.ComponentDetails;

            String dataSheetFileName = componentDetail.DataSheetId as String;
            if (dataSheetFileName == null) return;


            //fetch the datasheet - only exists as files atm.
            CustomColumnLayout layout = FetchDataSheet(dataSheetFileName, context);
            if (layout == null)
            {
                //If the datasheet could not be loaded then add a textbox message instead.
                String message =
                    String.Format(System.Globalization.CultureInfo.CurrentCulture,
                    Message.CustomColumnLayout_PlanogramReportFileNotFound, dataSheetFileName);

                Section pdfSection = pdfDoc.Sections[startSectIdx];
                FloatingBox positionalBox = ReportHelper.GetFloatingBox(printComponent);
                pdfSection.Paragraphs.Add(positionalBox);

                Aspose.Pdf.Generator.Text txt = new Text(message);
                txt.TextInfo.Alignment = AlignmentType.Center;
                positionalBox.Paragraphs.Add(txt);

                return;
            }

            String[] propertyDescs = layout.Columns.Select(c => c.Path).ToArray();


            List<ObjectFieldInfo> colFieldInfos = new List<ObjectFieldInfo>();
            foreach (CustomColumn col in layout.Columns)
            {
                //working on the assumption that there is one field per column.
                ObjectFieldInfo colField = ObjectFieldInfo.ExtractFieldsFromText(col.Path, context.MasterPlanogramFieldList).FirstOrDefault();
                colFieldInfos.Add(colField);
            }



            //create the datatable
            ReportTable table = new ReportTable();
            table.AddColumns(layout.Columns, colFieldInfos);

            //determine what data level the rows should be created as 
            List<ObjectFieldInfo> dataLevelCheckFields =
                 layout.Columns.Where(c => !ObjectFieldExpression.IsAggregatedOutput(c.Path)).SelectMany(c =>
                     ObjectFieldInfo.ExtractFieldsFromText(c.Path, PlanogramFieldHelper.EnumerateAllFields())).ToList();
            PlanogramItemType dataLevel = PlanogramFieldHelper.GetFieldDataLevel(dataLevelCheckFields);

            Int32 cellCount = layout.Columns.Count;

            List<PlanogramItemPlacement> planItems;
            if (dataLevel == PlanogramItemType.Planogram)
            {
                planItems = new List<PlanogramItemPlacement>(1) { PlanogramItemPlacement.NewPlanogramItemPlacement(plan) };
            }
            else
            {
                //cycle through the bay contexts adding items from each
                planItems = new List<PlanogramItemPlacement>();
                foreach (PlanogramFixtureItem fc in bayContexts)
                {
                    PlanogramFixture fixture = fc.GetPlanogramFixture();
                    if (fixture == null) continue;

                    PlanogramItemPlacement bayPlacement =
                        PlanogramItemPlacement.NewPlanogramItemPlacement(fixtureItem: fc, fixture: fixture, planogram: plan);

                    planItems.AddRange(PlanogramItemPlacement.GetItemsOfType(bayPlacement, dataLevel));
                }
            }



            //Create Rows:
            if (layout.IsGroupDetailHidden && layout.GroupList.Any())
            {
                //Rows should be aggregated:

                //Group plan items into rows.
                IEnumerable<IGrouping<Object, PlanogramItemPlacement>> itemGroups;
                if (layout.IsGroupDetailHidden && layout.GroupList.Any())
                {
                    //use the groupings to find the key value.
                    itemGroups = planItems.GroupBy(i => GetKeyValue(i, layout.GroupList));
                }
                else
                {
                    //use the item type key
                    itemGroups = planItems.GroupBy(i => PlanogramFieldHelper.GetKeyValue(i, dataLevel));
                }

                //Create rows.
                foreach (var itemGroup in itemGroups)
                {
                    Object[] values = new Object[cellCount];

                    //cycle through columns setting cells
                    Int32 idx = 0;
                    foreach (CustomColumn col in layout.Columns)
                    {
                        values[idx] = new DataSheetCell(itemGroup.ToList(), col).Value;
                        idx++;
                    }
                    table.Rows.Add(new ReportTableRow(values));
                }
            }
            else
            {
                //Row per item:
                foreach (PlanogramItemPlacement item in planItems)
                {
                    Object[] values = new Object[cellCount];

                    //cycle through columns setting cells
                    Int32 idx = 0;
                    foreach (CustomColumn col in layout.Columns)
                    {
                        values[idx] = new DataSheetCell(item, col).Value;
                        idx++;
                    }
                    table.Rows.Add(new ReportTableRow(values));
                }
            }


            //apply filters
            table.ApplyFilters(layout.FilterList);

            //apply sorts
            table.ApplySorts(layout.SortList);

            //apply groupings
            table.ApplyGroupingsAndTotals(layout);



            //set the table styling
            table.Style = componentDetail.DataSheetStyle;

            //header text info
            Aspose.Pdf.Generator.TextInfo headerTxtInfo = new TextInfo();
            headerTxtInfo.FontName = componentDetail.DataSheetHeaderFontName.ToString();
            headerTxtInfo.FontSize = componentDetail.DataSheetHeaderFontSize;
            headerTxtInfo.IsUnderline = componentDetail.DataSheetHeaderIsFontUnderlined;
            headerTxtInfo.Alignment = AlignmentType.Left;
            headerTxtInfo.IsTrueTypeFontBold = componentDetail.DataSheetHeaderIsFontBold;
            headerTxtInfo.IsTrueTypeFontItalic = componentDetail.DataSheetHeaderIsFontItalic;
            headerTxtInfo.Color = ReportHelper.IntToAsposeColor(componentDetail.DataSheetHeaderForeground);
            headerTxtInfo.IsUnicode = true;
            table.HeaderTextInfo = headerTxtInfo;


            //cell text info
            Aspose.Pdf.Generator.TextInfo txtInfo = new TextInfo();
            txtInfo.FontName = componentDetail.DataSheetRowFontName.ToString();
            txtInfo.FontSize = componentDetail.DataSheetRowFontSize;
            txtInfo.IsUnderline = componentDetail.DataSheetRowIsFontUnderlined;
            txtInfo.Alignment = AlignmentType.Left;
            txtInfo.IsTrueTypeFontBold = componentDetail.DataSheetRowIsFontBold;
            txtInfo.IsTrueTypeFontItalic = componentDetail.DataSheetRowIsFontItalic;
            txtInfo.Color = ReportHelper.IntToAsposeColor(componentDetail.DataSheetRowForeground);
            txtInfo.IsUnicode = true;
            table.CellTextInfo = txtInfo;



            //render the table
            ReportHelper.AddTableComponent(table, pdfDoc, startSectIdx, ReportHelper.GetComponentRect(printComponent));
        }

        /// <summary>
        /// Fetches the datasheet from the given path.
        /// Will fall back and check the print template file folder if possible.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        private static CustomColumnLayout FetchDataSheet(String fileName, ProcessContext context)
        {
            CustomColumnLayout layout = null;
            String dataSheetPath = fileName;

            //check if the file actually exists.
            Boolean fileExists = System.IO.File.Exists(dataSheetPath);

            //if it does not but the template is a file
            // then check the same folder just in case.
            if (!fileExists && context.PrintingTemplate.IsFile)
            {
                dataSheetPath =
                    System.IO.Path.Combine(
                    System.IO.Path.GetDirectoryName((String)context.PrintingTemplate.Id),
                    System.IO.Path.GetFileName(dataSheetPath));

                fileExists = System.IO.File.Exists(dataSheetPath);
            }


            //fetch the datasheet - only exists as files atm.
            if (fileExists)
            {
                try
                {
                    layout = CustomColumnLayout.FetchByFilename(dataSheetPath, /*asReadOnly*/true);
                }
                catch (DataPortalException) { }
            }

            return layout;
        }


        /// <summary>
        /// Gets the key for the item based on the aggregated groupings.
        /// </summary>
        private static Object GetKeyValue(PlanogramItemPlacement item, CustomColumnGroupList groupList)
        {
            List<String> resolvedValues = new List<String>(groupList.Count);

            foreach (CustomColumnGroup group in groupList)
            {
                resolvedValues.Add(Convert.ToString(
                    PlanogramFieldHelper.ResolveFieldExpression(group.Grouping, PlanogramFieldHelper.EnumerateAllFields(), item),
                    CultureInfo.CurrentCulture));
            }

            return String.Join(",", resolvedValues);
        }


        private sealed class DataSheetCell
        {
            private List<PlanogramItemPlacement> _items;

            public Object Value { get; private set; }

            public DataSheetCell(PlanogramItemPlacement item, CustomColumn column)
            {
                _items = new List<PlanogramItemPlacement> { item };

                Value = GetDataSheetCellValue(_items, column.Path);
            }

            public DataSheetCell(List<PlanogramItemPlacement> items, CustomColumn column)
            {
                _items = items;

                Value = GetDataSheetCellValue(items, column.Path);
            }

            private Object GetDataSheetCellValue(List<PlanogramItemPlacement> sourceItems, String path)
            {
                ObjectFieldExpression expression = new ObjectFieldExpression(path);
                expression.AddDynamicFieldParameters(PlanogramFieldHelper.EnumerateAllFields());

                expression.ResolveDynamicParameter += ResolveExpressionParameter;
                expression.GettingAggregationValues += AggregationValuesRequested;

                Object value = expression.Evaluate();

                expression.ResolveDynamicParameter -= ResolveExpressionParameter;
                expression.GettingAggregationValues -= AggregationValuesRequested;

                return value;
            }

            private void ResolveExpressionParameter(object sender, ObjectFieldExpressionResolveParameterArgs e)
            {
                PlanogramItemPlacement resolveTarget = e.TargetContext as PlanogramItemPlacement;

                if (resolveTarget != null)
                {
                    e.Result = PlanogramFieldHelper.ResolveField(e.Parameter.FieldInfo, resolveTarget);
                    return;
                }
                else if (_items.Count == 1)
                {
                    e.Result = PlanogramFieldHelper.ResolveField(e.Parameter.FieldInfo, _items[0]);
                    return;
                }
                else
                {
                    //we have no target so we resolve for the whole group
                    List<Object> values = new List<Object>(_items.Count);
                    foreach (PlanogramItemPlacement item in _items)
                    {
                        values.Add(PlanogramFieldHelper.ResolveField(e.Parameter.FieldInfo, item));
                    }

                    //return an aggregation
                    Object firstNotNull = values.First(v => v != null);

                    Type propertyType = firstNotNull.GetType();

                    if (propertyType == typeof(Single) || propertyType == typeof(Single?)
                    || propertyType == typeof(Decimal) || propertyType == typeof(Decimal))
                    {
                        e.Result = values.Select(v => Convert.ToDouble(v)).Sum();
                    }
                    else if (propertyType == typeof(Int32) || propertyType == typeof(Int32?)
                        || propertyType == typeof(Int16) || propertyType == typeof(Int16?)
                        || propertyType == typeof(Byte) || propertyType == typeof(Byte?))
                    {
                        e.Result = values.Select(v => Convert.ToInt32(v)).Sum();
                    }
                    else
                    {
                        if (values.Distinct().Count() == 1) e.Result = values[0];
                        else e.Result = null;
                    }
                    return;
                }

            }

            private void AggregationValuesRequested(object sender, ObjectFieldExpressionAggregationValuesArgs e)
            {
                if (String.IsNullOrWhiteSpace(e.Formula))
                {
                    e.Values = _items;
                    return;
                }

                var fields = ObjectFieldInfo.ExtractFieldsFromText(e.Formula, PlanogramFieldHelper.EnumerateAllFields());

                //create the new formula
                ObjectFieldExpression expression = new ObjectFieldExpression(e.Formula);
                fields.ForEach(f => expression.AddParameter(f));

                expression.ResolveDynamicParameter += ResolveExpressionParameter;
                expression.GettingAggregationValues += AggregationValuesRequested;


                //determine the data level
                PlanogramItemType aggregationLevel = PlanogramFieldHelper.GetFieldDataLevel(fields);
                List<PlanogramItemPlacement> resolveItems = _items.SelectMany(p => p.GetPlanogramItemPlacementOfType(aggregationLevel)).ToList();

                //resolve values for each item
                List<Object> values = new List<object>(resolveItems.Count);
                foreach (PlanogramItemPlacement p in resolveItems)
                {
                    values.Add(expression.Evaluate(p));
                }
                e.Values = values;
            }

        }

        #endregion



    }
}
