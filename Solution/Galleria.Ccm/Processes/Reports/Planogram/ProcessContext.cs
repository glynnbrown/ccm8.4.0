﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CcmPlanogram = Galleria.Framework.Planograms.Model.Planogram;
using Galleria.Ccm.Model;
using Csla;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Helpers;
using System.Collections.ObjectModel;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Planograms.Model;
using System.Globalization;

namespace Galleria.Ccm.Processes.Reports.Planogram
{
    using Gibraltar.Agent;

    /// <summary>
    /// Planogram report process context
    /// </summary>
    internal sealed class ProcessContext
    {
        #region Fields
        private PrintTemplate _printingTemplate;
        private CcmPlanogram _planogram;

        private readonly ReadOnlyCollection<ObjectFieldInfo> _masterPlanogramFieldList;

        private readonly Dictionary<Object, Label> _labelCache = new Dictionary<Object, Label>();
        private readonly Dictionary<Object, Highlight> _highlightCache = new Dictionary<Object, Highlight>();
        private readonly Dictionary<Object, PlanogramPositionLabelTexts> _positionLabelTextsCache = new Dictionary<Object, PlanogramPositionLabelTexts>();
        private readonly Dictionary<Object, PlanogramFixtureLabelTexts> _fixtureLabelTextsCache = new Dictionary<Object, PlanogramFixtureLabelTexts>();
        private readonly Dictionary<Object, PlanogramPositionHighlightColours> _highlightColoursCache = new Dictionary<Object, PlanogramPositionHighlightColours>();

        private Boolean _isOverridingLabels;
        private Boolean _isOverridingHighlights;
        const Int32 _overrideHighlightId = -1;
        const Int32 _overrideProductLabelId = -1;
        const Int32 _overrideFixtureLabelId = -2;

        #endregion

        #region Properties

        /// <summary>
        /// Returns the print template to use
        /// </summary>
        public PrintTemplate PrintingTemplate
        {
            get { return _printingTemplate; }
        }

        /// <summary>
        /// Returns the planogram context.
        /// </summary>
        public CcmPlanogram Planogram
        {
            get { return _planogram; }
        }


        /// <summary>
        /// Returns the master list of fields for a planogram.
        /// </summary>
        public ReadOnlyCollection<ObjectFieldInfo> MasterPlanogramFieldList
        {
            get { return _masterPlanogramFieldList; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public ProcessContext(PrintTemplate template, CcmPlanogram plan)
        {
            _printingTemplate = template;
            _planogram = plan;

            _masterPlanogramFieldList = PlanogramFieldHelper.EnumerateAllFields(plan).ToList().AsReadOnly();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the label with the given id from the cache or by fetching it 
        /// if it is not already loaded.
        /// </summary>
        /// <returns>Label or null</returns>
        public Label GetProductLabelById(Object id)
        {
            if (_isOverridingLabels) id = _overrideProductLabelId;

            return GetLabelById(id);
        }

        /// <summary>
        /// Returns the label with the given id from the cache or by fetching it 
        /// if it is not already loaded.
        /// </summary>
        /// <returns>Label or null</returns>
        public Label GetFixtureLabelById(Object id)
        {
            if (_isOverridingLabels) id = _overrideFixtureLabelId;

            return GetLabelById(id);
        }

        /// <summary>
        /// Returns the label with the given id from the cache or by fetching it 
        /// if it is not already loaded.
        /// </summary>
        /// <returns>Label or null</returns>
        private Label GetLabelById(Object id)
        {
            if (id == null) return null;

            Label item = null;

            if (!_labelCache.TryGetValue(id, out item))
            {
                try
                {
                    Int32 repositoryId;
                    if (id is Int32)
                    {
                        item = Label.FetchById((Int32)id, /*asReadonly*/true);
                    }
                    else if (Int32.TryParse(id as String, out repositoryId))
                    {
                        item = Label.FetchById(repositoryId, /*asReadonly*/true);
                    }
                    else if (id is String)
                    {
                        item = Label.FetchByFilename((String)id, /*asReadonly*/true);
                    }
                }
                catch (DataPortalException)
                {
                    //there not much we can do so just carry on.
                }

                //add the item to the cache even if it was null
                // so we don't try to fetch it again.
                _labelCache[id] = item;

            }

            return item;
        }

        /// <summary>
        /// Returns the position label texts for the label with the given id.
        /// </summary>
        /// <param name="id">the label id</param>
        /// <returns>PlanogramPositionLabelTexts or null</returns>
        public PlanogramPositionLabelTexts GetPositionLabelTextsByLabelId(Object id)
        {
            //if we are overriding then force the id.
            if (_isOverridingLabels) id = _overrideProductLabelId;

            if (id == null) return null;

            PlanogramPositionLabelTexts texts = null;

            if (!_positionLabelTextsCache.TryGetValue(id, out texts))
            {
                //create the item
                Label label = GetLabelById(id);
                if (label != null)
                {
                    texts = new PlanogramPositionLabelTexts(label.Text, this.Planogram);
                }

                //add to the cache
                _positionLabelTextsCache[id] = texts;
            }

            return texts;
        }

        /// <summary>
        /// Returns the fixture label texts for the label with the given id.
        /// </summary>
        /// <param name="id">the label id</param>
        /// <returns>PlanogramFixtureLabelTexts or null</returns>
        public PlanogramFixtureLabelTexts GetFixtureLabelTextsByLabelId(Object id)
        {
            //if we are overriding then force the id value.
            if (_isOverridingLabels) id = _overrideFixtureLabelId;

            if (id == null) return null;


            PlanogramFixtureLabelTexts texts = null;

            if (!_fixtureLabelTextsCache.TryGetValue(id, out texts))
            {
                //create the item
                Label label = GetLabelById(id);
                if (label != null)
                {
                    texts = new PlanogramFixtureLabelTexts(label.Text, this.Planogram);
                }

                //add to the cache
                _fixtureLabelTextsCache[id] = texts;
            }

            return texts;
        }

        /// <summary>
        /// Sets this context to override labels with those given.
        /// </summary>
        public void SetLabelOverrides(Label productLabel, Label fixtureLabel)
        {
            _isOverridingLabels = true;
            _labelCache.Add(_overrideProductLabelId, productLabel);
            _labelCache.Add(_overrideFixtureLabelId, fixtureLabel);
        }

        /// <summary>
        /// Returns the fixture colours for the highlight with the given id.
        /// </summary>
        /// <param name="id">the highlight id</param>
        /// <returns>PlanogramPositionHighlightColours or null</returns>
        public PlanogramPositionHighlightColours GetHighlightColoursByHighlightId(Object id)
        {
            //if we are overriding then force the id.
            if (_isOverridingHighlights) id = _overrideHighlightId;


            if (id == null) return null;

            PlanogramPositionHighlightColours colours = null;

            if (!_highlightColoursCache.TryGetValue(id, out colours))
            {
                //Fetch the highlight
                Highlight highlight = null;
                if (!_highlightCache.TryGetValue(id, out highlight))
                {
                    try
                    {
                        Int32 repositoryId;
                        if (id is Int32)
                        {
                            highlight = Highlight.FetchById((Int32)id, /*asReadonly*/true);
                        }
                        else if (Int32.TryParse(id as String, out repositoryId))
                        {
                            highlight = Highlight.FetchById(repositoryId, /*asReadonly*/true);
                        }
                        else if (id is String)
                        {
                            highlight = Highlight.FetchByFilename((String)id, /*asReadonly*/true);
                        }
                        _highlightCache[id] = highlight;
                    }
                    catch (DataPortalException)
                    {
                        //there not much we can do so just carry on.
                    }
                }

                //Process the colours.
                if (highlight != null)
                {
                    colours = new PlanogramPositionHighlightColours(
                       PlanogramHighlightHelper.ProcessPositionHighlight(this.Planogram, highlight));
                }

                //add to the cache
                _highlightColoursCache[id] = colours;
            }

            return colours;
        }

        /// <summary>
        /// Sets this context to override highlights with that given.
        /// </summary>
        public void SetHiglightOverride(Highlight highlight)
        {
            _isOverridingHighlights = true;
            _highlightCache.Add(_overrideHighlightId, highlight);
        }

        /// <summary>
        /// Resolves the given textbox string for this planogram.
        /// </summary>
        public String ResolveTextBoxText(String textFormat)
        {
            StringBuilder sb = new StringBuilder(textFormat);

            //now extract all of the remaining fields
            List<ObjectFieldInfo> usedFields = ObjectFieldInfo.ExtractFieldsFromText(textFormat,
                PrintTemplateComponentDetail.EnumerateSpecialTextFields()
                .Union(CcmPlanogram.EnumerateDisplayableFieldInfos(/*incMeta*/true)));

            foreach (ObjectFieldInfo field in usedFields)
            {
                if (field.IsSpecialType)
                {
                    switch (field.PropertyName)
                    {
                        case PrintTemplateComponentDetail.SpecialFieldNameDate:
                            sb.Replace(field.FieldPlaceholder, DateTime.Now.ToShortDateString());
                            continue;
                           

                        case PrintTemplateComponentDetail.SpecialFieldNameDateTime:
                            sb.Replace(field.FieldPlaceholder, DateTime.Now.ToShortDateString() + ' ' + DateTime.Now.ToShortTimeString());
                            continue;

                        case PrintTemplateComponentDetail.SpecialFieldNamePageNumber:
                            sb.Replace(field.FieldPlaceholder, " $p "); //special aspose pdf char
                            sb.Replace("  $p", " $p");
                            sb.Replace("$p  ", "$p ");
                            continue;

                        case PrintTemplateComponentDetail.SpecialFieldNameTotalPages:
                            sb.Replace(field.FieldPlaceholder, " $P "); //special aspose pdf char
                            sb.Replace("  $P", " $P");
                            sb.Replace("$P  ", "$P ");
                            continue;
                    }
                }

                //replace the planogram field
                sb.Replace(field.FieldPlaceholder, 
                    Convert.ToString(
                    PlanogramItemPlacement.NewPlanogramItemPlacement(this.Planogram).ResolveField(field),
                    CultureInfo.CurrentCulture));
            }


            return sb.ToString();
        }


        #endregion

        public void PreloadExistingPlanogramImages()
        {
            if (!PrintingTemplate.SectionGroups.Any(HasSectionWithPlanogramComponentWithProductImages)) return;

            try
            {
                PlanogramImagesHelper.RealImageProvider.PreloadPlanogram(Planogram, false);
            }
            catch (Exception ex)
            {
                //if image load fails then just carry on rendering without.
                Log.RecordException(ex, null, /*canContinue*/true);
            }
        }

        public List<PlanogramFixtureItem> PlanogramFixtureItems()
        {
            List<PlanogramFixtureItem> allBays =
                Planogram.FixtureItems.OrderBy(b => PrintingTemplate.IsPlanMirrored ? -b.X : b.X).ToList();
            return allBays;
        }

        private static Boolean HasSectionWithPlanogramComponentWithProductImages(PrintTemplateSectionGroup sg)
        {
            return sg.Sections.Any(HasPlanogramComponentWithProductImages);
        }

        private static Boolean HasPlanogramComponentWithProductImages(PrintTemplateSection s)
        {
            return s.Components.Any(c => c.Type == PrintTemplateComponentType.Planogram && c.ComponentDetails.PlanogramProductImages);
        }
    }
}
