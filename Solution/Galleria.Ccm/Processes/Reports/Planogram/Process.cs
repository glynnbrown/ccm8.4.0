﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created
#endregion
#endregion


using System;
using Csla;
using Galleria.Ccm.Model;
using CcmPlanogram = Galleria.Framework.Planograms.Model.Planogram;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Rendering;
using System.Collections.Generic;

namespace Galleria.Ccm.Processes.Reports.Planogram
{
    /// <summary>
    /// A process that returns the pdf for a planogram report.
    /// </summary>
    [Serializable]
    public sealed partial class Process : ReportProcessBase<Process>
    {
        #region Properties

        #region PrintTemplateId

        /// <summary>
        /// PrintTemplateId property definition
        /// </summary>
        public static readonly PropertyInfo<Object> PrintTemplateIdProperty =
             RegisterProperty<Object>(p => p.PrintTemplateId);
        /// <summary>
        /// Gets the id of the print template to use, should one not be
        /// provided directly.
        /// </summary>
        public Object PrintTemplateId
        {
            get { return this.ReadProperty<Object>(PrintTemplateIdProperty); }
            private set { this.LoadProperty<Object>(PrintTemplateIdProperty, value); }
        }

        #endregion

        #region PrintingTemplate

        /// <summary>
        /// PrintingTemplate property definition
        /// </summary>
        public static readonly PropertyInfo<PrintTemplate> PrintingTemplateProperty =
             RegisterProperty<PrintTemplate>(p => p.PrintingTemplate);
        /// <summary>
        /// Gets the print template to use
        /// </summary>
        public PrintTemplate PrintingTemplate
        {
            get { return this.ReadProperty<PrintTemplate>(PrintingTemplateProperty); }
            private set { this.LoadProperty<PrintTemplate>(PrintingTemplateProperty, value); }
        }

        #endregion

        #region PlanogramId

        /// <summary>
        /// PlanogramId property definition
        /// </summary>
        public static readonly PropertyInfo<Object> PlanogramIdProperty =
             RegisterProperty<Object>(p => p.PlanogramId);
        /// <summary>
        /// Gets the id of the planogram to use, should one not be
        /// provided directly.
        /// </summary>
        public Object PlanogramId
        {
            get { return this.ReadProperty<Object>(PlanogramIdProperty); }
            private set { this.LoadProperty<Object>(PlanogramIdProperty, value); }
        }

        #endregion

        #region Planogram

        /// <summary>
        /// PlanogramContext property definition
        /// </summary>
        public static readonly PropertyInfo<CcmPlanogram> PlanogramContextProperty =
             RegisterProperty<CcmPlanogram>(p => p.PlanogramContext);
        /// <summary>
        /// Gets the planogram for which the report is to be produced.
        /// </summary>
        public CcmPlanogram PlanogramContext
        {
            get { return this.ReadProperty<CcmPlanogram>(PlanogramContextProperty); }
            private set { this.LoadProperty<CcmPlanogram>(PlanogramContextProperty, value); }
        }

        #endregion

        #region IsOverridingPlanogramHighlights

        /// <summary>
        /// IsOverridingPlanogramHighlights property definition
        /// </summary>
        public static readonly PropertyInfo<Boolean> IsOverridingPlanogramHighlightsProperty =
            RegisterProperty<Boolean>(p => p.IsOverridingPlanogramHighlights);

        /// <summary>
        /// Gets/Sets whether the planogram component highlights should be overridden by
        /// the value of the PlanogramHighlightOverride property
        /// </summary>
        public Boolean IsOverridingPlanogramHighlights
        {
            get { return this.ReadProperty<Boolean>(IsOverridingPlanogramHighlightsProperty); }
            set { this.LoadProperty<Boolean>(IsOverridingPlanogramHighlightsProperty, value); }
        }

        #endregion

        #region PlanogramHighlightOverride

        /// <summary>
        /// PlanogramHighlightOverride property definition
        /// </summary>
        public static readonly PropertyInfo<Highlight> PlanogramHighlightOverrideProperty =
            RegisterProperty<Highlight>(p => p.PlanogramHighlightOverride);

        /// <summary>
        /// Gets/Sets the overriding highlight to use.
        /// </summary>
        public Highlight PlanogramHighlightOverride
        {
            get { return this.ReadProperty<Highlight>(PlanogramHighlightOverrideProperty); }
            set { this.LoadProperty<Highlight>(PlanogramHighlightOverrideProperty, value); }
        }

        #endregion

        #region IsOverridingPlanogramLabels

        /// <summary>
        /// IsOverridingPlanogramLabels property definition
        /// </summary>
        public static readonly PropertyInfo<Boolean> IsOverridingPlanogramLabelsProperty =
            RegisterProperty<Boolean>(p => p.IsOverridingPlanogramLabels);

        /// <summary>
        /// Gets/Sets whether the planogram component highlights should be overridden by
        /// the value of the PlanogramLabelOverride properties
        /// </summary>
        public Boolean IsOverridingPlanogramLabels
        {
            get { return this.ReadProperty<Boolean>(IsOverridingPlanogramLabelsProperty); }
            set { this.LoadProperty<Boolean>(IsOverridingPlanogramLabelsProperty, value); }
        }

        #endregion

        #region PlanogramFixtureLabelOverride

        /// <summary>
        /// PlanogramFixtureLabelOverride property definition
        /// </summary>
        public static readonly PropertyInfo<Label> PlanogramFixtureLabelOverrideProperty =
           RegisterProperty<Label>(p => p.PlanogramFixtureLabelOverride);

        /// <summary>
        /// Gets/Sets the overriding fixture label to use.
        /// </summary>
        public Label PlanogramFixtureLabelOverride
        {
            get { return this.ReadProperty<Label>(PlanogramFixtureLabelOverrideProperty); }
            set { this.LoadProperty<Label>(PlanogramFixtureLabelOverrideProperty, value); }
        }

        #endregion

        #region PlanogramModelDataList

        /// <summary>
        /// PlanogramModelDataList property definition
        /// </summary>
        public static readonly PropertyInfo<List<Plan3DData>> PlanogramModelDataListProperty =
           RegisterProperty<List<Plan3DData>>(p => p.PlanogramModelDataList);

        /// <summary>
        /// Gets/Sets the PlanogramModelData.
        /// </summary>
        public List<Plan3DData> PlanogramModelDataList
        {
            get { return this.ReadProperty<List<Plan3DData>>(PlanogramModelDataListProperty); }
            set { this.LoadProperty<List<Plan3DData>>(PlanogramModelDataListProperty, value); }
        }

        #endregion

        #region PlanogramProductLabelOverride

        /// <summary>
        /// PlanogramProductLabelOverride property definition
        /// </summary>
        public static readonly PropertyInfo<Label> PlanogramProductLabelOverrideProperty =
           RegisterProperty<Label>(p => p.PlanogramProductLabelOverride);

        /// <summary>
        /// Gets/Sets the overriding fixture label to use.
        /// </summary>
        public Label PlanogramProductLabelOverride
        {
            get { return this.ReadProperty<Label>(PlanogramProductLabelOverrideProperty); }
            set { this.LoadProperty<Label>(PlanogramProductLabelOverrideProperty, value); }
        }

        #endregion

        #region IsSoftwareRender

        /// <summary>
        /// IsSoftwareRender property definition
        /// </summary>
        public static readonly PropertyInfo<Boolean> IsSoftwareRenderProperty =
            RegisterProperty<Boolean>(p => p.IsSoftwareRender);
        /// <summary>
        /// Gets/Sets whether planogram images should use the software renderer.
        /// </summary>
        public Boolean IsSoftwareRender
        {
            get { return this.ReadProperty<Boolean>(IsSoftwareRenderProperty); }
            set { this.LoadProperty<Boolean>(IsSoftwareRenderProperty, value); }
        }
        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="printTemplateId">the id of the template to use</param>
        /// <param name="planogram">the planogram to produce the report for.</param>
        public Process(Object printTemplateId, CcmPlanogram planogram)
        {
            this.PrintTemplateId = printTemplateId;
            this.PlanogramContext = planogram;
            this.IsSoftwareRender = true;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="template">the template to use</param>
        /// <param name="planogram">the planogram to produce the report for.</param>
        public Process(PrintTemplate template, CcmPlanogram planogram)
        {
            this.PrintingTemplate = template;
            this.PlanogramContext = planogram;
            this.IsSoftwareRender = true;
        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion
    }
}
