﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Copied from GFS with amendments.
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Aspose.Pdf.Generator;
using Galleria.Ccm.Model;
using Galleria.Framework.ViewModel;
using Galleria.Framework.Algorithms.LinearRegression;
using CultureInfo = System.Globalization.CultureInfo;

namespace Galleria.Ccm.Processes.Reports
{
    /// <summary>
    /// Very simple lightweight structure describing report table data
    /// </summary>
    internal sealed class ReportTable
    {
        #region Nested Classes
        private class ReportPropertySortComparer<T> : IComparer<T>
        {
            private String _sortProperty;
            private ListSortDirection _sortDirection = ListSortDirection.Ascending;

            public ReportPropertySortComparer(String propertyName, System.ComponentModel.ListSortDirection direction)
            {
                _sortProperty = propertyName;
                _sortDirection = direction;
            }


            public int Compare(T x, T y)
            {
                object xValue = Framework.Helpers.WpfHelper.GetItemPropertyValue(x, _sortProperty);
                object yValue = Framework.Helpers.WpfHelper.GetItemPropertyValue(y, _sortProperty);

                Int32 sortValue = 0;

                if (xValue is IComparable)
                {
                    sortValue = ((IComparable)xValue).CompareTo(yValue);
                }
                else if (yValue is IComparable)
                {
                    sortValue = ((IComparable)yValue).CompareTo(xValue);
                }
                else if (!Object.Equals(xValue,yValue))
                {
                    sortValue = Convert.ToString(xValue).CompareTo(Convert.ToString(yValue));
                }

                return (_sortDirection == ListSortDirection.Ascending) ? sortValue : (sortValue * -1);
            }

        }
        #endregion

        #region Fields
        private List<ReportTableColumn> _columns = new List<ReportTableColumn>();
        private List<ReportTableRow> _rows = new List<ReportTableRow>();
        private PrintTemplateComponentGridStyle _style = PrintTemplateComponentGridStyle.Standard;
        #endregion

        #region Properties

        /// <summary>
        /// The table column descriptions
        /// </summary>
        public List<ReportTableColumn> Columns
        {
            get { return _columns; }
        }

        /// <summary>
        /// The table row descriptions
        /// </summary>
        public List<ReportTableRow> Rows
        {
            get { return _rows; }
        }

        /// <summary>
        /// Gets/Sets the style of the table
        /// </summary>
        public PrintTemplateComponentGridStyle Style
        {
            get { return _style; }
            set { _style = value; }
        }

        /// <summary>
        /// Gets/Sets the info for the header text
        /// </summary>
        public TextInfo HeaderTextInfo { get; set; }

        /// <summary>
        /// Gets/Sets the info for the cell text
        /// </summary>
        public TextInfo CellTextInfo { get; set; }


        #endregion

        #region Contructor
        public ReportTable() { }
        #endregion

        #region Methods

        /// <summary>
        /// Creates and adds the columns layout list
        /// </summary>
        public void AddColumns(IEnumerable<CustomColumn> layoutColumns, List<ObjectFieldInfo> colFieldInfos)
        {
            Int32 colCount = Math.Max(layoutColumns.Count(), colFieldInfos.Count);

            for (Int32 i = 0; i < colCount; i++)
            {
                CustomColumn col = layoutColumns.ElementAt(i);
                ObjectFieldInfo field = colFieldInfos[i];

                String header =
                    (!String.IsNullOrEmpty(col.DisplayName)) ?
                    col.DisplayName :
                    field.FieldFriendlyName;

                this.Columns.Add(new ReportTableColumn(header, col.Path, col.Width));
            }
        }

        /// <summary>
        /// Applies the given sorts to the rows list
        /// </summary>
        /// <param name="sortProperties"></param>
        /// <param name="sortDescriptions"></param>
        public void ApplySorts(CustomColumnSortList sortList)
        {
            if (sortList.Count == 0) return;


            IOrderedEnumerable<ReportTableRow> sortedList = null;
            List<String> propertyDescs = this.Columns.Select(c => c.PropertyMap).ToList();

            Boolean isFirst = true;
            foreach (CustomColumnSort sort in sortList)
            {
                Int32 rowIdx = propertyDescs.IndexOf(sort.Path);
                if (rowIdx == -1) continue;

                String scmPath = String.Format("ItemArray[{0}]", rowIdx);
                if (isFirst)
                {
                    sortedList = this.Rows.OrderBy(r => r, new ReportPropertySortComparer<ReportTableRow>(scmPath, sort.Direction));
                    isFirst = false;
                }
                else
                {
                    sortedList = sortedList.ThenBy(r => r, new ReportPropertySortComparer<ReportTableRow>(scmPath, sort.Direction));
                }
            }

            if (sortedList == null) return;

            List<ReportTableRow> finalList = sortedList.ToList();

            //clear the existing collection and re-add rows in the new order.
            this.Rows.Clear();
            foreach (ReportTableRow row in finalList)
            {
                this.Rows.Add(row);
            }
        }

        /// <summary>
        /// Applies the given sorts to the rows list
        /// </summary>
        public void ApplyFilters(CustomColumnFilterList filterList)
        {
            if (filterList.Count == 0) return;

            List<String> propertyDescs = this.Columns.Select(c => c.PropertyMap).ToList();

            foreach (ReportTableRow row in this.Rows.ToList())
            {
                foreach (CustomColumnFilter filter in filterList)
                {
                    Int32 rowIdx = propertyDescs.IndexOf(filter.Path);
                    if (rowIdx == -1) continue;

                    //String scmPath = String.Format("ItemArray[{0}]", rowIdx);

                    if (!PassesFilter(filter.Text, row.ItemArray[rowIdx]))
                    {
                        this.Rows.Remove(row);
                        break;
                    }

                }
            }
        }

        /// <summary>
        /// Returns true if the item passes the given filter.
        /// </summary>
        private static Boolean PassesFilter(String filter, Object objPropertyValue)
        {
            if (!String.IsNullOrEmpty(filter))
            {
                String filterValue = filter.ToUpperInvariant();

                //get the string value
                String propertyValue;
                if (objPropertyValue != null)
                {
                    propertyValue = objPropertyValue.ToString().ToUpperInvariant();
                }
                else { propertyValue = String.Empty; }

                Boolean returnValue = true;
                if (propertyValue != null)
                {
                    String filterCriteria = filterValue;

                    //remove the modifers from the filtercriteria
                    filterCriteria = filterValue.Replace("\"", String.Empty);

                    //check for search modifiers in the filtervalue
                    if (filterValue.StartsWith("\"") && filterValue.EndsWith("\""))
                    {
                        //exact filter
                        if (propertyValue != filterCriteria)
                        {
                            returnValue = false;
                        }

                    }

                    //other just do a contains check
                    else
                    {
                        if (!propertyValue.Contains(filterCriteria))
                        {
                            returnValue = false;
                        }
                    }
                }

                return returnValue;
            }
            return true;
        }

        /// <summary>
        /// Adds in the grouping rows.
        /// </summary>
        public void ApplyGroupingsAndTotals(CustomColumnLayout layout)
        {
            if (!layout.IsGroupDetailHidden && layout.GroupList.Any())
            {
                List<ReportTableRow> rows = this.Rows.ToList();
                this.Rows.Clear();

                List<String> propertyDescs = this.Columns.Select(c => c.PropertyMap).ToList();
                ProcessGroups(0, layout, rows, propertyDescs);
            }

            //add the final totals row
            if (layout.HasTotals)
            {
                //add the final totals row with a blank one before it.
                if (this.Rows.Last().RowType != ReportTableRow.ReportTableRowType.Blank)
                {
                    AddBlankRow(this.Rows.Count);
                }
                AddTotalsRow(this.Rows.Count, this.Rows, layout.Columns);
            }
        }

        private void ProcessGroups(Int32 groupIdx, CustomColumnLayout layout,
            IList<ReportTableRow> rowSet, List<String> propertyDescs)
        {
            CustomColumnGroup curGroup = layout.GroupList.ElementAt(groupIdx);
            Int32 rowIdx = propertyDescs.IndexOf(curGroup.Grouping);

            foreach (var group in rowSet.GroupBy(g => g.ItemArray[rowIdx]))
            {
                //add a new grouping row.
                this.Rows.Add(
                    new ReportTableRow(new Object[] { group.Key },
                        ReportTableRow.ReportTableRowType.GroupHeader));

                if ((groupIdx + 1) < layout.GroupList.Count)
                {
                    //process next level
                    ProcessGroups(groupIdx + 1, layout, group.ToList(), propertyDescs);
                }
                else
                {
                    //insert in the rows from the group.
                    foreach (var row in group)
                    {
                        this.Rows.Add(row);
                    }
                }


                //add the group totals row followed by blank row.
                if(layout.HasTotals)
                {
                    Int32 idx = this.Rows.Count;//this.Rows.IndexOf(group.Last()) +1;
                   AddTotalsRow(idx, group, layout.Columns);
                    AddBlankRow(idx+1);
                }
                else if (layout.IsGroupBlankRowShown)
                {
                    Int32 idx = this.Rows.IndexOf(group.Last()) + 1;
                    AddBlankRow(idx);
                }
            }
        }


        /// <summary>
        /// Returns the totals row for the given row set.
        /// </summary>
        private void AddTotalsRow(Int32 addAtIdx, IEnumerable<ReportTableRow> rowSet, IEnumerable<CustomColumn> layoutColumns)
        {
            Object[] values = new Object[layoutColumns.Count()];

            List<String> propertyDescs = this.Columns.Select(c => c.PropertyMap).ToList();

            Int32 colIdx = 0;
            foreach (CustomColumn col in layoutColumns)
            {
                if (col.TotalType == CustomColumnTotalType.None
                    || !propertyDescs.Contains(col.Path))
                {
                    values[colIdx] = null;
                    colIdx++;
                    continue;
                }


                Int32 propertyIdx = propertyDescs.IndexOf(col.Path);
                if(propertyIdx == -1) continue;

                try
                {
                    switch (col.TotalType)
                    {
                        case CustomColumnTotalType.None:
                            values[colIdx] = null;
                            break;

                        case CustomColumnTotalType.Sum:
                            values[colIdx] = 
                                String.Format(CultureInfo.CurrentCulture,
                                "{0}: {1}", CustomColumnTotalTypeHelper.FriendlyNames[col.TotalType],
                                rowSet.Sum(r => Convert.ToDouble(r.ItemArray[propertyIdx])));
                            break;

                        case CustomColumnTotalType.Count:
                            values[colIdx] = 
                                String.Format(CultureInfo.CurrentCulture,
                                "{0}: {1}", CustomColumnTotalTypeHelper.FriendlyNames[col.TotalType],
                                rowSet.Count());
                            break;

                        case CustomColumnTotalType.CountFalse:
                            values[colIdx] = 
                                String.Format(CultureInfo.CurrentCulture,
                                "{0}: {1}", CustomColumnTotalTypeHelper.FriendlyNames[col.TotalType],
                                rowSet.Count(r => Convert.ToBoolean(r.ItemArray[propertyIdx]) == false));
                            break;

                        case CustomColumnTotalType.CountTrue:
                            values[colIdx] = 
                                String.Format(CultureInfo.CurrentCulture,
                                "{0}: {1}", CustomColumnTotalTypeHelper.FriendlyNames[col.TotalType],
                                rowSet.Count(r => Convert.ToBoolean(r.ItemArray[propertyIdx]) == true));
                            break;

                        case CustomColumnTotalType.Max:
                            if (rowSet.Any())
                            {
                                values[colIdx] = 
                                    String.Format(CultureInfo.CurrentCulture,
                                    "{0}: {1}", CustomColumnTotalTypeHelper.FriendlyNames[col.TotalType],
                                    rowSet.Max(r => Convert.ToDouble(r.ItemArray[propertyIdx])));
                            }
                            break;

                        case CustomColumnTotalType.Min:
                            if (rowSet.Any())
                            {
                                values[colIdx] =
                                    String.Format(CultureInfo.CurrentCulture,
                                    "{0}: {1}", CustomColumnTotalTypeHelper.FriendlyNames[col.TotalType],
                                    rowSet.Min(r => Convert.ToDouble(r.ItemArray[propertyIdx])));
                            }
                            break;

                        case CustomColumnTotalType.Average:
                            if (rowSet.Any())
                            {
                                values[colIdx] = 
                                     String.Format(CultureInfo.CurrentCulture,
                                    "{0}: {1}", CustomColumnTotalTypeHelper.FriendlyNames[col.TotalType],
                                    rowSet.Average(r => Convert.ToDouble(r.ItemArray[propertyIdx])));
                            }
                            break;

                        case CustomColumnTotalType.StandardDeviation:
                            {
                                    values[colIdx] =
                                        String.Format(CultureInfo.CurrentCulture,
                                    "{0}: {1}", CustomColumnTotalTypeHelper.FriendlyNames[col.TotalType],
                                         Convert.ToSingle(Math.Round(
                                         RegressionHelper.GetStandardDeviation(rowSet.Select(r => Convert.ToDouble(r.ItemArray[propertyIdx]))), 
                                         2, MidpointRounding.AwayFromZero)));

                            }
                            break;

                    }
                }
                catch (Exception) { }

                colIdx++;
            }


            //insert the row
            ReportTableRow row =new ReportTableRow(values, ReportTableRow.ReportTableRowType.Totals);

            if (addAtIdx >= this.Rows.Count)
            {
                this.Rows.Add(row);
            }
            else
            {
                this.Rows.Insert(addAtIdx, row);
            }
        }

        private void AddBlankRow(Int32 addAtIdx)
        {
            //insert the row
            ReportTableRow row = new ReportTableRow() 
            { RowType = ReportTableRow.ReportTableRowType.Blank };

            if (addAtIdx >= this.Rows.Count)
            {
                this.Rows.Add(row);
            }
            else
            {
                this.Rows.Insert(addAtIdx, row);
            }
        }

        #endregion
    }

    /// <summary>
    /// Represents a column in a ReportTable
    /// </summary>
    internal sealed class ReportTableColumn
    {
        public String ColumnName { get; set; }
        public AlignmentType Alignment { get; set; }
        public String PropertyMap { get; set; }
        public Int32 Width { get; set; }
        public String StringFormat { get; set; }

        public ReportTableColumn()
        {
            this.Alignment = AlignmentType.Left;
        }
        public ReportTableColumn(String colName, String propertyMap, Int32 width)
        {
            this.ColumnName = colName;
            this.Alignment = AlignmentType.Left;
            this.PropertyMap = propertyMap;
            this.Width = width;
        }

        /// <summary>
        /// Returns the cellstring for the given value.
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public String GetCellString(Object val)
        {
            String cellString = String.Empty;

            if (val != null)
            {
                if (!String.IsNullOrEmpty(this.StringFormat)
                    && val is Double)
                {
                    cellString = ((Double)val).ToString(this.StringFormat);
                }
                else
                {
                    cellString = Convert.ToString(val);
                }
            }

            return cellString;
        }
    }

    /// <summary>
    /// Represents a row in a ReportTable
    /// </summary>
    internal sealed class ReportTableRow
    {
        public enum ReportTableRowType
        {
            Normal = 0,
            GroupHeader = 1,
            Totals = 2, 
            Blank = 3
        }

        public ReportTableRowType RowType { get; set; }
        public Object[] ItemArray { get; set; }

        public ReportTableRow() { }
        public ReportTableRow(Object[] items)
        {
            this.ItemArray = items;
        }
        public ReportTableRow(Object[] items, ReportTableRowType rowType)
        {
            this.ItemArray = items;
            this.RowType = rowType;
        }
    }
}
