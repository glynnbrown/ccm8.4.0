﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Copied from GFS.
#endregion
#endregion


using System;
using Amib.Threading;
using Galleria.Framework.Processes;
using System.Threading;
using Aspose.Pdf.Generator;
using Csla;
using System.Configuration;
using System.IO;

namespace Galleria.Ccm.Processes.Reports
{
    /// <summary>
    /// A base class from which all report processes can inherit. Handles
    /// the work of ensuring that the report is executed on a STA thread
    /// so that we can use WPF inside the report. In addition manages
    /// a pool of STA threads to ensure that can multitask some report parts
    /// </summary>
    /// <typeparam name="T">The report process type</typeparam>
    public abstract class ReportProcessBase<T> : ProcessBase<T>, IReportProcess
        where T : ReportProcessBase<T>
    {
        #region Constants
        private const String _reportConcurrencyKey = "ReportConcurrency"; // the app config item key that controls
        private const Int32 _defaultReportConcurrency = 0;
        #endregion

        #region Fields
        private static Lazy<SmartThreadPool> _threadPool = new Lazy<SmartThreadPool>(InitializeThreadPool, LazyThreadSafetyMode.ExecutionAndPublication);
        #endregion

        #region Properties

        #region ThreadPool
        /// <summary>
        /// Returns a reference to the thread pool
        /// </summary>
        protected SmartThreadPool ThreadPool
        {
            get
            {
                return _threadPool.Value;
            }
        }
        #endregion

        #region Report
        /// <summary>
        /// Report property definition
        /// </summary>
        private static PropertyInfo<Pdf> ReportProperty =
            RegisterProperty<Pdf>(p => p.Report);
        /// <summary>
        /// Returns the report pdf document
        /// </summary>
        protected Pdf Report
        {
            get { return this.ReadProperty<Pdf>(ReportProperty); }
            set { this.LoadProperty<Pdf>(ReportProperty, value); }
        }
        #endregion

        #region ReportData
        /// <summary>
        /// Report property definition
        /// </summary>
        private static PropertyInfo<Byte[]> ReportDataProperty =
            RegisterProperty<Byte[]>(p => p.ReportData);
        /// <summary>
        /// Returns the report pdf document
        /// </summary>
        public Byte[] ReportData
        {
            get { return this.ReadProperty<Byte[]>(ReportDataProperty); }
            protected set { this.LoadProperty<Byte[]>(ReportDataProperty, value); }
        }
        #endregion

        #region ThreadCount
        /// <summary>
        /// Returns the number of threads that should be used for a report
        /// </summary>
        protected Int32 ReportThreadCount
        {
            get { return InitialThreadCount; }
        }
        #endregion

        #region InitialThreadCount
        /// <summary>
        /// Returns the number of threads that will be used to initialize the thread pool
        /// </summary>
        internal static Int32 InitialThreadCount
        {
            get
            {
                Int32 reportConcurrency;
                if (!Int32.TryParse(ConfigurationManager.AppSettings[_reportConcurrencyKey], out reportConcurrency))
                    reportConcurrency = _defaultReportConcurrency;
                if (reportConcurrency == 0)
                    return 1;
                return reportConcurrency * Environment.ProcessorCount;
            }
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Initializes the report thread pool when required
        /// </summary>
        /// <returns></returns>
        private static SmartThreadPool InitializeThreadPool()
        {
            // Create the start-up information for the thread pool
            STPStartInfo startInfo = new STPStartInfo();
            startInfo.ApartmentState = ApartmentState.STA; // we use STAs in order to be able utilise WPF inside a report
            startInfo.UseCallerCallContext = true;
            startInfo.UseCallerHttpContext = true;
            startInfo.MaxWorkerThreads = InitialThreadCount;
            startInfo.MinWorkerThreads = startInfo.MaxWorkerThreads;

            // create and start the thread pool
            SmartThreadPool threadPool = new SmartThreadPool(startInfo);
            threadPool.Start();

            // return the thread pool
            return threadPool;
        }

        /// <summary>
        /// Called when this report is executed
        /// </summary>
        protected override void OnExecute()
        {
            // we need to ensure that when we execute the report
            // we do so on a STA thread as this is required by WPF
            if (Thread.CurrentThread.GetApartmentState() == ApartmentState.STA)
            {
                // the thread is already an STA thread, so we simply call
                // the OnExecuteReport method
                OnInitializeReport(this);
            }
            else
            {
                // create a new workgroup for this report thread
                // this workgroup only needs a concurrency value of 1
                // as are only going to create a single work item
                IWorkItemsGroup group = this.ThreadPool.CreateWorkItemsGroup(1);

                // queue a work item for the report
                IWorkItemResult result = group.QueueWorkItem(new WorkItemCallback(OnInitializeReport), this);

                // wait for the work item to complete
                group.WaitForIdle();

                // if an exception occurred then throw the exception now
                if (result.Exception != null)
                {
                    throw new ReportException(result.Exception);
                }
            }
        }

        /// <summary>
        /// Method that is called when a report is executed
        /// after the threading has been checked
        /// </summary>
        private static Object OnInitializeReport(Object state)
        {
            // cast the state to the process base
            ReportProcessBase<T> process = state as ReportProcessBase<T>;
            if (process != null)
            {
                // at this point we are definately working on
                // a STA thread, so create our report object now
                process.Report = new Pdf();

                // call the report execute method
                process.OnExecuteReport();

                //create the report data
                using (MemoryStream ms = new MemoryStream())
                {
                    process.Report.Save(ms);
                    process.ReportData = ms.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Method that is called when a report is executed
        /// after the threading has been checked
        /// </summary>
        protected virtual void OnExecuteReport()
        {
        }
        #endregion
    }
}
