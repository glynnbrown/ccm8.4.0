﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Copied from GFS.
// V8-31534 : L.Ineson
//  Made sure that table header styling gets applied
#region Version History: (CCM 832)
// CCM-18477 : G.Richards
//	The print preview function could not calculate the row height when the row data contained a character that was not mapped and included a replacement character �.
//  This caused the preview to stop working. We now detect if the row data contains any invalid character maps and uses the average row height and continues.
#endregion
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using Aspose.Pdf.Generator;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Processes.Reports
{
    /// <summary>
    /// Provides general helper methods for use by the 
    /// Report processes
    /// </summary>
    internal static class ReportHelper
    {
        #region General

        /// <summary>
        /// Sets the size of the given pdf section
        /// </summary>
        /// <param name="pdfSection"></param>
        /// <param name="paperSize"></param>
        public static void SetSectionPageSize(Section pdfSection, PrintTemplatePaperSizeType paperSize, Boolean isLandscape)
        {
            pdfSection.PageInfo.Margin = new MarginInfo() { Top = 0, Bottom = 0, Left = 0, Right = 0 };

            switch (paperSize)
            {
                case PrintTemplatePaperSizeType.A3:
                    pdfSection.PageInfo.PageWidth = Aspose.Pdf.Generator.PageSize.A3Width;
                    pdfSection.PageInfo.PageHeight = Aspose.Pdf.Generator.PageSize.A3Height;
                    break;

                case PrintTemplatePaperSizeType.A4:
                    pdfSection.PageInfo.PageWidth = Aspose.Pdf.Generator.PageSize.A4Width;
                    pdfSection.PageInfo.PageHeight = Aspose.Pdf.Generator.PageSize.A4Height;
                    break;

                case PrintTemplatePaperSizeType.A5:
                    pdfSection.PageInfo.PageWidth = Aspose.Pdf.Generator.PageSize.A5Width;
                    pdfSection.PageInfo.PageHeight = Aspose.Pdf.Generator.PageSize.A5Height;
                    break;

                case PrintTemplatePaperSizeType.B4:
                    //B4 Size - 250 × 353mm
                    pdfSection.PageInfo.PageWidth = 708.75F;
                    pdfSection.PageInfo.PageHeight = 1000.75F;
                    break;

                case PrintTemplatePaperSizeType.B5:
                    pdfSection.PageInfo.PageWidth = Aspose.Pdf.Generator.PageSize.B5Width;
                    pdfSection.PageInfo.PageHeight = Aspose.Pdf.Generator.PageSize.B5Height;
                    break;

                case PrintTemplatePaperSizeType.Executive:
                    //Executive Size - 184 × 267
                    pdfSection.PageInfo.PageWidth = 521.64F;
                    pdfSection.PageInfo.PageHeight = 756.94F;
                    break;

                case PrintTemplatePaperSizeType.Legal:
                    pdfSection.PageInfo.PageWidth = Aspose.Pdf.Generator.PageSize.LegalWidth;
                    pdfSection.PageInfo.PageHeight = Aspose.Pdf.Generator.PageSize.LegalHeight;
                    break;

                case PrintTemplatePaperSizeType.Letter:
                    pdfSection.PageInfo.PageWidth = Aspose.Pdf.Generator.PageSize.LetterWidth;
                    pdfSection.PageInfo.PageHeight = Aspose.Pdf.Generator.PageSize.LetterHeight;
                    break;

                default:
                    Debug.Fail("PrintTemplatePaperSizeType value not recognised");
                    pdfSection.PageInfo.PageWidth = Aspose.Pdf.Generator.PageSize.LetterWidth;
                    pdfSection.PageInfo.PageHeight = Aspose.Pdf.Generator.PageSize.LetterHeight;
                    break;
            }

            if (isLandscape)
            {
                Single width = pdfSection.PageInfo.PageHeight;
                Single height = pdfSection.PageInfo.PageWidth;

                pdfSection.PageInfo.PageWidth = width;
                pdfSection.PageInfo.PageHeight = height;
            }
        }

        /// <summary>
        /// Adds a new section to the end of the given pdfdoc which is a copy of the last one.
        /// </summary>
        /// <param name="pdfDoc"></param>
        public static Section AppendNewSection(Pdf pdfDoc)
        {
            Section newSect = new Section(pdfDoc);

            if (pdfDoc.Sections.Count > 0)
            {
                Section lastSect = pdfDoc.Sections[pdfDoc.Sections.Count - 1];

                newSect.PageInfo.PageWidth = lastSect.PageInfo.PageWidth;
                newSect.PageInfo.PageHeight = lastSect.PageInfo.PageHeight;
                newSect.PageInfo.Margin = lastSect.PageInfo.Margin;
            }

            pdfDoc.Sections.Add(newSect);

            return newSect;
        }

        /// <summary>
        /// Returns the true positional rect for the given component
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public static Rect GetComponentRect(PrintTemplateComponent component)
        {
            //the number of px per mm - this seems really dodgey!!
            //A4 landscape page is 297mm X 210mm 
            //Aspose give this in pixels as 842.0 x 595.0 - 2.8px per mm

            Double pxToMM = (PageSize.A4Height / 297);

            return new Rect(
                component.X * pxToMM, component.Y * pxToMM,
                component.Width * pxToMM, component.Height * pxToMM);
        }

        /// <summary>
        /// Creates a floating box based on the given component.
        /// </summary>
        /// <param name="component"></param>
        /// <returns></returns>
        public static FloatingBox GetFloatingBox(PrintTemplateComponent component)
        {
            return GetFloatingBox(GetComponentRect(component));
        }

        /// <summary>
        /// Creates a floating box based on the given dimensions.
        /// </summary>
        /// <param name="componentRect">A rectangle describing the size and position of the box to be created</param>
        /// <returns></returns>
        public static FloatingBox GetFloatingBox(Rect componentRect)
        {
            FloatingBox positionalBox = new FloatingBox();
            positionalBox.Margin = new MarginInfo() { Left = 0, Right = 0, Top = 0, Bottom = 0 };
            positionalBox.BoxHorizontalPositioning = BoxHorizontalPositioningType.Page;
            positionalBox.BoxVerticalPositioning = BoxVerticalPositioningType.Page;
            positionalBox.Left = (Single)componentRect.X;
            positionalBox.Top = (Single)componentRect.Y;
            positionalBox.BoxWidth = (Single)componentRect.Width;
            positionalBox.BoxHeight = (Single)componentRect.Height;

            positionalBox.Border = new BorderInfo((Int32)BorderSide.None);
            //positionalBox.Border = new BorderInfo((Int32)BorderSide.All, 0.1F, new Aspose.Pdf.Generator.Color("LightGray"));

            return positionalBox;
        }

        /// <summary>
        /// Creates a Byte array from a Stream
        /// </summary>
        /// <param name="dataStream">Stream to read</param>
        /// <param name="CloseStream">Closes the Stream once finished if True.</param>
        /// <returns>Array of Bytes</returns>
        public static Byte[] CreateImageBlob(Stream dataStream, Boolean CloseStream)
        {
            Byte[] blob = new Byte[dataStream.Length];
            //Ensure stream is at the start
            dataStream.Position = 0;

            //Stream read does not necessarily read all the bytes asked for so
            //a loop is required to ensure all bytes are read.
            for (Int32 pos = 0; pos < dataStream.Length; )
            {
                Int32 len = dataStream.Read(blob, pos, (Int32)dataStream.Length - pos);
                if (len == 0)
                {
                    break;
                }
                pos += len;

            }

            if (CloseStream) dataStream.Close();
            return blob;
        }

        /// <summary>
        /// Converts the given integer to a color value.
        /// </summary>
        public static Color IntToAsposeColor(Int32 intValue, Boolean forceAlpha = false)
        {
            Int32 colorAsInt = intValue;

            Color returnColour = new Color
                (
                    Clamp((Byte)((colorAsInt >> 16) & 0xff)),
                    Clamp((Byte)((colorAsInt >> 8) & 0xff)),
                    Clamp((Byte)((colorAsInt) & 0xff))
                );

            return returnColour;
        }

        /// <summary>
        /// Clamps the supplied value bettween 0 and 255
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private static Byte Clamp(Single value)
        {
            Single output = Math.Max(value, 0);
            output = Math.Min(value, 255);
            return (Byte)(output);
        }

        #endregion

        #region Component Helpers

        /// <summary>
        /// Adds the textbox component to the given section
        /// </summary>
        /// <param name="pdfSection"></param>
        /// <param name="printComponent"></param>
        public static Text AddTextBoxComponent(Section pdfSection, PrintTemplateComponent printComponent, String resolvedText)
        {
            PrintTemplateComponentDetail componentDetail = printComponent.ComponentDetails;

            //create the position box
            FloatingBox positionalBox = ReportHelper.GetFloatingBox(printComponent);
            pdfSection.Paragraphs.Add(positionalBox);

            //create the text
            Aspose.Pdf.Generator.TextInfo txtInfo = new TextInfo();
            txtInfo.FontSize = componentDetail.TextBoxFontSize;
            txtInfo.IsUnderline = componentDetail.TextBoxIsFontUnderlined;
            txtInfo.IsTrueTypeFontBold = componentDetail.TextBoxIsFontBold;
            txtInfo.IsTrueTypeFontItalic = componentDetail.TextBoxIsFontItalic;
            txtInfo.Color = IntToAsposeColor(componentDetail.TextBoxForeground);
            txtInfo.FontName = componentDetail.TextBoxFontName;
            txtInfo.IsUnicode = true;

            switch (componentDetail.TextBoxTextAlignment)
            {
                case TextAlignment.Center:
                    txtInfo.Alignment = AlignmentType.Center;
                    break;

                case TextAlignment.Left:
                    txtInfo.Alignment = AlignmentType.Left;
                    break;

                case TextAlignment.Justify:
                    txtInfo.Alignment = AlignmentType.Justify;
                    break;

                case TextAlignment.Right:
                    txtInfo.Alignment = AlignmentType.Right;
                    break;
            }

            Aspose.Pdf.Generator.Text txt = new Text(resolvedText);
            txt.TextInfo = txtInfo;
            positionalBox.Paragraphs.Add(txt);

            //add padding because this is for text.
            Single mrg = 11.32F;
            txt.Margin = new MarginInfo() { Top = mrg, Bottom = mrg, Left = mrg, Right = mrg };

            return txt;
        }

        /// <summary>
        /// Adds the line component to the given section.
        /// </summary>
        /// <param name="pdfSection"></param>
        /// <param name="printComponent"></param>
        public static void AddLineComponent(Section pdfSection, PrintTemplateComponent printComponent)
        {
            PrintTemplateComponentDetail componentDetail = printComponent.ComponentDetails;

            Double pxToMM = (PageSize.A4Height / 297);
            Single x1 = (Single)Math.Round(printComponent.X * pxToMM, 0);
            Single x2 = (Single)Math.Round(componentDetail.LineEndX * pxToMM, 0);
            Single y1 = (Single)Math.Round(printComponent.Y * pxToMM, 0);
            Single y2 = (Single)Math.Round(componentDetail.LineEndY * pxToMM, 0);


            //create the position box
            FloatingBox positionalBox = ReportHelper.GetFloatingBox(printComponent);

            positionalBox.Left = Math.Min(x1, x2);
            positionalBox.BoxWidth = (Single)Math.Max((Math.Max(x1, x2) - positionalBox.Left), componentDetail.LineWeight);

            positionalBox.Top = Math.Min(y1, y2);
            positionalBox.BoxHeight = (Single)Math.Max((Math.Max(y1, y2) - positionalBox.Top), componentDetail.LineWeight);

            pdfSection.Paragraphs.Add(positionalBox);

            //create the graph
            Graph drawing = new Graph(positionalBox.BoxWidth, positionalBox.BoxHeight);
            positionalBox.Paragraphs.Add(drawing);

            //draw the line
            Single[] linecoords =
                new Single[] 
                    { 
                        x1 - positionalBox.Left, 
                        (positionalBox.Top + positionalBox.BoxHeight) - y1, 
                        x2 - positionalBox.Left, 
                        (positionalBox.Top + positionalBox.BoxHeight) - y2,
                    };
            for (Int32 i = 0; i < linecoords.Count(); i++)
            {
                if (linecoords[i] < 0) linecoords[i] = 0;
            }


            Line line = new Line(drawing, linecoords);
            line.GraphInfo.Color = IntToAsposeColor(componentDetail.LineForeground);
            line.GraphInfo.LineWidth = (Single)componentDetail.LineWeight;

            drawing.Shapes.Add(line);
        }

        /// <summary>
        /// Adds a table component to the given section.
        /// This may also cause new sections to be created.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="pdfDoc"></param>
        /// <param name="startSectionIdx"></param>
        /// <param name="printComponent"></param>
        /// <param name="cellTextInfo"></param>
        /// <param name="style"></param>
        /// <returns>A dictionary of section indexes to positional rects for placed tables.</returns>
        public static void AddTableComponent(ReportTable data, Pdf pdfDoc, Int32 startSectionIdx, Rect sectionRect)
        {
            var shadedHeaderBackground = new Aspose.Pdf.Generator.Color(152, 175, 202);
            var alternatingRowBackground = new Aspose.Pdf.Generator.Color(237, 243, 252);
            var cellBorderColor =  new Aspose.Pdf.Generator.Color(132, 160, 191);
            var defaultCellBorder = new Aspose.Pdf.Generator.BorderInfo((Int32)Aspose.Pdf.Generator.BorderSide.All, 0.1F,cellBorderColor);

            float rowHeight =0;
            float rowHeightTotal = 0;
            float rowHeightAverage = 0;
            int rowHeightCount = 0;
            
            //if the table has no columns just return out.
            if (data.Columns.Count == 0) { return; }

            //if we have no rows then add a blank one so that the column headers get drawn.
            if (data.Rows.Count == 0)
            {
                data.Rows.Add(null);
            }
            else
            {
                //determine column cell alignments from the first row.
                ReportTableRow firstRow = data.Rows.FirstOrDefault(r => r.RowType == ReportTableRow.ReportTableRowType.Normal);
                if (firstRow != null)
                {
                    for (Int32 i = 0; i < data.Columns.Count; i++)
                    {
                        AlignmentType alignment = AlignmentType.Left;

                        Object cellValue = firstRow.ItemArray[i];

                        if (cellValue is String)
                        {
                            alignment = Aspose.Pdf.Generator.AlignmentType.Left;
                        }
                        if (cellValue is Int32 || cellValue is Int32?
                            || cellValue is Int16 || cellValue is Int16?
                            || cellValue is Int64 || cellValue is Int64?
                            || cellValue is Byte || cellValue is Byte?
                            || cellValue is Single || cellValue is Single?)
                        {
                            alignment = Aspose.Pdf.Generator.AlignmentType.Right;
                        }
                        else if (cellValue is Double || cellValue is Double?)
                        {
                            alignment = Aspose.Pdf.Generator.AlignmentType.Right;
                            data.Columns[i].StringFormat = "0.00"; //force to 2 dp.
                        }
                        else
                        {
                            alignment = Aspose.Pdf.Generator.AlignmentType.Left;
                        }

                        data.Columns[i].Alignment = alignment;
                    }
                }
            }

            //for performance keep this up here as will stay the same for each page.
            Double headerRowHeight = 0;
            
            //Create the tables
            List<ReportTableRow> remainingRows = new List<ReportTableRow>();
            foreach (ReportTableRow row in data.Rows) remainingRows.Add(row);

            Int32 currSectionIdx = startSectionIdx;
            Boolean shouldAlternate = (data.Style == PrintTemplateComponentGridStyle.Standard
                    || data.Style == PrintTemplateComponentGridStyle.Full
                    || data.Style == PrintTemplateComponentGridStyle.Light);
            Boolean isAlternatingRow = false;

            while (remainingRows.Count > 0)
            {
                if (pdfDoc.Sections.Count < (currSectionIdx + 1))
                {
                    AppendNewSection(pdfDoc);
                }
                Section currentSection = pdfDoc.Sections[currSectionIdx];

                //create the position box
                Rect positionRect = sectionRect;
                FloatingBox positionalBox = ReportHelper.GetFloatingBox(positionRect);
                currentSection.Paragraphs.Add(positionalBox);

                //create the table
                Aspose.Pdf.Generator.Table pdfTable = new Aspose.Pdf.Generator.Table();
                pdfTable.DefaultCellTextInfo = data.CellTextInfo;
                pdfTable.DefaultCellPadding = new MarginInfo() { Left = 1, Right = 1, Bottom = 2, Top = 2 };
                pdfTable.DefaultCellBorder = defaultCellBorder;
                positionalBox.Paragraphs.Add(pdfTable);

                //set the column widths
                Int32 colAutoWidth = (Int32)(positionRect.Width - data.Columns.Where(c => c.Width > 0).Sum(c => c.Width)) / Math.Max(1, data.Columns.Count(c => c.Width == 0));
               
                
                String colWidths = String.Empty;
                foreach (ReportTableColumn col in data.Columns)
                {
                    Int32 colWidth = (col.Width != 0) ? col.Width : colAutoWidth;
                    colWidths = colWidths + colWidth.ToString() + " ";
                }
                colWidths.Trim();
                pdfTable.ColumnWidths = colWidths;

                //render the table header row
                Aspose.Pdf.Generator.Row headerRow = pdfTable.Rows.Add();
                headerRow.DefaultCellTextInfo = data.HeaderTextInfo;
                headerRow.DefaultRowCellPadding = new MarginInfo() { Left = 2, Right = 2, Bottom = 2, Top = 2 };
                headerRow.VerticalAlignment = VerticalAlignmentType.Center;
                foreach (ReportTableColumn col in data.Columns)
                {
                    Aspose.Pdf.Generator.Cell headerCell = headerRow.Cells.Add(col.ColumnName);
                    headerCell.Alignment = AlignmentType.Left;
                }
                if (data.Style == PrintTemplateComponentGridStyle.Standard || data.Style == PrintTemplateComponentGridStyle.Full)
                {
                    headerRow.BackgroundColor = shadedHeaderBackground;
                }
                
                //calculate average row height
                rowHeightAverage = (rowHeightTotal == 0 ? 50 : rowHeightTotal / rowHeightCount == 0 ? 1 : rowHeightCount);
                //for speed - guess whether there is a chance this table might need to break across pages.
                Boolean isLikelyToBreak = (positionalBox.BoxHeight / remainingRows.Count + 1) < rowHeightAverage;

                //render the data rows
                Double tableHeight = 0;

                //if the table is likely to break and we haven't
                // already done so then measure the header row height.
                if (isLikelyToBreak && headerRowHeight == 0)
                {
                    headerRowHeight = headerRow.GetHeight(pdfDoc);
                }

                while ((tableHeight < positionalBox.BoxHeight) && remainingRows.Count > 0)
                {
                    bool containsSymbol = false;
                    ReportTableRow nextRow = remainingRows.First();

                    if (nextRow != null)
                    {
                        Aspose.Pdf.Generator.Row dataRow = pdfTable.Rows.Add();
                        dataRow.VerticalAlignment = VerticalAlignmentType.Center;

                        //add cells
                        if (nextRow.RowType == ReportTableRow.ReportTableRowType.GroupHeader)
                        {
                            //merge into a single row
                            var dataCell = dataRow.Cells.Add(Convert.ToString(nextRow.ItemArray[0]));
                            dataCell.Alignment = AlignmentType.Left;
                            dataCell.ColumnsSpan = data.Columns.Count;

                            if (data.Style == PrintTemplateComponentGridStyle.Standard || data.Style == PrintTemplateComponentGridStyle.Full)
                            {
                                dataCell.BackgroundColor = shadedHeaderBackground;
                            }
                        }
                        else if (nextRow.RowType == ReportTableRow.ReportTableRowType.Blank)
                        {
                            var dataCell = dataRow.Cells.Add();
                            dataCell.ColumnsSpan = data.Columns.Count;
                            dataCell.Border = new BorderInfo((Int32)BorderSide.None);
                        }
                        else
                        {
                            for (Int32 i = 0; i < nextRow.ItemArray.Count(); i++)
                            {
                                Object val = nextRow.ItemArray[i];
                                ReportTableColumn col = data.Columns[i];
                                    
                                String stringVal = col.GetCellString(val);

                                //does the cell data contain a symbol
                                if (!containsSymbol)
                                {
                                    for (int symbolCheck = 0; symbolCheck < stringVal.Length; symbolCheck++)
                                    {
                                        containsSymbol = char.IsSymbol(stringVal, symbolCheck);
                                    }                                    
                                }

                                var dataCell = dataRow.Cells.Add(stringVal);
                                dataCell.Alignment = col.Alignment;
                                
                                if (nextRow.RowType == ReportTableRow.ReportTableRowType.Totals)
                                {
                                    dataCell.Border = new BorderInfo((Int32)BorderSide.Top, 0.1F, cellBorderColor);
                                }
                                else if (shouldAlternate && isAlternatingRow)
                                {
                                    //alternate row background
                                    dataCell.BackgroundColor = alternatingRowBackground;
                                }
                            }

                            //flip alternating flag.
                            isAlternatingRow = !isAlternatingRow;
                        }
                        
                        if (isLikelyToBreak)
                        {
                            //add the header row height on if needed
                            if (tableHeight == 0)
                            {
                                tableHeight = headerRowHeight;
                            }

                            //get height caused an infinite loop when the datarow contained an unmapped character \ replacement character �.
                            //if the row data contains a symbol use the average row height.
                            if (containsSymbol)
                            {
                                rowHeight = rowHeightAverage;
                            }
                            else
                            {
                                rowHeight = dataRow.GetHeight(pdfDoc);
                            }
                                  
                            //increment row counter and calculate the row and table heights.
                            rowHeightTotal += rowHeight;                            
                            tableHeight += rowHeight;
                            rowHeightCount++;

                            //check if the table has broken the available height constraint.
                            if (tableHeight > positionalBox.BoxHeight
                                && pdfTable.Rows.Count > 2 /*so we dont infinate loop*/)
                            {
                                //remove the last row and break out.
                                pdfTable.Rows.Remove(dataRow);
                                break;
                            }
                            else
                            {
                                remainingRows.Remove(nextRow);
                            }
                        }
                        else
                        {
                            remainingRows.Remove(nextRow);
                        }
                    }
                    else { remainingRows.Remove(nextRow); }
                }
                
                currSectionIdx++;
            }
        }

        #endregion

        #region Data Helpers

        /// <summary>
        /// Returns the friendly name for the given enum value
        /// </summary>
        /// <param name="enumValue"></param>
        /// <param name="propertyType"></param>
        /// <returns></returns>
        public static String GetEnumFriendlyName(Object enumValue, Type propertyType)
        {
            Object returnValue = null;

            if (enumValue != null)
            {
                //if the property is an enum use reflection to get its friendly name
                if (propertyType.IsEnum)
                {
                    Type enumHelperClassType = Type.GetType("Galleria.Ccm.Model." + propertyType.Name + "Helper");
                    if (enumHelperClassType != null)
                    {
                        //get the friendlyNames dict
                        FieldInfo friendlyNamesInf = enumHelperClassType.GetField("FriendlyNames");
                        if (friendlyNamesInf != null)
                        {
                            Object dict = friendlyNamesInf.GetValue(null);
                            if (dict != null)
                            {
                                MethodInfo getFriendlyNameInf = dict.GetType().GetMethod("get_Item");
                                if (getFriendlyNameInf != null)
                                {
                                    try
                                    {
                                        returnValue = getFriendlyNameInf.Invoke(dict, new Object[] { enumValue });
                                    }
                                    catch (Exception) { }
                                }
                            }
                        }

                    }
                }
            }

            String friendlyName = (returnValue != null) ? returnValue.ToString() : null;

            return friendlyName;
        }

        /// <summary>
        /// Resolves page numbering placeholders.
        /// </summary>
        /// <param name="pdfDoc">The document to resolve for</param>
        /// <param name="textComponents">Text components to be processed</param>
        /// <remarks>Am forced to do this because the aspose totalpages holder is not working as required when chaining reports together.</remarks>
        public static void ResolvePageNumbering(Pdf pdfDoc, IEnumerable<Aspose.Pdf.Generator.Text> textComponents)
        {
            if (pdfDoc != null && pdfDoc.Sections.Count > 0)
            {
                //restart numbering on the first section
                pdfDoc.Sections[0].IsPageNumberRestarted = true;


                //resolve all total pages placeholders.
                Int32 totalPageCount = pdfDoc.Sections.Count;
                foreach (Aspose.Pdf.Generator.Text txt in textComponents)
                {
                    if (txt != null)
                    {
                        for (Int32 i = 0; i < txt.Segments.Count; i++)
                        {
                            String segValue = txt.Segments[0].Content as String;
                            if (!String.IsNullOrEmpty(segValue))
                            {
                                txt.Segments[0].Content = segValue.Replace("$P", totalPageCount.ToString());
                            }
                        }
                    }
                }
            }

        }

        #endregion

    }
}
