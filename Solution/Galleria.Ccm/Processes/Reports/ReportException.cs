﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Copied from GFS.
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Ccm.Processes.Reports
{
    /// <summary>
    /// A custom exception that is thrown when an exception occurs
    /// inside a report
    /// </summary>
    public class ReportException : Exception
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="message">The event log message</param>
        public ReportException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="message">The event log message</param>
        /// <param name="innerException">The inner exception</param>
        public ReportException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="innerException">The inner exception</param>
        public ReportException(Exception innerException)
            : base(innerException.Message, innerException)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="innerException">The inner exception</param>
        public ReportException(Object innerException)
            : this((Exception)innerException)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="info">The serialization info</param>
        /// <param name="context">The serialization context</param>
        protected ReportException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion
    }
}
