﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Copied from GFS.
#endregion
#endregion

using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;


namespace Galleria.Ccm.Processes.Reports
{
    internal sealed class ReportImage
    {
        #region Properties

        public Int32 Height { get; private set; }
        public Int32 Width { get; private set; }
        public Byte[] Data { get; private set; }

        #endregion

        #region Constructors

        public ReportImage(Int32 height, Int32 width, Visual imageControl)
        {
            this.Height = height;
            this.Width = width;
            this.Data = CreateImage(height, width, imageControl);
        }

        #endregion

        #region Methods

        private static Byte[] CreateImage(Int32 imgHeight, Int32 imgWidth, Visual render)
        {
            //render

            //This vers is meant to ensure that transforms are drawn correctly however I have commented it out for now as when
            // using it I can see no benefit. In fact it makes text even blurrier.
            //VisualBrush vb = new VisualBrush(render);
            //DrawingVisual dv = new DrawingVisual();
            //using (DrawingContext dc = dv.RenderOpen())
            //{
            //    dc.DrawRectangle(vb, null, new Rect(new System.Windows.Point(0, 0), new System.Windows.Point(imgWidth, imgHeight)));
            //}
            //RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap(imgWidth, imgHeight, 96, 96, PixelFormats.Pbgra32);
            // renderTargetBitmap.Render(dv);


            // Scale dimensions from 96 dpi to get better quality
            Double scale = 200 / 96;


            //perform a straight render.
            RenderTargetBitmap renderTargetBitmap =
                new RenderTargetBitmap(
                (Int32)(scale * (imgWidth + 1)),
                (Int32)(scale * (imgHeight + 1)),
                scale * 96,
                scale * 96,
                PixelFormats.Default);
            renderTargetBitmap.Render(render);

            // encode
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
                encoder.Save(memoryStream);

                // Export the stream to a byte array
                Byte[] renderBytes = ReportHelper.CreateImageBlob(memoryStream, true);

                // Shutdown rendering thread (if alive)
                if (renderTargetBitmap.Dispatcher.Thread.IsAlive)
                {
                    renderTargetBitmap.Dispatcher.InvokeShutdown();
                }

                return renderBytes;
            }
        }


        #endregion
    }
}
