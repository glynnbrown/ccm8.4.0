﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Copied from GFS.
#endregion
#endregion


using System;
using Aspose.Pdf.Generator;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Processes.Reports
{
    public interface IReportProcess : IProcess
    {
        #region Properties
        /// <summary>
        /// Gets the output report
        /// </summary>
        Byte[] ReportData { get; }
        #endregion
    }
}
