using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Processes.ProductAttributeValidation
{
    public interface IAttributeValidationEngine
    {
        void ValidateProductAttributes(IProductAttributeValidationWarning sourceObject, Entity entity);
    }
}