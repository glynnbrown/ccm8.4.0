using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Processes.ProductAttributeValidation
{
    /// <summary>
    /// Class that implements a Product Attribute Validation mechanism; used for comparing product attributes against the masterdata using a predefined comparison attribute template.
    /// </summary>
    public class AttributeValidationEngine : IAttributeValidationEngine
    {
        /// <summary>
        /// Validation method that compares the products against the porducts in the masterdata using the product attributes in the Entity object.
        /// </summary>
        /// <param name="planogram">Current planogram to check</param>
        /// <param name="entity">Current entity that holds the attribute comparison template</param>
        
        public void ValidateProductAttributes(IProductAttributeValidationWarning source, Entity entity)
        {
            var gtinsToProcess = source.GetProductsToValidateAttributesFor().Select(p => p.Gtin).ToList();

            // Get the product record for each GTIN from the master data
            ProductList masterDataProducts = ProductList.FetchByEntityIdProductGtins(entity.Id,
                gtinsToProcess);

            source.ClearProductAttributeValidationWarnings();
            foreach (PlanogramProduct product in source.GetProductsToValidateAttributesFor())
            {
                Product masterDataProduct = masterDataProducts.FirstOrDefault(prod => prod.Gtin == product.Gtin);
                if (masterDataProduct == null)
                {
                    continue;
                }

               CheckProductAttributes(source, product, masterDataProduct, entity.ComparisonAttributes);
            }
        }

        private void CheckProductAttributes(IProductAttributeValidationWarning source,  PlanogramProduct product, Product masterDataProduct, EntityComparisonAttributeList validationAttributeTemplate)
        {
            foreach (EntityComparisonAttribute attribute in validationAttributeTemplate)
            {
                String masterDataValue = String.Empty, productValue = String.Empty;

                if (
                    !ProductAttributeComparisonHelper.TryGetPropertyValueAsString(masterDataProduct, attribute.PropertyName,
                        out masterDataValue))
                    continue;

                ProductAttributeComparisonHelper.TryGetPropertyValueAsString(product, attribute.PropertyName, out productValue);

                Boolean match = productValue.Equals(masterDataValue);
                if (!match) {
                    source.AddProductAttributeValidationWarning(product.Gtin, 
                        !String.IsNullOrEmpty(attribute.PropertyDisplayName) ? attribute.PropertyDisplayName : attribute.PropertyName, 
                        masterDataValue, 
                        productValue);
                }

            }
        }
    }

    /// <summary>
    /// Helper class used for extracting a 
    /// </summary>
    internal class ProductAttributeComparisonHelper
    {
        private const String ProductAttributePropertyNameRegexString = @"[\[\]']+";

        private static String _ProductAttributePropertyNameRegexString;
        private const String ProductType = "Product";
        private const String PlanogramproductType = "PlanogramProduct";

        public static Boolean TryGetPropertyValueAsString(Product sourceObject, String propertyName, out String valueToReturn)
        {
            return GetPropertyFromObjectPropertyValueAsString(sourceObject, propertyName, out valueToReturn);
        }

        public static Boolean TryGetPropertyValueAsString(PlanogramProduct sourceObject, String propertyName, out String valueToReturn)
        {
            return GetPropertyFromObjectPropertyValueAsString(sourceObject, propertyName, out valueToReturn);
        }

        private static Boolean GetPropertyFromObjectPropertyValueAsString(object sourceObject, String propertyToInspect, out String valueToReturn)
        {
            valueToReturn = String.Empty;
            if (sourceObject == null) return false;

            return GetProductProperty(sourceObject, propertyToInspect, out valueToReturn);
        }

        
       private static Boolean GetProductProperty(object sourceObject, String propertyToInspect, out String valueToReturn)
        {
           valueToReturn = String.Empty;
           Object value = null;

           String[] propertyNames = Regex.Replace(propertyToInspect, ProductAttributePropertyNameRegexString, String.Empty).Split('.');
           if (!ObjectFromString(sourceObject, propertyNames, out value)) return false;

           valueToReturn = value?.ToString();
           return true;
        }
        
        private static Boolean ObjectFromString(object basePoint, IEnumerable<string> pathToSearch, out Object valueToReturn)
        {
            valueToReturn = null;
            var currentObject = basePoint;
            foreach (var propertyName in pathToSearch)
            {
                if (propertyName.Equals(PlanogramproductType, StringComparison.OrdinalIgnoreCase) || propertyName.Equals(ProductType, StringComparison.OrdinalIgnoreCase)) continue;

                var property = currentObject.GetType().GetProperty(propertyName);
                if (property == null) return false;

                valueToReturn = property.GetValue(currentObject, null);
            }
            return true;
        }
    }
}