﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Processes
{
    /// <summary>
    /// An exception that may be raised when running a process
    /// </summary>
    [Serializable]
    public class ProcessException : Galleria.Framework.Processes.ProcessException
    {
        #region Fields
        private Int32 _entityId; // the current entity id
        private EventLogEvent _eventId = EventLogEvent.UnexpectedException; // the event log id
        #endregion

        #region Properties
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return _entityId; }
        }

        /// <summary>
        /// The event id
        /// </summary>
        public EventLogEvent EventId
        {
            get { return _eventId; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="message">The event log message</param>
        public ProcessException(String message)
            : base(message)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="message">The event log message</param>
        /// <param name="innerException">The inner exception</param>
        public ProcessException(String message, Exception innerException)
            : base(message, innerException)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="info">The serialization info</param>
        /// <param name="context">The serialization context</param>
        protected ProcessException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="entityId">The entity id</param>
        /// <param name="entryType">The event log entry type</param>
        /// <param name="eventId">The event id</param>
        /// <param name="message">The event message</param>
        public ProcessException(Int32 entityId, EventLogEvent eventId, String message) :
            base(message)
        {
            _entityId = entityId;
            _eventId = eventId;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="eventId">The event id</param>
        /// <param name="innerException">The inner exception</param>
        public ProcessException(EventLogEvent eventId, Exception innerException) :
            base(innerException.Message, innerException)
        {
            _eventId = eventId;
        }
        #endregion
    }
}
