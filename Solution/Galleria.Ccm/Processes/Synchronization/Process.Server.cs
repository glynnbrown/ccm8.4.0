﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Processes;
using Csla;
using Galleria.Ccm.Helpers;
using System.Diagnostics;

namespace Galleria.Ccm.Processes.Synchronization
{
    public partial class Process
    {
        #region Constants
        //if need to change, then also needs changing in SynchroniseTarget.Process.Server
        int _syncTargetMaxRetryCount = 3; // maximum retry count per sync target per schedule
        int _syncTargetMaxFailedCount = 5; // maximum failed count before a sync target errors and stops trying to sync.
        #endregion

        #region Methods
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            // set the sync source
            IEnumerable<SyncSource> syncSourceList;
            SyncSource syncSource = null;
            IEnumerable<SyncTarget> syncTargetList = null;

            try
            {
                // fetch all sources
                syncSourceList = SyncSourceList.GetAllSyncSources();

                // get the sync source
                syncSource = syncSourceList.FirstOrDefault();

                // fetch all the current configured synchronisation targets that are active
                syncTargetList = SyncTargetList.GetAllSyncTargets(false).Where(i => i.IsActive == true);
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }

            if (syncTargetList != null && syncTargetList.Any() && syncSource != null)
            {
                // define a variable that will hold the sync target
                // that we are going to sync as part of this process
                SyncTarget syncTargetItem = null;

                // first, search for the first sync target that has never
                // been synchronised
                syncTargetItem = syncTargetList.FirstOrDefault(o => o.LastRun == null);

                // sort the sync targets based on their last sync date
                syncTargetList = syncTargetList.OrderBy(i => i.LastSync);

                // now find the first item in this list
                // which is due to be synchronised
                foreach (SyncTarget syncTarget in syncTargetList)
                {
                    try
                    {
                        // ensure that we do not try and re-run errored processes more than the maximum retry count (currently set to 3)
                        if (syncTarget.RetryCount < _syncTargetMaxRetryCount)
                        {
                            // if never been successful and still less than maximum retry count then try to sync
                            if (syncTarget.LastSync != null)
                            {
                                // determine if the current time is now past the
                                // next sync date for the current sync target
                                DateTime lastSync = (DateTime)syncTarget.LastSync;
                                DateTime nextSync = lastSync.AddMinutes(SyncTargetFrequencyHelper.SyncTargetFrequencyGetValue(syncTarget.Frequency));
                                if (DateTime.UtcNow > nextSync)
                                {
                                    // if there is not already a selected sync target then set it, 
                                    // else set the status to queued
                                    if (syncTargetItem == null)
                                    {
                                        syncTargetItem = syncTarget;
                                    }
                                    else
                                    {
                                        SyncTarget syncTargetUpdate = SyncTarget.GetSyncTargetById(syncTarget.Id);
                                        syncTargetUpdate.Status = SyncTargetStatus.Queued;
                                        syncTargetUpdate.Save();
                                    }
                                }
                            }
                            else
                            {
                                // if there is not already a selected sync target then set it, 
                                // else set the status to queued
                                if (syncTargetItem == null)
                                {
                                    syncTargetItem = syncTarget;
                                }
                                else
                                {
                                    SyncTarget syncTargetUpdate = SyncTarget.GetSyncTargetById(syncTarget.Id);
                                    syncTargetUpdate.Status = SyncTargetStatus.Queued;
                                    syncTargetUpdate.Save();
                                }
                            }
                        }
                        else
                        {
                            // see if the sync target has failed less than the maximum allowed (currently set to 5)
                            if (syncTarget.FailedCount < _syncTargetMaxFailedCount)
                            {
                                // determine if the current time is now past the
                                // next sync date for the current sync target
                                DateTime lastRun = (DateTime)syncTarget.LastRun;
                                DateTime nextSync = lastRun.AddMinutes(SyncTargetFrequencyHelper.SyncTargetFrequencyGetValue(syncTarget.Frequency));
                                if (DateTime.UtcNow > nextSync)
                                {
                                    //reset the retry count as it has reached it's scheduled sync time
                                    //and is currently under the maximum amount of failures.
                                    SyncTarget syncTargetUpdate = SyncTarget.GetSyncTargetById(syncTarget.Id);
                                    syncTargetUpdate.RetryCount = 0;

                                    // if there is not already a selected sync target then set it, 
                                    // else set the status to queued
                                    if (syncTargetItem == null)
                                    {
                                        syncTargetUpdate.Save();

                                        //try to sync again
                                        syncTargetItem = syncTargetUpdate;
                                    }
                                    else
                                    {
                                        syncTargetUpdate.Status = SyncTargetStatus.Queued;
                                        syncTargetUpdate.Save();
                                    }
                                }
                            }
                        }
                    }
                    catch (DataPortalException ex)
                    {
                        //Do nothing, should re-poll and update
                        syncTargetItem = null;

                        //log to gfs and Windows event log
                        GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
                    }

                    // if we have a sync target at this point,
                    // then perform the synchronisation
                    if (syncTargetItem != null)
                    {
                        SynchronizeTarget.Process process = new SynchronizeTarget.Process(syncTargetItem, syncSource);
                        ProcessFactory.Execute(process);
                    }
                }
            }
        }
        #endregion
    }
}