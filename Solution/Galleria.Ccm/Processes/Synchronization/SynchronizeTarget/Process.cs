﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using Csla;
using Galleria.Framework.Processes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services.Entity;

namespace Galleria.Ccm.Processes.Synchronization.SynchronizeTarget
{
    /// <summary>
    /// Process that performs the synchronization
    /// of a single target
    /// </summary>
    [Serializable]
    public partial class Process : ProcessBase<Process>
    {
        #region Properties
        /// <summary>
        /// The sync target id property
        /// </summary>
        private static readonly PropertyInfo<Int32> SyncTargetIdProperty =
            RegisterProperty<Int32>(c => c.SyncTargetId);
        public Int32 SyncTargetId
        {
            get { return ReadProperty<Int32>(SyncTargetIdProperty); }
        }

        /// <summary>
        /// The sync target property
        /// </summary>
        private static readonly PropertyInfo<SyncTarget> SyncTargetProperty =
            RegisterProperty<SyncTarget>(c => c.SyncTarget);
        public SyncTarget SyncTarget
        {
            get { return ReadProperty<SyncTarget>(SyncTargetProperty); }
            set { LoadProperty<SyncTarget>(SyncTargetProperty, value); }
        }

        /// <summary>
        /// The sync source property
        /// </summary>
        private static readonly PropertyInfo<SyncSource> SyncSourceProperty =
            RegisterProperty<SyncSource>(c => c.SyncSource);
        public SyncSource SyncSource
        {
            get { return ReadProperty<SyncSource>(SyncSourceProperty); }
            set { LoadProperty<SyncSource>(SyncSourceProperty, value); }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="syncTarget">The syncrhonisation target</param>
        public Process(SyncTarget syncTarget, SyncSource syncSource)
        {
            this.LoadProperty<Int32>(SyncTargetIdProperty, syncTarget.Id);
            this.LoadProperty<SyncTarget>(SyncTargetProperty, syncTarget);
            this.LoadProperty<SyncSource>(SyncSourceProperty, syncSource);
        }
        #endregion
    }
}