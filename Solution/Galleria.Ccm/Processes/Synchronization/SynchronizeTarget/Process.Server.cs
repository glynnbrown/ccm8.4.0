﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014
#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26041 : A.Kuszyk
//  Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
//  Changed use of CCM ProductOrientationType to Framework PlanogramProductOrientationType.
// V8-26236 : L.Ineson
//  Added creation of default plan hierarchy
// V8-26332 : L.Ineson
//  Added sync of custom attribute data for products.
// V8-26365 : L.Ineson
//  Updated so that mock services can be used in testing.
// V8-25556 : J.Pickup
//  Added Metric, LocationProductAttribute, LocationProductIllegal, Metric, Assortments, AssormentMinorRevision to the sync.
//  ConsumerDecisionTreeNode now holds the GFSId and is now populated in relevant sync process. 
// V8-27505 : J.Pickup
//  Product sync custom attributes rewritten for V8 to populate based on data type of from GFS custom product attribute.
// V8-27577 : J.Pickup
//  CCM ClusterScheme now syncs the resolved category id from GFS.
// V8-27411 : M.Pettit
//  CCM now stores last successful and failed sync dates in its System Settings against each entity
#endregion
#region Version History: (CCM 8.0.2)
// V8-28988 \ V8-29132 : D.Pleasance
//  Added functionality to create default Planogram Import Template.
// V8-29079 : L.Ineson
//  Amended assortment and assortment minor syncs to 
// find the existing ccm object using name instead of ucr.
#endregion
#region Version History: (CCM 8.0.3)
// V8-29215 : D.Pleasance
//  Added SynchronizeConsumerDecisionTreeDeleted.
//  Also reworked SynchronizeConsumerDecisionTree slightly so that root level isnt deleted if it exists, 
//  this is because with the changes to delete outright it expects at least one level to always exist. (this is required for UI purposes)
// V8-29134 : D.Pleasance
//  Added SynchronizeAssortmentsDeleted. 
// V8-29214 : D.Pleasance
//  Added SynchronizeAssortmentMinorRevisionsDeleted.
// V8-29268 : L.Ineson
//  Added SynchronizeLocationProductIllegalDeleted
// V8-29267 : L.Ineson
//  Added SynchronizeLocationProductDeleted
// V8-29257 : L.Ineson
//  Added SynchronizeProductUniverseDeleted
// V8-29223 : D.Pleasance
//  Added SynchronizeLocationSpaceDeleted
// V8-29627 : D.Pleasance
//  Fix applied to Convert Product CustomAttribute data rather than cast as this causes 'unboxing' issues.
#endregion
#region Version History: (CCM 8.1.0)
// V8-29498 : N.Haywood
//  Changed content synced from GFS to user ParentUniqueContentReference
// V8-29667 : N.Haywood
//  Removed product and location FetchAll()'s and replaced with entity specific methods
// V8-29809 : N.Haywood
//  Added communicated dates sync for plan assignment
// V8-29678 : D.Pleasance
//  Amended CreateDefaultPlanogramImportTemplateMappings, added missing default mappings
// V8-29946 : N.Haywood
//  Added default content for entity creation
// V8-30000 : L.Ineson
//  Added naming template defaults.
// V8-29936 : N.Haywood
//  Amended content syncs
// V8-30068 : M.Brumby
//  Default task lists are now all in one location and get sequenced
// V8-29593 : L.Ineson
//  Set default product fill colour to white.
// V8-30140 : N.Haywood
//  Amended gfs sync details
// V8-30154 : N.Haywood
//  Fixed location space bay count sync
// V8-30189 : N.Haywood
//  Fixed assortment minor revisions for when actions have been removed from gfs and resync'd
// V8-29122 : N.Haywood
//  Added assortment minor revision inserts for where we're adding new actions for pre-existing assortment minor revisions
// V8-30178 : D.Pleasance
//  Amended MakeWorkflowDtos so that WorkflowTaskParameterValues are only created if there is a DefaultValue against the task parameter.
// V8-30223 : N.Haywood
//  Fixed issue where product universes were not being updated
// V8-30194 : M.Shelley
//  Add default highlights and labels when a new entity is created.
// V8-30204 : N.Haywood
//  Removed un-needed null check in SynchroniseLocationPlanAssignmentDates
// V8-30237 : N.Haywood
//  Fixed name updates for cluster schemes, product universes and cdts
// V8-30276 : N.Haywood
//  Added temporary try catch to location product attribute delete sync and made it so it would actually work once a GFS change is done
// V8-30299 : N.Haywood
//  Fixed Location Product Illegal delete sync (multiple location/products being deleted at once)
#endregion
#region Version History: (CCM 8.1.1)
// V8-30431 : N.Haywood
//  Added logging of sync errors to v8
// V8-30541 : N.Haywood
//  Added date last modified for content
#endregion
#region Version History: (CCM 8.2.0)
// V8-30838 : N.Haywood
//  Fixed Location Product Attribute sync by making it sync in batches
// V8-30803/30804/30805 : A.Kuszyk
//  Added additional workflows.
// V8-30897/30898/30899/30900/30901/30902 : L.Luong
//  Changed default values for workflows.
// V8-30989 : A.Kuszyk
//  Added additional tasks to Merchandise workflows.
// V8-31057 - M.Brumby
//  PCR01417 Default Apollo Tempate
// V8-31076 : A.Probyn
//  Removed Merchandised Planogram from the default workflow tasks.
// V8-31073 : A.Silva
//  Amended CreateDefaultPlanogramImportTemplate to get the default max version and name for the required file types.
// V8-31111 : A.Silva
//  Added Default data creation for ProSpace Import Template.
// V8-31200 : A.Probyn
//  Added creation of default print template on entity sync.
// V8-31185 : J.Pickup
//  LocationPlanAssinment now only syncs if a publish has taken place and communicated content / live has been updated.
//  LocationPlanAssinment now pulls back content based on type so less unrequired information is retrived via web svc's.
// V8-31246 : A.Kuszyk
//  Added Update Product Attributes task to Create Sequence Template workflow.
#endregion
#region Version History: (CCM 8.3.0)
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31835 : N.Haywood
//  Added LocationProductLegal sync
// V8-31831 : A.Probyn
//  Removed SeparatorType from PlanogramNameTemplate defaults, and added PlanogramAttribute
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
// V8-32396 : A.Probyn
//  Updated assortment sync to including buddies and inventory rules
// V8-32621 : A.Silva
//  Added default PlanogramComparisonTemplate to SynchronizeEntities.
// V8-32431 : A.Probyn
//  Fixed LocationProductAttribute sync so that it doesn't throw an exception when null is returned from GFS.
// V8-32954 : A.Probyn
//  Fixed ApplyGfsAssortmentProduct so that ExactListUnits syncing on AssortmentProducts
// CCM-13748 : R.Cooper
//  Added FinancialGroupCode \ FinancialGroupName for product sync.
// V8-13711 : J.Pickup
//  Change to product sync to prevent the fill colour pattern and shape from being overwritten to the default values on updates.
// CCM-18623 : M.Brumby
//  Ensure on a full sync location product attributes uses GFS locations and doesn't assume its own list is correct as it might not
//  be in some fringe cases.
// CCM-18933 : M.Pettit
//  Updated Metric sync to use the new GetMetricIncludingCalculatedByEntityName service method to fetch all metrics including calculated
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Csla;
using Galleria.Framework.Helpers;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Ccm.Services.Cluster;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Ccm.Services.Metric;
using Galleria.Ccm.Services.LocationSpace;
using Galleria.Ccm.Services.LocationProduct;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services.Assortment;
using Galleria.Ccm.Services.AssortmentMinorRevision;
using Galleria.Framework.Planograms.External;
using FrameworkLanguage = Galleria.Framework.Planograms.Resources.Language;
using Galleria.Ccm.Services.Content;
using Galleria.Ccm.Services.Location;
using System.Configuration;
using Galleria.Ccm.Services.Publishing;

namespace Galleria.Ccm.Processes.Synchronization.SynchronizeTarget
{
    /// <summary>
    /// Process that performs the synchronization
    /// of a single target
    /// </summary>
    public partial class Process
    {
        #region Constants
        private const String _eventLogName = "Synchronization"; //The event log name of the process

        private const string _foundationServicesEndpoint = "Foundation Services Endpoint";

        private const int _syncTargetMaxRetryCount = 3; // maximum retry count per sync target per schedule
        private const int _syncTargetMaxFailedCount = 5; // maximum failed count before a sync target errors and stops trying to sync.

        private const byte _dummyPerformanceSourceSyncPeriod = 12;

        #endregion

        #region Fields
        private Int32 _entityId; // The current entity id
        private String _entityName; // The current entity name
        private Int32 _gfsEntityId; // The current entity id

        private Int32 _performanceSourceId; // the performance source Id
        private Dictionary<String, Int16> _performanceLocations; // the stores needed for performance data sync
        private List<String> _performanceLocationCodes; // the stores codes needed for performance data sync
        private List<List<String>> _performanceLocationCodeSplit; // the stores codes needed for performance data sync split in to a collection of collections to reduce each request size
        private Dictionary<String, Int32> _performanceProducts; // the products needed for performance data sync
        private List<String> _performanceProductGtins; // the products codes needed for performance data sync

        private bool _webServiceProxiesCreated = false; // boolean to denote whether we've successfully created the web service proxies. Needed for logging to gfs event log.
        private CultureInfo _locale;

        #endregion

        #region Properties
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return _entityId; }
            set { _entityId = value; }
        }

        /// <summary>
        /// The entity name
        /// </summary>
        public String EntityName
        {
            get { return _entityName; }
            set { _entityName = value; }
        }

        /// <summary>
        /// The GFS entity id
        /// </summary>
        public Int32 GFSEntityId
        {
            get { return _gfsEntityId; }
            set { _gfsEntityId = value; }
        }

        #region Performance Sync Properties

        /// <summary>
        /// The Performance Source Id
        /// </summary>
        public Int32 PerformanceSourceId
        {
            get { return _performanceSourceId; }
            set { _performanceSourceId = value; }
        }

        /// <summary>
        /// The locations needed for the performance sync
        /// Key = Code, Value = Id
        /// </summary>
        public Dictionary<String, Int16> PerformanceLocations
        {
            get { return _performanceLocations; }
            set { _performanceLocations = value; }
        }

        /// <summary>
        /// The location codes needed for the performance sync
        /// </summary>
        public List<String> PerformanceLocationCodes
        {
            get { return _performanceLocationCodes; }
            set { _performanceLocationCodes = value; }
        }

        /// <summary>
        /// The store codes needed for the performance sync split in to  
        /// a collection of collections to reduce the request call to the server
        /// </summary>
        public List<List<String>> PerformanceLocationCodeSplit
        {
            get { return _performanceLocationCodeSplit; }
            set { _performanceLocationCodeSplit = value; }
        }

        /// <summary>
        /// The products needed for the performance sync
        /// ISO-17222: Key = Gtin, Value = Id
        /// </summary>
        public Dictionary<String, Int32> PerformanceProducts
        {
            get { return _performanceProducts; }
            set { _performanceProducts = value; }
        }

        /// <summary>
        /// The product codes needed for the performance sync
        /// </summary>
        public List<String> PerformanceProductGtins
        {
            get { return _performanceProductGtins; }
            set { _performanceProductGtins = value; }
        }

        #endregion

        #endregion

        #region Methods

        #region OnExecute
        /// <summary>
        /// Called when executing this process
        /// </summary>
        protected override void OnExecute()
        {
            if (GfsLoggingHelper.Logger == null)
            {
                GfsLoggingHelper logger = new GfsLoggingHelper(true);
            }

            //Set the start date and time of the sync
            DateTime syncStartDate = DateTime.UtcNow;

            // Update Sync Target to Running
            this.UpdateSyncTargetStatus(SyncTargetStatus.Running, syncStartDate);
            try
            {
                // create the process context
                using (ProcessContext context = new ProcessContext(this.SyncTargetId))
                {
                    // verify that the target database can be located
                    if (!context.IsDatabaseAvailable())
                    {
                        this.UpdateSyncTargetStatus(SyncTargetError.TargetCouldNotBeLocated, syncStartDate);
                        return;
                    }

                    // attempt to create the foundation services web proxies
                    _webServiceProxiesCreated = context.CreateWebServiceProxies();
                    if (!_webServiceProxiesCreated)
                    {
                        this.UpdateSyncTargetStatus(SyncTargetError.CouldNotConnectToFoundationServices, syncStartDate);
                        return;
                    }

                    // now perform the synchronisation process
                    this.Synchronize(context);

                    // set the final process status to idle and successful
                    this.UpdateSyncTargetStatus_Successful(SyncTargetStatus.Idle, syncStartDate);
                }
            }
            catch (Exception ex)
            {
                this.UpdateSyncTargetStatus(SyncTargetStatus.Failed, syncStartDate);
            }
        }
        #endregion

        #region Synchronize
        /// <summary>
        /// Performs the synchronization process
        /// </summary>
        /// <param name="context">The process context</param>
        private void Synchronize(ProcessContext context)
        {
            // start by authenticating the user that is hosting this process
            DomainPrincipal.Authenticate();

            // Entities
            IEnumerable<EntityDto> entities = this.SynchronizeEntities(context);

            foreach (EntityDto entity in entities)
            {
                if (entity.GFSId != null)
                {
                    this.EntityId = entity.Id;
                    this.EntityName = entity.Name;
                    this.GFSEntityId = entity.GFSId.Value;

                    // Write Start Up log details to GFS
                    WriteSyncInformationToGFS(context, EventLogEvent.SyncProcessStart, new String[] { this.EntityName });

                    this.SetGfsConnectionDetails(context);

                    this.SynchroniseCDTUcrs(context);
                    this.SynchroniseProductUniverseUcrs(context);
                    this.SynchroniseLocationSpaceUcrs(context);
                    this.SynchroniseClusterSchemeUcrs(context);
                    this.SynchroniseAssortmentUcrs(context);
                    this.SynchroniseAssortmentMinorRevisionUcrs(context);

                    //Synchronize Metric
                    this.SynchronizeMetric(context);

                    //Syncrhonize Merchandising Hierarchy Data
                    this.SynchronizeMerchandisingHierarchy(context);

                    //Write Product event log information to GFS
                    this.SynchronizeProduct(context);

                    //Syncrhonize Product Universe Data
                    this.SyncrhonizeProductUniverse(context);
                    this.SynchronizeProductUniverseDeleted(context);

                    //Syncrhonize Location Hierarchy Data
                    this.SynchronizeLocationHierarchy(context);

                    //Syncrhonize Location Data
                    this.SynchronizeLocation(context);

                    //Synchronize Location Space
                    this.SynchronizeLocationSpace(context);
                    this.SynchronizeLocationSpaceDeleted(context);

                    //Synchronize LocationProductAttribute
                    this.SynchronizeLocationProductAttribute(context);
                    this.SynchronizeLocationProductAttributeDeleted(context);

                    //LocationProductIllegal
                    this.SynchronizeLocationProductIllegal(context);
                    this.SynchronizeLocationProductIllegalDeleted(context);

                    //LocationProductLegal
                    this.SynchronizeLocationProductLegal(context);
                    this.SynchronizeLocationProductLegalDeleted(context);

                    //Syncrhonize Cluster Data
                    this.SynchronizeCluster(context);
                    this.SynchronizeClustersDeleted(context);

                    // Syncrhonize CDT Data
                    this.SynchronizeConsumerDecisionTree(context);
                    this.SynchronizeConsumerDecisionTreeDeleted(context);

                    //Synchronize Assortment Data
                    this.SynchronizeAssortments(context);
                    this.SynchronizeAssortmentsDeleted(context);

                    //Synchronize AssortmentMinorRevision
                    this.SynchronizeAssortmentMinorRevision(context);
                    this.SynchronizeAssortmentMinorRevisionsDeleted(context);

                    //Synchronise Location Plan assignment dates
                    this.SynchroniseLocationPlanAssignmentDates(context);

                    // Write Start Up log details to GFS
                    WriteSyncInformationToGFS(context, EventLogEvent.SyncProcessEnd, new String[] { "" });
                }
            }
        }

        #endregion

        #region Sync Processes

        #region Synchronize Entities

        /// <summary>
        /// Syncrhonises the entities within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        private IEnumerable<EntityDto> SynchronizeEntities(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            List<EntityDto> dtoList = new List<EntityDto>();

            try
            {
                using (IEntityDal dal = context.DalContext.GetDal<IEntityDal>())
                {
                    List<EntityDto> ccmEntities = dal.FetchAll().ToList();

                    List<Services.Entity.Entity> gfsEntities = null;
                    context.DoWorkWithRetries(delegate ()
                    {
                        gfsEntities = context.Services.EntityServiceClient.GetAllEntities(new Services.Entity.GetAllEntitiesRequest()).Entities;
                    });

                    #region GFS-19627 - Mark as deleted any CCM Entities which no longer have a matching GFS entity

                    //get ids for all not-deleted GFS entities
                    IEnumerable<Int32> currentGfsEntityIds = gfsEntities.Select(e => e.Id);

                    //get all current CCM entities which have a reference to GFS 
                    IEnumerable<EntityDto> ccmEntitiesWithGfsRef = ccmEntities.Where(e => e.GFSId.HasValue);
                    //check if the reference points to a valid (undeleted) GFS entity                    
                    foreach (EntityDto ccmEntityDto in ccmEntitiesWithGfsRef.ToList())
                    {
                        if (!currentGfsEntityIds.Contains(ccmEntityDto.GFSId.Value))
                        {
                            dal.DeleteById(ccmEntityDto.Id);
                            ccmEntities.Remove(ccmEntityDto);
                        }
                    }

                    #endregion

                    foreach (var gfsEntity in gfsEntities)
                    {
                        EntityDto ccmEntity = null;

                        if (ccmEntities != null)
                        {
                            ccmEntity = ccmEntities.FirstOrDefault(i => i.GFSId == gfsEntity.Id);
                        }

                        if (ccmEntity == null)
                        {
                            ccmEntity = new EntityDto();

                            ccmEntity.Name = gfsEntity.Name;

                            ccmEntity.Description = String.Format(_locale, Message.Synchronization_EntityDescription, gfsEntity.Name);
                            ccmEntity.GFSId = gfsEntity.Id;

                            dal.Insert(ccmEntity);

                            //create default data for the entity
                            CreateDefaultLocationHierarchy(context, ccmEntity.Id, ccmEntity.Name);
                            CreateDefaultProductHierarchy(context, ccmEntity.Id, ccmEntity.Name);
                            CreateDefaultPlanogramHierarchy(context, ccmEntity.Id, ccmEntity.Name);
                            CreateDefaultPlanogramImportTemplate(context, ccmEntity.Id, PlanogramImportFileType.SpacemanV9);
                            CreateDefaultPlanogramImportTemplate(context, ccmEntity.Id, PlanogramImportFileType.Apollo);
                            CreateDefaultPlanogramImportTemplate(context, ccmEntity.Id, PlanogramImportFileType.ProSpace);
                            ValidationTemplate.InsertDefaultValidationTemplate(context.DalContext, ccmEntity.Id);
                            CreateDefaultRenumberingStrategy(context, ccmEntity.Id);
                            Entity.InsertDefaultWorkflows(context.DalContext, ccmEntity.Id);
                            CreateDefaultInventoryProfile(context, ccmEntity.Id);
                            CreateDefaultPlanogramNamingTemplates(context, ccmEntity.Id);

                            Entity.InsertDefaultHighlights(context.DalContext, ccmEntity.Id);
                            Entity.InsertDefaultLabels(context.DalContext, ccmEntity.Id);
                            Entity.InsertDefaultPrintTemplates(context.DalContext, ccmEntity.Id);
                            Entity.InsertDefaultComparisonTemplate(context.DalContext, ccmEntity.Id);

                            dtoList.Add(ccmEntity);
                        }
                        else
                        {
                            if (ccmEntity.Name != gfsEntity.Name)
                            {
                                ccmEntity.Description = String.Format(_locale, Message.Synchronization_EntityDescription, gfsEntity.Name);
                                ccmEntity.Name = gfsEntity.Name;

                                dal.Update(ccmEntity);
                            }
                            dtoList.Add(ccmEntity);
                        }

                        _entityId = ccmEntity.Id;
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Entity], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();

            return dtoList;
        }

        #region Default Data Creation

        private void CreateDefaultPlanogramImportTemplate(ProcessContext context, Int32 entityId, PlanogramImportFileType fileType)
        {
            //Insert hierarchy
            PlanogramImportTemplateDto planogramImportTemplateDto;
            using (IPlanogramImportTemplateDal dal = context.DalContext.GetDal<IPlanogramImportTemplateDal>())
            {
                planogramImportTemplateDto = new PlanogramImportTemplateDto
                {
                    Name = PlanogramImportFileTypeHelper.DefaultTemplateName[fileType],
                    EntityId = entityId,
                    FileType = (Byte)fileType,
                    FileVersion = PlanogramImportFileTypeHelper.MaxSupportedVersion[fileType]
                };

                dal.Insert(planogramImportTemplateDto);
            }

            //Insert default mappings
            List<PlanogramImportTemplateMappingDto> mappings = CreateDefaultPlanogramImportTemplateMappings((Int32)planogramImportTemplateDto.Id, fileType);
            using (IPlanogramImportTemplateMappingDal dal = context.DalContext.GetDal<IPlanogramImportTemplateMappingDal>())
            {
                foreach (PlanogramImportTemplateMappingDto mapping in mappings)
                {
                    dal.Insert(mapping);
                }
            }

            // Insert default metrics
            List<PlanogramImportTemplatePerformanceMetricDto> metrics = CreateDefaultPlanogramImportTemplateMetrics((Int32)planogramImportTemplateDto.Id, fileType);
            using (IPlanogramImportTemplatePerformanceMetricDal dal = context.DalContext.GetDal<IPlanogramImportTemplatePerformanceMetricDal>())
            {
                foreach (PlanogramImportTemplatePerformanceMetricDto metric in metrics)
                {
                    dal.Insert(metric);
                }
            }
        }

        private List<PlanogramImportTemplatePerformanceMetricDto> CreateDefaultPlanogramImportTemplateMetrics(Int32 planogramImportTemplateId, PlanogramImportFileType fileType)
        {
            List<PlanogramImportTemplatePerformanceMetricDto> metrics = new List<PlanogramImportTemplatePerformanceMetricDto>();

            switch (fileType)
            {
                case PlanogramImportFileType.SpacemanV9:
                    metrics.Add(NewMetric1SalesValue(planogramImportTemplateId, SpacemanFieldHelper.ProductSales));
                    metrics.Add(NewMetric2SalesVolume(planogramImportTemplateId, SpacemanFieldHelper.ProductUnitMovement));
                    metrics.Add(NewMetric3SalesMargin(planogramImportTemplateId, SpacemanFieldHelper.ProductUnitProfit));
                    break;
                case PlanogramImportFileType.Apollo:
                    metrics.Add(NewMetric1SalesValue(planogramImportTemplateId, ApolloImportHelper.PositionSales));
                    metrics.Add(NewMetric2SalesVolume(planogramImportTemplateId, ApolloImportHelper.ProductRealMovement));
                    metrics.Add(NewMetric3SalesMargin(planogramImportTemplateId, ApolloImportHelper.PositionPROFIT));
                    break;
                case PlanogramImportFileType.ProSpace:
                    metrics.Add(NewMetric2SalesVolume(planogramImportTemplateId, ProSpaceImportHelper.PerformanceUnitMovement));
                    break;
            }
            return metrics;
        }

        private List<PlanogramImportTemplateMappingDto> CreateDefaultPlanogramImportTemplateMappings(Int32 planogramImportTemplateId, PlanogramImportFileType fileType)
        {
            List<PlanogramImportTemplateMappingDto> mappings = new List<PlanogramImportTemplateMappingDto>();

            IEnumerable<PlanogramFieldMappingType> mappingTypes = Enum.GetValues(typeof(PlanogramFieldMappingType)).Cast<PlanogramFieldMappingType>().ToList();
            IEnumerable<PlanogramImportTemplateMappingDto> mappingDtos;
            Func<PlanogramFieldMappingType, IEnumerable<Tuple<String, String>>> mappingFunction;
            switch (fileType)
            {
                case PlanogramImportFileType.SpacemanV9:
                    #region Mappings - Planogram
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.NameProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CategoryCodeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CategoryNameProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });

                    #region Mappings - Planogram, Custom Attributes
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionString1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionString2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionString3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionString4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionString5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text11Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text12Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text13Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text14Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text15Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text16Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text17Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text18Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text19Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text20Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text21Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text22Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text23Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text24Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text25Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text26Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text27Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text28Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text29Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text30Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text31Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text32Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text33Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text34Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text35Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text36Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text37Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text38Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text39Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text40Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text41Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text42Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text43Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text44Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text45Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text46Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text47Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text48Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text49Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text50Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionDouble1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionDouble2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionDouble3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionDouble4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionDouble5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionInteger1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionInteger2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionInteger3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionInteger4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionInteger5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value11Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionLong1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value12Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionLong2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value13Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionLong3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value14Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionLong4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value15Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionLong5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value16Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value17Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value18Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value19Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value20Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value21Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value22Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value23Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value24Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value25Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value26Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value27Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value28Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value29Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value30Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value31Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value32Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value33Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value34Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value35Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value36Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value37Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value38Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value39Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value40Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value41Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value42Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value43Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value44Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value45Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value46Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value47Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value48Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value49Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value50Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.SectionDate });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Planogram, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });

                    #endregion

                    #endregion

                    #region Mappings - Fixture
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramFixture.NameProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Fixture, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelName });
                    #endregion

                    #region Mappings - Component

                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.NameProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelName });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.IsMoveableProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.IsDisplayOnlyProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.CanAttachShelfEdgeLabelProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.RetailerReferenceCodeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.BarCodeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.ManufacturerProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelManufacturer });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.ManufacturerPartNameProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.ManufacturerPartNumberProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.SupplierNameProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.SupplierPartNumberProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.SupplierCostPriceProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.SupplierDiscountProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.SupplierLeadTimeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.MinPurchaseQtyProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.WeightLimitProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.WeightProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.VolumeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.DiameterProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramComponent.CapacityProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });

                    #region Mappings - Component, Custom Attributes
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelString1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelString2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelString3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelString4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelString5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text11Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text12Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text13Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text14Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text15Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text16Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text17Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text18Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text19Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text20Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text21Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text22Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text23Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text24Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text25Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text26Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text27Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text28Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text29Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text30Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text31Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text32Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text33Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text34Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text35Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text36Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text37Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text38Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text39Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text40Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text41Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text42Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text43Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text44Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text45Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text46Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text47Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text48Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text49Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text50Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelDouble1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelDouble2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelDouble3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelDouble4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelDouble5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelInteger1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelInteger2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelInteger3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelInteger4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelInteger5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value11Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelLong1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value12Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelLong2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value13Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelLong3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value14Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelLong4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value15Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.FixelLong5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value16Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value17Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value18Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value19Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value20Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value21Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value22Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value23Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value24Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value25Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value26Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value27Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value28Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value29Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value30Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value31Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value32Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value33Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value34Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value35Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value36Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value37Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value38Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value39Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value40Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value41Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value42Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value43Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value44Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value45Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value46Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value47Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value48Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value49Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value50Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Component, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });

                    #endregion

                    #endregion

                    #region Mappings - Product

                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.GtinProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, ExternalField = SpacemanFieldHelper.ProductGTIN, PlanogramImportTemplateId = planogramImportTemplateId });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.NameProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductName });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.BrandProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, ExternalField = SpacemanFieldHelper.ProductBrand, PlanogramImportTemplateId = planogramImportTemplateId });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.PointOfPurchaseHeightProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.PointOfPurchaseWidthProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.PointOfPurchaseDepthProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.StatusTypeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.IsPlaceHolderProductProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.IsActiveProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ShapeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.PointOfPurchaseDescriptionProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ShortDescriptionProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.SubcategoryProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductSubcategory });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.CustomerStatusProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ColourProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductColour });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.FlavourProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.PackagingShapeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.PackagingTypeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.CountryOfOriginProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.CountryOfProcessingProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ShelfLifeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.DeliveryFrequencyDaysProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.DeliveryMethodProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.VendorCodeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.VendorProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ManufacturerCodeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ManufacturerProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, ExternalField = SpacemanFieldHelper.ProductManufacturer, PlanogramImportTemplateId = planogramImportTemplateId });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.SizeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductSize });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.UnitOfMeasureProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductUOM });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.DateIntroducedProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.DateDiscontinuedProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.DateEffectiveProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.HealthProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.CorporateCodeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.BarcodeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.SellPriceProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductPrice });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.SellPackCountProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.SellPackDescriptionProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.RecommendedRetailPriceProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ManufacturerRecommendedRetailPriceProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.CostPriceProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductCost });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.CaseCostProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductCaseCost });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.TaxRateProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductTax_Code });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ConsumerInformationProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.TextureProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.StyleNumberProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.PatternProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.ModelProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.GarmentTypeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.IsPrivateLabelProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.IsNewProductProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.FinancialGroupCodeProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.FinancialGroupNameProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = PlanogramProduct.CasePackUnitsProperty.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductUnitsCase });

                    #region Mappings - Product, Custom Attributes
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescA });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescB });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescC });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescD });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescE });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescF });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescG });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescH });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescI });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDescJ });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text11Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductString1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text12Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductString2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text13Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductString3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text14Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductString4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text15Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductString5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text16Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text17Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text18Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text19Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text20Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text21Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text22Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text23Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text24Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text25Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text26Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text27Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text28Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text29Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text30Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text31Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text32Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text33Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text34Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text35Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text36Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text37Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text38Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text39Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text40Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text41Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text42Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text43Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text44Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text45Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text46Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text47Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text48Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text49Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Text50Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDouble1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDouble2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDouble3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDouble4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductDouble5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductInteger1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductInteger2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductInteger3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductInteger4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductInteger5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value11Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductLong1 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value12Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductLong2 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value13Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductLong3 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value14Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductLong4 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value15Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = SpacemanFieldHelper.ProductLong5 });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value16Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value17Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value18Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value19Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value20Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value21Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value22Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value23Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value24Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value25Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value26Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value27Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value28Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value29Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value30Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value31Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value32Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value33Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value34Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value35Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value36Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value37Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value38Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value39Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value40Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value41Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value42Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value43Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value44Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value45Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value46Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value47Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value48Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value49Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Value50Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Flag10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date1Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date2Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date3Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date4Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date5Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date6Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date7Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date8Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date9Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });
                    mappings.Add(new PlanogramImportTemplateMappingDto() { Field = Planogram.CustomAttributesProperty.Name + "." + CustomAttributeData.Date10Property.Name, FieldType = (Byte)PlanogramFieldMappingType.Product, PlanogramImportTemplateId = planogramImportTemplateId, ExternalField = String.Empty });

                    #endregion

                    #endregion
                    break;
                case PlanogramImportFileType.Apollo:
                    mappingFunction = ApolloImportHelper.EnumerateDefaultMappings;
                    mappingDtos = mappingTypes
                        .SelectMany(mappingFunction, (type, tuple) => NewPlanogramImportTemplateMappingDto(planogramImportTemplateId, tuple, type));
                    mappings.AddRange(mappingDtos);
                    break;
                case PlanogramImportFileType.ProSpace:
                    mappingFunction = ProSpaceImportHelper.EnumerateDefaultMappings;
                    mappingDtos = mappingTypes
                        .SelectMany(mappingFunction, (type, tuple) => NewPlanogramImportTemplateMappingDto(planogramImportTemplateId, tuple, type));
                    mappings.AddRange(mappingDtos);
                    break;
            }
            return mappings;
        }

        private void CreateDefaultLocationHierarchy(ProcessContext context, Int32 entityId, String entityName)
        {
            //Insert hierarchy
            LocationHierarchyDto hierarchyDto;
            using (ILocationHierarchyDal dal = context.DalContext.GetDal<ILocationHierarchyDal>())
            {
                hierarchyDto = new LocationHierarchyDto
                {
                    Name = String.Format(Message.LocationHierarchy_DefaultName, entityName),
                    EntityId = entityId
                };

                dal.Insert(hierarchyDto);
            }

            //Insert levels
            LocationLevelDto levelrootDto;
            using (ILocationLevelDal dal = context.DalContext.GetDal<ILocationLevelDal>())
            {
                levelrootDto = new LocationLevelDto
                {
                    Name = Message.LocationHierarchy_RootLevel_DefaultName,
                    LocationHierarchyId = hierarchyDto.Id,
                    ParentLevelId = null
                };
                dal.Insert(levelrootDto);

                LocationLevelDto level1Dto = new LocationLevelDto
                {
                    Name = Message.LocationHierarchy_BannerLevel_DefaultName,
                    ShapeNo = 10,
                    Colour = 1270125823,
                    LocationHierarchyId = hierarchyDto.Id,
                    ParentLevelId = levelrootDto.Id
                };
                dal.Insert(level1Dto);

                LocationLevelDto level2Dto = new LocationLevelDto
                {
                    Name = Message.LocationHierarchy_RegionLevel_DefaultName,
                    ShapeNo = 7,
                    Colour = -1587642625,
                    LocationHierarchyId = hierarchyDto.Id,
                    ParentLevelId = level1Dto.Id
                };
                dal.Insert(level2Dto);
            }

            //Insert root group
            using (ILocationGroupDal dal = context.DalContext.GetDal<ILocationGroupDal>())
            {
                LocationGroupDto rootGroupDto = new LocationGroupDto
                {
                    Code = "0",
                    Name = Message.LocationHierarchy_RootGroup_DefaultName,
                    LocationHierarchyId = hierarchyDto.Id,
                    ParentGroupId = null,
                    LocationLevelId = levelrootDto.Id
                };
                dal.Insert(rootGroupDto);
            }
        }

        private void CreateDefaultProductHierarchy(ProcessContext context, Int32 entityId, String entityName)
        {
            //Insert hierarchy
            ProductHierarchyDto hierarchyDto;
            using (IProductHierarchyDal dal = context.DalContext.GetDal<IProductHierarchyDal>())
            {
                hierarchyDto = new ProductHierarchyDto
                {
                    Name = String.Format(Message.ProductHierarchy_DefaultName, entityName),
                    EntityId = entityId
                };

                dal.Insert(hierarchyDto);
            }

            //Insert levels
            ProductLevelDto levelrootDto;
            using (IProductLevelDal dal = context.DalContext.GetDal<IProductLevelDal>())
            {
                levelrootDto = new ProductLevelDto
                {
                    Name = Message.ProductHierarchy_RootLevel_DefaultName,
                    ProductHierarchyId = hierarchyDto.Id,
                    ParentLevelId = null
                };
                dal.Insert(levelrootDto);

                ProductLevelDto level1Dto = new ProductLevelDto
                {
                    Name = Message.ProductHierarchy_DepartmentLevel_DefaultName,
                    ShapeNo = 10,
                    Colour = 1270125823,
                    ProductHierarchyId = hierarchyDto.Id,
                    ParentLevelId = levelrootDto.Id
                };
                dal.Insert(level1Dto);

                ProductLevelDto level2Dto = new ProductLevelDto
                {
                    Name = Message.ProductHierarchy_CategoryLevel_DefaultName,
                    ShapeNo = 7,
                    Colour = -1587642625,
                    ProductHierarchyId = hierarchyDto.Id,
                    ParentLevelId = level1Dto.Id
                };
                dal.Insert(level2Dto);
            }

            //Insert root group
            using (IProductGroupDal dal = context.DalContext.GetDal<IProductGroupDal>())
            {
                ProductGroupDto rootGroupDto = new ProductGroupDto
                {
                    Code = "0",
                    Name = Message.ProductHierarchy_RootGroup_DefaultName,
                    ProductHierarchyId = hierarchyDto.Id,
                    ParentGroupId = null,
                    ProductLevelId = levelrootDto.Id
                };
                dal.Insert(rootGroupDto);
            }
        }

        private void CreateDefaultPlanogramHierarchy(ProcessContext context, Int32 entityId, String entityName)
        {
            //Insert hierarchy
            PlanogramHierarchyDto hierarchyDto;
            using (IPlanogramHierarchyDal dal = context.DalContext.GetDal<IPlanogramHierarchyDal>())
            {
                hierarchyDto = new PlanogramHierarchyDto
                {
                    Name = String.Format(Message.Entity_PlanogramHierarchyDefaultName, entityName),
                    EntityId = entityId
                };

                dal.Insert(hierarchyDto);
            }

            //Insert root group
            using (IPlanogramGroupDal dal = context.DalContext.GetDal<IPlanogramGroupDal>())
            {
                PlanogramGroupDto rootGroupDto = new PlanogramGroupDto
                {
                    Name = Message.Entity_PlanogramHierarchyDefaultRootName,
                    PlanogramHierarchyId = hierarchyDto.Id,
                    ParentGroupId = null,
                };
                dal.Insert(rootGroupDto);
            }
        }

        private void CreateDefaultRenumberingStrategy(ProcessContext context, Int32 entityId)
        {
            using (IRenumberingStrategyDal renumberingDal = context.DalContext.GetDal<IRenumberingStrategyDal>())
            {
                RenumberingStrategyDto renumberingDto = new RenumberingStrategyDto()
                {
                    EntityId = entityId,
                    Name = Message.RenumberingStrategy_EntityDefaultStrategyName,
                    BayComponentXRenumberStrategyPriority = 1,
                    BayComponentYRenumberStrategyPriority = 2,
                    BayComponentZRenumberStrategyPriority = 3,
                    PositionXRenumberStrategyPriority = 1,
                    PositionYRenumberStrategyPriority = 2,
                    PositionZRenumberStrategyPriority = 3,
                    BayComponentXRenumberStrategy = (Byte)PlanogramRenumberingStrategyXRenumberType.LeftToRight,
                    BayComponentYRenumberStrategy = (Byte)PlanogramRenumberingStrategyYRenumberType.BottomToTop,
                    BayComponentZRenumberStrategy = (Byte)PlanogramRenumberingStrategyZRenumberType.FrontToBack,
                    PositionXRenumberStrategy = (Byte)PlanogramRenumberingStrategyXRenumberType.LeftToRight,
                    PositionYRenumberStrategy = (Byte)PlanogramRenumberingStrategyYRenumberType.BottomToTop,
                    PositionZRenumberStrategy = (Byte)PlanogramRenumberingStrategyZRenumberType.FrontToBack,
                    RestartComponentRenumberingPerBay = true,
                    IgnoreNonMerchandisingComponents = true,
                    RestartPositionRenumberingPerComponent = true,
                    UniqueNumberMultiPositionProductsPerComponent = false,
                    ExceptAdjacentPositions = false
                };
                renumberingDal.Insert(renumberingDto);
            }
        }

        private void CreateDefaultInventoryProfile(ProcessContext context, Int32 entityId)
        {
            using (IInventoryProfileDal inventoryDal = context.DalContext.GetDal<IInventoryProfileDal>())
            {
                InventoryProfileDto inventoryDto = new InventoryProfileDto()
                {
                    EntityId = entityId,
                    Name = Message.InventoryProfile_DefaultName,
                    CasePack = 1.25F,
                    DaysOfSupply = 3,
                };
                inventoryDal.Insert(inventoryDto);

            }
        }

        private void CreateDefaultPlanogramNamingTemplates(ProcessContext context, Int32 entityId)
        {
            using (IPlanogramNameTemplateDal nameTemplateDal = context.DalContext.GetDal<IPlanogramNameTemplateDal>())
            {
                PlanogramNameTemplateDto clusterDto = new PlanogramNameTemplateDto()
                {
                    EntityId = entityId,
                    Name = Message.PlanogramNameTemplate_ClusterDefaultName,
                    BuilderFormula = "[Planogram.CategoryCode]_[Planogram.CategoryName]_[Planogram.ClusterName]_[LocationSpaceProductGroup.BayCount]_[Planogram.Width]_[Planogram.Height]",
                    PlanogramAttribute = (Byte)0
                };
                nameTemplateDal.Insert(clusterDto);

                PlanogramNameTemplateDto storeSpecificDto = new PlanogramNameTemplateDto()
                {
                    EntityId = entityId,
                    Name = Message.PlanogramNameTemplate_StoreSpecificDefaultName,
                    BuilderFormula = "[Planogram.CategoryCode]_[Planogram.CategoryName]_[Location.Code]_[LocationSpaceProductGroup.BayCount]_[Planogram.Width]",
                    PlanogramAttribute = (Byte)0
                };
                nameTemplateDal.Insert(storeSpecificDto);
            }

        }

        #endregion

        #endregion

        #region MerchandisingHierarchy

        /// <summary>
        /// Syncrhonises the products within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="dtolist">List of the Entities</param>
        private void SynchronizeMerchandisingHierarchy(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                context.ProductGroupMap.Clear();
                context.ProductLevelMap.Clear();

                #region CCM Product hierachy
                ProductHierarchyDto dtoHierarchyStructure;

                using (IProductHierarchyDal dal = context.DalContext.GetDal<IProductHierarchyDal>())
                {
                    dtoHierarchyStructure = dal.FetchByEntityId(this.EntityId);
                }
                #endregion

                //Call the Web Service For the Merchandising Hierachy
                Services.Product.ProductHierarchy gfsProductHierarchy = null;
                context.DoWorkWithRetries(delegate ()
                {
                    gfsProductHierarchy = context.Services.ProductServiceClient.GetMerchandisingHierarchyByEntityId(
                        new Services.Product.GetMerchandisingHierarchyByEntityIdRequest(this.GFSEntityId)).MerchandisingHierarchy;
                });

                //If there is a merchandising hierarchy returned
                if (gfsProductHierarchy.RootLevel != null)
                {
                    #region CCM Product level

                    List<ProductLevelDto> ccmHierarchyLevels = new List<ProductLevelDto>();

                    using (IProductLevelDal dalLevel = context.DalContext.GetDal<IProductLevelDal>())
                    {
                        //Fetch the CCM Merchandising Levels
                        List<ProductLevelDto> ccmProductLevelDtos = dalLevel.FetchByProductHierarchyId(dtoHierarchyStructure.Id).ToList();

                        #region Order CCM product levels
                        //Find & add root level
                        ProductLevelDto ccmProductLevelDto = ccmProductLevelDtos.FirstOrDefault(p => p.ParentLevelId == null);
                        ccmHierarchyLevels.Add(ccmProductLevelDto);

                        //While a child level is still being found
                        while (ccmProductLevelDto != null)
                        {
                            //Select child of the current level dto
                            ccmProductLevelDto = ccmProductLevelDtos.FirstOrDefault(p => p.ParentLevelId.Equals(ccmProductLevelDto.Id));

                            //If child object is found
                            if (ccmProductLevelDto != null)
                            {
                                //Add to levels
                                ccmHierarchyLevels.Add(ccmProductLevelDto);
                            }
                        }
                        #endregion

                        //Set the Product level from the web-service
                        Services.Product.ProductLevel gfsProductlevel = gfsProductHierarchy.RootLevel;

                        //Get all levels in gfs hierarchy from the web service
                        List<Services.Product.ProductLevel> gfsProductHierarchyLevels = new List<Services.Product.ProductLevel>();
                        //move down through the levels adding each one
                        Services.Product.ProductLevel currentLevel = gfsProductHierarchy.RootLevel;
                        while (currentLevel != null)
                        {
                            gfsProductHierarchyLevels.Add(currentLevel);
                            currentLevel = currentLevel.ChildLevel;
                        }

                        // Set the CCM level to the root
                        Int32 levelCount = 0;
                        ProductLevelDto ccmLevel = ccmHierarchyLevels[levelCount];

                        #region Insert/Update level
                        // Loop through every gfs level
                        while (gfsProductlevel != null)
                        {
                            if (ccmLevel == null)
                            {
                                ccmLevel = new ProductLevelDto();

                                //Find parent
                                Services.Product.ProductLevel parentLevel = gfsProductHierarchyLevels.FirstOrDefault(p => p.ChildLevel.Id == gfsProductlevel.Id);

                                ccmLevel.Name = gfsProductlevel.Name;
                                ccmLevel.Colour = gfsProductlevel.Colour;
                                ccmLevel.ShapeNo = 1;
                                ccmLevel.ProductHierarchyId = dtoHierarchyStructure.Id;
                                if (parentLevel != null)
                                {
                                    //Lookup parent level CCM id
                                    ccmLevel.ParentLevelId = context.ProductLevelMap[parentLevel.Id];
                                }
                                else
                                {
                                    ccmLevel.ParentLevelId = null;
                                }

                                try
                                {
                                    dalLevel.Insert(ccmLevel);
                                }
                                catch
                                {
                                    dalLevel.Update(ccmLevel);
                                }
                                ccmHierarchyLevels.Add(ccmLevel);

                                if (levelCount > 0)
                                {
                                    //Update the child id, if a child level exists in the hierarchy levels
                                    //below current level. Count() isn't zero based hence the + 2
                                    if (ccmHierarchyLevels.Count >= levelCount + 2)
                                    {
                                        //Check if level below current level is assigned this level as its parent
                                        if (ccmHierarchyLevels[levelCount + 1].ParentLevelId != ccmLevel.Id)
                                        {
                                            //If not, set
                                            ccmHierarchyLevels[levelCount + 1].ParentLevelId = ccmLevel.Id;
                                            dalLevel.Update(ccmHierarchyLevels[levelCount + 1]);
                                        }
                                    }
                                }

                                // update the lookup
                                context.ProductLevelMap.Add(gfsProductlevel.Id, ccmLevel.Id);
                            }
                            else
                            {
                                //If the name has changed then update it
                                if (ccmLevel.Name != gfsProductlevel.Name)
                                {
                                    ccmLevel.Name = gfsProductlevel.Name;
                                    try
                                    {
                                        dalLevel.Update(ccmLevel);
                                    }
                                    catch
                                    {
                                        //catch means it already exists so we need to insert the level inbetween other levels
                                        IEnumerable<ProductLevelDto> levelDtos = dalLevel.FetchByProductHierarchyId(ccmLevel.ProductHierarchyId);

                                        ProductLevelDto dtoToMove = null;

                                        foreach (ProductLevelDto levelDto in levelDtos)
                                        {
                                            if (levelDto.ParentLevelId == ccmLevel.ParentLevelId)
                                            {
                                                dtoToMove = levelDto;
                                                break;
                                            }
                                        }

                                        dalLevel.Insert(ccmLevel);

                                        if (dtoToMove != null)
                                        {
                                            dtoToMove.ParentLevelId = ccmLevel.Id;
                                            dalLevel.Update(dtoToMove);
                                        }
                                    }
                                }

                                // update the lookkup
                                context.ProductLevelMap.Add(gfsProductlevel.Id, ccmLevel.Id);
                            }

                            //Set the next gfs merch level
                            gfsProductlevel = gfsProductlevel.ChildLevel;
                            //Increase the level count
                            levelCount++;

                            if (levelCount < ccmHierarchyLevels.Count)
                            {
                                //Set the next CCM merch level
                                ccmLevel = ccmHierarchyLevels[levelCount];
                            }
                            else
                            {
                                ccmLevel = null;
                            }
                        }

                        // Remove the extra levels that are not in gfs
                        while (levelCount < ccmHierarchyLevels.Count)
                        {
                            //remove extra level
                            dalLevel.DeleteById(ccmHierarchyLevels[levelCount].Id);

                            //increase the level count
                            levelCount++;
                        }
                        #endregion

                    }
                    #endregion

                    #region CCM Product Group

                    //Create the Dal for the CCM Product Group
                    using (IProductGroupDal dalGroup = context.DalContext.GetDal<IProductGroupDal>())
                    {
                        // CCM unit and dictionary of units
                        ProductGroupDto ccmGroup;
                        Dictionary<String, ProductGroupDto> ccmGroupList = new Dictionary<String, ProductGroupDto>();

                        // Fetch the hierarchy structure for this entity
                        Ccm.Model.ProductHierarchy structure = Ccm.Model.ProductHierarchy.FetchByEntityIdContext(this.EntityId, context.DalContext);
                        // Fetch all the merch hierarchy groups for the structure
                        IEnumerable<Ccm.Model.ProductGroup> groupList = structure.FetchAllGroups();
                        // Fetch the CCM root group 
                        ProductGroupDto ccmRootGroup = dalGroup.FetchById(groupList.First(p => p.ParentGroup == null).Id);

                        // dictionary of gfs product groups
                        Dictionary<String, Services.Product.ProductGroup> gfsUnitList = new Dictionary<String, Services.Product.ProductGroup>();

                        // Loop through the gfs merch hierarchy unit and add to the gfs list
                        LoadGFSChildGroup(gfsProductHierarchy.RootGroup, gfsUnitList);

                        // Loop through each CCM merch hierarchy unit and add to a list aslong as it also exists in gfs
                        foreach (Ccm.Model.ProductGroup group in groupList)
                        {
                            // Ensure it's not the root group
                            if (group.ParentGroup != null)
                            {
                                // Ensure that the merch hierarchy unit exists in gfs. If not, delete it
                                if (gfsUnitList.ContainsKey(group.Code))
                                {
                                    ccmGroup = new ProductGroupDto();

                                    ccmGroup.Id = group.Id;
                                    ccmGroup.Code = group.Code;
                                    ccmGroup.ProductLevelId = (Int32)group.ProductLevelId;
                                    ccmGroup.Name = group.Name;
                                    ccmGroup.ParentGroupId = group.ParentGroup.Id;
                                    ccmGroup.ProductHierarchyId = group.ParentHierarchy.Id;

                                    ccmGroupList.Add(ccmGroup.Code, ccmGroup);
                                }
                                else
                                {
                                    // Delete the unit as it does not exist in gfs
                                    dalGroup.DeleteById(group.Id);
                                }
                            }
                        }

                        // Set the gfs product group to the root
                        Services.Product.ProductGroup serviceGroup = gfsProductHierarchy.RootGroup;

                        if (serviceGroup != null)
                        {
                            if (ccmRootGroup == null)
                            {
                                ccmRootGroup = new ProductGroupDto();

                                ccmRootGroup.Code = serviceGroup.Code;
                                ccmRootGroup.ProductLevelId = ccmHierarchyLevels[0].Id;
                                ccmRootGroup.Name = serviceGroup.Name;
                                ccmRootGroup.ProductHierarchyId = dtoHierarchyStructure.Id;
                                ccmRootGroup.ParentGroupId = null;

                                dalGroup.Insert(ccmRootGroup);
                            }
                            else
                            {
                                if (ccmRootGroup.Code != serviceGroup.Code || ccmRootGroup.Name != serviceGroup.Name)
                                {
                                    ccmRootGroup.Code = serviceGroup.Code;
                                    ccmRootGroup.Name = serviceGroup.Name;

                                    dalGroup.Update(ccmRootGroup);
                                }
                            }

                            //Add root group to lookup
                            context.ProductGroupMap.Add(ccmRootGroup.Code, ccmRootGroup.Id);
                            context.ProductGroupIdMap.Add(serviceGroup.Id, ccmRootGroup.Id);

                            LoadChildGroup(serviceGroup, ccmGroupList, context, ccmRootGroup.Id, dalGroup, dtoHierarchyStructure.Id);
                        }

                    }
                    #endregion
                }
            }

            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Merchandising], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #region Load Child Group
        /// <summary>
        /// Loads the children of a product group
        /// </summary>
        /// <param name="group">The product group object</param>
        /// <param name="isoUnitList">The list of ISO units</param>
        /// <param name="context">The process context</param>
        /// <param name="parentId">The Id of the Parent group</param>
        /// <param name="dalUnit">The Entity Id</param>
        private void LoadChildGroup(Services.Product.ProductGroup group, Dictionary<String, ProductGroupDto> ccmGroupList, ProcessContext context, Int32 parentId, IProductGroupDal dalGroup, Int32 productHierarchyId)
        {
            ProductGroupDto ccmGroup;

            foreach (Services.Product.ProductGroup childGroup in group.Children)
            {
                Int32 ccmLevelId = context.ProductLevelMap[childGroup.ProductLevelId];


                #region Insert/Update unit
                if (ccmGroupList.ContainsKey(childGroup.Code))
                {
                    ccmGroup = ccmGroupList[childGroup.Code];

                    if (ccmGroup.ParentGroupId != parentId || ccmGroup.Name != childGroup.Name)
                    {
                        ccmGroup.ParentGroupId = parentId;
                        ccmGroup.Name = childGroup.Name;

                        dalGroup.Update(ccmGroup);
                    }
                }
                else
                {
                    //Insert new Product Group
                    ccmGroup = new ProductGroupDto();

                    ccmGroup.Code = childGroup.Code;
                    ccmGroup.Name = childGroup.Name;
                    ccmGroup.ParentGroupId = parentId;
                    ccmGroup.ProductLevelId = ccmLevelId;
                    ccmGroup.ProductHierarchyId = productHierarchyId;

                    dalGroup.Insert(ccmGroup);
                }

                context.ProductGroupMap.Add(ccmGroup.Code, ccmGroup.Id);
                context.ProductGroupIdMap.Add(childGroup.Id, ccmGroup.Id);

                #endregion

                LoadChildGroup(childGroup, ccmGroupList, context, ccmGroup.Id, dalGroup, productHierarchyId);
            }
        }
        #endregion

        #region Load GFS Child Group
        /// <summary>
        /// Loads the children of a gfs product group
        /// </summary>
        /// <param name="group">The product group object</param>
        /// /// <param name="group">The gfs unit list</param>
        private void LoadGFSChildGroup(Services.Product.ProductGroup gfsGroup, Dictionary<String, Services.Product.ProductGroup> gfsUnitList)
        {
            foreach (Services.Product.ProductGroup gfsChildGroup in gfsGroup.Children)
            {
                gfsUnitList.Add(gfsChildGroup.Code, gfsChildGroup);

                LoadGFSChildGroup(gfsChildGroup, gfsUnitList);
            }
        }
        #endregion

        #endregion

        #region Products

        #region Product Sync

        /// <summary>
        /// Syncrhonises the products within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        private void SynchronizeProduct(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IProductDal dal = context.DalContext.GetDal<IProductDal>())
                {
                    using (ICustomAttributeDataDal attributeDataDal = context.DalContext.GetDal<ICustomAttributeDataDal>())
                    {
                        context.ProductMap.Clear();

                        List<Services.Product.ProductInfo> gfsProductInfo = null;

                        List<Services.Product.ProductInfo> deletedGfsProductInfo = null;

                        //Get the product info 
                        if (SyncTarget.LastSync != null)
                        {
                            context.DoWorkWithRetries(delegate ()
                            {
                                gfsProductInfo = context.Services.ProductServiceClient.GetProductInfoByEntityIdDateChanged(
                                    new Services.Product.GetProductInfoByEntityIdDateChangedRequest(this.GFSEntityId, SyncTarget.LastSync.Value)).ProductInfos;

                                deletedGfsProductInfo = context.Services.ProductServiceClient.GetProductInfoByEntityIdDateDeleted(
                                    new Services.Product.GetProductInfoByEntityIdDateDeletedRequest(this.GFSEntityId, SyncTarget.LastSync.Value)).ProductInfos;
                            });
                        }
                        else
                        {
                            context.DoWorkWithRetries(delegate ()
                            {
                                gfsProductInfo = context.Services.ProductServiceClient.GetProductInfoByEntityId(
                                    new Services.Product.GetProductInfoByEntityIdRequest(this.GFSEntityId)).ProductInfos;

                                deletedGfsProductInfo = context.Services.ProductServiceClient.GetProductInfoByEntityIdDateDeleted(
                                    new Services.Product.GetProductInfoByEntityIdDateDeletedRequest(this.GFSEntityId, null)).ProductInfos;
                            });
                        }


                        // Build up a list of product ids to be fetched
                        List<List<Int32>> productIds = new List<List<Int32>>();
                        List<Int32> sublistIds = new List<Int32>();

                        List<IEnumerable<String>> productGtins = new List<IEnumerable<String>>();
                        List<String> sublistGtins = new List<String>();

                        int batch = 999;

                        // Loop through the product info list previously 
                        // created to generate the product Ids for the product fetch.
                        foreach (Services.Product.ProductInfo prodInfo in gfsProductInfo)
                        {

                            if (sublistIds.Count <= batch)
                            {
                                sublistIds.Add(prodInfo.Id);
                                sublistGtins.Add(prodInfo.GTIN);
                            }
                            else
                            {
                                productIds.Add(sublistIds);
                                productGtins.Add(sublistGtins);
                                sublistIds = new List<Int32>();
                                sublistGtins = new List<String>();
                                sublistIds.Add(prodInfo.Id);
                                sublistGtins.Add(prodInfo.GTIN);
                            }

                        }

                        if (sublistIds.Count > 0)
                        {
                            productIds.Add(sublistIds);
                            productGtins.Add(sublistGtins);
                        }

                        // Loop through the product info list previously 
                        // created to generate the product Ids for the product fetch.
                        foreach (Services.Product.ProductInfo delProdInfo in deletedGfsProductInfo)
                        {

                            if (sublistIds.Count <= batch)
                            {
                                sublistIds.Add(delProdInfo.Id);
                                sublistGtins.Add(delProdInfo.GTIN);
                            }
                            else
                            {
                                productIds.Add(sublistIds);
                                productGtins.Add(sublistGtins);
                                sublistIds = new List<Int32>();
                                sublistGtins = new List<String>();
                                sublistIds.Add(delProdInfo.Id);
                                sublistGtins.Add(delProdInfo.GTIN);
                            }

                        }

                        if (sublistIds.Count > 0)
                        {
                            productIds.Add(sublistIds);
                            productGtins.Add(sublistGtins);
                        }

                        //We need to fetch the 'ProductAttribute' for the current entity we are in. This will tell us all about the datatypes later on when we need to 
                        //Set the correct corresponding custom attribute datatype.
                        List<Services.Product.ProductAttribute> gfsProductAttributesForEntity = new List<Services.Product.ProductAttribute>();
                        context.DoWorkWithRetries(delegate ()
                        {
                            gfsProductAttributesForEntity = context.Services.ProductServiceClient.GetProductAttributeByEntityId(
                                new Services.Product.GetProductAttributeByEntityIdRequest(this.GFSEntityId)).ProductAttributes;
                        });

                        //Call the Web Service For the Financial Hierachy
                        Services.Product.ProductHierarchy gfsProductHierarchy = null;
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsProductHierarchy = context.Services.ProductServiceClient.GetFinancialHierarchyByEntityId(
                                new Services.Product.GetFinancialHierarchyByEntityIdRequest(this.GFSEntityId)).FinancialHierarchy;
                        });

                        // dictionary of gfs financial groups
                        Dictionary<String, Services.Product.ProductGroup> gfsUnitList = new Dictionary<String, Services.Product.ProductGroup>();

                        // Loop through the gfs financial hierarchy unit and add to the gfs unit list
                        LoadGFSChildGroup(gfsProductHierarchy.RootGroup, gfsUnitList);

                        for (int i = 0; i < productIds.Count; i++)
                        {
                            IEnumerable<ProductDto> ccmProducts = dal.FetchByEntityIdProductGtins(this.EntityId, productGtins[i]);

                            List<Services.Product.Product> gfsProducts = null;
                            context.DoWorkWithRetries(delegate ()
                            {
                                gfsProducts = context.Services.ProductServiceClient.GetProductByEntityIdProductId(
                                    new Services.Product.GetProductByEntityIdProductIdRequest(this.GFSEntityId, productIds[i])).Products;
                            });

                            List<ProductDto> productDtoList = new List<ProductDto>();
                            List<CustomAttributeDataDto> attributeDataDtoList = new List<CustomAttributeDataDto>();

                            foreach (var gfsProduct in gfsProducts)
                            {
                                Boolean isInitialInsert = true;

                                ProductDto productDto = new ProductDto();
                                ProductDto ccmProductDto = null;

                                CustomAttributeDataDto attributeDataDto = new CustomAttributeDataDto();

                                //Try and lookup product dto
                                if (ccmProducts != null)
                                {
                                    ccmProductDto = ccmProducts.FirstOrDefault(p => p.Gtin == gfsProduct.GTIN);

                                    if (ccmProductDto != null)
                                    {
                                        productDto = ccmProductDto;
                                        isInitialInsert = false;
                                    }
                                }

                                //find the related product group in order to update financial group code \ name attributes.
                                Services.Product.ProductGroup prodGroup = gfsUnitList.Where(z => z.Value.Id == gfsProduct.ProductGroupId).FirstOrDefault().Value;

                                #region Products Insert/Update
                                productDto.EntityId = this.EntityId;
                                productDto.ProductGroupId = null;
                                productDto.Gtin = gfsProduct.GTIN;
                                productDto.Name = gfsProduct.Name;
                                productDto.Height = gfsProduct.Height;
                                productDto.Width = gfsProduct.Width;
                                productDto.Depth = gfsProduct.Depth;
                                productDto.DisplayHeight = gfsProduct.DisplayHeight;
                                productDto.DisplayWidth = gfsProduct.DisplayWidth;
                                productDto.DisplayDepth = gfsProduct.DisplayDepth;
                                productDto.AlternateHeight = gfsProduct.AlternateHeight;
                                productDto.AlternateWidth = gfsProduct.AlternateWidth;
                                productDto.AlternateDepth = gfsProduct.AlternateDepth;
                                productDto.PointOfPurchaseHeight = gfsProduct.PointOfPurchaseHeight;
                                productDto.PointOfPurchaseWidth = gfsProduct.PointOfPurchaseWidth;
                                productDto.PointOfPurchaseDepth = gfsProduct.PointOfPurchaseDepth;
                                productDto.NumberOfPegHoles = gfsProduct.NumberOfPegHoles;
                                productDto.PegX = gfsProduct.PegX;
                                productDto.PegX2 = gfsProduct.PegX2;
                                productDto.PegX3 = gfsProduct.PegX3;
                                productDto.PegY = gfsProduct.PegY;
                                productDto.PegY2 = gfsProduct.PegY2;
                                productDto.PegY3 = gfsProduct.PegY3;
                                productDto.PegProngOffsetX = gfsProduct.PegProngOffset;
                                productDto.PegDepth = gfsProduct.PegDepth;
                                productDto.SqueezeHeight = gfsProduct.SqueezeHeight;
                                productDto.SqueezeWidth = gfsProduct.SqueezeWidth;
                                productDto.SqueezeDepth = gfsProduct.SqueezeDepth;
                                productDto.NestingHeight = gfsProduct.NestingHeight;
                                productDto.NestingWidth = gfsProduct.NestingWidth;
                                productDto.NestingDepth = gfsProduct.NestingDepth;
                                productDto.CasePackUnits = gfsProduct.CasePackUnits;
                                productDto.CaseHigh = gfsProduct.CaseHigh;
                                productDto.CaseWide = gfsProduct.CaseWide;
                                productDto.CaseDeep = gfsProduct.CaseDeep;
                                productDto.CaseHeight = gfsProduct.CaseHeight;
                                productDto.CaseWidth = gfsProduct.CaseWidth;
                                productDto.CaseDepth = gfsProduct.CaseDepth;
                                productDto.MaxStack = gfsProduct.MaxStack;
                                productDto.MaxTopCap = gfsProduct.MaxTopCap;
                                productDto.MaxRightCap = gfsProduct.MaxRightCap;
                                productDto.MinDeep = gfsProduct.MinDeep;
                                productDto.MaxDeep = gfsProduct.MaxDeep;
                                productDto.TrayPackUnits = gfsProduct.TrayPackUnits;
                                productDto.TrayHigh = gfsProduct.TrayHigh;
                                productDto.TrayWide = gfsProduct.TrayWide;
                                productDto.TrayDeep = gfsProduct.TrayDeep;
                                productDto.TrayHeight = gfsProduct.TrayHeight;
                                productDto.TrayWidth = gfsProduct.TrayWidth;
                                productDto.TrayDepth = gfsProduct.TrayDepth;
                                productDto.TrayThickHeight = gfsProduct.TrayThickHeight;
                                productDto.TrayThickWidth = gfsProduct.TrayThickWidth;
                                productDto.TrayThickDepth = gfsProduct.TrayThickDepth;
                                productDto.FrontOverhang = gfsProduct.FrontOverhang;
                                productDto.FingerSpaceAbove = gfsProduct.FingerSpaceAbove;
                                productDto.FingerSpaceToTheSide = gfsProduct.FingerSpaceToTheSide;
                                productDto.StatusType = (Byte)(EnumHelper.Parse<PlanogramProductStatusType>(gfsProduct.StatusType, PlanogramProductStatusType.Active));
                                productDto.OrientationType = (Byte)(EnumHelper.Parse<PlanogramProductOrientationType>(gfsProduct.OrientationType, PlanogramProductOrientationType.Front0));
                                productDto.MerchandisingStyle = (Byte)(EnumHelper.Parse<ProductMerchandisingStyle>(gfsProduct.MerchandisingStyle, ProductMerchandisingStyle.Unit));
                                productDto.IsFrontOnly = gfsProduct.IsFrontOnly;
                                productDto.IsPlaceHolderProduct = gfsProduct.IsPlaceHolderProduct;
                                productDto.IsActive = gfsProduct.IsActive;
                                productDto.CanBreakTrayUp = gfsProduct.CanBreakTrayUp;
                                productDto.CanBreakTrayDown = gfsProduct.CanBreakTrayDown;
                                productDto.CanBreakTrayBack = gfsProduct.CanBreakTrayBack;
                                productDto.CanBreakTrayTop = gfsProduct.CanBreakTrayTop;
                                productDto.ForceMiddleCap = gfsProduct.ForceMiddleCap;
                                productDto.ForceBottomCap = gfsProduct.ForceBottomCap;


                                // If we are inserting for the first time we give a default (fields do not exist in GFS).
                                // Otherwise we leave alone as may have changed in CCM by user.
                                if (isInitialInsert)
                                {
                                productDto.ShapeType = (Byte)PlanogramProductShapeType.Box;
                                productDto.FillColour = /*white*/-1;
                                productDto.FillPatternType = (Byte)PlanogramProductFillPatternType.Solid;
                                }


                                productDto.PointOfPurchaseDescription = gfsProduct.AttributeData.PointOfPurchaseDescription;
                                productDto.ShortDescription = gfsProduct.AttributeData.ShortDescription;
                                productDto.Subcategory = gfsProduct.AttributeData.SubCategory;
                                productDto.CustomerStatus = gfsProduct.AttributeData.CustomerStatus;
                                productDto.Colour = gfsProduct.AttributeData.Colour;
                                productDto.Flavour = gfsProduct.AttributeData.Flavour;
                                productDto.PackagingShape = gfsProduct.AttributeData.PackagingShape;
                                productDto.PackagingType = gfsProduct.AttributeData.PackagingType;
                                productDto.CountryOfOrigin = gfsProduct.AttributeData.CountryOfOrigin;
                                productDto.CountryOfProcessing = gfsProduct.AttributeData.CountryOfProcessing;
                                productDto.ShelfLife = gfsProduct.AttributeData.ShelfLife;
                                productDto.DeliveryFrequencyDays = gfsProduct.AttributeData.DeliveryFrequencyDays;
                                productDto.DeliveryMethod = gfsProduct.AttributeData.DeliveryMethod;
                                productDto.Brand = gfsProduct.AttributeData.Brand;
                                productDto.VendorCode = gfsProduct.AttributeData.VendorCode;
                                productDto.Vendor = gfsProduct.AttributeData.Vendor;
                                productDto.ManufacturerCode = gfsProduct.AttributeData.ManufacturerCode;
                                productDto.Manufacturer = gfsProduct.AttributeData.Manufacturer;
                                productDto.Size = gfsProduct.AttributeData.Size;
                                productDto.UnitOfMeasure = gfsProduct.AttributeData.UnitOfMeasure;
                                productDto.DateIntroduced = gfsProduct.AttributeData.DateIntroduced;
                                productDto.DateDiscontinued = gfsProduct.AttributeData.DateDiscontinued;
                                productDto.DateEffective = gfsProduct.AttributeData.DateEffective;
                                productDto.Health = gfsProduct.AttributeData.Health;
                                productDto.CorporateCode = gfsProduct.AttributeData.CorporateCode;
                                productDto.Barcode = gfsProduct.AttributeData.BarCode;
                                productDto.SellPrice = gfsProduct.AttributeData.SellPrice;
                                productDto.SellPackCount = gfsProduct.AttributeData.SellPackCount;
                                productDto.SellPackDescription = gfsProduct.AttributeData.SellPackDescription;
                                productDto.RecommendedRetailPrice = gfsProduct.AttributeData.RecommendedRetailPrice;
                                productDto.ManufacturerRecommendedRetailPrice = gfsProduct.AttributeData.ManufacturerRecommendedRetailPrice;
                                productDto.CostPrice = gfsProduct.AttributeData.CostPrice;
                                productDto.CaseCost = gfsProduct.AttributeData.CaseCost;
                                productDto.TaxRate = gfsProduct.AttributeData.TaxRate;
                                productDto.ConsumerInformation = gfsProduct.AttributeData.ConsumerInformation;
                                productDto.Texture = gfsProduct.AttributeData.Texture;
                                productDto.StyleNumber = gfsProduct.AttributeData.StyleNumber;
                                productDto.Pattern = gfsProduct.AttributeData.Pattern;
                                productDto.Model = gfsProduct.AttributeData.Model;
                                productDto.GarmentType = gfsProduct.AttributeData.GarmentType;
                                productDto.IsPrivateLabel = gfsProduct.AttributeData.IsPrivateLabel;
                                productDto.IsNewProduct = gfsProduct.AttributeData.IsNewProduct;
                                productDto.FinancialGroupCode = null;
                                productDto.FinancialGroupName = null;
                                if (prodGroup != null)
                                {
                                    productDto.FinancialGroupCode = prodGroup.Code;
                                    productDto.FinancialGroupName = prodGroup.Name;
                                }

                                productDtoList.Add(productDto);
                                #endregion

                                #region Product Attribute Data Insert/Update

                                //Variables to store the index we are at. Index start point will be 1!
                                Int32 textIndex = 1;
                                Int32 flagIndex = 1;
                                Int32 singleIndex = 1;
                                //This represents the index of the gfs property we are setting
                                Int32 gfsAttributePropertyIndex = 1;

                                //gfsProductAttributesForEntity contains details for all the custom attributes that are available in gfs and theirdatatype.
                                //The type off property to be loaded will be the next available free property for the specific data type.
                                foreach (Services.Product.ProductAttribute currentProductAttributeToProcess in gfsProductAttributesForEntity)
                                {
                                    ProductAttributeDataType? currentDataType = (ProductAttributeDataType?)Enum.Parse(typeof(ProductAttributeDataType), currentProductAttributeToProcess.DataType);

                                    //!Important: The load methods do not set the parent ids etc. This must be done later.
                                    if (currentDataType != null)
                                    {
                                        switch (currentDataType)
                                        {
                                            //We must send through the appropriate property based on index of the gfsAttributePropertyIndex which shows which gfs property we are at:
                                            case ProductAttributeDataType.Text:
                                                switch (gfsAttributePropertyIndex)
                                                {
                                                    #region Text properties

                                                    case 1:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute01, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 2:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute02, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 3:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute03, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 4:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute04, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 5:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute05, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 6:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute06, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 7:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute07, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 8:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute08, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 9:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute09, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 10:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute10, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 11:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute11, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 12:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute12, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 13:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute13, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 14:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute14, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 15:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute15, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 16:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute16, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 17:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute17, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 18:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute18, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 19:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute19, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 20:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute20, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 21:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute21, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 22:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute22, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 23:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute23, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 24:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute24, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 25:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute25, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 26:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute26, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 27:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute27, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 28:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute28, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 29:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute29, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 30:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute30, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 31:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute31, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 32:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute32, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 33:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute33, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 34:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute34, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 35:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute35, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 36:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute36, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 37:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute37, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 38:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute38, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 39:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute39, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 40:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute40, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 41:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute41, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 42:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute42, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 43:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute43, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 44:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute44, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 45:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute45, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 46:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute46, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 47:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute47, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 48:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute48, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 49:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute49, CultureInfo.InvariantCulture));
                                                        break;
                                                    case 50:
                                                        attributeDataDto = LoadNextCustomAttributeTextValueForGivenIndex(attributeDataDto, textIndex, Convert.ToString(gfsProduct.AttributeData.CustomAttribute50, CultureInfo.InvariantCulture));
                                                        break;
                                                    default:
                                                        break;

                                                    #endregion
                                                }
                                                textIndex++;
                                                break;

                                            case ProductAttributeDataType.Currency:
                                            case ProductAttributeDataType.Decimal:
                                            case ProductAttributeDataType.Integer:
                                                switch (gfsAttributePropertyIndex)
                                                {
                                                    #region Single properties

                                                    case 1:
                                                        if (gfsProduct.AttributeData.CustomAttribute01 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute01));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 2:
                                                        if (gfsProduct.AttributeData.CustomAttribute02 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute02));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 3:
                                                        if (gfsProduct.AttributeData.CustomAttribute03 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute03));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 4:
                                                        if (gfsProduct.AttributeData.CustomAttribute04 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute04));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 5:
                                                        if (gfsProduct.AttributeData.CustomAttribute05 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute05));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 6:
                                                        if (gfsProduct.AttributeData.CustomAttribute06 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute06));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 7:
                                                        if (gfsProduct.AttributeData.CustomAttribute07 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute07));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 8:
                                                        if (gfsProduct.AttributeData.CustomAttribute08 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute08));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 9:
                                                        if (gfsProduct.AttributeData.CustomAttribute09 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute09));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 10:
                                                        if (gfsProduct.AttributeData.CustomAttribute10 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute10));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 11:
                                                        if (gfsProduct.AttributeData.CustomAttribute11 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute11));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 12:
                                                        if (gfsProduct.AttributeData.CustomAttribute12 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute12));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 13:
                                                        if (gfsProduct.AttributeData.CustomAttribute13 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute13));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 14:
                                                        if (gfsProduct.AttributeData.CustomAttribute14 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute14));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 15:
                                                        if (gfsProduct.AttributeData.CustomAttribute15 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute15));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 16:
                                                        if (gfsProduct.AttributeData.CustomAttribute16 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute16));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 17:
                                                        if (gfsProduct.AttributeData.CustomAttribute17 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute17));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 18:
                                                        if (gfsProduct.AttributeData.CustomAttribute18 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute18));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 19:
                                                        if (gfsProduct.AttributeData.CustomAttribute19 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute19));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 20:
                                                        if (gfsProduct.AttributeData.CustomAttribute20 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute20));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 21:
                                                        if (gfsProduct.AttributeData.CustomAttribute21 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute21));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 22:
                                                        if (gfsProduct.AttributeData.CustomAttribute22 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute22));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 23:
                                                        if (gfsProduct.AttributeData.CustomAttribute23 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute23));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 24:
                                                        if (gfsProduct.AttributeData.CustomAttribute24 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute24));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 25:
                                                        if (gfsProduct.AttributeData.CustomAttribute25 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute25));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 26:
                                                        if (gfsProduct.AttributeData.CustomAttribute26 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute26));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 27:
                                                        if (gfsProduct.AttributeData.CustomAttribute27 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute27));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 28:
                                                        if (gfsProduct.AttributeData.CustomAttribute28 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute28));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 29:
                                                        if (gfsProduct.AttributeData.CustomAttribute29 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute29));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 30:
                                                        if (gfsProduct.AttributeData.CustomAttribute30 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute30));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 31:
                                                        if (gfsProduct.AttributeData.CustomAttribute31 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute31));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 32:
                                                        if (gfsProduct.AttributeData.CustomAttribute32 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute32));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 33:
                                                        if (gfsProduct.AttributeData.CustomAttribute33 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute33));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 34:
                                                        if (gfsProduct.AttributeData.CustomAttribute34 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute34));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 35:
                                                        if (gfsProduct.AttributeData.CustomAttribute35 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute35));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 36:
                                                        if (gfsProduct.AttributeData.CustomAttribute36 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute36));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 37:
                                                        if (gfsProduct.AttributeData.CustomAttribute37 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute37));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 38:
                                                        if (gfsProduct.AttributeData.CustomAttribute38 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute38));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 39:
                                                        if (gfsProduct.AttributeData.CustomAttribute39 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute39));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 40:
                                                        if (gfsProduct.AttributeData.CustomAttribute40 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute40));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 41:
                                                        if (gfsProduct.AttributeData.CustomAttribute41 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute41));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 42:
                                                        if (gfsProduct.AttributeData.CustomAttribute42 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute42));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 43:
                                                        if (gfsProduct.AttributeData.CustomAttribute43 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute43));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 44:
                                                        if ((Single)gfsProduct.AttributeData.CustomAttribute44 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute44));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 45:
                                                        if ((Single)gfsProduct.AttributeData.CustomAttribute45 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute45));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 46:
                                                        if (gfsProduct.AttributeData.CustomAttribute46 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute46));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 47:
                                                        if (gfsProduct.AttributeData.CustomAttribute47 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute47));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 48:
                                                        if (gfsProduct.AttributeData.CustomAttribute48 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute48));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 49:
                                                        if (gfsProduct.AttributeData.CustomAttribute49 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute49));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    case 50:
                                                        if (gfsProduct.AttributeData.CustomAttribute50 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, Convert.ToSingle(gfsProduct.AttributeData.CustomAttribute50));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeSingleValueForGivenIndex(attributeDataDto, singleIndex, 0);
                                                        }
                                                        break;
                                                    default:
                                                        break;

                                                    #endregion
                                                }
                                                singleIndex++;
                                                break;

                                            case ProductAttributeDataType.Boolean:
                                                switch (gfsAttributePropertyIndex)
                                                {
                                                    #region Boolean properties

                                                    case 1:
                                                        if (gfsProduct.AttributeData.CustomAttribute01 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute01 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 2:
                                                        if (gfsProduct.AttributeData.CustomAttribute02 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute02 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 3:
                                                        if (gfsProduct.AttributeData.CustomAttribute03 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute03 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 4:
                                                        if (gfsProduct.AttributeData.CustomAttribute04 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute04 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 5:
                                                        if (gfsProduct.AttributeData.CustomAttribute05 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute05 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 6:
                                                        if (gfsProduct.AttributeData.CustomAttribute06 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute06 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 7:
                                                        if (gfsProduct.AttributeData.CustomAttribute07 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute07 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 8:
                                                        if (gfsProduct.AttributeData.CustomAttribute08 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute08 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 9:
                                                        if (gfsProduct.AttributeData.CustomAttribute09 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute09 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 10:
                                                        if (gfsProduct.AttributeData.CustomAttribute10 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute10 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 11:
                                                        if (gfsProduct.AttributeData.CustomAttribute11 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute11 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 12:
                                                        if (gfsProduct.AttributeData.CustomAttribute12 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute12 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 13:
                                                        if (gfsProduct.AttributeData.CustomAttribute13 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute13 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 14:
                                                        if (gfsProduct.AttributeData.CustomAttribute14 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute14 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 15:
                                                        if (gfsProduct.AttributeData.CustomAttribute15 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute15 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 16:
                                                        if (gfsProduct.AttributeData.CustomAttribute16 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute16 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 17:
                                                        if (gfsProduct.AttributeData.CustomAttribute17 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute17 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 18:
                                                        if (gfsProduct.AttributeData.CustomAttribute18 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute18 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 19:
                                                        if (gfsProduct.AttributeData.CustomAttribute19 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute19 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 20:
                                                        if (gfsProduct.AttributeData.CustomAttribute20 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute20 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 21:
                                                        if (gfsProduct.AttributeData.CustomAttribute21 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute21 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 22:
                                                        if (gfsProduct.AttributeData.CustomAttribute22 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute22 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 23:
                                                        if (gfsProduct.AttributeData.CustomAttribute23 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute23 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 24:
                                                        if (gfsProduct.AttributeData.CustomAttribute24 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute24 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 25:
                                                        if (gfsProduct.AttributeData.CustomAttribute25 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute25 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 26:
                                                        if (gfsProduct.AttributeData.CustomAttribute26 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute26 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 27:
                                                        if (gfsProduct.AttributeData.CustomAttribute27 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute27 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 28:
                                                        if (gfsProduct.AttributeData.CustomAttribute28 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute28 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 29:
                                                        if (gfsProduct.AttributeData.CustomAttribute29 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute29 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 30:
                                                        if (gfsProduct.AttributeData.CustomAttribute30 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute30 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 31:
                                                        if (gfsProduct.AttributeData.CustomAttribute31 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute31 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 32:
                                                        if (gfsProduct.AttributeData.CustomAttribute32 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute32 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 33:
                                                        if (gfsProduct.AttributeData.CustomAttribute33 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute33 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 34:
                                                        if (gfsProduct.AttributeData.CustomAttribute34 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute34 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 35:
                                                        if (gfsProduct.AttributeData.CustomAttribute35 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute35 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 36:
                                                        if (gfsProduct.AttributeData.CustomAttribute36 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute36 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 37:
                                                        if (gfsProduct.AttributeData.CustomAttribute37 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute37 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 38:
                                                        if (gfsProduct.AttributeData.CustomAttribute38 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute38 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 39:
                                                        if (gfsProduct.AttributeData.CustomAttribute39 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute39 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 40:
                                                        if (gfsProduct.AttributeData.CustomAttribute40 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute40 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 41:
                                                        if (gfsProduct.AttributeData.CustomAttribute41 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute41 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 42:
                                                        if (gfsProduct.AttributeData.CustomAttribute42 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute42 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 43:
                                                        if (gfsProduct.AttributeData.CustomAttribute43 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute43 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 44:
                                                        if (gfsProduct.AttributeData.CustomAttribute44 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute44 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 45:
                                                        if (gfsProduct.AttributeData.CustomAttribute45 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute45 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 46:
                                                        if (gfsProduct.AttributeData.CustomAttribute46 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute46 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 47:
                                                        if (gfsProduct.AttributeData.CustomAttribute47 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute47 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 48:
                                                        if (gfsProduct.AttributeData.CustomAttribute48 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute48 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 49:
                                                        if (gfsProduct.AttributeData.CustomAttribute49 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute49 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    case 50:
                                                        if (gfsProduct.AttributeData.CustomAttribute50 != null)
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, (gfsProduct.AttributeData.CustomAttribute50 != "0.0"));
                                                        }
                                                        else
                                                        {
                                                            attributeDataDto = LoadNextCustomAttributeFlagValueForGivenIndex(attributeDataDto, flagIndex, false);
                                                        }
                                                        break;
                                                    default:
                                                        break;

                                                    #endregion
                                                }
                                                flagIndex++;
                                                break;

                                            default:
                                                //This should never get reached. But if does, lets not set any value.
                                                break;
                                        }
                                    }

                                    gfsAttributePropertyIndex++;
                                }

                                attributeDataDtoList.Add(attributeDataDto);
                                #endregion
                            }

                            dal.Upsert(productDtoList, new ProductIsSetDto(true));

                            //update product ids to the attribute dtos
                            for (Int32 j = 0; j < productDtoList.Count; j++)
                            {
                                ProductDto productDto = productDtoList[j];
                                CustomAttributeDataDto attributeDto = attributeDataDtoList[j];

                                attributeDto.ParentType = (Byte)CustomAttributeDataParentType.Product;
                                attributeDto.ParentId = productDto.Id;
                            }

                            //Because we have gone through and set all properties from clear for CustomAttribute it is safe to call upsert instead of Delete & Insert.
                            attributeDataDal.Upsert(attributeDataDtoList, new CustomAttributeDataIsSetDto(true));

                            // clear the dto lists
                            productDtoList.Clear();
                            attributeDataDtoList.Clear();
                        }

                        IProductInfoDal infodal = context.DalContext.GetDal<IProductInfoDal>();

                        //Map product Gtins's between GFS and CCM
                        foreach (ProductInfoDto ccmProduct in infodal.FetchByEntityId(this.EntityId))
                        {
                            context.ProductMap.Add(ccmProduct.Gtin, ccmProduct.Id);
                        }

                        //now delete the deleted products
                        List<String> gtinsToDelete = new List<String>();
                        foreach (Services.Product.ProductInfo delProduct in deletedGfsProductInfo)
                        {
                            gtinsToDelete.Add(delProduct.GTIN);
                        }

                        foreach (ProductDto delProduct in dal.FetchByEntityIdProductGtins(this.EntityId, gtinsToDelete))
                        {
                            dal.DeleteById(delProduct.Id);
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Product], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region CustomAttributeHelperMethods (ProductSync)

        private CustomAttributeDataDto LoadNextCustomAttributeTextValueForGivenIndex(CustomAttributeDataDto attibuteToUpdate, Int32 index, String value)
        {
            CustomAttributeDataDto attributeToReturn = attibuteToUpdate;

            switch (index)
            {
                case 1:
                    attributeToReturn.Text1 = value;
                    break;
                case 2:
                    attributeToReturn.Text2 = value;
                    break;
                case 3:
                    attributeToReturn.Text3 = value;
                    break;
                case 4:
                    attributeToReturn.Text4 = value;
                    break;
                case 5:
                    attributeToReturn.Text5 = value;
                    break;
                case 6:
                    attributeToReturn.Text6 = value;
                    break;
                case 7:
                    attributeToReturn.Text7 = value;
                    break;
                case 8:
                    attributeToReturn.Text8 = value;
                    break;
                case 9:
                    attributeToReturn.Text9 = value;
                    break;
                case 10:
                    attributeToReturn.Text10 = value;
                    break;
                case 11:
                    attributeToReturn.Text11 = value;
                    break;
                case 12:
                    attributeToReturn.Text12 = value;
                    break;
                case 13:
                    attributeToReturn.Text13 = value;
                    break;
                case 14:
                    attributeToReturn.Text14 = value;
                    break;
                case 15:
                    attributeToReturn.Text15 = value;
                    break;
                case 16:
                    attributeToReturn.Text16 = value;
                    break;
                case 17:
                    attributeToReturn.Text17 = value;
                    break;
                case 18:
                    attributeToReturn.Text18 = value;
                    break;
                case 19:
                    attributeToReturn.Text19 = value;
                    break;
                case 20:
                    attributeToReturn.Text20 = value;
                    break;
                case 21:
                    attributeToReturn.Text21 = value;
                    break;
                case 22:
                    attributeToReturn.Text22 = value;
                    break;
                case 23:
                    attributeToReturn.Text23 = value;
                    break;
                case 24:
                    attributeToReturn.Text24 = value;
                    break;
                case 25:
                    attributeToReturn.Text25 = value;
                    break;
                case 26:
                    attributeToReturn.Text26 = value;
                    break;
                case 27:
                    attributeToReturn.Text27 = value;
                    break;
                case 28:
                    attributeToReturn.Text28 = value;
                    break;
                case 29:
                    attributeToReturn.Text29 = value;
                    break;
                case 30:
                    attributeToReturn.Text30 = value;
                    break;
                case 31:
                    attributeToReturn.Text31 = value;
                    break;
                case 32:
                    attributeToReturn.Text32 = value;
                    break;
                case 33:
                    attributeToReturn.Text33 = value;
                    break;
                case 34:
                    attributeToReturn.Text34 = value;
                    break;
                case 35:
                    attributeToReturn.Text35 = value;
                    break;
                case 36:
                    attributeToReturn.Text36 = value;
                    break;
                case 37:
                    attributeToReturn.Text37 = value;
                    break;
                case 38:
                    attributeToReturn.Text38 = value;
                    break;
                case 39:
                    attributeToReturn.Text39 = value;
                    break;
                case 40:
                    attributeToReturn.Text40 = value;
                    break;
                case 41:
                    attributeToReturn.Text41 = value;
                    break;
                case 42:
                    attributeToReturn.Text42 = value;
                    break;
                case 43:
                    attributeToReturn.Text43 = value;
                    break;
                case 44:
                    attributeToReturn.Text44 = value;
                    break;
                case 45:
                    attributeToReturn.Text45 = value;
                    break;
                case 46:
                    attributeToReturn.Text46 = value;
                    break;
                case 47:
                    attributeToReturn.Text47 = value;
                    break;
                case 48:
                    attributeToReturn.Text48 = value;
                    break;
                case 49:
                    attributeToReturn.Text49 = value;
                    break;
                case 50:
                    attributeToReturn.Text50 = value;
                    break;
                default:
                    break;
            }

            return attributeToReturn;
        }

        private CustomAttributeDataDto LoadNextCustomAttributeSingleValueForGivenIndex(CustomAttributeDataDto attibuteToUpdate, Int32 index, Single value)
        {
            CustomAttributeDataDto attributeToReturn = attibuteToUpdate;

            switch (index)
            {
                case 1:
                    attributeToReturn.Value1 = value;
                    break;
                case 2:
                    attributeToReturn.Value2 = value;
                    break;
                case 3:
                    attributeToReturn.Value3 = value;
                    break;
                case 4:
                    attributeToReturn.Value4 = value;
                    break;
                case 5:
                    attributeToReturn.Value5 = value;
                    break;
                case 6:
                    attributeToReturn.Value6 = value;
                    break;
                case 7:
                    attributeToReturn.Value7 = value;
                    break;
                case 8:
                    attributeToReturn.Value8 = value;
                    break;
                case 9:
                    attributeToReturn.Value9 = value;
                    break;
                case 10:
                    attributeToReturn.Value10 = value;
                    break;
                case 11:
                    attributeToReturn.Value11 = value;
                    break;
                case 12:
                    attributeToReturn.Value12 = value;
                    break;
                case 13:
                    attributeToReturn.Value13 = value;
                    break;
                case 14:
                    attributeToReturn.Value14 = value;
                    break;
                case 15:
                    attributeToReturn.Value15 = value;
                    break;
                case 16:
                    attributeToReturn.Value16 = value;
                    break;
                case 17:
                    attributeToReturn.Value17 = value;
                    break;
                case 18:
                    attributeToReturn.Value18 = value;
                    break;
                case 19:
                    attributeToReturn.Value19 = value;
                    break;
                case 20:
                    attributeToReturn.Value20 = value;
                    break;
                case 21:
                    attributeToReturn.Value21 = value;
                    break;
                case 22:
                    attributeToReturn.Value22 = value;
                    break;
                case 23:
                    attributeToReturn.Value23 = value;
                    break;
                case 24:
                    attributeToReturn.Value24 = value;
                    break;
                case 25:
                    attributeToReturn.Value25 = value;
                    break;
                case 26:
                    attributeToReturn.Value26 = value;
                    break;
                case 27:
                    attributeToReturn.Value27 = value;
                    break;
                case 28:
                    attributeToReturn.Value28 = value;
                    break;
                case 29:
                    attributeToReturn.Value29 = value;
                    break;
                case 30:
                    attributeToReturn.Value30 = value;
                    break;
                case 31:
                    attributeToReturn.Value31 = value;
                    break;
                case 32:
                    attributeToReturn.Value32 = value;
                    break;
                case 33:
                    attributeToReturn.Value33 = value;
                    break;
                case 34:
                    attributeToReturn.Value34 = value;
                    break;
                case 35:
                    attributeToReturn.Value35 = value;
                    break;
                case 36:
                    attributeToReturn.Value36 = value;
                    break;
                case 37:
                    attributeToReturn.Value37 = value;
                    break;
                case 38:
                    attributeToReturn.Value38 = value;
                    break;
                case 39:
                    attributeToReturn.Value39 = value;
                    break;
                case 40:
                    attributeToReturn.Value40 = value;
                    break;
                case 41:
                    attributeToReturn.Value41 = value;
                    break;
                case 42:
                    attributeToReturn.Value42 = value;
                    break;
                case 43:
                    attributeToReturn.Value43 = value;
                    break;
                case 44:
                    attributeToReturn.Value44 = value;
                    break;
                case 45:
                    attributeToReturn.Value45 = value;
                    break;
                case 46:
                    attributeToReturn.Value46 = value;
                    break;
                case 47:
                    attributeToReturn.Value47 = value;
                    break;
                case 48:
                    attributeToReturn.Value48 = value;
                    break;
                case 49:
                    attributeToReturn.Value49 = value;
                    break;
                case 50:
                    attributeToReturn.Value50 = value;
                    break;
                default:
                    break;
            }

            return attributeToReturn;
        }

        private CustomAttributeDataDto LoadNextCustomAttributeFlagValueForGivenIndex(CustomAttributeDataDto attibuteToUpdate, Int32 index, Boolean value)
        {
            CustomAttributeDataDto attributeToReturn = attibuteToUpdate;

            switch (index)
            {
                case 1:
                    attributeToReturn.Flag1 = value;
                    break;
                case 2:
                    attributeToReturn.Flag2 = value;
                    break;
                case 3:
                    attributeToReturn.Flag3 = value;
                    break;
                case 4:
                    attributeToReturn.Flag4 = value;
                    break;
                case 5:
                    attributeToReturn.Flag5 = value;
                    break;
                case 6:
                    attributeToReturn.Flag6 = value;
                    break;
                case 7:
                    attributeToReturn.Flag7 = value;
                    break;
                case 8:
                    attributeToReturn.Flag8 = value;
                    break;
                case 9:
                    attributeToReturn.Flag9 = value;
                    break;
                case 10:
                    attributeToReturn.Flag10 = value;
                    break;
                default:
                    //If in here we have more indexes than available within 'CustomAttributes' for boolean properties. 
                    //At the moment we will just ignore theese extra flag properties. - Not Synced.
                    break;
            }

            return attributeToReturn;
        }

        #endregion

        #endregion

        #region Products Universe

        /// <summary>
        /// Syncrhonises the product universe within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="dtolist">List of the Entities</param>
        private void SyncrhonizeProductUniverse(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                ProductUniverseProductDto ccmProductUniverseProduct = null;

                using (IProductDal dalProduct = context.DalContext.GetDal<IProductDal>())
                {
                    using (IProductUniverseDal dalProductUniverse = context.DalContext.GetDal<IProductUniverseDal>())
                    {
                        using (IProductUniverseProductDal dalProductUniverseProduct = context.DalContext.GetDal<IProductUniverseProductDal>())
                        {
                            List<Services.Product.ProductUniverseInfo> gfsProductUniverseInfo = null;

                            //Get the product universe info 
                            context.DoWorkWithRetries(delegate ()
                            {
                                gfsProductUniverseInfo = context.Services.ProductServiceClient.GetProductUniverseInfoByEntityId(
                                    new Services.Product.GetProductUniverseInfoByEntityIdRequest(this.GFSEntityId)).ProductUniverseInfos;
                            });

                            //Fetch all product universes
                            IEnumerable<ProductUniverseDto> productUniverseList = dalProductUniverse.FetchByEntityId(this.EntityId);

                            // Loop through the product universe info list previously 
                            // created to generate the product universe Ids for the product fetch.
                            foreach (Services.Product.ProductUniverseInfo prodUniverse in gfsProductUniverseInfo)
                            {
                                // check if it is the latest version
                                if (prodUniverse.IsLatestVersion == true)
                                {
                                    Services.Product.ProductUniverse gfsProductUniverse = null;
                                    

                                    //Find CCM copy of the universe (if exists)
                                    Guid ucr;
                                    if (prodUniverse.ParentUniqueContentReference != null)
                                    {
                                        ucr = prodUniverse.ParentUniqueContentReference.Value;
                                    }
                                    else
                                    {
                                        ucr = prodUniverse.UniqueContentReference;
                                    }
                                    ProductUniverseDto ccmProductUniverse = productUniverseList.FirstOrDefault(i => i.ParentUniqueContentReference == ucr);

                                    //find one with the same name
                                    ProductUniverseDto ccmDuplicateProductUniverse = productUniverseList.FirstOrDefault(i => (i.Name == prodUniverse.Name && i.ParentUniqueContentReference == null));

                                    if (ccmDuplicateProductUniverse != null)
                                    {
                                        ccmDuplicateProductUniverse.Name += " (Local Copy)";
                                        dalProductUniverse.Update(ccmDuplicateProductUniverse);
                                    }

                                    if (ccmProductUniverse == null ||
                                        ccmProductUniverse.UniqueContentReference != prodUniverse.UniqueContentReference
                                        || SyncTarget.LastSync == null ||
                                        (prodUniverse.DateLastModified != null && SyncTarget.LastSync != null && prodUniverse.DateLastModified > SyncTarget.LastSync.Value))
                                    {
                                        context.DoWorkWithRetries(delegate ()
                                        {
                                            gfsProductUniverse = context.Services.ProductServiceClient.GetProductUniverseByUniverseId(
                                                new Services.Product.GetProductUniverseByUniverseIdRequest(prodUniverse.Id)).ProductUniverse;
                                        });

                                        //Lookup product group id
                                        Int32 productGroupId;
                                        if (context.ProductGroupMap.TryGetValue(gfsProductUniverse.ProductGroupCode, out productGroupId))
                                        {
                                            if (productGroupId > 0)
                                            {
                                                #region INSERT/UPDATE Product Universe
                                                Boolean updateChildren = false;
                                                if (ccmProductUniverse == null)
                                                {
                                                    //INSERT Library
                                                    ccmProductUniverse = new ProductUniverseDto();

                                                    ccmProductUniverse.EntityId = this.EntityId;
                                                    ccmProductUniverse.ProductGroupId = productGroupId;
                                                    ccmProductUniverse.Name = gfsProductUniverse.Name;
                                                    ccmProductUniverse.DateCreated = DateTime.Now;
                                                    ccmProductUniverse.DateLastModified = DateTime.Now;
                                                    ccmProductUniverse.IsMaster = false;
                                                    ccmProductUniverse.UniqueContentReference = gfsProductUniverse.UniqueContentReference;

                                                    if (gfsProductUniverse.ParentUniqueContentReference != null)
                                                    {
                                                        ccmProductUniverse.ParentUniqueContentReference = gfsProductUniverse.ParentUniqueContentReference;
                                                    }
                                                    else
                                                    {
                                                        ccmProductUniverse.ParentUniqueContentReference = gfsProductUniverse.UniqueContentReference;
                                                    }
                                                    dalProductUniverse.Insert(ccmProductUniverse);
                                                    updateChildren = true;
                                                }
                                                else
                                                {
                                                    //UPDATE Product Universe
                                                    ccmProductUniverse.UniqueContentReference = gfsProductUniverse.UniqueContentReference;
                                                    ccmProductUniverse.DateLastModified = DateTime.Now;
                                                    ccmProductUniverse.Name = gfsProductUniverse.Name;
                                                    ccmProductUniverse.ProductGroupId = productGroupId;
                                                    if (gfsProductUniverse.ParentUniqueContentReference != null)
                                                    {
                                                        ccmProductUniverse.ParentUniqueContentReference = gfsProductUniverse.ParentUniqueContentReference;
                                                    }
                                                    else
                                                    {
                                                        ccmProductUniverse.ParentUniqueContentReference = gfsProductUniverse.UniqueContentReference;
                                                    }

                                                    dalProductUniverse.Update(ccmProductUniverse);
                                                    updateChildren = true;
                                                }
                                                #endregion
                                                if (updateChildren)
                                                {
                                                    #region INSERT/UPDATE Product Universe Products

                                                    List<String> productGTINs = new List<String>();

                                                    foreach (Services.Product.ProductUniverseProduct product in gfsProductUniverse.Products)
                                                    {
                                                        productGTINs.Add(product.GTIN);
                                                    }

                                                    IEnumerable<ProductDto> ccmProducts = dalProduct.FetchByEntityIdProductGtins(this.EntityId, productGTINs);

                                                    // create a new list of product universe products to upsert
                                                    List<ProductUniverseProductDto> productUniverseProductDtoList = new List<ProductUniverseProductDto>();

                                                    // loop through each product and add to the dto list
                                                    foreach (ProductDto prod in ccmProducts)
                                                    {
                                                        #region Create Products

                                                        ccmProductUniverseProduct = new ProductUniverseProductDto();

                                                        ccmProductUniverseProduct.Gtin = prod.Gtin;
                                                        ccmProductUniverseProduct.Name = prod.Name;
                                                        ccmProductUniverseProduct.ProductId = prod.Id;
                                                        ccmProductUniverseProduct.ProductUniverseId = ccmProductUniverse.Id;

                                                        productUniverseProductDtoList.Add(ccmProductUniverseProduct);
                                                        #endregion
                                                    }

                                                    if (ccmProductUniverse != null)
                                                    {
                                                        // unassign any products deleted in GFS
                                                        IEnumerable<ProductUniverseProductDto> ccmExistingProducts = dalProductUniverseProduct.FetchByProductUniverseId(ccmProductUniverse.Id);

                                                        foreach (ProductUniverseProductDto prod in ccmExistingProducts.Where(p => !productUniverseProductDtoList.Any(p2 => p2.ProductId == p.ProductId)))
                                                        {
                                                            dalProductUniverseProduct.DeleteById(prod.Id);
                                                        }
                                                    }

                                                    // upsert the list in to the db
                                                    dalProductUniverseProduct.Upsert(productUniverseProductDtoList, new ProductUniverseProductIsSetDto(true));

                                                    // clear the dto list
                                                    productUniverseProductDtoList.Clear();
                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.ProductUnivese], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Product Universe Deletes

        private void SynchronizeProductUniverseDeleted(ProcessContext context)
        {
            // Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IProductUniverseDal dalCdt = context.DalContext.GetDal<IProductUniverseDal>())
                {
                    #region old delete
                    //List<Services.Product.ProductUniverseInfo> gfsProductUniversesDeleted = null;
                    // Ensure that it has sync'd before
                    //if (SyncTarget.LastSync != null)
                    //{
                    //    // Fetch the deleted items
                    //    context.DoWorkWithRetries(delegate()
                    //    {
                    //        gfsProductUniversesDeleted = context.Services.ProductServiceClient.GetProductUniverseInfoByEntityIdDateDeleted(
                    //            new Services.Product.GetProductUniverseInfoByEntityIdDateDeletedRequest(this.GFSEntityId, SyncTarget.LastSync)).ProductUniverseInfos;
                    //    });

                    //    IEnumerable<ProductUniverseInfoDto> ccmProductUniverses;
                    //    using (IProductUniverseInfoDal dalProductUniverseInfo = context.DalContext.GetDal<IProductUniverseInfoDal>())
                    //    {
                    //        ccmProductUniverses = dalProductUniverseInfo.FetchByEntityId(this.EntityId);
                    //    }

                    //    // loop through the items to delete
                    //    foreach (Services.Product.ProductUniverseInfo cdt in gfsProductUniversesDeleted)
                    //    {
                    //        // check if item exists in CCM
                    //        ProductUniverseInfoDto ccmItem = ccmProductUniverses.FirstOrDefault(i => i.UniqueContentReference == cdt.UniqueContentReference);

                    //        if (ccmItem != null)
                    //        {
                    //            dalCdt.DeleteById(ccmItem.Id);
                    //        }
                    //    }
                    //}




                    #endregion

                    List<Services.Product.ProductUniverseInfo> gfsProductUniverseInfo = null;

                    //fetch all universes
                    context.DoWorkWithRetries(delegate ()
                    {
                        gfsProductUniverseInfo = context.Services.ProductServiceClient.GetProductUniverseInfoByEntityId(
                            new Services.Product.GetProductUniverseInfoByEntityIdRequest(this.GFSEntityId)).ProductUniverseInfos;
                    });

                    //fetch ccm universes
                    IEnumerable<ProductUniverseDto> productUniverseList = dalCdt.FetchByEntityId(this.EntityId);

                    //loop through ccm product universes
                    foreach (ProductUniverseDto ccmProductUniverse in productUniverseList)
                    {
                        //came from a sync
                        if (ccmProductUniverse.ParentUniqueContentReference != null)
                        {
                            //try to find the gfs product universe
                            Services.Product.ProductUniverseInfo gfsUniverseInfo = gfsProductUniverseInfo.FirstOrDefault(u => u.UniqueContentReference == ccmProductUniverse.UniqueContentReference);

                            //doesn't exist in gfs so delete it
                            if (gfsUniverseInfo == null)
                            {
                                dalCdt.DeleteById(ccmProductUniverse.Id);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.ProductUnivese], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Location Hierarchy

        /// <summary>
        /// Syncrhonises the location hierarchy within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="dtolist">List of the Entities</param>
        private void SynchronizeLocationHierarchy(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                context.LocationGroupMap.Clear();
                context.LocationLevelMap.Clear();

                #region CCM Location hierachy
                LocationHierarchyDto dtoHierarchyStructure;

                using (ILocationHierarchyDal dal = context.DalContext.GetDal<ILocationHierarchyDal>())
                {
                    dtoHierarchyStructure = dal.FetchByEntityId(this.EntityId);
                }
                #endregion

                //Call the Web Service For the Location Hierachy
                Services.Location.LocationHierarchy gfsLocationHierarchy = null;
                context.DoWorkWithRetries(delegate ()
                {
                    gfsLocationHierarchy = context.Services.LocationServiceClient.GetLocationHierarchyByEntityName(
                        new Services.Location.GetLocationHierarchyByEntityNameRequest(this.EntityName)).LocationHierarchy;
                });

                #region CCM Location level

                List<LocationLevelDto> hierarchyLevels = new List<LocationLevelDto>();

                using (ILocationLevelDal dalLevel = context.DalContext.GetDal<ILocationLevelDal>())
                {
                    //Fetch the CCM Location Levels
                    List<LocationLevelDto> ccmLocationLevelDtos = dalLevel.FetchByLocationHierarchyId(dtoHierarchyStructure.Id).ToList();

                    #region Order CCM location levels
                    //Find & add root level
                    LocationLevelDto ccmLocationLevelDto = ccmLocationLevelDtos.FirstOrDefault(p => p.ParentLevelId == null);
                    hierarchyLevels.Add(ccmLocationLevelDto);

                    //While a child level is still being found
                    while (ccmLocationLevelDto != null)
                    {
                        //Select child of the current level dto
                        ccmLocationLevelDto = ccmLocationLevelDtos.FirstOrDefault(p => p.ParentLevelId.Equals(ccmLocationLevelDto.Id));

                        //If child object is found
                        if (ccmLocationLevelDto != null)
                        {
                            //Add to levels
                            hierarchyLevels.Add(ccmLocationLevelDto);
                        }
                    }
                    #endregion

                    //Set the Location level from the web-service
                    Services.Location.LocationLevel level;
                    level = gfsLocationHierarchy.RootLevel;

                    //Get all levels in hierarchy from the web service
                    List<Services.Location.LocationLevel> gfsLocationHierarchyLevels = new List<Services.Location.LocationLevel>();
                    //move down through the levels adding each one
                    Services.Location.LocationLevel currentLevel = gfsLocationHierarchy.RootLevel;
                    while (currentLevel != null)
                    {
                        gfsLocationHierarchyLevels.Add(currentLevel);
                        currentLevel = currentLevel.ChildLevel;
                    }

                    Int32 levelCount = 0;

                    LocationLevelDto ccmLevel;
                    ccmLevel = hierarchyLevels[levelCount];

                    #region Insert/Update level
                    while (level != null)
                    {
                        if (ccmLevel == null)
                        {
                            ccmLevel = new LocationLevelDto();

                            //Find parent
                            Services.Location.LocationLevel parentLevel = gfsLocationHierarchyLevels.FirstOrDefault(p => p.ChildLevel.Id == level.Id);

                            ccmLevel.Name = level.Name;
                            ccmLevel.Colour = level.Colour;
                            ccmLevel.ShapeNo = 1;
                            ccmLevel.LocationHierarchyId = dtoHierarchyStructure.Id;
                            if (parentLevel != null)
                            {
                                ccmLevel.ParentLevelId = context.LocationLevelMap[parentLevel.Id];
                            }
                            else
                            {
                                ccmLevel.ParentLevelId = null;
                            }

                            dalLevel.Insert(ccmLevel);
                            hierarchyLevels.Add(ccmLevel);

                            //Update the child id
                            if (levelCount > 0)
                            {
                                //Update the child id, if a child level exists in the hierarchy levels
                                //below current level. Count() isn't zero based hence the + 2
                                if (hierarchyLevels.Count >= levelCount + 2)
                                {
                                    //Check if level below current level is assigned this level as its parent
                                    if (hierarchyLevels[levelCount + 1].ParentLevelId != ccmLevel.Id)
                                    {
                                        //If not, set
                                        hierarchyLevels[levelCount + 1].ParentLevelId = ccmLevel.Id;
                                        dalLevel.Update(hierarchyLevels[levelCount + 1]);
                                    }
                                }
                            }

                            // update the lookkup
                            context.LocationLevelMap.Add(level.Id, ccmLevel.Id);

                        }
                        else
                        {
                            if (ccmLevel.Name != level.Name)
                            {
                                ccmLevel.Name = level.Name;
                                dalLevel.Update(ccmLevel);
                            }

                            // update the lookkup
                            context.LocationLevelMap.Add(level.Id, ccmLevel.Id);
                        }

                        level = level.ChildLevel;
                        levelCount++;

                        if (levelCount < hierarchyLevels.Count)
                        {
                            ccmLevel = hierarchyLevels[levelCount];
                        }
                        else
                        {
                            ccmLevel = null;
                        }
                    }

                    #endregion

                }
                #endregion

                #region CCM Location Group

                //Create the Dal for the CCM Location Group
                using (ILocationGroupDal dalGroup = context.DalContext.GetDal<ILocationGroupDal>())
                {
                    LocationGroupDto ccmGroup;

                    List<LocationGroupDto> ccmGroupList = new List<LocationGroupDto>();

                    Ccm.Model.LocationHierarchy structure = Ccm.Model.LocationHierarchy.FetchByEntityIdContext(this.EntityId, context.DalContext);
                    IEnumerable<Ccm.Model.LocationGroup> groupList = structure.FetchAllGroups();

                    LocationGroupDto ccmRootGroup = dalGroup.FetchById(groupList.First(p => p.ParentGroup == null).Id);

                    foreach (Ccm.Model.LocationGroup group in groupList)
                    {
                        if (group.ParentGroup != null)
                        {
                            ccmGroup = new LocationGroupDto();

                            ccmGroup.Id = group.Id;
                            ccmGroup.Code = group.Code;
                            ccmGroup.LocationLevelId = (Int32)group.LocationLevelId;
                            ccmGroup.Name = group.Name;
                            ccmGroup.ParentGroupId = group.ParentGroup.Id;
                            ccmGroup.LocationHierarchyId = group.ParentHierarchy.Id;

                            ccmGroupList.Add(ccmGroup);
                        }
                    }

                    Services.Location.LocationGroup serviceGroup;
                    serviceGroup = gfsLocationHierarchy.RootGroup;

                    if (serviceGroup != null)
                    {
                        if (ccmRootGroup == null)
                        {
                            ccmRootGroup = new LocationGroupDto();

                            ccmRootGroup.Code = serviceGroup.Code;
                            ccmRootGroup.LocationLevelId = hierarchyLevels[0].Id;
                            ccmRootGroup.Name = serviceGroup.Name;
                            ccmRootGroup.LocationHierarchyId = dtoHierarchyStructure.Id;
                            ccmRootGroup.ParentGroupId = null;

                            dalGroup.Insert(ccmRootGroup);
                        }
                        else
                        {
                            if (ccmRootGroup.Code != serviceGroup.Code || ccmRootGroup.Name != serviceGroup.Name)
                            {
                                ccmRootGroup.Code = serviceGroup.Code;
                                ccmRootGroup.Name = serviceGroup.Name;

                                dalGroup.Update(ccmRootGroup);
                            }
                        }

                        //Add root group to lookup
                        context.LocationGroupMap.Add(ccmRootGroup.Code, ccmRootGroup.Id);
                        context.LocationGroupIdMap.Add(serviceGroup.Id, ccmRootGroup.Id);

                        LoadChildLocationGroup(serviceGroup, ccmGroupList, context, ccmRootGroup.Id, dalGroup, dtoHierarchyStructure.Id);
                    }

                }
                #endregion
            }

            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationHierarchy], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        /// <summary>
        /// Loads the children of a location group
        /// </summary>
        /// <param name="group">The location group object</param>
        /// <param name="ccmGroupList">The list of CCM groups</param>
        /// <param name="context">The process context</param>
        /// <param name="parentId">The Id of the Parent group</param>
        /// <param name="dalUnit">The Entity Id</param>
        private void LoadChildLocationGroup(Services.Location.LocationGroup group, List<LocationGroupDto> ccmGroupList, ProcessContext context, Int32 parentId, ILocationGroupDal dalGroup, Int32 locationHierarchyId)
        {
            LocationGroupDto ccmGroup;

            foreach (Services.Location.LocationGroup childGroup in group.Children)
            {
                Int32 ccmLevelId = context.LocationLevelMap[childGroup.LocationLevelId];

                ccmGroup = ccmGroupList.Where(s => s.Code == childGroup.Code).FirstOrDefault();

                #region Insert/Update unit
                if (ccmGroup == null)
                {
                    //Insert new Location Group
                    ccmGroup = new LocationGroupDto();

                    ccmGroup.Code = childGroup.Code;
                    ccmGroup.Name = childGroup.Name;
                    ccmGroup.ParentGroupId = parentId;
                    ccmGroup.LocationLevelId = ccmLevelId;
                    ccmGroup.LocationHierarchyId = locationHierarchyId;

                    dalGroup.Insert(ccmGroup);
                }
                else
                {
                    Boolean needsUpdate = false;
                    if (ccmGroup.ParentGroupId != parentId)
                    {
                        ccmGroup.ParentGroupId = parentId;
                        needsUpdate = true;
                    }
                    if (ccmGroup.Name != childGroup.Name)
                    {
                        ccmGroup.Name = childGroup.Name;
                        needsUpdate = true;
                    }
                    if (ccmGroup.LocationLevelId != ccmLevelId)
                    {
                        ccmGroup.LocationLevelId = ccmLevelId;
                        needsUpdate = true;
                    }
                    if (needsUpdate)
                    {
                        dalGroup.Update(ccmGroup);
                    }
                }

                context.LocationGroupMap.Add(ccmGroup.Code, ccmGroup.Id);
                context.LocationGroupIdMap.Add(childGroup.Id, ccmGroup.Id);

                #endregion

                LoadChildLocationGroup(childGroup, ccmGroupList, context, ccmGroup.Id, dalGroup, locationHierarchyId);
            }
        }

        #endregion

        #region Location
        /// <summary>
        /// Syncrhonises the locations within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        private void SynchronizeLocation(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationDal dal = context.DalContext.GetDal<ILocationDal>())
                {
                    //Clear location lookups
                    context.LocationMap.Clear();
                    context.LocationIdMap.Clear();

                    //Fetch all GFS Locations
                    List<Services.Location.Location> gfsLocations = null;
                    context.DoWorkWithRetries(delegate ()
                    {
                        gfsLocations = context.Services.LocationServiceClient.GetLocationByEntityIdIncludingDeleted(
                            new Services.Location.GetLocationByEntityIdIncludingDeletedRequest(this.GFSEntityId)).Locations;
                    });

                    //Fetch all CCM locations
                    Dictionary<String, LocationDto> ccmLocations = new Dictionary<String, LocationDto>();
                    foreach (LocationDto ccmLocation in dal.FetchByEntityIdIncludingDeleted(this.EntityId))
                    {
                        ccmLocations.Add(ccmLocation.Code, ccmLocation);
                    }

                    //Create new upsert list
                    List<LocationDto> locationDtoList = new List<LocationDto>();

                    //Create a gfs location lookup, to quickly ensure that all locatons added to CCM-GFS lookup dictionaries exist within GFS
                    Dictionary<String, Services.Location.Location> gfsLocationLookup = new Dictionary<String, Services.Location.Location>();

                    //Loop through all the GFS locations and Insert/Update
                    foreach (Services.Location.Location gfsLocation in gfsLocations)
                    {
                        LocationDto locationDto = new LocationDto();
                        LocationDto ccmLocationDto = null;

                        if (ccmLocations.TryGetValue(gfsLocation.Code, out ccmLocationDto))
                        {
                            locationDto = ccmLocationDto;
                        }

                        #region Insert/Update

                        locationDto.EntityId = this.EntityId;
                        //Look CCM Location Group id based on GFS Product Group Id
                        Int32 ccmLocationGroupId;
                        if (context.LocationGroupIdMap.TryGetValue(gfsLocation.LocationGroupId, out ccmLocationGroupId))
                        {
                            locationDto.LocationGroupId = ccmLocationGroupId;
                        }
                        locationDto.Code = gfsLocation.Code;
                        locationDto.Name = gfsLocation.Name;
                        locationDto.Region = gfsLocation.Region;
                        locationDto.TVRegion = gfsLocation.TvRegion;
                        //locationDto.Location = gfsLocation.Location;  
                        locationDto.DefaultClusterAttribute = gfsLocation.DefaultClusterAttribute;
                        locationDto.Address1 = gfsLocation.Address1;
                        locationDto.Address2 = gfsLocation.Address2;
                        locationDto.City = gfsLocation.City;
                        locationDto.County = gfsLocation.County;
                        locationDto.State = gfsLocation.State;
                        locationDto.PostalCode = gfsLocation.PostalCode;
                        locationDto.Country = gfsLocation.Country;
                        locationDto.Longitude = gfsLocation.Longitude;
                        locationDto.Latitude = gfsLocation.Latitude;
                        locationDto.DateOpen = gfsLocation.DateOpen;
                        locationDto.DateLastRefitted = gfsLocation.DateLastRefitted;
                        locationDto.DateClosed = gfsLocation.DateClosed;
                        locationDto.CarParkSpaces = gfsLocation.CarParkSpaces;
                        locationDto.CarParkManagement = gfsLocation.CarParkManagement;
                        locationDto.PetrolForecourtType = gfsLocation.PetrolForecourtType;
                        locationDto.Restaurant = gfsLocation.Restaurant;
                        locationDto.SizeGrossFloorArea = gfsLocation.SizeGrossFloorArea;
                        locationDto.SizeNetSalesArea = gfsLocation.SizeNetSalesArea;
                        locationDto.SizeMezzSalesArea = gfsLocation.SizeMezzSalesArea;
                        locationDto.TelephoneCountryCode = gfsLocation.TelephoneCountryCode;
                        locationDto.TelephoneAreaCode = gfsLocation.TelephoneAreaCode;
                        locationDto.TelephoneNumber = gfsLocation.TelephoneNumber;
                        locationDto.FaxCountryCode = gfsLocation.FaxCountryCode;
                        locationDto.FaxAreaCode = gfsLocation.FaxAreaCode;
                        locationDto.FaxNumber = gfsLocation.FaxNumber;
                        locationDto.OpeningHours = gfsLocation.OpeningHours;
                        locationDto.AverageOpeningHours = gfsLocation.AverageOpeningHours;
                        locationDto.ManagerName = gfsLocation.ManagerName;
                        locationDto.ManagerEmail = gfsLocation.ManagerEmail;
                        locationDto.RegionalManagerName = gfsLocation.RegionalManagerName;
                        locationDto.RegionalManagerEmail = gfsLocation.RegionalManagerEmail;
                        locationDto.DivisionalManagerName = gfsLocation.DivisionalManagerName;
                        locationDto.DivisionalManagerEmail = gfsLocation.DivisionalManagerEmail;
                        locationDto.AdvertisingZone = gfsLocation.AdvertisingZone;
                        locationDto.DistributionCentre = gfsLocation.DistributionCentre;
                        locationDto.NoOfCheckouts = gfsLocation.NoOfCheckouts;
                        locationDto.IsMezzFitted = gfsLocation.IsMezzFitted;
                        locationDto.IsFreehold = gfsLocation.IsFreeHold;
                        locationDto.Is24Hours = gfsLocation.Is24Hours;
                        locationDto.IsOpenMonday = gfsLocation.IsOpenMonday;
                        locationDto.IsOpenTuesday = gfsLocation.IsOpenTuesday;
                        locationDto.IsOpenWednesday = gfsLocation.IsOpenWednesday;
                        locationDto.IsOpenThursday = gfsLocation.IsOpenThursday;
                        locationDto.IsOpenFriday = gfsLocation.IsOpenFriday;
                        locationDto.IsOpenSaturday = gfsLocation.IsOpenSaturday;
                        locationDto.IsOpenSunday = gfsLocation.IsOpenSunday;
                        locationDto.HasPetrolForecourt = gfsLocation.HasPetrolForecourt;
                        locationDto.HasNewsCube = gfsLocation.HasNewsCube;
                        locationDto.HasAtmMachines = gfsLocation.HasAtmMachines;
                        locationDto.HasCustomerWC = gfsLocation.HasCustomerWC;
                        locationDto.HasBabyChanging = gfsLocation.HasBabyChanging;
                        locationDto.HasInStoreBakery = gfsLocation.HasInStoreBakery;
                        locationDto.HasHotFoodToGo = gfsLocation.HasHotFoodToGo;
                        locationDto.HasRotisserie = gfsLocation.HasRotisserie;
                        locationDto.HasFishmonger = gfsLocation.HasFishMonger;
                        locationDto.HasButcher = gfsLocation.HasButcher;
                        locationDto.HasPizza = gfsLocation.HasPizza;
                        locationDto.HasDeli = gfsLocation.HasDeli;
                        locationDto.HasSaladBar = gfsLocation.HasSaladBar;
                        locationDto.HasOrganic = gfsLocation.HasOrganic;
                        locationDto.HasGrocery = gfsLocation.HasGrocery;
                        locationDto.HasMobilePhones = gfsLocation.HasMobilePhones;
                        locationDto.HasDryCleaning = gfsLocation.HasDryCleaning;
                        locationDto.HasHomeShoppingAvailable = gfsLocation.HasHomeShoppingAvailable;
                        locationDto.HasOptician = gfsLocation.HasOptician;
                        locationDto.HasPharmacy = gfsLocation.HasPharmacy;
                        locationDto.HasTravel = gfsLocation.HasTravel;
                        locationDto.HasPhotoDepartment = gfsLocation.HasPhotoDepartment;
                        locationDto.HasCarServiceArea = gfsLocation.HasCarServiceArea;
                        locationDto.HasGardenCentre = gfsLocation.HasGardenCenter;
                        locationDto.HasClinic = gfsLocation.HasClinic;
                        locationDto.HasAlcohol = gfsLocation.HasAlcohol;
                        locationDto.HasFashion = gfsLocation.HasFashion;
                        locationDto.HasCafe = gfsLocation.HasCafe;
                        locationDto.HasRecycling = gfsLocation.HasRecycling;
                        locationDto.HasPhotocopier = gfsLocation.HasPhotocopier;
                        locationDto.HasLottery = gfsLocation.HasLottery;
                        locationDto.HasPostOffice = gfsLocation.HasPostOffice;
                        locationDto.HasMovieRental = gfsLocation.HasMovieRental;
                        locationDto.HasJewellery = gfsLocation.HasJewellery;
                        locationDto.DateDeleted = gfsLocation.DateDeleted;

                        locationDtoList.Add(locationDto);
                        #endregion

                        //Add to GFS lookup
                        gfsLocationLookup.Add(gfsLocation.Code, gfsLocation);
                    }

                    //Upsert all records
                    dal.Upsert(locationDtoList, new LocationIsSetDto(true), /*updateDeleted*/true);

                    //Clear the dto list
                    locationDtoList.Clear();

                    //Fetch CCM locations again to add to dictionaries
                    IEnumerable<LocationDto> ccmLocationList = dal.FetchByEntityIdIncludingDeleted(EntityId);

                    //Map locations between GFS and CCM
                    foreach (LocationDto ccmLocation in ccmLocationList)
                    {
                        //If the location is also in GFS then add to lookups
                        Services.Location.Location gfsLocation = null;
                        if (gfsLocationLookup.TryGetValue(ccmLocation.Code, out gfsLocation))
                        {
                            context.LocationIdMap.Add(gfsLocation.Id, ccmLocation.Id);
                            context.LocationMap.Add(ccmLocation.Id, gfsLocation);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Location], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }
        #endregion

        #region Synchronize Clusters

        /// <summary>
        /// Syncrhonises the clusters within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        private void SynchronizeCluster(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IClusterSchemeDal dal = context.DalContext.GetDal<IClusterSchemeDal>())
                {
                    using (IClusterDal dalCluster = context.DalContext.GetDal<IClusterDal>())
                    {
                        using (IClusterLocationDal dalClusterLocation = context.DalContext.GetDal<IClusterLocationDal>())
                        {
                            List<Services.Cluster.ClusterSchemeInfo> gfsClusterSchemeInfo = null;

                            //Get the cluster info 
                            context.DoWorkWithRetries(delegate ()
                            {
                                gfsClusterSchemeInfo = context.Services.ClusterServiceClient.GetClusterSchemeInfoByEntityId(
                                    new GetClusterSchemeInfoByEntityIdRequest(this.GFSEntityId)).ClusterSchemeInfos;
                            });

                            IEnumerable<ClusterSchemeDto> ccmClusterSchemes = dal.FetchByEntityId(this.EntityId);

                            // Loop through the cluster info list 
                            foreach (Services.Cluster.ClusterSchemeInfo clusterScheme in gfsClusterSchemeInfo)
                            {
                                // check if it is the latest version
                                if (clusterScheme.IsLatestVersion == true)
                                {
                                    Services.Cluster.ClusterScheme gfsCluster = null;
                                   

                                    Guid ucr;
                                    if (clusterScheme.ParentUniqueContentReference != null)
                                    {
                                        ucr = clusterScheme.ParentUniqueContentReference.Value;
                                    }
                                    else
                                    {
                                        ucr = clusterScheme.UniqueContentReference;
                                    }
                                    ClusterSchemeDto ccmClusterScheme = ccmClusterSchemes.FirstOrDefault(i => i.ParentUniqueContentReference == ucr);

                                    //find one with the same name
                                    ClusterSchemeDto ccmDuplicateCdt = ccmClusterSchemes.FirstOrDefault(i => (i.Name == clusterScheme.Name && i.ParentUniqueContentReference == null));

                                    if (ccmDuplicateCdt != null)
                                    {
                                        //get the new one
                                        ClusterSchemeDto duplicateClusterScheme = dal.FetchById(ccmDuplicateCdt.Id);
                                        if (duplicateClusterScheme != null)
                                        {
                                            duplicateClusterScheme.Name += " (Local Copy)";
                                            dal.Update(duplicateClusterScheme);
                                        }
                                    }



                                    #region INSERT/UPDATE CCM Cluster schemes from GFS cluster scheme
                                    Boolean updateChildren = false;
                                    if (ccmClusterScheme == null)
                                    {
                                        context.DoWorkWithRetries(delegate ()
                                        {
                                            gfsCluster = context.Services.ClusterServiceClient.GetClusterSchemeById(
                                                new GetClusterSchemeByIdRequest(clusterScheme.Id)).ClusterScheme;
                                        });
                                        ccmClusterScheme = new ClusterSchemeDto();

                                        ccmClusterScheme.EntityId = this.EntityId;
                                        ccmClusterScheme.Name = gfsCluster.Name;
                                        ccmClusterScheme.UniqueContentReference = gfsCluster.UniqueContentReference;

                                        //We want to assign the ccm id for the product group code we have from GFS service
                                        if (gfsCluster.ProductGroupCode != null)
                                        {
                                            ccmClusterScheme.ProductGroupId = context.ProductGroupMap[gfsCluster.ProductGroupCode];
                                        }

                                        if (gfsCluster.ParentUniqueContentReference != null)
                                        {
                                            ccmClusterScheme.ParentUniqueContentReference = gfsCluster.ParentUniqueContentReference;
                                        }
                                        else
                                        {
                                            ccmClusterScheme.ParentUniqueContentReference = gfsCluster.UniqueContentReference;
                                        }


                                        dal.Insert(ccmClusterScheme);
                                        updateChildren = true;
                                    }
                                    else
                                    {
                                        ccmClusterScheme = dal.FetchById(ccmClusterScheme.Id);

                                        if (ccmClusterScheme.UniqueContentReference != clusterScheme.UniqueContentReference
                                            || SyncTarget.LastSync == null ||
                                            (clusterScheme.DateLastModified != null && SyncTarget.LastSync != null && clusterScheme.DateLastModified > SyncTarget.LastSync.Value))
                                        {
                                            context.DoWorkWithRetries(delegate ()
                                            {
                                                gfsCluster = context.Services.ClusterServiceClient.GetClusterSchemeById(
                                                    new GetClusterSchemeByIdRequest(clusterScheme.Id)).ClusterScheme;
                                            });
                                            //Update fields
                                            ccmClusterScheme.UniqueContentReference = gfsCluster.UniqueContentReference;
                                            ccmClusterScheme.Name = gfsCluster.Name;

                                            if (gfsCluster.ProductGroupCode != null)
                                            {
                                                ccmClusterScheme.ProductGroupId = context.ProductGroupMap[gfsCluster.ProductGroupCode];
                                            }

                                            if (gfsCluster.ParentUniqueContentReference != null)
                                            {
                                                ccmClusterScheme.ParentUniqueContentReference = gfsCluster.ParentUniqueContentReference;
                                            }
                                            else
                                            {
                                                ccmClusterScheme.ParentUniqueContentReference = gfsCluster.UniqueContentReference;
                                            }

                                            dal.Update(ccmClusterScheme);
                                            updateChildren = true;
                                        }
                                    }
                                    #endregion
                                    if (updateChildren)
                                    {
                                        //get the cluster group levels from the web-service
                                        IEnumerable<ClusterGroup> clusters;
                                        clusters = gfsCluster.Sublevels;

                                        ClusterDto ccmCluster;
                                        ClusterLocationDto ccmClusterLocation;

                                        //get the Clusters from CCM
                                        IEnumerable<ClusterDto> ccmClusters = dalCluster.FetchByClusterSchemeId(ccmClusterScheme.Id);

                                        #region DELETE clusters and cluster locations
                                        foreach (ClusterDto cluster in ccmClusters)
                                        {
                                            //Delete the cluster
                                            dalCluster.DeleteById(cluster.Id);
                                        }
                                        #endregion

                                        foreach (ClusterGroup cluster in clusters)
                                        {
                                            #region INSERT CCM cluster from GFS Cluster Group
                                            //Add Cluster
                                            ccmCluster = new ClusterDto();

                                            ccmCluster.Name = cluster.Name;
                                            ccmCluster.ClusterSchemeId = ccmClusterScheme.Id;

                                            dalCluster.Insert(ccmCluster);
                                            #endregion

                                            //get the cluster group locations from the web-service
                                            IEnumerable<Services.Cluster.ClusterLocation> clusterLocations;
                                            clusterLocations = cluster.Locations;

                                            foreach (Services.Cluster.ClusterLocation clusterLocation in clusterLocations)
                                            {
                                                //get the location id from CCM
                                                Int16 locationId;
                                                if (context.LocationIdMap.TryGetValue(clusterLocation.LocationId, out locationId))
                                                {
                                                    #region INSERT CCM Cluster Location from GFS Cluster Location

                                                    //Add Location to cluster
                                                    ccmClusterLocation = new ClusterLocationDto();

                                                    ccmClusterLocation.ClusterId = ccmCluster.Id;
                                                    ccmClusterLocation.LocationId = locationId;

                                                    dalClusterLocation.Insert(ccmClusterLocation);

                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Cluster], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }
        #endregion

        #region Synchronize Cluster Deletes
        /// <summary>
        /// Syncrhonises the cluster deletes from GFS
        /// </summary>
        /// <param name="context">The process context</param>
        private void SynchronizeClustersDeleted(ProcessContext context)
        {
            // Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IClusterSchemeInfoDal dalSchemeInfo = context.DalContext.GetDal<IClusterSchemeInfoDal>())
                {
                    using (IClusterSchemeDal dalScheme = context.DalContext.GetDal<IClusterSchemeDal>())
                    {
                        #region old delete
                        //List<Services.Cluster.ClusterSchemeInfo> gfsClustersDeleted = null;

                        //// Ensure that it has sync'd before
                        //if (SyncTarget.LastSync != null)
                        //{
                        //    // Fetch the deleted clusters
                        //    context.DoWorkWithRetries(delegate()
                        //    {
                        //        gfsClustersDeleted = context.Services.ClusterServiceClient.GetClusterSchemeInfoByEntityIdDateDeleted(
                        //            new GetClusterSchemeInfoByEntityIdDateDeletedRequest(this.GFSEntityId, SyncTarget.LastSync)).ClusterSchemeInfos;
                        //    });

                        //    IEnumerable<ClusterSchemeDto> ccmClusterSchemes = dalScheme.FetchByEntityId(this.EntityId);

                        //    // loop through the clusters to delete
                        //    foreach (Services.Cluster.ClusterSchemeInfo cluster in gfsClustersDeleted)
                        //    {
                        //        // check if cluster exists in CCM
                        //        ClusterSchemeDto ccmCluster = ccmClusterSchemes.FirstOrDefault(i => i.UniqueContentReference == cluster.UniqueContentReference);

                        //        if (ccmCluster != null)
                        //        {
                        //            dalScheme.DeleteById(ccmCluster.Id);
                        //        }
                        //    }
                        //}
                        #endregion
                        List<Services.Cluster.ClusterSchemeInfo> gfsClustersDeleted = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate ()
                        {
                            gfsClustersDeleted = context.Services.ClusterServiceClient.GetClusterSchemeInfoByEntityId(
                                new Services.Cluster.GetClusterSchemeInfoByEntityIdRequest(this.GFSEntityId)).ClusterSchemeInfos;
                        });

                        //fetch ccm universes
                        IEnumerable<ClusterSchemeInfoDto> cdtList = dalSchemeInfo.FetchByEntityId(this.EntityId);

                        //loop through ccm cluster schemes
                        foreach (ClusterSchemeInfoDto ccmContent in cdtList)
                        {
                            //came from a sync
                            if (ccmContent.ParentUniqueContentReference != null)
                            {
                                //try to find the gfs product universe
                                Services.Cluster.ClusterSchemeInfo gfsContent = gfsClustersDeleted.FirstOrDefault(u => u.UniqueContentReference == ccmContent.UniqueContentReference);

                                //doesn't exist in gfs so delete it
                                if (gfsContent == null)
                                {
                                    dalScheme.DeleteById(ccmContent.Id);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Cluster], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }
        #endregion

        #region Syncrhonize Consumer Decision Tree

        /// <summary>
        /// Syncrhonises the consumer decision tree records within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        private void SynchronizeConsumerDecisionTree(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();
            try
            {
                using (IConsumerDecisionTreeInfoDal dalCDTInfo = context.DalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                {
                    using (IConsumerDecisionTreeDal dalCDT = context.DalContext.GetDal<IConsumerDecisionTreeDal>())
                    {
                        using (IConsumerDecisionTreeLevelDal dalCDTLevel = context.DalContext.GetDal<IConsumerDecisionTreeLevelDal>())
                        {
                            using (IConsumerDecisionTreeNodeDal dalCDTNode = context.DalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                            {
                                using (IConsumerDecisionTreeNodeProductDal dalCDTProduct = context.DalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
                                {
                                    List<Services.ConsumerDecisionTree.ConsumerDecisionTreeInfo> gfsConsumerDecisionTreeInfo = null;

                                    //Get the cluster info 
                                    context.DoWorkWithRetries(delegate ()
                                    {
                                        gfsConsumerDecisionTreeInfo =
                                            context.Services.ConsumerDecisionTreeServiceClient.GetConsumerDecisionTreeInfoByEntityName(
                                            new Services.ConsumerDecisionTree.GetConsumerDecisionTreeInfoByEntityNameRequest(this.EntityName))
                                            .ConsumerDecisionTreeInfos;
                                    });

                                    //Fetch all CCM CDT info records
                                    IEnumerable<ConsumerDecisionTreeInfoDto> ccmCDTInfoList = dalCDTInfo.FetchByEntityId(this.EntityId);

                                    // Loop through the CDT info list 
                                    foreach (Services.ConsumerDecisionTree.ConsumerDecisionTreeInfo consumerDecisionTree in gfsConsumerDecisionTreeInfo)
                                    {
                                        // check if it is the latest version
                                        if (consumerDecisionTree.IsLatestVersion == true)
                                        {
                                            Services.ConsumerDecisionTree.ConsumerDecisionTree gfsConsumerDecisionTree = null;

                                            //Check if the CDT info record exists
                                            Guid ucr;
                                            if (consumerDecisionTree.ParentUniqueContentReference != null)
                                            {
                                                ucr = consumerDecisionTree.ParentUniqueContentReference.Value;
                                            }
                                            else
                                            {
                                                ucr = consumerDecisionTree.UniqueContentReference;
                                            }
                                            ConsumerDecisionTreeInfoDto ccmCdt = ccmCDTInfoList.FirstOrDefault(i => i.ParentUniqueContentReference == ucr);

                                            //find one with the same name
                                            ConsumerDecisionTreeInfoDto ccmDuplicateCdt = ccmCDTInfoList.FirstOrDefault(i => (i.Name == consumerDecisionTree.Name && i.ParentUniqueContentReference == null));

                                            if (ccmDuplicateCdt != null)
                                            {
                                                //get the new one
                                                ConsumerDecisionTreeDto duplicateCdt = dalCDT.FetchById(ccmDuplicateCdt.Id);
                                                if (duplicateCdt != null)
                                                {
                                                    duplicateCdt.Name += " (Local Copy)";
                                                    dalCDT.Update(duplicateCdt);
                                                }
                                            }

                                            //Create new CDT dto
                                            ConsumerDecisionTreeDto ccmCDTDto;

                                            Boolean updateChildren = false;
                                            #region INSERT/UPDATE CCM CDT from GFS CDT
                                            if (ccmCdt == null)
                                            {
                                                //Fetch the consumer decision tree details for the content ref
                                                context.DoWorkWithRetries(delegate ()
                                                {
                                                    gfsConsumerDecisionTree =
                                                        context.Services.ConsumerDecisionTreeServiceClient.GetConsumerDecisionTreeByUniqueContentReference(
                                                        new Services.ConsumerDecisionTree.GetConsumerDecisionTreeByUniqueContentReferenceRequest(consumerDecisionTree.UniqueContentReference))
                                                        .ConsumerDecisionTree;
                                                });
                                                ccmCDTDto = new ConsumerDecisionTreeDto();

                                                Int32 productGroupId;
                                                if (context.ProductGroupMap.TryGetValue(gfsConsumerDecisionTree.ProductGroupCode, out productGroupId))
                                                {
                                                    ccmCDTDto.EntityId = this.EntityId;
                                                    ccmCDTDto.Name = gfsConsumerDecisionTree.Name;
                                                    ccmCDTDto.UniqueContentReference = gfsConsumerDecisionTree.UniqueContentReference;
                                                    ccmCDTDto.ProductGroupId = productGroupId;

                                                    if (gfsConsumerDecisionTree.ParentUniqueContentReference != null)
                                                    {
                                                        ccmCDTDto.ParentUniqueContentReference = gfsConsumerDecisionTree.ParentUniqueContentReference;
                                                    }
                                                    else
                                                    {
                                                        ccmCDTDto.ParentUniqueContentReference = gfsConsumerDecisionTree.UniqueContentReference;
                                                    }

                                                    dalCDT.Insert(ccmCDTDto);
                                                    updateChildren = true;
                                                }
                                            }
                                            else
                                            {
                                                ccmCDTDto = dalCDT.FetchById(ccmCdt.Id);

                                                if (ccmCDTDto.UniqueContentReference != consumerDecisionTree.UniqueContentReference
                                                    || SyncTarget.LastSync == null ||
                                                    (consumerDecisionTree.DateLastModified != null && SyncTarget.LastSync != null && consumerDecisionTree.DateLastModified > SyncTarget.LastSync.Value))
                                                {
                                                    //Fetch the consumer decision tree details for the content ref
                                                    context.DoWorkWithRetries(delegate ()
                                                    {
                                                        gfsConsumerDecisionTree =
                                                            context.Services.ConsumerDecisionTreeServiceClient.GetConsumerDecisionTreeByUniqueContentReference(
                                                            new Services.ConsumerDecisionTree.GetConsumerDecisionTreeByUniqueContentReferenceRequest(consumerDecisionTree.UniqueContentReference))
                                                            .ConsumerDecisionTree;
                                                    });

                                                    //Update fields
                                                    ccmCDTDto.UniqueContentReference = gfsConsumerDecisionTree.UniqueContentReference;
                                                    ccmCDTDto.Name = gfsConsumerDecisionTree.Name;

                                                    Int32 productGroupId;
                                                    if (context.ProductGroupMap.TryGetValue(gfsConsumerDecisionTree.ProductGroupCode, out productGroupId))
                                                    {
                                                        ccmCDTDto.ProductGroupId = productGroupId;
                                                    }

                                                    if (gfsConsumerDecisionTree.ParentUniqueContentReference != null)
                                                    {
                                                        ccmCDTDto.ParentUniqueContentReference = gfsConsumerDecisionTree.ParentUniqueContentReference;
                                                    }
                                                    else
                                                    {
                                                        ccmCDTDto.ParentUniqueContentReference = gfsConsumerDecisionTree.UniqueContentReference;
                                                    }

                                                    dalCDT.Update(ccmCDTDto);
                                                    updateChildren = true;
                                                }
                                            }
                                            #endregion

                                            if (updateChildren)
                                            {

                                                Services.ConsumerDecisionTree.ConsumerDecisionTreeLevel gfsConsumerDecisionTreeRootLevel;
                                                gfsConsumerDecisionTreeRootLevel = gfsConsumerDecisionTree.RootLevel;

                                                //get all the cdt level records from CCM
                                                IEnumerable<ConsumerDecisionTreeLevelDto> ccmConsumerDecisionTreeLevels = dalCDTLevel.FetchByConsumerDecisionTreeId(ccmCDTDto.Id);
                                                ConsumerDecisionTreeLevelDto ccmCDTRootLevelDto = null;
                                                #region DELETE all CDT levels in tree

                                                foreach (ConsumerDecisionTreeLevelDto level in ccmConsumerDecisionTreeLevels)
                                                {
                                                    if (level.ParentLevelId != null)
                                                    {
                                                        //Delete the node
                                                        dalCDTLevel.DeleteById(level.Id);
                                                    }
                                                    else
                                                    {
                                                        ccmCDTRootLevelDto = level;
                                                    }
                                                }

                                                #endregion

                                                #region Insert Root Level

                                                if (ccmCDTRootLevelDto == null)
                                                {
                                                    ccmCDTRootLevelDto = new ConsumerDecisionTreeLevelDto();
                                                    ccmCDTRootLevelDto.Name = gfsConsumerDecisionTreeRootLevel.Name;
                                                    ccmCDTRootLevelDto.ParentLevelId = null;
                                                    ccmCDTRootLevelDto.ConsumerDecisionTreeId = ccmCDTDto.Id;
                                                    dalCDTLevel.Insert(ccmCDTRootLevelDto);
                                                }

                                                #endregion

                                                //Load hierarchical structure starting from the root level
                                                LoadChildConsumerDecisionTreeLevels(gfsConsumerDecisionTree.RootLevel, context, ccmCDTRootLevelDto.Id, dalCDTLevel, ccmCDTDto.Id);


                                                Services.ConsumerDecisionTree.ConsumerDecisionNode gfsConsumerDecisionTreeRootNode;
                                                gfsConsumerDecisionTreeRootNode = gfsConsumerDecisionTree.RootNode;

                                                //get the consumer decision tree node records from CCM
                                                IEnumerable<ConsumerDecisionTreeNodeDto> ccmConsumerDecisionTreeNodes = dalCDTNode.FetchByConsumerDecisionTreeId(ccmCDTDto.Id);

                                                #region DELETE all CDT nodes in tree

                                                foreach (ConsumerDecisionTreeNodeDto node in ccmConsumerDecisionTreeNodes)
                                                {
                                                    //Delete the node
                                                    dalCDTNode.DeleteById(node.Id);
                                                }

                                                #endregion

                                                #region Insert Root Node & Products (If assigned)

                                                ConsumerDecisionTreeNodeDto ccmCDTRootNodeDto = new ConsumerDecisionTreeNodeDto();
                                                ccmCDTRootNodeDto.ParentNodeId = null;
                                                ccmCDTRootNodeDto.Name = gfsConsumerDecisionTreeRootNode.Name;
                                                ccmCDTRootNodeDto.ConsumerDecisionTreeLevelId = ccmCDTRootLevelDto.Id;
                                                ccmCDTRootNodeDto.ConsumerDecisionTreeId = ccmCDTDto.Id;
                                                ccmCDTRootNodeDto.GFSId = gfsConsumerDecisionTreeRootNode.Id;
                                                dalCDTNode.Insert(ccmCDTRootNodeDto);

                                                foreach (Services.ConsumerDecisionTree.ConsumerDecisionProduct childNodeProduct in gfsConsumerDecisionTreeRootNode.AssignedProducts)
                                                {
                                                    ConsumerDecisionTreeNodeProductDto cdtProduct = new ConsumerDecisionTreeNodeProductDto();

                                                    Int32 productId;
                                                    if (context.ProductMap.TryGetValue(childNodeProduct.GTIN, out productId))
                                                    {
                                                        cdtProduct.ConsumerDecisionTreeNodeId = ccmCDTRootNodeDto.Id; //SA19687
                                                        cdtProduct.ProductId = productId;

                                                        dalCDTProduct.Insert(cdtProduct);
                                                    }
                                                }

                                                #endregion

                                                //Load hierarchical structure starting from the root node
                                                LoadChildConsumerDecisionTreeNodes(
                                                    gfsConsumerDecisionTree.RootNode,
                                                    context,
                                                    ccmCDTRootNodeDto.Id,
                                                    dalCDTNode,
                                                    dalCDTProduct,
                                                    dalCDTLevel.FetchByConsumerDecisionTreeId(ccmCDTDto.Id),
                                                    ccmCDTDto.Id);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.CDT], ex);

                // throw exception back up the stack
                throw;
            }
            //Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        /// <summary>
        /// Loads the children of a CDT Node
        /// </summary>
        /// <param name="node">The CDT node</param>
        /// <param name="context">The process context</param>
        /// <param name="parentId">The parent identifier</param>
        /// <param name="dalNode">The dal node</param>
        /// <param name="dalNodeProduct">The dal node product</param>
        /// <param name="levelDtos"></param>
        /// <param name="consumerDecisionTreeId"></param>
        private void LoadChildConsumerDecisionTreeNodes(
            Services.ConsumerDecisionTree.ConsumerDecisionNode node,
            ProcessContext context,
            Int32 parentId,
            IConsumerDecisionTreeNodeDal dalNode,
            IConsumerDecisionTreeNodeProductDal dalNodeProduct,
            IEnumerable<ConsumerDecisionTreeLevelDto> levelDtos,
            Int32 consumerDecisionTreeId)
        {
            ConsumerDecisionTreeNodeDto ccmNode;

            foreach (Services.ConsumerDecisionTree.ConsumerDecisionNode childNode in node.Children)
            {
                String levelName = childNode.ConsumerDecisionTreeLevelName;
                ConsumerDecisionTreeLevelDto levelDto = levelDtos.First(l => l.Name == levelName);

                #region Insert node

                //Insert new Consumer Decision Tree Node
                ccmNode = new ConsumerDecisionTreeNodeDto();

                ccmNode.Name = childNode.Name;
                ccmNode.ParentNodeId = parentId;
                ccmNode.ConsumerDecisionTreeId = consumerDecisionTreeId;
                ccmNode.ConsumerDecisionTreeLevelId = levelDto.Id;
                ccmNode.GFSId = childNode.Id;

                dalNode.Insert(ccmNode);

                #endregion

                #region Insert Node Products

                foreach (Services.ConsumerDecisionTree.ConsumerDecisionProduct childNodeProduct in childNode.AssignedProducts)
                {
                    ConsumerDecisionTreeNodeProductDto cdtProduct = new ConsumerDecisionTreeNodeProductDto();

                    Int32 productId;
                    if (context.ProductMap.TryGetValue(childNodeProduct.GTIN, out productId))
                    {
                        cdtProduct.ConsumerDecisionTreeNodeId = ccmNode.Id;
                        cdtProduct.ProductId = productId;

                        dalNodeProduct.Insert(cdtProduct);
                    }
                }

                #endregion

                LoadChildConsumerDecisionTreeNodes(childNode, context, ccmNode.Id, dalNode, dalNodeProduct, levelDtos, consumerDecisionTreeId);
            }
        }

        /// <summary>
        /// Loads the children of a CDT level
        /// </summary>
        /// <param name="level"></param>
        /// <param name="context"></param>
        /// <param name="parentId"></param>
        /// <param name="dalLevel"></param>
        /// <param name="consumerDecisionTreeId"></param>
        private void LoadChildConsumerDecisionTreeLevels(
            Services.ConsumerDecisionTree.ConsumerDecisionTreeLevel level,
            ProcessContext context,
            Int32 parentId,
            IConsumerDecisionTreeLevelDal dalLevel,
            Int32 consumerDecisionTreeId)
        {
            if (level.ChildLevel != null)
            {
                ConsumerDecisionTreeLevelDto ccmLevel = new ConsumerDecisionTreeLevelDto();
                ccmLevel.Name = level.ChildLevel.Name;
                ccmLevel.ParentLevelId = parentId;
                ccmLevel.ConsumerDecisionTreeId = consumerDecisionTreeId;

                dalLevel.Insert(ccmLevel);

                //recurse for child level
                LoadChildConsumerDecisionTreeLevels(level.ChildLevel, context, ccmLevel.Id, dalLevel, consumerDecisionTreeId);
            }
        }

        #endregion

        #region Synchronize Consumer Decision Tree Deletes

        private void SynchronizeConsumerDecisionTreeDeleted(ProcessContext context)
        {
            // Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IConsumerDecisionTreeDal dalCdt = context.DalContext.GetDal<IConsumerDecisionTreeDal>())
                {
                    using (IConsumerDecisionTreeInfoDal infoDalCdt = context.DalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                    {

                        #region old delete
                        //List<Services.ConsumerDecisionTree.ConsumerDecisionTreeInfo> gfsCdtsDeleted = null;

                        //// Ensure that it has sync'd before
                        //if (SyncTarget.LastSync != null)
                        //{
                        //    // Fetch the deleted clusters
                        //    context.DoWorkWithRetries(delegate()
                        //    {
                        //        gfsCdtsDeleted = context.Services.ConsumerDecisionTreeServiceClient.GetConsumerDecisionTreeInfoByEntityNameDateDeleted(
                        //            new Services.ConsumerDecisionTree.GetConsumerDecisionTreeInfoByEntityNameDateDeletedRequest(this.EntityName, SyncTarget.LastSync)).ConsumerDecisionTreeInfos;
                        //    });

                        //    IEnumerable<ConsumerDecisionTreeInfoDto> ccmCdts;
                        //    using (IConsumerDecisionTreeInfoDal dalCdtInfo = context.DalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                        //    {
                        //        ccmCdts = dalCdtInfo.FetchByEntityId(this.EntityId);
                        //    }

                        //    // loop through the cdts to delete
                        //    foreach (Services.ConsumerDecisionTree.ConsumerDecisionTreeInfo cdt in gfsCdtsDeleted)
                        //    {
                        //        // check if cdt exists in CCM
                        //        ConsumerDecisionTreeInfoDto ccmCdt = ccmCdts.FirstOrDefault(i => i.UniqueContentReference == cdt.UniqueContentReference);

                        //        if (ccmCdt != null)
                        //        {
                        //            dalCdt.DeleteById(ccmCdt.Id);
                        //        }
                        //    }
                        //}
                        #endregion

                        List<Services.ConsumerDecisionTree.ConsumerDecisionTreeInfo> gfsCdtsDeleted = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate ()
                        {
                            gfsCdtsDeleted = context.Services.ConsumerDecisionTreeServiceClient.GetConsumerDecisionTreeInfoByEntityName(
                                new Services.ConsumerDecisionTree.GetConsumerDecisionTreeInfoByEntityNameRequest(this.EntityName)).ConsumerDecisionTreeInfos;
                        });

                        //fetch ccm universes
                        IEnumerable<ConsumerDecisionTreeInfoDto> cdtList = infoDalCdt.FetchByEntityId(this.EntityId);

                        //loop through ccm product universes
                        foreach (ConsumerDecisionTreeInfoDto ccmContent in cdtList)
                        {
                            //came from a sync
                            if (ccmContent.ParentUniqueContentReference != null)
                            {
                                //try to find the gfs cdt
                                Services.ConsumerDecisionTree.ConsumerDecisionTreeInfo gfsContent = gfsCdtsDeleted.FirstOrDefault(u => u.UniqueContentReference == ccmContent.UniqueContentReference);

                                //doesn't exist in gfs so delete it
                                if (gfsContent == null)
                                {
                                    dalCdt.DeleteById(ccmContent.Id);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.CDT], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Metric

        /// <summary>
        /// Synchronizes the metrics within the solution
        /// </summary>
        /// <param name="context"></param>
        private void SynchronizeMetric(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IMetricDal dal = context.DalContext.GetDal<IMetricDal>())
                {
                    IEnumerable<MetricDto> ccmMetrics = dal.FetchByEntityId(this.EntityId);

                    ObservableCollection<Services.Metric.Metric> gfsMetrics = null;

                    GetMetricIncludingCalculatedByEntityNameRequest MetricByEntityRequest = new GetMetricIncludingCalculatedByEntityNameRequest(this.EntityName);

                    context.DoWorkWithRetries(delegate ()
                    {
                        gfsMetrics = new ObservableCollection<Services.Metric.Metric>(
                            context.Services.MetricServiceClient.GetMetricIncludingCalculatedByEntityName(MetricByEntityRequest).Metrics);
                    });

                    foreach (var gfsMetric in gfsMetrics)
                    {
                        MetricDto ccmMetric = null;

                        if (ccmMetrics != null)
                        {
                            ccmMetric = ccmMetrics.FirstOrDefault(i => String.Equals(i.Name, gfsMetric.Name, StringComparison.OrdinalIgnoreCase));
                        }

                        if (ccmMetric == null)
                        {
                            ccmMetric = new MetricDto();

                            ccmMetric.Name = gfsMetric.Name;
                            ccmMetric.Description = gfsMetric.Description;
                            ccmMetric.Direction = (Byte)(EnumHelper.Parse<MetricDirectionType>(gfsMetric.Direction, MetricDirectionType.MaximiseIncrease));
                            ccmMetric.EntityId = this.EntityId;
                            ccmMetric.SpecialType = (Byte)(EnumHelper.Parse<MetricSpecialType>(gfsMetric.SpecialType, MetricSpecialType.None));
                            ccmMetric.Type = (Byte)(EnumHelper.Parse<MetricType>(gfsMetric.Type, MetricType.Decimal));
                            ccmMetric.DataModelType = (Byte)(EnumHelper.Parse<MetricElasticityDataModelType>(gfsMetric.DataModelType, MetricElasticityDataModelType.None));
                            dal.Insert(ccmMetric);
                        }
                        else
                        {
                            if (ccmMetric.Name != gfsMetric.Name ||
                                ccmMetric.Description != gfsMetric.Description ||
                                ((MetricDirectionType)ccmMetric.Direction).ToString() != gfsMetric.Direction ||
                                ((MetricSpecialType)ccmMetric.SpecialType).ToString() != gfsMetric.SpecialType ||
                                ((MetricType)ccmMetric.Type).ToString() != gfsMetric.Type ||
                                ((MetricElasticityDataModelType)ccmMetric.DataModelType).ToString() != gfsMetric.DataModelType)
                            {
                                ccmMetric.Name = gfsMetric.Name;
                                ccmMetric.Description = gfsMetric.Description;
                                ccmMetric.Direction = (Byte)(EnumHelper.Parse<MetricDirectionType>(gfsMetric.Direction, MetricDirectionType.MaximiseIncrease));
                                ccmMetric.SpecialType = (Byte)(EnumHelper.Parse<MetricSpecialType>(gfsMetric.SpecialType, MetricSpecialType.None));
                                ccmMetric.Type = (Byte)(EnumHelper.Parse<MetricType>(gfsMetric.Type, MetricType.Decimal));
                                ccmMetric.DataModelType = (Byte)(EnumHelper.Parse<MetricElasticityDataModelType>(gfsMetric.DataModelType, MetricElasticityDataModelType.None));
                                dal.Update(ccmMetric);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Metric], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to SA
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Location Product Attribute

        /// <summary>
        /// Synchronizes the location product atributes within the solution
        /// </summary>
        /// <param name="context"></param>
        private void SynchronizeLocationProductAttribute(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationProductAttributeDal dal = context.DalContext.GetDal<ILocationProductAttributeDal>())
                {
                    using (ILocationInfoDal locDal = context.DalContext.GetDal<ILocationInfoDal>())
                    {
                        #region Fetch GFS Locations

                        IEnumerable<String> gfsLocations = null;

                        //Fetch GFS Locations
                        if (this.SyncTarget.LastSync != null)
                        {
                            GetLocationInfoByEntityIdLocationProductDateChangedRequest locRequest = new GetLocationInfoByEntityIdLocationProductDateChangedRequest(this.GFSEntityId, this.SyncTarget.LastSync.Value);

                            //Fetch the changed GFS Locations
                            context.DoWorkWithRetries(delegate ()
                            {
                                gfsLocations = new ObservableCollection<String>(context.Services.LocationServiceClient.GetLocationInfoByEntityIdLocationProductDateChanged(locRequest).LocationInfos.Select(l => l.Code).Distinct());
                            });
                        }
                        else
                        {
                            GetLocationInfoByEntityIdRequest locRequest = new GetLocationInfoByEntityIdRequest(this.GFSEntityId);

                            //Fetch the changed GFS Locations
                            context.DoWorkWithRetries(delegate ()
                            {
                                //Fetch all GFS Locations
                                gfsLocations = context.Services.LocationServiceClient.GetLocationInfoByEntityId(locRequest)?.LocationInfos?.Select(l => l.Code)?.Distinct()?.ToList() ?? new List<String>();
                            });
                        }

                        #endregion

                        Int32 batch;
                        ObservableCollection<Services.LocationProduct.LocationProductAttribute> gfsLocationProductAttributes = null;
                        Int32.TryParse(ConfigurationManager.AppSettings["LocationProductAttributeFetchAmount"], out batch);

                        if (batch == 0)
                        {
                            batch = 9999;
                        }

                        foreach (String locCode in gfsLocations)
                        {
                            #region Fetch GFS Location Product Attributes

                            if (this.SyncTarget.LastSync != null)
                            {
                                GetLocationProductAttributeByEntityNameLocationCodeDateChangedRequest LocationProductAttributeByEntityRequest = new GetLocationProductAttributeByEntityNameLocationCodeDateChangedRequest(this.EntityName, locCode, this.SyncTarget.LastSync.Value);

                                //Fetch changed GFS Location Product Attributes
                                context.DoWorkWithRetries(delegate ()
                                {
                                    gfsLocationProductAttributes = new ObservableCollection<Services.LocationProduct.LocationProductAttribute>(context.Services.LocationProductServiceClient.GetLocationProductAttributeByEntityNameLocationCodeDateChanged(LocationProductAttributeByEntityRequest).LocationProductAttributes);
                                });
                            }
                            else
                            {
                                GetLocationProductAttributeByEntityNameLocationCodeRequest LocationProductAttributeByEntityRequest = new GetLocationProductAttributeByEntityNameLocationCodeRequest(this.EntityName, locCode);

                                //Fetch all GFS Location Product Attributes
                                context.DoWorkWithRetries(delegate ()
                                {
                                    gfsLocationProductAttributes = new ObservableCollection<Services.LocationProduct.LocationProductAttribute>(context.Services.LocationProductServiceClient.GetLocationProductAttributeByEntityNameLocationCode(LocationProductAttributeByEntityRequest).LocationProductAttributes);
                                });
                            }

                            #endregion

                            #region Fetch CCM Location Product Attributes

                            //Fetch the CCM Location Product Attributes By Location for only products bought back from GFS
                            IEnumerable<LocationProductAttributeDto> ccmLocationProducts = null;
                            IEnumerable<String> productCodes = null;

                            //LocationId
                            Int32 locId = (short)(context.LocationMap.FirstOrDefault(x => x.Value.Code == locCode).Key);

                            productCodes = new ObservableCollection<String>(gfsLocationProductAttributes.Select(p => p.ProductGtin).Distinct());

                            List<Int32> productList = new List<Int32>();

                            foreach (Services.LocationProduct.LocationProductAttribute locationProductAttribute in gfsLocationProductAttributes)
                            {
                                productList.Add(context.ProductMap[locationProductAttribute.ProductGtin]);
                            }

                            context.DoWorkWithRetries(delegate ()
                            {
                                ccmLocationProducts = dal.FetchByEntityIdLocationIdProductIds(this.EntityId, Convert.ToInt16(locId), productList);
                                //ccmStoreProducts = dal.FetchByStoreIdProductCodes(storeId, productCodes);
                            });

                            #endregion

                            //Create ordered lists for CCM \ GFS location product attributes
                            List<Services.LocationProduct.LocationProductAttribute> gfsLocationProductAttributesOrderedList = gfsLocationProductAttributes.OrderBy(o =>
                            {
                                Int32 productId;
                                return context.ProductMap.TryGetValue(o.ProductGtin, out productId) ? productId : -1;
                            }
                                            ).ToList();
                            List<LocationProductAttributeDto> ccmLocationProductsOrderedList = ccmLocationProducts.OrderBy(o => o.ProductId).ToList();

                            //Create Store Product Dto List
                            List<LocationProductAttributeDto> ccmLocationProductDtoList = new List<LocationProductAttributeDto>();

                            int ccmPosition = 0;
                            int ccmProductFoundCounter = 0;

                            for (int gfsLocationProduct = 0; gfsLocationProduct < gfsLocationProductAttributesOrderedList.Count; gfsLocationProduct++)
                            {
                                LocationProductAttributeDto ccmLocationProductAttribute = null;
                                bool ccmProductFound = false;

                                Int32 productId;
                                if (!context.ProductMap.TryGetValue(gfsLocationProductAttributesOrderedList[gfsLocationProduct].ProductGtin, out productId)) continue;

                                for (int ccmLocationProduct = ccmPosition; ccmLocationProduct < ccmLocationProductsOrderedList.Count; ccmLocationProduct++)
                                {
                                    if (productId == ccmLocationProductsOrderedList[ccmLocationProduct].ProductId)
                                    {
                                        //The product has been found in CCM so we can use the store product details to compare sync differences etc
                                        ccmPosition = ccmLocationProduct + 1;
                                        ccmProductFoundCounter++;

                                        //Translation flag no set to tru to indicate product is found in CCM
                                        ccmLocationProductAttribute = ccmLocationProductsOrderedList[ccmLocationProduct];
                                        ccmProductFound = true;
                                        break;
                                    }


                                    if (ccmLocationProductsOrderedList[ccmLocationProduct].ProductId > productId)
                                    {
                                        ccmPosition = ccmLocationProduct;
                                        ccmProductFound = false;
                                        break;
                                    }
                                }

                                //CCM ProductId
                                Int32 ccmProdId = context.ProductMap[gfsLocationProductAttributesOrderedList[gfsLocationProduct].ProductGtin];

                                if (ccmProductFound)
                                {
                                    #region Existing Location Product Attribute
                                    //Check if a value has changed and update that little beast
                                    if (ccmLocationProductAttribute.Description != gfsLocationProductAttributesOrderedList[gfsLocationProduct].Description ||
                                        ccmLocationProductAttribute.CaseDeep != gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseDeep ||
                                        ccmLocationProductAttribute.CaseDepth != gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseDepth ||
                                        ccmLocationProductAttribute.CaseHeight != gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseHeight ||
                                        ccmLocationProductAttribute.CaseHigh != gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseHigh ||
                                        ccmLocationProductAttribute.CasePackUnits != gfsLocationProductAttributesOrderedList[gfsLocationProduct].CasePackUnits ||
                                        ccmLocationProductAttribute.CaseWide != gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseWide ||
                                        ccmLocationProductAttribute.CaseWidth != gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseWidth ||
                                        ccmLocationProductAttribute.ConsumerInformation != gfsLocationProductAttributesOrderedList[gfsLocationProduct].ConsumerInformation ||
                                        ccmLocationProductAttribute.CorporateCode != gfsLocationProductAttributesOrderedList[gfsLocationProduct].CorporateCode ||
                                        ccmLocationProductAttribute.DaysOfSupply != gfsLocationProductAttributesOrderedList[gfsLocationProduct].DaysOfSupply ||
                                        ccmLocationProductAttribute.DeliveryFrequencyDays != gfsLocationProductAttributesOrderedList[gfsLocationProduct].DeliveryFrequencyDays ||
                                        ccmLocationProductAttribute.DeliveryMethod != gfsLocationProductAttributesOrderedList[gfsLocationProduct].DeliveryMethod ||
                                        ccmLocationProductAttribute.Depth != gfsLocationProductAttributesOrderedList[gfsLocationProduct].Depth ||
                                        ccmLocationProductAttribute.GTIN != gfsLocationProductAttributesOrderedList[gfsLocationProduct].GTIN ||
                                        ccmLocationProductAttribute.Height != gfsLocationProductAttributesOrderedList[gfsLocationProduct].Height ||
                                        ccmLocationProductAttribute.ManufacturerCode != gfsLocationProductAttributesOrderedList[gfsLocationProduct].ManufacturerCode ||
                                        ccmLocationProductAttribute.MaxDeep != gfsLocationProductAttributesOrderedList[gfsLocationProduct].MaxDeep ||
                                        ccmLocationProductAttribute.MinDeep != gfsLocationProductAttributesOrderedList[gfsLocationProduct].MinDeep ||
                                        ccmLocationProductAttribute.Model != gfsLocationProductAttributesOrderedList[gfsLocationProduct].Model ||
                                        ccmLocationProductAttribute.Pattern != gfsLocationProductAttributesOrderedList[gfsLocationProduct].Pattern ||
                                        ccmLocationProductAttribute.RecommendedRetailPrice != gfsLocationProductAttributesOrderedList[gfsLocationProduct].RecommendedRetailPrice ||
                                        ccmLocationProductAttribute.SellPackCount != gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPackCount ||
                                        ccmLocationProductAttribute.SellPackDescription != gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPackDescription ||
                                        ccmLocationProductAttribute.SellPrice != gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPrice ||
                                        ccmLocationProductAttribute.ShelfLife != gfsLocationProductAttributesOrderedList[gfsLocationProduct].ShelfLife ||
                                        ccmLocationProductAttribute.Size != gfsLocationProductAttributesOrderedList[gfsLocationProduct].Size ||
                                        //((PlanogramProductStatusType)ccmLocationProductAttribute.StatusType).ToString() != gfsLocationProductAttributesOrderedList[gfsLocationProduct].StatusType ||
                                        ccmLocationProductAttribute.TrayDeep != gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayDeep ||
                                        ccmLocationProductAttribute.TrayDepth != gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayDepth ||
                                        ccmLocationProductAttribute.TrayThickHeight != gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickHeight ||
                                        ccmLocationProductAttribute.TrayThickWidth != gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickWidth ||
                                        ccmLocationProductAttribute.TrayWide != gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayWide ||
                                        ccmLocationProductAttribute.TrayWidth != gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayWidth ||
                                        ccmLocationProductAttribute.UnitOfMeasure != gfsLocationProductAttributesOrderedList[gfsLocationProduct].UnitOfMeasure ||
                                        ccmLocationProductAttribute.VendorCode != gfsLocationProductAttributesOrderedList[gfsLocationProduct].VendorCode ||
                                        ccmLocationProductAttribute.Width != gfsLocationProductAttributesOrderedList[gfsLocationProduct].Width)
                                    {
                                        ccmLocationProductAttribute.Description = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Description;
                                        //ccmLocationProductAttribute.CaseCost = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseCost;
                                        //ccmLocationProductAttribute.CostPrice = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CostPrice;
                                        ccmLocationProductAttribute.CaseDeep = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseDeep;
                                        ccmLocationProductAttribute.CaseDepth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseDepth;
                                        ccmLocationProductAttribute.CaseHeight = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseHeight;
                                        ccmLocationProductAttribute.CaseHigh = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseHigh;
                                        ccmLocationProductAttribute.CasePackUnits = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CasePackUnits;
                                        ccmLocationProductAttribute.CaseWide = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseWide;
                                        ccmLocationProductAttribute.CaseWidth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseWidth;
                                        ccmLocationProductAttribute.ConsumerInformation = gfsLocationProductAttributesOrderedList[gfsLocationProduct].ConsumerInformation;
                                        ccmLocationProductAttribute.CorporateCode = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CorporateCode;
                                        ccmLocationProductAttribute.DaysOfSupply = gfsLocationProductAttributesOrderedList[gfsLocationProduct].DaysOfSupply;
                                        ccmLocationProductAttribute.DeliveryFrequencyDays = gfsLocationProductAttributesOrderedList[gfsLocationProduct].DeliveryFrequencyDays;
                                        ccmLocationProductAttribute.DeliveryMethod = gfsLocationProductAttributesOrderedList[gfsLocationProduct].DeliveryMethod;
                                        ccmLocationProductAttribute.Depth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Depth;
                                        ccmLocationProductAttribute.EntityId = this.EntityId;
                                        ccmLocationProductAttribute.GTIN = gfsLocationProductAttributesOrderedList[gfsLocationProduct].GTIN;
                                        ccmLocationProductAttribute.Height = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Height;
                                        //Location map is: ccmproductgtin | services location dto
                                        ccmLocationProductAttribute.LocationId = (short)(context.LocationMap.FirstOrDefault(x => x.Value.Code == gfsLocationProductAttributesOrderedList[gfsLocationProduct].LocationCode).Key);
                                        ccmLocationProductAttribute.Manufacturer = gfsLocationProductAttributesOrderedList[gfsLocationProduct].LocationCode;
                                        ccmLocationProductAttribute.ManufacturerCode = gfsLocationProductAttributesOrderedList[gfsLocationProduct].ManufacturerCode;
                                        ccmLocationProductAttribute.MaxDeep = gfsLocationProductAttributesOrderedList[gfsLocationProduct].MaxDeep;
                                        ccmLocationProductAttribute.MinDeep = gfsLocationProductAttributesOrderedList[gfsLocationProduct].MinDeep;
                                        ccmLocationProductAttribute.Model = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Model;
                                        ccmLocationProductAttribute.Pattern = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Pattern;
                                        //Product map : ccmproductgtin | ccm product id
                                        ccmLocationProductAttribute.ProductId = context.ProductMap[gfsLocationProductAttributesOrderedList[gfsLocationProduct].ProductGtin];
                                        ccmLocationProductAttribute.RecommendedRetailPrice = gfsLocationProductAttributesOrderedList[gfsLocationProduct].RecommendedRetailPrice;
                                        ccmLocationProductAttribute.SellPackCount = gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPackCount;
                                        ccmLocationProductAttribute.SellPackDescription = gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPackDescription;
                                        ccmLocationProductAttribute.SellPrice = gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPrice;
                                        ccmLocationProductAttribute.ShelfLife = gfsLocationProductAttributesOrderedList[gfsLocationProduct].ShelfLife;
                                        ccmLocationProductAttribute.Size = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Size;
                                        ccmLocationProductAttribute.StatusType = (Byte)(EnumHelper.Parse<PlanogramProductStatusType>(gfsLocationProductAttributesOrderedList[gfsLocationProduct].StatusType, PlanogramProductStatusType.Active));
                                        ccmLocationProductAttribute.TrayDeep = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayDeep;
                                        ccmLocationProductAttribute.TrayDepth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayDepth;
                                        ccmLocationProductAttribute.TrayHeight = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickHeight;
                                        ccmLocationProductAttribute.TrayHigh = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayHigh;
                                        ccmLocationProductAttribute.TrayPackUnits = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayPackUnits;
                                        ccmLocationProductAttribute.TrayThickDepth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickDepth;
                                        ccmLocationProductAttribute.TrayThickHeight = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickHeight;
                                        ccmLocationProductAttribute.TrayThickWidth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickWidth;
                                        ccmLocationProductAttribute.TrayWide = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayWide;
                                        ccmLocationProductAttribute.TrayWidth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayWidth;
                                        ccmLocationProductAttribute.UnitOfMeasure = gfsLocationProductAttributesOrderedList[gfsLocationProduct].UnitOfMeasure;
                                        ccmLocationProductAttribute.Vendor = gfsLocationProductAttributesOrderedList[gfsLocationProduct].UnitOfMeasure;
                                        ccmLocationProductAttribute.VendorCode = gfsLocationProductAttributesOrderedList[gfsLocationProduct].VendorCode;
                                        ccmLocationProductAttribute.Width = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Width;

                                        ccmLocationProductDtoList.Add(ccmLocationProductAttribute);
                                    }
                                    #endregion
                                }
                                else
                                {
                                    #region New Location Product Attribute
                                    ccmLocationProductAttribute = new LocationProductAttributeDto();

                                    ccmLocationProductAttribute.Description = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Description;
                                    //ccmLocationProductAttribute.CaseCost = gfsLocationProductAttribute.CaseCost;
                                    //ccmLocationProductAttribute.CostPrice = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CostPrice;
                                    ccmLocationProductAttribute.CaseDeep = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseDeep;
                                    ccmLocationProductAttribute.CaseDepth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseDepth;
                                    ccmLocationProductAttribute.CaseHeight = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseHeight;
                                    ccmLocationProductAttribute.CaseHigh = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseHigh;
                                    ccmLocationProductAttribute.CasePackUnits = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CasePackUnits;
                                    ccmLocationProductAttribute.CaseWide = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseWide;
                                    ccmLocationProductAttribute.CaseWidth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CaseWidth;
                                    ccmLocationProductAttribute.ConsumerInformation = gfsLocationProductAttributesOrderedList[gfsLocationProduct].ConsumerInformation;
                                    ccmLocationProductAttribute.CorporateCode = gfsLocationProductAttributesOrderedList[gfsLocationProduct].CorporateCode;
                                    ccmLocationProductAttribute.DaysOfSupply = gfsLocationProductAttributesOrderedList[gfsLocationProduct].DaysOfSupply;
                                    ccmLocationProductAttribute.DeliveryFrequencyDays = gfsLocationProductAttributesOrderedList[gfsLocationProduct].DeliveryFrequencyDays;
                                    ccmLocationProductAttribute.DeliveryMethod = gfsLocationProductAttributesOrderedList[gfsLocationProduct].DeliveryMethod;
                                    ccmLocationProductAttribute.Depth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Depth;
                                    ccmLocationProductAttribute.EntityId = this.EntityId;
                                    ccmLocationProductAttribute.GTIN = gfsLocationProductAttributesOrderedList[gfsLocationProduct].GTIN;
                                    ccmLocationProductAttribute.Height = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Height;
                                    //Location map is: ccmproductgtin | services location dto
                                    ccmLocationProductAttribute.LocationId = (short)(context.LocationMap.FirstOrDefault(x => x.Value.Code == gfsLocationProductAttributesOrderedList[gfsLocationProduct].LocationCode).Key);
                                    ccmLocationProductAttribute.Manufacturer = gfsLocationProductAttributesOrderedList[gfsLocationProduct].LocationCode;
                                    ccmLocationProductAttribute.ManufacturerCode = gfsLocationProductAttributesOrderedList[gfsLocationProduct].ManufacturerCode;
                                    ccmLocationProductAttribute.MaxDeep = gfsLocationProductAttributesOrderedList[gfsLocationProduct].MaxDeep;
                                    ccmLocationProductAttribute.MinDeep = gfsLocationProductAttributesOrderedList[gfsLocationProduct].MinDeep;
                                    ccmLocationProductAttribute.Model = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Model;
                                    ccmLocationProductAttribute.Pattern = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Pattern;
                                    //Product map : ccmproductgtin | ccm product id
                                    ccmLocationProductAttribute.ProductId = context.ProductMap[gfsLocationProductAttributesOrderedList[gfsLocationProduct].ProductGtin];
                                    ccmLocationProductAttribute.RecommendedRetailPrice = gfsLocationProductAttributesOrderedList[gfsLocationProduct].RecommendedRetailPrice;
                                    ccmLocationProductAttribute.SellPackCount = gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPackCount;
                                    ccmLocationProductAttribute.SellPackDescription = gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPackDescription;
                                    ccmLocationProductAttribute.SellPrice = gfsLocationProductAttributesOrderedList[gfsLocationProduct].SellPrice;
                                    ccmLocationProductAttribute.ShelfLife = gfsLocationProductAttributesOrderedList[gfsLocationProduct].ShelfLife;
                                    ccmLocationProductAttribute.Size = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Size;
                                    ccmLocationProductAttribute.StatusType = (Byte)(EnumHelper.Parse<PlanogramProductStatusType>(gfsLocationProductAttributesOrderedList[gfsLocationProduct].StatusType, PlanogramProductStatusType.Active));
                                    ccmLocationProductAttribute.TrayDeep = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayDeep;
                                    ccmLocationProductAttribute.TrayDepth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayDepth;
                                    ccmLocationProductAttribute.TrayHeight = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickHeight;
                                    ccmLocationProductAttribute.TrayHigh = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayHigh;
                                    ccmLocationProductAttribute.TrayPackUnits = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayPackUnits;
                                    ccmLocationProductAttribute.TrayThickDepth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickDepth;
                                    ccmLocationProductAttribute.TrayThickHeight = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickHeight;
                                    ccmLocationProductAttribute.TrayThickWidth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayThickWidth;
                                    ccmLocationProductAttribute.TrayWide = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayWide;
                                    ccmLocationProductAttribute.TrayWidth = gfsLocationProductAttributesOrderedList[gfsLocationProduct].TrayWidth;
                                    ccmLocationProductAttribute.UnitOfMeasure = gfsLocationProductAttributesOrderedList[gfsLocationProduct].UnitOfMeasure;
                                    ccmLocationProductAttribute.Vendor = gfsLocationProductAttributesOrderedList[gfsLocationProduct].UnitOfMeasure;
                                    ccmLocationProductAttribute.VendorCode = gfsLocationProductAttributesOrderedList[gfsLocationProduct].VendorCode;
                                    ccmLocationProductAttribute.Width = gfsLocationProductAttributesOrderedList[gfsLocationProduct].Width;

                                    ccmLocationProductDtoList.Add(ccmLocationProductAttribute);
                                    #endregion
                                }

                                if (ccmLocationProductDtoList.Count > batch)
                                {
                                    dal.Upsert(ccmLocationProductDtoList, null);
                                    ccmLocationProductDtoList.Clear();
                                }
                            }

                            if (ccmLocationProductDtoList.Count > 0)
                            {
                                dal.Upsert(ccmLocationProductDtoList, null);
                                ccmLocationProductDtoList.Clear();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationProductAttribute], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to SA
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Location Product Attribute Deletes

        private void SynchronizeLocationProductAttributeDeleted(ProcessContext context)
        {
            // Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationProductAttributeDal dal = context.DalContext.GetDal<ILocationProductAttributeDal>())
                {
                    List<Services.LocationProduct.LocationProductAttribute> gfsItemsDeleted = null;

                    // Ensure that it has sync'd before
                    if (SyncTarget.LastSync != null)
                    {
                        //todo V8-30276
                        try
                        {
                            // Fetch the deleted clusters
                            context.DoWorkWithRetries(delegate()
                            {
                                gfsItemsDeleted = context.Services.LocationProductServiceClient.GetLocationProductAttributeByEntityNameDateDeleted(
                                    new Services.LocationProduct.GetLocationProductAttributeByEntityNameDateDeletedRequest(this.EntityName, SyncTarget.LastSync)).LocationProductAttributes;
                            });
                        }
                        catch (Exception e)
                        { }
                        if (gfsItemsDeleted != null && gfsItemsDeleted.Any())
                        {
                            IEnumerable<LocationProductAttributeDto> ccmItems = dal.FetchByEntityId(this.EntityId);
                            if (ccmItems.Any())
                            {
                                Dictionary<String, Int16> locCodeToIds;
                                using (ILocationInfoDal locInfoDal = context.DalContext.GetDal<ILocationInfoDal>())
                                {
                                    locCodeToIds =
                                        locInfoDal.FetchByEntityIdLocationCodes(this.EntityId, gfsItemsDeleted.Select(s => s.LocationCode)).Distinct()
                                        .ToDictionary(l => l.Code, l => l.Id);
                                }

                                Dictionary<String, Int32> prodCodeToIds;
                                using (IProductInfoDal infoDal = context.DalContext.GetDal<IProductInfoDal>())
                                {
                                    prodCodeToIds =
                                        infoDal.FetchByEntityIdProductGtins(this.EntityId, gfsItemsDeleted.Select(s => s.ProductGtin)).Distinct()
                                        .ToDictionary(l => l.Gtin, l => l.Id);
                                }


                                // loop through the items to delete
                                foreach (Services.LocationProduct.LocationProductAttribute gfsItem in gfsItemsDeleted)
                                {
                                    Int16 ccmLocId;
                                    if (!locCodeToIds.TryGetValue(gfsItem.LocationCode, out ccmLocId)) continue;

                                    Int32 ccmProdId;
                                    if (!prodCodeToIds.TryGetValue(gfsItem.ProductGtin, out ccmProdId)) continue;

                                    LocationProductAttributeDto ccmItem = ccmItems.FirstOrDefault(i => i.LocationId == ccmLocId && i.ProductId == ccmProdId);
                                    if (ccmItem == null) continue;


                                    dal.DeleteById(ccmLocId, ccmProdId);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationProductAttribute], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Location Product Illegal

        /// <summary>
        /// Synchronizes the metrics within the solution
        /// </summary>
        /// <param name="context"></param>
        private void SynchronizeLocationProductIllegal(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationProductIllegalDal dal = context.DalContext.GetDal<ILocationProductIllegalDal>())
                {
                    IEnumerable<LocationProductIllegalDto> ccmLocationProductIllegals = dal.FetchByEntityId(this.EntityId);

                    ObservableCollection<Services.LocationProduct.LocationProductIllegal> gfsLocationProductIllegals = null;

                    GetLocationProductIllegalByEntityNameRequest LocationProductIllegalByEntityRequest = new GetLocationProductIllegalByEntityNameRequest(this.EntityName);

                    context.DoWorkWithRetries(delegate()
                    {
                        gfsLocationProductIllegals = new ObservableCollection<Services.LocationProduct.LocationProductIllegal>(context.Services.LocationProductServiceClient.GetLocationProductIllegalByEntityName(LocationProductIllegalByEntityRequest).LocationProductsIllegal);
                    });

                    foreach (var gfsLocationProductIllegal in gfsLocationProductIllegals)
                    {
                        LocationProductIllegalDto ccmLocationProductIllegal = null;

                        //Resolve matches on Id's using the:
                        //LocationId ~ LocationCode
                        //ProductId ~ ProductGtin

                        short ccmLocationIdToMatchOn = (short)(context.LocationMap.FirstOrDefault(x => x.Value.Code == gfsLocationProductIllegal.LocationCode).Key);
                        Int32 ccmProductIdToMapOn = context.ProductMap[gfsLocationProductIllegal.GTIN]; //(Gtin is the produts gtin)

                        if (ccmLocationProductIllegals != null)
                        {
                            ccmLocationProductIllegal = ccmLocationProductIllegals.FirstOrDefault(i => i.LocationId == ccmLocationIdToMatchOn && i.ProductId == ccmProductIdToMapOn);
                        }

                        if (ccmLocationProductIllegal == null)
                        {
                            ccmLocationProductIllegal = new LocationProductIllegalDto();

                            ccmLocationProductIllegal.EntityId = this.EntityId;
                            ccmLocationProductIllegal.LocationId = ccmLocationIdToMatchOn;
                            ccmLocationProductIllegal.ProductId = ccmProductIdToMapOn;

                            dal.Insert(ccmLocationProductIllegal);
                        }
                        else
                        {
                            if (ccmLocationProductIllegal.LocationId == ccmLocationIdToMatchOn ||
                                ccmLocationProductIllegal.ProductId == ccmProductIdToMapOn)
                            {
                                //I don't think we should ever come into here because we match on theese values however just to be safe:
                                ccmLocationProductIllegal.LocationId = ccmLocationIdToMatchOn;
                                ccmLocationProductIllegal.ProductId = ccmProductIdToMapOn;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationProductIllegal], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to SA
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Location Product Illegal Deletes

        private void SynchronizeLocationProductIllegalDeleted(ProcessContext context)
        {
            // Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationProductIllegalDal dal = context.DalContext.GetDal<ILocationProductIllegalDal>())
                {
                    List<Services.LocationProduct.LocationProductIllegal> gfsItemsDeleted = null;

                    // Ensure that it has sync'd before
                    if (SyncTarget.LastSync != null)
                    {
                        // Fetch the deleted clusters
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsItemsDeleted = context.Services.LocationProductServiceClient.GetLocationProductIllegalByEntityNameDateDeleted(
                                new Services.LocationProduct.GetLocationProductIllegalByEntityNameDateDeletedRequest(this.EntityName, SyncTarget.LastSync)).LocationProductsIllegal;
                        });

                        if (gfsItemsDeleted.Any())
                        {
                            IEnumerable<LocationProductIllegalDto> ccmItems = dal.FetchByEntityId(this.EntityId);
                            if (ccmItems.Any())
                            {
                                Dictionary<String, Int16> locCodeToIds;
                                using (ILocationInfoDal locInfoDal = context.DalContext.GetDal<ILocationInfoDal>())
                                {
                                    locCodeToIds =
                                        locInfoDal.FetchByEntityIdLocationCodes(this.EntityId, gfsItemsDeleted.Select(s => s.LocationCode)).Distinct()
                                        .ToDictionary(l => l.Code, l => l.Id);
                                }

                                Dictionary<String, Int32> prodCodeToIds;
                                using (IProductInfoDal infoDal = context.DalContext.GetDal<IProductInfoDal>())
                                {
                                    prodCodeToIds =
                                        infoDal.FetchByEntityIdProductGtins(this.EntityId, gfsItemsDeleted.Select(s => s.GTIN)).Distinct()
                                        .ToDictionary(l => l.Gtin, l => l.Id);
                                }


                                // loop through the items to delete
                                foreach (Services.LocationProduct.LocationProductIllegal gfsItem in gfsItemsDeleted)
                                {
                                    Int16 ccmLocId;
                                    if (!locCodeToIds.TryGetValue(gfsItem.LocationCode, out ccmLocId)) continue;

                                    Int32 ccmProdId;
                                    if (!prodCodeToIds.TryGetValue(gfsItem.GTIN, out ccmProdId)) continue;

                                    LocationProductIllegalDto ccmItem = ccmItems.FirstOrDefault(i => i.LocationId == ccmLocId && i.ProductId == ccmProdId);
                                    if (ccmItem == null) continue;


                                    dal.DeleteById(ccmLocId, ccmProdId);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationProductIllegal], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Location Product Legal

        /// <summary>
        /// Synchronizes the metrics within the solution
        /// </summary>
        /// <param name="context"></param>
        private void SynchronizeLocationProductLegal(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationProductLegalDal dal = context.DalContext.GetDal<ILocationProductLegalDal>())
                {
                    IEnumerable<LocationProductLegalDto> ccmLocationProductLegals = dal.FetchByEntityId(this.EntityId);

                    ObservableCollection<Services.LocationProduct.LocationProductLegal> gfsLocationProductLegals = null;

                    GetLocationProductLegalByEntityNameRequest LocationProductLegalByEntityRequest = new GetLocationProductLegalByEntityNameRequest(this.EntityName);

                    context.DoWorkWithRetries(delegate()
                    {
                        gfsLocationProductLegals = new ObservableCollection<Services.LocationProduct.LocationProductLegal>(context.Services.LocationProductServiceClient.GetLocationProductLegalByEntityName(LocationProductLegalByEntityRequest).LocationProductsLegal);
                    });

                    foreach (var gfsLocationProductLegal in gfsLocationProductLegals)
                    {
                        LocationProductLegalDto ccmLocationProductLegal = null;

                        //Resolve matches on Id's using the:
                        //LocationId ~ LocationCode
                        //ProductId ~ ProductGtin

                        short ccmLocationIdToMatchOn = (short)(context.LocationMap.FirstOrDefault(x => x.Value.Code == gfsLocationProductLegal.LocationCode).Key);
                        Int32 ccmProductIdToMapOn = context.ProductMap[gfsLocationProductLegal.GTIN]; //(Gtin is the produts gtin)

                        if (ccmLocationProductLegals != null)
                        {
                            ccmLocationProductLegal = ccmLocationProductLegals.FirstOrDefault(i => i.LocationId == ccmLocationIdToMatchOn && i.ProductId == ccmProductIdToMapOn);
                        }

                        if (ccmLocationProductLegal == null)
                        {
                            ccmLocationProductLegal = new LocationProductLegalDto();

                            ccmLocationProductLegal.EntityId = this.EntityId;
                            ccmLocationProductLegal.LocationId = ccmLocationIdToMatchOn;
                            ccmLocationProductLegal.ProductId = ccmProductIdToMapOn;

                            dal.Insert(ccmLocationProductLegal);
                        }
                        else
                        {
                            if (ccmLocationProductLegal.LocationId == ccmLocationIdToMatchOn ||
                                ccmLocationProductLegal.ProductId == ccmProductIdToMapOn)
                            {
                                //I don't think we should ever come into here because we match on theese values however just to be safe:
                                ccmLocationProductLegal.LocationId = ccmLocationIdToMatchOn;
                                ccmLocationProductLegal.ProductId = ccmProductIdToMapOn;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationProductLegal], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to SA
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Location Product Legal Deletes

        private void SynchronizeLocationProductLegalDeleted(ProcessContext context)
        {
            // Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationProductLegalDal dal = context.DalContext.GetDal<ILocationProductLegalDal>())
                {
                    List<Services.LocationProduct.LocationProductLegal> gfsItemsDeleted = null;

                    // Ensure that it has sync'd before
                    if (SyncTarget.LastSync != null)
                    {
                        // Fetch the deleted clusters
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsItemsDeleted = context.Services.LocationProductServiceClient.GetLocationProductLegalByEntityNameDateDeleted(
                                new Services.LocationProduct.GetLocationProductLegalByEntityNameDateDeletedRequest(this.EntityName, SyncTarget.LastSync)).LocationProductsLegal;
                        });

                        if (gfsItemsDeleted.Any())
                        {
                            IEnumerable<LocationProductLegalDto> ccmItems = dal.FetchByEntityId(this.EntityId);
                            if (ccmItems.Any())
                            {
                                Dictionary<String, Int16> locCodeToIds;
                                using (ILocationInfoDal locInfoDal = context.DalContext.GetDal<ILocationInfoDal>())
                                {
                                    locCodeToIds =
                                        locInfoDal.FetchByEntityIdLocationCodes(this.EntityId, gfsItemsDeleted.Select(s => s.LocationCode)).Distinct()
                                        .ToDictionary(l => l.Code, l => l.Id);
                                }

                                Dictionary<String, Int32> prodCodeToIds;
                                using (IProductInfoDal infoDal = context.DalContext.GetDal<IProductInfoDal>())
                                {
                                    prodCodeToIds =
                                        infoDal.FetchByEntityIdProductGtins(this.EntityId, gfsItemsDeleted.Select(s => s.GTIN)).Distinct()
                                        .ToDictionary(l => l.Gtin, l => l.Id);
                                }


                                // loop through the items to delete
                                foreach (Services.LocationProduct.LocationProductLegal gfsItem in gfsItemsDeleted)
                                {
                                    Int16 ccmLocId;
                                    if (!locCodeToIds.TryGetValue(gfsItem.LocationCode, out ccmLocId)) continue;

                                    Int32 ccmProdId;
                                    if (!prodCodeToIds.TryGetValue(gfsItem.GTIN, out ccmProdId)) continue;

                                    LocationProductLegalDto ccmItem = ccmItems.FirstOrDefault(i => i.LocationId == ccmLocId && i.ProductId == ccmProdId);
                                    if (ccmItem == null) continue;


                                    dal.DeleteById(ccmLocId, ccmProdId);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationProductLegal], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Location Space

        /// <summary>
        /// Syncrhonises the location space records within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        private void SynchronizeLocationSpace(ProcessContext context)
        {
            //Start the tranccmction
            context.DalContext.Begin();

            try
            {
                using (ILocationSpaceInfoDal dalLocationSpaceInfo = context.DalContext.GetDal<ILocationSpaceInfoDal>())
                {
                    using (ILocationSpaceDal dalLocationSpace = context.DalContext.GetDal<ILocationSpaceDal>())
                    {
                        using (ILocationSpaceProductGroupDal dalLocationSpaceProductGroup = context.DalContext.GetDal<ILocationSpaceProductGroupDal>())
                        {
                            using (ILocationSpaceBayDal dalLocationSpaceBay = context.DalContext.GetDal<ILocationSpaceBayDal>())
                            {
                                using (ILocationSpaceElementDal dalLocationSpaceElement = context.DalContext.GetDal<ILocationSpaceElementDal>())
                                {
                                    using (ILocationInfoDal dalLocation = context.DalContext.GetDal<ILocationInfoDal>())
                                    {

                                        ObservableCollection<Services.LocationSpace.LocationSpaceInfo> gfsLocationSpaceInfo = null;

                                        //Get the cluster info 
                                        context.DoWorkWithRetries(delegate()
                                        {
                                            GetLocationSpaceInfoByEntityNameRequest locationSpaceByEntityNameRequest = new GetLocationSpaceInfoByEntityNameRequest(this.EntityName);

                                            gfsLocationSpaceInfo = new ObservableCollection<Services.LocationSpace.LocationSpaceInfo>(context.Services.LocationSpaceServiceClient.GetLocationSpaceInfoByEntityName(locationSpaceByEntityNameRequest).LocationSpaceInfos);
                                        });

                                        //If theres location space records to process
                                        if (gfsLocationSpaceInfo != null && gfsLocationSpaceInfo.Count > 0)
                                        {
                                            //Create lookup for ccm location space info
                                            Dictionary<Int16, LocationSpaceInfoDto> ccmLocationSpaceInfoLookup = new Dictionary<Int16, LocationSpaceInfoDto>();

                                            //Fetch all CCM location space info records
                                            IEnumerable<LocationSpaceInfoDto> ccmLocationSpaceInfoList = dalLocationSpaceInfo.FetchByEntityId(this.EntityId);

                                            //Enumerate through and add to the dictionary
                                            foreach (LocationSpaceInfoDto locationSpaceInfoDto in ccmLocationSpaceInfoList)
                                            {
                                                //If item does not already exist
                                                if (!ccmLocationSpaceInfoLookup.ContainsKey(locationSpaceInfoDto.LocationId))
                                                {
                                                    //Add to dictionary
                                                    ccmLocationSpaceInfoLookup.Add(locationSpaceInfoDto.LocationId, locationSpaceInfoDto);
                                                }
                                            }

                                            // Loop through the cluster info list 
                                            foreach (Services.LocationSpace.LocationSpaceInfo locationSpace in gfsLocationSpaceInfo)
                                            {
                                                // check if it is the latest version
                                                if (locationSpace.IsLatestVersion == true)
                                                {
                                                    //Lookup Location Id from the code.
                                                    IEnumerable<LocationInfoDto> locations = dalLocation.FetchByEntityIdLocationCodes(this.EntityId, new List<String>() { locationSpace.LocationCode });
                                                    LocationInfoDto location = locations.FirstOrDefault();
                                                    Int16 locationId = location.Id;

                                                    //Fetch the location space details for the content ref
                                                    Services.LocationSpace.LocationSpace gfsLocationSpace = null;

                                                    //Check if the location space info record exists
                                                    LocationSpaceInfoDto ccmLocationSpaceInfo = null;
                                                    ccmLocationSpaceInfoLookup.TryGetValue(locationId, out ccmLocationSpaceInfo);

                                                    //Create new location space dto
                                                    LocationSpaceDto ccmLocationSpaceDto;

                                                    #region INSERT/UPDATE CCM Location Space
                                                    Boolean updateChildren = false;
                                                    if (ccmLocationSpaceInfo == null)
                                                    {
                                                        context.DoWorkWithRetries(delegate()
                                                        {
                                                            GetLocationSpaceByUniqueContentReferenceRequest GetLocationSpaceByUniqueContentReference = new GetLocationSpaceByUniqueContentReferenceRequest(locationSpace.UniqueContentReference);

                                                            gfsLocationSpace = (context.Services.LocationSpaceServiceClient.GetLocationSpaceByUniqueContentReference(GetLocationSpaceByUniqueContentReference).LocationSpace);
                                                        });
                                                        ccmLocationSpaceDto = new LocationSpaceDto();

                                                        ccmLocationSpaceDto.UniqueContentReference = gfsLocationSpace.UniqueContentReference;
                                                        ccmLocationSpaceDto.LocationId = locationId;
                                                        ccmLocationSpaceDto.EntityId = this.EntityId;

                                                        if (gfsLocationSpace.ParentUniqueContentReference != null)
                                                        {
                                                            ccmLocationSpaceDto.ParentUniqueContentReference = gfsLocationSpace.ParentUniqueContentReference;
                                                        }
                                                        else
                                                        {
                                                            ccmLocationSpaceDto.ParentUniqueContentReference = gfsLocationSpace.UniqueContentReference;
                                                        }

                                                        dalLocationSpace.Insert(ccmLocationSpaceDto);
                                                        updateChildren = true;
                                                    }
                                                    else
                                                    {
                                                        ccmLocationSpaceDto = dalLocationSpace.FetchById(ccmLocationSpaceInfo.Id);

                                                        if (ccmLocationSpaceDto.UniqueContentReference != locationSpace.UniqueContentReference ||
                                                            SyncTarget.LastSync == null ||
                                                            (locationSpace.DateLastModified != null && SyncTarget.LastSync != null && locationSpace.DateLastModified > SyncTarget.LastSync.Value))
                                                        {
                                                            context.DoWorkWithRetries(delegate()
                                                            {
                                                                GetLocationSpaceByUniqueContentReferenceRequest GetLocationSpaceByUniqueContentReference = new GetLocationSpaceByUniqueContentReferenceRequest(locationSpace.UniqueContentReference);

                                                                gfsLocationSpace = (context.Services.LocationSpaceServiceClient.GetLocationSpaceByUniqueContentReference(GetLocationSpaceByUniqueContentReference).LocationSpace);
                                                            });
                                                            //Update fields
                                                            ccmLocationSpaceDto.UniqueContentReference = gfsLocationSpace.UniqueContentReference;

                                                            if (gfsLocationSpace.ParentUniqueContentReference != null)
                                                            {
                                                                ccmLocationSpaceDto.ParentUniqueContentReference = gfsLocationSpace.ParentUniqueContentReference;
                                                            }
                                                            else
                                                            {
                                                                ccmLocationSpaceDto.ParentUniqueContentReference = gfsLocationSpace.UniqueContentReference;
                                                            }

                                                            dalLocationSpace.Update(ccmLocationSpaceDto);
                                                            updateChildren = true;
                                                        }
                                                    }
                                                    #endregion
                                                    if (updateChildren)
                                                    {
                                                        ObservableCollection<Services.LocationSpace.LocationSpaceProductGroup> gfsLocationSpaceProdGroups;
                                                        gfsLocationSpaceProdGroups = new ObservableCollection<Services.LocationSpace.LocationSpaceProductGroup>(gfsLocationSpace.ProductGroups);

                                                        LocationSpaceProductGroupDto ccmLocationSpaceProductGroupDto;
                                                        LocationSpaceBayDto ccmLocationSpaceBayDto;
                                                        LocationSpaceElementDto ccmLocationSpaceElementDto;

                                                        //get the location space product group records from CCM
                                                        IEnumerable<LocationSpaceProductGroupDto> ccmLocationSpaceProductGroups = dalLocationSpaceProductGroup.FetchByLocationSpaceId(ccmLocationSpaceDto.Id);

                                                        #region DELETE location space product groups

                                                        //GFS-23153 Updated to delete in bulk instead of individual deletes
                                                        dalLocationSpaceProductGroup.DeleteByLocationSpaceId(ccmLocationSpaceDto.Id);

                                                        #endregion

                                                        //Create dictionaries to store the data for insertion further on
                                                        Dictionary<Int32, LocationSpaceProductGroupDto> ccmLocationSpaceProductGroupDtoList = new Dictionary<Int32, LocationSpaceProductGroupDto>();
                                                        Dictionary<Int32, Dictionary<Byte, LocationSpaceBayDto>> ccmLocationSpaceBayDtoList = new Dictionary<Int32, Dictionary<Byte, LocationSpaceBayDto>>();
                                                        Dictionary<Tuple<Int32, Byte>, List<LocationSpaceElementDto>> ccmLocationSpaceElementDtoList = new Dictionary<Tuple<Int32, Byte>, List<LocationSpaceElementDto>>();

                                                        //Enumerate through gfs location space product groups
                                                        foreach (Services.LocationSpace.LocationSpaceProductGroup gfsProdGroup in gfsLocationSpaceProdGroups)
                                                        {
                                                            Int32 productGroupId;
                                                            if (context.ProductGroupIdMap.TryGetValue(gfsProdGroup.ProductGroupId, out productGroupId))
                                                            {
                                                                //Create new Location Space Product Group dto
                                                                ccmLocationSpaceProductGroupDto = new LocationSpaceProductGroupDto();

                                                                ccmLocationSpaceProductGroupDto.ProductGroupId = productGroupId;
                                                                ccmLocationSpaceProductGroupDto.LocationSpaceId = ccmLocationSpaceDto.Id;
                                                                ccmLocationSpaceProductGroupDto.BayCount = gfsProdGroup.BayCount;
                                                                ccmLocationSpaceProductGroupDto.ProductCount = gfsProdGroup.ProductCount;
                                                                ccmLocationSpaceProductGroupDto.AverageBayWidth = gfsProdGroup.AverageBayWidth;
                                                                ccmLocationSpaceProductGroupDto.AisleName = gfsProdGroup.AisleName;
                                                                ccmLocationSpaceProductGroupDto.ValleyName = gfsProdGroup.ValleyName;
                                                                ccmLocationSpaceProductGroupDto.ZoneName = gfsProdGroup.ZoneName;
                                                                ccmLocationSpaceProductGroupDto.CustomAttribute01 = gfsProdGroup.CustomAttribute01;
                                                                ccmLocationSpaceProductGroupDto.CustomAttribute02 = gfsProdGroup.CustomAttribute02;
                                                                ccmLocationSpaceProductGroupDto.CustomAttribute03 = gfsProdGroup.CustomAttribute03;
                                                                ccmLocationSpaceProductGroupDto.CustomAttribute04 = gfsProdGroup.CustomAttribute04;
                                                                ccmLocationSpaceProductGroupDto.CustomAttribute05 = gfsProdGroup.CustomAttribute05;

                                                                #region Build up CCM Location Space Bay Dtos
                                                                //Enumerate through gfs location space product group and get all of the bays
                                                                List<LocationSpaceBayDto> ccmGroupLocationSpaceBayDtoList = new List<LocationSpaceBayDto>();
                                                                foreach (Services.LocationSpace.LocationSpaceBay gfsBay in gfsProdGroup.Bays)
                                                                {
                                                                    //Create new Location Space Bay dto
                                                                    ccmLocationSpaceBayDto = new LocationSpaceBayDto();

                                                                    ccmLocationSpaceBayDto.BaseDepth = gfsBay.BaseDepth;
                                                                    ccmLocationSpaceBayDto.BaseHeight = gfsBay.BaseHeight;
                                                                    ccmLocationSpaceBayDto.BaseWidth = gfsBay.BaseWidth;
                                                                    ccmLocationSpaceBayDto.Depth = gfsBay.Depth;
                                                                    ccmLocationSpaceBayDto.Height = gfsBay.Height;
                                                                    ccmLocationSpaceBayDto.Order = gfsBay.Order;
                                                                    ccmLocationSpaceBayDto.Width = gfsBay.Width;

                                                                    ccmLocationSpaceBayDto.IsAisleStart = gfsBay.IsAisleStart;
                                                                    ccmLocationSpaceBayDto.IsAisleEnd = gfsBay.IsAisleEnd;
                                                                    ccmLocationSpaceBayDto.IsAisleRight = gfsBay.IsAisleRight;
                                                                    ccmLocationSpaceBayDto.IsAisleLeft = gfsBay.IsAisleLeft;
                                                                    ccmLocationSpaceBayDto.ManufacturerName = gfsBay.ManufacturerName;
                                                                    ccmLocationSpaceBayDto.Barcode = gfsBay.Barcode;
                                                                    ccmLocationSpaceBayDto.HasPower = gfsBay.HasPower;
                                                                    ccmLocationSpaceBayDto.AssetNumber = gfsBay.AssetNumber;
                                                                    ccmLocationSpaceBayDto.Temperature = gfsBay.Temperature;
                                                                    ccmLocationSpaceBayDto.Colour = gfsBay.Colour;
                                                                    ccmLocationSpaceBayDto.BayLocationRef = gfsBay.BayLocationRef;
                                                                    ccmLocationSpaceBayDto.BayFloorDrawingNo = gfsBay.BayFloorDrawingNo;
                                                                    ccmLocationSpaceBayDto.NotchStartY = gfsBay.NotchStartY;
                                                                    ccmLocationSpaceBayDto.IsPromotional = gfsBay.IsPromotional;
                                                                    ccmLocationSpaceBayDto.FixtureShape = (Byte)(EnumHelper.Parse<LocationSpaceFixtureShapeType>(gfsBay.BayType, LocationSpaceFixtureShapeType.Rectangle));
                                                                    ccmLocationSpaceBayDto.FixtureType = (Byte)(EnumHelper.Parse<LocationSpaceFixtureType>(gfsBay.BayType, LocationSpaceFixtureType.Standard));
                                                                    ccmLocationSpaceBayDto.FixtureName = gfsBay.FixtureName;
                                                                    ccmLocationSpaceBayDto.NotchPitch = gfsBay.NotchPitch;
                                                                    ccmLocationSpaceBayDto.BayTypeCalculation = gfsBay.BayTypeCalculation;
                                                                    ccmLocationSpaceBayDto.BayTypePostFix = gfsBay.BayTypePostFix;
                                                                    ccmLocationSpaceBayDto.BayType = (Byte)(EnumHelper.Parse<LocationSpaceBayType>(gfsBay.BayType, LocationSpaceBayType.None));

                                                                    //Set LocationSpaceProductGroupId after product groups are inserted

                                                                    //Add to list ready to added to grouped list for insertion
                                                                    ccmGroupLocationSpaceBayDtoList.Add(ccmLocationSpaceBayDto);

                                                                    #region Build up CCM Location Space Element Dtos
                                                                    List<LocationSpaceElementDto> ccmBayLocationSpaceElementDtoList = new List<LocationSpaceElementDto>();
                                                                    //CCM26468 - Force sequencial order
                                                                    Byte elementOrder = 1;
                                                                    //Enumerate through location space bay and get all of the elements
                                                                    foreach (Services.LocationSpace.LocationSpaceElement gfsElement in gfsBay.Elements.OrderBy(p => p.Order))
                                                                    {
                                                                        //Create new location space element dto
                                                                        ccmLocationSpaceElementDto = new LocationSpaceElementDto();

                                                                        ccmLocationSpaceElementDto.Depth = gfsElement.Depth;
                                                                        ccmLocationSpaceElementDto.FaceThickness = gfsElement.FaceThickness;
                                                                        ccmLocationSpaceElementDto.Height = gfsElement.Height;
                                                                        ccmLocationSpaceElementDto.MerchandisableHeight = gfsElement.MerchandisableHeight;
                                                                        ccmLocationSpaceElementDto.NotchNumber = gfsElement.NotchNumber;
                                                                        ccmLocationSpaceElementDto.Order = elementOrder;
                                                                        ccmLocationSpaceElementDto.Width = gfsElement.Width;
                                                                        ccmLocationSpaceElementDto.X = gfsElement.X;
                                                                        ccmLocationSpaceElementDto.Y = gfsElement.Y;
                                                                        ccmLocationSpaceElementDto.Z = gfsElement.Z;

                                                                        //Set LocationSpaceBayId after location space bays are inserted

                                                                        //Add to list ready to added to grouped list for insertion
                                                                        ccmBayLocationSpaceElementDtoList.Add(ccmLocationSpaceElementDto);

                                                                        //CCM26468 - Increment order
                                                                        elementOrder++;
                                                                    }
                                                                    //Add to list for bulk insert
                                                                    ccmLocationSpaceElementDtoList.Add(new Tuple<Int32, Byte>(gfsProdGroup.ProductGroupId, gfsBay.Order), ccmBayLocationSpaceElementDtoList);
                                                                    #endregion
                                                                }


                                                                //Add to list for bulk insert
                                                                ccmLocationSpaceProductGroupDtoList.Add(gfsProdGroup.ProductGroupId, ccmLocationSpaceProductGroupDto);

                                                                //Add to list for bulk insert
                                                                ccmLocationSpaceBayDtoList.Add(gfsProdGroup.ProductGroupId, ccmGroupLocationSpaceBayDtoList.ToDictionary(p => p.Order));
                                                                #endregion
                                                            }
                                                        }

                                                        #region Insert Location Space Product Group
                                                        //Insert all location space product groups at once
                                                        dalLocationSpaceProductGroup.Upsert(ccmLocationSpaceProductGroupDtoList.Values);
                                                        #endregion

                                                        #region Resolve & Insert Location Space Bays

                                                        //Resolve location space bay parent ids
                                                        List<LocationSpaceBayDto> ccmLocationSpaceBayDtoListToInsert = new List<LocationSpaceBayDto>();
                                                        foreach (Int32 gfsProductGroupId in ccmLocationSpaceBayDtoList.Keys)
                                                        {
                                                            //Resolve the id
                                                            LocationSpaceProductGroupDto ccmProductGroupDto;
                                                            if (ccmLocationSpaceProductGroupDtoList.TryGetValue(gfsProductGroupId, out ccmProductGroupDto))
                                                            {
                                                                Dictionary<Byte, LocationSpaceBayDto> ccmBayDtoList = ccmLocationSpaceBayDtoList[gfsProductGroupId];
                                                                foreach (LocationSpaceBayDto ccmBayDto in ccmBayDtoList.Values)
                                                                {
                                                                    //Set now updated CCM location space product group reference
                                                                    ccmBayDto.LocationSpaceProductGroupId = ccmProductGroupDto.Id;
                                                                }
                                                                ccmLocationSpaceBayDtoListToInsert.AddRange(ccmBayDtoList.Values);
                                                            }
                                                        }

                                                        //Insert location space bay dtos
                                                        dalLocationSpaceBay.Upsert(ccmLocationSpaceBayDtoListToInsert);

                                                        #endregion

                                                        #region Resolve & Insert Location Space Elements

                                                        //Resolve location space element parent ids
                                                        List<LocationSpaceElementDto> ccmLocationSpaceElementDtoListToInsert = new List<LocationSpaceElementDto>();
                                                        foreach (Tuple<Int32, Byte> ccmProductGroupBayCombo in ccmLocationSpaceElementDtoList.Keys)
                                                        {
                                                            //Find inserted CCM product groups bays
                                                            Dictionary<Byte, LocationSpaceBayDto> ccmBayDtoList = null;
                                                            if (ccmLocationSpaceBayDtoList.TryGetValue(ccmProductGroupBayCombo.Item1, out ccmBayDtoList))
                                                            {
                                                                //Find elements parent bay
                                                                LocationSpaceBayDto ccmParentBayDto = null;
                                                                if (ccmBayDtoList.TryGetValue(ccmProductGroupBayCombo.Item2, out ccmParentBayDto))
                                                                {
                                                                    List<LocationSpaceElementDto> ccmElementDtoList = ccmLocationSpaceElementDtoList[ccmProductGroupBayCombo];
                                                                    foreach (LocationSpaceElementDto ccmElementDto in ccmElementDtoList)
                                                                    {
                                                                        //Set now updated CCM location space bay reference
                                                                        ccmElementDto.LocationSpaceBayId = ccmParentBayDto.Id;
                                                                    }
                                                                    ccmLocationSpaceElementDtoListToInsert.AddRange(ccmElementDtoList);
                                                                }
                                                            }
                                                        }

                                                        //Insert location space element dtos
                                                        dalLocationSpaceElement.Upsert(ccmLocationSpaceElementDtoListToInsert);

                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationSpace], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data tranccmction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Location Space Deletes

        private void SynchronizeLocationSpaceDeleted(ProcessContext context)
        {
            // Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationSpaceInfoDal dalLocationSpaceInfo = context.DalContext.GetDal<ILocationSpaceInfoDal>())
                {
                    using (ILocationSpaceDal dalLocationSpace = context.DalContext.GetDal<ILocationSpaceDal>())
                    {
                        #region old delete
                        //List<Services.LocationSpace.LocationSpaceInfo> gfsLocationSpacesDeleted = null;

                        // Ensure that it has sync'd before
                        //if (SyncTarget.LastSync != null)
                        //{
                        // Fetch the deleted location space
                        //    context.DoWorkWithRetries(delegate()
                        //    {
                        //        gfsLocationSpacesDeleted = context.Services.LocationSpaceServiceClient.GetLocationSpaceInfoByEntityNameDateDeleted(
                        //            new Services.LocationSpace.GetLocationSpaceInfoByEntityNameDateDeletedRequest(this.EntityName, SyncTarget.LastSync)).LocationSpaceInfos;
                        //    });

                        //    IEnumerable<LocationSpaceInfoDto> ccmLocationSpaces;
                        //    using (ILocationSpaceInfoDal dalLocationSpaceInfo = context.DalContext.GetDal<ILocationSpaceInfoDal>())
                        //    {
                        //        ccmLocationSpaces = dalLocationSpaceInfo.FetchByEntityId(this.EntityId);
                        //    }

                        //    // loop through the location space to delete
                        //    foreach (Services.LocationSpace.LocationSpaceInfo locationSpace in gfsLocationSpacesDeleted)
                        //    {
                        //        // check if Location Space exists in CCM
                        //        LocationSpaceInfoDto ccmLocationSpace = ccmLocationSpaces.FirstOrDefault(i => i.UniqueContentReference == locationSpace.UniqueContentReference);

                        //        if (ccmLocationSpace != null)
                        //        {
                        //            dalLocationSpace.DeleteById(ccmLocationSpace.Id);
                        //        }
                        //    }
                        //}
                        #endregion

                        List<Services.LocationSpace.LocationSpaceInfo> gfsAssortmentsDeleted = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsAssortmentsDeleted = context.Services.LocationSpaceServiceClient.GetLocationSpaceInfoByEntityName(
                                new Services.LocationSpace.GetLocationSpaceInfoByEntityNameRequest(this.EntityName)).LocationSpaceInfos;
                        });

                        //fetch ccm universes
                        IEnumerable<LocationSpaceInfoDto> cdtList = dalLocationSpaceInfo.FetchByEntityId(this.EntityId);

                        //loop through ccm location space
                        foreach (LocationSpaceInfoDto ccmContent in cdtList)
                        {
                            //came from a sync
                            if (ccmContent.ParentUniqueContentReference != null)
                            {
                                //try to find the gfs product universe
                                Services.LocationSpace.LocationSpaceInfo gfsContent = gfsAssortmentsDeleted.FirstOrDefault(u => u.UniqueContentReference == ccmContent.UniqueContentReference);

                                //doesn't exist in gfs so delete it
                                if (gfsContent == null)
                                {
                                    dalLocationSpace.DeleteById(ccmContent.Id);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationSpace], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Synchronize Assortments

        #region Synchronize Assortments

        /// <summary>
        /// Syncrhonises the assortments within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        private void SynchronizeAssortments(ProcessContext context)
        {
            DateTime processStartDate = DateTime.UtcNow;

            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IAssortmentDal dalAssortment = context.DalContext.GetDal<IAssortmentDal>())
                {
                    using (IAssortmentProductDal dalProduct = context.DalContext.GetDal<IAssortmentProductDal>())
                    {
                        using (IAssortmentLocationDal dalLocation = context.DalContext.GetDal<IAssortmentLocationDal>())
                        {
                            using (IAssortmentLocalProductDal dalLocalProduct = context.DalContext.GetDal<IAssortmentLocalProductDal>())
                            {
                                using (IAssortmentInventoryRuleDal dalInventoryRule = context.DalContext.GetDal<IAssortmentInventoryRuleDal>())
                                {
                                    using (IAssortmentLocationBuddyDal dalLocationBuddy = context.DalContext.GetDal<IAssortmentLocationBuddyDal>())
                                    {
                                        using (IAssortmentProductBuddyDal dalProductBuddy = context.DalContext.GetDal<IAssortmentProductBuddyDal>())
                                        {
                                            using (IAssortmentRegionDal dalRegion = context.DalContext.GetDal<IAssortmentRegionDal>())
                                            {
                                                using (IAssortmentRegionLocationDal dalRegionLocation = context.DalContext.GetDal<IAssortmentRegionLocationDal>())
                                                {
                                                    using (IAssortmentRegionProductDal dalRegionProduct = context.DalContext.GetDal<IAssortmentRegionProductDal>())
                                                    {
                                                        // Ensure that the merch hierarchy process is active for the sync target
                                                        ObservableCollection<Services.Assortment.AssortmentInfo> gfsAssortmentInfo = null;

                                                        //Get the Assortment info 
                                                        context.DoWorkWithRetries(delegate()
                                                        {
                                                            GetAssortmentInfoByEntityIdRequest assortmentInfoByEntityIdRequest = new GetAssortmentInfoByEntityIdRequest(GFSEntityId);

                                                            gfsAssortmentInfo = new ObservableCollection<Services.Assortment.AssortmentInfo>
                                                            (context.Services.AssortmentServiceClient.GetAssortmentInfoByEntityId(assortmentInfoByEntityIdRequest).AssortmentInfos);
                                                        });

                                                        //Fetch the current CCM assortments
                                                        IEnumerable<AssortmentDto> ccmAssortments = dalAssortment.FetchAll();
                                                        Dictionary<String, Int16> locationCodeLookup = new Dictionary<String, Int16>();
                                                        Dictionary<String, Int32> productCodeLookup = new Dictionary<String, Int32>();
                                                        Dictionary<Guid?, Int32?> consumerDecisionTreeLookup = new Dictionary<Guid?, Int32?>();

                                                        //If there are any GFS assortments to process
                                                        if (gfsAssortmentInfo.Count > 0)
                                                        {
                                                            using (ILocationInfoDal dalStore = context.DalContext.GetDal<ILocationInfoDal>())
                                                            {
                                                                //Fetch all stores from CCM and add to dictionary
                                                                foreach (LocationInfoDto locationInfo in dalStore.FetchByEntityIdIncludingDeleted(this.EntityId))
                                                                {
                                                                    locationCodeLookup.Add(locationInfo.Code, locationInfo.Id);
                                                                }
                                                            }

                                                            using (IProductInfoDal dalProductInfo = context.DalContext.GetDal<IProductInfoDal>())
                                                            {
                                                                //Fetch all products from CCM and add to dictionary
                                                                foreach (ProductInfoDto productInfo in dalProductInfo.FetchByEntityIdIncludingDeleted(this.EntityId))
                                                                {
                                                                    productCodeLookup.Add(productInfo.Gtin, productInfo.Id);
                                                                }
                                                            }

                                                            using (IConsumerDecisionTreeInfoDal consumerDecisionTreeInfo = context.DalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                                                            {
                                                                //Fetch all products from CCM and add to dictionary
                                                                foreach (ConsumerDecisionTreeInfoDto cdtInfo in consumerDecisionTreeInfo.FetchByEntityId(this.EntityId))
                                                                {
                                                                    consumerDecisionTreeLookup.Add(cdtInfo.UniqueContentReference, cdtInfo.Id);
                                                                }
                                                            }


                                                            // Loop through the assortment info list 
                                                            foreach (Services.Assortment.AssortmentInfo assortment in gfsAssortmentInfo)
                                                            {
                                                                // check if it is the latest version
                                                                if (assortment.IsLatestVersion == true)
                                                                {
                                                                    //Fetch full assortment from GFS
                                                                    Galleria.Ccm.Services.Assortment.Assortment gfsAssortment = null;

                                                                    //Check if the version already exists
                                                                    //Formatted code is not updated by ccm, so perform the check on this field to ensure that the records match
                                                                    AssortmentDto ccmAssortment = null;

                                                                    //ccm assortments may be null if no records returned by dal fetch
                                                                    if (ccmAssortments != null)
                                                                    {
                                                                        Guid ucr;
                                                                        if (assortment.ParentUniqueContentReference != null)
                                                                        {
                                                                            ucr = assortment.ParentUniqueContentReference.Value;
                                                                        }
                                                                        else
                                                                        {
                                                                            ucr = assortment.UniqueContentReference;
                                                                        }

                                                                        ccmAssortment = ccmAssortments.FirstOrDefault(i => i.ParentUniqueContentReference == ucr);

                                                                        //find one with the same name
                                                                        AssortmentDto ccmDuplicateCdt = ccmAssortments.FirstOrDefault(i => (i.Name == assortment.Name && i.ParentUniqueContentReference == null));

                                                                        if (ccmDuplicateCdt != null)
                                                                        {
                                                                            //get the new one
                                                                            AssortmentDto duplicateContent = dalAssortment.FetchById(ccmDuplicateCdt.Id);
                                                                            if (duplicateContent != null)
                                                                            {
                                                                                duplicateContent.Name += " (Local Copy)";
                                                                                dalAssortment.Update(duplicateContent);
                                                                            }
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        ccmAssortment = null;
                                                                    }

                                                                    if (ccmAssortment == null ||
                                                                        ccmAssortment.UniqueContentReference != assortment.UniqueContentReference
                                                                          || SyncTarget.LastSync == null ||
                                                                        (assortment.DateLastModified != null && SyncTarget.LastSync != null && assortment.DateLastModified > SyncTarget.LastSync.Value))
                                                                    {
                                                                        context.DoWorkWithRetries(delegate()
                                                                        {
                                                                            GetAssortmentByUniqueContentReferenceRequest assortmentByUniqueContentRequest = new GetAssortmentByUniqueContentReferenceRequest(assortment.UniqueContentReference);
                                                                            gfsAssortment = context.Services.AssortmentServiceClient.GetAssortmentByUniqueContentReference(assortmentByUniqueContentRequest).Assortment;
                                                                        });
                                                                        if (context.ProductGroupMap.ContainsKey(gfsAssortment.ProductGroupCode))
                                                                        {
                                                                            //Add new assortment
                                                                            if (ccmAssortment == null)
                                                                            {
                                                                                #region Insert Assortment

                                                                                ccmAssortment = new AssortmentDto();

                                                                                ccmAssortment.Name = gfsAssortment.Name;
                                                                                ccmAssortment.UniqueContentReference = gfsAssortment.UniqueContentReference;
                                                                                ccmAssortment.ProductGroupId = context.ProductGroupMap[gfsAssortment.ProductGroupCode]; //works?
                                                                                ccmAssortment.EntityId = this.EntityId;

                                                                                if (gfsAssortment.ConsumerDecisionTreeUniqueContentReference != null)
                                                                                {
                                                                                    Int32? consId;
                                                                                    consumerDecisionTreeLookup.TryGetValue(gfsAssortment.ConsumerDecisionTreeUniqueContentReference, out consId);
                                                                                    ccmAssortment.ConsumerDecisionTreeId = consId;
                                                                                }

                                                                                if (gfsAssortment.ParentUniqueContentReference != null)
                                                                                {
                                                                                    ccmAssortment.ParentUniqueContentReference = gfsAssortment.ParentUniqueContentReference;
                                                                                }
                                                                                else
                                                                                {
                                                                                    ccmAssortment.ParentUniqueContentReference = gfsAssortment.UniqueContentReference;
                                                                                }

                                                                                dalAssortment.Insert(ccmAssortment);

                                                                                #endregion

                                                                                #region Insert Assortment Products

                                                                                AssortmentProductDto ccmAssortmentProduct = new AssortmentProductDto();
                                                                                foreach (Services.Assortment.AssortmentProduct gfsAssortmentProduct in gfsAssortment.Products)
                                                                                {
                                                                                    if (productCodeLookup.ContainsKey(gfsAssortmentProduct.GTIN))
                                                                                    {
                                                                                        ccmAssortmentProduct = new AssortmentProductDto();
                                                                                        ccmAssortmentProduct.ProductId = productCodeLookup[gfsAssortmentProduct.GTIN];
                                                                                        ccmAssortmentProduct.AssortmentId = ccmAssortment.Id;

                                                                                        ApplyGfsAssortmentProduct(ccmAssortmentProduct, gfsAssortmentProduct);
                                                                                        dalProduct.Insert(ccmAssortmentProduct);
                                                                                    }
                                                                                }

                                                                                #endregion

                                                                                #region Insert Assortment Locations

                                                                                AssortmentLocationDto ccmAssortmentLocation = new AssortmentLocationDto();
                                                                                foreach (Services.Assortment.AssortmentLocation assortmentLocation in gfsAssortment.Locations)
                                                                                {
                                                                                    if (locationCodeLookup.ContainsKey(assortmentLocation.LocationCode))
                                                                                    {
                                                                                        ccmAssortmentLocation = new AssortmentLocationDto();
                                                                                        ccmAssortmentLocation.AssortmentId = ccmAssortment.Id;
                                                                                        ccmAssortmentLocation.LocationId = (short)locationCodeLookup[assortmentLocation.LocationCode];
                                                                                        ccmAssortmentLocation.Code = assortmentLocation.LocationCode;

                                                                                        dalLocation.Insert(ccmAssortmentLocation);
                                                                                    }
                                                                                }

                                                                                #endregion

                                                                                #region Assortment Local Product

                                                                                AssortmentLocalProductDto ccmAssortmentLocalProduct = new AssortmentLocalProductDto();
                                                                                foreach (Services.Assortment.AssortmentLocalProduct assortmentLocalProduct in gfsAssortment.LocalProducts)
                                                                                {
                                                                                    if (locationCodeLookup.ContainsKey(assortmentLocalProduct.LocationCode) &&
                                                                                        productCodeLookup.ContainsKey(assortmentLocalProduct.ProductCode))
                                                                                    {
                                                                                        ccmAssortmentLocalProduct = new AssortmentLocalProductDto();
                                                                                        ccmAssortmentLocalProduct.AssortmentId = ccmAssortment.Id;
                                                                                        ccmAssortmentLocalProduct.LocationId = (short)locationCodeLookup[assortmentLocalProduct.LocationCode];
                                                                                        ccmAssortmentLocalProduct.ProductId = productCodeLookup[assortmentLocalProduct.ProductCode];
                                                                                        ccmAssortmentLocalProduct.LocationCode = assortmentLocalProduct.LocationCode;

                                                                                        dalLocalProduct.Insert(ccmAssortmentLocalProduct);
                                                                                    }
                                                                                }

                                                                                #endregion

                                                                                #region Assortment Region \ Region Products \ Region Locations

                                                                                AssortmentRegionDto ccmAssortmentRegion = new AssortmentRegionDto();
                                                                                AssortmentRegionProductDto ccmAssortmentRegionProduct = new AssortmentRegionProductDto();
                                                                                AssortmentRegionLocationDto ccmAssortmentRegionLocation = new AssortmentRegionLocationDto();
                                                                                foreach (Services.Assortment.AssortmentRegion assortmentRegion in gfsAssortment.Regions)
                                                                                {
                                                                                    ccmAssortmentRegion = new AssortmentRegionDto();
                                                                                    ccmAssortmentRegion.AssortmentId = ccmAssortment.Id;
                                                                                    ccmAssortmentRegion.Name = assortmentRegion.Name;

                                                                                    dalRegion.Insert(ccmAssortmentRegion);

                                                                                    foreach (Services.Assortment.AssortmentRegionProduct assortmentRegionProduct in assortmentRegion.Products)
                                                                                    {
                                                                                        if (assortmentRegionProduct.PrimaryProductCode != null &&
                                                                                            assortmentRegionProduct.RegionalProductCode != null)
                                                                                        {
                                                                                            if (productCodeLookup.ContainsKey(assortmentRegionProduct.PrimaryProductCode) &&
                                                                                                productCodeLookup.ContainsKey(assortmentRegionProduct.RegionalProductCode))
                                                                                            {
                                                                                                ccmAssortmentRegionProduct = new AssortmentRegionProductDto();
                                                                                                ccmAssortmentRegionProduct.AssortmentRegionId = ccmAssortmentRegion.Id;
                                                                                                ccmAssortmentRegionProduct.PrimaryProductId = productCodeLookup[assortmentRegionProduct.PrimaryProductCode];
                                                                                                ccmAssortmentRegionProduct.RegionalProductId = productCodeLookup[assortmentRegionProduct.RegionalProductCode];

                                                                                                dalRegionProduct.Insert(ccmAssortmentRegionProduct);
                                                                                            }
                                                                                        }
                                                                                    }

                                                                                    foreach (
                                                                                        Services.Assortment.AssortmentRegionLocation assortmentRegionLocation in assortmentRegion.Locations)
                                                                                    {
                                                                                        if (assortmentRegionLocation.LocationCode != null)
                                                                                        {
                                                                                            if (locationCodeLookup.ContainsKey(assortmentRegionLocation.LocationCode))
                                                                                            {
                                                                                                ccmAssortmentRegionLocation = new AssortmentRegionLocationDto();
                                                                                                ccmAssortmentRegionLocation.AssortmentRegionId = ccmAssortmentRegion.Id;
                                                                                                ccmAssortmentRegionLocation.LocationId = (short)locationCodeLookup[assortmentRegionLocation.LocationCode];

                                                                                                dalRegionLocation.Insert(ccmAssortmentRegionLocation);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                #endregion

                                                                                #region Assortment Inventory Rules

                                                                                AssortmentInventoryRuleDto ccmAssortmentInventoryRule = new AssortmentInventoryRuleDto();
                                                                                foreach (Services.Assortment.AssortmentInventoryRule assortmentInventoryRule in gfsAssortment.InventoryRules)
                                                                                {
                                                                                    if (productCodeLookup.ContainsKey(assortmentInventoryRule.ProductGtin))
                                                                                    {
                                                                                        ccmAssortmentInventoryRule = new AssortmentInventoryRuleDto();
                                                                                        ccmAssortmentInventoryRule.AssortmentId = ccmAssortment.Id;
                                                                                        ccmAssortmentInventoryRule.ProductId = productCodeLookup[assortmentInventoryRule.ProductGtin];
                                                                                        ccmAssortmentInventoryRule.ProductGtin = assortmentInventoryRule.ProductGtin;
                                                                                        
                                                                                        //Update values
                                                                                        ApplyGFSInventoryRuleValuesToCCM(ccmAssortmentInventoryRule, assortmentInventoryRule);

                                                                                        dalInventoryRule.Insert(ccmAssortmentInventoryRule);
                                                                                    }
                                                                                }

                                                                                #endregion

                                                                                #region Assortment Product Buddies

                                                                                AssortmentProductBuddyDto ccmAssortmentProductBuddy = new AssortmentProductBuddyDto();
                                                                                foreach (Services.Assortment.AssortmentProductBuddy assortmentProductBuddy in gfsAssortment.ProductBuddies)
                                                                                {
                                                                                    //Ensure we support source type
                                                                                    PlanogramAssortmentProductBuddySourceType gfsProductBuddySourceType;
                                                                                    PlanogramAssortmentProductBuddyTreatmentType gfsProductBuddyTreatmentType;
                                                                                    PlanogramAssortmentProductBuddyProductAttributeType gfsProductBuddyAttributeType;
                                                                                    if (Enum.TryParse<PlanogramAssortmentProductBuddySourceType>(assortmentProductBuddy.SourceType, out gfsProductBuddySourceType)
                                                                                        && Enum.TryParse<PlanogramAssortmentProductBuddyTreatmentType>(assortmentProductBuddy.TreatmentType, out gfsProductBuddyTreatmentType)
                                                                                        && Enum.TryParse<PlanogramAssortmentProductBuddyProductAttributeType>(assortmentProductBuddy.ProductAttributeType, out gfsProductBuddyAttributeType))
                                                                                    {
                                                                                        if (productCodeLookup.ContainsKey(assortmentProductBuddy.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentProductBuddy = new AssortmentProductBuddyDto();
                                                                                            ccmAssortmentProductBuddy.AssortmentId = ccmAssortment.Id;
                                                                                            ccmAssortmentProductBuddy.ProductId = productCodeLookup[assortmentProductBuddy.ProductGtin];
                                                                                            ccmAssortmentProductBuddy.ProductGtin = assortmentProductBuddy.ProductGtin;
                                                                                            
                                                                                            //Update properties
                                                                                            ApplyGFSProductBuddyValuesToCCM(ccmAssortmentProductBuddy,
                                                                                                assortmentProductBuddy,
                                                                                                gfsProductBuddyAttributeType,
                                                                                                gfsProductBuddySourceType,
                                                                                                gfsProductBuddyTreatmentType,
                                                                                                productCodeLookup);

                                                                                            dalProductBuddy.Insert(ccmAssortmentProductBuddy);
                                                                                        }
                                                                                    }
                                                                                }

                                                                                #endregion

                                                                                #region Assortment Location Buddies
                                                                                
                                                                                AssortmentLocationBuddyDto ccmAssortmentLocationBuddy = new AssortmentLocationBuddyDto();
                                                                                foreach (Services.Assortment.AssortmentLocationBuddy assortmentLocationBuddy in gfsAssortment.LocationBuddies)
                                                                                {
                                                                                    //Ensure we support source type
                                                                                    PlanogramAssortmentLocationBuddyTreatmentType gfsLocationBuddyTreatmentType;
                                                                                    if (Enum.TryParse<PlanogramAssortmentLocationBuddyTreatmentType>(assortmentLocationBuddy.TreatmentType, out gfsLocationBuddyTreatmentType))
                                                                                    {
                                                                                        if (locationCodeLookup.ContainsKey(assortmentLocationBuddy.LocationCode))
                                                                                        {
                                                                                            ccmAssortmentLocationBuddy = new AssortmentLocationBuddyDto();
                                                                                            ccmAssortmentLocationBuddy.AssortmentId = ccmAssortment.Id;
                                                                                            ccmAssortmentLocationBuddy.LocationId = locationCodeLookup[assortmentLocationBuddy.LocationCode];
                                                                                            ccmAssortmentLocationBuddy.LocationCode = assortmentLocationBuddy.LocationCode;
                                                                                            
                                                                                            //update properties
                                                                                            ApplyGFSLocationBuddyValuesToCCM(ccmAssortmentLocationBuddy,
                                                                                                assortmentLocationBuddy,
                                                                                                gfsLocationBuddyTreatmentType,
                                                                                                locationCodeLookup);

                                                                                            dalLocationBuddy.Insert(ccmAssortmentLocationBuddy);
                                                                                        }
                                                                                    }
                                                                                }

                                                                                #endregion
                                                                            }
                                                                            else
                                                                            {
                                                                                #region Update Assortment

                                                                                ccmAssortment.Name = gfsAssortment.Name;
                                                                                ccmAssortment.UniqueContentReference = gfsAssortment.UniqueContentReference;
                                                                                ccmAssortment.ProductGroupId = context.ProductGroupMap[gfsAssortment.ProductGroupCode]; //works?
                                                                                ccmAssortment.EntityId = this.EntityId;

                                                                                if (gfsAssortment.ConsumerDecisionTreeUniqueContentReference != null)
                                                                                {
                                                                                    Int32? consId;
                                                                                    consumerDecisionTreeLookup.TryGetValue(gfsAssortment.ConsumerDecisionTreeUniqueContentReference, out consId);
                                                                                    ccmAssortment.ConsumerDecisionTreeId = consId;
                                                                                }

                                                                                if (gfsAssortment.ParentUniqueContentReference != null)
                                                                                {
                                                                                    ccmAssortment.ParentUniqueContentReference = gfsAssortment.ParentUniqueContentReference;
                                                                                }
                                                                                else
                                                                                {
                                                                                    ccmAssortment.ParentUniqueContentReference = gfsAssortment.UniqueContentReference;
                                                                                }

                                                                                dalAssortment.Update(ccmAssortment);

                                                                                #endregion

                                                                                #region Update Assortment Products

                                                                                List<AssortmentProductDto> ccmAssortmentProducts = dalProduct.FetchByAssortmentId(ccmAssortment.Id).ToList();
                                                                                AssortmentProductDto ccmAssortmentProduct;

                                                                                foreach (Services.Assortment.AssortmentProduct gfsAssortmentProduct in gfsAssortment.Products)
                                                                                {
                                                                                    if (gfsAssortmentProduct.GTIN != null && productCodeLookup.ContainsKey(gfsAssortmentProduct.GTIN))
                                                                                    {
                                                                                        ccmAssortmentProduct = ccmAssortmentProducts.Where(p => p.ProductId == productCodeLookup[gfsAssortmentProduct.GTIN]).FirstOrDefault();

                                                                                        if (ccmAssortmentProduct != null)
                                                                                        {
                                                                                            ApplyGfsAssortmentProduct(ccmAssortmentProduct, gfsAssortmentProduct);
                                                                                            dalProduct.Update(ccmAssortmentProduct);

                                                                                            ccmAssortmentProducts.Remove(ccmAssortmentProduct);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            if (productCodeLookup.ContainsKey(gfsAssortmentProduct.GTIN))
                                                                                            {
                                                                                                ccmAssortmentProduct = new AssortmentProductDto();
                                                                                                ccmAssortmentProduct.ProductId = productCodeLookup[gfsAssortmentProduct.GTIN];
                                                                                                ccmAssortmentProduct.AssortmentId = ccmAssortment.Id;

                                                                                                ApplyGfsAssortmentProduct(ccmAssortmentProduct, gfsAssortmentProduct);
                                                                                                dalProduct.Insert(ccmAssortmentProduct);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                //If any ccmProducts remaining, they have been removed
                                                                                foreach (AssortmentProductDto assortmentProduct in ccmAssortmentProducts)
                                                                                {
                                                                                    dalProduct.DeleteById(assortmentProduct.Id);
                                                                                }
                                                                                #endregion

                                                                                #region Update Assortment Locations

                                                                                List<AssortmentLocationDto> ccmAssortmentLocations = dalLocation.FetchByAssortmentId(ccmAssortment.Id).ToList();
                                                                                AssortmentLocationDto ccmAssortmentLocation;

                                                                                foreach (Services.Assortment.AssortmentLocation gfsAssortmentLocation in gfsAssortment.Locations)
                                                                                {
                                                                                    if (locationCodeLookup.ContainsKey(gfsAssortmentLocation.LocationCode))
                                                                                    {
                                                                                        ccmAssortmentLocation = ccmAssortmentLocations.Where(p => p.LocationId == locationCodeLookup[gfsAssortmentLocation.LocationCode]).FirstOrDefault();

                                                                                        if (ccmAssortmentLocation != null)
                                                                                        {
                                                                                            // No properties to update
                                                                                            ccmAssortmentLocation.Code = gfsAssortmentLocation.LocationCode;

                                                                                            ccmAssortmentLocations.Remove(ccmAssortmentLocation);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            ccmAssortmentLocation = new AssortmentLocationDto();
                                                                                            ccmAssortmentLocation.AssortmentId = ccmAssortment.Id;
                                                                                            ccmAssortmentLocation.LocationId = (short)locationCodeLookup[gfsAssortmentLocation.LocationCode];

                                                                                            dalLocation.Insert(ccmAssortmentLocation);
                                                                                        }
                                                                                    }
                                                                                }

                                                                                //If any ccmLocations remaining, they have been removed
                                                                                foreach (AssortmentLocationDto assortmentLocation in ccmAssortmentLocations)
                                                                                {
                                                                                    dalLocation.DeleteById(assortmentLocation.Id);
                                                                                }
                                                                                #endregion

                                                                                #region Update Assortment Local Products

                                                                                List<AssortmentLocalProductDto> ccmAssortmentLocalProducts = dalLocalProduct.FetchByAssortmentId(ccmAssortment.Id).ToList();
                                                                                AssortmentLocalProductDto ccmAssortmentLocalProduct;

                                                                                foreach (Services.Assortment.AssortmentLocalProduct gfsAssortmentLocalProduct in gfsAssortment.LocalProducts)
                                                                                {
                                                                                    if (locationCodeLookup.ContainsKey(gfsAssortmentLocalProduct.LocationCode) && productCodeLookup.ContainsKey(gfsAssortmentLocalProduct.ProductCode))
                                                                                    {
                                                                                        ccmAssortmentLocalProduct = ccmAssortmentLocalProducts.Where(p => p.LocationId == locationCodeLookup[gfsAssortmentLocalProduct.LocationCode]
                                                                                                                                        && p.ProductId == productCodeLookup[gfsAssortmentLocalProduct.ProductCode]).FirstOrDefault();

                                                                                        if (ccmAssortmentLocalProduct != null)
                                                                                        {
                                                                                            // No properties to update
                                                                                            ccmAssortmentLocalProducts.Remove(ccmAssortmentLocalProduct);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            ccmAssortmentLocalProduct = new AssortmentLocalProductDto();
                                                                                            ccmAssortmentLocalProduct.AssortmentId = ccmAssortment.Id;
                                                                                            ccmAssortmentLocalProduct.LocationId = (short)locationCodeLookup[gfsAssortmentLocalProduct.LocationCode];
                                                                                            ccmAssortmentLocalProduct.ProductId = productCodeLookup[gfsAssortmentLocalProduct.ProductCode];
                                                                                            ccmAssortmentLocalProduct.LocationCode = gfsAssortmentLocalProduct.LocationCode;

                                                                                            dalLocalProduct.Insert(ccmAssortmentLocalProduct);
                                                                                        }
                                                                                    }
                                                                                }

                                                                                //If any ccmLocations remaining, they have been removed
                                                                                foreach (AssortmentLocalProductDto assortmentLocalProduct in ccmAssortmentLocalProducts)
                                                                                {
                                                                                    dalLocalProduct.DeleteById(assortmentLocalProduct.Id);
                                                                                }
                                                                                #endregion

                                                                                #region Update Assortment Regions

                                                                                List<AssortmentRegionDto> ccmAssortmentRegions = dalRegion.FetchByAssortmentId(ccmAssortment.Id).ToList();
                                                                                AssortmentRegionDto ccmAssortmentRegion;

                                                                                foreach (Services.Assortment.AssortmentRegion gfsAssortmentRegion in gfsAssortment.Regions)
                                                                                {
                                                                                    ccmAssortmentRegion = ccmAssortmentRegions.Where(p => p.Name == gfsAssortmentRegion.Name).FirstOrDefault();

                                                                                    if (ccmAssortmentRegion != null)
                                                                                    {
                                                                                        #region Validate Region Locations

                                                                                        List<AssortmentRegionLocationDto> ccmAssortmentRegionLocations = dalRegionLocation.FetchByRegionId(ccmAssortmentRegion.Id).ToList();
                                                                                        AssortmentRegionLocationDto ccmAssortmentRegionLocation;
                                                                                        foreach (Services.Assortment.AssortmentRegionLocation gfsAssortmentRegionLocation in gfsAssortmentRegion.Locations)
                                                                                        {
                                                                                            if (gfsAssortmentRegionLocation.LocationCode != null)
                                                                                            {
                                                                                                if (locationCodeLookup.ContainsKey(gfsAssortmentRegionLocation.LocationCode))
                                                                                                {
                                                                                                    ccmAssortmentRegionLocation = ccmAssortmentRegionLocations.Where(p => p.LocationId == locationCodeLookup[gfsAssortmentRegionLocation.LocationCode]).FirstOrDefault();

                                                                                                    if (ccmAssortmentRegionLocation != null)
                                                                                                    {
                                                                                                        // No properties to update
                                                                                                        ccmAssortmentRegionLocations.Remove(ccmAssortmentRegionLocation);
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        ccmAssortmentRegionLocation = new AssortmentRegionLocationDto();
                                                                                                        ccmAssortmentRegionLocation.AssortmentRegionId = ccmAssortmentRegion.Id;
                                                                                                        ccmAssortmentRegionLocation.LocationId = (short)locationCodeLookup[gfsAssortmentRegionLocation.LocationCode];

                                                                                                        dalRegionLocation.Insert(ccmAssortmentRegionLocation);
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        //If any ccmRegionLocations remaining, they have been removed
                                                                                        foreach (AssortmentRegionLocationDto assortmentRegionLocation in ccmAssortmentRegionLocations)
                                                                                        {
                                                                                            dalRegionLocation.DeleteById(assortmentRegionLocation.Id);
                                                                                        }

                                                                                        #endregion

                                                                                        #region Validate Region Products

                                                                                        List<AssortmentRegionProductDto> ccmAssortmentRegionProducts = dalRegionProduct.FetchByRegionId(ccmAssortmentRegion.Id).ToList();
                                                                                        AssortmentRegionProductDto ccmAssortmentRegionProduct;
                                                                                        foreach (Services.Assortment.AssortmentRegionProduct gfsAssortmentRegionProduct in gfsAssortmentRegion.Products)
                                                                                        {
                                                                                            if (productCodeLookup.ContainsKey(gfsAssortmentRegionProduct.PrimaryProductCode) &&
                                                                                                    productCodeLookup.ContainsKey(gfsAssortmentRegionProduct.RegionalProductCode))
                                                                                            {
                                                                                                ccmAssortmentRegionProduct = ccmAssortmentRegionProducts.Where(p => p.PrimaryProductId == productCodeLookup[gfsAssortmentRegionProduct.PrimaryProductCode]
                                                                                                                            && p.RegionalProductId == productCodeLookup[gfsAssortmentRegionProduct.RegionalProductCode]).FirstOrDefault();

                                                                                                if (ccmAssortmentRegionProduct != null)
                                                                                                {
                                                                                                    // No properties to update
                                                                                                    ccmAssortmentRegionProducts.Remove(ccmAssortmentRegionProduct);
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    ccmAssortmentRegionProduct = new AssortmentRegionProductDto();
                                                                                                    ccmAssortmentRegionProduct.AssortmentRegionId = ccmAssortmentRegion.Id;
                                                                                                    ccmAssortmentRegionProduct.PrimaryProductId = productCodeLookup[gfsAssortmentRegionProduct.PrimaryProductCode];
                                                                                                    ccmAssortmentRegionProduct.RegionalProductId = productCodeLookup[gfsAssortmentRegionProduct.RegionalProductCode];

                                                                                                    dalRegionProduct.Insert(ccmAssortmentRegionProduct);
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        //If any ccmRegionProducts remaining, they have been removed
                                                                                        foreach (AssortmentRegionProductDto assortmentRegionProduct in ccmAssortmentRegionProducts)
                                                                                        {
                                                                                            dalRegionProduct.DeleteById(assortmentRegionProduct.Id);
                                                                                        }

                                                                                        #endregion

                                                                                        // No properties to update
                                                                                        ccmAssortmentRegions.Remove(ccmAssortmentRegion);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ccmAssortmentRegion = new AssortmentRegionDto();
                                                                                        ccmAssortmentRegion.AssortmentId = ccmAssortment.Id;
                                                                                        ccmAssortmentRegion.Name = gfsAssortmentRegion.Name;

                                                                                        dalRegion.Insert(ccmAssortmentRegion);

                                                                                        AssortmentRegionProductDto ccmAssortmentRegionProduct = new AssortmentRegionProductDto();
                                                                                        AssortmentRegionLocationDto ccmAssortmentRegionLocation = new AssortmentRegionLocationDto();

                                                                                        foreach (Services.Assortment.AssortmentRegionProduct assortmentRegionProduct in gfsAssortmentRegion.Products)
                                                                                        {
                                                                                            if (assortmentRegionProduct.PrimaryProductCode != null &&
                                                                                                assortmentRegionProduct.RegionalProductCode != null)
                                                                                            {
                                                                                                if (productCodeLookup.ContainsKey(assortmentRegionProduct.PrimaryProductCode) &&
                                                                                                    productCodeLookup.ContainsKey(assortmentRegionProduct.RegionalProductCode))
                                                                                                {
                                                                                                    ccmAssortmentRegionProduct = new AssortmentRegionProductDto();
                                                                                                    ccmAssortmentRegionProduct.AssortmentRegionId = ccmAssortmentRegion.Id;
                                                                                                    ccmAssortmentRegionProduct.PrimaryProductId = productCodeLookup[assortmentRegionProduct.PrimaryProductCode];
                                                                                                    ccmAssortmentRegionProduct.RegionalProductId = productCodeLookup[assortmentRegionProduct.RegionalProductCode];

                                                                                                    dalRegionProduct.Insert(ccmAssortmentRegionProduct);
                                                                                                }
                                                                                            }
                                                                                        }

                                                                                        foreach (Services.Assortment.AssortmentRegionLocation assortmentRegionLocation in gfsAssortmentRegion.Locations)
                                                                                        {
                                                                                            if (assortmentRegionLocation.LocationCode != null)
                                                                                            {
                                                                                                if (locationCodeLookup.ContainsKey(assortmentRegionLocation.LocationCode))
                                                                                                {
                                                                                                    ccmAssortmentRegionLocation = new AssortmentRegionLocationDto();
                                                                                                    ccmAssortmentRegionLocation.AssortmentRegionId = ccmAssortmentRegion.Id;
                                                                                                    ccmAssortmentRegionLocation.LocationId = (short)locationCodeLookup[assortmentRegionLocation.LocationCode];

                                                                                                    dalRegionLocation.Insert(ccmAssortmentRegionLocation);
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                //If any ccmRegions remaining, they have been removed
                                                                                foreach (AssortmentRegionDto assortmentRegion in ccmAssortmentRegions)
                                                                                {
                                                                                    dalRegion.DeleteById(assortmentRegion.Id);
                                                                                }
                                                                                #endregion

                                                                                #region Update Assortment Inventory Rules
                                                                                
                                                                                List<AssortmentInventoryRuleDto> ccmAssortmentInventoryRules = dalInventoryRule.FetchByAssortmentId(ccmAssortment.Id).ToList();
                                                                                AssortmentInventoryRuleDto ccmAssortmentInventoryRule;

                                                                                foreach (Services.Assortment.AssortmentInventoryRule gfsAssortmentInventoryRule in gfsAssortment.InventoryRules)
                                                                                {
                                                                                    if (productCodeLookup.ContainsKey(gfsAssortmentInventoryRule.ProductGtin))
                                                                                    {
                                                                                        ccmAssortmentInventoryRule = ccmAssortmentInventoryRules.FirstOrDefault(p => p.ProductId == productCodeLookup[gfsAssortmentInventoryRule.ProductGtin]);

                                                                                        if (ccmAssortmentInventoryRule != null)
                                                                                        {
                                                                                            //Update values
                                                                                            ApplyGFSInventoryRuleValuesToCCM(ccmAssortmentInventoryRule, gfsAssortmentInventoryRule); 
                                                                                            
                                                                                            dalInventoryRule.Update(ccmAssortmentInventoryRule);

                                                                                            ccmAssortmentInventoryRules.Remove(ccmAssortmentInventoryRule);
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                            ccmAssortmentInventoryRule = new AssortmentInventoryRuleDto();
                                                                                            ccmAssortmentInventoryRule.AssortmentId = ccmAssortment.Id;
                                                                                            ccmAssortmentInventoryRule.ProductId = productCodeLookup[gfsAssortmentInventoryRule.ProductGtin];
                                                                                            ccmAssortmentInventoryRule.ProductGtin = gfsAssortmentInventoryRule.ProductGtin;

                                                                                            //Update values
                                                                                            ApplyGFSInventoryRuleValuesToCCM(ccmAssortmentInventoryRule, gfsAssortmentInventoryRule);

                                                                                            dalInventoryRule.Insert(ccmAssortmentInventoryRule);
                                                                                        }
                                                                                    }
                                                                                }

                                                                                //If any ccmLocations remaining, they have been removed
                                                                                foreach (AssortmentInventoryRuleDto assortmentInventoryRule in ccmAssortmentInventoryRules)
                                                                                {
                                                                                    dalInventoryRule.DeleteById(assortmentInventoryRule.Id);
                                                                                }

                                                                                #endregion

                                                                                #region Update Assortment Product Buddies

                                                                                List<AssortmentProductBuddyDto> ccmAssortmentProductBuddys = dalProductBuddy.FetchByAssortmentId(ccmAssortment.Id).ToList();
                                                                                AssortmentProductBuddyDto ccmAssortmentProductBuddy;

                                                                                foreach (Services.Assortment.AssortmentProductBuddy gfsAssortmentProductBuddy in gfsAssortment.ProductBuddies)
                                                                                {
                                                                                    if (productCodeLookup.ContainsKey(gfsAssortmentProductBuddy.ProductGtin))
                                                                                    {
                                                                                        ccmAssortmentProductBuddy = ccmAssortmentProductBuddys.FirstOrDefault(p => p.ProductId == productCodeLookup[gfsAssortmentProductBuddy.ProductGtin]);

                                                                                        //Ensure we support source type
                                                                                        PlanogramAssortmentProductBuddySourceType gfsProductBuddySourceType;
                                                                                        PlanogramAssortmentProductBuddyTreatmentType gfsProductBuddyTreatmentType;
                                                                                        PlanogramAssortmentProductBuddyProductAttributeType gfsProductBuddyAttributeType;
                                                                                        if (Enum.TryParse<PlanogramAssortmentProductBuddySourceType>(gfsAssortmentProductBuddy.SourceType, out gfsProductBuddySourceType)
                                                                                            && Enum.TryParse<PlanogramAssortmentProductBuddyTreatmentType>(gfsAssortmentProductBuddy.TreatmentType, out gfsProductBuddyTreatmentType)
                                                                                            && Enum.TryParse<PlanogramAssortmentProductBuddyProductAttributeType>(gfsAssortmentProductBuddy.ProductAttributeType, out gfsProductBuddyAttributeType))
                                                                                        {
                                                                                            if (ccmAssortmentProductBuddy != null)
                                                                                            {
                                                                                                //Clear any existing source products
                                                                                                ClearCCMProductBuddySources(ccmAssortmentProductBuddy);

                                                                                                //update properties
                                                                                                ApplyGFSProductBuddyValuesToCCM(ccmAssortmentProductBuddy,
                                                                                                    gfsAssortmentProductBuddy,
                                                                                                    gfsProductBuddyAttributeType,
                                                                                                    gfsProductBuddySourceType,
                                                                                                    gfsProductBuddyTreatmentType,
                                                                                                    productCodeLookup);
                                                                                                
                                                                                                dalProductBuddy.Update(ccmAssortmentProductBuddy);

                                                                                                ccmAssortmentProductBuddys.Remove(ccmAssortmentProductBuddy);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                ccmAssortmentProductBuddy = new AssortmentProductBuddyDto();
                                                                                                ccmAssortmentProductBuddy.AssortmentId = ccmAssortment.Id;
                                                                                                ccmAssortmentProductBuddy.ProductId = productCodeLookup[gfsAssortmentProductBuddy.ProductGtin];
                                                                                                ccmAssortmentProductBuddy.ProductGtin = gfsAssortmentProductBuddy.ProductGtin;

                                                                                                //Update properties
                                                                                                ApplyGFSProductBuddyValuesToCCM(ccmAssortmentProductBuddy,
                                                                                                    gfsAssortmentProductBuddy,
                                                                                                    gfsProductBuddyAttributeType,
                                                                                                    gfsProductBuddySourceType,
                                                                                                    gfsProductBuddyTreatmentType,
                                                                                                    productCodeLookup);

                                                                                                dalProductBuddy.Insert(ccmAssortmentProductBuddy);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                //If any ccmLocations remaining, they have been removed
                                                                                foreach (AssortmentProductBuddyDto assortmentProductBuddy in ccmAssortmentProductBuddys)
                                                                                {
                                                                                    dalProductBuddy.DeleteById(assortmentProductBuddy.Id);
                                                                                }

                                                                                #endregion

                                                                                #region Update Assortment Location Buddies
                                                                                
                                                                                List<AssortmentLocationBuddyDto> ccmAssortmentLocationBuddys = dalLocationBuddy.FetchByAssortmentId(ccmAssortment.Id).ToList();
                                                                                AssortmentLocationBuddyDto ccmAssortmentLocationBuddy;

                                                                                foreach (Services.Assortment.AssortmentLocationBuddy gfsAssortmentLocationBuddy in gfsAssortment.LocationBuddies)
                                                                                {
                                                                                    if (locationCodeLookup.ContainsKey(gfsAssortmentLocationBuddy.LocationCode))
                                                                                    {
                                                                                        ccmAssortmentLocationBuddy = ccmAssortmentLocationBuddys.FirstOrDefault(p => p.LocationId == locationCodeLookup[gfsAssortmentLocationBuddy.LocationCode]);

                                                                                        //Ensure we support source type
                                                                                        PlanogramAssortmentLocationBuddyTreatmentType gfsLocationBuddyTreatmentType;
                                                                                        if (Enum.TryParse<PlanogramAssortmentLocationBuddyTreatmentType>(gfsAssortmentLocationBuddy.TreatmentType, out gfsLocationBuddyTreatmentType))
                                                                                        {
                                                                                            if (ccmAssortmentLocationBuddy != null)
                                                                                            {
                                                                                                //Clear any existing source locations
                                                                                                ClearCCMLocationBuddySources(ccmAssortmentLocationBuddy);

                                                                                                //update properties
                                                                                                ApplyGFSLocationBuddyValuesToCCM(ccmAssortmentLocationBuddy,
                                                                                                    gfsAssortmentLocationBuddy,
                                                                                                    gfsLocationBuddyTreatmentType,
                                                                                                    locationCodeLookup);

                                                                                                dalLocationBuddy.Update(ccmAssortmentLocationBuddy);

                                                                                                ccmAssortmentLocationBuddys.Remove(ccmAssortmentLocationBuddy);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                ccmAssortmentLocationBuddy = new AssortmentLocationBuddyDto();
                                                                                                ccmAssortmentLocationBuddy.AssortmentId = ccmAssortment.Id;
                                                                                                ccmAssortmentLocationBuddy.LocationId = locationCodeLookup[gfsAssortmentLocationBuddy.LocationCode];
                                                                                                ccmAssortmentLocationBuddy.LocationCode = gfsAssortmentLocationBuddy.LocationCode;

                                                                                                //Update properties
                                                                                                ApplyGFSLocationBuddyValuesToCCM(ccmAssortmentLocationBuddy,
                                                                                                    gfsAssortmentLocationBuddy,
                                                                                                    gfsLocationBuddyTreatmentType,
                                                                                                    locationCodeLookup);

                                                                                                dalLocationBuddy.Insert(ccmAssortmentLocationBuddy);
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }

                                                                                //If any ccmLocations remaining, they have been removed
                                                                                foreach (AssortmentLocationBuddyDto assortmentLocationBuddy in ccmAssortmentLocationBuddys)
                                                                                {
                                                                                    dalLocationBuddy.DeleteById(assortmentLocationBuddy.Id);
                                                                                }

                                                                                #endregion
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Assortment], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Syncrhonize Assortment Methods

        private static void ApplyGfsAssortmentProduct(AssortmentProductDto ccmAssortmentProduct, Services.Assortment.AssortmentProduct gfsAssortmentProduct)
        {
            ccmAssortmentProduct.Name = gfsAssortmentProduct.Name;
            ccmAssortmentProduct.GTIN = gfsAssortmentProduct.GTIN;
            ccmAssortmentProduct.IsRanged = gfsAssortmentProduct.IsRanged;
            ccmAssortmentProduct.Rank = gfsAssortmentProduct.Rank;
            ccmAssortmentProduct.Segmentation = gfsAssortmentProduct.Segmentation;
            ccmAssortmentProduct.Facings = gfsAssortmentProduct.Facings;
            ccmAssortmentProduct.Units = gfsAssortmentProduct.Units;
            ccmAssortmentProduct.ProductTreatmentType =
                (Byte)(EnumHelper.Parse<PlanogramAssortmentProductTreatmentType>(gfsAssortmentProduct.ProductTreatmentType,
                                                PlanogramAssortmentProductTreatmentType.Normal));
            ccmAssortmentProduct.Comments = gfsAssortmentProduct.Comments;
            ccmAssortmentProduct.ProductLocalizationType =
                (Byte)(EnumHelper.Parse<PlanogramAssortmentProductLocalizationType>(gfsAssortmentProduct.ProductLocalizationType,
                                                PlanogramAssortmentProductLocalizationType.None));
            ccmAssortmentProduct.ExactListFacings = gfsAssortmentProduct.ExactListFacings;
            ccmAssortmentProduct.ExactListUnits = gfsAssortmentProduct.ExactListUnits;
            ccmAssortmentProduct.PreserveListFacings = gfsAssortmentProduct.PreserveListFacings;
            ccmAssortmentProduct.PreserveListUnits = gfsAssortmentProduct.PreserveListUnits;
            ccmAssortmentProduct.MaxListFacings = gfsAssortmentProduct.MaxListFacings;
            ccmAssortmentProduct.MinListFacings = gfsAssortmentProduct.MinListFacings;
            ccmAssortmentProduct.MinListUnits = gfsAssortmentProduct.MinListUnits;
            ccmAssortmentProduct.MaxListUnits = gfsAssortmentProduct.MaxListUnits;

            ccmAssortmentProduct.FamilyRuleName = gfsAssortmentProduct.FamilyRuleName;
            ccmAssortmentProduct.FamilyRuleType =
                (Byte)(EnumHelper.Parse<PlanogramAssortmentProductFamilyRuleType>(gfsAssortmentProduct.FamilyRuleType,
                                                PlanogramAssortmentProductFamilyRuleType.None));
            ccmAssortmentProduct.FamilyRuleValue = gfsAssortmentProduct.FamilyRuleValue;
            ccmAssortmentProduct.PreserveListUnits = gfsAssortmentProduct.PreserveListUnits;
            ccmAssortmentProduct.IsPrimaryRegionalProduct = gfsAssortmentProduct.IsPrimaryRegionalProduct;
        }

        private static void ApplyGFSProductBuddyValuesToCCM(AssortmentProductBuddyDto ccmAssortmentProductBuddy, 
            Services.Assortment.AssortmentProductBuddy gfsAssortmentProductBuddy,
            PlanogramAssortmentProductBuddyProductAttributeType gfsProductBuddyAttributeType,
            PlanogramAssortmentProductBuddySourceType gfsProductBuddySourceType,
            PlanogramAssortmentProductBuddyTreatmentType gfsProductBuddyTreatmentType,
            Dictionary<String, Int32> productCodeLookup)
        {
            ccmAssortmentProductBuddy.SourceType = (Byte)gfsProductBuddySourceType;
            ccmAssortmentProductBuddy.ProductAttributeType = (Byte)gfsProductBuddyAttributeType;
            ccmAssortmentProductBuddy.TreatmentType = (Byte)gfsProductBuddyTreatmentType;
            ccmAssortmentProductBuddy.TreatmentTypePercentage = gfsAssortmentProductBuddy.TreatmentTypePercentage;
            if (!String.IsNullOrEmpty(gfsAssortmentProductBuddy.S1ProductGtin) && productCodeLookup.ContainsKey(gfsAssortmentProductBuddy.S1ProductGtin))
            {
                ccmAssortmentProductBuddy.S1ProductId = productCodeLookup[gfsAssortmentProductBuddy.S1ProductGtin];
                ccmAssortmentProductBuddy.S1Percentage = gfsAssortmentProductBuddy.S1Percentage;
            }
            if (!String.IsNullOrEmpty(gfsAssortmentProductBuddy.S2ProductGtin) && productCodeLookup.ContainsKey(gfsAssortmentProductBuddy.S2ProductGtin))
            {
                ccmAssortmentProductBuddy.S2ProductId = productCodeLookup[gfsAssortmentProductBuddy.S2ProductGtin];
                ccmAssortmentProductBuddy.S2Percentage = gfsAssortmentProductBuddy.S2Percentage;
            }
            if (!String.IsNullOrEmpty(gfsAssortmentProductBuddy.S3ProductGtin) && productCodeLookup.ContainsKey(gfsAssortmentProductBuddy.S3ProductGtin))
            {
                ccmAssortmentProductBuddy.S3ProductId = productCodeLookup[gfsAssortmentProductBuddy.S3ProductGtin];
                ccmAssortmentProductBuddy.S3Percentage = gfsAssortmentProductBuddy.S3Percentage;
            }
            if (!String.IsNullOrEmpty(gfsAssortmentProductBuddy.S4ProductGtin) && productCodeLookup.ContainsKey(gfsAssortmentProductBuddy.S4ProductGtin))
            {
                ccmAssortmentProductBuddy.S4ProductId = productCodeLookup[gfsAssortmentProductBuddy.S4ProductGtin];
                ccmAssortmentProductBuddy.S4Percentage = gfsAssortmentProductBuddy.S4Percentage;
            }
            if (!String.IsNullOrEmpty(gfsAssortmentProductBuddy.S5ProductGtin) && productCodeLookup.ContainsKey(gfsAssortmentProductBuddy.S5ProductGtin))
            {
                ccmAssortmentProductBuddy.S5ProductId = productCodeLookup[gfsAssortmentProductBuddy.S5ProductGtin];
                ccmAssortmentProductBuddy.S5Percentage = gfsAssortmentProductBuddy.S5Percentage;
            }
        }

        private static void ClearCCMProductBuddySources(AssortmentProductBuddyDto productBuddyDto)
        {
            productBuddyDto.S1ProductId = null;
            productBuddyDto.S1ProductGtin = null;
            productBuddyDto.S1Percentage = null;
            productBuddyDto.S2ProductId = null;
            productBuddyDto.S2ProductGtin = null;
            productBuddyDto.S2Percentage = null;
            productBuddyDto.S3ProductId = null;
            productBuddyDto.S3ProductGtin = null;
            productBuddyDto.S3Percentage = null;
            productBuddyDto.S4ProductId = null;
            productBuddyDto.S4ProductGtin = null;
            productBuddyDto.S4Percentage = null;
            productBuddyDto.S5ProductId = null;
            productBuddyDto.S5ProductGtin = null;
            productBuddyDto.S5Percentage = null;
        }

        private static void ApplyGFSLocationBuddyValuesToCCM(AssortmentLocationBuddyDto ccmAssortmentLocationBuddy,
            Services.Assortment.AssortmentLocationBuddy assortmentLocationBuddy,
            PlanogramAssortmentLocationBuddyTreatmentType gfsLocationBuddyTreatmentType,
            Dictionary<String, Int16> locationCodeLookup)
        {
            ccmAssortmentLocationBuddy.TreatmentType = (Byte)gfsLocationBuddyTreatmentType;
            if (!String.IsNullOrEmpty(assortmentLocationBuddy.S1LocationCode) && locationCodeLookup.ContainsKey(assortmentLocationBuddy.S1LocationCode))
            {
                ccmAssortmentLocationBuddy.S1LocationId = locationCodeLookup[assortmentLocationBuddy.S1LocationCode];
                ccmAssortmentLocationBuddy.S1Percentage = assortmentLocationBuddy.S1Percentage;
            }
            if (!String.IsNullOrEmpty(assortmentLocationBuddy.S2LocationCode) && locationCodeLookup.ContainsKey(assortmentLocationBuddy.S2LocationCode))
            {
                ccmAssortmentLocationBuddy.S2LocationId = locationCodeLookup[assortmentLocationBuddy.S2LocationCode];
                ccmAssortmentLocationBuddy.S2Percentage = assortmentLocationBuddy.S2Percentage;
            }
            if (!String.IsNullOrEmpty(assortmentLocationBuddy.S3LocationCode) && locationCodeLookup.ContainsKey(assortmentLocationBuddy.S3LocationCode))
            {
                ccmAssortmentLocationBuddy.S3LocationId = locationCodeLookup[assortmentLocationBuddy.S3LocationCode];
                ccmAssortmentLocationBuddy.S3Percentage = assortmentLocationBuddy.S3Percentage;
            }
            if (!String.IsNullOrEmpty(assortmentLocationBuddy.S4LocationCode) && locationCodeLookup.ContainsKey(assortmentLocationBuddy.S4LocationCode))
            {
                ccmAssortmentLocationBuddy.S4LocationId = locationCodeLookup[assortmentLocationBuddy.S4LocationCode];
                ccmAssortmentLocationBuddy.S4Percentage = assortmentLocationBuddy.S4Percentage;
            }
            if (!String.IsNullOrEmpty(assortmentLocationBuddy.S5LocationCode) && locationCodeLookup.ContainsKey(assortmentLocationBuddy.S5LocationCode))
            {
                ccmAssortmentLocationBuddy.S5LocationId = locationCodeLookup[assortmentLocationBuddy.S5LocationCode];
                ccmAssortmentLocationBuddy.S5Percentage = assortmentLocationBuddy.S5Percentage;
            }
        }

        private static void ClearCCMLocationBuddySources(AssortmentLocationBuddyDto locationBuddyDto)
        {
            locationBuddyDto.S1LocationId = null;
            locationBuddyDto.S1LocationCode = null;
            locationBuddyDto.S1Percentage = null;
            locationBuddyDto.S2LocationId = null;
            locationBuddyDto.S2LocationCode = null;
            locationBuddyDto.S2Percentage = null;
            locationBuddyDto.S3LocationId = null;
            locationBuddyDto.S3LocationCode = null;
            locationBuddyDto.S3Percentage = null;
            locationBuddyDto.S4LocationId = null;
            locationBuddyDto.S4LocationCode = null;
            locationBuddyDto.S4Percentage = null;
            locationBuddyDto.S5LocationId = null;
            locationBuddyDto.S5LocationCode = null;
            locationBuddyDto.S5Percentage = null;
        }

        private static void ApplyGFSInventoryRuleValuesToCCM(AssortmentInventoryRuleDto ccmAssortmentInventoryRule, Services.Assortment.AssortmentInventoryRule gfsAssortmentInventoryRule)
        {
            ccmAssortmentInventoryRule.CasePack = gfsAssortmentInventoryRule.CasePack;
            ccmAssortmentInventoryRule.DaysOfSupply = gfsAssortmentInventoryRule.DaysOfSupply;
            ccmAssortmentInventoryRule.MinFacings = gfsAssortmentInventoryRule.MinFacings;
            ccmAssortmentInventoryRule.MinUnits = gfsAssortmentInventoryRule.MinUnits;
            ccmAssortmentInventoryRule.ReplenishmentDays = gfsAssortmentInventoryRule.ReplenishmentDays;
            ccmAssortmentInventoryRule.ShelfLife = gfsAssortmentInventoryRule.ShelfLife;
            ccmAssortmentInventoryRule.WasteHurdleCasePack = gfsAssortmentInventoryRule.WasteHurdleCasePack;
            ccmAssortmentInventoryRule.WasteHurdleUnits = gfsAssortmentInventoryRule.WasteHurdleUnits;
                                                                                            
        }

        #endregion

        #region Syncrhonize Deleted Assortments

        private void SynchronizeAssortmentsDeleted(ProcessContext context)
        {
            try
            {
                using (IAssortmentInfoDal dalInfo = context.DalContext.GetDal<IAssortmentInfoDal>())
                {
                    using (IAssortmentDal dal = context.DalContext.GetDal<IAssortmentDal>())
                    {
                        #region old delete
                        //ObservableCollection<Services.Assortment.AssortmentInfo> gfsAssortmentsDeleted = null;

                        //if (SyncTarget.LastSync != null)
                        //{
                        //    context.DoWorkWithRetries(delegate()
                        //    {
                        //        GetAssortmentInfoByEntityIdDateDeletedRequest assortmentByEntityIdDateDeleted = new GetAssortmentInfoByEntityIdDateDeletedRequest(this.GFSEntityId, SyncTarget.LastSync.Value);
                        //        gfsAssortmentsDeleted = new ObservableCollection<Services.Assortment.AssortmentInfo>(context.Services.AssortmentServiceClient.GetAssortmentInfoByEntityIdDateDeleted(assortmentByEntityIdDateDeleted).AssortmentInfos);
                        //    });


                        //    IEnumerable<AssortmentInfoDto> ccmAssortments;
                        //    using (IAssortmentInfoDal dalAssortmentInfo = context.DalContext.GetDal<IAssortmentInfoDal>())
                        //    {
                        //        ccmAssortments = dalAssortmentInfo.FetchByEntityId(this.EntityId);
                        //    }

                        //    // loop through the assortments to delete
                        //    foreach (Services.Assortment.AssortmentInfo gfsAssortment in gfsAssortmentsDeleted)
                        //    {
                        //        // check if assortment exists in CCM
                        //        AssortmentInfoDto ccmAssortment = ccmAssortments.FirstOrDefault(i => i.UniqueContentReference == gfsAssortment.UniqueContentReference);

                        //        if (ccmAssortment != null)
                        //        {
                        //            dal.DeleteById(ccmAssortment.Id);
                        //        }
                        //    }
                        //}
                        #endregion

                        List<Services.Assortment.AssortmentInfo> gfsAssortmentDeleted = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsAssortmentDeleted = context.Services.AssortmentServiceClient.GetAssortmentInfoByEntityId(
                                new Services.Assortment.GetAssortmentInfoByEntityIdRequest(this.GFSEntityId)).AssortmentInfos;
                        });

                        //fetch ccm universes
                        IEnumerable<AssortmentInfoDto> cdtList = dalInfo.FetchByEntityId(this.EntityId);

                        //loop through ccm product universes
                        foreach (AssortmentInfoDto ccmAssortment in cdtList)
                        {
                            //came from a sync
                            if (ccmAssortment.ParentUniqueContentReference != null)
                            {
                                //try to find the gfs product universe
                                Services.Assortment.AssortmentInfo gfsContent = gfsAssortmentDeleted.FirstOrDefault(u => u.UniqueContentReference == ccmAssortment.UniqueContentReference);

                                //doesn't exist in gfs so delete it
                                if (gfsContent == null)
                                {
                                    dal.DeleteById(ccmAssortment.Id);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, "Assortment Delete", ex);

                // throw exception back up the stack
                throw;
            }
        }

        #endregion

        #endregion

        #region Synchronise Assortment Minor Revision

        /// <summary>
        /// Syncrhonises the assortments within the solution
        /// </summary>
        /// <param name="context">The process context</param>
        private void SynchronizeAssortmentMinorRevision(ProcessContext context)
        {
            DateTime processStartDate = DateTime.UtcNow;

            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IAssortmentMinorRevisionDal dalAssortmentMinorRevision = context.DalContext.GetDal<IAssortmentMinorRevisionDal>())
                {
                    using (IAssortmentMinorRevisionListActionDal dalAssortmentMinorRevisonListAction = context.DalContext.GetDal<IAssortmentMinorRevisionListActionDal>())
                    {
                        using (IAssortmentMinorRevisionListActionLocationDal dalAssortmentMinorRevisionListActionLocation = context.DalContext.GetDal<IAssortmentMinorRevisionListActionLocationDal>())
                        {
                            using (IAssortmentMinorRevisionDeListActionDal dalAssortmentMinorRevisionDeListAction = context.DalContext.GetDal<IAssortmentMinorRevisionDeListActionDal>())
                            {
                                using (IAssortmentMinorRevisionDeListActionLocationDal dalAssortmentMinorRevisionDeListActionLocation = context.DalContext.GetDal<IAssortmentMinorRevisionDeListActionLocationDal>())
                                {
                                    using (IAssortmentMinorRevisionAmendDistributionActionDal dalAssortmentMinorRevisionAmendDistributionAction = context.DalContext.GetDal<IAssortmentMinorRevisionAmendDistributionActionDal>())
                                    {
                                        using (IAssortmentMinorRevisionAmendDistributionActionLocationDal dalAssortmentMinorRevisionAmendDistributionActionLocation = context.DalContext.GetDal<IAssortmentMinorRevisionAmendDistributionActionLocationDal>())
                                        {
                                            using (IAssortmentMinorRevisionReplaceActionDal dalAssortmentMinorRevisionReplaceAction = context.DalContext.GetDal<IAssortmentMinorRevisionReplaceActionDal>())
                                            {
                                                using (IAssortmentMinorRevisionReplaceActionLocationDal dalAssortmentMinorRevisionReplaceActionLocation = context.DalContext.GetDal<IAssortmentMinorRevisionReplaceActionLocationDal>())
                                                {
                                                    // Ensure that the merch hierarchy process is active for the sync target
                                                    ObservableCollection<Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo> gfsAssortmentMinorRevisionInfos = null;

                                                    #region Get Collection to Update

                                                    //Get the Assortment info 
                                                    context.DoWorkWithRetries(delegate()
                                                    {
                                                        GetAssortmentMinorRevisionInfoByEntityIdRequest assortmentMinorRevisionInfoByEntityIdRequest = new GetAssortmentMinorRevisionInfoByEntityIdRequest(GFSEntityId);

                                                        gfsAssortmentMinorRevisionInfos = new ObservableCollection<Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo>(
                                                            context.Services.AssortmentMinorRevisionServiceClient.GetAssortmentMinorRevisionInfoByEntityId(assortmentMinorRevisionInfoByEntityIdRequest).AssortmentMinorRevisionInfos);
                                                    });

                                                    #endregion

                                                    #region Create Lookups

                                                    IEnumerable<AssortmentMinorRevisionDto> ccmAssortmentMinorRevisions;

                                                    //Fetch the current CCM assortment Minor Revisions
                                                    try
                                                    {
                                                        ccmAssortmentMinorRevisions = dalAssortmentMinorRevision.FetchAll();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        //If there are no dtos i.e first time sync etc we just need an empty list
                                                        ccmAssortmentMinorRevisions = new List<AssortmentMinorRevisionDto>();
                                                    }


                                                    Dictionary<String, short> storeCodeLookup = new Dictionary<String, short>();
                                                    Dictionary<String, Int32> productCodeLookup = new Dictionary<String, Int32>();
                                                    Dictionary<Guid?, Int32?> consumerDecisionTreeLookup = new Dictionary<Guid?, Int32?>();
                                                    Dictionary<Int32?, Int32> consumerDecisionTreeNodeLookup = new Dictionary<Int32?, Int32>();

                                                    //If there are any GFS assortments to process
                                                    if (gfsAssortmentMinorRevisionInfos.Count >= 1)
                                                    {
                                                        using (ILocationInfoDal dalStore = context.DalContext.GetDal<ILocationInfoDal>())
                                                        {
                                                            //Fetch all stores from CCM and add to dictionary
                                                            foreach (LocationInfoDto locationInfo in dalStore.FetchByEntityIdIncludingDeleted(this.EntityId))
                                                            {
                                                                storeCodeLookup.Add(locationInfo.Code, locationInfo.Id);
                                                            }
                                                        }

                                                        using (IProductInfoDal dalProductInfo = context.DalContext.GetDal<IProductInfoDal>())
                                                        {
                                                            //Fetch all products from CCM and add to dictionary
                                                            foreach (ProductInfoDto productInfo in dalProductInfo.FetchByEntityIdIncludingDeleted(this.EntityId))
                                                            {
                                                                productCodeLookup.Add(productInfo.Gtin, productInfo.Id);
                                                            }
                                                        }

                                                        using (IConsumerDecisionTreeInfoDal consumerDecisionTreeInfo = context.DalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                                                        {
                                                            //Fetch all products from CCM and add to dictionary
                                                            foreach (ConsumerDecisionTreeInfoDto cdtInfo in consumerDecisionTreeInfo.FetchByEntityId(this.EntityId))
                                                            {
                                                                consumerDecisionTreeLookup.Add(cdtInfo.UniqueContentReference, cdtInfo.Id);
                                                            }
                                                        }

                                                        using (IConsumerDecisionTreeNodeDal consumerDecisionTreeInfo = context.DalContext.GetDal<IConsumerDecisionTreeNodeDal>())
                                                        {
                                                            //Fetch all products from CCM and add to dictionary
                                                            foreach (ConsumerDecisionTreeNodeDto cdtNode in consumerDecisionTreeInfo.FetchAll())
                                                            {
                                                                //Cdt node can be null but we expect that all records pertaining to have come from GFS will have value.
                                                                //We are only syncing values from GFS therefore all is good.
                                                                if (cdtNode.GFSId != null)
                                                                {
                                                                    // GFS ID : CCM ID
                                                                    if (consumerDecisionTreeNodeLookup.ContainsKey(cdtNode.GFSId))
                                                                    {
                                                                        consumerDecisionTreeNodeLookup.Remove(cdtNode.GFSId);
                                                                    }
                                                                    consumerDecisionTreeNodeLookup.Add(cdtNode.GFSId, cdtNode.Id);
                                                                }

                                                            }
                                                        }

                                                    #endregion

                                                        // Loop through the assortment info list 
                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo assortmentMinorRevision in gfsAssortmentMinorRevisionInfos)
                                                        {
                                                            // check if it is the latest version
                                                            if (assortmentMinorRevision.IsLatestVersion == true)
                                                            {
                                                                //Fetch full assortment from GFS
                                                                Galleria.Ccm.Services.AssortmentMinorRevision.AssortmentMinorRevision gfsAssortmentMinorRevision = null;

                                                                //Check if the version already exists
                                                                //Formatted code is not updated by ccm, so perform the check on this field to ensure that the records match
                                                                AssortmentMinorRevisionDto ccmAssortmentMinorRevision = null;

                                                                //ccm assortments may be null if no records returned by dal fetch
                                                                if (ccmAssortmentMinorRevisions != null && ccmAssortmentMinorRevisions.Any())
                                                                {
                                                                    Guid ucr;
                                                                    if (assortmentMinorRevision.ParentUniqueContentReference != null)
                                                                    {
                                                                        ucr = assortmentMinorRevision.ParentUniqueContentReference.Value;
                                                                    }
                                                                    else
                                                                    {
                                                                        ucr = assortmentMinorRevision.UniqueContentReference;
                                                                    }

                                                                    ccmAssortmentMinorRevision = ccmAssortmentMinorRevisions.FirstOrDefault(i => i.ParentUniqueContentReference == ucr);

                                                                    //find one with the same name
                                                                    AssortmentMinorRevisionDto ccmDuplicateAssortmentMinorRevision = ccmAssortmentMinorRevisions.FirstOrDefault(i => (i.Name == assortmentMinorRevision.Name && i.ParentUniqueContentReference == null));

                                                                    if (ccmDuplicateAssortmentMinorRevision != null)
                                                                    {
                                                                        //get the new one
                                                                        AssortmentMinorRevisionDto duplicateCdt = dalAssortmentMinorRevision.FetchById(ccmDuplicateAssortmentMinorRevision.Id);
                                                                        if (duplicateCdt != null)
                                                                        {
                                                                            duplicateCdt.Name += " (Local Copy)";
                                                                            dalAssortmentMinorRevision.Update(duplicateCdt);
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    ccmAssortmentMinorRevision = null;
                                                                }

                                                                if (ccmAssortmentMinorRevision == null ||
                                                                    ccmAssortmentMinorRevision.UniqueContentReference != assortmentMinorRevision.UniqueContentReference
                                                                    || SyncTarget.LastSync == null ||
                                                                    (assortmentMinorRevision.DateLastModified != null && SyncTarget.LastSync != null && assortmentMinorRevision.DateLastModified > SyncTarget.LastSync.Value))
                                                                {
                                                                    context.DoWorkWithRetries(delegate()
                                                                    {
                                                                        GetAssortmentMinorRevisionByUniqueContentReferenceRequest assortmentByUniqueContentRequest = new GetAssortmentMinorRevisionByUniqueContentReferenceRequest(assortmentMinorRevision.UniqueContentReference);
                                                                        gfsAssortmentMinorRevision = context.Services.AssortmentMinorRevisionServiceClient.GetAssortmentMinorRevisionByUniqueContentReference(assortmentByUniqueContentRequest).AssortmentMinorRevision;
                                                                    });
                                                                    if (context.ProductGroupMap.ContainsKey(gfsAssortmentMinorRevision.ProductGroupCode))
                                                                    {
                                                                        //Add new assortment
                                                                        if (ccmAssortmentMinorRevision == null)
                                                                        {
                                                                            #region Insert AssortmentMinorRevision

                                                                            ccmAssortmentMinorRevision = new AssortmentMinorRevisionDto();

                                                                            ccmAssortmentMinorRevision.Name = gfsAssortmentMinorRevision.Name;
                                                                            ccmAssortmentMinorRevision.UniqueContentReference = gfsAssortmentMinorRevision.UniqueContentReference;
                                                                            ccmAssortmentMinorRevision.EntityId = this.EntityId;

                                                                            if (gfsAssortmentMinorRevision.ParentUniqueContentReference != null)
                                                                            {
                                                                                ccmAssortmentMinorRevision.ParentUniqueContentReference = gfsAssortmentMinorRevision.ParentUniqueContentReference;
                                                                            }
                                                                            else
                                                                            {
                                                                                ccmAssortmentMinorRevision.ParentUniqueContentReference = gfsAssortmentMinorRevision.UniqueContentReference;
                                                                            }

                                                                            //Following values must be resolved into local id:
                                                                            if (gfsAssortmentMinorRevision.ConsumerDecisionTreeUniqueContentReference != null)
                                                                            {
                                                                                Int32? consId;
                                                                                consumerDecisionTreeLookup.TryGetValue(gfsAssortmentMinorRevision.ConsumerDecisionTreeUniqueContentReference, out consId);
                                                                                ccmAssortmentMinorRevision.ConsumerDecisionTreeId = consId;
                                                                            }

                                                                            if (gfsAssortmentMinorRevision.ProductGroupCode != null)
                                                                            {
                                                                                Int32 grpId;
                                                                                context.ProductGroupMap.TryGetValue(gfsAssortmentMinorRevision.ProductGroupCode, out grpId);
                                                                                ccmAssortmentMinorRevision.ProductGroupId = context.ProductGroupMap[gfsAssortmentMinorRevision.ProductGroupCode];
                                                                            }

                                                                            dalAssortmentMinorRevision.Insert(ccmAssortmentMinorRevision);

                                                                            #endregion

                                                                            #region Insert AssortmentMinorRevisionListAction & ListActionLocations

                                                                            AssortmentMinorRevisionListActionDto ccmAssortmentMinorRevisionListAction = new AssortmentMinorRevisionListActionDto();
                                                                            foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionListAction gfsAssortmentMinorRevisionListAction in gfsAssortmentMinorRevision.ListActions)
                                                                            {
                                                                                ccmAssortmentMinorRevisionListAction = new AssortmentMinorRevisionListActionDto();

                                                                                ccmAssortmentMinorRevisionListAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                ccmAssortmentMinorRevisionListAction.ProductName = gfsAssortmentMinorRevisionListAction.ProductName;
                                                                                ccmAssortmentMinorRevisionListAction.Priority = gfsAssortmentMinorRevisionListAction.Priority;
                                                                                ccmAssortmentMinorRevisionListAction.Comments = gfsAssortmentMinorRevisionListAction.Comments;
                                                                                ccmAssortmentMinorRevisionListAction.ProductGtin = gfsAssortmentMinorRevisionListAction.ProductGtin;

                                                                                //Resolved Id's
                                                                                if (gfsAssortmentMinorRevisionListAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionListAction.ProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionListAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionListAction.ProductGtin];
                                                                                }

                                                                                //AssortmentConsumerDecisionTreeNodeId is special case - no gtin, ucr, or any other link. So use the CCM DB field added for this.
                                                                                if (gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId != null && consumerDecisionTreeNodeLookup.ContainsKey(gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionListAction.AssortmentConsumerDecisionTreeNodeId = consumerDecisionTreeNodeLookup[gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId];
                                                                                }

                                                                                dalAssortmentMinorRevisonListAction.Insert(ccmAssortmentMinorRevisionListAction);


                                                                                //Now Insert children
                                                                                #region Insert AssortmentMinorRevisionListActionLocation

                                                                                AssortmentMinorRevisionListActionLocationDto ccmAssortmentMinorRevisionListActionLocation = new AssortmentMinorRevisionListActionLocationDto();

                                                                                //foreach set of locations in each list action
                                                                                foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionListActionLocation gfsAssortmentMinorRevisionListActionLocation in gfsAssortmentMinorRevisionListAction.Locations)
                                                                                {
                                                                                    ccmAssortmentMinorRevisionListActionLocation = new AssortmentMinorRevisionListActionLocationDto();

                                                                                    ccmAssortmentMinorRevisionListActionLocation.AssortmentMinorRevisionListActionId = ccmAssortmentMinorRevisionListAction.Id;
                                                                                    ccmAssortmentMinorRevisionListActionLocation.LocationCode = gfsAssortmentMinorRevisionListActionLocation.LocationCode;
                                                                                    ccmAssortmentMinorRevisionListActionLocation.LocationName = gfsAssortmentMinorRevisionListActionLocation.LocationName;
                                                                                    ccmAssortmentMinorRevisionListActionLocation.Units = gfsAssortmentMinorRevisionListActionLocation.Units;
                                                                                    ccmAssortmentMinorRevisionListActionLocation.Facings = gfsAssortmentMinorRevisionListActionLocation.Facings;

                                                                                    ////Resolved Id's
                                                                                    if (gfsAssortmentMinorRevisionListActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionListActionLocation.LocationCode))
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionListActionLocation.LocationId = (short?)storeCodeLookup[gfsAssortmentMinorRevisionListActionLocation.LocationCode];
                                                                                    }

                                                                                    dalAssortmentMinorRevisionListActionLocation.Insert(ccmAssortmentMinorRevisionListActionLocation);
                                                                                }

                                                                                #endregion
                                                                            }

                                                                            #endregion

                                                                            #region Insert AssortmentMinorRevisionDeListAction & DeListActionLocation

                                                                            AssortmentMinorRevisionDeListActionDto ccmAssortmentMinorRevisionDeListAction = new AssortmentMinorRevisionDeListActionDto();
                                                                            foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionDeListAction gfsAssortmentMinorRevisionDeListAction in gfsAssortmentMinorRevision.DeListActions)
                                                                            {
                                                                                ccmAssortmentMinorRevisionDeListAction = new AssortmentMinorRevisionDeListActionDto();

                                                                                ccmAssortmentMinorRevisionDeListAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                ccmAssortmentMinorRevisionDeListAction.ProductName = gfsAssortmentMinorRevisionDeListAction.ProductName;
                                                                                ccmAssortmentMinorRevisionDeListAction.Priority = gfsAssortmentMinorRevisionDeListAction.Priority;
                                                                                ccmAssortmentMinorRevisionDeListAction.Comments = gfsAssortmentMinorRevisionDeListAction.Comments;
                                                                                ccmAssortmentMinorRevisionDeListAction.ProductGtin = gfsAssortmentMinorRevisionDeListAction.ProductGtin;

                                                                                //Resolved Id's
                                                                                if (gfsAssortmentMinorRevisionDeListAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionDeListAction.ProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionDeListAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionDeListAction.ProductGtin];
                                                                                }

                                                                                dalAssortmentMinorRevisionDeListAction.Insert(ccmAssortmentMinorRevisionDeListAction);


                                                                                //Now insert children....
                                                                                #region AssortmentMinorRevisionDeListActionLocationDto

                                                                                AssortmentMinorRevisionDeListActionLocationDto ccmAssortmentMinorRevisionDeListActionLocation = new AssortmentMinorRevisionDeListActionLocationDto();

                                                                                foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionDeListActionLocation gfsAssortmentMinorRevisionDeListActionLocation in gfsAssortmentMinorRevisionDeListAction.Locations)
                                                                                {
                                                                                    ccmAssortmentMinorRevisionDeListActionLocation = new AssortmentMinorRevisionDeListActionLocationDto();

                                                                                    ccmAssortmentMinorRevisionDeListActionLocation.AssortmentMinorRevisionDeListActionId = ccmAssortmentMinorRevisionDeListAction.Id;
                                                                                    ccmAssortmentMinorRevisionDeListActionLocation.LocationCode = gfsAssortmentMinorRevisionDeListActionLocation.LocationCode;
                                                                                    ccmAssortmentMinorRevisionDeListActionLocation.LocationName = gfsAssortmentMinorRevisionDeListActionLocation.LocationName;


                                                                                    ////Resolved Id's
                                                                                    if (gfsAssortmentMinorRevisionDeListActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionDeListActionLocation.LocationCode))
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionDeListActionLocation.LocationId = (short?)storeCodeLookup[gfsAssortmentMinorRevisionDeListActionLocation.LocationCode];
                                                                                    }

                                                                                    dalAssortmentMinorRevisionDeListActionLocation.Insert(ccmAssortmentMinorRevisionDeListActionLocation);
                                                                                }

                                                                                #endregion
                                                                            }

                                                                            #endregion

                                                                            #region Insert AssortmentMinorRevisionAmendDistributionAction & AmendDistributionActionLocation

                                                                            AssortmentMinorRevisionAmendDistributionActionDto ccmAssortmentMinorRevisionAmendDistributionAction = new AssortmentMinorRevisionAmendDistributionActionDto();
                                                                            foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionAmendDistributionAction gfsAssortmentMinorRevisionAmendDistributionAction in gfsAssortmentMinorRevision.AmendDistributionActions)
                                                                            {
                                                                                ccmAssortmentMinorRevisionAmendDistributionAction = new AssortmentMinorRevisionAmendDistributionActionDto();

                                                                                ccmAssortmentMinorRevisionAmendDistributionAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                ccmAssortmentMinorRevisionAmendDistributionAction.ProductName = gfsAssortmentMinorRevisionAmendDistributionAction.ProductName;
                                                                                ccmAssortmentMinorRevisionAmendDistributionAction.Priority = gfsAssortmentMinorRevisionAmendDistributionAction.Priority;
                                                                                ccmAssortmentMinorRevisionAmendDistributionAction.Comments = gfsAssortmentMinorRevisionAmendDistributionAction.Comments;
                                                                                ccmAssortmentMinorRevisionAmendDistributionAction.ProductGtin = gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin;

                                                                                //Resolved Id's
                                                                                if (gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionAmendDistributionAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin];
                                                                                }

                                                                                dalAssortmentMinorRevisionAmendDistributionAction.Insert(ccmAssortmentMinorRevisionAmendDistributionAction);

                                                                                //Insert children....
                                                                                #region Insert AssortmentMinorRevisionAmendDistributionActionLocation

                                                                                AssortmentMinorRevisionAmendDistributionActionLocationDto ccmAssortmentMinorRevisionAmendDistributionActionLocation = new AssortmentMinorRevisionAmendDistributionActionLocationDto();

                                                                                foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionAmendDistributionActionLocation gfsAssortmentMinorRevisionAmendDistributionActionLocation in gfsAssortmentMinorRevisionAmendDistributionAction.Locations)
                                                                                {
                                                                                    ccmAssortmentMinorRevisionAmendDistributionActionLocation = new AssortmentMinorRevisionAmendDistributionActionLocationDto();

                                                                                    ccmAssortmentMinorRevisionAmendDistributionActionLocation.AssortmentMinorRevisionAmendDistributionActionId = ccmAssortmentMinorRevisionAmendDistributionAction.Id;
                                                                                    ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode = gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode;
                                                                                    ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationName = gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationName;
                                                                                    ccmAssortmentMinorRevisionAmendDistributionActionLocation.Units = gfsAssortmentMinorRevisionAmendDistributionActionLocation.Units;
                                                                                    ccmAssortmentMinorRevisionAmendDistributionActionLocation.Facings = gfsAssortmentMinorRevisionAmendDistributionActionLocation.Facings;

                                                                                    //Resolved Id's
                                                                                    if (gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode))
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationId = storeCodeLookup[gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode];
                                                                                    }

                                                                                    dalAssortmentMinorRevisionAmendDistributionActionLocation.Insert(ccmAssortmentMinorRevisionAmendDistributionActionLocation);
                                                                                }


                                                                                #endregion
                                                                            }

                                                                            #endregion

                                                                            #region Insert AssortmentMinorRevisionReplaceAction & ReplaceActionLocation

                                                                            AssortmentMinorRevisionReplaceActionDto ccmAssortmentMinorRevisionReplaceAction = new AssortmentMinorRevisionReplaceActionDto();
                                                                            foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionReplaceAction gfsAssortmentMinorRevisionReplaceAction in gfsAssortmentMinorRevision.ReplaceActions)
                                                                            {
                                                                                ccmAssortmentMinorRevisionReplaceAction = new AssortmentMinorRevisionReplaceActionDto();

                                                                                ccmAssortmentMinorRevisionReplaceAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                ccmAssortmentMinorRevisionReplaceAction.ProductGtin = gfsAssortmentMinorRevisionReplaceAction.ProductGtin;
                                                                                ccmAssortmentMinorRevisionReplaceAction.ProductName = gfsAssortmentMinorRevisionReplaceAction.ProductName;
                                                                                ccmAssortmentMinorRevisionReplaceAction.ReplacementProductGtin = gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin;
                                                                                ccmAssortmentMinorRevisionReplaceAction.ReplacementProductName = gfsAssortmentMinorRevisionReplaceAction.ReplacementProductName;
                                                                                ccmAssortmentMinorRevisionReplaceAction.Priority = gfsAssortmentMinorRevisionReplaceAction.Priority;
                                                                                ccmAssortmentMinorRevisionReplaceAction.Comments = gfsAssortmentMinorRevisionReplaceAction.Comments;


                                                                                //Resolved Id's
                                                                                if (gfsAssortmentMinorRevisionReplaceAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceAction.ProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionReplaceAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionReplaceAction.ProductGtin];
                                                                                }

                                                                                if (gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionReplaceAction.ReplacementProductId = productCodeLookup[gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin];
                                                                                }

                                                                                dalAssortmentMinorRevisionReplaceAction.Insert(ccmAssortmentMinorRevisionReplaceAction);


                                                                                //Add children.....
                                                                                #region Insert AssortmentMinorRevisionReplaceActionLocation

                                                                                AssortmentMinorRevisionReplaceActionLocationDto ccmAssortmentMinorRevisionReplaceActionLocation = new AssortmentMinorRevisionReplaceActionLocationDto();

                                                                                foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionReplaceActionLocation gfsAssortmentMinorRevisionReplaceActionLocation in gfsAssortmentMinorRevisionReplaceAction.Locations)
                                                                                {
                                                                                    ccmAssortmentMinorRevisionReplaceActionLocation = new AssortmentMinorRevisionReplaceActionLocationDto();

                                                                                    ccmAssortmentMinorRevisionReplaceActionLocation.AssortmentMinorRevisionReplaceActionId = ccmAssortmentMinorRevisionReplaceAction.Id;
                                                                                    ccmAssortmentMinorRevisionReplaceActionLocation.LocationCode = gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode;
                                                                                    ccmAssortmentMinorRevisionReplaceActionLocation.LocationName = gfsAssortmentMinorRevisionReplaceActionLocation.LocationName;
                                                                                    ccmAssortmentMinorRevisionReplaceActionLocation.Units = gfsAssortmentMinorRevisionReplaceActionLocation.Units;
                                                                                    ccmAssortmentMinorRevisionReplaceActionLocation.Facings = gfsAssortmentMinorRevisionReplaceActionLocation.Facings;

                                                                                    //Resolved Id's
                                                                                    if (gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode))
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionReplaceActionLocation.LocationId = storeCodeLookup[gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode];
                                                                                    }

                                                                                    dalAssortmentMinorRevisionReplaceActionLocation.Insert(ccmAssortmentMinorRevisionReplaceActionLocation);
                                                                                }

                                                                                #endregion

                                                                            }

                                                                            #endregion
                                                                        }
                                                                        else
                                                                        {
                                                                            //Do Updates (We must check for child deletes here).

                                                                            #region Update AssortmentMinorRevision

                                                                            ccmAssortmentMinorRevision.Name = gfsAssortmentMinorRevision.Name;
                                                                            ccmAssortmentMinorRevision.UniqueContentReference = gfsAssortmentMinorRevision.UniqueContentReference;
                                                                            ccmAssortmentMinorRevision.EntityId = this.EntityId;
                                                                            ccmAssortmentMinorRevision.DateLastModified = gfsAssortmentMinorRevision.DateLastModified;

                                                                            if (gfsAssortmentMinorRevision.ParentUniqueContentReference != null)
                                                                            {
                                                                                ccmAssortmentMinorRevision.ParentUniqueContentReference = gfsAssortmentMinorRevision.ParentUniqueContentReference;
                                                                            }
                                                                            else
                                                                            {
                                                                                ccmAssortmentMinorRevision.ParentUniqueContentReference = gfsAssortmentMinorRevision.UniqueContentReference;
                                                                            }

                                                                            //Following values must be resolved into local id:
                                                                            if (gfsAssortmentMinorRevision.ConsumerDecisionTreeUniqueContentReference != null)
                                                                            {
                                                                                Int32? consId;
                                                                                consumerDecisionTreeLookup.TryGetValue(gfsAssortmentMinorRevision.ConsumerDecisionTreeUniqueContentReference, out consId);
                                                                                ccmAssortmentMinorRevision.ConsumerDecisionTreeId = consId;
                                                                            }

                                                                            if (gfsAssortmentMinorRevision.ProductGroupCode != null)
                                                                            {
                                                                                Int32 grpId;
                                                                                context.ProductGroupMap.TryGetValue(gfsAssortmentMinorRevision.ProductGroupCode, out grpId);
                                                                                ccmAssortmentMinorRevision.ProductGroupId = context.ProductGroupMap[gfsAssortmentMinorRevision.ProductGroupCode];
                                                                            }


                                                                            dalAssortmentMinorRevision.Update(ccmAssortmentMinorRevision);

                                                                            #endregion

                                                                            #region Update AssortmentMinorRevisionListAction & AssortmentMinorRevisionListActionLocation

                                                                            List<AssortmentMinorRevisionListActionDto> ccmAssortmentMinorRevisionListActions = dalAssortmentMinorRevisonListAction.FetchByAssortmentMinorRevisionId(ccmAssortmentMinorRevision.Id).ToList();
                                                                            AssortmentMinorRevisionListActionDto ccmAssortmentMinorRevisionListAction;

                                                                            foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionListAction gfsAssortmentMinorRevisionListAction in gfsAssortmentMinorRevision.ListActions)
                                                                            {
                                                                                if (gfsAssortmentMinorRevisionListAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionListAction.ProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionListAction = ccmAssortmentMinorRevisionListActions.Where(p => p.ProductId == productCodeLookup[gfsAssortmentMinorRevisionListAction.ProductGtin]).FirstOrDefault();

                                                                                    if (ccmAssortmentMinorRevisionListAction != null)
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionListAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                        ccmAssortmentMinorRevisionListAction.ProductName = gfsAssortmentMinorRevisionListAction.ProductName;
                                                                                        ccmAssortmentMinorRevisionListAction.Priority = gfsAssortmentMinorRevisionListAction.Priority;
                                                                                        ccmAssortmentMinorRevisionListAction.Comments = gfsAssortmentMinorRevisionListAction.Comments;
                                                                                        ccmAssortmentMinorRevisionListAction.ProductGtin = gfsAssortmentMinorRevisionListAction.ProductGtin;

                                                                                        //Resolved Id's
                                                                                        if (gfsAssortmentMinorRevisionListAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionListAction.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionListAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionListAction.ProductGtin];
                                                                                        }

                                                                                        //AssortmentConsumerDecisionTreeNodeId is special case - no gtin, ucr, or any other link. So use the CCM DB field added for this.
                                                                                        if (gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId != null && consumerDecisionTreeNodeLookup.ContainsKey(gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionListAction.AssortmentConsumerDecisionTreeNodeId = consumerDecisionTreeNodeLookup[gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId];
                                                                                        }

                                                                                        dalAssortmentMinorRevisonListAction.Update(ccmAssortmentMinorRevisionListAction);
                                                                                        ccmAssortmentMinorRevisionListActions.Remove(ccmAssortmentMinorRevisionListAction);

                                                                                        #region AssortmentMinorRevisionListActionLocation (Child update sync for children)
                                                                                        //Now ensure its children are updated

                                                                                        List<AssortmentMinorRevisionListActionLocationDto> ccmAssortmentMinorRevisionListActionLocations = dalAssortmentMinorRevisionListActionLocation.FetchByAssortmentMinorRevisionListActionId(ccmAssortmentMinorRevisionListAction.Id).ToList();
                                                                                        AssortmentMinorRevisionListActionLocationDto ccmAssortmentMinorRevisionListActionLocation;

                                                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionListActionLocation gfsAssortmentMinorRevisionListActionLocation in gfsAssortmentMinorRevisionListAction.Locations)
                                                                                        {
                                                                                            //We link on code as only one location code per MRListAction structure
                                                                                            ccmAssortmentMinorRevisionListActionLocation = ccmAssortmentMinorRevisionListActionLocations.Where(p => p.LocationCode == gfsAssortmentMinorRevisionListActionLocation.LocationCode).FirstOrDefault();

                                                                                            if (ccmAssortmentMinorRevisionListActionLocation != null)
                                                                                            {
                                                                                                ccmAssortmentMinorRevisionListActionLocation.AssortmentMinorRevisionListActionId = ccmAssortmentMinorRevisionListAction.Id;
                                                                                                ccmAssortmentMinorRevisionListActionLocation.LocationCode = gfsAssortmentMinorRevisionListActionLocation.LocationCode;
                                                                                                ccmAssortmentMinorRevisionListActionLocation.LocationName = gfsAssortmentMinorRevisionListActionLocation.LocationName;
                                                                                                ccmAssortmentMinorRevisionListActionLocation.Units = gfsAssortmentMinorRevisionListActionLocation.Units;
                                                                                                ccmAssortmentMinorRevisionListActionLocation.Facings = gfsAssortmentMinorRevisionListActionLocation.Facings;

                                                                                                ////Resolved Id's
                                                                                                if (gfsAssortmentMinorRevisionListActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionListActionLocation.LocationCode))
                                                                                                {
                                                                                                    ccmAssortmentMinorRevisionListActionLocation.LocationId = (short?)storeCodeLookup[gfsAssortmentMinorRevisionListActionLocation.LocationCode];
                                                                                                }

                                                                                                dalAssortmentMinorRevisionListActionLocation.Update(ccmAssortmentMinorRevisionListActionLocation);
                                                                                                ccmAssortmentMinorRevisionListActionLocations.Remove(ccmAssortmentMinorRevisionListActionLocation);
                                                                                            }
                                                                                        }

                                                                                        //If and ccmAssortmentMinorRevisionListActionLocationsLeft they have been removed.
                                                                                        //This is the lowest level so can remove without worrying about parents.
                                                                                        foreach (AssortmentMinorRevisionListActionLocationDto assortmentMinorRevisionListActionLocation in ccmAssortmentMinorRevisionListActionLocations)
                                                                                        {
                                                                                            dalAssortmentMinorRevisionListActionLocation.DeleteById(assortmentMinorRevisionListActionLocation.Id);
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionListAction = new AssortmentMinorRevisionListActionDto();

                                                                                        ccmAssortmentMinorRevisionListAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                        ccmAssortmentMinorRevisionListAction.ProductName = gfsAssortmentMinorRevisionListAction.ProductName;
                                                                                        ccmAssortmentMinorRevisionListAction.Priority = gfsAssortmentMinorRevisionListAction.Priority;
                                                                                        ccmAssortmentMinorRevisionListAction.Comments = gfsAssortmentMinorRevisionListAction.Comments;
                                                                                        ccmAssortmentMinorRevisionListAction.ProductGtin = gfsAssortmentMinorRevisionListAction.ProductGtin;

                                                                                        //Resolved Id's
                                                                                        if (gfsAssortmentMinorRevisionListAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionListAction.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionListAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionListAction.ProductGtin];
                                                                                        }

                                                                                        //AssortmentConsumerDecisionTreeNodeId is special case - no gtin, ucr, or any other link. So use the CCM DB field added for this.
                                                                                        if (gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId != null && consumerDecisionTreeNodeLookup.ContainsKey(gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionListAction.AssortmentConsumerDecisionTreeNodeId = consumerDecisionTreeNodeLookup[gfsAssortmentMinorRevisionListAction.ConsumerDecisionTreeNodeId];
                                                                                        }

                                                                                        dalAssortmentMinorRevisonListAction.Insert(ccmAssortmentMinorRevisionListAction);


                                                                                        //Now Insert children
                                                                                        #region Insert AssortmentMinorRevisionListActionLocation

                                                                                        AssortmentMinorRevisionListActionLocationDto ccmAssortmentMinorRevisionListActionLocation = new AssortmentMinorRevisionListActionLocationDto();

                                                                                        //foreach set of locations in each list action
                                                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionListActionLocation gfsAssortmentMinorRevisionListActionLocation in gfsAssortmentMinorRevisionListAction.Locations)
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionListActionLocation = new AssortmentMinorRevisionListActionLocationDto();

                                                                                            ccmAssortmentMinorRevisionListActionLocation.AssortmentMinorRevisionListActionId = ccmAssortmentMinorRevisionListAction.Id;
                                                                                            ccmAssortmentMinorRevisionListActionLocation.LocationCode = gfsAssortmentMinorRevisionListActionLocation.LocationCode;
                                                                                            ccmAssortmentMinorRevisionListActionLocation.LocationName = gfsAssortmentMinorRevisionListActionLocation.LocationName;
                                                                                            ccmAssortmentMinorRevisionListActionLocation.Units = gfsAssortmentMinorRevisionListActionLocation.Units;
                                                                                            ccmAssortmentMinorRevisionListActionLocation.Facings = gfsAssortmentMinorRevisionListActionLocation.Facings;

                                                                                            ////Resolved Id's
                                                                                            if (gfsAssortmentMinorRevisionListActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionListActionLocation.LocationCode))
                                                                                            {
                                                                                                ccmAssortmentMinorRevisionListActionLocation.LocationId = (short?)storeCodeLookup[gfsAssortmentMinorRevisionListActionLocation.LocationCode];
                                                                                            }

                                                                                            dalAssortmentMinorRevisionListActionLocation.Insert(ccmAssortmentMinorRevisionListActionLocation);
                                                                                        }
                                                                                        #endregion
                                                                                    }

                                                                                        #endregion
                                                                                }
                                                                               
                                                                            }

                                                                            //If any ccmProducts remaining, they have been removed.
                                                                            foreach (AssortmentMinorRevisionListActionDto assortmentMinorRevisionListAction in ccmAssortmentMinorRevisionListActions)
                                                                            {
                                                                                dalAssortmentMinorRevisonListAction.DeleteById(assortmentMinorRevisionListAction.Id);
                                                                            }

                                                                            #endregion

                                                                            #region Update AssortmentMinorRevisionDeListAction & AssortmentMinorRevisionDeListActionLocation

                                                                            List<AssortmentMinorRevisionDeListActionDto> ccmAssortmentMinorRevisionDeListActions = dalAssortmentMinorRevisionDeListAction.FetchByAssortmentMinorRevisionId(ccmAssortmentMinorRevision.Id).ToList();
                                                                            AssortmentMinorRevisionDeListActionDto ccmAssortmentMinorRevisionDeListAction;

                                                                            foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionDeListAction gfsAssortmentMinorRevisionDeListAction in gfsAssortmentMinorRevision.DeListActions)
                                                                            {
                                                                                if (gfsAssortmentMinorRevisionDeListAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionDeListAction.ProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionDeListAction = ccmAssortmentMinorRevisionDeListActions.Where(p => p.ProductId == productCodeLookup[gfsAssortmentMinorRevisionDeListAction.ProductGtin]).FirstOrDefault();

                                                                                    if (ccmAssortmentMinorRevisionDeListAction != null)
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionDeListAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                        ccmAssortmentMinorRevisionDeListAction.ProductName = gfsAssortmentMinorRevisionDeListAction.ProductName;
                                                                                        ccmAssortmentMinorRevisionDeListAction.Priority = gfsAssortmentMinorRevisionDeListAction.Priority;
                                                                                        ccmAssortmentMinorRevisionDeListAction.Comments = gfsAssortmentMinorRevisionDeListAction.Comments;
                                                                                        ccmAssortmentMinorRevisionDeListAction.ProductGtin = gfsAssortmentMinorRevisionDeListAction.ProductGtin;

                                                                                        //Resolved Id's
                                                                                        if (gfsAssortmentMinorRevisionDeListAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionDeListAction.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionDeListAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionDeListAction.ProductGtin];
                                                                                        }


                                                                                        dalAssortmentMinorRevisionDeListAction.Update(ccmAssortmentMinorRevisionDeListAction);
                                                                                        ccmAssortmentMinorRevisionDeListActions.Remove(ccmAssortmentMinorRevisionDeListAction);


                                                                                        #region AssortmentMinorRevisionDeListActionLocation (Child update sync for children)
                                                                                        //Now ensure its child is updated

                                                                                        List<AssortmentMinorRevisionDeListActionLocationDto> ccmAssortmentMinorRevisionDeListActionLocations = dalAssortmentMinorRevisionDeListActionLocation.FetchByAssortmentMinorRevisionDeListActionId(ccmAssortmentMinorRevisionDeListAction.Id).ToList();
                                                                                        AssortmentMinorRevisionDeListActionLocationDto ccmAssortmentMinorRevisionDeListActionLocation;

                                                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionDeListActionLocation gfsAssortmentMinorRevisionDeListActionLocation in gfsAssortmentMinorRevisionDeListAction.Locations)
                                                                                        {
                                                                                            //We link on code as only one location code per MRListAction structure
                                                                                            ccmAssortmentMinorRevisionDeListActionLocation = ccmAssortmentMinorRevisionDeListActionLocations.Where(p => p.LocationCode == gfsAssortmentMinorRevisionDeListActionLocation.LocationCode).FirstOrDefault();

                                                                                            if (ccmAssortmentMinorRevisionDeListActionLocation != null)
                                                                                            {
                                                                                                ccmAssortmentMinorRevisionDeListActionLocation.AssortmentMinorRevisionDeListActionId = ccmAssortmentMinorRevisionDeListAction.Id;
                                                                                                ccmAssortmentMinorRevisionDeListActionLocation.LocationCode = gfsAssortmentMinorRevisionDeListActionLocation.LocationCode;
                                                                                                ccmAssortmentMinorRevisionDeListActionLocation.LocationName = gfsAssortmentMinorRevisionDeListActionLocation.LocationName;


                                                                                                ////Resolved Id's
                                                                                                if (gfsAssortmentMinorRevisionDeListActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionDeListActionLocation.LocationCode))
                                                                                                {
                                                                                                    ccmAssortmentMinorRevisionDeListActionLocation.LocationId = (short?)storeCodeLookup[gfsAssortmentMinorRevisionDeListActionLocation.LocationCode];
                                                                                                }

                                                                                                dalAssortmentMinorRevisionDeListActionLocation.Update(ccmAssortmentMinorRevisionDeListActionLocation);
                                                                                                ccmAssortmentMinorRevisionDeListActionLocations.Remove(ccmAssortmentMinorRevisionDeListActionLocation);
                                                                                            }
                                                                                        }

                                                                                        //If and ccmAssortmentMinorRevisionListActionLocationsLeft they have been removed.
                                                                                        //This is the lowest level so can remove without worrying about parents.
                                                                                        foreach (AssortmentMinorRevisionDeListActionLocationDto assortmentMinorRevisionDeListActionLocation in ccmAssortmentMinorRevisionDeListActionLocations)
                                                                                        {
                                                                                            dalAssortmentMinorRevisionDeListActionLocation.DeleteById(assortmentMinorRevisionDeListActionLocation.Id);
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionDeListAction = new AssortmentMinorRevisionDeListActionDto();

                                                                                        ccmAssortmentMinorRevisionDeListAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                        ccmAssortmentMinorRevisionDeListAction.ProductName = gfsAssortmentMinorRevisionDeListAction.ProductName;
                                                                                        ccmAssortmentMinorRevisionDeListAction.Priority = gfsAssortmentMinorRevisionDeListAction.Priority;
                                                                                        ccmAssortmentMinorRevisionDeListAction.Comments = gfsAssortmentMinorRevisionDeListAction.Comments;
                                                                                        ccmAssortmentMinorRevisionDeListAction.ProductGtin = gfsAssortmentMinorRevisionDeListAction.ProductGtin;

                                                                                        //Resolved Id's
                                                                                        if (gfsAssortmentMinorRevisionDeListAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionDeListAction.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionDeListAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionDeListAction.ProductGtin];
                                                                                        }

                                                                                        dalAssortmentMinorRevisionDeListAction.Insert(ccmAssortmentMinorRevisionDeListAction);


                                                                                        //Now insert children....
                                                                                        #region AssortmentMinorRevisionDeListActionLocationDto

                                                                                        AssortmentMinorRevisionDeListActionLocationDto ccmAssortmentMinorRevisionDeListActionLocation = new AssortmentMinorRevisionDeListActionLocationDto();

                                                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionDeListActionLocation gfsAssortmentMinorRevisionDeListActionLocation in gfsAssortmentMinorRevisionDeListAction.Locations)
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionDeListActionLocation = new AssortmentMinorRevisionDeListActionLocationDto();

                                                                                            ccmAssortmentMinorRevisionDeListActionLocation.AssortmentMinorRevisionDeListActionId = ccmAssortmentMinorRevisionDeListAction.Id;
                                                                                            ccmAssortmentMinorRevisionDeListActionLocation.LocationCode = gfsAssortmentMinorRevisionDeListActionLocation.LocationCode;
                                                                                            ccmAssortmentMinorRevisionDeListActionLocation.LocationName = gfsAssortmentMinorRevisionDeListActionLocation.LocationName;


                                                                                            ////Resolved Id's
                                                                                            if (gfsAssortmentMinorRevisionDeListActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionDeListActionLocation.LocationCode))
                                                                                            {
                                                                                                ccmAssortmentMinorRevisionDeListActionLocation.LocationId = (short?)storeCodeLookup[gfsAssortmentMinorRevisionDeListActionLocation.LocationCode];
                                                                                            }

                                                                                            dalAssortmentMinorRevisionDeListActionLocation.Insert(ccmAssortmentMinorRevisionDeListActionLocation);
                                                                                        }

                                                                                        #endregion
                                                                                    }



                                                                                    #endregion
                                                                                }
                                                                            }

                                                                            //If any ccmProducts remaining, they have been removed.
                                                                            foreach (AssortmentMinorRevisionDeListActionDto assortmentMinorRevisionDeListAction in ccmAssortmentMinorRevisionDeListActions)
                                                                            {
                                                                                dalAssortmentMinorRevisionDeListAction.DeleteById(assortmentMinorRevisionDeListAction.Id);
                                                                            }

                                                                            #endregion

                                                                            #region Update AssortmentMinorRevisionAmendDistributionAction & AssortmentMinorRevisionAmendDistributionActionLocation

                                                                            List<AssortmentMinorRevisionAmendDistributionActionDto> ccmAssortmentMinorRevisionAmendDistributionActions = dalAssortmentMinorRevisionAmendDistributionAction.FetchByAssortmentMinorRevisionId(ccmAssortmentMinorRevision.Id).ToList();
                                                                            AssortmentMinorRevisionAmendDistributionActionDto ccmAssortmentMinorRevisionAmendDistributionAction;

                                                                            foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionAmendDistributionAction gfsAssortmentMinorRevisionAmendDistributionAction in gfsAssortmentMinorRevision.AmendDistributionActions)
                                                                            {
                                                                                if (gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionAmendDistributionAction = ccmAssortmentMinorRevisionAmendDistributionActions.Where(p => p.ProductId == productCodeLookup[gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin]).FirstOrDefault();

                                                                                    if (ccmAssortmentMinorRevisionAmendDistributionAction != null)
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.ProductName = gfsAssortmentMinorRevisionAmendDistributionAction.ProductName;
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.Priority = gfsAssortmentMinorRevisionAmendDistributionAction.Priority;
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.Comments = gfsAssortmentMinorRevisionAmendDistributionAction.Comments;
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.ProductGtin = gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin;

                                                                                        //Resolved Id's
                                                                                        if (gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionAmendDistributionAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin];
                                                                                        }

                                                                                        dalAssortmentMinorRevisionAmendDistributionAction.Update(ccmAssortmentMinorRevisionAmendDistributionAction);
                                                                                        ccmAssortmentMinorRevisionAmendDistributionActions.Remove(ccmAssortmentMinorRevisionAmendDistributionAction);


                                                                                        #region AssortmentMinorRevisionAmendDistributionActionLocation (Child update sync for children)
                                                                                        //Now ensure its children are updated

                                                                                        List<AssortmentMinorRevisionAmendDistributionActionLocationDto> ccmAssortmentMinorRevisionAmendDistributionActionLocations = dalAssortmentMinorRevisionAmendDistributionActionLocation.FetchByAssortmentMinorRevisionAmendDistributionActionId(ccmAssortmentMinorRevisionAmendDistributionAction.Id).ToList();
                                                                                        AssortmentMinorRevisionAmendDistributionActionLocationDto ccmAssortmentMinorRevisionAmendDistributionActionLocation;

                                                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionAmendDistributionActionLocation gfsAssortmentMinorRevisionAmendDistributionActionLocation in gfsAssortmentMinorRevisionAmendDistributionAction.Locations)
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionAmendDistributionActionLocation = ccmAssortmentMinorRevisionAmendDistributionActionLocations.Where(p => p.LocationCode == gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode).FirstOrDefault();

                                                                                            if (ccmAssortmentMinorRevisionAmendDistributionActionLocation != null)
                                                                                            {
                                                                                                ccmAssortmentMinorRevisionAmendDistributionActionLocation.AssortmentMinorRevisionAmendDistributionActionId = ccmAssortmentMinorRevisionAmendDistributionAction.Id;
                                                                                                ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode = gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode;
                                                                                                ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationName = gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationName;
                                                                                                ccmAssortmentMinorRevisionAmendDistributionActionLocation.Units = gfsAssortmentMinorRevisionAmendDistributionActionLocation.Units;
                                                                                                ccmAssortmentMinorRevisionAmendDistributionActionLocation.Facings = gfsAssortmentMinorRevisionAmendDistributionActionLocation.Facings;

                                                                                                //Resolved Id's
                                                                                                if (gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode))
                                                                                                {
                                                                                                    ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationId = storeCodeLookup[gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode];
                                                                                                }

                                                                                                dalAssortmentMinorRevisionAmendDistributionActionLocation.Update(ccmAssortmentMinorRevisionAmendDistributionActionLocation);
                                                                                                ccmAssortmentMinorRevisionAmendDistributionActionLocations.Remove(ccmAssortmentMinorRevisionAmendDistributionActionLocation);
                                                                                            }
                                                                                        }

                                                                                        //If ccmAssortmentMinorRevisionListActionLocations Left they have been removed.
                                                                                        //This is the lowest level so can remove without worrying about parents.
                                                                                        foreach (AssortmentMinorRevisionAmendDistributionActionLocationDto assortmentMinorRevisionAmendDistributionActionLocation in ccmAssortmentMinorRevisionAmendDistributionActionLocations)
                                                                                        {
                                                                                            dalAssortmentMinorRevisionAmendDistributionActionLocation.DeleteById(assortmentMinorRevisionAmendDistributionActionLocation.Id);
                                                                                        }
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction = new AssortmentMinorRevisionAmendDistributionActionDto();

                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.ProductName = gfsAssortmentMinorRevisionAmendDistributionAction.ProductName;
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.Priority = gfsAssortmentMinorRevisionAmendDistributionAction.Priority;
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.Comments = gfsAssortmentMinorRevisionAmendDistributionAction.Comments;
                                                                                        ccmAssortmentMinorRevisionAmendDistributionAction.ProductGtin = gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin;

                                                                                        //Resolved Id's
                                                                                        if (gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionAmendDistributionAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionAmendDistributionAction.ProductGtin];
                                                                                        }

                                                                                        dalAssortmentMinorRevisionAmendDistributionAction.Insert(ccmAssortmentMinorRevisionAmendDistributionAction);

                                                                                        //Insert children....
                                                                                        #region Insert AssortmentMinorRevisionAmendDistributionActionLocation

                                                                                        AssortmentMinorRevisionAmendDistributionActionLocationDto ccmAssortmentMinorRevisionAmendDistributionActionLocation = new AssortmentMinorRevisionAmendDistributionActionLocationDto();

                                                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionAmendDistributionActionLocation gfsAssortmentMinorRevisionAmendDistributionActionLocation in gfsAssortmentMinorRevisionAmendDistributionAction.Locations)
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionAmendDistributionActionLocation = new AssortmentMinorRevisionAmendDistributionActionLocationDto();

                                                                                            ccmAssortmentMinorRevisionAmendDistributionActionLocation.AssortmentMinorRevisionAmendDistributionActionId = ccmAssortmentMinorRevisionAmendDistributionAction.Id;
                                                                                            ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode = gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode;
                                                                                            ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationName = gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationName;
                                                                                            ccmAssortmentMinorRevisionAmendDistributionActionLocation.Units = gfsAssortmentMinorRevisionAmendDistributionActionLocation.Units;
                                                                                            ccmAssortmentMinorRevisionAmendDistributionActionLocation.Facings = gfsAssortmentMinorRevisionAmendDistributionActionLocation.Facings;

                                                                                            //Resolved Id's
                                                                                            if (gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode))
                                                                                            {
                                                                                                ccmAssortmentMinorRevisionAmendDistributionActionLocation.LocationId = storeCodeLookup[gfsAssortmentMinorRevisionAmendDistributionActionLocation.LocationCode];
                                                                                            }

                                                                                            dalAssortmentMinorRevisionAmendDistributionActionLocation.Insert(ccmAssortmentMinorRevisionAmendDistributionActionLocation);
                                                                                        }


                                                                                        #endregion
                                                                                    }

                                                                                    #endregion
                                                                                }
                                                                            }

                                                                            //If any ccmProducts remaining, they have been removed.
                                                                            foreach (AssortmentMinorRevisionAmendDistributionActionDto assortmentMinorRevisionAmendDistributionAction in ccmAssortmentMinorRevisionAmendDistributionActions)
                                                                            {
                                                                                dalAssortmentMinorRevisionAmendDistributionAction.DeleteById(assortmentMinorRevisionAmendDistributionAction.Id);
                                                                            }

                                                                            #endregion

                                                                            #region Update AssortmentMinorRevisionReplaceAction & AssortmentMinorRevisionReplaceActionLocation

                                                                            List<AssortmentMinorRevisionReplaceActionDto> ccmAssortmentMinorRevisionReplaceActions = dalAssortmentMinorRevisionReplaceAction.FetchByAssortmentMinorRevisionId(ccmAssortmentMinorRevision.Id).ToList();
                                                                            AssortmentMinorRevisionReplaceActionDto ccmAssortmentMinorRevisionReplaceAction;

                                                                            foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionReplaceAction gfsAssortmentMinorRevisionReplaceAction in gfsAssortmentMinorRevision.ReplaceActions)
                                                                            {
                                                                                if (gfsAssortmentMinorRevisionReplaceAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceAction.ProductGtin))
                                                                                {
                                                                                    ccmAssortmentMinorRevisionReplaceAction = ccmAssortmentMinorRevisionReplaceActions.Where(p => p.ProductId == productCodeLookup[gfsAssortmentMinorRevisionReplaceAction.ProductGtin]).FirstOrDefault();

                                                                                    if (ccmAssortmentMinorRevisionReplaceAction != null)
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionReplaceAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.ProductGtin = gfsAssortmentMinorRevisionReplaceAction.ProductGtin;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.ProductName = gfsAssortmentMinorRevisionReplaceAction.ProductName;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.ReplacementProductGtin = gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.ReplacementProductName = gfsAssortmentMinorRevisionReplaceAction.ReplacementProductName;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.Priority = gfsAssortmentMinorRevisionReplaceAction.Priority;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.Comments = gfsAssortmentMinorRevisionReplaceAction.Comments;

                                                                                        //Resolved Id's
                                                                                        if (gfsAssortmentMinorRevisionReplaceAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceAction.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionReplaceAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionReplaceAction.ProductGtin];
                                                                                        }

                                                                                        if (gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionReplaceAction.ReplacementProductId = productCodeLookup[gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin];
                                                                                        }

                                                                                        dalAssortmentMinorRevisionReplaceAction.Update(ccmAssortmentMinorRevisionReplaceAction);
                                                                                        ccmAssortmentMinorRevisionReplaceActions.Remove(ccmAssortmentMinorRevisionReplaceAction);


                                                                                        //Update Children...
                                                                                        #region AssortmentMinorRevisionReplaceActionLocation (Child update sync for children)
                                                                                        //Now ensure its children are updated

                                                                                        List<AssortmentMinorRevisionReplaceActionLocationDto> ccmAssortmentMinorRevisionReplaceActionLocations = dalAssortmentMinorRevisionReplaceActionLocation.FetchByAssortmentMinorRevisionReplaceActionId(ccmAssortmentMinorRevisionReplaceAction.Id).ToList();
                                                                                        AssortmentMinorRevisionReplaceActionLocationDto ccmAssortmentMinorRevisionReplaceActionLocation;

                                                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionReplaceActionLocation gfsAssortmentMinorRevisionReplaceActionLocation in gfsAssortmentMinorRevisionReplaceAction.Locations)
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionReplaceActionLocation = ccmAssortmentMinorRevisionReplaceActionLocations.Where(p => p.LocationCode == gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode).FirstOrDefault();

                                                                                            if (ccmAssortmentMinorRevisionReplaceActionLocation != null)
                                                                                            {
                                                                                                ccmAssortmentMinorRevisionReplaceActionLocation.AssortmentMinorRevisionReplaceActionId = ccmAssortmentMinorRevisionReplaceAction.Id;
                                                                                                ccmAssortmentMinorRevisionReplaceActionLocation.LocationCode = gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode;
                                                                                                ccmAssortmentMinorRevisionReplaceActionLocation.LocationName = gfsAssortmentMinorRevisionReplaceActionLocation.LocationName;
                                                                                                ccmAssortmentMinorRevisionReplaceActionLocation.Units = gfsAssortmentMinorRevisionReplaceActionLocation.Units;
                                                                                                ccmAssortmentMinorRevisionReplaceActionLocation.Facings = gfsAssortmentMinorRevisionReplaceActionLocation.Facings;

                                                                                                //Resolved Id's
                                                                                                if (gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode))
                                                                                                {
                                                                                                    ccmAssortmentMinorRevisionReplaceActionLocation.LocationId = storeCodeLookup[gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode];
                                                                                                }

                                                                                                dalAssortmentMinorRevisionReplaceActionLocation.Update(ccmAssortmentMinorRevisionReplaceActionLocation);
                                                                                                ccmAssortmentMinorRevisionReplaceActionLocations.Remove(ccmAssortmentMinorRevisionReplaceActionLocation);
                                                                                            }
                                                                                        }

                                                                                        //If and ccmAssortmentMinorRevisionListActionLocationsLeft they have been removed.
                                                                                        //This is the lowest level so can remove without worrying about parents.
                                                                                        foreach (AssortmentMinorRevisionReplaceActionLocationDto assortmentMinorRevisionReplaceActionLocation in ccmAssortmentMinorRevisionReplaceActionLocations)
                                                                                        {
                                                                                            dalAssortmentMinorRevisionReplaceActionLocation.DeleteById(assortmentMinorRevisionReplaceActionLocation.Id);
                                                                                        }
                                                                                        #endregion
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        ccmAssortmentMinorRevisionReplaceAction = new AssortmentMinorRevisionReplaceActionDto();

                                                                                        ccmAssortmentMinorRevisionReplaceAction.AssortmentMinorRevisionId = ccmAssortmentMinorRevision.Id;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.ProductGtin = gfsAssortmentMinorRevisionReplaceAction.ProductGtin;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.ProductName = gfsAssortmentMinorRevisionReplaceAction.ProductName;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.ReplacementProductGtin = gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.ReplacementProductName = gfsAssortmentMinorRevisionReplaceAction.ReplacementProductName;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.Priority = gfsAssortmentMinorRevisionReplaceAction.Priority;
                                                                                        ccmAssortmentMinorRevisionReplaceAction.Comments = gfsAssortmentMinorRevisionReplaceAction.Comments;


                                                                                        //Resolved Id's
                                                                                        if (gfsAssortmentMinorRevisionReplaceAction.ProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceAction.ProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionReplaceAction.ProductId = productCodeLookup[gfsAssortmentMinorRevisionReplaceAction.ProductGtin];
                                                                                        }

                                                                                        if (gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin != null && productCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin))
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionReplaceAction.ReplacementProductId = productCodeLookup[gfsAssortmentMinorRevisionReplaceAction.ReplacementProductGtin];
                                                                                        }

                                                                                        dalAssortmentMinorRevisionReplaceAction.Insert(ccmAssortmentMinorRevisionReplaceAction);


                                                                                        //Add children.....
                                                                                        #region Insert AssortmentMinorRevisionReplaceActionLocation

                                                                                        AssortmentMinorRevisionReplaceActionLocationDto ccmAssortmentMinorRevisionReplaceActionLocation = new AssortmentMinorRevisionReplaceActionLocationDto();

                                                                                        foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionReplaceActionLocation gfsAssortmentMinorRevisionReplaceActionLocation in gfsAssortmentMinorRevisionReplaceAction.Locations)
                                                                                        {
                                                                                            ccmAssortmentMinorRevisionReplaceActionLocation = new AssortmentMinorRevisionReplaceActionLocationDto();

                                                                                            ccmAssortmentMinorRevisionReplaceActionLocation.AssortmentMinorRevisionReplaceActionId = ccmAssortmentMinorRevisionReplaceAction.Id;
                                                                                            ccmAssortmentMinorRevisionReplaceActionLocation.LocationCode = gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode;
                                                                                            ccmAssortmentMinorRevisionReplaceActionLocation.LocationName = gfsAssortmentMinorRevisionReplaceActionLocation.LocationName;
                                                                                            ccmAssortmentMinorRevisionReplaceActionLocation.Units = gfsAssortmentMinorRevisionReplaceActionLocation.Units;
                                                                                            ccmAssortmentMinorRevisionReplaceActionLocation.Facings = gfsAssortmentMinorRevisionReplaceActionLocation.Facings;

                                                                                            //Resolved Id's
                                                                                            if (gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode != null && storeCodeLookup.ContainsKey(gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode))
                                                                                            {
                                                                                                ccmAssortmentMinorRevisionReplaceActionLocation.LocationId = storeCodeLookup[gfsAssortmentMinorRevisionReplaceActionLocation.LocationCode];
                                                                                            }

                                                                                            dalAssortmentMinorRevisionReplaceActionLocation.Insert(ccmAssortmentMinorRevisionReplaceActionLocation);
                                                                                        }

                                                                                        #endregion
                                                                                    }
                                                                            

                                                                                }
                                                                            }

                                                                            //If any ccmProducts remaining, they have been removed.
                                                                            foreach (AssortmentMinorRevisionReplaceActionDto assortmentMinorRevisionReplaceAction in ccmAssortmentMinorRevisionReplaceActions)
                                                                            {
                                                                                dalAssortmentMinorRevisionReplaceAction.DeleteById(assortmentMinorRevisionReplaceAction.Id);
                                                                            }

                                                                            #endregion
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Assortment], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Syncrhonize Deleted Assortments

        private void SynchronizeAssortmentMinorRevisionsDeleted(ProcessContext context)
        {
            try
            {
                using (IAssortmentMinorRevisionInfoDal dalInfo = context.DalContext.GetDal<IAssortmentMinorRevisionInfoDal>())
                {
                    using (IAssortmentMinorRevisionDal dal = context.DalContext.GetDal<IAssortmentMinorRevisionDal>())
                    {
                        #region old delete
                        //ObservableCollection<Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo> gfsAssortmentMinorRevisionsDeleted = null;

                        //if (SyncTarget.LastSync != null)
                        //{
                        //    context.DoWorkWithRetries(delegate()
                        //    {
                        //        GetAssortmentMinorRevisionInfoByEntityIdDateDeletedRequest assortmentMinorRevisionByEntityIdDateDeleted = new GetAssortmentMinorRevisionInfoByEntityIdDateDeletedRequest(this.GFSEntityId, SyncTarget.LastSync.Value);
                        //        gfsAssortmentMinorRevisionsDeleted = new ObservableCollection<Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo>(context.Services.AssortmentMinorRevisionServiceClient.GetAssortmentMinorRevisionInfoByEntityIdDateDeleted(assortmentMinorRevisionByEntityIdDateDeleted).AssortmentMinorRevisionInfos);
                        //    });


                        //    IEnumerable<AssortmentMinorRevisionInfoDto> ccmAssortmentMinorRevisions;
                        //    using (IAssortmentMinorRevisionInfoDal dalAssortmentMinorRevisionInfo = context.DalContext.GetDal<IAssortmentMinorRevisionInfoDal>())
                        //    {
                        //        ccmAssortmentMinorRevisions = dalAssortmentMinorRevisionInfo.FetchByEntityId(this.EntityId);
                        //    }

                        //    // loop through the assortment minor revisions to delete
                        //    foreach (Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo gfsAssortmentMinorRevision in gfsAssortmentMinorRevisionsDeleted)
                        //    {
                        //        // check if assortment minor revision exists in CCM
                        //        AssortmentMinorRevisionInfoDto ccmAssortmentMinorRevision = ccmAssortmentMinorRevisions.FirstOrDefault(i => i.UniqueContentReference == gfsAssortmentMinorRevision.UniqueContentReference);

                        //        if (ccmAssortmentMinorRevision != null)
                        //        {
                        //            dal.DeleteById(ccmAssortmentMinorRevision.Id);
                        //        }
                        //    }
                        //}
                        #endregion

                        List<Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo> gfsAssortmentMinorRevisionDeletedList = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsAssortmentMinorRevisionDeletedList = context.Services.AssortmentMinorRevisionServiceClient.GetAssortmentMinorRevisionInfoByEntityId(
                                new Services.AssortmentMinorRevision.GetAssortmentMinorRevisionInfoByEntityIdRequest(this.GFSEntityId)).AssortmentMinorRevisionInfos;
                        });

                        //fetch ccm universes
                        IEnumerable<AssortmentMinorRevisionInfoDto> ccmAssortmentMinorRevisionInfoList = dalInfo.FetchByEntityId(this.EntityId);

                        //loop through ccm product universes
                        foreach (AssortmentMinorRevisionInfoDto ccmAssortmentMinorRevision in ccmAssortmentMinorRevisionInfoList)
                        {
                            //came from a sync
                            if (ccmAssortmentMinorRevision.ParentUniqueContentReference != null)
                            {
                                //try to find the gfs assortment minor revision
                                Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo gfsContent = gfsAssortmentMinorRevisionDeletedList.FirstOrDefault(u => u.UniqueContentReference == ccmAssortmentMinorRevision.UniqueContentReference);

                                //doesn't exist in gfs so delete it
                                if (gfsContent == null)
                                {
                                    dal.DeleteById(ccmAssortmentMinorRevision.Id);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, "Assortment Minor Revision Delete", ex);

                // throw exception back up the stack
                throw;
            }
        }

        #endregion

        #region update ucrs

        #region Product Universe Update UCRs

        private void SynchroniseProductUniverseUcrs(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IProductUniverseDal dalProductUniverse = context.DalContext.GetDal<IProductUniverseDal>())
                {
                    using (IProductUniverseProductDal dalProductUniverseProduct = context.DalContext.GetDal<IProductUniverseProductDal>())
                    {
                        List<Services.Product.ProductUniverseInfo> gfsProductUniverseInfoList = null;

                        //fetch all universes from gfs
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsProductUniverseInfoList = context.Services.ProductServiceClient.GetProductUniverseInfoByEntityId(
                                new Services.Product.GetProductUniverseInfoByEntityIdRequest(this.GFSEntityId)).ProductUniverseInfos;
                        });

                        //Fetch all product universes from ccm
                        IEnumerable<ProductUniverseDto> ccmProductUniverseList = dalProductUniverse.FetchByEntityId(this.EntityId).Where(c => c.ParentUniqueContentReference == null);

                        //loop through ccm product universes
                        foreach (ProductUniverseDto ccmProductUniverse in ccmProductUniverseList)
                        {
                            //get the product universe from GFS list
                            Services.Product.ProductUniverseInfo gfsProductUniverseInfo = gfsProductUniverseInfoList.FirstOrDefault(u => u.UniqueContentReference == ccmProductUniverse.UniqueContentReference);

                            if (gfsProductUniverseInfo != null)
                            {
                                //ok we've found the gfs universe
                                //populate the parent version
                                if (gfsProductUniverseInfo.ParentUniqueContentReference != null)
                                {
                                    ccmProductUniverse.ParentUniqueContentReference = gfsProductUniverseInfo.ParentUniqueContentReference;
                                }
                                else//first version
                                {
                                    //otherwise the PUCR is the same as content's ucr
                                    ccmProductUniverse.ParentUniqueContentReference = gfsProductUniverseInfo.UniqueContentReference;
                                }

                                ccmProductUniverse.DateLastModified = DateTime.Now;
                                dalProductUniverse.Update(ccmProductUniverse);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.ProductUnivese], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Consumer Decision Tree UCRs

        private void SynchroniseCDTUcrs(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IConsumerDecisionTreeInfoDal dalCdtInfo = context.DalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                {
                    using (IConsumerDecisionTreeDal dalCdt = context.DalContext.GetDal<IConsumerDecisionTreeDal>())
                    {
                        List<Services.ConsumerDecisionTree.ConsumerDecisionTreeInfo> gfsConsumerDecisionTreeInfoList = null;

                        //fetch all gfs cdts
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsConsumerDecisionTreeInfoList = context.Services.ConsumerDecisionTreeServiceClient.GetConsumerDecisionTreeInfoByEntityName(
                                new Services.ConsumerDecisionTree.GetConsumerDecisionTreeInfoByEntityNameRequest(this.EntityName)).ConsumerDecisionTreeInfos;
                        });

                        //Fetch all product universes from gfs without parent ucrs
                        IEnumerable<ConsumerDecisionTreeInfoDto> ccmConsumerDecisionTreeList = dalCdtInfo.FetchByEntityId(this.EntityId).Where(c => c.ParentUniqueContentReference == null);

                        //loop through ccm product universes
                        foreach (ConsumerDecisionTreeInfoDto ccmConsumerDecisionTreeInfoDto in ccmConsumerDecisionTreeList)
                        {
                            ConsumerDecisionTreeDto ccmConsumerDecisionTreeDto = dalCdt.FetchById(ccmConsumerDecisionTreeInfoDto.Id);

                            if (ccmConsumerDecisionTreeDto != null)
                            {
                                //get the product universe from GFS list
                                Services.ConsumerDecisionTree.ConsumerDecisionTreeInfo gfsConsumerDecisionTreeInfo = gfsConsumerDecisionTreeInfoList.FirstOrDefault(u => u.UniqueContentReference == ccmConsumerDecisionTreeInfoDto.UniqueContentReference);

                                if (gfsConsumerDecisionTreeInfo != null)
                                {
                                    //ok we've found the gfs cdt
                                    //populate the parent version
                                    if (gfsConsumerDecisionTreeInfo.ParentUniqueContentReference != null)
                                    {
                                        ccmConsumerDecisionTreeDto.ParentUniqueContentReference = gfsConsumerDecisionTreeInfo.ParentUniqueContentReference;
                                    }
                                    else//first version
                                    {
                                        //otherwise the PUCR is the same as content's ucr
                                        ccmConsumerDecisionTreeDto.ParentUniqueContentReference = gfsConsumerDecisionTreeInfo.UniqueContentReference;
                                    }

                                    ccmConsumerDecisionTreeDto.DateLastModified = DateTime.Now;
                                    dalCdt.Update(ccmConsumerDecisionTreeDto);
                                }
                                
                            }
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.CDT], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Location Space UCRs

        private void SynchroniseLocationSpaceUcrs(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (ILocationSpaceInfoDal dalCdtInfo = context.DalContext.GetDal<ILocationSpaceInfoDal>())
                {
                    using (ILocationSpaceDal dalCdt = context.DalContext.GetDal<ILocationSpaceDal>())
                    {
                        List<Services.LocationSpace.LocationSpaceInfo> gfsLocationSpaceInfoList = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsLocationSpaceInfoList = context.Services.LocationSpaceServiceClient.GetLocationSpaceInfoByEntityName(
                                new Services.LocationSpace.GetLocationSpaceInfoByEntityNameRequest(this.EntityName)).LocationSpaceInfos;
                        });

                        //Fetch all product universes from ccm
                        IEnumerable<LocationSpaceInfoDto> ccmLocationSpaceList = dalCdtInfo.FetchByEntityId(this.EntityId).Where(c => c.ParentUniqueContentReference == null);

                        //loop through ccm product universes
                        foreach (LocationSpaceInfoDto ccmLocationSpaceInfo in ccmLocationSpaceList)
                        {
                            LocationSpaceDto ccmLocationSpaceDto = dalCdt.FetchById(ccmLocationSpaceInfo.Id);

                            if (ccmLocationSpaceDto != null)
                            {
                                //get the product universe from GFS list
                                Services.LocationSpace.LocationSpaceInfo gfsLocationSpace = gfsLocationSpaceInfoList.FirstOrDefault(u => u.UniqueContentReference == ccmLocationSpaceInfo.UniqueContentReference);

                                if (gfsLocationSpace != null)
                                {
                                    //ok we've found the gfs universe
                                    //populate the parent version
                                    if (gfsLocationSpace.ParentUniqueContentReference != null)
                                    {
                                        ccmLocationSpaceDto.ParentUniqueContentReference = gfsLocationSpace.ParentUniqueContentReference;
                                    }
                                    else//first version
                                    {
                                        //otherwise the PUCR is the same as content's ucr
                                        ccmLocationSpaceDto.ParentUniqueContentReference = gfsLocationSpace.UniqueContentReference;
                                    }

                                    ccmLocationSpaceDto.DateLastModified = DateTime.Now;
                                    dalCdt.Update(ccmLocationSpaceDto);
                                }
                            }
                        }
                        
                    }
                }

            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.LocationSpace], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Cluster Scheme UCRs

        private void SynchroniseClusterSchemeUcrs(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IClusterSchemeInfoDal dalCdtInfo = context.DalContext.GetDal<IClusterSchemeInfoDal>())
                {
                    using (IClusterSchemeDal dalCdt = context.DalContext.GetDal<IClusterSchemeDal>())
                    {
                        List<Services.Cluster.ClusterSchemeInfo> gfsClusterSchemeInfoList = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsClusterSchemeInfoList = context.Services.ClusterServiceClient.GetClusterSchemeInfoByEntityId(
                                       new GetClusterSchemeInfoByEntityIdRequest(this.GFSEntityId)).ClusterSchemeInfos;
                        });

                        //Fetch all product universes
                        IEnumerable<ClusterSchemeInfoDto> ccmClusterSchemeInfoList = dalCdtInfo.FetchByEntityId(this.EntityId).Where(c => c.ParentUniqueContentReference == null);

                        //loop through ccm product universes
                        foreach (ClusterSchemeInfoDto ccmClusterSchemeInfo in ccmClusterSchemeInfoList)
                        {
                            ClusterSchemeDto ccmClusterSchemeDto = dalCdt.FetchById(ccmClusterSchemeInfo.Id);

                            if (ccmClusterSchemeDto != null)
                            {
                                //get the product universe from GFS list
                                Services.Cluster.ClusterSchemeInfo gfsClusterSchemeInfo = gfsClusterSchemeInfoList.FirstOrDefault(u => u.UniqueContentReference == ccmClusterSchemeInfo.UniqueContentReference);

                                if (gfsClusterSchemeInfo != null)
                                {
                                    //ok we've found the gfs universe
                                    //populate the parent version
                                    if (gfsClusterSchemeInfo.ParentUniqueContentReference != null)
                                    {
                                        ccmClusterSchemeDto.ParentUniqueContentReference = gfsClusterSchemeInfo.ParentUniqueContentReference;
                                    }
                                    else//first version
                                    {
                                        //otherwise the PUCR is the same as content's ucr
                                        ccmClusterSchemeDto.ParentUniqueContentReference = gfsClusterSchemeInfo.UniqueContentReference;
                                    }

                                    ccmClusterSchemeDto.DateLastModified = DateTime.Now;
                                    dalCdt.Update(ccmClusterSchemeDto);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Cluster], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Assortment UCRs

        private void SynchroniseAssortmentUcrs(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IAssortmentInfoDal dalCdtInfo = context.DalContext.GetDal<IAssortmentInfoDal>())
                {
                    using (IAssortmentDal dalCdt = context.DalContext.GetDal<IAssortmentDal>())
                    {
                        List<Services.Assortment.AssortmentInfo> gfsAssortmentInfoList = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsAssortmentInfoList = context.Services.AssortmentServiceClient.GetAssortmentInfoByEntityId(
                                new GetAssortmentInfoByEntityIdRequest(this.GFSEntityId)).AssortmentInfos;
                        });

                        //Fetch all product universes
                        IEnumerable<AssortmentInfoDto> ccmAssortmentList = dalCdtInfo.FetchByEntityId(this.EntityId).Where(c => c.ParentUniqueContentReference == null);

                        //loop through ccm product universes
                        foreach (AssortmentInfoDto ccmAssortmentInfo in ccmAssortmentList)
                        {
                            AssortmentDto ccmAssortmentDto = dalCdt.FetchById(ccmAssortmentInfo.Id);

                            if (ccmAssortmentDto != null)
                            {
                                //get the product universe from GFS list
                                Services.Assortment.AssortmentInfo gfsUniverse = gfsAssortmentInfoList.FirstOrDefault(u => u.UniqueContentReference == ccmAssortmentInfo.UniqueContentReference);

                                if (gfsUniverse != null)
                                {
                                    //ok we've found the gfs universe
                                    //populate the parent version
                                    if (gfsUniverse.ParentUniqueContentReference != null)
                                    {
                                        ccmAssortmentDto.ParentUniqueContentReference = gfsUniverse.ParentUniqueContentReference;
                                    }
                                    else//first version
                                    {
                                        //otherwise the PUCR is the same as content's ucr
                                        ccmAssortmentDto.ParentUniqueContentReference = gfsUniverse.UniqueContentReference;
                                    }

                                    ccmAssortmentDto.DateLastModified = DateTime.Now;
                                    dalCdt.Update(ccmAssortmentDto);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Assortment], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #region Assortment Minor Revision UCRs

        private void SynchroniseAssortmentMinorRevisionUcrs(ProcessContext context)
        {
            //Start the transaction
            context.DalContext.Begin();

            try
            {
                using (IAssortmentMinorRevisionInfoDal dalCdtInfo = context.DalContext.GetDal<IAssortmentMinorRevisionInfoDal>())
                {
                    using (IAssortmentMinorRevisionDal dalCdt = context.DalContext.GetDal<IAssortmentMinorRevisionDal>())
                    {
                        List<Services.AssortmentMinorRevision.AssortmentMinorRevisionInfo> gfsAssortmentMinorRevisionInfoList = null;

                        //fetch all universes
                        context.DoWorkWithRetries(delegate()
                        {
                            gfsAssortmentMinorRevisionInfoList = context.Services.AssortmentMinorRevisionServiceClient.GetAssortmentMinorRevisionInfoByEntityId(
                                new GetAssortmentMinorRevisionInfoByEntityIdRequest(this.GFSEntityId)).AssortmentMinorRevisionInfos;
                        });

                        //Fetch all product universes
                        IEnumerable<AssortmentMinorRevisionInfoDto> ccmAssortmentMinorRevisionList = dalCdtInfo.FetchByEntityId(this.EntityId).Where(c => c.ParentUniqueContentReference == null);

                        //loop through ccm product universes
                        foreach (AssortmentMinorRevisionInfoDto ccmAssortmentMinorRevisionInfo in ccmAssortmentMinorRevisionList)
                        {
                            AssortmentMinorRevisionDto ccmAssortmentMinorRevisionDto = dalCdt.FetchById(ccmAssortmentMinorRevisionInfo.Id);

                            if (ccmAssortmentMinorRevisionDto != null)
                            {
                                //get the product universe from GFS list
                                Services.AssortmentMinorRevision.ContentInfo gfsAssortmentMinorRevision = gfsAssortmentMinorRevisionInfoList.FirstOrDefault(u => u.UniqueContentReference == ccmAssortmentMinorRevisionInfo.UniqueContentReference);

                                if (gfsAssortmentMinorRevision != null)
                                {
                                    //ok we've found the gfs universe
                                    //populate the parent version
                                    if (gfsAssortmentMinorRevision.ParentUniqueContentReference != null)
                                    {
                                        ccmAssortmentMinorRevisionDto.ParentUniqueContentReference = gfsAssortmentMinorRevision.ParentUniqueContentReference;
                                    }
                                    else//first version
                                    {
                                        //otherwise the PUCR is the same as content's ucr
                                        ccmAssortmentMinorRevisionDto.ParentUniqueContentReference = gfsAssortmentMinorRevision.UniqueContentReference;
                                    }

                                    ccmAssortmentMinorRevisionDto.DateLastModified = DateTime.Now;
                                    dalCdt.Update(ccmAssortmentMinorRevisionDto);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.Assortment], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #endregion

        #region Set GFS Connection Details

        private void SetGfsConnectionDetails(ProcessContext processContext)
        {
            try
            {
                Entity currentEntity = Entity.FetchById(this.EntityId);
                String connectionPath = "";

                if (String.IsNullOrEmpty(processContext.SiteName))
                {
                    connectionPath = String.Format("http://{0}:{1}/", processContext.ServerName, processContext.PortNumber.ToString());
                }
                else
                {
                    connectionPath = String.Format("http://{0}:{1}/{2}/", processContext.ServerName, processContext.PortNumber.ToString(), processContext.SiteName);
                }

                currentEntity.SystemSettings.FoundationServicesEndpoint = connectionPath;

                GFSModel.Compression comp = GFSModel.CompressionList.FetchAll(currentEntity).ToList().FirstOrDefault();
                if (String.IsNullOrEmpty(currentEntity.SystemSettings.CompressionName) && comp != null)
                {
                    currentEntity.SystemSettings.CompressionName = comp.Name;
                }

                currentEntity.Save();
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(processContext, SyncTypeHelper.FriendlyDescriptions[SyncType.Entity], ex);

                // throw exception back up the stack
                throw;
            }
        }

        #endregion

        #region SynchroniseLocationPlanAssignmentDates

        private void SynchroniseLocationPlanAssignmentDates(ProcessContext context)
        {
            DateTime processStartDate = DateTime.UtcNow;

            //Start the transaction
            context.DalContext.Begin();

            try
            {

                // Get info about last publish
                Services.Publishing.PublishInfo gfsPublishInfo = null;
                context.DoWorkWithRetries(delegate()
                {
                    GetLatestAuditDatesByEntityNameRequest auditDatesByEntityNameRequestPublishInfoRequest = new GetLatestAuditDatesByEntityNameRequest(this.EntityName);
                    gfsPublishInfo = context.Services.PublishingServiceClient.GetLatestAuditDatesByEntityName(auditDatesByEntityNameRequestPublishInfoRequest).PublishInfo;
                });

                // If date last published was after the last sync then continue, else no need to sync.
                if (SyncTarget.LastSync == null
                    || gfsPublishInfo.CommunicatedContentDate > SyncTarget.LastSync.Value
                    || gfsPublishInfo.LiveContentDate > SyncTarget.LastSync.Value)
                {
                    using (IProductHierarchyDal productHierarchyDal = context.DalContext.GetDal<IProductHierarchyDal>())
                    {
                        using (IProductGroupDal productGroupDal = context.DalContext.GetDal<IProductGroupDal>())
                        {
                            using (ILocationPlanAssignmentDal locationPlanAssignmentDal = context.DalContext.GetDal<ILocationPlanAssignmentDal>())
                            {
                                using (IPlanogramDal planogramDal = context.DalContext.GetDal<IPlanogramDal>())
                                {
                                    using (ILocationInfoDal locationInfoDal = context.DalContext.GetDal<ILocationInfoDal>())
                                    {
                                        using (IWorkpackageInfoDal workPackageInfoDal = context.DalContext.GetDal<IWorkpackageInfoDal>())
                                        {
                                            IEnumerable<LocationInfoDto> ccmLocationInfos = locationInfoDal.FetchByEntityId(this.EntityId);

                                            foreach (LocationInfoDto locationInfo in ccmLocationInfos)
                                            {
                                                IEnumerable<LocationPlanAssignmentDto> locationPlanAssignments = locationPlanAssignmentDal.FetchByLocationId(locationInfo.Id);
                                                if (locationPlanAssignments.Any())
                                                {
                                                    ObservableCollection<Services.Content.CommunicatedContentInfo> gfsCommunicatedContentList = new ObservableCollection<Services.Content.CommunicatedContentInfo>();
                                                    context.DoWorkWithRetries(delegate()
                                                    {
                                                        GetCommunicatedContentInfoByEntityNameContentTypeLocationCodeRequest communicatedContentInfoByEntityNameContentTypeLocationCodeRequest = new GetCommunicatedContentInfoByEntityNameContentTypeLocationCodeRequest(this.EntityName, Galleria.Ccm.GFSModel.ContentType.Planogram.ToString(), locationInfo.Code);
                                                        gfsCommunicatedContentList = new ObservableCollection<Services.Content.CommunicatedContentInfo>(context.Services.ContentServiceClient.GetCommunicatedContentInfoByEntityNameContentTypeLocationCode(communicatedContentInfoByEntityNameContentTypeLocationCodeRequest).CommunicatedContentInfos);
                                                    });

                                                    ObservableCollection<Services.Content.LiveContentInfo> gfsLiveContentList = new ObservableCollection<Services.Content.LiveContentInfo>();
                                                    context.DoWorkWithRetries(delegate()
                                                    {
                                                        GetLiveContentInfoByEntityNameContentTypeLocationCodeRequest liveContentinfoByEntityNameContentTypeLocationCodeRequest = new GetLiveContentInfoByEntityNameContentTypeLocationCodeRequest(this.EntityName, Galleria.Ccm.GFSModel.ContentType.Planogram.ToString(), locationInfo.Code);
                                                        gfsLiveContentList = new ObservableCollection<Services.Content.LiveContentInfo>(context.Services.ContentServiceClient.GetLiveContentInfoByEntityNameContentTypeLocationCode(liveContentinfoByEntityNameContentTypeLocationCodeRequest).LiveContentInfos);
                                                    });

                                                    foreach (LocationPlanAssignmentDto locPlanAssignment in locationPlanAssignments)
                                                    {
                                                        Guid? ucr;
                                                        PlanogramDto planogramDto = planogramDal.FetchByPackageId(locPlanAssignment.PlanogramId).FirstOrDefault();
                                                        ProductGroupDto prodGroup = productGroupDal.FetchById(locPlanAssignment.ProductGroupId);
                                                        if (planogramDto != null && prodGroup != null)
                                                        {
                                                            if (!String.IsNullOrWhiteSpace(planogramDto.UniqueContentReference))
                                                            {
                                                                ucr = new Guid(planogramDto.UniqueContentReference);
                                                                Boolean changed = false;

                                                                Services.Content.CommunicatedContentInfo gfsContent = gfsCommunicatedContentList.FirstOrDefault(
                                                                    c => c.UniqueContentReference == ucr &&
                                                                        c.LocationCode == locationInfo.Code &&
                                                                        c.ProductGroupCode == prodGroup.Code);

                                                                if (locPlanAssignment.DateCommunicated == null)
                                                                {

                                                                    if (gfsContent != null && gfsContent.DateCommunicated != null)
                                                                    {
                                                                        locPlanAssignment.DateCommunicated = gfsContent.DateCommunicated;
                                                                        changed = true;
                                                                    }
                                                                }
                                                                if (gfsContent == null)
                                                                {
                                                                    //no longer in gfs
                                                                    locPlanAssignment.DateCommunicated = null;
                                                                    changed = true;
                                                                }


                                                                Services.Content.LiveContentInfo gfsLiveContent = gfsLiveContentList.FirstOrDefault(
                                                                    c => c.UniqueContentReference == ucr &&
                                                                        c.LocationCode == locationInfo.Code &&
                                                                        c.ProductGroupCode == prodGroup.Code);
                                                                if (locPlanAssignment.DateLive == null)
                                                                {

                                                                    if (gfsLiveContent != null && gfsLiveContent.DateLive != null)
                                                                    {
                                                                        locPlanAssignment.DateLive = gfsLiveContent.DateLive;
                                                                        changed = true;
                                                                    }
                                                                }
                                                                if (gfsLiveContent == null)
                                                                {
                                                                    locPlanAssignment.DateLive = null;
                                                                    changed = true;
                                                                }

                                                                if (changed)
                                                                {
                                                                    locationPlanAssignmentDal.Update(locPlanAssignment);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log error to gfs
                WriteSyncErrorToGFS(context, SyncTypeHelper.FriendlyDescriptions[SyncType.CommunicatedDates], ex);

                // throw exception back up the stack
                throw;
            }

            // Commit the data transaction to CCM
            context.DalContext.Commit();
        }

        #endregion

        #endregion

        #region WriteSyncErrorToGFS

        /// <summary>
        /// Writes the error to the GFS eventlog
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="syncType">The synchronization type</param>
        private void WriteSyncErrorToGFS(ProcessContext context, String syncType, Exception ex)
        {
            GfsLoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Error, GfsLoggingHelper.SyncEventLogName, this.EntityName, EventLogEvent.ErrorWhileSyncingData, context, syncType, ex.Message);
            LoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Error, GfsLoggingHelper.SyncEventLogName, this.EntityId, EventLogEvent.ErrorWhileSyncingData, null, null, syncType, ex.Message);
        }
        #endregion

        #region WriteSyncInformationToGFS

        /// <summary>
        /// Writes the information to the GFS eventlog
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="syncType">The synchronization type</param>
        private void WriteSyncInformationToGFS(ProcessContext context, EventLogEvent eventLogEvent, String[] args)
        {
            GfsLoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Information, GfsLoggingHelper.SyncEventLogName, this.EntityName, eventLogEvent, context, args);
            LoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Information, GfsLoggingHelper.SyncEventLogName, this.EntityId, eventLogEvent, null, null, args);

        }
        #endregion

        #region WriteSyncMaxRetryCountReachedToGFS

        /// <summary>
        /// Writes the max retry count reached to the GFS eventlog
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="syncType">The synchronization type</param>
        private void WriteSyncMaxRetryCountReachedToGFS()
        {
            ProcessContext context = null;

            try
            {
                using (context = new ProcessContext(this.SyncTargetId))
                {
                    //this to ensure that the reason for the failure wasn't while trying to create the web service proxies
                    //if this is the case, it will not be able to log to gfs but will still log to the windows event log
                    if (_webServiceProxiesCreated)
                    {
                        context.CreateWebServiceProxies();
                    }
                }
            }
            catch (Exception)
            {
                //This is to handle if a connection to the CCM database cannot be established. 
                //Either the DB has been deleted or the user does not have permissions                  
            }

            //log to gfs and Windows event log
            GfsLoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Error, GfsLoggingHelper.SyncEventLogName, this.EntityName, EventLogEvent.MaximumRetryCountReached, context, new String[] { _syncTargetMaxRetryCount.ToString() });
            LoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Error, GfsLoggingHelper.SyncEventLogName, this.EntityId, EventLogEvent.MaximumRetryCountReached, null, null, new String[] { _syncTargetMaxRetryCount.ToString() });

        }
        #endregion

        #region WriteSyncMaxFailureCountReachedToGFS

        /// <summary>
        /// Writes the failure to the GFS eventlog
        /// </summary>
        /// <param name="context">The process context</param>
        /// <param name="syncType">The synchronization type</param>
        private void WriteSyncMaxFailureCountReachedToGFS()
        {
            ProcessContext context = null;

            try
            {
                using (context = new ProcessContext(this.SyncTargetId))
                {
                    //this to ensure that the reason for the failure wasn't while trying to create the web service proxies
                    //if this is the case, it will not be able to log to gfs but will still log to the windows event log
                    if (_webServiceProxiesCreated)
                    {
                        context.CreateWebServiceProxies();
                    }
                }
            }
            catch (Exception)
            {
                //This is to handle if a connection to the CCM database cannot be established. 
                //Either the DB has been deleted or the user does not have permissions   
            }

            //log to gfs and Windows event log
            GfsLoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Error, GfsLoggingHelper.SyncEventLogName, this.EntityName, EventLogEvent.MaximumFailureCountReached, context, new String[] { _syncTargetMaxFailedCount.ToString() });
            LoggingHelper.Logger.Write(System.Diagnostics.EventLogEntryType.Error, GfsLoggingHelper.SyncEventLogName, this.EntityId, EventLogEvent.MaximumFailureCountReached, null, null, new String[] { _syncTargetMaxFailedCount.ToString() });
        }
        #endregion

        #region Update System Settings with Success or Failed Sync Date

        /// <summary>
        /// Update the system settings for the entity being synced for UI processor status updates
        /// </summary>
        private void WriteSyncFailedDateToSystemSettings(DateTime syncStart)
        {
            if (this.EntityId > 0)
            {
                try
                {
                    //Get the sync target's entity
                    Entity currentEntity = Entity.FetchById(this.EntityId);
                    //update the entity's failed sync date
                    currentEntity.SystemSettings.LastFailedSyncDate = syncStart;
                    //save the changes
                    currentEntity.Save();
                }
                catch (Exception ex)
                {
                    //log to gfs and Windows event log
                    GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
                }
            }
        }

        /// <summary>
        /// Update the system settings for the entity being synced for UI processor status updates
        /// </summary>
        private void WriteSyncSuccessDateToSystemSettings(DateTime syncStart)
        {
            if (this.EntityId > 0)
            {
                try
                {
                    //Get the sync target's entity
                    Entity currentEntity = Entity.FetchById(this.EntityId);
                    //update the entity's failed sync date
                    currentEntity.SystemSettings.LastSuccessfulSyncDate = syncStart;
                    //save the changes
                    currentEntity.Save();
                }
                catch (Exception ex)
                {
                    //log to gfs and Windows event log
                    GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
                }
            }
        }

        #endregion

        #region Update Sync Target Status
        /// <summary>
        /// Updates the status for the successful sync target
        /// </summary>
        /// <param name="status">The new sync target status</param>
        private void UpdateSyncTargetStatus_Successful(SyncTargetStatus status, DateTime syncStart)
        {
            this.UpdateSyncTargetStatus(status, SyncTargetError.None, syncStart, true);
        }

        /// <summary>
        /// Updates the status for the currently processing sync target
        /// </summary>
        /// <param name="status">The new sync target status</param>
        private void UpdateSyncTargetStatus(SyncTargetStatus status, DateTime syncStart)
        {
            this.UpdateSyncTargetStatus(status, SyncTargetError.None, syncStart);
        }

        /// <summary>
        /// Updates the status of the currently processing sync target
        /// </summary>
        /// <param name="error">The sync target error code</param>
        private void UpdateSyncTargetStatus(SyncTargetError error, DateTime syncStart)
        {
            this.UpdateSyncTargetStatus(SyncTargetStatus.Failed, error, syncStart);
        }

        /// <summary>
        /// Updates the status for the currently processing sync target
        /// </summary>
        /// <param name="status">The new status</param>
        private void UpdateSyncTargetStatus(SyncTargetStatus status, SyncTargetError error, DateTime syncStart, Boolean successful = false)
        {
            try
            {
                // get the sync target
                SyncTarget syncTarget = SyncTarget.GetSyncTargetById(this.SyncTargetId);

                if (successful)
                {
                    // if sync has been successful set the last sync date to now, 
                    // reset the retry count and reset the failed count
                    syncTarget.LastSync = syncStart;
                    syncTarget.RetryCount = 0;
                    syncTarget.FailedCount = 0;
                }
                if (status == SyncTargetStatus.Failed)
                {
                    // if sync target has errored then increment the retry count
                    syncTarget.RetryCount++;
                }
                if (syncTarget.RetryCount == _syncTargetMaxRetryCount)
                {
                    // if sync target is at it's maximum retry count then increment the failed count
                    syncTarget.FailedCount++;

                    // log the failure to the gfs event log
                    WriteSyncMaxRetryCountReachedToGFS();
                }
                if (syncTarget.FailedCount == _syncTargetMaxFailedCount)
                {
                    // if sync target is at it's maximum failed count then set to errored
                    syncTarget.Status = SyncTargetStatus.Errored;
                    syncTarget.Error = SyncTargetError.MaximumFailedCount;

                    // log the error to the gfs event log
                    WriteSyncMaxFailureCountReachedToGFS();
                }
                else
                {
                    syncTarget.Status = status;
                    syncTarget.Error = error;
                }

                //For the UI Sync Status, we need to store the last successful and last failed Sync dates
                if (status == SyncTargetStatus.Failed)
                {

                    WriteSyncFailedDateToSystemSettings(syncStart);
                }
                if (successful)
                {
                    WriteSyncSuccessDateToSystemSettings(syncStart);
                }


                syncTarget.LastRun = syncStart;
                syncTarget = syncTarget.Save();
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }
        }
        #endregion

        #endregion

        #region Static Helper Methods

        #region New Planogram Import Template Mapping Creation

        /// <summary>
        ///     Instantiate a new <see cref="PlanogramImportTemplateMappingDto"/> from the given parameters.
        /// </summary>
        /// <param name="templateId">Id of the <see cref="PlanogramImportTemplate"/> that this mapping will belong to.</param>
        /// <param name="fields">Item1 contains Field and Item2 contains the ExternalField.</param>
        /// <param name="fieldType">The type of field being mapped.</param>
        /// <returns></returns>
        private static PlanogramImportTemplateMappingDto NewPlanogramImportTemplateMappingDto(Int32 templateId,
                                                                                              Tuple<String, String> fields,
                                                                                              PlanogramFieldMappingType fieldType)
        {
            return new PlanogramImportTemplateMappingDto
                   {
                       Field = fields.Item1,
                FieldType = (Byte)fieldType,
                       PlanogramImportTemplateId = templateId,
                       ExternalField = fields.Item2
                   };
        }

        #endregion

        #region New Metric Creation

        /// <summary>
        ///     Instantiate a new <see cref="PlanogramImportTemplatePerformanceMetricDto"/> for Metric 1 - Sales Value.
        /// </summary>
        /// <param name="templateId">Id of the <see cref="PlanogramImportTemplate"/> that this mapping will belong to.</param>
        /// <param name="externalField">The externalField used when mapping this metric.</param>
        /// <returns></returns>
        private static PlanogramImportTemplatePerformanceMetricDto NewMetric1SalesValue(Int32 templateId, String externalField)
        {
            return new PlanogramImportTemplatePerformanceMetricDto
            {
                       PlanogramImportTemplateId = templateId,
                       Name = FrameworkLanguage.Message.PlanogramPerformance_DefaultMetric1_SalesValue,
                       Description = FrameworkLanguage.Message.PlanogramPerformance_DefaultMetric1_RegularSalesValue,
                       ExternalField = externalField,
                       MetricId = 1,
                MetricType = (Byte)MetricType.Decimal
                   };
        }

        /// <summary>
        ///     Instantiate a new <see cref="PlanogramImportTemplatePerformanceMetricDto"/> for Metric 2 - Sales Volume.
        /// </summary>
        /// <param name="templateId">Id of the <see cref="PlanogramImportTemplate"/> that this mapping will belong to.</param>
        /// <param name="externalField">The externalField used when mapping this metric.</param>
        /// <returns></returns>
        private static PlanogramImportTemplatePerformanceMetricDto NewMetric2SalesVolume(Int32 templateId, String externalField)
        {
            return new PlanogramImportTemplatePerformanceMetricDto
            {
                       PlanogramImportTemplateId = templateId,
                       Name = FrameworkLanguage.Message.PlanogramPerformance_DefaultMetric2_SalesVolume,
                       Description = FrameworkLanguage.Message.PlanogramPerformance_DefaultMetric2_RegularSalesVolume,
                       ExternalField = externalField,
                       MetricId = 2,
                MetricType = (Byte)MetricType.Integer,
                SpecialType = (Byte)MetricSpecialType.RegularSalesUnits
                   };
        }

        /// <summary>
        ///     Instantiate a new <see cref="PlanogramImportTemplatePerformanceMetricDto"/> for Metric 3 - Sales Margin.
        /// </summary>
        /// <param name="templateId">Id of the <see cref="PlanogramImportTemplate"/> that this mapping will belong to.</param>
        /// <param name="externalField">The externalField used when mapping this metric.</param>
        /// <returns></returns>
        private static PlanogramImportTemplatePerformanceMetricDto NewMetric3SalesMargin(Int32 templateId, String externalField)
        {
            return new PlanogramImportTemplatePerformanceMetricDto
            {
                       PlanogramImportTemplateId = templateId,
                       Name = FrameworkLanguage.Message.PlanogramPerformance_DefaultMetric3_SalesMargin,
                       Description = FrameworkLanguage.Message.PlanogramPerformance_DefaultMetric3_RegularSalesMargin,
                       ExternalField = externalField,
                       MetricId = 3,
                MetricType = (Byte)MetricType.Decimal
                   };
        }

        #endregion

        #endregion
    }
}