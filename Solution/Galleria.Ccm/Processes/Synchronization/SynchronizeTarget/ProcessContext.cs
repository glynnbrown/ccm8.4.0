﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#region Version History : CCM820
// V8-31246 : A.Kuszyk
//  Added RegisteredTasks property.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Threading;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Entity;
using Galleria.Ccm.Services.Product;
using Csla;
using Galleria.Ccm.Helpers;
using System.ServiceModel;
using Galleria.Ccm.Services.Performance;

namespace Galleria.Ccm.Processes.Synchronization.SynchronizeTarget
{
    /// <summary>
    /// Defines a context object for this process
    /// </summary>
    public class ProcessContext : IDisposable
    {
        #region Constants

        private const Int32 _webServiceRetryCount = 5;
        private const Int32 _webServiceRetryDelay = 1000;

        #endregion

        #region Fields
        private Int32 _syncTargetId; // the sync target id
        private IDalFactory _dalFactory; // the dal factory to use for this process
        private IDalContext _dalContext; // the dal context to use for this process
        private FoundationServiceClient _services; // the foundation services client proxy manager
        private String _webServiceAddress;

        private Dictionary<Int32, Int32> _productLevelMap = new Dictionary<Int32, Int32>(); //The Product Level Mapping
        private Dictionary<String, Int32> _productGroupMap = new Dictionary<String, Int32>(); // The Product Group Mapping
        private Dictionary<Int32, Int32> _productGroupIdMap = new Dictionary<Int32, Int32>(); // The Product Group Mapping
        private Dictionary<Int32, Int32> _locationLevelMap = new Dictionary<Int32, Int32>(); //The Location Level Mapping
        private Dictionary<String, Int32> _locationGroupMap = new Dictionary<String, Int32>(); // The Location Group Mapping
        private Dictionary<Int32, Int32> _locationGroupIdMap = new Dictionary<Int32, Int32>(); // The Location Group Mapping
        private Dictionary<Int32, Int16> _locationIdMap = new Dictionary<Int32, Int16>(); // The location Id Mapping
        private Dictionary<Int32, Services.Location.Location> _locationMap = new Dictionary<Int32, Services.Location.Location>(); // The location Mapping
        private Dictionary<Int32, Int32> _locationTypeIdMap = new Dictionary<Int32, Int32>(); // The Location Type Id Mapping
        private Dictionary<String, Int32> _productMap = new Dictionary<String, Int32>(); //The product mapping

        private String _serverName = "";
        private Int32 _portNumber = 0;
        private String _siteName = "";
        #endregion

        #region Delegates

        public delegate void DoWork();

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="syncTargetId">The sync target id</param>
        public ProcessContext(Int32 syncTargetId)
        {
            // store the sync target id
            _syncTargetId = syncTargetId;

            // create the dal factory
            this.CreateDalFactory();
        }
        #endregion

        #region Properties
        /// <summary>
        /// The sync target id
        /// </summary>
        public Int32 SyncTargetId
        {
            get { return _syncTargetId; }
        }

        /// <summary>
        /// The ISO dal context for this process
        /// </summary>
        public IDalContext DalContext
        {
            get { return _dalContext; }
        }

        /// <summary>
        /// The foundation services client proxy
        /// </summary>
        public FoundationServiceClient Services
        {
            get { return _services; }
        }

        public Dictionary<Int32, Int32> ProductLevelMap
        {
            get { return _productLevelMap; }
        }

        public Dictionary<String, Int32> ProductGroupMap
        {
            get { return _productGroupMap; }
        }

        public Dictionary<Int32, Int32> ProductGroupIdMap
        {
            get { return _productGroupIdMap; }
        }

        public Dictionary<Int32, Int32> LocationLevelMap
        {
            get { return _locationLevelMap; }
        }

        public Dictionary<String, Int32> LocationGroupMap
        {
            get { return _locationGroupMap; }
        }

        public Dictionary<Int32, Int32> LocationGroupIdMap
        {
            get { return _locationGroupIdMap; }
        }

        public Dictionary<Int32, Int16> LocationIdMap
        {
            get { return _locationIdMap; }
        }

        public Dictionary<Int32, Int32> LocationTypeIdMap
        {
            get { return _locationTypeIdMap; }
        }

        public Dictionary<String, Int32> ProductMap
        {
            get { return _productMap; }
        }

        public Dictionary<Int32, Services.Location.Location> LocationMap
        {
            get { return _locationMap; }
        }

        public String ServerName
        {
            get { return _serverName; }
        }

        public Int32 PortNumber
        {
            get { return _portNumber; }
        }

        public String SiteName
        {
            get { return _siteName; }
        }

        public EngineTaskInfoList RegisteredTasks { get; set; }

        #endregion

        #region Methods

        #region CreateDalFactory
        /// <summary>
        /// Creates the dal factory
        /// </summary>
        private void CreateDalFactory()
        {
            try
            {
                // get the sync target
                SyncTarget syncTarget = SyncTarget.GetSyncTargetById(this.SyncTargetId);

                // create a new dal factory config element
                DalFactoryConfigElement dalFactoryConfig = null;
                switch (syncTarget.TargetType)
                {
                    case SyncTargetType.Mssql:
                        dalFactoryConfig = new DalFactoryConfigElement("SyncTarget", "Galleria.Ccm.Dal.Mssql.dll");
                        break;
                    //case SyncTargetType.VistaDb:
                    //    dalFactoryConfig = new DalFactoryConfigElement("SyncTarget", "Galleria.StrategicAssortment.Dal.VistaDb.dll");
                    //    break;
                }

                // set the fal factory name
                dalFactoryConfig.Name = Guid.NewGuid().ToString();

                // split the connection string based on the key value pairs
                string[] parameters = syncTarget.ConnectionString.Split(';');
                foreach (string parameter in parameters)
                {
                    // split the parameter into its key and value
                    string[] values = parameter.Split('=');

                    // create a new config parameter for the dal factory
                    dalFactoryConfig.DalParameters.Add(new DalFactoryParameterConfigElement(values[0].Trim(), values[1].Trim()));
                }

                // create the dal factory
                _dalFactory = DalContainer.CreateFactory(dalFactoryConfig);
                DalContainer.RegisterFactory(dalFactoryConfig.Name, _dalFactory);
                DalContainer.DalName = dalFactoryConfig.Name;

                // and start a new context
                _dalContext = _dalFactory.CreateContext();
            }
            catch (DataPortalException ex)
            {
                //Do nothing, should re-poll and update

                //log to gfs and Windows event log
                GfsLoggingHelper.Logger.Write(GfsLoggingHelper.SyncEventLogName, null, ex.GetBaseException());
            }
        }
        #endregion

        #region IsDatabaseAvailable()
        /// <summary>
        /// Indicates if the target database is available
        /// </summary>
        /// <returns>True of the database is available, else false</returns>
        public bool IsDatabaseAvailable()
        {
            if (_dalFactory != null)
            {
                return _dalFactory.IsDatabaseAvailable();
            }
            return false;
        }
        #endregion

        #region CreateWebServiceProxies
        /// <summary>
        /// Attempts to create the foundation services
        /// web service proxies
        /// </summary>
        /// <returns>True if successful, else false</returns>
        public bool CreateWebServiceProxies()
        {
            SyncSourceList _syncSources = SyncSourceList.GetAllSyncSources();
            SyncSource _syncSource;

            //if we have a dto, then attempt to create the proxies
            if (_syncSources.Count > 0)
            {
                _syncSource = _syncSources.FirstOrDefault<SyncSource>();

                _serverName = _syncSource.ServerName;
                _portNumber = _syncSource.PortNumber;

                if (String.IsNullOrEmpty(_syncSource.SiteName))
                {
                    _webServiceAddress = String.Format("http://{0}:{1}/Services", _syncSource.ServerName, _syncSource.PortNumber.ToString());
                }
                else
                {
                    _webServiceAddress = String.Format("http://{0}:{1}/{2}/Services", _syncSource.ServerName, _syncSource.PortNumber.ToString(), _syncSource.SiteName);
                    _siteName = _syncSource.SiteName;
                }

                _services = new FoundationServiceClient(_webServiceAddress);

                return true;
            }
            return false;
        }
        #endregion

        #region DoWorkWithRetries
        /// <summary>
        /// Performs a bit of work (using the web services), and if it fails, retries several times before giving
        /// up and rethrowing the error.  Before each retry the proxy is destroyed and created.
        /// </summary>
        public void DoWorkWithRetries(DoWork doWork)
        {
            try
            {
                doWork();
            }
            catch (Exception ex)
            {
                //Flag for whether a reply is valid, certain exceptions wont be
                Boolean isRetryValid = true;
                if (ex.GetType() == typeof(FaultException<PerformanceServiceRowCountFault>))
                {
                    //Set flag
                    isRetryValid = false;
                    //Throw exception
                    throw;
                }

                //If retry is valid
                if (isRetryValid)
                {
                    for (Int32 i = 0; i < _webServiceRetryCount; i++)
                    {
                        Thread.Sleep(_webServiceRetryDelay);
                        try
                        {
                            _services.Dispose();
                        }
                        catch (Exception) { }
                        try
                        {
                            _services = new FoundationServiceClient(_webServiceAddress);
                            doWork();
                            break;
                        }
                        catch (Exception)
                        {
                            if (i == _webServiceRetryCount - 1)
                            {
                                throw;
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Called when this instance is being disposed
        /// </summary>
        public void Dispose()
        {
            if (_dalContext != null) _dalContext.Dispose();
            if (_dalFactory != null) _dalFactory.Dispose();
            if (_services != null) _services.Dispose();
        }
        #endregion

        #endregion
    }
}