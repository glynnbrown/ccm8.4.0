﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Processes.Synchronization
{
    /// <summary>
    /// The main synchornisation process
    /// </summary>
    public partial class Process : ProcessBase<Process>
    {
    }
}