﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//  Corrected EventLogNameName to be EventLogName
#endregion
#endregion

using System;
using System.Globalization;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Processes.ExportEventLog
{
    [Serializable]
    public class EventLogImportMappingList : ImportMappingList<EventLogImportMappingList>
    {
        #region Constants

        public const Int32 Id_Id = 1;
        public const Int32 EntityId_Id = 2;
        public const Int32 EventLogNameId_Id = 3;
        public const Int32 EventLogName_Id = 4;
        public const Int32 Source_Id = 5;
        public const Int32 EventId_Id = 6;
        public const Int32 EntryType_Id = 7;
        public const Int32 UserId_Id = 8;
        public const Int32 DateTime_Id = 9;
        public const Int32 Process_Id = 10;
        public const Int32 ComputerName_Id = 11;
        public const Int32 URLHelperLink_Id = 12;
        public const Int32 Description_Id = 13;
        public const Int32 Content_Id = 14;
        public const Int32 GibraltarSessionId_Id = 15;

        #endregion

        #region Constructors
        private EventLogImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static EventLogImportMappingList NewEventLogImportMappingList()
        {
            EventLogImportMappingList list = new EventLogImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        private new void Create()
        {
            this.RaiseListChangedEvents = false;

            #region EventLog Properties
            //(int identifier, string propertyName, string propertyNotes, bool propertyIsMandatory, Type propertyType, object propertyDefault, int propertyMin, int propertyMax, bool canDefaultData);
            //Id
            this.Add(ImportMapping.NewImportMapping(Id_Id, Galleria.Ccm.Model.EventLog.IdProperty.FriendlyName, Galleria.Ccm.Model.EventLog.IdProperty.Description, true, Galleria.Ccm.Model.EventLog.IdProperty.Type, Galleria.Ccm.Model.EventLog.IdProperty.DefaultValue, 0, Int32.MaxValue, false));
            //Entity_Id
            this.Add(ImportMapping.NewImportMapping(EntityId_Id, Galleria.Ccm.Model.EventLog.EntityIdProperty.FriendlyName, Galleria.Ccm.Model.EventLog.EntityIdProperty.Description, true, Galleria.Ccm.Model.EventLog.EntityIdProperty.Type, Galleria.Ccm.Model.EventLog.EntityIdProperty.DefaultValue, 0, Int32.MaxValue, false));
            //EventLogName_Id
            this.Add(ImportMapping.NewImportMapping(EventLogNameId_Id, Galleria.Ccm.Model.EventLog.EventLogNameIdProperty.FriendlyName, Galleria.Ccm.Model.EventLog.EventLogNameIdProperty.Description, true, Galleria.Ccm.Model.EventLog.EventLogNameIdProperty.Type, Galleria.Ccm.Model.EventLog.EventLogNameIdProperty.DefaultValue, 0, Int32.MaxValue, false));
            //EventLogName
            this.Add(ImportMapping.NewImportMapping(EventLogName_Id, Galleria.Ccm.Model.EventLog.EventLogNameProperty.FriendlyName, Galleria.Ccm.Model.EventLog.EventLogNameProperty.Description, true, Galleria.Ccm.Model.EventLog.EventLogNameProperty.Type, Galleria.Ccm.Model.EventLog.EventLogNameProperty.DefaultValue, 0, 1000, true));
            //Source
            this.Add(ImportMapping.NewImportMapping(Source_Id, Galleria.Ccm.Model.EventLog.SourceProperty.FriendlyName, Galleria.Ccm.Model.EventLog.SourceProperty.Description, true, Galleria.Ccm.Model.EventLog.SourceProperty.Type, Galleria.Ccm.Model.EventLog.SourceProperty.DefaultValue, 0, 1000, true));
            //Event_Id
            this.Add(ImportMapping.NewImportMapping(EventId_Id, Galleria.Ccm.Model.EventLog.EventIdProperty.FriendlyName, Galleria.Ccm.Model.EventLog.EventIdProperty.Description, true, Galleria.Ccm.Model.EventLog.EventIdProperty.Type, Galleria.Ccm.Model.EventLog.EventIdProperty.DefaultValue, 0, Int32.MaxValue, false));
            //EntryType
            this.Add(ImportMapping.NewImportMapping(EntryType_Id, Galleria.Ccm.Model.EventLog.EntryTypeProperty.FriendlyName, Galleria.Ccm.Model.EventLog.EntryTypeProperty.Description, true, Galleria.Ccm.Model.EventLog.EntryTypeProperty.Type, Galleria.Ccm.Model.EventLog.EntryTypeProperty.DefaultValue, 0, Int32.MaxValue, false));
            //User_Id
            this.Add(ImportMapping.NewImportMapping(UserId_Id, Galleria.Ccm.Model.EventLog.UserIdProperty.FriendlyName, Galleria.Ccm.Model.EventLog.UserIdProperty.Description, true, Galleria.Ccm.Model.EventLog.UserIdProperty.Type, Galleria.Ccm.Model.EventLog.UserIdProperty.DefaultValue, 0, Int32.MaxValue, false));
            //DateTime
            this.Add(ImportMapping.NewImportMapping(DateTime_Id, Galleria.Ccm.Model.EventLog.DateTimeProperty.FriendlyName, Galleria.Ccm.Model.EventLog.DateTimeProperty.Description, true, Galleria.Ccm.Model.EventLog.DateTimeProperty.Type, Galleria.Ccm.Model.EventLog.DateTimeProperty.DefaultValue, 0, 1000, true));
            //Process
            this.Add(ImportMapping.NewImportMapping(Process_Id, Galleria.Ccm.Model.EventLog.ProcessProperty.FriendlyName, Galleria.Ccm.Model.EventLog.ProcessProperty.Description, true, Galleria.Ccm.Model.EventLog.ProcessProperty.Type, Galleria.Ccm.Model.EventLog.ProcessProperty.DefaultValue, 0, 1000, true));
            //ComputerName
            this.Add(ImportMapping.NewImportMapping(ComputerName_Id, Galleria.Ccm.Model.EventLog.ComputerNameProperty.FriendlyName, Galleria.Ccm.Model.EventLog.ComputerNameProperty.Description, true, Galleria.Ccm.Model.EventLog.ComputerNameProperty.Type, Galleria.Ccm.Model.EventLog.ComputerNameProperty.DefaultValue, 0, 1000, true));
            //URLHelperLin
            this.Add(ImportMapping.NewImportMapping(URLHelperLink_Id, Galleria.Ccm.Model.EventLog.UrlHelperLinkProperty.FriendlyName, Galleria.Ccm.Model.EventLog.UrlHelperLinkProperty.Description, true, Galleria.Ccm.Model.EventLog.UrlHelperLinkProperty.Type, Galleria.Ccm.Model.EventLog.UrlHelperLinkProperty.DefaultValue, 0, 1000, true));
            //Description
            this.Add(ImportMapping.NewImportMapping(Description_Id, Galleria.Ccm.Model.EventLog.DescriptionProperty.FriendlyName, Galleria.Ccm.Model.EventLog.DescriptionProperty.Description, true, Galleria.Ccm.Model.EventLog.DescriptionProperty.Type, Galleria.Ccm.Model.EventLog.DescriptionProperty.DefaultValue, 0, 1000, true));
            //Content
            this.Add(ImportMapping.NewImportMapping(Content_Id, Galleria.Ccm.Model.EventLog.ContentProperty.FriendlyName, Galleria.Ccm.Model.EventLog.ContentProperty.Description, true, Galleria.Ccm.Model.EventLog.ContentProperty.Type, Galleria.Ccm.Model.EventLog.ContentProperty.DefaultValue, 0, 1000, true));
            //GibraltarSessionId
            this.Add(ImportMapping.NewImportMapping(GibraltarSessionId_Id, Galleria.Ccm.Model.EventLog.GibraltarSessionIdProperty.FriendlyName, Galleria.Ccm.Model.EventLog.GibraltarSessionIdProperty.Description, true, Galleria.Ccm.Model.EventLog.GibraltarSessionIdProperty.Type, Galleria.Ccm.Model.EventLog.GibraltarSessionIdProperty.DefaultValue, 0, 1000, true));

            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, Galleria.Ccm.Model.EventLog eventLog)
        {
            switch (mappingId)
            {
                #region EventLog Properties

                //Id
                case EventLogImportMappingList.Id_Id: return eventLog.Id;
                //Entity_Id
                case EventLogImportMappingList.EntityId_Id: return eventLog.EntityId;
                //EventLogName_Id
                case EventLogImportMappingList.EventLogNameId_Id: return eventLog.EventLogNameId;
                //EventLogName
                case EventLogImportMappingList.EventLogName_Id: return eventLog.EventLogName;
                //Source
                case EventLogImportMappingList.Source_Id: return eventLog.Source;
                //Event_Id
                case EventLogImportMappingList.EventId_Id: return eventLog.EventId;
                //EntryType
                case EventLogImportMappingList.EntryType_Id: return eventLog.EntryType;
                //User_Id
                case EventLogImportMappingList.UserId_Id: return eventLog.UserId;
                //DateTime
                case EventLogImportMappingList.DateTime_Id: return eventLog.DateTime;
                //Process
                case EventLogImportMappingList.Process_Id: return eventLog.Process;
                //ComputerName
                case EventLogImportMappingList.ComputerName_Id: return eventLog.ComputerName;
                //URLHelperLink
                case EventLogImportMappingList.URLHelperLink_Id: return eventLog.UrlHelperLink;
                //Description
                case EventLogImportMappingList.Description_Id: return eventLog.Description;
                //Content
                case EventLogImportMappingList.Content_Id: return eventLog.Content;
                //GibraltarSessionId
                case EventLogImportMappingList.GibraltarSessionId_Id: return eventLog.GibraltarSessionId;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="locationDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            EventLogDto EventLogDto)
        {
            IFormatProvider prov = CultureInfo.CurrentCulture;

            switch (mappingId)
            {
                #region EventLog Properties
                //Id
                case EventLogImportMappingList.Id_Id:
                    EventLogDto.Id = Convert.ToInt32(cellValue, prov);
                    break;
                //Entity_Id
                case EventLogImportMappingList.EntityId_Id:
                    EventLogDto.EntityId = Convert.ToInt32(cellValue, prov);
                    break;
                //EventLogName_Id
                case EventLogImportMappingList.EventLogNameId_Id:
                    EventLogDto.EventLogNameId = Convert.ToInt32(cellValue, prov);
                    break;
                //EventLogName
                case EventLogImportMappingList.EventLogName_Id:
                    EventLogDto.EventLogName = Convert.ToString(cellValue, prov);
                    break;
                //Source
                case EventLogImportMappingList.Source_Id:
                    EventLogDto.Source = Convert.ToString(cellValue, prov);
                    break;
                //Event_Id
                case EventLogImportMappingList.EventId_Id:
                    EventLogDto.EventId = Convert.ToInt32(cellValue, prov);
                    break;
                //EntryType
                case EventLogImportMappingList.EntryType_Id:
                    EventLogDto.EntryType = Convert.ToInt16(cellValue, prov);
                    break;
                //User_Id
                case EventLogImportMappingList.UserId_Id:
                    EventLogDto.UserId = Convert.ToInt32(cellValue, prov);
                    break;
                //DateTime
                case EventLogImportMappingList.DateTime_Id:
                    EventLogDto.DateTime = Convert.ToDateTime(cellValue, prov);
                    break;
                //Process
                case EventLogImportMappingList.Process_Id:
                    EventLogDto.Process = Convert.ToString(cellValue, prov);
                    break;
                //ComputerName
                case EventLogImportMappingList.ComputerName_Id:
                    EventLogDto.ComputerName = Convert.ToString(cellValue, prov);
                    break;
                //URLHelperLink
                case EventLogImportMappingList.URLHelperLink_Id:
                    EventLogDto.URLHelperLink = Convert.ToString(cellValue, prov);
                    break;
                //Description
                case EventLogImportMappingList.Description_Id:
                    EventLogDto.Description = Convert.ToString(cellValue, prov);
                    break;
                //Content
                case EventLogImportMappingList.Content_Id:
                    EventLogDto.Content = Convert.ToString(cellValue, prov);
                    break;
                //GibraltarSessionId
                case EventLogImportMappingList.GibraltarSessionId_Id:
                    EventLogDto.GibraltarSessionId = Convert.ToString(cellValue, prov);
                    break;
                #endregion
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override string GetBindingPath(int mappingId)
        {
            switch (mappingId)
            {
                #region EventLog Properties
                //Id
                case EventLogImportMappingList.Id_Id: return Galleria.Ccm.Model.EventLog.IdProperty.Name;
                //Entity_Id
                case EventLogImportMappingList.EntityId_Id: return Galleria.Ccm.Model.EventLog.EntityIdProperty.Name;
                //EventLogName_Id
                case EventLogImportMappingList.EventLogNameId_Id: return Galleria.Ccm.Model.EventLog.EventLogNameIdProperty.Name;
                //EventLogName
                case EventLogImportMappingList.EventLogName_Id: return Galleria.Ccm.Model.EventLog.EventLogNameProperty.Name;
                //Source
                case EventLogImportMappingList.Source_Id: return Galleria.Ccm.Model.EventLog.SourceProperty.Name;
                //Event_Id
                case EventLogImportMappingList.EventId_Id: return Galleria.Ccm.Model.EventLog.EventIdProperty.Name;
                //EntryType
                case EventLogImportMappingList.EntryType_Id: return Galleria.Ccm.Model.EventLog.EntryTypeProperty.Name;
                //User_Id
                case EventLogImportMappingList.UserId_Id: return Galleria.Ccm.Model.EventLog.UserIdProperty.Name;
                //DateTime
                case EventLogImportMappingList.DateTime_Id: return Galleria.Ccm.Model.EventLog.DateTimeProperty.Name;
                //Process
                case EventLogImportMappingList.Process_Id: return Galleria.Ccm.Model.EventLog.ProcessProperty.Name;
                //ComputerName
                case EventLogImportMappingList.ComputerName_Id: return Galleria.Ccm.Model.EventLog.ComputerNameProperty.Name;
                //URLHelperLink
                case EventLogImportMappingList.URLHelperLink_Id: return Galleria.Ccm.Model.EventLog.UrlHelperLinkProperty.Name;
                //Description
                case EventLogImportMappingList.Description_Id: return Galleria.Ccm.Model.EventLog.DescriptionProperty.Name;
                //Content
                case EventLogImportMappingList.Content_Id: return Galleria.Ccm.Model.EventLog.ContentProperty.Name;
                //GibraltarSessionId
                case EventLogImportMappingList.GibraltarSessionId_Id: return Galleria.Ccm.Model.EventLog.GibraltarSessionIdProperty.Name;

                default: throw new NotImplementedException();

                #endregion
            }
        }

        #endregion
    }
}
