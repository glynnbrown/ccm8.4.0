﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
#endregion

#endregion

using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Processes.ExportEventLog
{
    public partial class Process : ProcessBase<Process>
    {
        #region Constants
        private const System.String _eventLogName = "Imports"; // the event log name used with LoggingHelper.Logger
        #endregion

        #region Properties

        /// <summary>
        /// Event log property
        /// </summary>
        public static readonly PropertyInfo<IEnumerable<EventLog>> EventLogsProperty =
            RegisterProperty<IEnumerable<EventLog>>(c => c.EventLogs);
        public IEnumerable<EventLog> EventLogs
        {
            get { return ReadProperty<IEnumerable<EventLog>>(EventLogsProperty); }
        }

        /// <summary>
        // File Name property
        /// </summary>
        private string FileName;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="includeColumnHeaders"></param>
        /// <param name="eventLogs"></param>
        public Process(string filename, bool includeColumnHeaders, IEnumerable<EventLog> eventLogs)
        {
            //Pass file name through
            FileName = (string)filename;           
            this.LoadProperty<IEnumerable<EventLog>>(EventLogsProperty, eventLogs);

        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Process), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(Process), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(Process), new IsInRole(AuthorizationActions.EditObject, DomainPermission.EventLogGet.ToString()));
            BusinessRules.AddRule(typeof(Process), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
