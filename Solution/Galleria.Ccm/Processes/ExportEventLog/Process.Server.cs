﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
#endregion
#region Version History: (CCM CCM830)
// V8-32294 : M.Pettit
//  DateTime columns are now exported as local time strings
#endregion

#endregion

using System;
using System.Diagnostics;
using Aspose.Cells;
using Galleria.Ccm.Helpers;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Processes.ExportEventLog
{
    public partial class Process
    {

        #region Methods
        /// <summary>
        /// Called when executing this process
        /// </summary>
        protected override void OnExecute()
        {

            //get the mapping list of the export type
            IImportMappingList mappingList = EventLogImportMappingList.NewEventLogImportMappingList();

            //create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }

            Int32 row = 1;
            //Loop through each eventlog to export to excel
            foreach (Galleria.Ccm.Model.EventLog eventLog in this.EventLogs)
            {
                //Loop through each propery for current event log to send to excel
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];

                    //Get event log property value
                    Object cellValue =
                        EventLogImportMappingList.GetValueByMappingId(mapping.PropertyIdentifier, eventLog);

                    //if not null the write to excel
                    if (cellValue != null)
                    {
                        //convert utc dates to local and export as a string value
                        if (mapping.PropertyType == typeof(System.DateTime))
                        {
                            cellValue = GetValueAsLocalDateTime(cellValue);
                        } 
                        
                        workSheet.Cells[row, i].Value = cellValue;
                    }

                }
                //Move to next row
                row++;
            }
            //Once finished, save excel file

            try
            {
                workbook.Save(FileName);
                Trace.TraceInformation("{0} event log records exported successfully.", workSheet.Cells.Rows.Count);
            }
            catch (System.IO.IOException)
            {
                //file is already opened elsewhere
                LoggingHelper.Logger.Write(
                    EventLogEntryType.Error,
                    _eventLogName,
                    null,
                    Model.EventLogEvent.FileInUse,
                    null,
                    null,
                    FileName);
            }
        }
        #endregion

        private Object GetValueAsLocalDateTime(Object value)
        {
            if (value == null) return value;
            try
            {
                DateTime returnValue = ((DateTime)value).ToLocalTime();
                return returnValue.ToString("yyyy-MM-dd hh:mm:ss");
            }
            catch
            {
                return value;
            }
        }
    }
}
