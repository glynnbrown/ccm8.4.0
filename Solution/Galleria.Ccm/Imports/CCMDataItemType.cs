﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Hodson
//	Copied from SA
// CCM-25449 : N.Haywood
//  Added cluster locations
// CCM-25452 : N.Haywood
//		Added product imports
// CCM-25448 : N.Haywood
//  Added location space
// CCM-25446 : N.Haywood
//  Added LocationProductAttribute
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-25455 : J.Pickup
//  Added Assortment 
#endregion
#region Version History: (CCM CCM8.1.1)
// CCM-29840 : I.George
//  Changed the order of CCMDataItemType to match the order of items on the screen
#endregion

#endregion

using System;

using Csla.Serialization;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Imports.Processes;
using Galleria.Framework.Processes;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Imports
{
    /// <summary>
    /// Represents the data type selected on the data management import screen
    /// </summary>
    public enum CCMDataItemType
    {
        MerchandisingHierarchy,
        LocationHierarchy,
        Location,
        Product,
        Assortment,
        LocationSpace,
        LocationSpaceBay,
        LocationSpaceElement,
        LocationClusters,
        LocationProductAttributes,
        LocationProductIllegal,
        LocationProductLegal,
    }

    public static class CCMDataItemTypeHelper
    {
        public static readonly Dictionary<CCMDataItemType, String> FriendlyNames =
            new Dictionary<CCMDataItemType, String>()
            {
                {CCMDataItemType.MerchandisingHierarchy, Message.Enum_CCMDataType_MerchandisingHierarchy},
                {CCMDataItemType.LocationHierarchy, Message.Enum_CCMDataType_LocationHierarchy},
                {CCMDataItemType.Location, Message.Enum_CCMDataType_Location},
                {CCMDataItemType.LocationClusters, Message.Enum_CCMDataType_LocationClusters},
                {CCMDataItemType.Product, Message.Enum_CCMDataType_Product},
                {CCMDataItemType.LocationSpace, Message.Enum_CCMDataType_LocationSpecificCategorySpace},
                {CCMDataItemType.LocationSpaceBay, Message.Enum_CCMDataType_DetailedLocationSpecificCategoryBaySpace},
                {CCMDataItemType.LocationSpaceElement, Message.Enum_CCMDataType_DetailedLocationSpecificCategoryElementSpace},
                {CCMDataItemType.LocationProductAttributes, Message.Enum_CCMDataType_DetailedLocationProductAttributes},
                {CCMDataItemType.LocationProductIllegal, Message.Enum_CCMDataType_LocationProductIllegal},
                {CCMDataItemType.LocationProductLegal, Message.Enum_CCMDataType_LocationProductLegal},
                {CCMDataItemType.Assortment, Message.Enum_CCMDataItemType_Assortment},
            };

        /// <summary>
        /// Returns the permission type for the given datatype
        /// </summary>
        /// <returns></returns>
        public static Type GetPermissionType(CCMDataItemType dataType)
        {
            Type permissionType = null;
            switch (dataType)
            {
                case CCMDataItemType.Location:
                    permissionType = typeof(Location);
                    break;
                case CCMDataItemType.Product:
                    permissionType = typeof(Product);
                    break;
                //case CCMDataItemType.LocationClusters:
                //    permissionType = typeof(ClusterScheme);
                //    break;
                case CCMDataItemType.MerchandisingHierarchy:
                    permissionType = typeof(ProductHierarchy);
                    break;
                case CCMDataItemType.LocationHierarchy:
                    permissionType = typeof(LocationHierarchy);
                    break;
                case CCMDataItemType.LocationSpace:
                    permissionType = typeof(LocationSpace);
                    break;
                case CCMDataItemType.LocationSpaceBay:
                    permissionType = typeof(LocationSpaceBay);
                    break;
                case CCMDataItemType.LocationSpaceElement:
                    permissionType = typeof(LocationSpaceElement);
                    break;
                case CCMDataItemType.LocationClusters:
                    permissionType = typeof(ClusterLocation);
                    break;
                case CCMDataItemType.LocationProductAttributes:
                    permissionType = typeof(LocationProductAttribute);
                    break;
                case CCMDataItemType.LocationProductIllegal:
                    permissionType = typeof(LocationProductIllegal);
                    break;
                case CCMDataItemType.LocationProductLegal:
                    permissionType = typeof(LocationProductLegal);
                    break;
                case CCMDataItemType.Assortment:
                    permissionType = typeof(Assortment);
                    break;
                default:
                    //[TODO]
                    break;
            }

            return permissionType;
        }
    }
}
