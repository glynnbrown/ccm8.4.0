﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
// V8-24264 : A.Probyn
//  Fixed so it actually works for data management view.
#endregion

#region Version History: (CCM 8.3.0)
// V8-31877 : N.Haywood
//  Added Product Group Code
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class LocationClusterImportMappingList : ImportMappingList<LocationClusterImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        public const Int32 SchemeNameMappingId = 0;
        public const Int32 ClusterNameMappingId = 1;
        public const Int32 LocationCodeMappingId = 2;
        public const Int32 ProductGroupCodeMappingId = 3;

        #endregion

        #region Constructors
        private LocationClusterImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static LocationClusterImportMappingList NewLocationClusterImportMappingList()
        {
            LocationClusterImportMappingList list = new LocationClusterImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region Location Cluster Properties

            this.Add(ImportMapping.NewImportMapping(SchemeNameMappingId, Message.LocationCluster_SchemeName, Message.LocationCluster_SchemeName_Description, true, /*identifier*/true, ClusterScheme.NameProperty.Type, ClusterScheme.NameProperty.DefaultValue, 1, 255, false));
            this.Add(ImportMapping.NewImportMapping(ClusterNameMappingId, Message.LocationCluster_ClusterName, Message.LocationCluster_ClusterName_Description, true, /*identifier*/true, Cluster.NameProperty.Type, Cluster.NameProperty.DefaultValue, 1, 100, false));
            this.Add(ImportMapping.NewImportMapping(LocationCodeMappingId, Message.LocationCluster_LocationCode, Message.LocationCluster_LocationCode_Description, true, /*identifier*/true, ClusterLocation.CodeProperty.Type, ClusterLocation.CodeProperty.DefaultValue, 1, 50, false));
            this.Add(ImportMapping.NewImportMapping(ProductGroupCodeMappingId, Message.LocationCluster_ProductGroupCode, Message.LocationCluster_ProductGroupCode_Description, false, /*identifier*/false, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 1, 50, false));

            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, ClusterLocationSearchCriteriaDto clusterCriteriaDto)
        {
            switch (mappingId)
            {
                #region Location Cluster Properties
                case LocationClusterImportMappingList.SchemeNameMappingId: return clusterCriteriaDto.ClusterSchemeName;
                case LocationClusterImportMappingList.ClusterNameMappingId: return clusterCriteriaDto.ClusterName;
                case LocationClusterImportMappingList.LocationCodeMappingId: return clusterCriteriaDto.LocationCode;
                case LocationClusterImportMappingList.ProductGroupCodeMappingId: return clusterCriteriaDto.ProductGroupCode;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override String GetBindingPath(Int32 mappingId)
        {
            return GetBindingPath(mappingId, null);
        }
        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;

            switch (mappingId)
            {
                #region Location Properties
                case LocationClusterImportMappingList.SchemeNameMappingId: return String.Format("{0}{1}", prefix, ClusterScheme.NameProperty.Name);
                case LocationClusterImportMappingList.ClusterNameMappingId: return String.Format("{0}{1}", prefix, Cluster.NameProperty.Name);
                case LocationClusterImportMappingList.LocationCodeMappingId: return String.Format("{0}{1}", prefix, Location.CodeProperty.Name);
                case LocationClusterImportMappingList.ProductGroupCodeMappingId: return String.Format("{0}{1}", prefix, ProductGroup.CodeProperty.Name);

                #endregion

                default: throw new NotImplementedException();
            }
        }

        #endregion

        #region IModelObjectColumnHelper members

        //Boolean IModelObjectColumnHelper.IsUOMDisplayConversionRequired(Int32 mappingId)
        //{
        //    return LocationImportMappingList.IsUOMDisplayConversionRequired(mappingId);
        //}

        /// <summary>
        /// Implementation of the GetColumnGroupName method
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns></returns>
        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mapping)
        {
            return null;
        }

        #endregion
    }
}
