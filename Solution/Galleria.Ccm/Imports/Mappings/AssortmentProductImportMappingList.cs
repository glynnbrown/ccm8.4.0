﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GFS 1.0)
// V8-25455 : J.Pickup
//  Copied over from GFS

#endregion

#region Version History: (CCM 8.3.0)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

using Galleria.Framework.Imports;

using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class AssortmentProductImportMappingList : ImportMappingList<AssortmentProductImportMappingList>
    {
        #region Constants

        public const Int32 ProductGTINMapId = 1;
        public const Int32 IsRangedMapId = 2;
        public const Int32 RankMapId = 3;
        public const Int32 SegmentationMapId = 4;
        public const Int32 FacingsMapId = 5;
        public const Int32 UnitsMapId = 6;
        public const Int32 ProductTreatmentTypeMapId = 7;
        public const Int32 CommentsMapId = 8;

        #endregion

        #region Constructors
        private AssortmentProductImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods

        public static AssortmentProductImportMappingList NewAssortmentProductImportMappingList()
        {
            AssortmentProductImportMappingList list = new AssortmentProductImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region AssortmentProductProperties

            this.Add(ImportMapping.NewImportMapping(ProductGTINMapId, String.Format(Message.AssortmentProduct_ProductGTIN, Product.GtinProperty.FriendlyName), Product.GtinProperty.Description, true, true, Product.GtinProperty.Type, Product.GtinProperty.DefaultValue, 0, 14, false));
            this.Add(ImportMapping.NewImportMapping(IsRangedMapId, String.Format(Message.AssortmentProduct_IsRanged, AssortmentProduct.IsRangedProperty.FriendlyName), AssortmentProduct.IsRangedProperty.Description, true, AssortmentProduct.IsRangedProperty.Type, AssortmentProduct.IsRangedProperty.DefaultValue, 0, 0, false));
            this.Add(ImportMapping.NewImportMapping(RankMapId, String.Format(Message.AssortmentProduct_Rank, AssortmentProduct.RankProperty.FriendlyName), AssortmentProduct.RankProperty.Description, true, AssortmentProduct.RankProperty.Type, AssortmentProduct.RankProperty.DefaultValue, 0, Int16.MaxValue, false));
            this.Add(ImportMapping.NewImportMapping(SegmentationMapId, String.Format(Message.AssortmentProduct_Segmentation, AssortmentProduct.SegmentationProperty.FriendlyName), AssortmentProduct.SegmentationProperty.Description, true, AssortmentProduct.SegmentationProperty.Type, AssortmentProduct.SegmentationProperty.DefaultValue, 0, 300, false));
            this.Add(ImportMapping.NewImportMapping(FacingsMapId, String.Format(Message.AssortmentProduct_Facings, AssortmentProduct.FacingsProperty.FriendlyName), AssortmentProduct.FacingsProperty.Description, true, AssortmentProduct.FacingsProperty.Type, AssortmentProduct.FacingsProperty.DefaultValue, 0, Byte.MaxValue, false));
            this.Add(ImportMapping.NewImportMapping(UnitsMapId, String.Format(Message.AssortmentProduct_Units, AssortmentProduct.UnitsProperty.FriendlyName), AssortmentProduct.UnitsProperty.Description, true, AssortmentProduct.UnitsProperty.Type, AssortmentProduct.UnitsProperty.DefaultValue, 0, Int16.MaxValue, false));
            this.Add(ImportMapping.NewImportMapping(ProductTreatmentTypeMapId, String.Format(Message.AssortmentProduct_ProductTreatmentType, AssortmentProduct.ProductTreatmentTypeProperty.FriendlyName), AssortmentProduct.ProductTreatmentTypeProperty.Description, true, AssortmentProduct.ProductTreatmentTypeProperty.Type, AssortmentProduct.ProductTreatmentTypeProperty.DefaultValue, 0, Byte.MaxValue, false));
            this.Add(ImportMapping.NewImportMapping(CommentsMapId, String.Format(Message.AssortmentProduct_Comments, AssortmentProduct.CommentsProperty.FriendlyName), AssortmentProduct.CommentsProperty.Description, true, AssortmentProduct.CommentsProperty.Type, AssortmentProduct.CommentsProperty.DefaultValue, 0, 100, false));

            #endregion

            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="assortmentProductDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, AssortmentProductDto assortmentProductDto)
        {
            switch (mappingId)
            {
                #region AssortmentProductProperties
                case AssortmentProductImportMappingList.ProductGTINMapId: return assortmentProductDto.ProductId;
                case AssortmentProductImportMappingList.IsRangedMapId: return assortmentProductDto.IsRanged;
                case AssortmentProductImportMappingList.RankMapId: return assortmentProductDto.Rank;
                case AssortmentProductImportMappingList.SegmentationMapId: return assortmentProductDto.Segmentation;
                case AssortmentProductImportMappingList.FacingsMapId: return assortmentProductDto.Facings;
                case AssortmentProductImportMappingList.UnitsMapId: return assortmentProductDto.Units;
                case AssortmentProductImportMappingList.ProductTreatmentTypeMapId: return assortmentProductDto.ProductTreatmentType;
                case AssortmentProductImportMappingList.CommentsMapId: return assortmentProductDto.Comments;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue">derived from initial cell value</param>
        /// <param name="assortmentProductDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue, AssortmentProductDto assortmentProductDto)
        {
            IFormatProvider prov = CultureInfo.CurrentCulture;

            switch (mappingId)
            {
                #region AssortmentProductProperties
                case AssortmentProductImportMappingList.ProductGTINMapId:
                    assortmentProductDto.ProductId = Convert.ToInt32(cellValue, prov);
                    break;
                case AssortmentProductImportMappingList.IsRangedMapId:
                    assortmentProductDto.IsRanged = Convert.ToBoolean(cellValue, prov);
                    break;
                case AssortmentProductImportMappingList.RankMapId:
                    assortmentProductDto.Rank = Convert.ToInt16(cellValue, prov);
                    break;
                case AssortmentProductImportMappingList.SegmentationMapId:
                    assortmentProductDto.Segmentation = Convert.ToString(cellValue, prov);
                    break;
                case AssortmentProductImportMappingList.FacingsMapId:
                    assortmentProductDto.Facings = Convert.ToByte(cellValue, prov);
                    break;
                case AssortmentProductImportMappingList.UnitsMapId:
                    assortmentProductDto.Units = Convert.ToInt16(cellValue, prov);
                    break;
                case AssortmentProductImportMappingList.ProductTreatmentTypeMapId:
                    assortmentProductDto.ProductTreatmentType = Convert.ToByte(cellValue, prov);
                    break;
                case AssortmentProductImportMappingList.CommentsMapId:
                    assortmentProductDto.Comments = Convert.ToString(cellValue, prov);
                    break;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override string GetBindingPath(int mappingId)
        {
            switch (mappingId)
            {
                #region AssortmentProductProperties
                case AssortmentProductImportMappingList.ProductGTINMapId: return AssortmentProduct.GtinProperty.Name;
                case AssortmentProductImportMappingList.IsRangedMapId: return AssortmentProduct.IsRangedProperty.Name;
                case AssortmentProductImportMappingList.RankMapId: return AssortmentProduct.RankProperty.Name;
                case AssortmentProductImportMappingList.SegmentationMapId: return AssortmentProduct.SegmentationProperty.Name;
                case AssortmentProductImportMappingList.FacingsMapId: return AssortmentProduct.FacingsProperty.Name;
                case AssortmentProductImportMappingList.UnitsMapId: return AssortmentProduct.UnitsProperty.Name;
                case AssortmentProductImportMappingList.ProductTreatmentTypeMapId: return AssortmentProduct.ProductTreatmentTypeProperty.Name;
                case AssortmentProductImportMappingList.CommentsMapId: return AssortmentProduct.CommentsProperty.Name;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        #endregion
    }
}