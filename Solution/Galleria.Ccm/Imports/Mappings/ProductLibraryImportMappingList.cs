﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created
// V8-26041 : A.Kuszyk
//      Updated Product.FillPattern to Product.FillPatternType.
//      Updated ProductDto.IsNew to ProductDto.IsNewProduct.
//      Changed use of CCM ProductOrientationType to Framework PlanogramProductOrientationType.
//      Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
//      Introduced PlanogramProductMerchandisingStyle for Product.Merchandising style.
//      Added ShapeType and changed FillColour to Int32.
// V8-27034 : A.Silva ~ Amended lower bound limit for Value Custom attributes.
// V8-27424 : L.Luong
//      Added FinancialGroupCode and FinancialGroupName
// V8-27571 : I.George ~ Added more Product properties for mapping
// V8-27648 : A.Probyn ~ Updated to include planogram performance data population if setup in mappings
// V8-28139 : L.Ineson
//  Performnance data fields are no longer mandatory.
// V8-27961 : A.Probyn
//  Corrected costruction of ImportMappings for enums to include friendly names.
// V8-27148 : A.Probyn
//  Added missing use case for IsNewMapId in SetValueByMappingId
// V8-28295 : A.Probyn
//  Updated so performance metrics are always shown regardless of a planogram being open/having metrics
// V8-28461 : A.Probyn
//  Extended for TrayPackUnits 
// V8-28707 : J.Pickup
//  Now forces the Orientation and Status cell values through to the enum helper in their lower invariant.
#endregion

#region Version History : CCM 801
// V8-27897 : A.Kuszyk
//  Added ShapeMapId, FillColourMapId and FillPatternMapId to GetBindingPath.
// V8-28895 : A.Kuszyk
//  Amended FillColour input range to full Int range.
// V8-28898 : A.Kuszyk
//  Fixed issue parsing ProductFillPatternType friendly names back to enum.
#endregion

#region Version History : CCM 802
// V8-29030 : J.Pickup
//  GetBindingPath now seperates the object and property names with '.' like custom attriubutes.
// V8-29112 : I.George
//  Removed AchievedCasePacks, AchievedDeliveries,AchievedDos,AchievedShelfLife,UnitsSoldPerDay Performance fields from being mapped
#endregion

#region Version History : CCM 830
// V8-31531 : A.Heathcote
//  Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion

#region Version History: CCM 840
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data
#endregion

#endregion

using System;
using System.Globalization;
using System.Linq;
using Galleria.Framework.Imports;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public sealed class ProductLibraryImportMappingList : ImportMappingList<ProductLibraryImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        //Added gaps between numbers so map ids can be added in more logical places
        //without changing all following map ids

        public const Int32 GtinMapId = 2;
        public const Int32 NameMapId = 5;
        public const Int32 HeightMapId = 6;
        public const Int32 WidthMapId = 7;
        public const Int32 DepthMapId = 8;
        public const Int32 DisplayHeightMapId = 9;
        public const Int32 DisplayWidthMapId = 10;
        public const Int32 DisplayDepthMapId = 11;
        public const Int32 AlternateHeightMapId = 12;
        public const Int32 AlternateWidthMapId = 13;
        public const Int32 AlternateDepthMapId = 14;

        public const Int32 NumberOfPegHolesMapId = 15;
        public const Int32 PegXMapId = 16;
        public const Int32 PegX2MapId = 17;
        public const Int32 PegX3MapId = 18;
        public const Int32 PegYMapId = 19;
        public const Int32 PegY2MapId = 20;
        public const Int32 PegY3MapId = 21;
        public const Int32 PegProngOffsetXMapId = 22;
        public const Int32 PegDepthMapId = 23;

        public const Int32 SqueezeHeightMapId = 24;
        public const Int32 SqueezeWidthMapId = 25;
        public const Int32 SqueezeDepthMapId = 26;

        public const Int32 NestingHeightMapId = 27;
        public const Int32 NestingWidthMapId = 28;
        public const Int32 NestingDepthMapId = 29;

        public const Int32 CasePackUnitsMapId = 30;
        public const Int32 MaxStackMapId = 31;
        public const Int32 MaxTopCapMapId = 32;
        public const Int32 MaxRightCapMapId = 33;
        public const Int32 MinDeepMapId = 34;
        public const Int32 MaxDeepMapId = 35;

        public const Int32 TrayHighMapId = 38;
        public const Int32 TrayWideMapId = 39;
        public const Int32 TrayDeepMapId = 40;
        public const Int32 TrayThickHeightMapId = 41;
        public const Int32 TrayThickWidthMapId = 42;
        public const Int32 TrayThickDepthMapId = 43;

        public const Int32 FrontOverhangMapId = 44;
        public const Int32 FingerSpaceAboveMapId = 45;
        public const Int32 FingerSpaceToTheSideMapId = 46;

        public const Int32 StatusTypeMapId = 47;
        public const Int32 OrientationTypeMapId = 48;
        public const Int32 MerchandisingStyleMapId = 49;

        public const Int32 IsFrontOnlyMapId = 50;
        public const Int32 IsPlaceHolderProductMapId = 52;
        public const Int32 IsActiveMapId = 53;
        public const Int32 CanBreakTrayUpMapId = 54;
        public const Int32 CanBreakTrayDownMapId = 55;
        public const Int32 CanBreakTrayBackMapId = 56;
        public const Int32 CanBreakTrayTopMapId = 57;
        public const Int32 ForceMiddleCapMapId = 58;
        public const Int32 ForceBottomCapMapId = 59;
        public const Int32 PointOfPurchaseHeightMapId = 60;
        public const Int32 PointOfPurchaseWidthMapId = 61;
        public const Int32 PointOfPurchaseDepthMapId = 62;


        //product attribute data
        public const Int32 SubCategoryMapId = 63;
        public const Int32 ColourMapId = 64;
        public const Int32 FlavourMapId = 65;
        public const Int32 PackagingShapeMapId = 66;
        public const Int32 PackagingTypeMapId = 67;
        public const Int32 CountryOfOriginMapId = 68;
        public const Int32 CountryOfProcessingMapId = 69;

        public const Int32 ShelfLifeMapId = 70;
        public const Int32 DeliveryFrequencyDaysMapId = 71;
        public const Int32 DeliveryMethodMapId = 72;
        public const Int32 BrandMapId = 73;
        public const Int32 VendorCodeMapId = 74;
        public const Int32 VendorMapId = 75;
        public const Int32 ManufacturerCodeMapId = 76;
        public const Int32 ManufacturerMapId = 77;
        public const Int32 SizeMapId = 78;
        public const Int32 UnitOfMeasureMapId = 79;

        public const Int32 DateIntroducedMapId = 80;
        public const Int32 DateDiscontinuedMapId = 81;
        public const Int32 DateEffectiveMapId = 82;

        public const Int32 HealthMapId = 83;
        public const Int32 CorporateCodeMapId = 84;
        public const Int32 BarCodeMapId = 85;
        public const Int32 SellPriceMapId = 86;
        public const Int32 SellPackCountMapId = 87;
        public const Int32 SellPackDescriptionMapId = 88;
        public const Int32 RecommendedRetailPriceMapId = 89;
        public const Int32 ManufacturerRecommendedRetailPriceMapId = 90;
        public const Int32 CostPriceMapId = 91;
        public const Int32 CaseCostMapId = 92;
        public const Int32 TaxRateMapId = 93;

        public const Int32 ConsumerInformationMapId = 94;
        public const Int32 TextureMapId = 95;
        public const Int32 StyleNumberMapId = 96;
        public const Int32 PatternMapId = 97;
        public const Int32 ModelMapId = 98;
        public const Int32 GarmentTypeMapId = 99;

        public const Int32 IsPrivateLabelMapId = 100;
        public const Int32 IsNewMapId = 101;
        public const Int32 FinancialGroupCodeMapId = 102;
        public const Int32 FinancialGroupNameMapId = 103;
        public const Int32 PointOfPurchaseDescriptionMapId = 104;
        public const Int32 ShortDescriptionMapId = 105;

        //New CCM properties
        public const Int32 ShapeMapId = 106;
        public const Int32 FillColourMapId = 107;
        public const Int32 FillPatternMapId = 108;

        //custom fields
        public const Int32 CustomText1MapId = 109;
        public const Int32 CustomText2MapId = 110;
        public const Int32 CustomText3MapId = 111;
        public const Int32 CustomText4MapId = 112;
        public const Int32 CustomText5MapId = 113;
        public const Int32 CustomText6MapId = 114;
        public const Int32 CustomText7MapId = 115;
        public const Int32 CustomText8MapId = 116;
        public const Int32 CustomText9MapId = 117;
        public const Int32 CustomText10MapId = 118;
        public const Int32 CustomText11MapId = 119;
        public const Int32 CustomText12MapId = 120;
        public const Int32 CustomText13MapId = 121;
        public const Int32 CustomText14MapId = 122;
        public const Int32 CustomText15MapId = 123;
        public const Int32 CustomText16MapId = 124;
        public const Int32 CustomText17MapId = 125;
        public const Int32 CustomText18MapId = 126;
        public const Int32 CustomText19MapId = 127;
        public const Int32 CustomText20MapId = 128;
        public const Int32 CustomText21MapId = 129;
        public const Int32 CustomText22MapId = 130;
        public const Int32 CustomText23MapId = 131;
        public const Int32 CustomText24MapId = 132;
        public const Int32 CustomText25MapId = 133;
        public const Int32 CustomText26MapId = 134;
        public const Int32 CustomText27MapId = 135;
        public const Int32 CustomText28MapId = 136;
        public const Int32 CustomText29MapId = 137;
        public const Int32 CustomText30MapId = 138;
        public const Int32 CustomText31MapId = 139;
        public const Int32 CustomText32MapId = 140;
        public const Int32 CustomText33MapId = 141;
        public const Int32 CustomText34MapId = 142;
        public const Int32 CustomText35MapId = 143;
        public const Int32 CustomText36MapId = 144;
        public const Int32 CustomText37MapId = 145;
        public const Int32 CustomText38MapId = 146;
        public const Int32 CustomText39MapId = 147;
        public const Int32 CustomText40MapId = 148;
        public const Int32 CustomText41MapId = 149;
        public const Int32 CustomText42MapId = 150;
        public const Int32 CustomText43MapId = 151;
        public const Int32 CustomText44MapId = 152;
        public const Int32 CustomText45MapId = 153;
        public const Int32 CustomText46MapId = 154;
        public const Int32 CustomText47MapId = 155;
        public const Int32 CustomText48MapId = 156;
        public const Int32 CustomText49MapId = 157;
        public const Int32 CustomText50MapId = 158;

        public const Int32 CustomValue1MapId = 159;
        public const Int32 CustomValue2MapId = 160;
        public const Int32 CustomValue3MapId = 161;
        public const Int32 CustomValue4MapId = 162;
        public const Int32 CustomValue5MapId = 163;
        public const Int32 CustomValue6MapId = 164;
        public const Int32 CustomValue7MapId = 165;
        public const Int32 CustomValue8MapId = 166;
        public const Int32 CustomValue9MapId = 167;
        public const Int32 CustomValue10MapId = 168;
        public const Int32 CustomValue11MapId = 169;
        public const Int32 CustomValue12MapId = 170;
        public const Int32 CustomValue13MapId = 171;
        public const Int32 CustomValue14MapId = 172;
        public const Int32 CustomValue15MapId = 173;
        public const Int32 CustomValue16MapId = 174;
        public const Int32 CustomValue17MapId = 175;
        public const Int32 CustomValue18MapId = 176;
        public const Int32 CustomValue19MapId = 177;
        public const Int32 CustomValue20MapId = 178;
        public const Int32 CustomValue21MapId = 179;
        public const Int32 CustomValue22MapId = 180;
        public const Int32 CustomValue23MapId = 181;
        public const Int32 CustomValue24MapId = 182;
        public const Int32 CustomValue25MapId = 183;
        public const Int32 CustomValue26MapId = 184;
        public const Int32 CustomValue27MapId = 185;
        public const Int32 CustomValue28MapId = 186;
        public const Int32 CustomValue29MapId = 187;
        public const Int32 CustomValue30MapId = 188;
        public const Int32 CustomValue31MapId = 189;
        public const Int32 CustomValue32MapId = 190;
        public const Int32 CustomValue33MapId = 191;
        public const Int32 CustomValue34MapId = 192;
        public const Int32 CustomValue35MapId = 193;
        public const Int32 CustomValue36MapId = 194;
        public const Int32 CustomValue37MapId = 195;
        public const Int32 CustomValue38MapId = 196;
        public const Int32 CustomValue39MapId = 197;
        public const Int32 CustomValue40MapId = 198;
        public const Int32 CustomValue41MapId = 199;
        public const Int32 CustomValue42MapId = 200;
        public const Int32 CustomValue43MapId = 201;
        public const Int32 CustomValue44MapId = 202;
        public const Int32 CustomValue45MapId = 203;
        public const Int32 CustomValue46MapId = 204;
        public const Int32 CustomValue47MapId = 205;
        public const Int32 CustomValue48MapId = 206;
        public const Int32 CustomValue49MapId = 207;
        public const Int32 CustomValue50MapId = 208;

        public const Int32 CustomFlag1MapId = 209;
        public const Int32 CustomFlag2MapId = 210;
        public const Int32 CustomFlag3MapId = 211;
        public const Int32 CustomFlag4MapId = 212;
        public const Int32 CustomFlag5MapId = 213;
        public const Int32 CustomFlag6MapId = 214;
        public const Int32 CustomFlag7MapId = 215;
        public const Int32 CustomFlag8MapId = 216;
        public const Int32 CustomFlag9MapId = 217;
        public const Int32 CustomFlag10MapId = 218;

        public const Int32 CustomDate1MapId = 219;
        public const Int32 CustomDate2MapId = 220;
        public const Int32 CustomDate3MapId = 221;
        public const Int32 CustomDate4MapId = 222;
        public const Int32 CustomDate5MapId = 223;
        public const Int32 CustomDate6MapId = 224;
        public const Int32 CustomDate7MapId = 225;
        public const Int32 CustomDate8MapId = 226;
        public const Int32 CustomDate9MapId = 227;
        public const Int32 CustomDate10MapId = 228;
        public const Int32 TrayHeightMapId = 229;
        public const Int32 TrayWidthMapId = 230;
        public const Int32 TrayDepthMapId = 231;
        public const Int32 CaseHeightMapId = 232;
        public const Int32 CaseWidthMapId = 233;
        public const Int32 CaseDepthMapId = 234;
        public const Int32 CaseHighMapId = 235;
        public const Int32 CaseWideMapId = 236;
        public const Int32 CaseDeepMapId = 237;
        public const Int32 CustomerStatusMapId = 238;
        public const Int32 ShapeTypeMapId = 239;
        //public const Int32 AchievedCasePacksMapId = 240;
        //public const Int32 AchievedDeliveriesMapId = 241;
        //public const Int32 AchievedDosMapId = 242;
        //public const Int32 AchievedShelfLifeMapId = 243;
        //public const Int32 UnitsSoldPerDayMapId = 244;
        public const Int32 PerformanceMetric1MapId = 245;
        public const Int32 PerformanceMetric2MapId = 246;
        public const Int32 PerformanceMetric3MapId = 247;
        public const Int32 PerformanceMetric4MapId = 248;
        public const Int32 PerformanceMetric5MapId = 249;
        public const Int32 PerformanceMetric6MapId = 250;
        public const Int32 PerformanceMetric7MapId = 251;
        public const Int32 PerformanceMetric8MapId = 252;
        public const Int32 PerformanceMetric9MapId = 253;
        public const Int32 PerformanceMetric10MapId = 254;
        public const Int32 PerformanceMetric11MapId = 255;
        public const Int32 PerformanceMetric12MapId = 256;
        public const Int32 PerformanceMetric13MapId = 257;
        public const Int32 PerformanceMetric14MapId = 258;
        public const Int32 PerformanceMetric15MapId = 259;
        public const Int32 PerformanceMetric16MapId = 260;
        public const Int32 PerformanceMetric17MapId = 261;
        public const Int32 PerformanceMetric18MapId = 262;
        public const Int32 PerformanceMetric19MapId = 263;
        public const Int32 PerformanceMetric20MapId = 264;

        public const Int32 TrayPackUnitsMapId = 265;

        public const Int32 CustomNote1MapId = 266;
        public const Int32 CustomNote2MapId = 267;
        public const Int32 CustomNote3MapId = 268;
        public const Int32 CustomNote4MapId = 269;
        public const Int32 CustomNote5MapId = 270;
        #endregion

        #region Constructors

        private ProductLibraryImportMappingList() { } // force use of factory methods

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        public static ProductLibraryImportMappingList NewProductLibraryImportMappingList()
        {
            ProductLibraryImportMappingList list = new ProductLibraryImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create()
        {
            this.RaiseListChangedEvents = false;

            #region Product Properties

            this.Add(ImportMapping.NewImportMapping(GtinMapId, Product.GtinProperty.FriendlyName, Product.GtinProperty.Description, true, true, Product.GtinProperty.Type, Product.GtinProperty.DefaultValue, 1, 14, false));
            this.Add(ImportMapping.NewImportMapping(NameMapId, Product.NameProperty.FriendlyName, Product.NameProperty.Description, true, Product.NameProperty.Type, Product.NameProperty.DefaultValue, 1, 100, true));

            this.Add(ImportMapping.NewImportMapping(HeightMapId, Product.HeightProperty.FriendlyName, Product.HeightProperty.Description, false, Product.HeightProperty.Type, Product.HeightProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(WidthMapId, Product.WidthProperty.FriendlyName, Product.WidthProperty.Description, false, Product.WidthProperty.Type, Product.WidthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(DepthMapId, Product.DepthProperty.FriendlyName, Product.DepthProperty.Description, false, Product.DepthProperty.Type, Product.DepthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(DisplayHeightMapId, Product.DisplayHeightProperty.FriendlyName, Product.DisplayHeightProperty.Description, false, Product.DisplayHeightProperty.Type, Product.DisplayHeightProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(DisplayWidthMapId, Product.DisplayWidthProperty.FriendlyName, Product.DisplayWidthProperty.Description, false, Product.DisplayWidthProperty.Type, Product.DisplayWidthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(DisplayDepthMapId, Product.DisplayDepthProperty.FriendlyName, Product.DisplayDepthProperty.Description, false, Product.DisplayDepthProperty.Type, Product.DisplayDepthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(AlternateHeightMapId, Product.AlternateHeightProperty.FriendlyName, Product.AlternateHeightProperty.Description, false, Product.AlternateHeightProperty.Type, Product.AlternateHeightProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(AlternateWidthMapId, Product.AlternateWidthProperty.FriendlyName, Product.AlternateWidthProperty.Description, false, Product.AlternateWidthProperty.Type, Product.AlternateWidthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(AlternateDepthMapId, Product.AlternateDepthProperty.FriendlyName, Product.AlternateDepthProperty.Description, false, Product.AlternateDepthProperty.Type, Product.AlternateDepthProperty.DefaultValue, 0, Int32.MaxValue, true));

            this.Add(ImportMapping.NewImportMapping(NumberOfPegHolesMapId, Product.NumberOfPegHolesProperty.FriendlyName, Product.NumberOfPegHolesProperty.Description, false, Product.NumberOfPegHolesProperty.Type, Product.NumberOfPegHolesProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PegXMapId, Product.PegXProperty.FriendlyName, Product.PegXProperty.Description, false, Product.PegXProperty.Type, Product.PegXProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PegX2MapId, Product.PegX2Property.FriendlyName, Product.PegX2Property.Description, false, Product.PegX2Property.Type, Product.PegX2Property.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PegX3MapId, Product.PegX3Property.FriendlyName, Product.PegX3Property.Description, false, Product.PegX3Property.Type, Product.PegX3Property.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PegYMapId, Product.PegYProperty.FriendlyName, Product.PegYProperty.Description, false, Product.PegYProperty.Type, Product.PegYProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PegY2MapId, Product.PegY2Property.FriendlyName, Product.PegY2Property.Description, false, Product.PegY2Property.Type, Product.PegY2Property.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PegY3MapId, Product.PegY3Property.FriendlyName, Product.PegY3Property.Description, false, Product.PegY3Property.Type, Product.PegY3Property.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PegProngOffsetXMapId, Product.PegProngOffsetXProperty.FriendlyName, Product.PegProngOffsetXProperty.Description, false, Product.PegProngOffsetXProperty.Type, Product.PegProngOffsetXProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PegDepthMapId, Product.PegDepthProperty.FriendlyName, Product.PegDepthProperty.Description, false, Product.PegDepthProperty.Type, Product.PegDepthProperty.DefaultValue, 0, Int32.MaxValue, true));

            this.Add(ImportMapping.NewImportMapping(SqueezeHeightMapId, Product.SqueezeHeightProperty.FriendlyName, Product.SqueezeHeightProperty.Description, false, Product.SqueezeHeightProperty.Type, Product.SqueezeHeightProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(SqueezeWidthMapId, Product.SqueezeWidthProperty.FriendlyName, Product.SqueezeWidthProperty.Description, false, Product.SqueezeWidthProperty.Type, Product.SqueezeWidthProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(SqueezeDepthMapId, Product.SqueezeDepthProperty.FriendlyName, Product.SqueezeDepthProperty.Description, false, Product.SqueezeDepthProperty.Type, Product.SqueezeDepthProperty.DefaultValue, 0, 1, true));

            this.Add(ImportMapping.NewImportMapping(NestingHeightMapId, Product.NestingHeightProperty.FriendlyName, Product.NestingHeightProperty.Description, false, Product.NestingHeightProperty.Type, Product.NestingHeightProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(NestingWidthMapId, Product.NestingWidthProperty.FriendlyName, Product.NestingWidthProperty.Description, false, Product.NestingWidthProperty.Type, Product.NestingWidthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(NestingDepthMapId, Product.NestingDepthProperty.FriendlyName, Product.NestingDepthProperty.Description, false, Product.NestingDepthProperty.Type, Product.NestingDepthProperty.DefaultValue, 0, Int32.MaxValue, true));

            this.Add(ImportMapping.NewImportMapping(CasePackUnitsMapId, Product.CasePackUnitsProperty.FriendlyName, Product.CasePackUnitsProperty.Description, false, Product.CasePackUnitsProperty.Type, Product.CasePackUnitsProperty.DefaultValue, 0, Int16.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(MaxStackMapId, Product.MaxStackProperty.FriendlyName, Product.MaxStackProperty.Description, false, Product.MaxStackProperty.Type, Product.MaxStackProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(MaxTopCapMapId, Product.MaxTopCapProperty.FriendlyName, Product.MaxTopCapProperty.Description, false, Product.MaxTopCapProperty.Type, Product.MaxTopCapProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(MaxRightCapMapId, Product.MaxRightCapProperty.FriendlyName, Product.MaxRightCapProperty.Description, false, Product.MaxRightCapProperty.Type, Product.MaxRightCapProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(MinDeepMapId, Product.MinDeepProperty.FriendlyName, Product.MinDeepProperty.Description, false, Product.MinDeepProperty.Type, Product.MinDeepProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(MaxDeepMapId, Product.MaxDeepProperty.FriendlyName, Product.MaxDeepProperty.Description, false, Product.MaxDeepProperty.Type, Product.MaxDeepProperty.DefaultValue, 0, Byte.MaxValue, true));

            this.Add(ImportMapping.NewImportMapping(TrayHighMapId, Product.TrayHighProperty.FriendlyName, Product.TrayHighProperty.Description, false, Product.TrayHighProperty.Type, Product.TrayHighProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TrayWideMapId, Product.TrayWideProperty.FriendlyName, Product.TrayWideProperty.Description, false, Product.TrayWideProperty.Type, Product.TrayWideProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TrayDeepMapId, Product.TrayDeepProperty.FriendlyName, Product.TrayDeepProperty.Description, false, Product.TrayDeepProperty.Type, Product.TrayDeepProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TrayThickHeightMapId, Product.TrayThickHeightProperty.FriendlyName, Product.TrayThickHeightProperty.Description, false, Product.TrayThickHeightProperty.Type, Product.TrayThickHeightProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TrayThickWidthMapId, Product.TrayThickWidthProperty.FriendlyName, Product.TrayThickWidthProperty.Description, false, Product.TrayThickWidthProperty.Type, Product.TrayThickWidthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TrayThickDepthMapId, Product.TrayThickDepthProperty.FriendlyName, Product.TrayThickDepthProperty.Description, false, Product.TrayThickDepthProperty.Type, Product.TrayThickDepthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TrayPackUnitsMapId, Product.TrayPackUnitsProperty.FriendlyName, Product.TrayPackUnitsProperty.Description, false, Product.TrayPackUnitsProperty.Type, Product.TrayPackUnitsProperty.DefaultValue, 0, Int16.MaxValue, true));

            this.Add(ImportMapping.NewImportMapping(FrontOverhangMapId, Product.FrontOverhangProperty.FriendlyName, Product.FrontOverhangProperty.Description, false, Product.FrontOverhangProperty.Type, Product.FrontOverhangProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(FingerSpaceAboveMapId, Product.FingerSpaceAboveProperty.FriendlyName, Product.FingerSpaceAboveProperty.Description, false, Product.FingerSpaceAboveProperty.Type, Product.FingerSpaceAboveProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(FingerSpaceToTheSideMapId, Product.FingerSpaceToTheSideProperty.FriendlyName, Product.FingerSpaceToTheSideProperty.Description, false, Product.FingerSpaceToTheSideProperty.Type, Product.FingerSpaceToTheSideProperty.DefaultValue, 0, Int32.MaxValue, true));

            this.Add(ImportMapping.NewImportMapping(StatusTypeMapId, Product.StatusTypeProperty.FriendlyName, Product.StatusTypeProperty.Description, false, false, Product.StatusTypeProperty.Type, Product.StatusTypeProperty.DefaultValue, PlanogramProductStatusTypeHelper.FriendlyNames, true));
            this.Add(ImportMapping.NewImportMapping(OrientationTypeMapId, Product.OrientationTypeProperty.FriendlyName, Product.OrientationTypeProperty.Description, false, false, Product.OrientationTypeProperty.Type, Product.OrientationTypeProperty.DefaultValue, PlanogramProductOrientationTypeHelper.FriendlyNames, true));
            this.Add(ImportMapping.NewImportMapping(MerchandisingStyleMapId, Product.MerchandisingStyleProperty.FriendlyName, Product.MerchandisingStyleProperty.Description, false, false, Product.MerchandisingStyleProperty.Type, Product.MerchandisingStyleProperty.DefaultValue, PlanogramProductMerchandisingStyleHelper.FriendlyNames, true));
            
            this.Add(ImportMapping.NewImportMapping(IsFrontOnlyMapId, Product.IsFrontOnlyProperty.FriendlyName, Product.IsFrontOnlyProperty.Description, false, Product.IsFrontOnlyProperty.Type, Product.IsFrontOnlyProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(IsActiveMapId, Product.IsActiveProperty.FriendlyName, Product.IsActiveProperty.Description, false, Product.IsActiveProperty.Type, Product.IsActiveProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CanBreakTrayUpMapId, Product.CanBreakTrayUpProperty.FriendlyName, Product.CanBreakTrayUpProperty.Description, false, Product.CanBreakTrayUpProperty.Type, Product.CanBreakTrayUpProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CanBreakTrayDownMapId, Product.CanBreakTrayDownProperty.FriendlyName, Product.CanBreakTrayDownProperty.Description, false, Product.CanBreakTrayDownProperty.Type, Product.CanBreakTrayDownProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CanBreakTrayBackMapId, Product.CanBreakTrayBackProperty.FriendlyName, Product.CanBreakTrayBackProperty.Description, false, Product.CanBreakTrayBackProperty.Type, Product.CanBreakTrayBackProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CanBreakTrayTopMapId, Product.CanBreakTrayTopProperty.FriendlyName, Product.CanBreakTrayTopProperty.Description, false, Product.CanBreakTrayTopProperty.Type, Product.CanBreakTrayTopProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(ForceMiddleCapMapId, Product.ForceMiddleCapProperty.FriendlyName, Product.ForceMiddleCapProperty.Description, false, Product.ForceMiddleCapProperty.Type, Product.ForceMiddleCapProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(ForceBottomCapMapId, Product.ForceBottomCapProperty.FriendlyName, Product.ForceBottomCapProperty.Description, false, Product.ForceBottomCapProperty.Type, Product.ForceBottomCapProperty.DefaultValue, 0, 1, true));

            this.Add(ImportMapping.NewImportMapping(PointOfPurchaseHeightMapId, Product.PointOfPurchaseHeightProperty.FriendlyName, Product.PointOfPurchaseHeightProperty.Description, false, Product.PointOfPurchaseHeightProperty.Type, Product.PointOfPurchaseHeightProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PointOfPurchaseWidthMapId, Product.PointOfPurchaseWidthProperty.FriendlyName, Product.PointOfPurchaseWidthProperty.Description, false, Product.PointOfPurchaseWidthProperty.Type, Product.PointOfPurchaseWidthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PointOfPurchaseDepthMapId, Product.PointOfPurchaseDepthProperty.FriendlyName, Product.PointOfPurchaseDepthProperty.Description, false, Product.PointOfPurchaseDepthProperty.Type, Product.PointOfPurchaseDepthProperty.DefaultValue, 0, Int32.MaxValue, true));

            //CCM properties
            this.Add(ImportMapping.NewImportMapping(ShapeMapId, Product.ShapeProperty.FriendlyName, Product.ShapeProperty.Description, false, Product.ShapeProperty.Type, Product.ShapeProperty.DefaultValue, 0, 150, true));
            this.Add(ImportMapping.NewImportMapping(FillColourMapId, Product.FillColourProperty.FriendlyName, Product.FillColourProperty.Description, false, Product.FillColourProperty.Type, Product.FillColourProperty.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(FillPatternMapId, Product.FillPatternTypeProperty.FriendlyName, Product.FillPatternTypeProperty.Description, false, false, Product.FillPatternTypeProperty.Type, Product.FillPatternTypeProperty.DefaultValue, PlanogramProductFillPatternTypeHelper.FriendlyNames, true));

            //Product attribute data
            this.Add(ImportMapping.NewImportMapping(SubCategoryMapId, Product.SubcategoryProperty.FriendlyName, Product.SubcategoryProperty.Description, false, Product.SubcategoryProperty.Type, Product.SubcategoryProperty.DefaultValue, 1, 50, true));
            this.Add(ImportMapping.NewImportMapping(ColourMapId, Product.ColourProperty.FriendlyName, Product.ColourProperty.Description, false, Product.ColourProperty.Type, Product.ColourProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(FlavourMapId, Product.FlavourProperty.FriendlyName, Product.FlavourProperty.Description, false, Product.FlavourProperty.Type, Product.FlavourProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(PackagingShapeMapId, Product.PackagingShapeProperty.FriendlyName, Product.PackagingShapeProperty.Description, false, Product.PackagingShapeProperty.Type, Product.PackagingShapeProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(PackagingTypeMapId, Product.PackagingTypeProperty.FriendlyName, Product.PackagingTypeProperty.Description, false, Product.PackagingTypeProperty.Type, Product.PackagingTypeProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(CountryOfOriginMapId, Product.CountryOfOriginProperty.FriendlyName, Product.CountryOfOriginProperty.Description, false, Product.CountryOfOriginProperty.Type, Product.CountryOfOriginProperty.DefaultValue, 1, 40, true));
            this.Add(ImportMapping.NewImportMapping(CountryOfProcessingMapId, Product.CountryOfProcessingProperty.FriendlyName, Product.CountryOfProcessingProperty.Description, false, Product.CountryOfProcessingProperty.Type, Product.CountryOfProcessingProperty.DefaultValue, 1, 40, true));

            this.Add(ImportMapping.NewImportMapping(ShelfLifeMapId, Product.ShelfLifeProperty.FriendlyName, Product.ShelfLifeProperty.Description, false, Product.ShelfLifeProperty.Type, Product.ShelfLifeProperty.DefaultValue, 0, Int16.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(DeliveryFrequencyDaysMapId, Product.DeliveryFrequencyDaysProperty.FriendlyName, Product.DeliveryFrequencyDaysProperty.Description, false, Product.DeliveryFrequencyDaysProperty.Type, Product.DeliveryFrequencyDaysProperty.DefaultValue, 0, Int16.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(DeliveryMethodMapId, Product.DeliveryMethodProperty.FriendlyName, Product.DeliveryMethodProperty.Description, false, Product.DeliveryMethodProperty.Type, Product.DeliveryMethodProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(BrandMapId, Product.BrandProperty.FriendlyName, Product.BrandProperty.Description, false, Product.BrandProperty.Type, Product.BrandProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(VendorCodeMapId, Product.VendorCodeProperty.FriendlyName, Product.VendorCodeProperty.Description, false, Product.VendorCodeProperty.Type, Product.VendorCodeProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(VendorMapId, Product.VendorProperty.FriendlyName, Product.VendorProperty.Description, false, Product.VendorProperty.Type, Product.VendorProperty.DefaultValue, 1, 50, true));
            this.Add(ImportMapping.NewImportMapping(ManufacturerCodeMapId, Product.ManufacturerCodeProperty.FriendlyName, Product.ManufacturerCodeProperty.Description, false, Product.ManufacturerCodeProperty.Type, Product.ManufacturerCodeProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(ManufacturerMapId, Product.ManufacturerProperty.FriendlyName, Product.ManufacturerProperty.Description, false, Product.ManufacturerProperty.Type, Product.ManufacturerProperty.DefaultValue, 1, 50, true));
            this.Add(ImportMapping.NewImportMapping(SizeMapId, Product.SizeProperty.FriendlyName, Product.SizeProperty.Description, false, Product.SizeProperty.Type, Product.SizeProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(UnitOfMeasureMapId, Product.UnitOfMeasureProperty.FriendlyName, Product.UnitOfMeasureProperty.Description, false, Product.UnitOfMeasureProperty.Type, Product.UnitOfMeasureProperty.DefaultValue, 1, 20, true));

            this.Add(ImportMapping.NewImportMapping(DateIntroducedMapId, Product.DateIntroducedProperty.FriendlyName, Product.DateIntroducedProperty.Description, false, Product.DateIntroducedProperty.Type, Product.DateIntroducedProperty.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(DateDiscontinuedMapId, Product.DateDiscontinuedProperty.FriendlyName, Product.DateDiscontinuedProperty.Description, false, Product.DateDiscontinuedProperty.Type, Product.DateDiscontinuedProperty.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(DateEffectiveMapId, Product.DateEffectiveProperty.FriendlyName, Product.DateEffectiveProperty.Description, false, Product.DateEffectiveProperty.Type, Product.DateEffectiveProperty.DefaultValue, 0, 0, true));

            this.Add(ImportMapping.NewImportMapping(HealthMapId, Product.HealthProperty.FriendlyName, Product.HealthProperty.Description, false, Product.HealthProperty.Type, Product.HealthProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(CorporateCodeMapId, Product.CorporateCodeProperty.FriendlyName, Product.CorporateCodeProperty.Description, false, Product.CorporateCodeProperty.Type, Product.CorporateCodeProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(BarCodeMapId, Product.BarcodeProperty.FriendlyName, Product.BarcodeProperty.Description, false, Product.BarcodeProperty.Type, Product.BarcodeProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(SellPriceMapId, Product.SellPriceProperty.FriendlyName, Product.SellPriceProperty.Description, false, Product.SellPriceProperty.Type, Product.SellPriceProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(SellPackCountMapId, Product.SellPackCountProperty.FriendlyName, Product.SellPackCountProperty.Description, false, Product.SellPackCountProperty.Type, Product.SellPackCountProperty.DefaultValue, 0, Int16.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(SellPackDescriptionMapId, Product.SellPackDescriptionProperty.FriendlyName, Product.SellPackDescriptionProperty.Description, false, Product.SellPackDescriptionProperty.Type, Product.SellPackDescriptionProperty.DefaultValue, 1, 50, true));
            this.Add(ImportMapping.NewImportMapping(RecommendedRetailPriceMapId, Product.RecommendedRetailPriceProperty.FriendlyName, Product.RecommendedRetailPriceProperty.Description, false, Product.RecommendedRetailPriceProperty.Type, Product.RecommendedRetailPriceProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(ManufacturerRecommendedRetailPriceMapId, Product.ManufacturerRecommendedRetailPriceProperty.FriendlyName, Product.ManufacturerRecommendedRetailPriceProperty.Description, false, Product.ManufacturerRecommendedRetailPriceProperty.Type, Product.ManufacturerRecommendedRetailPriceProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CostPriceMapId, Product.CostPriceProperty.FriendlyName, Product.CostPriceProperty.Description, false, Product.CostPriceProperty.Type, Product.CostPriceProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CaseCostMapId, Product.CaseCostProperty.FriendlyName, Product.CaseCostProperty.Description, false, Product.CaseCostProperty.Type, Product.CaseCostProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TaxRateMapId, Product.TaxRateProperty.FriendlyName, Product.TaxRateProperty.Description, false, Product.TaxRateProperty.Type, Product.TaxRateProperty.DefaultValue, 0, Int32.MaxValue, true));

            this.Add(ImportMapping.NewImportMapping(ConsumerInformationMapId, Product.ConsumerInformationProperty.FriendlyName, Product.ConsumerInformationProperty.Description, false, Product.ConsumerInformationProperty.Type, Product.ConsumerInformationProperty.DefaultValue, 1, 50, true));
            this.Add(ImportMapping.NewImportMapping(TextureMapId, Product.TextureProperty.FriendlyName, Product.TextureProperty.Description, false, Product.TextureProperty.Type, Product.TextureProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(StyleNumberMapId, Product.StyleNumberProperty.FriendlyName, Product.StyleNumberProperty.Description, false, Product.StyleNumberProperty.Type, Product.StyleNumberProperty.DefaultValue, 0, Int16.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PatternMapId, Product.PatternProperty.FriendlyName, Product.PatternProperty.Description, false, Product.PatternProperty.Type, Product.PatternProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(ModelMapId, Product.ModelProperty.FriendlyName, Product.ModelProperty.Description, false, Product.ModelProperty.Type, Product.ModelProperty.DefaultValue, 1, 20, true));
            this.Add(ImportMapping.NewImportMapping(GarmentTypeMapId, Product.GarmentTypeProperty.FriendlyName, Product.GarmentTypeProperty.Description, false, Product.GarmentTypeProperty.Type, Product.GarmentTypeProperty.DefaultValue, 1, 20, true));

            this.Add(ImportMapping.NewImportMapping(IsPrivateLabelMapId, Product.IsPrivateLabelProperty.FriendlyName, Product.IsPrivateLabelProperty.Description, false, Product.IsPrivateLabelProperty.Type, Product.IsPrivateLabelProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(IsNewMapId, Product.IsNewProductProperty.FriendlyName, Product.IsNewProductProperty.Description, false, Product.IsNewProductProperty.Type, Product.IsNewProductProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(FinancialGroupCodeMapId, Product.FinancialGroupCodeProperty.FriendlyName, Product.FinancialGroupCodeProperty.Description, false, Product.FinancialGroupCodeProperty.Type, Product.FinancialGroupCodeProperty.DefaultValue, 1, 50, true));
            this.Add(ImportMapping.NewImportMapping(FinancialGroupNameMapId, Product.FinancialGroupNameProperty.FriendlyName, Product.FinancialGroupNameProperty.Description, false, Product.FinancialGroupNameProperty.Type, Product.FinancialGroupNameProperty.DefaultValue, 1, 100, true));

            this.Add(ImportMapping.NewImportMapping(PointOfPurchaseDescriptionMapId, Product.PointOfPurchaseDescriptionProperty.FriendlyName, Product.PointOfPurchaseDescriptionProperty.Description, false, Product.PointOfPurchaseDescriptionProperty.Type, Product.PointOfPurchaseDescriptionProperty.DefaultValue, 0, 50, true));
            this.Add(ImportMapping.NewImportMapping(ShortDescriptionMapId, Product.ShortDescriptionProperty.FriendlyName, Product.ShortDescriptionProperty.Description, false, Product.ShortDescriptionProperty.Type, Product.ShortDescriptionProperty.DefaultValue, 0, 20, true));

            //new 
            this.Add(ImportMapping.NewImportMapping(TrayHeightMapId, Product.TrayHeightProperty.FriendlyName, Product.TrayHeightProperty.Description, false, Product.TrayHeightProperty.Type, Product.TrayHeightProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TrayWidthMapId, Product.TrayWidthProperty.FriendlyName, Product.TrayWidthProperty.Description, false, Product.TrayWidthProperty.Type, Product.TrayWidthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(TrayDepthMapId, Product.TrayDepthProperty.FriendlyName, Product.TrayDepthProperty.Description, false, Product.TrayDepthProperty.Type, Product.TrayDepthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CaseHeightMapId, Product.CaseHeightProperty.FriendlyName, Product.CaseHeightProperty.Description, false, Product.CaseHeightProperty.Type, Product.CaseHeightProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CaseWidthMapId, Product.CaseWidthProperty.FriendlyName, Product.CaseWidthProperty.Description, false, Product.CaseWidthProperty.Type, Product.CaseWidthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CaseDepthMapId, Product.CaseDepthProperty.FriendlyName, Product.CaseDepthProperty.Description, false, Product.CaseDepthProperty.Type, Product.CaseDepthProperty.DefaultValue, 0, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CaseHighMapId, Product.CaseHighProperty.FriendlyName, Product.CaseHighProperty.Description, false, Product.CaseHighProperty.Type, Product.CaseHighProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CaseWideMapId, Product.CaseWideProperty.FriendlyName, Product.CaseWideProperty.Description, false, Product.CaseWideProperty.Type, Product.CaseWideProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CaseDeepMapId, Product.CaseDeepProperty.FriendlyName, Product.CaseDeepProperty.Description, false, Product.CaseDeepProperty.Type, Product.CaseDeepProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(IsPlaceHolderProductMapId, Product.IsPlaceHolderProductProperty.FriendlyName, Product.IsPlaceHolderProductProperty.Description, false, Product.IsPlaceHolderProductProperty.Type, Product.IsPlaceHolderProductProperty.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomerStatusMapId, Product.CustomerStatusProperty.FriendlyName, Product.CustomerStatusProperty.Description, false, Product.CustomerStatusProperty.Type, Product.CustomerStatusProperty.DefaultValue, 0, 25, true));
            this.Add(ImportMapping.NewImportMapping(ShapeTypeMapId, Product.ShapeTypeProperty.FriendlyName, Product.ShapeTypeProperty.Description, false, false, Product.ShapeTypeProperty.Type, Product.ShapeTypeProperty.DefaultValue, PlanogramProductShapeTypeHelper.FriendlyNames, true));



            //custom text fields
            this.Add(ImportMapping.NewImportMapping(CustomText1MapId, CustomAttributeData.Text1Property.FriendlyName, CustomAttributeData.Text1Property.Description, false, CustomAttributeData.Text1Property.Type, CustomAttributeData.Text1Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText2MapId, CustomAttributeData.Text2Property.FriendlyName, CustomAttributeData.Text2Property.Description, false, CustomAttributeData.Text2Property.Type, CustomAttributeData.Text2Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText3MapId, CustomAttributeData.Text3Property.FriendlyName, CustomAttributeData.Text3Property.Description, false, CustomAttributeData.Text3Property.Type, CustomAttributeData.Text3Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText4MapId, CustomAttributeData.Text4Property.FriendlyName, CustomAttributeData.Text4Property.Description, false, CustomAttributeData.Text4Property.Type, CustomAttributeData.Text4Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText5MapId, CustomAttributeData.Text5Property.FriendlyName, CustomAttributeData.Text5Property.Description, false, CustomAttributeData.Text5Property.Type, CustomAttributeData.Text5Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText6MapId, CustomAttributeData.Text6Property.FriendlyName, CustomAttributeData.Text6Property.Description, false, CustomAttributeData.Text6Property.Type, CustomAttributeData.Text6Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText7MapId, CustomAttributeData.Text7Property.FriendlyName, CustomAttributeData.Text7Property.Description, false, CustomAttributeData.Text7Property.Type, CustomAttributeData.Text7Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText8MapId, CustomAttributeData.Text8Property.FriendlyName, CustomAttributeData.Text8Property.Description, false, CustomAttributeData.Text8Property.Type, CustomAttributeData.Text8Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText9MapId, CustomAttributeData.Text9Property.FriendlyName, CustomAttributeData.Text9Property.Description, false, CustomAttributeData.Text9Property.Type, CustomAttributeData.Text9Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText10MapId, CustomAttributeData.Text10Property.FriendlyName, CustomAttributeData.Text10Property.Description, false, CustomAttributeData.Text10Property.Type, CustomAttributeData.Text10Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText11MapId, CustomAttributeData.Text11Property.FriendlyName, CustomAttributeData.Text11Property.Description, false, CustomAttributeData.Text11Property.Type, CustomAttributeData.Text11Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText12MapId, CustomAttributeData.Text12Property.FriendlyName, CustomAttributeData.Text12Property.Description, false, CustomAttributeData.Text12Property.Type, CustomAttributeData.Text12Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText13MapId, CustomAttributeData.Text13Property.FriendlyName, CustomAttributeData.Text13Property.Description, false, CustomAttributeData.Text13Property.Type, CustomAttributeData.Text13Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText14MapId, CustomAttributeData.Text14Property.FriendlyName, CustomAttributeData.Text14Property.Description, false, CustomAttributeData.Text14Property.Type, CustomAttributeData.Text14Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText15MapId, CustomAttributeData.Text15Property.FriendlyName, CustomAttributeData.Text15Property.Description, false, CustomAttributeData.Text15Property.Type, CustomAttributeData.Text15Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText16MapId, CustomAttributeData.Text16Property.FriendlyName, CustomAttributeData.Text16Property.Description, false, CustomAttributeData.Text16Property.Type, CustomAttributeData.Text16Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText17MapId, CustomAttributeData.Text17Property.FriendlyName, CustomAttributeData.Text17Property.Description, false, CustomAttributeData.Text17Property.Type, CustomAttributeData.Text17Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText18MapId, CustomAttributeData.Text18Property.FriendlyName, CustomAttributeData.Text18Property.Description, false, CustomAttributeData.Text18Property.Type, CustomAttributeData.Text18Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText19MapId, CustomAttributeData.Text19Property.FriendlyName, CustomAttributeData.Text19Property.Description, false, CustomAttributeData.Text19Property.Type, CustomAttributeData.Text19Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText20MapId, CustomAttributeData.Text20Property.FriendlyName, CustomAttributeData.Text20Property.Description, false, CustomAttributeData.Text20Property.Type, CustomAttributeData.Text20Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText21MapId, CustomAttributeData.Text21Property.FriendlyName, CustomAttributeData.Text21Property.Description, false, CustomAttributeData.Text21Property.Type, CustomAttributeData.Text21Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText22MapId, CustomAttributeData.Text22Property.FriendlyName, CustomAttributeData.Text22Property.Description, false, CustomAttributeData.Text22Property.Type, CustomAttributeData.Text22Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText23MapId, CustomAttributeData.Text23Property.FriendlyName, CustomAttributeData.Text23Property.Description, false, CustomAttributeData.Text23Property.Type, CustomAttributeData.Text23Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText24MapId, CustomAttributeData.Text24Property.FriendlyName, CustomAttributeData.Text24Property.Description, false, CustomAttributeData.Text24Property.Type, CustomAttributeData.Text24Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText25MapId, CustomAttributeData.Text25Property.FriendlyName, CustomAttributeData.Text25Property.Description, false, CustomAttributeData.Text25Property.Type, CustomAttributeData.Text25Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText26MapId, CustomAttributeData.Text26Property.FriendlyName, CustomAttributeData.Text26Property.Description, false, CustomAttributeData.Text26Property.Type, CustomAttributeData.Text26Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText27MapId, CustomAttributeData.Text27Property.FriendlyName, CustomAttributeData.Text27Property.Description, false, CustomAttributeData.Text27Property.Type, CustomAttributeData.Text27Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText28MapId, CustomAttributeData.Text28Property.FriendlyName, CustomAttributeData.Text28Property.Description, false, CustomAttributeData.Text28Property.Type, CustomAttributeData.Text28Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText29MapId, CustomAttributeData.Text29Property.FriendlyName, CustomAttributeData.Text29Property.Description, false, CustomAttributeData.Text29Property.Type, CustomAttributeData.Text29Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText30MapId, CustomAttributeData.Text30Property.FriendlyName, CustomAttributeData.Text30Property.Description, false, CustomAttributeData.Text30Property.Type, CustomAttributeData.Text30Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText31MapId, CustomAttributeData.Text31Property.FriendlyName, CustomAttributeData.Text31Property.Description, false, CustomAttributeData.Text31Property.Type, CustomAttributeData.Text31Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText32MapId, CustomAttributeData.Text32Property.FriendlyName, CustomAttributeData.Text32Property.Description, false, CustomAttributeData.Text32Property.Type, CustomAttributeData.Text32Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText33MapId, CustomAttributeData.Text33Property.FriendlyName, CustomAttributeData.Text33Property.Description, false, CustomAttributeData.Text33Property.Type, CustomAttributeData.Text33Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText34MapId, CustomAttributeData.Text34Property.FriendlyName, CustomAttributeData.Text34Property.Description, false, CustomAttributeData.Text34Property.Type, CustomAttributeData.Text34Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText35MapId, CustomAttributeData.Text35Property.FriendlyName, CustomAttributeData.Text35Property.Description, false, CustomAttributeData.Text35Property.Type, CustomAttributeData.Text35Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText36MapId, CustomAttributeData.Text36Property.FriendlyName, CustomAttributeData.Text36Property.Description, false, CustomAttributeData.Text36Property.Type, CustomAttributeData.Text36Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText37MapId, CustomAttributeData.Text37Property.FriendlyName, CustomAttributeData.Text37Property.Description, false, CustomAttributeData.Text37Property.Type, CustomAttributeData.Text37Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText38MapId, CustomAttributeData.Text38Property.FriendlyName, CustomAttributeData.Text38Property.Description, false, CustomAttributeData.Text38Property.Type, CustomAttributeData.Text38Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText39MapId, CustomAttributeData.Text39Property.FriendlyName, CustomAttributeData.Text39Property.Description, false, CustomAttributeData.Text39Property.Type, CustomAttributeData.Text39Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText40MapId, CustomAttributeData.Text40Property.FriendlyName, CustomAttributeData.Text40Property.Description, false, CustomAttributeData.Text40Property.Type, CustomAttributeData.Text40Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText41MapId, CustomAttributeData.Text41Property.FriendlyName, CustomAttributeData.Text41Property.Description, false, CustomAttributeData.Text41Property.Type, CustomAttributeData.Text41Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText42MapId, CustomAttributeData.Text42Property.FriendlyName, CustomAttributeData.Text42Property.Description, false, CustomAttributeData.Text42Property.Type, CustomAttributeData.Text42Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText43MapId, CustomAttributeData.Text43Property.FriendlyName, CustomAttributeData.Text43Property.Description, false, CustomAttributeData.Text43Property.Type, CustomAttributeData.Text43Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText44MapId, CustomAttributeData.Text44Property.FriendlyName, CustomAttributeData.Text44Property.Description, false, CustomAttributeData.Text44Property.Type, CustomAttributeData.Text44Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText45MapId, CustomAttributeData.Text45Property.FriendlyName, CustomAttributeData.Text45Property.Description, false, CustomAttributeData.Text45Property.Type, CustomAttributeData.Text45Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText46MapId, CustomAttributeData.Text46Property.FriendlyName, CustomAttributeData.Text46Property.Description, false, CustomAttributeData.Text46Property.Type, CustomAttributeData.Text46Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText47MapId, CustomAttributeData.Text47Property.FriendlyName, CustomAttributeData.Text47Property.Description, false, CustomAttributeData.Text47Property.Type, CustomAttributeData.Text47Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText48MapId, CustomAttributeData.Text48Property.FriendlyName, CustomAttributeData.Text48Property.Description, false, CustomAttributeData.Text48Property.Type, CustomAttributeData.Text48Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText49MapId, CustomAttributeData.Text49Property.FriendlyName, CustomAttributeData.Text49Property.Description, false, CustomAttributeData.Text49Property.Type, CustomAttributeData.Text49Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText50MapId, CustomAttributeData.Text50Property.FriendlyName, CustomAttributeData.Text50Property.Description, false, CustomAttributeData.Text50Property.Type, CustomAttributeData.Text50Property.DefaultValue, 1, 255, true));

            //custom value fields
            this.Add(ImportMapping.NewImportMapping(CustomValue1MapId, CustomAttributeData.Value1Property.FriendlyName, CustomAttributeData.Value1Property.Description, false, CustomAttributeData.Value1Property.Type, CustomAttributeData.Value1Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue2MapId, CustomAttributeData.Value2Property.FriendlyName, CustomAttributeData.Value2Property.Description, false, CustomAttributeData.Value2Property.Type, CustomAttributeData.Value2Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue3MapId, CustomAttributeData.Value3Property.FriendlyName, CustomAttributeData.Value3Property.Description, false, CustomAttributeData.Value3Property.Type, CustomAttributeData.Value3Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue4MapId, CustomAttributeData.Value4Property.FriendlyName, CustomAttributeData.Value4Property.Description, false, CustomAttributeData.Value4Property.Type, CustomAttributeData.Value4Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue5MapId, CustomAttributeData.Value5Property.FriendlyName, CustomAttributeData.Value5Property.Description, false, CustomAttributeData.Value5Property.Type, CustomAttributeData.Value5Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue6MapId, CustomAttributeData.Value6Property.FriendlyName, CustomAttributeData.Value6Property.Description, false, CustomAttributeData.Value6Property.Type, CustomAttributeData.Value6Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue7MapId, CustomAttributeData.Value7Property.FriendlyName, CustomAttributeData.Value7Property.Description, false, CustomAttributeData.Value7Property.Type, CustomAttributeData.Value7Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue8MapId, CustomAttributeData.Value8Property.FriendlyName, CustomAttributeData.Value8Property.Description, false, CustomAttributeData.Value8Property.Type, CustomAttributeData.Value8Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue9MapId, CustomAttributeData.Value9Property.FriendlyName, CustomAttributeData.Value9Property.Description, false, CustomAttributeData.Value9Property.Type, CustomAttributeData.Value9Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue10MapId, CustomAttributeData.Value10Property.FriendlyName, CustomAttributeData.Value10Property.Description, false, CustomAttributeData.Value10Property.Type, CustomAttributeData.Value10Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue11MapId, CustomAttributeData.Value11Property.FriendlyName, CustomAttributeData.Value11Property.Description, false, CustomAttributeData.Value11Property.Type, CustomAttributeData.Value11Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue12MapId, CustomAttributeData.Value12Property.FriendlyName, CustomAttributeData.Value12Property.Description, false, CustomAttributeData.Value12Property.Type, CustomAttributeData.Value12Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue13MapId, CustomAttributeData.Value13Property.FriendlyName, CustomAttributeData.Value13Property.Description, false, CustomAttributeData.Value13Property.Type, CustomAttributeData.Value13Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue14MapId, CustomAttributeData.Value14Property.FriendlyName, CustomAttributeData.Value14Property.Description, false, CustomAttributeData.Value14Property.Type, CustomAttributeData.Value14Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue15MapId, CustomAttributeData.Value15Property.FriendlyName, CustomAttributeData.Value15Property.Description, false, CustomAttributeData.Value15Property.Type, CustomAttributeData.Value15Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue16MapId, CustomAttributeData.Value16Property.FriendlyName, CustomAttributeData.Value16Property.Description, false, CustomAttributeData.Value16Property.Type, CustomAttributeData.Value16Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue17MapId, CustomAttributeData.Value17Property.FriendlyName, CustomAttributeData.Value17Property.Description, false, CustomAttributeData.Value17Property.Type, CustomAttributeData.Value17Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue18MapId, CustomAttributeData.Value18Property.FriendlyName, CustomAttributeData.Value18Property.Description, false, CustomAttributeData.Value18Property.Type, CustomAttributeData.Value18Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue19MapId, CustomAttributeData.Value19Property.FriendlyName, CustomAttributeData.Value19Property.Description, false, CustomAttributeData.Value19Property.Type, CustomAttributeData.Value19Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue20MapId, CustomAttributeData.Value20Property.FriendlyName, CustomAttributeData.Value20Property.Description, false, CustomAttributeData.Value20Property.Type, CustomAttributeData.Value20Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue21MapId, CustomAttributeData.Value21Property.FriendlyName, CustomAttributeData.Value21Property.Description, false, CustomAttributeData.Value21Property.Type, CustomAttributeData.Value21Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue22MapId, CustomAttributeData.Value22Property.FriendlyName, CustomAttributeData.Value22Property.Description, false, CustomAttributeData.Value22Property.Type, CustomAttributeData.Value22Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue23MapId, CustomAttributeData.Value23Property.FriendlyName, CustomAttributeData.Value23Property.Description, false, CustomAttributeData.Value23Property.Type, CustomAttributeData.Value23Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue24MapId, CustomAttributeData.Value24Property.FriendlyName, CustomAttributeData.Value24Property.Description, false, CustomAttributeData.Value24Property.Type, CustomAttributeData.Value24Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue25MapId, CustomAttributeData.Value25Property.FriendlyName, CustomAttributeData.Value25Property.Description, false, CustomAttributeData.Value25Property.Type, CustomAttributeData.Value25Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue26MapId, CustomAttributeData.Value26Property.FriendlyName, CustomAttributeData.Value26Property.Description, false, CustomAttributeData.Value26Property.Type, CustomAttributeData.Value26Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue27MapId, CustomAttributeData.Value27Property.FriendlyName, CustomAttributeData.Value27Property.Description, false, CustomAttributeData.Value27Property.Type, CustomAttributeData.Value27Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue28MapId, CustomAttributeData.Value28Property.FriendlyName, CustomAttributeData.Value28Property.Description, false, CustomAttributeData.Value28Property.Type, CustomAttributeData.Value28Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue29MapId, CustomAttributeData.Value29Property.FriendlyName, CustomAttributeData.Value29Property.Description, false, CustomAttributeData.Value29Property.Type, CustomAttributeData.Value29Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue30MapId, CustomAttributeData.Value30Property.FriendlyName, CustomAttributeData.Value30Property.Description, false, CustomAttributeData.Value30Property.Type, CustomAttributeData.Value30Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue31MapId, CustomAttributeData.Value31Property.FriendlyName, CustomAttributeData.Value31Property.Description, false, CustomAttributeData.Value31Property.Type, CustomAttributeData.Value31Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue32MapId, CustomAttributeData.Value32Property.FriendlyName, CustomAttributeData.Value32Property.Description, false, CustomAttributeData.Value32Property.Type, CustomAttributeData.Value32Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue33MapId, CustomAttributeData.Value33Property.FriendlyName, CustomAttributeData.Value33Property.Description, false, CustomAttributeData.Value33Property.Type, CustomAttributeData.Value33Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue34MapId, CustomAttributeData.Value34Property.FriendlyName, CustomAttributeData.Value34Property.Description, false, CustomAttributeData.Value34Property.Type, CustomAttributeData.Value34Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue35MapId, CustomAttributeData.Value35Property.FriendlyName, CustomAttributeData.Value35Property.Description, false, CustomAttributeData.Value35Property.Type, CustomAttributeData.Value35Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue36MapId, CustomAttributeData.Value36Property.FriendlyName, CustomAttributeData.Value36Property.Description, false, CustomAttributeData.Value36Property.Type, CustomAttributeData.Value36Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue37MapId, CustomAttributeData.Value37Property.FriendlyName, CustomAttributeData.Value37Property.Description, false, CustomAttributeData.Value37Property.Type, CustomAttributeData.Value37Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue38MapId, CustomAttributeData.Value38Property.FriendlyName, CustomAttributeData.Value38Property.Description, false, CustomAttributeData.Value38Property.Type, CustomAttributeData.Value38Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue39MapId, CustomAttributeData.Value39Property.FriendlyName, CustomAttributeData.Value39Property.Description, false, CustomAttributeData.Value39Property.Type, CustomAttributeData.Value39Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue40MapId, CustomAttributeData.Value40Property.FriendlyName, CustomAttributeData.Value40Property.Description, false, CustomAttributeData.Value40Property.Type, CustomAttributeData.Value40Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue41MapId, CustomAttributeData.Value41Property.FriendlyName, CustomAttributeData.Value41Property.Description, false, CustomAttributeData.Value41Property.Type, CustomAttributeData.Value41Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue42MapId, CustomAttributeData.Value42Property.FriendlyName, CustomAttributeData.Value42Property.Description, false, CustomAttributeData.Value42Property.Type, CustomAttributeData.Value42Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue43MapId, CustomAttributeData.Value43Property.FriendlyName, CustomAttributeData.Value43Property.Description, false, CustomAttributeData.Value43Property.Type, CustomAttributeData.Value43Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue44MapId, CustomAttributeData.Value44Property.FriendlyName, CustomAttributeData.Value44Property.Description, false, CustomAttributeData.Value44Property.Type, CustomAttributeData.Value44Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue45MapId, CustomAttributeData.Value45Property.FriendlyName, CustomAttributeData.Value45Property.Description, false, CustomAttributeData.Value45Property.Type, CustomAttributeData.Value45Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue46MapId, CustomAttributeData.Value46Property.FriendlyName, CustomAttributeData.Value46Property.Description, false, CustomAttributeData.Value46Property.Type, CustomAttributeData.Value46Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue47MapId, CustomAttributeData.Value47Property.FriendlyName, CustomAttributeData.Value47Property.Description, false, CustomAttributeData.Value47Property.Type, CustomAttributeData.Value47Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue48MapId, CustomAttributeData.Value48Property.FriendlyName, CustomAttributeData.Value48Property.Description, false, CustomAttributeData.Value48Property.Type, CustomAttributeData.Value48Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue49MapId, CustomAttributeData.Value49Property.FriendlyName, CustomAttributeData.Value49Property.Description, false, CustomAttributeData.Value49Property.Type, CustomAttributeData.Value49Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue50MapId, CustomAttributeData.Value50Property.FriendlyName, CustomAttributeData.Value50Property.Description, false, CustomAttributeData.Value50Property.Type, CustomAttributeData.Value50Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));

            //custom flag fields
            this.Add(ImportMapping.NewImportMapping(CustomFlag1MapId, CustomAttributeData.Flag1Property.FriendlyName, CustomAttributeData.Flag1Property.Description, false, CustomAttributeData.Flag1Property.Type, CustomAttributeData.Flag1Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag2MapId, CustomAttributeData.Flag2Property.FriendlyName, CustomAttributeData.Flag2Property.Description, false, CustomAttributeData.Flag2Property.Type, CustomAttributeData.Flag2Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag3MapId, CustomAttributeData.Flag3Property.FriendlyName, CustomAttributeData.Flag3Property.Description, false, CustomAttributeData.Flag3Property.Type, CustomAttributeData.Flag3Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag4MapId, CustomAttributeData.Flag4Property.FriendlyName, CustomAttributeData.Flag4Property.Description, false, CustomAttributeData.Flag4Property.Type, CustomAttributeData.Flag4Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag5MapId, CustomAttributeData.Flag5Property.FriendlyName, CustomAttributeData.Flag5Property.Description, false, CustomAttributeData.Flag5Property.Type, CustomAttributeData.Flag5Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag6MapId, CustomAttributeData.Flag6Property.FriendlyName, CustomAttributeData.Flag6Property.Description, false, CustomAttributeData.Flag6Property.Type, CustomAttributeData.Flag6Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag7MapId, CustomAttributeData.Flag7Property.FriendlyName, CustomAttributeData.Flag7Property.Description, false, CustomAttributeData.Flag7Property.Type, CustomAttributeData.Flag7Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag8MapId, CustomAttributeData.Flag8Property.FriendlyName, CustomAttributeData.Flag8Property.Description, false, CustomAttributeData.Flag8Property.Type, CustomAttributeData.Flag8Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag9MapId, CustomAttributeData.Flag9Property.FriendlyName, CustomAttributeData.Flag9Property.Description, false, CustomAttributeData.Flag9Property.Type, CustomAttributeData.Flag9Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag10MapId, CustomAttributeData.Flag10Property.FriendlyName, CustomAttributeData.Flag10Property.Description, false, CustomAttributeData.Flag10Property.Type, CustomAttributeData.Flag10Property.DefaultValue, 0, 1, true));

            //custom date fields
            this.Add(ImportMapping.NewImportMapping(CustomDate1MapId, CustomAttributeData.Date1Property.FriendlyName, CustomAttributeData.Date1Property.Description, false, CustomAttributeData.Date1Property.Type, CustomAttributeData.Date1Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate2MapId, CustomAttributeData.Date2Property.FriendlyName, CustomAttributeData.Date2Property.Description, false, CustomAttributeData.Date2Property.Type, CustomAttributeData.Date2Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate3MapId, CustomAttributeData.Date3Property.FriendlyName, CustomAttributeData.Date3Property.Description, false, CustomAttributeData.Date3Property.Type, CustomAttributeData.Date3Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate4MapId, CustomAttributeData.Date4Property.FriendlyName, CustomAttributeData.Date4Property.Description, false, CustomAttributeData.Date4Property.Type, CustomAttributeData.Date4Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate5MapId, CustomAttributeData.Date5Property.FriendlyName, CustomAttributeData.Date5Property.Description, false, CustomAttributeData.Date5Property.Type, CustomAttributeData.Date5Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate6MapId, CustomAttributeData.Date6Property.FriendlyName, CustomAttributeData.Date6Property.Description, false, CustomAttributeData.Date6Property.Type, CustomAttributeData.Date6Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate7MapId, CustomAttributeData.Date7Property.FriendlyName, CustomAttributeData.Date7Property.Description, false, CustomAttributeData.Date7Property.Type, CustomAttributeData.Date7Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate8MapId, CustomAttributeData.Date8Property.FriendlyName, CustomAttributeData.Date8Property.Description, false, CustomAttributeData.Date8Property.Type, CustomAttributeData.Date8Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate9MapId, CustomAttributeData.Date9Property.FriendlyName, CustomAttributeData.Date9Property.Description, false, CustomAttributeData.Date9Property.Type, CustomAttributeData.Date9Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate10MapId, CustomAttributeData.Date10Property.FriendlyName, CustomAttributeData.Date10Property.Description, false, CustomAttributeData.Date10Property.Type, CustomAttributeData.Date10Property.DefaultValue, 0, 0, true));

            this.Add(ImportMapping.NewImportMapping(CustomNote1MapId, CustomAttributeData.Note1Property.FriendlyName, CustomAttributeData.Note1Property.Description, false, CustomAttributeData.Note1Property.Type, CustomAttributeData.Note1Property.DefaultValue, 1, 1000, true));
            this.Add(ImportMapping.NewImportMapping(CustomNote2MapId, CustomAttributeData.Note2Property.FriendlyName, CustomAttributeData.Note2Property.Description, false, CustomAttributeData.Note2Property.Type, CustomAttributeData.Note2Property.DefaultValue, 1, 1000, true));
            this.Add(ImportMapping.NewImportMapping(CustomNote3MapId, CustomAttributeData.Note3Property.FriendlyName, CustomAttributeData.Note3Property.Description, false, CustomAttributeData.Note3Property.Type, CustomAttributeData.Note3Property.DefaultValue, 1, 1000, true));
            this.Add(ImportMapping.NewImportMapping(CustomNote4MapId, CustomAttributeData.Note4Property.FriendlyName, CustomAttributeData.Note4Property.Description, false, CustomAttributeData.Note4Property.Type, CustomAttributeData.Note4Property.DefaultValue, 1, 1000, true));
            this.Add(ImportMapping.NewImportMapping(CustomNote5MapId, CustomAttributeData.Note5Property.FriendlyName, CustomAttributeData.Note5Property.Description, false, CustomAttributeData.Note5Property.Type, CustomAttributeData.Note5Property.DefaultValue, 1, 1000, true));

            #endregion

            #region Performance Data Properties & Metrics

            //this.Add(ImportMapping.NewImportMapping(AchievedCasePacksMapId, PlanogramPerformanceData.AchievedCasePacksProperty.FriendlyName, PlanogramPerformanceData.AchievedCasePacksProperty.Description, /*isMandatory*/false, true, PlanogramPerformanceData.AchievedCasePacksProperty.Type, PlanogramPerformanceData.AchievedCasePacksProperty.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            //this.Add(ImportMapping.NewImportMapping(AchievedDeliveriesMapId, PlanogramPerformanceData.AchievedDeliveriesProperty.FriendlyName, PlanogramPerformanceData.AchievedDeliveriesProperty.Description,  /*isMandatory*/false, true, PlanogramPerformanceData.AchievedDeliveriesProperty.Type, PlanogramPerformanceData.AchievedDeliveriesProperty.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            //this.Add(ImportMapping.NewImportMapping(AchievedDosMapId, PlanogramPerformanceData.AchievedDosProperty.FriendlyName, PlanogramPerformanceData.AchievedDosProperty.Description,  /*isMandatory*/false, true, PlanogramPerformanceData.AchievedDosProperty.Type, PlanogramPerformanceData.AchievedDosProperty.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            //this.Add(ImportMapping.NewImportMapping(AchievedShelfLifeMapId, PlanogramPerformanceData.AchievedShelfLifeProperty.FriendlyName, PlanogramPerformanceData.AchievedShelfLifeProperty.Description,  /*isMandatory*/false, true, PlanogramPerformanceData.AchievedShelfLifeProperty.Type, PlanogramPerformanceData.AchievedShelfLifeProperty.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            //this.Add(ImportMapping.NewImportMapping(UnitsSoldPerDayMapId, PlanogramPerformanceData.UnitsSoldPerDayProperty.FriendlyName, PlanogramPerformanceData.UnitsSoldPerDayProperty.Description,  /*isMandatory*/false, true, PlanogramPerformanceData.UnitsSoldPerDayProperty.Type, PlanogramPerformanceData.UnitsSoldPerDayProperty.DefaultValue, Int32.MinValue, Int32.MaxValue, true));

            this.Add(ImportMapping.NewImportMapping(PerformanceMetric1MapId, PlanogramPerformanceData.P1Property.FriendlyName, PlanogramPerformanceData.P1Property.Description, false, false, PlanogramPerformanceData.P1Property.Type, PlanogramPerformanceData.P1Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric2MapId, PlanogramPerformanceData.P2Property.FriendlyName, PlanogramPerformanceData.P2Property.Description, false, false, PlanogramPerformanceData.P2Property.Type, PlanogramPerformanceData.P2Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric3MapId, PlanogramPerformanceData.P3Property.FriendlyName, PlanogramPerformanceData.P3Property.Description, false, false, PlanogramPerformanceData.P3Property.Type, PlanogramPerformanceData.P3Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric4MapId, PlanogramPerformanceData.P4Property.FriendlyName, PlanogramPerformanceData.P4Property.Description, false, false, PlanogramPerformanceData.P4Property.Type, PlanogramPerformanceData.P4Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric5MapId, PlanogramPerformanceData.P5Property.FriendlyName, PlanogramPerformanceData.P5Property.Description, false, false, PlanogramPerformanceData.P5Property.Type, PlanogramPerformanceData.P5Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric6MapId, PlanogramPerformanceData.P6Property.FriendlyName, PlanogramPerformanceData.P6Property.Description, false, false, PlanogramPerformanceData.P6Property.Type, PlanogramPerformanceData.P6Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric7MapId, PlanogramPerformanceData.P7Property.FriendlyName, PlanogramPerformanceData.P7Property.Description, false, false, PlanogramPerformanceData.P7Property.Type, PlanogramPerformanceData.P7Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric8MapId, PlanogramPerformanceData.P8Property.FriendlyName, PlanogramPerformanceData.P8Property.Description, false, false, PlanogramPerformanceData.P8Property.Type, PlanogramPerformanceData.P8Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric9MapId, PlanogramPerformanceData.P9Property.FriendlyName, PlanogramPerformanceData.P9Property.Description, false, false, PlanogramPerformanceData.P9Property.Type, PlanogramPerformanceData.P9Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric10MapId, PlanogramPerformanceData.P10Property.FriendlyName, PlanogramPerformanceData.P10Property.Description, false, false, PlanogramPerformanceData.P10Property.Type, PlanogramPerformanceData.P10Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric11MapId, PlanogramPerformanceData.P11Property.FriendlyName, PlanogramPerformanceData.P11Property.Description, false, false, PlanogramPerformanceData.P11Property.Type, PlanogramPerformanceData.P11Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric12MapId, PlanogramPerformanceData.P12Property.FriendlyName, PlanogramPerformanceData.P12Property.Description, false, false, PlanogramPerformanceData.P12Property.Type, PlanogramPerformanceData.P12Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric13MapId, PlanogramPerformanceData.P13Property.FriendlyName, PlanogramPerformanceData.P13Property.Description, false, false, PlanogramPerformanceData.P13Property.Type, PlanogramPerformanceData.P13Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric14MapId, PlanogramPerformanceData.P14Property.FriendlyName, PlanogramPerformanceData.P14Property.Description, false, false, PlanogramPerformanceData.P14Property.Type, PlanogramPerformanceData.P14Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric15MapId, PlanogramPerformanceData.P15Property.FriendlyName, PlanogramPerformanceData.P15Property.Description, false, false, PlanogramPerformanceData.P15Property.Type, PlanogramPerformanceData.P15Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric16MapId, PlanogramPerformanceData.P16Property.FriendlyName, PlanogramPerformanceData.P16Property.Description, false, false, PlanogramPerformanceData.P16Property.Type, PlanogramPerformanceData.P16Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric17MapId, PlanogramPerformanceData.P17Property.FriendlyName, PlanogramPerformanceData.P17Property.Description, false, false, PlanogramPerformanceData.P17Property.Type, PlanogramPerformanceData.P17Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric18MapId, PlanogramPerformanceData.P18Property.FriendlyName, PlanogramPerformanceData.P18Property.Description, false, false, PlanogramPerformanceData.P18Property.Type, PlanogramPerformanceData.P18Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric19MapId, PlanogramPerformanceData.P19Property.FriendlyName, PlanogramPerformanceData.P19Property.Description, false, false, PlanogramPerformanceData.P19Property.Type, PlanogramPerformanceData.P19Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(PerformanceMetric20MapId, PlanogramPerformanceData.P20Property.FriendlyName, PlanogramPerformanceData.P20Property.Description, false, false, PlanogramPerformanceData.P20Property.Type, PlanogramPerformanceData.P20Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
                  
            #endregion

            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

        #region Static Helpers

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="productDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, ProductDto productDto)
        {
            switch (mappingId)
            {
                #region Product Properties

                case ProductLibraryImportMappingList.AlternateDepthMapId: return productDto.AlternateDepth;
                case ProductLibraryImportMappingList.AlternateHeightMapId: return productDto.AlternateHeight;
                case ProductLibraryImportMappingList.AlternateWidthMapId: return productDto.AlternateWidth;
                case ProductLibraryImportMappingList.TrayHeightMapId: return productDto.TrayHeight;
                case ProductLibraryImportMappingList.CanBreakTrayBackMapId: return productDto.CanBreakTrayBack;
                case ProductLibraryImportMappingList.CanBreakTrayDownMapId: return productDto.CanBreakTrayDown;
                case ProductLibraryImportMappingList.CanBreakTrayTopMapId: return productDto.CanBreakTrayTop;
                case ProductLibraryImportMappingList.CanBreakTrayUpMapId: return productDto.CanBreakTrayUp;
                case ProductLibraryImportMappingList.CasePackUnitsMapId: return productDto.CasePackUnits;
                case ProductLibraryImportMappingList.DepthMapId: return productDto.Depth;
                case ProductLibraryImportMappingList.DisplayDepthMapId: return productDto.DisplayDepth;
                case ProductLibraryImportMappingList.DisplayHeightMapId: return productDto.DisplayHeight;
                case ProductLibraryImportMappingList.DisplayWidthMapId: return productDto.DisplayWidth;
                case ProductLibraryImportMappingList.FillColourMapId: return productDto.FillColour;
                case ProductLibraryImportMappingList.FillPatternMapId: return productDto.FillPatternType;
                case ProductLibraryImportMappingList.FingerSpaceAboveMapId: return productDto.FingerSpaceAbove;
                case ProductLibraryImportMappingList.FingerSpaceToTheSideMapId: return productDto.FingerSpaceToTheSide;
                case ProductLibraryImportMappingList.ForceBottomCapMapId: return productDto.ForceBottomCap;
                case ProductLibraryImportMappingList.ForceMiddleCapMapId: return productDto.ForceMiddleCap;
                case ProductLibraryImportMappingList.FrontOverhangMapId: return productDto.FrontOverhang;
                case ProductLibraryImportMappingList.GtinMapId: return productDto.Gtin;
                case ProductLibraryImportMappingList.HeightMapId: return productDto.Height;
                case ProductLibraryImportMappingList.IsActiveMapId: return productDto.IsActive;
                case ProductLibraryImportMappingList.IsFrontOnlyMapId: return productDto.IsFrontOnly;
                case ProductLibraryImportMappingList.MaxDeepMapId: return productDto.MaxDeep;
                case ProductLibraryImportMappingList.MaxRightCapMapId: return productDto.MaxRightCap;
                case ProductLibraryImportMappingList.MaxStackMapId: return productDto.MaxStack;
                case ProductLibraryImportMappingList.MaxTopCapMapId: return productDto.MaxTopCap;
                case ProductLibraryImportMappingList.MerchandisingStyleMapId:
                    return ProductMerchandisingStyleHelper.FriendlyNames[(ProductMerchandisingStyle)(Byte)productDto.MerchandisingStyle];
                case ProductLibraryImportMappingList.MinDeepMapId: return productDto.MinDeep;
                case ProductLibraryImportMappingList.NameMapId: return productDto.Name;
                case ProductLibraryImportMappingList.NestingDepthMapId: return productDto.NestingDepth;
                case ProductLibraryImportMappingList.NestingHeightMapId: return productDto.NestingHeight;
                case ProductLibraryImportMappingList.NestingWidthMapId: return productDto.NestingWidth;
                case ProductLibraryImportMappingList.NumberOfPegHolesMapId: return productDto.NumberOfPegHoles;
                case ProductLibraryImportMappingList.OrientationTypeMapId:
                    return PlanogramProductOrientationTypeHelper.FriendlyNames[(PlanogramProductOrientationType)(Byte)productDto.OrientationType];
                case ProductLibraryImportMappingList.PegDepthMapId: return productDto.PegDepth;
                case ProductLibraryImportMappingList.PegProngOffsetXMapId: return productDto.PegProngOffsetX;
                case ProductLibraryImportMappingList.PegX2MapId: return productDto.PegX2;
                case ProductLibraryImportMappingList.PegX3MapId: return productDto.PegX3;
                case ProductLibraryImportMappingList.PegXMapId: return productDto.PegX;
                case ProductLibraryImportMappingList.PegY2MapId: return productDto.PegY2;
                case ProductLibraryImportMappingList.PegY3MapId: return productDto.PegY3;
                case ProductLibraryImportMappingList.PegYMapId: return productDto.PegY;
                case ProductLibraryImportMappingList.PointOfPurchaseDepthMapId: return productDto.PointOfPurchaseDepth;
                case ProductLibraryImportMappingList.PointOfPurchaseHeightMapId: return productDto.PointOfPurchaseHeight;
                case ProductLibraryImportMappingList.PointOfPurchaseWidthMapId: return productDto.PointOfPurchaseWidth;
                case ProductLibraryImportMappingList.ShapeMapId: return productDto.Shape;
                case ProductLibraryImportMappingList.SqueezeDepthMapId: return productDto.SqueezeDepth;
                case ProductLibraryImportMappingList.SqueezeHeightMapId: return productDto.SqueezeHeight;
                case ProductLibraryImportMappingList.SqueezeWidthMapId: return productDto.SqueezeWidth;
                case ProductLibraryImportMappingList.StatusTypeMapId:
                    return PlanogramProductStatusTypeHelper.FriendlyNames[(PlanogramProductStatusType)(Byte)productDto.StatusType];
                case ProductLibraryImportMappingList.TrayDeepMapId: return productDto.TrayDeep;
                case ProductLibraryImportMappingList.TrayHighMapId: return productDto.TrayHigh;
                case ProductLibraryImportMappingList.TrayThickDepthMapId: return productDto.TrayThickDepth;
                case ProductLibraryImportMappingList.TrayThickHeightMapId: return productDto.TrayThickHeight;
                case ProductLibraryImportMappingList.TrayThickWidthMapId: return productDto.TrayThickWidth;
                case ProductLibraryImportMappingList.TrayWideMapId: return productDto.TrayWide;
                case ProductLibraryImportMappingList.TrayPackUnitsMapId: return productDto.TrayPackUnits;
                case ProductLibraryImportMappingList.WidthMapId: return productDto.Width;

                //new Properties
                case ProductLibraryImportMappingList.TrayWidthMapId: return productDto.TrayWidth;
                case ProductLibraryImportMappingList.TrayDepthMapId: return productDto.TrayDepth;
                case ProductLibraryImportMappingList.CaseHeightMapId: return productDto.CaseHeight;
                case ProductLibraryImportMappingList.CaseWidthMapId: return productDto.CaseWidth;
                case ProductLibraryImportMappingList.CaseDepthMapId: return productDto.CaseDepth;
                case ProductLibraryImportMappingList.CaseHighMapId: return productDto.CaseHigh;
                case ProductLibraryImportMappingList.CaseWideMapId: return productDto.CaseWide;
                case ProductLibraryImportMappingList.CaseDeepMapId: return productDto.CaseDeep;
                case ProductLibraryImportMappingList.ShapeTypeMapId: return productDto.ShapeType;


                #endregion

                #region Product Attribute Properties

                case ProductLibraryImportMappingList.HealthMapId: return productDto.Health;
                case ProductLibraryImportMappingList.BarCodeMapId: return productDto.Barcode;
                case ProductLibraryImportMappingList.BrandMapId: return productDto.Brand;
                case ProductLibraryImportMappingList.CaseCostMapId: return productDto.CaseCost;
                case ProductLibraryImportMappingList.ColourMapId: return productDto.Colour;
                case ProductLibraryImportMappingList.ConsumerInformationMapId: return productDto.ConsumerInformation;
                case ProductLibraryImportMappingList.CorporateCodeMapId: return productDto.CorporateCode;
                case ProductLibraryImportMappingList.CostPriceMapId: return productDto.CostPrice;
                case ProductLibraryImportMappingList.CountryOfOriginMapId: return productDto.CountryOfOrigin;
                case ProductLibraryImportMappingList.CountryOfProcessingMapId: return productDto.CountryOfProcessing;
                case ProductLibraryImportMappingList.DateDiscontinuedMapId: return productDto.DateDiscontinued;
                case ProductLibraryImportMappingList.DateEffectiveMapId: return productDto.DateEffective;
                case ProductLibraryImportMappingList.DateIntroducedMapId: return productDto.DateIntroduced;
                case ProductLibraryImportMappingList.DeliveryFrequencyDaysMapId: return productDto.DeliveryFrequencyDays;
                case ProductLibraryImportMappingList.DeliveryMethodMapId: return productDto.DeliveryMethod;
                case ProductLibraryImportMappingList.FinancialGroupCodeMapId: return productDto.FinancialGroupCode;
                case ProductLibraryImportMappingList.FinancialGroupNameMapId: return productDto.FinancialGroupName;
                case ProductLibraryImportMappingList.FlavourMapId: return productDto.Flavour;
                case ProductLibraryImportMappingList.GarmentTypeMapId: return productDto.GarmentType;
                case ProductLibraryImportMappingList.IsNewMapId: return productDto.IsNewProduct;
                case ProductLibraryImportMappingList.IsPrivateLabelMapId: return productDto.IsPrivateLabel;
                case ProductLibraryImportMappingList.ManufacturerCodeMapId: return productDto.ManufacturerCode;
                case ProductLibraryImportMappingList.ManufacturerMapId: return productDto.Manufacturer;
                case ProductLibraryImportMappingList.ManufacturerRecommendedRetailPriceMapId: return productDto.ManufacturerRecommendedRetailPrice;
                case ProductLibraryImportMappingList.ModelMapId: return productDto.Model;
                case ProductLibraryImportMappingList.PackagingShapeMapId: return productDto.PackagingShape;
                case ProductLibraryImportMappingList.PackagingTypeMapId: return productDto.PackagingType;
                case ProductLibraryImportMappingList.PatternMapId: return productDto.Pattern;
                case ProductLibraryImportMappingList.PointOfPurchaseDescriptionMapId: return productDto.PointOfPurchaseDescription;
                case ProductLibraryImportMappingList.RecommendedRetailPriceMapId: return productDto.RecommendedRetailPrice;
                case ProductLibraryImportMappingList.SellPackCountMapId: return productDto.SellPackCount;
                case ProductLibraryImportMappingList.SellPackDescriptionMapId: return productDto.SellPackDescription;
                case ProductLibraryImportMappingList.SellPriceMapId: return productDto.SellPrice;
                case ProductLibraryImportMappingList.ShelfLifeMapId: return productDto.ShelfLife;
                case ProductLibraryImportMappingList.ShortDescriptionMapId: return productDto.ShortDescription;
                case ProductLibraryImportMappingList.SizeMapId: return productDto.Size;
                case ProductLibraryImportMappingList.StyleNumberMapId: return productDto.StyleNumber;
                case ProductLibraryImportMappingList.SubCategoryMapId: return productDto.Subcategory;
                case ProductLibraryImportMappingList.TaxRateMapId: return productDto.TaxRate;
                case ProductLibraryImportMappingList.TextureMapId: return productDto.Texture;
                case ProductLibraryImportMappingList.UnitOfMeasureMapId: return productDto.UnitOfMeasure;
                case ProductLibraryImportMappingList.VendorCodeMapId: return productDto.VendorCode;
                case ProductLibraryImportMappingList.VendorMapId: return productDto.Vendor;
                case ProductLibraryImportMappingList.IsPlaceHolderProductMapId: return productDto.IsPlaceHolderProduct;
                case ProductLibraryImportMappingList.CustomerStatusMapId: return productDto.CustomerStatus;


                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="productDto"></param>
        public static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            ProductDto productDto)
        {
            IFormatProvider prov = CultureInfo.CurrentCulture;

            switch (mappingId)
            {
                #region Main Properties

                case ProductLibraryImportMappingList.AlternateDepthMapId:
                    productDto.AlternateDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.TrayHeightMapId:
                    productDto.TrayHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.AlternateHeightMapId:
                    productDto.AlternateHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.AlternateWidthMapId:
                    productDto.AlternateWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.BarCodeMapId:
                    productDto.Barcode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.BrandMapId:
                    productDto.Brand = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CanBreakTrayBackMapId:
                    productDto.CanBreakTrayBack = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CanBreakTrayDownMapId:
                    productDto.CanBreakTrayDown = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CanBreakTrayTopMapId:
                    productDto.CanBreakTrayTop = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CanBreakTrayUpMapId:
                    productDto.CanBreakTrayUp = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CaseCostMapId:
                    productDto.CaseCost = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CasePackUnitsMapId:
                    productDto.CasePackUnits = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ColourMapId:
                    productDto.Colour = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ConsumerInformationMapId:
                    productDto.ConsumerInformation = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CorporateCodeMapId:
                    productDto.CorporateCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CostPriceMapId:
                    productDto.CostPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CountryOfOriginMapId:
                    productDto.CountryOfOrigin = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CountryOfProcessingMapId:
                    productDto.CountryOfProcessing = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DateDiscontinuedMapId:
                    productDto.DateDiscontinued = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.DateEffectiveMapId:
                    productDto.DateEffective = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.DateIntroducedMapId:
                    productDto.DateIntroduced = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.DeliveryFrequencyDaysMapId:
                    productDto.DeliveryFrequencyDays = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DeliveryMethodMapId:
                    productDto.DeliveryMethod = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DepthMapId:
                    productDto.Depth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DisplayDepthMapId:
                    productDto.DisplayDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DisplayHeightMapId:
                    productDto.DisplayHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DisplayWidthMapId:
                    productDto.DisplayWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FillColourMapId:
                    productDto.FillColour = Convert.ToInt32(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FillPatternMapId:
                    productDto.FillPatternType = (Byte)Enum.Parse(typeof(PlanogramProductFillPatternType), Convert.ToString(cellValue, prov));
                    break;

                case ProductLibraryImportMappingList.FinancialGroupCodeMapId:
                    productDto.FinancialGroupCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FinancialGroupNameMapId:
                    productDto.FinancialGroupName = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FingerSpaceAboveMapId:
                    productDto.FingerSpaceAbove = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FingerSpaceToTheSideMapId:
                    productDto.FingerSpaceToTheSide = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FlavourMapId:
                    productDto.Flavour = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ForceBottomCapMapId:
                    productDto.ForceBottomCap = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ForceMiddleCapMapId:
                    productDto.ForceMiddleCap = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FrontOverhangMapId:
                    productDto.FrontOverhang = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.GarmentTypeMapId:
                    productDto.GarmentType = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.GtinMapId:
                    productDto.Gtin = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.HealthMapId:
                    productDto.Health = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.HeightMapId:
                    productDto.Height = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.IsActiveMapId:
                    productDto.IsActive = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.IsFrontOnlyMapId:
                    productDto.IsFrontOnly = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.IsNewMapId:
                    productDto.IsNewProduct = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.IsPrivateLabelMapId:
                    productDto.IsPrivateLabel = Convert.ToBoolean(cellValue, prov);
                    break;


                case ProductLibraryImportMappingList.ManufacturerCodeMapId:
                    productDto.ManufacturerCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ManufacturerMapId:
                    productDto.Manufacturer = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ManufacturerRecommendedRetailPriceMapId:
                    productDto.ManufacturerRecommendedRetailPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MaxDeepMapId:
                    productDto.MaxDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MaxRightCapMapId:
                    productDto.MaxRightCap = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MaxStackMapId:
                    productDto.MaxStack = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MaxTopCapMapId:
                    productDto.MaxTopCap = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MerchandisingStyleMapId:
                    ProductMerchandisingStyle? merchStyle = ProductMerchandisingStyleHelper.ProductMerchandisingStyleGetEnum(cellValue.ToString());
                    productDto.MerchandisingStyle = (Byte)merchStyle;
                    break;

                case ProductLibraryImportMappingList.MinDeepMapId:
                    productDto.MinDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ModelMapId:
                    productDto.Model = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NameMapId:
                    productDto.Name = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NestingDepthMapId:
                    productDto.NestingDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NestingHeightMapId:
                    productDto.NestingHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NestingWidthMapId:
                    productDto.NestingWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NumberOfPegHolesMapId:
                    productDto.NumberOfPegHoles = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.OrientationTypeMapId:
                    PlanogramProductOrientationType? orientationType = PlanogramProductOrientationTypeHelper.PlanogramProductOrientationTypeGetEnum(cellValue.ToString());
                    productDto.OrientationType = (Byte)orientationType;
                    break;

                case ProductLibraryImportMappingList.PackagingShapeMapId:
                    productDto.PackagingShape = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PackagingTypeMapId:
                    productDto.PackagingType = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PatternMapId:
                    productDto.Pattern = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegDepthMapId:
                    productDto.PegDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegProngOffsetXMapId:
                    productDto.PegProngOffsetX = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegX2MapId:
                    productDto.PegX2 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegX3MapId:
                    productDto.PegX3 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegXMapId:
                    productDto.PegX = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegY2MapId:
                    productDto.PegY2 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegY3MapId:
                    productDto.PegY3 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegYMapId:
                    productDto.PegY = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PointOfPurchaseDepthMapId:
                    productDto.PointOfPurchaseDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PointOfPurchaseDescriptionMapId:
                    productDto.PointOfPurchaseDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PointOfPurchaseHeightMapId:
                    productDto.PointOfPurchaseHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PointOfPurchaseWidthMapId:
                    productDto.PointOfPurchaseWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.RecommendedRetailPriceMapId:
                    productDto.RecommendedRetailPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SellPackCountMapId:
                    productDto.SellPackCount = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SellPackDescriptionMapId:
                    productDto.SellPackDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SellPriceMapId:
                    productDto.SellPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ShapeMapId:
                    productDto.Shape = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ShortDescriptionMapId:
                    productDto.ShortDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ShelfLifeMapId:
                    productDto.ShelfLife = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SizeMapId:
                    productDto.Size = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SqueezeDepthMapId:
                    productDto.SqueezeDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SqueezeHeightMapId:
                    productDto.SqueezeHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SqueezeWidthMapId:
                    productDto.SqueezeWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.StatusTypeMapId:
                    PlanogramProductStatusType? productStatusType = PlanogramProductStatusTypeHelper.PlanogramProductStatusTypeGetEnum(cellValue.ToString());
                    productDto.StatusType = (Byte)productStatusType;
                    break;

                case ProductLibraryImportMappingList.StyleNumberMapId:
                    productDto.StyleNumber = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SubCategoryMapId:
                    productDto.Subcategory = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TaxRateMapId:
                    productDto.TaxRate = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TextureMapId:
                    productDto.Texture = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayDeepMapId:
                    productDto.TrayDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayHighMapId:
                    productDto.TrayHigh = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayThickDepthMapId:
                    productDto.TrayThickDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayThickHeightMapId:
                    productDto.TrayThickHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayThickWidthMapId:
                    productDto.TrayThickWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayWideMapId:
                    productDto.TrayWide = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayPackUnitsMapId:
                    productDto.TrayPackUnits = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.UnitOfMeasureMapId:
                    productDto.UnitOfMeasure = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.VendorCodeMapId:
                    productDto.VendorCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.VendorMapId:
                    productDto.Vendor = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.WidthMapId:
                    productDto.Width = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.TrayWidthMapId:
                    productDto.TrayWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.TrayDepthMapId:
                    productDto.TrayDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseHeightMapId:
                    productDto.CaseHeight = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseWidthMapId:
                    productDto.CaseWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseDepthMapId:
                    productDto.CaseDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseHighMapId:
                    productDto.CaseHigh = Convert.ToByte(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseWideMapId:
                    productDto.CaseWide = Convert.ToByte(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseDeepMapId:
                    productDto.CaseDeep = Convert.ToByte(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.IsPlaceHolderProductMapId:
                    productDto.IsPlaceHolderProduct = Convert.ToBoolean(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CustomerStatusMapId:
                    productDto.CustomerStatus = Convert.ToString(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.ShapeTypeMapId:
                    PlanogramProductShapeType? shapeType = PlanogramProductShapeTypeHelper.PlanogramProductShapeTypeGetEnum(cellValue.ToString());
                    productDto.ShapeType = (Byte)shapeType;
                    break;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="productLibraryProductModel"></param>
        public static void SetValueByMappingId(Int32 mappingId, Object cellValue, ProductLibraryProduct productLibraryProductModel)
        {
            Product productModel = productLibraryProductModel.Product;

            //Set the product properties
            IFormatProvider prov = CultureInfo.CurrentCulture;

            switch (mappingId)
            {
                #region Main Properties

                case ProductLibraryImportMappingList.AlternateDepthMapId:
                    productModel.AlternateDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.AlternateHeightMapId:
                    productModel.AlternateHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.AlternateWidthMapId:
                    productModel.AlternateWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.BarCodeMapId:
                    productModel.Barcode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.BrandMapId:
                    productModel.Brand = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CanBreakTrayBackMapId:
                    productModel.CanBreakTrayBack = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CanBreakTrayDownMapId:
                    productModel.CanBreakTrayDown = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CanBreakTrayTopMapId:
                    productModel.CanBreakTrayTop = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CanBreakTrayUpMapId:
                    productModel.CanBreakTrayUp = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CaseCostMapId:
                    productModel.CaseCost = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CasePackUnitsMapId:
                    productModel.CasePackUnits = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ColourMapId:
                    productModel.Colour = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ConsumerInformationMapId:
                    productModel.ConsumerInformation = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CorporateCodeMapId:
                    productModel.CorporateCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CostPriceMapId:
                    productModel.CostPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CountryOfOriginMapId:
                    productModel.CountryOfOrigin = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CountryOfProcessingMapId:
                    productModel.CountryOfProcessing = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DateDiscontinuedMapId:
                    productModel.DateDiscontinued = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.DateEffectiveMapId:
                    productModel.DateEffective = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.DateIntroducedMapId:
                    productModel.DateIntroduced = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.DeliveryFrequencyDaysMapId:
                    productModel.DeliveryFrequencyDays = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DeliveryMethodMapId:
                    productModel.DeliveryMethod = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DepthMapId:
                    productModel.Depth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DisplayDepthMapId:
                    productModel.DisplayDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DisplayHeightMapId:
                    productModel.DisplayHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.DisplayWidthMapId:
                    productModel.DisplayWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FillColourMapId:
                    productModel.FillColour = Convert.ToInt32(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FillPatternMapId:
                    productModel.FillPatternType = PlanogramProductFillPatternTypeHelper.EnumFromFriendlyName[cellValue.ToString().ToLowerInvariant()];
                    break;

                case ProductLibraryImportMappingList.FinancialGroupCodeMapId:
                    productModel.FinancialGroupCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FinancialGroupNameMapId:
                    productModel.FinancialGroupName = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FingerSpaceAboveMapId:
                    productModel.FingerSpaceAbove = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FingerSpaceToTheSideMapId:
                    productModel.FingerSpaceToTheSide = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FlavourMapId:
                    productModel.Flavour = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ForceBottomCapMapId:
                    productModel.ForceBottomCap = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ForceMiddleCapMapId:
                    productModel.ForceMiddleCap = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.FrontOverhangMapId:
                    productModel.FrontOverhang = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.GarmentTypeMapId:
                    productModel.GarmentType = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.GtinMapId:
                    productModel.Gtin = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayHeightMapId:
                    productModel.TrayHeight = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.HealthMapId:
                    productModel.Health = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.HeightMapId:
                    productModel.Height = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.IsActiveMapId:
                    productModel.IsActive = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.IsFrontOnlyMapId:
                    productModel.IsFrontOnly = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.IsPrivateLabelMapId:
                    productModel.IsPrivateLabel = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ManufacturerCodeMapId:
                    productModel.ManufacturerCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ManufacturerMapId:
                    productModel.Manufacturer = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ManufacturerRecommendedRetailPriceMapId:
                    productModel.ManufacturerRecommendedRetailPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MaxDeepMapId:
                    productModel.MaxDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MaxRightCapMapId:
                    productModel.MaxRightCap = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MaxStackMapId:
                    productModel.MaxStack = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MaxTopCapMapId:
                    productModel.MaxTopCap = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.MerchandisingStyleMapId:
                    ProductMerchandisingStyle? merchStyle = ProductMerchandisingStyleHelper.ProductMerchandisingStyleGetEnum(cellValue.ToString());
                    if (merchStyle != null) productModel.MerchandisingStyle = (PlanogramProductMerchandisingStyle)merchStyle;
                    break;

                case ProductLibraryImportMappingList.MinDeepMapId:
                    productModel.MinDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ModelMapId:
                    productModel.Model = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NameMapId:
                    productModel.Name = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NestingDepthMapId:
                    productModel.NestingDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NestingHeightMapId:
                    productModel.NestingHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NestingWidthMapId:
                    productModel.NestingWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.NumberOfPegHolesMapId:
                    productModel.NumberOfPegHoles = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.OrientationTypeMapId:
                    PlanogramProductOrientationType? orientationType = PlanogramProductOrientationTypeHelper.PlanogramProductOrientationTypeGetEnum(cellValue.ToString().ToLowerInvariant());
                    productModel.OrientationType = orientationType ?? PlanogramProductOrientationType.Front0;
                    break;

                case ProductLibraryImportMappingList.PackagingShapeMapId:
                    productModel.PackagingShape = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PackagingTypeMapId:
                    productModel.PackagingType = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PatternMapId:
                    productModel.Pattern = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegDepthMapId:
                    productModel.PegDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegProngOffsetXMapId:
                    productModel.PegProngOffsetX = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegX2MapId:
                    productModel.PegX2 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegX3MapId:
                    productModel.PegX3 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegXMapId:
                    productModel.PegX = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegY2MapId:
                    productModel.PegY2 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegY3MapId:
                    productModel.PegY3 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PegYMapId:
                    productModel.PegY = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PointOfPurchaseDepthMapId:
                    productModel.PointOfPurchaseDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PointOfPurchaseDescriptionMapId:
                    productModel.PointOfPurchaseDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PointOfPurchaseHeightMapId:
                    productModel.PointOfPurchaseHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.PointOfPurchaseWidthMapId:
                    productModel.PointOfPurchaseWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.RecommendedRetailPriceMapId:
                    productModel.RecommendedRetailPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SellPackCountMapId:
                    productModel.SellPackCount = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SellPackDescriptionMapId:
                    productModel.SellPackDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SellPriceMapId:
                    productModel.SellPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ShapeMapId:
                    productModel.Shape = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ShortDescriptionMapId:
                    productModel.ShortDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.ShelfLifeMapId:
                    productModel.ShelfLife = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SizeMapId:
                    productModel.Size = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SqueezeDepthMapId:
                    productModel.SqueezeDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SqueezeHeightMapId:
                    productModel.SqueezeHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SqueezeWidthMapId:
                    productModel.SqueezeWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.StatusTypeMapId:
                    PlanogramProductStatusType? productStatusType = PlanogramProductStatusTypeHelper.PlanogramProductStatusTypeGetEnum(cellValue.ToString().ToLowerInvariant());
                    productModel.StatusType = productStatusType ?? PlanogramProductStatusType.Active;
                    break;

                case ProductLibraryImportMappingList.StyleNumberMapId:
                    productModel.StyleNumber = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.SubCategoryMapId:
                    productModel.Subcategory = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TaxRateMapId:
                    productModel.TaxRate = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TextureMapId:
                    productModel.Texture = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayDeepMapId:
                    productModel.TrayDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayHighMapId:
                    productModel.TrayHigh = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayThickDepthMapId:
                    productModel.TrayThickDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayThickHeightMapId:
                    productModel.TrayThickHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayThickWidthMapId:
                    productModel.TrayThickWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayWideMapId:
                    productModel.TrayWide = Convert.ToByte(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayPackUnitsMapId:
                    productModel.TrayPackUnits = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.UnitOfMeasureMapId:
                    productModel.UnitOfMeasure = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.VendorCodeMapId:
                    productModel.VendorCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.VendorMapId:
                    productModel.Vendor = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.WidthMapId:
                    productModel.Width = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.TrayWidthMapId:
                    productModel.TrayWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.TrayDepthMapId:
                    productModel.TrayDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseHeightMapId:
                    productModel.CaseHeight = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseWidthMapId:
                    productModel.CaseWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseDepthMapId:
                    productModel.CaseDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseHighMapId:
                    productModel.CaseHigh = Convert.ToByte(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseWideMapId:
                    productModel.CaseWide = Convert.ToByte(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CaseDeepMapId:
                    productModel.CaseDeep = Convert.ToByte(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.IsPlaceHolderProductMapId:
                    productModel.IsPlaceHolderProduct = Convert.ToBoolean(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.CustomerStatusMapId:
                    productModel.CustomerStatus = Convert.ToString(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.ShapeTypeMapId:
                    PlanogramProductShapeType? shapeType = PlanogramProductShapeTypeHelper.PlanogramProductShapeTypeGetEnum(cellValue.ToString());
                    productModel.ShapeType = shapeType ?? PlanogramProductShapeType.Box;
                    break;
                case ProductLibraryImportMappingList.IsNewMapId:
                    productModel.IsNewProduct = Convert.ToBoolean(cellValue, prov);
                    break;
                #endregion

                #region Custom Text Fields

                case ProductLibraryImportMappingList.CustomText1MapId:
                    productModel.CustomAttributes.Text1 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText2MapId:
                    productModel.CustomAttributes.Text2 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText3MapId:
                    productModel.CustomAttributes.Text3 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText4MapId:
                    productModel.CustomAttributes.Text4 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText5MapId:
                    productModel.CustomAttributes.Text5 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText6MapId:
                    productModel.CustomAttributes.Text6 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText7MapId:
                    productModel.CustomAttributes.Text7 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText8MapId:
                    productModel.CustomAttributes.Text8 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText9MapId:
                    productModel.CustomAttributes.Text9 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText10MapId:
                    productModel.CustomAttributes.Text10 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText11MapId:
                    productModel.CustomAttributes.Text11 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText12MapId:
                    productModel.CustomAttributes.Text12 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText13MapId:
                    productModel.CustomAttributes.Text13 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText14MapId:
                    productModel.CustomAttributes.Text14 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText15MapId:
                    productModel.CustomAttributes.Text15 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText16MapId:
                    productModel.CustomAttributes.Text16 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText17MapId:
                    productModel.CustomAttributes.Text17 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText18MapId:
                    productModel.CustomAttributes.Text18 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText19MapId:
                    productModel.CustomAttributes.Text19 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText20MapId:
                    productModel.CustomAttributes.Text20 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText21MapId:
                    productModel.CustomAttributes.Text21 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText22MapId:
                    productModel.CustomAttributes.Text22 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText23MapId:
                    productModel.CustomAttributes.Text23 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText24MapId:
                    productModel.CustomAttributes.Text24 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText25MapId:
                    productModel.CustomAttributes.Text25 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText26MapId:
                    productModel.CustomAttributes.Text26 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText27MapId:
                    productModel.CustomAttributes.Text27 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText28MapId:
                    productModel.CustomAttributes.Text28 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText29MapId:
                    productModel.CustomAttributes.Text29 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText30MapId:
                    productModel.CustomAttributes.Text30 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText31MapId:
                    productModel.CustomAttributes.Text31 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText32MapId:
                    productModel.CustomAttributes.Text32 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText33MapId:
                    productModel.CustomAttributes.Text33 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText34MapId:
                    productModel.CustomAttributes.Text34 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText35MapId:
                    productModel.CustomAttributes.Text35 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText36MapId:
                    productModel.CustomAttributes.Text36 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText37MapId:
                    productModel.CustomAttributes.Text37 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText38MapId:
                    productModel.CustomAttributes.Text38 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText39MapId:
                    productModel.CustomAttributes.Text39 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText40MapId:
                    productModel.CustomAttributes.Text40 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText41MapId:
                    productModel.CustomAttributes.Text41 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText42MapId:
                    productModel.CustomAttributes.Text42 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText43MapId:
                    productModel.CustomAttributes.Text43 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText44MapId:
                    productModel.CustomAttributes.Text44 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText45MapId:
                    productModel.CustomAttributes.Text45 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText46MapId:
                    productModel.CustomAttributes.Text46 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText47MapId:
                    productModel.CustomAttributes.Text47 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText48MapId:
                    productModel.CustomAttributes.Text48 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText49MapId:
                    productModel.CustomAttributes.Text49 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomText50MapId:
                    productModel.CustomAttributes.Text50 = Convert.ToString(cellValue, prov);
                    break;

                #endregion

                #region Custom Value Fields


                case ProductLibraryImportMappingList.CustomValue1MapId:
                    productModel.CustomAttributes.Value1 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue2MapId:
                    productModel.CustomAttributes.Value2 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue3MapId:
                    productModel.CustomAttributes.Value3 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue4MapId:
                    productModel.CustomAttributes.Value4 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue5MapId:
                    productModel.CustomAttributes.Value5 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue6MapId:
                    productModel.CustomAttributes.Value6 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue7MapId:
                    productModel.CustomAttributes.Value7 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue8MapId:
                    productModel.CustomAttributes.Value8 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue9MapId:
                    productModel.CustomAttributes.Value9 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue10MapId:
                    productModel.CustomAttributes.Value10 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue11MapId:
                    productModel.CustomAttributes.Value11 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue12MapId:
                    productModel.CustomAttributes.Value12 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue13MapId:
                    productModel.CustomAttributes.Value13 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue14MapId:
                    productModel.CustomAttributes.Value14 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue15MapId:
                    productModel.CustomAttributes.Value15 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue16MapId:
                    productModel.CustomAttributes.Value16 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue17MapId:
                    productModel.CustomAttributes.Value17 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue18MapId:
                    productModel.CustomAttributes.Value18 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue19MapId:
                    productModel.CustomAttributes.Value19 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue20MapId:
                    productModel.CustomAttributes.Value20 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue21MapId:
                    productModel.CustomAttributes.Value21 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue22MapId:
                    productModel.CustomAttributes.Value22 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue23MapId:
                    productModel.CustomAttributes.Value23 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue24MapId:
                    productModel.CustomAttributes.Value24 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue25MapId:
                    productModel.CustomAttributes.Value25 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue26MapId:
                    productModel.CustomAttributes.Value26 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue27MapId:
                    productModel.CustomAttributes.Value27 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue28MapId:
                    productModel.CustomAttributes.Value28 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue29MapId:
                    productModel.CustomAttributes.Value29 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue30MapId:
                    productModel.CustomAttributes.Value30 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue31MapId:
                    productModel.CustomAttributes.Value31 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue32MapId:
                    productModel.CustomAttributes.Value32 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue33MapId:
                    productModel.CustomAttributes.Value33 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue34MapId:
                    productModel.CustomAttributes.Value34 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue35MapId:
                    productModel.CustomAttributes.Value35 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue36MapId:
                    productModel.CustomAttributes.Value36 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue37MapId:
                    productModel.CustomAttributes.Value37 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue38MapId:
                    productModel.CustomAttributes.Value38 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue39MapId:
                    productModel.CustomAttributes.Value39 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue40MapId:
                    productModel.CustomAttributes.Value40 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue41MapId:
                    productModel.CustomAttributes.Value41 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue42MapId:
                    productModel.CustomAttributes.Value42 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue43MapId:
                    productModel.CustomAttributes.Value43 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue44MapId:
                    productModel.CustomAttributes.Value44 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue45MapId:
                    productModel.CustomAttributes.Value45 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue46MapId:
                    productModel.CustomAttributes.Value46 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue47MapId:
                    productModel.CustomAttributes.Value47 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue48MapId:
                    productModel.CustomAttributes.Value48 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue49MapId:
                    productModel.CustomAttributes.Value49 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomValue50MapId:
                    productModel.CustomAttributes.Value50 = Convert.ToSingle(cellValue, prov);
                    break;

                #endregion

                #region Flag

                case ProductLibraryImportMappingList.CustomFlag1MapId:
                    productModel.CustomAttributes.Flag1 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag2MapId:
                    productModel.CustomAttributes.Flag2 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag3MapId:
                    productModel.CustomAttributes.Flag3 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag4MapId:
                    productModel.CustomAttributes.Flag4 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag5MapId:
                    productModel.CustomAttributes.Flag5 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag6MapId:
                    productModel.CustomAttributes.Flag6 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag7MapId:
                    productModel.CustomAttributes.Flag7 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag8MapId:
                    productModel.CustomAttributes.Flag8 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag9MapId:
                    productModel.CustomAttributes.Flag9 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomFlag10MapId:
                    productModel.CustomAttributes.Flag10 = Convert.ToBoolean(cellValue, prov);
                    break;

                #endregion

                #region Custom Date Fields

                case ProductLibraryImportMappingList.CustomDate1MapId:
                    productModel.CustomAttributes.Date1 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate2MapId:
                    productModel.CustomAttributes.Date2 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate3MapId:
                    productModel.CustomAttributes.Date3 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate4MapId:
                    productModel.CustomAttributes.Date4 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate5MapId:
                    productModel.CustomAttributes.Date5 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate6MapId:
                    productModel.CustomAttributes.Date6 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate7MapId:
                    productModel.CustomAttributes.Date7 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate8MapId:
                    productModel.CustomAttributes.Date8 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate9MapId:
                    productModel.CustomAttributes.Date9 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductLibraryImportMappingList.CustomDate10MapId:
                    productModel.CustomAttributes.Date10 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                #endregion

                #region Custom Note Fields

                case ProductLibraryImportMappingList.CustomNote1MapId:
                    productModel.CustomAttributes.Note1 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomNote2MapId:
                    productModel.CustomAttributes.Note2 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomNote3MapId:
                    productModel.CustomAttributes.Note3 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomNote4MapId:
                    productModel.CustomAttributes.Note4 = Convert.ToString(cellValue, prov);
                    break;

                case ProductLibraryImportMappingList.CustomNote5MapId:
                    productModel.CustomAttributes.Note5 = Convert.ToString(cellValue, prov);
                    break;

                #endregion

                #region Performance Fields

                //case ProductLibraryImportMappingList.AchievedCasePacksMapId:
                //    productLibraryProductModel.PerformanceData.AchievedCasePacks = Convert.ToSingle(cellValue, prov);
                //    break;
                //case ProductLibraryImportMappingList.AchievedDeliveriesMapId:
                //    productLibraryProductModel.PerformanceData.AchievedDeliveries = Convert.ToSingle(cellValue, prov);
                //    break;
                //case ProductLibraryImportMappingList.AchievedDosMapId:
                //    productLibraryProductModel.PerformanceData.AchievedDos = Convert.ToSingle(cellValue, prov);
                //    break;
                //case ProductLibraryImportMappingList.AchievedShelfLifeMapId:
                //    productLibraryProductModel.PerformanceData.AchievedShelfLife = Convert.ToSingle(cellValue, prov);
                //    break;
                //case ProductLibraryImportMappingList.UnitsSoldPerDayMapId:
                //    productLibraryProductModel.PerformanceData.UnitsSoldPerDay = Convert.ToSingle(cellValue, prov);
                //    break;
                case ProductLibraryImportMappingList.PerformanceMetric1MapId:
                    productLibraryProductModel.PerformanceData.P1 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric2MapId:
                    productLibraryProductModel.PerformanceData.P2 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric3MapId:
                    productLibraryProductModel.PerformanceData.P3 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric4MapId:
                    productLibraryProductModel.PerformanceData.P4 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric5MapId:
                    productLibraryProductModel.PerformanceData.P5 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric6MapId:
                    productLibraryProductModel.PerformanceData.P6 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric7MapId:
                    productLibraryProductModel.PerformanceData.P7 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric8MapId:
                    productLibraryProductModel.PerformanceData.P8 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric9MapId:
                    productLibraryProductModel.PerformanceData.P9 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric10MapId:
                    productLibraryProductModel.PerformanceData.P10 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric11MapId:
                    productLibraryProductModel.PerformanceData.P11 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric12MapId:
                    productLibraryProductModel.PerformanceData.P12 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric13MapId:
                    productLibraryProductModel.PerformanceData.P13 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric14MapId:
                    productLibraryProductModel.PerformanceData.P14 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric15MapId:
                    productLibraryProductModel.PerformanceData.P15 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric16MapId:
                    productLibraryProductModel.PerformanceData.P16 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric17MapId:
                    productLibraryProductModel.PerformanceData.P17 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric18MapId:
                    productLibraryProductModel.PerformanceData.P18 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric19MapId:
                    productLibraryProductModel.PerformanceData.P19 = Convert.ToSingle(cellValue, prov);
                    break;
                case ProductLibraryImportMappingList.PerformanceMetric20MapId:
                    productLibraryProductModel.PerformanceData.P20 = Convert.ToSingle(cellValue, prov);
                    break;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        public override String GetBindingPath(Int32 mappingId)
        {
            return GetBindingPath(mappingId, null);
        }

        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;

            switch (mappingId)
            {
                #region Product Properties

                case ProductLibraryImportMappingList.AlternateDepthMapId: return String.Format("{0}{1}", prefix, Product.AlternateDepthProperty.Name);
                case ProductLibraryImportMappingList.AlternateHeightMapId: return String.Format("{0}{1}", prefix, Product.AlternateHeightProperty.Name);
                case ProductLibraryImportMappingList.AlternateWidthMapId: return String.Format("{0}{1}", prefix, Product.AlternateWidthProperty.Name);

                case ProductLibraryImportMappingList.CanBreakTrayBackMapId: return String.Format("{0}{1}", prefix, Product.CanBreakTrayBackProperty.Name);
                case ProductLibraryImportMappingList.CanBreakTrayDownMapId: return String.Format("{0}{1}", prefix, Product.CanBreakTrayDownProperty.Name);
                case ProductLibraryImportMappingList.CanBreakTrayTopMapId: return String.Format("{0}{1}", prefix, Product.CanBreakTrayTopProperty.Name);
                case ProductLibraryImportMappingList.CanBreakTrayUpMapId: return String.Format("{0}{1}", prefix, Product.CanBreakTrayUpProperty.Name);
                //case ProductLibraryImportMappingList.CanMerchandiseInChestMapId: return String.Format("{0}{1}", prefix, Product.CanMerchandiseInChestProperty.Name);
                //case ProductLibraryImportMappingList.CanMerchandiseOnBarMapId: return String.Format("{0}{1}", prefix, Product.CanMerchandiseOnBarProperty.Name);
                //case ProductLibraryImportMappingList.CanMerchandiseOnPegMapId: return String.Format("{0}{1}", prefix, Product.CanMerchandiseOnPegProperty.Name);
                //case ProductLibraryImportMappingList.CanMerchandiseOnShelfMapId: return String.Format("{0}{1}", prefix, Product.CanMerchandiseOnShelfProperty.Name);
                case ProductLibraryImportMappingList.CasePackUnitsMapId: return String.Format("{0}{1}", prefix, Product.CasePackUnitsProperty.Name);
                case ProductLibraryImportMappingList.CaseWideMapId: return String.Format("{0}{1}", prefix, Product.CaseWideProperty.Name);
                case ProductLibraryImportMappingList.CaseDeepMapId: return String.Format("{0}{1}", prefix, Product.CaseDeepProperty.Name);
                case ProductLibraryImportMappingList.CaseHighMapId: return String.Format("{0}{1}", prefix, Product.CaseHighProperty.Name);
                case ProductLibraryImportMappingList.CaseDepthMapId: return String.Format("{0}{1}", prefix, Product.CaseDepthProperty.Name);
                case ProductLibraryImportMappingList.CaseHeightMapId: return String.Format("{0}{1}", prefix, Product.CaseHeightProperty.Name);
                case ProductLibraryImportMappingList.CaseWidthMapId: return String.Format("{0}{1}", prefix, Product.CaseWidthProperty.Name);
                case ProductLibraryImportMappingList.DepthMapId: return String.Format("{0}{1}", prefix, Product.DepthProperty.Name);
                case ProductLibraryImportMappingList.DisplayDepthMapId: return String.Format("{0}{1}", prefix, Product.DisplayDepthProperty.Name);
                case ProductLibraryImportMappingList.DisplayHeightMapId: return String.Format("{0}{1}", prefix, Product.DisplayHeightProperty.Name);
                case ProductLibraryImportMappingList.DisplayWidthMapId: return String.Format("{0}{1}", prefix, Product.DisplayWidthProperty.Name);
                case ProductLibraryImportMappingList.FingerSpaceAboveMapId: return String.Format("{0}{1}", prefix, Product.FingerSpaceAboveProperty.Name);
                case ProductLibraryImportMappingList.FingerSpaceToTheSideMapId: return String.Format("{0}{1}", prefix, Product.FingerSpaceToTheSideProperty.Name);
                case ProductLibraryImportMappingList.ForceBottomCapMapId: return String.Format("{0}{1}", prefix, Product.ForceBottomCapProperty.Name);
                case ProductLibraryImportMappingList.ForceMiddleCapMapId: return String.Format("{0}{1}", prefix, Product.ForceMiddleCapProperty.Name);
                case ProductLibraryImportMappingList.FrontOverhangMapId: return String.Format("{0}{1}", prefix, Product.FrontOverhangProperty.Name);
                case ProductLibraryImportMappingList.GtinMapId: return String.Format("{0}{1}", prefix, Product.GtinProperty.Name);
                case ProductLibraryImportMappingList.HeightMapId: return String.Format("{0}{1}", prefix, Product.HeightProperty.Name);
                case ProductLibraryImportMappingList.IsActiveMapId: return String.Format("{0}{1}", prefix, Product.IsActiveProperty.Name);
                case ProductLibraryImportMappingList.IsFrontOnlyMapId: return String.Format("{0}{1}", prefix, Product.IsFrontOnlyProperty.Name);
                case ProductLibraryImportMappingList.MaxDeepMapId: return String.Format("{0}{1}", prefix, Product.MaxDeepProperty.Name);
                case ProductLibraryImportMappingList.MaxRightCapMapId: return String.Format("{0}{1}", prefix, Product.MaxRightCapProperty.Name);
                case ProductLibraryImportMappingList.MaxStackMapId: return String.Format("{0}{1}", prefix, Product.MaxStackProperty.Name);
                case ProductLibraryImportMappingList.MaxTopCapMapId: return String.Format("{0}{1}", prefix, Product.MaxTopCapProperty.Name);
                case ProductLibraryImportMappingList.MerchandisingStyleMapId: return String.Format("{0}{1}", prefix, Product.MerchandisingStyleProperty.Name);
                case ProductLibraryImportMappingList.MinDeepMapId: return String.Format("{0}{1}", prefix, Product.MinDeepProperty.Name);
                case ProductLibraryImportMappingList.NameMapId: return String.Format("{0}{1}", prefix, Product.NameProperty.Name);
                case ProductLibraryImportMappingList.NestingDepthMapId: return String.Format("{0}{1}", prefix, Product.NestingDepthProperty.Name);
                case ProductLibraryImportMappingList.NestingHeightMapId: return String.Format("{0}{1}", prefix, Product.NestingHeightProperty.Name);
                case ProductLibraryImportMappingList.NestingWidthMapId: return String.Format("{0}{1}", prefix, Product.NestingWidthProperty.Name);
                case ProductLibraryImportMappingList.NumberOfPegHolesMapId: return String.Format("{0}{1}", prefix, Product.NumberOfPegHolesProperty.Name);
                case ProductLibraryImportMappingList.OrientationTypeMapId: return String.Format("{0}{1}", prefix, Product.OrientationTypeProperty.Name);
                case ProductLibraryImportMappingList.PegDepthMapId: return String.Format("{0}{1}", prefix, Product.PegDepthProperty.Name);
                case ProductLibraryImportMappingList.PegProngOffsetXMapId: return String.Format("{0}{1}", prefix, Product.PegProngOffsetXProperty.Name);
                case ProductLibraryImportMappingList.PegX2MapId: return String.Format("{0}{1}", prefix, Product.PegX2Property.Name);
                case ProductLibraryImportMappingList.PegX3MapId: return String.Format("{0}{1}", prefix, Product.PegX3Property.Name);
                case ProductLibraryImportMappingList.PegXMapId: return String.Format("{0}{1}", prefix, Product.PegXProperty.Name);
                case ProductLibraryImportMappingList.PegY2MapId: return String.Format("{0}{1}", prefix, Product.PegY2Property.Name);
                case ProductLibraryImportMappingList.PegY3MapId: return String.Format("{0}{1}", prefix, Product.PegY3Property.Name);
                case ProductLibraryImportMappingList.PegYMapId: return String.Format("{0}{1}", prefix, Product.PegYProperty.Name);
                case ProductLibraryImportMappingList.PointOfPurchaseDepthMapId: return String.Format("{0}{1}", prefix, Product.PointOfPurchaseDepthProperty.Name);
                case ProductLibraryImportMappingList.PointOfPurchaseHeightMapId: return String.Format("{0}{1}", prefix, Product.PointOfPurchaseHeightProperty.Name);
                case ProductLibraryImportMappingList.PointOfPurchaseWidthMapId: return String.Format("{0}{1}", prefix, Product.PointOfPurchaseWidthProperty.Name);
                case ProductLibraryImportMappingList.SqueezeDepthMapId: return String.Format("{0}{1}", prefix, Product.SqueezeDepthProperty.Name);
                case ProductLibraryImportMappingList.SqueezeHeightMapId: return String.Format("{0}{1}", prefix, Product.SqueezeHeightProperty.Name);
                case ProductLibraryImportMappingList.SqueezeWidthMapId: return String.Format("{0}{1}", prefix, Product.SqueezeWidthProperty.Name);
                case ProductLibraryImportMappingList.StatusTypeMapId: return String.Format("{0}{1}", prefix, Product.StatusTypeProperty.Name);
                case ProductLibraryImportMappingList.TrayPackUnitsMapId: return String.Format("{0}{1}", prefix, Product.TrayPackUnitsProperty.Name);
                case ProductLibraryImportMappingList.TrayWideMapId: return String.Format("{0}{1}", prefix, Product.TrayWideProperty.Name);
                case ProductLibraryImportMappingList.TrayDeepMapId: return String.Format("{0}{1}", prefix, Product.TrayDeepProperty.Name);
                case ProductLibraryImportMappingList.TrayHighMapId: return String.Format("{0}{1}", prefix, Product.TrayHighProperty.Name);
                case ProductLibraryImportMappingList.TrayDepthMapId: return String.Format("{0}{1}", prefix, Product.TrayDepthProperty.Name);
                case ProductLibraryImportMappingList.TrayHeightMapId: return String.Format("{0}{1}", prefix, Product.TrayHeightProperty.Name);
                case ProductLibraryImportMappingList.TrayWidthMapId: return String.Format("{0}{1}", prefix, Product.TrayWidthProperty.Name);
                case ProductLibraryImportMappingList.TrayThickDepthMapId: return String.Format("{0}{1}", prefix, Product.TrayThickDepthProperty.Name);
                case ProductLibraryImportMappingList.TrayThickHeightMapId: return String.Format("{0}{1}", prefix, Product.TrayThickHeightProperty.Name);
                case ProductLibraryImportMappingList.TrayThickWidthMapId: return String.Format("{0}{1}", prefix, Product.TrayThickWidthProperty.Name);
                case ProductLibraryImportMappingList.WidthMapId: return String.Format("{0}{1}", prefix, Product.WidthProperty.Name);
                case ProductLibraryImportMappingList.IsPlaceHolderProductMapId: return String.Format("{0}{1}", prefix, Product.IsPlaceHolderProductProperty.Name);
                case ProductLibraryImportMappingList.ShapeTypeMapId: return String.Format("{0}{1}", prefix, Product.ShapeTypeProperty.Name);

                case ProductLibraryImportMappingList.ShapeMapId: return String.Format("{0}{1}", prefix, Product.ShapeProperty.Name);
                case ProductLibraryImportMappingList.FillColourMapId: return String.Format("{0}{1}", prefix, Product.FillColourProperty.Name);
                case ProductLibraryImportMappingList.FillPatternMapId: return String.Format("{0}{1}", prefix, Product.FillPatternTypeProperty.Name);

                #endregion

                #region Product Attribute Properties

                case ProductLibraryImportMappingList.HealthMapId: return String.Format("{0}{1}", prefix, Product.HealthProperty.Name);
                case ProductLibraryImportMappingList.BarCodeMapId: return String.Format("{0}{1}", prefix, Product.BarcodeProperty.Name);
                case ProductLibraryImportMappingList.BrandMapId: return String.Format("{0}{1}", prefix, Product.BrandProperty.Name);
                case ProductLibraryImportMappingList.CaseCostMapId: return String.Format("{0}{1}", prefix, Product.CaseCostProperty.Name);
                case ProductLibraryImportMappingList.ColourMapId: return String.Format("{0}{1}", prefix, Product.ColourProperty.Name);
                case ProductLibraryImportMappingList.ConsumerInformationMapId: return String.Format("{0}{1}", prefix, Product.ConsumerInformationProperty.Name);
                case ProductLibraryImportMappingList.CorporateCodeMapId: return String.Format("{0}{1}", prefix, Product.CorporateCodeProperty.Name);
                case ProductLibraryImportMappingList.CostPriceMapId: return String.Format("{0}{1}", prefix, Product.CostPriceProperty.Name);
                case ProductLibraryImportMappingList.CountryOfOriginMapId: return String.Format("{0}{1}", prefix, Product.CountryOfOriginProperty.Name);
                case ProductLibraryImportMappingList.CountryOfProcessingMapId: return String.Format("{0}{1}", prefix, Product.CountryOfProcessingProperty.Name);
                //case ProductLibraryImportMappingList.CustomerStatusMapId: return String.Format("{0}{1}", prefix,  Product.CustomerStatusProperty.Name);
                case ProductLibraryImportMappingList.DateDiscontinuedMapId: return String.Format("{0}{1}", prefix, Product.DateDiscontinuedProperty.Name);
                case ProductLibraryImportMappingList.DateEffectiveMapId: return String.Format("{0}{1}", prefix, Product.DateEffectiveProperty.Name);
                case ProductLibraryImportMappingList.DateIntroducedMapId: return String.Format("{0}{1}", prefix, Product.DateIntroducedProperty.Name);
                case ProductLibraryImportMappingList.DeliveryFrequencyDaysMapId: return String.Format("{0}{1}", prefix, Product.DeliveryFrequencyDaysProperty.Name);
                case ProductLibraryImportMappingList.DeliveryMethodMapId: return String.Format("{0}{1}", prefix, Product.DeliveryMethodProperty.Name);
                case ProductLibraryImportMappingList.FinancialGroupCodeMapId: return String.Format("{0}{1}", prefix, Product.FinancialGroupCodeProperty.Name);
                case ProductLibraryImportMappingList.FinancialGroupNameMapId: return String.Format("{0}{1}", prefix, Product.FinancialGroupNameProperty.Name);
                case ProductLibraryImportMappingList.FlavourMapId: return String.Format("{0}{1}", prefix, Product.FlavourProperty.Name);
                case ProductLibraryImportMappingList.GarmentTypeMapId: return String.Format("{0}{1}", prefix, Product.GarmentTypeProperty.Name);
                case ProductLibraryImportMappingList.IsNewMapId: return String.Format("{0}{1}", prefix, Product.IsNewProductProperty.Name);
                case ProductLibraryImportMappingList.IsPrivateLabelMapId: return String.Format("{0}{1}", prefix, Product.IsPrivateLabelProperty.Name);
                case ProductLibraryImportMappingList.ManufacturerCodeMapId: return String.Format("{0}{1}", prefix, Product.ManufacturerCodeProperty.Name);
                case ProductLibraryImportMappingList.ManufacturerMapId: return String.Format("{0}{1}", prefix, Product.ManufacturerProperty.Name);
                case ProductLibraryImportMappingList.ManufacturerRecommendedRetailPriceMapId: return String.Format("{0}{1}", prefix, Product.ManufacturerRecommendedRetailPriceProperty.Name);
                case ProductLibraryImportMappingList.ModelMapId: return String.Format("{0}{1}", prefix, Product.ModelProperty.Name);
                case ProductLibraryImportMappingList.PackagingShapeMapId: return String.Format("{0}{1}", prefix, Product.PackagingShapeProperty.Name);
                case ProductLibraryImportMappingList.PackagingTypeMapId: return String.Format("{0}{1}", prefix, Product.PackagingTypeProperty.Name);
                case ProductLibraryImportMappingList.PatternMapId: return String.Format("{0}{1}", prefix, Product.PatternProperty.Name);
                case ProductLibraryImportMappingList.PointOfPurchaseDescriptionMapId: return String.Format("{0}{1}", prefix, Product.PointOfPurchaseDescriptionProperty.Name);
                case ProductLibraryImportMappingList.RecommendedRetailPriceMapId: return String.Format("{0}{1}", prefix, Product.RecommendedRetailPriceProperty.Name);
                case ProductLibraryImportMappingList.SellPackCountMapId: return String.Format("{0}{1}", prefix, Product.SellPackCountProperty.Name);
                case ProductLibraryImportMappingList.SellPackDescriptionMapId: return String.Format("{0}{1}", prefix, Product.SellPackDescriptionProperty.Name);
                case ProductLibraryImportMappingList.SellPriceMapId: return String.Format("{0}{1}", prefix, Product.SellPriceProperty.Name);
                case ProductLibraryImportMappingList.ShelfLifeMapId: return String.Format("{0}{1}", prefix, Product.ShelfLifeProperty.Name);
                case ProductLibraryImportMappingList.ShortDescriptionMapId: return String.Format("{0}{1}", prefix, Product.ShortDescriptionProperty.Name);
                case ProductLibraryImportMappingList.SizeMapId: return String.Format("{0}{1}", prefix, Product.SizeProperty.Name);
                case ProductLibraryImportMappingList.StyleNumberMapId: return String.Format("{0}{1}", prefix, Product.StyleNumberProperty.Name);
                case ProductLibraryImportMappingList.SubCategoryMapId: return String.Format("{0}{1}", prefix, Product.SubcategoryProperty.Name);
                case ProductLibraryImportMappingList.TaxRateMapId: return String.Format("{0}{1}", prefix, Product.TaxRateProperty.Name);
                case ProductLibraryImportMappingList.TextureMapId: return String.Format("{0}{1}", prefix, Product.TextureProperty.Name);
                case ProductLibraryImportMappingList.UnitOfMeasureMapId: return String.Format("{0}{1}", prefix, Product.UnitOfMeasureProperty.Name);
                case ProductLibraryImportMappingList.VendorCodeMapId: return String.Format("{0}{1}", prefix, Product.VendorCodeProperty.Name);
                case ProductLibraryImportMappingList.VendorMapId: return String.Format("{0}{1}", prefix, Product.VendorProperty.Name);
                case ProductLibraryImportMappingList.CustomerStatusMapId: return String.Format("{0}{1}", prefix, Product.CustomerStatusProperty.Name);

                #endregion

                #region Custom Fields

                case ProductLibraryImportMappingList.CustomText1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text1Property.Name);
                case ProductLibraryImportMappingList.CustomText2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text2Property.Name);
                case ProductLibraryImportMappingList.CustomText3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text3Property.Name);
                case ProductLibraryImportMappingList.CustomText4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text4Property.Name);
                case ProductLibraryImportMappingList.CustomText5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text5Property.Name);
                case ProductLibraryImportMappingList.CustomText6MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text6Property.Name);
                case ProductLibraryImportMappingList.CustomText7MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text7Property.Name);
                case ProductLibraryImportMappingList.CustomText8MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text8Property.Name);
                case ProductLibraryImportMappingList.CustomText9MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text9Property.Name);
                case ProductLibraryImportMappingList.CustomText10MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text10Property.Name);
                case ProductLibraryImportMappingList.CustomText11MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text11Property.Name);
                case ProductLibraryImportMappingList.CustomText12MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text12Property.Name);
                case ProductLibraryImportMappingList.CustomText13MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text13Property.Name);
                case ProductLibraryImportMappingList.CustomText14MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text14Property.Name);
                case ProductLibraryImportMappingList.CustomText15MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text15Property.Name);
                case ProductLibraryImportMappingList.CustomText16MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text16Property.Name);
                case ProductLibraryImportMappingList.CustomText17MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text17Property.Name);
                case ProductLibraryImportMappingList.CustomText18MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text18Property.Name);
                case ProductLibraryImportMappingList.CustomText19MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text19Property.Name);
                case ProductLibraryImportMappingList.CustomText20MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text20Property.Name);
                case ProductLibraryImportMappingList.CustomText21MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text21Property.Name);
                case ProductLibraryImportMappingList.CustomText22MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text22Property.Name);
                case ProductLibraryImportMappingList.CustomText23MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text23Property.Name);
                case ProductLibraryImportMappingList.CustomText24MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text24Property.Name);
                case ProductLibraryImportMappingList.CustomText25MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text25Property.Name);
                case ProductLibraryImportMappingList.CustomText26MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text26Property.Name);
                case ProductLibraryImportMappingList.CustomText27MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text27Property.Name);
                case ProductLibraryImportMappingList.CustomText28MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text28Property.Name);
                case ProductLibraryImportMappingList.CustomText29MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text29Property.Name);
                case ProductLibraryImportMappingList.CustomText30MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text30Property.Name);
                case ProductLibraryImportMappingList.CustomText31MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text31Property.Name);
                case ProductLibraryImportMappingList.CustomText32MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text32Property.Name);
                case ProductLibraryImportMappingList.CustomText33MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text33Property.Name);
                case ProductLibraryImportMappingList.CustomText34MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text34Property.Name);
                case ProductLibraryImportMappingList.CustomText35MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text35Property.Name);
                case ProductLibraryImportMappingList.CustomText36MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text36Property.Name);
                case ProductLibraryImportMappingList.CustomText37MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text37Property.Name);
                case ProductLibraryImportMappingList.CustomText38MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text38Property.Name);
                case ProductLibraryImportMappingList.CustomText39MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text39Property.Name);
                case ProductLibraryImportMappingList.CustomText40MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text40Property.Name);
                case ProductLibraryImportMappingList.CustomText41MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text41Property.Name);
                case ProductLibraryImportMappingList.CustomText42MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text42Property.Name);
                case ProductLibraryImportMappingList.CustomText43MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text43Property.Name);
                case ProductLibraryImportMappingList.CustomText44MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text44Property.Name);
                case ProductLibraryImportMappingList.CustomText45MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text45Property.Name);
                case ProductLibraryImportMappingList.CustomText46MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text46Property.Name);
                case ProductLibraryImportMappingList.CustomText47MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text47Property.Name);
                case ProductLibraryImportMappingList.CustomText48MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text48Property.Name);
                case ProductLibraryImportMappingList.CustomText49MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text49Property.Name);
                case ProductLibraryImportMappingList.CustomText50MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text50Property.Name);

                case ProductLibraryImportMappingList.CustomValue1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value1Property.Name);
                case ProductLibraryImportMappingList.CustomValue2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value2Property.Name);
                case ProductLibraryImportMappingList.CustomValue3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value3Property.Name);
                case ProductLibraryImportMappingList.CustomValue4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value4Property.Name);
                case ProductLibraryImportMappingList.CustomValue5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value5Property.Name);
                case ProductLibraryImportMappingList.CustomValue6MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value6Property.Name);
                case ProductLibraryImportMappingList.CustomValue7MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value7Property.Name);
                case ProductLibraryImportMappingList.CustomValue8MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value8Property.Name);
                case ProductLibraryImportMappingList.CustomValue9MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value9Property.Name);
                case ProductLibraryImportMappingList.CustomValue10MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value10Property.Name);
                case ProductLibraryImportMappingList.CustomValue11MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value11Property.Name);
                case ProductLibraryImportMappingList.CustomValue12MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value12Property.Name);
                case ProductLibraryImportMappingList.CustomValue13MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value13Property.Name);
                case ProductLibraryImportMappingList.CustomValue14MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value14Property.Name);
                case ProductLibraryImportMappingList.CustomValue15MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value15Property.Name);
                case ProductLibraryImportMappingList.CustomValue16MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value16Property.Name);
                case ProductLibraryImportMappingList.CustomValue17MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value17Property.Name);
                case ProductLibraryImportMappingList.CustomValue18MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value18Property.Name);
                case ProductLibraryImportMappingList.CustomValue19MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value19Property.Name);
                case ProductLibraryImportMappingList.CustomValue20MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value20Property.Name);
                case ProductLibraryImportMappingList.CustomValue21MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value21Property.Name);
                case ProductLibraryImportMappingList.CustomValue22MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value22Property.Name);
                case ProductLibraryImportMappingList.CustomValue23MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value23Property.Name);
                case ProductLibraryImportMappingList.CustomValue24MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value24Property.Name);
                case ProductLibraryImportMappingList.CustomValue25MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value25Property.Name);
                case ProductLibraryImportMappingList.CustomValue26MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value26Property.Name);
                case ProductLibraryImportMappingList.CustomValue27MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value27Property.Name);
                case ProductLibraryImportMappingList.CustomValue28MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value28Property.Name);
                case ProductLibraryImportMappingList.CustomValue29MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value29Property.Name);
                case ProductLibraryImportMappingList.CustomValue30MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value30Property.Name);
                case ProductLibraryImportMappingList.CustomValue31MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value31Property.Name);
                case ProductLibraryImportMappingList.CustomValue32MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value32Property.Name);
                case ProductLibraryImportMappingList.CustomValue33MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value33Property.Name);
                case ProductLibraryImportMappingList.CustomValue34MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value34Property.Name);
                case ProductLibraryImportMappingList.CustomValue35MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value35Property.Name);
                case ProductLibraryImportMappingList.CustomValue36MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value36Property.Name);
                case ProductLibraryImportMappingList.CustomValue37MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value37Property.Name);
                case ProductLibraryImportMappingList.CustomValue38MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value38Property.Name);
                case ProductLibraryImportMappingList.CustomValue39MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value39Property.Name);
                case ProductLibraryImportMappingList.CustomValue40MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value40Property.Name);
                case ProductLibraryImportMappingList.CustomValue41MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value41Property.Name);
                case ProductLibraryImportMappingList.CustomValue42MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value42Property.Name);
                case ProductLibraryImportMappingList.CustomValue43MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value43Property.Name);
                case ProductLibraryImportMappingList.CustomValue44MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value44Property.Name);
                case ProductLibraryImportMappingList.CustomValue45MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value45Property.Name);
                case ProductLibraryImportMappingList.CustomValue46MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value46Property.Name);
                case ProductLibraryImportMappingList.CustomValue47MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value47Property.Name);
                case ProductLibraryImportMappingList.CustomValue48MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value48Property.Name);
                case ProductLibraryImportMappingList.CustomValue49MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value49Property.Name);
                case ProductLibraryImportMappingList.CustomValue50MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value50Property.Name);

                case ProductLibraryImportMappingList.CustomFlag1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag1Property.Name);
                case ProductLibraryImportMappingList.CustomFlag2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag2Property.Name);
                case ProductLibraryImportMappingList.CustomFlag3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag3Property.Name);
                case ProductLibraryImportMappingList.CustomFlag4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag4Property.Name);
                case ProductLibraryImportMappingList.CustomFlag5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag5Property.Name);
                case ProductLibraryImportMappingList.CustomFlag6MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag6Property.Name);
                case ProductLibraryImportMappingList.CustomFlag7MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag7Property.Name);
                case ProductLibraryImportMappingList.CustomFlag8MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag8Property.Name);
                case ProductLibraryImportMappingList.CustomFlag9MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag9Property.Name);
                case ProductLibraryImportMappingList.CustomFlag10MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag10Property.Name);

                case ProductLibraryImportMappingList.CustomDate1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date1Property.Name);
                case ProductLibraryImportMappingList.CustomDate2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date2Property.Name); ;
                case ProductLibraryImportMappingList.CustomDate3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date3Property.Name); ;
                case ProductLibraryImportMappingList.CustomDate4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date4Property.Name); ;
                case ProductLibraryImportMappingList.CustomDate5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date5Property.Name); ;
                case ProductLibraryImportMappingList.CustomDate6MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date6Property.Name); ;
                case ProductLibraryImportMappingList.CustomDate7MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date7Property.Name); ;
                case ProductLibraryImportMappingList.CustomDate8MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date8Property.Name); ;
                case ProductLibraryImportMappingList.CustomDate9MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date9Property.Name); ;
                case ProductLibraryImportMappingList.CustomDate10MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date10Property.Name); ;

                case ProductLibraryImportMappingList.CustomNote1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note1Property.Name);
                case ProductLibraryImportMappingList.CustomNote2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note2Property.Name);
                case ProductLibraryImportMappingList.CustomNote3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note3Property.Name);
                case ProductLibraryImportMappingList.CustomNote4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note4Property.Name);
                case ProductLibraryImportMappingList.CustomNote5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note5Property.Name);

                #endregion

                #region Performance Field Related
                //case ProductLibraryImportMappingList.AchievedCasePacksMapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.AchievedCasePacksProperty.Name);
                //case ProductLibraryImportMappingList.AchievedDeliveriesMapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.AchievedDeliveriesProperty.Name);
                //case ProductLibraryImportMappingList.AchievedDosMapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.AchievedDosProperty.Name);
                //case ProductLibraryImportMappingList.AchievedShelfLifeMapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.AchievedShelfLifeProperty.Name);
                //case ProductLibraryImportMappingList.UnitsSoldPerDayMapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.UnitsSoldPerDayProperty.Name);
                case ProductLibraryImportMappingList.PerformanceMetric1MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P1Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric2MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P2Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric3MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P3Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric4MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P4Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric5MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P5Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric6MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P6Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric7MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P7Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric8MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P8Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric9MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P9Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric10MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P10Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric11MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P11Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric12MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P12Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric13MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P13Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric14MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P14Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric15MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P15Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric16MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P16Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric17MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P17Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric18MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P18Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric19MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P19Property.Name);
                case ProductLibraryImportMappingList.PerformanceMetric20MapId: return String.Format("{0}{1}.{2}", prefix, ProductLibraryProduct.PerformanceDataProperty.Name, ProductLibraryProductPerformanceData.P20Property.Name);

                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Static helper to help find column group name for product library columns
        /// </summary>
        /// <param name="destinationPropertyName"></param>
        /// <returns></returns>
        public static String GetDestinationColumnGroupName(String destinationPropertyName)
        {
            //Find corresponding maping
            ProductLibraryImportMappingList productMappingList = ProductLibraryImportMappingList.NewProductLibraryImportMappingList();
            ImportMapping importColumnMapping = productMappingList.Where(p => p.PropertyName.ToLowerInvariant() == destinationPropertyName.ToLowerInvariant()).FirstOrDefault();

            //If mapping has been found
            if (importColumnMapping != null)
            {
                return productMappingList.GetColumnGroupName(importColumnMapping.PropertyIdentifier);
            }

            //Default return no group
            return null;
        }

        public String GetColumnGroupName(Int32 mappingId)
        {
            String detailsGroup = Message.ProductLibraryImportMapping_DetailsGroup;
            String attributesGroup = Message.ProductLibraryImportMapping_AttributesGroup;
            String sizeGroup = Message.ProductLibraryImportMapping_SizeGroup;
            String placementsGroup = Message.ProductLibraryImportMapping_PlacementsGroup;
            String customTextGroup = Message.ProductLibraryImportMapping_CustomTextGroup;
            String customValueGroup = Message.ProductLibraryImportMapping_CustomValueGroup;
            String customFlagGroup = Message.ProductLibraryImportMapping_CustomFlagGroup;
            String customDateGroup = Message.ProductLibraryImportMapping_CustomDateGroup;
            String customNoteGroup = Message.ProductLibraryImportMapping_CustomNoteGroup;
            String performanceDataGroup = Message.ProductLibraryImportMapping_PerformanceDataGroup;

            String noGroup = null;

            switch (mappingId)
            {
                #region Product Properties

                case ProductLibraryImportMappingList.AlternateDepthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.AlternateHeightMapId: return sizeGroup;
                case ProductLibraryImportMappingList.AlternateWidthMapId: return sizeGroup;

                case ProductLibraryImportMappingList.CanBreakTrayBackMapId: return placementsGroup;
                case ProductLibraryImportMappingList.CanBreakTrayDownMapId: return placementsGroup;
                case ProductLibraryImportMappingList.CanBreakTrayTopMapId: return placementsGroup;
                case ProductLibraryImportMappingList.CanBreakTrayUpMapId: return placementsGroup;
                case ProductLibraryImportMappingList.CasePackUnitsMapId: return sizeGroup;
                case ProductLibraryImportMappingList.DepthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.DisplayDepthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.DisplayHeightMapId: return sizeGroup;
                case ProductLibraryImportMappingList.DisplayWidthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.FingerSpaceAboveMapId: return placementsGroup;
                case ProductLibraryImportMappingList.FingerSpaceToTheSideMapId: return placementsGroup;
                case ProductLibraryImportMappingList.ForceBottomCapMapId: return placementsGroup;
                case ProductLibraryImportMappingList.ForceMiddleCapMapId: return placementsGroup;
                case ProductLibraryImportMappingList.FrontOverhangMapId: return placementsGroup;
                case ProductLibraryImportMappingList.GtinMapId: return noGroup;
                case ProductLibraryImportMappingList.HeightMapId: return sizeGroup;
                case ProductLibraryImportMappingList.IsActiveMapId: return attributesGroup;
                case ProductLibraryImportMappingList.IsFrontOnlyMapId: return placementsGroup;
                case ProductLibraryImportMappingList.IsPlaceHolderProductMapId: return detailsGroup;
                case ProductLibraryImportMappingList.MaxDeepMapId: return placementsGroup;
                case ProductLibraryImportMappingList.MaxRightCapMapId: return placementsGroup;
                case ProductLibraryImportMappingList.MaxStackMapId: return placementsGroup;
                case ProductLibraryImportMappingList.MaxTopCapMapId: return placementsGroup;
                case ProductLibraryImportMappingList.MerchandisingStyleMapId: return placementsGroup;
                case ProductLibraryImportMappingList.MinDeepMapId: return placementsGroup;
                case ProductLibraryImportMappingList.NameMapId: return noGroup;
                case ProductLibraryImportMappingList.NestingDepthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.NestingHeightMapId: return sizeGroup;
                case ProductLibraryImportMappingList.NestingWidthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.NumberOfPegHolesMapId: return placementsGroup;
                case ProductLibraryImportMappingList.OrientationTypeMapId: return detailsGroup;
                case ProductLibraryImportMappingList.PegDepthMapId: return placementsGroup;
                case ProductLibraryImportMappingList.PegProngOffsetXMapId: return placementsGroup;
                case ProductLibraryImportMappingList.PegX2MapId: return placementsGroup;
                case ProductLibraryImportMappingList.PegX3MapId: return placementsGroup;
                case ProductLibraryImportMappingList.PegXMapId: return placementsGroup;
                case ProductLibraryImportMappingList.PegY2MapId: return placementsGroup;
                case ProductLibraryImportMappingList.PegY3MapId: return placementsGroup;
                case ProductLibraryImportMappingList.PegYMapId: return placementsGroup;
                case ProductLibraryImportMappingList.PointOfPurchaseDepthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.PointOfPurchaseHeightMapId: return sizeGroup;
                case ProductLibraryImportMappingList.PointOfPurchaseWidthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.SqueezeDepthMapId: return placementsGroup;
                case ProductLibraryImportMappingList.SqueezeHeightMapId: return placementsGroup;
                case ProductLibraryImportMappingList.SqueezeWidthMapId: return placementsGroup;
                case ProductLibraryImportMappingList.StatusTypeMapId: return detailsGroup;
                case ProductLibraryImportMappingList.TrayWideMapId: return sizeGroup;
                case ProductLibraryImportMappingList.TrayDeepMapId: return sizeGroup;
                case ProductLibraryImportMappingList.TrayHighMapId: return sizeGroup;
                case ProductLibraryImportMappingList.TrayHeightMapId: return sizeGroup;
                case ProductLibraryImportMappingList.TrayThickDepthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.TrayThickHeightMapId: return sizeGroup;
                case ProductLibraryImportMappingList.TrayThickWidthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.TrayPackUnitsMapId: return sizeGroup;
                case ProductLibraryImportMappingList.WidthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.FillColourMapId: return attributesGroup;
                case ProductLibraryImportMappingList.FillPatternMapId: return attributesGroup;
                case ProductLibraryImportMappingList.ShapeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.TrayWidthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.TrayDepthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.CaseHeightMapId: return sizeGroup;
                case ProductLibraryImportMappingList.CaseWidthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.CaseDepthMapId: return sizeGroup;
                case ProductLibraryImportMappingList.CaseHighMapId: return sizeGroup;
                case ProductLibraryImportMappingList.CaseWideMapId: return sizeGroup;
                case ProductLibraryImportMappingList.CaseDeepMapId: return sizeGroup;
                case ProductLibraryImportMappingList.ShapeTypeMapId: return detailsGroup;

                #endregion

                #region Product Attribute Properties

                case ProductLibraryImportMappingList.HealthMapId: return attributesGroup;
                case ProductLibraryImportMappingList.BarCodeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.BrandMapId: return attributesGroup;
                case ProductLibraryImportMappingList.CaseCostMapId: return detailsGroup;
                case ProductLibraryImportMappingList.ColourMapId: return attributesGroup;
                case ProductLibraryImportMappingList.ConsumerInformationMapId: return attributesGroup;
                case ProductLibraryImportMappingList.CorporateCodeMapId: return detailsGroup;
                case ProductLibraryImportMappingList.CostPriceMapId: return detailsGroup;
                case ProductLibraryImportMappingList.CountryOfOriginMapId: return attributesGroup;
                case ProductLibraryImportMappingList.CountryOfProcessingMapId: return attributesGroup;
                case ProductLibraryImportMappingList.DateDiscontinuedMapId: return detailsGroup;
                case ProductLibraryImportMappingList.DateEffectiveMapId: return detailsGroup;
                case ProductLibraryImportMappingList.DateIntroducedMapId: return detailsGroup;
                case ProductLibraryImportMappingList.DeliveryFrequencyDaysMapId: return attributesGroup;
                case ProductLibraryImportMappingList.DeliveryMethodMapId: return attributesGroup;
                case ProductLibraryImportMappingList.FinancialGroupCodeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.FinancialGroupNameMapId: return attributesGroup;
                case ProductLibraryImportMappingList.FlavourMapId: return attributesGroup;
                case ProductLibraryImportMappingList.GarmentTypeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.IsNewMapId: return attributesGroup;
                case ProductLibraryImportMappingList.IsPrivateLabelMapId: return detailsGroup;
                case ProductLibraryImportMappingList.ManufacturerCodeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.ManufacturerMapId: return attributesGroup;
                case ProductLibraryImportMappingList.ManufacturerRecommendedRetailPriceMapId: return attributesGroup;
                case ProductLibraryImportMappingList.ModelMapId: return attributesGroup;
                case ProductLibraryImportMappingList.PackagingShapeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.PackagingTypeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.PatternMapId: return attributesGroup;
                case ProductLibraryImportMappingList.PointOfPurchaseDescriptionMapId: return sizeGroup;
                case ProductLibraryImportMappingList.RecommendedRetailPriceMapId: return detailsGroup;
                case ProductLibraryImportMappingList.SellPackCountMapId: return attributesGroup;
                case ProductLibraryImportMappingList.SellPackDescriptionMapId: return attributesGroup;
                case ProductLibraryImportMappingList.SellPriceMapId: return detailsGroup;
                case ProductLibraryImportMappingList.ShelfLifeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.ShortDescriptionMapId: return detailsGroup;
                case ProductLibraryImportMappingList.SizeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.StyleNumberMapId: return attributesGroup;
                case ProductLibraryImportMappingList.SubCategoryMapId: return attributesGroup;
                case ProductLibraryImportMappingList.TaxRateMapId: return detailsGroup;
                case ProductLibraryImportMappingList.TextureMapId: return attributesGroup;
                case ProductLibraryImportMappingList.UnitOfMeasureMapId: return attributesGroup;
                case ProductLibraryImportMappingList.VendorCodeMapId: return attributesGroup;
                case ProductLibraryImportMappingList.VendorMapId: return attributesGroup;
                case ProductLibraryImportMappingList.CustomerStatusMapId: return attributesGroup;
                #endregion

                #region Custom Properties

                case ProductLibraryImportMappingList.CustomText1MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText2MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText3MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText4MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText5MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText6MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText7MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText8MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText9MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText10MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText11MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText12MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText13MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText14MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText15MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText16MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText17MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText18MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText19MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText20MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText21MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText22MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText23MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText24MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText25MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText26MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText27MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText28MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText29MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText30MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText31MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText32MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText33MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText34MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText35MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText36MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText37MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText38MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText39MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText40MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText41MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText42MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText43MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText44MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText45MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText46MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText47MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText48MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText49MapId: return customTextGroup;
                case ProductLibraryImportMappingList.CustomText50MapId: return customTextGroup;

                case ProductLibraryImportMappingList.CustomValue1MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue2MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue3MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue4MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue5MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue6MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue7MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue8MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue9MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue10MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue11MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue12MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue13MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue14MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue15MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue16MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue17MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue18MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue19MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue20MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue21MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue22MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue23MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue24MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue25MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue26MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue27MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue28MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue29MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue30MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue31MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue32MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue33MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue34MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue35MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue36MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue37MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue38MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue39MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue40MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue41MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue42MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue43MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue44MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue45MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue46MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue47MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue48MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue49MapId: return customValueGroup;
                case ProductLibraryImportMappingList.CustomValue50MapId: return customValueGroup;

                case ProductLibraryImportMappingList.CustomFlag1MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag2MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag3MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag4MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag5MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag6MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag7MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag8MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag9MapId: return customFlagGroup;
                case ProductLibraryImportMappingList.CustomFlag10MapId: return customFlagGroup;

                case ProductLibraryImportMappingList.CustomDate1MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate2MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate3MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate4MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate5MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate6MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate7MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate8MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate9MapId: return customDateGroup;
                case ProductLibraryImportMappingList.CustomDate10MapId: return customDateGroup;

                case ProductLibraryImportMappingList.CustomNote1MapId: return customNoteGroup;
                case ProductLibraryImportMappingList.CustomNote2MapId: return customNoteGroup;
                case ProductLibraryImportMappingList.CustomNote3MapId: return customNoteGroup;
                case ProductLibraryImportMappingList.CustomNote4MapId: return customNoteGroup;
                case ProductLibraryImportMappingList.CustomNote5MapId: return customNoteGroup;
                #endregion

                #region Performance field related

                //case ProductLibraryImportMappingList.AchievedCasePacksMapId: return performanceDataGroup;
                //case ProductLibraryImportMappingList.AchievedDeliveriesMapId: return performanceDataGroup;
                //case ProductLibraryImportMappingList.AchievedDosMapId: return performanceDataGroup;
                //case ProductLibraryImportMappingList.AchievedShelfLifeMapId: return performanceDataGroup;
                //case ProductLibraryImportMappingList.UnitsSoldPerDayMapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric1MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric2MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric3MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric4MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric5MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric6MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric7MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric8MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric9MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric10MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric11MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric12MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric13MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric14MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric15MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric16MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric17MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric18MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric19MapId: return performanceDataGroup;
                case ProductLibraryImportMappingList.PerformanceMetric20MapId: return performanceDataGroup;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        #endregion

    }
}
