﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (GFS 1.0)
// V8-25455 : J.Pickup
//  Copied over from GFS
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
#endregion

#region Version History: (CCM 8.3.0)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion

#endregion

using System;
using System.Globalization;
using Galleria.Framework.Imports;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Planograms.Model;



namespace Galleria.Ccm.Imports.Mappings
{
    public class AssortmentImportMappingList : ImportMappingList<AssortmentImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        public const Int16 AssortmentNameMapId = 0;
        public const Int32 ProductGroupCodeMapId = 1;
        public const Int32 ProductGTINMapId = 2;
        public const Int32 IsRangedMapId = 3;
        public const Int32 RankMapId = 4;
        public const Int32 FacingsMapId = 5;
        public const Int32 UnitsMapId = 6;
        public const Int32 SegmentationMapId = 7;
        public const Int32 ProductTreatmentTypeMapId = 8;
        public const Int32 CommentsMapId = 9;
        
        #endregion

        #region Constructors
        private AssortmentImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods

        public static AssortmentImportMappingList NewAssortmentImportMappingList()
        {
            AssortmentImportMappingList list = new AssortmentImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region AssortmentProperties

            this.Add(ImportMapping.NewImportMapping(AssortmentNameMapId, String.Format(Message.Assortment_Name, Assortment.NameProperty.FriendlyName), Assortment.NameProperty.Description, true, true, Assortment.NameProperty.Type, Assortment.NameProperty.DefaultValue, 0, 255, false));
            this.Add(ImportMapping.NewImportMapping(ProductGroupCodeMapId, Message.Assortment_ProductGroupCode, ProductGroup.CodeProperty.Description, true, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 0, 50, false));
            this.Add(ImportMapping.NewImportMapping(ProductGTINMapId, String.Format(Message.AssortmentProduct_ProductGTIN, Product.GtinProperty.FriendlyName), Product.GtinProperty.Description, true, true, Product.GtinProperty.Type, Product.GtinProperty.DefaultValue, 0, 14, false));
            this.Add(ImportMapping.NewImportMapping(IsRangedMapId, String.Format(Message.AssortmentProduct_IsRanged, AssortmentProduct.IsRangedProperty.FriendlyName), AssortmentProduct.IsRangedProperty.Description, false, AssortmentProduct.IsRangedProperty.Type, AssortmentProduct.IsRangedProperty.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(RankMapId, String.Format(Message.AssortmentProduct_Rank, AssortmentProduct.RankProperty.FriendlyName), AssortmentProduct.RankProperty.Description, false, AssortmentProduct.RankProperty.Type, AssortmentProduct.RankProperty.DefaultValue, 0, Int16.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(FacingsMapId, String.Format(Message.AssortmentProduct_Facings, AssortmentProduct.FacingsProperty.FriendlyName), AssortmentProduct.FacingsProperty.Description, false, AssortmentProduct.FacingsProperty.Type, AssortmentProduct.FacingsProperty.DefaultValue, 0, Byte.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(UnitsMapId, String.Format(Message.AssortmentProduct_Units, AssortmentProduct.UnitsProperty.FriendlyName), AssortmentProduct.UnitsProperty.Description, false, AssortmentProduct.UnitsProperty.Type, AssortmentProduct.UnitsProperty.DefaultValue, 0, Int16.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(SegmentationMapId, String.Format(Message.AssortmentProduct_Segmentation, AssortmentProduct.SegmentationProperty.FriendlyName), AssortmentProduct.SegmentationProperty.Description, false, AssortmentProduct.SegmentationProperty.Type, AssortmentProduct.SegmentationProperty.DefaultValue, 0, 300, true));
            this.Add(ImportMapping.NewImportMapping(ProductTreatmentTypeMapId, String.Format(Message.AssortmentProduct_ProductTreatmentType, AssortmentProduct.ProductTreatmentTypeProperty.FriendlyName), AssortmentProduct.ProductTreatmentTypeProperty.Description, false, false, AssortmentProduct.ProductTreatmentTypeProperty.Type, AssortmentProduct.ProductTreatmentTypeProperty.DefaultValue, PlanogramAssortmentProductTreatmentTypeHelper.FriendlyNames, true));
            this.Add(ImportMapping.NewImportMapping(CommentsMapId, String.Format(Message.AssortmentProduct_Comments, AssortmentProduct.CommentsProperty.FriendlyName), AssortmentProduct.CommentsProperty.Description, false, AssortmentProduct.CommentsProperty.Type, AssortmentProduct.CommentsProperty.DefaultValue, 0, 100, true));

            #endregion

            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Static method for returning a column group from its mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public static String GetColumnGroup(Int32 mappingId)
        {
            String detailsGroup = Message.ProductImportMappingList_ColGroup_Details;
            String attributesGroup = Message.ProductImportMappingList_ColGroup_Attributes;

            switch (mappingId)
            {
                #region Product Properties

                case AssortmentImportMappingList.AssortmentNameMapId: return detailsGroup;
                case AssortmentImportMappingList.ProductGroupCodeMapId: return detailsGroup;
                case AssortmentImportMappingList.ProductGTINMapId: return detailsGroup;
                case AssortmentImportMappingList.IsRangedMapId: return detailsGroup;
                case AssortmentImportMappingList.RankMapId: return detailsGroup;
                case AssortmentImportMappingList.FacingsMapId: return detailsGroup;
                case AssortmentImportMappingList.UnitsMapId: return detailsGroup;
                case AssortmentImportMappingList.SegmentationMapId: return detailsGroup;
                case AssortmentImportMappingList.ProductTreatmentTypeMapId: return detailsGroup;
                case AssortmentImportMappingList.CommentsMapId: return detailsGroup;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="assortmentDto"></param>
        /// <param name="assortmentProductDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, AssortmentInfoDto assortmentDto, AssortmentProductDto assortmentProductDto)
        {
            switch (mappingId)
            {
                #region AssortmentProperties
                case AssortmentImportMappingList.AssortmentNameMapId: return assortmentDto.Name;
                case AssortmentImportMappingList.ProductGroupCodeMapId: return assortmentDto.ProductGroupId;
                case AssortmentImportMappingList.ProductGTINMapId: return assortmentProductDto.ProductId;
                case AssortmentImportMappingList.IsRangedMapId: return assortmentProductDto.IsRanged;
                case AssortmentImportMappingList.RankMapId: return assortmentProductDto.Rank;
                case AssortmentImportMappingList.FacingsMapId: return assortmentProductDto.Facings;
                case AssortmentImportMappingList.UnitsMapId: return assortmentProductDto.Units;
                case AssortmentImportMappingList.SegmentationMapId: return assortmentProductDto.Segmentation;
                case AssortmentImportMappingList.ProductTreatmentTypeMapId: return assortmentProductDto.ProductTreatmentType;
                case AssortmentImportMappingList.CommentsMapId: return assortmentProductDto.Comments;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue">derived from initial cell value</param>
        /// <param name="assortmentDto"></param>
        /// <param name="assortmentProductDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            AssortmentDto assortmentDto, AssortmentProductDto assortmentProductDto)
        {
            IFormatProvider prov = CultureInfo.CurrentCulture;

            switch (mappingId)
            {
                #region AssortmentProperties
                case AssortmentImportMappingList.AssortmentNameMapId:
                    assortmentDto.Name = Convert.ToString(cellValue, prov);
                    break;
                case AssortmentImportMappingList.ProductGroupCodeMapId:
                    if (cellValue != null)
                    {
                        assortmentDto.ProductGroupId = Convert.ToInt32(cellValue, prov);
                    }
                    break;
                case AssortmentImportMappingList.ProductGTINMapId:
                    assortmentProductDto.ProductId = Convert.ToInt32(cellValue, prov);
                    break;
                case AssortmentImportMappingList.IsRangedMapId:
                    assortmentProductDto.IsRanged = Convert.ToBoolean(cellValue, prov);
                    break;
                case AssortmentImportMappingList.RankMapId:
                    assortmentProductDto.Rank = Convert.ToInt16(cellValue, prov);
                    break;
                case AssortmentImportMappingList.FacingsMapId:
                    assortmentProductDto.Facings = Convert.ToByte(cellValue, prov);
                    break;
                case AssortmentImportMappingList.UnitsMapId:
                    assortmentProductDto.Units = Convert.ToInt16(cellValue, prov);
                    break;
                case AssortmentImportMappingList.SegmentationMapId:
                    assortmentProductDto.Segmentation = Convert.ToString(cellValue, prov);
                    break;
                case AssortmentImportMappingList.ProductTreatmentTypeMapId:
                    //assortmentProductDto.ProductTreatmentType = Convert.ToByte(cellValue, prov);
                    //Use static string,enum dictionary to retrieve the byte value
                    //Using lower invariant to allow all case variants to import correctly
                    PlanogramAssortmentProductTreatmentType treatmentType;
                    if (PlanogramAssortmentProductTreatmentTypeHelper.EnumFromFriendlyName.TryGetValue(cellValue.ToString().ToLowerInvariant(), out treatmentType))
                    {
                        assortmentProductDto.ProductTreatmentType = (Byte)treatmentType;
                    }
                    break;
                case AssortmentImportMappingList.CommentsMapId:
                    assortmentProductDto.Comments = Convert.ToString(cellValue, prov);
                    break;
                #endregion

                default: throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override String GetBindingPath(Int32 mappingId)
        {
            return GetBindingPath(mappingId, null);
        }


        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public String GetBindingPath(int mappingId, String bindingPrefix)
        {
            switch (mappingId)
            {
                #region AssortmentProperties
                case AssortmentImportMappingList.AssortmentNameMapId: return Assortment.NameProperty.Name;
                case AssortmentImportMappingList.ProductGTINMapId: return AssortmentProduct.GtinProperty.Name;
                case AssortmentImportMappingList.IsRangedMapId: return AssortmentProduct.IsRangedProperty.Name;
                case AssortmentImportMappingList.RankMapId: return AssortmentProduct.RankProperty.Name;
                case AssortmentImportMappingList.FacingsMapId: return AssortmentProduct.FacingsProperty.Name;
                case AssortmentImportMappingList.UnitsMapId: return AssortmentProduct.UnitsProperty.Name;
                case AssortmentImportMappingList.SegmentationMapId: return AssortmentProduct.SegmentationProperty.Name;
                case AssortmentImportMappingList.ProductTreatmentTypeMapId: return AssortmentProduct.ProductTreatmentTypeProperty.Name;
                case AssortmentImportMappingList.CommentsMapId: return AssortmentProduct.CommentsProperty.Name;
                case AssortmentImportMappingList.ProductGroupCodeMapId: return Assortment.ProductGroupIdProperty.Name;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        #endregion

        #region IModelObjectColumnHelper Members

        //Boolean IModelObjectColumnHelper.IsUOMDisplayConversionRequired(Int32 mappingId)
        //{
        //    return ProductImportMappingList.IsUOMDisplayConversionRequired(mappingId);
        //}

        /// <summary>
        /// Implementation of the GetColumnGroupName method
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns></returns>
        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mapping)
        {
            return AssortmentImportMappingList.GetColumnGroup(mapping);
        }

        #endregion
    }


    #region Move Into Seperate class file : ProductUniverseImportMappingList

    [Serializable]
    public class ProductUniverseImportMappingList : ImportMappingList<ProductUniverseImportMappingList>
    {
        #region Constants

        public const Int32 ProductUniverseNameMappingId = 0;
        public const Int32 ProductGroupCodeMappingId = 1;
        public const Int32 ProductGtinMappingId = 2;

        #endregion

        #region Constructors
        private ProductUniverseImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static ProductUniverseImportMappingList NewProductUniverseImportMappingList()
        {
            ProductUniverseImportMappingList list = new ProductUniverseImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region LocationSpaceElementProperties

            this.Add(ImportMapping.NewImportMapping(ProductUniverseNameMappingId, Message.ProductUniverseImportList_ProductUniverseName, Message.ProductUniverseImportList_ProductUniverseName_Description, true, true, ProductUniverse.NameProperty.Type, ProductUniverse.NameProperty.DefaultValue, 0, 50, false));
            this.Add(ImportMapping.NewImportMapping(ProductGroupCodeMappingId, Message.ProductUniverseImportList_ProductGroupCode, Message.ProductUniverseImportList_ProductGroupCode_Description, true, true, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 0, 50, false));
            this.Add(ImportMapping.NewImportMapping(ProductGtinMappingId, Message.ProductUniverseImportList_ProductGtin, Message.ProductUniverseImportList_ProductGtin_Description, true, true, ProductUniverseProduct.GtinProperty.Type, ProductUniverseProduct.GtinProperty.DefaultValue, 1, 14, false));

            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, ProductUniverseSearchCriteria searchCriteria)
        {
            switch (mappingId)
            {
                #region Metric Properties
                case ProductUniverseImportMappingList.ProductUniverseNameMappingId: return searchCriteria.ProductUniverseName;
                case ProductUniverseImportMappingList.ProductGroupCodeMappingId: return searchCriteria.ProductGroupCode;
                case ProductUniverseImportMappingList.ProductGtinMappingId: return searchCriteria.ProductGTIN;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override string GetBindingPath(int mappingId)
        {
            switch (mappingId)
            {
                #region Product Universe Properties

                case ProductUniverseImportMappingList.ProductUniverseNameMappingId: return ProductUniverse.NameProperty.Name;
                case ProductUniverseImportMappingList.ProductGroupCodeMappingId: return ProductGroup.CodeProperty.Name;
                case ProductUniverseImportMappingList.ProductGtinMappingId: return ProductUniverseProduct.GtinProperty.Name;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        #endregion
    }



    #endregion
}