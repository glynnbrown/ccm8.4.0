﻿using Galleria.Framework.Imports;
using System;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class PlanogramAssignmentImportMappingList : ImportMappingList<PlanogramAssignmentImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        public const Int32 LocationCodeMappingId = 0;
        public const Int32 ProductGroupCodeMappingId = 1;
        public const Int32 PlanogramNameMappingId = 2;
        
        #endregion

        #region Constructors
        private PlanogramAssignmentImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static PlanogramAssignmentImportMappingList NewPlanogramAssignmentImportMappingList()
        {
            PlanogramAssignmentImportMappingList list = new PlanogramAssignmentImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region LocationSpaceBayProperties

            this.Add(ImportMapping.NewImportMapping(LocationCodeMappingId, Message.PlanogramAssignment_LocationCode, Message.PlanogramAssignment_LocationCode_Description, true, /*identifier*/true, typeof(String), "0", 0, 50, false));
            this.Add(ImportMapping.NewImportMapping(ProductGroupCodeMappingId, Message.PlanogramAssignment_CategoryCode, Message.PlanogramAssignment_CategoryCode_Description, true, /*identifier*/true, typeof(String), "0", 0, 50, false));
            this.Add(ImportMapping.NewImportMapping(PlanogramNameMappingId, Message.PlanogramAssignment_PlanogramName, Message.PlanogramAssignment_PlanogramName_Description, true, /*identifier*/true, typeof(String), "0", 0, 50, false));
            
            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Methods
        
        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override String GetBindingPath(Int32 mappingId)
        {
            return null;
        }


        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            return null;
        }


        #endregion

        #region IModelObjectColumnHelper members

        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mappingId)
        {
            //does this have any groups?
            return null;
        }
        
        #endregion
    }
}
