﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Imports;
using Galleria.Ccm.Model;
using Galleria.Ccm.Resources.Language;
using System.Globalization;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class LocationProductIllegalImportMappingList : ImportMappingList<LocationProductIllegalImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        public const Int16 LocationCodeMapId = 1;
        public const Int32 ProductGTINMapId = 2;

        #endregion

        #region Constructors
        private LocationProductIllegalImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods

        public static LocationProductIllegalImportMappingList NewLocationProductIllegalImportMappingList()
        {
            LocationProductIllegalImportMappingList list = new LocationProductIllegalImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region LocationProductIllegalProperties

            this.Add(ImportMapping.NewImportMapping(LocationCodeMapId, String.Format(Message.LocationProductIllegal_LocationCode, Location.CodeProperty.FriendlyName), Location.CodeProperty.Description, true, true, Location.CodeProperty.Type, Location.CodeProperty.DefaultValue, 0, 50, false));
            this.Add(ImportMapping.NewImportMapping(ProductGTINMapId, String.Format(Message.LocationProductIllegal_ProductGTIN, Product.GtinProperty.FriendlyName), Product.GtinProperty.Description, true, true, Product.GtinProperty.Type, Product.GtinProperty.DefaultValue, 0, 14, false));

            #endregion

            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, LocationProductIllegalDto locationProductIllegalDto)
        {
            switch (mappingId)
            {
                #region LocationProductIllegalProperties
                case LocationProductIllegalImportMappingList.LocationCodeMapId: return locationProductIllegalDto.LocationId;
                case LocationProductIllegalImportMappingList.ProductGTINMapId: return locationProductIllegalDto.ProductId;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue">Location or Product Id derived from initial cell value</param>
        /// <param name="locationProductIllegalDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            LocationProductIllegalDto locationProductIllegalDto)
        {
            IFormatProvider prov = CultureInfo.CurrentCulture;

            switch (mappingId)
            {
                #region LocationProductIllegalProperties
                case LocationProductIllegalImportMappingList.LocationCodeMapId:
                    locationProductIllegalDto.LocationId = Convert.ToInt16(cellValue, prov);
                    break;
                case LocationProductIllegalImportMappingList.ProductGTINMapId:
                    locationProductIllegalDto.ProductId = Convert.ToInt32(cellValue, prov);
                    break;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override string GetBindingPath(int mappingId)
        {
            switch (mappingId)
            {
                #region LocationProductIllegalProperties
                case LocationProductIllegalImportMappingList.LocationCodeMapId: return Location.CodeProperty.Name;
                case LocationProductIllegalImportMappingList.ProductGTINMapId: return Product.GtinProperty.Name;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;

            switch (mappingId)
            {
                #region
                case LocationProductIllegalImportMappingList.LocationCodeMapId: return String.Format("{0}{1}", prefix, Location.CodeProperty.Name);
                case LocationProductIllegalImportMappingList.ProductGTINMapId: return String.Format("{0}{1}", prefix, Product.GtinProperty.Name);
                #endregion

                default: throw new NotImplementedException();
            }
        }

        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mapping)
        {
            //does this have any groups?
            return null;
        }

        #endregion


    }
}
