﻿#region Header Information
// Copyright © Galleria RTS Ltd 2011

#region Version History: (GFS 1.1)
//V8-25455 : J.Pickup
//  Copied over from GFS
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;


namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class AssortmentLocationImportMappingList : ImportMappingList<AssortmentLocationImportMappingList>
    {
        #region Constants

        public const Byte AssortmentNameMapId = 0;
        public const Byte LocationCodeMapId = 1;

        #endregion

        #region Constructors
        private AssortmentLocationImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods

        public static AssortmentLocationImportMappingList NewAssortmentLocationImportMappingList()
        {
            AssortmentLocationImportMappingList list = new AssortmentLocationImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region AssortmentProperties

            this.Add(ImportMapping.NewImportMapping(AssortmentNameMapId, String.Format(Message.Assortment_Name, Assortment.NameProperty.FriendlyName), Assortment.NameProperty.Description, true, true, Assortment.NameProperty.Type, Assortment.NameProperty.DefaultValue, 0, 255, false));
            this.Add(ImportMapping.NewImportMapping(LocationCodeMapId, Message.Assortment_LocationCode, Location.CodeProperty.Description, true, Location.CodeProperty.Type, Location.CodeProperty.DefaultValue, 0, 50, false));

            #endregion

            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="assortmentDto"></param>
        /// <param name="assortmentProductDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, AssortmentInfoDto assortmentInfoDto, AssortmentLocationDto assortmentLocationDto)
        {
            switch (mappingId)
            {
                case AssortmentLocationImportMappingList.AssortmentNameMapId:
                    return assortmentInfoDto.Name;

                case AssortmentLocationImportMappingList.LocationCodeMapId:
                    return assortmentLocationDto.Code;

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue">derived from initial cell value</param>
        /// <param name="assortmentDto"></param>
        /// <param name="assortmentProductDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue, AssortmentLocationDto assortmentLocationDto)
        {
            IFormatProvider prov = CultureInfo.CurrentCulture;

            switch (mappingId)
            {
                case AssortmentLocationImportMappingList.AssortmentNameMapId:
                    assortmentLocationDto.AssortmentId = Convert.ToInt32(cellValue, prov);
                    break;

                case AssortmentLocationImportMappingList.LocationCodeMapId:
                    assortmentLocationDto.LocationId = Convert.ToInt16(cellValue, prov);
                    break;

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override string GetBindingPath(int mappingId)
        {
            switch (mappingId)
            {
                case AssortmentLocationImportMappingList.AssortmentNameMapId:
                    return Assortment.NameProperty.Name;
                case AssortmentLocationImportMappingList.LocationCodeMapId:
                    return AssortmentLocation.CodeProperty.Name;

                default: throw new NotImplementedException();
            }
        }

        #endregion
    }
}