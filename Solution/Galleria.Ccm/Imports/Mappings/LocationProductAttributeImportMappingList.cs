﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-26041 : A.Kuszyk
//  Changed the use of CCM ProductStatusType to Framework PlanogramProductStatusType.
// V8-24264 : A.Probyn
//	Extended for display type on import mapping constructor.
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Globalization;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class LocationProductAttributeImportMappingList : ImportMappingList<LocationProductAttributeImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        #region LocationProductAttributeProperties
        public const Int16 LocationCodeMapId = 1;
        public const Int16 ProductGTINMapId = 2;
        public const Int16 GTINMapId = 3;
        public const Int16 DescriptionMapId = 4;
        public const Int16 HeightMapId = 5;
        public const Int16 WidthMapId = 6;
        public const Int16 DepthMapId = 7;
        public const Int16 CasePackUnitsMapId = 8;
        public const Int16 CaseHighMapId = 9;
        public const Int16 CaseWideMapId = 10;
        public const Int16 CaseDeepMapId = 11;
        public const Int16 CaseHeightMapId = 12;
        public const Int16 CaseDepthMapId = 13;
        public const Int16 CaseWidthMapId = 14;
        public const Int16 DaysOfSupplyMapId = 15;
        public const Int16 MinDeepMapId = 16;
        public const Int16 MaxDeepMapId = 17;
        public const Int16 TrayPackUnitsMapId = 18;
        public const Int16 TrayHighMapId = 19;
        public const Int16 TrayWideMapId = 20;
        public const Int16 TrayDeepMapId = 21;
        public const Int16 TrayHeightMapId = 22;
        public const Int16 TrayDepthMapId = 23;
        public const Int16 TrayWidthMapId = 24;
        public const Int16 TrayThickHeightMapId = 25;
        public const Int16 TrayThickDepthMapId = 26;
        public const Int16 TrayThickWidthMapId = 27;
        public const Int16 StatusTypeMapId = 28;
        public const Int16 ShelfLifeMapId = 29;
        public const Int16 DeliveryFrequencyDaysMapId = 30;
        public const Int16 DeliveryMethodMapId = 31;
        public const Int16 VendorCodeMapId = 32;
        public const Int16 VendorMapId = 33;
        public const Int16 ManufacturerCodeMapId = 34;
        public const Int16 ManufacturerMapId = 35;
        public const Int16 SizeMapId = 36;
        public const Int16 UnitOfMeasureMapId = 37;
        public const Int16 SellPriceMapId = 38;
        public const Int16 SellPackCountMapId = 39;
        public const Int16 SellPackDescriptionMapId = 40;
        public const Int16 RecommendedRetailPriceMapId = 41;
        public const Int16 CostPriceMapId = 42;
        public const Int16 CaseCostMapId = 43;
        public const Int16 ConsumerInformationMapId = 44;
        public const Int16 PatternMapId = 45;
        public const Int16 ModelMapId = 46;
        public const Int16 CorporateCodeMapId = 47;
        #endregion

        //#region Custom Attributes
        //public const Int32 CustomAttribute01MapId = 50;
        //public const Int32 CustomAttribute02MapId = 51;
        //public const Int32 CustomAttribute03MapId = 52;
        //public const Int32 CustomAttribute04MapId = 53;
        //public const Int32 CustomAttribute05MapId = 54;
        //public const Int32 CustomAttribute06MapId = 55;
        //public const Int32 CustomAttribute07MapId = 56;
        //public const Int32 CustomAttribute08MapId = 57;
        //public const Int32 CustomAttribute09MapId = 58;
        //public const Int32 CustomAttribute10MapId = 59;
        //public const Int32 CustomAttribute11MapId = 60;
        //public const Int32 CustomAttribute12MapId = 61;
        //public const Int32 CustomAttribute13MapId = 62;
        //public const Int32 CustomAttribute14MapId = 63;
        //public const Int32 CustomAttribute15MapId = 64;
        //public const Int32 CustomAttribute16MapId = 65;
        //public const Int32 CustomAttribute17MapId = 66;
        //public const Int32 CustomAttribute18MapId = 67;
        //public const Int32 CustomAttribute19MapId = 68;
        //public const Int32 CustomAttribute20MapId = 69;
        //public const Int32 CustomAttribute21MapId = 70;
        //public const Int32 CustomAttribute22MapId = 71;
        //public const Int32 CustomAttribute23MapId = 72;
        //public const Int32 CustomAttribute24MapId = 73;
        //public const Int32 CustomAttribute25MapId = 74;
        //public const Int32 CustomAttribute26MapId = 75;
        //public const Int32 CustomAttribute27MapId = 76;
        //public const Int32 CustomAttribute28MapId = 77;
        //public const Int32 CustomAttribute29MapId = 78;
        //public const Int32 CustomAttribute30MapId = 79;
        //public const Int32 CustomAttribute31MapId = 80;
        //public const Int32 CustomAttribute32MapId = 81;
        //public const Int32 CustomAttribute33MapId = 82;
        //public const Int32 CustomAttribute34MapId = 83;
        //public const Int32 CustomAttribute35MapId = 84;
        //public const Int32 CustomAttribute36MapId = 85;
        //public const Int32 CustomAttribute37MapId = 86;
        //public const Int32 CustomAttribute38MapId = 87;
        //public const Int32 CustomAttribute39MapId = 88;
        //public const Int32 CustomAttribute40MapId = 89;
        //public const Int32 CustomAttribute41MapId = 90;
        //public const Int32 CustomAttribute42MapId = 91;
        //public const Int32 CustomAttribute43MapId = 92;
        //public const Int32 CustomAttribute44MapId = 93;
        //public const Int32 CustomAttribute45MapId = 94;
        //public const Int32 CustomAttribute46MapId = 95;
        //public const Int32 CustomAttribute47MapId = 96;
        //public const Int32 CustomAttribute48MapId = 97;
        //public const Int32 CustomAttribute49MapId = 98;
        //public const Int32 CustomAttribute50MapId = 99;
        //#endregion
        #endregion

        #region Constructors
        private LocationProductAttributeImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods

        public static LocationProductAttributeImportMappingList NewLocationProductAttributeImportMappingList()
        {
            LocationProductAttributeImportMappingList list = new LocationProductAttributeImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create

        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region LocationProductAttribute Properties

            this.Add(ImportMapping.NewImportMapping(LocationCodeMapId, Message.LocationProductAttribute_LocationCode, Location.CodeProperty.Description, true, true, Location.CodeProperty.Type, Location.CodeProperty.DefaultValue, 0, 50, false, Location.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ProductGTINMapId, Message.LocationProductAttribute_ProductGTIN, LocationProductAttribute.GTINProperty.Description, true, true, LocationProductAttribute.GTINProperty.Type, LocationProductAttribute.GTINProperty.DefaultValue, 0, 14, false, LocationProductAttribute.GTINProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(GTINMapId, LocationProductAttribute.GTINProperty.FriendlyName, LocationProductAttribute.GTINProperty.Description, false, LocationProductAttribute.GTINProperty.Type, LocationProductAttribute.GTINProperty.DefaultValue, 0, 14, true, LocationProductAttribute.GTINProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DescriptionMapId, LocationProductAttribute.DescriptionProperty.FriendlyName, LocationProductAttribute.DescriptionProperty.Description, false, LocationProductAttribute.DescriptionProperty.Type, LocationProductAttribute.DescriptionProperty.DefaultValue, 0, 50, true, LocationProductAttribute.DescriptionProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HeightMapId, LocationProductAttribute.HeightProperty.FriendlyName, LocationProductAttribute.HeightProperty.Description, false, LocationProductAttribute.HeightProperty.Type, LocationProductAttribute.HeightProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.HeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(WidthMapId, LocationProductAttribute.WidthProperty.FriendlyName, LocationProductAttribute.WidthProperty.Description, false, LocationProductAttribute.WidthProperty.Type, LocationProductAttribute.WidthProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.WidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DepthMapId, LocationProductAttribute.DepthProperty.FriendlyName, LocationProductAttribute.DepthProperty.Description, false, LocationProductAttribute.DepthProperty.Type, LocationProductAttribute.DepthProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.DepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CasePackUnitsMapId, LocationProductAttribute.CasePackUnitsProperty.FriendlyName, LocationProductAttribute.CasePackUnitsProperty.Description, false, LocationProductAttribute.CasePackUnitsProperty.Type, LocationProductAttribute.CasePackUnitsProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.CasePackUnitsProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseHighMapId, LocationProductAttribute.CaseHighProperty.FriendlyName, LocationProductAttribute.CaseHighProperty.Description, false, LocationProductAttribute.CaseHighProperty.Type, LocationProductAttribute.CaseHighProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.CaseHighProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseWideMapId, LocationProductAttribute.CaseWideProperty.FriendlyName, LocationProductAttribute.CaseWideProperty.Description, false, LocationProductAttribute.CaseWideProperty.Type, LocationProductAttribute.CaseWideProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.CaseWideProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseDeepMapId, LocationProductAttribute.CaseDeepProperty.FriendlyName, LocationProductAttribute.CaseDeepProperty.Description, false, LocationProductAttribute.CaseDeepProperty.Type, LocationProductAttribute.CaseDeepProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.CaseDeepProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseHeightMapId, LocationProductAttribute.CaseHeightProperty.FriendlyName, LocationProductAttribute.CaseHeightProperty.Description, false, LocationProductAttribute.CaseHeightProperty.Type, LocationProductAttribute.CaseHeightProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.CaseHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseDepthMapId, LocationProductAttribute.CaseDepthProperty.FriendlyName, LocationProductAttribute.CaseDepthProperty.Description, false, LocationProductAttribute.CaseDepthProperty.Type, LocationProductAttribute.CaseDepthProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.CaseDepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseWidthMapId, LocationProductAttribute.CaseWidthProperty.FriendlyName, LocationProductAttribute.CaseWidthProperty.Description, false, LocationProductAttribute.CaseWidthProperty.Type, LocationProductAttribute.CaseWidthProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.CaseWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DaysOfSupplyMapId, LocationProductAttribute.DaysOfSupplyProperty.FriendlyName, LocationProductAttribute.DaysOfSupplyProperty.Description, false, LocationProductAttribute.DaysOfSupplyProperty.Type, LocationProductAttribute.DaysOfSupplyProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.DaysOfSupplyProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(MinDeepMapId, LocationProductAttribute.MinDeepProperty.FriendlyName, LocationProductAttribute.MinDeepProperty.Description, false, LocationProductAttribute.MinDeepProperty.Type, LocationProductAttribute.MinDeepProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.MinDeepProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(MaxDeepMapId, LocationProductAttribute.MaxDeepProperty.FriendlyName, LocationProductAttribute.MaxDeepProperty.Description, false, LocationProductAttribute.MaxDeepProperty.Type, LocationProductAttribute.MaxDeepProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.MaxDeepProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayPackUnitsMapId, LocationProductAttribute.TrayPackUnitsProperty.FriendlyName, LocationProductAttribute.TrayPackUnitsProperty.Description, false, LocationProductAttribute.TrayPackUnitsProperty.Type, LocationProductAttribute.TrayPackUnitsProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.TrayPackUnitsProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayHighMapId, LocationProductAttribute.TrayHighProperty.FriendlyName, LocationProductAttribute.TrayHighProperty.Description, false, LocationProductAttribute.TrayHighProperty.Type, LocationProductAttribute.TrayHighProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.TrayHighProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayWideMapId, LocationProductAttribute.TrayWideProperty.FriendlyName, LocationProductAttribute.TrayWideProperty.Description, false, LocationProductAttribute.TrayWideProperty.Type, LocationProductAttribute.TrayWideProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.TrayWideProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayDeepMapId, LocationProductAttribute.TrayDeepProperty.FriendlyName, LocationProductAttribute.TrayDeepProperty.Description, false, LocationProductAttribute.TrayDeepProperty.Type, LocationProductAttribute.TrayDeepProperty.DefaultValue, 0, Byte.MaxValue, true, LocationProductAttribute.TrayDeepProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayHeightMapId, LocationProductAttribute.TrayHeightProperty.FriendlyName, LocationProductAttribute.TrayHeightProperty.Description, false, LocationProductAttribute.TrayHeightProperty.Type, LocationProductAttribute.TrayHeightProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.TrayHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayDepthMapId, LocationProductAttribute.TrayDepthProperty.FriendlyName, LocationProductAttribute.TrayDepthProperty.Description, false, LocationProductAttribute.TrayDepthProperty.Type, LocationProductAttribute.TrayDepthProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.TrayDepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayWidthMapId, LocationProductAttribute.TrayWidthProperty.FriendlyName, LocationProductAttribute.TrayWidthProperty.Description, false, LocationProductAttribute.TrayWidthProperty.Type, LocationProductAttribute.TrayWidthProperty.DefaultValue, 0, 9999, true, LocationProductAttribute.TrayWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayThickHeightMapId, LocationProductAttribute.TrayThickHeightProperty.FriendlyName, LocationProductAttribute.TrayThickHeightProperty.Description, false, LocationProductAttribute.TrayThickHeightProperty.Type, LocationProductAttribute.TrayThickHeightProperty.DefaultValue, 0, 10, true, LocationProductAttribute.TrayThickHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayThickDepthMapId, LocationProductAttribute.TrayThickDepthProperty.FriendlyName, LocationProductAttribute.TrayThickDepthProperty.Description, false, LocationProductAttribute.TrayThickDepthProperty.Type, LocationProductAttribute.TrayThickDepthProperty.DefaultValue, 0, 10, true, LocationProductAttribute.TrayThickDepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayThickWidthMapId, LocationProductAttribute.TrayThickWidthProperty.FriendlyName, LocationProductAttribute.TrayThickWidthProperty.Description, false, LocationProductAttribute.TrayThickWidthProperty.Type, LocationProductAttribute.TrayThickWidthProperty.DefaultValue, 0, 10, true, LocationProductAttribute.TrayThickWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(StatusTypeMapId, LocationProductAttribute.StatusTypeProperty.FriendlyName, LocationProductAttribute.StatusTypeProperty.Description, false, false, LocationProductAttribute.StatusTypeProperty.Type, LocationProductAttribute.StatusTypeProperty.DefaultValue, PlanogramProductStatusTypeHelper.FriendlyNames, true, LocationProductAttribute.StatusTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ShelfLifeMapId, LocationProductAttribute.ShelfLifeProperty.FriendlyName, LocationProductAttribute.ShelfLifeProperty.Description, false, LocationProductAttribute.ShelfLifeProperty.Type, LocationProductAttribute.ShelfLifeProperty.DefaultValue, 0, 999, true, LocationProductAttribute.ShelfLifeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DeliveryFrequencyDaysMapId, LocationProductAttribute.DeliveryFrequencyDaysProperty.FriendlyName, LocationProductAttribute.DeliveryFrequencyDaysProperty.Description, false, LocationProductAttribute.DeliveryFrequencyDaysProperty.Type, LocationProductAttribute.DeliveryFrequencyDaysProperty.DefaultValue, 0, 100, true, LocationProductAttribute.DeliveryFrequencyDaysProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DeliveryMethodMapId, LocationProductAttribute.DeliveryMethodProperty.FriendlyName, LocationProductAttribute.DeliveryMethodProperty.Description, false, LocationProductAttribute.DeliveryMethodProperty.Type, LocationProductAttribute.DeliveryMethodProperty.DefaultValue, 0, 20, true, LocationProductAttribute.DeliveryMethodProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(VendorCodeMapId, LocationProductAttribute.VendorCodeProperty.FriendlyName, LocationProductAttribute.VendorCodeProperty.Description, false, LocationProductAttribute.VendorCodeProperty.Type, LocationProductAttribute.VendorCodeProperty.DefaultValue, 0, 20, true, LocationProductAttribute.VendorCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(VendorMapId, LocationProductAttribute.VendorProperty.FriendlyName, LocationProductAttribute.VendorProperty.Description, false, LocationProductAttribute.VendorProperty.Type, LocationProductAttribute.VendorProperty.DefaultValue, 0, 50, true, LocationProductAttribute.VendorProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ManufacturerCodeMapId, LocationProductAttribute.ManufacturerCodeProperty.FriendlyName, LocationProductAttribute.ManufacturerCodeProperty.Description, false, LocationProductAttribute.ManufacturerCodeProperty.Type, LocationProductAttribute.ManufacturerCodeProperty.DefaultValue, 0, 20, true, LocationProductAttribute.ManufacturerCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ManufacturerMapId, LocationProductAttribute.ManufacturerProperty.FriendlyName, LocationProductAttribute.ManufacturerProperty.Description, false, LocationProductAttribute.ManufacturerProperty.Type, LocationProductAttribute.ManufacturerProperty.DefaultValue, 0, 50, true, LocationProductAttribute.ManufacturerProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SizeMapId, LocationProductAttribute.SizeProperty.FriendlyName, LocationProductAttribute.SizeProperty.Description, false, LocationProductAttribute.SizeProperty.Type, LocationProductAttribute.SizeProperty.DefaultValue, 0, 20, true, LocationProductAttribute.SizeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(UnitOfMeasureMapId, LocationProductAttribute.UnitOfMeasureProperty.FriendlyName, LocationProductAttribute.UnitOfMeasureProperty.Description, false, LocationProductAttribute.UnitOfMeasureProperty.Type, LocationProductAttribute.UnitOfMeasureProperty.DefaultValue, 0, 20, true, LocationProductAttribute.UnitOfMeasureProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SellPriceMapId, LocationProductAttribute.SellPriceProperty.FriendlyName, LocationProductAttribute.SellPriceProperty.Description, false, LocationProductAttribute.SellPriceProperty.Type, LocationProductAttribute.SellPriceProperty.DefaultValue, 0, 999999, true, LocationProductAttribute.SellPriceProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SellPackCountMapId, LocationProductAttribute.SellPackCountProperty.FriendlyName, LocationProductAttribute.SellPackCountProperty.Description, false, LocationProductAttribute.SellPackCountProperty.Type, LocationProductAttribute.SellPackCountProperty.DefaultValue, 0, 99, true, LocationProductAttribute.SellPackCountProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SellPackDescriptionMapId, LocationProductAttribute.SellPackDescriptionProperty.FriendlyName, LocationProductAttribute.SellPackDescriptionProperty.Description, false, LocationProductAttribute.SellPackDescriptionProperty.Type, LocationProductAttribute.SellPackDescriptionProperty.DefaultValue, 0, 100, true, LocationProductAttribute.SellPackDescriptionProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(RecommendedRetailPriceMapId, LocationProductAttribute.RecommendedRetailPriceProperty.FriendlyName, LocationProductAttribute.RecommendedRetailPriceProperty.Description, false, LocationProductAttribute.RecommendedRetailPriceProperty.Type, LocationProductAttribute.RecommendedRetailPriceProperty.DefaultValue, 0, 999999, true, LocationProductAttribute.RecommendedRetailPriceProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CostPriceMapId, LocationProductAttribute.CostPriceProperty.FriendlyName, LocationProductAttribute.CostPriceProperty.Description, false, LocationProductAttribute.CostPriceProperty.Type, LocationProductAttribute.CostPriceProperty.DefaultValue, 0, 999999, true, LocationProductAttribute.CostPriceProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseCostMapId, LocationProductAttribute.CaseCostProperty.FriendlyName, LocationProductAttribute.CaseCostProperty.Description, false, LocationProductAttribute.CaseCostProperty.Type, LocationProductAttribute.CaseCostProperty.DefaultValue, 0, 999999, true, LocationProductAttribute.CaseCostProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ConsumerInformationMapId, LocationProductAttribute.ConsumerInformationProperty.FriendlyName, LocationProductAttribute.ConsumerInformationProperty.Description, false, LocationProductAttribute.ConsumerInformationProperty.Type, LocationProductAttribute.ConsumerInformationProperty.DefaultValue, 0, 50, true, LocationProductAttribute.ConsumerInformationProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PatternMapId, LocationProductAttribute.PatternProperty.FriendlyName, LocationProductAttribute.PatternProperty.Description, false, LocationProductAttribute.PatternProperty.Type, LocationProductAttribute.PatternProperty.DefaultValue, 0, 20, true, LocationProductAttribute.PatternProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ModelMapId, LocationProductAttribute.ModelProperty.FriendlyName, LocationProductAttribute.ModelProperty.Description, false, LocationProductAttribute.ModelProperty.Type, LocationProductAttribute.ModelProperty.DefaultValue, 0, 20, true, LocationProductAttribute.ModelProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CorporateCodeMapId, LocationProductAttribute.CorporateCodeProperty.FriendlyName, LocationProductAttribute.CorporateCodeProperty.Description, false, LocationProductAttribute.CorporateCodeProperty.Type, LocationProductAttribute.CorporateCodeProperty.DefaultValue, 0, 20, true, LocationProductAttribute.CorporateCodeProperty.DisplayType));


            #endregion

            //#region Custom Attributes
            //foreach (LocationProductCustomAttribute customAttribute in attributeList)
            //{
            //    //get the attrbute type
            //    Type propertyType = null;
            //    Single minValue = 0;
            //    Single maxValue = 0;
            //    switch (customAttribute.DataType)
            //    {
            //        case LocationProductCustomAttributeDataType.Boolean:
            //            propertyType = typeof(Boolean);
            //            minValue = 0;
            //            maxValue = 1;
            //            break;

            //        case LocationProductCustomAttributeDataType.Currency:
            //            propertyType = typeof(Single);
            //            minValue = -999999999F;
            //            maxValue = 999999999F;
            //            break;

            //        case LocationProductCustomAttributeDataType.Decimal:
            //            propertyType = typeof(Single);
            //            minValue = -999999999F;
            //            maxValue = 999999999F;
            //            break;

            //        case LocationProductCustomAttributeDataType.Integer:
            //            propertyType = typeof(Int32);
            //            minValue = -99999;
            //            maxValue = 99999;
            //            break;

            //        case LocationProductCustomAttributeDataType.Text:
            //        default:
            //            propertyType = typeof(String);
            //            minValue = 0;
            //            maxValue = 100;
            //            break;
            //    }

            //    //add the attribute to the mapping
            //    //switch (customAttribute.ColumnNumber)
            //    //{
            //    //    case 1:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute01MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 2:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute02MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 3:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute03MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 4:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute04MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 5:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute05MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 6:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute06MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 7:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute07MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 8:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute08MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 9:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute09MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 10:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute10MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 11:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute11MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 12:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute12MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 13:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute13MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 14:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute14MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 15:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute15MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 16:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute16MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 17:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute17MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 18:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute18MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 19:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute19MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 20:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute20MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 21:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute21MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 22:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute22MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 23:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute23MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 24:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute24MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 25:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute25MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 26:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute26MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 27:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute27MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 28:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute28MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 29:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute29MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 30:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute30MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 31:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute31MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 32:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute32MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 33:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute33MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 34:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute34MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 35:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute35MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 36:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute36MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 37:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute37MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 38:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute38MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 39:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute39MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 40:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute40MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 41:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute41MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 42:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute42MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 43:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute43MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 44:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute44MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 45:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute45MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 46:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute46MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 47:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute47MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 48:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute48MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 49:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute49MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //    case 50:
            //    //        this.Add(ImportMapping.NewImportMapping(CustomAttribute50MapId, customAttribute.Name, String.Empty, false, propertyType, null, minValue, maxValue, true));
            //    //        break;
            //    //}

            //}
            //#endregion

            this.RaiseListChangedEvents = true;
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, LocationProductAttributeDto locationProductAttributeDto)
        {
            switch (mappingId)
            {
                #region LocationProductAttributeProperties

                case LocationProductAttributeImportMappingList.LocationCodeMapId: return locationProductAttributeDto.LocationId;
                case LocationProductAttributeImportMappingList.ProductGTINMapId: return locationProductAttributeDto.ProductId;
                case LocationProductAttributeImportMappingList.GTINMapId: return locationProductAttributeDto.GTIN;
                case LocationProductAttributeImportMappingList.DescriptionMapId: return locationProductAttributeDto.Description;
                case LocationProductAttributeImportMappingList.HeightMapId: return locationProductAttributeDto.Height;
                case LocationProductAttributeImportMappingList.WidthMapId: return locationProductAttributeDto.Width;
                case LocationProductAttributeImportMappingList.DepthMapId: return locationProductAttributeDto.Depth;
                case LocationProductAttributeImportMappingList.CasePackUnitsMapId: return locationProductAttributeDto.CasePackUnits;
                case LocationProductAttributeImportMappingList.CaseHighMapId: return locationProductAttributeDto.CaseHigh;
                case LocationProductAttributeImportMappingList.CaseWideMapId: return locationProductAttributeDto.CaseWide;
                case LocationProductAttributeImportMappingList.CaseDeepMapId: return locationProductAttributeDto.CaseDeep;
                case LocationProductAttributeImportMappingList.CaseHeightMapId: return locationProductAttributeDto.CaseHeight;
                case LocationProductAttributeImportMappingList.CaseDepthMapId: return locationProductAttributeDto.CaseDepth;
                case LocationProductAttributeImportMappingList.CaseWidthMapId: return locationProductAttributeDto.CaseWidth;
                case LocationProductAttributeImportMappingList.DaysOfSupplyMapId: return locationProductAttributeDto.DaysOfSupply;
                case LocationProductAttributeImportMappingList.MinDeepMapId: return locationProductAttributeDto.MinDeep;
                case LocationProductAttributeImportMappingList.MaxDeepMapId: return locationProductAttributeDto.MaxDeep;
                case LocationProductAttributeImportMappingList.TrayPackUnitsMapId: return locationProductAttributeDto.TrayPackUnits;
                case LocationProductAttributeImportMappingList.TrayHighMapId: return locationProductAttributeDto.TrayHigh;
                case LocationProductAttributeImportMappingList.TrayWideMapId: return locationProductAttributeDto.TrayWide;
                case LocationProductAttributeImportMappingList.TrayDeepMapId: return locationProductAttributeDto.TrayDeep;
                case LocationProductAttributeImportMappingList.TrayHeightMapId: return locationProductAttributeDto.TrayHeight;
                case LocationProductAttributeImportMappingList.TrayDepthMapId: return locationProductAttributeDto.TrayDepth;
                case LocationProductAttributeImportMappingList.TrayWidthMapId: return locationProductAttributeDto.TrayWidth;
                case LocationProductAttributeImportMappingList.TrayThickHeightMapId: return locationProductAttributeDto.TrayThickHeight;
                case LocationProductAttributeImportMappingList.TrayThickDepthMapId: return locationProductAttributeDto.TrayThickDepth;
                case LocationProductAttributeImportMappingList.TrayThickWidthMapId: return locationProductAttributeDto.TrayThickWidth;
                case LocationProductAttributeImportMappingList.StatusTypeMapId:
                    if (locationProductAttributeDto.StatusType != null)
                    {
                        return PlanogramProductStatusTypeHelper.FriendlyNames[(PlanogramProductStatusType)(Byte)locationProductAttributeDto.StatusType];
                    }
                    return locationProductAttributeDto.StatusType;
                case LocationProductAttributeImportMappingList.ShelfLifeMapId: return locationProductAttributeDto.ShelfLife;
                case LocationProductAttributeImportMappingList.DeliveryFrequencyDaysMapId: return locationProductAttributeDto.DeliveryFrequencyDays;
                case LocationProductAttributeImportMappingList.DeliveryMethodMapId: return locationProductAttributeDto.DeliveryMethod;
                case LocationProductAttributeImportMappingList.VendorCodeMapId: return locationProductAttributeDto.VendorCode;
                case LocationProductAttributeImportMappingList.VendorMapId: return locationProductAttributeDto.Vendor;
                case LocationProductAttributeImportMappingList.ManufacturerCodeMapId: return locationProductAttributeDto.ManufacturerCode;
                case LocationProductAttributeImportMappingList.ManufacturerMapId: return locationProductAttributeDto.Manufacturer;
                case LocationProductAttributeImportMappingList.SizeMapId: return locationProductAttributeDto.Size;
                case LocationProductAttributeImportMappingList.UnitOfMeasureMapId: return locationProductAttributeDto.UnitOfMeasure;
                case LocationProductAttributeImportMappingList.SellPriceMapId: return locationProductAttributeDto.SellPrice;
                case LocationProductAttributeImportMappingList.SellPackCountMapId: return locationProductAttributeDto.SellPackCount;
                case LocationProductAttributeImportMappingList.SellPackDescriptionMapId: return locationProductAttributeDto.SellPackDescription;
                case LocationProductAttributeImportMappingList.RecommendedRetailPriceMapId: return locationProductAttributeDto.RecommendedRetailPrice;
                case LocationProductAttributeImportMappingList.CostPriceMapId: return locationProductAttributeDto.CostPrice;
                case LocationProductAttributeImportMappingList.CaseCostMapId: return locationProductAttributeDto.CaseCost;
                case LocationProductAttributeImportMappingList.ConsumerInformationMapId: return locationProductAttributeDto.ConsumerInformation;
                case LocationProductAttributeImportMappingList.PatternMapId: return locationProductAttributeDto.Pattern;
                case LocationProductAttributeImportMappingList.ModelMapId: return locationProductAttributeDto.Model;
                case LocationProductAttributeImportMappingList.CorporateCodeMapId: return locationProductAttributeDto.CorporateCode;


                #endregion

                //#region Custom Attributes

                //case LocationProductAttributeImportMappingList.CustomAttribute01MapId: return locationProductAttributeDto.CustomAttribute01;
                //case LocationProductAttributeImportMappingList.CustomAttribute02MapId: return locationProductAttributeDto.CustomAttribute02;
                //case LocationProductAttributeImportMappingList.CustomAttribute03MapId: return locationProductAttributeDto.CustomAttribute03;
                //case LocationProductAttributeImportMappingList.CustomAttribute04MapId: return locationProductAttributeDto.CustomAttribute04;
                //case LocationProductAttributeImportMappingList.CustomAttribute05MapId: return locationProductAttributeDto.CustomAttribute05;
                //case LocationProductAttributeImportMappingList.CustomAttribute06MapId: return locationProductAttributeDto.CustomAttribute06;
                //case LocationProductAttributeImportMappingList.CustomAttribute07MapId: return locationProductAttributeDto.CustomAttribute07;
                //case LocationProductAttributeImportMappingList.CustomAttribute08MapId: return locationProductAttributeDto.CustomAttribute08;
                //case LocationProductAttributeImportMappingList.CustomAttribute09MapId: return locationProductAttributeDto.CustomAttribute09;
                //case LocationProductAttributeImportMappingList.CustomAttribute10MapId: return locationProductAttributeDto.CustomAttribute10;
                //case LocationProductAttributeImportMappingList.CustomAttribute11MapId: return locationProductAttributeDto.CustomAttribute11;
                //case LocationProductAttributeImportMappingList.CustomAttribute12MapId: return locationProductAttributeDto.CustomAttribute12;
                //case LocationProductAttributeImportMappingList.CustomAttribute13MapId: return locationProductAttributeDto.CustomAttribute13;
                //case LocationProductAttributeImportMappingList.CustomAttribute14MapId: return locationProductAttributeDto.CustomAttribute14;
                //case LocationProductAttributeImportMappingList.CustomAttribute15MapId: return locationProductAttributeDto.CustomAttribute15;
                //case LocationProductAttributeImportMappingList.CustomAttribute16MapId: return locationProductAttributeDto.CustomAttribute16;
                //case LocationProductAttributeImportMappingList.CustomAttribute17MapId: return locationProductAttributeDto.CustomAttribute17;
                //case LocationProductAttributeImportMappingList.CustomAttribute18MapId: return locationProductAttributeDto.CustomAttribute18;
                //case LocationProductAttributeImportMappingList.CustomAttribute19MapId: return locationProductAttributeDto.CustomAttribute19;
                //case LocationProductAttributeImportMappingList.CustomAttribute20MapId: return locationProductAttributeDto.CustomAttribute20;
                //case LocationProductAttributeImportMappingList.CustomAttribute21MapId: return locationProductAttributeDto.CustomAttribute21;
                //case LocationProductAttributeImportMappingList.CustomAttribute22MapId: return locationProductAttributeDto.CustomAttribute22;
                //case LocationProductAttributeImportMappingList.CustomAttribute23MapId: return locationProductAttributeDto.CustomAttribute23;
                //case LocationProductAttributeImportMappingList.CustomAttribute24MapId: return locationProductAttributeDto.CustomAttribute24;
                //case LocationProductAttributeImportMappingList.CustomAttribute25MapId: return locationProductAttributeDto.CustomAttribute25;
                //case LocationProductAttributeImportMappingList.CustomAttribute26MapId: return locationProductAttributeDto.CustomAttribute26;
                //case LocationProductAttributeImportMappingList.CustomAttribute27MapId: return locationProductAttributeDto.CustomAttribute27;
                //case LocationProductAttributeImportMappingList.CustomAttribute28MapId: return locationProductAttributeDto.CustomAttribute28;
                //case LocationProductAttributeImportMappingList.CustomAttribute29MapId: return locationProductAttributeDto.CustomAttribute29;
                //case LocationProductAttributeImportMappingList.CustomAttribute30MapId: return locationProductAttributeDto.CustomAttribute30;
                //case LocationProductAttributeImportMappingList.CustomAttribute31MapId: return locationProductAttributeDto.CustomAttribute31;
                //case LocationProductAttributeImportMappingList.CustomAttribute32MapId: return locationProductAttributeDto.CustomAttribute32;
                //case LocationProductAttributeImportMappingList.CustomAttribute33MapId: return locationProductAttributeDto.CustomAttribute33;
                //case LocationProductAttributeImportMappingList.CustomAttribute34MapId: return locationProductAttributeDto.CustomAttribute34;
                //case LocationProductAttributeImportMappingList.CustomAttribute35MapId: return locationProductAttributeDto.CustomAttribute35;
                //case LocationProductAttributeImportMappingList.CustomAttribute36MapId: return locationProductAttributeDto.CustomAttribute36;
                //case LocationProductAttributeImportMappingList.CustomAttribute37MapId: return locationProductAttributeDto.CustomAttribute37;
                //case LocationProductAttributeImportMappingList.CustomAttribute38MapId: return locationProductAttributeDto.CustomAttribute38;
                //case LocationProductAttributeImportMappingList.CustomAttribute39MapId: return locationProductAttributeDto.CustomAttribute39;
                //case LocationProductAttributeImportMappingList.CustomAttribute40MapId: return locationProductAttributeDto.CustomAttribute40;
                //case LocationProductAttributeImportMappingList.CustomAttribute41MapId: return locationProductAttributeDto.CustomAttribute41;
                //case LocationProductAttributeImportMappingList.CustomAttribute42MapId: return locationProductAttributeDto.CustomAttribute42;
                //case LocationProductAttributeImportMappingList.CustomAttribute43MapId: return locationProductAttributeDto.CustomAttribute43;
                //case LocationProductAttributeImportMappingList.CustomAttribute44MapId: return locationProductAttributeDto.CustomAttribute44;
                //case LocationProductAttributeImportMappingList.CustomAttribute45MapId: return locationProductAttributeDto.CustomAttribute45;
                //case LocationProductAttributeImportMappingList.CustomAttribute46MapId: return locationProductAttributeDto.CustomAttribute46;
                //case LocationProductAttributeImportMappingList.CustomAttribute47MapId: return locationProductAttributeDto.CustomAttribute47;
                //case LocationProductAttributeImportMappingList.CustomAttribute48MapId: return locationProductAttributeDto.CustomAttribute48;
                //case LocationProductAttributeImportMappingList.CustomAttribute49MapId: return locationProductAttributeDto.CustomAttribute49;
                //case LocationProductAttributeImportMappingList.CustomAttribute50MapId: return locationProductAttributeDto.CustomAttribute50;


                //#endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue">Location or Product Id derived from initial cell value</param>
        /// <param name="locationProductIllegalDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            LocationProductAttributeDto locationProductAttributeDto)
        {
            IFormatProvider prov = CultureInfo.CurrentCulture;

            switch (mappingId)
            {
                #region LocationProductAttributeProperties

                case LocationProductAttributeImportMappingList.LocationCodeMapId:
                    locationProductAttributeDto.LocationId = Convert.ToInt16(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.ProductGTINMapId:
                    locationProductAttributeDto.ProductId = Convert.ToInt32(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.GTINMapId:
                    locationProductAttributeDto.GTIN = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.DescriptionMapId:
                    locationProductAttributeDto.Description = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.HeightMapId:
                    locationProductAttributeDto.Height = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.WidthMapId:
                    locationProductAttributeDto.Width = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.DepthMapId:
                    locationProductAttributeDto.Depth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CasePackUnitsMapId:
                    //locationProductAttributeDto.CasePackUnits = Convert.ToInt16(cellValue, prov);
                    locationProductAttributeDto.CasePackUnits = ImportHelper.ConvertToValueOrDefault<Int16?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CaseHighMapId:
                    //locationProductAttributeDto.CaseHigh = Convert.ToByte(cellValue, prov); 
                    locationProductAttributeDto.CaseHigh = ImportHelper.ConvertToValueOrDefault<Byte?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CaseWideMapId:
                    //locationProductAttributeDto.CaseWide = Convert.ToByte(cellValue, prov);
                    locationProductAttributeDto.CaseWide = ImportHelper.ConvertToValueOrDefault<Byte?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CaseDeepMapId:
                    //locationProductAttributeDto.CaseDeep = Convert.ToByte(cellValue, prov);
                    locationProductAttributeDto.CaseDeep = ImportHelper.ConvertToValueOrDefault<Byte?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CaseHeightMapId:
                    locationProductAttributeDto.CaseHeight = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CaseDepthMapId:
                    locationProductAttributeDto.CaseDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CaseWidthMapId:
                    locationProductAttributeDto.CaseWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.DaysOfSupplyMapId:
                    //locationProductAttributeDto.DaysOfSupply = Convert.ToByte(cellValue, prov);
                    locationProductAttributeDto.DaysOfSupply = ImportHelper.ConvertToValueOrDefault<Byte?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.MinDeepMapId:
                    locationProductAttributeDto.MinDeep = Convert.ToByte(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.MaxDeepMapId:
                    locationProductAttributeDto.MaxDeep = Convert.ToByte(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayPackUnitsMapId:
                    //locationProductAttributeDto.TrayPackUnits =Convert.ToInt16(cellValue, prov);
                    locationProductAttributeDto.TrayPackUnits = ImportHelper.ConvertToValueOrDefault<Int16?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayHighMapId:
                    //locationProductAttributeDto.TrayHigh = Convert.ToByte(cellValue, prov);
                    locationProductAttributeDto.TrayHigh = ImportHelper.ConvertToValueOrDefault<Byte?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayWideMapId:
                    //locationProductAttributeDto.TrayWide = Convert.ToByte(cellValue, prov);
                    locationProductAttributeDto.TrayWide = ImportHelper.ConvertToValueOrDefault<Byte?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayDeepMapId:
                    //locationProductAttributeDto.TrayDeep = Convert.ToByte(cellValue, prov);
                    locationProductAttributeDto.TrayDeep = ImportHelper.ConvertToValueOrDefault<Byte?>(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayHeightMapId:
                    locationProductAttributeDto.TrayHeight = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayDepthMapId:
                    locationProductAttributeDto.TrayDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayWidthMapId:
                    locationProductAttributeDto.TrayWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayThickHeightMapId:
                    locationProductAttributeDto.TrayThickHeight = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayThickDepthMapId:
                    locationProductAttributeDto.TrayThickDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.TrayThickWidthMapId:
                    locationProductAttributeDto.TrayThickWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.StatusTypeMapId:
                    //Use static string,enum dictionary to retrieve the byte value
                    //Using lower invariant to allow all case variants to import correctly
                    PlanogramProductStatusType statusType;
                    //As enum is nullable need to check for null as well
                    if (cellValue == null)
                    {
                        locationProductAttributeDto.StatusType = null;
                    }
                    else if (PlanogramProductStatusTypeHelper.EnumFromFriendlyName.TryGetValue(cellValue.ToString().ToLowerInvariant(), out statusType))
                    {
                        locationProductAttributeDto.StatusType = (Byte?)statusType;
                    }
                    break;
                case LocationProductAttributeImportMappingList.ShelfLifeMapId:
                    locationProductAttributeDto.ShelfLife = Convert.ToInt16(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.DeliveryFrequencyDaysMapId:
                    locationProductAttributeDto.DeliveryFrequencyDays = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.DeliveryMethodMapId:
                    locationProductAttributeDto.DeliveryMethod = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.VendorCodeMapId:
                    locationProductAttributeDto.VendorCode = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.VendorMapId:
                    locationProductAttributeDto.Vendor = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.ManufacturerCodeMapId:
                    locationProductAttributeDto.ManufacturerCode = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.ManufacturerMapId:
                    locationProductAttributeDto.Manufacturer = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.SizeMapId:
                    locationProductAttributeDto.Size = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.UnitOfMeasureMapId:
                    locationProductAttributeDto.UnitOfMeasure = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.SellPriceMapId:
                    locationProductAttributeDto.SellPrice = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.SellPackCountMapId:
                    locationProductAttributeDto.SellPackCount = Convert.ToInt16(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.SellPackDescriptionMapId:
                    locationProductAttributeDto.SellPackDescription = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.RecommendedRetailPriceMapId:
                    locationProductAttributeDto.RecommendedRetailPrice = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CostPriceMapId:
                    locationProductAttributeDto.CostPrice = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CaseCostMapId:
                    locationProductAttributeDto.CaseCost = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.ConsumerInformationMapId:
                    locationProductAttributeDto.ConsumerInformation = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.PatternMapId:
                    locationProductAttributeDto.Pattern = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.ModelMapId:
                    locationProductAttributeDto.Model = Convert.ToString(cellValue, prov);
                    break;
                case LocationProductAttributeImportMappingList.CorporateCodeMapId:
                    locationProductAttributeDto.CorporateCode = Convert.ToString(cellValue, prov);
                    break;

                #endregion

                //#region Custom Attributes

                //case LocationProductAttributeImportMappingList.CustomAttribute01MapId:
                //    locationProductAttributeDto.CustomAttribute01 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute02MapId:
                //    locationProductAttributeDto.CustomAttribute02 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute03MapId:
                //    locationProductAttributeDto.CustomAttribute03 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute04MapId:
                //    locationProductAttributeDto.CustomAttribute04 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute05MapId:
                //    locationProductAttributeDto.CustomAttribute05 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute06MapId:
                //    locationProductAttributeDto.CustomAttribute06 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute07MapId:
                //    locationProductAttributeDto.CustomAttribute07 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute08MapId:
                //    locationProductAttributeDto.CustomAttribute08 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute09MapId:
                //    locationProductAttributeDto.CustomAttribute09 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute10MapId:
                //    locationProductAttributeDto.CustomAttribute10 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute11MapId:
                //    locationProductAttributeDto.CustomAttribute11 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute12MapId:
                //    locationProductAttributeDto.CustomAttribute12 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute13MapId:
                //    locationProductAttributeDto.CustomAttribute13 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute14MapId:
                //    locationProductAttributeDto.CustomAttribute14 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute15MapId:
                //    locationProductAttributeDto.CustomAttribute15 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute16MapId:
                //    locationProductAttributeDto.CustomAttribute16 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute17MapId:
                //    locationProductAttributeDto.CustomAttribute17 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute18MapId:
                //    locationProductAttributeDto.CustomAttribute18 = (Object)cellValue;
                //    break;


                //case LocationProductAttributeImportMappingList.CustomAttribute19MapId:
                //    locationProductAttributeDto.CustomAttribute19 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute20MapId:
                //    locationProductAttributeDto.CustomAttribute20 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute21MapId:
                //    locationProductAttributeDto.CustomAttribute21 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute22MapId:
                //    locationProductAttributeDto.CustomAttribute22 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute23MapId:
                //    locationProductAttributeDto.CustomAttribute23 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute24MapId:
                //    locationProductAttributeDto.CustomAttribute24 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute25MapId:
                //    locationProductAttributeDto.CustomAttribute25 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute26MapId:
                //    locationProductAttributeDto.CustomAttribute26 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute27MapId:
                //    locationProductAttributeDto.CustomAttribute27 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute28MapId:
                //    locationProductAttributeDto.CustomAttribute28 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute29MapId:
                //    locationProductAttributeDto.CustomAttribute29 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute30MapId:
                //    locationProductAttributeDto.CustomAttribute30 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute31MapId:
                //    locationProductAttributeDto.CustomAttribute31 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute32MapId:
                //    locationProductAttributeDto.CustomAttribute32 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute33MapId:
                //    locationProductAttributeDto.CustomAttribute33 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute34MapId:
                //    locationProductAttributeDto.CustomAttribute34 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute35MapId:
                //    locationProductAttributeDto.CustomAttribute35 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute36MapId:
                //    locationProductAttributeDto.CustomAttribute36 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute37MapId:
                //    locationProductAttributeDto.CustomAttribute37 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute38MapId:
                //    locationProductAttributeDto.CustomAttribute38 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute39MapId:
                //    locationProductAttributeDto.CustomAttribute39 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute40MapId:
                //    locationProductAttributeDto.CustomAttribute40 = (Object)cellValue;
                //    break;


                //case LocationProductAttributeImportMappingList.CustomAttribute41MapId:
                //    locationProductAttributeDto.CustomAttribute41 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute42MapId:
                //    locationProductAttributeDto.CustomAttribute42 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute43MapId:
                //    locationProductAttributeDto.CustomAttribute43 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute44MapId:
                //    locationProductAttributeDto.CustomAttribute44 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute45MapId:
                //    locationProductAttributeDto.CustomAttribute45 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute46MapId:
                //    locationProductAttributeDto.CustomAttribute46 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute47MapId:
                //    locationProductAttributeDto.CustomAttribute47 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute48MapId:
                //    locationProductAttributeDto.CustomAttribute48 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute49MapId:
                //    locationProductAttributeDto.CustomAttribute49 = (Object)cellValue;
                //    break;

                //case LocationProductAttributeImportMappingList.CustomAttribute50MapId:
                //    locationProductAttributeDto.CustomAttribute50 = (Object)cellValue;
                //    break;


                //#endregion

                default: throw new NotImplementedException();
            }
        }

        internal static void SetIsSetPropertyByMappingId(Int32 mappingId, LocationProductAttributeIsSetDto isSetDto)
        {
            switch (mappingId)
            {
                #region LocationProductAttributeProperties
                case LocationProductAttributeImportMappingList.CaseCostMapId:
                    isSetDto.IsCaseCostSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CaseDeepMapId:
                    isSetDto.IsCaseDeepSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CaseDepthMapId:
                    isSetDto.IsCaseDepthSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CaseHeightMapId:
                    isSetDto.IsCaseHeightSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CaseHighMapId:
                    isSetDto.IsCaseHighSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CasePackUnitsMapId:
                    isSetDto.IsCasePackUnitsSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CaseWideMapId:
                    isSetDto.IsCaseWideSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CaseWidthMapId:
                    isSetDto.IsCaseWidthSet = true;
                    break;
                case LocationProductAttributeImportMappingList.ConsumerInformationMapId:
                    isSetDto.IsConsumerInformationSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CostPriceMapId:
                    isSetDto.IsCostPriceSet = true;
                    break;
                case LocationProductAttributeImportMappingList.DaysOfSupplyMapId:
                    isSetDto.IsDaysOfSupplySet = true;
                    break;
                case LocationProductAttributeImportMappingList.DeliveryFrequencyDaysMapId:
                    isSetDto.IsDeliveryFrequencyDaysSet = true;
                    break;
                case LocationProductAttributeImportMappingList.DeliveryMethodMapId:
                    isSetDto.IsDeliveryMethodSet = true;
                    break;
                case LocationProductAttributeImportMappingList.DepthMapId:
                    isSetDto.IsDepthSet = true;
                    break;
                case LocationProductAttributeImportMappingList.DescriptionMapId:
                    isSetDto.IsDescriptionSet = true;
                    break;
                case LocationProductAttributeImportMappingList.GTINMapId:
                    isSetDto.IsGTINSet = true;
                    break;
                case LocationProductAttributeImportMappingList.HeightMapId:
                    isSetDto.IsHeightSet = true;
                    break;
                case LocationProductAttributeImportMappingList.LocationCodeMapId:
                    isSetDto.IsLocationIdSet = true;
                    break;
                case LocationProductAttributeImportMappingList.ManufacturerCodeMapId:
                    isSetDto.IsManufacturerCodeSet = true;
                    break;
                case LocationProductAttributeImportMappingList.ManufacturerMapId:
                    isSetDto.IsManufacturerSet = true;
                    break;
                case LocationProductAttributeImportMappingList.MaxDeepMapId:
                    isSetDto.IsMaxDeepSet = true;
                    break;
                case LocationProductAttributeImportMappingList.MinDeepMapId:
                    isSetDto.IsMinDeepSet = true;
                    break;
                case LocationProductAttributeImportMappingList.ModelMapId:
                    isSetDto.IsModelSet = true;
                    break;
                case LocationProductAttributeImportMappingList.PatternMapId:
                    isSetDto.IsPatternSet = true;
                    break;
                case LocationProductAttributeImportMappingList.ProductGTINMapId:
                    isSetDto.IsProductIdSet = true;
                    break;
                case LocationProductAttributeImportMappingList.RecommendedRetailPriceMapId:
                    isSetDto.IsRecommendedRetailPriceSet = true;
                    break;
                case LocationProductAttributeImportMappingList.SellPackCountMapId:
                    isSetDto.IsSellPackCountSet = true;
                    break;
                case LocationProductAttributeImportMappingList.SellPackDescriptionMapId:
                    isSetDto.IsSellPackDescriptionSet = true;
                    break;
                case LocationProductAttributeImportMappingList.SellPriceMapId:
                    isSetDto.IsSellPriceSet = true;
                    break;
                case LocationProductAttributeImportMappingList.ShelfLifeMapId:
                    isSetDto.IsShelfLifeSet = true;
                    break;
                case LocationProductAttributeImportMappingList.SizeMapId:
                    isSetDto.IsSizeSet = true;
                    break;
                case LocationProductAttributeImportMappingList.StatusTypeMapId:
                    isSetDto.IsStatusTypeSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayDeepMapId:
                    isSetDto.IsTrayDeepSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayDepthMapId:
                    isSetDto.IsTrayDepthSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayHeightMapId:
                    isSetDto.IsTrayHeightSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayHighMapId:
                    isSetDto.IsTrayHighSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayPackUnitsMapId:
                    isSetDto.IsTrayPackUnitsSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayThickDepthMapId:
                    isSetDto.IsTrayThickDepthSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayThickHeightMapId:
                    isSetDto.IsTrayThickHeightSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayThickWidthMapId:
                    isSetDto.IsTrayThickWidthSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayWideMapId:
                    isSetDto.IsTrayWideSet = true;
                    break;
                case LocationProductAttributeImportMappingList.TrayWidthMapId:
                    isSetDto.IsTrayWidthSet = true;
                    break;
                case LocationProductAttributeImportMappingList.UnitOfMeasureMapId:
                    isSetDto.IsUnitOfMeasureSet = true;
                    break;
                case LocationProductAttributeImportMappingList.VendorCodeMapId:
                    isSetDto.IsVendorCodeSet = true;
                    break;
                case LocationProductAttributeImportMappingList.VendorMapId:
                    isSetDto.IsVendorSet = true;
                    break;
                case LocationProductAttributeImportMappingList.WidthMapId:
                    isSetDto.IsWidthSet = true;
                    break;
                case LocationProductAttributeImportMappingList.CorporateCodeMapId:
                    isSetDto.IsCorporateCodeSet = true;
                    break;

                #endregion

                //#region Custom Attributes

                //case LocationProductAttributeImportMappingList.CustomAttribute01MapId:
                //    isSetDto.IsCustomAttribute01Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute02MapId:
                //    isSetDto.IsCustomAttribute02Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute03MapId:
                //    isSetDto.IsCustomAttribute03Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute04MapId:
                //    isSetDto.IsCustomAttribute04Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute05MapId:
                //    isSetDto.IsCustomAttribute05Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute06MapId:
                //    isSetDto.IsCustomAttribute06Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute07MapId:
                //    isSetDto.IsCustomAttribute07Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute08MapId:
                //    isSetDto.IsCustomAttribute08Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute09MapId:
                //    isSetDto.IsCustomAttribute09Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute10MapId:
                //    isSetDto.IsCustomAttribute10Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute11MapId:
                //    isSetDto.IsCustomAttribute11Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute12MapId:
                //    isSetDto.IsCustomAttribute12Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute13MapId:
                //    isSetDto.IsCustomAttribute13Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute14MapId:
                //    isSetDto.IsCustomAttribute14Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute15MapId:
                //    isSetDto.IsCustomAttribute15Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute16MapId:
                //    isSetDto.IsCustomAttribute16Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute17MapId:
                //    isSetDto.IsCustomAttribute17Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute18MapId:
                //    isSetDto.IsCustomAttribute18Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute19MapId:
                //    isSetDto.IsCustomAttribute19Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute20MapId:
                //    isSetDto.IsCustomAttribute20Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute21MapId:
                //    isSetDto.IsCustomAttribute21Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute22MapId:
                //    isSetDto.IsCustomAttribute22Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute23MapId:
                //    isSetDto.IsCustomAttribute23Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute24MapId:
                //    isSetDto.IsCustomAttribute24Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute25MapId:
                //    isSetDto.IsCustomAttribute25Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute26MapId:
                //    isSetDto.IsCustomAttribute26Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute27MapId:
                //    isSetDto.IsCustomAttribute27Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute28MapId:
                //    isSetDto.IsCustomAttribute28Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute29MapId:
                //    isSetDto.IsCustomAttribute29Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute30MapId:
                //    isSetDto.IsCustomAttribute30Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute31MapId:
                //    isSetDto.IsCustomAttribute31Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute32MapId:
                //    isSetDto.IsCustomAttribute32Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute33MapId:
                //    isSetDto.IsCustomAttribute33Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute34MapId:
                //    isSetDto.IsCustomAttribute34Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute35MapId:
                //    isSetDto.IsCustomAttribute35Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute36MapId:
                //    isSetDto.IsCustomAttribute36Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute37MapId:
                //    isSetDto.IsCustomAttribute37Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute38MapId:
                //    isSetDto.IsCustomAttribute38Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute39MapId:
                //    isSetDto.IsCustomAttribute39Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute40MapId:
                //    isSetDto.IsCustomAttribute40Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute41MapId:
                //    isSetDto.IsCustomAttribute41Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute42MapId:
                //    isSetDto.IsCustomAttribute42Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute43MapId:
                //    isSetDto.IsCustomAttribute43Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute44MapId:
                //    isSetDto.IsCustomAttribute44Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute45MapId:
                //    isSetDto.IsCustomAttribute45Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute46MapId:
                //    isSetDto.IsCustomAttribute46Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute47MapId:
                //    isSetDto.IsCustomAttribute47Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute48MapId:
                //    isSetDto.IsCustomAttribute48Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute49MapId:
                //    isSetDto.IsCustomAttribute49Set = true;
                //    break;
                //case LocationProductAttributeImportMappingList.CustomAttribute50MapId:
                //    isSetDto.IsCustomAttribute50Set = true;
                //    break;

                //#endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override string GetBindingPath(int mappingId)
        {
            switch (mappingId)
            {
                #region LocationProductIllegalProperties

                case LocationProductAttributeImportMappingList.LocationCodeMapId: return Message.LocationProductAttribute_LocationCodeBindingPath;
                case LocationProductAttributeImportMappingList.ProductGTINMapId: return Message.LocationProductAttribute_ProductGTINBindingPath;
                case LocationProductAttributeImportMappingList.GTINMapId: return LocationProductAttribute.GTINProperty.Name;
                case LocationProductAttributeImportMappingList.DescriptionMapId: return LocationProductAttribute.DescriptionProperty.Name;
                case LocationProductAttributeImportMappingList.HeightMapId: return LocationProductAttribute.HeightProperty.Name;
                case LocationProductAttributeImportMappingList.WidthMapId: return LocationProductAttribute.WidthProperty.Name;
                case LocationProductAttributeImportMappingList.DepthMapId: return LocationProductAttribute.DepthProperty.Name;
                case LocationProductAttributeImportMappingList.CasePackUnitsMapId: return LocationProductAttribute.CasePackUnitsProperty.Name;
                case LocationProductAttributeImportMappingList.CaseHighMapId: return LocationProductAttribute.CaseHighProperty.Name;
                case LocationProductAttributeImportMappingList.CaseWideMapId: return LocationProductAttribute.CaseWideProperty.Name;
                case LocationProductAttributeImportMappingList.CaseDeepMapId: return LocationProductAttribute.CaseDeepProperty.Name;
                case LocationProductAttributeImportMappingList.CaseHeightMapId: return LocationProductAttribute.CaseHeightProperty.Name;
                case LocationProductAttributeImportMappingList.CaseDepthMapId: return LocationProductAttribute.CaseDepthProperty.Name;
                case LocationProductAttributeImportMappingList.CaseWidthMapId: return LocationProductAttribute.CaseWidthProperty.Name;
                case LocationProductAttributeImportMappingList.DaysOfSupplyMapId: return LocationProductAttribute.DaysOfSupplyProperty.Name;
                case LocationProductAttributeImportMappingList.MinDeepMapId: return LocationProductAttribute.MinDeepProperty.Name;
                case LocationProductAttributeImportMappingList.MaxDeepMapId: return LocationProductAttribute.MaxDeepProperty.Name;
                case LocationProductAttributeImportMappingList.TrayPackUnitsMapId: return LocationProductAttribute.TrayPackUnitsProperty.Name;
                case LocationProductAttributeImportMappingList.TrayHighMapId: return LocationProductAttribute.TrayHighProperty.Name;
                case LocationProductAttributeImportMappingList.TrayWideMapId: return LocationProductAttribute.TrayWideProperty.Name;
                case LocationProductAttributeImportMappingList.TrayDeepMapId: return LocationProductAttribute.TrayDeepProperty.Name;
                case LocationProductAttributeImportMappingList.TrayHeightMapId: return LocationProductAttribute.TrayHeightProperty.Name;
                case LocationProductAttributeImportMappingList.TrayDepthMapId: return LocationProductAttribute.TrayDepthProperty.Name;
                case LocationProductAttributeImportMappingList.TrayWidthMapId: return LocationProductAttribute.TrayWidthProperty.Name;
                case LocationProductAttributeImportMappingList.TrayThickHeightMapId: return LocationProductAttribute.TrayThickHeightProperty.Name;
                case LocationProductAttributeImportMappingList.TrayThickDepthMapId: return LocationProductAttribute.TrayThickDepthProperty.Name;
                case LocationProductAttributeImportMappingList.TrayThickWidthMapId: return LocationProductAttribute.TrayThickWidthProperty.Name;
                case LocationProductAttributeImportMappingList.StatusTypeMapId: return LocationProductAttribute.StatusTypeProperty.Name;
                case LocationProductAttributeImportMappingList.ShelfLifeMapId: return LocationProductAttribute.ShelfLifeProperty.Name;
                case LocationProductAttributeImportMappingList.DeliveryFrequencyDaysMapId: return LocationProductAttribute.DeliveryFrequencyDaysProperty.Name;
                case LocationProductAttributeImportMappingList.DeliveryMethodMapId: return LocationProductAttribute.DeliveryMethodProperty.Name;
                case LocationProductAttributeImportMappingList.VendorCodeMapId: return LocationProductAttribute.VendorCodeProperty.Name;
                case LocationProductAttributeImportMappingList.VendorMapId: return LocationProductAttribute.VendorProperty.Name;
                case LocationProductAttributeImportMappingList.ManufacturerCodeMapId: return LocationProductAttribute.ManufacturerCodeProperty.Name;
                case LocationProductAttributeImportMappingList.ManufacturerMapId: return LocationProductAttribute.ManufacturerProperty.Name;
                case LocationProductAttributeImportMappingList.SizeMapId: return LocationProductAttribute.SizeProperty.Name;
                case LocationProductAttributeImportMappingList.UnitOfMeasureMapId: return LocationProductAttribute.UnitOfMeasureProperty.Name;
                case LocationProductAttributeImportMappingList.SellPriceMapId: return LocationProductAttribute.SellPriceProperty.Name;
                case LocationProductAttributeImportMappingList.SellPackCountMapId: return LocationProductAttribute.SellPackCountProperty.Name;
                case LocationProductAttributeImportMappingList.SellPackDescriptionMapId: return LocationProductAttribute.SellPackDescriptionProperty.Name;
                case LocationProductAttributeImportMappingList.RecommendedRetailPriceMapId: return LocationProductAttribute.RecommendedRetailPriceProperty.Name;
                case LocationProductAttributeImportMappingList.CostPriceMapId: return LocationProductAttribute.CostPriceProperty.Name;
                case LocationProductAttributeImportMappingList.CaseCostMapId: return LocationProductAttribute.CaseCostProperty.Name;
                case LocationProductAttributeImportMappingList.ConsumerInformationMapId: return LocationProductAttribute.ConsumerInformationProperty.Name;
                case LocationProductAttributeImportMappingList.PatternMapId: return LocationProductAttribute.PatternProperty.Name;
                case LocationProductAttributeImportMappingList.ModelMapId: return LocationProductAttribute.ModelProperty.Name;
                case LocationProductAttributeImportMappingList.CorporateCodeMapId: return LocationProductAttribute.CorporateCodeProperty.Name;

                #endregion

                //#region Custom Attributes

                //case LocationProductAttributeImportMappingList.CustomAttribute01MapId: return LocationProductAttribute.CustomAttribute01Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute02MapId: return LocationProductAttribute.CustomAttribute02Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute03MapId: return LocationProductAttribute.CustomAttribute03Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute04MapId: return LocationProductAttribute.CustomAttribute04Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute05MapId: return LocationProductAttribute.CustomAttribute05Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute06MapId: return LocationProductAttribute.CustomAttribute06Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute07MapId: return LocationProductAttribute.CustomAttribute07Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute08MapId: return LocationProductAttribute.CustomAttribute08Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute09MapId: return LocationProductAttribute.CustomAttribute09Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute10MapId: return LocationProductAttribute.CustomAttribute10Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute11MapId: return LocationProductAttribute.CustomAttribute11Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute12MapId: return LocationProductAttribute.CustomAttribute12Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute13MapId: return LocationProductAttribute.CustomAttribute13Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute14MapId: return LocationProductAttribute.CustomAttribute14Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute15MapId: return LocationProductAttribute.CustomAttribute15Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute16MapId: return LocationProductAttribute.CustomAttribute16Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute17MapId: return LocationProductAttribute.CustomAttribute17Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute18MapId: return LocationProductAttribute.CustomAttribute18Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute19MapId: return LocationProductAttribute.CustomAttribute19Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute20MapId: return LocationProductAttribute.CustomAttribute20Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute21MapId: return LocationProductAttribute.CustomAttribute21Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute22MapId: return LocationProductAttribute.CustomAttribute22Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute23MapId: return LocationProductAttribute.CustomAttribute23Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute24MapId: return LocationProductAttribute.CustomAttribute24Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute25MapId: return LocationProductAttribute.CustomAttribute25Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute26MapId: return LocationProductAttribute.CustomAttribute26Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute27MapId: return LocationProductAttribute.CustomAttribute27Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute28MapId: return LocationProductAttribute.CustomAttribute28Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute29MapId: return LocationProductAttribute.CustomAttribute29Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute30MapId: return LocationProductAttribute.CustomAttribute30Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute31MapId: return LocationProductAttribute.CustomAttribute31Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute32MapId: return LocationProductAttribute.CustomAttribute32Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute33MapId: return LocationProductAttribute.CustomAttribute33Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute34MapId: return LocationProductAttribute.CustomAttribute34Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute35MapId: return LocationProductAttribute.CustomAttribute35Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute36MapId: return LocationProductAttribute.CustomAttribute36Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute37MapId: return LocationProductAttribute.CustomAttribute37Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute38MapId: return LocationProductAttribute.CustomAttribute38Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute39MapId: return LocationProductAttribute.CustomAttribute39Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute40MapId: return LocationProductAttribute.CustomAttribute40Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute41MapId: return LocationProductAttribute.CustomAttribute41Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute42MapId: return LocationProductAttribute.CustomAttribute42Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute43MapId: return LocationProductAttribute.CustomAttribute43Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute44MapId: return LocationProductAttribute.CustomAttribute44Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute45MapId: return LocationProductAttribute.CustomAttribute45Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute46MapId: return LocationProductAttribute.CustomAttribute46Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute47MapId: return LocationProductAttribute.CustomAttribute47Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute48MapId: return LocationProductAttribute.CustomAttribute48Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute49MapId: return LocationProductAttribute.CustomAttribute49Property.Name;
                //case LocationProductAttributeImportMappingList.CustomAttribute50MapId: return LocationProductAttribute.CustomAttribute50Property.Name;
                //#endregion

                default: throw new NotImplementedException();
            }
        }

        #endregion

        #region IModelObjectColumnHelper members

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;

            switch (mappingId)
            {
                #region
                case LocationProductAttributeImportMappingList.CaseCostMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CaseCostProperty.Name);
                case LocationProductAttributeImportMappingList.CaseDeepMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CaseDeepProperty.Name);
                case LocationProductAttributeImportMappingList.CaseDepthMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CaseDepthProperty.Name);
                case LocationProductAttributeImportMappingList.CaseHeightMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CaseHeightProperty.Name);
                case LocationProductAttributeImportMappingList.CaseHighMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CaseHighProperty.Name);
                case LocationProductAttributeImportMappingList.CasePackUnitsMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CasePackUnitsProperty.Name);
                case LocationProductAttributeImportMappingList.CaseWideMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CaseWideProperty.Name);
                case LocationProductAttributeImportMappingList.CaseWidthMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CaseWidthProperty.Name);
                case LocationProductAttributeImportMappingList.ConsumerInformationMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.ConsumerInformationProperty.Name);
                case LocationProductAttributeImportMappingList.CorporateCodeMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CorporateCodeProperty.Name);
                case LocationProductAttributeImportMappingList.CostPriceMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.CostPriceProperty.Name);
                case LocationProductAttributeImportMappingList.DaysOfSupplyMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.DaysOfSupplyProperty.Name);
                case LocationProductAttributeImportMappingList.DeliveryFrequencyDaysMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.DeliveryFrequencyDaysProperty.Name);
                case LocationProductAttributeImportMappingList.DeliveryMethodMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.DeliveryMethodProperty.Name);
                case LocationProductAttributeImportMappingList.DepthMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.DepthProperty.Name);
                case LocationProductAttributeImportMappingList.DescriptionMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.DescriptionProperty.Name);
                case LocationProductAttributeImportMappingList.GTINMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.GTINProperty.Name);
                case LocationProductAttributeImportMappingList.HeightMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.HeightProperty.Name);
                case LocationProductAttributeImportMappingList.LocationCodeMapId: return String.Format("{0}{1}", prefix, Location.CodeProperty.Name);
                case LocationProductAttributeImportMappingList.ManufacturerCodeMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.ManufacturerCodeProperty.Name);
                case LocationProductAttributeImportMappingList.ManufacturerMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.ManufacturerProperty.Name);
                case LocationProductAttributeImportMappingList.MaxDeepMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.MaxDeepProperty.Name);
                case LocationProductAttributeImportMappingList.MinDeepMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.MinDeepProperty.Name);
                case LocationProductAttributeImportMappingList.ModelMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.ModelProperty.Name);
                case LocationProductAttributeImportMappingList.PatternMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.PatternProperty.Name);
                case LocationProductAttributeImportMappingList.ProductGTINMapId: return String.Format("{0}{1}", prefix, Product.GtinProperty.Name);
                case LocationProductAttributeImportMappingList.RecommendedRetailPriceMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.RecommendedRetailPriceProperty.Name);
                case LocationProductAttributeImportMappingList.SellPackCountMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.SellPackCountProperty.Name);
                case LocationProductAttributeImportMappingList.SellPackDescriptionMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.SellPackDescriptionProperty.Name);
                case LocationProductAttributeImportMappingList.SellPriceMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.SellPriceProperty.Name);
                case LocationProductAttributeImportMappingList.ShelfLifeMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.ShelfLifeProperty.Name);
                case LocationProductAttributeImportMappingList.SizeMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.SizeProperty.Name);
                case LocationProductAttributeImportMappingList.StatusTypeMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.StatusTypeProperty.Name);
                case LocationProductAttributeImportMappingList.TrayDeepMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayDeepProperty.Name);
                case LocationProductAttributeImportMappingList.TrayDepthMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayDepthProperty.Name);
                case LocationProductAttributeImportMappingList.TrayHeightMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayHeightProperty.Name);
                case LocationProductAttributeImportMappingList.TrayHighMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayHighProperty.Name);
                case LocationProductAttributeImportMappingList.TrayPackUnitsMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayPackUnitsProperty.Name);
                case LocationProductAttributeImportMappingList.TrayThickDepthMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayThickDepthProperty.Name);
                case LocationProductAttributeImportMappingList.TrayThickHeightMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayThickHeightProperty.Name);
                case LocationProductAttributeImportMappingList.TrayThickWidthMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayThickWidthProperty.Name);
                case LocationProductAttributeImportMappingList.TrayWideMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayWideProperty.Name);
                case LocationProductAttributeImportMappingList.TrayWidthMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.TrayWidthProperty.Name);
                case LocationProductAttributeImportMappingList.UnitOfMeasureMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.UnitOfMeasureProperty.Name);
                case LocationProductAttributeImportMappingList.VendorCodeMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.VendorCodeProperty.Name);
                case LocationProductAttributeImportMappingList.VendorMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.VendorProperty.Name);
                case LocationProductAttributeImportMappingList.WidthMapId: return String.Format("{0}{1}", prefix, LocationProductAttribute.WidthProperty.Name);

                #endregion

                default: throw new NotImplementedException();
            }
        }

        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mapping)
        {
            //does this have any groups?
            return null;
        }

        //Boolean IModelObjectColumnHelper.IsUOMDisplayConversionRequired(Int32 mappingId)
        //{
        //    return LocationSpaceElementImportMappingList.IsUOMDisplayConversionRequired(mappingId);
        //}

        #endregion
    }
}
