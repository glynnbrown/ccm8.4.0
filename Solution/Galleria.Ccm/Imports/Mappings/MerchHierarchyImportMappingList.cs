﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Hodson
//	Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

using Galleria.Framework.Imports;

using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class MerchHierarchyImportMappingList : ImportMappingList<MerchHierarchyImportMappingList>
    {
        #region Fields

        private Dictionary<Int32, Boolean> _levelToMappingIds = new Dictionary<Int32, Boolean>();

        #endregion

        #region Constructors
        private MerchHierarchyImportMappingList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static MerchHierarchyImportMappingList NewMerchHierarchyImportMappingList(IEnumerable<String> levelNameList)
        {
            MerchHierarchyImportMappingList list = new MerchHierarchyImportMappingList();
            list.Create(levelNameList);
            return list;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(IEnumerable<String> levelNameList)
        {
            this.RaiseListChangedEvents = false;

            Int32 mappingNum = 1;
            Boolean isCodeMapping = true;

            //cycle through adding the mappings for the structure
            foreach (String levelName in levelNameList)
            {
                for (int i = 0; i < 2; i++)
                {
                    //First level is mandatory
                    if (levelName == levelNameList.ToList()[0])
                    {
                        if (isCodeMapping)
                        {
                            this.Add(ImportMapping.NewImportMapping(mappingNum, String.Format("{0} {1}",
                            levelName, ProductGroup.CodeProperty.FriendlyName),
                            ProductGroup.CodeProperty.Description, true, true, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 1, 50, false));
                        }
                        else
                        {
                            this.Add(ImportMapping.NewImportMapping(mappingNum, String.Format("{0} {1}",
                           levelName, ProductGroup.NameProperty.FriendlyName),
                           ProductGroup.NameProperty.Description, true, ProductGroup.NameProperty.Type, ProductGroup.NameProperty.DefaultValue, 1, 100, false));
                        }
                    }
                    else
                    {
                        //Levels after the first can be null
                        if (isCodeMapping)
                        {
                            this.Add(ImportMapping.NewImportMapping(mappingNum, String.Format("{0} {1}",
                            levelName, ProductGroup.CodeProperty.FriendlyName),
                            ProductGroup.CodeProperty.Description, false, true, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 1, 50, false));
                        }
                        else
                        {
                            this.Add(ImportMapping.NewImportMapping(mappingNum, String.Format("{0} {1}",
                           levelName, ProductGroup.NameProperty.FriendlyName),
                           ProductGroup.NameProperty.Description, false, ProductGroup.NameProperty.Type, ProductGroup.NameProperty.DefaultValue, 1, 100, false));
                        }
                    }

                    //add the ref to the private dict
                    _levelToMappingIds.Add(mappingNum, isCodeMapping);

                    //swap the flag
                    isCodeMapping = !isCodeMapping;
                    mappingNum++;
                }
            }

            this.RaiseListChangedEvents = true;
        }


        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="productDto"></param>
        /// <param name="attributeDataDto"></param>
        /// <returns></returns>
        internal Object GetValueByMappingId(Int32 mappingId,
            ProductGroupDto productGroupDto, IEnumerable<ProductGroupDto> hierarchyDtos, Object rootDtoId)
        {
            //construct the path list for the given dto
            List<ProductGroupDto> pathList = new List<ProductGroupDto>();
            ProductGroupDto currentDto = productGroupDto;
            pathList.Add(currentDto);
            while (true)
            {
                Object parentId = currentDto.ParentGroupId;
                if (!Object.Equals(parentId, rootDtoId))
                {
                    currentDto = hierarchyDtos.First(c => Object.Equals(c.Id, parentId));
                    pathList.Add(currentDto);
                }
                else
                {
                    break;
                }
            }
            pathList.Reverse();


            Int32 levelIndex = ((mappingId + (mappingId % 2)) / 2) - 1;

            if (levelIndex < pathList.Count)
            {
                ProductGroupDto resultDto = pathList[levelIndex];
                Boolean isCodeMapping = _levelToMappingIds[mappingId];

                if (isCodeMapping)
                {
                    return resultDto.Code;
                }
                else
                {
                    return resultDto.Name;
                }
            }
            return null;
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="productDto"></param>
        /// <param name="attributeDataDto"></param>
        internal void SetValueByMappingId(Int32 mappingId, Int32 levelId, Object cellValue, ProductGroupDto productGroupDto)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;
            Boolean isCodeMapping = _levelToMappingIds[mappingId];

            if (isCodeMapping)
            {
                productGroupDto.Code = Convert.ToString(cellValue, prov);
            }
            else
            {
                productGroupDto.Name = Convert.ToString(cellValue, prov);
            }

        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override string GetBindingPath(Int32 mappingId)
        {
            throw new NotSupportedException();

            //this wont work properly
            //Boolean isCodeMapping = _levelToMappingIds[mappingId];

            //if (isCodeMapping)
            //{
            //    return ProductGroup.CodeProperty.Name;
            //}
            //else
            //{
            //    return ProductGroup.NameProperty.Name;
            //}

        }


        #endregion
    }
}
