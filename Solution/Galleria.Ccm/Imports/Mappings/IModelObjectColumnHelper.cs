﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Imports.Mappings
{
    public interface IModelObjectColumnHelper : IImportMappingList
    {
        /// <summary>
        /// Returns the binding paths for the given column
        /// </summary>
        /// <param name="mappingId">The map id to get the binding for</param>
        /// <param name="normalBindingPrefix">The binding prefix to use</param>
        /// <returns></returns>
        String GetBindingPath(Int32 mappingId, String bindingPrefix);

        /// <summary>
        /// Returns the column group name
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        String GetColumnGroupName(Int32 mappingId);
    }
}
