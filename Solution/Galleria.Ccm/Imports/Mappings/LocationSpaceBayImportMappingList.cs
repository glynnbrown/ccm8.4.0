﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
// V8-24264 : A.Probyn
//	Extended for display type on import mapping constructor.
// V8-30543 : J.Pickup
//	Imports for base height and base width were inverted. This incorrectly resulted in the export for theese values to be the wrong way round.
#endregion
#region Version History: (CCM 8.1.1)
// V8-30543 : J.Pickup
//	Imports for base height and base width were inverted. This incorrectly resulted in the export for theese values to be the wrong way round.
// V8-30542 : J.Pickup
//	Fixed an issue where an import where an enum value could not be correctly converted into a byte value, which would throw an error.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Globalization;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class LocationSpaceBayImportMappingList : ImportMappingList<LocationSpaceBayImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        public const Int32 LocationCodeMappingId = 0;
        public const Int32 ProductGroupCodeMappingId = 1;
        public const Int32 OrderMappingId = 2;
        public const Int32 HeightMappingId = 3;
        public const Int32 WidthMappingId = 4;
        public const Int32 DepthMappingId = 5;
        public const Int32 BaseHeightMappingId = 6;
        public const Int32 BaseWidthMappingId = 7;
        public const Int32 BaseDepthMappingId = 8;
        public const Int32 IsAisleStartMappingId = 9;
        public const Int32 IsAisleEndMappingId = 10;
        public const Int32 IsAisleRightMappingId = 11;
        public const Int32 IsAisleLeftMappingId = 12;
        public const Int32 BayLocationRefMappingId = 13;
        public const Int32 BayFloorDrawingNoMappingId = 14;
        public const Int32 FixtureNameMappingId = 15;
        public const Int32 ManufacturerNameMappingId = 16;
        public const Int32 BayTypeMappingId = 17;
        public const Int32 FixtureTypeMappingId = 18;
        public const Int32 BayTypePostFixMappingId = 19;
        public const Int32 BayTypeCalculationMappingId = 20;
        public const Int32 NotchPitchMappingId = 21;
        public const Int32 BarcodeMappingId = 22;
        public const Int32 ColourMappingId = 23;
        public const Int32 AssetNumberMappingId = 24;
        public const Int32 TemperatureMappingId = 25;
        public const Int32 IsPromotionalMappingId = 26;
        public const Int32 HasPowerMappingId = 27;
        public const Int32 NotchStartYMappingId = 28;
        public const Int32 FixtureShapeMappingId = 29;

        #endregion

        #region Constructors
        private LocationSpaceBayImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static LocationSpaceBayImportMappingList NewLocationSpaceBayImportMappingList()
        {
            LocationSpaceBayImportMappingList list = new LocationSpaceBayImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region LocationSpaceBayProperties

            this.Add(ImportMapping.NewImportMapping(LocationCodeMappingId, Message.LocationSpaceBayImportList_LocationCode, Message.LocationSpaceBayImportList_LocationCode_Description, true, /*identifier*/true, Location.CodeProperty.Type, Location.CodeProperty.DefaultValue, 0, 50, false, Location.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ProductGroupCodeMappingId, Message.LocationSpaceBayImportList_ProductGroupCode, Message.LocationSpaceBayImportList_ProductGroupCode_Description, true, /*identifier*/true, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 1, 50, false, ProductGroup.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(OrderMappingId, LocationSpaceBay.OrderProperty.FriendlyName, LocationSpaceBay.OrderProperty.Description, true, /*identifier*/true, LocationSpaceBay.OrderProperty.Type, LocationSpaceBay.OrderProperty.DefaultValue, 0, Byte.MaxValue, true, LocationSpaceBay.OrderProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HeightMappingId, LocationSpaceBay.HeightProperty.FriendlyName, LocationSpaceBay.HeightProperty.Description, true, LocationSpaceBay.HeightProperty.Type, LocationSpaceBay.HeightProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceBay.HeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(WidthMappingId, LocationSpaceBay.WidthProperty.FriendlyName, LocationSpaceBay.WidthProperty.Description, true, LocationSpaceBay.WidthProperty.Type, LocationSpaceBay.WidthProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceBay.WidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DepthMappingId, LocationSpaceBay.DepthProperty.FriendlyName, LocationSpaceBay.DepthProperty.Description, true, LocationSpaceBay.DepthProperty.Type, LocationSpaceBay.DepthProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceBay.DepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BaseHeightMappingId, LocationSpaceBay.BaseHeightProperty.FriendlyName, LocationSpaceBay.BaseHeightProperty.Description, true, LocationSpaceBay.BaseHeightProperty.Type, LocationSpaceBay.BaseHeightProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceBay.BaseHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BaseWidthMappingId, LocationSpaceBay.BaseWidthProperty.FriendlyName, LocationSpaceBay.BaseWidthProperty.Description, true, LocationSpaceBay.BaseWidthProperty.Type, LocationSpaceBay.BaseWidthProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceBay.BaseWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BaseDepthMappingId, LocationSpaceBay.BaseDepthProperty.FriendlyName, LocationSpaceBay.BaseDepthProperty.Description, true, LocationSpaceBay.BaseDepthProperty.Type, LocationSpaceBay.BaseDepthProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceBay.BaseDepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsAisleStartMappingId, LocationSpaceBay.IsAisleStartProperty.FriendlyName, LocationSpaceBay.IsAisleStartProperty.Description, false, LocationSpaceBay.IsAisleStartProperty.Type, LocationSpaceBay.IsAisleStartProperty.DefaultValue, 0, 1, true, LocationSpaceBay.IsAisleStartProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsAisleEndMappingId, LocationSpaceBay.IsAisleEndProperty.FriendlyName, LocationSpaceBay.IsAisleEndProperty.Description, false, LocationSpaceBay.IsAisleEndProperty.Type, LocationSpaceBay.IsAisleEndProperty.DefaultValue, 0, 1, true, LocationSpaceBay.IsAisleEndProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsAisleRightMappingId, LocationSpaceBay.IsAisleRightProperty.FriendlyName, LocationSpaceBay.IsAisleRightProperty.Description, false, LocationSpaceBay.IsAisleRightProperty.Type, LocationSpaceBay.IsAisleRightProperty.DefaultValue, 0, 1, true, LocationSpaceBay.IsAisleRightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsAisleLeftMappingId, LocationSpaceBay.IsAisleLeftProperty.FriendlyName, LocationSpaceBay.IsAisleLeftProperty.Description, false, LocationSpaceBay.IsAisleLeftProperty.Type, LocationSpaceBay.IsAisleLeftProperty.DefaultValue, 0, 1, true, LocationSpaceBay.IsAisleLeftProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BayLocationRefMappingId, LocationSpaceBay.BayLocationRefProperty.FriendlyName, LocationSpaceBay.BayLocationRefProperty.Description, false, LocationSpaceBay.BayLocationRefProperty.Type, LocationSpaceBay.BayLocationRefProperty.DefaultValue, 0, 100, true, LocationSpaceBay.BayLocationRefProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BayFloorDrawingNoMappingId, LocationSpaceBay.BayFloorDrawingNoProperty.FriendlyName, LocationSpaceBay.BayFloorDrawingNoProperty.Description, false, LocationSpaceBay.BayFloorDrawingNoProperty.Type, LocationSpaceBay.BayFloorDrawingNoProperty.DefaultValue, 0, 100, true, LocationSpaceBay.BayFloorDrawingNoProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FixtureNameMappingId, LocationSpaceBay.FixtureNameProperty.FriendlyName, LocationSpaceBay.FixtureNameProperty.Description, false, LocationSpaceBay.FixtureNameProperty.Type, LocationSpaceBay.FixtureNameProperty.DefaultValue, 1, 150, true, LocationSpaceBay.FixtureNameProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ManufacturerNameMappingId, LocationSpaceBay.ManufacturerProperty.FriendlyName, LocationSpaceBay.ManufacturerProperty.Description, false, LocationSpaceBay.ManufacturerProperty.Type, LocationSpaceBay.ManufacturerProperty.DefaultValue, 0, 100, true, LocationSpaceBay.ManufacturerProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BayTypeMappingId, LocationSpaceBay.BayTypeProperty.FriendlyName, LocationSpaceBay.BayTypeProperty.Description, false, false, LocationSpaceBay.BayTypeProperty.Type, LocationSpaceBay.BayTypeProperty.DefaultValue, LocationSpaceBayTypeHelper.FriendlyNames, true, LocationSpaceBay.BayTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FixtureTypeMappingId, LocationSpaceBay.FixtureTypeProperty.FriendlyName, LocationSpaceBay.FixtureTypeProperty.Description, false, false, LocationSpaceBay.FixtureTypeProperty.Type, LocationSpaceBay.FixtureTypeProperty.DefaultValue, LocationSpaceFixtureTypeHelper.FriendlyNames, true, LocationSpaceBay.FixtureTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BayTypePostFixMappingId, LocationSpaceBay.BayTypePostFixProperty.FriendlyName, LocationSpaceBay.BayTypePostFixProperty.Description, false, LocationSpaceBay.BayTypePostFixProperty.Type, LocationSpaceBay.BayTypePostFixProperty.DefaultValue, 0, 50, true, LocationSpaceBay.BayTypePostFixProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BayTypeCalculationMappingId, LocationSpaceBay.BayTypeCalculationProperty.FriendlyName, LocationSpaceBay.BayTypeCalculationProperty.Description, false, LocationSpaceBay.BayTypeCalculationProperty.Type, LocationSpaceBay.BayTypeCalculationProperty.DefaultValue, 0, 999, true, LocationSpaceBay.BayTypeCalculationProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(NotchPitchMappingId, LocationSpaceBay.NotchPitchProperty.FriendlyName, LocationSpaceBay.NotchPitchProperty.Description, false, LocationSpaceBay.NotchPitchProperty.Type, LocationSpaceBay.NotchPitchProperty.DefaultValue, 0, 100, true, LocationSpaceBay.NotchPitchProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BarcodeMappingId, LocationSpaceBay.BarcodeProperty.FriendlyName, LocationSpaceBay.BarcodeProperty.Description, false, LocationSpaceBay.BarcodeProperty.Type, LocationSpaceBay.BarcodeProperty.DefaultValue, 0, 50, true, LocationSpaceBay.BarcodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ColourMappingId, LocationSpaceBay.ColourProperty.FriendlyName, LocationSpaceBay.ColourProperty.Description, false, LocationSpaceBay.ColourProperty.Type, LocationSpaceBay.ColourProperty.DefaultValue, 0, 50, true, LocationSpaceBay.ColourProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(AssetNumberMappingId, LocationSpaceBay.AssetProperty.FriendlyName, LocationSpaceBay.AssetProperty.Description, false, LocationSpaceBay.AssetProperty.Type, LocationSpaceBay.AssetProperty.DefaultValue, 0, 100, true, LocationSpaceBay.AssetProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TemperatureMappingId, LocationSpaceBay.TemperatureProperty.FriendlyName, LocationSpaceBay.TemperatureProperty.Description, false, LocationSpaceBay.TemperatureProperty.Type, LocationSpaceBay.TemperatureProperty.DefaultValue, 0, 999, true, LocationSpaceBay.TemperatureProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsPromotionalMappingId, LocationSpaceBay.IsPromotionalProperty.FriendlyName, LocationSpaceBay.IsPromotionalProperty.Description, false, LocationSpaceBay.IsPromotionalProperty.Type, LocationSpaceBay.IsPromotionalProperty.DefaultValue, 0, 0, true, LocationSpaceBay.IsPromotionalProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasPowerMappingId, LocationSpaceBay.HasPowerProperty.FriendlyName, LocationSpaceBay.HasPowerProperty.Description, false, LocationSpaceBay.HasPowerProperty.Type, LocationSpaceBay.HasPowerProperty.DefaultValue, 0, 0, true, LocationSpaceBay.HasPowerProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(NotchStartYMappingId, LocationSpaceBay.NotchStartYProperty.FriendlyName, LocationSpaceBay.NotchStartYProperty.Description, false, LocationSpaceBay.NotchStartYProperty.Type, LocationSpaceBay.NotchStartYProperty.DefaultValue, -250f, 250, true, LocationSpaceBay.NotchStartYProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FixtureShapeMappingId, LocationSpaceBay.FixtureShapeTypeProperty.FriendlyName, LocationSpaceBay.FixtureShapeTypeProperty.Description, false, false, LocationSpaceBay.FixtureShapeTypeProperty.Type, LocationSpaceBay.FixtureShapeTypeProperty.DefaultValue, LocationSpaceFixtureShapeTypeHelper.FriendlyNames, true, LocationSpaceBay.FixtureShapeTypeProperty.DisplayType));

            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, LocationSpaceBayDto locationSpaceBayDto,
            LocationSpaceProductGroupDto locationSpaceProductGroupDto, String locationCode,
            IEnumerable<ProductGroupDto> productGroupDtoList)
        {
            switch (mappingId)
            {
                #region Location Space Bay Properties
                case LocationSpaceBayImportMappingList.LocationCodeMappingId: return locationCode;
                case LocationSpaceBayImportMappingList.ProductGroupCodeMappingId:
                    ProductGroupDto groupDto = productGroupDtoList.FirstOrDefault(p => p.Id == locationSpaceProductGroupDto.ProductGroupId);
                    return (groupDto != null) ? groupDto.Code : null;
                case LocationSpaceBayImportMappingList.OrderMappingId: return locationSpaceBayDto.Order;
                case LocationSpaceBayImportMappingList.HeightMappingId: return locationSpaceBayDto.Height;
                case LocationSpaceBayImportMappingList.WidthMappingId: return locationSpaceBayDto.Width;
                case LocationSpaceBayImportMappingList.DepthMappingId: return locationSpaceBayDto.Depth;
                case LocationSpaceBayImportMappingList.BaseHeightMappingId: return locationSpaceBayDto.BaseHeight;
                case LocationSpaceBayImportMappingList.BaseWidthMappingId: return locationSpaceBayDto.BaseWidth;
                case LocationSpaceBayImportMappingList.BaseDepthMappingId: return locationSpaceBayDto.BaseDepth;
                case LocationSpaceBayImportMappingList.IsAisleStartMappingId: return locationSpaceBayDto.IsAisleStart;
                case LocationSpaceBayImportMappingList.IsAisleEndMappingId: return locationSpaceBayDto.IsAisleEnd;
                case LocationSpaceBayImportMappingList.IsAisleRightMappingId: return locationSpaceBayDto.IsAisleRight;
                case LocationSpaceBayImportMappingList.IsAisleLeftMappingId: return locationSpaceBayDto.IsAisleLeft;
                case LocationSpaceBayImportMappingList.BayLocationRefMappingId: return locationSpaceBayDto.BayLocationRef;
                case LocationSpaceBayImportMappingList.BayFloorDrawingNoMappingId: return locationSpaceBayDto.BayFloorDrawingNo;
                case LocationSpaceBayImportMappingList.FixtureNameMappingId: return locationSpaceBayDto.FixtureName;
                case LocationSpaceBayImportMappingList.ManufacturerNameMappingId: return locationSpaceBayDto.ManufacturerName;
                case LocationSpaceBayImportMappingList.BayTypeMappingId: return locationSpaceBayDto.BayType;
                case LocationSpaceBayImportMappingList.FixtureTypeMappingId: return locationSpaceBayDto.FixtureType;
                case LocationSpaceBayImportMappingList.BayTypePostFixMappingId: return locationSpaceBayDto.BayTypePostFix;
                case LocationSpaceBayImportMappingList.BayTypeCalculationMappingId: return locationSpaceBayDto.BayTypeCalculation;
                case LocationSpaceBayImportMappingList.NotchPitchMappingId: return locationSpaceBayDto.NotchPitch;
                case LocationSpaceBayImportMappingList.BarcodeMappingId: return locationSpaceBayDto.Barcode;
                case LocationSpaceBayImportMappingList.ColourMappingId: return locationSpaceBayDto.Colour;
                case LocationSpaceBayImportMappingList.AssetNumberMappingId: return locationSpaceBayDto.AssetNumber;
                case LocationSpaceBayImportMappingList.TemperatureMappingId: return locationSpaceBayDto.Temperature;
                case LocationSpaceBayImportMappingList.IsPromotionalMappingId: return locationSpaceBayDto.IsPromotional;
                case LocationSpaceBayImportMappingList.HasPowerMappingId: return locationSpaceBayDto.HasPower;
                case LocationSpaceBayImportMappingList.NotchStartYMappingId: return locationSpaceBayDto.NotchStartY;
                case LocationSpaceBayImportMappingList.FixtureShapeMappingId: return locationSpaceBayDto.FixtureShape;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="locationDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            LocationSpaceBayDto locationSpaceBayDto)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            switch (mappingId)
            {
                case LocationSpaceBayImportMappingList.OrderMappingId:
                    locationSpaceBayDto.Order = Convert.ToByte(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.HeightMappingId:
                    locationSpaceBayDto.Height = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.WidthMappingId:
                    locationSpaceBayDto.Width = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.DepthMappingId:
                    locationSpaceBayDto.Depth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.BaseHeightMappingId:
                    locationSpaceBayDto.BaseHeight = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.BaseWidthMappingId:
                    locationSpaceBayDto.BaseWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.BaseDepthMappingId:
                    locationSpaceBayDto.BaseDepth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.IsAisleStartMappingId:
                    locationSpaceBayDto.IsAisleStart = Convert.ToBoolean(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.IsAisleEndMappingId:
                    locationSpaceBayDto.IsAisleEnd = Convert.ToBoolean(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.IsAisleRightMappingId:
                    locationSpaceBayDto.IsAisleRight = Convert.ToBoolean(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.IsAisleLeftMappingId:
                    locationSpaceBayDto.IsAisleLeft = Convert.ToBoolean(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.BayLocationRefMappingId:
                    locationSpaceBayDto.BayLocationRef = Convert.ToString(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.BayFloorDrawingNoMappingId:
                    locationSpaceBayDto.BayFloorDrawingNo = Convert.ToString(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.FixtureNameMappingId:
                    locationSpaceBayDto.FixtureName = Convert.ToString(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.ManufacturerNameMappingId:
                    locationSpaceBayDto.ManufacturerName = Convert.ToString(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.BayTypeMappingId:
                    //Need to switch from enum value to int value
                    LocationSpaceBayType bayType;
                    if (LocationSpaceBayTypeHelper.EnumFromFriendlyName.TryGetValue(cellValue.ToString().ToLowerInvariant(), out bayType))
                    {
                        locationSpaceBayDto.BayType = (Byte)bayType;
                    }
                    break;
                case LocationSpaceBayImportMappingList.FixtureTypeMappingId:
                    //Need to switch from enum value to byte value
                    LocationSpaceFixtureType fixtureType;
                    if (LocationSpaceFixtureTypeHelper.EnumFromFriendlyName.TryGetValue(cellValue.ToString().ToLowerInvariant(), out fixtureType))
                    {
                        locationSpaceBayDto.FixtureType = (Byte)fixtureType;
                    }
                    break;
                case LocationSpaceBayImportMappingList.BayTypePostFixMappingId:
                    locationSpaceBayDto.BayTypePostFix = Convert.ToString(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.BayTypeCalculationMappingId:
                    locationSpaceBayDto.BayTypeCalculation = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.NotchPitchMappingId:
                    locationSpaceBayDto.NotchPitch = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.BarcodeMappingId:
                    locationSpaceBayDto.Barcode = Convert.ToString(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.ColourMappingId:
                    locationSpaceBayDto.Colour = Convert.ToString(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.AssetNumberMappingId:
                    locationSpaceBayDto.AssetNumber = Convert.ToString(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.TemperatureMappingId:
                    locationSpaceBayDto.Temperature = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.IsPromotionalMappingId:
                    locationSpaceBayDto.IsPromotional = Convert.ToBoolean(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.HasPowerMappingId:
                    locationSpaceBayDto.HasPower = Convert.ToBoolean(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.NotchStartYMappingId:
                    locationSpaceBayDto.NotchStartY = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceBayImportMappingList.FixtureShapeMappingId:
                    LocationSpaceFixtureShapeType fixtureShapeType;
                    if (LocationSpaceFixtureShapeTypeHelper.EnumFromFriendlyName.TryGetValue(cellValue.ToString().ToLowerInvariant(), out fixtureShapeType))
                    {
                        locationSpaceBayDto.FixtureShape = (Byte)fixtureShapeType;
                    }
                    break;
                default:
                    //Do nothing
                    break;
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override String GetBindingPath(Int32 mappingId)
        {
            return GetBindingPath(mappingId, null);
            //switch (mappingId)
            //{
            //    #region Location Space Bay Properties
            //    case LocationSpaceBayImportMappingList.LocationCodeMappingId: return Location.CodeProperty.Name;
            //    case LocationSpaceBayImportMappingList.ProductGroupCodeMappingId: return ProductGroup.CodeProperty.Name;
            //    case LocationSpaceBayImportMappingList.OrderMappingId: return LocationSpaceBay.OrderProperty.Name;
            //    case LocationSpaceBayImportMappingList.HeightMappingId: return LocationSpaceBay.HeightProperty.Name;
            //    case LocationSpaceBayImportMappingList.WidthMappingId: return LocationSpaceBay.WidthProperty.Name;
            //    case LocationSpaceBayImportMappingList.DepthMappingId: return LocationSpaceBay.DepthProperty.Name;
            //    case LocationSpaceBayImportMappingList.BaseHeightMappingId: return LocationSpaceBay.BaseHeightProperty.Name;
            //    case LocationSpaceBayImportMappingList.BaseWidthMappingId: return LocationSpaceBay.BaseWidthProperty.Name;
            //    case LocationSpaceBayImportMappingList.BaseDepthMappingId: return LocationSpaceBay.BaseDepthProperty.Name;
            //    #endregion

            //    default: throw new NotImplementedException();
            //}
        }


        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;

            switch (mappingId)
            {
                #region Location Space Bay Properties
                case LocationSpaceBayImportMappingList.LocationCodeMappingId: return String.Format("{0}{1}", prefix, Location.CodeProperty.Name);
                case LocationSpaceBayImportMappingList.ProductGroupCodeMappingId: return String.Format("{0}{1}", prefix, ProductGroup.CodeProperty.Name);
                case LocationSpaceBayImportMappingList.OrderMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.OrderProperty.Name);
                case LocationSpaceBayImportMappingList.HeightMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.HeightProperty.Name);
                case LocationSpaceBayImportMappingList.WidthMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.WidthProperty.Name);
                case LocationSpaceBayImportMappingList.DepthMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.DepthProperty.Name);
                case LocationSpaceBayImportMappingList.BaseHeightMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BaseHeightProperty.Name);
                case LocationSpaceBayImportMappingList.BaseWidthMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BaseWidthProperty.Name);
                case LocationSpaceBayImportMappingList.BaseDepthMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BaseDepthProperty.Name);

                case LocationSpaceBayImportMappingList.IsAisleStartMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.IsAisleStartProperty.Name);
                case LocationSpaceBayImportMappingList.IsAisleEndMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.IsAisleEndProperty.Name);
                case LocationSpaceBayImportMappingList.IsAisleRightMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.IsAisleRightProperty.Name);
                case LocationSpaceBayImportMappingList.IsAisleLeftMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.IsAisleLeftProperty.Name);
                case LocationSpaceBayImportMappingList.BayLocationRefMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BayLocationRefProperty.Name);
                case LocationSpaceBayImportMappingList.BayFloorDrawingNoMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BayFloorDrawingNoProperty.Name);
                case LocationSpaceBayImportMappingList.FixtureNameMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.FixtureNameProperty.Name);
                case LocationSpaceBayImportMappingList.ManufacturerNameMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.ManufacturerProperty.Name);
                case LocationSpaceBayImportMappingList.BayTypeMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BayTypeProperty.Name);
                case LocationSpaceBayImportMappingList.FixtureTypeMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.FixtureTypeProperty.Name);
                case LocationSpaceBayImportMappingList.BayTypePostFixMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BayTypePostFixProperty.Name);
                case LocationSpaceBayImportMappingList.BayTypeCalculationMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BayTypeCalculationProperty.Name);
                case LocationSpaceBayImportMappingList.NotchPitchMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.NotchPitchProperty.Name);
                case LocationSpaceBayImportMappingList.BarcodeMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.BarcodeProperty.Name);
                case LocationSpaceBayImportMappingList.ColourMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.ColourProperty.Name);
                case LocationSpaceBayImportMappingList.AssetNumberMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.AssetProperty.Name);
                case LocationSpaceBayImportMappingList.TemperatureMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.TemperatureProperty.Name);
                case LocationSpaceBayImportMappingList.IsPromotionalMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.IsPromotionalProperty.Name);
                case LocationSpaceBayImportMappingList.HasPowerMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.HasPowerProperty.Name);
                case LocationSpaceBayImportMappingList.NotchStartYMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.NotchStartYProperty.Name);
                case LocationSpaceBayImportMappingList.FixtureShapeMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.FixtureShapeTypeProperty.Name);
                #endregion

                default: throw new NotImplementedException();
            }
        }

        public static Boolean IsUOMDisplayConversionRequired(Int32 mappingId)
        {
            switch (mappingId)
            {
                case HeightMappingId:
                case WidthMappingId:
                case DepthMappingId:
                case BaseHeightMappingId:
                case BaseWidthMappingId:
                case BaseDepthMappingId:
                case BayTypeCalculationMappingId:
                case NotchPitchMappingId:
                case NotchStartYMappingId:
                    return true;

                default: return false;
            }
        }

        #endregion

        #region IModelObjectColumnHelper members

        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mapping)
        {
            //does this have any groups?
            return null;
        }

        //Boolean IModelObjectColumnHelper.IsUOMDisplayConversionRequired(Int32 mappingId)
        //{
        //    return LocationSpaceBayImportMappingList.IsUOMDisplayConversionRequired(mappingId);
        //}

        #endregion
    }
}
