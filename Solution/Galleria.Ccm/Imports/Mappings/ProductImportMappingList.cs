﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
// V8-27424 : L.Luong
//  Added FinancialGroupCode and FinancialGroupName
// V8-24264 : A.Probyn
//	Extended for display type on import mapping constructor.
// V8-27641 : A.Probyn
//  Added missing TrayHeight, TrayWidth, TrayDepth & TrayTotalUnit
// V8-28010  : A.Probyn
//  Corrected SetValueByMappingId & GetValueByMappingId for MerchandisingStyle
// V8-26333 : A.Probyn
//  Extended to cover custom attributes on the import
#endregion

#region Version History: (CCM 802)
// V8-29143 : I.George
//	Corrected SetValueByMappingId for OrientationStyle
#endregion

#region Version History: (CCM 8.1.0)
// V8-29867 : N.Haywood
//  Added call to PlanogramProductStatusTypeGetEnum for status type
// V8-29024 : A.Probyn
//  Updated to missing properties 
//      Case Height, Case Width, Case Depth
//      Case High, Case Wide, Case Deep
//      Customer Status, Fill Colour
//      Fill Pattern, Shape, Shape Description
#endregion

#region Version History: (CCM 8.1.1)
// V8-30280 : M.Shelley
//  Add an exception handler around the "Date Effective" parsing.
// V8-30494 : M.Pettit
//  Allowed fillcolour to be negative
#endregion

#region Version History: (CCM 8.2.0)
// V8-30865 : N.Haywood
//  Increased length of some product attributes
#endregion

#region Version History: (CCM 8.3.0)
// V8-31531 : A.Heathcote
// Removed IsTrayProduct
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion

#region Version History: CCM 8.4.0
// V8-19069 : J.Mendes
//  Five new Note field attributes for the Custom Attribute Data
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Globalization;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class ProductImportMappingList : ImportMappingList<ProductImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        public const Int32 ProductGroupIdMapId = 0;
        //public const Int32 ReplacementProductIdMapId = 1;

        public const Int32 GtinMapId = 2;
        public const Int32 PerformanceSourceReferenceCodeMapId = 3;
        public const Int32 GroupCodeMapId = 4;
        public const Int32 NameMapId = 5;
        public const Int32 HeightMapId = 6;
        public const Int32 WidthMapId = 7;
        public const Int32 DepthMapId = 8;
        public const Int32 DisplayHeightMapId = 9;
        public const Int32 DisplayWidthMapId = 10;
        public const Int32 DisplayDepthMapId = 11;
        public const Int32 AlternateHeightMapId = 12;
        public const Int32 AlternateWidthMapId = 13;
        public const Int32 AlternateDepthMapId = 14;

        public const Int32 NumberOfPegHolesMapId = 15;
        public const Int32 PegXMapId = 16;
        public const Int32 PegX2MapId = 17;
        public const Int32 PegX3MapId = 18;
        public const Int32 PegYMapId = 19;
        public const Int32 PegY2MapId = 20;
        public const Int32 PegY3MapId = 21;
        public const Int32 PegProngOffsetXMapId = 22;
        public const Int32 PegDepthMapId = 23;

        public const Int32 SqueezeHeightMapId = 24;
        public const Int32 SqueezeWidthMapId = 25;
        public const Int32 SqueezeDepthMapId = 26;

        public const Int32 NestingHeightMapId = 27;
        public const Int32 NestingWidthMapId = 28;
        public const Int32 NestingDepthMapId = 29;

        public const Int32 CasePackUnitsMapId = 30;
        public const Int32 MaxStackMapId = 31;
        public const Int32 MaxTopCapMapId = 32;
        public const Int32 MaxRightCapMapId = 33;
        public const Int32 MinDeepMapId = 34;
        public const Int32 MaxDeepMapId = 35;

        public const Int32 TrayHighMapId = 38;
        public const Int32 TrayWideMapId = 39;
        public const Int32 TrayDeepMapId = 40;
        public const Int32 TrayThickHeightMapId = 41;
        public const Int32 TrayThickWidthMapId = 42;
        public const Int32 TrayThickDepthMapId = 43;

        public const Int32 FrontOverhangMapId = 44;
        public const Int32 FingerSpaceAboveMapId = 45;
        public const Int32 FingerSpaceToTheSideMapId = 46;

        public const Int32 StatusTypeMapId = 47;
        public const Int32 OrientationTypeMapId = 48;
        public const Int32 MerchandisingStyleMapId = 49;

        public const Int32 IsFrontOnlyMapId = 50;
       
        public const Int32 IsPlaceHolderProductMapId = 52;
        public const Int32 IsActiveMapId = 53;
        public const Int32 CanBreakTrayUpMapId = 54;
        public const Int32 CanBreakTrayDownMapId = 55;
        public const Int32 CanBreakTrayBackMapId = 56;
        public const Int32 CanBreakTrayTopMapId = 57;
        public const Int32 ForceMiddleCapMapId = 58;
        public const Int32 ForceBottomCapMapId = 59;
        //GFS-13789 ~ additional Product data
        public const Int32 PointOfPurchaseHeightMapId = 60;
        public const Int32 PointOfPurchaseWidthMapId = 61;
        public const Int32 PointOfPurchaseDepthMapId = 62;


        //product attribute data
        public const Int32 SubCategoryMapId = 63;
        public const Int32 ColourMapId = 64;
        public const Int32 FlavourMapId = 65;
        public const Int32 PackagingShapeMapId = 66;
        public const Int32 PackagingTypeMapId = 67;
        public const Int32 CountryOfOriginMapId = 68;
        public const Int32 CountryOfProcessingMapId = 69;

        public const Int32 ShelfLifeMapId = 70;
        public const Int32 DeliveryFrequencyDaysMapId = 71;
        public const Int32 DeliveryMethodMapId = 72;
        public const Int32 BrandMapId = 73;
        public const Int32 VendorCodeMapId = 74;
        public const Int32 VendorMapId = 75;
        public const Int32 ManufacturerCodeMapId = 76;
        public const Int32 ManufacturerMapId = 77;
        public const Int32 SizeMapId = 78;
        public const Int32 UnitOfMeasureMapId = 79;

        public const Int32 DateIntroducedMapId = 80;
        public const Int32 DateDiscontinuedMapId = 81;
        public const Int32 DateEffectiveMapId = 82;

        public const Int32 HealthMapId = 83;
        public const Int32 CorporateCodeMapId = 84;
        public const Int32 BarCodeMapId = 85;
        public const Int32 SellPriceMapId = 86;
        public const Int32 SellPackCountMapId = 87;
        public const Int32 SellPackDescriptionMapId = 88;
        public const Int32 RecommendedRetailPriceMapId = 89;
        public const Int32 ManufacturerRecommendedRetailPriceMapId = 90;
        public const Int32 CostPriceMapId = 91;
        public const Int32 CaseCostMapId = 92;
        public const Int32 TaxRateMapId = 93;

        public const Int32 ConsumerInformationMapId = 94;
        public const Int32 TextureMapId = 95;
        public const Int32 StyleNumberMapId = 96;
        public const Int32 PatternMapId = 97;
        public const Int32 ModelMapId = 98;
        public const Int32 GarmentTypeMapId = 99;

        public const Int32 IsPrivateLabelMapId = 100;
        public const Int32 IsNewMapId = 101;
        public const Int32 FinancialGroupCodeMapId = 102;
        public const Int32 FinancialGroupNameMapId = 103;
        //GFS-13789 ~ additional Product Attribute Data
        public const Int32 PointOfPurchaseDescriptionMapId = 104;
        public const Int32 ShortDescriptionMapId = 105;

        public const Int32 TrayHeightMapId = 106;
        public const Int32 TrayWidthMapId = 107;
        public const Int32 TrayDepthMapId = 108;
        public const Int32 TrayPackUnitsMapId = 109;

        //custom fields
        public const Int32 CustomText1MapId = 110;
        public const Int32 CustomText2MapId = 111;
        public const Int32 CustomText3MapId = 112;
        public const Int32 CustomText4MapId = 113;
        public const Int32 CustomText5MapId = 114;
        public const Int32 CustomText6MapId = 115;
        public const Int32 CustomText7MapId = 116;
        public const Int32 CustomText8MapId = 117;
        public const Int32 CustomText9MapId = 118;
        public const Int32 CustomText10MapId = 119;
        public const Int32 CustomText11MapId = 120;
        public const Int32 CustomText12MapId = 121;
        public const Int32 CustomText13MapId = 122;
        public const Int32 CustomText14MapId = 123;
        public const Int32 CustomText15MapId = 124;
        public const Int32 CustomText16MapId = 125;
        public const Int32 CustomText17MapId = 126;
        public const Int32 CustomText18MapId = 127;
        public const Int32 CustomText19MapId = 128;
        public const Int32 CustomText20MapId = 129;
        public const Int32 CustomText21MapId = 130;
        public const Int32 CustomText22MapId = 131;
        public const Int32 CustomText23MapId = 132;
        public const Int32 CustomText24MapId = 133;
        public const Int32 CustomText25MapId = 134;
        public const Int32 CustomText26MapId = 135;
        public const Int32 CustomText27MapId = 136;
        public const Int32 CustomText28MapId = 137;
        public const Int32 CustomText29MapId = 138;
        public const Int32 CustomText30MapId = 139;
        public const Int32 CustomText31MapId = 140;
        public const Int32 CustomText32MapId = 141;
        public const Int32 CustomText33MapId = 142;
        public const Int32 CustomText34MapId = 143;
        public const Int32 CustomText35MapId = 144;
        public const Int32 CustomText36MapId = 145;
        public const Int32 CustomText37MapId = 146;
        public const Int32 CustomText38MapId = 147;
        public const Int32 CustomText39MapId = 148;
        public const Int32 CustomText40MapId = 149;
        public const Int32 CustomText41MapId = 150;
        public const Int32 CustomText42MapId = 151;
        public const Int32 CustomText43MapId = 152;
        public const Int32 CustomText44MapId = 153;
        public const Int32 CustomText45MapId = 154;
        public const Int32 CustomText46MapId = 155;
        public const Int32 CustomText47MapId = 156;
        public const Int32 CustomText48MapId = 157;
        public const Int32 CustomText49MapId = 158;
        public const Int32 CustomText50MapId = 159;

        public const Int32 CustomValue1MapId = 160;
        public const Int32 CustomValue2MapId = 161;
        public const Int32 CustomValue3MapId = 162;
        public const Int32 CustomValue4MapId = 163;
        public const Int32 CustomValue5MapId = 164;
        public const Int32 CustomValue6MapId = 165;
        public const Int32 CustomValue7MapId = 166;
        public const Int32 CustomValue8MapId = 167;
        public const Int32 CustomValue9MapId = 168;
        public const Int32 CustomValue10MapId = 169;
        public const Int32 CustomValue11MapId = 170;
        public const Int32 CustomValue12MapId = 171;
        public const Int32 CustomValue13MapId = 172;
        public const Int32 CustomValue14MapId = 173;
        public const Int32 CustomValue15MapId = 174;
        public const Int32 CustomValue16MapId = 175;
        public const Int32 CustomValue17MapId = 176;
        public const Int32 CustomValue18MapId = 177;
        public const Int32 CustomValue19MapId = 178;
        public const Int32 CustomValue20MapId = 179;
        public const Int32 CustomValue21MapId = 180;
        public const Int32 CustomValue22MapId = 181;
        public const Int32 CustomValue23MapId = 182;
        public const Int32 CustomValue24MapId = 183;
        public const Int32 CustomValue25MapId = 184;
        public const Int32 CustomValue26MapId = 185;
        public const Int32 CustomValue27MapId = 186;
        public const Int32 CustomValue28MapId = 187;
        public const Int32 CustomValue29MapId = 188;
        public const Int32 CustomValue30MapId = 189;
        public const Int32 CustomValue31MapId = 190;
        public const Int32 CustomValue32MapId = 191;
        public const Int32 CustomValue33MapId = 192;
        public const Int32 CustomValue34MapId = 193;
        public const Int32 CustomValue35MapId = 194;
        public const Int32 CustomValue36MapId = 195;
        public const Int32 CustomValue37MapId = 196;
        public const Int32 CustomValue38MapId = 197;
        public const Int32 CustomValue39MapId = 198;
        public const Int32 CustomValue40MapId = 199;
        public const Int32 CustomValue41MapId = 200;
        public const Int32 CustomValue42MapId = 201;
        public const Int32 CustomValue43MapId = 202;
        public const Int32 CustomValue44MapId = 203;
        public const Int32 CustomValue45MapId = 204;
        public const Int32 CustomValue46MapId = 205;
        public const Int32 CustomValue47MapId = 206;
        public const Int32 CustomValue48MapId = 207;
        public const Int32 CustomValue49MapId = 208;
        public const Int32 CustomValue50MapId = 209;

        public const Int32 CustomFlag1MapId = 210;
        public const Int32 CustomFlag2MapId = 211;
        public const Int32 CustomFlag3MapId = 212;
        public const Int32 CustomFlag4MapId = 213;
        public const Int32 CustomFlag5MapId = 214;
        public const Int32 CustomFlag6MapId = 215;
        public const Int32 CustomFlag7MapId = 216;
        public const Int32 CustomFlag8MapId = 217;
        public const Int32 CustomFlag9MapId = 218;
        public const Int32 CustomFlag10MapId = 219;

        public const Int32 CustomDate1MapId = 220;
        public const Int32 CustomDate2MapId = 221;
        public const Int32 CustomDate3MapId = 222;
        public const Int32 CustomDate4MapId = 223;
        public const Int32 CustomDate5MapId = 224;
        public const Int32 CustomDate6MapId = 225;
        public const Int32 CustomDate7MapId = 226;
        public const Int32 CustomDate8MapId = 227;
        public const Int32 CustomDate9MapId = 228;
        public const Int32 CustomDate10MapId = 229;

        //V8-29024 - Missing fields 
        public const Int32 CaseHeightMapId = 230;
        public const Int32 CaseWidthMapId = 231;
        public const Int32 CaseDepthMapId = 232;
        public const Int32 CaseHighMapId = 233;
        public const Int32 CaseWideMapId = 234;
        public const Int32 CaseDeepMapId = 235;
        public const Int32 CustomerStatusMapId = 236;
        public const Int32 FillColourMapId = 237;
        public const Int32 FillPatternTypeMapId = 238;
        public const Int32 ShapeTypeMapId = 239;
        public const Int32 ShapeMapId = 240;

        public const Int32 CustomNote1MapId = 241;
        public const Int32 CustomNote2MapId = 242;
        public const Int32 CustomNote3MapId = 243;
        public const Int32 CustomNote4MapId = 244;
        public const Int32 CustomNote5MapId = 245;

        #endregion

        #region Constructors
        private ProductImportMappingList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static ProductImportMappingList NewProductImportMappingList(Boolean isMappingList)
        {
            ProductImportMappingList list = new ProductImportMappingList();
            list.Create(isMappingList);
            return list;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private void Create(Boolean isMappingList)
        {
            this.RaiseListChangedEvents = false;

            #region Product Properties
            //this.Add(ImportMapping.NewImportMapping(ProductGroupIdMapId, 0);
            //this.Add(ImportMapping.NewImportMapping(ReplacementProductIdMapId, Product.ReplacementProductIdProperty.FriendlyName;

            this.Add(ImportMapping.NewImportMapping(GtinMapId, Product.GtinProperty.FriendlyName, Product.GtinProperty.Description, /*mandarory*/true, /*uniqueIdentifier*/true, Product.GtinProperty.Type, Product.GtinProperty.DefaultValue, 1, 14, /*canDefault*/false, Product.GtinProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(NameMapId, Product.NameProperty.FriendlyName, Product.NameProperty.Description, /*mandarory*/true, Product.NameProperty.Type, Product.NameProperty.DefaultValue, 1, 100, /*canDefault*/false, Product.NameProperty.DisplayType));
            if (isMappingList) //[14657] Not required for View/Edit
            {
                this.Add(ImportMapping.NewImportMapping(GroupCodeMapId, String.Format(Message.ProductImportList_ProductGroup, ProductGroup.CodeProperty.FriendlyName), Message.ProductImportMappingList_ProductGroupCode_Notes, false, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 0, 50, true, ProductGroup.CodeProperty.DisplayType));
            }
            this.Add(ImportMapping.NewImportMapping(HeightMapId, Product.HeightProperty.FriendlyName, Product.HeightProperty.Description, false, Product.HeightProperty.Type, Product.HeightProperty.DefaultValue, 0, Int32.MaxValue, true, Product.HeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(WidthMapId, Product.WidthProperty.FriendlyName, Product.WidthProperty.Description, false, Product.WidthProperty.Type, Product.WidthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.WidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DepthMapId, Product.DepthProperty.FriendlyName, Product.DepthProperty.Description, false, Product.DepthProperty.Type, Product.DepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.DepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DisplayHeightMapId, Product.DisplayHeightProperty.FriendlyName, Product.DisplayHeightProperty.Description, false, Product.DisplayHeightProperty.Type, Product.DisplayHeightProperty.DefaultValue, 0, Int32.MaxValue, true, Product.DisplayHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DisplayWidthMapId, Product.DisplayWidthProperty.FriendlyName, Product.DisplayWidthProperty.Description, false, Product.DisplayWidthProperty.Type, Product.DisplayWidthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.DisplayWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DisplayDepthMapId, Product.DisplayDepthProperty.FriendlyName, Product.DisplayDepthProperty.Description, false, Product.DisplayDepthProperty.Type, Product.DisplayDepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.DisplayDepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(AlternateHeightMapId, Product.AlternateHeightProperty.FriendlyName, Product.AlternateHeightProperty.Description, false, Product.AlternateHeightProperty.Type, Product.AlternateHeightProperty.DefaultValue, 0, Int32.MaxValue, true, Product.AlternateHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(AlternateWidthMapId, Product.AlternateWidthProperty.FriendlyName, Product.AlternateWidthProperty.Description, false, Product.AlternateWidthProperty.Type, Product.AlternateWidthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.AlternateWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(AlternateDepthMapId, Product.AlternateDepthProperty.FriendlyName, Product.AlternateDepthProperty.Description, false, Product.AlternateDepthProperty.Type, Product.AlternateDepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.AlternateDepthProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(NumberOfPegHolesMapId, Product.NumberOfPegHolesProperty.FriendlyName, Product.NumberOfPegHolesProperty.Description, false, Product.NumberOfPegHolesProperty.Type, Product.NumberOfPegHolesProperty.DefaultValue, 0, Byte.MaxValue, true, Product.NumberOfPegHolesProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PegXMapId, Product.PegXProperty.FriendlyName, Product.PegXProperty.Description, false, Product.PegXProperty.Type, Product.PegXProperty.DefaultValue, 0, Int32.MaxValue, true, Product.PegXProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PegX2MapId, Product.PegX2Property.FriendlyName, Product.PegX2Property.Description, false, Product.PegX2Property.Type, Product.PegX2Property.DefaultValue, 0, Int32.MaxValue, true, Product.PegX2Property.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PegX3MapId, Product.PegX3Property.FriendlyName, Product.PegX3Property.Description, false, Product.PegX3Property.Type, Product.PegX3Property.DefaultValue, 0, Int32.MaxValue, true, Product.PegX3Property.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PegYMapId, Product.PegYProperty.FriendlyName, Product.PegYProperty.Description, false, Product.PegYProperty.Type, Product.PegYProperty.DefaultValue, 0, Int32.MaxValue, true, Product.PegYProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PegY2MapId, Product.PegY2Property.FriendlyName, Product.PegY2Property.Description, false, Product.PegY2Property.Type, Product.PegY2Property.DefaultValue, 0, Int32.MaxValue, true, Product.PegY2Property.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PegY3MapId, Product.PegY3Property.FriendlyName, Product.PegY3Property.Description, false, Product.PegY3Property.Type, Product.PegY3Property.DefaultValue, 0, Int32.MaxValue, true, Product.PegY3Property.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PegProngOffsetXMapId, Product.PegProngOffsetXProperty.FriendlyName, Product.PegProngOffsetXProperty.Description, false, Product.PegProngOffsetXProperty.Type, Product.PegProngOffsetXProperty.DefaultValue, 0, Int32.MaxValue, true, Product.PegProngOffsetXProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PegDepthMapId, Product.PegDepthProperty.FriendlyName, Product.PegDepthProperty.Description, false, Product.PegDepthProperty.Type, Product.PegDepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.PegDepthProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(SqueezeHeightMapId, Product.SqueezeHeightProperty.FriendlyName, Product.SqueezeHeightProperty.Description, false, Product.SqueezeHeightProperty.Type, Product.SqueezeHeightProperty.DefaultValue, 0, 1, true, Product.SqueezeHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SqueezeWidthMapId, Product.SqueezeWidthProperty.FriendlyName, Product.SqueezeWidthProperty.Description, false, Product.SqueezeWidthProperty.Type, Product.SqueezeWidthProperty.DefaultValue, 0, 1, true, Product.SqueezeWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SqueezeDepthMapId, Product.SqueezeDepthProperty.FriendlyName, Product.SqueezeDepthProperty.Description, false, Product.SqueezeDepthProperty.Type, Product.SqueezeDepthProperty.DefaultValue, 0, 1, true, Product.SqueezeDepthProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(NestingHeightMapId, Product.NestingHeightProperty.FriendlyName, Product.NestingHeightProperty.Description, false, Product.NestingHeightProperty.Type, Product.NestingHeightProperty.DefaultValue, 0, Int32.MaxValue, true, Product.NestingHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(NestingWidthMapId, Product.NestingWidthProperty.FriendlyName, Product.NestingWidthProperty.Description, false, Product.NestingWidthProperty.Type, Product.NestingWidthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.NestingWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(NestingDepthMapId, Product.NestingDepthProperty.FriendlyName, Product.NestingDepthProperty.Description, false, Product.NestingDepthProperty.Type, Product.NestingDepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.NestingDepthProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(CasePackUnitsMapId, Product.CasePackUnitsProperty.FriendlyName, Product.CasePackUnitsProperty.Description, false, Product.CasePackUnitsProperty.Type, Product.CasePackUnitsProperty.DefaultValue, 0, Int16.MaxValue, true, Product.CasePackUnitsProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseHeightMapId, Product.CaseHeightProperty.FriendlyName, Product.CaseHeightProperty.Description, false, Product.CaseHeightProperty.Type, Product.CaseHeightProperty.DefaultValue, 0, Int32.MaxValue, true, Product.CaseHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseWidthMapId, Product.CaseWidthProperty.FriendlyName, Product.CaseWidthProperty.Description, false, Product.CaseWidthProperty.Type, Product.CaseWidthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.CaseWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseDepthMapId, Product.CaseDepthProperty.FriendlyName, Product.CaseDepthProperty.Description, false, Product.CaseDepthProperty.Type, Product.CaseDepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.CaseDepthProperty.DisplayType));
           
            this.Add(ImportMapping.NewImportMapping(CaseHighMapId, Product.CaseHighProperty.FriendlyName, Product.CaseHighProperty.Description, false, Product.CaseHighProperty.Type, Product.CaseHighProperty.DefaultValue, 0, Byte.MaxValue, true, Product.CaseHighProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseWideMapId, Product.CaseWideProperty.FriendlyName, Product.CaseWideProperty.Description, false, Product.CaseWideProperty.Type, Product.CaseWideProperty.DefaultValue, 0, Byte.MaxValue, true, Product.CaseWideProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseDeepMapId, Product.CaseDeepProperty.FriendlyName, Product.CaseDeepProperty.Description, false, Product.CaseDeepProperty.Type, Product.CaseDeepProperty.DefaultValue, 0, Byte.MaxValue, true, Product.CaseDeepProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(CustomerStatusMapId, Product.CustomerStatusProperty.FriendlyName, Product.CustomerStatusProperty.Description, false, Product.CustomerStatusProperty.Type, Product.CustomerStatusProperty.DefaultValue, 0, 50, true, Product.CustomerStatusProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FillColourMapId, Product.FillColourProperty.FriendlyName, Product.FillColourProperty.Description, false, Product.FillColourProperty.Type, Product.FillColourProperty.DefaultValue, Int32.MinValue, Int32.MaxValue, true, Product.FillColourProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FillPatternTypeMapId, Product.FillPatternTypeProperty.FriendlyName, Product.FillPatternTypeProperty.Description, false, Product.FillPatternTypeProperty.Type, (Byte)Product.FillPatternTypeProperty.DefaultValue, 0, Byte.MaxValue, true, Product.FillPatternTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ShapeTypeMapId, Product.ShapeTypeProperty.FriendlyName, Product.ShapeTypeProperty.Description, false, Product.ShapeTypeProperty.Type, (Byte)Product.ShapeTypeProperty.DefaultValue, 0, Byte.MaxValue, true, Product.ShapeTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ShapeMapId, Product.ShapeProperty.FriendlyName, Product.ShapeProperty.Description, false, Product.ShapeProperty.Type, Product.ShapeProperty.DefaultValue, 0, 20, true, Product.ShapeProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(MaxStackMapId, Product.MaxStackProperty.FriendlyName, Product.MaxStackProperty.Description, false, Product.MaxStackProperty.Type, Product.MaxStackProperty.DefaultValue, 0, Byte.MaxValue, true, Product.MaxStackProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(MaxTopCapMapId, Product.MaxTopCapProperty.FriendlyName, Product.MaxTopCapProperty.Description, false, Product.MaxTopCapProperty.Type, Product.MaxTopCapProperty.DefaultValue, 0, Byte.MaxValue, true, Product.MaxTopCapProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(MaxRightCapMapId, Product.MaxRightCapProperty.FriendlyName, Product.MaxRightCapProperty.Description, false, Product.MaxRightCapProperty.Type, Product.MaxRightCapProperty.DefaultValue, 0, Byte.MaxValue, true, Product.MaxRightCapProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(MinDeepMapId, Product.MinDeepProperty.FriendlyName, Product.MinDeepProperty.Description, false, Product.MinDeepProperty.Type, Product.MinDeepProperty.DefaultValue, 0, Byte.MaxValue, true, Product.MinDeepProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(MaxDeepMapId, Product.MaxDeepProperty.FriendlyName, Product.MaxDeepProperty.Description, false, Product.MaxDeepProperty.Type, Product.MaxDeepProperty.DefaultValue, 0, Byte.MaxValue, true, Product.MaxDeepProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(TrayHighMapId, Product.TrayHighProperty.FriendlyName, Product.TrayHighProperty.Description, false, Product.TrayHighProperty.Type, Product.TrayHighProperty.DefaultValue, 0, Byte.MaxValue, true, Product.TrayHighProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayWideMapId, Product.TrayWideProperty.FriendlyName, Product.TrayWideProperty.Description, false, Product.TrayWideProperty.Type, Product.TrayWideProperty.DefaultValue, 0, Byte.MaxValue, true, Product.TrayWideProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayDeepMapId, Product.TrayDeepProperty.FriendlyName, Product.TrayDeepProperty.Description, false, Product.TrayDeepProperty.Type, Product.TrayDeepProperty.DefaultValue, 0, Byte.MaxValue, true, Product.TrayDeepProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayThickHeightMapId, Product.TrayThickHeightProperty.FriendlyName, Product.TrayThickHeightProperty.Description, false, Product.TrayThickHeightProperty.Type, Product.TrayThickHeightProperty.DefaultValue, 0, Int32.MaxValue, true, Product.TrayThickHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayThickWidthMapId, Product.TrayThickWidthProperty.FriendlyName, Product.TrayThickWidthProperty.Description, false, Product.TrayThickWidthProperty.Type, Product.TrayThickWidthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.TrayThickWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayThickDepthMapId, Product.TrayThickDepthProperty.FriendlyName, Product.TrayThickDepthProperty.Description, false, Product.TrayThickDepthProperty.Type, Product.TrayThickDepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.TrayThickDepthProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(FrontOverhangMapId, Product.FrontOverhangProperty.FriendlyName, Product.FrontOverhangProperty.Description, false, Product.FrontOverhangProperty.Type, Product.FrontOverhangProperty.DefaultValue, 0, Int32.MaxValue, true, Product.FrontOverhangProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FingerSpaceAboveMapId, Product.FingerSpaceAboveProperty.FriendlyName, Product.FingerSpaceAboveProperty.Description, false, Product.FingerSpaceAboveProperty.Type, Product.FingerSpaceAboveProperty.DefaultValue, 0, Int32.MaxValue, true, Product.FingerSpaceAboveProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FingerSpaceToTheSideMapId, Product.FingerSpaceToTheSideProperty.FriendlyName, Product.FingerSpaceToTheSideProperty.Description, false, Product.FingerSpaceToTheSideProperty.Type, Product.FingerSpaceToTheSideProperty.DefaultValue, 0, Int32.MaxValue, true, Product.FingerSpaceToTheSideProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(StatusTypeMapId, Product.StatusTypeProperty.FriendlyName, Product.StatusTypeProperty.Description, false, Product.StatusTypeProperty.Type, (Byte)Product.StatusTypeProperty.DefaultValue, 0, Byte.MaxValue, true, Product.StatusTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(OrientationTypeMapId, Product.OrientationTypeProperty.FriendlyName, Product.OrientationTypeProperty.Description, false, Product.OrientationTypeProperty.Type, (Byte)Product.OrientationTypeProperty.DefaultValue, 0, Byte.MaxValue, true, Product.OrientationTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(MerchandisingStyleMapId, Product.MerchandisingStyleProperty.FriendlyName, Product.MerchandisingStyleProperty.Description, false, Product.MerchandisingStyleProperty.Type, (Byte)Product.MerchandisingStyleProperty.DefaultValue, 0, Byte.MaxValue, true, Product.MerchandisingStyleProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(IsFrontOnlyMapId, Product.IsFrontOnlyProperty.FriendlyName, Product.IsFrontOnlyProperty.Description, false, Product.IsFrontOnlyProperty.Type, Product.IsFrontOnlyProperty.DefaultValue, 0, 1, true, Product.IsFrontOnlyProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsPlaceHolderProductMapId, Product.IsPlaceHolderProductProperty.FriendlyName, Product.IsPlaceHolderProductProperty.Description, false, Product.IsPlaceHolderProductProperty.Type, Product.IsPlaceHolderProductProperty.DefaultValue, 0, 1, true, Product.IsPlaceHolderProductProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsActiveMapId, Product.IsActiveProperty.FriendlyName, Product.IsActiveProperty.Description, false, Product.IsActiveProperty.Type, Product.IsActiveProperty.DefaultValue, 0, 1, true, Product.IsActiveProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CanBreakTrayUpMapId, Product.CanBreakTrayUpProperty.FriendlyName, Product.CanBreakTrayUpProperty.Description, false, Product.CanBreakTrayUpProperty.Type, Product.CanBreakTrayUpProperty.DefaultValue, 0, 1, true, Product.CanBreakTrayUpProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CanBreakTrayDownMapId, Product.CanBreakTrayDownProperty.FriendlyName, Product.CanBreakTrayDownProperty.Description, false, Product.CanBreakTrayDownProperty.Type, Product.CanBreakTrayDownProperty.DefaultValue, 0, 1, true, Product.CanBreakTrayDownProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CanBreakTrayBackMapId, Product.CanBreakTrayBackProperty.FriendlyName, Product.CanBreakTrayBackProperty.Description, false, Product.CanBreakTrayBackProperty.Type, Product.CanBreakTrayBackProperty.DefaultValue, 0, 1, true, Product.CanBreakTrayBackProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CanBreakTrayTopMapId, Product.CanBreakTrayTopProperty.FriendlyName, Product.CanBreakTrayTopProperty.Description, false, Product.CanBreakTrayTopProperty.Type, Product.CanBreakTrayTopProperty.DefaultValue, 0, 1, true, Product.CanBreakTrayTopProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ForceMiddleCapMapId, Product.ForceMiddleCapProperty.FriendlyName, Product.ForceMiddleCapProperty.Description, false, Product.ForceMiddleCapProperty.Type, Product.ForceMiddleCapProperty.DefaultValue, 0, 1, true, Product.ForceMiddleCapProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ForceBottomCapMapId, Product.ForceBottomCapProperty.FriendlyName, Product.ForceBottomCapProperty.Description, false, Product.ForceBottomCapProperty.Type, Product.ForceBottomCapProperty.DefaultValue, 0, 1, true, Product.ForceBottomCapProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(PointOfPurchaseHeightMapId, Product.PointOfPurchaseHeightProperty.FriendlyName, Product.PointOfPurchaseHeightProperty.Description, false, Product.PointOfPurchaseHeightProperty.Type, Product.PointOfPurchaseHeightProperty.DefaultValue, 0, Int32.MaxValue, true, Product.PointOfPurchaseHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PointOfPurchaseWidthMapId, Product.PointOfPurchaseWidthProperty.FriendlyName, Product.PointOfPurchaseWidthProperty.Description, false, Product.PointOfPurchaseWidthProperty.Type, Product.PointOfPurchaseWidthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.PointOfPurchaseWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PointOfPurchaseDepthMapId, Product.PointOfPurchaseDepthProperty.FriendlyName, Product.PointOfPurchaseDepthProperty.Description, false, Product.PointOfPurchaseDepthProperty.Type, Product.PointOfPurchaseDepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.PointOfPurchaseDepthProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(TrayHeightMapId, Product.TrayHeightProperty.FriendlyName, Product.TrayHeightProperty.Description, false, Product.TrayHeightProperty.Type, Product.TrayHeightProperty.DefaultValue, 0, Int32.MaxValue, true, Product.TrayHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayWidthMapId, Product.TrayWidthProperty.FriendlyName, Product.TrayWidthProperty.Description, false, Product.TrayWidthProperty.Type, Product.TrayWidthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.TrayWidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayDepthMapId, Product.TrayDepthProperty.FriendlyName, Product.TrayDepthProperty.Description, false, Product.TrayDepthProperty.Type, Product.TrayDepthProperty.DefaultValue, 0, Int32.MaxValue, true, Product.TrayDepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TrayPackUnitsMapId, Product.TrayPackUnitsProperty.FriendlyName, Product.TrayPackUnitsProperty.Description, false, Product.TrayPackUnitsProperty.Type, Product.TrayPackUnitsProperty.DefaultValue, 0, Int16.MaxValue, true, Product.TrayPackUnitsProperty.DisplayType));
            
            #endregion

            #region product attribute data properties
            this.Add(ImportMapping.NewImportMapping(SubCategoryMapId, Product.SubcategoryProperty.FriendlyName, Product.SubcategoryProperty.Description, false, Product.SubcategoryProperty.Type, Product.SubcategoryProperty.DefaultValue, 1, 100, true, Product.SubcategoryProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ColourMapId, Product.ColourProperty.FriendlyName, Product.ColourProperty.Description, false, Product.ColourProperty.Type, Product.ColourProperty.DefaultValue, 1, 50, true, Product.ColourProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FlavourMapId, Product.FlavourProperty.FriendlyName, Product.FlavourProperty.Description, false, Product.FlavourProperty.Type, Product.FlavourProperty.DefaultValue, 1, 100, true, Product.FlavourProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PackagingShapeMapId, Product.PackagingShapeProperty.FriendlyName, Product.PackagingShapeProperty.Description, false, Product.PackagingShapeProperty.Type, Product.PackagingShapeProperty.DefaultValue, 1, 50, true, Product.PackagingShapeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PackagingTypeMapId, Product.PackagingTypeProperty.FriendlyName, Product.PackagingTypeProperty.Description, false, Product.PackagingTypeProperty.Type, Product.PackagingTypeProperty.DefaultValue, 1, 50, true, Product.PackagingTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CountryOfOriginMapId, Product.CountryOfOriginProperty.FriendlyName, Product.CountryOfOriginProperty.Description, false, Product.CountryOfOriginProperty.Type, Product.CountryOfOriginProperty.DefaultValue, 1, 50, true, Product.CountryOfOriginProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CountryOfProcessingMapId, Product.CountryOfProcessingProperty.FriendlyName, Product.CountryOfProcessingProperty.Description, false, Product.CountryOfProcessingProperty.Type, Product.CountryOfProcessingProperty.DefaultValue, 1, 50, true, Product.CountryOfProcessingProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(ShelfLifeMapId, Product.ShelfLifeProperty.FriendlyName, Product.ShelfLifeProperty.Description, false, Product.ShelfLifeProperty.Type, Product.ShelfLifeProperty.DefaultValue, 0, Int16.MaxValue, true, Product.ShelfLifeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DeliveryFrequencyDaysMapId, Product.DeliveryFrequencyDaysProperty.FriendlyName, Product.DeliveryFrequencyDaysProperty.Description, false, Product.DeliveryFrequencyDaysProperty.Type, Product.DeliveryFrequencyDaysProperty.DefaultValue, 0, Int16.MaxValue, true, Product.DeliveryFrequencyDaysProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DeliveryMethodMapId, Product.DeliveryMethodProperty.FriendlyName, Product.DeliveryMethodProperty.Description, false, Product.DeliveryMethodProperty.Type, Product.DeliveryMethodProperty.DefaultValue, 1, 50, true, Product.DeliveryMethodProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BrandMapId, Product.BrandProperty.FriendlyName, Product.BrandProperty.Description, false, Product.BrandProperty.Type, Product.BrandProperty.DefaultValue, 1, 100, true, Product.BrandProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(VendorCodeMapId, Product.VendorCodeProperty.FriendlyName, Product.VendorCodeProperty.Description, false, Product.VendorCodeProperty.Type, Product.VendorCodeProperty.DefaultValue, 1, 50, true, Product.VendorCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(VendorMapId, Product.VendorProperty.FriendlyName, Product.VendorProperty.Description, false, Product.VendorProperty.Type, Product.VendorProperty.DefaultValue, 1, 100, true, Product.VendorProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ManufacturerCodeMapId, Product.ManufacturerCodeProperty.FriendlyName, Product.ManufacturerCodeProperty.Description, false, Product.ManufacturerCodeProperty.Type, Product.ManufacturerCodeProperty.DefaultValue, 1, 50, true, Product.ManufacturerCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ManufacturerMapId, Product.ManufacturerProperty.FriendlyName, Product.ManufacturerProperty.Description, false, Product.ManufacturerProperty.Type, Product.ManufacturerProperty.DefaultValue, 1, 100, true, Product.ManufacturerProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SizeMapId, Product.SizeProperty.FriendlyName, Product.SizeProperty.Description, false, Product.SizeProperty.Type, Product.SizeProperty.DefaultValue, 1, 50, true, Product.SizeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(UnitOfMeasureMapId, Product.UnitOfMeasureProperty.FriendlyName, Product.UnitOfMeasureProperty.Description, false, Product.UnitOfMeasureProperty.Type, Product.UnitOfMeasureProperty.DefaultValue, 1, 50, true, Product.UnitOfMeasureProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(DateIntroducedMapId, Product.DateIntroducedProperty.FriendlyName, Product.DateIntroducedProperty.Description, false, Product.DateIntroducedProperty.Type, Product.DateIntroducedProperty.DefaultValue, 0, 0, true, Product.DateIntroducedProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DateDiscontinuedMapId, Product.DateDiscontinuedProperty.FriendlyName, Product.DateDiscontinuedProperty.Description, false, Product.DateDiscontinuedProperty.Type, Product.DateDiscontinuedProperty.DefaultValue, 0, 0, true, Product.DateDiscontinuedProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DateEffectiveMapId, Product.DateEffectiveProperty.FriendlyName, Product.DateEffectiveProperty.Description, false, Product.DateEffectiveProperty.Type, Product.DateEffectiveProperty.DefaultValue, 0, 0, true, Product.DateEffectiveProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(HealthMapId, Product.HealthProperty.FriendlyName, Product.HealthProperty.Description, false, Product.HealthProperty.Type, Product.HealthProperty.DefaultValue, 1, 50, true, Product.HealthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CorporateCodeMapId, Product.CorporateCodeProperty.FriendlyName, Product.CorporateCodeProperty.Description, false, Product.CorporateCodeProperty.Type, Product.CorporateCodeProperty.DefaultValue, 1, 50, true, Product.CorporateCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BarCodeMapId, Product.BarcodeProperty.FriendlyName, Product.BarcodeProperty.Description, false, Product.BarcodeProperty.Type, Product.BarcodeProperty.DefaultValue, 1, 50, true, Product.BarcodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SellPriceMapId, Product.SellPriceProperty.FriendlyName, Product.SellPriceProperty.Description, false, Product.SellPriceProperty.Type, Product.SellPriceProperty.DefaultValue, 0, Int32.MaxValue, true, Product.SellPriceProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SellPackCountMapId, Product.SellPackCountProperty.FriendlyName, Product.SellPackCountProperty.Description, false, Product.SellPackCountProperty.Type, Product.SellPackCountProperty.DefaultValue, 0, Int16.MaxValue, true, Product.SellPackCountProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SellPackDescriptionMapId, Product.SellPackDescriptionProperty.FriendlyName, Product.SellPackDescriptionProperty.Description, false, Product.SellPackDescriptionProperty.Type, Product.SellPackDescriptionProperty.DefaultValue, 1, 50, true, Product.SellPackDescriptionProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(RecommendedRetailPriceMapId, Product.RecommendedRetailPriceProperty.FriendlyName, Product.RecommendedRetailPriceProperty.Description, false, Product.RecommendedRetailPriceProperty.Type, Product.RecommendedRetailPriceProperty.DefaultValue, 0, Int32.MaxValue, true, Product.RecommendedRetailPriceProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ManufacturerRecommendedRetailPriceMapId, Product.ManufacturerRecommendedRetailPriceProperty.FriendlyName, Product.ManufacturerRecommendedRetailPriceProperty.Description, false, Product.ManufacturerRecommendedRetailPriceProperty.Type, Product.ManufacturerRecommendedRetailPriceProperty.DefaultValue, 0, Int32.MaxValue, true, Product.ManufacturerRecommendedRetailPriceProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CostPriceMapId, Product.CostPriceProperty.FriendlyName, Product.CostPriceProperty.Description, false, Product.CostPriceProperty.Type, Product.CostPriceProperty.DefaultValue, 0, Int32.MaxValue, true, Product.CostPriceProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CaseCostMapId, Product.CaseCostProperty.FriendlyName, Product.CaseCostProperty.Description, false, Product.CaseCostProperty.Type, Product.CaseCostProperty.DefaultValue, 0, Int32.MaxValue, true, Product.CaseCostProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TaxRateMapId, Product.TaxRateProperty.FriendlyName, Product.TaxRateProperty.Description, false, Product.TaxRateProperty.Type, Product.TaxRateProperty.DefaultValue, 0, Int32.MaxValue, true, Product.TaxRateProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(ConsumerInformationMapId, Product.ConsumerInformationProperty.FriendlyName, Product.ConsumerInformationProperty.Description, false, Product.ConsumerInformationProperty.Type, Product.ConsumerInformationProperty.DefaultValue, 1, 100, true, Product.ConsumerInformationProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TextureMapId, Product.TextureProperty.FriendlyName, Product.TextureProperty.Description, false, Product.TextureProperty.Type, Product.TextureProperty.DefaultValue, 1, 100, true, Product.TextureProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(StyleNumberMapId, Product.StyleNumberProperty.FriendlyName, Product.StyleNumberProperty.Description, false, Product.StyleNumberProperty.Type, Product.StyleNumberProperty.DefaultValue, 0, Int16.MaxValue, true, Product.StyleNumberProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PatternMapId, Product.PatternProperty.FriendlyName, Product.PatternProperty.Description, false, Product.PatternProperty.Type, Product.PatternProperty.DefaultValue, 1, 100, true, Product.PatternProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ModelMapId, Product.ModelProperty.FriendlyName, Product.ModelProperty.Description, false, Product.ModelProperty.Type, Product.ModelProperty.DefaultValue, 1, 100, true, Product.ModelProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(GarmentTypeMapId, Product.GarmentTypeProperty.FriendlyName, Product.GarmentTypeProperty.Description, false, Product.GarmentTypeProperty.Type, Product.GarmentTypeProperty.DefaultValue, 1, 100, true, Product.GarmentTypeProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(IsPrivateLabelMapId, Product.IsPrivateLabelProperty.FriendlyName, Product.IsPrivateLabelProperty.Description, false, Product.IsPrivateLabelProperty.Type, Product.IsPrivateLabelProperty.DefaultValue, 0, 1, true, Product.IsPrivateLabelProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsNewMapId, Product.IsNewProductProperty.FriendlyName, Product.IsNewProductProperty.Description, false, Product.IsNewProductProperty.Type, Product.IsNewProductProperty.DefaultValue, 0, 1, true, Product.IsNewProductProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FinancialGroupCodeMapId, Product.FinancialGroupCodeProperty.FriendlyName, Product.FinancialGroupCodeProperty.Description, false, Product.FinancialGroupCodeProperty.Type, Product.FinancialGroupCodeProperty.DefaultValue, 1, 50, true, Product.FinancialGroupCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FinancialGroupNameMapId, Product.FinancialGroupNameProperty.FriendlyName, Product.FinancialGroupNameProperty.Description, false, Product.FinancialGroupNameProperty.Type, Product.FinancialGroupNameProperty.DefaultValue, 1, 100, true, Product.FinancialGroupNameProperty.DisplayType));

            this.Add(ImportMapping.NewImportMapping(PointOfPurchaseDescriptionMapId, Product.PointOfPurchaseDescriptionProperty.FriendlyName, Product.PointOfPurchaseDescriptionProperty.Description, false, Product.PointOfPurchaseDescriptionProperty.Type, Product.PointOfPurchaseDescriptionProperty.DefaultValue, 0, 50, true, Product.PointOfPurchaseDescriptionProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ShortDescriptionMapId, Product.ShortDescriptionProperty.FriendlyName, Product.ShortDescriptionProperty.Description, false, Product.ShortDescriptionProperty.Type, Product.ShortDescriptionProperty.DefaultValue, 0, 50, true, Product.ShortDescriptionProperty.DisplayType));
            #endregion

            #region Product Custom Attribute Properties

            //custom text fields
            this.Add(ImportMapping.NewImportMapping(CustomText1MapId, CustomAttributeData.Text1Property.FriendlyName, CustomAttributeData.Text1Property.Description, false, CustomAttributeData.Text1Property.Type, CustomAttributeData.Text1Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText2MapId, CustomAttributeData.Text2Property.FriendlyName, CustomAttributeData.Text2Property.Description, false, CustomAttributeData.Text2Property.Type, CustomAttributeData.Text2Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText3MapId, CustomAttributeData.Text3Property.FriendlyName, CustomAttributeData.Text3Property.Description, false, CustomAttributeData.Text3Property.Type, CustomAttributeData.Text3Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText4MapId, CustomAttributeData.Text4Property.FriendlyName, CustomAttributeData.Text4Property.Description, false, CustomAttributeData.Text4Property.Type, CustomAttributeData.Text4Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText5MapId, CustomAttributeData.Text5Property.FriendlyName, CustomAttributeData.Text5Property.Description, false, CustomAttributeData.Text5Property.Type, CustomAttributeData.Text5Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText6MapId, CustomAttributeData.Text6Property.FriendlyName, CustomAttributeData.Text6Property.Description, false, CustomAttributeData.Text6Property.Type, CustomAttributeData.Text6Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText7MapId, CustomAttributeData.Text7Property.FriendlyName, CustomAttributeData.Text7Property.Description, false, CustomAttributeData.Text7Property.Type, CustomAttributeData.Text7Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText8MapId, CustomAttributeData.Text8Property.FriendlyName, CustomAttributeData.Text8Property.Description, false, CustomAttributeData.Text8Property.Type, CustomAttributeData.Text8Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText9MapId, CustomAttributeData.Text9Property.FriendlyName, CustomAttributeData.Text9Property.Description, false, CustomAttributeData.Text9Property.Type, CustomAttributeData.Text9Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText10MapId, CustomAttributeData.Text10Property.FriendlyName, CustomAttributeData.Text10Property.Description, false, CustomAttributeData.Text10Property.Type, CustomAttributeData.Text10Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText11MapId, CustomAttributeData.Text11Property.FriendlyName, CustomAttributeData.Text11Property.Description, false, CustomAttributeData.Text11Property.Type, CustomAttributeData.Text11Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText12MapId, CustomAttributeData.Text12Property.FriendlyName, CustomAttributeData.Text12Property.Description, false, CustomAttributeData.Text12Property.Type, CustomAttributeData.Text12Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText13MapId, CustomAttributeData.Text13Property.FriendlyName, CustomAttributeData.Text13Property.Description, false, CustomAttributeData.Text13Property.Type, CustomAttributeData.Text13Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText14MapId, CustomAttributeData.Text14Property.FriendlyName, CustomAttributeData.Text14Property.Description, false, CustomAttributeData.Text14Property.Type, CustomAttributeData.Text14Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText15MapId, CustomAttributeData.Text15Property.FriendlyName, CustomAttributeData.Text15Property.Description, false, CustomAttributeData.Text15Property.Type, CustomAttributeData.Text15Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText16MapId, CustomAttributeData.Text16Property.FriendlyName, CustomAttributeData.Text16Property.Description, false, CustomAttributeData.Text16Property.Type, CustomAttributeData.Text16Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText17MapId, CustomAttributeData.Text17Property.FriendlyName, CustomAttributeData.Text17Property.Description, false, CustomAttributeData.Text17Property.Type, CustomAttributeData.Text17Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText18MapId, CustomAttributeData.Text18Property.FriendlyName, CustomAttributeData.Text18Property.Description, false, CustomAttributeData.Text18Property.Type, CustomAttributeData.Text18Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText19MapId, CustomAttributeData.Text19Property.FriendlyName, CustomAttributeData.Text19Property.Description, false, CustomAttributeData.Text19Property.Type, CustomAttributeData.Text19Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText20MapId, CustomAttributeData.Text20Property.FriendlyName, CustomAttributeData.Text20Property.Description, false, CustomAttributeData.Text20Property.Type, CustomAttributeData.Text20Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText21MapId, CustomAttributeData.Text21Property.FriendlyName, CustomAttributeData.Text21Property.Description, false, CustomAttributeData.Text21Property.Type, CustomAttributeData.Text21Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText22MapId, CustomAttributeData.Text22Property.FriendlyName, CustomAttributeData.Text22Property.Description, false, CustomAttributeData.Text22Property.Type, CustomAttributeData.Text22Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText23MapId, CustomAttributeData.Text23Property.FriendlyName, CustomAttributeData.Text23Property.Description, false, CustomAttributeData.Text23Property.Type, CustomAttributeData.Text23Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText24MapId, CustomAttributeData.Text24Property.FriendlyName, CustomAttributeData.Text24Property.Description, false, CustomAttributeData.Text24Property.Type, CustomAttributeData.Text24Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText25MapId, CustomAttributeData.Text25Property.FriendlyName, CustomAttributeData.Text25Property.Description, false, CustomAttributeData.Text25Property.Type, CustomAttributeData.Text25Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText26MapId, CustomAttributeData.Text26Property.FriendlyName, CustomAttributeData.Text26Property.Description, false, CustomAttributeData.Text26Property.Type, CustomAttributeData.Text26Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText27MapId, CustomAttributeData.Text27Property.FriendlyName, CustomAttributeData.Text27Property.Description, false, CustomAttributeData.Text27Property.Type, CustomAttributeData.Text27Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText28MapId, CustomAttributeData.Text28Property.FriendlyName, CustomAttributeData.Text28Property.Description, false, CustomAttributeData.Text28Property.Type, CustomAttributeData.Text28Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText29MapId, CustomAttributeData.Text29Property.FriendlyName, CustomAttributeData.Text29Property.Description, false, CustomAttributeData.Text29Property.Type, CustomAttributeData.Text29Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText30MapId, CustomAttributeData.Text30Property.FriendlyName, CustomAttributeData.Text30Property.Description, false, CustomAttributeData.Text30Property.Type, CustomAttributeData.Text30Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText31MapId, CustomAttributeData.Text31Property.FriendlyName, CustomAttributeData.Text31Property.Description, false, CustomAttributeData.Text31Property.Type, CustomAttributeData.Text31Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText32MapId, CustomAttributeData.Text32Property.FriendlyName, CustomAttributeData.Text32Property.Description, false, CustomAttributeData.Text32Property.Type, CustomAttributeData.Text32Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText33MapId, CustomAttributeData.Text33Property.FriendlyName, CustomAttributeData.Text33Property.Description, false, CustomAttributeData.Text33Property.Type, CustomAttributeData.Text33Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText34MapId, CustomAttributeData.Text34Property.FriendlyName, CustomAttributeData.Text34Property.Description, false, CustomAttributeData.Text34Property.Type, CustomAttributeData.Text34Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText35MapId, CustomAttributeData.Text35Property.FriendlyName, CustomAttributeData.Text35Property.Description, false, CustomAttributeData.Text35Property.Type, CustomAttributeData.Text35Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText36MapId, CustomAttributeData.Text36Property.FriendlyName, CustomAttributeData.Text36Property.Description, false, CustomAttributeData.Text36Property.Type, CustomAttributeData.Text36Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText37MapId, CustomAttributeData.Text37Property.FriendlyName, CustomAttributeData.Text37Property.Description, false, CustomAttributeData.Text37Property.Type, CustomAttributeData.Text37Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText38MapId, CustomAttributeData.Text38Property.FriendlyName, CustomAttributeData.Text38Property.Description, false, CustomAttributeData.Text38Property.Type, CustomAttributeData.Text38Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText39MapId, CustomAttributeData.Text39Property.FriendlyName, CustomAttributeData.Text39Property.Description, false, CustomAttributeData.Text39Property.Type, CustomAttributeData.Text39Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText40MapId, CustomAttributeData.Text40Property.FriendlyName, CustomAttributeData.Text40Property.Description, false, CustomAttributeData.Text40Property.Type, CustomAttributeData.Text40Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText41MapId, CustomAttributeData.Text41Property.FriendlyName, CustomAttributeData.Text41Property.Description, false, CustomAttributeData.Text41Property.Type, CustomAttributeData.Text41Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText42MapId, CustomAttributeData.Text42Property.FriendlyName, CustomAttributeData.Text42Property.Description, false, CustomAttributeData.Text42Property.Type, CustomAttributeData.Text42Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText43MapId, CustomAttributeData.Text43Property.FriendlyName, CustomAttributeData.Text43Property.Description, false, CustomAttributeData.Text43Property.Type, CustomAttributeData.Text43Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText44MapId, CustomAttributeData.Text44Property.FriendlyName, CustomAttributeData.Text44Property.Description, false, CustomAttributeData.Text44Property.Type, CustomAttributeData.Text44Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText45MapId, CustomAttributeData.Text45Property.FriendlyName, CustomAttributeData.Text45Property.Description, false, CustomAttributeData.Text45Property.Type, CustomAttributeData.Text45Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText46MapId, CustomAttributeData.Text46Property.FriendlyName, CustomAttributeData.Text46Property.Description, false, CustomAttributeData.Text46Property.Type, CustomAttributeData.Text46Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText47MapId, CustomAttributeData.Text47Property.FriendlyName, CustomAttributeData.Text47Property.Description, false, CustomAttributeData.Text47Property.Type, CustomAttributeData.Text47Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText48MapId, CustomAttributeData.Text48Property.FriendlyName, CustomAttributeData.Text48Property.Description, false, CustomAttributeData.Text48Property.Type, CustomAttributeData.Text48Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText49MapId, CustomAttributeData.Text49Property.FriendlyName, CustomAttributeData.Text49Property.Description, false, CustomAttributeData.Text49Property.Type, CustomAttributeData.Text49Property.DefaultValue, 1, 255, true));
            this.Add(ImportMapping.NewImportMapping(CustomText50MapId, CustomAttributeData.Text50Property.FriendlyName, CustomAttributeData.Text50Property.Description, false, CustomAttributeData.Text50Property.Type, CustomAttributeData.Text50Property.DefaultValue, 1, 255, true));

            //custom value fields
            this.Add(ImportMapping.NewImportMapping(CustomValue1MapId, CustomAttributeData.Value1Property.FriendlyName, CustomAttributeData.Value1Property.Description, false, CustomAttributeData.Value1Property.Type, CustomAttributeData.Value1Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue2MapId, CustomAttributeData.Value2Property.FriendlyName, CustomAttributeData.Value2Property.Description, false, CustomAttributeData.Value2Property.Type, CustomAttributeData.Value2Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue3MapId, CustomAttributeData.Value3Property.FriendlyName, CustomAttributeData.Value3Property.Description, false, CustomAttributeData.Value3Property.Type, CustomAttributeData.Value3Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue4MapId, CustomAttributeData.Value4Property.FriendlyName, CustomAttributeData.Value4Property.Description, false, CustomAttributeData.Value4Property.Type, CustomAttributeData.Value4Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue5MapId, CustomAttributeData.Value5Property.FriendlyName, CustomAttributeData.Value5Property.Description, false, CustomAttributeData.Value5Property.Type, CustomAttributeData.Value5Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue6MapId, CustomAttributeData.Value6Property.FriendlyName, CustomAttributeData.Value6Property.Description, false, CustomAttributeData.Value6Property.Type, CustomAttributeData.Value6Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue7MapId, CustomAttributeData.Value7Property.FriendlyName, CustomAttributeData.Value7Property.Description, false, CustomAttributeData.Value7Property.Type, CustomAttributeData.Value7Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue8MapId, CustomAttributeData.Value8Property.FriendlyName, CustomAttributeData.Value8Property.Description, false, CustomAttributeData.Value8Property.Type, CustomAttributeData.Value8Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue9MapId, CustomAttributeData.Value9Property.FriendlyName, CustomAttributeData.Value9Property.Description, false, CustomAttributeData.Value9Property.Type, CustomAttributeData.Value9Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue10MapId, CustomAttributeData.Value10Property.FriendlyName, CustomAttributeData.Value10Property.Description, false, CustomAttributeData.Value10Property.Type, CustomAttributeData.Value10Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue11MapId, CustomAttributeData.Value11Property.FriendlyName, CustomAttributeData.Value11Property.Description, false, CustomAttributeData.Value11Property.Type, CustomAttributeData.Value11Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue12MapId, CustomAttributeData.Value12Property.FriendlyName, CustomAttributeData.Value12Property.Description, false, CustomAttributeData.Value12Property.Type, CustomAttributeData.Value12Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue13MapId, CustomAttributeData.Value13Property.FriendlyName, CustomAttributeData.Value13Property.Description, false, CustomAttributeData.Value13Property.Type, CustomAttributeData.Value13Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue14MapId, CustomAttributeData.Value14Property.FriendlyName, CustomAttributeData.Value14Property.Description, false, CustomAttributeData.Value14Property.Type, CustomAttributeData.Value14Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue15MapId, CustomAttributeData.Value15Property.FriendlyName, CustomAttributeData.Value15Property.Description, false, CustomAttributeData.Value15Property.Type, CustomAttributeData.Value15Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue16MapId, CustomAttributeData.Value16Property.FriendlyName, CustomAttributeData.Value16Property.Description, false, CustomAttributeData.Value16Property.Type, CustomAttributeData.Value16Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue17MapId, CustomAttributeData.Value17Property.FriendlyName, CustomAttributeData.Value17Property.Description, false, CustomAttributeData.Value17Property.Type, CustomAttributeData.Value17Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue18MapId, CustomAttributeData.Value18Property.FriendlyName, CustomAttributeData.Value18Property.Description, false, CustomAttributeData.Value18Property.Type, CustomAttributeData.Value18Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue19MapId, CustomAttributeData.Value19Property.FriendlyName, CustomAttributeData.Value19Property.Description, false, CustomAttributeData.Value19Property.Type, CustomAttributeData.Value19Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue20MapId, CustomAttributeData.Value20Property.FriendlyName, CustomAttributeData.Value20Property.Description, false, CustomAttributeData.Value20Property.Type, CustomAttributeData.Value20Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue21MapId, CustomAttributeData.Value21Property.FriendlyName, CustomAttributeData.Value21Property.Description, false, CustomAttributeData.Value21Property.Type, CustomAttributeData.Value21Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue22MapId, CustomAttributeData.Value22Property.FriendlyName, CustomAttributeData.Value22Property.Description, false, CustomAttributeData.Value22Property.Type, CustomAttributeData.Value22Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue23MapId, CustomAttributeData.Value23Property.FriendlyName, CustomAttributeData.Value23Property.Description, false, CustomAttributeData.Value23Property.Type, CustomAttributeData.Value23Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue24MapId, CustomAttributeData.Value24Property.FriendlyName, CustomAttributeData.Value24Property.Description, false, CustomAttributeData.Value24Property.Type, CustomAttributeData.Value24Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue25MapId, CustomAttributeData.Value25Property.FriendlyName, CustomAttributeData.Value25Property.Description, false, CustomAttributeData.Value25Property.Type, CustomAttributeData.Value25Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue26MapId, CustomAttributeData.Value26Property.FriendlyName, CustomAttributeData.Value26Property.Description, false, CustomAttributeData.Value26Property.Type, CustomAttributeData.Value26Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue27MapId, CustomAttributeData.Value27Property.FriendlyName, CustomAttributeData.Value27Property.Description, false, CustomAttributeData.Value27Property.Type, CustomAttributeData.Value27Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue28MapId, CustomAttributeData.Value28Property.FriendlyName, CustomAttributeData.Value28Property.Description, false, CustomAttributeData.Value28Property.Type, CustomAttributeData.Value28Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue29MapId, CustomAttributeData.Value29Property.FriendlyName, CustomAttributeData.Value29Property.Description, false, CustomAttributeData.Value29Property.Type, CustomAttributeData.Value29Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue30MapId, CustomAttributeData.Value30Property.FriendlyName, CustomAttributeData.Value30Property.Description, false, CustomAttributeData.Value30Property.Type, CustomAttributeData.Value30Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue31MapId, CustomAttributeData.Value31Property.FriendlyName, CustomAttributeData.Value31Property.Description, false, CustomAttributeData.Value31Property.Type, CustomAttributeData.Value31Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue32MapId, CustomAttributeData.Value32Property.FriendlyName, CustomAttributeData.Value32Property.Description, false, CustomAttributeData.Value32Property.Type, CustomAttributeData.Value32Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue33MapId, CustomAttributeData.Value33Property.FriendlyName, CustomAttributeData.Value33Property.Description, false, CustomAttributeData.Value33Property.Type, CustomAttributeData.Value33Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue34MapId, CustomAttributeData.Value34Property.FriendlyName, CustomAttributeData.Value34Property.Description, false, CustomAttributeData.Value34Property.Type, CustomAttributeData.Value34Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue35MapId, CustomAttributeData.Value35Property.FriendlyName, CustomAttributeData.Value35Property.Description, false, CustomAttributeData.Value35Property.Type, CustomAttributeData.Value35Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue36MapId, CustomAttributeData.Value36Property.FriendlyName, CustomAttributeData.Value36Property.Description, false, CustomAttributeData.Value36Property.Type, CustomAttributeData.Value36Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue37MapId, CustomAttributeData.Value37Property.FriendlyName, CustomAttributeData.Value37Property.Description, false, CustomAttributeData.Value37Property.Type, CustomAttributeData.Value37Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue38MapId, CustomAttributeData.Value38Property.FriendlyName, CustomAttributeData.Value38Property.Description, false, CustomAttributeData.Value38Property.Type, CustomAttributeData.Value38Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue39MapId, CustomAttributeData.Value39Property.FriendlyName, CustomAttributeData.Value39Property.Description, false, CustomAttributeData.Value39Property.Type, CustomAttributeData.Value39Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue40MapId, CustomAttributeData.Value40Property.FriendlyName, CustomAttributeData.Value40Property.Description, false, CustomAttributeData.Value40Property.Type, CustomAttributeData.Value40Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue41MapId, CustomAttributeData.Value41Property.FriendlyName, CustomAttributeData.Value41Property.Description, false, CustomAttributeData.Value41Property.Type, CustomAttributeData.Value41Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue42MapId, CustomAttributeData.Value42Property.FriendlyName, CustomAttributeData.Value42Property.Description, false, CustomAttributeData.Value42Property.Type, CustomAttributeData.Value42Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue43MapId, CustomAttributeData.Value43Property.FriendlyName, CustomAttributeData.Value43Property.Description, false, CustomAttributeData.Value43Property.Type, CustomAttributeData.Value43Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue44MapId, CustomAttributeData.Value44Property.FriendlyName, CustomAttributeData.Value44Property.Description, false, CustomAttributeData.Value44Property.Type, CustomAttributeData.Value44Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue45MapId, CustomAttributeData.Value45Property.FriendlyName, CustomAttributeData.Value45Property.Description, false, CustomAttributeData.Value45Property.Type, CustomAttributeData.Value45Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue46MapId, CustomAttributeData.Value46Property.FriendlyName, CustomAttributeData.Value46Property.Description, false, CustomAttributeData.Value46Property.Type, CustomAttributeData.Value46Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue47MapId, CustomAttributeData.Value47Property.FriendlyName, CustomAttributeData.Value47Property.Description, false, CustomAttributeData.Value47Property.Type, CustomAttributeData.Value47Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue48MapId, CustomAttributeData.Value48Property.FriendlyName, CustomAttributeData.Value48Property.Description, false, CustomAttributeData.Value48Property.Type, CustomAttributeData.Value48Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue49MapId, CustomAttributeData.Value49Property.FriendlyName, CustomAttributeData.Value49Property.Description, false, CustomAttributeData.Value49Property.Type, CustomAttributeData.Value49Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));
            this.Add(ImportMapping.NewImportMapping(CustomValue50MapId, CustomAttributeData.Value50Property.FriendlyName, CustomAttributeData.Value50Property.Description, false, CustomAttributeData.Value50Property.Type, CustomAttributeData.Value50Property.DefaultValue, Int32.MinValue, Int32.MaxValue, true));

            //custom flag fields
            this.Add(ImportMapping.NewImportMapping(CustomFlag1MapId, CustomAttributeData.Flag1Property.FriendlyName, CustomAttributeData.Flag1Property.Description, false, CustomAttributeData.Flag1Property.Type, CustomAttributeData.Flag1Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag2MapId, CustomAttributeData.Flag2Property.FriendlyName, CustomAttributeData.Flag2Property.Description, false, CustomAttributeData.Flag2Property.Type, CustomAttributeData.Flag2Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag3MapId, CustomAttributeData.Flag3Property.FriendlyName, CustomAttributeData.Flag3Property.Description, false, CustomAttributeData.Flag3Property.Type, CustomAttributeData.Flag3Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag4MapId, CustomAttributeData.Flag4Property.FriendlyName, CustomAttributeData.Flag4Property.Description, false, CustomAttributeData.Flag4Property.Type, CustomAttributeData.Flag4Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag5MapId, CustomAttributeData.Flag5Property.FriendlyName, CustomAttributeData.Flag5Property.Description, false, CustomAttributeData.Flag5Property.Type, CustomAttributeData.Flag5Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag6MapId, CustomAttributeData.Flag6Property.FriendlyName, CustomAttributeData.Flag6Property.Description, false, CustomAttributeData.Flag6Property.Type, CustomAttributeData.Flag6Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag7MapId, CustomAttributeData.Flag7Property.FriendlyName, CustomAttributeData.Flag7Property.Description, false, CustomAttributeData.Flag7Property.Type, CustomAttributeData.Flag7Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag8MapId, CustomAttributeData.Flag8Property.FriendlyName, CustomAttributeData.Flag8Property.Description, false, CustomAttributeData.Flag8Property.Type, CustomAttributeData.Flag8Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag9MapId, CustomAttributeData.Flag9Property.FriendlyName, CustomAttributeData.Flag9Property.Description, false, CustomAttributeData.Flag9Property.Type, CustomAttributeData.Flag9Property.DefaultValue, 0, 1, true));
            this.Add(ImportMapping.NewImportMapping(CustomFlag10MapId, CustomAttributeData.Flag10Property.FriendlyName, CustomAttributeData.Flag10Property.Description, false, CustomAttributeData.Flag10Property.Type, CustomAttributeData.Flag10Property.DefaultValue, 0, 1, true));

            //custom date fields
            this.Add(ImportMapping.NewImportMapping(CustomDate1MapId, CustomAttributeData.Date1Property.FriendlyName, CustomAttributeData.Date1Property.Description, false, CustomAttributeData.Date1Property.Type, CustomAttributeData.Date1Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate2MapId, CustomAttributeData.Date2Property.FriendlyName, CustomAttributeData.Date2Property.Description, false, CustomAttributeData.Date2Property.Type, CustomAttributeData.Date2Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate3MapId, CustomAttributeData.Date3Property.FriendlyName, CustomAttributeData.Date3Property.Description, false, CustomAttributeData.Date3Property.Type, CustomAttributeData.Date3Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate4MapId, CustomAttributeData.Date4Property.FriendlyName, CustomAttributeData.Date4Property.Description, false, CustomAttributeData.Date4Property.Type, CustomAttributeData.Date4Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate5MapId, CustomAttributeData.Date5Property.FriendlyName, CustomAttributeData.Date5Property.Description, false, CustomAttributeData.Date5Property.Type, CustomAttributeData.Date5Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate6MapId, CustomAttributeData.Date6Property.FriendlyName, CustomAttributeData.Date6Property.Description, false, CustomAttributeData.Date6Property.Type, CustomAttributeData.Date6Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate7MapId, CustomAttributeData.Date7Property.FriendlyName, CustomAttributeData.Date7Property.Description, false, CustomAttributeData.Date7Property.Type, CustomAttributeData.Date7Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate8MapId, CustomAttributeData.Date8Property.FriendlyName, CustomAttributeData.Date8Property.Description, false, CustomAttributeData.Date8Property.Type, CustomAttributeData.Date8Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate9MapId, CustomAttributeData.Date9Property.FriendlyName, CustomAttributeData.Date9Property.Description, false, CustomAttributeData.Date9Property.Type, CustomAttributeData.Date9Property.DefaultValue, 0, 0, true));
            this.Add(ImportMapping.NewImportMapping(CustomDate10MapId, CustomAttributeData.Date10Property.FriendlyName, CustomAttributeData.Date10Property.Description, false, CustomAttributeData.Date10Property.Type, CustomAttributeData.Date10Property.DefaultValue, 0, 0, true));

            //custom note fields
            this.Add(ImportMapping.NewImportMapping(CustomNote1MapId, CustomAttributeData.Note1Property.FriendlyName, CustomAttributeData.Note1Property.Description, false, CustomAttributeData.Note1Property.Type, CustomAttributeData.Note1Property.DefaultValue, 1, 1000, true));
            this.Add(ImportMapping.NewImportMapping(CustomNote2MapId, CustomAttributeData.Note2Property.FriendlyName, CustomAttributeData.Note2Property.Description, false, CustomAttributeData.Note2Property.Type, CustomAttributeData.Note2Property.DefaultValue, 1, 1000, true));
            this.Add(ImportMapping.NewImportMapping(CustomNote3MapId, CustomAttributeData.Note3Property.FriendlyName, CustomAttributeData.Note3Property.Description, false, CustomAttributeData.Note3Property.Type, CustomAttributeData.Note3Property.DefaultValue, 1, 1000, true));
            this.Add(ImportMapping.NewImportMapping(CustomNote4MapId, CustomAttributeData.Note4Property.FriendlyName, CustomAttributeData.Note4Property.Description, false, CustomAttributeData.Note4Property.Type, CustomAttributeData.Note4Property.DefaultValue, 1, 1000, true));
            this.Add(ImportMapping.NewImportMapping(CustomNote5MapId, CustomAttributeData.Note5Property.FriendlyName, CustomAttributeData.Note5Property.Description, false, CustomAttributeData.Note5Property.Type, CustomAttributeData.Note5Property.DefaultValue, 1, 1000, true));


            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="productDto"></param>
        /// <param name="attributeDataDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, ProductDto productDto, IEnumerable<ProductGroupDto> productGroupDtoList, CustomAttributeDataDto customAttributeDto)
        {
            switch (mappingId)
            {
                #region Product Properties

                case ProductImportMappingList.AlternateDepthMapId: return productDto.AlternateDepth;
                case ProductImportMappingList.AlternateHeightMapId: return productDto.AlternateHeight;
                case ProductImportMappingList.AlternateWidthMapId: return productDto.AlternateWidth;

                case ProductImportMappingList.CanBreakTrayBackMapId: return productDto.CanBreakTrayBack;
                case ProductImportMappingList.CanBreakTrayDownMapId: return productDto.CanBreakTrayDown;
                case ProductImportMappingList.CanBreakTrayTopMapId: return productDto.CanBreakTrayTop;
                case ProductImportMappingList.CanBreakTrayUpMapId: return productDto.CanBreakTrayUp;
                case ProductImportMappingList.CasePackUnitsMapId: return productDto.CasePackUnits;
                case ProductImportMappingList.DepthMapId: return productDto.Depth;
                case ProductImportMappingList.DisplayDepthMapId: return productDto.DisplayDepth;
                case ProductImportMappingList.DisplayHeightMapId: return productDto.DisplayHeight;
                case ProductImportMappingList.DisplayWidthMapId: return productDto.DisplayWidth;
                case ProductImportMappingList.FingerSpaceAboveMapId: return productDto.FingerSpaceAbove;
                case ProductImportMappingList.FingerSpaceToTheSideMapId: return productDto.FingerSpaceToTheSide;
                case ProductImportMappingList.ForceBottomCapMapId: return productDto.ForceBottomCap;
                case ProductImportMappingList.ForceMiddleCapMapId: return productDto.ForceMiddleCap;
                case ProductImportMappingList.FrontOverhangMapId: return productDto.FrontOverhang;
                case ProductImportMappingList.GtinMapId: return productDto.Gtin;
                case ProductImportMappingList.GroupCodeMapId:
                    ProductGroupDto groupDto = productGroupDtoList.FirstOrDefault(p => p.Id == productDto.ProductGroupId);
                    return (groupDto != null) ? groupDto.Code : null;
                case ProductImportMappingList.HeightMapId: return productDto.Height;
                case ProductImportMappingList.IsActiveMapId: return productDto.IsActive;
                case ProductImportMappingList.IsFrontOnlyMapId: return productDto.IsFrontOnly;
                case ProductImportMappingList.IsPlaceHolderProductMapId: return productDto.IsPlaceHolderProduct;
               
                case ProductImportMappingList.MaxDeepMapId: return productDto.MaxDeep;
                case ProductImportMappingList.MaxRightCapMapId: return productDto.MaxRightCap;
                case ProductImportMappingList.MaxStackMapId: return productDto.MaxStack;
                case ProductImportMappingList.MaxTopCapMapId: return productDto.MaxTopCap;
                case ProductImportMappingList.MerchandisingStyleMapId:
                    if (productDto.MerchandisingStyle != null)
                    {
                        return ProductMerchandisingStyleHelper.FriendlyNames[(ProductMerchandisingStyle)(Byte)productDto.MerchandisingStyle];
                    }
                    return productDto.MerchandisingStyle;
                case ProductImportMappingList.MinDeepMapId: return productDto.MinDeep;
                case ProductImportMappingList.NameMapId: return productDto.Name;
                case ProductImportMappingList.NestingDepthMapId: return productDto.NestingDepth;
                case ProductImportMappingList.NestingHeightMapId: return productDto.NestingHeight;
                case ProductImportMappingList.NestingWidthMapId: return productDto.NestingWidth;
                case ProductImportMappingList.NumberOfPegHolesMapId: return productDto.NumberOfPegHoles;
                case ProductImportMappingList.OrientationTypeMapId:
                    return PlanogramProductOrientationTypeHelper.FriendlyNames[(PlanogramProductOrientationType)(Byte)productDto.OrientationType];
                case ProductImportMappingList.PegDepthMapId: return productDto.PegDepth;
                case ProductImportMappingList.PegProngOffsetXMapId: return productDto.PegProngOffsetX;
                case ProductImportMappingList.PegX2MapId: return productDto.PegX2;
                case ProductImportMappingList.PegX3MapId: return productDto.PegX3;
                case ProductImportMappingList.PegXMapId: return productDto.PegX;
                case ProductImportMappingList.PegY2MapId: return productDto.PegY2;
                case ProductImportMappingList.PegY3MapId: return productDto.PegY3;
                case ProductImportMappingList.PegYMapId: return productDto.PegY;
                case ProductImportMappingList.PointOfPurchaseDepthMapId: return productDto.PointOfPurchaseDepth;
                case ProductImportMappingList.PointOfPurchaseHeightMapId: return productDto.PointOfPurchaseHeight;
                case ProductImportMappingList.PointOfPurchaseWidthMapId: return productDto.PointOfPurchaseWidth;
                case ProductImportMappingList.SqueezeDepthMapId: return productDto.SqueezeDepth;
                case ProductImportMappingList.SqueezeHeightMapId: return productDto.SqueezeHeight;
                case ProductImportMappingList.SqueezeWidthMapId: return productDto.SqueezeWidth;
                case ProductImportMappingList.StatusTypeMapId:
                    return PlanogramProductStatusTypeHelper.FriendlyNames[(PlanogramProductStatusType)(Byte)productDto.StatusType];
                case ProductImportMappingList.TrayDeepMapId: return productDto.TrayDeep;
                case ProductImportMappingList.TrayHighMapId: return productDto.TrayHigh;
                case ProductImportMappingList.TrayThickDepthMapId: return productDto.TrayThickDepth;
                case ProductImportMappingList.TrayThickHeightMapId: return productDto.TrayThickHeight;
                case ProductImportMappingList.TrayThickWidthMapId: return productDto.TrayThickWidth;
                case ProductImportMappingList.TrayWideMapId: return productDto.TrayWide;
                case ProductImportMappingList.WidthMapId: return productDto.Width;
                case ProductImportMappingList.TrayPackUnitsMapId: return productDto.TrayPackUnits;
                case ProductImportMappingList.TrayDepthMapId: return productDto.TrayDepth;
                case ProductImportMappingList.TrayHeightMapId: return productDto.TrayHeight;
                case ProductImportMappingList.TrayWidthMapId: return productDto.TrayWidth;
                case ProductImportMappingList.CaseHeightMapId: return productDto.CaseHeight;
                case ProductImportMappingList.CaseWidthMapId: return productDto.CaseWidth;
                case ProductImportMappingList.CaseDepthMapId: return productDto.CaseDepth;
                case ProductImportMappingList.CaseHighMapId: return productDto.CaseHigh;
                case ProductImportMappingList.CaseWideMapId: return productDto.CaseWide;
                case ProductImportMappingList.CaseDeepMapId: return productDto.CaseDeep;
                case ProductImportMappingList.CustomerStatusMapId: return productDto.CustomerStatus;
                case ProductImportMappingList.FillColourMapId: return productDto.FillColour;
                case ProductImportMappingList.FillPatternTypeMapId:
                    return PlanogramProductFillPatternTypeHelper.FriendlyNames[(PlanogramProductFillPatternType)(Byte)productDto.FillPatternType];
                case ProductImportMappingList.ShapeTypeMapId:
                    return PlanogramProductShapeTypeHelper.FriendlyNames[(PlanogramProductShapeType)(Byte)productDto.ShapeType];
                case ProductImportMappingList.ShapeMapId: return productDto.Shape;

                #endregion

                #region Product Attribute Properties

                case ProductImportMappingList.HealthMapId: return productDto.Health;
                case ProductImportMappingList.BarCodeMapId: return productDto.Barcode;
                case ProductImportMappingList.BrandMapId: return productDto.Brand;
                case ProductImportMappingList.CaseCostMapId: return productDto.CaseCost;
                case ProductImportMappingList.ColourMapId: return productDto.Colour;
                case ProductImportMappingList.ConsumerInformationMapId: return productDto.ConsumerInformation;
                case ProductImportMappingList.CorporateCodeMapId: return productDto.CorporateCode;
                case ProductImportMappingList.CostPriceMapId: return productDto.CostPrice;
                case ProductImportMappingList.CountryOfOriginMapId: return productDto.CountryOfOrigin;
                case ProductImportMappingList.CountryOfProcessingMapId: return productDto.CountryOfProcessing;
                case ProductImportMappingList.DateDiscontinuedMapId: return productDto.DateDiscontinued;
                case ProductImportMappingList.DateEffectiveMapId: return productDto.DateEffective;
                case ProductImportMappingList.DateIntroducedMapId: return productDto.DateIntroduced;
                case ProductImportMappingList.DeliveryFrequencyDaysMapId: return productDto.DeliveryFrequencyDays;
                case ProductImportMappingList.DeliveryMethodMapId: return productDto.DeliveryMethod;
                case ProductImportMappingList.FinancialGroupCodeMapId: return productDto.FinancialGroupCode;
                case ProductImportMappingList.FinancialGroupNameMapId: return productDto.FinancialGroupName;
                case ProductImportMappingList.FlavourMapId: return productDto.Flavour;
                case ProductImportMappingList.GarmentTypeMapId: return productDto.GarmentType;
                case ProductImportMappingList.IsNewMapId: return productDto.IsNewProduct;
                case ProductImportMappingList.IsPrivateLabelMapId: return productDto.IsPrivateLabel;
                case ProductImportMappingList.ManufacturerCodeMapId: return productDto.ManufacturerCode;
                case ProductImportMappingList.ManufacturerMapId: return productDto.Manufacturer;
                case ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId: return productDto.ManufacturerRecommendedRetailPrice;
                case ProductImportMappingList.ModelMapId: return productDto.Model;
                case ProductImportMappingList.PackagingShapeMapId: return productDto.PackagingShape;
                case ProductImportMappingList.PackagingTypeMapId: return productDto.PackagingType;
                case ProductImportMappingList.PatternMapId: return productDto.Pattern;
                case ProductImportMappingList.PointOfPurchaseDescriptionMapId: return productDto.PointOfPurchaseDescription;
                case ProductImportMappingList.RecommendedRetailPriceMapId: return productDto.RecommendedRetailPrice;
                case ProductImportMappingList.SellPackCountMapId: return productDto.SellPackCount;
                case ProductImportMappingList.SellPackDescriptionMapId: return productDto.SellPackDescription;
                case ProductImportMappingList.SellPriceMapId: return productDto.SellPrice;
                case ProductImportMappingList.ShelfLifeMapId: return productDto.ShelfLife;
                case ProductImportMappingList.ShortDescriptionMapId: return productDto.ShortDescription;
                case ProductImportMappingList.SizeMapId: return productDto.Size;
                case ProductImportMappingList.StyleNumberMapId: return productDto.StyleNumber;
                case ProductImportMappingList.SubCategoryMapId: return productDto.Subcategory;
                case ProductImportMappingList.TaxRateMapId: return productDto.TaxRate;
                case ProductImportMappingList.TextureMapId: return productDto.Texture;
                case ProductImportMappingList.UnitOfMeasureMapId: return productDto.UnitOfMeasure;
                case ProductImportMappingList.VendorCodeMapId: return productDto.VendorCode;
                case ProductImportMappingList.VendorMapId: return productDto.Vendor;
                #endregion

                #region Custom Attribute Properties

                case ProductImportMappingList.CustomText1MapId: return customAttributeDto.Text1;
                case ProductImportMappingList.CustomText2MapId: return customAttributeDto.Text2;
                case ProductImportMappingList.CustomText3MapId: return customAttributeDto.Text3;
                case ProductImportMappingList.CustomText4MapId: return customAttributeDto.Text4;
                case ProductImportMappingList.CustomText5MapId: return customAttributeDto.Text5;
                case ProductImportMappingList.CustomText6MapId: return customAttributeDto.Text6;
                case ProductImportMappingList.CustomText7MapId: return customAttributeDto.Text7;
                case ProductImportMappingList.CustomText8MapId: return customAttributeDto.Text8;
                case ProductImportMappingList.CustomText9MapId: return customAttributeDto.Text9;
                case ProductImportMappingList.CustomText10MapId: return customAttributeDto.Text10;
                case ProductImportMappingList.CustomText11MapId: return customAttributeDto.Text11;
                case ProductImportMappingList.CustomText12MapId: return customAttributeDto.Text12;
                case ProductImportMappingList.CustomText13MapId: return customAttributeDto.Text13;
                case ProductImportMappingList.CustomText14MapId: return customAttributeDto.Text14;
                case ProductImportMappingList.CustomText15MapId: return customAttributeDto.Text15;
                case ProductImportMappingList.CustomText16MapId: return customAttributeDto.Text16;
                case ProductImportMappingList.CustomText17MapId: return customAttributeDto.Text17;
                case ProductImportMappingList.CustomText18MapId: return customAttributeDto.Text18;
                case ProductImportMappingList.CustomText19MapId: return customAttributeDto.Text19;
                case ProductImportMappingList.CustomText20MapId: return customAttributeDto.Text20;
                case ProductImportMappingList.CustomText21MapId: return customAttributeDto.Text21;
                case ProductImportMappingList.CustomText22MapId: return customAttributeDto.Text22;
                case ProductImportMappingList.CustomText23MapId: return customAttributeDto.Text23;
                case ProductImportMappingList.CustomText24MapId: return customAttributeDto.Text24;
                case ProductImportMappingList.CustomText25MapId: return customAttributeDto.Text25;
                case ProductImportMappingList.CustomText26MapId: return customAttributeDto.Text26;
                case ProductImportMappingList.CustomText27MapId: return customAttributeDto.Text27;
                case ProductImportMappingList.CustomText28MapId: return customAttributeDto.Text28;
                case ProductImportMappingList.CustomText29MapId: return customAttributeDto.Text29;
                case ProductImportMappingList.CustomText30MapId: return customAttributeDto.Text30;
                case ProductImportMappingList.CustomText31MapId: return customAttributeDto.Text31;
                case ProductImportMappingList.CustomText32MapId: return customAttributeDto.Text32;
                case ProductImportMappingList.CustomText33MapId: return customAttributeDto.Text33;
                case ProductImportMappingList.CustomText34MapId: return customAttributeDto.Text34;
                case ProductImportMappingList.CustomText35MapId: return customAttributeDto.Text35;
                case ProductImportMappingList.CustomText36MapId: return customAttributeDto.Text36;
                case ProductImportMappingList.CustomText37MapId: return customAttributeDto.Text37;
                case ProductImportMappingList.CustomText38MapId: return customAttributeDto.Text38;
                case ProductImportMappingList.CustomText39MapId: return customAttributeDto.Text39;
                case ProductImportMappingList.CustomText40MapId: return customAttributeDto.Text40;
                case ProductImportMappingList.CustomText41MapId: return customAttributeDto.Text41;
                case ProductImportMappingList.CustomText42MapId: return customAttributeDto.Text42;
                case ProductImportMappingList.CustomText43MapId: return customAttributeDto.Text43;
                case ProductImportMappingList.CustomText44MapId: return customAttributeDto.Text44;
                case ProductImportMappingList.CustomText45MapId: return customAttributeDto.Text45;
                case ProductImportMappingList.CustomText46MapId: return customAttributeDto.Text46;
                case ProductImportMappingList.CustomText47MapId: return customAttributeDto.Text47;
                case ProductImportMappingList.CustomText48MapId: return customAttributeDto.Text48;
                case ProductImportMappingList.CustomText49MapId: return customAttributeDto.Text49;
                case ProductImportMappingList.CustomText50MapId: return customAttributeDto.Text50;

                case ProductImportMappingList.CustomValue1MapId: return customAttributeDto.Value1;
                case ProductImportMappingList.CustomValue2MapId: return customAttributeDto.Value2;
                case ProductImportMappingList.CustomValue3MapId: return customAttributeDto.Value3;
                case ProductImportMappingList.CustomValue4MapId: return customAttributeDto.Value4;
                case ProductImportMappingList.CustomValue5MapId: return customAttributeDto.Value5;
                case ProductImportMappingList.CustomValue6MapId: return customAttributeDto.Value6;
                case ProductImportMappingList.CustomValue7MapId: return customAttributeDto.Value7;
                case ProductImportMappingList.CustomValue8MapId: return customAttributeDto.Value8;
                case ProductImportMappingList.CustomValue9MapId: return customAttributeDto.Value9;
                case ProductImportMappingList.CustomValue10MapId: return customAttributeDto.Value10;
                case ProductImportMappingList.CustomValue11MapId: return customAttributeDto.Value11;
                case ProductImportMappingList.CustomValue12MapId: return customAttributeDto.Value12;
                case ProductImportMappingList.CustomValue13MapId: return customAttributeDto.Value13;
                case ProductImportMappingList.CustomValue14MapId: return customAttributeDto.Value14;
                case ProductImportMappingList.CustomValue15MapId: return customAttributeDto.Value15;
                case ProductImportMappingList.CustomValue16MapId: return customAttributeDto.Value16;
                case ProductImportMappingList.CustomValue17MapId: return customAttributeDto.Value17;
                case ProductImportMappingList.CustomValue18MapId: return customAttributeDto.Value18;
                case ProductImportMappingList.CustomValue19MapId: return customAttributeDto.Value19;
                case ProductImportMappingList.CustomValue20MapId: return customAttributeDto.Value20;
                case ProductImportMappingList.CustomValue21MapId: return customAttributeDto.Value21;
                case ProductImportMappingList.CustomValue22MapId: return customAttributeDto.Value22;
                case ProductImportMappingList.CustomValue23MapId: return customAttributeDto.Value23;
                case ProductImportMappingList.CustomValue24MapId: return customAttributeDto.Value24;
                case ProductImportMappingList.CustomValue25MapId: return customAttributeDto.Value25;
                case ProductImportMappingList.CustomValue26MapId: return customAttributeDto.Value26;
                case ProductImportMappingList.CustomValue27MapId: return customAttributeDto.Value27;
                case ProductImportMappingList.CustomValue28MapId: return customAttributeDto.Value28;
                case ProductImportMappingList.CustomValue29MapId: return customAttributeDto.Value29;
                case ProductImportMappingList.CustomValue30MapId: return customAttributeDto.Value30;
                case ProductImportMappingList.CustomValue31MapId: return customAttributeDto.Value31;
                case ProductImportMappingList.CustomValue32MapId: return customAttributeDto.Value32;
                case ProductImportMappingList.CustomValue33MapId: return customAttributeDto.Value33;
                case ProductImportMappingList.CustomValue34MapId: return customAttributeDto.Value34;
                case ProductImportMappingList.CustomValue35MapId: return customAttributeDto.Value35;
                case ProductImportMappingList.CustomValue36MapId: return customAttributeDto.Value36;
                case ProductImportMappingList.CustomValue37MapId: return customAttributeDto.Value37;
                case ProductImportMappingList.CustomValue38MapId: return customAttributeDto.Value38;
                case ProductImportMappingList.CustomValue39MapId: return customAttributeDto.Value39;
                case ProductImportMappingList.CustomValue40MapId: return customAttributeDto.Value40;
                case ProductImportMappingList.CustomValue41MapId: return customAttributeDto.Value41;
                case ProductImportMappingList.CustomValue42MapId: return customAttributeDto.Value42;
                case ProductImportMappingList.CustomValue43MapId: return customAttributeDto.Value43;
                case ProductImportMappingList.CustomValue44MapId: return customAttributeDto.Value44;
                case ProductImportMappingList.CustomValue45MapId: return customAttributeDto.Value45;
                case ProductImportMappingList.CustomValue46MapId: return customAttributeDto.Value46;
                case ProductImportMappingList.CustomValue47MapId: return customAttributeDto.Value47;
                case ProductImportMappingList.CustomValue48MapId: return customAttributeDto.Value48;
                case ProductImportMappingList.CustomValue49MapId: return customAttributeDto.Value49;
                case ProductImportMappingList.CustomValue50MapId: return customAttributeDto.Value50;

                case ProductImportMappingList.CustomDate1MapId: return customAttributeDto.Date1;
                case ProductImportMappingList.CustomDate2MapId: return customAttributeDto.Date2;
                case ProductImportMappingList.CustomDate3MapId: return customAttributeDto.Date3;
                case ProductImportMappingList.CustomDate4MapId: return customAttributeDto.Date4;
                case ProductImportMappingList.CustomDate5MapId: return customAttributeDto.Date5;
                case ProductImportMappingList.CustomDate6MapId: return customAttributeDto.Date6;
                case ProductImportMappingList.CustomDate7MapId: return customAttributeDto.Date7;
                case ProductImportMappingList.CustomDate8MapId: return customAttributeDto.Date8;
                case ProductImportMappingList.CustomDate9MapId: return customAttributeDto.Date9;
                case ProductImportMappingList.CustomDate10MapId: return customAttributeDto.Date10;

                case ProductImportMappingList.CustomFlag1MapId: return customAttributeDto.Flag1;
                case ProductImportMappingList.CustomFlag2MapId: return customAttributeDto.Flag2;
                case ProductImportMappingList.CustomFlag3MapId: return customAttributeDto.Flag3;
                case ProductImportMappingList.CustomFlag4MapId: return customAttributeDto.Flag4;
                case ProductImportMappingList.CustomFlag5MapId: return customAttributeDto.Flag5;
                case ProductImportMappingList.CustomFlag6MapId: return customAttributeDto.Flag6;
                case ProductImportMappingList.CustomFlag7MapId: return customAttributeDto.Flag7;
                case ProductImportMappingList.CustomFlag8MapId: return customAttributeDto.Flag8;
                case ProductImportMappingList.CustomFlag9MapId: return customAttributeDto.Flag9;
                case ProductImportMappingList.CustomFlag10MapId: return customAttributeDto.Flag10;

                case ProductImportMappingList.CustomNote1MapId: return customAttributeDto.Note1;
                case ProductImportMappingList.CustomNote2MapId: return customAttributeDto.Note2;
                case ProductImportMappingList.CustomNote3MapId: return customAttributeDto.Note3;
                case ProductImportMappingList.CustomNote4MapId: return customAttributeDto.Note4;
                case ProductImportMappingList.CustomNote5MapId: return customAttributeDto.Note5;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        private static DateTime? DateConvert(Object cellObject)
        {
            DateTime? resultDate = null;
            DateTime tryDate;
            bool result = DateTime.TryParse(cellObject.ToString(), out tryDate);
            resultDate = (result ? (DateTime?)tryDate : null);

            return resultDate;
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="productDto"></param>
        /// <param name="attributeDataDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            ProductDto productDto, CustomAttributeDataDto customAttributeDataDto)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            switch (mappingId)
            {
                #region Main Properties

                case ProductImportMappingList.AlternateDepthMapId:
                    productDto.AlternateDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.AlternateHeightMapId:
                    productDto.AlternateHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.AlternateWidthMapId:
                    productDto.AlternateWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.BarCodeMapId:
                    productDto.Barcode = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.BrandMapId:
                    productDto.Brand = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CanBreakTrayBackMapId:
                    productDto.CanBreakTrayBack = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CanBreakTrayDownMapId:
                    productDto.CanBreakTrayDown = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CanBreakTrayTopMapId:
                    productDto.CanBreakTrayTop = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CanBreakTrayUpMapId:
                    productDto.CanBreakTrayUp = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CaseCostMapId:
                    productDto.CaseCost = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CasePackUnitsMapId:
                    productDto.CasePackUnits = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductImportMappingList.ColourMapId:
                    productDto.Colour = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.ConsumerInformationMapId:
                    productDto.ConsumerInformation = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CorporateCodeMapId:
                    productDto.CorporateCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CostPriceMapId:
                    productDto.CostPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CountryOfOriginMapId:
                    productDto.CountryOfOrigin = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CountryOfProcessingMapId:
                    productDto.CountryOfProcessing = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.DateDiscontinuedMapId:
                    productDto.DateDiscontinued = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.DateEffectiveMapId:
                    //productDto.DateEffective = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;

                    if (cellValue != null)
                    {
                        DateTime? convDate1 = null;
                        try
                        {
                            // Try parsing the date using the invariant culture
                            convDate1 = (DateTime?)Convert.ToDateTime(cellValue, prov);
                        }
                        catch (FormatException)
                        {
                            // if that fails use TryParse against the current culture
                            convDate1 = DateConvert(cellValue);
                        }
                        catch (InvalidCastException)
                        {
                            convDate1 = DateConvert(cellValue);
                        }

                        productDto.DateEffective = convDate1;
                    }
                    else
                    {
                        productDto.DateEffective = null;
                    }
                    break;

                case ProductImportMappingList.DateIntroducedMapId:
                    productDto.DateIntroduced = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.DeliveryFrequencyDaysMapId:
                    productDto.DeliveryFrequencyDays = (short?)Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.DeliveryMethodMapId:
                    productDto.DeliveryMethod = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.DepthMapId:
                    productDto.Depth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.DisplayDepthMapId:
                    productDto.DisplayDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.DisplayHeightMapId:
                    productDto.DisplayHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.DisplayWidthMapId:
                    productDto.DisplayWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.FinancialGroupCodeMapId:
                    productDto.FinancialGroupCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.FinancialGroupNameMapId:
                    productDto.FinancialGroupName = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.FingerSpaceAboveMapId:
                    productDto.FingerSpaceAbove = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.FingerSpaceToTheSideMapId:
                    productDto.FingerSpaceToTheSide = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.FlavourMapId:
                    productDto.Flavour = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.ForceBottomCapMapId:
                    productDto.ForceBottomCap = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.ForceMiddleCapMapId:
                    productDto.ForceMiddleCap = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.FrontOverhangMapId:
                    productDto.FrontOverhang = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.GarmentTypeMapId:
                    productDto.GarmentType = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.GtinMapId:
                    productDto.Gtin = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.HealthMapId:
                    productDto.Health = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.HeightMapId:
                    productDto.Height = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.IsActiveMapId:
                    productDto.IsActive = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.IsFrontOnlyMapId:
                    productDto.IsFrontOnly = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.IsNewMapId:
                    productDto.IsNewProduct = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.IsPlaceHolderProductMapId:
                    productDto.IsPlaceHolderProduct = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.IsPrivateLabelMapId:
                    productDto.IsPrivateLabel = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.ManufacturerCodeMapId:
                    productDto.ManufacturerCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.ManufacturerMapId:
                    productDto.Manufacturer = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId:
                    productDto.ManufacturerRecommendedRetailPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.MaxDeepMapId:
                    productDto.MaxDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.MaxRightCapMapId:
                    productDto.MaxRightCap = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.MaxStackMapId:
                    productDto.MaxStack = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.MaxTopCapMapId:
                    productDto.MaxTopCap = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.MerchandisingStyleMapId:
                    ProductMerchandisingStyle? merchStyle = ProductMerchandisingStyleHelper.ProductMerchandisingStyleGetEnum(cellValue.ToString());
                    //V8-28010 : If users default, this value could be null
                    if (merchStyle != null)
                    {
                        productDto.MerchandisingStyle = (Byte)merchStyle;
                    }
                    break;

                case ProductImportMappingList.MinDeepMapId:
                    productDto.MinDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.ModelMapId:
                    productDto.Model = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.NameMapId:
                    productDto.Name = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.NestingDepthMapId:
                    productDto.NestingDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.NestingHeightMapId:
                    productDto.NestingHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.NestingWidthMapId:
                    productDto.NestingWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.NumberOfPegHolesMapId:
                    productDto.NumberOfPegHoles = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.OrientationTypeMapId:
                    //V8-29143
                    PlanogramProductOrientationType? orientationType = PlanogramProductOrientationTypeHelper.PlanogramProductOrientationTypeGetEnum(cellValue.ToString());
                    if (orientationType != null)
                    {
                        productDto.OrientationType = (Byte)orientationType;
                    }
                    break;

                case ProductImportMappingList.PackagingShapeMapId:
                    productDto.PackagingShape = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.PackagingTypeMapId:
                    productDto.PackagingType = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.PatternMapId:
                    productDto.Pattern = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.PegDepthMapId:
                    productDto.PegDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PegProngOffsetXMapId:
                    productDto.PegProngOffsetX = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PegX2MapId:
                    productDto.PegX2 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PegX3MapId:
                    productDto.PegX3 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PegXMapId:
                    productDto.PegX = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PegY2MapId:
                    productDto.PegY2 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PegY3MapId:
                    productDto.PegY3 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PegYMapId:
                    productDto.PegY = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PointOfPurchaseDepthMapId:
                    productDto.PointOfPurchaseDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PointOfPurchaseDescriptionMapId:
                    productDto.PointOfPurchaseDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.PointOfPurchaseHeightMapId:
                    productDto.PointOfPurchaseHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.PointOfPurchaseWidthMapId:
                    productDto.PointOfPurchaseWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.RecommendedRetailPriceMapId:
                    productDto.RecommendedRetailPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.SellPackCountMapId:
                    productDto.SellPackCount = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductImportMappingList.SellPackDescriptionMapId:
                    productDto.SellPackDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.SellPriceMapId:
                    productDto.SellPrice = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.ShortDescriptionMapId:
                    productDto.ShortDescription = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.ShelfLifeMapId:
                    productDto.ShelfLife = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductImportMappingList.SizeMapId:
                    productDto.Size = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.SqueezeDepthMapId:
                    productDto.SqueezeDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.SqueezeHeightMapId:
                    productDto.SqueezeHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.SqueezeWidthMapId:
                    productDto.SqueezeWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.StatusTypeMapId:
                    PlanogramProductStatusType? statusType = PlanogramProductStatusTypeHelper.PlanogramProductStatusTypeGetEnum(cellValue.ToString().ToLowerInvariant());
                    if (statusType != null)
                    {
                        productDto.StatusType = (Byte)statusType;
                    }
                    break;

                case ProductImportMappingList.StyleNumberMapId:
                    productDto.StyleNumber = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductImportMappingList.SubCategoryMapId:
                    productDto.Subcategory = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.TaxRateMapId:
                    productDto.TaxRate = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.TextureMapId:
                    productDto.Texture = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayDeepMapId:
                    productDto.TrayDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayHighMapId:
                    productDto.TrayHigh = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayThickDepthMapId:
                    productDto.TrayThickDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayThickHeightMapId:
                    productDto.TrayThickHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayThickWidthMapId:
                    productDto.TrayThickWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayWideMapId:
                    productDto.TrayWide = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.UnitOfMeasureMapId:
                    productDto.UnitOfMeasure = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.VendorCodeMapId:
                    productDto.VendorCode = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.VendorMapId:
                    productDto.Vendor = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.WidthMapId:
                    productDto.Width = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayPackUnitsMapId:
                    productDto.TrayPackUnits = Convert.ToInt16(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayDepthMapId:
                    productDto.TrayDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayHeightMapId:
                    productDto.TrayHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.TrayWidthMapId:
                    productDto.TrayWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CaseHeightMapId:
                    productDto.CaseHeight = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CaseWidthMapId:
                    productDto.CaseWidth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CaseDepthMapId:
                    productDto.CaseDepth = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CaseHighMapId: 
                    productDto.CaseHigh = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.CaseWideMapId: 
                    productDto.CaseWide = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.CaseDeepMapId:
                    productDto.CaseDeep = Convert.ToByte(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomerStatusMapId: 
                    productDto.CustomerStatus = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.FillColourMapId:
                    productDto.FillColour = Convert.ToInt32(cellValue, prov);
                    break;

                case ProductImportMappingList.FillPatternTypeMapId:
                    PlanogramProductFillPatternType? fillPatternType = PlanogramProductFillPatternTypeHelper.PlanogramProductFillPatternTypeGetEnum(cellValue.ToString().ToLowerInvariant());
                    if (fillPatternType != null)
                    {
                        productDto.FillPatternType = (Byte)fillPatternType;
                    }
                    break;

                case ProductImportMappingList.ShapeTypeMapId:
                    PlanogramProductShapeType? shapeType = PlanogramProductShapeTypeHelper.PlanogramProductShapeTypeGetEnum(cellValue.ToString().ToLowerInvariant());
                    if (shapeType != null)
                    {
                        productDto.ShapeType = (Byte)shapeType;
                    }
                    break;
                
                case ProductImportMappingList.ShapeMapId: 
                    productDto.Shape = Convert.ToString(cellValue, prov);
                    break;

                #endregion

                #region Custom Attribute Properties
                    
                #region Custom Text Fields

                case ProductImportMappingList.CustomText1MapId:
                    customAttributeDataDto.Text1 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText2MapId:
                    customAttributeDataDto.Text2 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText3MapId:
                    customAttributeDataDto.Text3 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText4MapId:
                    customAttributeDataDto.Text4 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText5MapId:
                    customAttributeDataDto.Text5 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText6MapId:
                    customAttributeDataDto.Text6 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText7MapId:
                    customAttributeDataDto.Text7 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText8MapId:
                    customAttributeDataDto.Text8 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText9MapId:
                    customAttributeDataDto.Text9 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText10MapId:
                    customAttributeDataDto.Text10 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText11MapId:
                    customAttributeDataDto.Text11 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText12MapId:
                    customAttributeDataDto.Text12 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText13MapId:
                    customAttributeDataDto.Text13 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText14MapId:
                    customAttributeDataDto.Text14 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText15MapId:
                    customAttributeDataDto.Text15 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText16MapId:
                    customAttributeDataDto.Text16 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText17MapId:
                    customAttributeDataDto.Text17 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText18MapId:
                    customAttributeDataDto.Text18 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText19MapId:
                    customAttributeDataDto.Text19 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText20MapId:
                    customAttributeDataDto.Text20 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText21MapId:
                    customAttributeDataDto.Text21 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText22MapId:
                    customAttributeDataDto.Text22 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText23MapId:
                    customAttributeDataDto.Text23 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText24MapId:
                    customAttributeDataDto.Text24 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText25MapId:
                    customAttributeDataDto.Text25 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText26MapId:
                    customAttributeDataDto.Text26 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText27MapId:
                    customAttributeDataDto.Text27 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText28MapId:
                    customAttributeDataDto.Text28 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText29MapId:
                    customAttributeDataDto.Text29 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText30MapId:
                    customAttributeDataDto.Text30 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText31MapId:
                    customAttributeDataDto.Text31 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText32MapId:
                    customAttributeDataDto.Text32 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText33MapId:
                    customAttributeDataDto.Text33 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText34MapId:
                    customAttributeDataDto.Text34 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText35MapId:
                    customAttributeDataDto.Text35 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText36MapId:
                    customAttributeDataDto.Text36 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText37MapId:
                    customAttributeDataDto.Text37 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText38MapId:
                    customAttributeDataDto.Text38 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText39MapId:
                    customAttributeDataDto.Text39 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText40MapId:
                    customAttributeDataDto.Text40 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText41MapId:
                    customAttributeDataDto.Text41 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText42MapId:
                    customAttributeDataDto.Text42 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText43MapId:
                    customAttributeDataDto.Text43 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText44MapId:
                    customAttributeDataDto.Text44 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText45MapId:
                    customAttributeDataDto.Text45 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText46MapId:
                    customAttributeDataDto.Text46 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText47MapId:
                    customAttributeDataDto.Text47 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText48MapId:
                    customAttributeDataDto.Text48 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText49MapId:
                    customAttributeDataDto.Text49 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomText50MapId:
                    customAttributeDataDto.Text50 = Convert.ToString(cellValue, prov);
                    break;

                #endregion

                #region Custom Value Fields


                case ProductImportMappingList.CustomValue1MapId:
                    customAttributeDataDto.Value1 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue2MapId:
                    customAttributeDataDto.Value2 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue3MapId:
                    customAttributeDataDto.Value3 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue4MapId:
                    customAttributeDataDto.Value4 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue5MapId:
                    customAttributeDataDto.Value5 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue6MapId:
                    customAttributeDataDto.Value6 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue7MapId:
                    customAttributeDataDto.Value7 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue8MapId:
                    customAttributeDataDto.Value8 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue9MapId:
                    customAttributeDataDto.Value9 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue10MapId:
                    customAttributeDataDto.Value10 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue11MapId:
                    customAttributeDataDto.Value11 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue12MapId:
                    customAttributeDataDto.Value12 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue13MapId:
                    customAttributeDataDto.Value13 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue14MapId:
                    customAttributeDataDto.Value14 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue15MapId:
                    customAttributeDataDto.Value15 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue16MapId:
                    customAttributeDataDto.Value16 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue17MapId:
                    customAttributeDataDto.Value17 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue18MapId:
                    customAttributeDataDto.Value18 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue19MapId:
                    customAttributeDataDto.Value19 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue20MapId:
                    customAttributeDataDto.Value20 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue21MapId:
                    customAttributeDataDto.Value21 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue22MapId:
                    customAttributeDataDto.Value22 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue23MapId:
                    customAttributeDataDto.Value23 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue24MapId:
                    customAttributeDataDto.Value24 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue25MapId:
                    customAttributeDataDto.Value25 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue26MapId:
                    customAttributeDataDto.Value26 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue27MapId:
                    customAttributeDataDto.Value27 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue28MapId:
                    customAttributeDataDto.Value28 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue29MapId:
                    customAttributeDataDto.Value29 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue30MapId:
                    customAttributeDataDto.Value30 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue31MapId:
                    customAttributeDataDto.Value31 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue32MapId:
                    customAttributeDataDto.Value32 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue33MapId:
                    customAttributeDataDto.Value33 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue34MapId:
                    customAttributeDataDto.Value34 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue35MapId:
                    customAttributeDataDto.Value35 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue36MapId:
                    customAttributeDataDto.Value36 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue37MapId:
                    customAttributeDataDto.Value37 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue38MapId:
                    customAttributeDataDto.Value38 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue39MapId:
                    customAttributeDataDto.Value39 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue40MapId:
                    customAttributeDataDto.Value40 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue41MapId:
                    customAttributeDataDto.Value41 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue42MapId:
                    customAttributeDataDto.Value42 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue43MapId:
                    customAttributeDataDto.Value43 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue44MapId:
                    customAttributeDataDto.Value44 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue45MapId:
                    customAttributeDataDto.Value45 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue46MapId:
                    customAttributeDataDto.Value46 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue47MapId:
                    customAttributeDataDto.Value47 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue48MapId:
                    customAttributeDataDto.Value48 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue49MapId:
                    customAttributeDataDto.Value49 = Convert.ToSingle(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomValue50MapId:
                    customAttributeDataDto.Value50 = Convert.ToSingle(cellValue, prov);
                    break;

                #endregion

                #region Custom Flag Fields

                case ProductImportMappingList.CustomFlag1MapId:
                    customAttributeDataDto.Flag1 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag2MapId:
                    customAttributeDataDto.Flag2 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag3MapId:
                    customAttributeDataDto.Flag3 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag4MapId:
                    customAttributeDataDto.Flag4 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag5MapId:
                    customAttributeDataDto.Flag5 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag6MapId:
                    customAttributeDataDto.Flag6 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag7MapId:
                    customAttributeDataDto.Flag7 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag8MapId:
                    customAttributeDataDto.Flag8 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag9MapId:
                    customAttributeDataDto.Flag9 = Convert.ToBoolean(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomFlag10MapId:
                    customAttributeDataDto.Flag10 = Convert.ToBoolean(cellValue, prov);
                    break;

                #endregion

                #region Custom Date Fields

                case ProductImportMappingList.CustomDate1MapId:
                    customAttributeDataDto.Date1 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate2MapId:
                    customAttributeDataDto.Date2 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate3MapId:
                    customAttributeDataDto.Date3 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate4MapId:
                    customAttributeDataDto.Date4 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate5MapId:
                    customAttributeDataDto.Date5 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate6MapId:
                    customAttributeDataDto.Date6 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate7MapId:
                    customAttributeDataDto.Date7 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate8MapId:
                    customAttributeDataDto.Date8 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate9MapId:
                    customAttributeDataDto.Date9 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case ProductImportMappingList.CustomDate10MapId:
                    customAttributeDataDto.Date10 = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                #endregion

                #region Custom Note Fields

                case ProductImportMappingList.CustomNote1MapId:
                    customAttributeDataDto.Note1 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomNote2MapId:
                    customAttributeDataDto.Note2 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomNote3MapId:
                    customAttributeDataDto.Note3 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomNote4MapId:
                    customAttributeDataDto.Note4 = Convert.ToString(cellValue, prov);
                    break;

                case ProductImportMappingList.CustomNote5MapId:
                    customAttributeDataDto.Note5 = Convert.ToString(cellValue, prov);
                    break;

                #endregion

                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override String GetBindingPath(Int32 mappingId)
        {
            return GetBindingPath(mappingId, null);
        }

        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;

            switch (mappingId)
            {
                #region Product Properties

                case ProductImportMappingList.AlternateDepthMapId: return String.Format("{0}{1}", prefix, Product.AlternateDepthProperty.Name);
                case ProductImportMappingList.AlternateHeightMapId: return String.Format("{0}{1}", prefix, Product.AlternateHeightProperty.Name);
                case ProductImportMappingList.AlternateWidthMapId: return String.Format("{0}{1}", prefix, Product.AlternateWidthProperty.Name);

                case ProductImportMappingList.CanBreakTrayBackMapId: return String.Format("{0}{1}", prefix, Product.CanBreakTrayBackProperty.Name);
                case ProductImportMappingList.CanBreakTrayDownMapId: return String.Format("{0}{1}", prefix, Product.CanBreakTrayDownProperty.Name);
                case ProductImportMappingList.CanBreakTrayTopMapId: return String.Format("{0}{1}", prefix, Product.CanBreakTrayTopProperty.Name);
                case ProductImportMappingList.CanBreakTrayUpMapId: return String.Format("{0}{1}", prefix, Product.CanBreakTrayUpProperty.Name);
                //case ProductImportMappingList.CanMerchandiseInChestMapId: return String.Format("{0}{1}", prefix, Product.CanMerchandiseInChestProperty.Name);
                //case ProductImportMappingList.CanMerchandiseOnBarMapId: return String.Format("{0}{1}", prefix, Product.CanMerchandiseOnBarProperty.Name);
                //case ProductImportMappingList.CanMerchandiseOnPegMapId: return String.Format("{0}{1}", prefix, Product.CanMerchandiseOnPegProperty.Name);
                //case ProductImportMappingList.CanMerchandiseOnShelfMapId: return String.Format("{0}{1}", prefix, Product.CanMerchandiseOnShelfProperty.Name);
                case ProductImportMappingList.CasePackUnitsMapId: return String.Format("{0}{1}", prefix, Product.CasePackUnitsProperty.Name);
                case ProductImportMappingList.DepthMapId: return String.Format("{0}{1}", prefix, Product.DepthProperty.Name);
                case ProductImportMappingList.DisplayDepthMapId: return String.Format("{0}{1}", prefix, Product.DisplayDepthProperty.Name);
                case ProductImportMappingList.DisplayHeightMapId: return String.Format("{0}{1}", prefix, Product.DisplayHeightProperty.Name);
                case ProductImportMappingList.DisplayWidthMapId: return String.Format("{0}{1}", prefix, Product.DisplayWidthProperty.Name);
                case ProductImportMappingList.FingerSpaceAboveMapId: return String.Format("{0}{1}", prefix, Product.FingerSpaceAboveProperty.Name);
                case ProductImportMappingList.FingerSpaceToTheSideMapId: return String.Format("{0}{1}", prefix, Product.FingerSpaceToTheSideProperty.Name);
                case ProductImportMappingList.ForceBottomCapMapId: return String.Format("{0}{1}", prefix, Product.ForceBottomCapProperty.Name);
                case ProductImportMappingList.ForceMiddleCapMapId: return String.Format("{0}{1}", prefix, Product.ForceMiddleCapProperty.Name);
                case ProductImportMappingList.FrontOverhangMapId: return String.Format("{0}{1}", prefix, Product.FrontOverhangProperty.Name);
                case ProductImportMappingList.GroupCodeMapId: return String.Empty;// - not available on a product model object
                case ProductImportMappingList.GtinMapId: return String.Format("{0}{1}", prefix, Product.GtinProperty.Name);
                case ProductImportMappingList.HeightMapId: return String.Format("{0}{1}", prefix, Product.HeightProperty.Name);
                case ProductImportMappingList.IsActiveMapId: return String.Format("{0}{1}", prefix, Product.IsActiveProperty.Name);
                case ProductImportMappingList.IsFrontOnlyMapId: return String.Format("{0}{1}", prefix, Product.IsFrontOnlyProperty.Name);
                case ProductImportMappingList.IsPlaceHolderProductMapId: return String.Format("{0}{1}", prefix, Product.IsPlaceHolderProductProperty.Name);
                case ProductImportMappingList.MaxDeepMapId: return String.Format("{0}{1}", prefix, Product.MaxDeepProperty.Name);
                case ProductImportMappingList.MaxRightCapMapId: return String.Format("{0}{1}", prefix, Product.MaxRightCapProperty.Name);
                case ProductImportMappingList.MaxStackMapId: return String.Format("{0}{1}", prefix, Product.MaxStackProperty.Name);
                case ProductImportMappingList.MaxTopCapMapId: return String.Format("{0}{1}", prefix, Product.MaxTopCapProperty.Name);
                case ProductImportMappingList.MerchandisingStyleMapId: return String.Format("{0}{1}", prefix, Product.MerchandisingStyleProperty.Name);
                case ProductImportMappingList.MinDeepMapId: return String.Format("{0}{1}", prefix, Product.MinDeepProperty.Name);
                case ProductImportMappingList.NameMapId: return String.Format("{0}{1}", prefix, Product.NameProperty.Name);
                case ProductImportMappingList.NestingDepthMapId: return String.Format("{0}{1}", prefix, Product.NestingDepthProperty.Name);
                case ProductImportMappingList.NestingHeightMapId: return String.Format("{0}{1}", prefix, Product.NestingHeightProperty.Name);
                case ProductImportMappingList.NestingWidthMapId: return String.Format("{0}{1}", prefix, Product.NestingWidthProperty.Name);
                case ProductImportMappingList.NumberOfPegHolesMapId: return String.Format("{0}{1}", prefix, Product.NumberOfPegHolesProperty.Name);
                case ProductImportMappingList.OrientationTypeMapId: return String.Format("{0}{1}", prefix, Product.OrientationTypeProperty.Name);
                case ProductImportMappingList.PegDepthMapId: return String.Format("{0}{1}", prefix, Product.PegDepthProperty.Name);
                case ProductImportMappingList.PegProngOffsetXMapId: return String.Format("{0}{1}", prefix, Product.PegProngOffsetXProperty.Name);
                case ProductImportMappingList.PegX2MapId: return String.Format("{0}{1}", prefix, Product.PegX2Property.Name);
                case ProductImportMappingList.PegX3MapId: return String.Format("{0}{1}", prefix, Product.PegX3Property.Name);
                case ProductImportMappingList.PegXMapId: return String.Format("{0}{1}", prefix, Product.PegXProperty.Name);
                case ProductImportMappingList.PegY2MapId: return String.Format("{0}{1}", prefix, Product.PegY2Property.Name);
                case ProductImportMappingList.PegY3MapId: return String.Format("{0}{1}", prefix, Product.PegY3Property.Name);
                case ProductImportMappingList.PegYMapId: return String.Format("{0}{1}", prefix, Product.PegYProperty.Name);
                case ProductImportMappingList.PointOfPurchaseDepthMapId: return String.Format("{0}{1}", prefix, Product.PointOfPurchaseDepthProperty.Name);
                case ProductImportMappingList.PointOfPurchaseHeightMapId: return String.Format("{0}{1}", prefix, Product.PointOfPurchaseHeightProperty.Name);
                case ProductImportMappingList.PointOfPurchaseWidthMapId: return String.Format("{0}{1}", prefix, Product.PointOfPurchaseWidthProperty.Name);
                case ProductImportMappingList.SqueezeDepthMapId: return String.Format("{0}{1}", prefix, Product.SqueezeDepthProperty.Name);
                case ProductImportMappingList.SqueezeHeightMapId: return String.Format("{0}{1}", prefix, Product.SqueezeHeightProperty.Name);
                case ProductImportMappingList.SqueezeWidthMapId: return String.Format("{0}{1}", prefix, Product.SqueezeWidthProperty.Name);
                case ProductImportMappingList.StatusTypeMapId: return String.Format("{0}{1}", prefix, Product.StatusTypeProperty.Name);
                case ProductImportMappingList.TrayPackUnitsMapId: return String.Format("{0}{1}", prefix, Product.TrayPackUnitsProperty.Name);
                case ProductImportMappingList.TrayWideMapId: return String.Format("{0}{1}", prefix, Product.TrayWideProperty.Name);
                case ProductImportMappingList.TrayDeepMapId: return String.Format("{0}{1}", prefix, Product.TrayDeepProperty.Name);
                case ProductImportMappingList.TrayHighMapId: return String.Format("{0}{1}", prefix, Product.TrayHighProperty.Name);
                case ProductImportMappingList.TrayDepthMapId: return String.Format("{0}{1}", prefix, Product.TrayDepthProperty.Name);
                case ProductImportMappingList.TrayHeightMapId: return String.Format("{0}{1}", prefix, Product.TrayHeightProperty.Name);
                case ProductImportMappingList.TrayWidthMapId: return String.Format("{0}{1}", prefix, Product.TrayWidthProperty.Name);
                case ProductImportMappingList.TrayThickDepthMapId: return String.Format("{0}{1}", prefix, Product.TrayThickDepthProperty.Name);
                case ProductImportMappingList.TrayThickHeightMapId: return String.Format("{0}{1}", prefix, Product.TrayThickHeightProperty.Name);
                case ProductImportMappingList.TrayThickWidthMapId: return String.Format("{0}{1}", prefix, Product.TrayThickWidthProperty.Name);
                case ProductImportMappingList.WidthMapId: return String.Format("{0}{1}", prefix, Product.WidthProperty.Name);
                case ProductImportMappingList.CaseHeightMapId: return String.Format("{0}{1}", prefix, Product.CaseHeightProperty.Name);
                case ProductImportMappingList.CaseWidthMapId: return String.Format("{0}{1}", prefix, Product.CaseWidthProperty.Name);
                case ProductImportMappingList.CaseDepthMapId: return String.Format("{0}{1}", prefix, Product.CaseDepthProperty.Name);
                case ProductImportMappingList.CaseHighMapId: return String.Format("{0}{1}", prefix, Product.CaseHighProperty.Name);
                case ProductImportMappingList.CaseWideMapId: return String.Format("{0}{1}", prefix, Product.CaseWideProperty.Name);
                case ProductImportMappingList.CaseDeepMapId: return String.Format("{0}{1}", prefix, Product.CaseDeepProperty.Name);
                case ProductImportMappingList.CustomerStatusMapId: return String.Format("{0}{1}", prefix, Product.CustomerStatusProperty.Name);
                case ProductImportMappingList.FillColourMapId: return String.Format("{0}{1}", prefix, Product.FillColourProperty.Name);
                case ProductImportMappingList.FillPatternTypeMapId: return String.Format("{0}{1}", prefix, Product.FillPatternTypeProperty.Name);
                case ProductImportMappingList.ShapeTypeMapId: return String.Format("{0}{1}", prefix, Product.ShapeTypeProperty.Name);
                case ProductImportMappingList.ShapeMapId: return String.Format("{0}{1}", prefix, Product.ShapeProperty.Name);

                #endregion

                #region Product Attribute Properties

                case ProductImportMappingList.HealthMapId: return String.Format("{0}{1}", prefix, Product.HealthProperty.Name);
                case ProductImportMappingList.BarCodeMapId: return String.Format("{0}{1}", prefix, Product.BarcodeProperty.Name);
                case ProductImportMappingList.BrandMapId: return String.Format("{0}{1}", prefix, Product.BrandProperty.Name);
                case ProductImportMappingList.CaseCostMapId: return String.Format("{0}{1}", prefix, Product.CaseCostProperty.Name);
                case ProductImportMappingList.ColourMapId: return String.Format("{0}{1}", prefix, Product.ColourProperty.Name);
                case ProductImportMappingList.ConsumerInformationMapId: return String.Format("{0}{1}", prefix, Product.ConsumerInformationProperty.Name);
                case ProductImportMappingList.CorporateCodeMapId: return String.Format("{0}{1}", prefix, Product.CorporateCodeProperty.Name);
                case ProductImportMappingList.CostPriceMapId: return String.Format("{0}{1}", prefix, Product.CostPriceProperty.Name);
                case ProductImportMappingList.CountryOfOriginMapId: return String.Format("{0}{1}", prefix, Product.CountryOfOriginProperty.Name);
                case ProductImportMappingList.CountryOfProcessingMapId: return String.Format("{0}{1}", prefix, Product.CountryOfProcessingProperty.Name);
                //case ProductImportMappingList.CustomerStatusMapId: return String.Format("{0}{1}", prefix, Product.CustomerStatusProperty.Name);
                case ProductImportMappingList.DateDiscontinuedMapId: return String.Format("{0}{1}", prefix, Product.DateDiscontinuedProperty.Name);
                case ProductImportMappingList.DateEffectiveMapId: return String.Format("{0}{1}", prefix, Product.DateEffectiveProperty.Name);
                case ProductImportMappingList.DateIntroducedMapId: return String.Format("{0}{1}", prefix, Product.DateIntroducedProperty.Name);
                case ProductImportMappingList.DeliveryFrequencyDaysMapId: return String.Format("{0}{1}", prefix, Product.DeliveryFrequencyDaysProperty.Name);
                case ProductImportMappingList.DeliveryMethodMapId: return String.Format("{0}{1}", prefix, Product.DeliveryMethodProperty.Name);
                case ProductImportMappingList.FinancialGroupCodeMapId: return String.Format("{0}{1}", prefix, Product.FinancialGroupCodeProperty.Name);
                case ProductImportMappingList.FinancialGroupNameMapId: return String.Format("{0}{1}", prefix, Product.FinancialGroupNameProperty.Name);
                case ProductImportMappingList.FlavourMapId: return String.Format("{0}{1}", prefix, Product.FlavourProperty.Name);
                case ProductImportMappingList.GarmentTypeMapId: return String.Format("{0}{1}", prefix, Product.GarmentTypeProperty.Name);
                case ProductImportMappingList.IsNewMapId: return String.Format("{0}{1}", prefix, Product.IsNewProductProperty.Name);
                case ProductImportMappingList.IsPrivateLabelMapId: return String.Format("{0}{1}", prefix, Product.IsPrivateLabelProperty.Name);
                case ProductImportMappingList.ManufacturerCodeMapId: return String.Format("{0}{1}", prefix, Product.ManufacturerCodeProperty.Name);
                case ProductImportMappingList.ManufacturerMapId: return String.Format("{0}{1}", prefix, Product.ManufacturerProperty.Name);
                case ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId: return String.Format("{0}{1}", prefix, Product.ManufacturerRecommendedRetailPriceProperty.Name);
                case ProductImportMappingList.ModelMapId: return String.Format("{0}{1}", prefix, Product.ModelProperty.Name);
                case ProductImportMappingList.PackagingShapeMapId: return String.Format("{0}{1}", prefix, Product.PackagingShapeProperty.Name);
                case ProductImportMappingList.PackagingTypeMapId: return String.Format("{0}{1}", prefix, Product.PackagingTypeProperty.Name);
                case ProductImportMappingList.PatternMapId: return String.Format("{0}{1}", prefix, Product.PatternProperty.Name);
                case ProductImportMappingList.PointOfPurchaseDescriptionMapId: return String.Format("{0}{1}", prefix, Product.PointOfPurchaseDescriptionProperty.Name);
                case ProductImportMappingList.RecommendedRetailPriceMapId: return String.Format("{0}{1}", prefix, Product.RecommendedRetailPriceProperty.Name);
                case ProductImportMappingList.SellPackCountMapId: return String.Format("{0}{1}", prefix, Product.SellPackCountProperty.Name);
                case ProductImportMappingList.SellPackDescriptionMapId: return String.Format("{0}{1}", prefix, Product.SellPackDescriptionProperty.Name);
                case ProductImportMappingList.SellPriceMapId: return String.Format("{0}{1}", prefix, Product.SellPriceProperty.Name);
                case ProductImportMappingList.ShelfLifeMapId: return String.Format("{0}{1}", prefix, Product.ShelfLifeProperty.Name);
                case ProductImportMappingList.ShortDescriptionMapId: return String.Format("{0}{1}", prefix, Product.ShortDescriptionProperty.Name);
                case ProductImportMappingList.SizeMapId: return String.Format("{0}{1}", prefix, Product.SizeProperty.Name);
                case ProductImportMappingList.StyleNumberMapId: return String.Format("{0}{1}", prefix, Product.StyleNumberProperty.Name);
                case ProductImportMappingList.SubCategoryMapId: return String.Format("{0}{1}", prefix, Product.SubcategoryProperty.Name);
                case ProductImportMappingList.TaxRateMapId: return String.Format("{0}{1}", prefix, Product.TaxRateProperty.Name);
                case ProductImportMappingList.TextureMapId: return String.Format("{0}{1}", prefix, Product.TextureProperty.Name);
                case ProductImportMappingList.UnitOfMeasureMapId: return String.Format("{0}{1}", prefix, Product.UnitOfMeasureProperty.Name);
                case ProductImportMappingList.VendorCodeMapId: return String.Format("{0}{1}", prefix, Product.VendorCodeProperty.Name);
                case ProductImportMappingList.VendorMapId: return String.Format("{0}{1}", prefix, Product.VendorProperty.Name);
                #endregion

                #region Custom Fields

                case ProductImportMappingList.CustomText1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text1Property.Name);
                case ProductImportMappingList.CustomText2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text2Property.Name);
                case ProductImportMappingList.CustomText3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text3Property.Name);
                case ProductImportMappingList.CustomText4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text4Property.Name);
                case ProductImportMappingList.CustomText5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text5Property.Name);
                case ProductImportMappingList.CustomText6MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text6Property.Name);
                case ProductImportMappingList.CustomText7MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text7Property.Name);
                case ProductImportMappingList.CustomText8MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text8Property.Name);
                case ProductImportMappingList.CustomText9MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text9Property.Name);
                case ProductImportMappingList.CustomText10MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text10Property.Name);
                case ProductImportMappingList.CustomText11MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text11Property.Name);
                case ProductImportMappingList.CustomText12MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text12Property.Name);
                case ProductImportMappingList.CustomText13MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text13Property.Name);
                case ProductImportMappingList.CustomText14MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text14Property.Name);
                case ProductImportMappingList.CustomText15MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text15Property.Name);
                case ProductImportMappingList.CustomText16MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text16Property.Name);
                case ProductImportMappingList.CustomText17MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text17Property.Name);
                case ProductImportMappingList.CustomText18MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text18Property.Name);
                case ProductImportMappingList.CustomText19MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text19Property.Name);
                case ProductImportMappingList.CustomText20MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text20Property.Name);
                case ProductImportMappingList.CustomText21MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text21Property.Name);
                case ProductImportMappingList.CustomText22MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text22Property.Name);
                case ProductImportMappingList.CustomText23MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text23Property.Name);
                case ProductImportMappingList.CustomText24MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text24Property.Name);
                case ProductImportMappingList.CustomText25MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text25Property.Name);
                case ProductImportMappingList.CustomText26MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text26Property.Name);
                case ProductImportMappingList.CustomText27MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text27Property.Name);
                case ProductImportMappingList.CustomText28MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text28Property.Name);
                case ProductImportMappingList.CustomText29MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text29Property.Name);
                case ProductImportMappingList.CustomText30MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text30Property.Name);
                case ProductImportMappingList.CustomText31MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text31Property.Name);
                case ProductImportMappingList.CustomText32MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text32Property.Name);
                case ProductImportMappingList.CustomText33MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text33Property.Name);
                case ProductImportMappingList.CustomText34MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text34Property.Name);
                case ProductImportMappingList.CustomText35MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text35Property.Name);
                case ProductImportMappingList.CustomText36MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text36Property.Name);
                case ProductImportMappingList.CustomText37MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text37Property.Name);
                case ProductImportMappingList.CustomText38MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text38Property.Name);
                case ProductImportMappingList.CustomText39MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text39Property.Name);
                case ProductImportMappingList.CustomText40MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text40Property.Name);
                case ProductImportMappingList.CustomText41MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text41Property.Name);
                case ProductImportMappingList.CustomText42MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text42Property.Name);
                case ProductImportMappingList.CustomText43MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text43Property.Name);
                case ProductImportMappingList.CustomText44MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text44Property.Name);
                case ProductImportMappingList.CustomText45MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text45Property.Name);
                case ProductImportMappingList.CustomText46MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text46Property.Name);
                case ProductImportMappingList.CustomText47MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text47Property.Name);
                case ProductImportMappingList.CustomText48MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text48Property.Name);
                case ProductImportMappingList.CustomText49MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text49Property.Name);
                case ProductImportMappingList.CustomText50MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Text50Property.Name);

                case ProductImportMappingList.CustomValue1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value1Property.Name);
                case ProductImportMappingList.CustomValue2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value2Property.Name);
                case ProductImportMappingList.CustomValue3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value3Property.Name);
                case ProductImportMappingList.CustomValue4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value4Property.Name);
                case ProductImportMappingList.CustomValue5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value5Property.Name);
                case ProductImportMappingList.CustomValue6MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value6Property.Name);
                case ProductImportMappingList.CustomValue7MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value7Property.Name);
                case ProductImportMappingList.CustomValue8MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value8Property.Name);
                case ProductImportMappingList.CustomValue9MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value9Property.Name);
                case ProductImportMappingList.CustomValue10MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value10Property.Name);
                case ProductImportMappingList.CustomValue11MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value11Property.Name);
                case ProductImportMappingList.CustomValue12MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value12Property.Name);
                case ProductImportMappingList.CustomValue13MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value13Property.Name);
                case ProductImportMappingList.CustomValue14MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value14Property.Name);
                case ProductImportMappingList.CustomValue15MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value15Property.Name);
                case ProductImportMappingList.CustomValue16MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value16Property.Name);
                case ProductImportMappingList.CustomValue17MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value17Property.Name);
                case ProductImportMappingList.CustomValue18MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value18Property.Name);
                case ProductImportMappingList.CustomValue19MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value19Property.Name);
                case ProductImportMappingList.CustomValue20MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value20Property.Name);
                case ProductImportMappingList.CustomValue21MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value21Property.Name);
                case ProductImportMappingList.CustomValue22MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value22Property.Name);
                case ProductImportMappingList.CustomValue23MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value23Property.Name);
                case ProductImportMappingList.CustomValue24MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value24Property.Name);
                case ProductImportMappingList.CustomValue25MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value25Property.Name);
                case ProductImportMappingList.CustomValue26MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value26Property.Name);
                case ProductImportMappingList.CustomValue27MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value27Property.Name);
                case ProductImportMappingList.CustomValue28MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value28Property.Name);
                case ProductImportMappingList.CustomValue29MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value29Property.Name);
                case ProductImportMappingList.CustomValue30MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value30Property.Name);
                case ProductImportMappingList.CustomValue31MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value31Property.Name);
                case ProductImportMappingList.CustomValue32MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value32Property.Name);
                case ProductImportMappingList.CustomValue33MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value33Property.Name);
                case ProductImportMappingList.CustomValue34MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value34Property.Name);
                case ProductImportMappingList.CustomValue35MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value35Property.Name);
                case ProductImportMappingList.CustomValue36MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value36Property.Name);
                case ProductImportMappingList.CustomValue37MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value37Property.Name);
                case ProductImportMappingList.CustomValue38MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value38Property.Name);
                case ProductImportMappingList.CustomValue39MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value39Property.Name);
                case ProductImportMappingList.CustomValue40MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value40Property.Name);
                case ProductImportMappingList.CustomValue41MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value41Property.Name);
                case ProductImportMappingList.CustomValue42MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value42Property.Name);
                case ProductImportMappingList.CustomValue43MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value43Property.Name);
                case ProductImportMappingList.CustomValue44MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value44Property.Name);
                case ProductImportMappingList.CustomValue45MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value45Property.Name);
                case ProductImportMappingList.CustomValue46MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value46Property.Name);
                case ProductImportMappingList.CustomValue47MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value47Property.Name);
                case ProductImportMappingList.CustomValue48MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value48Property.Name);
                case ProductImportMappingList.CustomValue49MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value49Property.Name);
                case ProductImportMappingList.CustomValue50MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Value50Property.Name);

                case ProductImportMappingList.CustomFlag1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag1Property.Name);
                case ProductImportMappingList.CustomFlag2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag2Property.Name);
                case ProductImportMappingList.CustomFlag3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag3Property.Name);
                case ProductImportMappingList.CustomFlag4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag4Property.Name);
                case ProductImportMappingList.CustomFlag5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag5Property.Name);
                case ProductImportMappingList.CustomFlag6MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag6Property.Name);
                case ProductImportMappingList.CustomFlag7MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag7Property.Name);
                case ProductImportMappingList.CustomFlag8MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag8Property.Name);
                case ProductImportMappingList.CustomFlag9MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag9Property.Name);
                case ProductImportMappingList.CustomFlag10MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Flag10Property.Name);

                case ProductImportMappingList.CustomDate1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date1Property.Name);
                case ProductImportMappingList.CustomDate2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date2Property.Name); ;
                case ProductImportMappingList.CustomDate3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date3Property.Name); ;
                case ProductImportMappingList.CustomDate4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date4Property.Name); ;
                case ProductImportMappingList.CustomDate5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date5Property.Name); ;
                case ProductImportMappingList.CustomDate6MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date6Property.Name); ;
                case ProductImportMappingList.CustomDate7MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date7Property.Name); ;
                case ProductImportMappingList.CustomDate8MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date8Property.Name); ;
                case ProductImportMappingList.CustomDate9MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date9Property.Name); ;
                case ProductImportMappingList.CustomDate10MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Date10Property.Name); ;

                case ProductImportMappingList.CustomNote1MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note1Property.Name);
                case ProductImportMappingList.CustomNote2MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note2Property.Name);
                case ProductImportMappingList.CustomNote3MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note3Property.Name);
                case ProductImportMappingList.CustomNote4MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note4Property.Name);
                case ProductImportMappingList.CustomNote5MapId: return String.Format("{0}{1}.{2}", prefix, Product.CustomAttributesProperty.Name, CustomAttributeData.Note5Property.Name);

                #endregion

                default: throw new NotImplementedException();
            }


        }

        /// <summary>
        /// Static method for returning a column group from its mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public static String GetColumnGroup(Int32 mappingId)
        {
            String detailsGroup = Message.ProductImportMappingList_ColGroup_Details;
            String attributesGroup = Message.ProductImportMappingList_ColGroup_Attributes;
            String sizeGroup = Message.ProductImportMappingList_ColGroup_Size;
            String placementsGroup = Message.ProductImportMappingList_ColGroup_Placements;
            String customTextGroup = Message.ProductImportMapping_CustomTextGroup;
            String customValueGroup = Message.ProductImportMapping_CustomValueGroup;
            String customFlagGroup = Message.ProductImportMapping_CustomFlagGroup;
            String customDateGroup = Message.ProductImportMapping_CustomDateGroup;
            String customNoteGroup = Message.ProductImportMapping_CustomNoteGroup;
            String noGroup = null;

            switch (mappingId)
            {
                #region Product Properties

                case ProductImportMappingList.AlternateDepthMapId: return sizeGroup;
                case ProductImportMappingList.AlternateHeightMapId: return sizeGroup;
                case ProductImportMappingList.AlternateWidthMapId: return sizeGroup;

                case ProductImportMappingList.CanBreakTrayBackMapId: return placementsGroup;
                case ProductImportMappingList.CanBreakTrayDownMapId: return placementsGroup;
                case ProductImportMappingList.CanBreakTrayTopMapId: return placementsGroup;
                case ProductImportMappingList.CanBreakTrayUpMapId: return placementsGroup;
                //case ProductImportMappingList.CanMerchandiseInChestMapId: return placementsGroup;
                //case ProductImportMappingList.CanMerchandiseOnBarMapId: return placementsGroup;
                //case ProductImportMappingList.CanMerchandiseOnPegMapId: return placementsGroup;
                //case ProductImportMappingList.CanMerchandiseOnShelfMapId: return placementsGroup;
                case ProductImportMappingList.CasePackUnitsMapId: return sizeGroup;
                case ProductImportMappingList.DepthMapId: return sizeGroup;
                case ProductImportMappingList.DisplayDepthMapId: return sizeGroup;
                case ProductImportMappingList.DisplayHeightMapId: return sizeGroup;
                case ProductImportMappingList.DisplayWidthMapId: return sizeGroup;
                case ProductImportMappingList.FingerSpaceAboveMapId: return placementsGroup;
                case ProductImportMappingList.FingerSpaceToTheSideMapId: return placementsGroup;
                case ProductImportMappingList.ForceBottomCapMapId: return placementsGroup;
                case ProductImportMappingList.ForceMiddleCapMapId: return placementsGroup;
                case ProductImportMappingList.FrontOverhangMapId: return placementsGroup;
                case ProductImportMappingList.GroupCodeMapId: return noGroup;
                case ProductImportMappingList.GtinMapId: return noGroup;
                case ProductImportMappingList.HeightMapId: return sizeGroup;
                case ProductImportMappingList.IsActiveMapId: return attributesGroup;
                case ProductImportMappingList.IsFrontOnlyMapId: return placementsGroup;
                case ProductImportMappingList.IsPlaceHolderProductMapId: return attributesGroup;
                case ProductImportMappingList.MaxDeepMapId: return placementsGroup;
                case ProductImportMappingList.MaxRightCapMapId: return placementsGroup;
                case ProductImportMappingList.MaxStackMapId: return placementsGroup;
                case ProductImportMappingList.MaxTopCapMapId: return placementsGroup;
                case ProductImportMappingList.MerchandisingStyleMapId: return placementsGroup;
                case ProductImportMappingList.MinDeepMapId: return placementsGroup;
                case ProductImportMappingList.NameMapId: return noGroup;
                case ProductImportMappingList.NestingDepthMapId: return sizeGroup;
                case ProductImportMappingList.NestingHeightMapId: return sizeGroup;
                case ProductImportMappingList.NestingWidthMapId: return sizeGroup;
                case ProductImportMappingList.NumberOfPegHolesMapId: return placementsGroup;
                case ProductImportMappingList.OrientationTypeMapId: return detailsGroup;
                case ProductImportMappingList.PegDepthMapId: return placementsGroup;
                case ProductImportMappingList.PegProngOffsetXMapId: return placementsGroup;
                case ProductImportMappingList.PegX2MapId: return placementsGroup;
                case ProductImportMappingList.PegX3MapId: return placementsGroup;
                case ProductImportMappingList.PegXMapId: return placementsGroup;
                case ProductImportMappingList.PegY2MapId: return placementsGroup;
                case ProductImportMappingList.PegY3MapId: return placementsGroup;
                case ProductImportMappingList.PegYMapId: return placementsGroup;
                case ProductImportMappingList.PointOfPurchaseDepthMapId: return sizeGroup;
                case ProductImportMappingList.PointOfPurchaseHeightMapId: return sizeGroup;
                case ProductImportMappingList.PointOfPurchaseWidthMapId: return sizeGroup;
                case ProductImportMappingList.SqueezeDepthMapId: return placementsGroup;
                case ProductImportMappingList.SqueezeHeightMapId: return placementsGroup;
                case ProductImportMappingList.SqueezeWidthMapId: return placementsGroup;
                case ProductImportMappingList.StatusTypeMapId: return detailsGroup;
                case ProductImportMappingList.TrayPackUnitsMapId: return sizeGroup;
                case ProductImportMappingList.TrayWideMapId: return sizeGroup;
                case ProductImportMappingList.TrayDeepMapId: return sizeGroup;
                case ProductImportMappingList.TrayHighMapId: return sizeGroup;
                case ProductImportMappingList.TrayDepthMapId: return sizeGroup;
                case ProductImportMappingList.TrayHeightMapId: return sizeGroup;
                case ProductImportMappingList.TrayWidthMapId: return sizeGroup;
                case ProductImportMappingList.TrayThickDepthMapId: return sizeGroup;
                case ProductImportMappingList.TrayThickHeightMapId: return sizeGroup;
                case ProductImportMappingList.TrayThickWidthMapId: return sizeGroup;
                case ProductImportMappingList.WidthMapId: return sizeGroup;
                case ProductImportMappingList.CaseWideMapId: return sizeGroup;
                case ProductImportMappingList.CaseDeepMapId: return sizeGroup;
                case ProductImportMappingList.CaseHighMapId: return sizeGroup;
                case ProductImportMappingList.CaseDepthMapId: return sizeGroup;
                case ProductImportMappingList.CaseHeightMapId: return sizeGroup;
                case ProductImportMappingList.CaseWidthMapId: return sizeGroup;

                #endregion

                #region Product Attribute Properties

                case ProductImportMappingList.HealthMapId: return attributesGroup;
                case ProductImportMappingList.BarCodeMapId: return attributesGroup;
                case ProductImportMappingList.BrandMapId: return attributesGroup;
                case ProductImportMappingList.CaseCostMapId: return detailsGroup;
                case ProductImportMappingList.ColourMapId: return attributesGroup;
                case ProductImportMappingList.ConsumerInformationMapId: return attributesGroup;
                case ProductImportMappingList.CorporateCodeMapId: return detailsGroup;
                case ProductImportMappingList.CostPriceMapId: return detailsGroup;
                case ProductImportMappingList.CountryOfOriginMapId: return attributesGroup;
                case ProductImportMappingList.CountryOfProcessingMapId: return attributesGroup;
                //case ProductImportMappingList.CustomerStatusMapId: return attributesGroup;
                case ProductImportMappingList.DateDiscontinuedMapId: return detailsGroup;
                case ProductImportMappingList.DateEffectiveMapId: return detailsGroup;
                case ProductImportMappingList.DateIntroducedMapId: return detailsGroup;
                case ProductImportMappingList.DeliveryFrequencyDaysMapId: return attributesGroup;
                case ProductImportMappingList.DeliveryMethodMapId: return attributesGroup;
                case ProductImportMappingList.FinancialGroupCodeMapId: return attributesGroup;
                case ProductImportMappingList.FinancialGroupNameMapId: return attributesGroup;
                case ProductImportMappingList.FlavourMapId: return attributesGroup;
                case ProductImportMappingList.GarmentTypeMapId: return attributesGroup;
                case ProductImportMappingList.IsNewMapId: return attributesGroup;
                case ProductImportMappingList.IsPrivateLabelMapId: return detailsGroup;
                case ProductImportMappingList.ManufacturerCodeMapId: return attributesGroup;
                case ProductImportMappingList.ManufacturerMapId: return attributesGroup;
                case ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId: return attributesGroup;
                case ProductImportMappingList.ModelMapId: return attributesGroup;
                case ProductImportMappingList.PackagingShapeMapId: return attributesGroup;
                case ProductImportMappingList.PackagingTypeMapId: return attributesGroup;
                case ProductImportMappingList.PatternMapId: return attributesGroup;
                case ProductImportMappingList.PointOfPurchaseDescriptionMapId: return sizeGroup;
                case ProductImportMappingList.RecommendedRetailPriceMapId: return detailsGroup;
                case ProductImportMappingList.SellPackCountMapId: return attributesGroup;
                case ProductImportMappingList.SellPackDescriptionMapId: return attributesGroup;
                case ProductImportMappingList.SellPriceMapId: return detailsGroup;
                case ProductImportMappingList.ShelfLifeMapId: return attributesGroup;
                case ProductImportMappingList.ShortDescriptionMapId: return detailsGroup;
                case ProductImportMappingList.SizeMapId: return attributesGroup;
                case ProductImportMappingList.StyleNumberMapId: return attributesGroup;
                case ProductImportMappingList.SubCategoryMapId: return attributesGroup;
                case ProductImportMappingList.TaxRateMapId: return detailsGroup;
                case ProductImportMappingList.TextureMapId: return attributesGroup;
                case ProductImportMappingList.UnitOfMeasureMapId: return attributesGroup;
                case ProductImportMappingList.VendorCodeMapId: return attributesGroup;
                case ProductImportMappingList.VendorMapId: return attributesGroup;
                case ProductImportMappingList.CustomerStatusMapId: return attributesGroup;
                case ProductImportMappingList.FillColourMapId: return attributesGroup;
                case ProductImportMappingList.FillPatternTypeMapId: return attributesGroup;
                case ProductImportMappingList.ShapeTypeMapId: return attributesGroup;
                case ProductImportMappingList.ShapeMapId: return attributesGroup;

                #endregion

                #region Custom Properties

                case ProductImportMappingList.CustomText1MapId: return customTextGroup;
                case ProductImportMappingList.CustomText2MapId: return customTextGroup;
                case ProductImportMappingList.CustomText3MapId: return customTextGroup;
                case ProductImportMappingList.CustomText4MapId: return customTextGroup;
                case ProductImportMappingList.CustomText5MapId: return customTextGroup;
                case ProductImportMappingList.CustomText6MapId: return customTextGroup;
                case ProductImportMappingList.CustomText7MapId: return customTextGroup;
                case ProductImportMappingList.CustomText8MapId: return customTextGroup;
                case ProductImportMappingList.CustomText9MapId: return customTextGroup;
                case ProductImportMappingList.CustomText10MapId: return customTextGroup;
                case ProductImportMappingList.CustomText11MapId: return customTextGroup;
                case ProductImportMappingList.CustomText12MapId: return customTextGroup;
                case ProductImportMappingList.CustomText13MapId: return customTextGroup;
                case ProductImportMappingList.CustomText14MapId: return customTextGroup;
                case ProductImportMappingList.CustomText15MapId: return customTextGroup;
                case ProductImportMappingList.CustomText16MapId: return customTextGroup;
                case ProductImportMappingList.CustomText17MapId: return customTextGroup;
                case ProductImportMappingList.CustomText18MapId: return customTextGroup;
                case ProductImportMappingList.CustomText19MapId: return customTextGroup;
                case ProductImportMappingList.CustomText20MapId: return customTextGroup;
                case ProductImportMappingList.CustomText21MapId: return customTextGroup;
                case ProductImportMappingList.CustomText22MapId: return customTextGroup;
                case ProductImportMappingList.CustomText23MapId: return customTextGroup;
                case ProductImportMappingList.CustomText24MapId: return customTextGroup;
                case ProductImportMappingList.CustomText25MapId: return customTextGroup;
                case ProductImportMappingList.CustomText26MapId: return customTextGroup;
                case ProductImportMappingList.CustomText27MapId: return customTextGroup;
                case ProductImportMappingList.CustomText28MapId: return customTextGroup;
                case ProductImportMappingList.CustomText29MapId: return customTextGroup;
                case ProductImportMappingList.CustomText30MapId: return customTextGroup;
                case ProductImportMappingList.CustomText31MapId: return customTextGroup;
                case ProductImportMappingList.CustomText32MapId: return customTextGroup;
                case ProductImportMappingList.CustomText33MapId: return customTextGroup;
                case ProductImportMappingList.CustomText34MapId: return customTextGroup;
                case ProductImportMappingList.CustomText35MapId: return customTextGroup;
                case ProductImportMappingList.CustomText36MapId: return customTextGroup;
                case ProductImportMappingList.CustomText37MapId: return customTextGroup;
                case ProductImportMappingList.CustomText38MapId: return customTextGroup;
                case ProductImportMappingList.CustomText39MapId: return customTextGroup;
                case ProductImportMappingList.CustomText40MapId: return customTextGroup;
                case ProductImportMappingList.CustomText41MapId: return customTextGroup;
                case ProductImportMappingList.CustomText42MapId: return customTextGroup;
                case ProductImportMappingList.CustomText43MapId: return customTextGroup;
                case ProductImportMappingList.CustomText44MapId: return customTextGroup;
                case ProductImportMappingList.CustomText45MapId: return customTextGroup;
                case ProductImportMappingList.CustomText46MapId: return customTextGroup;
                case ProductImportMappingList.CustomText47MapId: return customTextGroup;
                case ProductImportMappingList.CustomText48MapId: return customTextGroup;
                case ProductImportMappingList.CustomText49MapId: return customTextGroup;
                case ProductImportMappingList.CustomText50MapId: return customTextGroup;

                case ProductImportMappingList.CustomValue1MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue2MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue3MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue4MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue5MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue6MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue7MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue8MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue9MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue10MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue11MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue12MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue13MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue14MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue15MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue16MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue17MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue18MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue19MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue20MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue21MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue22MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue23MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue24MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue25MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue26MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue27MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue28MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue29MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue30MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue31MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue32MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue33MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue34MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue35MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue36MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue37MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue38MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue39MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue40MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue41MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue42MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue43MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue44MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue45MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue46MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue47MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue48MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue49MapId: return customValueGroup;
                case ProductImportMappingList.CustomValue50MapId: return customValueGroup;

                case ProductImportMappingList.CustomFlag1MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag2MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag3MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag4MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag5MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag6MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag7MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag8MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag9MapId: return customFlagGroup;
                case ProductImportMappingList.CustomFlag10MapId: return customFlagGroup;

                case ProductImportMappingList.CustomDate1MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate2MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate3MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate4MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate5MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate6MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate7MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate8MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate9MapId: return customDateGroup;
                case ProductImportMappingList.CustomDate10MapId: return customDateGroup;

                case ProductImportMappingList.CustomNote1MapId: return customNoteGroup;
                case ProductImportMappingList.CustomNote2MapId: return customNoteGroup;
                case ProductImportMappingList.CustomNote3MapId: return customNoteGroup;
                case ProductImportMappingList.CustomNote4MapId: return customNoteGroup;
                case ProductImportMappingList.CustomNote5MapId: return customNoteGroup;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        internal static void SetIsSetPropertyByMappingId(Int32 mappingId, ProductIsSetDto productIsSetDto, CustomAttributeDataIsSetDto customAttributeIsSetDto)
        {
            switch (mappingId)
            {
                #region MainProperties

                case ProductImportMappingList.AlternateDepthMapId:
                    productIsSetDto.IsAlternateDepthSet = true;
                    break;
                case ProductImportMappingList.AlternateHeightMapId:
                    productIsSetDto.IsAlternateHeightSet = true;
                    break;
                case ProductImportMappingList.AlternateWidthMapId:
                    productIsSetDto.IsAlternateWidthSet = true;
                    break;
                case ProductImportMappingList.BarCodeMapId:
                    productIsSetDto.IsBarcodeSet = true;
                    break;
                case ProductImportMappingList.BrandMapId:
                    productIsSetDto.IsBrandSet = true;
                    break;
                case ProductImportMappingList.CanBreakTrayBackMapId:
                    productIsSetDto.IsCanBreakTrayBackSet = true;
                    break;
                case ProductImportMappingList.CanBreakTrayDownMapId:
                    productIsSetDto.IsCanBreakTrayDownSet = true;
                    break;
                case ProductImportMappingList.CanBreakTrayTopMapId:
                    productIsSetDto.IsCanBreakTrayTopSet = true;
                    break;
                case ProductImportMappingList.CanBreakTrayUpMapId:
                    productIsSetDto.IsCanBreakTrayUpSet = true;
                    break;
                case ProductImportMappingList.CaseCostMapId:
                    productIsSetDto.IsCaseCostSet = true;
                    break;
                case ProductImportMappingList.CaseHighMapId:
                    productIsSetDto.IsCaseHighSet = true;
                    break;
                case ProductImportMappingList.CaseWideMapId:
                    productIsSetDto.IsCaseWideSet = true;
                    break;
                case ProductImportMappingList.CaseDeepMapId:
                    productIsSetDto.IsCaseDeepSet = true;
                    break;
                case ProductImportMappingList.CaseHeightMapId:
                    productIsSetDto.IsCaseHeightSet = true;
                    break;
                case ProductImportMappingList.CaseWidthMapId:
                    productIsSetDto.IsCaseWidthSet = true;
                    break;
                case ProductImportMappingList.CaseDepthMapId:
                    productIsSetDto.IsCaseDepthSet = true;
                    break;
                case ProductImportMappingList.CasePackUnitsMapId:
                    productIsSetDto.IsCasePackUnitsSet = true;
                    break;
                case ProductImportMappingList.ColourMapId:
                    productIsSetDto.IsColourSet = true;
                    break;
                case ProductImportMappingList.ConsumerInformationMapId:
                    productIsSetDto.IsConsumerInformationSet = true;
                    break;
                case ProductImportMappingList.CorporateCodeMapId:
                    productIsSetDto.IsCorporateCodeSet = true;
                    break;
                case ProductImportMappingList.CostPriceMapId:
                    productIsSetDto.IsCostPriceSet = true;
                    break;
                case ProductImportMappingList.CountryOfOriginMapId:
                    productIsSetDto.IsCountryOfOriginSet = true;
                    break;
                case ProductImportMappingList.CountryOfProcessingMapId:
                    productIsSetDto.IsCountryOfProcessingSet = true;
                    break;
                case ProductImportMappingList.CustomerStatusMapId:
                    productIsSetDto.IsCustomerStatusSet = true;
                    break;
                case ProductImportMappingList.DateDiscontinuedMapId:
                    productIsSetDto.IsDateDiscontinuedSet = true;
                    break;
                case ProductImportMappingList.DateEffectiveMapId:
                    productIsSetDto.IsDateEffectiveSet = true;
                    break;
                case ProductImportMappingList.DateIntroducedMapId:
                    productIsSetDto.IsDateIntroducedSet = true;
                    break;
                case ProductImportMappingList.DeliveryFrequencyDaysMapId:
                    productIsSetDto.IsDeliveryFrequencyDaysSet = true;
                    break;
                case ProductImportMappingList.DeliveryMethodMapId:
                    productIsSetDto.IsDeliveryMethodSet = true;
                    break;
                case ProductImportMappingList.DepthMapId:
                    productIsSetDto.IsDepthSet = true;
                    break;
                case ProductImportMappingList.DisplayDepthMapId:
                    productIsSetDto.IsDisplayDepthSet = true;
                    break;
                case ProductImportMappingList.DisplayHeightMapId:
                    productIsSetDto.IsDisplayHeightSet = true;
                    break;
                case ProductImportMappingList.DisplayWidthMapId:
                    productIsSetDto.IsDisplayWidthSet = true;
                    break;
                case ProductImportMappingList.FillColourMapId:
                    productIsSetDto.IsFillColourSet = true;
                    break;
                case ProductImportMappingList.FillPatternTypeMapId:
                    productIsSetDto.IsFillPatternTypeSet = true;
                    break;
                case ProductImportMappingList.FinancialGroupCodeMapId:
                    productIsSetDto.IsFinancialGroupCodeSet = true;
                    break;
                case ProductImportMappingList.FinancialGroupNameMapId:
                    productIsSetDto.IsFinancialGroupNameSet = true;
                    break;
                case ProductImportMappingList.FingerSpaceAboveMapId:
                    productIsSetDto.IsFingerSpaceAboveSet = true;
                    break;
                case ProductImportMappingList.FingerSpaceToTheSideMapId:
                    productIsSetDto.IsFingerSpaceToTheSideSet = true;
                    break;
                case ProductImportMappingList.FlavourMapId:
                    productIsSetDto.IsFlavourSet = true;
                    break;
                case ProductImportMappingList.ForceBottomCapMapId:
                    productIsSetDto.IsForceBottomCapSet = true;
                    break;
                case ProductImportMappingList.ForceMiddleCapMapId:
                    productIsSetDto.IsForceMiddleCapSet = true;
                    break;
                case ProductImportMappingList.FrontOverhangMapId:
                    productIsSetDto.IsFrontOverhangSet = true;
                    break;
                case ProductImportMappingList.GarmentTypeMapId:
                    productIsSetDto.IsGarmentTypeSet = true;
                    break;
                case ProductImportMappingList.GroupCodeMapId:
                    productIsSetDto.IsProductGroupIdSet = true;
                    break;
                case ProductImportMappingList.GtinMapId:
                    productIsSetDto.IsGtinSet = true;
                    break;
                case ProductImportMappingList.HealthMapId:
                    productIsSetDto.IsHealthSet = true;
                    break;
                case ProductImportMappingList.HeightMapId:
                    productIsSetDto.IsHeightSet = true;
                    break;
                case ProductImportMappingList.IsActiveMapId:
                    productIsSetDto.IsIsActiveSet = true;
                    break;
                case ProductImportMappingList.IsFrontOnlyMapId:
                    productIsSetDto.IsIsFrontOnlySet = true;
                    break;
                case ProductImportMappingList.IsNewMapId:
                    productIsSetDto.IsIsNewProductSet = true;
                    break;
                case ProductImportMappingList.IsPlaceHolderProductMapId:
                    productIsSetDto.IsIsPlaceHolderProductSet = true;
                    break;
                case ProductImportMappingList.IsPrivateLabelMapId:
                    productIsSetDto.IsIsPrivateLabelSet = true;
                    break;
                case ProductImportMappingList.ManufacturerCodeMapId:
                    productIsSetDto.IsManufacturerCodeSet = true;
                    break;
                case ProductImportMappingList.ManufacturerMapId:
                    productIsSetDto.IsManufacturerSet = true;
                    break;
                case ProductImportMappingList.ManufacturerRecommendedRetailPriceMapId:
                    productIsSetDto.IsManufacturerRecommendedRetailPriceSet = true;
                    break;
                case ProductImportMappingList.MaxDeepMapId:
                    productIsSetDto.IsMaxDeepSet = true;
                    break;
                case ProductImportMappingList.MaxRightCapMapId:
                    productIsSetDto.IsMaxRightCapSet = true;
                    break;
                case ProductImportMappingList.MaxStackMapId:
                    productIsSetDto.IsMaxStackSet = true;
                    break;
                case ProductImportMappingList.MaxTopCapMapId:
                    productIsSetDto.IsMaxTopCapSet = true;
                    break;
                case ProductImportMappingList.MerchandisingStyleMapId:
                    productIsSetDto.IsMerchandisingStyleSet = true;
                    break;
                case ProductImportMappingList.MinDeepMapId:
                    productIsSetDto.IsMinDeepSet = true;
                    break;
                case ProductImportMappingList.ModelMapId:
                    productIsSetDto.IsModelSet = true;
                    break;
                case ProductImportMappingList.NameMapId:
                    productIsSetDto.IsNameSet = true;
                    break;
                case ProductImportMappingList.NestingDepthMapId:
                    productIsSetDto.IsNestingDepthSet = true;
                    break;
                case ProductImportMappingList.NestingHeightMapId:
                    productIsSetDto.IsNestingHeightSet = true;
                    break;
                case ProductImportMappingList.NestingWidthMapId:
                    productIsSetDto.IsNestingWidthSet = true;
                    break;
                case ProductImportMappingList.NumberOfPegHolesMapId:
                    productIsSetDto.IsNumberOfPegHolesSet = true;
                    break;
                case ProductImportMappingList.OrientationTypeMapId:
                    productIsSetDto.IsOrientationTypeSet = true;
                    break;
                case ProductImportMappingList.PackagingShapeMapId:
                    productIsSetDto.IsPackagingShapeSet = true;
                    break;
                case ProductImportMappingList.PackagingTypeMapId:
                    productIsSetDto.IsPackagingTypeSet = true;
                    break;
                case ProductImportMappingList.PatternMapId:
                    productIsSetDto.IsPatternSet = true;
                    break;
                case ProductImportMappingList.PegDepthMapId:
                    productIsSetDto.IsPegDepthSet = true;
                    break;
                case ProductImportMappingList.PegProngOffsetXMapId:
                    productIsSetDto.IsPegProngOffsetXSet = true;
                    break;
                case ProductImportMappingList.PegX2MapId:
                    productIsSetDto.IsPegX2Set = true;
                    break;
                case ProductImportMappingList.PegX3MapId:
                    productIsSetDto.IsPegX3Set = true;
                    break;
                case ProductImportMappingList.PegXMapId:
                    productIsSetDto.IsPegXSet = true;
                    break;
                case ProductImportMappingList.PegY2MapId:
                    productIsSetDto.IsPegY2Set = true;
                    break;
                case ProductImportMappingList.PegY3MapId:
                    productIsSetDto.IsPegY3Set = true;
                    break;
                case ProductImportMappingList.PegYMapId:
                    productIsSetDto.IsPegYSet = true;
                    break;
                case ProductImportMappingList.PointOfPurchaseDepthMapId:
                    productIsSetDto.IsPointOfPurchaseDepthSet = true;
                    break;
                case ProductImportMappingList.PointOfPurchaseDescriptionMapId:
                    productIsSetDto.IsPointOfPurchaseDescriptionSet = true;
                    break;
                case ProductImportMappingList.PointOfPurchaseHeightMapId:
                    productIsSetDto.IsPointOfPurchaseHeightSet = true;
                    break;
                case ProductImportMappingList.PointOfPurchaseWidthMapId:
                    productIsSetDto.IsPointOfPurchaseWidthSet = true;
                    break;
                case ProductImportMappingList.RecommendedRetailPriceMapId:
                    productIsSetDto.IsRecommendedRetailPriceSet = true;
                    break;
                case ProductImportMappingList.SellPackCountMapId:
                    productIsSetDto.IsSellPackCountSet = true;
                    break;
                case ProductImportMappingList.SellPackDescriptionMapId:
                    productIsSetDto.IsSellPackDescriptionSet = true;
                    break;
                case ProductImportMappingList.SellPriceMapId:
                    productIsSetDto.IsSellPriceSet = true;
                    break;
                case ProductImportMappingList.ShelfLifeMapId:
                    productIsSetDto.IsShelfLifeSet = true;
                    break;
                case ProductImportMappingList.ShortDescriptionMapId:
                    productIsSetDto.IsShortDescriptionSet = true;
                    break;
                case ProductImportMappingList.SizeMapId:
                    productIsSetDto.IsSizeSet = true;
                    break;
                case ProductImportMappingList.ShapeMapId:
                    productIsSetDto.IsShapeSet = true;
                    break;
                case ProductImportMappingList.ShapeTypeMapId:
                    productIsSetDto.IsShapeTypeSet = true;
                    break;
                case ProductImportMappingList.SqueezeDepthMapId:
                    productIsSetDto.IsSqueezeDepthSet = true;
                    break;
                case ProductImportMappingList.SqueezeHeightMapId:
                    productIsSetDto.IsSqueezeHeightSet = true;
                    break;
                case ProductImportMappingList.SqueezeWidthMapId:
                    productIsSetDto.IsSqueezeWidthSet = true;
                    break;
                case ProductImportMappingList.StatusTypeMapId:
                    productIsSetDto.IsStatusTypeSet = true;
                    break;
                case ProductImportMappingList.StyleNumberMapId:
                    productIsSetDto.IsStyleNumberSet = true;
                    break;
                case ProductImportMappingList.SubCategoryMapId:
                    productIsSetDto.IsSubcategorySet = true;
                    break;
                case ProductImportMappingList.TaxRateMapId:
                    productIsSetDto.IsTaxRateSet = true;
                    break;
                case ProductImportMappingList.TextureMapId:
                    productIsSetDto.IsTextureSet = true;
                    break;
                case ProductImportMappingList.TrayDeepMapId:
                    productIsSetDto.IsTrayDeepSet = true;
                    break;
                case ProductImportMappingList.TrayHighMapId:
                    productIsSetDto.IsTrayHighSet = true;
                    break;
                case ProductImportMappingList.TrayThickDepthMapId:
                    productIsSetDto.IsTrayThickDepthSet = true;
                    break;
                case ProductImportMappingList.TrayThickHeightMapId:
                    productIsSetDto.IsTrayThickHeightSet = true;
                    break;
                case ProductImportMappingList.TrayThickWidthMapId:
                    productIsSetDto.IsTrayThickWidthSet = true;
                    break;
                case ProductImportMappingList.TrayWideMapId:
                    productIsSetDto.IsTrayWideSet = true;
                    break;
                case ProductImportMappingList.UnitOfMeasureMapId:
                    productIsSetDto.IsUnitOfMeasureSet = true;
                    break;
                case ProductImportMappingList.VendorCodeMapId:
                    productIsSetDto.IsVendorCodeSet = true;
                    break;
                case ProductImportMappingList.VendorMapId:
                    productIsSetDto.IsVendorSet = true;
                    break;
                case ProductImportMappingList.WidthMapId:
                    productIsSetDto.IsWidthSet = true;
                    break;
                case ProductImportMappingList.TrayWidthMapId:
                    productIsSetDto.IsTrayWidthSet = true;
                    break;
                case ProductImportMappingList.TrayHeightMapId:
                    productIsSetDto.IsTrayHeightSet = true;
                    break;
                case ProductImportMappingList.TrayDepthMapId:
                    productIsSetDto.IsTrayDepthSet = true;
                    break;
                case ProductImportMappingList.TrayPackUnitsMapId:
                    productIsSetDto.IsTrayPackUnitsSet = true;
                    break;

                #endregion

                #region Custom Properties

                case ProductImportMappingList.CustomText1MapId: customAttributeIsSetDto.IsText1Set = true; break;
                case ProductImportMappingList.CustomText2MapId: customAttributeIsSetDto.IsText2Set = true; break;
                case ProductImportMappingList.CustomText3MapId: customAttributeIsSetDto.IsText3Set = true; break;
                case ProductImportMappingList.CustomText4MapId: customAttributeIsSetDto.IsText4Set = true; break;
                case ProductImportMappingList.CustomText5MapId: customAttributeIsSetDto.IsText5Set = true; break;
                case ProductImportMappingList.CustomText6MapId: customAttributeIsSetDto.IsText6Set = true; break;
                case ProductImportMappingList.CustomText7MapId: customAttributeIsSetDto.IsText7Set = true; break;
                case ProductImportMappingList.CustomText8MapId: customAttributeIsSetDto.IsText8Set = true; break;
                case ProductImportMappingList.CustomText9MapId: customAttributeIsSetDto.IsText9Set = true; break;
                case ProductImportMappingList.CustomText10MapId: customAttributeIsSetDto.IsText10Set = true; break;
                case ProductImportMappingList.CustomText11MapId: customAttributeIsSetDto.IsText11Set = true; break;
                case ProductImportMappingList.CustomText12MapId: customAttributeIsSetDto.IsText12Set = true; break;
                case ProductImportMappingList.CustomText13MapId: customAttributeIsSetDto.IsText13Set = true; break;
                case ProductImportMappingList.CustomText14MapId: customAttributeIsSetDto.IsText14Set = true; break;
                case ProductImportMappingList.CustomText15MapId: customAttributeIsSetDto.IsText15Set = true; break;
                case ProductImportMappingList.CustomText16MapId: customAttributeIsSetDto.IsText16Set = true; break;
                case ProductImportMappingList.CustomText17MapId: customAttributeIsSetDto.IsText17Set = true; break;
                case ProductImportMappingList.CustomText18MapId: customAttributeIsSetDto.IsText18Set = true; break;
                case ProductImportMappingList.CustomText19MapId: customAttributeIsSetDto.IsText19Set = true; break;
                case ProductImportMappingList.CustomText20MapId: customAttributeIsSetDto.IsText20Set = true; break;
                case ProductImportMappingList.CustomText21MapId: customAttributeIsSetDto.IsText21Set = true; break;
                case ProductImportMappingList.CustomText22MapId: customAttributeIsSetDto.IsText22Set = true; break;
                case ProductImportMappingList.CustomText23MapId: customAttributeIsSetDto.IsText23Set = true; break;
                case ProductImportMappingList.CustomText24MapId: customAttributeIsSetDto.IsText24Set = true; break;
                case ProductImportMappingList.CustomText25MapId: customAttributeIsSetDto.IsText25Set = true; break;
                case ProductImportMappingList.CustomText26MapId: customAttributeIsSetDto.IsText26Set = true; break;
                case ProductImportMappingList.CustomText27MapId: customAttributeIsSetDto.IsText27Set = true; break;
                case ProductImportMappingList.CustomText28MapId: customAttributeIsSetDto.IsText28Set = true; break;
                case ProductImportMappingList.CustomText29MapId: customAttributeIsSetDto.IsText29Set = true; break;
                case ProductImportMappingList.CustomText30MapId: customAttributeIsSetDto.IsText30Set = true; break;
                case ProductImportMappingList.CustomText31MapId: customAttributeIsSetDto.IsText31Set = true; break;
                case ProductImportMappingList.CustomText32MapId: customAttributeIsSetDto.IsText32Set = true; break;
                case ProductImportMappingList.CustomText33MapId: customAttributeIsSetDto.IsText33Set = true; break;
                case ProductImportMappingList.CustomText34MapId: customAttributeIsSetDto.IsText34Set = true; break;
                case ProductImportMappingList.CustomText35MapId: customAttributeIsSetDto.IsText35Set = true; break;
                case ProductImportMappingList.CustomText36MapId: customAttributeIsSetDto.IsText36Set = true; break;
                case ProductImportMappingList.CustomText37MapId: customAttributeIsSetDto.IsText37Set = true; break;
                case ProductImportMappingList.CustomText38MapId: customAttributeIsSetDto.IsText38Set = true; break;
                case ProductImportMappingList.CustomText39MapId: customAttributeIsSetDto.IsText39Set = true; break;
                case ProductImportMappingList.CustomText40MapId: customAttributeIsSetDto.IsText40Set = true; break;
                case ProductImportMappingList.CustomText41MapId: customAttributeIsSetDto.IsText41Set = true; break;
                case ProductImportMappingList.CustomText42MapId: customAttributeIsSetDto.IsText42Set = true; break;
                case ProductImportMappingList.CustomText43MapId: customAttributeIsSetDto.IsText43Set = true; break;
                case ProductImportMappingList.CustomText44MapId: customAttributeIsSetDto.IsText44Set = true; break;
                case ProductImportMappingList.CustomText45MapId: customAttributeIsSetDto.IsText45Set = true; break;
                case ProductImportMappingList.CustomText46MapId: customAttributeIsSetDto.IsText46Set = true; break;
                case ProductImportMappingList.CustomText47MapId: customAttributeIsSetDto.IsText47Set = true; break;
                case ProductImportMappingList.CustomText48MapId: customAttributeIsSetDto.IsText48Set = true; break;
                case ProductImportMappingList.CustomText49MapId: customAttributeIsSetDto.IsText49Set = true; break;
                case ProductImportMappingList.CustomText50MapId: customAttributeIsSetDto.IsText50Set = true; break;

                case ProductImportMappingList.CustomValue1MapId: customAttributeIsSetDto.IsValue1Set = true; break;
                case ProductImportMappingList.CustomValue2MapId: customAttributeIsSetDto.IsValue2Set = true; break;
                case ProductImportMappingList.CustomValue3MapId: customAttributeIsSetDto.IsValue3Set = true; break;
                case ProductImportMappingList.CustomValue4MapId: customAttributeIsSetDto.IsValue4Set = true; break;
                case ProductImportMappingList.CustomValue5MapId: customAttributeIsSetDto.IsValue5Set = true; break;
                case ProductImportMappingList.CustomValue6MapId: customAttributeIsSetDto.IsValue6Set = true; break;
                case ProductImportMappingList.CustomValue7MapId: customAttributeIsSetDto.IsValue7Set = true; break;
                case ProductImportMappingList.CustomValue8MapId: customAttributeIsSetDto.IsValue8Set = true; break;
                case ProductImportMappingList.CustomValue9MapId: customAttributeIsSetDto.IsValue9Set = true; break;
                case ProductImportMappingList.CustomValue10MapId: customAttributeIsSetDto.IsValue10Set = true; break;
                case ProductImportMappingList.CustomValue11MapId: customAttributeIsSetDto.IsValue11Set = true; break;
                case ProductImportMappingList.CustomValue12MapId: customAttributeIsSetDto.IsValue12Set = true; break;
                case ProductImportMappingList.CustomValue13MapId: customAttributeIsSetDto.IsValue13Set = true; break;
                case ProductImportMappingList.CustomValue14MapId: customAttributeIsSetDto.IsValue14Set = true; break;
                case ProductImportMappingList.CustomValue15MapId: customAttributeIsSetDto.IsValue15Set = true; break;
                case ProductImportMappingList.CustomValue16MapId: customAttributeIsSetDto.IsValue16Set = true; break;
                case ProductImportMappingList.CustomValue17MapId: customAttributeIsSetDto.IsValue17Set = true; break;
                case ProductImportMappingList.CustomValue18MapId: customAttributeIsSetDto.IsValue18Set = true; break;
                case ProductImportMappingList.CustomValue19MapId: customAttributeIsSetDto.IsValue19Set = true; break;
                case ProductImportMappingList.CustomValue20MapId: customAttributeIsSetDto.IsValue20Set = true; break;
                case ProductImportMappingList.CustomValue21MapId: customAttributeIsSetDto.IsValue21Set = true; break;
                case ProductImportMappingList.CustomValue22MapId: customAttributeIsSetDto.IsValue22Set = true; break;
                case ProductImportMappingList.CustomValue23MapId: customAttributeIsSetDto.IsValue23Set = true; break;
                case ProductImportMappingList.CustomValue24MapId: customAttributeIsSetDto.IsValue24Set = true; break;
                case ProductImportMappingList.CustomValue25MapId: customAttributeIsSetDto.IsValue25Set = true; break;
                case ProductImportMappingList.CustomValue26MapId: customAttributeIsSetDto.IsValue26Set = true; break;
                case ProductImportMappingList.CustomValue27MapId: customAttributeIsSetDto.IsValue27Set = true; break;
                case ProductImportMappingList.CustomValue28MapId: customAttributeIsSetDto.IsValue28Set = true; break;
                case ProductImportMappingList.CustomValue29MapId: customAttributeIsSetDto.IsValue29Set = true; break;
                case ProductImportMappingList.CustomValue30MapId: customAttributeIsSetDto.IsValue30Set = true; break;
                case ProductImportMappingList.CustomValue31MapId: customAttributeIsSetDto.IsValue31Set = true; break;
                case ProductImportMappingList.CustomValue32MapId: customAttributeIsSetDto.IsValue32Set = true; break;
                case ProductImportMappingList.CustomValue33MapId: customAttributeIsSetDto.IsValue33Set = true; break;
                case ProductImportMappingList.CustomValue34MapId: customAttributeIsSetDto.IsValue34Set = true; break;
                case ProductImportMappingList.CustomValue35MapId: customAttributeIsSetDto.IsValue35Set = true; break;
                case ProductImportMappingList.CustomValue36MapId: customAttributeIsSetDto.IsValue36Set = true; break;
                case ProductImportMappingList.CustomValue37MapId: customAttributeIsSetDto.IsValue37Set = true; break;
                case ProductImportMappingList.CustomValue38MapId: customAttributeIsSetDto.IsValue38Set = true; break;
                case ProductImportMappingList.CustomValue39MapId: customAttributeIsSetDto.IsValue39Set = true; break;
                case ProductImportMappingList.CustomValue40MapId: customAttributeIsSetDto.IsValue40Set = true; break;
                case ProductImportMappingList.CustomValue41MapId: customAttributeIsSetDto.IsValue41Set = true; break;
                case ProductImportMappingList.CustomValue42MapId: customAttributeIsSetDto.IsValue42Set = true; break;
                case ProductImportMappingList.CustomValue43MapId: customAttributeIsSetDto.IsValue43Set = true; break;
                case ProductImportMappingList.CustomValue44MapId: customAttributeIsSetDto.IsValue44Set = true; break;
                case ProductImportMappingList.CustomValue45MapId: customAttributeIsSetDto.IsValue45Set = true; break;
                case ProductImportMappingList.CustomValue46MapId: customAttributeIsSetDto.IsValue46Set = true; break;
                case ProductImportMappingList.CustomValue47MapId: customAttributeIsSetDto.IsValue47Set = true; break;
                case ProductImportMappingList.CustomValue48MapId: customAttributeIsSetDto.IsValue48Set = true; break;
                case ProductImportMappingList.CustomValue49MapId: customAttributeIsSetDto.IsValue49Set = true; break;
                case ProductImportMappingList.CustomValue50MapId: customAttributeIsSetDto.IsValue50Set = true; break;

                case ProductImportMappingList.CustomFlag1MapId: customAttributeIsSetDto.IsFlag1Set = true; break;
                case ProductImportMappingList.CustomFlag2MapId: customAttributeIsSetDto.IsFlag2Set = true; break;
                case ProductImportMappingList.CustomFlag3MapId: customAttributeIsSetDto.IsFlag3Set = true; break;
                case ProductImportMappingList.CustomFlag4MapId: customAttributeIsSetDto.IsFlag4Set = true; break;
                case ProductImportMappingList.CustomFlag5MapId: customAttributeIsSetDto.IsFlag5Set = true; break;
                case ProductImportMappingList.CustomFlag6MapId: customAttributeIsSetDto.IsFlag6Set = true; break;
                case ProductImportMappingList.CustomFlag7MapId: customAttributeIsSetDto.IsFlag7Set = true; break;
                case ProductImportMappingList.CustomFlag8MapId: customAttributeIsSetDto.IsFlag8Set = true; break;
                case ProductImportMappingList.CustomFlag9MapId: customAttributeIsSetDto.IsFlag9Set = true; break;
                case ProductImportMappingList.CustomFlag10MapId: customAttributeIsSetDto.IsFlag10Set = true; break;

                case ProductImportMappingList.CustomDate1MapId: customAttributeIsSetDto.IsDate1Set = true; break;
                case ProductImportMappingList.CustomDate2MapId: customAttributeIsSetDto.IsDate2Set = true; break;
                case ProductImportMappingList.CustomDate3MapId: customAttributeIsSetDto.IsDate3Set = true; break;
                case ProductImportMappingList.CustomDate4MapId: customAttributeIsSetDto.IsDate4Set = true; break;
                case ProductImportMappingList.CustomDate5MapId: customAttributeIsSetDto.IsDate5Set = true; break;
                case ProductImportMappingList.CustomDate6MapId: customAttributeIsSetDto.IsDate6Set = true; break;
                case ProductImportMappingList.CustomDate7MapId: customAttributeIsSetDto.IsDate7Set = true; break;
                case ProductImportMappingList.CustomDate8MapId: customAttributeIsSetDto.IsDate8Set = true; break;
                case ProductImportMappingList.CustomDate9MapId: customAttributeIsSetDto.IsDate9Set = true; break;
                case ProductImportMappingList.CustomDate10MapId: customAttributeIsSetDto.IsDate10Set = true; break;

                case ProductImportMappingList.CustomNote1MapId: customAttributeIsSetDto.IsNote1Set = true; break;
                case ProductImportMappingList.CustomNote2MapId: customAttributeIsSetDto.IsNote2Set = true; break;
                case ProductImportMappingList.CustomNote3MapId: customAttributeIsSetDto.IsNote3Set = true; break;
                case ProductImportMappingList.CustomNote4MapId: customAttributeIsSetDto.IsNote4Set = true; break;
                case ProductImportMappingList.CustomNote5MapId: customAttributeIsSetDto.IsNote5Set = true; break;
                    #endregion
            }
        }

        /// <summary>
        /// Returns true if the field with the given mapping id should have a display uom converter applied.
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public static Boolean IsUOMDisplayConversionRequired(Int32 mappingId)
        {
            switch (mappingId)
            {
                case ProductImportMappingList.HeightMapId:
                case ProductImportMappingList.WidthMapId:
                case ProductImportMappingList.DepthMapId:
                case ProductImportMappingList.DisplayHeightMapId:
                case ProductImportMappingList.DisplayWidthMapId:
                case ProductImportMappingList.DisplayDepthMapId:
                case ProductImportMappingList.AlternateHeightMapId:
                case ProductImportMappingList.AlternateWidthMapId:
                case ProductImportMappingList.AlternateDepthMapId:
                case ProductImportMappingList.PointOfPurchaseHeightMapId:
                case ProductImportMappingList.PointOfPurchaseWidthMapId:
                case ProductImportMappingList.PointOfPurchaseDepthMapId:
                case ProductImportMappingList.PegProngOffsetXMapId:
                case ProductImportMappingList.PegDepthMapId:
                case ProductImportMappingList.SqueezeHeightMapId:
                case ProductImportMappingList.SqueezeWidthMapId:
                case ProductImportMappingList.SqueezeDepthMapId:
                case ProductImportMappingList.NestingHeightMapId:
                case ProductImportMappingList.NestingWidthMapId:
                case ProductImportMappingList.NestingDepthMapId:
                case ProductImportMappingList.TrayThickHeightMapId:
                case ProductImportMappingList.TrayThickWidthMapId:
                case ProductImportMappingList.TrayThickDepthMapId:
                case ProductImportMappingList.FrontOverhangMapId:
                case ProductImportMappingList.FingerSpaceAboveMapId:
                case ProductImportMappingList.FingerSpaceToTheSideMapId:
                case ProductImportMappingList.TrayHeightMapId:
                case ProductImportMappingList.TrayWidthMapId:
                case ProductImportMappingList.TrayDepthMapId:
                case ProductImportMappingList.CaseHeightMapId:
                case ProductImportMappingList.CaseWidthMapId:
                case ProductImportMappingList.CaseDepthMapId:
                    return true;


                default: return false;
            }

        }

        #endregion

        #region IModelObjectColumnHelper Members

        //Boolean IModelObjectColumnHelper.IsUOMDisplayConversionRequired(Int32 mappingId)
        //{
        //    return ProductImportMappingList.IsUOMDisplayConversionRequired(mappingId);
        //}

        /// <summary>
        /// Implementation of the GetColumnGroupName method
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns></returns>
        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mapping)
        {
            return ProductImportMappingList.GetColumnGroup(mapping);
        }

        #endregion

    }
}
