﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
// V8-24264 : A.Probyn
//	Extended for display type on import mapping constructor.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Globalization;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class LocationSpaceImportMappingList : ImportMappingList<LocationSpaceImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        public const Int32 LocationCodeMappingId = 0;
        public const Int32 ProductGroupCodeMappingId = 1;
        public const Int32 BayCountMappingId = 2;
        public const Int32 ProductCountMappingId = 3;
        public const Int32 AverageBayWidthMappingId = 4;
        #endregion

        #region Constructors
        private LocationSpaceImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static LocationSpaceImportMappingList NewLocationSpaceImportMappingList()
        {
            LocationSpaceImportMappingList list = new LocationSpaceImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region LocationSpaceBayProperties

            this.Add(ImportMapping.NewImportMapping(LocationCodeMappingId, Message.LocationSpaceImportList_LocationCode, Message.LocationSpaceImportList_LocationCode_Description, true, /*identifier*/true, Location.CodeProperty.Type, Location.CodeProperty.DefaultValue, 0, 50, false, Location.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ProductGroupCodeMappingId, Message.LocationSpaceImportList_ProductGroupCode, Message.LocationSpaceImportList_ProductGroupCode_Description, true, /*identifier*/true, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 1, 50, false, ProductGroup.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BayCountMappingId, LocationSpaceProductGroup.BayCountProperty.FriendlyName, LocationSpaceProductGroup.BayCountProperty.Description, true, LocationSpaceProductGroup.BayCountProperty.Type, LocationSpaceProductGroup.BayCountProperty.DefaultValue, 0, Byte.MaxValue, true, LocationSpaceProductGroup.BayCountProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ProductCountMappingId, LocationSpaceProductGroup.ProductCountProperty.FriendlyName, LocationSpaceProductGroup.ProductCountProperty.Description, false, LocationSpaceProductGroup.ProductCountProperty.Type, LocationSpaceProductGroup.ProductCountProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceProductGroup.ProductCountProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(AverageBayWidthMappingId, LocationSpaceProductGroup.AverageBayWidthProperty.FriendlyName, LocationSpaceProductGroup.AverageBayWidthProperty.Description, true, LocationSpaceProductGroup.AverageBayWidthProperty.Type, LocationSpaceProductGroup.AverageBayWidthProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceProductGroup.AverageBayWidthProperty.DisplayType));
            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, LocationSpaceProductGroupDto locationSpaceProductGroupDto,
            String locationCode, IEnumerable<ProductGroupDto> productGroupDtoList)
        {
            switch (mappingId)
            {
                #region Location Space Bay Properties
                case LocationSpaceImportMappingList.LocationCodeMappingId: return locationCode;
                case LocationSpaceImportMappingList.ProductGroupCodeMappingId:
                    ProductGroupDto groupDto = productGroupDtoList.FirstOrDefault(p => p.Id == locationSpaceProductGroupDto.ProductGroupId);
                    return (groupDto != null) ? groupDto.Code : null;
                case LocationSpaceImportMappingList.BayCountMappingId: return locationSpaceProductGroupDto.BayCount;
                case LocationSpaceImportMappingList.ProductCountMappingId: return locationSpaceProductGroupDto.ProductCount;
                case LocationSpaceImportMappingList.AverageBayWidthMappingId: return locationSpaceProductGroupDto.AverageBayWidth;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="locationDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            LocationSpaceProductGroupDto locationSpaceProductGroupDto)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            switch (mappingId)
            {
                case LocationSpaceImportMappingList.BayCountMappingId:
                    locationSpaceProductGroupDto.BayCount = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceImportMappingList.AverageBayWidthMappingId:
                    locationSpaceProductGroupDto.AverageBayWidth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceImportMappingList.ProductCountMappingId:
                    locationSpaceProductGroupDto.ProductCount = Convert.ToInt32(cellValue, prov);
                    break;
                default:
                    //Do nothing
                    break;
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override String GetBindingPath(Int32 mappingId)
        {
            switch (mappingId)
            {
                #region Location Space Properties
                case LocationSpaceImportMappingList.LocationCodeMappingId: return Location.CodeProperty.Name;
                case LocationSpaceImportMappingList.ProductGroupCodeMappingId: return ProductGroup.CodeProperty.Name;
                case LocationSpaceImportMappingList.BayCountMappingId: return LocationSpaceProductGroup.BayCountProperty.Name;
                case LocationSpaceImportMappingList.ProductCountMappingId: return LocationSpaceProductGroup.ProductCountProperty.Name;
                case LocationSpaceImportMappingList.AverageBayWidthMappingId: return LocationSpaceProductGroup.AverageBayWidthProperty.Name;
                #endregion

                default: throw new NotImplementedException();
            }
        }


        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;

            switch (mappingId)
            {
                #region Location Space Bay Properties
                case LocationSpaceImportMappingList.LocationCodeMappingId: return String.Format("{0}{1}", prefix, Location.CodeProperty.Name);
                case LocationSpaceImportMappingList.ProductGroupCodeMappingId: return String.Format("{0}{1}", prefix, ProductGroup.CodeProperty.Name);
                case LocationSpaceImportMappingList.BayCountMappingId: return String.Format("{0}{1}", prefix, LocationSpaceProductGroup.BayCountProperty.Name);
                case LocationSpaceImportMappingList.ProductCountMappingId: return String.Format("{0}{1}", prefix, LocationSpaceProductGroup.ProductCountProperty.Name);
                case LocationSpaceImportMappingList.AverageBayWidthMappingId: return String.Format("{0}{1}", prefix, LocationSpaceProductGroup.AverageBayWidthProperty.Name);
                #endregion

                default: throw new NotImplementedException();
            }
        }

        public static Boolean IsUOMDisplayConversionRequired(Int32 mappingId)
        {
            if (mappingId == AverageBayWidthMappingId)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region IModelObjectColumnHelper members

        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mappingId)
        {
            //does this have any groups?
            return null;
        }

        //Boolean IModelObjectColumnHelper.IsUOMDisplayConversionRequired(Int32 mappingId)
        //{
        //    return LocationSpaceImportMappingList.IsUOMDisplayConversionRequired(mappingId);
        //}


        #endregion
    }
}
