﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
// V8-24264 : A.Probyn
//	Extended for display type on import mapping constructor.
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Globalization;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class LocationSpaceElementImportMappingList : ImportMappingList<LocationSpaceElementImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        public const Int32 LocationCodeMappingId = 0;
        public const Int32 ProductGroupCodeMappingId = 1;
        public const Int32 BayOrderMappingId = 2;
        public const Int32 OrderMappingId = 3;
        public const Int32 XPositionMappingId = 4;
        public const Int32 YPositionMappingId = 5;
        public const Int32 ZPositionMappingId = 6;
        public const Int32 MerchandisableHeightMappingId = 7;
        public const Int32 HeightMappingId = 8;
        public const Int32 WidthMappingId = 9;
        public const Int32 DepthMappingId = 10;
        public const Int32 FaceThicknessMappingId = 11;
        public const Int32 NotchNumberMappingId = 12;
        #endregion

        #region Constructors
        private LocationSpaceElementImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static LocationSpaceElementImportMappingList NewLocationSpaceElementImportMappingList()
        {
            LocationSpaceElementImportMappingList list = new LocationSpaceElementImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region LocationSpaceElementProperties

            this.Add(ImportMapping.NewImportMapping(LocationCodeMappingId, Message.LocationSpaceElementImportList_LocationCode, Message.LocationSpaceElementImportList_LocationCode_Description, true, /*identifier*/true, Location.CodeProperty.Type, Location.CodeProperty.DefaultValue, 0, 50, false, Location.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ProductGroupCodeMappingId, Message.LocationSpaceElementImportList_ProductGroupCode, Message.LocationSpaceElementImportList_ProductGroupCode_Description, true, /*identifier*/true, ProductGroup.CodeProperty.Type, ProductGroup.CodeProperty.DefaultValue, 1, 50, false, ProductGroup.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(BayOrderMappingId, Message.LocationSpaceElementImportList_BayNumber, LocationSpaceBay.OrderProperty.Description, true, /*identifier*/true, LocationSpaceBay.OrderProperty.Type, LocationSpaceBay.OrderProperty.DefaultValue, 0, Byte.MaxValue, true, LocationSpaceBay.OrderProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(OrderMappingId, LocationSpaceElement.OrderProperty.FriendlyName, LocationSpaceElement.OrderProperty.Description, true, /*identifier*/true, LocationSpaceElement.OrderProperty.Type, LocationSpaceElement.OrderProperty.DefaultValue, 0, Byte.MaxValue, true, LocationSpaceElement.OrderProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HeightMappingId, LocationSpaceElement.HeightProperty.FriendlyName, LocationSpaceElement.HeightProperty.Description, false, LocationSpaceElement.HeightProperty.Type, LocationSpaceElement.HeightProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceElement.HeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(WidthMappingId, LocationSpaceElement.WidthProperty.FriendlyName, LocationSpaceElement.WidthProperty.Description, false, LocationSpaceElement.WidthProperty.Type, LocationSpaceElement.WidthProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceElement.WidthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DepthMappingId, LocationSpaceElement.DepthProperty.FriendlyName, LocationSpaceElement.DepthProperty.Description, false, LocationSpaceElement.DepthProperty.Type, LocationSpaceElement.DepthProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceElement.DepthProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(MerchandisableHeightMappingId, LocationSpaceElement.MerchandisableHeightProperty.FriendlyName, LocationSpaceElement.MerchandisableHeightProperty.Description, false, LocationSpaceElement.MerchandisableHeightProperty.Type, LocationSpaceElement.MerchandisableHeightProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceElement.MerchandisableHeightProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(XPositionMappingId, LocationSpaceElement.XProperty.FriendlyName, LocationSpaceElement.XProperty.Description, false, LocationSpaceElement.XProperty.Type, LocationSpaceElement.XProperty.DefaultValue, -99999, 99999, true, LocationSpaceElement.XProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(YPositionMappingId, LocationSpaceElement.YProperty.FriendlyName, LocationSpaceElement.YProperty.Description, false, LocationSpaceElement.YProperty.Type, LocationSpaceElement.YProperty.DefaultValue, -99999, 99999, true, LocationSpaceElement.YProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ZPositionMappingId, LocationSpaceElement.ZProperty.FriendlyName, LocationSpaceElement.ZProperty.Description, false, LocationSpaceElement.ZProperty.Type, LocationSpaceElement.ZProperty.DefaultValue, -99999, 99999, true, LocationSpaceElement.ZProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FaceThicknessMappingId, LocationSpaceElement.FaceThicknessProperty.FriendlyName, LocationSpaceElement.FaceThicknessProperty.Description, false, LocationSpaceElement.FaceThicknessProperty.Type, LocationSpaceElement.FaceThicknessProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceElement.FaceThicknessProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(NotchNumberMappingId, LocationSpaceElement.NotchNumberProperty.FriendlyName, LocationSpaceElement.NotchNumberProperty.Description, false, LocationSpaceElement.NotchNumberProperty.Type, LocationSpaceElement.NotchNumberProperty.DefaultValue, 0, Int32.MaxValue, true, LocationSpaceElement.NotchNumberProperty.DisplayType));
            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, LocationSpaceElementDto locationSpaceElementDto,
            LocationSpaceBayDto locationSpaceBayDto, LocationSpaceProductGroupDto locationSpaceProductGroupDto,
            String locationCode, IEnumerable<ProductGroupDto> productGroupDtoList)
        {
            switch (mappingId)
            {
                #region Location Space Element Properties
                case LocationSpaceElementImportMappingList.LocationCodeMappingId: return locationCode;
                case LocationSpaceElementImportMappingList.ProductGroupCodeMappingId:
                    ProductGroupDto groupDto = productGroupDtoList.FirstOrDefault(p => p.Id == locationSpaceProductGroupDto.ProductGroupId);
                    return (groupDto != null) ? groupDto.Code : null;
                case LocationSpaceElementImportMappingList.BayOrderMappingId: return locationSpaceBayDto.Order;
                case LocationSpaceElementImportMappingList.OrderMappingId: return locationSpaceElementDto.Order;
                case LocationSpaceElementImportMappingList.HeightMappingId: return locationSpaceElementDto.Height;
                case LocationSpaceElementImportMappingList.WidthMappingId: return locationSpaceElementDto.Width;
                case LocationSpaceElementImportMappingList.DepthMappingId: return locationSpaceElementDto.Depth;
                case LocationSpaceElementImportMappingList.MerchandisableHeightMappingId: return locationSpaceElementDto.MerchandisableHeight;
                case LocationSpaceElementImportMappingList.XPositionMappingId: return locationSpaceElementDto.X;
                case LocationSpaceElementImportMappingList.YPositionMappingId: return locationSpaceElementDto.Y;
                case LocationSpaceElementImportMappingList.ZPositionMappingId: return locationSpaceElementDto.Z;
                case LocationSpaceElementImportMappingList.FaceThicknessMappingId: return locationSpaceElementDto.FaceThickness;
                case LocationSpaceElementImportMappingList.NotchNumberMappingId: return locationSpaceElementDto.NotchNumber;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="locationDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            LocationSpaceElementDto locationSpaceElementDto)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            switch (mappingId)
            {
                case LocationSpaceElementImportMappingList.OrderMappingId:
                    locationSpaceElementDto.Order = Convert.ToByte(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.XPositionMappingId:
                    locationSpaceElementDto.X = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.YPositionMappingId:
                    locationSpaceElementDto.Y = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.ZPositionMappingId:
                    locationSpaceElementDto.Z = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.MerchandisableHeightMappingId:
                    locationSpaceElementDto.MerchandisableHeight = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.HeightMappingId:
                    locationSpaceElementDto.Height = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.WidthMappingId:
                    locationSpaceElementDto.Width = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.DepthMappingId:
                    locationSpaceElementDto.Depth = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.FaceThicknessMappingId:
                    locationSpaceElementDto.FaceThickness = Convert.ToSingle(cellValue, prov);
                    break;
                case LocationSpaceElementImportMappingList.NotchNumberMappingId:
                    locationSpaceElementDto.NotchNumber = Convert.ToSingle(cellValue, prov);
                    break;
                default:
                    //Do nothing
                    break;
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override String GetBindingPath(Int32 mappingId)
        {
            return GetBindingPath(mappingId, null);
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;

            switch (mappingId)
            {
                #region Location Space Element Properties
                case LocationSpaceElementImportMappingList.LocationCodeMappingId: return String.Format("{0}{1}", prefix, Location.CodeProperty.Name);
                case LocationSpaceElementImportMappingList.ProductGroupCodeMappingId: return String.Format("{0}{1}", prefix, ProductGroup.CodeProperty.Name);
                case LocationSpaceElementImportMappingList.BayOrderMappingId: return String.Format("{0}{1}", prefix, LocationSpaceBay.OrderProperty.Name);
                case LocationSpaceElementImportMappingList.OrderMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.OrderProperty.Name);
                case LocationSpaceElementImportMappingList.HeightMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.HeightProperty.Name);
                case LocationSpaceElementImportMappingList.WidthMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.WidthProperty.Name);
                case LocationSpaceElementImportMappingList.DepthMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.DepthProperty.Name);
                case LocationSpaceElementImportMappingList.MerchandisableHeightMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.MerchandisableHeightProperty.Name);
                case LocationSpaceElementImportMappingList.XPositionMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.XProperty.Name);
                case LocationSpaceElementImportMappingList.YPositionMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.YProperty.Name);
                case LocationSpaceElementImportMappingList.ZPositionMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.ZProperty.Name);
                case LocationSpaceElementImportMappingList.FaceThicknessMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.FaceThicknessProperty.Name);
                case LocationSpaceElementImportMappingList.NotchNumberMappingId: return String.Format("{0}{1}", prefix, LocationSpaceElement.NotchNumberProperty.Name);
                #endregion

                default: throw new NotImplementedException();
            }
        }

        public static Boolean IsUOMDisplayConversionRequired(Int32 mappingId)
        {
            switch (mappingId)
            {
                case HeightMappingId:
                case WidthMappingId:
                case DepthMappingId:
                case FaceThicknessMappingId:
                    return true;

                default: return false;
            }
        }

        #endregion

        #region IModelObjectColumnHelper members

        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mapping)
        {
            //does this have any groups?
            return null;
        }

        //Boolean IModelObjectColumnHelper.IsUOMDisplayConversionRequired(Int32 mappingId)
        //{
        //    return LocationSpaceElementImportMappingList.IsUOMDisplayConversionRequired(mappingId);
        //}

        #endregion
    }
}
