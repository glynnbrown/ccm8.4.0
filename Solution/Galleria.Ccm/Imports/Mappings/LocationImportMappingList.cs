﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
// V8-24264 : A.Probyn
//	Extended for display type on import mapping constructor.
#endregion
#region Version History: (CCM 8.3.0)
// V8-32180 : N.Haywood
//  Reduced length of telCountryCode, telAreaCode, faxCountryCode and faxAreaCode to 5
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Model;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Globalization;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Imports.Mappings
{
    [Serializable]
    public class LocationImportMappingList : ImportMappingList<LocationImportMappingList>, IModelObjectColumnHelper
    {
        #region Constants

        //public const Int32 LocationTypeMapId = 0;
        //public const Int32 PerformanceSourceReferenceMapId = 1;
        public const Int32 CodeMapId = 2;
        //[ISO-13791] Added group link
        public const Int32 GroupCodeMapId = 3;
        public const Int32 NameMapId = 4;
        public const Int32 RegionMapId = 5;
        public const Int32 CountyMapId = 6;
        public const Int32 TVRegionMapId = 7;
        public const Int32 LocationAttributeMapId = 8;
        public const Int32 DefaultClusterAttributeMapId = 9;
        public const Int32 Address1MapId = 10;
        public const Int32 Address2MapId = 11;
        public const Int32 CityMapId = 12;
        public const Int32 PostalCodeMapId = 14;
        public const Int32 CountryMapId = 15;
        public const Int32 LatitudeMapId = 16;
        public const Int32 LongitudeMapId = 17;
        public const Int32 DateOpenMapId = 18;
        public const Int32 DateLastRefittedMapId = 19;
        public const Int32 DateClosedMapId = 20;
        public const Int32 CarParkSpacesMapId = 21;
        public const Int32 CarParkManagementMapId = 22;
        public const Int32 PetrolForecourtTypeMapId = 23;
        public const Int32 RestaurantMapId = 24;
        public const Int32 SizeGrossFloorAreaMapId = 25;
        public const Int32 SizeNetSalesAreaMapId = 26;
        public const Int32 SizeMezzSalesAreaMapId = 27;
        public const Int32 TelephoneCountryCodeMapId = 28;
        public const Int32 TelephoneAreaCodeMapId = 29;
        public const Int32 TelephoneNumberMapId = 30;
        public const Int32 FaxCountryCodeMapId = 31;
        public const Int32 FaxAreaCodeMapId = 32;
        public const Int32 FaxNumberMapId = 33;
        public const Int32 OpeningHoursMapId = 34;
        public const Int32 AverageOpeningHoursMapId = 35;
        public const Int32 ManagerNameMapId = 36;
        public const Int32 RegionalManagerNameMapId = 37;
        public const Int32 DivisionalManagerNameMapId = 38;
        public const Int32 ManagerEmailMapId = 39;
        public const Int32 RegionalManagerEmailMapId = 40;
        public const Int32 DivisionalManagerEmailMapId = 41;
        public const Int32 AdvertisingZoneMapId = 42;
        public const Int32 DistributionCentreMapId = 43;
        public const Int32 NoOfCheckoutsMapId = 44;
        //public const Int32 FootfallMapId = 45;
        public const Int32 IsMezzFittedMapId = 46;
        public const Int32 IsFreeholdMapId = 47;
        public const Int32 Is24HoursMapId = 48;
        public const Int32 IsOpenMondayMapId = 49;
        public const Int32 IsOpenTuesdayMapId = 50;
        public const Int32 IsOpenWednesdayMapId = 51;
        public const Int32 IsOpenThursdayMapId = 52;
        public const Int32 IsOpenFridayMapId = 53;
        public const Int32 IsOpenSaturdayMapId = 54;
        public const Int32 IsOpenSundayMapId = 55;
        public const Int32 HasPetrolForecourtMapId = 56;
        public const Int32 HasNewsCubeMapId = 57;
        public const Int32 HasAtmMachinesMapId = 58;
        public const Int32 HasCustomerWCMapId = 59;
        public const Int32 HasBabyChangingMapId = 60;
        public const Int32 HasInStoreBakeryMapId = 61;
        public const Int32 HasHotFoodToGoMapId = 62;
        public const Int32 HasRotisserieMapId = 63;
        public const Int32 HasFishmongerMapId = 64;
        public const Int32 HasButcherMapId = 65;
        public const Int32 HasPizzaMapId = 66;
        public const Int32 HasDeliMapId = 67;
        public const Int32 HasSaladBarMapId = 68;
        public const Int32 HasOrganicMapId = 69;
        public const Int32 HasGroceryMapId = 70;
        public const Int32 HasMobilePhonesMapId = 71;
        public const Int32 HasDryCleaningMapId = 72;
        public const Int32 HasHomeShoppingAvailableMapId = 73;
        public const Int32 HasOpticianMapId = 74;
        public const Int32 HasPharmacyMapId = 75;
        public const Int32 HasTravelMapId = 76;
        public const Int32 HasPhotoDepartmentMapId = 77;
        public const Int32 HasCarServiceAreaMapId = 78;
        public const Int32 HasGardenCentreMapId = 79;
        public const Int32 HasClinicMapId = 80;
        public const Int32 HasAlcoholMapId = 81;
        public const Int32 HasFashionMapId = 82;
        public const Int32 HasCafeMapId = 83;
        public const Int32 HasRecyclingMapId = 84;
        public const Int32 HasPhotocopierMapId = 85;
        public const Int32 HasLotteryMapId = 86;
        public const Int32 HasPostOfficeMapId = 87;
        public const Int32 HasMovieRentalMapId = 88;
        public const Int32 HasJewelleryMapId = 89;
        public const Int32 StateMapId = 90;

        #endregion

        #region Constructors
        private LocationImportMappingList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new import mapping list
        /// </summary>
        /// <returns></returns>
        public static LocationImportMappingList NewLocationImportMappingList()
        {
            LocationImportMappingList list = new LocationImportMappingList();
            list.Create();
            return list;
        }

        #endregion

        #region Data Access

        #region Create
        private void Create()
        {
            this.RaiseListChangedEvents = false;

            #region LocationProperties

            this.Add(ImportMapping.NewImportMapping(CodeMapId, Location.CodeProperty.FriendlyName, Location.CodeProperty.Description, true, /*uniqueIdentifier*/true, Location.CodeProperty.Type, Location.CodeProperty.DefaultValue, 0, 50, false, Location.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(GroupCodeMapId, String.Format(Message.LocationImportMappingList_LocationGroupCode, LocationGroup.CodeProperty.FriendlyName), LocationGroup.CodeProperty.Description, true, LocationGroup.CodeProperty.Type, LocationGroup.CodeProperty.DefaultValue, 0, 50, true, LocationGroup.CodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(NameMapId, Location.NameProperty.FriendlyName, Location.NameProperty.Description, true, Location.NameProperty.Type, Location.NameProperty.DefaultValue, 0, 50, true, Location.NameProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(RegionMapId, Location.RegionProperty.FriendlyName, Location.RegionProperty.Description, false, Location.RegionProperty.Type, Location.RegionProperty.DefaultValue, 0, 50, true, Location.RegionProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CountyMapId, Location.CountyProperty.FriendlyName, Location.CountyProperty.Description, false, Location.CountyProperty.Type, Location.CountyProperty.DefaultValue, 0, 50, true, Location.CountyProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(StateMapId, Location.StateProperty.FriendlyName, Location.StateProperty.Description, false, Location.StateProperty.Type, Location.StateProperty.DefaultValue, 0, 50, true, Location.StateProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TVRegionMapId, Location.TVRegionProperty.FriendlyName, Location.TVRegionProperty.Description, false, Location.TVRegionProperty.Type, Location.TVRegionProperty.DefaultValue, 0, 50, true, Location.TVRegionProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(LocationAttributeMapId, Location.LocationAttributeProperty.FriendlyName, Location.LocationAttributeProperty.Description, false, Location.LocationAttributeProperty.Type, Location.LocationAttributeProperty.DefaultValue, 0, 50, true, Location.LocationAttributeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DefaultClusterAttributeMapId, Location.DefaultClusterAttributeProperty.FriendlyName, Location.DefaultClusterAttributeProperty.Description, false, Location.DefaultClusterAttributeProperty.Type, Location.DefaultClusterAttributeProperty.DefaultValue, 0, 50, true, Location.DefaultClusterAttributeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(Address1MapId, Location.Address1Property.FriendlyName, Location.Address1Property.Description, false, Location.Address1Property.Type, Location.Address1Property.DefaultValue, 0, 50, true, Location.Address1Property.DisplayType));
            this.Add(ImportMapping.NewImportMapping(Address2MapId, Location.Address2Property.FriendlyName, Location.Address2Property.Description, false, Location.Address2Property.Type, Location.Address2Property.DefaultValue, 0, 50, true, Location.Address2Property.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CityMapId, Location.CityProperty.FriendlyName, Location.CityProperty.Description, false, Location.CityProperty.Type, Location.CityProperty.DefaultValue, 0, 50, true, Location.CityProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PostalCodeMapId, Location.PostalCodeProperty.FriendlyName, Location.PostalCodeProperty.Description, false, Location.PostalCodeProperty.Type, Location.PostalCodeProperty.DefaultValue, 0, 50, true, Location.PostalCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CountryMapId, Location.CountryProperty.FriendlyName, Location.CountryProperty.Description, false, Location.CountryProperty.Type, Location.CountryProperty.DefaultValue, 0, 50, true, Location.CountryProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(LongitudeMapId, Location.LongitudeProperty.FriendlyName, Location.LongitudeProperty.Description, false, Location.LongitudeProperty.Type, Location.LongitudeProperty.DefaultValue, -180, 180, true, Location.LongitudeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(LatitudeMapId, Location.LatitudeProperty.FriendlyName, Location.LatitudeProperty.Description, false, Location.LatitudeProperty.Type, Location.LatitudeProperty.DefaultValue, -90, 90, true, Location.LatitudeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DateOpenMapId, Location.DateOpenProperty.FriendlyName, Location.DateOpenProperty.Description, false, Location.DateOpenProperty.Type, Location.DateOpenProperty.DefaultValue, 0, 50, true, Location.DateOpenProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DateLastRefittedMapId, Location.DateLastRefittedProperty.FriendlyName, Location.DateLastRefittedProperty.Description, false, Location.DateLastRefittedProperty.Type, Location.DateLastRefittedProperty.DefaultValue, 0, 50, true, Location.DateLastRefittedProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DateClosedMapId, Location.DateClosedProperty.FriendlyName, Location.DateClosedProperty.Description, false, Location.DateClosedProperty.Type, Location.DateClosedProperty.DefaultValue, 0, 50, true, Location.DateClosedProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CarParkSpacesMapId, Location.CarParkSpacesProperty.FriendlyName, Location.CarParkSpacesProperty.Description, false, Location.CarParkSpacesProperty.Type, Location.CarParkSpacesProperty.DefaultValue, 0, Int16.MaxValue, true, Location.CarParkSpacesProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(CarParkManagementMapId, Location.CarParkManagementProperty.FriendlyName, Location.CarParkManagementProperty.Description, false, Location.CarParkManagementProperty.Type, Location.CarParkManagementProperty.DefaultValue, 0, 50, true, Location.CarParkManagementProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(PetrolForecourtTypeMapId, Location.PetrolForecourtTypeProperty.FriendlyName, Location.PetrolForecourtTypeProperty.Description, false, Location.PetrolForecourtTypeProperty.Type, Location.PetrolForecourtTypeProperty.DefaultValue, 0, 50, true, Location.PetrolForecourtTypeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(RestaurantMapId, Location.RestaurantProperty.FriendlyName, Location.RestaurantProperty.Description, false, Location.RestaurantProperty.Type, Location.RestaurantProperty.DefaultValue, 0, 50, true, Location.RestaurantProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SizeGrossFloorAreaMapId, Location.SizeGrossFloorAreaProperty.FriendlyName, Location.SizeGrossFloorAreaProperty.Description, false, Location.SizeGrossFloorAreaProperty.Type, Location.SizeGrossFloorAreaProperty.DefaultValue, 0, Int32.MaxValue, true, Location.SizeGrossFloorAreaProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SizeNetSalesAreaMapId, Location.SizeNetSalesAreaProperty.FriendlyName, Location.SizeNetSalesAreaProperty.Description, false, Location.SizeNetSalesAreaProperty.Type, Location.SizeNetSalesAreaProperty.DefaultValue, 0, Int32.MaxValue, true, Location.SizeNetSalesAreaProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(SizeMezzSalesAreaMapId, Location.SizeMezzSalesAreaProperty.FriendlyName, Location.SizeMezzSalesAreaProperty.Description, false, Location.SizeMezzSalesAreaProperty.Type, Location.SizeMezzSalesAreaProperty.DefaultValue, 0, Int32.MaxValue, true, Location.SizeMezzSalesAreaProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TelephoneCountryCodeMapId, Location.TelephoneCountryCodeProperty.FriendlyName, Location.TelephoneCountryCodeProperty.Description, false, Location.TelephoneCountryCodeProperty.Type, Location.TelephoneCountryCodeProperty.DefaultValue, 0, 5, true, Location.TelephoneCountryCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TelephoneAreaCodeMapId, Location.TelephoneAreaCodeProperty.FriendlyName, Location.TelephoneAreaCodeProperty.Description, false, Location.TelephoneAreaCodeProperty.Type, Location.TelephoneAreaCodeProperty.DefaultValue, 0, 5, true, Location.TelephoneAreaCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(TelephoneNumberMapId, Location.TelephoneNumberProperty.FriendlyName, Location.TelephoneNumberProperty.Description, false, Location.TelephoneNumberProperty.Type, Location.TelephoneNumberProperty.DefaultValue, 0, 50, true, Location.TelephoneNumberProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FaxCountryCodeMapId, Location.FaxCountryCodeProperty.FriendlyName, Location.FaxCountryCodeProperty.Description, false, Location.FaxCountryCodeProperty.Type, Location.FaxCountryCodeProperty.DefaultValue, 0, 5, true, Location.FaxCountryCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FaxAreaCodeMapId, Location.FaxAreaCodeProperty.FriendlyName, Location.FaxAreaCodeProperty.Description, false, Location.FaxAreaCodeProperty.Type, Location.FaxAreaCodeProperty.DefaultValue, 0, 5, true, Location.FaxAreaCodeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(FaxNumberMapId, Location.FaxNumberProperty.FriendlyName, Location.FaxNumberProperty.Description, false, Location.FaxNumberProperty.Type, Location.FaxNumberProperty.DefaultValue, 0, 50, true, Location.FaxNumberProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(OpeningHoursMapId, Location.OpeningHoursProperty.FriendlyName, Location.OpeningHoursProperty.Description, false, Location.OpeningHoursProperty.Type, Location.OpeningHoursProperty.DefaultValue, 0, 50, true, Location.OpeningHoursProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(AverageOpeningHoursMapId, Location.AverageOpeningHoursProperty.FriendlyName, Location.AverageOpeningHoursProperty.Description, false, Location.AverageOpeningHoursProperty.Type, Location.AverageOpeningHoursProperty.DefaultValue, 0, Int32.MaxValue, true, Location.AverageOpeningHoursProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ManagerNameMapId, Location.ManagerNameProperty.FriendlyName, Location.ManagerNameProperty.Description, false, Location.ManagerNameProperty.Type, Location.ManagerNameProperty.DefaultValue, 0, 50, true, Location.ManagerNameProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(ManagerEmailMapId, Location.ManagerEmailProperty.FriendlyName, Location.ManagerEmailProperty.Description, false, Location.ManagerEmailProperty.Type, Location.ManagerEmailProperty.DefaultValue, 0, 50, true, Location.ManagerEmailProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(RegionalManagerNameMapId, Location.RegionalManagerNameProperty.FriendlyName, Location.RegionalManagerNameProperty.Description, false, Location.RegionalManagerNameProperty.Type, Location.RegionalManagerNameProperty.DefaultValue, 0, 50, true, Location.RegionalManagerNameProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(RegionalManagerEmailMapId, Location.RegionalManagerEmailProperty.FriendlyName, Location.RegionalManagerEmailProperty.Description, false, Location.RegionalManagerEmailProperty.Type, Location.RegionalManagerEmailProperty.DefaultValue, 0, 256, true, Location.RegionalManagerEmailProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DivisionalManagerNameMapId, Location.DivisionalManagerNameProperty.FriendlyName, Location.DivisionalManagerNameProperty.Description, false, Location.DivisionalManagerNameProperty.Type, Location.DivisionalManagerNameProperty.DefaultValue, 0, 50, true, Location.DivisionalManagerNameProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DivisionalManagerEmailMapId, Location.DivisionalManagerEmailProperty.FriendlyName, Location.DivisionalManagerEmailProperty.Description, false, Location.DivisionalManagerEmailProperty.Type, Location.DivisionalManagerEmailProperty.DefaultValue, 0, 256, true, Location.DivisionalManagerEmailProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(AdvertisingZoneMapId, Location.AdvertisingZoneProperty.FriendlyName, Location.AdvertisingZoneProperty.Description, false, Location.AdvertisingZoneProperty.Type, Location.AdvertisingZoneProperty.DefaultValue, 0, 50, true, Location.AdvertisingZoneProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(DistributionCentreMapId, Location.DistributionCentreProperty.FriendlyName, Location.DistributionCentreProperty.Description, false, Location.DistributionCentreProperty.Type, Location.DistributionCentreProperty.DefaultValue, 0, 50, true, Location.DistributionCentreProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(NoOfCheckoutsMapId, Location.NoOfCheckoutsProperty.FriendlyName, Location.NoOfCheckoutsProperty.Description, false, Location.NoOfCheckoutsProperty.Type, Location.NoOfCheckoutsProperty.DefaultValue, 0, Byte.MaxValue, true, Location.NoOfCheckoutsProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsMezzFittedMapId, Location.IsMezzFittedProperty.FriendlyName, Location.IsMezzFittedProperty.Description, false, Location.IsMezzFittedProperty.Type, Location.IsMezzFittedProperty.DefaultValue, 0, 0, true, Location.IsMezzFittedProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsFreeholdMapId, Location.IsFreeholdProperty.FriendlyName, Location.IsFreeholdProperty.Description, false, Location.IsFreeholdProperty.Type, Location.IsFreeholdProperty.DefaultValue, 0, 0, true, Location.IsFreeholdProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(Is24HoursMapId, Location.Is24HoursProperty.FriendlyName, Location.Is24HoursProperty.Description, false, Location.Is24HoursProperty.Type, Location.Is24HoursProperty.DefaultValue, 0, 0, true, Location.Is24HoursProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsOpenMondayMapId, Location.IsOpenMondayProperty.FriendlyName, Location.IsOpenMondayProperty.Description, false, Location.IsOpenMondayProperty.Type, Location.IsOpenMondayProperty.DefaultValue, 0, 0, true, Location.IsOpenMondayProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsOpenTuesdayMapId, Location.IsOpenTuesdayProperty.FriendlyName, Location.IsOpenTuesdayProperty.Description, false, Location.IsOpenTuesdayProperty.Type, Location.IsOpenTuesdayProperty.DefaultValue, 0, 0, true, Location.IsOpenTuesdayProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsOpenWednesdayMapId, Location.IsOpenWednesdayProperty.FriendlyName, Location.IsOpenWednesdayProperty.Description, false, Location.IsOpenWednesdayProperty.Type, Location.IsOpenWednesdayProperty.DefaultValue, 0, 0, true, Location.IsOpenWednesdayProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsOpenThursdayMapId, Location.IsOpenThursdayProperty.FriendlyName, Location.IsOpenThursdayProperty.Description, false, Location.IsOpenThursdayProperty.Type, Location.IsOpenThursdayProperty.DefaultValue, 0, 0, true, Location.IsOpenThursdayProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsOpenFridayMapId, Location.IsOpenFridayProperty.FriendlyName, Location.IsOpenFridayProperty.Description, false, Location.IsOpenFridayProperty.Type, Location.IsOpenFridayProperty.DefaultValue, 0, 0, true, Location.IsOpenFridayProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsOpenSaturdayMapId, Location.IsOpenSaturdayProperty.FriendlyName, Location.IsOpenSaturdayProperty.Description, false, Location.IsOpenSaturdayProperty.Type, Location.IsOpenSaturdayProperty.DefaultValue, 0, 0, true, Location.IsOpenSaturdayProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(IsOpenSundayMapId, Location.IsOpenSundayProperty.FriendlyName, Location.IsOpenSundayProperty.Description, false, Location.IsOpenSundayProperty.Type, Location.IsOpenSundayProperty.DefaultValue, 0, 0, true, Location.IsOpenSundayProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasPetrolForecourtMapId, Location.HasPetrolForecourtProperty.FriendlyName, Location.HasPetrolForecourtProperty.Description, false, Location.HasPetrolForecourtProperty.Type, Location.HasPetrolForecourtProperty.DefaultValue, 0, 0, true, Location.HasPetrolForecourtProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasNewsCubeMapId, Location.HasNewsCubeProperty.FriendlyName, Location.HasNewsCubeProperty.Description, false, Location.HasNewsCubeProperty.Type, Location.HasNewsCubeProperty.DefaultValue, 0, 0, true, Location.HasNewsCubeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasAtmMachinesMapId, Location.HasAtmMachinesProperty.FriendlyName, Location.HasAtmMachinesProperty.Description, false, Location.HasAtmMachinesProperty.Type, Location.HasAtmMachinesProperty.DefaultValue, 0, 0, true, Location.HasAtmMachinesProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasCustomerWCMapId, Location.HasCustomerWCProperty.FriendlyName, Location.HasCustomerWCProperty.Description, false, Location.HasCustomerWCProperty.Type, Location.HasCustomerWCProperty.DefaultValue, 0, 0, true, Location.HasCustomerWCProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasBabyChangingMapId, Location.HasBabyChangingProperty.FriendlyName, Location.HasBabyChangingProperty.Description, false, Location.HasBabyChangingProperty.Type, Location.HasBabyChangingProperty.DefaultValue, 0, 0, true, Location.HasBabyChangingProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasInStoreBakeryMapId, Location.HasInStoreBakeryProperty.FriendlyName, Location.HasInStoreBakeryProperty.Description, false, Location.HasInStoreBakeryProperty.Type, Location.HasInStoreBakeryProperty.DefaultValue, 0, 0, true, Location.HasInStoreBakeryProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasHotFoodToGoMapId, Location.HasHotFoodToGoProperty.FriendlyName, Location.HasHotFoodToGoProperty.Description, false, Location.HasHotFoodToGoProperty.Type, Location.HasHotFoodToGoProperty.DefaultValue, 0, 0, true, Location.HasHotFoodToGoProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasRotisserieMapId, Location.HasRotisserieProperty.FriendlyName, Location.HasRotisserieProperty.Description, false, Location.HasRotisserieProperty.Type, Location.HasRotisserieProperty.DefaultValue, 0, 0, true, Location.HasRotisserieProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasFishmongerMapId, Location.HasFishmongerProperty.FriendlyName, Location.HasFishmongerProperty.Description, false, Location.HasFishmongerProperty.Type, Location.HasFishmongerProperty.DefaultValue, 0, 0, true, Location.HasFishmongerProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasButcherMapId, Location.HasButcherProperty.FriendlyName, Location.HasButcherProperty.Description, false, Location.HasButcherProperty.Type, Location.HasButcherProperty.DefaultValue, 0, 0, true, Location.HasButcherProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasPizzaMapId, Location.HasPizzaProperty.FriendlyName, Location.HasPizzaProperty.Description, false, Location.HasPizzaProperty.Type, Location.HasPizzaProperty.DefaultValue, 0, 0, true, Location.HasPizzaProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasDeliMapId, Location.HasDeliProperty.FriendlyName, Location.HasDeliProperty.Description, false, Location.HasDeliProperty.Type, Location.HasDeliProperty.DefaultValue, 0, 0, true, Location.HasDeliProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasSaladBarMapId, Location.HasSaladBarProperty.FriendlyName, Location.HasSaladBarProperty.Description, false, Location.HasSaladBarProperty.Type, Location.HasSaladBarProperty.DefaultValue, 0, 0, true, Location.HasSaladBarProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasOrganicMapId, Location.HasOrganicProperty.FriendlyName, Location.HasOrganicProperty.Description, false, Location.HasOrganicProperty.Type, Location.HasOrganicProperty.DefaultValue, 0, 0, true, Location.HasOrganicProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasGroceryMapId, Location.HasGroceryProperty.FriendlyName, Location.HasGroceryProperty.Description, false, Location.HasGroceryProperty.Type, Location.HasGroceryProperty.DefaultValue, 0, 0, true, Location.HasGroceryProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasMobilePhonesMapId, Location.HasMobilePhonesProperty.FriendlyName, Location.HasMobilePhonesProperty.Description, false, Location.HasMobilePhonesProperty.Type, Location.HasMobilePhonesProperty.DefaultValue, 0, 0, true, Location.HasMobilePhonesProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasDryCleaningMapId, Location.HasDryCleaningProperty.FriendlyName, Location.HasDryCleaningProperty.Description, false, Location.HasDryCleaningProperty.Type, Location.HasDryCleaningProperty.DefaultValue, 0, 0, true, Location.HasDryCleaningProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasHomeShoppingAvailableMapId, Location.HasHomeShoppingAvailableProperty.FriendlyName, Location.HasHomeShoppingAvailableProperty.Description, false, Location.HasHomeShoppingAvailableProperty.Type, Location.HasHomeShoppingAvailableProperty.DefaultValue, 0, 0, true, Location.HasHomeShoppingAvailableProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasOpticianMapId, Location.HasOpticianProperty.FriendlyName, Location.HasOpticianProperty.Description, false, Location.HasOpticianProperty.Type, Location.HasOpticianProperty.DefaultValue, 0, 0, true, Location.HasOpticianProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasPharmacyMapId, Location.HasPharmacyProperty.FriendlyName, Location.HasPharmacyProperty.Description, false, Location.HasPharmacyProperty.Type, Location.HasPharmacyProperty.DefaultValue, 0, 0, true, Location.HasPharmacyProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasTravelMapId, Location.HasTravelProperty.FriendlyName, Location.HasTravelProperty.Description, false, Location.HasTravelProperty.Type, Location.HasTravelProperty.DefaultValue, 0, 0, true, Location.HasTravelProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasPhotoDepartmentMapId, Location.HasPhotoDepartmentProperty.FriendlyName, Location.HasPhotoDepartmentProperty.Description, false, Location.HasPhotoDepartmentProperty.Type, Location.HasPhotoDepartmentProperty.DefaultValue, 0, 0, true, Location.HasPhotoDepartmentProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasCarServiceAreaMapId, Location.HasCarServiceAreaProperty.FriendlyName, Location.HasCarServiceAreaProperty.Description, false, Location.HasCarServiceAreaProperty.Type, Location.HasCarServiceAreaProperty.DefaultValue, 0, 0, true, Location.HasCarServiceAreaProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasGardenCentreMapId, Location.HasGardenCentreProperty.FriendlyName, Location.HasGardenCentreProperty.Description, false, Location.HasGardenCentreProperty.Type, Location.HasGardenCentreProperty.DefaultValue, 0, 0, true, Location.HasGardenCentreProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasClinicMapId, Location.HasClinicProperty.FriendlyName, Location.HasClinicProperty.Description, false, Location.HasClinicProperty.Type, Location.HasClinicProperty.DefaultValue, 0, 0, true, Location.HasClinicProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasAlcoholMapId, Location.HasAlcoholProperty.FriendlyName, Location.HasAlcoholProperty.Description, false, Location.HasAlcoholProperty.Type, Location.HasAlcoholProperty.DefaultValue, 0, 0, true, Location.HasAlcoholProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasFashionMapId, Location.HasFashionProperty.FriendlyName, Location.HasFashionProperty.Description, false, Location.HasFashionProperty.Type, Location.HasFashionProperty.DefaultValue, 0, 0, true, Location.HasFashionProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasCafeMapId, Location.HasCafeProperty.FriendlyName, Location.HasCafeProperty.Description, false, Location.HasCafeProperty.Type, Location.HasCafeProperty.DefaultValue, 0, 0, true, Location.HasCafeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasRecyclingMapId, Location.HasRecyclingProperty.FriendlyName, Location.HasRecyclingProperty.Description, false, Location.HasRecyclingProperty.Type, Location.HasRecyclingProperty.DefaultValue, 0, 0, true, Location.HasRecyclingProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasPhotocopierMapId, Location.HasPhotocopierProperty.FriendlyName, Location.HasPhotocopierProperty.Description, false, Location.HasPhotocopierProperty.Type, Location.HasPhotocopierProperty.DefaultValue, 0, 0, true, Location.HasPhotocopierProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasLotteryMapId, Location.HasLotteryProperty.FriendlyName, Location.HasLotteryProperty.Description, false, Location.HasLotteryProperty.Type, Location.HasLotteryProperty.DefaultValue, 0, 0, true, Location.HasLotteryProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasPostOfficeMapId, Location.HasPostOfficeProperty.FriendlyName, Location.HasPostOfficeProperty.Description, false, Location.HasPostOfficeProperty.Type, Location.HasPostOfficeProperty.DefaultValue, 0, 0, true, Location.HasPostOfficeProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasMovieRentalMapId, Location.HasMovieRentalProperty.FriendlyName, Location.HasMovieRentalProperty.Description, false, Location.HasMovieRentalProperty.Type, Location.HasMovieRentalProperty.DefaultValue, 0, 0, true, Location.HasMovieRentalProperty.DisplayType));
            this.Add(ImportMapping.NewImportMapping(HasJewelleryMapId, Location.HasJewelleryProperty.FriendlyName, Location.HasJewelleryProperty.Description, false, Location.HasJewelleryProperty.Type, Location.HasJewelleryProperty.DefaultValue, 0, 0, true, Location.HasJewelleryProperty.DisplayType));

            #endregion

            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Returns the value for the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="locationDto"></param>
        /// <returns></returns>
        internal static Object GetValueByMappingId(Int32 mappingId, LocationDto locationDto, IEnumerable<LocationGroupDto> locationGroupDtoList)
        {
            switch (mappingId)
            {
                #region Location Properties
                case LocationImportMappingList.CodeMapId: return locationDto.Code;
                case LocationImportMappingList.GroupCodeMapId: return locationGroupDtoList.First(p => p.Id == locationDto.LocationGroupId).Code;
                case LocationImportMappingList.NameMapId: return locationDto.Name;
                case LocationImportMappingList.RegionMapId: return locationDto.Region;
                case LocationImportMappingList.CountyMapId: return locationDto.County;
                case LocationImportMappingList.StateMapId: return locationDto.State;
                case LocationImportMappingList.TVRegionMapId: return locationDto.TVRegion;
                case LocationImportMappingList.LocationAttributeMapId: return locationDto.Location;
                case LocationImportMappingList.DefaultClusterAttributeMapId: return locationDto.DefaultClusterAttribute;
                case LocationImportMappingList.Address1MapId: return locationDto.Address1;
                case LocationImportMappingList.Address2MapId: return locationDto.Address2;
                case LocationImportMappingList.CityMapId: return locationDto.City;
                case LocationImportMappingList.PostalCodeMapId: return locationDto.PostalCode;
                case LocationImportMappingList.CountryMapId: return locationDto.Country;
                case LocationImportMappingList.LatitudeMapId: return locationDto.Latitude;
                case LocationImportMappingList.LongitudeMapId: return locationDto.Longitude;
                case LocationImportMappingList.DateOpenMapId: return locationDto.DateOpen;
                case LocationImportMappingList.DateLastRefittedMapId: return locationDto.DateLastRefitted;
                case LocationImportMappingList.DateClosedMapId: return locationDto.DateClosed;
                case LocationImportMappingList.CarParkSpacesMapId: return locationDto.CarParkSpaces;
                case LocationImportMappingList.CarParkManagementMapId: return locationDto.CarParkManagement;
                case LocationImportMappingList.PetrolForecourtTypeMapId: return locationDto.PetrolForecourtType;
                case LocationImportMappingList.RestaurantMapId: return locationDto.Restaurant;
                case LocationImportMappingList.SizeGrossFloorAreaMapId: return locationDto.SizeGrossFloorArea;
                case LocationImportMappingList.SizeNetSalesAreaMapId: return locationDto.SizeNetSalesArea;
                case LocationImportMappingList.SizeMezzSalesAreaMapId: return locationDto.SizeMezzSalesArea;
                case LocationImportMappingList.TelephoneCountryCodeMapId: return locationDto.TelephoneCountryCode;
                case LocationImportMappingList.TelephoneAreaCodeMapId: return locationDto.TelephoneAreaCode;
                case LocationImportMappingList.TelephoneNumberMapId: return locationDto.TelephoneNumber;
                case LocationImportMappingList.FaxCountryCodeMapId: return locationDto.FaxCountryCode;
                case LocationImportMappingList.FaxAreaCodeMapId: return locationDto.FaxAreaCode;
                case LocationImportMappingList.FaxNumberMapId: return locationDto.FaxNumber;
                case LocationImportMappingList.OpeningHoursMapId: return locationDto.OpeningHours;
                case LocationImportMappingList.AverageOpeningHoursMapId: return locationDto.AverageOpeningHours;
                case LocationImportMappingList.ManagerNameMapId: return locationDto.ManagerName;
                case LocationImportMappingList.RegionalManagerNameMapId: return locationDto.RegionalManagerName;
                case LocationImportMappingList.DivisionalManagerNameMapId: return locationDto.DivisionalManagerName;
                case LocationImportMappingList.ManagerEmailMapId: return locationDto.ManagerEmail;
                case LocationImportMappingList.RegionalManagerEmailMapId: return locationDto.RegionalManagerEmail;
                case LocationImportMappingList.DivisionalManagerEmailMapId: return locationDto.DivisionalManagerEmail;
                case LocationImportMappingList.AdvertisingZoneMapId: return locationDto.AdvertisingZone;
                case LocationImportMappingList.DistributionCentreMapId: return locationDto.DistributionCentre;
                case LocationImportMappingList.NoOfCheckoutsMapId: return locationDto.NoOfCheckouts;
                case LocationImportMappingList.IsMezzFittedMapId: return locationDto.IsMezzFitted;
                case LocationImportMappingList.IsFreeholdMapId: return locationDto.IsFreehold;
                case LocationImportMappingList.Is24HoursMapId: return locationDto.Is24Hours;
                case LocationImportMappingList.IsOpenMondayMapId: return locationDto.IsOpenMonday;
                case LocationImportMappingList.IsOpenTuesdayMapId: return locationDto.IsOpenTuesday;
                case LocationImportMappingList.IsOpenWednesdayMapId: return locationDto.IsOpenWednesday;
                case LocationImportMappingList.IsOpenThursdayMapId: return locationDto.IsOpenThursday;
                case LocationImportMappingList.IsOpenFridayMapId: return locationDto.IsOpenFriday;
                case LocationImportMappingList.IsOpenSaturdayMapId: return locationDto.IsOpenSaturday;
                case LocationImportMappingList.IsOpenSundayMapId: return locationDto.IsOpenSunday;
                case LocationImportMappingList.HasPetrolForecourtMapId: return locationDto.HasPetrolForecourt;
                case LocationImportMappingList.HasNewsCubeMapId: return locationDto.HasNewsCube;
                case LocationImportMappingList.HasAtmMachinesMapId: return locationDto.HasAtmMachines;
                case LocationImportMappingList.HasCustomerWCMapId: return locationDto.HasCustomerWC;
                case LocationImportMappingList.HasBabyChangingMapId: return locationDto.HasBabyChanging;
                case LocationImportMappingList.HasInStoreBakeryMapId: return locationDto.HasInStoreBakery;
                case LocationImportMappingList.HasHotFoodToGoMapId: return locationDto.HasHotFoodToGo;
                case LocationImportMappingList.HasRotisserieMapId: return locationDto.HasRotisserie;
                case LocationImportMappingList.HasFishmongerMapId: return locationDto.HasFishmonger;
                case LocationImportMappingList.HasButcherMapId: return locationDto.HasButcher;
                case LocationImportMappingList.HasPizzaMapId: return locationDto.HasPizza;
                case LocationImportMappingList.HasDeliMapId: return locationDto.HasDeli;
                case LocationImportMappingList.HasSaladBarMapId: return locationDto.HasSaladBar;
                case LocationImportMappingList.HasOrganicMapId: return locationDto.HasOrganic;
                case LocationImportMappingList.HasGroceryMapId: return locationDto.HasGrocery;
                case LocationImportMappingList.HasMobilePhonesMapId: return locationDto.HasMobilePhones;
                case LocationImportMappingList.HasDryCleaningMapId: return locationDto.HasDryCleaning;
                case LocationImportMappingList.HasHomeShoppingAvailableMapId: return locationDto.HasHomeShoppingAvailable;
                case LocationImportMappingList.HasOpticianMapId: return locationDto.HasOptician;
                case LocationImportMappingList.HasPharmacyMapId: return locationDto.HasPharmacy;
                case LocationImportMappingList.HasTravelMapId: return locationDto.HasTravel;
                case LocationImportMappingList.HasPhotoDepartmentMapId: return locationDto.HasPhotoDepartment;
                case LocationImportMappingList.HasCarServiceAreaMapId: return locationDto.HasCarServiceArea;
                case LocationImportMappingList.HasGardenCentreMapId: return locationDto.HasGardenCentre;
                case LocationImportMappingList.HasClinicMapId: return locationDto.HasClinic;
                case LocationImportMappingList.HasAlcoholMapId: return locationDto.HasAlcohol;
                case LocationImportMappingList.HasFashionMapId: return locationDto.HasFashion;
                case LocationImportMappingList.HasCafeMapId: return locationDto.HasCafe;
                case LocationImportMappingList.HasRecyclingMapId: return locationDto.HasRecycling;
                case LocationImportMappingList.HasPhotocopierMapId: return locationDto.HasPhotocopier;
                case LocationImportMappingList.HasLotteryMapId: return locationDto.HasLottery;
                case LocationImportMappingList.HasPostOfficeMapId: return locationDto.HasPostOffice;
                case LocationImportMappingList.HasMovieRentalMapId: return locationDto.HasMovieRental;
                case LocationImportMappingList.HasJewelleryMapId: return locationDto.HasJewellery;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Sets the given value against the property represented by the given mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="cellValue"></param>
        /// <param name="locationDto"></param>
        internal static void SetValueByMappingId(Int32 mappingId, Object cellValue,
            LocationDto locationDto)
        {
            IFormatProvider prov = CultureInfo.InvariantCulture;

            switch (mappingId)
            {
                #region Location Properties
                case LocationImportMappingList.CodeMapId:
                    locationDto.Code = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.GroupCodeMapId:
                    locationDto.LocationGroupId = Convert.ToInt32(cellValue, prov);
                    break;

                case LocationImportMappingList.NameMapId:
                    locationDto.Name = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.RegionMapId:
                    locationDto.Region = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.TVRegionMapId:
                    locationDto.TVRegion = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.LocationAttributeMapId:
                    locationDto.Location = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.DefaultClusterAttributeMapId:
                    locationDto.DefaultClusterAttribute = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.Address1MapId:
                    locationDto.Address1 = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.Address2MapId:
                    locationDto.Address2 = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.CityMapId:
                    locationDto.City = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.CountyMapId:
                    locationDto.County = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.StateMapId:
                    locationDto.State = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.PostalCodeMapId:
                    locationDto.PostalCode = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.CountryMapId:
                    locationDto.Country = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.LatitudeMapId:
                    locationDto.Latitude = (cellValue != DBNull.Value) ? (Single?)Convert.ToSingle(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.LongitudeMapId:
                    locationDto.Longitude = (cellValue != DBNull.Value) ? (Single?)Convert.ToSingle(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.DateOpenMapId:
                    locationDto.DateOpen = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.DateLastRefittedMapId:
                    locationDto.DateLastRefitted = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.DateClosedMapId:
                    locationDto.DateClosed = (cellValue != null) ? (DateTime?)Convert.ToDateTime(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.CarParkSpacesMapId:
                    locationDto.CarParkSpaces = (cellValue != DBNull.Value) ? (Int16?)Convert.ToInt16(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.CarParkManagementMapId:
                    locationDto.CarParkManagement = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.PetrolForecourtTypeMapId:
                    locationDto.PetrolForecourtType = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.RestaurantMapId:
                    locationDto.Restaurant = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.SizeGrossFloorAreaMapId:
                    locationDto.SizeGrossFloorArea = (cellValue != DBNull.Value) ? (Int32?)Convert.ToInt32(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.SizeNetSalesAreaMapId:
                    locationDto.SizeNetSalesArea = (cellValue != DBNull.Value) ? (Int32?)Convert.ToInt32(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.SizeMezzSalesAreaMapId:
                    locationDto.SizeMezzSalesArea = (cellValue != DBNull.Value) ? (Int32?)Convert.ToInt32(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.TelephoneCountryCodeMapId:
                    locationDto.TelephoneCountryCode = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.TelephoneAreaCodeMapId:
                    locationDto.TelephoneAreaCode = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.TelephoneNumberMapId:
                    locationDto.TelephoneNumber = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.FaxCountryCodeMapId:
                    locationDto.FaxCountryCode = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.FaxAreaCodeMapId:
                    locationDto.FaxAreaCode = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.FaxNumberMapId:
                    locationDto.FaxNumber = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.OpeningHoursMapId:
                    locationDto.OpeningHours = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.AverageOpeningHoursMapId:
                    locationDto.AverageOpeningHours = (cellValue != DBNull.Value) ? (Single?)Convert.ToSingle(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.ManagerNameMapId:
                    locationDto.ManagerName = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.RegionalManagerNameMapId:
                    locationDto.RegionalManagerName = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.DivisionalManagerNameMapId:
                    locationDto.DivisionalManagerName = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.ManagerEmailMapId:
                    locationDto.ManagerEmail = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.RegionalManagerEmailMapId:
                    locationDto.RegionalManagerEmail = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.DivisionalManagerEmailMapId:
                    locationDto.DivisionalManagerEmail = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.AdvertisingZoneMapId:
                    locationDto.AdvertisingZone = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.DistributionCentreMapId:
                    locationDto.DistributionCentre = Convert.ToString(cellValue, prov);
                    break;

                case LocationImportMappingList.NoOfCheckoutsMapId:
                    locationDto.NoOfCheckouts = (cellValue != DBNull.Value) ? (Byte?)Convert.ToByte(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsMezzFittedMapId:
                    locationDto.IsMezzFitted = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsFreeholdMapId:
                    locationDto.IsFreehold = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.Is24HoursMapId:
                    locationDto.Is24Hours = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsOpenMondayMapId:
                    locationDto.IsOpenMonday = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsOpenTuesdayMapId:
                    locationDto.IsOpenTuesday = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsOpenWednesdayMapId:
                    locationDto.IsOpenWednesday = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsOpenThursdayMapId:
                    locationDto.IsOpenThursday = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsOpenFridayMapId:
                    locationDto.IsOpenFriday = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsOpenSaturdayMapId:
                    locationDto.IsOpenSaturday = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.IsOpenSundayMapId:
                    locationDto.IsOpenSunday = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasPetrolForecourtMapId:
                    locationDto.HasPetrolForecourt = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasNewsCubeMapId:
                    locationDto.HasNewsCube = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasAtmMachinesMapId:
                    locationDto.HasAtmMachines = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasCustomerWCMapId:
                    locationDto.HasCustomerWC = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasBabyChangingMapId:
                    locationDto.HasBabyChanging = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasInStoreBakeryMapId:
                    locationDto.HasInStoreBakery = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasHotFoodToGoMapId:
                    locationDto.HasHotFoodToGo = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasRotisserieMapId:
                    locationDto.HasRotisserie = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasFishmongerMapId:
                    locationDto.HasFishmonger = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasButcherMapId:
                    locationDto.HasButcher = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasPizzaMapId:
                    locationDto.HasPizza = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasDeliMapId:
                    locationDto.HasDeli = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasSaladBarMapId:
                    locationDto.HasSaladBar = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasOrganicMapId:
                    locationDto.HasOrganic = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasGroceryMapId:
                    locationDto.HasGrocery = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasMobilePhonesMapId:
                    locationDto.HasMobilePhones = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasDryCleaningMapId:
                    locationDto.HasDryCleaning = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasHomeShoppingAvailableMapId:
                    locationDto.HasHomeShoppingAvailable = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasOpticianMapId:
                    locationDto.HasOptician = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasPharmacyMapId:
                    locationDto.HasPharmacy = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasTravelMapId:
                    locationDto.HasTravel = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasPhotoDepartmentMapId:
                    locationDto.HasPhotoDepartment = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasCarServiceAreaMapId:
                    locationDto.HasCarServiceArea = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasGardenCentreMapId:
                    locationDto.HasGardenCentre = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasClinicMapId:
                    locationDto.HasClinic = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasAlcoholMapId:
                    locationDto.HasAlcohol = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasFashionMapId:
                    locationDto.HasFashion = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasCafeMapId:
                    locationDto.HasCafe = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasRecyclingMapId:
                    locationDto.HasRecycling = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasPhotocopierMapId:
                    locationDto.HasPhotocopier = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasLotteryMapId:
                    locationDto.HasLottery = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasPostOfficeMapId:
                    locationDto.HasPostOffice = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasMovieRentalMapId:
                    locationDto.HasMovieRental = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;

                case LocationImportMappingList.HasJewelleryMapId:
                    locationDto.HasJewellery = (cellValue != DBNull.Value) ? (Boolean?)Convert.ToBoolean(cellValue, prov) : null;
                    break;
                #endregion
            }
        }

        /// <summary>
        /// Returns the column binding path to use for the maping id 
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public override String GetBindingPath(Int32 mappingId)
        {
            return GetBindingPath(mappingId, null);
        }
        public String GetBindingPath(Int32 mappingId, String bindingPrefix)
        {
            String prefix = (!String.IsNullOrEmpty(bindingPrefix)) ? bindingPrefix + '.' : String.Empty;
            //This is to access  the linked location type in the view data row
            String locationType = "LocationTypeName";

            switch (mappingId)
            {
                #region Location Properties
                case LocationImportMappingList.CodeMapId: return String.Format("{0}{1}", prefix, Location.CodeProperty.Name);
                case LocationImportMappingList.GroupCodeMapId: return String.Format("{0}{1}", prefix, Location.LocationGroupIdProperty.Name);
                case LocationImportMappingList.NameMapId: return String.Format("{0}{1}", prefix, Location.NameProperty.Name);
                case LocationImportMappingList.RegionMapId: return String.Format("{0}{1}", prefix, Location.RegionProperty.Name);
                case LocationImportMappingList.CountyMapId: return String.Format("{0}{1}", prefix, Location.CountyProperty.Name);
                case LocationImportMappingList.StateMapId: return String.Format("{0}{1}", prefix, Location.StateProperty.Name);
                case LocationImportMappingList.TVRegionMapId: return String.Format("{0}{1}", prefix, Location.TVRegionProperty.Name);
                case LocationImportMappingList.LocationAttributeMapId: return String.Format("{0}{1}", prefix, Location.LocationAttributeProperty.Name);
                case LocationImportMappingList.DefaultClusterAttributeMapId: return String.Format("{0}{1}", prefix, Location.DefaultClusterAttributeProperty.Name);
                case LocationImportMappingList.Address1MapId: return String.Format("{0}{1}", prefix, Location.Address1Property.Name);
                case LocationImportMappingList.Address2MapId: return String.Format("{0}{1}", prefix, Location.Address2Property.Name);
                case LocationImportMappingList.CityMapId: return String.Format("{0}{1}", prefix, Location.CityProperty.Name);
                case LocationImportMappingList.PostalCodeMapId: return String.Format("{0}{1}", prefix, Location.PostalCodeProperty.Name);
                case LocationImportMappingList.CountryMapId: return String.Format("{0}{1}", prefix, Location.CountryProperty.Name);
                case LocationImportMappingList.LatitudeMapId: return String.Format("{0}{1}", prefix, Location.LatitudeProperty.Name);
                case LocationImportMappingList.LongitudeMapId: return String.Format("{0}{1}", prefix, Location.LongitudeProperty.Name);
                case LocationImportMappingList.DateOpenMapId: return String.Format("{0}{1}", prefix, Location.DateOpenProperty.Name);
                case LocationImportMappingList.DateLastRefittedMapId: return String.Format("{0}{1}", prefix, Location.DateLastRefittedProperty.Name);
                case LocationImportMappingList.DateClosedMapId: return String.Format("{0}{1}", prefix, Location.DateClosedProperty.Name);
                case LocationImportMappingList.CarParkSpacesMapId: return String.Format("{0}{1}", prefix, Location.CarParkSpacesProperty.Name);
                case LocationImportMappingList.CarParkManagementMapId: return String.Format("{0}{1}", prefix, Location.CarParkManagementProperty.Name);
                case LocationImportMappingList.PetrolForecourtTypeMapId: return String.Format("{0}{1}", prefix, Location.PetrolForecourtTypeProperty.Name);
                case LocationImportMappingList.RestaurantMapId: return String.Format("{0}{1}", prefix, Location.RestaurantProperty.Name);
                case LocationImportMappingList.SizeGrossFloorAreaMapId: return String.Format("{0}{1}", prefix, Location.SizeGrossFloorAreaProperty.Name);
                case LocationImportMappingList.SizeNetSalesAreaMapId: return String.Format("{0}{1}", prefix, Location.SizeNetSalesAreaProperty.Name);
                case LocationImportMappingList.SizeMezzSalesAreaMapId: return String.Format("{0}{1}", prefix, Location.SizeMezzSalesAreaProperty.Name);
                case LocationImportMappingList.TelephoneCountryCodeMapId: return String.Format("{0}{1}", prefix, Location.TelephoneCountryCodeProperty.Name);
                case LocationImportMappingList.TelephoneAreaCodeMapId: return String.Format("{0}{1}", prefix, Location.TelephoneAreaCodeProperty.Name);
                case LocationImportMappingList.TelephoneNumberMapId: return String.Format("{0}{1}", prefix, Location.TelephoneNumberProperty.Name);
                case LocationImportMappingList.FaxCountryCodeMapId: return String.Format("{0}{1}", prefix, Location.FaxCountryCodeProperty.Name);
                case LocationImportMappingList.FaxAreaCodeMapId: return String.Format("{0}{1}", prefix, Location.FaxAreaCodeProperty.Name);
                case LocationImportMappingList.FaxNumberMapId: return String.Format("{0}{1}", prefix, Location.FaxNumberProperty.Name);
                case LocationImportMappingList.OpeningHoursMapId: return String.Format("{0}{1}", prefix, Location.OpeningHoursProperty.Name);
                case LocationImportMappingList.AverageOpeningHoursMapId: return String.Format("{0}{1}", prefix, Location.AverageOpeningHoursProperty.Name);
                case LocationImportMappingList.ManagerNameMapId: return String.Format("{0}{1}", prefix, Location.ManagerNameProperty.Name);
                case LocationImportMappingList.RegionalManagerNameMapId: return String.Format("{0}{1}", prefix, Location.RegionalManagerNameProperty.Name);
                case LocationImportMappingList.DivisionalManagerNameMapId: return String.Format("{0}{1}", prefix, Location.DivisionalManagerNameProperty.Name);
                case LocationImportMappingList.ManagerEmailMapId: return String.Format("{0}{1}", prefix, Location.ManagerEmailProperty.Name);
                case LocationImportMappingList.RegionalManagerEmailMapId: return String.Format("{0}{1}", prefix, Location.RegionalManagerEmailProperty.Name);
                case LocationImportMappingList.DivisionalManagerEmailMapId: return String.Format("{0}{1}", prefix, Location.DivisionalManagerEmailProperty.Name);
                case LocationImportMappingList.AdvertisingZoneMapId: return String.Format("{0}{1}", prefix, Location.AdvertisingZoneProperty.Name);
                case LocationImportMappingList.DistributionCentreMapId: return String.Format("{0}{1}", prefix, Location.DistributionCentreProperty.Name);
                case LocationImportMappingList.NoOfCheckoutsMapId: return String.Format("{0}{1}", prefix, Location.NoOfCheckoutsProperty.Name);
                case LocationImportMappingList.IsMezzFittedMapId: return String.Format("{0}{1}", prefix, Location.IsMezzFittedProperty.Name);
                case LocationImportMappingList.IsFreeholdMapId: return String.Format("{0}{1}", prefix, Location.IsFreeholdProperty.Name);
                case LocationImportMappingList.Is24HoursMapId: return String.Format("{0}{1}", prefix, Location.Is24HoursProperty.Name);
                case LocationImportMappingList.IsOpenMondayMapId: return String.Format("{0}{1}", prefix, Location.IsOpenMondayProperty.Name);
                case LocationImportMappingList.IsOpenTuesdayMapId: return String.Format("{0}{1}", prefix, Location.IsOpenTuesdayProperty.Name);
                case LocationImportMappingList.IsOpenWednesdayMapId: return String.Format("{0}{1}", prefix, Location.IsOpenWednesdayProperty.Name);
                case LocationImportMappingList.IsOpenThursdayMapId: return String.Format("{0}{1}", prefix, Location.IsOpenThursdayProperty.Name);
                case LocationImportMappingList.IsOpenFridayMapId: return String.Format("{0}{1}", prefix, Location.IsOpenFridayProperty.Name);
                case LocationImportMappingList.IsOpenSaturdayMapId: return String.Format("{0}{1}", prefix, Location.IsOpenSaturdayProperty.Name);
                case LocationImportMappingList.IsOpenSundayMapId: return String.Format("{0}{1}", prefix, Location.IsOpenSundayProperty.Name);
                case LocationImportMappingList.HasPetrolForecourtMapId: return String.Format("{0}{1}", prefix, Location.HasPetrolForecourtProperty.Name);
                case LocationImportMappingList.HasNewsCubeMapId: return String.Format("{0}{1}", prefix, Location.HasNewsCubeProperty.Name);
                case LocationImportMappingList.HasAtmMachinesMapId: return String.Format("{0}{1}", prefix, Location.HasAtmMachinesProperty.Name);
                case LocationImportMappingList.HasCustomerWCMapId: return String.Format("{0}{1}", prefix, Location.HasCustomerWCProperty.Name);
                case LocationImportMappingList.HasBabyChangingMapId: return String.Format("{0}{1}", prefix, Location.HasBabyChangingProperty.Name);
                case LocationImportMappingList.HasInStoreBakeryMapId: return String.Format("{0}{1}", prefix, Location.HasInStoreBakeryProperty.Name);
                case LocationImportMappingList.HasHotFoodToGoMapId: return String.Format("{0}{1}", prefix, Location.HasHotFoodToGoProperty.Name);
                case LocationImportMappingList.HasRotisserieMapId: return String.Format("{0}{1}", prefix, Location.HasRotisserieProperty.Name);
                case LocationImportMappingList.HasFishmongerMapId: return String.Format("{0}{1}", prefix, Location.HasFishmongerProperty.Name);
                case LocationImportMappingList.HasButcherMapId: return String.Format("{0}{1}", prefix, Location.HasButcherProperty.Name);
                case LocationImportMappingList.HasPizzaMapId: return String.Format("{0}{1}", prefix, Location.HasPizzaProperty.Name);
                case LocationImportMappingList.HasDeliMapId: return String.Format("{0}{1}", prefix, Location.HasDeliProperty.Name);
                case LocationImportMappingList.HasSaladBarMapId: return String.Format("{0}{1}", prefix, Location.HasSaladBarProperty.Name);
                case LocationImportMappingList.HasOrganicMapId: return String.Format("{0}{1}", prefix, Location.HasOrganicProperty.Name);
                case LocationImportMappingList.HasGroceryMapId: return String.Format("{0}{1}", prefix, Location.HasGroceryProperty.Name);
                case LocationImportMappingList.HasMobilePhonesMapId: return String.Format("{0}{1}", prefix, Location.HasMobilePhonesProperty.Name);
                case LocationImportMappingList.HasDryCleaningMapId: return String.Format("{0}{1}", prefix, Location.HasDryCleaningProperty.Name);
                case LocationImportMappingList.HasHomeShoppingAvailableMapId: return String.Format("{0}{1}", prefix, Location.HasHomeShoppingAvailableProperty.Name);
                case LocationImportMappingList.HasOpticianMapId: return String.Format("{0}{1}", prefix, Location.HasOpticianProperty.Name);
                case LocationImportMappingList.HasPharmacyMapId: return String.Format("{0}{1}", prefix, Location.HasPharmacyProperty.Name);
                case LocationImportMappingList.HasTravelMapId: return String.Format("{0}{1}", prefix, Location.HasTravelProperty.Name);
                case LocationImportMappingList.HasPhotoDepartmentMapId: return String.Format("{0}{1}", prefix, Location.HasPhotoDepartmentProperty.Name);
                case LocationImportMappingList.HasCarServiceAreaMapId: return String.Format("{0}{1}", prefix, Location.HasCarServiceAreaProperty.Name);
                case LocationImportMappingList.HasGardenCentreMapId: return String.Format("{0}{1}", prefix, Location.HasGardenCentreProperty.Name);
                case LocationImportMappingList.HasClinicMapId: return String.Format("{0}{1}", prefix, Location.HasClinicProperty.Name);
                case LocationImportMappingList.HasAlcoholMapId: return String.Format("{0}{1}", prefix, Location.HasAlcoholProperty.Name);
                case LocationImportMappingList.HasFashionMapId: return String.Format("{0}{1}", prefix, Location.HasFashionProperty.Name);
                case LocationImportMappingList.HasCafeMapId: return String.Format("{0}{1}", prefix, Location.HasCafeProperty.Name);
                case LocationImportMappingList.HasRecyclingMapId: return String.Format("{0}{1}", prefix, Location.HasRecyclingProperty.Name);
                case LocationImportMappingList.HasPhotocopierMapId: return String.Format("{0}{1}", prefix, Location.HasPhotocopierProperty.Name);
                case LocationImportMappingList.HasLotteryMapId: return String.Format("{0}{1}", prefix, Location.HasLotteryProperty.Name);
                case LocationImportMappingList.HasPostOfficeMapId: return String.Format("{0}{1}", prefix, Location.HasPostOfficeProperty.Name);
                case LocationImportMappingList.HasMovieRentalMapId: return String.Format("{0}{1}", prefix, Location.HasMovieRentalProperty.Name);
                case LocationImportMappingList.HasJewelleryMapId: return String.Format("{0}{1}", prefix, Location.HasJewelleryProperty.Name);
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Static method for returning a column group from its mapping id
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public static String GetColumnGroup(Int32 mappingId)
        {
            String detailsGroup = Message.LocationImportMappingList_ColGroup_Details;
            String buildingGroup = Message.LocationImportMappingList_ColGroup_Building;
            String locationAttributesGroup = Message.LocationImportMappingList_ColGroup_LocationAttributes;
            String countersGroup = Message.LocationImportMappingList_ColGroup_Counters;
            String servicesGroup = Message.LocationImportMappingList_ColGroup_Services;
            String noGroup = null;


            switch (mappingId)
            {
                #region Location Properties
                case LocationImportMappingList.CodeMapId: return noGroup;
                case LocationImportMappingList.GroupCodeMapId: return noGroup;
                case LocationImportMappingList.NameMapId: return noGroup;
                case LocationImportMappingList.RegionMapId: return locationAttributesGroup;
                case LocationImportMappingList.CountyMapId: return detailsGroup;
                case LocationImportMappingList.StateMapId: return detailsGroup;
                case LocationImportMappingList.TVRegionMapId: return locationAttributesGroup;
                case LocationImportMappingList.LocationAttributeMapId: return locationAttributesGroup;
                case LocationImportMappingList.DefaultClusterAttributeMapId: return locationAttributesGroup;
                case LocationImportMappingList.Address1MapId: return detailsGroup;
                case LocationImportMappingList.Address2MapId: return detailsGroup;
                case LocationImportMappingList.CityMapId: return detailsGroup;
                case LocationImportMappingList.PostalCodeMapId: return detailsGroup;
                case LocationImportMappingList.CountryMapId: return detailsGroup;
                case LocationImportMappingList.LatitudeMapId: return detailsGroup;
                case LocationImportMappingList.LongitudeMapId: return detailsGroup;
                case LocationImportMappingList.DateOpenMapId: return buildingGroup;
                case LocationImportMappingList.DateLastRefittedMapId: return buildingGroup;
                case LocationImportMappingList.DateClosedMapId: return buildingGroup;
                case LocationImportMappingList.CarParkSpacesMapId: return buildingGroup;
                case LocationImportMappingList.CarParkManagementMapId: return buildingGroup;
                case LocationImportMappingList.PetrolForecourtTypeMapId: return buildingGroup;
                case LocationImportMappingList.RestaurantMapId: return buildingGroup;
                case LocationImportMappingList.SizeGrossFloorAreaMapId: return buildingGroup;
                case LocationImportMappingList.SizeNetSalesAreaMapId: return buildingGroup;
                case LocationImportMappingList.SizeMezzSalesAreaMapId: return buildingGroup;
                case LocationImportMappingList.TelephoneCountryCodeMapId: return detailsGroup;
                case LocationImportMappingList.TelephoneAreaCodeMapId: return detailsGroup;
                case LocationImportMappingList.TelephoneNumberMapId: return detailsGroup;
                case LocationImportMappingList.FaxCountryCodeMapId: return detailsGroup;
                case LocationImportMappingList.FaxAreaCodeMapId: return detailsGroup;
                case LocationImportMappingList.FaxNumberMapId: return detailsGroup;
                case LocationImportMappingList.OpeningHoursMapId: return detailsGroup;
                case LocationImportMappingList.AverageOpeningHoursMapId: return detailsGroup;
                case LocationImportMappingList.ManagerNameMapId: return detailsGroup;
                case LocationImportMappingList.RegionalManagerNameMapId: return detailsGroup;
                case LocationImportMappingList.DivisionalManagerNameMapId: return detailsGroup;
                case LocationImportMappingList.ManagerEmailMapId: return detailsGroup;
                case LocationImportMappingList.RegionalManagerEmailMapId: return detailsGroup;
                case LocationImportMappingList.DivisionalManagerEmailMapId: return detailsGroup;
                case LocationImportMappingList.AdvertisingZoneMapId: return locationAttributesGroup;
                case LocationImportMappingList.DistributionCentreMapId: return locationAttributesGroup;
                case LocationImportMappingList.NoOfCheckoutsMapId: return buildingGroup;
                case LocationImportMappingList.IsMezzFittedMapId: return buildingGroup;
                case LocationImportMappingList.IsFreeholdMapId: return buildingGroup;
                case LocationImportMappingList.Is24HoursMapId: return detailsGroup;
                case LocationImportMappingList.IsOpenMondayMapId: return detailsGroup;
                case LocationImportMappingList.IsOpenTuesdayMapId: return detailsGroup;
                case LocationImportMappingList.IsOpenWednesdayMapId: return detailsGroup;
                case LocationImportMappingList.IsOpenThursdayMapId: return detailsGroup;
                case LocationImportMappingList.IsOpenFridayMapId: return detailsGroup;
                case LocationImportMappingList.IsOpenSaturdayMapId: return detailsGroup;
                case LocationImportMappingList.IsOpenSundayMapId: return detailsGroup;
                case LocationImportMappingList.HasPetrolForecourtMapId: return servicesGroup;
                case LocationImportMappingList.HasNewsCubeMapId: return servicesGroup;
                case LocationImportMappingList.HasAtmMachinesMapId: return buildingGroup;
                case LocationImportMappingList.HasCustomerWCMapId: return buildingGroup;
                case LocationImportMappingList.HasBabyChangingMapId: return buildingGroup;
                case LocationImportMappingList.HasInStoreBakeryMapId: return countersGroup;
                case LocationImportMappingList.HasHotFoodToGoMapId: return countersGroup;
                case LocationImportMappingList.HasRotisserieMapId: return countersGroup;
                case LocationImportMappingList.HasFishmongerMapId: return countersGroup;
                case LocationImportMappingList.HasButcherMapId: return countersGroup;
                case LocationImportMappingList.HasPizzaMapId: return countersGroup;
                case LocationImportMappingList.HasDeliMapId: return countersGroup;
                case LocationImportMappingList.HasSaladBarMapId: return countersGroup;
                case LocationImportMappingList.HasOrganicMapId: return countersGroup;
                case LocationImportMappingList.HasGroceryMapId: return servicesGroup;
                case LocationImportMappingList.HasMobilePhonesMapId: return servicesGroup;
                case LocationImportMappingList.HasDryCleaningMapId: return servicesGroup;
                case LocationImportMappingList.HasHomeShoppingAvailableMapId: return servicesGroup;
                case LocationImportMappingList.HasOpticianMapId: return servicesGroup;
                case LocationImportMappingList.HasPharmacyMapId: return servicesGroup;
                case LocationImportMappingList.HasTravelMapId: return servicesGroup;
                case LocationImportMappingList.HasPhotoDepartmentMapId: return servicesGroup;
                case LocationImportMappingList.HasCarServiceAreaMapId: return servicesGroup;
                case LocationImportMappingList.HasGardenCentreMapId: return servicesGroup;
                case LocationImportMappingList.HasClinicMapId: return servicesGroup;
                case LocationImportMappingList.HasAlcoholMapId: return servicesGroup;
                case LocationImportMappingList.HasFashionMapId: return servicesGroup;
                case LocationImportMappingList.HasCafeMapId: return servicesGroup;
                case LocationImportMappingList.HasRecyclingMapId: return servicesGroup;
                case LocationImportMappingList.HasPhotocopierMapId: return servicesGroup;
                case LocationImportMappingList.HasLotteryMapId: return servicesGroup;
                case LocationImportMappingList.HasPostOfficeMapId: return servicesGroup;
                case LocationImportMappingList.HasMovieRentalMapId: return servicesGroup;
                case LocationImportMappingList.HasJewelleryMapId: return servicesGroup;
                #endregion

                default: throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Returns true if the field with the given mapping id should have a display uom converter applied.
        /// </summary>
        /// <param name="mappingId"></param>
        /// <returns></returns>
        public static Boolean IsUOMDisplayConversionRequired(Int32 mappingId)
        {
            switch (mappingId)
            {
                case LocationImportMappingList.SizeGrossFloorAreaMapId:
                case LocationImportMappingList.SizeNetSalesAreaMapId:
                case LocationImportMappingList.SizeMezzSalesAreaMapId:
                    return true;


                default: return false;
            }

        }

        internal static void SetIsSetPropertyByMappingId(Int32 mappingId, LocationIsSetDto isSetDto)
        {
            switch (mappingId)
            {
                #region MainProperties
                case LocationImportMappingList.Address1MapId:
                    isSetDto.IsAddress1Set = true;
                    break;
                case LocationImportMappingList.Address2MapId:
                    isSetDto.IsAddress2Set = true;
                    break;
                case LocationImportMappingList.AdvertisingZoneMapId:
                    isSetDto.IsAdvertisingZoneSet = true;
                    break;
                case LocationImportMappingList.AverageOpeningHoursMapId:
                    isSetDto.IsAverageOpeningHoursSet = true;
                    break;
                case LocationImportMappingList.CarParkManagementMapId:
                    isSetDto.IsCarParkManagementSet = true;
                    break;
                case LocationImportMappingList.CarParkSpacesMapId:
                    isSetDto.IsCarParkSpacesSet = true;
                    break;
                case LocationImportMappingList.CityMapId:
                    isSetDto.IsCitySet = true;
                    break;
                case LocationImportMappingList.CodeMapId:
                    isSetDto.IsCodeSet = true;
                    break;
                case LocationImportMappingList.CountryMapId:
                    isSetDto.IsCountrySet = true;
                    break;
                case LocationImportMappingList.CountyMapId:
                    isSetDto.IsCountySet = true;
                    break;
                case LocationImportMappingList.StateMapId:
                    isSetDto.IsStateSet = true;
                    break;
                case LocationImportMappingList.DateClosedMapId:
                    isSetDto.IsDateClosedSet = true;
                    break;
                case LocationImportMappingList.DateLastRefittedMapId:
                    isSetDto.IsDateLastRefittedSet = true;
                    break;
                case LocationImportMappingList.DateOpenMapId:
                    isSetDto.IsDateOpenSet = true;
                    break;
                case LocationImportMappingList.DefaultClusterAttributeMapId:
                    isSetDto.IsDefaultClusterAttributeSet = true;
                    break;
                case LocationImportMappingList.DistributionCentreMapId:
                    isSetDto.IsDistributionCentreSet = true;
                    break;
                case LocationImportMappingList.DivisionalManagerEmailMapId:
                    isSetDto.IsDivisionalManagerEmailSet = true;
                    break;
                case LocationImportMappingList.DivisionalManagerNameMapId:
                    isSetDto.IsDivisionalManagerNameSet = true;
                    break;
                case LocationImportMappingList.FaxAreaCodeMapId:
                    isSetDto.IsFaxAreaCodeSet = true;
                    break;
                case LocationImportMappingList.FaxCountryCodeMapId:
                    isSetDto.IsFaxCountryCodeSet = true;
                    break;
                case LocationImportMappingList.FaxNumberMapId:
                    isSetDto.IsFaxNumberSet = true;
                    break;
                case LocationImportMappingList.GroupCodeMapId:
                    isSetDto.IsLocationGroupIdSet = true;
                    break;
                case LocationImportMappingList.HasAlcoholMapId:
                    isSetDto.IsHasAlcoholSet = true;
                    break;
                case LocationImportMappingList.HasAtmMachinesMapId:
                    isSetDto.IsHasAtmMachinesSet = true;
                    break;
                case LocationImportMappingList.HasBabyChangingMapId:
                    isSetDto.IsHasBabyChangingSet = true;
                    break;
                case LocationImportMappingList.HasButcherMapId:
                    isSetDto.IsHasButcherSet = true;
                    break;
                case LocationImportMappingList.HasCafeMapId:
                    isSetDto.IsHasCafeSet = true;
                    break;
                case LocationImportMappingList.HasCarServiceAreaMapId:
                    isSetDto.IsHasCarServiceAreaSet = true;
                    break;
                case LocationImportMappingList.HasClinicMapId:
                    isSetDto.IsHasClinicSet = true;
                    break;
                case LocationImportMappingList.HasCustomerWCMapId:
                    isSetDto.IsHasCustomerWCSet = true;
                    break;
                case LocationImportMappingList.HasDeliMapId:
                    isSetDto.IsHasDeliSet = true;
                    break;
                case LocationImportMappingList.HasDryCleaningMapId:
                    isSetDto.IsHasDryCleaningSet = true;
                    break;
                case LocationImportMappingList.HasFashionMapId:
                    isSetDto.IsHasFashionSet = true;
                    break;
                case LocationImportMappingList.HasFishmongerMapId:
                    isSetDto.IsHasFishmongerSet = true;
                    break;
                case LocationImportMappingList.HasGardenCentreMapId:
                    isSetDto.IsHasGardenCentreSet = true;
                    break;
                case LocationImportMappingList.HasGroceryMapId:
                    isSetDto.IsHasGrocerySet = true;
                    break;
                case LocationImportMappingList.HasHomeShoppingAvailableMapId:
                    isSetDto.IsHasHomeShoppingAvailableSet = true;
                    break;
                case LocationImportMappingList.HasHotFoodToGoMapId:
                    isSetDto.IsHasHotFoodToGoSet = true;
                    break;
                case LocationImportMappingList.HasInStoreBakeryMapId:
                    isSetDto.IsHasInStoreBakerySet = true;
                    break;
                case LocationImportMappingList.HasJewelleryMapId:
                    isSetDto.IsHasJewellerySet = true;
                    break;
                case LocationImportMappingList.HasLotteryMapId:
                    isSetDto.IsHasLotterySet = true;
                    break;
                case LocationImportMappingList.HasMobilePhonesMapId:
                    isSetDto.IsHasMobilePhonesSet = true;
                    break;
                case LocationImportMappingList.HasMovieRentalMapId:
                    isSetDto.IsHasMovieRentalSet = true;
                    break;
                case LocationImportMappingList.HasNewsCubeMapId:
                    isSetDto.IsHasNewsCubeSet = true;
                    break;
                case LocationImportMappingList.HasOpticianMapId:
                    isSetDto.IsHasOpticianSet = true;
                    break;
                case LocationImportMappingList.HasOrganicMapId:
                    isSetDto.IsHasOrganicSet = true;
                    break;
                case LocationImportMappingList.HasPetrolForecourtMapId:
                    isSetDto.IsHasPetrolForecourtSet = true;
                    break;
                case LocationImportMappingList.HasPharmacyMapId:
                    isSetDto.IsHasPharmacySet = true;
                    break;
                case LocationImportMappingList.HasPhotocopierMapId:
                    isSetDto.IsHasPhotocopierSet = true;
                    break;
                case LocationImportMappingList.HasPhotoDepartmentMapId:
                    isSetDto.IsHasPhotoDepartmentSet = true;
                    break;
                case LocationImportMappingList.HasPizzaMapId:
                    isSetDto.IsHasPizzaSet = true;
                    break;
                case LocationImportMappingList.HasPostOfficeMapId:
                    isSetDto.IsHasPostOfficeSet = true;
                    break;
                case LocationImportMappingList.HasRecyclingMapId:
                    isSetDto.IsHasRecyclingSet = true;
                    break;
                case LocationImportMappingList.HasRotisserieMapId:
                    isSetDto.IsHasRotisserieSet = true;
                    break;
                case LocationImportMappingList.HasSaladBarMapId:
                    isSetDto.IsHasSaladBarSet = true;
                    break;
                case LocationImportMappingList.HasTravelMapId:
                    isSetDto.IsHasTravelSet = true;
                    break;
                case LocationImportMappingList.Is24HoursMapId:
                    isSetDto.IsIs24HoursSet = true;
                    break;
                case LocationImportMappingList.IsFreeholdMapId:
                    isSetDto.IsIsFreeholdSet = true;
                    break;
                case LocationImportMappingList.IsMezzFittedMapId:
                    isSetDto.IsIsMezzFittedSet = true;
                    break;
                case LocationImportMappingList.IsOpenFridayMapId:
                    isSetDto.IsIsOpenFridaySet = true;
                    break;
                case LocationImportMappingList.IsOpenMondayMapId:
                    isSetDto.IsIsOpenMondaySet = true;
                    break;
                case LocationImportMappingList.IsOpenSaturdayMapId:
                    isSetDto.IsIsOpenSaturdaySet = true;
                    break;
                case LocationImportMappingList.IsOpenSundayMapId:
                    isSetDto.IsIsOpenSundaySet = true;
                    break;
                case LocationImportMappingList.IsOpenThursdayMapId:
                    isSetDto.IsIsOpenThursdaySet = true;
                    break;
                case LocationImportMappingList.IsOpenTuesdayMapId:
                    isSetDto.IsIsOpenTuesdaySet = true;
                    break;
                case LocationImportMappingList.IsOpenWednesdayMapId:
                    isSetDto.IsIsOpenWednesdaySet = true;
                    break;
                case LocationImportMappingList.LatitudeMapId:
                    isSetDto.IsLatitudeSet = true;
                    break;
                case LocationImportMappingList.LocationAttributeMapId:
                    isSetDto.IsLocationSet = true;
                    break;
                case LocationImportMappingList.LongitudeMapId:
                    isSetDto.IsLongitudeSet = true;
                    break;
                case LocationImportMappingList.ManagerEmailMapId:
                    isSetDto.IsManagerEmailSet = true;
                    break;
                case LocationImportMappingList.ManagerNameMapId:
                    isSetDto.IsManagerNameSet = true;
                    break;
                case LocationImportMappingList.NameMapId:
                    isSetDto.IsNameSet = true;
                    break;
                case LocationImportMappingList.NoOfCheckoutsMapId:
                    isSetDto.IsNoOfCheckoutsSet = true;
                    break;
                case LocationImportMappingList.OpeningHoursMapId:
                    isSetDto.IsOpeningHoursSet = true;
                    break;
                case LocationImportMappingList.PetrolForecourtTypeMapId:
                    isSetDto.IsPetrolForecourtTypeSet = true;
                    break;
                case LocationImportMappingList.PostalCodeMapId:
                    isSetDto.IsPostalCodeSet = true;
                    break;
                case LocationImportMappingList.RegionalManagerEmailMapId:
                    isSetDto.IsRegionalManagerEmailSet = true;
                    break;
                case LocationImportMappingList.RegionalManagerNameMapId:
                    isSetDto.IsRegionalManagerNameSet = true;
                    break;
                case LocationImportMappingList.RegionMapId:
                    isSetDto.IsRegionSet = true;
                    break;
                case LocationImportMappingList.RestaurantMapId:
                    isSetDto.IsRestaurantSet = true;
                    break;
                case LocationImportMappingList.SizeGrossFloorAreaMapId:
                    isSetDto.IsSizeGrossFloorAreaSet = true;
                    break;
                case LocationImportMappingList.SizeMezzSalesAreaMapId:
                    isSetDto.IsSizeMezzSalesAreaSet = true;
                    break;
                case LocationImportMappingList.SizeNetSalesAreaMapId:
                    isSetDto.IsSizeNetSalesAreaSet = true;
                    break;
                case LocationImportMappingList.TelephoneAreaCodeMapId:
                    isSetDto.IsTelephoneAreaCodeSet = true;
                    break;
                case LocationImportMappingList.TelephoneCountryCodeMapId:
                    isSetDto.IsTelephoneCountryCodeSet = true;
                    break;
                case LocationImportMappingList.TelephoneNumberMapId:
                    isSetDto.IsTelephoneNumberSet = true;
                    break;
                case LocationImportMappingList.TVRegionMapId:
                    isSetDto.IsTVRegionSet = true;
                    break;

                #endregion

                default: throw new NotImplementedException();
            }
        }

        #endregion

        #region IModelObjectColumnHelper members

        //Boolean IModelObjectColumnHelper.IsUOMDisplayConversionRequired(Int32 mappingId)
        //{
        //    return LocationImportMappingList.IsUOMDisplayConversionRequired(mappingId);
        //}

        /// <summary>
        /// Implementation of the GetColumnGroupName method
        /// </summary>
        /// <param name="mapping"></param>
        /// <returns></returns>
        String IModelObjectColumnHelper.GetColumnGroupName(Int32 mapping)
        {
            return LocationImportMappingList.GetColumnGroup(mapping);
        }

        #endregion
    }
}
