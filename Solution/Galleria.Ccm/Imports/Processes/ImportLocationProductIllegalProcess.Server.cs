﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationProductIllegalProcess
    {
        #region Constants

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} Location Product Illegal started.", this.ValidRowsCount);

            ImportLocationProductIllegals();

            Trace.TraceInformation("Import of {0} Location Product Illegal complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Main Import
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportLocationProductIllegals()
        {
            //get a list of all mappings that need to be processed
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            //now perform the import
            //don't dispose of the dal factory instances fetched from the dalcontainer
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                EntityDto currentEntity = null;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(this.EntityId);
                }

                //Create dictionary to lookup location id
                Dictionary<String, Int16> locationIdLookupDictionary = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> locationInfoDtos = dal.FetchByEntityId(this.EntityId);

                    //Add codes to dictionary for lookup later on
                    foreach (LocationInfoDto info in locationInfoDtos)
                    {
                        if (!locationIdLookupDictionary.ContainsKey(info.Code))
                        {
                            locationIdLookupDictionary.Add(info.Code, info.Id);
                        }
                    }

                    locationInfoDtos = null;
                }

                //Create dictionary to lookup product id
                Dictionary<String, Int32> productIdLookupDictionary = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> productInfoDtos = dal.FetchByEntityId(this.EntityId);

                    //Add GTIN's to dictionary for lookup later on
                    foreach (ProductInfoDto info in productInfoDtos)
                    {
                        if (!productIdLookupDictionary.ContainsKey(info.Gtin))
                        {
                            productIdLookupDictionary.Add(info.Gtin, info.Id);
                        }
                    }

                    productInfoDtos = null;
                }

                //now cycle through the rows
                List<LocationProductIllegalDto> locationProductIllegalDtoList = new List<LocationProductIllegalDto>();
                Dictionary<String, Int16> parentLocationObjects = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                Dictionary<String, Int32> parentProductObjects = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    //create the insert dtos
                    LocationProductIllegalDto locationProductIllegalDto = new LocationProductIllegalDto();
                    locationProductIllegalDto.DateCreated = DateTime.UtcNow;
                    locationProductIllegalDto.DateLastModified = DateTime.UtcNow;
                    locationProductIllegalDtoList.Add(locationProductIllegalDto);

                    //entityId
                    locationProductIllegalDto.EntityId = this.EntityId;

                    String locationCode = row[LocationProductIllegalImportMappingList.LocationCodeMapId].ToString();

                    if (parentLocationObjects.ContainsKey(locationCode))
                    {
                        locationProductIllegalDto.LocationId = parentLocationObjects[locationCode];
                    }
                    else
                    {
                        //Lookup location code in dictionary
                        Int16 locationId;
                        if (locationIdLookupDictionary.TryGetValue(locationCode, out locationId))
                        {
                            locationProductIllegalDto.LocationId = locationId;
                            parentLocationObjects.Add(locationCode, locationId);
                        }
                    }

                    String productGTIN = row[LocationProductIllegalImportMappingList.ProductGTINMapId].ToString();

                    if (parentProductObjects.ContainsKey(productGTIN))
                    {
                        locationProductIllegalDto.ProductId = parentProductObjects[productGTIN];
                    }
                    else
                    {
                        //Lookup product gtin in dictionary
                        Int32 productId;
                        if (productIdLookupDictionary.TryGetValue(productGTIN, out productId))
                        {
                            locationProductIllegalDto.ProductId = productId;
                            parentProductObjects.Add(productGTIN, productId);
                        }
                    }

                    //loop through other resolved mappings
                    //IFormatProvider prov = CultureInfo.InvariantCulture;
                    //foreach (ImportMapping mapping in resolvedMappings)
                    //{
                    //    Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                    //    Object cellValue = row[columnNumber].CurrentValue;

                    //    //use mapping list method to set the value
                    //    LocationProductIllegalImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, locationProductIllegalDto);
                    //}
                }

                if (locationProductIllegalDtoList.Count > 0)
                {
                    //upsert all the locationproductillegal list
                    using (ILocationProductIllegalDal dal = dalContext.GetDal<ILocationProductIllegalDal>())
                    {
                        dal.Upsert(locationProductIllegalDtoList);
                    }

                    //clear out the lists
                    locationProductIllegalDtoList.Clear();
                }

                dalContext.Commit();
            }
        }

        #endregion
    }
}
