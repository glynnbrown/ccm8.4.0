﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;


namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Import class for Merchandising Hierarchy
    /// </summary>
    [Serializable]
    public partial class ImportMerchandisingHierarchyProcess : ImportProcessBase<ImportMerchandisingHierarchyProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ImportMerchandisingHierarchyProcess(Int32 entityId, ImportFileData importData) :
            base(entityId, importData)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ImportMerchandisingHierarchyProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportMerchandisingHierarchyProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportMerchandisingHierarchyProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportMerchandisingHierarchyData.ToString()));
            BusinessRules.AddRule(typeof(ImportMerchandisingHierarchyProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
