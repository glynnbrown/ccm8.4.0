﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Cells;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using System.Diagnostics;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportLocationProductAttributeProcess
    {
        #region Methods

        protected override void OnExecute()
        {
            //get the mapping list of the export type
            IImportMappingList mappingList = LocationProductAttributeImportMappingList.NewLocationProductAttributeImportMappingList();

            //create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }

            //now write the data if required
            if (this.ExportHeadersOnly)
            {
                //apply a text style format to each column
                Style textDataStyle = new Style();
                textDataStyle.Number = 49;
                StyleFlag flag = new StyleFlag();
                flag.All = true;

                //set column data formats
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells.Columns[i].ApplyStyle(textDataStyle, flag);
                }
            }
            else
            {
                IEnumerable<LocationInfoDto> locationInfoDtoList;
                IEnumerable<ProductInfoDto> productInfoDtoList;
                IEnumerable<LocationProductAttributeDto> locationProductAttributeDtoList;

                //get the data to be exported
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    //Fetch list of locations to retrieve codes
                    using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                    {
                        locationInfoDtoList = dal.FetchByEntityId(this.EntityId);
                    }
                    //Fetch list of products to retrieve GTINs
                    using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                    {
                        productInfoDtoList = dal.FetchByEntityId(this.EntityId);
                    }
                    using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                    {
                        locationProductAttributeDtoList = dal.FetchByEntityId(this.EntityId);
                    }
                }

                //now run through and write all the locationProductIllegals to the file
                Int32 row = 1;
                foreach (LocationProductAttributeDto link in locationProductAttributeDtoList)
                {
                    //cycle column mappings
                    for (Int32 i = 0; i < mappingList.Count; i++)
                    {
                        ImportMapping mapping = mappingList[i];

                        //get the value to be exported
                        Object cellValue =
                            LocationProductAttributeImportMappingList.GetValueByMappingId(mapping.PropertyIdentifier, link);
                        String cellStringValue = null;

                        switch (mapping.PropertyIdentifier)
                        {
                            case LocationProductAttributeImportMappingList.LocationCodeMapId:
                                //Get location code to write
                                cellStringValue = locationInfoDtoList.FirstOrDefault(l => l.Id == (Int16)cellValue).Code;
                                break;

                            case LocationProductAttributeImportMappingList.ProductGTINMapId:
                                //get product code to write
                                cellStringValue = productInfoDtoList.FirstOrDefault(p => p.Id == (Int32)cellValue).Gtin;
                                break;

                            default:
                                cellStringValue = (cellValue != null) ? cellValue.ToString() : "";
                                break;
                        }

                        //write to worksheet
                        WpfHelper.SetColumnValue(workSheet.Cells[row, i], (cellStringValue != null) ? cellStringValue : null);
                    }
                    row++;
                }
            }

            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} location product attribute records exported successfully.", workSheet.Cells.Rows.Count);
        }

        #endregion
    }
}
