﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspose.Cells;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using System.Diagnostics;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportLocationProductLegalProcess
    {
        #region Methods

        protected override void OnExecute()
        {
            //get the mapping list of the export type
            IImportMappingList mappingList = LocationProductLegalImportMappingList.NewLocationProductLegalImportMappingList();

            //create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }

            //now write the data if required
            if (this.ExportHeadersOnly)
            {
                //apply a text style format to each column
                Style textDataStyle = new Style();
                textDataStyle.Number = 49;
                StyleFlag flag = new StyleFlag();
                flag.All = true;

                //set column data formats
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells.Columns[i].ApplyStyle(textDataStyle, flag);
                }
            }
            else
            {
                IEnumerable<LocationInfoDto> locationInfoDtoList;
                IEnumerable<ProductInfoDto> productInfoDtoList;
                IEnumerable<LocationProductLegalDto> locationProductLegalDtoList;

                //[TODO] consider if there are any conditions which define that we are only 
                //exporting a subset of the data rather than everything for the entity.

                //get the data to be exported
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    //Fetch list of locations to retrieve codes
                    using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                    {
                        locationInfoDtoList = dal.FetchByEntityId(this.EntityId);
                    }
                    //Fetch list of products to retrieve GTINs
                    using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                    {
                        productInfoDtoList = dal.FetchByEntityId(this.EntityId);
                    }
                    using (ILocationProductLegalDal dal = dalContext.GetDal<ILocationProductLegalDal>())
                    {
                        locationProductLegalDtoList = dal.FetchByEntityId(this.EntityId);
                    }
                }

                //now run through and write all the locationProductLegals to the file
                Int32 row = 1;
                foreach (LocationProductLegalDto link in locationProductLegalDtoList)
                {
                    //cycle column mappings
                    for (Int32 i = 0; i < mappingList.Count; i++)
                    {
                        ImportMapping mapping = mappingList[i];

                        //get the value to be exported
                        Object cellValue =
                            LocationProductLegalImportMappingList.GetValueByMappingId(mapping.PropertyIdentifier, link);
                        String cellStringValue = null;

                        if (mapping.PropertyIdentifier == LocationProductLegalImportMappingList.LocationCodeMapId)
                        {
                            //Get location code to write
                            cellStringValue = locationInfoDtoList.FirstOrDefault(l => l.Id == (Int16)cellValue).Code;
                        }
                        if (mapping.PropertyIdentifier == LocationProductLegalImportMappingList.ProductGTINMapId)
                        {
                            //get product code to write
                            cellStringValue = productInfoDtoList.FirstOrDefault(p => p.Id == (Int32)cellValue).Gtin;
                        }

                        //write to worksheet
                        WpfHelper.SetColumnValue(workSheet.Cells[row, i], (cellStringValue != null) ? cellStringValue : null);
                    }
                    row++;
                }
            }

            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} location product Legal records exported successfully.", workSheet.Cells.Rows.Count);
        }

        #endregion
    }
}
