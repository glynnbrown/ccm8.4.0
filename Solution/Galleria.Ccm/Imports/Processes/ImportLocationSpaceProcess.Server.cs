﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using SmartAssembly.Attributes;
using Galleria.Framework.Imports;
using Galleria.Framework.Dal;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Globalization;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationSpaceProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} location space bays started.", this.ImportData.Rows.Count);

            ImportLocationSpace();

            Trace.TraceInformation("Import of {0} location space bays complete.", this.ImportData.Rows.Count);
        }

        /// <summary>
        /// Does the import.
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportLocationSpace()
        {
            Int32 entityId = this.EntityId;

            //get a list of all mappings that need to be processed
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            //Construct dictionary of location space id and and location id
            Dictionary<Int16, Int32> locationSpaceProductGroupLookupList = new Dictionary<Int16, Int32>();
            Dictionary<Int16, Int32> locationSpaceLookupList = new Dictionary<Int16, Int32>();
            Dictionary<String, Int16> locationLookup = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
            Dictionary<String, Int32> productGroupLookup = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);

            ImportMapping locationCodeMapping = this.GetColumnMapping(LocationSpaceImportMappingList.LocationCodeMappingId);
            ImportMapping productGroupCodeMapping = this.GetColumnMapping(LocationSpaceImportMappingList.ProductGroupCodeMappingId);

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //get the entity
                EntityDto currentEntity = null;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(entityId);
                }

                #region Create Lookup Lists

                // build our optimised location lookup
                IEnumerable<LocationInfoDto> locationDtoList;
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    locationDtoList = dal.FetchByEntityId(entityId);
                }
                foreach (LocationInfoDto locationDto in locationDtoList)
                {
                    locationLookup.Add(locationDto.Code, locationDto.Id);
                }
                locationDtoList = null;

                //build our optimised location space product group lookup
                IEnumerable<LocationSpaceSearchCriteriaDto> searchCriteriaList;
                using (ILocationSpaceSearchCriteriaDal dal = dalContext.GetDal<ILocationSpaceSearchCriteriaDal>())
                {
                    searchCriteriaList = dal.FetchByEntityId(this.EntityId).ToList();
                }
                foreach (LocationSpaceSearchCriteriaDto locationSpaceSearchCriteria in searchCriteriaList)
                {
                    Int16 locationId = locationLookup[locationSpaceSearchCriteria.LocationCode];

                    if (!locationSpaceProductGroupLookupList.ContainsKey(locationId))
                    {
                        locationSpaceProductGroupLookupList.Add(locationId, locationSpaceSearchCriteria.LocationSpaceId);
                    }
                }
                searchCriteriaList = null;

                //build our optimised location space lookup
                IEnumerable<LocationSpaceInfoDto> locationSpaceDtoList;
                using (ILocationSpaceInfoDal dal = dalContext.GetDal<ILocationSpaceInfoDal>())
                {
                    locationSpaceDtoList = dal.FetchByEntityId(this.EntityId);
                }
                foreach (LocationSpaceInfoDto locationSpaceInfoDto in locationSpaceDtoList)
                {
                    if (locationSpaceInfoDto.LocationId != null)
                    {
                        if (!locationSpaceLookupList.ContainsKey((Int16)locationSpaceInfoDto.LocationId))
                        {
                            locationSpaceLookupList.Add((Int16)locationSpaceInfoDto.LocationId, locationSpaceInfoDto.Id);
                        }
                    }
                }
                locationSpaceDtoList = null;

                //Get product hierarchy for this entity
                ProductHierarchyDto productHierarchyDto;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    productHierarchyDto = dal.FetchByEntityId(entityId);
                }

                // build our optimised product group lookup
                IEnumerable<ProductGroupDto> productGroupSourceDtoList;
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    productGroupSourceDtoList = dal.FetchByProductHierarchyId(productHierarchyDto.Id);
                }
                foreach (ProductGroupDto productGroupDto in productGroupSourceDtoList)
                {
                    productGroupLookup.Add(productGroupDto.Code, productGroupDto.Id);
                }
                productGroupSourceDtoList = null;

                #endregion

                //List to hold all product groups for import
                List<LocationSpaceProductGroupDto> existingLocationSpaceProductGroupDtoList = new List<LocationSpaceProductGroupDto>();
                List<Tuple<LocationSpaceProductGroupDto, String>> newLocationSpaceProductGroupDtoList = new List<Tuple<LocationSpaceProductGroupDto, String>>();

                //Create a whole bunch of Location Space Product Group Dto's
                foreach (ImportFileDataRow row in this.ImportData.Rows)
                {
                    //Create dto to hold data
                    LocationSpaceProductGroupDto locationSpaceProductGroupDto = new LocationSpaceProductGroupDto();

                    String locationCode = String.Empty;
                    String productGroupCode = String.Empty;

                    //loop through other resolved mappings
                    IFormatProvider prov = CultureInfo.InvariantCulture;
                    foreach (ImportMapping mapping in resolvedMappings)
                    {
                        Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                        Object cellValue = row[columnNumber].Value;

                        //Correct null value checks
                        cellValue = ProcessHelpers.CheckNullCellValues(mapping, cellValue);

                        if (mapping.PropertyIdentifier == LocationSpaceImportMappingList.LocationCodeMappingId)
                        {
                            locationCode = cellValue.ToString();
                        }
                        else if (mapping.PropertyIdentifier == LocationSpaceImportMappingList.ProductGroupCodeMappingId)
                        {
                            productGroupCode = cellValue.ToString();
                        }
                        else
                        {
                            //use the mapping list method to set the value
                            LocationSpaceImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, locationSpaceProductGroupDto);
                        }
                    }

                    //lookup product group id
                    locationSpaceProductGroupDto.ProductGroupId = productGroupLookup[productGroupCode];

                    //lookup location id
                    Int16 locationId = locationLookup[locationCode];

                    //try and lookup location space id
                    Int32 locationSpaceId;
                    if (locationSpaceProductGroupLookupList.TryGetValue(locationId, out locationSpaceId))
                    {
                        locationSpaceProductGroupDto.LocationSpaceId = locationSpaceId;
                        existingLocationSpaceProductGroupDtoList.Add(locationSpaceProductGroupDto);
                    }
                    else if (locationSpaceLookupList.TryGetValue(locationId, out locationSpaceId))
                    {
                        locationSpaceProductGroupDto.LocationSpaceId = locationSpaceId;
                        existingLocationSpaceProductGroupDtoList.Add(locationSpaceProductGroupDto);
                    }
                    else
                    {
                        newLocationSpaceProductGroupDtoList.Add(new Tuple<LocationSpaceProductGroupDto, string>(locationSpaceProductGroupDto, locationCode));
                    }
                }

                //Enumerate through all existing to create new versions
                foreach (Int32 locationSpaceId in existingLocationSpaceProductGroupDtoList.Select(p => p.LocationSpaceId).Distinct())
                {
                    //Fetch all location space product groups for location space
                    IEnumerable<LocationSpaceProductGroupDto> productGroupDtoList = existingLocationSpaceProductGroupDtoList.Where(p => p.LocationSpaceId == locationSpaceId);

                    //Fetch location space
                    LocationSpace locationSpace = LocationSpace.GetLocationSpaceById(locationSpaceId);

                    //clear existing product groups
                    locationSpace.LocationSpaceProductGroups.Clear();

                    //Update product groups
                    foreach (LocationSpaceProductGroupDto productGroupDto in productGroupDtoList)
                    {
                        LocationSpaceProductGroup productGroup = LocationSpaceProductGroup.NewLocationSpaceProductGroup(false);
                        productGroup.ProductGroupId = productGroupDto.ProductGroupId;
                        productGroup.BayCount = productGroupDto.BayCount;
                        productGroup.ProductCount = productGroupDto.ProductCount;
                        productGroup.AverageBayWidth = productGroupDto.AverageBayWidth;

                        locationSpace.LocationSpaceProductGroups.Add(productGroup);
                    }

                    //Save
                    locationSpace.Save();
                }

                //Enumerate through all existing to create new first version by 
                foreach (String locationCode in newLocationSpaceProductGroupDtoList.Select(p => p.Item2).Distinct())
                {
                    //Fetch all location space product groups for location space
                    IEnumerable<LocationSpaceProductGroupDto> productGroupDtoList = newLocationSpaceProductGroupDtoList.Where(p => p.Item2 == locationCode).Select(p => p.Item1);

                    //Create new objects
                    LocationSpace newLocationSpace = LocationSpace.NewLocationSpace(this.EntityId);

                    //Lookup location details and set for header record
                    Int16 locationId = locationLookup[locationCode];
                    newLocationSpace.LocationId = locationId;

                    //Add product groups
                    foreach (LocationSpaceProductGroupDto productGroupDto in productGroupDtoList)
                    {
                        LocationSpaceProductGroup productGroup = LocationSpaceProductGroup.NewLocationSpaceProductGroup(false);
                        productGroup.ProductGroupId = productGroupDto.ProductGroupId;
                        productGroup.BayCount = productGroupDto.BayCount;
                        productGroup.ProductCount = productGroupDto.ProductCount;
                        productGroup.AverageBayWidth = productGroupDto.AverageBayWidth;

                        newLocationSpace.LocationSpaceProductGroups.Add(productGroup);
                    }

                    //Save 
                    newLocationSpace.Save();
                }

                dalContext.Commit();
            }
        }

        #endregion
    }
}
