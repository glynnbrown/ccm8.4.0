﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-25455 : J.Pickup
//  Copied over from GFS 
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
// V8-27982 : J.Pickup
//  No longer marks as initialised when finds existing. 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Csla;
using SmartAssembly.Attributes;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using System.Collections;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportAssortmentProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} ImportAssortmentProcess started.", this.ValidRowsCount);

            ImportAssortments();

            Trace.TraceInformation("Import of {0} ImportAssortmentProcess complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Main Import
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportAssortments()
        {
            //get a list of all mappings that need to be processed
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            Dictionary<String, Int32> productGroupLookup = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);
            Dictionary<String, ProductInfoDto> productLookup = new Dictionary<String, ProductInfoDto>(StringComparer.OrdinalIgnoreCase);
            Dictionary<String, Int32> assortmentLookupList = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);

            //now perform the import
            //don't dispose of the dal factory instances fetched from the dalcontainer
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //build out optimised product group lookup
                ProductHierarchyDto hierarchyDto;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyDto = dal.FetchByEntityId(this.EntityId);
                }
                List<ProductGroupDto> productGroupDtoList = new List<ProductGroupDto>();
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    productGroupDtoList = dal.FetchByProductHierarchyId(hierarchyDto.Id).ToList();
                }
                foreach (ProductGroupDto productGroupDto in productGroupDtoList)
                {
                    productGroupLookup.Add(productGroupDto.Code, productGroupDto.Id);
                }
                productGroupDtoList = null;

                // build our optimised location lookup
                IEnumerable<ProductInfoDto> productInfoDtoList;
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    productInfoDtoList = dal.FetchByEntityId(this.EntityId);
                }

                foreach (ProductInfoDto productInfoDto in productInfoDtoList)
                {
                    productLookup.Add(productInfoDto.Gtin, productInfoDto);
                }
                productInfoDtoList = null;

                //build our optimised assortment lookup
                List<AssortmentInfoDto> assortmentInfoDtoList;
                using (IAssortmentInfoDal dal = dalContext.GetDal<IAssortmentInfoDal>())
                {
                    assortmentInfoDtoList = dal.FetchByEntityId(this.EntityId).ToList();
                }

                // GFS-24718 Get latest version of assortment or first version for deleteted records
                IEnumerable<IGrouping<String, AssortmentInfoDto>> groupedAssortments = assortmentInfoDtoList.GroupBy(a => a.Name);
                foreach (IGrouping<String, AssortmentInfoDto> assortmentinfoGroup in groupedAssortments)
                {
                    AssortmentInfoDto latestAssortment = assortmentinfoGroup.FirstOrDefault();

                    // only add if doesn't exist in the dictionary (this should be fulfilled by previous logic anyway)
                    if (!assortmentLookupList.ContainsKey(latestAssortment.Name))
                    {
                        assortmentLookupList.Add(latestAssortment.Name, latestAssortment.Id);
                    }
                }


                Dictionary<AssortmentDto, List<AssortmentProductDto>> assortmentData = new Dictionary<AssortmentDto, List<AssortmentProductDto>>();
                List<AssortmentProductDto> assortmentProductList = new List<AssortmentProductDto>();

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    AssortmentDto assortmentDto = new AssortmentDto();
                    AssortmentProductDto assortmentProductDto = new AssortmentProductDto();

                    //loop through other resolved mappings
                    foreach (ImportMapping mapping in resolvedMappings)
                    {
                        Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                        Object cellValue = row[columnNumber].CurrentValue;

                        switch (mapping.PropertyIdentifier)
                        {
                            case AssortmentImportMappingList.AssortmentNameMapId:
                                assortmentDto.Name = cellValue.ToString();
                                break;

                            case AssortmentImportMappingList.ProductGroupCodeMapId:
                                if (cellValue.ToString() != String.Empty && cellValue != DBNull.Value)
                                {
                                    assortmentDto.ProductGroupId = productGroupLookup[cellValue.ToString()];
                                }
                                break;

                            case AssortmentImportMappingList.ProductGTINMapId:
                                if (cellValue.ToString() != String.Empty && cellValue != DBNull.Value)
                                {
                                    assortmentProductDto.ProductId = productLookup[cellValue.ToString()].Id;
                                }
                                break;

                            default:
                                cellValue = ProcessHelpers.CheckNullCellValues(mapping, cellValue);
                                //use the mapping list method to set the value
                                AssortmentImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, assortmentDto, assortmentProductDto);
                                break;
                        }
                    }

                    assortmentData.TryGetValue(assortmentDto, out assortmentProductList);

                    if (assortmentProductList != null)
                    {
                        // add to current assortment product detail
                        assortmentProductList.Add(assortmentProductDto);
                    }
                    else
                    {
                        // create new assortment product list
                        assortmentProductList = new List<AssortmentProductDto>();
                        assortmentProductList.Add(assortmentProductDto);
                    }

                    assortmentData[assortmentDto] = assortmentProductList;
                }

                // import assortment details
                foreach (AssortmentDto assortmentDto in assortmentData.Keys)
                {
                    Assortment assortment = Assortment.NewAssortment(this.EntityId);

                    if (assortmentLookupList.ContainsKey(assortmentDto.Name))
                    {
                        assortment = Assortment.GetById(assortmentLookupList[assortmentDto.Name]);
                        //Mark as new version
                        //assortment.MarkGraphAsNew();
                    }

                    UpdateAssortmentDetail(assortmentDto, assortment);

                    // Get assortment products
                    List<AssortmentProductDto> assortmentProducts = assortmentData[assortmentDto];
                    assortment.Products.Clear();

                    foreach (AssortmentProductDto assortmentProductDto in assortmentProducts)
                    {
                        AssortmentProduct assortmentProduct = AssortmentProduct.NewAssortmentProduct(assortmentProductDto.ProductId);

                        UpdateAssortmentProductDetail(assortmentProductDto, assortmentProduct);

                        assortment.Products.Add(assortmentProduct);
                    }

                    // GEM-24718 because locations have been moved out into a separate report 
                    //  make sure that no locations are inherited from the previous (latest undeleted) assortment
                    // Location Space import operates on DTOs rather than Models and hence never inherits Bay, Element and Constraint
                    assortment.Locations.Clear();

                    assortment.Save();
                }

                assortmentData.Clear();
                assortmentInfoDtoList = null;
            }
        }

        /// <summary>
        /// Update the model object with dto data
        /// </summary>
        /// <param name="assortmentProductDto"></param>
        /// <param name="assortmentProduct"></param>
        private void UpdateAssortmentProductDetail(AssortmentProductDto assortmentProductDto, AssortmentProduct assortmentProduct)
        {
            assortmentProduct.IsRanged = assortmentProductDto.IsRanged;
            assortmentProduct.Rank = assortmentProductDto.Rank;
            assortmentProduct.Segmentation = assortmentProductDto.Segmentation;
            assortmentProduct.Facings = assortmentProductDto.Facings;
            assortmentProduct.Units = assortmentProductDto.Units;
            assortmentProduct.ProductTreatmentType = (PlanogramAssortmentProductTreatmentType)assortmentProductDto.ProductTreatmentType;
            assortmentProduct.Comments = assortmentProductDto.Comments;
        }

        /// <summary>
        /// Update the model object with dto data
        /// </summary>
        /// <param name="assortmentDto"></param>
        /// <param name="assortment"></param>
        private void UpdateAssortmentDetail(AssortmentDto assortmentDto, Assortment assortment)
        {
            assortment.Name = assortmentDto.Name;
            assortment.ProductGroupId = assortmentDto.ProductGroupId;
        }

        #endregion
    }
}
