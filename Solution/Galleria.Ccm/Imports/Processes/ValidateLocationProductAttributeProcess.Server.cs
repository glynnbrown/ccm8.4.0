﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-26041 : A.Kuszyk
//  Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Dal;
using System.Globalization;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management LocationProductAttribute validation process class
    /// </summary>
    public partial class ValidateLocationProductAttributeProcess
    {
        #region Fields

        private HashSet<String> _validationParentLocationObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentProductObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<LocationProductAttributeIdSearchCriteria> _validationImportActionObjects = new HashSet<LocationProductAttributeIdSearchCriteria>();
        private HashSet<Tuple<String, String>> _validationIdentifierFields = new HashSet<Tuple<String, String>>(new ValidationComparer());
        private HashSet<Tuple<String, String>> _validationDuplicates = new HashSet<Tuple<String, String>>(new ValidationComparer());

        #endregion

        #region Properties

        /// <summary>
        /// Get/Set Validation Parent Location Objects
        /// </summary>
        public HashSet<String> ValidationParentLocationObjects
        {
            get { return _validationParentLocationObjects; }
            set { _validationParentLocationObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Parent Product Objects
        /// </summary>
        public HashSet<String> ValidationParentProductObjects
        {
            get { return _validationParentProductObjects; }
            set { _validationParentProductObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Import Action Objects
        /// </summary>
        public HashSet<LocationProductAttributeIdSearchCriteria> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Identifier Fields
        /// </summary>
        public HashSet<Tuple<String, String>> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }
        /// <summary>
        /// Get/Set Validation Duplicates
        /// </summary>
        public HashSet<Tuple<String, String>> ValidationDuplicates
        {
            get { return _validationDuplicates; }
            set { _validationDuplicates = value; }
        }

        /// <summary>
        /// Local property for the location id mapping
        /// </summary>
        private ImportMapping locationCodeMappingForImportType;
        /// <summary>
        /// Local property for the product id mapping
        /// </summary>
        private ImportMapping productGTINMappingForImportType;

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation Started.");

            //Set location id mapping
            locationCodeMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationProductAttributeImportMappingList.LocationCodeMapId).FirstOrDefault();
            //Set product id mapping
            productGTINMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationProductAttributeImportMappingList.ProductGTINMapId).FirstOrDefault();

            //setup datatable
            Trace.TraceInformation("{0} Location Product Attribute Records being validated", this.ValidRowsCount);

            #region Fetch Relational Check Objects

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //start a transaction for this data validation
                dalContext.Begin();

                //get location id mapping
                Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationCodeMappingForImportType);
                //get product id mapping
                Int32 productGTINMappingColumnNumber = this.FileData.GetMappedColNumber(productGTINMappingForImportType);

                //create list to store code links
                HashSet<Tuple<String, String>> locationProductAttributeLinks = new HashSet<Tuple<String, String>>(new ValidationComparer());
                HashSet<String> productCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                HashSet<String> locationCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

                Dictionary<String, Int32> productIdLookupDictionary = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);
                Dictionary<String, Int16> locationIdLookupDictionary = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                Dictionary<Int32, String> productGTINLookupDictionary = new Dictionary<Int32, String>();
                Dictionary<Int16, String> locationCodeLookupDictionary = new Dictionary<Int16, String>();

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    String locationCode = Convert.ToString(row[locationCodeMappingColumnNumber], CultureInfo.InvariantCulture);
                    String productCode = Convert.ToString(row[productGTINMappingColumnNumber], CultureInfo.InvariantCulture);

                    if (locationCode != null && productCode != null)
                    {
                        //Convert max property to int32
                        Int32 locationCodeMaxValue = Convert.ToInt32(locationCodeMappingForImportType.PropertyMax);

                        //[GFS-13847] correct lengths of codes
                        if (locationCode.Length > locationCodeMaxValue)
                        {
                            locationCode = locationCode.Substring(0, locationCodeMaxValue);
                        }

                        //Convert max property to int32
                        Int32 productGtinMaxValue = Convert.ToInt32(productGTINMappingForImportType.PropertyMax);

                        if (productCode.Length > productGtinMaxValue)
                        {
                            productCode = productCode.Substring(0, productGtinMaxValue);
                        }

                        //Build up collection of codes
                        Tuple<String, String> locationProductLink = new Tuple<String, String>(locationCode, productCode);
                        locationProductAttributeLinks.Add(locationProductLink);
                        locationCodes.Add(locationProductLink.Item1);
                        productCodes.Add(locationProductLink.Item2);

                        //While looping also find duplicates
                        if (this.ValidationIdentifierFields.Contains(locationProductLink))
                        {
                            this.ValidationDuplicates.Add(locationProductLink);
                        }
                        else
                        {
                            this.ValidationIdentifierFields.Add(locationProductLink);
                        }
                    }
                }

                using (ILocationInfoDal locationInfoDal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> allLocationInfoDtos = locationInfoDal.FetchByEntityIdLocationCodes(this.EntityId, locationCodes);
                    //GFS-22188 - exclude deleted locations from this list
                    List<LocationInfoDto> locationInfoDtos = allLocationInfoDtos.Where(p => p.DateDeleted == null).ToList();

                    //Add codes to hashset for faster searching later on
                    foreach (LocationInfoDto info in locationInfoDtos)
                    {
                        ValidationParentLocationObjects.Add(info.Code);

                        //[GFS-17158] Add to dictionaries
                        if (!locationIdLookupDictionary.ContainsKey(info.Code))
                        {
                            locationIdLookupDictionary.Add(info.Code, info.Id);
                            locationCodeLookupDictionary.Add(info.Id, info.Code);
                        }
                    }
                    locationInfoDtos = null;
                }


                using (IProductInfoDal productInfoDal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> allProductInfoDtos = productInfoDal.FetchByEntityIdProductGtins(this.EntityId, productCodes);
                    //GFS-22188 - exclude deleted products from this list
                    List<ProductInfoDto> productInfoDtos = allProductInfoDtos.ToList();

                    //Add codes to hashset for faster searching later on
                    foreach (ProductInfoDto info in productInfoDtos)
                    {
                        ValidationParentProductObjects.Add(info.Gtin);

                        //[GFS-17158] Add to dictionaries
                        if (!productIdLookupDictionary.ContainsKey(info.Gtin))
                        {
                            productIdLookupDictionary.Add(info.Gtin, info.Id);
                            productGTINLookupDictionary.Add(info.Id, info.Gtin);
                        }
                    }
                    productInfoDtos = null;
                }

                HashSet<Tuple<Int16, Int32>> combinationsList = new HashSet<Tuple<Int16, Int32>>();
                foreach (Tuple<String, String> locationProductLink in locationProductAttributeLinks)
                {
                    //[GFS-17158] Lookup using dictionary
                    Int16 locationId;
                    if (locationIdLookupDictionary.TryGetValue(locationProductLink.Item1, out locationId))
                    {
                        //[GFS-17158] Lookup using dictionary
                        Int32 productId;
                        if (productIdLookupDictionary.TryGetValue(locationProductLink.Item2, out productId))
                        {
                            combinationsList.Add(new Tuple<Int16, Int32>(locationId, productId));
                        }
                    }
                }

                //now retrieve all LocationProductAttribute links that match the location/product codes
                //to inform the user of the import action that will take place
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    IEnumerable<LocationProductAttributeDto> locationProductAttributeDtos =
                        dal.FetchByLocationIdProductIdCombinations(combinationsList);

                    //Add codes to hashset for faster searching later on
                    foreach (LocationProductAttributeDto dto in locationProductAttributeDtos)
                    {
                        //Lookup values in dictionaries
                        String locationCode;
                        String productGTIN;
                        if (locationCodeLookupDictionary.TryGetValue(dto.LocationId, out locationCode)
                                && productGTINLookupDictionary.TryGetValue(dto.ProductId, out productGTIN))
                        {
                            //Create search critiera
                            LocationProductAttributeIdSearchCriteria criteria = new LocationProductAttributeIdSearchCriteria()
                            {
                                LocationCode = locationCode,
                                ProductGTIN = productGTIN
                            };

                            this.ValidationImportActionObjects.Add(criteria);
                        }
                    }
                    locationProductAttributeDtos = null;
                }

                locationCodes.Clear();
                locationCodes = null;
                productCodes.Clear();
                productCodes = null;
                locationCodeLookupDictionary.Clear();
                locationCodeLookupDictionary = null;
                productGTINLookupDictionary.Clear();
                productGTINLookupDictionary = null;
                locationIdLookupDictionary.Clear();
                locationIdLookupDictionary = null;
                productIdLookupDictionary.Clear();
                productIdLookupDictionary = null;
            }

            #endregion

            //validate data against mappings using generic methods
            this.RunDataTypeValidation(FileData, this.ValidRows);

            //update datatable to include import action and excel row number columns
            this.AddExtraColumns(this.FileData);

            //set validated data to be updated data item and set it to be returned
            this.ValidatedData = this.FileData;

            Trace.TraceInformation("Validation of {0} Location Product Attribute records complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Override parent record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationProductAttributeImportMappingList.LocationCodeMapId)
            {
                //Check whether the location code matches a location in the database
                if (!this.ValidationParentLocationObjects.Contains(columnValue.ToString()))
                {
                    //no matching location code, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                        Message.DataManagement_Validation_NoMatchingLocation, ValidationErrorType.Error,
                        true, false, false, false, col, false);
                }
            }
            else if (col.PropertyIdentifier == LocationProductAttributeImportMappingList.ProductGTINMapId)
            {
                //Check whether the product code matches a product in the database
                if (!this.ValidationParentProductObjects.Contains(columnValue.ToString()))
                {
                    //no matching product GTIN, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                        Message.DataManagement_Validation_NoMatchingProduct, ValidationErrorType.Error,
                        true, false, false, false, col, false);
                }
            }
            else if (col.PropertyIdentifier == LocationProductAttributeImportMappingList.StatusTypeMapId)
            {

            }
            return null;
        }

        /// <summary>
        /// Overrride dupicate record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationProductAttributeImportMappingList.LocationCodeMapId)
            {
                String locationCode = columnValue.ToString();

                Int32 firstRowNumber = FileData.Rows[0].RowNumber;
                Int32 rowIndex = rowNumber - firstRowNumber;

                Int32 productGTINMappingColumnNumber = this.FileData.GetMappedColNumber(productGTINMappingForImportType);
                String productGTIN = this.FileData.Rows[rowIndex][productGTINMappingColumnNumber].Value.ToString();

                Tuple<String, String> searchItem = new Tuple<String, String>(locationCode, productGTIN);

                //Check whether the link is a duplicate of another being imported
                if (this.ValidationDuplicates.Contains(searchItem))
                {
                    //Duplicate found, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                        locationCode, Message.DataManagement_Validation_DuplicateLocationProductIllegal,
                        ValidationErrorType.Error, true, false, false, false, col, true);
                }
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        /// <param name="rowData"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            Int32 locationCodeColNumber = this.FileData.GetMappedColNumber(locationCodeMappingForImportType);
            Int32 productGTINColNumber = this.FileData.GetMappedColNumber(productGTINMappingForImportType);

            if (locationCodeColNumber != 0 && productGTINColNumber != 0)
            {
                //Extract link from rowdata
                LocationProductAttributeIdSearchCriteria criteria =
                    new LocationProductAttributeIdSearchCriteria()
                    {
                        LocationCode = rowData[locationCodeColNumber].Value.ToString(),
                        ProductGTIN = rowData[productGTINColNumber].Value.ToString()
                    };

                //Check whether the product code matches a product in the database
                if (!this.ValidationImportActionObjects.Contains(criteria))
                {
                    //if ths product code is new, but a duplicate record is also being imported, the first record
                    //will be insert, all others will then be updates
                    this.ValidationImportActionObjects.Add(criteria);
                    return Message.DataManagement_ImportType_Add;
                }
                else
                {
                    //Otherwise it is an update
                    return Message.DataManagement_ImportType_Update;
                }
            }
            else
            {
                //[TODO] Work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }

        protected override ValidationErrorItem RunEnumLookupValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (columnValue != null)
            {
                switch (col.PropertyIdentifier)
                {
                    case LocationProductAttributeImportMappingList.StatusTypeMapId:
                        //Check if an enum exists for the friendly name entered
                        //Using lower invariant to allow all case variants to import correctly
                        PlanogramProductStatusType statusType;
                        if (!PlanogramProductStatusTypeHelper.EnumFromFriendlyName.TryGetValue(columnValue.ToString().ToLowerInvariant(), out statusType))
                        {
                            if (col.PropertyIsUniqueIdentifier)
                            {
                                //Friendly name incorrect return error
                                return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                                    columnValue.ToString(), Message.DataManagement_Validation_InvalidProductStatusType,
                                    ValidationErrorType.Error, true, false, false, false, col, false);
                            }
                            else
                            {
                                //Friendly name incorrect return error
                                return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                                    columnValue.ToString(), Message.DataManagement_Validation_InvalidProductStatusType,
                                    ValidationErrorType.Error, true, true, false, false, col, false);
                            }
                        }
                        return null;
                    default:
                        return null;
                }
            }
            return null;
        }

        #endregion

        #region Private Helper Classes

        /// <summary>
        /// IEqualityComparer to ensure String in Tuple<Int32, String>
        /// is compared using StringComparer.OrdinalIgnoreCase
        /// </summary>
        private sealed class ValidationComparer :
            IEqualityComparer<Tuple<String, String>>
        {
            public Boolean Equals(Tuple<String, String> x, Tuple<String, String> y)
            {
                StringComparer s = StringComparer.OrdinalIgnoreCase;

                if (s.Compare(x.Item1, y.Item1) != 0) { return false; }
                if (s.Compare(x.Item2, y.Item2) != 0) { return false; }

                return true;
            }

            public Int32 GetHashCode(Tuple<String, String> obj)
            {
                return obj.Item1.ToLowerInvariant().GetHashCode() +
                    obj.Item2.ToLowerInvariant().GetHashCode();
            }
        }

        #endregion
    }
}
