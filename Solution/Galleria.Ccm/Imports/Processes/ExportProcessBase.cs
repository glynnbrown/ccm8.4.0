﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Base class for export processes
    /// </summary>
    /// <typeparam name="T">The export process type</typeparam>
    [Serializable]
    public abstract class ExportProcessBase<T> : Galleria.Framework.Processes.ExportProcessBase<T>
        where T : ExportProcessBase<T>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="fileName">The export filename</param>
        /// <param name="exportHeadersOnly">Indicates if headers should be exported</param>
        public ExportProcessBase(Int32 entityId, String fileName, Boolean exportHeadersOnly)
            : base(fileName, exportHeadersOnly)
        {
            this.EntityId = entityId;
        }
        #endregion

        #region Properties

        #region EntityId
        /// <summary>
        /// The entity id property definition
        /// </summary>
        public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id for this process
        /// </summary>
        public Int32 EntityId
        {
            get { return this.ReadProperty<Int32>(EntityIdProperty); }
            private set { this.LoadProperty<Int32>(EntityIdProperty, value); }
        }
        #endregion

        #endregion
    }
}
