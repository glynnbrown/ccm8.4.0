﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules.CommonRules;
using Galleria.Framework.Imports;
using Csla.Rules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management location product attribute validation process class
    /// </summary>
    [Serializable]
    public partial class ValidateLocationProductAttributeProcess : ValidateProcessBase<ValidateLocationProductAttributeProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ValidateLocationProductAttributeProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstRow)
            : base(entityId, fileData, startingRowNumber, headersInFirstRow)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidateLocationProductAttributeProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationProductAttributeProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationProductAttributeProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationProductAttributeData.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationProductAttributeProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
