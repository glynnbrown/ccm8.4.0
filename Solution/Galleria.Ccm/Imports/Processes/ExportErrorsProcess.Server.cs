﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

using Csla;

using SmartAssembly.Attributes;

using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Helpers;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Resources.Language;

using Aspose.Cells;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportErrorsProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            //The row offset. 
            //If the first row of the data file contained headers, the first row starts at index 2, otherwise it starts at 1
            Int32 dataFirstRowNumber = 0;

            //Create workbook
            Workbook workbook = new Workbook();
            Worksheet worksheet = workbook.Worksheets[0];

            #region Add data to worksheet

            int columnIndex = 0; //set column index to start at 0 for header row
            foreach (ImportFileDataColumn column in this.ExportData.Columns)
            {
                //If not last column number(import action column - ignore)
                if (column.ColumnNumber != this.ExportData.Columns.OrderBy(p => p.ColumnNumber).Last().ColumnNumber)
                {
                    //Find matching mapping object
                    ImportMapping mappingItem = this.ExportData.MappingList.Where(c => c.ColumnReference == column.Header).FirstOrDefault();

                    //If mapping item can be found - enter mapping name
                    if (mappingItem != null)
                    {
                        //Enter propery name
                        worksheet.Cells[0, columnIndex].Value = mappingItem.PropertyName;
                    }
                    else
                    {
                        //Otherwise enter column name as the user passed this in originally anyway
                        worksheet.Cells[0, columnIndex].Value = column.Header;
                    }

                    int rowIndex = 1; //start to enter data from row 1

                    foreach (ImportFileDataRow rowItem in this.ExportData.Rows)
                    {
                        Object rowItemObject = null;
                        ImportFileDataCell cell = rowItem.Cells.FirstOrDefault(p => p.ColumnNumber == column.ColumnNumber);
                        if (cell != null)
                        {
                            rowItemObject = cell.Value;
                        }
                        else
                        {
                            rowItemObject = "";
                        }

                        //If matching column has been found
                        if (mappingItem != null)
                        {
                            if (rowItemObject.ToString() != "NULL" && rowItemObject != DBNull.Value && rowItemObject != null)
                            {
                                if (mappingItem.PropertyType == typeof(int) || mappingItem.PropertyType == typeof(Nullable<int>)
                                        || mappingItem.PropertyType == typeof(byte) || mappingItem.PropertyType == typeof(Nullable<byte>)
                                        || mappingItem.PropertyType == typeof(short) || mappingItem.PropertyType == typeof(Nullable<short>))
                                {
                                    Nullable<Int32> int32Value = rowItemObject as Nullable<Int32>;
                                    if (int32Value != null)
                                    {
                                        WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToInt32(int32Value, CultureInfo.CurrentCulture));
                                    }
                                    else
                                    {
                                        WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToString(rowItemObject, CultureInfo.CurrentCulture));
                                    }
                                }
                                if (mappingItem.PropertyType == typeof(Double) || mappingItem.PropertyType == typeof(Nullable<Double>)
                                        || mappingItem.PropertyType == typeof(float) || mappingItem.PropertyType == typeof(Nullable<float>)
                                        || mappingItem.PropertyType == typeof(Single) || mappingItem.PropertyType == typeof(Nullable<Single>))
                                {
                                    Nullable<float> floatValue = rowItemObject as Nullable<float>;
                                    if (floatValue != null)
                                    {
                                        WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToSingle(floatValue, CultureInfo.CurrentCulture));
                                    }
                                    else
                                    {
                                        WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToString(rowItemObject, CultureInfo.CurrentCulture));
                                    }
                                }
                                else if (mappingItem.PropertyType == typeof(DateTime) || mappingItem.PropertyType == typeof(Nullable<DateTime>))
                                {
                                    Nullable<DateTime> datetimeValue = rowItemObject as Nullable<DateTime>;
                                    if (datetimeValue != null)
                                    {
                                        WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToDateTime(datetimeValue, CultureInfo.CurrentCulture));
                                    }
                                    else
                                    {
                                        WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToString(rowItemObject, CultureInfo.CurrentCulture));
                                    }
                                }
                                else if (mappingItem.PropertyType == typeof(Boolean) || mappingItem.PropertyType == typeof(Nullable<Boolean>))
                                {
                                    Nullable<Boolean> booleanValue = rowItemObject as Nullable<Boolean>;
                                    if (booleanValue != null)
                                    {
                                        WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToBoolean(booleanValue, CultureInfo.CurrentCulture));
                                    }
                                    else
                                    {
                                        WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToString(rowItemObject, CultureInfo.CurrentCulture));
                                    }

                                }
                                else
                                {
                                    WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToString(rowItemObject, CultureInfo.CurrentCulture));
                                }
                            }
                        }
                        else
                        {
                            //Enter into cell as string as we dont know type for definate
                            WpfHelper.SetColumnValue(worksheet.Cells[rowIndex, columnIndex], Convert.ToString(rowItemObject, CultureInfo.CurrentCulture));
                        }
                        rowIndex += 1; //increase row index
                    }
                    columnIndex += 1; //increase column index
                }
            }

            #endregion

            #region Apply error & comments style
            //Setup error style (red solid fill)
            Aspose.Cells.Style errorStyle = new Aspose.Cells.Style();
            errorStyle.ForegroundColor = System.Drawing.Color.Red;
            errorStyle.Pattern = BackgroundType.Solid;

            //Get collection of all invalid column numbers from all error groups
            List<int> invalidColumnNumbers = new List<int>();
            foreach (ValidationErrorGroup errorGroup in this.ErrorGroups)
            {
                foreach (int invalidColNum in errorGroup.InvalidColumnNumbers)
                {
                    invalidColumnNumbers.Add(invalidColNum);
                }
            }

            //Get the offset of the datarows to ensure we highlight the correct row:
            // if data file has a header row, first data row has a rownumber of 2
            // if data file has no header row, first data row has a rownumber of 1
            if (this.ExportData.Rows.Count > 0)
            {
                dataFirstRowNumber = this.ExportData.Rows[0].RowNumber;
            }

            //Format the cells to show error
            foreach (ValidationErrorGroup groupedRow in this.ErrorGroups)
            {
                foreach (ValidationErrorItem error in groupedRow.Errors)
                {
                    //[ISO-13577] Stop individual errors being entered if column is invalid
                    if (!invalidColumnNumbers.Contains(error.ColumnNumber))
                    {
                        //Setup row 
                        int rowNumber = error.RowNumber - (dataFirstRowNumber - 1);
                        //Adjust column number for zero based index in aspose
                        int columnNumber = (error.ColumnNumber - 1);

                        //Get appropriate cell that the error relates to
                        Cell cellToUpdate = worksheet.Cells[rowNumber, columnNumber];
                        //Set the cell style
                        cellToUpdate.SetStyle(errorStyle);
                        //Create a new comment for this cell
                        worksheet.Comments.Add(rowNumber, columnNumber);
                        //Setup a new comment and apply with error problem description
                        Comment comment = worksheet.Comments[rowNumber, columnNumber];
                        comment.Note = error.ProblemDescription;
                    }
                }
            }

            //Add error formatting for invalid columns
            if (invalidColumnNumbers.Count > 0)
            {
                //Create style
                int styleId = workbook.Styles.Add();
                Style style = workbook.Styles[styleId];
                style.Pattern = BackgroundType.Solid;
                style.ForegroundColor = System.Drawing.Color.Red;

                //Create StyleFlag
                StyleFlag styleFlag = new StyleFlag();
                styleFlag.CellShading = true;

                foreach (int columnNum in invalidColumnNumbers.Distinct())
                {
                    //Get index value
                    int columnSheetIndex = columnNum - 1;
                    //Get column
                    Column column = worksheet.Cells.Columns[columnSheetIndex];
                    //Apply style to column
                    column.ApplyStyle(style, styleFlag);

                    //Add invalid column comment in the header cell
                    Cell cellToUpdate = worksheet.Cells[0, columnSheetIndex];
                    //Create a new comment for this cell
                    int commentIndex = worksheet.Comments.Add(0, columnSheetIndex);
                    //Setup a new comment and apply with error problem description
                    Comment comment = worksheet.Comments[commentIndex];
                    comment.AutoSize = true;
                    comment.Note = Message.DataManagement_ExportToExcel_InvalidColumnComment;
                }
            }
            #endregion

            // save the work book
            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} records exported to excel successfuly.", worksheet.Cells.Rows.Count);
        }

        #endregion
    }
}
