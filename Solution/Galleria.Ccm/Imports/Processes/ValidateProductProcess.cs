﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25395 : A.Probyn 
//      Created.
// CCM-25452 : N.Haywood
//      Added validation for products
// CCM-26560 : A.Probyn
//      Fixed typo in AddObjectAuthorizationRules
#endregion
#region Version History: (CCM 801)
// V8-26114  : J.Pickup
//      Removed Unused Usings.
#endregion

#endregion

using System;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Our product library validation process class
    /// </summary>
    [Serializable]
    public partial class ValidateProductLibraryProcess : ValidateProcessBase<ValidateProductLibraryProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ValidateProductLibraryProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstrow)
            : base(entityId, fileData, startingRowNumber, headersInFirstrow)
        {
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion
    }

    [Serializable]
    public sealed partial class ValidateProductProcess : ValidateProcessBase<ValidateProductProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ValidateProductProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstrow)
            : base(entityId, fileData, startingRowNumber, headersInFirstrow)
        {
        }
        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion
    }
}

