﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules.CommonRules;
using Csla.Rules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    [Serializable]
    public partial class ExportLocationProductLegalProcess : ExportProcessBase<ExportLocationProductLegalProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="fileName">The full name and path of the export file</param>
        /// <param name="exportHeadersOnly">Indicates if only the export headers should be exported</param>
        public ExportLocationProductLegalProcess(Int32 entityId, String fileName, Boolean exportHeadersOnly)
            : base(entityId, fileName, exportHeadersOnly)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ExportLocationProductLegalProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ExportLocationProductLegalProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ExportLocationProductLegalProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ExportLocationProductLegalData.ToString()));
            BusinessRules.AddRule(typeof(ExportLocationProductLegalProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
