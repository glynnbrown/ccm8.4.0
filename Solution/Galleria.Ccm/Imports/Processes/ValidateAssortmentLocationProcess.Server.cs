﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// GFS-25455 :J.Pickup
//  Copied over from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

using Csla;

using SmartAssembly.Attributes;

using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Logging;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management Assortment Location validation process class
    /// </summary>
    public partial class ValidateAssortmentLocationProcess
    {
        #region Fields

        private HashSet<String> _validationParentAssortmentObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentLocationObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<AssortmentLocationSearchCriteria> _validationImportActionObjects = new HashSet<AssortmentLocationSearchCriteria>();
        private HashSet<AssortmentLocationSearchCriteria> _validationIdentifierFields = new HashSet<AssortmentLocationSearchCriteria>();
        private HashSet<AssortmentLocationSearchCriteria> _validationDuplicates = new HashSet<AssortmentLocationSearchCriteria>();

        private String _validationDuplicateRecordIdentifierName = String.Empty;

        #endregion

        #region Properties

        /// <summary>
        /// Get/Set Validation Assortment Parent Objects
        /// </summary>
        public HashSet<String> ValidationParentAssortmentObjects
        {
            get { return _validationParentAssortmentObjects; }
            set { _validationParentAssortmentObjects = value; }
        }

        /// <summary>
        /// Get/Set Validation Location Parent Objects
        /// </summary>
        public HashSet<String> ValidationParentLocationObjects
        {
            get { return _validationParentLocationObjects; }
            set { _validationParentLocationObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Import Action Objects
        /// </summary>
        public HashSet<AssortmentLocationSearchCriteria> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Identifier Fields
        /// </summary>
        public HashSet<AssortmentLocationSearchCriteria> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }
        /// <summary>
        /// Get/Set Validation Duplicates
        /// </summary>
        public HashSet<AssortmentLocationSearchCriteria> ValidationDuplicates
        {
            get { return _validationDuplicates; }
            set { _validationDuplicates = value; }
        }

        /// Local property for the mappings
        /// </summary>
        private ImportMapping assortmentNameMappingForImportType;
        private ImportMapping locationMappingForImportType;

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation Started.");

            // set assortment name mapping
            assortmentNameMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == AssortmentLocationImportMappingList.AssortmentNameMapId).FirstOrDefault();
            //Set location mapping
            locationMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == AssortmentLocationImportMappingList.LocationCodeMapId).FirstOrDefault();

            //set duplicate record identifier columns name
            _validationDuplicateRecordIdentifierName = String.Format("{0} - {1}",
                assortmentNameMappingForImportType.ColumnReference, locationMappingForImportType.ColumnReference);

            //setup datatable
            Trace.TraceInformation("{0} Assortment Location Records being validated", this.ValidRowsCount);

            #region Fetch Relational Check Objects

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // start a transaction for this data validation
                dalContext.Begin();
                //Get mappings
                Int32 assortmentNameMappingColumnNumber = this.FileData.GetMappedColNumber(assortmentNameMappingForImportType);
                Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationMappingForImportType);
                //Get current entity id
                Int32 entityId = this.EntityId;

                //Create lists to store codes
                HashSet<String> locationCodes = new HashSet<String>();

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    //Build up collections of details
                    String assortmentName = Convert.ToString(row[assortmentNameMappingColumnNumber], CultureInfo.InvariantCulture);
                    String locationCode = Convert.ToString(row[locationCodeMappingColumnNumber], CultureInfo.InvariantCulture);

                    if ((assortmentName != null && locationCode != null))
                    {
                        //Constructor criteria
                        AssortmentLocationSearchCriteria criteria = new AssortmentLocationSearchCriteria()
                        {
                            AssortmentName = assortmentName,
                            LocationCode = locationCode
                        };

                        //[ISO-13387] While we're looping here anyway, we might as well go ahead and find duplicates.
                        if (this.ValidationIdentifierFields.Contains(criteria))
                        {
                            this.ValidationDuplicates.Add(criteria);
                        }
                        else
                        {
                            this.ValidationIdentifierFields.Add(criteria);
                        }
                    }

                    if (!locationCodes.Contains(locationCode)) { locationCodes.Add(locationCode); }
                }

                //Add assortment parents to hashset for faster searching later
                using (IAssortmentInfoDal dal = dalContext.GetDal<IAssortmentInfoDal>())
                {
                    List<AssortmentInfoDto> assortmentInfoDtos = dal.FetchByEntityId(entityId).ToList();

                    //Add assortments to hashset for faster searching later on
                    foreach (AssortmentInfoDto assortmentInfo in assortmentInfoDtos)
                    {
                        this.ValidationParentAssortmentObjects.Add(assortmentInfo.Name);
                    }
                }

                //Add possible parent codes to hashset for faster searching later
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    List<LocationInfoDto> allLocationInfoDtos = dal.FetchByEntityIdLocationCodes(
                                           entityId, locationCodes.Distinct()).ToList();

                    //GFS-22188 - exclude deleted locations from this list
                    List<LocationInfoDto> locationInfoDtos = allLocationInfoDtos.Where(p => p.DateDeleted == null).ToList();

                    //Add codes to hashset for faster searching later on
                    locationInfoDtos.ForEach(p => this.ValidationParentLocationObjects.Add(p.Code));
                }

                // now retrieve all latest assortment locations for this entity id
                // to inform the user of the import action that will take place
                using (IAssortmentSearchCriteriaDal dal = dalContext.GetDal<IAssortmentSearchCriteriaDal>())
                {
                    List<AssortmentLocationSearchCriteria> assortmentLocations = dal.FetchLatestAssortmentLocationVersionsByEntityId(entityId).ToList();

                    //Add codes to hashset for faster searching later on
                    assortmentLocations.ForEach(c => this.ValidationImportActionObjects.Add(c));
                }
            }
            #endregion

            //validate data against mappings using generic methods
            this.RunDataTypeValidation(FileData, this.ValidRows);

            //update datatable to include import action and excel row number columns
            this.AddExtraColumns(FileData);

            //set validated data to be updated data item and set it to be returned
            this.ValidatedData = FileData;

            Trace.TraceInformation("Validation of {0} Assortment Location records complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Override parent record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            switch (col.PropertyIdentifier)
            {
                case AssortmentLocationImportMappingList.AssortmentNameMapId:
                    //Check whether the assortment name matches an assortment in the database
                    if (!this.ValidationParentAssortmentObjects.Contains(columnValue.ToString()))
                    {
                        //no matching assortment, create error item
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_NoMatchingAssortment, ValidationErrorType.Error,
                            true, false, false, false, col, false);
                    }
                    break;

                case AssortmentLocationImportMappingList.LocationCodeMapId:
                    //Check whether the location code matches a location in the database
                    if (!this.ValidationParentLocationObjects.Contains(columnValue.ToString()))
                    {
                        //no matching location code, create error item
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_NoMatchingLocation, ValidationErrorType.Error,
                            true, false, false, false, col, false);
                    }
                    break;
            }

            return null;
        }

        /// <summary>
        /// Overrride dupicate record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == AssortmentLocationImportMappingList.AssortmentNameMapId)
            {
                String assortmentName = columnValue.ToString();

                //Calculate row index
                Int32 firstRowNumber = FileData.GetValidRows().ToList()[0].RowNumber;
                Int32 rowIndex = rowNumber - firstRowNumber;

                //Construct unique data group
                Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationMappingForImportType);

                String locationCode = String.Empty;
                object tempCodeValue = this.FileData.GetValidRows().ToList()[rowIndex][locationCodeMappingColumnNumber].Value;
                if (tempCodeValue != null)
                {
                    locationCode = tempCodeValue.ToString();
                }

                //Construct criteria
                AssortmentLocationSearchCriteria searchItem = new AssortmentLocationSearchCriteria()
                {
                    AssortmentName = assortmentName,
                    LocationCode = locationCode
                };

                //Check whether the assortment location record is a duplicate of another being imported
                if (this.ValidationDuplicates.Contains(searchItem))
                {
                    //Duplicate found, create error item
                    ValidationErrorItem validationErrorItem = ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                        assortmentName, Message.DataManagement_Validation_DuplicateAssortmentLocation,
                        ValidationErrorType.Error, true, false, false, false, col, true);

                    //update column name to contain full search criteria
                    validationErrorItem.ColumnName = _validationDuplicateRecordIdentifierName;

                    //return updated error item
                    return validationErrorItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            Int32 assortmentNameColNumber = this.FileData.GetMappedColNumber(assortmentNameMappingForImportType);
            Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationMappingForImportType);

            if ((assortmentNameColNumber != 0 && locationCodeMappingColumnNumber != 0))
            {
                //Extract link from rowdata
                AssortmentLocationSearchCriteria criteria =
                    new AssortmentLocationSearchCriteria()
                    {
                        AssortmentName = rowData[assortmentNameColNumber].Value.ToString(),
                        LocationCode = rowData[locationCodeMappingColumnNumber].Value.ToString()
                    };

                //Check whether the assortment link matches and existing link
                if (!this.ValidationImportActionObjects.Contains(criteria))
                {
                    //if the link is new, but a duplicate is also being inported the first record will
                    //be inserted, all others will be updates
                    this.ValidationImportActionObjects.Add(criteria);
                    return Message.DataManagement_ImportType_Add;
                }
                else
                {
                    //otherwise it is and update
                    return Message.DataManagement_ImportType_Update;
                }
            }
            else
            {
                //[TODO] work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }

        #endregion
    }
}