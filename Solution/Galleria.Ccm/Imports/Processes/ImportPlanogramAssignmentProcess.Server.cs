﻿using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportPlanogramAssignmentProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} Planogram Assignments started.", this.ValidRowsCount);

            ImportPlanogramAssignments();

            Trace.TraceInformation("Import of {0} Planogram Assignments complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Main Import
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportPlanogramAssignments()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                #region Create Dictionary lookups

                //Create dictionary to lookup location id
                Dictionary<String, Int16> locationIdLookupDictionary = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> locationInfoDtos = dal.FetchByEntityId(this.EntityId);

                    //Add codes to dictionary for lookup later on
                    foreach (LocationInfoDto info in locationInfoDtos)
                    {
                        if (!locationIdLookupDictionary.ContainsKey(info.Code))
                        {
                            locationIdLookupDictionary.Add(info.Code, info.Id);
                        }
                    }

                    locationInfoDtos = null;
                }
								
                Dictionary<String, Int32> productGroupIdLookupDictionary = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);
				
                Int32 hierarchyId;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyId = dal.FetchByEntityId(this.EntityId).Id;
                }
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    List<ProductGroupDto> groups = dal.FetchByProductHierarchyId(hierarchyId).ToList();
                    foreach (ProductGroupDto group in groups)
                    {
                        if (!productGroupIdLookupDictionary.ContainsKey(group.Code))
                        {
                            productGroupIdLookupDictionary.Add(group.Code, group.Id);
                        }
                    }
                }
				
                Dictionary<String, Int32> planogramIdLookupDictionary = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);

                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    foreach (var planogramGroup in _planogramGroup.EnumerateAllChildGroups())
                    {
                        List<PlanogramInfoDto> planogramInfos = dal.FetchByPlanogramGroupId(planogramGroup.Id).ToList();
						
                        if (_planogramAssignmentStatusType != PlanogramAssignmentStatusType.Any)
                        {
                            planogramInfos = planogramInfos.Where(p => p.Status == (Byte)_planogramAssignmentStatusType).ToList();
                        }

                        foreach (PlanogramInfoDto planogramInfo in planogramInfos)
                        {
                            if (!planogramIdLookupDictionary.ContainsKey(planogramInfo.Name))
                            {
                                planogramIdLookupDictionary.Add(planogramInfo.Name, planogramInfo.Id);
                            }
                        }
                    }
                }

                #endregion

                #region Load valid planogram asignments

                //Set location id mapping
                ImportMapping locationCodeMappingForImportType = this.ImportData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId).FirstOrDefault();
                //Set product id mapping
                ImportMapping productGroupMappingForImportType = this.ImportData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.ProductGroupCodeMappingId).FirstOrDefault();
                //Set planogram id mapping
                ImportMapping planogramNameMappingForImportType = this.ImportData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId).FirstOrDefault();

                //get location id mapping
                Int32 locationCodeMappingColumnNumber = this.ImportData.GetMappedColNumber(locationCodeMappingForImportType);
                //get product group id mapping
                Int32 productGroupMappingColumnNumber = this.ImportData.GetMappedColNumber(productGroupMappingForImportType);
                //get planogram id mapping
                Int32 planogramMappingColumnNumber = this.ImportData.GetMappedColNumber(planogramNameMappingForImportType);
                
                Dictionary<String, List<LocationPlanAssignmentSearchCriteriaDto>> dataToImport = new Dictionary<String, List<LocationPlanAssignmentSearchCriteriaDto>>();

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    String locationCode = Convert.ToString(row[locationCodeMappingColumnNumber], CultureInfo.InvariantCulture)?.Trim();
                    String productGroupCode = Convert.ToString(row[productGroupMappingColumnNumber], CultureInfo.InvariantCulture)?.Trim();
                    String planogramName = Convert.ToString(row[planogramMappingColumnNumber], CultureInfo.InvariantCulture)?.Trim();

                    if (locationCode != null && productGroupCode != null && planogramName != null &&
                        locationIdLookupDictionary.ContainsKey(locationCode) &&
                        productGroupIdLookupDictionary.ContainsKey(productGroupCode) &&
                        planogramIdLookupDictionary.ContainsKey(planogramName))
                    {
                        LocationPlanAssignmentSearchCriteriaDto locationPlanAssignmentLink = new LocationPlanAssignmentSearchCriteriaDto()
                        {
                            LocationCode = locationCode,
                            ProductGroupCode = productGroupCode,
                            PlanogramName = planogramName
                        };

                        List<LocationPlanAssignmentSearchCriteriaDto> linkedPlanograms;
                        String key = _locationInfo != null ? productGroupCode : locationCode;

                        if (!dataToImport.TryGetValue(key, out linkedPlanograms))
                        {
                            linkedPlanograms = new List<LocationPlanAssignmentSearchCriteriaDto>();
                            dataToImport.Add(key, linkedPlanograms);
                        }
                        if (!linkedPlanograms.Contains(locationPlanAssignmentLink))
                        {
                            linkedPlanograms.Add(locationPlanAssignmentLink);
                        }
                    }
                }

                #endregion

                #region Import Planogram Assignments

                if (dataToImport.Any())
                {
                    using (ILocationPlanAssignmentDal dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
                    {
                        List<LocationPlanAssignmentDto> planogramAssignments = new List<LocationPlanAssignmentDto>();

                        if (_productGroupInfo != null)
                        {
                            planogramAssignments = dal.FetchByProductGroupId(_productGroupInfo.Id).ToList();
                        }
                        else
                        {
                            planogramAssignments = dal.FetchByLocationId(_locationInfo.Id).ToList();
                        }
						
						foreach(String key in dataToImport.Keys)
                        {
                            // depending on type of key we could be looking to add for locations for a category or categories for a location.
                            List<LocationPlanAssignmentDto> existingPlanogramAssignments = planogramAssignments.Where(p => _locationInfo != null ? 
															p.ProductGroupId == productGroupIdLookupDictionary[key] : p.LocationId == locationIdLookupDictionary[key]).ToList();
							
                            foreach (var planAssignment in dataToImport[key])
                            {
                                LocationPlanAssignmentDto newLocationPlanAssignmentDto = new LocationPlanAssignmentDto();
								
                                if (existingPlanogramAssignments.Any())
                                {
                                    LocationPlanAssignmentDto existingPlanAssignment = existingPlanogramAssignments.FirstOrDefault(p => p.LocationId == locationIdLookupDictionary[planAssignment.LocationCode] &&
                                                                        p.ProductGroupId == productGroupIdLookupDictionary[planAssignment.ProductGroupCode] &&
                                                                        p.PlanogramId == planogramIdLookupDictionary[planAssignment.PlanogramName]);

                                    if (existingPlanAssignment != null)
                                    {
                                        // remove from list of existing planogram assignments so it isnt deleted
                                        existingPlanogramAssignments.Remove(existingPlanAssignment);
                                    }
                                    else
                                    {
                                        // add
                                        newLocationPlanAssignmentDto.LocationId = locationIdLookupDictionary[planAssignment.LocationCode];
                                        newLocationPlanAssignmentDto.ProductGroupId = productGroupIdLookupDictionary[planAssignment.ProductGroupCode];
                                        newLocationPlanAssignmentDto.PlanogramId = planogramIdLookupDictionary[planAssignment.PlanogramName];
                                        newLocationPlanAssignmentDto.AssignedByUserId = (Int32)DomainPrincipal.CurrentUserId;
                                        newLocationPlanAssignmentDto.DateAssigned = DateTime.UtcNow;
                                        dal.Insert(newLocationPlanAssignmentDto);
                                    }
                                }
                                else
                                {
                                    // add new 
                                    newLocationPlanAssignmentDto.LocationId = locationIdLookupDictionary[planAssignment.LocationCode];
                                    newLocationPlanAssignmentDto.ProductGroupId = productGroupIdLookupDictionary[planAssignment.ProductGroupCode];
                                    newLocationPlanAssignmentDto.PlanogramId = planogramIdLookupDictionary[planAssignment.PlanogramName];
                                    newLocationPlanAssignmentDto.AssignedByUserId = (Int32)DomainPrincipal.CurrentUserId;
                                    newLocationPlanAssignmentDto.DateAssigned = DateTime.UtcNow;
                                    dal.Insert(newLocationPlanAssignmentDto);
                                }
                            }

							foreach(var existingPlanAssignment in existingPlanogramAssignments)
                            {
                                dal.DeleteById(existingPlanAssignment.Id);
                            }
                        }
                    }
                }

                #endregion

                dalContext.Commit();
            }
        }

        #endregion
    }
}
