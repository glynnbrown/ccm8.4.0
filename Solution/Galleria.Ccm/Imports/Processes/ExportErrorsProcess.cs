﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Imports.Processes
{
    [Serializable]
    public partial class ExportErrorsProcess : ProcessBase<ExportErrorsProcess>
    {
        #region Properties

        /// <summary>
        /// The file data being exported to excel
        /// </summary>
        public static readonly PropertyInfo<ImportFileData> ExportDataProperty =
            RegisterProperty<ImportFileData>(o => o.ExportData);
        public ImportFileData ExportData
        {
            get { return ReadProperty<ImportFileData>(ExportDataProperty); }
        }

        /// <summary>
        /// The export file name and path
        /// </summary>
        public static readonly PropertyInfo<String> FileNameProperty =
            RegisterProperty<String>(o => o.FileName);
        public String FileName
        {
            get { return ReadProperty<String>(FileNameProperty); }
        }

        /// <summary>
        /// The validation errors
        /// </summary>
        public static readonly PropertyInfo<IEnumerable<ValidationErrorGroup>> ErrorGroupsProperty =
            RegisterProperty<IEnumerable<ValidationErrorGroup>>(o => o.ErrorGroups);
        public IEnumerable<ValidationErrorGroup> ErrorGroups
        {
            get { return ReadProperty<IEnumerable<ValidationErrorGroup>>(ErrorGroupsProperty); }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ExportErrorsProcess(ImportFileData data, IEnumerable<ValidationErrorGroup> errors, String filePath)
        {
            LoadProperty<ImportFileData>(ExportDataProperty, data);
            LoadProperty<IEnumerable<ValidationErrorGroup>>(ErrorGroupsProperty, errors);
            LoadProperty<String>(FileNameProperty, filePath);
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ExportErrorsProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ExportErrorsProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ExportErrorsProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ExportErrorsProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
