﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Csla.Rules.CommonRules;
using Csla.Rules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management location validation process class
    /// </summary>
    [Serializable]
    public sealed partial class ValidateLocationsProcess : ValidateProcessBase<ValidateLocationsProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ValidateLocationsProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstRow)
            : base(entityId, fileData, startingRowNumber, headersInFirstRow)
        {

        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidateLocationsProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationsProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationsProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationData.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationsProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
