﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Our data management location hierarchy validation process class
    /// </summary>
    [Serializable]
    public partial class ValidateLocationHierarchyProcess : ValidateProcessBase<ValidateLocationHierarchyProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ValidateLocationHierarchyProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstrow)
            : base(entityId, fileData, startingRowNumber, headersInFirstrow)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidateLocationHierarchyProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationHierarchyProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationHierarchyProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationHierarchyData.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationHierarchyProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
