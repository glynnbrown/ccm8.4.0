﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#region Version History: (CCM 8.0)
// V8-29702 : M.Shelley
//  Check for empty "Bay Number" and "Element Order" columns that return a DBNull value
//  to prevent an invalid cast exception and premature termination of the validation
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using System.Globalization;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management location space element validation process class
    /// </summary>
    public partial class ValidateLocationSpaceElementProcess
    {
        #region Fields

        private HashSet<String> _validationParentProductGroupObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentLocationObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<LocationSpaceBaySearchCriteriaDto> _validationParentBayObjects = new HashSet<LocationSpaceBaySearchCriteriaDto>();
        private HashSet<LocationSpaceElementSearchCriteriaDto> _validationImportActionObjects = new HashSet<LocationSpaceElementSearchCriteriaDto>();
        private HashSet<LocationSpaceElementSearchCriteriaDto> _validationIdentifierFields = new HashSet<LocationSpaceElementSearchCriteriaDto>();
        private HashSet<LocationSpaceElementSearchCriteriaDto> _validationDuplicates = new HashSet<LocationSpaceElementSearchCriteriaDto>();

        // SA-19138:
        private Int32 _firstRowNum;

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the validation parent product group objects
        /// </summary>
        public HashSet<String> ValidationParentProductGroupObjects
        {
            get { return _validationParentProductGroupObjects; }
            set { _validationParentProductGroupObjects = value; }
        }


        /// <summary>
        /// Gets/Sets the validation parent location objects
        /// </summary>
        public HashSet<String> ValidationParentLocationObjects
        {
            get { return _validationParentLocationObjects; }
            set { _validationParentLocationObjects = value; }
        }

        /// <summary>
        /// Gets/Sets the validation parent bay objects
        /// </summary>
        public HashSet<LocationSpaceBaySearchCriteriaDto> ValidationParentBayObjects
        {
            get { return _validationParentBayObjects; }
            set { _validationParentBayObjects = value; }
        }

        /// <summary>
        /// Gets/Sets the validation impot action objects
        /// </summary>
        public HashSet<LocationSpaceElementSearchCriteriaDto> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }

        /// Gets the validation identifier fields
        /// </summary>
        public HashSet<LocationSpaceElementSearchCriteriaDto> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }

        /// <summary>
        /// Gets the validation duplicates
        /// </summary>
        public HashSet<LocationSpaceElementSearchCriteriaDto> ValidationDuplicates
        {
            get { return _validationDuplicates; }
        }

        /// <summary>
        /// Local property for the mappings
        /// </summary>
        private ImportMapping locationCodeMappingforImportType;
        private ImportMapping productGroupCodeMappingforImportType;
        private ImportMapping bayOrderMappingforImportType;
        private ImportMapping orderMappingforImportType;
        private Int32 categoryCodeColumnIndex;
        private Int32 bayOrderColumnIndex;

        #endregion

        #region Methods
        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation started.");

            //Set mappings
            locationCodeMappingforImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId).FirstOrDefault();
            productGroupCodeMappingforImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId).FirstOrDefault();
            bayOrderMappingforImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationSpaceElementImportMappingList.BayOrderMappingId).FirstOrDefault();
            orderMappingforImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationSpaceElementImportMappingList.OrderMappingId).FirstOrDefault();

            //Setup datatable
            ImportFileData worksheetData = this.FileData;
            // SA-19138:
            _firstRowNum = FileData.Rows[0].RowNumber;

            Trace.TraceInformation("{0} location space element's being validated.", worksheetData.Rows.Count);

            #region Fetch Relational Check Objects
            // ISO-13449: Don't Dispose of DalFactory instances fetched from the DalContainer.
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // start a transaction for this data validation
                dalContext.Begin();
                //Get mappings
                Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationCodeMappingforImportType);
                Int32 productGroupCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productGroupCodeMappingforImportType);
                Int32 bayOrderMappingColumnNumber = this.FileData.GetMappedColNumber(bayOrderMappingforImportType);
                Int32 orderMappingColumnNumber = this.FileData.GetMappedColNumber(orderMappingforImportType);
                //Get current entity id
                Int32 entityId = this.EntityId;

                //Create lists to codes
                HashSet<String> locationCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                HashSet<String> productGroupCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                HashSet<LocationSpaceBaySearchCriteriaDto> bayOrders = new HashSet<LocationSpaceBaySearchCriteriaDto>();
                HashSet<String> orders = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

                foreach (ImportFileDataRow row in worksheetData.Rows)
                {
                    // Build up collections of details
                    String locationCode = Convert.ToString(row[locationCodeMappingColumnNumber], CultureInfo.InvariantCulture);
                    String productGroupCode = Convert.ToString(row[productGroupCodeMappingColumnNumber], CultureInfo.InvariantCulture);
                    Byte bayOrder = Convert.ToByte(CheckBayOrderNull(row[bayOrderMappingColumnNumber].Value), CultureInfo.InvariantCulture);
                    Byte order = Convert.ToByte(CheckBayOrderNull(row[orderMappingColumnNumber].Value), CultureInfo.InvariantCulture);

                    if (locationCode != null && productGroupCode != null && bayOrder != null && order != null)
                    {
                        locationCodes.Add(locationCode);

                        //Constructor criteria
                        LocationSpaceElementSearchCriteriaDto criteria = new LocationSpaceElementSearchCriteriaDto()
                        {
                            LocationCode = locationCode,
                            ProductGroupCode = productGroupCode,
                            BayOrder = bayOrder,
                            Order = order
                        };

                        //[ISO-13387] While we're looping here anyway, we might as well go ahead and find duplicates.
                        if (this.ValidationIdentifierFields.Contains(criteria))
                        {
                            this.ValidationDuplicates.Add(criteria);
                        }
                        else
                        {
                            this.ValidationIdentifierFields.Add(criteria);
                        }
                    }
                }

                // now retrieve all locations that match the location codes
                // to inform the user of the import action that will take place
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    List<LocationInfoDto> locationInfoDtosIncludingDeleted = dal.FetchByEntityIdLocationCodes(
                        entityId, locationCodes.Distinct()).ToList();

                    //Strip deleted items
                    List<LocationInfoDto> locationInfoDtos = locationInfoDtosIncludingDeleted.Where(l => l.DateDeleted == null).ToList();

                    //Add codes to hashset for faster searching later on
                    locationInfoDtos.ForEach(p => this.ValidationParentLocationObjects.Add(p.Code));
                }

                //Add possible parent codes to hashset for faster searching later
                Int32 hierarchyId;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyId = dal.FetchByEntityId(entityId).Id;
                }
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    List<ProductGroupDto> groups = dal.FetchByProductHierarchyId(hierarchyId).ToList();
                    foreach (ProductGroupDto group in groups)
                    {
                        ValidationParentProductGroupObjects.Add(group.Code);
                    }
                }

                // now retrieve all location space bays for this entity id
                // to inform the user of the import action that will take place
                using (ILocationSpaceBaySearchCriteriaDal dal = dalContext.GetDal<ILocationSpaceBaySearchCriteriaDal>())
                {
                    List<LocationSpaceBaySearchCriteriaDto> locationSpaceBays = dal.FetchByEntityId(entityId).ToList();

                    //Add codes to hashset for faster searching later on
                    locationSpaceBays.ForEach(c => this.ValidationParentBayObjects.Add(c));
                }

                // now retrieve all location space elements for this entity id
                // to inform the user of the import action that will take place
                using (ILocationSpaceElementSearchCriteriaDal dal = dalContext.GetDal<ILocationSpaceElementSearchCriteriaDal>())
                {
                    List<LocationSpaceElementSearchCriteriaDto> locationSpaceElements = dal.FetchByEntityId(entityId).ToList();

                    //Add codes to hashset for faster searching later on
                    locationSpaceElements.ForEach(c => this.ValidationImportActionObjects.Add(c));
                }
            }
            #endregion

            //Validate Data against mappings using Generic methods
            this.RunDataTypeValidation(worksheetData);

            //update datatable to include import action and excel row number columns
            this.AddExtraColumns(worksheetData);

            //Set validated data to be the updated data item and set it to be returned
            this.ValidatedData = worksheetData;

            Trace.TraceInformation("Validation of {0} location space elements complete.", worksheetData.Rows.Count);
        }

        private Object CheckBayOrderNull(Object checkValue)
        {
            Object bayOrder = (checkValue is System.DBNull ? 1 : checkValue);

            return bayOrder;
        }

        /// <summary>
        /// Override parent record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationSpaceElementImportMappingList.LocationCodeMappingId)
            {
                //Check whether the location code matches a store in the database
                if (!this.ValidationParentLocationObjects.Contains(columnValue.ToString()))
                {
                    //No matching store code, create new error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(), Message.DataManagement_Validation_NoMatchingLocation, ValidationErrorType.Error, true, false, false, false, col, false);
                }
            }
            else if (col.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId)
            {
                //Check whether the location code matches a store in the database
                if (!this.ValidationParentProductGroupObjects.Contains(columnValue.ToString()))
                {
                    //No matching store code, create new error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(), Message.DataManagement_Validation_NoMatchingProductGroup, ValidationErrorType.Error, true, false, false, false, col, false);
                }
            }
            else if (col.PropertyIdentifier == LocationSpaceElementImportMappingList.BayOrderMappingId)
            {
                if (this.FileData != null)
                {
                    //Construct unique data grouping
                    Int32 locationCodeColNumber = this.FileData.GetMappedColNumber(locationCodeMappingforImportType);
                    Int32 productGroupCodeColNumber = this.FileData.GetMappedColNumber(productGroupCodeMappingforImportType);

                    //Get product group code
                    String productGroupCode = this.FileData.Rows.FirstOrDefault(r => r.RowNumber == rowNumber)
                        .Cells.FirstOrDefault(c => c.ColumnNumber == productGroupCodeColNumber).Value.ToString();

                    //Get location code
                    String locationCode = this.FileData.Rows.FirstOrDefault(r => r.RowNumber == rowNumber)
                        .Cells.FirstOrDefault(c => c.ColumnNumber == locationCodeColNumber).Value.ToString();

                    var orderItemValue = CheckBayOrderNull(columnValue);

                    try
                    {
                        //Create bay criteria
                        LocationSpaceBaySearchCriteriaDto bayCriteria = new LocationSpaceBaySearchCriteriaDto()
                        {
                            LocationCode = locationCode,
                            ProductGroupCode = productGroupCode,
                            Order = Convert.ToByte(orderItemValue)
                        };

                        //Check whether the bay exists in the database
                        if (!this.ValidationParentBayObjects.Contains(bayCriteria))
                        {
                            return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, orderItemValue.ToString(), Message.DataManagement_Validation_NoMatchingBay, ValidationErrorType.Error, true, false, false, false, col, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex is FormatException || ex is OverflowException)
                        {
                            return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, orderItemValue.ToString(), Message.DataManagement_Validation_NoMatchingBay, ValidationErrorType.Error, true, false, false, false, col, false);
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Override duplicate record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationSpaceElementImportMappingList.ProductGroupCodeMappingId)
            {
                //Construct unique data group
                String productGroupCode = columnValue.ToString();

                Int32 rowIndex = rowNumber - _firstRowNum;
                ImportFileDataRow row = FileData.Rows[rowIndex];

                Int32 locationCodeMappingColumnNumber = FileData.GetMappedColNumber(locationCodeMappingforImportType);
                String locationCode = row[locationCodeMappingColumnNumber].Value.ToString();

                Int32 bayOrderMappingColumnNumber = FileData.GetMappedColNumber(bayOrderMappingforImportType);
                Byte bayOrder = Convert.ToByte(CheckBayOrderNull(row[bayOrderMappingColumnNumber].Value));

                Int32 orderMappingColumnNumber = FileData.GetMappedColNumber(orderMappingforImportType);
                Byte order = Convert.ToByte(CheckBayOrderNull(row[orderMappingColumnNumber].Value));

                //Construct criteria
                LocationSpaceElementSearchCriteriaDto searchItem = new LocationSpaceElementSearchCriteriaDto()
                {
                    LocationCode = locationCode,
                    ProductGroupCode = productGroupCode,
                    BayOrder = bayOrder,
                    Order = order
                };

                //Check whether the location space bay is a duplicate of another being imported
                if (this.ValidationDuplicates.Contains(searchItem))
                {
                    //Duplicate found, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, productGroupCode, Message.DataManagement_Validation_DuplicateLocationSpaceElement, ValidationErrorType.Error, true, false, false, false, col, true);
                }
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        /// <param name="rowData"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            //Construct unique data grouping
            Int32 locationCodeColNumber = this.FileData.GetMappedColNumber(locationCodeMappingforImportType);
            Int32 productGroupCodeColNumber = this.FileData.GetMappedColNumber(productGroupCodeMappingforImportType);
            Int32 bayOrderMappingColumnNumber = this.FileData.GetMappedColNumber(bayOrderMappingforImportType);
            Int32 orderColNumber = this.FileData.GetMappedColNumber(orderMappingforImportType);

            if (locationCodeColNumber != 0 && productGroupCodeColNumber != 0 && bayOrderMappingColumnNumber != 0 && orderColNumber != 0)
            {
                if (rowData[productGroupCodeColNumber].Value != null
                    && rowData[orderColNumber].Value != null
                        && rowData[bayOrderMappingColumnNumber].Value != null
                            && rowData[locationCodeColNumber].Value != null)
                {
                    //Construct search criteria

                    LocationSpaceElementSearchCriteriaDto searchItem = new LocationSpaceElementSearchCriteriaDto()
                    {
                        LocationCode = rowData[locationCodeColNumber].Value.ToString(),
                        ProductGroupCode = rowData[productGroupCodeColNumber].Value.ToString(),
                        BayOrder = Convert.ToByte(CheckBayOrderNull(rowData[bayOrderMappingColumnNumber].Value)),
                        Order = Convert.ToByte(CheckBayOrderNull(rowData[orderColNumber].Value)),
                    };

                    // Check whether the cluster data matches a cluster in the database
                    if (!this.ValidationImportActionObjects.Contains(searchItem))
                    {
                        // If ths location code is new, but a duplicate record is also being imported, the first record
                        // will be insert, all others will then be updates
                        this.ValidationImportActionObjects.Add(searchItem);
                        return Message.DataManagement_ImportType_Add;
                    }
                    else
                    {
                        //Otherwise it is an update
                        return Message.DataManagement_ImportType_Update;
                    }
                }
                else
                {
                    return Message.DataManagement_ImportType_Add;
                }
            }
            else
            {
                //[TODO] work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }

        #endregion
    }
}
