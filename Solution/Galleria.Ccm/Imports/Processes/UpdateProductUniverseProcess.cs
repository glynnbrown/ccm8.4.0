﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
// CCM-26560 : A.Probyn
//  Corrected authorisation rule.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Processes;
using Csla;
using Galleria.Ccm.Security;
using Csla.Rules.CommonRules;
using Csla.Rules;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// A process that builds and updates all libraries
    /// following the import of new products or categories
    /// into the solution
    /// </summary>
    [Serializable]
    public partial class UpdateProductUniverseProcess : ProcessBase<UpdateProductUniverseProcess>
    {
        #region Properties
        /// <summary>
        /// Force this process to run on the server
        /// </summary>
        public override Boolean RunLocal
        {
            get { return false; }
        }


        public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The id of the entity to run this process for.
        /// </summary>
        public Int32 EntityId
        {
            get { return ReadProperty<Int32>(EntityIdProperty); }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public UpdateProductUniverseProcess(Int32 entityId)
        {
            LoadProperty<Int32>(EntityIdProperty, entityId);
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(UpdateProductUniverseProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(UpdateProductUniverseProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(UpdateProductUniverseProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductUniverseEdit.ToString()));
            BusinessRules.AddRule(typeof(UpdateProductUniverseProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
