﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Framework.Dal;
using System.Globalization;
using Galleria.Ccm.Imports.Mappings;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationsProcess
    {
        #region Constants

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} locations started.", this.ImportData.Rows.Count);

            ImportLocations();

            Trace.TraceInformation("Import of {0} locations complete.", this.ImportData.Rows.Count);
        }

        /// <summary>
        /// Does the import
        /// </summary>
        #region Version History: (GFS 1.0)
        // GFS-13601 : N.Donohoe
        //  Created
        // GFS-13645 : C Kelsall
        //	Remove property EntityId
        #endregion
        [ObfuscateControlFlow]
        private void ImportLocations()
        {
            Int32 entityId = this.EntityId;
            Int32 locationHierarchyId;
            Int32 locationTypeId;
            Boolean deleteNewGroup = false;

            //get a list of all mappings that need to be processed 
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            // now perform the import
            //Don't dispose of DalFactory instances fetched from DalContainer
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //create a location group to which all these new products can be assigned
                EntityDto currentEntity = null;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(entityId);
                }

                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    locationHierarchyId = dal.FetchByEntityId(entityId).Id;
                }
                //Only create levels and groups if the import includes locations with null group codes
                LocationGroupDto locationGroupDto = new LocationGroupDto();
                locationGroupDto.LocationHierarchyId = locationHierarchyId;
                locationGroupDto.Name = this.NewLocationGroupName;
                //[GFS-13782] Set code so no null reference exceptions in hierarchy imports afterwards
                locationGroupDto.Code = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 14);
                locationGroupDto.DateCreated = DateTime.UtcNow;
                locationGroupDto.DateLastModified = DateTime.UtcNow;

                //check if a level exists for the dto if not add one
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    IEnumerable<LocationLevelDto> existingDtos = dal.FetchByLocationHierarchyId(locationHierarchyId);
                    Int32 rootLevelId = existingDtos.First(l => !l.ParentLevelId.HasValue).Id;

                    LocationLevelDto existing2ndLevel = existingDtos.FirstOrDefault(l => l.ParentLevelId == rootLevelId);
                    if (existing2ndLevel == null)
                    {
                        existing2ndLevel = new LocationLevelDto();
                        existing2ndLevel.ParentLevelId = rootLevelId;
                        existing2ndLevel.LocationHierarchyId = locationHierarchyId;
                        existing2ndLevel.Name = "#2";
                        dal.Insert(existing2ndLevel);
                    }

                    //update the group to be inserted with the id
                    locationGroupDto.LocationLevelId = existing2ndLevel.Id;
                }


                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    //set parent id
                    IEnumerable<LocationGroupDto> existingDtos = dal.FetchByLocationHierarchyId(locationHierarchyId);
                    locationGroupDto.ParentGroupId = existingDtos.First(e => e.ParentGroupId == null).Id;

                    //insert the new group
                    dal.Insert(locationGroupDto);
                }

                //now cycle through rows
                List<LocationDto> locationDtoList = new List<LocationDto>();

                //Setup isSetDto
                LocationIsSetDto isSetDto = new LocationIsSetDto();

                //Get list of existing groups
                List<LocationGroupDto> locationGroups;
                Dictionary<String, Int32> locGroupCodeToIdLookup;
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    locationGroups = dal.FetchByLocationHierarchyId(locationHierarchyId).ToList();
                }
                locGroupCodeToIdLookup = locationGroups.ToDictionary(g => g.Code, g => g.Id, StringComparer.OrdinalIgnoreCase);

                //Build Location type lookup dictionary
                Dictionary<String, Int32> locationTypeIdLookup;

                foreach (ImportFileDataRow row in this.ImportData.Rows)
                {
                    //create the insert dtos 
                    LocationDto locationDto = new LocationDto();
                    locationDtoList.Add(locationDto);

                    //entityId
                    locationDto.EntityId = entityId;

                    if (locationGroupDto != null)
                    {
                        //location group id
                        locationDto.LocationGroupId = locationGroupDto.Id;
                    }

                    //loop through other resolved mappings
                    IFormatProvider prov = CultureInfo.InvariantCulture;
                    foreach (ImportMapping mapping in resolvedMappings)
                    {
                        Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                        Object cellValue = row[columnNumber].Value;
                        //If location group code, find group
                        if (mapping.PropertyIdentifier == LocationImportMappingList.GroupCodeMapId)
                        {
                            if (cellValue.ToString() != String.Empty && cellValue != DBNull.Value)
                            {
                                Int32 groupId;
                                if (locGroupCodeToIdLookup.TryGetValue(cellValue.ToString(), out groupId))
                                {
                                    locationDto.LocationGroupId = groupId;
                                }
                            }
                        }
                        else
                        {
                            // [GFS-13815] Corrected null value checks
                            cellValue = ProcessHelpers.CheckNullCellValues(mapping, cellValue);

                            //use the mapping list method to set the value
                            LocationImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, locationDto);
                        }

                        //Set isSetDto accordingly
                        LocationImportMappingList.SetIsSetPropertyByMappingId(mapping.PropertyIdentifier, isSetDto);
                    }
                }

                if (locationDtoList.Count > 0)
                {
                    //upsert all the locationlist
                    using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                    {
                        dal.Upsert(locationDtoList, isSetDto, false);
                    }

                    if (locationDtoList.FirstOrDefault(p => p.LocationGroupId == locationGroupDto.Id) == null)
                    {
                        deleteNewGroup = true;
                    }

                    //clear out lists
                    locationDtoList.Clear();
                }

                if (deleteNewGroup)
                {
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        //delete the new group as it is empty
                        dal.DeleteById(locationGroupDto.Id);
                        //[TODO] need to not add this at all rather than deleting
                        //however can't see a way without checking every row beforehand to see if a product group code is set
                    }
                }

                dalContext.Commit();
            }
        }

        #endregion
    }
}
