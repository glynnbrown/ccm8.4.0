﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationSpaceElementProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} location space elements started.", this.ImportData.Rows.Count);

            ImportLocationSpaceElements();

            Trace.TraceInformation("Import of {0} location space elements complete.", this.ImportData.Rows.Count);
        }

        /// <summary>
        /// Does the import.
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportLocationSpaceElements()
        {
            Int32 entityId = this.EntityId;

            //get a list of all mappings that need to be processed
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            ImportMapping locationCodeMapping = this.GetColumnMapping(LocationSpaceElementImportMappingList.LocationCodeMappingId);
            ImportMapping productGroupCodeMapping = this.GetColumnMapping(LocationSpaceElementImportMappingList.ProductGroupCodeMappingId);
            ImportMapping locationSpaceBayOrderMapping = this.GetColumnMapping(LocationSpaceElementImportMappingList.BayOrderMappingId);

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //get the entity
                EntityDto currentEntity = null;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(entityId);
                }

                #region Setup Location Space Bay lookup list

                // build our optimised location space info lookup
                IEnumerable<LocationSpaceBaySearchCriteriaDto> locationSpaceBaySearchCriteriaDtoList;
                using (ILocationSpaceBaySearchCriteriaDal dal = dalContext.GetDal<ILocationSpaceBaySearchCriteriaDal>())
                {
                    locationSpaceBaySearchCriteriaDtoList = dal.FetchByEntityId(entityId);
                }
                Dictionary<LocationSpaceBaySearchCriteriaDto, Int32> locationSpaceBaySearchCriteriaLookup = new Dictionary<LocationSpaceBaySearchCriteriaDto, Int32>();
                foreach (LocationSpaceBaySearchCriteriaDto locationSpaceBaySearchCriteriaDto in locationSpaceBaySearchCriteriaDtoList)
                {
                    locationSpaceBaySearchCriteriaLookup.Add(locationSpaceBaySearchCriteriaDto, locationSpaceBaySearchCriteriaDto.LocationSpaceBayId);
                }
                locationSpaceBaySearchCriteriaDtoList = null;

                #endregion

                #region Upsert Location Space Element
                // setup collection to store all location space elements that need upserting
                ObservableCollection<LocationSpaceElementDto> locationSpaceElementDtoList = new ObservableCollection<LocationSpaceElementDto>();
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    // now run through each row in the import table
                    foreach (ImportFileDataRow row in this.ImportData.Rows)
                    {
                        // create new dto
                        LocationSpaceElementDto locationSpaceElementDto = new LocationSpaceElementDto();

                        LocationSpaceBaySearchCriteriaDto searchCriteriaDto = new LocationSpaceBaySearchCriteriaDto()
                        {
                            LocationCode = Convert.ToString(row[this.ImportData.GetMappedColNumber(locationCodeMapping)].Value),
                            ProductGroupCode = Convert.ToString(row[this.ImportData.GetMappedColNumber(productGroupCodeMapping)].Value),
                            Order = Convert.ToByte(row[this.ImportData.GetMappedColNumber(locationSpaceBayOrderMapping)].Value)
                        };

                        //loop through other resolved mappings
                        foreach (ImportMapping mapping in resolvedMappings)
                        {
                            Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                            Object cellValue = row[columnNumber].Value;

                            if (mapping.PropertyIdentifier == LocationSpaceElementImportMappingList.BayOrderMappingId)
                            {
                                if (cellValue.ToString() != String.Empty && cellValue != DBNull.Value)
                                {
                                    //Lookup locationSpaceBayId
                                    locationSpaceElementDto.LocationSpaceBayId = Convert.ToInt32(locationSpaceBaySearchCriteriaLookup[searchCriteriaDto]);
                                }
                            }
                            else
                            {
                                // [GFS-13815] Corrected null value checks
                                cellValue = ProcessHelpers.CheckNullCellValues(mapping, cellValue);

                                //use the mapping list method to set the value
                                LocationSpaceElementImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, locationSpaceElementDto);
                            }
                        }

                        //Add to upsert list
                        if (!locationSpaceElementDtoList.Contains(locationSpaceElementDto))
                        {
                            locationSpaceElementDtoList.Add(locationSpaceElementDto);
                        }
                    }

                    //Upsert the unqiue data
                    dal.Upsert(locationSpaceElementDtoList);
                }

                #endregion

                dalContext.Commit();
            }
        }

        #endregion
    }
}
