﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Ccm.Security;
using Csla.Rules.CommonRules;
using Csla.Rules;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Import class for Location
    /// </summary>
    [Serializable]
    public partial class ImportLocationsProcess : ImportProcessBase<ImportLocationsProcess>
    {
        #region Properties

        public static readonly PropertyInfo<String> NewLocationGroupNameProperty =
            RegisterProperty<String>(l => l.NewLocationGroupName);
        /// <summary>
        /// Gets/Sets the name to  use for the new location group
        /// </summary>
        public String NewLocationGroupName
        {
            get { return ReadProperty<String>(NewLocationGroupNameProperty); }
            set { LoadProperty<String>(NewLocationGroupNameProperty, value); }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ImportLocationsProcess(Int32 entityId, ImportFileData importData, String locationGroupName) :
            base(entityId, importData)
        {
            LoadProperty<String>(NewLocationGroupNameProperty, locationGroupName);
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ImportLocationsProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationsProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationsProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationData.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationsProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
