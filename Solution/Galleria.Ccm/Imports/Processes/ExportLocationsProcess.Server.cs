﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Aspose.Cells;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Helpers;
using System.Diagnostics;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportLocationsProcess
    {
        #region Methods

        protected override void OnExecute()
        {
            Int32 entityId = this.EntityId;

            //get the mapping list of this export type
            IImportMappingList mappingList = LocationImportMappingList.NewLocationImportMappingList();

            //create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }

            //now write the data if required
            if (this.ExportHeadersOnly)
            {
                //apply a text style format to each column
                Style textDataStyle = new Style();
                textDataStyle.Number = 49;
                StyleFlag flag = new StyleFlag();
                flag.All = true;

                //set column data formats
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells.Columns[i].ApplyStyle(textDataStyle, flag);
                }
            }
            else
            {
                IEnumerable<LocationDto> locationDtoList;
                IEnumerable<LocationGroupDto> locationGroupDtoList;
                Int32 locationHierarchyId;

                //[TODO] consider if there are any conditions which define that we are only 
                //exporting a subset of the data rather than everything for the entity.

                //get the data to be exported
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                    {
                        locationDtoList = dal.FetchByEntityId(entityId);
                    }
                    using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                    {
                        locationHierarchyId = dal.FetchByEntityId(entityId).Id;
                    }
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        locationGroupDtoList = dal.FetchByLocationHierarchyId(locationHierarchyId);
                    }
                }

                //now run through and write all the products to the file
                Int32 row = 1;
                foreach (LocationDto location in locationDtoList)
                {
                    //cycle column mappings
                    for (Int32 i = 0; i < mappingList.Count; i++)
                    {
                        ImportMapping mapping = mappingList[i];

                        //get the value to be exported
                        Object cellValue =
                            LocationImportMappingList.GetValueByMappingId(mapping.PropertyIdentifier, location, locationGroupDtoList);

                        //write to worksheet
                        WpfHelper.SetColumnValue(workSheet.Cells[row, i], (cellValue != null) ? cellValue.ToString() : null);
                        
                    }
                    row++;
                }
            }

            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} location records exported successfuly.", workSheet.Cells.Rows.Count);
        }

        #endregion
    }
}
