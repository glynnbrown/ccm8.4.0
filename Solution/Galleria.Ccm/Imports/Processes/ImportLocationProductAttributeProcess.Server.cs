﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Framework.Imports;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using System.Globalization;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationProductAttributeProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} Location Product Attribute started.", this.ValidRowsCount);

            ImportLocationProductAttributes();

            Trace.TraceInformation("Import of {0} Location Product Attribute complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Does the import
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportLocationProductAttributes()
        {
            //get a list of all mappings that need to be processed 
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            // now perform the import
            //Don't dispose of DalFactory instances fetched from DalContainer
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                EntityDto currentEntity = null;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(this.EntityId);
                }

                //Create dictionary to lookup location id
                Dictionary<String, Int16> locationIdLookupDictionary = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> locationInfoDtos = dal.FetchByEntityId(this.EntityId);

                    //Add codes to dictionary for lookup later on
                    foreach (LocationInfoDto info in locationInfoDtos)
                    {
                        if (!locationIdLookupDictionary.ContainsKey(info.Code))
                        {
                            locationIdLookupDictionary.Add(info.Code, info.Id);
                        }
                    }

                    locationInfoDtos = null;
                }

                //Create dictionary to lookup product id
                Dictionary<String, Int32> productIdLookupDictionary = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> productInfoDtos = dal.FetchByEntityId(this.EntityId);

                    //Add GTIN's to dictionary for lookup later on
                    foreach (ProductInfoDto info in productInfoDtos)
                    {
                        if (!productIdLookupDictionary.ContainsKey(info.Gtin))
                        {
                            productIdLookupDictionary.Add(info.Gtin, info.Id);
                        }
                    }

                    productInfoDtos = null;
                }

                //now cycle through rows
                List<LocationProductAttributeDto> locationProductAttributeDtoList = new List<LocationProductAttributeDto>();
                Dictionary<String, Int16> parentLocationObjects = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                Dictionary<String, Int32> parentProductObjects = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);

                //Setup isSetDto 
                LocationProductAttributeIsSetDto isSetDto = new LocationProductAttributeIsSetDto();

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    //create the insert dtos 
                    LocationProductAttributeDto locationProductAttributeDto = new LocationProductAttributeDto();
                    locationProductAttributeDto.DateCreated = DateTime.UtcNow;
                    locationProductAttributeDto.DateLastModified = DateTime.UtcNow;
                    locationProductAttributeDtoList.Add(locationProductAttributeDto);

                    //entityId
                    locationProductAttributeDto.EntityId = this.EntityId;

                    //loop through other resolved mappings
                    IFormatProvider prov = CultureInfo.InvariantCulture;
                    foreach (ImportMapping mapping in resolvedMappings)
                    {

                        if (mapping.PropertyIdentifier == LocationProductAttributeImportMappingList.LocationCodeMapId)
                        {
                            //Set location id
                            String locationCode = row[this.ImportData.GetMappedColNumber(mapping)].ToString();
                            if (parentLocationObjects.ContainsKey(locationCode))
                            {
                                locationProductAttributeDto.LocationId = parentLocationObjects[locationCode];
                            }
                            else
                            {
                                //Lookup location code in dictionary
                                Int16 locationId;
                                if (locationIdLookupDictionary.TryGetValue(locationCode, out locationId))
                                {
                                    locationProductAttributeDto.LocationId = locationId;
                                    parentLocationObjects.Add(locationCode, locationId);
                                }
                            }
                        }
                        else if (mapping.PropertyIdentifier == LocationProductAttributeImportMappingList.ProductGTINMapId)
                        {
                            //Set product id
                            String productGTIN = row[this.ImportData.GetMappedColNumber(mapping)].ToString();
                            if (parentProductObjects.ContainsKey(productGTIN))
                            {
                                locationProductAttributeDto.ProductId = parentProductObjects[productGTIN];
                            }
                            else
                            {
                                //Lookup product gtin in dictionary
                                Int32 productId;
                                if (productIdLookupDictionary.TryGetValue(productGTIN, out productId))
                                {
                                    locationProductAttributeDto.ProductId = productId;
                                    parentProductObjects.Add(productGTIN, productId);
                                }
                            }
                        }
                        else
                        {

                            Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                            Object cellValue = row[columnNumber].CurrentValue;

                            // [GFS-13815] Corrected null value checks
                            cellValue = ProcessHelpers.CheckNullCellValues(mapping, cellValue);

                            if (mapping.PropertyIdentifier != LocationProductAttributeImportMappingList.LocationCodeMapId
                                && mapping.PropertyIdentifier != LocationProductAttributeImportMappingList.ProductGTINMapId)
                            {
                                //use the mapping list method to set the value
                                LocationProductAttributeImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, locationProductAttributeDto);
                            }
                        }
                        //Set isSetDto accordingly
                        LocationProductAttributeImportMappingList.SetIsSetPropertyByMappingId(mapping.PropertyIdentifier, isSetDto);
                    }
                }

                if (locationProductAttributeDtoList.Count > 0)
                {
                    //upsert all the metric list
                    using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                    {
                        dal.Upsert(locationProductAttributeDtoList, isSetDto);
                    }

                    //clear out lists
                    locationProductAttributeDtoList.Clear();
                }

                dalContext.Commit();
            }
        }

        #endregion
    }
}
