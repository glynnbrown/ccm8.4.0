﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Globalization;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management LocationProductLegal validation process class
    /// </summary>
    public partial class ValidateLocationProductLegalProcess
    {
        #region Fields

        private HashSet<String> _validationParentLocationObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentProductObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<LocationProductLegalCodeSearchCriteria> _validationImportActionObjects = new HashSet<LocationProductLegalCodeSearchCriteria>();
        private HashSet<LocationProductLegalCodeSearchCriteria> _validationIdentifierFields = new HashSet<LocationProductLegalCodeSearchCriteria>();
        private HashSet<LocationProductLegalCodeSearchCriteria> _validationDuplicates = new HashSet<LocationProductLegalCodeSearchCriteria>();

        #endregion

        #region Properties

        /// <summary>
        /// Get/Set Validation Parent Location Objects
        /// </summary>
        public HashSet<String> ValidationParentLocationObjects
        {
            get { return _validationParentLocationObjects; }
            set { _validationParentLocationObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Parent Product Objects
        /// </summary>
        public HashSet<String> ValidationParentProductObjects
        {
            get { return _validationParentProductObjects; }
            set { _validationParentProductObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Import Action Objects
        /// </summary>
        public HashSet<LocationProductLegalCodeSearchCriteria> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Identifier Fields
        /// </summary>
        public HashSet<LocationProductLegalCodeSearchCriteria> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }
        /// <summary>
        /// Get/Set Validation Duplicates
        /// </summary>
        public HashSet<LocationProductLegalCodeSearchCriteria> ValidationDuplicates
        {
            get { return _validationDuplicates; }
            set { _validationDuplicates = value; }
        }

        /// <summary>
        /// Local property for the location id mapping
        /// </summary>
        private ImportMapping locationCodeMappingForImportType;
        /// <summary>
        /// Local property for the product id mapping
        /// </summary>
        private ImportMapping productGTINMappingForImportType;
        private List<LocationInfoDto> LocationInfos = new List<LocationInfoDto>();
        private List<ProductInfoDto> ProductInfos = new List<ProductInfoDto>();

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation Started.");

            //Set location id mapping
            locationCodeMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationProductLegalImportMappingList.LocationCodeMapId).FirstOrDefault();
            //Set product id mapping
            productGTINMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationProductLegalImportMappingList.ProductGTINMapId).FirstOrDefault();

            Trace.TraceInformation("{0} Location Product Legal Records being validated", this.ValidRowsCount);

            #region Fetch Relational Check Objects

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //start a transaction for this data validation
                dalContext.Begin();

                //get location id mapping
                Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationCodeMappingForImportType);
                //get product id mapping
                Int32 productGTINMappingColumnNumber = this.FileData.GetMappedColNumber(productGTINMappingForImportType);

                //Create list tot store code links
                HashSet<LocationProductLegalCodeSearchCriteria> locationProductLegalLinks = new HashSet<LocationProductLegalCodeSearchCriteria>();
                HashSet<String> locationCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                HashSet<String> productCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

                Dictionary<String, Int32> productIdLookupDictionary = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);
                Dictionary<String, Int16> locationIdLookupDictionary = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                Dictionary<Int32, String> productGTINLookupDictionary = new Dictionary<Int32, String>();
                Dictionary<Int16, String> locationCodeLookupDictionary = new Dictionary<Int16, String>();

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    //Build up collection of ids
                    LocationProductLegalCodeSearchCriteria locationProductLink = new LocationProductLegalCodeSearchCriteria()
                    {
                        Code = Convert.ToString(row[locationCodeMappingColumnNumber], CultureInfo.InvariantCulture),
                        GTIN = Convert.ToString(row[productGTINMappingColumnNumber], CultureInfo.InvariantCulture)
                    };

                    if (locationProductLink.Code != null && locationProductLink.GTIN != null)
                    {
                        //Convert max property to int32
                        Int32 codeMaxValue = Convert.ToInt32(locationCodeMappingForImportType.PropertyMax);

                        //[GFS-13847] correct lengths of codes
                        if (locationProductLink.Code.Length > codeMaxValue)
                        {
                            locationProductLink.Code = locationProductLink.Code.Substring(0, codeMaxValue);
                        }

                        //Convert max property to int32
                        Int32 gtinMaxValue = Convert.ToInt32(productGTINMappingForImportType.PropertyMax);

                        if (locationProductLink.GTIN.Length > gtinMaxValue)
                        {
                            locationProductLink.GTIN = locationProductLink.GTIN.Substring(0, gtinMaxValue);
                        }


                        locationProductLegalLinks.Add(locationProductLink);
                        locationCodes.Add(locationProductLink.Code);
                        productCodes.Add(locationProductLink.GTIN);

                        //While looping also find duplicates
                        if (this.ValidationIdentifierFields.Contains(locationProductLink))
                        {
                            this.ValidationDuplicates.Add(locationProductLink);
                        }
                        else
                        {
                            this.ValidationIdentifierFields.Add(locationProductLink);
                        }
                    }
                }

                using (ILocationInfoDal locationInfoDal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> allLocationInfoDtos = locationInfoDal.FetchByEntityIdLocationCodes(this.EntityId, locationCodes);
                    //GFS-22188 - exclude deleted locations from this list
                    List<LocationInfoDto> locationInfoDtos = allLocationInfoDtos.Where(p => p.DateDeleted == null).ToList();

                    //Add codes to hashset for faster searching later on
                    foreach (LocationInfoDto info in locationInfoDtos)
                    {
                        ValidationParentLocationObjects.Add(info.Code);

                        //[GFS-17158] Add to dictionaries
                        if (!locationIdLookupDictionary.ContainsKey(info.Code))
                        {
                            locationIdLookupDictionary.Add(info.Code, info.Id);
                            locationCodeLookupDictionary.Add(info.Id, info.Code);
                        }
                    }

                    locationInfoDtos = null;
                }

                using (IProductInfoDal productInfoDal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> allProductInfoDtos = productInfoDal.FetchByEntityIdProductGtins(this.EntityId, productCodes);

                    //Add codes to hashset for faster searching later on
                    foreach (ProductInfoDto info in allProductInfoDtos)
                    {
                        ValidationParentProductObjects.Add(info.Gtin);

                        //[GFS-17158] Add to dictionaries
                        if (!productIdLookupDictionary.ContainsKey(info.Gtin))
                        {
                            productIdLookupDictionary.Add(info.Gtin, info.Id);
                            productGTINLookupDictionary.Add(info.Id, info.Gtin);
                        }
                    }
                    allProductInfoDtos = null;
                }

                List<Tuple<Int16, Int32>> combinationsList = new List<Tuple<Int16, Int32>>();
                foreach (LocationProductLegalCodeSearchCriteria locationProductLink in locationProductLegalLinks)
                {
                    //[GFS-17158] Lookup using dictionary
                    Int16 locationId;
                    if (locationIdLookupDictionary.TryGetValue(locationProductLink.Code, out locationId))
                    {
                        //[GFS-17158] Lookup using dictionary
                        Int32 productId;
                        if (productIdLookupDictionary.TryGetValue(locationProductLink.GTIN, out productId))
                        {
                            combinationsList.Add(new Tuple<Int16, Int32>(locationId, productId));
                        }
                    }
                }

                //now retrieve all LocationProductLegal links that match the location/product codes
                //to inform the user of the import action that will take place
                using (ILocationProductLegalDal dal = dalContext.GetDal<ILocationProductLegalDal>())
                {
                    IEnumerable<LocationProductLegalDto> locationProductLegalDtos =
                        dal.FetchByLocationIdProductIdCombinations(combinationsList);

                    //Add codes to hashset for faster searching later on
                    foreach (LocationProductLegalDto dto in locationProductLegalDtos)
                    {
                        //Lookup values in dictionaries
                        String locationCode;
                        String productGTIN;
                        if (locationCodeLookupDictionary.TryGetValue(dto.LocationId, out locationCode)
                                && productGTINLookupDictionary.TryGetValue(dto.ProductId, out productGTIN))
                        {
                            LocationProductLegalCodeSearchCriteria criteria = new LocationProductLegalCodeSearchCriteria()
                            {
                                Code = locationCode,
                                GTIN = productGTIN
                            };

                            this.ValidationImportActionObjects.Add(criteria);
                        }
                    }

                    productCodes.Clear();
                    productCodes = null;
                    locationCodes.Clear();
                    locationCodes = null;
                    locationProductLegalDtos = null;
                }
            }

            #endregion

            //validate data against mappings using generic methods
            this.RunDataTypeValidation(FileData, this.ValidRows);

            //update datatable to include import action and excel row number columns
            this.AddExtraColumns(FileData);

            //set validated data to be updated data item and set it to be returned
            this.ValidatedData = FileData;

            Trace.TraceInformation("Validation of {0} Location Product Legal records complete.", this.ValidRowsCount);

        }

        /// <summary>
        /// Override parent record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationProductLegalImportMappingList.LocationCodeMapId)
            {
                //Check whether the location code matches a location in the database
                if (!this.ValidationParentLocationObjects.Contains(columnValue.ToString()))
                {
                    //no matching location code, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                        Message.DataManagement_Validation_NoMatchingLocation, ValidationErrorType.Error,
                        true, false, false, false, col, false);
                }
            }
            else if (col.PropertyIdentifier == LocationProductLegalImportMappingList.ProductGTINMapId)
            {
                //Check whether the product code matches a product in the database
                if (!this.ValidationParentProductObjects.Contains(columnValue.ToString()))
                {
                    //no matching product GTIN, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                        Message.DataManagement_Validation_NoMatchingProduct, ValidationErrorType.Error,
                        true, false, false, false, col, false);
                }
            }
            return null;
        }

        /// <summary>
        /// Overrride dupicate record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationProductLegalImportMappingList.LocationCodeMapId)
            {
                String locationCode = columnValue.ToString();

                Int32 firstRowNumber = FileData.Rows[0].RowNumber;
                Int32 rowIndex = rowNumber - firstRowNumber;

                Int32 productGTINMappingColumnNumber = this.FileData.GetMappedColNumber(productGTINMappingForImportType);
                String productGTIN = this.FileData.Rows[rowIndex][productGTINMappingColumnNumber].Value.ToString();

                LocationProductLegalCodeSearchCriteria searchItem = new LocationProductLegalCodeSearchCriteria()
                {
                    Code = locationCode,
                    GTIN = productGTIN
                };

                //Check whether the link is a duplicate of another being imported
                if (this.ValidationDuplicates.Contains(searchItem))
                {
                    //Duplicate found, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                        locationCode, Message.DataManagement_Validation_DuplicateLocationProductLegal,
                        ValidationErrorType.Error, true, false, false, false, col, true);
                }
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            Int32 locationCodeColNumber = this.FileData.GetMappedColNumber(locationCodeMappingForImportType);
            Int32 productGTINColNumber = this.FileData.GetMappedColNumber(productGTINMappingForImportType);

            if (locationCodeColNumber != 0 && productGTINColNumber != 0)
            {
                //Extract link from rowdata
                LocationProductLegalCodeSearchCriteria criteria =
                    new LocationProductLegalCodeSearchCriteria()
                    {
                        Code = rowData[locationCodeColNumber].Value.ToString(),
                        GTIN = rowData[productGTINColNumber].Value.ToString()
                    };

                //Check whether the location/product link matches and existing link
                if (!this.ValidationImportActionObjects.Contains(criteria))
                {
                    //if the link is new, but a duplicate is also being inported the first record will
                    //be inserted, all others will be updates
                    this.ValidationImportActionObjects.Add(criteria);
                    return Message.DataManagement_ImportType_Add;
                }
                else
                {
                    //otherwise it is and update
                    return Message.DataManagement_ImportType_Update;
                }
            }
            else
            {
                //[TODO] work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }

        #endregion
    }
}
