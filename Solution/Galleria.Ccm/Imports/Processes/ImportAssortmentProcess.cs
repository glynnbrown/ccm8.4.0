﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-25455 : J.Pickup
//  Copied over from GFS 
#endregion

#endregion


using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Imports.Processes
{
    [Serializable]
    public partial class ImportAssortmentProcess : ImportProcessBase<ImportAssortmentProcess>
    {
        #region Constructors
        public ImportAssortmentProcess(Int32 entityId, ImportFileData importData)
            : base(entityId, importData)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ImportAssortmentProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportAssortmentProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportAssortmentProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportAssortmentData.ToString()));
            BusinessRules.AddRule(typeof(ImportAssortmentProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}