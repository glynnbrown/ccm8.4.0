﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Various helper methods for import processes
    /// </summary>
    public static class ProcessHelpers
    {
        /// <summary>
        /// Corrects null import values if necessary
        /// </summary>
        /// <param name="mapping">Current import mapping</param>
        /// <param name="cellValue">Current cell value</param>
        /// <returns>Modified cellValue</returns>
        public static Object CheckNullCellValues(ImportMapping mapping, Object cellValue)
        {
            //[GFS-13786] correct the value if necessary
            if (cellValue == null || cellValue == DBNull.Value || cellValue.ToString() == String.Empty)
            {
                //[GFS-13815] Added DateTime check
                if (mapping.PropertyType == typeof(DateTime?))
                {
                    cellValue = null;
                }
                else if (mapping.PropertyType.IsValueType)
                {
                    cellValue = Activator.CreateInstance(mapping.PropertyType);
                }
                else { cellValue = null; }
            }
            return cellValue;
        }
    }
}
