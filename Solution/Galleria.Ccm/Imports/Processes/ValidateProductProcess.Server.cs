﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25395 : A.Probyn 
//      Created.
// CCM-25452 : N.Haywood
//      Added validation for products
// V8-26041 : A.Kuszyk
//      Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
//      Changed use of CCM ProductOrientationType to Framework PlanogramProductOrientationType.
// V8-27628  : A.Probyn
//      Extended validation for FillPattern and ShapeType
#endregion
#region Version History: (CCM 801)
// V8-26114  : J.Pickup
//      Validation now passess enum dictionary keys through as lower invariants to match expected keys.
//      I have also refactored alot of the code for performance, correctability and readability. (Was a mess).
// V8-28863 : D.Pleasance
//  Amended to validate productCode value from data row. 
#endregion
#region Version History: (CCM 8.3.0)
// V8-30496 : N.Haywood
//  Changed product group validation to be a warning rather than an error as it is not mandatory in v8
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using SmartAssembly.Attributes;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Our product library validation process class
    /// </summary>
    public partial class ValidateProductLibraryProcess
    {
        #region Fields

        private HashSet<String> _validationImportActionObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationIdentifierFields = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private readonly HashSet<String> _validationDuplicates = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the validation impot action objects
        /// </summary>
        public HashSet<String> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }

        /// <summary>
        /// Gets the validation identifier fields
        /// </summary>
        private HashSet<String> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
        }

        /// <summary>
        /// Gets the validation duplicates
        /// </summary>
        private HashSet<String> ValidationDuplicates
        {
            get { return _validationDuplicates; }
        }

        /// <summary>
        /// Local property for the product code mapping
        /// </summary>
        private ImportMapping productCodeMappingforImportType;

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation started.");

            //Set product code mapping
            productCodeMappingforImportType = FileData.MappingList.FirstOrDefault(c => c.PropertyIdentifier == ProductLibraryImportMappingList.GtinMapId);

            Trace.TraceInformation("{0} product's being validated.", ValidRowsCount);

            #region Build Check Objects

            //Get code mapping
            ImportMapping productCodeMapping = FileData.MappingList.FirstOrDefault(c => c.PropertyIdentifier == ProductLibraryImportMappingList.GtinMapId);
            Int32 mappedColNumber = FileData.GetMappedColNumber(productCodeMapping);

            //Create lists to store product codes
            HashSet<String> productCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);//[ISO-13387

            foreach (ImportFileDataRow row in ValidRows)
            {
                //Build up collections of codes
                String productCode = Convert.ToString(row[mappedColNumber], CultureInfo.InvariantCulture);

                if (productCode == null) { productCode = String.Empty; }

                // cut down the code to its max length
                if (productCodeMapping == null) continue;

                //  Convert max property to int32
                Int32 maxValue = Convert.ToInt32(productCodeMapping.PropertyMax);

                if (productCode.Length > maxValue)
                {
                    productCode = productCode.Substring(0, maxValue);
                }

                //  Only add if not already in there to keep distinct
                if (!productCodes.Contains(productCode))
                {
                    productCodes.Add(productCode);
                }

                //  While we're looping here anyway, we might as well go ahead and find duplicates.
                if (ValidationIdentifierFields.Contains(productCode))
                {
                    ValidationDuplicates.Add(productCode);
                }
                else
                {
                    ValidationIdentifierFields.Add(productCode);
                }
            }
             
            #endregion

            //Validate Data against mappings using Generic methods
            RunDataTypeValidation(FileData, ValidRows);

            //Update datatable to include import action & excel row number columns
            AddExtraColumns(FileData);

            //Set validated data to be the updated data item and set it to be returned
            ValidatedData = FileData;

            Trace.TraceInformation("Validation of {0} products complete.", ValidRowsCount);
        }

        /// <summary>
        /// Override duplicate record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="colNumber"></param>
        /// <param name="columnValue"></param>
        /// <param name="rowNumber"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier != ProductLibraryImportMappingList.GtinMapId) return null;

            //Check whether the product code is a duplicate of another being imported
            String columnValueString = columnValue.ToString();
            if (ValidationDuplicates.Contains(columnValueString))
            {
                //Duplicate found, create error item
                return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                    columnValueString, Message.DataManagement_Validation_DuplicateProduct,
                    ValidationErrorType.Error, true, false, false, false, col, true);
            }
            return null;
        }

        /// <summary>
        /// Overrides import type validation base method
        /// </summary>
        /// <param name="rowData"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            //Always going to be an add as we are not storing this data down
            return Message.DataManagement_ImportType_Add;
        }

        /// <summary>
        /// Overrides Enum Lookup validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowNumber"></param>
        /// <param name="colNumber"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        protected override ValidationErrorItem RunEnumLookupValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (columnValue == null) return null;

            //Check if an enum exists for the friendly name entered (Using lower invariant)
            switch (col.PropertyIdentifier)
            {
                case ProductLibraryImportMappingList.StatusTypeMapId:
                    PlanogramProductStatusType statusType;
                    if (!PlanogramProductStatusTypeHelper.EnumFromFriendlyName.TryGetValue(columnValue.ToString().ToLowerInvariant(), out statusType))
                    {
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_InvalidProductStatusType, ValidationErrorType.Error, true,
                            !col.PropertyIsUniqueIdentifier, false, false, col, false);
                    }
                    return null;

                case ProductLibraryImportMappingList.OrientationTypeMapId:
                    PlanogramProductOrientationType orientationType;
                    if (!PlanogramProductOrientationTypeHelper.EnumFromFriendlyName.TryGetValue(columnValue.ToString().ToLowerInvariant(), out orientationType))
                    {
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_InvalidProductOrientationType, ValidationErrorType.Error,
                            true, !col.PropertyIsUniqueIdentifier, false, false, col, false);
                    }
                    return null;

                case ProductLibraryImportMappingList.MerchandisingStyleMapId:
                    ProductMerchandisingStyle merchStyle;
                    if (!ProductMerchandisingStyleHelper.EnumFromFriendlyName.TryGetValue(columnValue.ToString().ToLowerInvariant(), out merchStyle))
                    {
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_InvalidProductMerchandisingStyle,
                            ValidationErrorType.Error, true, !col.PropertyIsUniqueIdentifier, false, false, col, false);
                    }
                    break;
                case ProductLibraryImportMappingList.FillPatternMapId:
                    PlanogramProductFillPatternType fillPattern;
                    if (!PlanogramProductFillPatternTypeHelper.EnumFromFriendlyName.TryGetValue(columnValue.ToString().ToLowerInvariant(), out fillPattern))
                    {
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_InvalidProductFillPattern, ValidationErrorType.Error, true,
                            !col.PropertyIsUniqueIdentifier, false, false, col, false);
                    }
                    break;

                case ProductLibraryImportMappingList.ShapeTypeMapId:
                    PlanogramProductShapeType shapeType;
                    if (!PlanogramProductShapeTypeHelper.EnumFromFriendlyName.TryGetValue(columnValue.ToString().ToLowerInvariant(), out shapeType))
                    {
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_InvalidProductShapeType, ValidationErrorType.Error, true,
                            !col.PropertyIsUniqueIdentifier, false, false, col, false);
                    }
                    break;

                default:
                    return null;
            }
            return null;
        }

        #endregion
    }

    /// <summary>
    /// Our data management product validation process class
    /// </summary>
    public partial class ValidateProductProcess
    {
        #region Fields

        private HashSet<String> _validationParentObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationImportActionObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationIdentifierFields = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private readonly HashSet<String> _validationDuplicates = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the validation parent objects
        /// </summary>
        private HashSet<string> ValidationParentObjects
        {
            get { return _validationParentObjects; }
            set { _validationParentObjects = value; }
        }

        /// <summary>
        /// Gets/Sets the validation impot action objects
        /// </summary>
        private HashSet<string> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }

        /// <summary>
        /// Gets the validation identifier fields
        /// </summary>
        private HashSet<string> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }

        /// <summary>
        /// Gets the validation duplicates
        /// </summary>
        private HashSet<string> ValidationDuplicates
        {
            get { return _validationDuplicates; }
        }

        /// <summary>
        /// Local property for the product code mapping
        /// </summary>
        private ImportMapping productCodeMappingforImportType;

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation started.");

            // Set product code mapping
            productCodeMappingforImportType = FileData.MappingList.FirstOrDefault(c => c.PropertyIdentifier == ProductImportMappingList.GtinMapId);

            // Setup datatable
            ImportFileData worksheetData = FileData;

            Trace.TraceInformation("{0} product's being validated.", worksheetData.Rows.Count);

            #region Fetch Relational Check Objects
            // Get category codes from the worksheet data
            // now perform the import
            // Don't Dispose of DalFactory instances fetched from the DalContainer.
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // start a transaction for this data validation
                dalContext.Begin();

                // Get code mapping
                ImportMapping productCodeMapping = FileData.MappingList.FirstOrDefault(c => c.PropertyIdentifier == ProductImportMappingList.GtinMapId);
                Int32 mappedColNumber = FileData.GetMappedColNumber(productCodeMapping);

                // Create lists to store category & product codes
                HashSet<String> productCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);//[ISO-13387]

                foreach (ImportFileDataRow row in worksheetData.Rows)
                {
                    //Build up collections of codes
                    String productCode = Convert.ToString(row[mappedColNumber], CultureInfo.InvariantCulture);

                    if (productCode == null) { productCode = String.Empty; }

                    // Cut down the code to its max length
                    if (productCodeMapping == null) continue;

                    if (productCode.Length > productCodeMapping.PropertyMax)
                    {
                        productCode = productCode.Substring(0, Convert.ToInt32(productCodeMapping.PropertyMax));
                    }

                    productCodes.Add(productCode);

                    // While we're looping here anyway, we might as well go ahead and find duplicates.
                    if (ValidationIdentifierFields.Contains(productCode))
                    {
                        ValidationDuplicates.Add(productCode);
                    }
                    else
                    {
                        ValidationIdentifierFields.Add(productCode);
                    }
                }

                Int32 entityId = EntityId;
                //Add possible parent codes to hashset for faster searching later
                Int32 hierarchyId;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyId = dal.FetchByEntityId(entityId).Id;
                }
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    List<ProductGroupDto> groups = dal.FetchByProductHierarchyId(hierarchyId).ToList();
                    foreach (ProductGroupDto group in groups)
                    {
                        ValidationParentObjects.Add(group.Code);
                    }
                }

                // now retrieve all products that match the product codes
                // to inform the user of the import action that will take place

                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    //If validating ROM products, only fetch ROM products.
                    var productInfoDtos = dal.FetchByEntityIdProductGtins(entityId, productCodes.Distinct());

                    //Add codes to hashset for faster searching later on
                    foreach (ProductInfoDto dto in productInfoDtos)
                    {
                        ValidationImportActionObjects.Add(dto.Gtin);
                    }
                }
            }
            #endregion

            //Validate Data against mappings using Generic methods
            RunDataTypeValidation(worksheetData);

            //Update datatable to include import action & excel row number columns
            AddExtraColumns(worksheetData);

            //Set validated data to be the updated data item and set it to be returned
            ValidatedData = worksheetData;

            Trace.TraceInformation("Validation of {0} products complete.", worksheetData.Rows.Count);
        }

        /// <summary>
        /// Runs the Parent Record Validation.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowNumber"></param>
        /// <param name="colNumber"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier != ProductImportMappingList.GroupCodeMapId) return null;

            if (!ValidationParentObjects.Contains(columnValue.ToString()))
            {
                //no matching product group code
                return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                    Message.DataManagement_Validation_NoMatchingProductGroup, ValidationErrorType.Warning,
                    true, false, false, false, col, false);
            }
            return null;
        }

        /// <summary>
        /// Override duplicate record validation base method with colNumber.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="colNumber"></param>
        /// <param name="columnValue"></param>
        /// <param name="rowNumber"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier != ProductImportMappingList.GtinMapId) return null;
            //Check whether the product code is a duplicate of another being imported
            String columnValueString = columnValue.ToString();
            if (ValidationDuplicates.Contains(columnValueString))
            {
                //Duplicate found, create error item
                return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                    columnValueString, Message.DataManagement_Validation_DuplicateProduct,
                    ValidationErrorType.Error, true, false, false, false, col, true);
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        /// <param name="rowData"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            Int32 colNumber = FileData.GetMappedColNumber(productCodeMappingforImportType);

            if (colNumber == 0) return Message.DataManagement_ImportType_Update;

            // Get mappings -> this is set when class is constructed
            // Extract the product from the row data using this index
            String productCode = (rowData[colNumber].Value != null) ? rowData[colNumber].Value.ToString() : null;

            // Check whether the product code matches a product in the database
            if (ValidationImportActionObjects.Contains(productCode)) return Message.DataManagement_ImportType_Update;
                
            // If ths product code is new, but a duplicate record is also being imported, the first record
            // will be insert, all others will then be updates
            ValidationImportActionObjects.Add(productCode);
            return Message.DataManagement_ImportType_Add;

            //Otherwise it is an update
            //[TODO] Work out what should be done if this happens
        }

        /// <summary>
        /// Override the enum lookup validation
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowNumber"></param>
        /// <param name="colNumber"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        protected override ValidationErrorItem RunEnumLookupValidation(ImportMapping col, Int32 rowNumber, Int32 colNumber, Object columnValue)
        {
            if (columnValue == null) return null;

            //  Find the problem description according to the property if any.
            String problemDesc = String.Empty;
            var friendlyName = columnValue.ToString();
            switch (col.PropertyIdentifier)
            {
                case ProductImportMappingList.StatusTypeMapId:
                    if (PlanogramProductStatusTypeHelper.PlanogramProductStatusTypeGetEnum(friendlyName.ToLowerInvariant()) == null)
                    {
                        problemDesc = Message.DataManagement_Validation_InvalidProductStatusType;
                    }
                    break;

                case ProductImportMappingList.OrientationTypeMapId:
                    if (PlanogramProductOrientationTypeHelper.PlanogramProductOrientationTypeGetEnum(friendlyName.ToLowerInvariant()) == null)
                    {
                        problemDesc = Message.DataManagement_Validation_InvalidProductOrientationType;
                    }
                    break;

                case ProductImportMappingList.MerchandisingStyleMapId:
                    if (ProductMerchandisingStyleHelper.ProductMerchandisingStyleGetEnum(friendlyName) == null)
                    {
                        problemDesc = Message.DataManagement_Validation_InvalidProductMerchandisingStyle;
                    }
                    break;
            }

            //  Return the validation error if we found a problem, null otherwise.
            return !String.IsNullOrEmpty(problemDesc)
                ? ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                    problemDesc,
                    ValidationErrorType.Error, true, !col.PropertyIsUniqueIdentifier, false, false, col, false)
                : null;
        }

        #endregion
    }
}
