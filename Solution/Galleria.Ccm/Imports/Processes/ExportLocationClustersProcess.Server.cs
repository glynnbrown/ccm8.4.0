﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Imports;
using Aspose.Cells;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportLocationClustersProcess
    {
        #region Methods

        protected override void OnExecute()
        {
            Int32 entityId = this.EntityId;

            //get the mapping list of this export type
            IImportMappingList mappingList = LocationClusterImportMappingList.NewLocationClusterImportMappingList();

            //create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }

            //now write the data if required
            if (this.ExportHeadersOnly)
            {
                //apply a text style format to each column
                Style textDataStyle = new Style();
                textDataStyle.Number = 49;
                StyleFlag flag = new StyleFlag();
                flag.All = true;

                //set column data formats
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells.Columns[i].ApplyStyle(textDataStyle, flag);
                }
            }
            else
            {
                IEnumerable<ClusterLocationSearchCriteriaDto> clusterSchemeDtoList;

                //get the data to be exported
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IClusterLocationSearchCriteriaDal dal = dalContext.GetDal<IClusterLocationSearchCriteriaDal>())
                    {
                        clusterSchemeDtoList = dal.FetchByEntityId(entityId);
                    }
                }

                //now run through and write all the products to the file
                Int32 row = 1;
                foreach (ClusterLocationSearchCriteriaDto location in clusterSchemeDtoList)
                {
                    //cycle column mappings
                    for (Int32 i = 0; i < mappingList.Count; i++)
                    {
                        ImportMapping mapping = mappingList[i];

                        //get the value to be exported
                        Object cellValue =
                            LocationClusterImportMappingList.GetValueByMappingId(mapping.PropertyIdentifier, location);
                        //write to worksheet
                        WpfHelper.SetColumnValue(workSheet.Cells[row, i], (cellValue != null) ? cellValue.ToString() : null);
                    }
                    row++;
                }
            }

            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} location cluster records exported successfuly.", workSheet.Cells.Rows.Count);
        }

        #endregion
    }
}
