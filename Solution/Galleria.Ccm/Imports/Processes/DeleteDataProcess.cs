﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    [Serializable]
    public partial class DeleteDataProcess : ProcessBase<DeleteDataProcess>
    {
        #region Properties

        /// <summary>
        /// The type of data that requires deleting
        /// </summary>
        public static readonly PropertyInfo<CCMDataItemType> DataTypeProperty =
            RegisterProperty<CCMDataItemType>(o => o.DataType);
        public CCMDataItemType DataType
        {
            get { return ReadProperty<CCMDataItemType>(DataTypeProperty); }
        }

        /// <summary>
        /// The entity id to delete from
        /// </summary>
        public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(o => o.EntityId);
        public Int32 EntityId
        {
            get { return ReadProperty<Int32>(EntityIdProperty); }
        }



        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public DeleteDataProcess(
            CCMDataItemType dataType,
            Int32 entityId)
        {
            LoadProperty<CCMDataItemType>(DataTypeProperty, dataType);
            LoadProperty<Int32>(EntityIdProperty, entityId);
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(DeleteDataProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(DeleteDataProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(DeleteDataProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.DeleteData.ToString()));
            BusinessRules.AddRule(typeof(DeleteDataProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
