﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-25455 : J.Pickup
//  Copied over from GFS & removed versioning
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Aspose.Cells;
using Csla;
using SmartAssembly.Attributes;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportAssortmentProcess
    {
        #region Methods

        protected override void OnExecute()
        {
            //get the mapping list of the export type
            IImportMappingList mappingList = AssortmentImportMappingList.NewAssortmentImportMappingList();

            // get all the import mappings for this export type
            ImportMapping assortmentNameMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.AssortmentNameMapId);
            ImportMapping productGroupCodeMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.ProductGroupCodeMapId);
            ImportMapping productGTINMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.ProductGTINMapId);
            ImportMapping isRangedMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.IsRangedMapId);
            ImportMapping rankMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.RankMapId);
            ImportMapping facingsMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.FacingsMapId);
            ImportMapping unitsMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.UnitsMapId);
            ImportMapping segmentationMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.SegmentationMapId);
            ImportMapping productTreatmentTypeMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.ProductTreatmentTypeMapId);
            ImportMapping commentsMapping = this.GetColumnMapping(mappingList, AssortmentImportMappingList.CommentsMapId);


            //create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }

            //now write the data if required
            if (this.ExportHeadersOnly)
            {
                //apply a text style format to each column
                Style textDataStyle = new Style();
                textDataStyle.Number = 49;
                StyleFlag flag = new StyleFlag();
                flag.All = true;

                //set column data formats
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells.Columns[i].ApplyStyle(textDataStyle, flag);
                }
            }
            else
            {
                IEnumerable<ProductInfoDto> productInfoDtoList;
                IEnumerable<AssortmentInfoDto> assortmentInfoDtoList;
                IEnumerable<AssortmentProductDto> assortmentProductDtoList;

                //get the data to be exported
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    // get hierarchy
                    ProductHierarchyDto hierarchyDto;
                    using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                    {
                        hierarchyDto = dal.FetchByEntityId(this.EntityId);
                    }
                    //get product groups [GFS-24648 also include deleted]
                    List<ProductGroupDto> productGroupDtoList = new List<ProductGroupDto>();
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        productGroupDtoList = dal.FetchByProductHierarchyIdIncludingDeleted(hierarchyDto.Id).ToList();
                    }
                    //Fetch list of products to retrieve GTINs
                    using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                    {
                        productInfoDtoList = dal.FetchByEntityId(this.EntityId);
                    }
                    using (IAssortmentInfoDal dal = dalContext.GetDal<IAssortmentInfoDal>())
                    {
                        assortmentInfoDtoList = dal.FetchByEntityId(this.EntityId);
                    }

                    //now run through and write all the assortments to the file
                    Int32 row = 1;
                    foreach (AssortmentInfoDto assort in assortmentInfoDtoList)
                    {
                        using (IAssortmentProductDal dal = dalContext.GetDal<IAssortmentProductDal>())
                        {
                            assortmentProductDtoList = dal.FetchByAssortmentId(assort.Id);
                        }

                        String productGroupCode = productGroupDtoList.Where(p => p.Id == assort.ProductGroupId).First().Code;

                        foreach (AssortmentProductDto assortProduct in assortmentProductDtoList)
                        {
                            String productGTIN = productInfoDtoList.Where(p => p.Id == assortProduct.ProductId).First().Gtin;

                            WpfHelper.SetColumnValue(workSheet.Cells[row, assortmentNameMapping.PropertyIdentifier], assort.Name);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, productGroupCodeMapping.PropertyIdentifier], productGroupCode);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, productGTINMapping.PropertyIdentifier], productGTIN);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, isRangedMapping.PropertyIdentifier], assortProduct.IsRanged);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, rankMapping.PropertyIdentifier], assortProduct.Rank);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, facingsMapping.PropertyIdentifier], assortProduct.Facings);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, unitsMapping.PropertyIdentifier], assortProduct.Units);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, segmentationMapping.PropertyIdentifier], assortProduct.Segmentation);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, productTreatmentTypeMapping.PropertyIdentifier], PlanogramAssortmentProductTreatmentTypeHelper.FriendlyNames[(PlanogramAssortmentProductTreatmentType)assortProduct.ProductTreatmentType]);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, commentsMapping.PropertyIdentifier], assortProduct.Comments);

                            row++;
                        }
                    }
                }
            }

            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} assortment records exported successfully.", workSheet.Cells.Rows.Count);
        }

        #endregion
    }
}