﻿using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management planogram assignment validation process class
    /// </summary>
    public partial class ValidatePlanogramAssignmentProcess
    {
        #region Fields

        private HashSet<String> _validationParentLocationObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentProductGroupObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentPlanogramObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentPlanogramStatusObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        
        private HashSet<LocationPlanAssignmentSearchCriteriaDto> _validationImportActionObjects = new HashSet<LocationPlanAssignmentSearchCriteriaDto>();
        private HashSet<LocationPlanAssignmentSearchCriteriaDto> _validationIdentifierFields = new HashSet<LocationPlanAssignmentSearchCriteriaDto>();
        private HashSet<LocationPlanAssignmentSearchCriteriaDto> _validationDuplicates = new HashSet<LocationPlanAssignmentSearchCriteriaDto>();

        #endregion

        #region Properties

        /// <summary>
        /// Get/Set Validation Parent Location Objects
        /// </summary>
        public HashSet<String> ValidationParentLocationObjects
        {
            get { return _validationParentLocationObjects; }
            set { _validationParentLocationObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Parent Product Group Objects
        /// </summary>
        public HashSet<String> ValidationParentProductGroupObjects
        {
            get { return _validationParentProductGroupObjects; }
            set { _validationParentProductGroupObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Parent Planogram Objects
        /// </summary>
        public HashSet<String> ValidationParentPlanogramObjects
        {
            get { return _validationParentPlanogramObjects; }
            set { _validationParentPlanogramObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Parent Planogram Status Objects
        /// </summary>
        public HashSet<String> ValidationParentPlanogramStatusObjects
        {
            get { return _validationParentPlanogramStatusObjects; }
            set { _validationParentPlanogramStatusObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Import Action Objects
        /// </summary>
        public HashSet<LocationPlanAssignmentSearchCriteriaDto> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Identifier Fields
        /// </summary>
        public HashSet<LocationPlanAssignmentSearchCriteriaDto> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }
        /// <summary>
        /// Get/Set Validation Duplicates
        /// </summary>
        public HashSet<LocationPlanAssignmentSearchCriteriaDto> ValidationDuplicates
        {
            get { return _validationDuplicates; }
            set { _validationDuplicates = value; }
        }

        /// <summary>
        /// Local property for the location id mapping
        /// </summary>
        private ImportMapping locationCodeMappingForImportType;
        
        /// <summary>
        /// Local property for the product group id mapping
        /// </summary>
        private ImportMapping productGroupMappingForImportType;

        /// <summary>
        /// Local property for the planogram id mapping
        /// </summary>
        private ImportMapping planogramNameMappingForImportType;
        
        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation Started.");

            //Set location id mapping
            locationCodeMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId).FirstOrDefault();
            //Set product id mapping
            productGroupMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.ProductGroupCodeMappingId).FirstOrDefault();
            //Set planogram id mapping
            planogramNameMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId).FirstOrDefault();

            Trace.TraceInformation("{0} Planogram Assignment Records being validated", this.ValidRowsCount);

            #region Fetch Relational Check Objects

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //start a transaction for this data validation
                dalContext.Begin();

                //get location id mapping
                Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationCodeMappingForImportType);
                //get product group id mapping
                Int32 productGroupMappingColumnNumber = this.FileData.GetMappedColNumber(productGroupMappingForImportType);
                //get planogram id mapping
                Int32 planogramMappingColumnNumber = this.FileData.GetMappedColNumber(planogramNameMappingForImportType);
                
                HashSet<String> locationCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                HashSet<String> productGroupCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                
                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    //Build up collection of ids
                    LocationPlanAssignmentSearchCriteriaDto locationPlanAssignmentLink = new LocationPlanAssignmentSearchCriteriaDto()
                    {
                        LocationCode = Convert.ToString(row[locationCodeMappingColumnNumber], CultureInfo.InvariantCulture)?.Trim(),
                        ProductGroupCode = Convert.ToString(row[productGroupMappingColumnNumber], CultureInfo.InvariantCulture)?.Trim(),
                        PlanogramName = Convert.ToString(row[planogramMappingColumnNumber], CultureInfo.InvariantCulture)?.Trim()
                    };

                    if (locationPlanAssignmentLink.LocationCode != null && locationPlanAssignmentLink.ProductGroupCode != null
                            && locationPlanAssignmentLink.PlanogramName != null)
                    {
                        Int32 locationCodeMaxValue = Convert.ToInt32(locationCodeMappingForImportType.PropertyMax);

                        if (locationPlanAssignmentLink.LocationCode.Length > locationCodeMaxValue)
                        {
                            locationPlanAssignmentLink.LocationCode = locationPlanAssignmentLink.LocationCode.Substring(0, locationCodeMaxValue);
                        }

                        Int32 productGroupMaxValue = Convert.ToInt32(productGroupMappingForImportType.PropertyMax);

                        if (locationPlanAssignmentLink.ProductGroupCode.Length > productGroupMaxValue)
                        {
                            locationPlanAssignmentLink.ProductGroupCode = locationPlanAssignmentLink.ProductGroupCode.Substring(0, productGroupMaxValue);
                        }
                        
                        locationCodes.Add(locationPlanAssignmentLink.LocationCode);
                        productGroupCodes.Add(locationPlanAssignmentLink.ProductGroupCode);

                        //While looping also find duplicates
                        if (this.ValidationIdentifierFields.Contains(locationPlanAssignmentLink))
                        {
                            this.ValidationDuplicates.Add(locationPlanAssignmentLink);
                        }
                        else
                        {
                            this.ValidationIdentifierFields.Add(locationPlanAssignmentLink);
                        }
                    }
                }
                
                Dictionary<Int16, String> locationLookup = new Dictionary<Int16, String>();

                // now retrieve all locations that match the location codes
                // to inform the user of the import action that will take place
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    List<LocationInfoDto> locationInfoDtosIncludingDeleted = dal.FetchByEntityIdLocationCodes(
                        this.EntityId, locationCodes.Distinct()).ToList();

                    //Strip deleted items
                    List<LocationInfoDto> locationInfoDtos = locationInfoDtosIncludingDeleted.Where(l => l.DateDeleted == null).ToList();
                    locationLookup = locationInfoDtos.ToDictionary(p => p.Id, p => p.Code);

                    //Add codes to hashset for faster searching later on
                    locationInfoDtos.ForEach(p => this.ValidationParentLocationObjects.Add(p.Code));
                }

                Dictionary<Int32, String> productGroupIdLookupDictionary = new Dictionary<Int32, String>();

                //Add possible parent codes to hashset for faster searching later
                Int32 hierarchyId;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyId = dal.FetchByEntityId(this.EntityId).Id;
                }
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    List<ProductGroupDto> groups = dal.FetchByProductHierarchyId(hierarchyId).ToList();
                    foreach (ProductGroupDto group in groups)
                    {
                        ValidationParentProductGroupObjects.Add(group.Code);

                        if (!productGroupIdLookupDictionary.ContainsKey(group.Id))
                        {
                            productGroupIdLookupDictionary.Add(group.Id, group.Code);
                        }
                    }
                }

                using (IPlanogramInfoDal dal = dalContext.GetDal<IPlanogramInfoDal>())
                {
                    foreach (var planogramGroup in _planogramGroup.EnumerateAllChildGroups())
                    {
                        List<PlanogramInfoDto> planogramInfos = dal.FetchByPlanogramGroupId(planogramGroup.Id).ToList();

                        // get list of all planograms regardless of status
                        planogramInfos.ForEach(p => this.ValidationParentPlanogramStatusObjects.Add(p.Name));

                        if (_planogramAssignmentStatusType != PlanogramAssignmentStatusType.Any)
                        {
                            planogramInfos = planogramInfos.Where(p => p.Status == (Byte)_planogramAssignmentStatusType).ToList();
                        }

                        // get list of planograms of selected status
                        planogramInfos.ForEach(p => this.ValidationParentPlanogramObjects.Add(p.Name));
                    }
                }
                
                using (ILocationPlanAssignmentDal dal = dalContext.GetDal<ILocationPlanAssignmentDal>())
                {
                    List<LocationPlanAssignmentDto> planogramAssignments = new List<LocationPlanAssignmentDto>();

                    if (_productGroupInfo != null)
                    {
                        planogramAssignments = dal.FetchByProductGroupId(_productGroupInfo.Id).ToList();
                    }
                    else
                    {
                        planogramAssignments = dal.FetchByLocationId(_locationInfo.Id).ToList();
                    }

                    foreach(var planogramAssignment in planogramAssignments)
                    {
                        if (locationLookup.ContainsKey(planogramAssignment.LocationId) && productGroupIdLookupDictionary.ContainsKey(planogramAssignment.ProductGroupId))
                        {
                            LocationPlanAssignmentSearchCriteriaDto locationPlanAssignmentSearchCriteriaDto = new LocationPlanAssignmentSearchCriteriaDto(locationLookup[planogramAssignment.LocationId], productGroupIdLookupDictionary[planogramAssignment.ProductGroupId]);

                            if (!ValidationImportActionObjects.Contains(locationPlanAssignmentSearchCriteriaDto))
                            {
                                ValidationImportActionObjects.Add(locationPlanAssignmentSearchCriteriaDto);
                            }
                        }
                    }
                }  
            }

            #endregion

            //validate data against mappings using generic methods
            this.RunDataTypeValidation(FileData, this.ValidRows);

            //update datatable to include import action and excel row number columns
            this.AddExtraColumns(FileData);

            //set validated data to be updated data item and set it to be returned
            this.ValidatedData = FileData;

            Trace.TraceInformation("Validation of {0} Planogram Assignment records complete.", this.ValidRowsCount);

        }

        /// <summary>
        /// Override parent record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId)
            {
                //Check whether the location code matches a location in the database
                if (!this.ValidationParentLocationObjects.Contains(columnValue.ToString().Trim()))
                {
                    //no matching location code, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                        Message.DataManagement_Validation_NoMatchingLocation, ValidationErrorType.Error,
                        true, false, false, false, col, false);
                }
            }
            else if (col.PropertyIdentifier == PlanogramAssignmentImportMappingList.ProductGroupCodeMappingId)
            {
                //Check whether the product group code matches a product group in the database
                if (!this.ValidationParentProductGroupObjects.Contains(columnValue.ToString().Trim()))
                {
                    //no matching product GTIN, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                        Message.DataManagement_Validation_NoMatchingProductGroup, ValidationErrorType.Error,
                        true, false, false, false, col, false);
                }
            }
            else if (col.PropertyIdentifier == PlanogramAssignmentImportMappingList.PlanogramNameMappingId)
            {
                //Check whether the planogram matches a planogram in the database
                if (!this.ValidationParentPlanogramObjects.Contains(columnValue.ToString().Trim()))
                {
                    if (this.ValidationParentPlanogramStatusObjects.Contains(columnValue.ToString().Trim()))
                    {
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                        Message.DataManagement_Validation_NoMatchingPlanogramStatus, ValidationErrorType.Error,
                        true, false, false, false, col, false);
                    }
                    else
                    {
                        //no matching planogram, create error item
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_NoMatchingPlanogram, ValidationErrorType.Error,
                            true, false, false, false, col, false);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Overrride dupicate record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId)
            {
                String locationCode = columnValue.ToString();

                Int32 firstRowNumber = FileData.Rows[0].RowNumber;
                Int32 rowIndex = rowNumber - firstRowNumber;

                Int32 productGroupMappingColumnNumber = this.FileData.GetMappedColNumber(productGroupMappingForImportType);
                String productGroup = this.FileData.Rows[rowIndex][productGroupMappingColumnNumber].Value.ToString();

                Int32 planogramNameMappingColumnNumber = this.FileData.GetMappedColNumber(planogramNameMappingForImportType);
                String planogramName = this.FileData.Rows[rowIndex][planogramNameMappingColumnNumber].Value.ToString();

                LocationPlanAssignmentSearchCriteriaDto searchItem = new LocationPlanAssignmentSearchCriteriaDto()
                {
                    LocationCode = locationCode,
                    ProductGroupCode = productGroup,
                    PlanogramName = planogramName
                };

                //Check whether the link is a duplicate of another being imported
                if (this.ValidationDuplicates.Contains(searchItem))
                {
                    //Duplicate found, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                        locationCode, Message.DataManagement_Validation_DuplicatePlanogramAssignment,
                        ValidationErrorType.Error, true, false, false, false, col, true);
                }
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            Int32 locationCodeColNumber = this.FileData.GetMappedColNumber(locationCodeMappingForImportType);
            Int32 productGroupColNumber = this.FileData.GetMappedColNumber(productGroupMappingForImportType);

            if (locationCodeColNumber != 0 && productGroupColNumber != 0)
            {
                if (!rowData.IsIgnored)
                {
                    //Extract link from rowdata
                    LocationPlanAssignmentSearchCriteriaDto criteria =
                        new LocationPlanAssignmentSearchCriteriaDto()
                        {
                            LocationCode = rowData[locationCodeColNumber].Value.ToString(),
                            ProductGroupCode = rowData[productGroupColNumber].Value.ToString()
                        };

                    //Check whether the location/product link matches and existing link
                    if (!this.ValidationImportActionObjects.Contains(criteria))
                    {
                        //if the link is new, but a duplicate is also being inported the first record will
                        //be inserted, all others will be updates
                        this.ValidationImportActionObjects.Add(criteria);
                        return Message.DataManagement_ImportType_Add;
                    }
                    else
                    {
                        //otherwise it is and update
                        return Message.DataManagement_ImportType_Update;
                    }
                }
                else
                {
                    return Message.DataManagement_ImportType_Excluded;
                }
            }
            else
            {
                //[TODO] work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }

        #endregion
    }   
}