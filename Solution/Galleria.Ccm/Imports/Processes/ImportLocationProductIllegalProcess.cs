﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules.CommonRules;
using Csla.Rules;
using Galleria.Framework.Imports;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    [Serializable]
    public partial class ImportLocationProductIllegalProcess : ImportProcessBase<ImportLocationProductIllegalProcess>
    {
        #region Constructors
        public ImportLocationProductIllegalProcess(Int32 entityId, ImportFileData importData)
            : base(entityId, importData)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ImportLocationProductIllegalProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationProductIllegalProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationProductIllegalProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationProductIllegalData.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationProductIllegalProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
