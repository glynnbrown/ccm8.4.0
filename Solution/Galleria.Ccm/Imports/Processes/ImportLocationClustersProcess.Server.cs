﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#region Version History: (CCM 8.3.0)
// V8-31877 : N.Haywood
//  Added Product Group Code
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationClustersProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} location cluster schemes started.", this.ImportData.Rows.Count);

            ImportLocationClusters();

            Trace.TraceInformation("Import of {0} location cluster schemes complete.", this.ImportData.Rows.Count);
        }

        /// <summary>
        /// Does the import.
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportLocationClusters()
        {
            Int32 entityId = this.EntityId;

            //get a list of all mappings that need to be processed 
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            // get all the mappings for this import as we use
            // them throughout this process
            ImportMapping schemeNameMapping = this.GetColumnMapping(LocationClusterImportMappingList.SchemeNameMappingId);
            ImportMapping clusterNameMapping = this.GetColumnMapping(LocationClusterImportMappingList.ClusterNameMappingId);
            ImportMapping locationCodeMapping = this.GetColumnMapping(LocationClusterImportMappingList.LocationCodeMappingId);


            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //get the entity
                EntityDto currentEntity = null;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(entityId);
                }

                #region Create Lookup Lists

                // build our optimised location lookup
                IEnumerable<LocationInfoDto> locationDtoList;
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    locationDtoList = dal.FetchByEntityId(entityId);
                }
                Dictionary<string, int> locationLookup = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
                foreach (LocationInfoDto locationDto in locationDtoList)
                {
                    locationLookup.Add(locationDto.Code, locationDto.Id);
                }
                locationDtoList = null;

                Dictionary<String, Int32> productGroupLookup = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);
                ProductHierarchyDto hierarchyDto;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyDto = dal.FetchByEntityId(this.EntityId);
                }
                List<ProductGroupDto> productGroupDtoList = new List<ProductGroupDto>();
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    productGroupDtoList = dal.FetchByProductHierarchyId(hierarchyDto.Id).ToList();
                }
                foreach (ProductGroupDto productGroupDto in productGroupDtoList)
                {
                    productGroupLookup.Add(productGroupDto.Code, productGroupDto.Id);
                }
                productGroupDtoList = null;

                #endregion

                #region Upsert Clsuter Schemes & setup lookup list
                // setup collection to store all cluster schemes that need upserting
                ObservableCollection<ClusterSchemeDto> clusterSchemeDtoList = new ObservableCollection<ClusterSchemeDto>();
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    // SA-17728 - Refactored so that ucr could be set.
                    Int32 schemeNameColNumber = this.ImportData.GetMappedColNumber(schemeNameMapping);

                    List<String> distinctSchemeNames =
                        this.ImportData.Rows.Select(r => Convert.ToString(r[schemeNameColNumber].Value)).Distinct().ToList();


                    
                    
                    Dictionary<String, Int32> schemeNamesProductGroupDict = new Dictionary<String, Int32>();

                    if (resolvedMappings.Any(m => m.PropertyIdentifier == LocationClusterImportMappingList.ProductGroupCodeMappingId))
                    {
                        foreach (ImportFileDataRow row in this.ImportData.Rows)
                        {
                            String clusterName = "";
                            Int32 productGroupId = 0;

                            foreach (ImportMapping mapping in resolvedMappings)
                            {
                                Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                                Object cellValue = row[columnNumber].CurrentValue;

                                switch (mapping.PropertyIdentifier)
                                {
                                    case LocationClusterImportMappingList.ClusterNameMappingId:
                                        clusterName = cellValue.ToString();
                                        break;
                                    case LocationClusterImportMappingList.ProductGroupCodeMappingId:
                                        if (cellValue.ToString() != String.Empty && cellValue != DBNull.Value)
                                        {
                                            productGroupId = productGroupLookup[cellValue.ToString()];
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                            
                                schemeNamesProductGroupDict[clusterName] = productGroupId;
                            
                        }
                    }


                    foreach (String schemeName in distinctSchemeNames)
                    {
                        ClusterSchemeDto clusterSchemeDto =
                            clusterSchemeDtoList.FirstOrDefault(c => c.Name == schemeName &&
                                c.EntityId == entityId);

                        // create new dto & add to upsert list
                        if (clusterSchemeDto == null)
                        {
                            clusterSchemeDto = new ClusterSchemeDto();
                            clusterSchemeDto.EntityId = entityId;
                            clusterSchemeDto.Name = schemeName;
                            clusterSchemeDto.IsDefault = false;
                            clusterSchemeDto.UniqueContentReference = Guid.NewGuid();

                            if (resolvedMappings.Any(m => m.PropertyIdentifier == LocationClusterImportMappingList.ProductGroupCodeMappingId))
                            {
                                if (schemeNamesProductGroupDict.ContainsKey(schemeName))
                                {
                                    clusterSchemeDto.ProductGroupId = schemeNamesProductGroupDict[schemeName];
                                }
                            }

                            clusterSchemeDtoList.Add(clusterSchemeDto);
                        }
                    }

                    //pre 17728
                    //// now run through each row in the import table
                    //foreach (ImportFileDataRow row in this.ImportData.Rows)
                    //{
                    //    // create new dto
                    //    ClusterSchemeDto clusterSchemeDto = new ClusterSchemeDto();
                    //    clusterSchemeDto.EntityId = entityId;
                    //    clusterSchemeDto.Name = Convert.ToString(row[this.ImportData.GetMappedColNumber(schemeNameMapping)].Value);
                    //    clusterSchemeDto.IsDefault = false;

                    //    //Add to upsert list
                    //    if (!clusterSchemeDtoList.Contains(clusterSchemeDto))
                    //    {
                    //        clusterSchemeDtoList.Add(clusterSchemeDto);
                    //    }
                    //}

                    //Upsert the unqiue data
                    dal.Upsert(clusterSchemeDtoList, new ClusterSchemeIsSetDto(true));
                }

                //Setup dictionary
                Dictionary<string, int> clusterSchemeLookupList = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
                foreach (ClusterSchemeDto clusterScheme in clusterSchemeDtoList.Distinct())
                {
                    clusterSchemeLookupList.Add(clusterScheme.Name, clusterScheme.Id);
                }

                #endregion

                #region Upsert Clusters & setup lookup lists

                // setup collection to store all clusters that need 'upserting'
                ObservableCollection<ClusterDto> clusterDtoList = new ObservableCollection<ClusterDto>();
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    // now run through each row in the import table
                    foreach (ImportFileDataRow row in this.ImportData.Rows)
                    {
                        // create new dto
                        ClusterDto clusterDto = new ClusterDto();
                        clusterDto.ClusterSchemeId = clusterSchemeLookupList[Convert.ToString(row[this.ImportData.GetMappedColNumber(schemeNameMapping)].Value)];
                        clusterDto.Name = Convert.ToString(row[this.ImportData.GetMappedColNumber(clusterNameMapping)].Value);

                        //Add to upsert list
                        if (!clusterDtoList.Contains(clusterDto))
                        {
                            clusterDtoList.Add(clusterDto);
                        }
                    }

                    //Upsert the unqiue data
                    dal.Upsert(clusterDtoList, new ClusterIsSetDto(true));
                }

                //Setup cluster dictionary
                Dictionary<ClusterDtoKey, int> clusterLookupList = new Dictionary<ClusterDtoKey, int>();
                foreach (ClusterDto cluster in clusterDtoList.Distinct())
                {
                    //Create criteria
                    ClusterDtoKey criteria = new ClusterDtoKey()
                    {
                        Name = cluster.Name,
                        ClusterSchemeId = cluster.ClusterSchemeId
                    };
                    //Add to lookup list
                    clusterLookupList.Add(criteria, cluster.Id);
                }

                #endregion

                #region Upsert Cluster Locations

                // setup collection to store all cluster locations that need 'upserting'
                ObservableCollection<ClusterLocationDto> clusterLocationDtoList = new ObservableCollection<ClusterLocationDto>();
                using (IClusterLocationDal dal = dalContext.GetDal<IClusterLocationDal>())
                {
                    // now run through each row in the import table
                    foreach (ImportFileDataRow row in this.ImportData.Rows)
                    {
                        // create new dto
                        ClusterLocationDto clusterLocationDto = new ClusterLocationDto();

                        //Find location id
                        clusterLocationDto.LocationId = Convert.ToInt16(locationLookup[Convert.ToString(row[this.ImportData.GetMappedColNumber(locationCodeMapping)].Value)]);

                        //Setup criteria to find cluster id
                        ClusterDtoKey criteria = new ClusterDtoKey()
                        {
                            Name = Convert.ToString(row[this.ImportData.GetMappedColNumber(clusterNameMapping)].Value),
                            ClusterSchemeId = clusterSchemeLookupList[Convert.ToString(row[this.ImportData.GetMappedColNumber(schemeNameMapping)].Value)]
                        };
                        //Set cluster id
                        clusterLocationDto.ClusterId = clusterLookupList[criteria];

                        //Add to upsert list
                        if (!clusterLocationDtoList.Contains(clusterLocationDto))
                        {
                            clusterLocationDtoList.Add(clusterLocationDto);
                        }
                    }
                    //Upsert the unqiue data
                    dal.Upsert(clusterLocationDtoList, new ClusterLocationIsSetDto(true));
                }

                #endregion

                dalContext.Commit();
            }
        }

        #endregion
    }
}
