﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Ccm.Dal.Interfaces;
using System.Globalization;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationSpaceBayProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} location space bays started.", this.ImportData.Rows.Count);

            ImportLocationSpaceBays();

            Trace.TraceInformation("Import of {0} location space bays complete.", this.ImportData.Rows.Count);
        }

        /// <summary>
        /// Main Import
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportLocationSpaceBays()
        {
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);
            Dictionary<String, List<LocationSpaceBayDto>> locationSpaceBayDetails = new Dictionary<String, List<LocationSpaceBayDto>>();

            List<LocationSpaceBayDto> locationSpaceBays = new List<LocationSpaceBayDto>();
            LocationSpaceBayDto locationSpaceBay;

            #region Get lookups - Location Space \ Product Group

            Dictionary<Int16, Int32> locationSpaceLookupList = new Dictionary<Int16, Int32>();
            Dictionary<String, LocationInfoDto> locationLookup = new Dictionary<String, LocationInfoDto>(StringComparer.OrdinalIgnoreCase);
            Dictionary<String, Int32> productGroupLookup = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                // build our optimised location lookup
                IEnumerable<LocationInfoDto> locationInfoDtoList;
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    locationInfoDtoList = dal.FetchByEntityId(this.EntityId);
                }

                foreach (LocationInfoDto locationInfoDto in locationInfoDtoList)
                {
                    locationLookup.Add(locationInfoDto.Code, locationInfoDto);
                }
                locationInfoDtoList = null;

                List<LocationSpaceSearchCriteriaDto> searchCriteriaList;
                using (ILocationSpaceSearchCriteriaDal dal = dalContext.GetDal<ILocationSpaceSearchCriteriaDal>())
                {
                    searchCriteriaList = dal.FetchByEntityId(this.EntityId).ToList();
                }

                foreach (LocationSpaceSearchCriteriaDto locationSpaceSearchCriteria in searchCriteriaList)
                {
                    Int16 locationId = locationLookup[locationSpaceSearchCriteria.LocationCode].Id;

                    if (!locationSpaceLookupList.ContainsKey(locationId))
                    {
                        locationSpaceLookupList.Add(locationId, locationSpaceSearchCriteria.LocationSpaceId);
                    }
                }
                searchCriteriaList = null;

                Int32 hierarchyId;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyId = dal.FetchByEntityId(this.EntityId).Id;
                }
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    List<ProductGroupDto> groups = dal.FetchByProductHierarchyId(hierarchyId).ToList();

                    //Add codes to hashset for faster searching later on
                    groups.ForEach(p => productGroupLookup.Add(p.Code, p.Id));

                    groups = null;
                }
            }

            #endregion

            #region load location space bays

            foreach (ImportFileDataRow row in this.ImportData.Rows)
            {
                String locationCode = String.Empty;
                String productGroupCode = String.Empty;

                locationSpaceBay = new LocationSpaceBayDto();

                //loop through other resolved mappings
                IFormatProvider prov = CultureInfo.InvariantCulture;
                foreach (ImportMapping mapping in resolvedMappings)
                {
                    Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                    Object cellValue = row[columnNumber].Value;

                    //Correct null value checks
                    cellValue = ProcessHelpers.CheckNullCellValues(mapping, cellValue);

                    if (mapping.PropertyIdentifier == LocationSpaceBayImportMappingList.LocationCodeMappingId)
                    {
                        locationCode = cellValue.ToString();
                    }
                    else if (mapping.PropertyIdentifier == LocationSpaceBayImportMappingList.ProductGroupCodeMappingId)
                    {
                        productGroupCode = cellValue.ToString();
                    }
                    else
                    {
                        //use the mapping list method to set the value
                        LocationSpaceBayImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, locationSpaceBay);
                    }
                }

                // use to hold reference to product group id
                locationSpaceBay.LocationSpaceProductGroupId = productGroupLookup[productGroupCode];

                locationSpaceBayDetails.TryGetValue(locationCode, out locationSpaceBays);

                if (locationSpaceBays != null)
                {
                    // add to current location space detail
                    locationSpaceBays.Add(locationSpaceBay);
                }
                else
                {
                    // create new location space detail
                    locationSpaceBays = new List<LocationSpaceBayDto>();
                    locationSpaceBays.Add(locationSpaceBay);
                }
                locationSpaceBayDetails[locationCode] = locationSpaceBays;
            }

            #endregion

            #region import

            foreach (String location in locationSpaceBayDetails.Keys)
            {
                LocationSpace locationSpace = LocationSpace.GetLocationSpaceById(locationSpaceLookupList[locationLookup[location].Id]);
                List<LocationSpaceBayDto> bays = locationSpaceBayDetails[location];

                foreach (Int32 productGroupId in bays.Select(p => p.LocationSpaceProductGroupId).Distinct())
                {
                    LocationSpaceProductGroup locationSpaceProductGroup = locationSpace.LocationSpaceProductGroups.Where(p => p.ProductGroupId == productGroupId).FirstOrDefault();
                    locationSpaceProductGroup.Bays.Clear();

                    List<LocationSpaceBayDto> categoryBays = bays.Where(p => p.LocationSpaceProductGroupId == productGroupId).ToList();

                    foreach (LocationSpaceBayDto locationSpacebayDto in categoryBays)
                    {
                        LocationSpaceBay bay = LocationSpaceBay.NewLocationSpaceBay(false);
                        locationSpaceProductGroup.Bays.Add(bay);

                        UpdateLocationSpaceBay(bay, locationSpacebayDto);
                    }
                }

                locationSpace.Save();
            }

            #endregion

            locationSpaceBayDetails.Clear();
        }

        private void UpdateLocationSpaceBay(LocationSpaceBay bay, LocationSpaceBayDto locationSpacebayDto)
        {
            bay.Order = locationSpacebayDto.Order;
            bay.Height = locationSpacebayDto.Height;
            bay.Width = locationSpacebayDto.Width;
            bay.Depth = locationSpacebayDto.Depth;
            bay.BaseHeight = locationSpacebayDto.BaseHeight;
            bay.BaseWidth = locationSpacebayDto.BaseWidth;
            bay.BaseDepth = locationSpacebayDto.BaseDepth;
            bay.IsAisleStart = locationSpacebayDto.IsAisleStart;
            bay.IsAisleEnd = locationSpacebayDto.IsAisleEnd;
            bay.IsAisleRight = locationSpacebayDto.IsAisleRight;
            bay.IsAisleLeft = locationSpacebayDto.IsAisleLeft;
            bay.BayLocationRef = locationSpacebayDto.BayLocationRef;
            bay.BayFloorDrawingNo = locationSpacebayDto.BayFloorDrawingNo;
            bay.FixtureName = locationSpacebayDto.FixtureName;
            bay.Manufacturer = locationSpacebayDto.ManufacturerName;
            bay.BayType = (LocationSpaceBayType)locationSpacebayDto.BayType;
            bay.FixtureType = (LocationSpaceFixtureType)locationSpacebayDto.FixtureType;
            bay.BayTypePostFix = locationSpacebayDto.BayTypePostFix;
            bay.BayTypeCalculation = locationSpacebayDto.BayTypeCalculation;
            bay.NotchPitch = locationSpacebayDto.NotchPitch;
            bay.Barcode = locationSpacebayDto.Barcode;
            bay.Colour = locationSpacebayDto.Colour;
            bay.Asset = locationSpacebayDto.AssetNumber;
            bay.Temperature = locationSpacebayDto.Temperature;
            bay.IsPromotional = locationSpacebayDto.IsPromotional;
            bay.HasPower = locationSpacebayDto.HasPower;
            bay.NotchStartY = locationSpacebayDto.NotchStartY;
            bay.FixtureShapeType = (LocationSpaceFixtureShapeType)locationSpacebayDto.FixtureShape;
        }

        #endregion
    }
}
