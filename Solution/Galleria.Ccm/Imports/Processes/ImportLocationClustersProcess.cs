﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Csla.Rules.CommonRules;
using Csla.Rules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Import class for Location Clusters
    /// </summary>
    [Serializable]
    public sealed partial class ImportLocationClustersProcess : ImportProcessBase<ImportLocationClustersProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ImportLocationClustersProcess(Int32 entityId, ImportFileData importData) :
            base(entityId, importData)
        {
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ImportLocationClustersProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationClustersProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationClustersProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationClusterData.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationClustersProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
