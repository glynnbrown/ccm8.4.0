﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationHierarchyProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} location hierarchy started.", this.ValidRowsCount);

            ImportLocationHierarchy();

            Trace.TraceInformation("Import of {0} location hierarchy complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Does the import.
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportLocationHierarchy()
        {
            LocationHierarchyImportMappingList mappingList = (LocationHierarchyImportMappingList)this.ImportData.MappingList;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //get the entity
                EntityDto currentEntity = null;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(this.EntityId);
                }

                LocationHierarchyDto currentHierarchy = null;
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    currentHierarchy = dal.FetchByEntityId(this.EntityId);
                }

                //get the existing hierarchy levels
                List<LocationLevelDto> hierarchyLevelList = new List<LocationLevelDto>();
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    List<LocationLevelDto> fetchedLevels = dal.FetchByLocationHierarchyId(currentHierarchy.Id).ToList();

                    //order the levels
                    LocationLevelDto rootDto = fetchedLevels.FirstOrDefault(d => d.ParentLevelId == null);
                    Int32 lastLevelId = rootDto.Id;
                    while (true)
                    {
                        LocationLevelDto nextDto = fetchedLevels.FirstOrDefault(l => l.ParentLevelId == lastLevelId);
                        if (nextDto != null)
                        {
                            hierarchyLevelList.Add(nextDto);
                            lastLevelId = nextDto.Id;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                //get the existing hierarchy groups
                List<LocationGroupDto> hierarchyExistingGroupsList;
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    hierarchyExistingGroupsList =  dal.FetchByLocationHierarchyId(currentHierarchy.Id).ToList();
                }

                LocationGroupDto rootGroup = hierarchyExistingGroupsList.First(g => g.ParentGroupId == null);

                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    //cycle through rows
                    List<LocationGroupDto> newDtosList = new List<LocationGroupDto>();
                    foreach (ImportFileDataRow row in this.ValidRows)
                    {
                        //start at the top level of the row and check units down the tree
                        LocationGroupDto currentGroup = rootGroup;

                        //cycle through the mappings
                        Int32 levelNum = 0;
                        for (Int32 i = 0; i < mappingList.Count; i += 2)
                        {
                            LocationLevelDto currentLevel = hierarchyLevelList[levelNum];

                            ImportMapping codeMapping = mappingList[i];
                            ImportMapping nameMapping = mappingList[i + 1];

                            String code = row[this.ImportData.GetMappedColNumber(codeMapping)].CurrentValue as String;
                            String name = row[this.ImportData.GetMappedColNumber(nameMapping)].CurrentValue as String;
                            if (name == null && nameMapping.PropertyType == typeof(System.String))
                            {
                                name = String.Empty; // where cast as string returns null, provide String.Empty
                            }

                            if (!String.IsNullOrEmpty(code))
                            {
                                String codeLookup = code.ToLowerInvariant();

                                //attempt to get the existing unit by the code
                                Int32 parentId = currentGroup.Id;
                                LocationGroupDto group =
                                    hierarchyExistingGroupsList
                                    .FirstOrDefault(g =>
                                        //g.ParentId == parentId &&
                                        g.LocationLevelId == currentLevel.Id &&
                                        g.Code.ToLowerInvariant() == codeLookup);

                                if (group == null)
                                {
                                    group = new LocationGroupDto();
                                    group.Code = code;
                                    group.Name = name;
                                    group.ParentGroupId = parentId;
                                    group.LocationHierarchyId = currentHierarchy.Id;
                                    group.LocationLevelId = currentLevel.Id;
                                    group.DateCreated = DateTime.UtcNow;
                                    group.DateLastModified = DateTime.UtcNow;

                                    //insert immediately as we will need the id - 
                                    //not sure how to get round this :S
                                    dal.Insert(group);

                                    hierarchyExistingGroupsList.Add(group);
                                }
                                group.Name = name;
                                group.ParentGroupId = parentId;
                                group.LocationLevelId = currentLevel.Id;

                                //set as the new parent
                                currentGroup = group;

                            }
                            //increment the level
                            levelNum++;
                        }
                    }

                    //save down all the group dtos
                    if (hierarchyExistingGroupsList.Count > 0)
                    {
                        dal.Upsert(hierarchyExistingGroupsList, new LocationGroupIsSetDto(true));
                    }
                }


                //Update the assigned location group id of any affected locations.
                //This is usually required when a new location group has been added as a
                // child of one which already has locations.
                CorrectLocationAssignments(dalContext, currentHierarchy.Id);


                dalContext.Commit();
            }
        }

        /// <summary>
        /// Corrects the assignments of locations to location groups
        /// ensuring that locations are only assigned to leaf group nodes.
        /// </summary>
        [ObfuscateControlFlow]
        private void CorrectLocationAssignments(IDalContext dalContext, Int32 hierarchyId)
        {
            //reget the final groups list for the hierarchy
            List<LocationGroupDto> groupDtos;
            List<Int32> validGroupIds = new List<Int32>();
            List<LocationGroupDto> invalidGroupDtos = new List<LocationGroupDto>();
            using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
            {
                groupDtos = dal.FetchByLocationHierarchyId(hierarchyId).ToList();

                foreach (LocationGroupDto dto in groupDtos)
                {
                    Int32 id = dto.Id;
                    if (groupDtos.Any(g => g.ParentGroupId == id))
                    {
                        //not a leaf group so is invalid
                        // for location assignment but has locations assigned.
                        if (dto.AssignedLocationsCount > 0)
                        {
                            invalidGroupDtos.Add(dto);
                        }
                    }
                    else
                    {
                        validGroupIds.Add(id);
                    }
                }
            }

            //if we have invalid groups and valid groups that locations can be moved to.
            if (invalidGroupDtos.Count > 0 && validGroupIds.Count > 0)
            {
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    List<LocationDto> locsToUpdate = new List<LocationDto>();

                    foreach (LocationGroupDto invalidGroupDto in invalidGroupDtos)
                    {
                        //get the locations to be moved
                        List<LocationDto> groupLocations = dal.FetchByLocationGroupId(invalidGroupDto.Id).ToList();
                        if (groupLocations.Count > 0)
                        {
                            //determine a valid group id to reassign to -
                            //the first available leaf location group child
                            LocationGroupDto validChildDto = invalidGroupDto;
                            while (validChildDto != null && !validGroupIds.Contains(validChildDto.Id))
                            {
                                validChildDto = groupDtos.FirstOrDefault(g => g.ParentGroupId == validChildDto.Id);
                            }

                            if (validChildDto != null
                                && validChildDto.Id != 0)
                            {
                                foreach (LocationDto locDto in groupLocations)
                                {
                                    locDto.LocationGroupId = validChildDto.Id;
                                    locsToUpdate.Add(locDto);
                                }
                            }
                        }
                        groupLocations = null;
                    }

                    //bulk upsert the assignment changes
                    if (locsToUpdate.Count > 0)
                    {
                        LocationIsSetDto isSetDto = new LocationIsSetDto(/*initialSet*/false);
                        isSetDto.IsLocationGroupIdSet = true;
                        dal.Upsert(locsToUpdate, isSetDto, false);
                    }
                    locsToUpdate = null;
                }
            }

            groupDtos = null;
            validGroupIds = null;
            invalidGroupDtos = null;
        }

        #endregion
    }
}
