﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules.CommonRules;
using Galleria.Framework.Imports;
using Csla.Rules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management LocationProductIllegal validation process class
    /// </summary>
    [Serializable]
    public partial class ValidateLocationProductIllegalProcess : ValidateProcessBase<ValidateLocationProductIllegalProcess>
    {
        #region Constructors
        public ValidateLocationProductIllegalProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstRow)
            : base(entityId, fileData, startingRowNumber, headersInFirstRow)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidateLocationProductIllegalProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationProductIllegalProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationProductIllegalProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationProductIllegalData.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationProductIllegalProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
