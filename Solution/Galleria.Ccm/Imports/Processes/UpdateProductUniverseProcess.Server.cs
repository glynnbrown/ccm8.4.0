﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SmartAssembly.Attributes;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class UpdateProductUniverseProcess
    {
        /// <summary>
        /// Called when executing this process
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                {
                    dal.UpdateProductUniverses(this.EntityId);

                }
            }
        }
    }
}
