﻿using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Imports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Import class for Location Space
    /// </summary>
    [Serializable]
    public sealed partial class ImportPlanogramAssignmentProcess : ImportProcessBase<ImportPlanogramAssignmentProcess>
    {
        private PlanogramGroup _planogramGroup;
        private ProductGroupInfo _productGroupInfo;
        private LocationInfo _locationInfo;
        private PlanogramAssignmentStatusType _planogramAssignmentStatusType;

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ImportPlanogramAssignmentProcess(Int32 entityId, PlanogramGroup planogramGroup, ProductGroupInfo productGroupInfo, 
                                    LocationInfo locationInfo, PlanogramAssignmentStatusType planogramAssignmentStatusType, ImportFileData importData) :
            base(entityId, importData)
        {
            _planogramGroup = planogramGroup;
            _productGroupInfo = productGroupInfo;
            _locationInfo = locationInfo;
            _planogramAssignmentStatusType = planogramAssignmentStatusType;
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ImportPlanogramAssignmentProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportPlanogramAssignmentProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportPlanogramAssignmentProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationData.ToString()));
            BusinessRules.AddRule(typeof(ImportPlanogramAssignmentProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
