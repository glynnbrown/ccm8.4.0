﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// GFS-25455 :J.Pickup
//  Copied over from GFS
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
#endregion
#endregion


using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;

using Csla;

using SmartAssembly.Attributes;

using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Logging;

using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management Assortment validation process class
    /// </summary>
    public partial class ValidateAssortmentProcess
    {
        #region Fields

        private HashSet<String> _validationParentProductGroupObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentProductObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<AssortmentSearchCriteria> _validationImportActionObjects = new HashSet<AssortmentSearchCriteria>();
        private HashSet<AssortmentSearchCriteria> _validationIdentifierFields = new HashSet<AssortmentSearchCriteria>();
        private HashSet<AssortmentSearchCriteria> _validationDuplicates = new HashSet<AssortmentSearchCriteria>();
        private HashSet<Tuple<String, String>> _validationCustomIdentyfingDataDatabaseIssues = new HashSet<Tuple<String, String>>();
        private HashSet<Tuple<String, String>> _validationCustomIdentyfingDataFileIssues = new HashSet<Tuple<String, String>>();

        private String _validationDuplicateRecordIdentifierName = String.Empty;
        private String _validationCustomIdentygingDataIssueIdentifierName = String.Empty;

        #endregion

        #region Properties

        /// <summary>
        /// Get/Set Validation Product Group Parent Objects
        /// </summary>
        public HashSet<String> ValidationParentProductGroupObjects
        {
            get { return _validationParentProductGroupObjects; }
            set { _validationParentProductGroupObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Product Parent Objects
        /// </summary>
        public HashSet<String> ValidationParentProductObjects
        {
            get { return _validationParentProductObjects; }
            set { _validationParentProductObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Import Action Objects
        /// </summary>
        public HashSet<AssortmentSearchCriteria> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Identifier Fields
        /// </summary>
        public HashSet<AssortmentSearchCriteria> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }
        /// <summary>
        /// Get/Set Validation Duplicates
        /// </summary>
        public HashSet<AssortmentSearchCriteria> ValidationDuplicates
        {
            get { return _validationDuplicates; }
            set { _validationDuplicates = value; }
        }

        /// <summary>
        /// Get/Set Validation Duplicate Content Name 
        /// </summary>
        public HashSet<Tuple<String, String>> ValidationCustomIdentyfingDataDatabase
        {
            get { return _validationCustomIdentyfingDataDatabaseIssues; }
            set { _validationCustomIdentyfingDataDatabaseIssues = value; }
        }

        /// <summary>
        /// Get/Set Validation Duplicate Content Name 
        /// </summary>
        public HashSet<Tuple<String, String>> ValidationCustomIdentyfingDataFile
        {
            get { return _validationCustomIdentyfingDataFileIssues; }
            set { _validationCustomIdentyfingDataFileIssues = value; }
        }



        /// Local property for the mappings
        /// </summary>
        private ImportMapping assortmentNameMappingForImportType;
        private ImportMapping productGroupMappingForImportType;
        private ImportMapping productMappingForImportType;

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation Started.");

            // set assortment name mapping
            assortmentNameMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == AssortmentImportMappingList.AssortmentNameMapId).FirstOrDefault();
            //Set Product group mapping
            productGroupMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == AssortmentImportMappingList.ProductGroupCodeMapId).FirstOrDefault();
            //Set Product mapping
            productMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == AssortmentImportMappingList.ProductGTINMapId).FirstOrDefault();

            //set duplicate record identifier columns name
            _validationDuplicateRecordIdentifierName = String.Format("{0} - {1} - {2}",
                assortmentNameMappingForImportType.ColumnReference, productGroupMappingForImportType.ColumnReference,
                productMappingForImportType.ColumnReference);
            _validationCustomIdentygingDataIssueIdentifierName = String.Format("{0} - {1}",
                assortmentNameMappingForImportType.ColumnReference, productGroupMappingForImportType.ColumnReference);

            //setup datatable
            Trace.TraceInformation("{0} Assortment Records being validated", this.ValidRowsCount);

            #region Fetch Relational Check Objects

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // start a transaction for this data validation
                dalContext.Begin();
                //Get mappings
                Int32 assortmentNameMappingColumnNumber = this.FileData.GetMappedColNumber(assortmentNameMappingForImportType);
                Int32 productGroupCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productGroupMappingForImportType);
                Int32 productCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productMappingForImportType);
                //Get current entity id
                Int32 entityId = this.EntityId;

                //Create lists to store codes
                HashSet<String> locationCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                HashSet<String> productCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    //Build up collections of details
                    String assortmentName = Convert.ToString(row[assortmentNameMappingColumnNumber], CultureInfo.InvariantCulture);
                    String productGroupCode = Convert.ToString(row[productGroupCodeMappingColumnNumber], CultureInfo.InvariantCulture);
                    String productCode = Convert.ToString(row[productCodeMappingColumnNumber], CultureInfo.InvariantCulture);

                    if ((assortmentName != null && productGroupCode != null && productCode != null))
                    {
                        productCodes.Add(productCode);

                        //Constructor criteria
                        AssortmentSearchCriteria criteria = new AssortmentSearchCriteria()
                        {
                            AssortmentName = assortmentName,
                            ProductGroupCode = productGroupCode,
                            ProductCode = productCode
                        };

                        //[ISO-13387] While we're looping here anyway, we might as well go ahead and find duplicates.
                        if (this.ValidationIdentifierFields.Contains(criteria))
                        {
                            this.ValidationDuplicates.Add(criteria);
                        }
                        else
                        {
                            this.ValidationIdentifierFields.Add(criteria);
                        }
                    }
                }

                //get the existing hierarchy groups
                Int32 hierarchyId;

                //Add possible parent codes to hashset for faster searching later
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyId = dal.FetchByEntityId(entityId).Id;
                }
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    List<ProductGroupDto> groups = dal.FetchByProductHierarchyId(hierarchyId).ToList();
                    foreach (ProductGroupDto group in groups)
                    {
                        ValidationParentProductGroupObjects.Add(group.Code);
                    }
                }

                //Add possible parent codes to hashset for faster searching later
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    List<ProductInfoDto> allProductInfoDtos = dal.FetchByEntityIdProductGtins(
                                            entityId, productCodes.Distinct()).ToList();

                    List<ProductInfoDto> productInfoDtos = allProductInfoDtos.ToList();

                    //Add codes to hashset for faster searching later on
                    productInfoDtos.ForEach(p => this.ValidationParentProductObjects.Add(p.Gtin));
                }
            }
            #endregion

            //validate data against mappings using generic methods
            this.RunDataTypeValidation(this.FileData, this.ValidRows);

            //update datatable to include import action and excel row number columns
            this.AddExtraColumns(this.FileData);

            //set validated data to be updated data item and set it to be returned
            this.ValidatedData = this.FileData;

            Trace.TraceInformation("Validation of {0} Assortment records complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Override parent record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {

            switch (col.PropertyIdentifier)
            {
                case AssortmentImportMappingList.ProductGTINMapId:
                    //Check whether the product code matches a product in the database
                    if (!this.ValidationParentProductObjects.Contains(columnValue.ToString()))
                    {
                        //no matching product GTIN, create error item
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_NoMatchingProduct, ValidationErrorType.Error,
                            true, false, false, false, col, false);
                    }
                    break;

                case AssortmentImportMappingList.ProductGroupCodeMapId:
                    //Check whether the product group code matches a product group in the database
                    if (!this.ValidationParentProductGroupObjects.Contains(columnValue.ToString()))
                    {
                        //no matching product group, create error item
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                            Message.DataManagement_Validation_NoMatchingProductGroup, ValidationErrorType.Error,
                            true, false, false, false, col, false);
                    }
                    break;
            }

            return null;
        }

        /// <summary>
        /// Overrride dupicate record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == AssortmentImportMappingList.AssortmentNameMapId)
            {
                String assortmentName = columnValue.ToString();

                //Calculate row index
                Int32 firstRowNumber = FileData.Rows[0].RowNumber;
                Int32 rowIndex = rowNumber - firstRowNumber;

                //Construct unique data group
                Int32 productGroupCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productGroupMappingForImportType);
                Int32 productCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productMappingForImportType);

                String productGroupCode = this.FileData.Rows[rowIndex][productGroupCodeMappingColumnNumber].Value.ToString();

                String productCode = String.Empty;
                object tempCodeValue = this.FileData.Rows[rowIndex][productCodeMappingColumnNumber].Value;
                if (tempCodeValue != null)
                {
                    productCode = tempCodeValue.ToString();
                }

                //Construct criteria
                AssortmentSearchCriteria searchItem = new AssortmentSearchCriteria()
                {
                    AssortmentName = assortmentName,
                    ProductGroupCode = productGroupCode,
                    ProductCode = productCode
                };

                //Check whether the assortment record is a duplicate of another being imported
                if (this.ValidationDuplicates.Contains(searchItem))
                {
                    //Duplicate found, create error item
                    ValidationErrorItem validationErrorItem = ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                         assortmentName, Message.DataManagement_Validation_DuplicateAssortment,
                         ValidationErrorType.Error, true, false, false, false, col, true);

                    //update column name to contain full search criteria
                    validationErrorItem.ColumnName = _validationDuplicateRecordIdentifierName;

                    //return updated error item
                    return validationErrorItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Override duplicate record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunCustomIdentifyingDataValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == ProductUniverseImportMappingList.ProductUniverseNameMappingId)
            {
                //find  content name and product group code
                String assortmentName = columnValue.ToString();
                Int32 firstRowNumber = FileData.Rows[0].RowNumber;
                Int32 rowIndex = rowNumber - firstRowNumber;
                Int32 productGroupCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productGroupMappingForImportType);
                String productGroupCode = this.FileData.Rows[rowIndex][productGroupCodeMappingColumnNumber].Value.ToString();

                //Check whether the assortment is already assigned to a different product group
                if (this.ValidationCustomIdentyfingDataDatabase.Any(c => c.Item1 == assortmentName
                    && c.Item2 != productGroupCode))
                {
                    String problemDescription = String.Format(Message.DataManagement_Validation_ExistingProductUniverseAssginedToDifferentProductGroup,
                        Message.ContentManagement_Assortment.ToLowerInvariant());

                    //match found, create warning
                    ValidationErrorItem validationErrorItem = ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                         assortmentName, problemDescription, ValidationErrorType.Warning, true, false, false, false, col, false);

                    //update column name to contain full search criteria
                    validationErrorItem.ColumnName = _validationCustomIdentygingDataIssueIdentifierName;

                    //return updated error item
                    return validationErrorItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Override duplicate record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunCustomIdentifyingDataFileValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == ProductUniverseImportMappingList.ProductUniverseNameMappingId)
            {
                //find  content name and product group code
                String assortmentName = columnValue.ToString();
                Int32 firstRowNumber = FileData.Rows[0].RowNumber;
                Int32 rowIndex = rowNumber - firstRowNumber;
                Int32 productGroupCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productGroupMappingForImportType);
                String productGroupCode = this.FileData.Rows[rowIndex][productGroupCodeMappingColumnNumber].Value.ToString();

                //Check whether the assortment is already assigned to a different product group
                if (this.ValidationCustomIdentyfingDataFile.Any(c => c.Item1 == assortmentName
                    && c.Item2 != productGroupCode))
                {
                    String problemDescription = String.Format(Message.DataManagement_Validation_ExistingProductUniverseAssginedToDifferentProductGroupFile,
                        Message.ContentManagement_Assortment.ToLowerInvariant());

                    //match found, create warning
                    ValidationErrorItem validationErrorItem = ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                         assortmentName, problemDescription, ValidationErrorType.Warning, true, false, false, false, col, false);

                    //update column name to contain full search criteria
                    validationErrorItem.ColumnName = _validationCustomIdentygingDataIssueIdentifierName;

                    //return updated error item
                    return validationErrorItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            Int32 assortmentNameColNumber = this.FileData.GetMappedColNumber(assortmentNameMappingForImportType);
            Int32 productGroupCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productGroupMappingForImportType);
            Int32 productCodeMappingColumnNumber = this.FileData.GetMappedColNumber(productMappingForImportType);

            if ((assortmentNameColNumber != 0 && productGroupCodeMappingColumnNumber != 0 && productCodeMappingColumnNumber != 0))
            {
                //Extract link from rowdata
                AssortmentSearchCriteria criteria =
                    new AssortmentSearchCriteria()
                    {
                        AssortmentName = rowData[assortmentNameColNumber].Value.ToString(),
                        ProductGroupCode = rowData[productGroupCodeMappingColumnNumber].Value.ToString(),
                        ProductCode = rowData[productCodeMappingColumnNumber].Value.ToString()
                    };

                //Check whether the assortment link matches and existing link
                if (!this.ValidationImportActionObjects.Contains(criteria))
                {
                    //if the link is new, but a duplicate is also being inported the first record will
                    //be inserted, all others will be updates
                    this.ValidationImportActionObjects.Add(criteria);
                    return Message.DataManagement_ImportType_Add;
                }
                else
                {
                    //otherwise it is and update
                    return Message.DataManagement_ImportType_Update;
                }
            }
            else
            {
                //[TODO] work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }

        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunEnumLookupValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (columnValue != null)
            {
                switch (col.PropertyIdentifier)
                {
                    case AssortmentImportMappingList.ProductTreatmentTypeMapId:
                        //check if an enum exists for the friendly name entered
                        //using lower invariant to allow all case variants to import correctly
                        PlanogramAssortmentProductTreatmentType treatmentType;
                        if (!PlanogramAssortmentProductTreatmentTypeHelper.EnumFromFriendlyName.TryGetValue(columnValue.ToString().ToLowerInvariant(), out treatmentType))
                        {
                            if (col.PropertyIsUniqueIdentifier)
                            {
                                //Friendly name incorrect return error
                                return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                                    columnValue.ToString(), Message.DataManagement_Validation_InvalidAssortmentProductTreatmentType,
                                    ValidationErrorType.Error, true, false, false, false, col, false);
                            }
                            else
                            {
                                //Friendly name incorrect return error
                                return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                                    columnValue.ToString(), Message.DataManagement_Validation_InvalidAssortmentProductTreatmentType,
                                    ValidationErrorType.Error, true, true, false, false, col, false);
                            }
                        }
                        return null;
                    default:
                        return null;
                }
            }
            return null;
        }

        #endregion
    }
}