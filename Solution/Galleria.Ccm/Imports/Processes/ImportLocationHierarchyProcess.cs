﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;


namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Import class for Location Hierarchy
    /// </summary>
    [Serializable]
    public partial class ImportLocationHierarchyProcess : ImportProcessBase<ImportLocationHierarchyProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ImportLocationHierarchyProcess(Int32 entityId, ImportFileData importData) :
            base(entityId, importData)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ImportLocationHierarchyProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationHierarchyProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationHierarchyProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationHierarchyData.ToString()));
            BusinessRules.AddRule(typeof(ImportLocationHierarchyProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
