﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25452 : N.Haywood
//	Added from SA
// V8-26333 : A.Probyn
//  Extended to be include with mapping list changes for custom attributes. Temporary change made, needs updating in
//  ticket 28470 to use required custom attribute fetch method.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Helpers;
using System.Diagnostics;
using Aspose.Cells;
using SmartAssembly.Attributes;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportProductsProcess
    {
        #region Methods
        /// <summary>
        /// Called when executing this process
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Int32 entityId = this.EntityId;

            // get the mapping list of this export type
            IImportMappingList mappingList = ProductImportMappingList.NewProductImportMappingList(true);


            // create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }


            // now write the data if required
            if (this.ExportHeadersOnly)
            {
                //Apply a text style format to each column
                Style textDataTypeStyle = new Style();
                textDataTypeStyle.Number = 49;
                StyleFlag flag = new StyleFlag();
                flag.All = true;

                // SetColumn Data Formats
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells.Columns[i].ApplyStyle(textDataTypeStyle, flag);
                }

            }
            else
            {
                IEnumerable<ProductDto> productDtoList;
                IEnumerable<ProductGroupDto> productGroupDtoList;
                Int32 productHierarchyId;

                //[TODO] Consider if there are any conditions which define that we are only 
                //importing a subset of the data rather than everything for the entity.

                //get the data to be exported
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IProductDal dal = dalContext.GetDal<IProductDal>())
                    {
                        productDtoList = dal.FetchByEntityId(entityId);
                    }
                    using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                    {
                        productHierarchyId = dal.FetchByEntityId(entityId).Id;
                    }
                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        productGroupDtoList = dal.FetchByProductHierarchyId(productHierarchyId);
                    }
                }


                // now run through and write all the products to the file
                Int32 row = 1;
                foreach (ProductDto product in productDtoList)
                {
                    //get the corresponding attributedata
                    Int32 productId = product.Id;

                    //cycle through column mappings
                    for (Int32 i = 0; i < mappingList.Count; i++)
                    {
                        ImportMapping mapping = mappingList[i];

                        //get the value to be exported
                        //V8-26333 : Correct to use fetched CustomAttributeDataDto from database
                        Object cellValue =
                            ProductImportMappingList.GetValueByMappingId(mapping.PropertyIdentifier, product, productGroupDtoList
                            , new Framework.Planograms.Dal.DataTransferObjects.CustomAttributeDataDto());

                        //write to worksheet
                        WpfHelper.SetColumnValue(workSheet.Cells[row, i], (cellValue != null) ? cellValue.ToString() : null);
                    }
                    row++;
                }

            }


            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} product records exported successfuly.", workSheet.Cells.Rows.Count);
        }
        #endregion
    }
}
