﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Aspose.Cells;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Helpers;
using System.Diagnostics;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportLocationSpaceBayProcess
    {
        #region Methods

        protected override void OnExecute()
        {
            Int32 entityId = this.EntityId;

            //get the mapping list of this export type
            IImportMappingList mappingList = LocationSpaceBayImportMappingList.NewLocationSpaceBayImportMappingList();

            //create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }

            //now write the data if required
            if (this.ExportHeadersOnly)
            {
                //apply a text style format to each column
                Style textDataStyle = new Style();
                textDataStyle.Number = 49;
                StyleFlag flag = new StyleFlag();
                flag.All = true;

                //set column data formats
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells.Columns[i].ApplyStyle(textDataStyle, flag);
                }
            }
            else
            {
                IEnumerable<LocationSpaceInfoDto> locationSpaceDtoList;
                IEnumerable<ProductGroupDto> productGroupDtoList;
                Int32 productHierarchyId;

                //get the data to be exported
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (ILocationSpaceInfoDal dal = dalContext.GetDal<ILocationSpaceInfoDal>())
                    {
                        locationSpaceDtoList = dal.FetchByEntityId(entityId);
                    }

                    using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                    {
                        productHierarchyId = dal.FetchByEntityId(entityId).Id;
                    }

                    using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                    {
                        productGroupDtoList = dal.FetchByProductHierarchyId(productHierarchyId);
                    }

                    //now run through and write all the location space data to the file
                    Int32 row = 1;
                    foreach (LocationSpaceInfoDto locationSpaceInfoDto in locationSpaceDtoList)
                    {
                        //Fetch product groups
                        using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                        {
                            IEnumerable<LocationSpaceProductGroupDto> locSpaceProductGroupDtoList = dal.FetchByLocationSpaceId(locationSpaceInfoDto.Id);
                            foreach (LocationSpaceProductGroupDto locSpaceProductGroupDto in locSpaceProductGroupDtoList)
                            {
                                //Fetch product bays
                                using (ILocationSpaceBayDal bayDal = dalContext.GetDal<ILocationSpaceBayDal>())
                                {
                                    IEnumerable<LocationSpaceBayDto> locSpaceBayDtoList = bayDal.FetchByLocationSpaceProductGroupId(locSpaceProductGroupDto.Id);
                                    foreach (LocationSpaceBayDto locSpaceBayDto in locSpaceBayDtoList)
                                    {
                                        //cycle column mappings
                                        for (Int32 i = 0; i < mappingList.Count; i++)
                                        {
                                            ImportMapping mapping = mappingList[i];

                                            //get the value to be exported
                                            Object cellValue =
                                                LocationSpaceBayImportMappingList.GetValueByMappingId(mapping.PropertyIdentifier, locSpaceBayDto, locSpaceProductGroupDto, locationSpaceInfoDto.LocationCode, productGroupDtoList);
                                            //write to worksheet
                                            WpfHelper.SetColumnValue(workSheet.Cells[row, i], (cellValue != null) ? cellValue.ToString() : null);
                                        }
                                        row++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} location space bay records exported successfuly.", workSheet.Cells.Rows.Count);
        }

        #endregion
    }
}
