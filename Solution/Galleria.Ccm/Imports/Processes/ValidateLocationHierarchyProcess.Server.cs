﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Model;
using System.Globalization;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ValidateLocationHierarchyProcess
    {
        #region Fields

        private HashSet<String> _validationImportActionObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationDuplicateDeletedItems = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<Tuple<Int32, String>> _validationIdentifierFields = new HashSet<Tuple<Int32, String>>(new ValidationComparer());
        private HashSet<Tuple<Int32, String>> _validationDuplicates = new HashSet<Tuple<Int32, String>>(new ValidationComparer());
        private HashSet<Tuple<Int32, ImportMapping>> _groupCodeMappingList = new HashSet<Tuple<Int32, ImportMapping>>();
        private ImportMapping _lowestLevelGroupCodeMapping = null;

        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the validation impot action objects
        /// </summary>
        public HashSet<string> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }

        /// <summary>
        /// Returns all merch group codes marked as deleted.
        /// </summary>
        public HashSet<String> ValidationDuplicateDeletedItems
        {
            get { return _validationDuplicateDeletedItems; }
        }

        /// Gets the validation identifier fields
        /// </summary>
        public HashSet<Tuple<Int32, String>> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }

        /// <summary>
        /// Gets the validation duplicates
        /// </summary>
        public HashSet<Tuple<Int32, String>> ValidationDuplicates
        {
            get { return _validationDuplicates; }
        }

        /// <summary>
        /// Gets the group code mapping list
        /// </summary>
        public HashSet<Tuple<Int32, ImportMapping>> GroupCodeMappingList
        {
            get { return _groupCodeMappingList; }
        }

        /// <summary>
        /// Gets/Sets the lowest levels group code mapping
        /// </summary>
        public ImportMapping LowestLevelGroupCodeMapping
        {
            get { return _lowestLevelGroupCodeMapping; }
            set { _lowestLevelGroupCodeMapping = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation started.");

            #region Fetch Relational Check Objects
            //get all existing groups
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //Get all of the unique identifying mappings (group code mappings)
                IEnumerable<ImportMapping> groupCodeMappingList = FileData.MappingList.Where(p => p.PropertyIsUniqueIdentifier);

                this.GroupCodeMappingList.Clear();
                Int32 groupMappinglevelNumber = 1;
                foreach (ImportMapping mapping in groupCodeMappingList)
                {
                    this.GroupCodeMappingList.Add(Tuple.Create<Int32, ImportMapping>(groupMappinglevelNumber, mapping));
                    groupMappinglevelNumber++;
                }

                //Last mapping in this list is the lowest level code, each of these need to be unique
                this.LowestLevelGroupCodeMapping = groupCodeMappingList.OrderBy(p => p.PropertyIdentifier).Last();

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    Int32 levelNumber = 1;
                    foreach (ImportMapping mapping in groupCodeMappingList.OrderBy(p => p.PropertyIdentifier))
                    {
                        Int32 columnNumber = FileData.GetMappedColNumber(mapping);

                        //Get group code for this mapping
                        String groupCode = Convert.ToString(row[columnNumber], CultureInfo.InvariantCulture);

                        if (!String.IsNullOrEmpty(groupCode))
                        {
                            //Create pairing
                            Tuple<Int32, String> levelGroupPairing = Tuple.Create<Int32, String>(levelNumber, groupCode);

                            //If not lowest level group this is a parent group too
                            if (mapping != this.LowestLevelGroupCodeMapping)
                            {
                                //Add to identifer fields if not already
                                if (!this.ValidationIdentifierFields.Contains(levelGroupPairing))
                                {
                                    this.ValidationIdentifierFields.Add(levelGroupPairing);
                                }

                                //Check no group across different levels has the the same code.
                                //NB - Groups can have the same code in the same level
                                if (this.ValidationIdentifierFields.Any(p => p.Item2 == groupCode && p.Item1 != levelNumber))
                                {
                                    this.ValidationDuplicates.Add(levelGroupPairing);
                                }
                            }
                            else
                            {
                                //If duplicate at the same level
                                if (this.ValidationIdentifierFields.Contains(levelGroupPairing))
                                {
                                    this.ValidationDuplicates.Add(levelGroupPairing);
                                }
                                //If duplicate code over different levels
                                else if (this.ValidationIdentifierFields.Any(p => p.Item2 == groupCode))
                                {
                                    this.ValidationDuplicates.Add(levelGroupPairing);
                                }
                                else
                                {
                                    this.ValidationIdentifierFields.Add(levelGroupPairing);
                                }
                            }
                        }

                        levelNumber++;
                    }
                }

                //get the entity's location hierarchy
                LocationHierarchyDto locationHierarchyDto;
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    locationHierarchyDto = dal.FetchByEntityId(this.EntityId);
                }
                //get level dtos in order
                List<LocationLevelDto> levelDtos = new List<LocationLevelDto>();
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    List<LocationLevelDto> dalLevelDtos = dal.FetchByLocationHierarchyId(locationHierarchyDto.Id).ToList();
                    LocationLevelDto currentDto = dalLevelDtos.FirstOrDefault(l => l.ParentLevelId == null);
                    while (currentDto != null)
                    {
                        levelDtos.Add(currentDto);

                        //get the child level
                        currentDto = dalLevelDtos.FirstOrDefault(l => l.ParentLevelId == currentDto.Id);
                    }
                    dalLevelDtos = null;
                }
                //get the existing hierarchy groups
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    IEnumerable<LocationGroupDto> groups = dal.FetchByLocationHierarchyIdIncludingDeleted(locationHierarchyDto.Id);
                    foreach (LocationGroupDto dto in groups)
                    {
                        this.ValidationImportActionObjects.Add(dto.Code);

                        if (dto.DateDeleted == null)
                        {
                            //[GFS-22916] Check for groups to be inserted where the code already exists for a group
                            // on a different level.
                            LocationLevelDto thisLevel = levelDtos.FirstOrDefault(l => l.Id == dto.LocationLevelId);
                            Int32 levelNumber = levelDtos.IndexOf(thisLevel);

                            foreach (var insert in
                                this.ValidationIdentifierFields.Where(p =>
                                    String.Compare(p.Item2, dto.Code, StringComparison.OrdinalIgnoreCase) == 0
                                    && p.Item1 != levelNumber))
                            {
                                if (!this.ValidationDuplicates.Contains(insert))
                                {
                                    this.ValidationDuplicates.Add(insert);
                                }
                            }
                        }
                        else
                        {
                            //item is deleted so add to the deleted items duplicate check collection.
                            if (!this.ValidationDuplicateDeletedItems.Contains(dto.Code))
                            {
                                this.ValidationDuplicateDeletedItems.Add(dto.Code);
                            }
                        }
                    }
                    groups = null;
                }
                dalContext.Commit();
            }

            #endregion

            //Validate Data against mappings using Generic methods
            this.RunDataTypeValidation(FileData, this.ValidRows);

            //Update datatable to include import action & excel row number columns
            this.AddExtraColumns(FileData);

            //Set validated data to be the updated data item and set it to be returned
            this.ValidatedData = FileData;

            Trace.TraceInformation("Validation of {0} location hierarchy complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Override duplicate record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowNumber"></param>
        /// <param name="colNumber"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            //If column mapping is a group code mapping
            if (col.PropertyIsUniqueIdentifier)
            {
                //Find correct level mapping
                Tuple<Int32, ImportMapping> groupMapping = this.GroupCodeMappingList.FirstOrDefault(p => p.Item2.PropertyIdentifier == col.PropertyIdentifier);
                if (groupMapping != null)
                {
                    //Get level number
                    Int32 levelNumber = groupMapping.Item1;

                    //check whether the location group code is a dupliacte of another being imported
                    String columnValueString = columnValue.ToString();
                    //Create pairing
                    Tuple<Int32, String> levelGroupPairing = Tuple.Create<Int32, String>(levelNumber, columnValueString);

                    if (this.ValidationDuplicates.Contains(levelGroupPairing))
                    {
                        //duplicate found create error item
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                            columnValueString, Message.DataManagement_Validation_DuplicateRecordHeader,
                            ValidationErrorType.Error, true, false, false, false, col, true);
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// Override duplicate deleted item record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateDeleteditemRecordValidation(ImportMapping col, Int32 rowNumber, Int32 colNumber, object columnValue)
        {
            if (col.PropertyIsUniqueIdentifier)
            {
                //Find correct level mapping
                Tuple<Int32, ImportMapping> groupMapping = this.GroupCodeMappingList.FirstOrDefault(p => p.Item2.PropertyIdentifier == col.PropertyIdentifier);
                if (groupMapping != null)
                {
                    //check whether the location group code is a duplicate of another deleted group
                    String columnValueString = columnValue.ToString();

                    if (this.ValidationDuplicateDeletedItems.Contains(columnValueString))
                    {
                        //Duplicate found, create error item
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                            columnValueString, Message.DataManagement_Validation_DuplicateDeletedGroup,
                            ValidationErrorType.Error, true, false, false, false, col, true);
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// Override import type validation base method
        /// </summary>
        /// <param name="rowData"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            Int32 colNumber = this.FileData.GetMappedColNumber(this.LowestLevelGroupCodeMapping);

            if (colNumber != 0)
            {
                //Extract the code from the row data using this index
                String code = rowData[colNumber].Value.ToString();

                //Check whether the location code matches a location in the database
                if (!this.ValidationImportActionObjects.Contains(code))
                {
                    //if ths location code is new, but a duplicate record is also being imported, the first record
                    //will be insert, all others will then be updates
                    this.ValidationImportActionObjects.Add(code);
                    return Message.DataManagement_ImportType_Add;
                }
                else
                {
                    //Otherwise it is an update
                    return Message.DataManagement_ImportType_Update;
                }

            }
            else
            {
                //[TODO] Work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }

        /// <summary>
        /// Override Parent Record Validation (VALIDATION FOR MISSING CODES/NAMES - THE ONLY WAY TO VALIDATE THIS WAS TO
        /// OVERRIDE ANOTHER PROCESS)
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowNumber"></param>
        /// <param name="colNumber"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            //If column mapping is a group code mapping
            if (col.PropertyIsUniqueIdentifier)
            {
                //find the row
                ImportFileDataRow row = this.FileData.Rows.FirstOrDefault(p => p.RowNumber == rowNumber);

                if (row != null)
                {
                    //find identifier for the import
                    String groupIdentifier = col.PropertyName;
                    Int32 pos = groupIdentifier.LastIndexOf(' ');
                    groupIdentifier = groupIdentifier.Substring(0, pos);

                    //find name mapping
                    ImportMapping nameMapping = this.FileData.MappingList.FirstOrDefault(c => c.PropertyName.Equals(groupIdentifier + " " + LocationGroup.NameProperty.FriendlyName));

                    if (nameMapping != null)
                    {
                        //find the name column
                        ImportFileDataColumn nameColumn = this.FileData.Columns.FirstOrDefault(c => c.Header.Equals(nameMapping.ColumnReference));

                        if (nameColumn != null)
                        {
                            String nameValue = Convert.ToString(row.Cells.FirstOrDefault(c => c.ColumnNumber == nameColumn.ColumnNumber).Value);
                            Boolean nameEmpty = String.IsNullOrEmpty(nameValue);

                            //if code is not empty but name is
                            if (!String.IsNullOrEmpty(columnValue.ToString()) && nameEmpty)
                            {
                                //return error for no name but has code
                                return ValidationErrorItem.NewValidationErrorItem(rowNumber, nameColumn.ColumnNumber,
                                    nameValue, Message.ValidateHierarchy_NoName,
                                    ValidationErrorType.Error, true, false, false, false, nameMapping, false);
                            }
                            //if name code is empty but name is not
                            if (String.IsNullOrEmpty(columnValue.ToString()) && !nameEmpty)
                            {
                                //return error for missing code
                                return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                                    columnValue.ToString(), Message.ValidateHierarchy_NoCode,
                                    ValidationErrorType.Error, true, false, false, false, col, false);
                            }
                        }
                    }
                }
            }
            return null;
        }

        #endregion

        #region Private Helper Classes

        /// <summary>
        /// IEqualityComparer to ensure String in Tuple<Int32, String>
        /// is compared using StringComparer.OrdinalIgnoreCase
        /// </summary>
        private sealed class ValidationComparer :
            IEqualityComparer<Tuple<Int32, String>>
        {
            public Boolean Equals(Tuple<Int32, String> x, Tuple<Int32, String> y)
            {
                StringComparer s = StringComparer.OrdinalIgnoreCase;

                if (x.Item1.CompareTo(y.Item1) != 0) { return false; }
                if (s.Compare(x.Item2, y.Item2) != 0) { return false; }

                return true;
            }

            public Int32 GetHashCode(Tuple<Int32, String> obj)
            {
                return obj.Item1.GetHashCode() +
                    obj.Item2.ToLowerInvariant().GetHashCode();
            }
        }

        #endregion
    }
}
