﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-25455 : J.Pickup
//  Copied over from GFS & removed versioning
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    [Serializable]
    public partial class ExportAssortmentProcess : ExportProcessBase<ExportAssortmentProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="fileName">The full name and path of the export file</param>
        /// <param name="exportHeadersOnly">Indicates if only the export headers should be exported</param>
        public ExportAssortmentProcess(Int32 entityId, String fileName, Boolean exportHeadersOnly)
            : base(entityId, fileName, exportHeadersOnly)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ExportAssortmentProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ExportAssortmentProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ExportAssortmentProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ExportAssortmentData.ToString()));
            BusinessRules.AddRule(typeof(ExportAssortmentProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}