﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    [Serializable]
    public sealed partial class ExportLocationSpaceElementProcess : ExportProcessBase<ExportLocationSpaceElementProcess>
    {
        #region Constructors

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="fileName">The full name and path of the export file</param>
        /// <param name="exportHeadersOnly">Indicates if only the export headers should be exported</param>
        public ExportLocationSpaceElementProcess(Int32 entityId, String fileName, Boolean exportHeadersOnly)
            : base(entityId, fileName, exportHeadersOnly)
        {

        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ExportLocationSpaceElementProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ExportLocationSpaceElementProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ExportLocationSpaceElementProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ExportLocationSpaceData.ToString()));
            BusinessRules.AddRule(typeof(ExportLocationSpaceElementProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
