﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportMerchandisingHierarchyProcess
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} products started.", this.ValidRowsCount);

            ImportMerchandisingHierarchy();

            Trace.TraceInformation("Import of {0} products complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Does the import.
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportMerchandisingHierarchy()
        {
            MerchHierarchyImportMappingList mappingList = (MerchHierarchyImportMappingList)this.ImportData.MappingList;

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                ProductHierarchyDto hierarchy = null;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchy = dal.FetchByEntityId(this.EntityId);
                }
                Int32 hierarchyId = hierarchy.Id;

                //get the existing hierarchy levels
                List<ProductLevelDto> hierarchyLevelList = new List<ProductLevelDto>();
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    List<ProductLevelDto> fetchedLevels = dal.FetchByProductHierarchyId(hierarchyId).ToList();

                    //order the levels
                    ProductLevelDto rootDto = fetchedLevels.FirstOrDefault(d => d.ParentLevelId == null);
                    Int32 lastLevelId = rootDto.Id;
                    while (true)
                    {
                        ProductLevelDto nextDto = fetchedLevels.FirstOrDefault(l => l.ParentLevelId == lastLevelId);
                        if (nextDto != null)
                        {
                            hierarchyLevelList.Add(nextDto);
                            lastLevelId = nextDto.Id;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                //get the existing hierarchy groups
                List<ProductGroupDto> hierarchyExistingGroupsList;
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    hierarchyExistingGroupsList =  dal.FetchByProductHierarchyId(hierarchyId).ToList();
                }

                ProductGroupDto rootGroup = hierarchyExistingGroupsList.First(g => g.ParentGroupId == null);

                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {

                    //cycle through rows
                    List<ProductGroupDto> newDtosList = new List<ProductGroupDto>();
                    foreach (ImportFileDataRow row in this.ValidRows)
                    {
                        //start at the top level of the row and check units down the tree
                        ProductGroupDto currentGroup = rootGroup;

                        //cycle through the mappings
                        Int32 levelNum = 0;
                        for (Int32 i = 0; i < mappingList.Count; i += 2)
                        {
                            ProductLevelDto currentLevel = hierarchyLevelList[levelNum];

                            ImportMapping codeMapping = mappingList[i];
                            ImportMapping nameMapping = mappingList[i + 1];

                            String columnRef = codeMapping.ColumnReference;
                            ImportFileDataColumn importColumn = ImportData.Columns.FirstOrDefault(c => c.Header == columnRef);
                            Int32 value = (importColumn != null) ? importColumn.ColumnNumber : 0;

                            String columnRef1 = nameMapping.ColumnReference;
                            ImportFileDataColumn importColumn1 = ImportData.Columns.FirstOrDefault(c => c.Header == columnRef1);
                            Int32 value1 = (importColumn1 != null) ? importColumn1.ColumnNumber : 0;


                            String code = row[this.ImportData.GetMappedColNumber(codeMapping)].CurrentValue as String;
                            String name = row[this.ImportData.GetMappedColNumber(nameMapping)].CurrentValue as String;
                            if (name == null && nameMapping.PropertyType == typeof(System.String))
                            {
                                name = String.Empty; // where cast as string returns null, provide String.Empty
                            }

                            if (!String.IsNullOrEmpty(code))
                            {
                                String codeLookup = code.ToLowerInvariant();

                                //attempt to get the existing unit by the code
                                Int32 parentId = currentGroup.Id;
                                ProductGroupDto group =
                                    hierarchyExistingGroupsList
                                    .FirstOrDefault(g =>
                                        g.ProductLevelId == currentLevel.Id &&
                                        g.Code.ToLowerInvariant() == codeLookup);

                                if (group == null)
                                {
                                    group = new ProductGroupDto();
                                    group.Code = code;
                                    group.Name = name;
                                    group.ParentGroupId = parentId;
                                    group.ProductHierarchyId = hierarchyId;
                                    group.ProductLevelId = currentLevel.Id;
                                    group.DateCreated = DateTime.UtcNow;
                                    group.DateLastModified = DateTime.UtcNow;

                                    //insert immediately as we will need the id - 
                                    //not sure how to get round this :S
                                    dal.Insert(group);

                                    hierarchyExistingGroupsList.Add(group);
                                }

                                group.Name = name;
                                group.ParentGroupId = parentId;
                                group.ProductLevelId = currentLevel.Id;

                                //set as the new parent
                                currentGroup = group;
                            }
                            //increment the level
                            levelNum++;
                        }
                    }
                    //save down all the group dtos
                    if (hierarchyExistingGroupsList.Count > 0)
                    {
                        hierarchyExistingGroupsList.Remove(rootGroup);//dont update the root
                        dal.Upsert(hierarchyExistingGroupsList, new ProductGroupIsSetDto(true));
                    }
                }
                dalContext.Commit();
            }
        }

        #endregion
    }
}
