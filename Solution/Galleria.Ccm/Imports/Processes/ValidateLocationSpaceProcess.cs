﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25448 : N.Haywood
//  Copied from SA
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using Csla.Rules.CommonRules;
using Csla.Rules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management location space validation process class
    /// </summary>
    [Serializable]
    public sealed partial class ValidateLocationSpaceProcess : ValidateProcessBase<ValidateLocationSpaceProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ValidateLocationSpaceProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstRow)
            : base(entityId, fileData, startingRowNumber, headersInFirstRow)
        {

        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidateLocationSpaceProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationSpaceProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationSpaceProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationSpaceData.ToString()));
            BusinessRules.AddRule(typeof(ValidateLocationSpaceProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
