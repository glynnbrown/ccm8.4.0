﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25452 : N.Haywood
//	Added from SA
#endregion
#region Version History: CCM802
// V8-29230 : N.Foster
//  Removed unused parameter from import products process
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Import class for Product
    /// </summary>
    [Serializable]
    public sealed partial class ImportProductsProcess : ImportProcessBase<ImportProductsProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ImportProductsProcess(Int32 entityId, ImportFileData importData) :
            base(entityId, importData)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ImportProductsProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportProductsProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ImportProductsProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ProductCreate.ToString()));
            BusinessRules.AddRule(typeof(ImportProductsProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
