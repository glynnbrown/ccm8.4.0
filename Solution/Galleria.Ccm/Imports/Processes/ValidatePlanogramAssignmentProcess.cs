﻿using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Imports;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Imports.Processes
{ 
    /// <summary>
    /// Data management planogram assignment validation process class
    /// </summary>
    [Serializable]
    public sealed partial class ValidatePlanogramAssignmentProcess : ValidateProcessBase<ValidatePlanogramAssignmentProcess>
    {
        private PlanogramGroup _planogramGroup;
        private ProductGroupInfo _productGroupInfo;
        private LocationInfo _locationInfo;
        private PlanogramAssignmentStatusType _planogramAssignmentStatusType;

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ValidatePlanogramAssignmentProcess(Int32 entityId, PlanogramGroup planogramGroup, ProductGroupInfo productGroupInfo, LocationInfo locationInfo, PlanogramAssignmentStatusType planogramAssignmentStatusType, 
                                                    ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstRow)
            : base(entityId, fileData, startingRowNumber, headersInFirstRow)
        {
            _planogramGroup = planogramGroup;
            _productGroupInfo = productGroupInfo;
            _locationInfo = locationInfo;
            _planogramAssignmentStatusType = planogramAssignmentStatusType;

            FilterRowData(fileData);
        }

        private void FilterRowData(ImportFileData fileData)
        {
            ImportMapping locationCodeMappingForImportType = fileData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.LocationCodeMappingId).FirstOrDefault();
            ImportMapping productGroupMappingForImportType = fileData.MappingList.Where(c => c.PropertyIdentifier == PlanogramAssignmentImportMappingList.ProductGroupCodeMappingId).FirstOrDefault();
            
            //get location id mapping
            Int32 locationCodeMappingColumnNumber = fileData.GetMappedColNumber(locationCodeMappingForImportType);
            //get product group id mapping
            Int32 productGroupMappingColumnNumber = fileData.GetMappedColNumber(productGroupMappingForImportType);

            foreach (var row in fileData.Rows)
            {
                Boolean isValid = false;
                String productGroupCode = Convert.ToString(row[productGroupMappingColumnNumber], CultureInfo.InvariantCulture)?.Trim();
                String locationCode = Convert.ToString(row[locationCodeMappingColumnNumber], CultureInfo.InvariantCulture)?.Trim();
                
                if (_productGroupInfo != null)
                {
                    isValid = (productGroupCode == _productGroupInfo.Code);
                }
                else
                {
                    isValid = (locationCode == _locationInfo.Code);
                }

                if (!isValid)
                {
                    row.IsIgnored = true;
                }
            }
        }

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidatePlanogramAssignmentProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidatePlanogramAssignmentProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidatePlanogramAssignmentProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportLocationSpaceData.ToString()));
            BusinessRules.AddRule(typeof(ValidatePlanogramAssignmentProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
