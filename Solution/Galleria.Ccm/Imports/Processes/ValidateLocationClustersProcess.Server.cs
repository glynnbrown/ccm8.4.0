﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25449 : N.Haywood
//	Copied from SA
#endregion

#region Version History: (CCM 8.3.0)
// V8-31877 : N.Haywood
//  Added Product Group Code
#endregion

#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics;
using Galleria.Ccm.Resources.Language;
using System.Globalization;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management location clusters validation process class
    /// </summary>
    public partial class ValidateLocationClustersProcess
    {
        #region Fields

        private HashSet<string> _validationParentObjects = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
        private HashSet<ClusterLocationSearchCriteriaDto> _validationImportActionObjects = new HashSet<ClusterLocationSearchCriteriaDto>();
        private HashSet<ClusterLocationSearchCriteriaDto> _validationIdentifierFields = new HashSet<ClusterLocationSearchCriteriaDto>();
        private HashSet<ClusterLocationSearchCriteriaDto> _validationDuplicates = new HashSet<ClusterLocationSearchCriteriaDto>();
        private HashSet<String> _invalidProductGroupCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);


        #endregion

        #region Properties

        /// <summary>
        /// Gets/Sets the validation parent objects
        /// </summary>
        public HashSet<string> ValidationParentObjects
        {
            get { return _validationParentObjects; }
            set { _validationParentObjects = value; }
        }

        /// <summary>
        /// Gets/Sets the validation impot action objects
        /// </summary>
        public HashSet<ClusterLocationSearchCriteriaDto> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }

        /// Gets the validation identifier fields
        /// </summary>
        public HashSet<ClusterLocationSearchCriteriaDto> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }

        /// <summary>
        /// Gets the validation duplicates
        /// </summary>
        public HashSet<ClusterLocationSearchCriteriaDto> ValidationDuplicates
        {
            get { return _validationDuplicates; }
        }

        /// <summary>
        /// Returns the hashset of product group codes that exist
        /// but are not valid for product assignment.
        /// </summary>
        public HashSet<String> InvalidProductGroupCodes
        {
            get { return _invalidProductGroupCodes; }
        }

        /// <summary>
        /// Local property for the mappings
        /// </summary>
        private ImportMapping locationCodeMappingforImportType;
        private ImportMapping clusterNameMappingforImportType;
        private ImportMapping schemeNameMappingforImportType;

        #endregion

        #region Methods
        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation started.");

            //Set mappings
            locationCodeMappingforImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationClusterImportMappingList.LocationCodeMappingId).FirstOrDefault();
            clusterNameMappingforImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationClusterImportMappingList.ClusterNameMappingId).FirstOrDefault();
            schemeNameMappingforImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId).FirstOrDefault();

            //Setup datatable
            ImportFileData worksheetData = this.FileData;

            Trace.TraceInformation("{0} clusters's being validated.", worksheetData.Rows.Count);

            #region Fetch Relational Check Objects
            // ISO-13449: Don't Dispose of DalFactory instances fetched from the DalContainer.
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                // start a transaction for this data validation
                dalContext.Begin();
                //Get mappings
                Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationCodeMappingforImportType);
                Int32 clusterNameMappingColumnNumber = this.FileData.GetMappedColNumber(clusterNameMappingforImportType);
                Int32 schemeNameMappingColumnNumber = this.FileData.GetMappedColNumber(schemeNameMappingforImportType);
                //Get current entity id
                Int32 entityId = this.EntityId;

                //Create lists to store codes
                HashSet<String> locationCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                HashSet<String> clusterNames = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
                HashSet<String> schemeNames = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

                foreach (ImportFileDataRow row in worksheetData.Rows)
                {
                    //Build up collections of details
                    String schemeName = Convert.ToString(row[schemeNameMappingColumnNumber], CultureInfo.InvariantCulture);
                    String clusterName = Convert.ToString(row[clusterNameMappingColumnNumber], CultureInfo.InvariantCulture);
                    String locationCode = Convert.ToString(row[locationCodeMappingColumnNumber], CultureInfo.InvariantCulture);

                    if (schemeName != null && clusterName != null && locationCode != null)
                    {
                        locationCodes.Add(locationCode);

                        //Constructor criteria
                        ClusterLocationSearchCriteriaDto criteria = new ClusterLocationSearchCriteriaDto()
                        {
                            ClusterSchemeName = schemeName,
                            ClusterName = clusterName,
                            LocationCode = locationCode,
                            ProductGroupCode = ""
                        };

                        //[ISO-13387] While we're looping here anyway, we might as well go ahead and find duplicates.
                        if (this.ValidationIdentifierFields.Contains(criteria))
                        {
                            this.ValidationDuplicates.Add(criteria);
                        }
                        else
                        {
                            this.ValidationIdentifierFields.Add(criteria);
                        }
                    }
                }

                // now retrieve all locations that match the location codes
                // to inform the user of the import action that will take place
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    List<LocationInfoDto> locationInfoDtos = dal.FetchByEntityIdLocationCodes(
                        entityId, locationCodes.Distinct()).ToList();
                    ///Strip deleted items
                    locationInfoDtos = locationInfoDtos.Where(l => l.DateDeleted == null).ToList();

                    //Add codes to hashset for faster searching later on
                    locationInfoDtos.ForEach(p => this.ValidationParentObjects.Add(p.Code));
                }

                // now retrieve all cluster schemes for this entity id
                // to inform the user of the import action that will take place
                using (IClusterLocationSearchCriteriaDal dal = dalContext.GetDal<IClusterLocationSearchCriteriaDal>())
                {
                    List<ClusterLocationSearchCriteriaDto> peerGroups = dal.FetchByEntityId(entityId).ToList();

                    //Add codes to hashset for faster searching later on
                    peerGroups.ForEach(c => this.ValidationImportActionObjects.Add(c));
                }

                //Add possible parent codes to hashset for faster searching later
                Int32 hierarchyId;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyId = dal.FetchByEntityId(entityId).Id;
                }
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    List<ProductGroupDto> groups = dal.FetchByProductHierarchyId(hierarchyId).ToList();
                    foreach (ProductGroupDto group in groups)
                    {
                        ValidationParentObjects.Add(group.Code);
                    }
                    groups = null;
                }
            }
            #endregion

            //Validate Data against mappings using Generic methods
            this.RunDataTypeValidation(worksheetData);

            //update datatable to include import action and excel row number columns
            this.AddExtraColumns(worksheetData);

            //Set validated data to be the updated data item and set it to be returned
            this.ValidatedData = worksheetData;

            Trace.TraceInformation("Validation of {0} products complete.", worksheetData.Rows.Count);
        }

        /// <summary>
        /// Override parent record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationClusterImportMappingList.LocationCodeMappingId)
            {
                //Check whether the store code matches a store in the database
                if (!this.ValidationParentObjects.Contains(columnValue.ToString()))
                {
                    //No matching store code, create new error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(), Message.DataManagement_Validation_NoMatchingLocation, ValidationErrorType.Error, true, false, false, false, col, false);
                }
            }
            if (col.PropertyIdentifier == LocationClusterImportMappingList.ProductGroupCodeMappingId)
            {
                String value = columnValue.ToString();
                if (!this.ValidationParentObjects.Contains(value))
                {
                    if (this.InvalidProductGroupCodes.Contains(value))
                    {
                        //product group code is invalid for product assignment
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, value,
                            Message.DataManagement_Validation_InvalidProductGroup, ValidationErrorType.Error,
                            true, false, false, false, col, false);
                    }
                    else
                    {
                        //no matching product group code
                        return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, value,
                            Message.DataManagement_Validation_NoMatchingProductGroup, ValidationErrorType.Error,
                            true, false, false, false, col, false);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Override duplicate record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationClusterImportMappingList.SchemeNameMappingId)
            {
                //Construct unique data group
                String schemeName = columnValue.ToString();
                Int32 clusterNameMappingColumnNumber = this.FileData.GetMappedColNumber(clusterNameMappingforImportType);
                String clusterName = this.FileData.Rows.FirstOrDefault(r => r.RowNumber == rowNumber)
                    .Cells.FirstOrDefault(c => c.ColumnNumber == clusterNameMappingColumnNumber).Value.ToString();
                Int32 locationCodeMappingColumnNumber = this.FileData.GetMappedColNumber(locationCodeMappingforImportType);
                String locationCode = this.FileData.Rows.FirstOrDefault(r => r.RowNumber == rowNumber)
                    .Cells.FirstOrDefault(c => c.ColumnNumber == locationCodeMappingColumnNumber).Value.ToString();

                //Construct criteria
                ClusterLocationSearchCriteriaDto searchItem = new ClusterLocationSearchCriteriaDto()
                {
                    ClusterSchemeName = schemeName,
                    ClusterName = clusterName,
                    LocationCode = locationCode
                };

                //Check whether the cluster is a duplicate of another being imported
                if (this.ValidationDuplicates.Contains(searchItem))
                {
                    //Duplicate found, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, schemeName, Message.DataManagement_Validation_DuplicateCluster, ValidationErrorType.Error, true, false, false, false, col, true);
                }
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        /// <param name="rowData"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            //Construct unique data grouping
            Int32 locationCodeColNumber = this.FileData.GetMappedColNumber(locationCodeMappingforImportType);
            Int32 clusterNameColNumber = this.FileData.GetMappedColNumber(clusterNameMappingforImportType);
            Int32 schemeNameColNumber = this.FileData.GetMappedColNumber(schemeNameMappingforImportType);

            if (locationCodeColNumber != 0 && clusterNameColNumber != 0 && schemeNameColNumber != 0)
            {
                if (rowData[schemeNameColNumber].Value != null
                    && rowData[clusterNameColNumber].Value != null
                        && rowData[locationCodeColNumber].Value != null)
                {
                    //Construct search criteria
                    ClusterLocationSearchCriteriaDto searchItem = new ClusterLocationSearchCriteriaDto()
                    {
                        ClusterSchemeName = rowData[schemeNameColNumber].Value.ToString(),
                        ClusterName = rowData[clusterNameColNumber].Value.ToString(),
                        LocationCode = rowData[locationCodeColNumber].Value.ToString(),
                        ProductGroupCode = ""
                    };

                    //Check whether the clsuter data matches a cluster in the database
                    if (!this.ValidationImportActionObjects.Contains(searchItem))
                    {
                        //if ths location code is new, but a duplicate record is also being imported, the first record
                        //will be insert, all others will then be updates
                        this.ValidationImportActionObjects.Add(searchItem);
                        return Message.DataManagement_ImportType_Add;
                    }
                    else
                    {
                        //Otherwise it is an update
                        return Message.DataManagement_ImportType_Update;
                    }
                }
                else
                {
                    return Message.DataManagement_ImportType_Add;
                }
            }
            else
            {
                //[TODO] work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }

        #endregion
    }
}
