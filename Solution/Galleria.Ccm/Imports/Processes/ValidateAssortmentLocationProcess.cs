﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// GFS-25455 :J.Pickup
//  Copied over from GFS
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Imports;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management Assortment Location validation process class
    /// </summary>
    [Serializable]
    public partial class ValidateAssortmentLocationProcess : ValidateProcessBase<ValidateAssortmentLocationProcess>
    {
        #region Constructors
        public ValidateAssortmentLocationProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstRow)
            : base(entityId, fileData, startingRowNumber, headersInFirstRow)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidateAssortmentLocationProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateAssortmentLocationProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateAssortmentLocationProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportAssortmentData.ToString()));
            BusinessRules.AddRule(typeof(ValidateAssortmentLocationProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}