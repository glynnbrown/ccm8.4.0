﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using System.Collections.Generic;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Base class for all import processes
    /// </summary>
    [Serializable]
    public abstract class ImportProcessBase<T> : Galleria.Framework.Processes.ImportProcessBase<T>
        where T : ImportProcessBase<T>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="importData">The data to import</param>
        public ImportProcessBase(Int32 entityId, ImportFileData importData)
            : base(importData)
        {
            this.EntityId = entityId;
            this.ValidRows = importData.GetValidRows();
            this.ValidRowsCount = this.ValidRows.ToList().Count();
        }
        #endregion

        #region Properties

        #region EntityId
        /// <summary>
        /// The entity id property definition
        /// </summary>
        public static readonly PropertyInfo<Int32> EntityIdProperty =
            RegisterProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The entity id for this process
        /// </summary>
        public Int32 EntityId
        {
            get { return this.ReadProperty<Int32>(EntityIdProperty); }
            private set { this.LoadProperty<Int32>(EntityIdProperty, value); }
        }
        #endregion

        #region ValidRows
        /// <summary>
        /// The valid rows property definition
        /// </summary>
        public static readonly PropertyInfo<IEnumerable<ImportFileDataRow>> ValidRowsProperty =
            RegisterProperty<IEnumerable<ImportFileDataRow>>(c => c.ValidRows);
        /// <summary>
        /// The valid rows for this process
        /// </summary>
        public IEnumerable<ImportFileDataRow> ValidRows
        {
            get { return this.ReadProperty<IEnumerable<ImportFileDataRow>>(ValidRowsProperty); }
            private set { this.LoadProperty<IEnumerable<ImportFileDataRow>>(ValidRowsProperty, value); }
        }
        #endregion

        #region ValidRowsCount
        /// <summary>
        /// The valid rows count property definition
        /// </summary>
        public static readonly PropertyInfo<Int32> ValidRowsCountProperty =
            RegisterProperty<Int32>(c => c.ValidRowsCount);
        /// <summary>
        /// The valid rows count for this process
        /// </summary>
        public Int32 ValidRowsCount
        {
            get { return this.ReadProperty<Int32>(ValidRowsCountProperty); }
            private set { this.LoadProperty<Int32>(ValidRowsCountProperty, value); }
        }
        #endregion

        #endregion
    }
}
