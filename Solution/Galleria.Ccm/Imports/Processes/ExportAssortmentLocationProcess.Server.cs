﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-25455 : J.Pickup
//  Copied over from GFS & removed versioning
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

using Aspose.Cells;

using Csla;

using SmartAssembly.Attributes;

using Galleria.Framework.Dal;
using Galleria.Framework.Imports;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportAssortmentLocationProcess
    {
        #region Methods

        protected override void OnExecute()
        {
            //get the mapping list of the export type
            IImportMappingList mappingList = AssortmentLocationImportMappingList.NewAssortmentLocationImportMappingList();

            ImportMapping assortmentNameMapping = this.GetColumnMapping(mappingList, AssortmentLocationImportMappingList.AssortmentNameMapId);
            ImportMapping locationCodeMapping = this.GetColumnMapping(mappingList, AssortmentLocationImportMappingList.LocationCodeMapId);

            //create a new workbook to hold the export
            Workbook workbook = new Workbook();
            Worksheet workSheet = workbook.Worksheets[0];

            //write all headers to file
            for (Int32 i = 0; i < mappingList.Count; i++)
            {
                ImportMapping mapping = mappingList[i];
                workSheet.Cells[0, i].Value = mapping.PropertyName;
            }

            //now write the data if required
            if (this.ExportHeadersOnly)
            {
                //apply a text style format to each column
                Style textDataStyle = new Style();
                textDataStyle.Number = 49;
                StyleFlag flag = new StyleFlag();
                flag.All = true;

                //set column data formats
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells.Columns[i].ApplyStyle(textDataStyle, flag);
                }
            }
            else
            {
                IEnumerable<AssortmentInfoDto> assortmentInfoDtoList;
                IEnumerable<AssortmentLocationDto> assortmentLocationDtoList;

                //get the data to be exported
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IAssortmentInfoDal dal = dalContext.GetDal<IAssortmentInfoDal>())
                    {
                        assortmentInfoDtoList = dal.FetchByEntityId(this.EntityId);
                    }

                    //now run through and write all the assortments to the file
                    Int32 row = 1;
                    foreach (AssortmentInfoDto assort in assortmentInfoDtoList)
                    {
                        using (IAssortmentLocationDal dal = dalContext.GetDal<IAssortmentLocationDal>())
                        {
                            assortmentLocationDtoList = dal.FetchByAssortmentId(assort.Id);
                        }

                        foreach (AssortmentLocationDto assortLocation in assortmentLocationDtoList)
                        {
                            WpfHelper.SetColumnValue(workSheet.Cells[row, assortmentNameMapping.PropertyIdentifier], assort.Name);
                            WpfHelper.SetColumnValue(workSheet.Cells[row, locationCodeMapping.PropertyIdentifier], assortLocation.Code);
                            row++;
                        }
                    }
                }
            }

            workbook.Save(this.FileName);

            Trace.TraceInformation("{0} assortment location records exported successfully.", workSheet.Cells.Rows.Count);
        }

        #endregion
    }
}