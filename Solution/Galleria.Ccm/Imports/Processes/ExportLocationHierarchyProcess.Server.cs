﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25445 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Aspose.Cells;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Framework.Dal;
using Galleria.Framework.Helpers;
using Galleria.Framework.Imports;
using SmartAssembly.Attributes;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ExportLocationHierarchyProcess
    {
        #region Methods
        /// <summary>
        /// Called when executing this process
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //get the entity
                EntityDto currentEntity;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(this.EntityId);
                }

                // get the location hierarchy
                LocationHierarchyDto currentHierarchy;
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    currentHierarchy = dal.FetchByEntityId(currentEntity.Id);
                }

                //get the existing hierarchy levels
                List<LocationLevelDto> levelList = new List<LocationLevelDto>();
                Int32 lastLevelId;
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    IEnumerable<LocationLevelDto> fetchedLevels = dal.FetchByLocationHierarchyId(currentHierarchy.Id);

                    //order the levels
                    LocationLevelDto rootDto = fetchedLevels.FirstOrDefault(d => d.ParentLevelId == null);
                    lastLevelId = rootDto.Id;
                    while (true)
                    {
                        LocationLevelDto nextDto = fetchedLevels.FirstOrDefault(l => l.ParentLevelId == lastLevelId);
                        if (nextDto != null)
                        {
                            levelList.Add(nextDto);
                            lastLevelId = nextDto.Id;
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                // get the mapping list of this export type
                LocationHierarchyImportMappingList mappingList =
                    LocationHierarchyImportMappingList.NewList(levelList.Select(l => l.Name));

                // create a new workbook to hold the export
                Workbook workbook = new Workbook();
                Worksheet workSheet = workbook.Worksheets[0];

                //write all headers to file
                for (Int32 i = 0; i < mappingList.Count; i++)
                {
                    ImportMapping mapping = mappingList[i];
                    workSheet.Cells[0, i].Value = mapping.PropertyName;
                }

                if (this.ExportHeadersOnly)
                {
                    //Apply a text style format to each column
                    Style textDataTypeStyle = new Style();
                    textDataTypeStyle.Number = 49;
                    StyleFlag flag = new StyleFlag();
                    flag.All = true;

                    // SetColumn Data Formats
                    for (Int32 i = 0; i < mappingList.Count; i++)
                    {
                        ImportMapping mapping = mappingList[i];
                        workSheet.Cells.Columns[i].ApplyStyle(textDataTypeStyle, flag);
                    }
                }
                else
                {
                    //get the hierarchy groups to be exported
                    List<LocationGroupDto> groupList;
                    using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                    {
                        groupList = dal.FetchByLocationHierarchyId(currentHierarchy.Id).ToList();
                    }
                    LocationGroupDto rootDto = groupList.FirstOrDefault(g => g.ParentGroupId == null);
                    groupList.Remove(rootDto);

                    // now run through and write all the groups to the file
                    Int32 row = 1;
                    foreach (LocationGroupDto group in groupList)
                    {
                        //If the group is lowest level write to file
                        if (group.LocationLevelId == lastLevelId)
                        {
                            //cycle through column mappings
                            for (Int32 i = 0; i < mappingList.Count; i++)
                            {
                                ImportMapping mapping = mappingList[i];

                                //get the value to be exported
                                Object cellValue =
                                    mappingList.GetValueByMappingId(mapping.PropertyIdentifier, group, groupList, rootDto.Id);

                                //write to the worksheet
                                WpfHelper.SetColumnValue(workSheet.Cells[row, i], (cellValue != null) ? cellValue.ToString() : null);
                            }

                            row++;
                        }
                    }
                }

                workbook.Save(this.FileName);

                Trace.TraceInformation("{0} location hierarchy records exported successfuly.", workSheet.Cells.Rows.Count);
            }
        }
        #endregion

    }
}
