﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportLocationProductLegalProcess
    {
        #region Constants

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} Location Product Legal started.", this.ValidRowsCount);

            ImportLocationProductLegals();

            Trace.TraceInformation("Import of {0} Location Product Legal complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Main Import
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportLocationProductLegals()
        {
            //get a list of all mappings that need to be processed
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            //now perform the import
            //don't dispose of the dal factory instances fetched from the dalcontainer
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                EntityDto currentEntity = null;
                using (IEntityDal dal = dalContext.GetDal<IEntityDal>())
                {
                    currentEntity = dal.FetchById(this.EntityId);
                }

                //Create dictionary to lookup location id
                Dictionary<String, Int16> locationIdLookupDictionary = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> locationInfoDtos = dal.FetchByEntityId(this.EntityId);

                    //Add codes to dictionary for lookup later on
                    foreach (LocationInfoDto info in locationInfoDtos)
                    {
                        if (!locationIdLookupDictionary.ContainsKey(info.Code))
                        {
                            locationIdLookupDictionary.Add(info.Code, info.Id);
                        }
                    }

                    locationInfoDtos = null;
                }

                //Create dictionary to lookup product id
                Dictionary<String, Int32> productIdLookupDictionary = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);
                using (IProductInfoDal dal = dalContext.GetDal<IProductInfoDal>())
                {
                    IEnumerable<ProductInfoDto> productInfoDtos = dal.FetchByEntityId(this.EntityId);

                    //Add GTIN's to dictionary for lookup later on
                    foreach (ProductInfoDto info in productInfoDtos)
                    {
                        if (!productIdLookupDictionary.ContainsKey(info.Gtin))
                        {
                            productIdLookupDictionary.Add(info.Gtin, info.Id);
                        }
                    }

                    productInfoDtos = null;
                }

                //now cycle through the rows
                List<LocationProductLegalDto> locationProductLegalDtoList = new List<LocationProductLegalDto>();
                Dictionary<String, Int16> parentLocationObjects = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
                Dictionary<String, Int32> parentProductObjects = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);

                foreach (ImportFileDataRow row in this.ValidRows)
                {
                    //create the insert dtos
                    LocationProductLegalDto locationProductLegalDto = new LocationProductLegalDto();
                    locationProductLegalDto.DateCreated = DateTime.UtcNow;
                    locationProductLegalDto.DateLastModified = DateTime.UtcNow;
                    locationProductLegalDtoList.Add(locationProductLegalDto);

                    //entityId
                    locationProductLegalDto.EntityId = this.EntityId;

                    String locationCode = row[LocationProductLegalImportMappingList.LocationCodeMapId].ToString();

                    if (parentLocationObjects.ContainsKey(locationCode))
                    {
                        locationProductLegalDto.LocationId = parentLocationObjects[locationCode];
                    }
                    else
                    {
                        //Lookup location code in dictionary
                        Int16 locationId;
                        if (locationIdLookupDictionary.TryGetValue(locationCode, out locationId))
                        {
                            locationProductLegalDto.LocationId = locationId;
                            parentLocationObjects.Add(locationCode, locationId);
                        }
                    }

                    String productGTIN = row[LocationProductLegalImportMappingList.ProductGTINMapId].ToString();

                    if (parentProductObjects.ContainsKey(productGTIN))
                    {
                        locationProductLegalDto.ProductId = parentProductObjects[productGTIN];
                    }
                    else
                    {
                        //Lookup product gtin in dictionary
                        Int32 productId;
                        if (productIdLookupDictionary.TryGetValue(productGTIN, out productId))
                        {
                            locationProductLegalDto.ProductId = productId;
                            parentProductObjects.Add(productGTIN, productId);
                        }
                    }

                    //loop through other resolved mappings
                    //IFormatProvider prov = CultureInfo.InvariantCulture;
                    //foreach (ImportMapping mapping in resolvedMappings)
                    //{
                    //    Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                    //    Object cellValue = row[columnNumber].CurrentValue;

                    //    //use mapping list method to set the value
                    //    LocationProductLegalImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, locationProductLegalDto);
                    //}
                }

                if (locationProductLegalDtoList.Count > 0)
                {
                    //upsert all the locationproductLegal list
                    using (ILocationProductLegalDal dal = dalContext.GetDal<ILocationProductLegalDal>())
                    {
                        dal.Upsert(locationProductLegalDtoList);
                    }

                    //clear out the lists
                    locationProductLegalDtoList.Clear();
                }

                dalContext.Commit();
            }
        }

        #endregion
    }
}
