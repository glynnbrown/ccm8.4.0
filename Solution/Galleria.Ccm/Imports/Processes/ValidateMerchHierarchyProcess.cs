﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-25450 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;


namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Our data management financial hierarchy validation process class
    /// </summary>
    [Serializable]
    public sealed partial class ValidateMerchandisingHierarchyProcess : ValidateProcessBase<ValidateMerchandisingHierarchyProcess>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ValidateMerchandisingHierarchyProcess(Int32 entityId, ImportFileData fileData, Int32 startingRowNumber, Boolean headersInFirstrow)
            : base(entityId, fileData, startingRowNumber, headersInFirstrow)
        {
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ValidateMerchandisingHierarchyProcess), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateMerchandisingHierarchyProcess), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ValidateMerchandisingHierarchyProcess), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ImportMerchandisingHierarchyData.ToString()));
            BusinessRules.AddRule(typeof(ValidateMerchandisingHierarchyProcess), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion
    }
}
