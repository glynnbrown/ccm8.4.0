﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// V8-25455 : J.Pickup
//  Copied over from GFS 
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using Csla;
using SmartAssembly.Attributes;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using System.Collections;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportAssortmentLocationProcess : ImportProcessBase<ImportAssortmentLocationProcess>
    {
        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} ImportAssortmentLocations started.", this.ValidRowsCount);

            ImportAssortmentLocations();

            Trace.TraceInformation("Import of {0} ImportAssortmentLocations complete.", this.ValidRowsCount);
        }

        /// <summary>
        /// Main Import
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportAssortmentLocations()
        {
            #region Load lookup data

            Dictionary<String, Int16> locationLookup = new Dictionary<String, Int16>(StringComparer.OrdinalIgnoreCase);
            Dictionary<String, Int32> assortmentLookupList = new Dictionary<String, Int32>(StringComparer.OrdinalIgnoreCase);

            //now perform the import
            //don't dispose of the dal factory instances fetched from the dalcontainer
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                // build our optimised location lookup
                IEnumerable<LocationInfoDto> locationInfoDtoList;
                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    locationInfoDtoList = dal.FetchByEntityId(this.EntityId);
                }

                foreach (LocationInfoDto locationInfoDto in locationInfoDtoList)
                {
                    locationLookup.Add(locationInfoDto.Code, locationInfoDto.Id);
                }
                locationInfoDtoList = null;

                //build our optimised assortment lookup
                List<AssortmentInfoDto> assortmentInfoDtoList;
                using (IAssortmentInfoDal dal = dalContext.GetDal<IAssortmentInfoDal>())
                {
                    assortmentInfoDtoList = dal.FetchByEntityId(this.EntityId).ToList();
                }

                foreach (AssortmentInfoDto assortmentinfo in assortmentInfoDtoList)
                {
                    if (!assortmentLookupList.ContainsKey(assortmentinfo.Name))
                    {
                        assortmentLookupList.Add(assortmentinfo.Name, assortmentinfo.Id);
                    }
                }

                assortmentInfoDtoList = null;
            }

            #endregion

            #region Get assortment location data

            Int32 assortmentNameColumnNumber = this.ImportData.GetMappedColNumber(this.ImportData.MappingList.Where(p => p.PropertyIdentifier == AssortmentLocationImportMappingList.AssortmentNameMapId).First());
            Int32 locationCodeColumnNumber = this.ImportData.GetMappedColNumber(this.ImportData.MappingList.Where(p => p.PropertyIdentifier == AssortmentLocationImportMappingList.LocationCodeMapId).First());

            Dictionary<String, List<AssortmentLocationDto>> assortmentLocationData = new Dictionary<String, List<AssortmentLocationDto>>(StringComparer.OrdinalIgnoreCase);
            List<AssortmentLocationDto> assortmentLocationList = new List<AssortmentLocationDto>();

            //Get assortment location import data
            foreach (ImportFileDataRow row in this.ValidRows)
            {
                String assortmentName = String.Empty;
                AssortmentLocationDto assortmentLocationDto = new AssortmentLocationDto();

                String locationCode = row[locationCodeColumnNumber].ToString();

                if (locationLookup.ContainsKey(locationCode))
                {
                    assortmentLocationDto.LocationId = locationLookup[locationCode];
                }

                assortmentName = row[assortmentNameColumnNumber].ToString();

                if (assortmentLookupList.ContainsKey(assortmentName))
                {
                    assortmentLocationDto.AssortmentId = assortmentLookupList[assortmentName];
                }

                // Attempt to get assortments location data
                assortmentLocationData.TryGetValue(assortmentName, out assortmentLocationList);

                if (assortmentLocationList != null)
                {
                    // add to current assortment location detail
                    assortmentLocationList.Add(assortmentLocationDto);
                }
                else
                {
                    // create new assortment product list
                    assortmentLocationList = new List<AssortmentLocationDto>();
                    assortmentLocationList.Add(assortmentLocationDto);
                }

                assortmentLocationData[assortmentName] = assortmentLocationList;
            }

            #endregion

            #region Import assortment locations

            // import assortment locations
            foreach (String assortmentName in assortmentLocationData.Keys)
            {
                Assortment assortment = null;

                if (assortmentLookupList.ContainsKey(assortmentName))
                {
                    assortment = Assortment.GetById(assortmentLookupList[assortmentName]);
                }

                if (assortment != null)
                {
                    // Get assortment locations
                    List<AssortmentLocationDto> assortmentLocations = assortmentLocationData[assortmentName];
                    assortment.Locations.Clear();

                    foreach (AssortmentLocationDto assortmentLocationDto in assortmentLocations)
                    {
                        AssortmentLocation assortmentLocation = AssortmentLocation.NewAssortmentLocation(assortmentLocationDto.LocationId);

                        assortmentLocation.LocationId = assortmentLocationDto.LocationId;

                        assortment.Locations.Add(assortmentLocation);
                    }

                    assortment.Save();
                }
            }

            #endregion

            assortmentLocationData.Clear();
        }

        #endregion
    }
}