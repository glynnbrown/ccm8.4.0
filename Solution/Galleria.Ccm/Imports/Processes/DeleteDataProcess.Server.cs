﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25450 : L.Ineson
//	Copied from SA
// CCM-25445 : L.Ineson
//  Added loc hierarchy delete
// CCM-25452 : N.Haywood
//  Added product deletion
// CCM-25446 : N.Haywood
//  Added location product attributes
// CCM-25447 : N.Haywood
//  Added LocationProductIllegal
// CCM-25455 : J.Pickup
//  Added Assortment
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Framework.Imports;
using Galleria.Framework.Processes;
using SmartAssembly.Attributes;
using System.Diagnostics;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Collections.Generic;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class DeleteDataProcess
    {
        #region Execute

        /// <summary>
        /// Called when executing this process
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Delete process started.");

            switch (this.DataType)
            {
                case (CCMDataItemType.MerchandisingHierarchy):
                    DeleteMerchandisingHierarchy(this.EntityId);
                    break;
                case CCMDataItemType.Location:
                    DeleteLocations(this.EntityId);
                    break;
                case CCMDataItemType.Product:
                    DeleteProducts(this.EntityId);
                    break;
                case (CCMDataItemType.LocationHierarchy):
                    DeleteLocationHierarchy();
                    break;
                case CCMDataItemType.LocationClusters:
                    DeleteClusters(this.EntityId);
                    break;
                case CCMDataItemType.LocationSpace:
                    DeleteLocationSpace(this.EntityId);
                    break;
                case CCMDataItemType.LocationSpaceBay:
                    DeleteLocationSpaceBay(this.EntityId);
                    break;
                case CCMDataItemType.LocationSpaceElement:
                    DeleteLocationSpaceElement(this.EntityId);
                    break;
                case CCMDataItemType.LocationProductAttributes:
                    DeleteLocationProductAttribute(this.EntityId);
                    break;
                case CCMDataItemType.LocationProductIllegal:
                    DeleteLocationProductIllegal(this.EntityId);
                    break;
                case CCMDataItemType.LocationProductLegal:
                    DeleteLocationProductLegal(this.EntityId);
                    break;
                case CCMDataItemType.Assortment:
                    DeleteAssortment(this.EntityId);
                    break;

                default: throw new NotImplementedException();
            }

        }

        #endregion

        #region Data Methods


        /// <summary>
        /// Deletes all product groups in the merchandising hierarchy
        /// except for the root
        /// </summary>
        private static void DeleteMerchandisingHierarchy(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //get the entity dto
                ProductHierarchyDto hierarchyDto;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    hierarchyDto =
                    dal.FetchByEntityId(entityId);
                }

                ProductLevelDto levelDto;
                using (IProductLevelDal dal = dalContext.GetDal<IProductLevelDal>())
                {
                    levelDto =
                    dal.FetchByProductHierarchyId(hierarchyDto.Id)
                    .First(l => l.ParentLevelId == null);
                }

                //get all the existing hierarchy dtos
                List<ProductGroupDto> groupDtoList;
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    groupDtoList = dal.FetchByProductHierarchyId(hierarchyDto.Id).ToList();
                }

                //get the root
                ProductGroupDto rootGroup = groupDtoList.First(g => g.ParentGroupId == null);
                Int32 rootId = rootGroup.Id;

                ////assign all products for the entity to the root
                //using (IProductDal dal = dalContext.GetDal<IProductDal>())
                //{
                //    foreach (ProductDto dto in dal.FetchByEntityId(entityId))
                //    {
                //        dto.ProductGroupId = rootId;
                //        dal.Update(dto); // should probably put in a stored proc to make this faster
                //    }
                //}

                //if (deleteExistingProductUniverses != null)
                //{
                //    if (deleteExistingProductUniverses.Value)
                //    {
                //        //delete all product universes
                //        using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                //        {
                //            dal.DeleteByEntityId(entityId);
                //        }

                //        //delete all ROM product universes
                //        using (IRomProductUniverseDal dal = dalContext.GetDal<IRomProductUniverseDal>())
                //        {
                //            dal.DeleteByEntityId(entityId);
                //        }
                //    }
                //    else
                //    {
                //        //assign all product universes to the root group
                //        using (IProductUniverseDal dal = dalContext.GetDal<IProductUniverseDal>())
                //        {
                //            foreach (ProductUniverseDto dto in dal.FetchByEntityId(entityId))
                //            {
                //                dto.ProductGroupId = rootId;
                //                dal.Update(dto); // should probably put in a stored proc to make this faster
                //            }
                //        }
                //    }
                //}

                //delete all other groups
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    foreach (ProductGroupDto dto in
                        groupDtoList.Where(d => d.Id != rootId))
                    {
                        dal.DeleteById(dto.Id);
                    }
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Deletes all groups in the location hierarchy
        /// then re adds a root.
        /// </summary>
        private void DeleteLocationHierarchy()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                LocationHierarchyDto hierarchyDto;
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    hierarchyDto =
                    dal.FetchByEntityId(this.EntityId);
                }

                LocationLevelDto levelDto;
                using (ILocationLevelDal dal = dalContext.GetDal<ILocationLevelDal>())
                {
                    levelDto =
                    dal.FetchByLocationHierarchyId(hierarchyDto.Id)
                    .First(l => l.ParentLevelId == null);
                }

                //get all the existing hierarchy dtos
                List<LocationGroupDto> groupDtoList;
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    groupDtoList = dal.FetchByLocationHierarchyId(hierarchyDto.Id).ToList();
                }

                //get the root
                LocationGroupDto rootGroup = groupDtoList.First(g => g.ParentGroupId == null);
                Int32 rootId = rootGroup.Id;

                //assign all products for the entity to the root
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    foreach (LocationDto dto in dal.FetchByEntityId(this.EntityId))
                    {
                        dto.LocationGroupId = rootId;
                        dal.Update(dto); // should probably put in a stored proc to make this faster
                    }
                }

                //delete all other groups
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    foreach (LocationGroupDto dto in
                        groupDtoList.Where(d => d.Id != rootId))
                    {
                        dal.DeleteById(dto.Id);
                    }
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Deletes all products for the current entity
        /// </summary>
        /// <param name="dalFactory"></param>
        private static void DeleteProducts(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //Delete all products for this entity id
                using (IProductDal dal = dalContext.GetDal<IProductDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Deletes all locations for the current entity
        /// </summary>
        private static void DeleteLocations(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //Delete all locations for this entityid
                using (ILocationDal dal = dalContext.GetDal<ILocationDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Deletes all clusters for the current entity
        /// </summary>
        private static void DeleteClusters(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //Delete all cluster schemes
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Delets all location space and parent location space records
        /// </summary>
        private static void DeleteLocationSpace(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //Delete all location space plus child & parent objects up to location space for this entityid
                using (ILocationSpaceDal dal = dalContext.GetDal<ILocationSpaceDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Delets all location space bay and parent location space records
        /// </summary>
        private static void DeleteLocationSpaceBay(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //Delete all location space bay plus child & parent objects up to location space for this entityid
                using (ILocationSpaceBayDal dal = dalContext.GetDal<ILocationSpaceBayDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Delets all location space element records
        /// </summary>
        private static void DeleteLocationSpaceElement(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //Delete all location space bay plus child & parent objects up to location space for this entityid
                using (ILocationSpaceElementDal dal = dalContext.GetDal<ILocationSpaceElementDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Delets all location product attributes
        /// </summary>
        private static void DeleteLocationProductAttribute(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Delets all location product illegal
        /// </summary>
        private static void DeleteLocationProductIllegal(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (ILocationProductIllegalDal dal = dalContext.GetDal<ILocationProductIllegalDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Delets all location product Legal
        /// </summary>
        private static void DeleteLocationProductLegal(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (ILocationProductLegalDal dal = dalContext.GetDal<ILocationProductLegalDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }


        /// <summary>
        /// Deletes all assortments
        /// </summary>
        private static void DeleteAssortment(Int32 entityId)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (IAssortmentDal dal = dalContext.GetDal<IAssortmentDal>())
                {
                    dal.DeleteByEntityId(entityId);
                }

                dalContext.Commit();
            }
        }

        #endregion
    }
}
