﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-25452 : N.Haywood
//	Added from SA
// V8-26333 : A.Probyn
//  Extended to cover custom attributes on the import
#endregion
#region Version History: CCM802
// V8-29230 : N.Foster
//  Removed unused parameter from import products process
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Imports.Mappings;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Imports;
using Galleria.Framework.Planograms.Dal.DataTransferObjects;
using Galleria.Framework.Planograms.Dal.Interfaces;
using Galleria.Framework.Processes;
using SmartAssembly.Attributes;

namespace Galleria.Ccm.Imports.Processes
{
    public partial class ImportProductsProcess
    {
        #region Constants
        private const int _commitBatchSize = 100000; // the number of records to commit in a single batch
        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Import of {0} products started.", this.ImportData.Rows.Count);

            ImportProducts();
            UpdateProductUniverses(this.EntityId);

            Trace.TraceInformation("Import of {0} products complete.", this.ImportData.Rows.Count);
        }


        /// <summary>
        /// Does the import.
        /// </summary>
        [ObfuscateControlFlow]
        private void ImportProducts()
        {
            Int32 entityId = this.EntityId;
            //Boolean deleteNewGroup = false;
            //get a list of all mappings that need to be processed
            IEnumerable<ImportMapping> resolvedMappings = this.ImportData.MappingList.Where(m => m.IsColumnReferenceSet);

            // now perform the import
            // ISO-13449: Don't Dispose of DalFactory instances fetched from the DalContainer.
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                //create a product group to which all these new products can be assigned
                ProductHierarchyDto currentProductHierarchy = null;
                using (IProductHierarchyDal dal = dalContext.GetDal<IProductHierarchyDal>())
                {
                    currentProductHierarchy = dal.FetchByEntityId(entityId);
                }

                //now cycle through rows 
                List<ProductDto> productDtoList = new List<ProductDto>();
                List<CustomAttributeDataDto> productAttributeDtoList = new List<CustomAttributeDataDto>();

                //Setup isSetDto 
                ProductIsSetDto productIsSetDto = new ProductIsSetDto();
                CustomAttributeDataIsSetDto customAttributeIsSetDto = new CustomAttributeDataIsSetDto();

                //Get list of existing groups
                IEnumerable<ProductGroupDto> productGroups = null;
                using (IProductGroupDal dal = dalContext.GetDal<IProductGroupDal>())
                {
                    productGroups = dal.FetchByProductHierarchyId(currentProductHierarchy.Id);
                }
                Dictionary<String, Int32> productGroupCodeToIdLookup = productGroups.ToDictionary(g => g.Code, g => g.Id, StringComparer.OrdinalIgnoreCase);

                foreach (ImportFileDataRow row in this.ImportData.Rows)
                {
                    //create the insert dtos
                    ProductDto productDto = new ProductDto();
                    productDtoList.Add(productDto);

                    CustomAttributeDataDto customAttributeDataDto = new CustomAttributeDataDto();
                    productAttributeDtoList.Add(customAttributeDataDto);

                    //entity Id
                    productDto.EntityId = entityId;

                    //loop through other resolved mappings
                    foreach (ImportMapping mapping in resolvedMappings)
                    {
                        Int32 columnNumber = this.ImportData.GetMappedColNumber(mapping);
                        Object cellValue = row[columnNumber].Value;

                        if (mapping.PropertyIdentifier == ProductImportMappingList.GroupCodeMapId)
                        {
                            if (cellValue.ToString() != String.Empty && cellValue != DBNull.Value)
                            {
                                Int32 groupId;
                                if (productGroupCodeToIdLookup.TryGetValue(cellValue.ToString(), out groupId))
                                {
                                    productDto.ProductGroupId = groupId; 
                                }
                            }
                        }
                        else
                        {

                            // [GFS-13815] Corrected null value checks
                            cellValue = ProcessHelpers.CheckNullCellValues(mapping, cellValue);

                            //use the mapping list method to set the value
                            ProductImportMappingList.SetValueByMappingId(mapping.PropertyIdentifier, cellValue, productDto, customAttributeDataDto);
                        }

                        //Set isSetDto accordingly
                        ProductImportMappingList.SetIsSetPropertyByMappingId(mapping.PropertyIdentifier, productIsSetDto, customAttributeIsSetDto);
                    }
                }


                if (productDtoList.Count > 0)
                {
                    //upsert all the productlist
                    using (IProductDal dal = dalContext.GetDal<IProductDal>())
                    {
                        dal.Upsert(productDtoList, productIsSetDto);
                    }

                    //update product ids to the custom attribute dtos
                    for (Int32 i = 0; i < productDtoList.Count; i++)
                    {
                        ProductDto productDto = productDtoList[i];
                        CustomAttributeDataDto attributeDto = productAttributeDtoList[i];
                        attributeDto.ParentId = productDto.Id;
                        attributeDto.ParentType = (Byte)CustomAttributeDataParentType.Product;
                    }

                    //upsert all product attribute data dtos
                    using (ICustomAttributeDataDal dal = dalContext.GetDal<ICustomAttributeDataDal>())
                    {
                        dal.Upsert(productAttributeDtoList, customAttributeIsSetDto);
                    }

                    //clear out the lists
                    productDtoList.Clear();
                    productAttributeDtoList.Clear();
                }

                dalContext.Commit();
            }

        }

        /// <summary>
        /// Updates/creates libraries for new/updated products.
        /// </summary>
        [ObfuscateControlFlow]
        private static void UpdateProductUniverses(Int32 entityId)
        {
            ProcessFactory.Execute<Galleria.Ccm.Imports.Processes.UpdateProductUniverseProcess>(
                new Galleria.Ccm.Imports.Processes.UpdateProductUniverseProcess(entityId));
        }
        #endregion
    }
}
