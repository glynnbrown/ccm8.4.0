﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25444 : N.Haywood
//	Copied from SA
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;
using SmartAssembly.Attributes;
using Galleria.Framework.Imports;
using Galleria.Ccm.Imports.Mappings;
using System.Diagnostics;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using System.Globalization;

namespace Galleria.Ccm.Imports.Processes
{
    /// <summary>
    /// Data management location validation process class
    /// </summary>
    public partial class ValidateLocationsProcess
    {
        #region Fields

        private HashSet<String> _validationParentLocationTypeObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationParentObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationImportActionObjects = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationIdentifierFields = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationDuplicates = new HashSet<String>(StringComparer.OrdinalIgnoreCase);
        private HashSet<String> _validationDuplicateDeletedItems = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

        #endregion

        #region Properties
        /// <summary>
        /// Get/Set Validation Parent Location Type Objects
        /// </summary>
        public HashSet<String> ValidationParentLocationTypeObjects
        {
            get { return _validationParentLocationTypeObjects; }
            set { _validationParentLocationTypeObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Parent Objects
        /// </summary>
        public HashSet<String> ValidationParentObjects
        {
            get { return _validationParentObjects; }
            set { _validationParentObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Import Action Objects
        /// </summary>
        public HashSet<String> ValidationImportActionObjects
        {
            get { return _validationImportActionObjects; }
            set { _validationImportActionObjects = value; }
        }
        /// <summary>
        /// Get/Set Validation Identifier Fields
        /// </summary>
        public HashSet<String> ValidationIdentifierFields
        {
            get { return _validationIdentifierFields; }
            set { _validationIdentifierFields = value; }
        }
        /// <summary>
        /// Get/Set Validation Duplicates
        /// </summary>
        public HashSet<String> ValidationDuplicates
        {
            get { return _validationDuplicates; }
            set { _validationDuplicates = value; }
        }

        /// <summary>
        /// Gets the location codes of items in the database that match the codes of the
        /// items being imported, but which are already deleted - these should not be able to be deleted
        /// </summary>
        public HashSet<String> ValidationDuplicateDeletedItems
        {
            get { return _validationDuplicateDeletedItems; }
        }

        /// <summary>
        /// Local property for the location code mapping
        /// </summary>
        private ImportMapping locationCodeMappingForImportType;

        #endregion

        #region Methods

        /// <summary>
        /// Our main process execution method
        /// </summary>
        [ObfuscateControlFlow]
        protected override void OnExecute()
        {
            Trace.TraceInformation("Validation Started.");

            //Set location code mapping
            locationCodeMappingForImportType = this.FileData.MappingList.Where(c => c.PropertyIdentifier == LocationImportMappingList.CodeMapId).FirstOrDefault();

            //Setup datatable
            ImportFileData worksheetData = this.FileData;

            Trace.TraceInformation("{0} location's being validated", worksheetData.Rows.Count);

            #region Fetch Relational Check Objects

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //start a transaction for this data validation
                dalContext.Begin();

                //get code mapping 
                ImportMapping locationCodeMapping = this.FileData.MappingList.FirstOrDefault(c => c.PropertyIdentifier == LocationImportMappingList.CodeMapId);
                Int32 mappedColumnNumber = this.FileData.GetMappedColNumber(locationCodeMapping);

                //create lists to store location codes
                HashSet<String> locationCodes = new HashSet<String>(StringComparer.OrdinalIgnoreCase);

                foreach (ImportFileDataRow row in worksheetData.Rows)
                {
                    //Build up collection of codes
                    String locationCode = Convert.ToString(row[mappedColumnNumber], CultureInfo.InvariantCulture);

                    if (locationCode != null)
                    {
                        //[GFS-13847] cut down the code to its max length
                        if (locationCode.Length > locationCodeMapping.PropertyMax)
                        {
                            locationCode = locationCode.Substring(0, Convert.ToInt32(locationCodeMapping.PropertyMax));
                        }

                        locationCodes.Add(locationCode);

                        //While looping also find duplicates
                        if (this.ValidationIdentifierFields.Contains(locationCode))
                        {
                            this.ValidationDuplicates.Add(locationCode);
                        }
                        else
                        {
                            this.ValidationIdentifierFields.Add(locationCode);
                        }
                    }
                }
                Int32 entityId = this.EntityId;
                //Add possible parent codes to hashset for faster searching later
                Int32 hierarchyId;
                using (ILocationHierarchyDal dal = dalContext.GetDal<ILocationHierarchyDal>())
                {
                    hierarchyId = dal.FetchByEntityId(entityId).Id;
                }
                using (ILocationGroupDal dal = dalContext.GetDal<ILocationGroupDal>())
                {
                    List<LocationGroupDto> groups = dal.FetchByLocationHierarchyId(hierarchyId).ToList();
                    foreach (LocationGroupDto group in groups)
                    {
                        ValidationParentObjects.Add(group.Code);
                    }
                }

                //now retrieve all locations that match the location codes
                //to inform the user of the import action that will take place

                using (ILocationInfoDal dal = dalContext.GetDal<ILocationInfoDal>())
                {
                    IEnumerable<LocationInfoDto> locationInfoDtos =
                        dal.FetchByEntityIdLocationCodes(entityId, locationCodes.Distinct());

                    //Add codes to hashset for faster searching later on
                    foreach (LocationInfoDto dto in locationInfoDtos)
                    {
                        this.ValidationImportActionObjects.Add(dto.Code);

                        //Add in any items that are duplicated in the database but have been deleted
                        if (dto.DateDeleted != null)
                        {
                            if (!this.ValidationDuplicateDeletedItems.Contains(dto.Code))
                            {
                                this.ValidationDuplicateDeletedItems.Add(dto.Code);
                            }
                        }
                    }
                }
            }

            #endregion

            //validate data against mappings using generic methods
            this.RunDataTypeValidation(worksheetData);

            //update datatable to include import action and excel row number columns
            this.AddExtraColumns(worksheetData);

            //set validated data to be the updated data item and set it to be returned
            this.ValidatedData = worksheetData;

            Trace.TraceInformation("Validation of {0} locations complete.", worksheetData.Rows.Count);
        }

        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunParentRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationImportMappingList.GroupCodeMapId)
            {
                if (!this.ValidationParentObjects.Contains(columnValue.ToString()))
                {
                    //no matching location group code
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber, columnValue.ToString(),
                        Message.DataManagement_Validation_NoMatchingLocationGroup, ValidationErrorType.Error,
                        true, false, false, false, col, false);
                }
            }
            return null;
        }

        /// <summary>
        /// Overrride dupicate record validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationImportMappingList.CodeMapId)
            {
                //check whether the location code is a dupliacte of another being imported
                String columnValueString = columnValue.ToString();
                if (this.ValidationDuplicates.Contains(columnValueString))
                {
                    //duplicate found create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                        columnValueString, Message.DataManagement_Validation_DuplicateLocation,
                        ValidationErrorType.Error, true, false, false, false, col, true);
                }
            }
            return null;
        }

        /// <summary>
        /// Override duplicate deleted item record validation base method
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <param name="columnValue"></param>
        /// <returns></returns>
        [ObfuscateControlFlow]
        protected override ValidationErrorItem RunDuplicateDeleteditemRecordValidation(ImportMapping col, int rowNumber, int colNumber, object columnValue)
        {
            if (col.PropertyIdentifier == LocationImportMappingList.CodeMapId)
            {
                //Check whether the location code is a duplicate of another being imported
                String columnValueString = columnValue.ToString();
                //Check for deleted duplicates in the target database table
                if (this.ValidationDuplicateDeletedItems.Contains(columnValueString))
                {
                    //Duplicate found, create error item
                    return ValidationErrorItem.NewValidationErrorItem(rowNumber, colNumber,
                        columnValueString, Message.DataManagement_Validation_DuplicateDeletedLocation,
                        ValidationErrorType.Error, true, false, false, false, col, true);
                }
            }
            return null;
        }

        /// <summary>
        /// Override import type validation base method
        /// </summary>
        [ObfuscateControlFlow]
        protected override string RunImportTypeValidation(ImportFileDataRow rowData)
        {
            Int32 colNumber = this.FileData.GetMappedColNumber(locationCodeMappingForImportType);

            if (colNumber != 0)
            {
                //get mapping -> this is set when class is constructed
                //extract the location from the row data using this index
                String locationCode = (rowData[colNumber].Value != null) ? rowData[colNumber].Value.ToString() : null;

                //Check whether the location code matches the location in the database
                if (!this.ValidationImportActionObjects.Contains(locationCode))
                {
                    //if the location code is new , but a duplicate record is also being imported, the first 
                    //record will be inserted, all others will but updates
                    this.ValidationImportActionObjects.Add(locationCode);
                    return Message.DataManagement_ImportType_Add;
                }
                else
                {
                    //otherwise it is an update
                    return Message.DataManagement_ImportType_Update;
                }
            }
            else
            {
                //[TODO] work out what should be done if this happens
                return Message.DataManagement_ImportType_Update;
            }
        }
        #endregion
    }
}
