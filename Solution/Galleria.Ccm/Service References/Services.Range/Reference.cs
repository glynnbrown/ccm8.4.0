﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Galleria.Ccm.Services.Range {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="RangeLive", Namespace="http://www.galleria-rts.com/foundation/services/2012/01")]
    [System.SerializableAttribute()]
    public partial class RangeLive : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string ProductGroupCodeField;
        
        private string LocationCodeField;
        
        private string ProductGtinField;
        
        private System.Nullable<short> UnitsField;
        
        private System.Nullable<short> FacingsField;
        
        private System.DateTime DateLiveField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string ProductGroupCode {
            get {
                return this.ProductGroupCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ProductGroupCodeField, value) != true)) {
                    this.ProductGroupCodeField = value;
                    this.RaisePropertyChanged("ProductGroupCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public string LocationCode {
            get {
                return this.LocationCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.LocationCodeField, value) != true)) {
                    this.LocationCodeField = value;
                    this.RaisePropertyChanged("LocationCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public string ProductGtin {
            get {
                return this.ProductGtinField;
            }
            set {
                if ((object.ReferenceEquals(this.ProductGtinField, value) != true)) {
                    this.ProductGtinField = value;
                    this.RaisePropertyChanged("ProductGtin");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public System.Nullable<short> Units {
            get {
                return this.UnitsField;
            }
            set {
                if ((this.UnitsField.Equals(value) != true)) {
                    this.UnitsField = value;
                    this.RaisePropertyChanged("Units");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public System.Nullable<short> Facings {
            get {
                return this.FacingsField;
            }
            set {
                if ((this.FacingsField.Equals(value) != true)) {
                    this.FacingsField = value;
                    this.RaisePropertyChanged("Facings");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public System.DateTime DateLive {
            get {
                return this.DateLiveField;
            }
            set {
                if ((this.DateLiveField.Equals(value) != true)) {
                    this.DateLiveField = value;
                    this.RaisePropertyChanged("DateLive");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", ConfigurationName="Services.Range.RangeService")]
    public interface RangeService {
        
        // CODEGEN: Generating message contract since the wrapper name (GetRangeLiveByEntityNameProductGroupCodeRequest) of message GetRangeLiveByEntityNameProductGroupCodeRequest does not match the default value (GetRangeLiveByEntityNameProductGroupCode)
        [System.ServiceModel.OperationContractAttribute(Action="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameProductGroupCode", ReplyAction="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameProductGroupCodeResponse")]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameProductGroupCodeResponse GetRangeLiveByEntityNameProductGroupCode(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameProductGroupCodeRequest request);
        
        // CODEGEN: Generating message contract since the wrapper name (GetRangeLiveByEntityNameLocationCodeDateAddedRequest) of message GetRangeLiveByEntityNameLocationCodeDateAddedRequest does not match the default value (GetRangeLiveByEntityNameLocationCodeDateAdded)
        [System.ServiceModel.OperationContractAttribute(Action="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameLocationCodeDateAdded", ReplyAction="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameLocationCodeDateAddedResponse")]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateAddedResponse GetRangeLiveByEntityNameLocationCodeDateAdded(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateAddedRequest request);
        
        // CODEGEN: Generating message contract since the wrapper name (GetRangeLiveByEntityNameLocationCodeDateChangedRequest) of message GetRangeLiveByEntityNameLocationCodeDateChangedRequest does not match the default value (GetRangeLiveByEntityNameLocationCodeDateChanged)
        [System.ServiceModel.OperationContractAttribute(Action="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameLocationCodeDateChanged", ReplyAction="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameLocationCodeDateChangedResponse")]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateChangedResponse GetRangeLiveByEntityNameLocationCodeDateChanged(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateChangedRequest request);
        
        // CODEGEN: Generating message contract since the wrapper name (GetRangeLiveByEntityNameLocationCodeDateDeletedRequest) of message GetRangeLiveByEntityNameLocationCodeDateDeletedRequest does not match the default value (GetRangeLiveByEntityNameLocationCodeDateDeleted)
        [System.ServiceModel.OperationContractAttribute(Action="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameLocationCodeDateDeleted", ReplyAction="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameLocationCodeDateDeletedResponse")]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateDeletedResponse GetRangeLiveByEntityNameLocationCodeDateDeleted(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateDeletedRequest request);
        
        // CODEGEN: Generating message contract since the wrapper name (GetRangeLiveByEntityNameLocationCodeRequest) of message GetRangeLiveByEntityNameLocationCodeRequest does not match the default value (GetRangeLiveByEntityNameLocationCode)
        [System.ServiceModel.OperationContractAttribute(Action="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameLocationCode", ReplyAction="http://www.galleria-rts.com/foundation/services/2012/01/RangeService/GetRangeLive" +
            "ByEntityNameLocationCodeResponse")]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeResponse GetRangeLiveByEntityNameLocationCode(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameProductGroupCodeRequest", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameProductGroupCodeRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public string EntityName;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=1)]
        public string ProductGroupCode;
        
        public GetRangeLiveByEntityNameProductGroupCodeRequest() {
        }
        
        public GetRangeLiveByEntityNameProductGroupCodeRequest(string EntityName, string ProductGroupCode) {
            this.EntityName = EntityName;
            this.ProductGroupCode = ProductGroupCode;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameProductGroupCodeResponse", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameProductGroupCodeResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges;
        
        public GetRangeLiveByEntityNameProductGroupCodeResponse() {
        }
        
        public GetRangeLiveByEntityNameProductGroupCodeResponse(System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges) {
            this.LiveRanges = LiveRanges;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameLocationCodeDateAddedRequest", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameLocationCodeDateAddedRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public string EntityName;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=1)]
        public string LocationCode;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=2)]
        public System.DateTime AddedDate;
        
        public GetRangeLiveByEntityNameLocationCodeDateAddedRequest() {
        }
        
        public GetRangeLiveByEntityNameLocationCodeDateAddedRequest(string EntityName, string LocationCode, System.DateTime AddedDate) {
            this.EntityName = EntityName;
            this.LocationCode = LocationCode;
            this.AddedDate = AddedDate;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameLocationCodeDateAddedResponse", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameLocationCodeDateAddedResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges;
        
        public GetRangeLiveByEntityNameLocationCodeDateAddedResponse() {
        }
        
        public GetRangeLiveByEntityNameLocationCodeDateAddedResponse(System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges) {
            this.LiveRanges = LiveRanges;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameLocationCodeDateChangedRequest", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameLocationCodeDateChangedRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public string EntityName;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=1)]
        public string LocationCode;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=2)]
        public System.DateTime ChangeDate;
        
        public GetRangeLiveByEntityNameLocationCodeDateChangedRequest() {
        }
        
        public GetRangeLiveByEntityNameLocationCodeDateChangedRequest(string EntityName, string LocationCode, System.DateTime ChangeDate) {
            this.EntityName = EntityName;
            this.LocationCode = LocationCode;
            this.ChangeDate = ChangeDate;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameLocationCodeDateChangedResponse", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameLocationCodeDateChangedResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges;
        
        public GetRangeLiveByEntityNameLocationCodeDateChangedResponse() {
        }
        
        public GetRangeLiveByEntityNameLocationCodeDateChangedResponse(System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges) {
            this.LiveRanges = LiveRanges;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameLocationCodeDateDeletedRequest", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameLocationCodeDateDeletedRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public string EntityName;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=1)]
        public string LocationCode;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=2)]
        public System.Nullable<System.DateTime> DeletedDate;
        
        public GetRangeLiveByEntityNameLocationCodeDateDeletedRequest() {
        }
        
        public GetRangeLiveByEntityNameLocationCodeDateDeletedRequest(string EntityName, string LocationCode, System.Nullable<System.DateTime> DeletedDate) {
            this.EntityName = EntityName;
            this.LocationCode = LocationCode;
            this.DeletedDate = DeletedDate;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameLocationCodeDateDeletedResponse", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameLocationCodeDateDeletedResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges;
        
        public GetRangeLiveByEntityNameLocationCodeDateDeletedResponse() {
        }
        
        public GetRangeLiveByEntityNameLocationCodeDateDeletedResponse(System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges) {
            this.LiveRanges = LiveRanges;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameLocationCodeRequest", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameLocationCodeRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public string EntityName;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=1)]
        public string LocationCode;
        
        public GetRangeLiveByEntityNameLocationCodeRequest() {
        }
        
        public GetRangeLiveByEntityNameLocationCodeRequest(string EntityName, string LocationCode) {
            this.EntityName = EntityName;
            this.LocationCode = LocationCode;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetRangeLiveByEntityNameLocationCodeResponse", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetRangeLiveByEntityNameLocationCodeResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges;
        
        public GetRangeLiveByEntityNameLocationCodeResponse() {
        }
        
        public GetRangeLiveByEntityNameLocationCodeResponse(System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> LiveRanges) {
            this.LiveRanges = LiveRanges;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface RangeServiceChannel : Galleria.Ccm.Services.Range.RangeService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RangeServiceClient : System.ServiceModel.ClientBase<Galleria.Ccm.Services.Range.RangeService>, Galleria.Ccm.Services.Range.RangeService {
        
        public RangeServiceClient() {
        }
        
        public RangeServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public RangeServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RangeServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public RangeServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameProductGroupCodeResponse Galleria.Ccm.Services.Range.RangeService.GetRangeLiveByEntityNameProductGroupCode(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameProductGroupCodeRequest request) {
            return base.Channel.GetRangeLiveByEntityNameProductGroupCode(request);
        }
        
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> GetRangeLiveByEntityNameProductGroupCode(string EntityName, string ProductGroupCode) {
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameProductGroupCodeRequest inValue = new Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameProductGroupCodeRequest();
            inValue.EntityName = EntityName;
            inValue.ProductGroupCode = ProductGroupCode;
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameProductGroupCodeResponse retVal = ((Galleria.Ccm.Services.Range.RangeService)(this)).GetRangeLiveByEntityNameProductGroupCode(inValue);
            return retVal.LiveRanges;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateAddedResponse Galleria.Ccm.Services.Range.RangeService.GetRangeLiveByEntityNameLocationCodeDateAdded(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateAddedRequest request) {
            return base.Channel.GetRangeLiveByEntityNameLocationCodeDateAdded(request);
        }
        
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> GetRangeLiveByEntityNameLocationCodeDateAdded(string EntityName, string LocationCode, System.DateTime AddedDate) {
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateAddedRequest inValue = new Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateAddedRequest();
            inValue.EntityName = EntityName;
            inValue.LocationCode = LocationCode;
            inValue.AddedDate = AddedDate;
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateAddedResponse retVal = ((Galleria.Ccm.Services.Range.RangeService)(this)).GetRangeLiveByEntityNameLocationCodeDateAdded(inValue);
            return retVal.LiveRanges;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateChangedResponse Galleria.Ccm.Services.Range.RangeService.GetRangeLiveByEntityNameLocationCodeDateChanged(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateChangedRequest request) {
            return base.Channel.GetRangeLiveByEntityNameLocationCodeDateChanged(request);
        }
        
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> GetRangeLiveByEntityNameLocationCodeDateChanged(string EntityName, string LocationCode, System.DateTime ChangeDate) {
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateChangedRequest inValue = new Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateChangedRequest();
            inValue.EntityName = EntityName;
            inValue.LocationCode = LocationCode;
            inValue.ChangeDate = ChangeDate;
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateChangedResponse retVal = ((Galleria.Ccm.Services.Range.RangeService)(this)).GetRangeLiveByEntityNameLocationCodeDateChanged(inValue);
            return retVal.LiveRanges;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateDeletedResponse Galleria.Ccm.Services.Range.RangeService.GetRangeLiveByEntityNameLocationCodeDateDeleted(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateDeletedRequest request) {
            return base.Channel.GetRangeLiveByEntityNameLocationCodeDateDeleted(request);
        }
        
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> GetRangeLiveByEntityNameLocationCodeDateDeleted(string EntityName, string LocationCode, System.Nullable<System.DateTime> DeletedDate) {
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateDeletedRequest inValue = new Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateDeletedRequest();
            inValue.EntityName = EntityName;
            inValue.LocationCode = LocationCode;
            inValue.DeletedDate = DeletedDate;
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeDateDeletedResponse retVal = ((Galleria.Ccm.Services.Range.RangeService)(this)).GetRangeLiveByEntityNameLocationCodeDateDeleted(inValue);
            return retVal.LiveRanges;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeResponse Galleria.Ccm.Services.Range.RangeService.GetRangeLiveByEntityNameLocationCode(Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeRequest request) {
            return base.Channel.GetRangeLiveByEntityNameLocationCode(request);
        }
        
        public System.Collections.Generic.List<Galleria.Ccm.Services.Range.RangeLive> GetRangeLiveByEntityNameLocationCode(string EntityName, string LocationCode) {
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeRequest inValue = new Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeRequest();
            inValue.EntityName = EntityName;
            inValue.LocationCode = LocationCode;
            Galleria.Ccm.Services.Range.GetRangeLiveByEntityNameLocationCodeResponse retVal = ((Galleria.Ccm.Services.Range.RangeService)(this)).GetRangeLiveByEntityNameLocationCode(inValue);
            return retVal.LiveRanges;
        }
    }
}
