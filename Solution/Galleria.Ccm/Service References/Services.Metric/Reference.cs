﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Galleria.Ccm.Services.Metric {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Metric", Namespace="http://www.galleria-rts.com/foundation/services/2012/01")]
    [System.SerializableAttribute()]
    public partial class Metric : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string NameField;
        
        private string DescriptionField;
        
        private string TypeField;
        
        private string DirectionField;
        
        private string SpecialTypeField;
        
        private string DataModelTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string CalculationField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true)]
        public string Name {
            get {
                return this.NameField;
            }
            set {
                if ((object.ReferenceEquals(this.NameField, value) != true)) {
                    this.NameField = value;
                    this.RaisePropertyChanged("Name");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=1)]
        public string Description {
            get {
                return this.DescriptionField;
            }
            set {
                if ((object.ReferenceEquals(this.DescriptionField, value) != true)) {
                    this.DescriptionField = value;
                    this.RaisePropertyChanged("Description");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=2)]
        public string Type {
            get {
                return this.TypeField;
            }
            set {
                if ((object.ReferenceEquals(this.TypeField, value) != true)) {
                    this.TypeField = value;
                    this.RaisePropertyChanged("Type");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=3)]
        public string Direction {
            get {
                return this.DirectionField;
            }
            set {
                if ((object.ReferenceEquals(this.DirectionField, value) != true)) {
                    this.DirectionField = value;
                    this.RaisePropertyChanged("Direction");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=4)]
        public string SpecialType {
            get {
                return this.SpecialTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.SpecialTypeField, value) != true)) {
                    this.SpecialTypeField = value;
                    this.RaisePropertyChanged("SpecialType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(IsRequired=true, Order=5)]
        public string DataModelType {
            get {
                return this.DataModelTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.DataModelTypeField, value) != true)) {
                    this.DataModelTypeField = value;
                    this.RaisePropertyChanged("DataModelType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute(Order=6)]
        public string Calculation {
            get {
                return this.CalculationField;
            }
            set {
                if ((object.ReferenceEquals(this.CalculationField, value) != true)) {
                    this.CalculationField = value;
                    this.RaisePropertyChanged("Calculation");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", ConfigurationName="Services.Metric.MetricService")]
    public interface MetricService {
        
        // CODEGEN: Generating message contract since the wrapper name (GetMetricByEntityNameRequest) of message GetMetricByEntityNameRequest does not match the default value (GetMetricByEntityName)
        [System.ServiceModel.OperationContractAttribute(Action="http://www.galleria-rts.com/foundation/services/2012/01/MetricService/GetMetricBy" +
            "EntityName", ReplyAction="http://www.galleria-rts.com/foundation/services/2012/01/MetricService/GetMetricBy" +
            "EntityNameResponse")]
        Galleria.Ccm.Services.Metric.GetMetricByEntityNameResponse GetMetricByEntityName(Galleria.Ccm.Services.Metric.GetMetricByEntityNameRequest request);
        
        // CODEGEN: Generating message contract since the wrapper name (GetMetricIncludingCalculatedByEntityNameRequest) of message GetMetricIncludingCalculatedByEntityNameRequest does not match the default value (GetMetricIncludingCalculatedByEntityName)
        [System.ServiceModel.OperationContractAttribute(Action="http://www.galleria-rts.com/foundation/services/2012/01/MetricService/GetMetricIn" +
            "cludingCalculatedByEntityName", ReplyAction="http://www.galleria-rts.com/foundation/services/2012/01/MetricService/GetMetricIn" +
            "cludingCalculatedByEntityNameResponse")]
        Galleria.Ccm.Services.Metric.GetMetricIncludingCalculatedByEntityNameResponse GetMetricIncludingCalculatedByEntityName(Galleria.Ccm.Services.Metric.GetMetricIncludingCalculatedByEntityNameRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetMetricByEntityNameRequest", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetMetricByEntityNameRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public string EntityName;
        
        public GetMetricByEntityNameRequest() {
        }
        
        public GetMetricByEntityNameRequest(string EntityName) {
            this.EntityName = EntityName;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetMetricByEntityNameResponse", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetMetricByEntityNameResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public System.Collections.Generic.List<Galleria.Ccm.Services.Metric.Metric> Metrics;
        
        public GetMetricByEntityNameResponse() {
        }
        
        public GetMetricByEntityNameResponse(System.Collections.Generic.List<Galleria.Ccm.Services.Metric.Metric> Metrics) {
            this.Metrics = Metrics;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetMetricIncludingCalculatedByEntityNameRequest", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetMetricIncludingCalculatedByEntityNameRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public string EntityName;
        
        public GetMetricIncludingCalculatedByEntityNameRequest() {
        }
        
        public GetMetricIncludingCalculatedByEntityNameRequest(string EntityName) {
            this.EntityName = EntityName;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="GetMetricIncludingCalculatedByEntityNameResponse", WrapperNamespace="http://www.galleria-rts.com/foundation/services/2012/01", IsWrapped=true)]
    public partial class GetMetricIncludingCalculatedByEntityNameResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://www.galleria-rts.com/foundation/services/2012/01", Order=0)]
        public System.Collections.Generic.List<Galleria.Ccm.Services.Metric.Metric> Metrics;
        
        public GetMetricIncludingCalculatedByEntityNameResponse() {
        }
        
        public GetMetricIncludingCalculatedByEntityNameResponse(System.Collections.Generic.List<Galleria.Ccm.Services.Metric.Metric> Metrics) {
            this.Metrics = Metrics;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface MetricServiceChannel : Galleria.Ccm.Services.Metric.MetricService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class MetricServiceClient : System.ServiceModel.ClientBase<Galleria.Ccm.Services.Metric.MetricService>, Galleria.Ccm.Services.Metric.MetricService {
        
        public MetricServiceClient() {
        }
        
        public MetricServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public MetricServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MetricServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public MetricServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Galleria.Ccm.Services.Metric.GetMetricByEntityNameResponse Galleria.Ccm.Services.Metric.MetricService.GetMetricByEntityName(Galleria.Ccm.Services.Metric.GetMetricByEntityNameRequest request) {
            return base.Channel.GetMetricByEntityName(request);
        }
        
        public System.Collections.Generic.List<Galleria.Ccm.Services.Metric.Metric> GetMetricByEntityName(string EntityName) {
            Galleria.Ccm.Services.Metric.GetMetricByEntityNameRequest inValue = new Galleria.Ccm.Services.Metric.GetMetricByEntityNameRequest();
            inValue.EntityName = EntityName;
            Galleria.Ccm.Services.Metric.GetMetricByEntityNameResponse retVal = ((Galleria.Ccm.Services.Metric.MetricService)(this)).GetMetricByEntityName(inValue);
            return retVal.Metrics;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        Galleria.Ccm.Services.Metric.GetMetricIncludingCalculatedByEntityNameResponse Galleria.Ccm.Services.Metric.MetricService.GetMetricIncludingCalculatedByEntityName(Galleria.Ccm.Services.Metric.GetMetricIncludingCalculatedByEntityNameRequest request) {
            return base.Channel.GetMetricIncludingCalculatedByEntityName(request);
        }
        
        public System.Collections.Generic.List<Galleria.Ccm.Services.Metric.Metric> GetMetricIncludingCalculatedByEntityName(string EntityName) {
            Galleria.Ccm.Services.Metric.GetMetricIncludingCalculatedByEntityNameRequest inValue = new Galleria.Ccm.Services.Metric.GetMetricIncludingCalculatedByEntityNameRequest();
            inValue.EntityName = EntityName;
            Galleria.Ccm.Services.Metric.GetMetricIncludingCalculatedByEntityNameResponse retVal = ((Galleria.Ccm.Services.Metric.MetricService)(this)).GetMetricIncludingCalculatedByEntityName(inValue);
            return retVal.Metrics;
        }
    }
}
