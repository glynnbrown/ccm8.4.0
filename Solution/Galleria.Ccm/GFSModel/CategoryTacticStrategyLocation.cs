﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;


namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class CategoryTacticStrategyLocation : ModelReadOnlyObject<CategoryTacticStrategyLocation>
    {
        #region Properties

        /// <summary>
        /// The name of the linked category tactic strategy location
        /// </summary>
        public static readonly ModelPropertyInfo<String> StrategyNameProperty =
            RegisterModelProperty<String>(c => c.StrategyName);
        public String StrategyName
        {
            get { return GetProperty<String>(StrategyNameProperty); }
        }

        /// <summary>
        /// The code of the linked location
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.StrategyName;
        }

        #endregion
    }
}
