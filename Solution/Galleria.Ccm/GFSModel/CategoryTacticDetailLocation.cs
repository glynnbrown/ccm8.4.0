﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class CategoryTacticDetailLocation : ModelReadOnlyObject<CategoryTacticDetailLocation>
    {
        #region Properties

        /// <summary>
        /// The category tactic detail location's name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The category tactic detail locations's code
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }

        /// <summary>
        /// Returns 1 if the category tactic detail location is included
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IsIncludedProperty =
            RegisterModelProperty<Int32>(c => c.IsIncluded);
        public Int32 IsIncluded
        {
            get { return GetProperty<Int32>(IsIncludedProperty); }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
