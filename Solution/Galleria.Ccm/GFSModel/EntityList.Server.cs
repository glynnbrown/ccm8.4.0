﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26365 : L.Ineson
//  Updated so that mock services can be used in testing.
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class EntityList
    {
        #region Constructors
        private EntityList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all entities in gfs
        /// </summary>
        public static EntityList FetchAll(Model.Entity entity)
        {
            return DataPortal.Fetch<EntityList>(new SingleCriteria<String>(entity.SystemSettings.FoundationServicesEndpoint));
        }

        /// <summary>
        /// Returns all entities in gfs
        /// </summary>
        public static EntityList FetchAll(String endpointRoot)
        {
            return DataPortal.Fetch<EntityList>(new SingleCriteria<String>(endpointRoot));
        }

        /// <summary>
        /// Returns all entities in gfs
        /// </summary>
        public static EntityList FetchAll(Int32 ccmEntityId)
        {
            return DataPortal.Fetch<EntityList>(new SingleCriteria<Int32>(ccmEntityId));
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when fetching all entities
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<String> criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.Value))
            {
                var svc = client.EntityServiceClient;
                var response = svc.GetAllEntities(new Services.Entity.GetAllEntitiesRequest());
                foreach (Services.Entity.Entity dc in response.Entities)
                {
                    this.Add(Entity.GetEntity(criteria.Value, dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when fetching all entities
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<Int32> criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            Model.Entity entity = Model.Entity.FetchById(criteria.Value);
            String endpointRoot = entity.SystemSettings.FoundationServicesEndpoint;
            using (FoundationServiceClient client = new FoundationServiceClient(endpointRoot))
            {
                var svc = client.EntityServiceClient;
                var response = svc.GetAllEntities(new Services.Entity.GetAllEntitiesRequest());
                foreach (Services.Entity.Entity dc in response.Entities)
                {
                    this.Add(Entity.GetEntity(endpointRoot, dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}