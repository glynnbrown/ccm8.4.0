﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryRoleFile
    {
        #region Constructor
        private CategoryRoleFile() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the CategoryRoleFile
        /// </summary>
        internal static CategoryRoleFile GetCategoryRoleFile(Services.CategoryRole.CategoryRoleFile dc)
        {
            return DataPortal.FetchChild<CategoryRoleFile>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryRole.CategoryRoleFile dc)
        {
            LoadProperty<Int32>(FileIdProperty, dc.FileId);
            LoadProperty<String>(FileNameProperty, dc.FileName);
            LoadProperty<String>(FileTypeProperty, dc.FileType);
            LoadProperty<Int64>(SizeInKiloBytesProperty, dc.SizeInKiloBytes);
            LoadProperty<String>(SourceFilePathProperty, dc.SourceFilePath);
            LoadProperty<String>(UserNameProperty, dc.UserName);
            LoadProperty<String>(UserFullNameProperty, dc.UserFullName);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dc">The data contract</param>
        private void Child_Fetch(Services.CategoryRole.CategoryRoleFile dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}
