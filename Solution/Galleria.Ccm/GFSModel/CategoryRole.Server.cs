﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryRole
    {
        #region Constructor
        private CategoryRole() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the category role for the the given GFS entity name and product group code
        /// </summary>
        /// <returns></returns>
        public static CategoryRole FetchCategoryRoleByGfsEntityNameProductGroupCode(String endpointRoot, String gfsEntityName, String productGroupCode)
        {
            return DataPortal.Fetch<CategoryRole>(new FetchCategoryRoleByGfsEntityNameProductGroupCodeCriteria(endpointRoot, gfsEntityName, productGroupCode));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryRole.CategoryRole dc, IEnumerable<Services.CategoryRole.CategoryRoleLocation> categoryRoleLocations)
        {
            //If this product group is unassigned in GFS a null CategoryRole is returned
            if (dc != null)
            {
                LoadProperty<Int32>(IdProperty, dc.Id);
                LoadProperty<Guid>(UniqueContentReferenceProperty, dc.UniqueContentReference);
                LoadProperty<String>(NameProperty, dc.Name);
                LoadProperty<String>(DescriptionProperty, dc.Description);
                LoadProperty<Int32>(ContentVersionProperty, dc.ContentVersion);
                LoadProperty<CategoryRoleFileList>(FilesProperty, CategoryRoleFileList.GetFromDataContracts(dc.Files));
            }
            else
            {
                LoadProperty<String>(NameProperty, "Unassigned");
                LoadProperty<String>(DescriptionProperty, "This Product Group is not assigned to a Category Role in GFS.");
                LoadProperty<Int32>(ContentVersionProperty, 0);
                LoadProperty<CategoryRoleFileList>(FilesProperty, CategoryRoleFileList.GetFromDataContracts(new Services.CategoryRole.CategoryRoleFile[] { }));
            }
            LoadProperty<CategoryRoleLocationList>(LocationsProperty, CategoryRoleLocationList.GetFromDataContracts(categoryRoleLocations));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchByEntityIdProductGroupCode
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchCategoryRoleByGfsEntityNameProductGroupCodeCriteria criteria)
        {
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var svc = client.CategoryRoleServiceClient;
                var request = new Services.CategoryRole.GetCategoryRoleByEntityNameProductGroupCodeRequest(criteria.EntityName, criteria.ProductGroupCode);
                var response = svc.GetCategoryRoleByEntityNameProductGroupCode(request);
                LoadDataContract(response.CategoryRole, response.CategoryRoleLocations);
            }
        }

        #endregion

        #endregion
    }
}
