﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System.Collections.Generic;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryRoleFileList
    {
        #region Constructors
        private CategoryRoleFileList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a category role file list from a list of data contracts
        /// </summary>
        internal static CategoryRoleFileList GetFromDataContracts(IEnumerable<Services.CategoryRole.CategoryRoleFile> categoryRoleFileDcs)
        {
            return DataPortal.Fetch<CategoryRoleFileList>(categoryRoleFileDcs);
        }
        #endregion

        #region Data Access

        private void DataPortal_Fetch(IEnumerable<Services.CategoryRole.CategoryRoleFile> categoryRoleFileDcs)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            foreach (Services.CategoryRole.CategoryRoleFile dc in categoryRoleFileDcs)
            {
                this.Add(CategoryRoleFile.GetCategoryRoleFile(dc));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
