﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System.Collections.Generic;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryTacticStrategyLocationList
    {
        #region Constructors
        private CategoryTacticStrategyLocationList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a category tactic strategy location list from a list of data contracts
        /// </summary>
        internal static CategoryTacticStrategyLocationList GetFromDataContracts(IEnumerable<Services.CategoryTactic.CategoryTacticStrategyLocation> categoryTacticStrategyLocationDcs)
        {
            return DataPortal.Fetch<CategoryTacticStrategyLocationList>(categoryTacticStrategyLocationDcs);
        }
        #endregion

        #region Data Access

        private void DataPortal_Fetch(IEnumerable<Services.CategoryTactic.CategoryTacticStrategyLocation> categoryTacticStrategyLocationDcs)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            foreach (Services.CategoryTactic.CategoryTacticStrategyLocation dc in categoryTacticStrategyLocationDcs)
            {
                this.Add(CategoryTacticStrategyLocation.GetCategoryTacticStrategyLocation(dc));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
