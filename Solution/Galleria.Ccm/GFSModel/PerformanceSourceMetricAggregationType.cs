﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (CCM 8.0)
//// V8-27035 : L.Ineson
////  Created
//#endregion
//#endregion

//using System;

//namespace Galleria.Ccm.GFSModel
//{
//    public enum PerformanceSourceMetricAggregationType : byte
//    {
//        Sum = 0,
//        Average = 1,
//        Min = 2,
//        Max = 3,
//        Count = 4
//    }
//}
