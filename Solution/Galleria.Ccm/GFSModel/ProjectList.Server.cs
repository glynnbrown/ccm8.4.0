﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//  V8-27783 : M.Brumby
//      Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class ProjectList
    {
        #region Constructors
        private ProjectList() { }//Force use of factory methods
        #endregion
        
        #region Factory Methods

        /// <summary>
        /// Returns all performance sources in gfs
        /// </summary>
        public static ProjectList FetchAll(Entity entity)
        {
            return DataPortal.Fetch<ProjectList>(new FetchAllCriteria(entity));
        }

        /// <summary>
        /// Returns all performance sources in gfs
        /// </summary>
        public static ProjectList GetProjectInfoByEntityNameProductGroupCode(String entityName, String productGroupCode, Entity entity)
        {
            return DataPortal.Fetch<ProjectList>(new GetProjectInfoByEntityNameProductGroupCodeCriteria(entity, productGroupCode));
        }

        /// <summary>
        /// Returns all projects where there are no assigned product groups
        /// </summary>
        public static ProjectList GetProjectInfosWithNoProductGroupAssignedByEntityName(Entity entity)
        {
            return DataPortal.Fetch<ProjectList>(new GetProjectInfosWithNoProductGroupAssignedByEntityNameCriteria(entity));
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Called by FetchAll
        /// </summary>
        private void DataPortal_Fetch(FetchAllCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var projectSvc = client.ProjectServiceClient;
                var request = new Services.Project.GetProjectInfoByEntityNameRequest(criteria.EntityName);
                var response = projectSvc.GetProjectInfoByEntityName(request);
                foreach (Services.Project.ProjectInfo dc in response.ProjectInfos)
                {
                    this.Add(Project.GetProject(dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called by FetchByProductGroupCodeEntityIdCriteriaIncludingUnavailable
        /// </summary>
        private void DataPortal_Fetch(GetProjectInfoByEntityNameProductGroupCodeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var projectSvc = client.ProjectServiceClient;
                var request = new Services.Project.GetProjectInfoByEntityNameProductGroupCodeRequest(criteria.EntityName, criteria.ProductGroupCode);
                var response = projectSvc.GetProjectInfoByEntityNameProductGroupCode(request);
                foreach (Services.Project.ProjectInfo dc in response.ProjectInfos)
                {
                    this.Add(Project.GetProject(dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called by GetProjectInfosWithNoProductGroupAssignedByEntityName
        /// </summary>
        private void DataPortal_Fetch(GetProjectInfosWithNoProductGroupAssignedByEntityNameCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var projectSvc = client.ProjectServiceClient;
                var request = new Services.Project.GetProjectInfosWithNoProductGroupAssignedByEntityNameRequest(criteria.EntityName);
                var response = projectSvc.GetProjectInfosWithNoProductGroupAssignedByEntityName(request);
                foreach (Services.Project.ProjectInfo dc in response.ProjectInfos)
                {
                    this.Add(Project.GetProject(dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
