﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//  V8-27783 : M.Brumby
//      Created.
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class Project : ModelReadOnlyObject<Project>
    {
        #region Properties
        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the entity id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the entity name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
        }
        #endregion

        #region DateAvailable
        /// <summary>
        /// Date Available property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateAvailableProperty =
            RegisterModelProperty<DateTime?>(c => c.DateAvailable);
        /// <summary>
        /// Returns the Date Available
        /// </summary>
        public DateTime? DateAvailable
        {
            get { return this.GetProperty<DateTime?>(DateAvailableProperty); }
        }
        #endregion

        #region DateAvailable
        /// <summary>
        /// Date Unavailable property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateUnavailableProperty =
            RegisterModelProperty<DateTime?>(c => c.DateUnavailable);
        /// <summary>
        /// Returns the Date Unavailable
        /// </summary>
        public DateTime? DateUnavailable
        {
            get { return this.GetProperty<DateTime?>(DateUnavailableProperty); }
        }
        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
