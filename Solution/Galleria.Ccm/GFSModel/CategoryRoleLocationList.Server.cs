﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System.Collections.Generic;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryRoleLocationList
    {
        #region Constructors
        private CategoryRoleLocationList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a category role file list from a list of data contracts
        /// </summary>
        internal static CategoryRoleLocationList GetFromDataContracts(IEnumerable<Services.CategoryRole.CategoryRoleLocation> categoryRoleLocationDcs)
        {
            return DataPortal.Fetch<CategoryRoleLocationList>(categoryRoleLocationDcs);
        }
        #endregion

        #region Data Access

        private void DataPortal_Fetch(IEnumerable<Services.CategoryRole.CategoryRoleLocation> categoryRoleLocationDcs)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            foreach (Services.CategoryRole.CategoryRoleLocation dc in categoryRoleLocationDcs)
            {
                this.Add(CategoryRoleLocation.GetCategoryRoleLocation(dc));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
