﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class Image : ModelReadOnlyObject<Image>
    {
        #region Properties

        /// <summary>
        /// The property id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The size in bytes
        /// </summary>
        public static readonly ModelPropertyInfo<Int64> SizeInBytesProperty =
            RegisterModelProperty<Int64>(c => c.SizeInBytes);
        public Int64 SizeInBytes
        {
            get { return GetProperty<Int64>(SizeInBytesProperty); }
        }

        /// <summary>
        /// The Height
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> HeightProperty =
            RegisterModelProperty<Int16>(c => c.Height);
        public Int16 Height
        {
            get { return GetProperty<Int16>(HeightProperty); }
        }

        /// <summary>
        /// The Width
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> WidthProperty =
            RegisterModelProperty<Int16>(c => c.Width);
        public Int16 Width
        {
            get { return GetProperty<Int16>(WidthProperty); }
        }

        /// <summary>
        /// The compression name
        /// </summary>
        public static readonly ModelPropertyInfo<String> CompressionNameProperty =
            RegisterModelProperty<String>(c => c.CompressionName);
        public String CompressionName
        {
            get { return GetProperty<String>(CompressionNameProperty); }
        }

        /// <summary>
        /// The Blob
        /// </summary>
        public static readonly ModelPropertyInfo<GFSModel.Blob> BlobProperty =
            RegisterModelProperty<GFSModel.Blob>(c => c.Blob);
        public GFSModel.Blob Blob
        {
            get { return GetProperty<GFSModel.Blob>(BlobProperty); }
        }

        /// <summary>
        /// The Image Preview Byte Array property.
        /// </summary>
        public static readonly ModelPropertyInfo<Byte[]> DocumentPreviewByteArrayProperty =
            RegisterModelProperty<Byte[]>(c => c.DocumentPreviewByteArray);
        public Byte[] DocumentPreviewByteArray
        {
            get
            {
                return GetProperty<Byte[]>(DocumentPreviewByteArrayProperty);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Converts a Blob to a BitmapImage
        /// </summary>
        /// <param name="Blob"></param>
        /// <returns></returns>
        public static BitmapImage GetBitmapImage(Byte[] Blob)
        {
            MemoryStream dataStream = new MemoryStream(Blob);
            return GetBitmapImage(dataStream);
        }

        /// <summary>
        /// Gets a bitmap image from a memory stream
        /// </summary>
        /// <param name="dataStream">memory stream</param>
        /// <returns>BitmapImage</returns>
        public static BitmapImage GetBitmapImage(MemoryStream dataStream)
        {
            BitmapImage img = new BitmapImage();
            dataStream.Position = 0;
            img.BeginInit();
            img.StreamSource = new MemoryStream(dataStream.ToArray());
            img.CreateOptions = BitmapCreateOptions.IgnoreColorProfile;
            img.CacheOption = BitmapCacheOption.Default;
            img.EndInit();
            img.Freeze();

            return img;
        }

        /// <summary>
        /// Creates a Bitmap image from a stream with a particular size and pixel format
        /// </summary>
        /// <param name="dataStream">Original Stream</param>
        /// <param name="size">Height and Width of the image</param>
        /// <param name="format">Pixel format of the image</param>
        /// <returns></returns>
        public BitmapSource CreateBitmap(MemoryStream dataStream, Size size, PixelFormat format, Boolean maintainAspectRatio)
        {
            BitmapSource image = GetBitmapImage(dataStream);
            image = ReSize(image, size, maintainAspectRatio);
            return FormatBitmap(image, format);
        }

        /// <summary>
        /// Creates a Byte array from a Stream
        /// </summary>
        /// <param name="dataStream">Stream to read</param>
        /// <param name="CloseStream">Closes the Stream once finished if True.</param>
        /// <returns>Array of Bytes</returns>
        private static Byte[] CreateBlob(Stream dataStream, Boolean CloseStream)
        {
            Byte[] blob = new Byte[dataStream.Length];
            //Ensure stream is at the start
            dataStream.Position = 0;

            //Stream read does not necessarily read all the bytes asked for so
            //a loop is required to ensure all bytes are read.
            for (Int32 pos = 0; pos < dataStream.Length; )
            {
                Int32 len = dataStream.Read(blob, pos, (Int32)dataStream.Length - pos);
                if (len == 0)
                {
                    break;
                }
                pos += len;

            }

            if (CloseStream) dataStream.Close();
            return blob;
        }

        public static Byte[] CreateBlob(ImageSource img)
        {
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create((BitmapImage)img));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            return CreateBlob(stream, true);
        }

        /// <summary>
        /// Redraws an image to a new size.
        /// </summary>
        /// <param name="originalImage">the image you wish resizing</param>
        /// <param name="size">height and widht to resize to</param>
        /// <param name="maintainAspectRatio">maintains the aspect ratio of the image</param>
        /// <returns>The resized image</returns>
        public static BitmapSource ReSize(BitmapSource originalImage, Size size, Boolean maintainAspectRatio)
        {
            Single nPercent = 0;
            Single nPercentW = 0;
            Single nPercentH = 0;

            if (size.Width == 0) nPercentW = 1;
            else nPercentW = ((Single)size.Width / (Single)originalImage.PixelWidth);

            if (size.Height == 0) nPercentH = 1;
            else nPercentH = ((Single)size.Height / (Single)originalImage.PixelHeight);

            if (nPercentH < nPercentW) nPercent = nPercentH;
            else nPercent = nPercentW;

            TransformedBitmap resizedImage;
            if (maintainAspectRatio)
            {
                resizedImage = new TransformedBitmap(originalImage, new ScaleTransform(nPercent, nPercent));
            }
            else
            {
                resizedImage = new TransformedBitmap(originalImage, new ScaleTransform(nPercentW, nPercentH));
            }
            resizedImage.Freeze();
            return resizedImage;
        }

        private BitmapSource FormatBitmap(BitmapSource originalImage, PixelFormat format)
        {
            FormatConvertedBitmap img = new FormatConvertedBitmap(originalImage, format, originalImage.Palette, 0.0f);

            return img;
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new Image from the data contract
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static GFSModel.Image CreateImage(Services.Image.Image dc)
        {
            GFSModel.Image image = new GFSModel.Image();
            image.Create(dc);
            return image;
        }
        #endregion

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Services.Image.Image dc)
        {
            LoadProperty<Int32>(IdProperty, dc.Id);
            LoadProperty<Int64>(SizeInBytesProperty, dc.SizeInBytes);
            LoadProperty<Int16>(HeightProperty, dc.Height);
            LoadProperty<Int16>(WidthProperty, dc.Width);
            LoadProperty<String>(CompressionNameProperty, dc.CompressionName);
            LoadProperty<GFSModel.Blob>(BlobProperty, GFSModel.Blob.CreateBlob(dc.ImageBlob));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchByEntityIdProductGroupCode
        /// </summary>
        [Serializable]
        public class FetchPreviewImageByFileIdCriteria : Csla.CriteriaBase<FetchPreviewImageByFileIdCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region FileId

            public static readonly PropertyInfo<Int32> FileIdProperty =
                RegisterProperty<Int32>(c => c.FileId);
            public Int32 FileId
            {
                get { return ReadProperty<Int32>(FileIdProperty); }
            }

            #endregion


            #endregion

            public FetchPreviewImageByFileIdCriteria(String endpointRoot, Int32 fileId)
            {
                LoadProperty<Int32>(FileIdProperty, fileId);
                LoadProperty<String>(EndpointRootProperty, endpointRoot);
            }
        }

        #endregion
    }
}
