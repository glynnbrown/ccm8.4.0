﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26159 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// A list of timeline groups from GFS.
    /// </summary>
    [Serializable]
    public sealed partial class TimelineGroupList : ModelReadOnlyList<TimelineGroupList, TimelineGroup>
    {
        #region Criteria
        /// <summary>
        /// Criteria for FetchByDateRangeCriteria
        /// </summary>
        [Serializable]
        public sealed class FetchByDateRangeCriteria : CriteriaBase<FetchByDateRangeCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root details
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty);}
            }
            #endregion

            #region EntityName
            public static readonly PropertyInfo<String> EntityNameProperty =
                RegisterProperty<String>(c => c.EntityName);

            public String EntityName
            {
                get { return ReadProperty<String>(EntityNameProperty); }
            }
            #endregion

            #region PerformanceSourceName
            public static readonly PropertyInfo<String> PerformanceSourceNameProperty =
                RegisterProperty<String>(c => c.PerformanceSourceName);

            public String PerformanceSourceName
            {
                get { return ReadProperty<String>(PerformanceSourceNameProperty); }
            }
            #endregion

            #region StartDate
            public static readonly PropertyInfo<DateTime> StartDateProperty =
                RegisterProperty<DateTime>(c => c.StartDate);

            public DateTime StartDate
            {
                get { return ReadProperty<DateTime>(StartDateProperty); }
            }
            #endregion

            #region EndDate
            public static readonly PropertyInfo<DateTime> EndDateProperty =
                RegisterProperty<DateTime>(c => c.EndDate);

            public DateTime EndDate
            {
                get { return ReadProperty<DateTime>(EndDateProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByDateRangeCriteria(
                Entity entity, PerformanceSource performanceSource,
                DateTime startDate, DateTime endDate)
            {
                this.LoadProperty<String>(EndpointRootProperty, entity.EndpointRoot);
                LoadProperty<String>(EntityNameProperty, entity.Name);
                LoadProperty<String>(PerformanceSourceNameProperty, performanceSource.Name);
                LoadProperty<DateTime>(StartDateProperty, startDate);
                LoadProperty<DateTime>(EndDateProperty, endDate);
            }
            #endregion
        }
        #endregion
    }
}
