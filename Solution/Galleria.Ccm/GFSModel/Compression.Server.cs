﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26157 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class Compression
    {
        #region Constructor
        private Compression() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a compression from the given dc
        /// </summary>
        /// <param name="dc"></param>
        /// <returns></returns>
        internal static Compression GetCompression(Services.Image.Compression dc)
        {
            return DataPortal.FetchChild<Compression>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.Image.Compression dc)
        {
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<Int32>(WidthProperty, dc.Width);
            LoadProperty<Int32>(HeightProperty, dc.Height);
            LoadProperty<Boolean>(MaintainAspectRatioProperty, dc.MaintainAspectRatio);
            LoadProperty<String>(ColourDepthProperty, dc.ColourDepth);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(Services.Image.Compression dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}
