﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.GFSModel
{
    public partial class PerformanceSource
    {
        #region Constructor
        private PerformanceSource() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates an performance source from the given dc
        /// </summary>
        /// <param name="dc"></param>
        /// <returns></returns>
        internal static PerformanceSource GetPerformanceSource(Services.Performance.PerformanceSource dc)
        {
            return DataPortal.FetchChild<PerformanceSource>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.Performance.PerformanceSource dc)
        {
            LoadProperty<Int32>(IdProperty, dc.Id);
            LoadProperty<String>(EntityNameProperty, dc.EntityName);
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<String>(DescriptionProperty, dc.Description);
            LoadProperty<String>(LocationLevelNameProperty, dc.LocationLevelName);
            LoadProperty<String>(ProductLevelNameProperty, dc.ProductLevelName);
            LoadProperty<String>(TimelineLevelNameProperty, dc.TimelineLevelName);
            LoadProperty<PerformanceSourceType>(TypeProperty, EnumHelper.Parse<PerformanceSourceType>(dc.Type,PerformanceSourceType.Unknown));
            LoadProperty<PerformanceSourceMetricList>(PerformanceSourceMetricsProperty, PerformanceSourceMetricList.GetFromDataContracts(dc.PerformanceSourceMetrics));
            LoadProperty<DateTime?>(DateLastProcessedProperty, dc.DateLastProcessed);
            LoadProperty<String>(DataTypeProperty, dc.DataType);
            LoadProperty<String>(DaysPerPeriodProperty, dc.DaysPerPeriod);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(Services.Performance.PerformanceSource dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}