﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Framework.Enums;
using Galleria.Framework.Interfaces;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class PerformanceSourceMetric : ModelReadOnlyObject<PerformanceSourceMetric>, IPerformanceSourceMetric
    {
        #region Properties

        /// <summary>
        /// The property id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The metric name
        /// </summary>
        public static readonly ModelPropertyInfo<String> MetricNameProperty =
            RegisterModelProperty<String>(c => c.MetricName);
        public String MetricName
        {
            get { return GetProperty<String>(MetricNameProperty); }
        }

        /// <summary>
        /// The name
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSourceMetricColumnNumber> ColumnNumberProperty =
            RegisterModelProperty<PerformanceSourceMetricColumnNumber>(c => c.ColumnNumber);
        public PerformanceSourceMetricColumnNumber ColumnNumber
        {
            get { return GetProperty<PerformanceSourceMetricColumnNumber>(ColumnNumberProperty); }
        }

        /// <summary>
        /// The description
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsDuplicateColumnMappingProperty =
            RegisterModelProperty<Boolean>(c => c.IsDuplicateColumnMapping);
        public Boolean IsDuplicateColumnMapping
        {
            get { return GetProperty<Boolean>(IsDuplicateColumnMappingProperty); }
        }

        /// <summary>
        ///  AggregationType property description
        /// </summary>
        public static readonly ModelPropertyInfo<AggregationType> AggregationTypeProperty =
            RegisterModelProperty<AggregationType>(c => c.AggregationType);
        /// <summary>
        /// The aggregation type set against this metric in gfs
        /// </summary>
        public AggregationType AggregationType
        {
            get { return GetProperty<AggregationType>(AggregationTypeProperty); }
        }

        // <summary>
        /// The metric Description
        /// </summary>
        public static readonly ModelPropertyInfo<String> MetricDescriptionProperty =
            RegisterModelProperty<String>(c => c.MetricDescription);
        public String MetricDescription
        {
            get
            {
                return GetProperty<String>(MetricDescriptionProperty);
            }
        }

        // <summary>
        /// Is a calculated metric
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsCalculatedMetricProperty =
            RegisterModelProperty<Boolean>(c => c.IsCalculatedMetric);
        public Boolean IsCalculatedMetric
        {
            get
            {
                return GetProperty<Boolean>(IsCalculatedMetricProperty);
            }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.MetricName;
        }

        #endregion
    }
}