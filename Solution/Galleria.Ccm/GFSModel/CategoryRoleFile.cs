﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class CategoryRoleFile : ModelReadOnlyObject<CategoryRoleFile>
    {
        #region Properties

        /// <summary>
        /// The property id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> FileIdProperty =
            RegisterModelProperty<Int32>(c => c.FileId);
        public Int32 FileId
        {
            get { return GetProperty<Int32>(FileIdProperty); }
        }

        /// <summary>
        /// The category role file's name
        /// </summary>
        public static readonly ModelPropertyInfo<String> FileNameProperty =
            RegisterModelProperty<String>(c => c.FileName);
        public String FileName
        {
            get { return GetProperty<String>(FileNameProperty); }
        }

        /// <summary>
        /// The category role file's type
        /// </summary>
        public static readonly ModelPropertyInfo<String> FileTypeProperty =
            RegisterModelProperty<String>(c => c.FileType);
        public String FileType
        {
            get { return GetProperty<String>(FileTypeProperty); }
        }

        /// <summary>
        /// The category role file's size
        /// </summary>
        public static readonly ModelPropertyInfo<Int64> SizeInKiloBytesProperty =
            RegisterModelProperty<Int64>(c => c.SizeInKiloBytes);
        public Int64 SizeInKiloBytes
        {
            get { return GetProperty<Int64>(SizeInKiloBytesProperty); }
        }

        /// <summary>
        /// The category role file's file path
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceFilePathProperty =
            RegisterModelProperty<String>(c => c.SourceFilePath);
        public String SourceFilePath
        {
            get { return GetProperty<String>(SourceFilePathProperty); }
        }

        /// <summary>
        /// The category role file's author
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName);
        public String UserName
        {
            get { return GetProperty<String>(UserNameProperty); }
        }

        /// <summary>
        /// The category role file author's full name
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserFullNameProperty =
            RegisterModelProperty<String>(c => c.UserFullName);
        public String UserFullName
        {
            get { return GetProperty<String>(UserFullNameProperty); }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.FileName;
        }

        #endregion
    }
}
