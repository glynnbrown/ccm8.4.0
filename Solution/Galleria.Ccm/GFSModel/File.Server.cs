﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class File
    {
        #region Constructor
        private File() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the entity with the given id
        /// </summary>
        /// <remarks>Temporary until iso starts storing the entity name aswell.</remarks>
        /// <returns></returns>
        public static File FetchByFileId(String endpointRoot, Int32 fileId)
        {
            return DataPortal.Fetch<File>(new FetchFileByFileIdCriteria(endpointRoot, fileId));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.File.File dc)
        {
            LoadProperty<Int32>(IdProperty, dc.Id);
            LoadProperty<Byte[]>(DataProperty, dc.Data);
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<DateTime>(DateModifiedProperty, dc.DateModified);
            LoadProperty<String>(EntityNameProperty, dc.EntityName);
            LoadProperty<Int64>(SizeInBytesProperty, dc.SizeInBytes);
            LoadProperty<String>(SourceFilePathProperty, dc.SourceFilePath);
            LoadProperty<String>(UserNameProperty, dc.UserName);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchByISOEntityName
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchFileByFileIdCriteria criteria)
        {
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var svc = client.FileServiceClient;
                var request = new Services.File.GetFileByFileIdRequest(criteria.FileId);
                var response = svc.GetFileByFileId(request);
                LoadDataContract(response.File);
            }
        }

        #endregion

        #endregion

    }
}
