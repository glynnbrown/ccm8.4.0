﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (GFS 2.1.10)
// GFS-31185 : J.Pickup
//  Introduced to CCM. (Brought over from GFS).

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

//NB - really, this should exist as part of the framework as it is primarily a GFS Enum, and will therefore require updating whenever the GFS one is. 
namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// An enum which defines the available content types within GFS. 
    /// </summary>
    public enum ContentType
    {
        Unknown = 0,
        ProductUniverse = 1,
        CategoryStrategy = 2,
        CategoryTactic = 3,
        CategoryRole = 4,
        CategoryGoal = 5,
        ProjectTemplate = 6,
        Fixture = 7,
        ConsumerDecisionTree = 8,
        LocationSpace = 9,
        LocationSpaceAisle = 10,
        LocationSpaceValley = 11,
        LocationSpaceZone = 12,
        ClusterScheme = 13,
        Programme = 14,
        Planogram = 15,
        Assortment = 16,
        FloorLayout = 17,
        MacroSpaceRecommendation = 18,
        AssortmentMinorRevision = 19
    }

    /// <summary>
    /// Helper class for the content type enum
    /// </summary>
    public static class ContentTypeHelper
    {
        public static readonly Dictionary<ContentType, String> FriendlyNames =
            new Dictionary<ContentType, String>()
            {
                {ContentType.Unknown, Message.Enum_ContentType_Unknown},
                {ContentType.ProductUniverse, Message.Enum_ContentType_ProductUniverse},
                {ContentType.CategoryStrategy, Message.Enum_ContentType_CategoryStrategy},//no content id
                {ContentType.CategoryTactic, Message.Enum_ContentType_CategoryTactic},
                {ContentType.CategoryRole, Message.Enum_ContentType_CategoryRole},
                {ContentType.CategoryGoal, Message.Enum_ContentType_CategoryGoal},
                {ContentType.ProjectTemplate, Message.Enum_ContentType_ProjectTemplate},
                {ContentType.Fixture, Message.Enum_ContentType_Fixture},
                {ContentType.ConsumerDecisionTree, Message.Enum_ContentType_ConsumerDecisionTree},
                {ContentType.LocationSpace, Message.Enum_ContentType_LocationSpace},
                {ContentType.LocationSpaceAisle, Message.Enum_ContentType_LocationSpaceAisle},//removed
                {ContentType.LocationSpaceValley, Message.Enum_ContentType_LocationSpaceValley},//removed
                {ContentType.LocationSpaceZone, Message.Enum_ContentType_LocationSpaceZone},//removed
                {ContentType.ClusterScheme, Message.Enum_ContentType_ClusterScheme},
                {ContentType.Programme, Message.Enum_ContentType_Programme},//removed
                {ContentType.Planogram, Message.Enum_ContentType_Planogram},
                {ContentType.Assortment, Message.Enum_ContentType_Assortment},
                {ContentType.FloorLayout, Message.Enum_ContentType_FloorLayout},//removed
                {ContentType.MacroSpaceRecommendation, Message.Enum_ContentType_MacroSpaceRecommendation},
                {ContentType.AssortmentMinorRevision, Message.Enum_ContentType_AssortmentMinorRevision}
            };

        /// <summary>
        /// Returns the matching enum value from the specified description
        /// Returns null if not match found
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        [Obsolete("Use ResolveEnumFromFriendlyName instead")]
        public static ContentType? ExternalLinkSetupTypeGetEnum(String description)
        {
            foreach (KeyValuePair<ContentType, String> keyPair in ContentTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }

        /// <summary>
        /// Returns the matching enum value from the passed Friendly name
        /// Returns null if not match found
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static ContentType? ResolveEnumFromFriendlyName(String friendlyName)
        {
            foreach (KeyValuePair<ContentType, String> keyValuePair in ContentTypeHelper.FriendlyNames)
            {
                if (keyValuePair.Value.ToUpperInvariant().Equals(friendlyName.ToUpperInvariant()))
                {
                    return keyValuePair.Key;
                }
            }
            return null;
        }
    }
}