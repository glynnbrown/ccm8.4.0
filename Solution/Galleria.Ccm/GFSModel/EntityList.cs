﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// A list of gfs entities
    /// </summary>
    [Serializable]
    public partial class EntityList : ModelReadOnlyList<EntityList, Entity>
    {
    }
}