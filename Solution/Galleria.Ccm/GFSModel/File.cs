﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class File : ModelReadOnlyObject<File>
    {
        #region Properties

        /// <summary>
        /// The property id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The Entity name
        /// </summary>
        public static readonly ModelPropertyInfo<String> EntityNameProperty =
            RegisterModelProperty<String>(c => c.EntityName);
        public String EntityName
        {
            get { return GetProperty<String>(EntityNameProperty); }
        }

        /// <summary>
        /// The File name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The file source path where the file was loaded from
        /// </summary>
        public static readonly ModelPropertyInfo<String> SourceFilePathProperty =
            RegisterModelProperty<String>(c => c.SourceFilePath);
        public String SourceFilePath
        {
            get { return GetProperty<String>(SourceFilePathProperty); }
        }

        /// <summary>
        /// The file SizeInBytes
        /// </summary>
        public static readonly ModelPropertyInfo<Int64> SizeInBytesProperty =
            RegisterModelProperty<Int64>(c => c.SizeInBytes);
        public Int64 SizeInBytes
        {
            get { return GetProperty<Int64>(SizeInBytesProperty); }
        }

        /// <summary>
        /// The last modified date of the file
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateModified);
        public DateTime DateModified
        {
            get { return GetProperty<DateTime>(DateModifiedProperty); }
        }

        /// <summary>
        /// The File data 
        /// </summary>
        public static readonly ModelPropertyInfo<Byte[]> DataProperty =
            RegisterModelProperty<Byte[]>(c => c.Data);
        public Byte[] Data
        {
            get { return GetProperty<Byte[]>(DataProperty); }
        }

        /// <summary>
        /// The user name
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName);
        public String UserName
        {
            get { return GetProperty<String>(UserNameProperty); }
        }

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchFileByFileIdCriteria
        /// </summary>
        [Serializable]
        public class FetchFileByFileIdCriteria : Csla.CriteriaBase<FetchFileByFileIdCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region FileId

            public static readonly PropertyInfo<Int32> FileIdProperty =
                RegisterProperty<Int32>(c => c.FileId);
            public Int32 FileId
            {
                get { return ReadProperty<Int32>(FileIdProperty); }
            }

            #endregion


            #endregion

            public FetchFileByFileIdCriteria(String endpointRoot, Int32 fileId)
            {
                LoadProperty<Int32>(FileIdProperty, fileId);
                LoadProperty<String>(EndpointRootProperty, endpointRoot);
            }
        }

        #endregion
    }
}
