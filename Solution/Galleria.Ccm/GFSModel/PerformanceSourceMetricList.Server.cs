﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System.Collections.Generic;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class PerformanceSourceMetricList
    {
        #region Constructors
        private PerformanceSourceMetricList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a performance source metric list from a list of data contracts
        /// </summary>
        internal static PerformanceSourceMetricList GetFromDataContracts(List<Services.Performance.PerformanceSourceMetric> performanceSourceMetricDcs)
        {
            return DataPortal.FetchChild<PerformanceSourceMetricList>(performanceSourceMetricDcs);
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when fetching a child collection
        /// </summary>
        private void Child_Fetch(List<Services.Performance.PerformanceSourceMetric> performanceSourceMetricDcs)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            foreach (Services.Performance.PerformanceSourceMetric dc in performanceSourceMetricDcs)
            {
                this.Add(PerformanceSourceMetric.GetPerformanceSourceMetric(dc));
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}