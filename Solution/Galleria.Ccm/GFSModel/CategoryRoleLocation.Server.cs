﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryRoleLocation
    {
        #region Constructor
        private CategoryRoleLocation() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the category role location from the given datacontract
        /// </summary>
        internal static CategoryRoleLocation GetCategoryRoleLocation(Services.CategoryRole.CategoryRoleLocation dc)
        {
            return DataPortal.FetchChild<CategoryRoleLocation>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryRole.CategoryRoleLocation dc)
        {
            if (String.IsNullOrEmpty(dc.Name))
            {
                LoadProperty<String>(NameProperty, "Unassigned");
            }
            else
            {
                LoadProperty<String>(NameProperty, dc.Name);
            }
            LoadProperty<String>(CodeProperty, dc.Code);
            LoadProperty<Int32>(RoleIdProperty, dc.RoleId);
            LoadProperty<String>(RoleNameProperty, dc.RoleName);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return GetCategoryRoleLocation
        /// </summary>
        private void Child_Fetch(Services.CategoryRole.CategoryRoleLocation dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}
