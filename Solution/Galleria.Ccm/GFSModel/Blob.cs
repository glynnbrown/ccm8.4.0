﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class Blob : ModelReadOnlyObject<Blob>
    {
        #region Properties

        /// <summary>
        /// The property id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The size in bytes
        /// </summary>
        public static readonly ModelPropertyInfo<Byte[]> DataProperty =
            RegisterModelProperty<Byte[]>(c => c.Data);
        public Byte[] Data
        {
            get { return GetProperty<Byte[]>(DataProperty); }
        }

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new Blob from the data contract
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static GFSModel.Blob CreateBlob(Services.Image.Blob dc)
        {
            GFSModel.Blob blob = new GFSModel.Blob();
            blob.Create(dc);
            return blob;
        }
        #endregion

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Services.Image.Blob dc)
        {
            LoadProperty<Int32>(IdProperty, dc.Id);
            LoadProperty<Byte[]>(DataProperty, dc.Data);
        }
        #endregion
    }
}
