﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System.Collections.Generic;
using Csla;


namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryTacticDetailList
    {
        #region Constructors
        private CategoryTacticDetailList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a category tactic detail list from a list of data contracts
        /// </summary>
        internal static CategoryTacticDetailList GetFromDataContracts(IEnumerable<Services.CategoryTactic.CategoryTacticDetail> categoryTacticDetailDcs)
        {
            return DataPortal.Fetch<CategoryTacticDetailList>(categoryTacticDetailDcs);
        }
        #endregion

        #region Data Access

        private void DataPortal_Fetch(IEnumerable<Services.CategoryTactic.CategoryTacticDetail> categoryTacticDetailDcs)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            foreach (Services.CategoryTactic.CategoryTacticDetail dc in categoryTacticDetailDcs)
            {
                this.Add(CategoryTacticDetail.GetCategoryTacticDetail(dc));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
