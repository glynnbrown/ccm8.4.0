﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// Category Goal Detail Priority Type Enum
    /// </summary>
    public enum CategoryGoalDetailPriorityType
    {
        Critical = 0,
        High = 1,
        Medium = 2,
        Low = 3
    }

    [Serializable]
    public sealed partial class CategoryGoalDetail : ModelReadOnlyObject<CategoryGoalDetail>
    {
        #region Properties

        /// <summary>
        /// The category goal detail's name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The category goal detail's Description
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
        }

        /// <summary>
        /// The category goal detail's SuccessCriteria
        /// </summary>
        public static readonly ModelPropertyInfo<String> SuccessCriteriaProperty =
            RegisterModelProperty<String>(c => c.SuccessCriteria);
        public String SuccessCriteria
        {
            get { return GetProperty<String>(SuccessCriteriaProperty); }
        }

        /// <summary>
        /// The category goal detail's Priority
        /// </summary>
        public static readonly ModelPropertyInfo<CategoryGoalDetailPriorityType> PriorityProperty =
            RegisterModelProperty<CategoryGoalDetailPriorityType>(c => c.Priority);
        public CategoryGoalDetailPriorityType Priority
        {
            get { return GetProperty<CategoryGoalDetailPriorityType>(PriorityProperty); }
        }

        /// <summary>
        /// The category goal detail's Completion status
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> CompleteProperty =
            RegisterModelProperty<Int16>(c => c.Complete);
        public Int16 Complete
        {
            get { return GetProperty<Int16>(CompleteProperty); }
        }

        /// <summary>
        /// The category goal detail's Locations
        /// </summary>
        public static readonly ModelPropertyInfo<CategoryGoalDetailLocationList> LocationsProperty =
            RegisterModelProperty<CategoryGoalDetailLocationList>(c => c.Locations);
        public CategoryGoalDetailLocationList Locations
        {
            get { return GetProperty<CategoryGoalDetailLocationList>(LocationsProperty); }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
