﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryTacticDetail
    {
        #region Constructor
        private CategoryTacticDetail() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the CategoryTacticDetail
        /// </summary>
        internal static CategoryTacticDetail GetCategoryTacticDetail(Services.CategoryTactic.CategoryTacticDetail dc)
        {
            return DataPortal.FetchChild<CategoryTacticDetail>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryTactic.CategoryTacticDetail dc)
        {
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<String>(TacticTypeNameProperty, dc.TacticTypeName);
            LoadProperty<CategoryTacticDetailLocationList>(LocationsProperty, CategoryTacticDetailLocationList.GetFromDataContracts(dc.Locations));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dc">The data contract</param>
        private void Child_Fetch(Services.CategoryTactic.CategoryTacticDetail dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}
