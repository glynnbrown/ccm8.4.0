﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System.Collections.Generic;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryGoalDetailList
    {
        #region Constructors
        private CategoryGoalDetailList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a category goal detail list from a list of data contracts
        /// </summary>
        internal static CategoryGoalDetailList GetFromDataContracts(IEnumerable<Services.CategoryGoal.CategoryGoalDetail> categoryGoalDetailDcs)
        {
            return DataPortal.Fetch<CategoryGoalDetailList>(categoryGoalDetailDcs);
        }
        #endregion

        #region Data Access

        private void DataPortal_Fetch(IEnumerable<Services.CategoryGoal.CategoryGoalDetail> categoryGoalDetailDcs)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            foreach (Services.CategoryGoal.CategoryGoalDetail dc in categoryGoalDetailDcs)
            {
                this.Add(CategoryGoalDetail.GetCategoryGoalDetail(dc));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
