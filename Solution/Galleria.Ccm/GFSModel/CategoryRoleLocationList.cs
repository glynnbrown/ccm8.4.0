﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// A list of category role locations
    /// </summary>
    [Serializable]
    public partial class CategoryRoleLocationList : ModelReadOnlyList<CategoryRoleLocationList, CategoryRoleLocation>
    {
    }
}
