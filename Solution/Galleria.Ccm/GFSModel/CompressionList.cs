﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26157 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// A list of gfs compressions
    /// </summary>
    [Serializable]
    public partial class CompressionList : ModelReadOnlyList<CompressionList, Compression>
    {
    }
}
