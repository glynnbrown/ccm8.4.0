﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion


using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class Image
    {
        #region Constructor
        private Image() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the BitmapImage preview for the file with specified ID
        /// </summary>
        public static Image FetchByFileId(String endpointRoot, Int32 fileId)
        {
            return DataPortal.Fetch<Image>(new FetchPreviewImageByFileIdCriteria(endpointRoot, fileId));
        }

        #endregion


        #region Data Access

        #region Data Transfer Object

        private void LoadImage(Byte[] documentPreviewArray)
        {
            LoadProperty<Byte[]>(DocumentPreviewByteArrayProperty, documentPreviewArray);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchByISOEntityName
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchPreviewImageByFileIdCriteria criteria)
        {
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var svc = client.ImageServiceClient;
                var request = new Services.Image.GetPreviewImageByFileIdRequest(criteria.FileId);
                var response = svc.GetPreviewImageByFileId(request);

                if (response.Data!= null)
                {
                    LoadImage(response.Data);
                }
            }

            }

        #endregion

        #endregion
        
    }
}
