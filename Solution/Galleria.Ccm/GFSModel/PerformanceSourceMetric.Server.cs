﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Helpers;
using Galleria.Framework.Enums;

namespace Galleria.Ccm.GFSModel
{
    public partial class PerformanceSourceMetric
    {
        #region Constructor
        private PerformanceSourceMetric() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the PerformanceSourceMetric
        /// </summary>
        internal static PerformanceSourceMetric GetPerformanceSourceMetric(Services.Performance.PerformanceSourceMetric dc)
        {
            return DataPortal.FetchChild<PerformanceSourceMetric>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.Performance.PerformanceSourceMetric dc)
        {
            LoadProperty<Int32>(IdProperty, dc.Id);
            LoadProperty<String>(MetricNameProperty, dc.MetricName);
            LoadProperty<PerformanceSourceMetricColumnNumber>(ColumnNumberProperty, (PerformanceSourceMetricColumnNumber)Enum.Parse(typeof(PerformanceSourceMetricColumnNumber), dc.ColumnNumber));
            LoadProperty<Boolean>(IsDuplicateColumnMappingProperty, dc.IsDuplicateColumnMapping);
            LoadProperty<AggregationType>(AggregationTypeProperty,
                EnumHelper.Parse<AggregationType>(dc.AggregationType, AggregationType.Sum));
            LoadProperty<String>(MetricDescriptionProperty, dc.MetricDescription);
            LoadProperty<Boolean>(IsCalculatedMetricProperty, dc.IsCalculatedMetric);

        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dc">The data contract</param>
        private void Child_Fetch(Services.Performance.PerformanceSourceMetric dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}