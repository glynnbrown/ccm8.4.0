﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryTacticStrategyLocation
    {
        #region Constructor
        private CategoryTacticStrategyLocation() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the CategoryTacticStrategyLocation
        /// </summary>
        internal static CategoryTacticStrategyLocation GetCategoryTacticStrategyLocation(Services.CategoryTactic.CategoryTacticStrategyLocation dc)
        {
            return DataPortal.FetchChild<CategoryTacticStrategyLocation>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryTactic.CategoryTacticStrategyLocation dc)
        {
            LoadProperty<String>(StrategyNameProperty, dc.StrategyName);
            LoadProperty<String>(CodeProperty, dc.Code);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dc">The data contract</param>
        private void Child_Fetch(Services.CategoryTactic.CategoryTacticStrategyLocation dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}
