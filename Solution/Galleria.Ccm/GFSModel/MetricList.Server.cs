﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Services;
using Galleria.Ccm.Services.Metric;

namespace Galleria.Ccm.GFSModel
{
    public partial class MetricList
    {
        #region Constructors
        private MetricList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all metrics for the specified entity
        /// </summary>
        public static MetricList FetchAll(Entity entity)
        {
            return DataPortal.Fetch<MetricList>(new FetchAllCriteria(entity));
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning all metrics
        /// for a specified entity name
        /// </summary>
        private void DataPortal_Fetch(FetchAllCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var request = new GetMetricIncludingCalculatedByEntityNameRequest(criteria.EntityName);
                var response = client.MetricServiceClient.GetMetricIncludingCalculatedByEntityName(request);
                foreach (var dc in response.Metrics)
                {
                    this.Add(Metric.GetMetric(dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = false;
        }
        #endregion

        #endregion
    }
}
