﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26159 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class TimelineGroup
    {
        #region Constructor
        private TimelineGroup() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the TimelineGroup
        /// </summary>
        internal static TimelineGroup GetTimelineGroup(Services.Timeline.TimelineGroup dc)
        {
            return DataPortal.FetchChild<TimelineGroup>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads from the given data contract
        /// </summary>
        private void LoadDataContract(Services.Timeline.TimelineGroup dc)
        {
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<Int64>(CodeProperty, dc.Code);
            LoadProperty<String>(TimelineLevelNameProperty, dc.TimelineLevelName);
            LoadProperty<TimelineGroupList>(ChildrenProperty, TimelineGroupList.GetFromDataContracts(dc.Children));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dc">The data contract</param>
        private void Child_Fetch(Services.Timeline.TimelineGroup dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}
