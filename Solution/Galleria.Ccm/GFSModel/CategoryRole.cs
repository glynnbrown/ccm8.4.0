﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class CategoryRole : ModelReadOnlyObject<CategoryRole>
    {
        #region Properties

        /// <summary>
        /// The property id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The category role UCR
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }

        /// <summary>
        /// The category role name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The category role description
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
        }

        /// <summary>
        /// The category role content version
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ContentVersionProperty =
            RegisterModelProperty<Int32>(c => c.ContentVersion);
        public Int32 ContentVersion
        {
            get { return GetProperty<Int32>(ContentVersionProperty); }
        }

        /// <summary>
        /// The collection of files for this category role
        /// </summary>
        public static readonly ModelPropertyInfo<CategoryRoleFileList> FilesProperty =
            RegisterModelProperty<CategoryRoleFileList>(c => c.Files);
        public CategoryRoleFileList Files
        {
            get { return GetProperty<CategoryRoleFileList>(FilesProperty); }
        }

        public static readonly ModelPropertyInfo<CategoryRoleLocationList> LocationsProperty =
            RegisterModelProperty<CategoryRoleLocationList>(c => c.Locations);
        public CategoryRoleLocationList Locations
        {
            get { return GetProperty<CategoryRoleLocationList>(LocationsProperty); }
        }

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchCategoryRoleByGfsEntityNameProductGroupCode
        /// </summary>
        [Serializable]
        public class FetchCategoryRoleByGfsEntityNameProductGroupCodeCriteria : Csla.CriteriaBase<FetchCategoryRoleByGfsEntityNameProductGroupCodeCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region EntityName
            /// <summary>
            /// EntityName property definition
            /// </summary>
            public static readonly PropertyInfo<String> EntityNameProperty =
                RegisterProperty<String>(c => c.EntityName);
            /// <summary>
            /// Returns the gfs entity name
            /// </summary>
            public String EntityName
            {
                get { return this.ReadProperty<String>(EntityNameProperty); }
            }
            #endregion

            #region ProductGroupCode

            public static readonly PropertyInfo<String> ProductGroupCodeProperty =
                RegisterProperty<String>(c => c.ProductGroupCode);
            public String ProductGroupCode
            {
                get { return ReadProperty<String>(ProductGroupCodeProperty); }
            }

            #endregion

            #endregion

            public FetchCategoryRoleByGfsEntityNameProductGroupCodeCriteria(String endpointRoot, String gfsEntityName, String productGroupCode)
            {
                LoadProperty<String>(EndpointRootProperty, endpointRoot);
                LoadProperty<String>(EntityNameProperty, gfsEntityName);
                LoadProperty<String>(ProductGroupCodeProperty, productGroupCode);
            }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
