﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class Entity : ModelReadOnlyObject<Entity>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the entity id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the entity name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// Returns the entity description
        /// </summary>
        public String Description
        {
            get { return this.GetProperty<String>(DescriptionProperty); }
        }
        #endregion

        #region EndpointRoot
        /// <summary>
        /// EndpointRoot property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> EndpointRootProperty =
            RegisterModelProperty<String>(c => c.EndpointRoot);
        /// <summary>
        /// Returns the endpoint root
        /// </summary>
        public String EndpointRoot
        {
            get { return this.GetProperty<String>(EndpointRootProperty); }
        }
        #endregion

        #endregion

        #region Criteria

        #region FetchByCcmEntityCriteria
        /// <summary>
        /// Criteria for FetchByCcmEntity
        /// </summary>
        [Serializable]
        public sealed class FetchByCcmEntityCriteria : CriteriaBase<FetchByCcmEntityCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region GfsEntityId
            /// <summary>
            /// GfsEntityId property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> GfsEntityIdProperty =
                RegisterProperty<Int32>(c => c.GfsEntityId);
            /// <summary>
            /// Returns the gfs entity id
            /// </summary>
            public Int32 GfsEntityId
            {
                get { return this.ReadProperty<Int32>(GfsEntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByCcmEntityCriteria(Model.Entity entity)
            {
                this.LoadProperty<String>(EndpointRootProperty, entity.SystemSettings.FoundationServicesEndpoint);
                this.LoadProperty<Int32>(GfsEntityIdProperty, (Int32)entity.GFSId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}