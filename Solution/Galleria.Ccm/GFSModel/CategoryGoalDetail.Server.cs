﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryGoalDetail
    {
        #region Constructor
        private CategoryGoalDetail() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the CategoryGoalDetail
        /// </summary>
        internal static CategoryGoalDetail GetCategoryGoalDetail(Services.CategoryGoal.CategoryGoalDetail dc)
        {
            return DataPortal.FetchChild<CategoryGoalDetail>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryGoal.CategoryGoalDetail dc)
        {
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<String>(DescriptionProperty, dc.Description);
            LoadProperty<String>(SuccessCriteriaProperty, dc.SuccessCriteria);

            if (Enum.IsDefined(typeof(CategoryGoalDetailPriorityType), dc.Priority))
            {
                LoadProperty<CategoryGoalDetailPriorityType>(PriorityProperty,
                    (CategoryGoalDetailPriorityType)Enum.Parse(typeof(CategoryGoalDetailPriorityType), dc.Priority));
            }

            LoadProperty<Int16>(CompleteProperty, dc.Complete);
            LoadProperty<CategoryGoalDetailLocationList>(LocationsProperty, CategoryGoalDetailLocationList.GetFromDataContracts(dc.Locations));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dc">The data contract</param>
        private void Child_Fetch(Services.CategoryGoal.CategoryGoalDetail dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}
