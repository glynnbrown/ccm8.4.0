﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryTactic
    {
        #region Constructor
        private CategoryTactic() { }
        #endregion

        #region Factory Methods


        /// <summary>
        /// Returns the category tactic for the the given GFS entity name and product group code
        /// </summary>
        /// <returns></returns>
        public static CategoryTactic FetchCategoryTacticByGfsEntityNameProductGroupCode(String endpointRoot, String gfsEntityName, String productGroupCode)
        {
            return DataPortal.Fetch<CategoryTactic>(new FetchCategoryTacticByGfsEntityNameProductGroupCodeCriteria(endpointRoot, gfsEntityName, productGroupCode));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryTactic.CategoryTactic dc)
        {
            LoadProperty<Int32>(IdProperty, dc.Id);
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dc.UniqueContentReference);
            LoadProperty<Int32>(ContentVersionProperty, dc.ContentVersion);
            LoadProperty<String>(UserFullNameProperty, dc.UserFullName);
            LoadProperty<DateTime>(DateLastModifiedProperty, dc.DateLastModified);

            LoadProperty<String>(StrategyNameProperty, dc.StrategyName);
            LoadProperty<String>(StrategyDescriptionProperty, dc.StrategyDescription);
            LoadProperty<CategoryTacticDetailList>(DetailsProperty, CategoryTacticDetailList.GetFromDataContracts(dc.Details));
            LoadProperty<CategoryTacticStrategyLocationList>(StrategyLocationsProperty, CategoryTacticStrategyLocationList.GetFromDataContracts(dc.StrategyLocations));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchByEntityIdProductGroupCode
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchCategoryTacticByGfsEntityNameProductGroupCodeCriteria criteria)
        {
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var svc = client.CategoryTacticServiceClient;
                var request = new Services.CategoryTactic.GetCategoryTacticByEntityNameProductGroupCodeRequest(criteria.EntityName, criteria.ProductGroupCode);
                var response = svc.GetCategoryTacticByEntityNameProductGroupCode(request);
                LoadDataContract(response.CategoryTactic);
            }
        }

        #endregion

        #endregion
    }
}
