﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Helpers;
using Galleria.Framework.Enums;

namespace Galleria.Ccm.GFSModel
{
    public partial class Metric
    {
        #region Constructor
        private Metric() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a metric from a provided data contract
        /// </summary>
        internal static Metric GetMetric(Services.Metric.Metric dc)
        {
            return DataPortal.FetchChild<Metric>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data contract
        /// </summary>
        private void LoadDataContract(Services.Metric.Metric dc)
        {
            this.LoadProperty<String>(NameProperty, dc.Name);
            this.LoadProperty<String>(DescriptionProperty, dc.Description);
            this.LoadProperty<MetricDirectionType>(DirectionProperty, EnumHelper.Parse<MetricDirectionType>(dc.Direction, MetricDirectionType.MaximiseIncrease));
            this.LoadProperty<MetricSpecialType>(SpecialTypeProperty, EnumHelper.Parse<MetricSpecialType>(dc.SpecialType, MetricSpecialType.None));
            this.LoadProperty<MetricType>(MetricTypeProperty, EnumHelper.Parse<MetricType>(dc.Type, MetricType.Integer));
            this.LoadProperty<String>(CalculationProperty, dc.Calculation);
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance
        /// from a data contract
        /// </summary>
        private void Child_Fetch(Services.Metric.Metric dc)
        {
            this.LoadDataContract(dc);
        }
        #endregion

        #endregion
    }
}
