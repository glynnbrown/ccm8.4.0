﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//  V8-27783 : M.Brumby
//      Created.
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class Project
    {
        #region Constructor
        private Project() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates an Project from the given dc
        /// </summary>
        internal static Project GetProject(Services.Project.ProjectInfo dc)
        {
            return DataPortal.FetchChild<Project>(dc);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data contract
        /// </summary>
        private void LoadDataContract(Services.Project.ProjectInfo dc)
        {
            
            this.LoadProperty<Int32>(IdProperty, dc.Id);
            this.LoadProperty<String>(NameProperty, dc.Name);
            this.LoadProperty<DateTime?>(DateUnavailableProperty, dc.DateAvailable);
            this.LoadProperty<DateTime?>(DateUnavailableProperty, dc.DateUnavailable);
            
        }
        #endregion

        #region Fetch
        
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(Services.Project.ProjectInfo dc)
        {
            LoadDataContract(dc);
        }
        #endregion

        #endregion
    }
}
