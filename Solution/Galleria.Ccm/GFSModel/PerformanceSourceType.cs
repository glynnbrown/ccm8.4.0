﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// Enum of values available for PerformanceSourceType property
    /// Represents a type of the performance source
    /// </summary>
    public enum PerformanceSourceType
    {
        Unknown = 0,
        Simple = 1,
        Advanced = 2,
    }

    public static class PerformanceSourceTypeHelper
    {
        public static readonly Dictionary<PerformanceSourceType, String> FriendlyNames =
            new Dictionary<PerformanceSourceType, String>()
            {
                {PerformanceSourceType.Unknown, Message.Enum_GFSPerformanceSourceType_Unknown},
                {PerformanceSourceType.Simple, Message.Enum_GFSPerformanceSourceType_Simple},
                {PerformanceSourceType.Advanced, Message.Enum_GFSPerformanceSourceType_Advanced},
            };

        public static PerformanceSourceType? PerformanceSourceTypeGetEnum(String description)
        {
            foreach (KeyValuePair<PerformanceSourceType, String> keyPair in PerformanceSourceTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}