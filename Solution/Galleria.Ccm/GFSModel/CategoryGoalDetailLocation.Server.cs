﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryGoalDetailLocation
    {
        #region Constructor
        private CategoryGoalDetailLocation() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the CategoryGoalDetailLocation
        /// </summary>
        internal static CategoryGoalDetailLocation GetCategoryGoalDetailLocation(Services.CategoryGoal.CategoryGoalDetailLocation dc)
        {
            return DataPortal.FetchChild<CategoryGoalDetailLocation>(dc);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryGoal.CategoryGoalDetailLocation dc)
        {
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<String>(CodeProperty, dc.Code);
            LoadProperty<Int32>(IsIncludedProperty, dc.IsIncluded);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dc">The data contract</param>
        private void Child_Fetch(Services.CategoryGoal.CategoryGoalDetailLocation dc)
        {
            LoadDataContract(dc);
        }

        #endregion

        #endregion
    }
}
