﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25556 : D.Pleasance
//  Created
// V8-26773 : N.Foster
//  Added planogram performance interfaces
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class PerformanceSource :
        ModelReadOnlyObject<PerformanceSource>,
        IPlanogramPerformance
    {
        #region Properties

        #region Id
        /// <summary>
        /// The property id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region EntityName
        /// <summary>
        /// The entity name
        /// </summary>
        public static readonly ModelPropertyInfo<String> EntityNameProperty =
            RegisterModelProperty<String>(c => c.EntityName);
        public String EntityName
        {
            get { return GetProperty<String>(EntityNameProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// The name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }
        #endregion

        #region Description
        /// <summary>
        /// The description
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
        }
        #endregion

        #region LocationLevelName
        /// <summary>
        /// Location Level Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationLevelNameProperty =
            RegisterModelProperty<String>(c => c.LocationLevelName);
        public String LocationLevelName
        {
            get { return GetProperty<String>(LocationLevelNameProperty); }
        }
        #endregion

        #region ProductLevelName
        /// <summary>
        /// Product Level Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductLevelNameProperty =
            RegisterModelProperty<String>(c => c.ProductLevelName);
        public String ProductLevelName
        {
            get { return GetProperty<String>(ProductLevelNameProperty); }
        }
        #endregion

        #region TimelineLevelName
        /// <summary>
        /// Timeline Level Name
        /// </summary>
        public static readonly ModelPropertyInfo<String> TimelineLevelNameProperty =
            RegisterModelProperty<String>(c => c.TimelineLevelName);
        public String TimelineLevelName
        {
            get { return GetProperty<String>(TimelineLevelNameProperty); }
        }
        #endregion

        #region Type
        /// <summary>
        /// Type
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSourceType> TypeProperty =
            RegisterModelProperty<PerformanceSourceType>(c => c.Type);
        public PerformanceSourceType Type
        {
            get { return GetProperty<PerformanceSourceType>(TypeProperty); }
        }
        #endregion

        #region PerformanceSourceMetrics
        /// <summary>
        /// Performance Source Metrics
        /// </summary>
        public static readonly ModelPropertyInfo<PerformanceSourceMetricList> PerformanceSourceMetricsProperty =
            RegisterModelProperty<PerformanceSourceMetricList>(c => c.PerformanceSourceMetrics);
        public PerformanceSourceMetricList PerformanceSourceMetrics
        {
            get { return GetProperty<PerformanceSourceMetricList>(PerformanceSourceMetricsProperty); }
        }
        #endregion

        #region DateLastProcessed
        /// <summary>
        /// Date Last Processed
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateLastProcessedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateLastProcessed);
        public DateTime? DateLastProcessed
        {
            get { return GetProperty<DateTime?>(DateLastProcessedProperty); }
        }
        #endregion

        #region DataType
        /// <summary>
        /// Data Type
        /// </summary>
        public static readonly ModelPropertyInfo<String> DataTypeProperty =
            RegisterModelProperty<String>(c => c.DataType);
        public String DataType
        {
            get { return GetProperty<String>(DataTypeProperty); }
        }
        #endregion

        #region DaysPerPeriod
        /// <summary>
        /// Days Per Period
        /// </summary>
        public static readonly ModelPropertyInfo<String> DaysPerPeriodProperty =
            RegisterModelProperty<String>(c => c.DaysPerPeriod);
        public String DaysPerPeriod
        {
            get { return GetProperty<String>(DaysPerPeriodProperty); }
        }
        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion

        #region IPlanogramPerformance Members
        /// <summary>
        /// Returns all the metrics within the performance source
        /// </summary>
        /// <remarks>
        /// We return an empty list as a we cannot map
        /// performance source metrics to planogram
        /// performance metrics
        /// </remarks>
        IEnumerable<IPlanogramPerformanceMetric> IPlanogramPerformance.Metrics
        {
            get { return new List<IPlanogramPerformanceMetric>(); }
        }
        #endregion
    }
}