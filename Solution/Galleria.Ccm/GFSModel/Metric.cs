﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Model;
using Galleria.Framework.Interfaces;
using Galleria.Framework.Enums;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class Metric :
        ModelReadOnlyObject<Metric>,
        IMetric, IPlanogramPerformanceMetric
    {
        #region Properties

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the metric name
        /// </summary>
        public String Name
        {
            get { return this.ReadProperty<String>(NameProperty); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);
        /// <summary>
        /// Returns the metric description
        /// </summary>
        public String Description
        {
            get { return this.ReadProperty<String>(DescriptionProperty); }
        }
        #endregion

        #region Direction
        /// <summary>
        /// Direction property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricDirectionType> DirectionProperty =
            RegisterModelProperty<MetricDirectionType>(c => c.Direction);
        /// <summary>
        /// Returns the metric direction
        /// </summary>
        public MetricDirectionType Direction
        {
            get { return this.ReadProperty<MetricDirectionType>(DirectionProperty); }
        }
        #endregion

        #region SpecialType
        /// <summary>
        /// SpecialType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricSpecialType> SpecialTypeProperty =
            RegisterModelProperty<MetricSpecialType>(c => c.SpecialType);
        /// <summary>
        /// Returns the planogram metric special type
        /// </summary>
        public MetricSpecialType SpecialType
        {
            get { return this.ReadProperty<MetricSpecialType>(SpecialTypeProperty); }
        }
        #endregion

        #region MetricType
        /// <summary>
        /// MetricType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricType> MetricTypeProperty =
            RegisterModelProperty<MetricType>(c => c.Type);
        /// <summary>
        /// Returns the metric type
        /// </summary>
        public MetricType Type
        {
            get { return this.ReadProperty<MetricType>(MetricTypeProperty); }
        }
        #endregion

        #region MetricId
        /// <summary>
        /// MetricId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> MetricIdProperty =
            RegisterModelProperty<Byte>(c => c.MetricId);
        /// <summary>
        /// Returns the metric id
        /// </summary>
        public Byte MetricId
        {
            get { return this.ReadProperty<Byte>(MetricIdProperty); }
        }

        #endregion

        #region DataModelType

        /// <summary>
        /// Not implemented in CCM
        /// </summary>
        public MetricElasticityDataModelType DataModelType
        {
            get
            {
                throw new NotImplementedException();
            }
        }
        #endregion
        #region Calculation

        /// <summary>
        /// Equation used to calculate performance that uses this metric
        /// </summary>
        public static readonly ModelPropertyInfo<String> CalculationProperty =
            RegisterModelProperty<String>(c => c.Calculation);
        public String Calculation
        {
            get
            {
                { return this.ReadProperty<String>(CalculationProperty); }
            }
        }
        #endregion
        #region IsCalculated

        /// <summary>
        /// Boolean to show if this is a calculated metric or not
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> IsCalculatedProperty =
            RegisterModelProperty<Boolean>(c => c.IsCalculated);
        public Boolean IsCalculated
        {
            get
            {
                return !String.IsNullOrWhiteSpace(Calculation);
            }
        }
        #endregion
        #endregion

        #region Overrides
        /// <summary>
        /// Retuns a string representation of this instance
        /// </summary>
        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
