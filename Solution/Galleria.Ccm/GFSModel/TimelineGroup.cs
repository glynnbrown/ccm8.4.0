﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26159 : L.Ineson
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// A timeline group in GFS.
    /// </summary>
    [Serializable]
    public sealed partial class TimelineGroup : ModelReadOnlyObject<TimelineGroup>
    {
        #region Properties

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Returns the timeline group name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Code

        /// <summary>
        /// Code property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int64> CodeProperty =
            RegisterModelProperty<Int64>(c => c.Code);
        /// <summary>
        /// Returns the timeline group name
        /// </summary>
        public Int64 Code
        {
            get { return this.GetProperty<Int64>(CodeProperty); }
        }

        #endregion

        #region TimelineLevelName

        /// <summary>
        /// TimelineLevelName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TimelineLevelNameProperty =
            RegisterModelProperty<String>(c => c.TimelineLevelName);
        /// <summary>
        /// Returns the timeline level name to which the group belongs
        /// </summary>
        public String TimelineLevelName
        {
            get { return this.GetProperty<String>(TimelineLevelNameProperty); }
        }

        #endregion

        #region Children

        /// <summary>
        /// Children property definition
        /// </summary>
        public static readonly ModelPropertyInfo<TimelineGroupList> ChildrenProperty =
            RegisterModelProperty<TimelineGroupList>(c => c.Children);
        /// <summary>
        /// Returns the list of child groups
        /// </summary>
        public TimelineGroupList Children
        {
            get { return GetProperty<TimelineGroupList>(ChildrenProperty); }
        }

        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
