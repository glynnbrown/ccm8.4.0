﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26773 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    // A list of gfs performance sources
    [Serializable]
    public partial class MetricList :
        ModelReadOnlyList<MetricList, Metric>
    {
        #region Criteria

        #region FetchAllCriteria
        /// <summary>
        /// Criteria for FetchByCcmEntity
        /// </summary>
        [Serializable]
        public sealed class FetchAllCriteria : CriteriaBase<FetchAllCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region EntityName
            /// <summary>
            /// EntityName property definition
            /// </summary>
            public static readonly PropertyInfo<String> EntityNameProperty =
                RegisterProperty<String>(c => c.EntityName);
            /// <summary>
            /// Returns the gfs entity name
            /// </summary>
            public String EntityName
            {
                get { return this.ReadProperty<String>(EntityNameProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchAllCriteria(GFSModel.Entity entity)
            {
                this.LoadProperty<String>(EndpointRootProperty, entity.EndpointRoot);
                this.LoadProperty<String>(EntityNameProperty, entity.Name);
            }
            #endregion
        }
        #endregion

        #endregion
    }
}
