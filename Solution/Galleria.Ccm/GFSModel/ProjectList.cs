﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//  V8-27783 : M.Brumby
//      Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Model;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class ProjectList : ModelReadOnlyList<ProjectList, Project>
    {
        #region Criteria

        #region FetchAllCriteria
        /// <summary>
        /// Criteria for FetchByCcmEntity
        /// </summary>
        [Serializable]
        public sealed class FetchAllCriteria : CriteriaBase<FetchAllCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region EntityName
            /// <summary>
            /// EntityName property definition
            /// </summary>
            public static readonly PropertyInfo<String> EntityNameProperty =
                RegisterProperty<String>(c => c.EntityName);
            /// <summary>
            /// Returns the gfs entity name
            /// </summary>
            public String EntityName
            {
                get { return this.ReadProperty<String>(EntityNameProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchAllCriteria(GFSModel.Entity entity)
            {
                this.LoadProperty<String>(EndpointRootProperty, entity.EndpointRoot);
                this.LoadProperty<String>(EntityNameProperty, entity.Name);
            }
            #endregion
        }
        #endregion

        #region FetchByProductGroupCodeEntityIdCriteria
        /// <summary>
        /// Criteria for FetchByProductGroupCodeEntityIdCriteria
        /// </summary>
        [Serializable]
        public sealed class GetProjectInfoByEntityNameProductGroupCodeCriteria : CriteriaBase<GetProjectInfoByEntityNameProductGroupCodeCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region EntityName
            /// <summary>
            /// EntityName property definition
            /// </summary>
            public static readonly PropertyInfo<String> EntityNameProperty =
                RegisterProperty<String>(c => c.EntityName);
            /// <summary>
            /// Returns the gfs entity name
            /// </summary>
            public String EntityName
            {
                get { return this.ReadProperty<String>(EntityNameProperty); }
            }
            #endregion

            #region ProductGroupCode
            /// <summary>
            /// ProductGroupCode property definition
            /// </summary>
            public static readonly PropertyInfo<String> ProductGroupCodeProperty =
                RegisterProperty<String>(c => c.ProductGroupCode);
            /// <summary>
            /// Returns the product group code
            /// </summary>
            public String ProductGroupCode
            {
                get { return this.ReadProperty<String>(ProductGroupCodeProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public GetProjectInfoByEntityNameProductGroupCodeCriteria(GFSModel.Entity entity, String productGroupCode)
            {
                this.LoadProperty<String>(EndpointRootProperty, entity.EndpointRoot);
                this.LoadProperty<String>(EntityNameProperty, entity.Name);
                this.LoadProperty<String>(ProductGroupCodeProperty, productGroupCode);
            }
            #endregion
        }
        #endregion

        #region GetProjectInfosWithNoProductGroupAssignedByEntityName
        /// <summary>
        /// Criteria for GetProjectInfosWithNoProductGroupAssignedByEntityName
        /// </summary>
        [Serializable]
        public sealed class GetProjectInfosWithNoProductGroupAssignedByEntityNameCriteria : CriteriaBase<GetProjectInfosWithNoProductGroupAssignedByEntityNameCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region EntityName
            /// <summary>
            /// EntityName property definition
            /// </summary>
            public static readonly PropertyInfo<String> EntityNameProperty =
                RegisterProperty<String>(c => c.EntityName);
            /// <summary>
            /// Returns the gfs entity name
            /// </summary>
            public String EntityName
            {
                get { return this.ReadProperty<String>(EntityNameProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public GetProjectInfosWithNoProductGroupAssignedByEntityNameCriteria(GFSModel.Entity entity)
            {
                this.LoadProperty<String>(EndpointRootProperty, entity.EndpointRoot);
                this.LoadProperty<String>(EntityNameProperty, entity.Name);
            }
            #endregion
        }
        #endregion

        #endregion
    }
}
