﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26365 : L.Ineson
//  Updated so that mock services can be used in testing.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class PerformanceSourceList
    {
        #region Constructors
        private PerformanceSourceList() { }//Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all performance sources in gfs
        /// </summary>
        public static PerformanceSourceList FetchAll(Entity entity)
        {
            return DataPortal.Fetch<PerformanceSourceList>(new FetchAllCriteria(entity));
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called by FetchByEntityName
        /// </summary>
        private void DataPortal_Fetch(FetchAllCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var performanceSvc = client.PerformanceServiceClient;
                var request = new Services.Performance.GetPerformanceSourceByEntityNameRequest(criteria.EntityName);
                var response = performanceSvc.GetPerformanceSourceByEntityName(request);
                foreach (Services.Performance.PerformanceSource dc in response.PerformanceSources)
                {
                    this.Add(PerformanceSource.GetPerformanceSource(dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}