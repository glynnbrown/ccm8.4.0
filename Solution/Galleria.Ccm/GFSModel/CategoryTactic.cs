﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Csla;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class CategoryTactic : ModelReadOnlyObject<CategoryTactic>
    {
        #region Properties

        /// <summary>
        /// The property id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The category tactic name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The category tactic UCR
        /// </summary>
        public static readonly ModelPropertyInfo<Guid> UniqueContentReferenceProperty =
            RegisterModelProperty<Guid>(c => c.UniqueContentReference);
        public Guid UniqueContentReference
        {
            get { return GetProperty<Guid>(UniqueContentReferenceProperty); }
        }

        /// <summary>
        /// The category tactic content version
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ContentVersionProperty =
            RegisterModelProperty<Int32>(c => c.ContentVersion);
        public Int32 ContentVersion
        {
            get { return GetProperty<Int32>(ContentVersionProperty); }
        }

        /// <summary>
        /// The category tactic strategy name
        /// </summary>
        public static readonly ModelPropertyInfo<String> StrategyNameProperty =
            RegisterModelProperty<String>(c => c.StrategyName);
        public String StrategyName
        {
            get { return GetProperty<String>(StrategyNameProperty); }
        }

        /// <summary>
        /// The category tactic strategy description
        /// </summary>
        public static readonly ModelPropertyInfo<String> StrategyDescriptionProperty =
            RegisterModelProperty<String>(c => c.StrategyDescription);
        public String StrategyDescription
        {
            get { return GetProperty<String>(StrategyDescriptionProperty); }
        }

        /// <summary>
        /// The category tactic author's full name
        /// </summary>
        public static readonly ModelPropertyInfo<String> UserFullNameProperty =
            RegisterModelProperty<String>(c => c.UserFullName);
        public String UserFullName
        {
            get { return GetProperty<String>(UserFullNameProperty); }
        }

        /// <summary>
        /// The last modified date of the category tactic
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
        }

        /// <summary>
        /// The collection of details for this category tactic
        /// </summary>
        public static readonly ModelPropertyInfo<CategoryTacticDetailList> DetailsProperty =
            RegisterModelProperty<CategoryTacticDetailList>(c => c.Details);
        public CategoryTacticDetailList Details
        {
            get { return GetProperty<CategoryTacticDetailList>(DetailsProperty); }
        }

        /// <summary>
        /// The collection of locaations for this category tactic's strategy
        /// </summary>
        public static readonly ModelPropertyInfo<CategoryTacticStrategyLocationList> StrategyLocationsProperty =
            RegisterModelProperty<CategoryTacticStrategyLocationList>(c => c.StrategyLocations);
        public CategoryTacticStrategyLocationList StrategyLocations
        {
            get { return GetProperty<CategoryTacticStrategyLocationList>(StrategyLocationsProperty); }
        }

        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for FetchCategoryTacticByGfsEntityNameProductGroupCode
        /// </summary>
        [Serializable]
        public class FetchCategoryTacticByGfsEntityNameProductGroupCodeCriteria : Csla.CriteriaBase<FetchCategoryTacticByGfsEntityNameProductGroupCodeCriteria>
        {
            #region Properties

            #region EndpointRoot
            /// <summary>
            /// EndpointRoot property definition
            /// </summary>
            public static readonly PropertyInfo<String> EndpointRootProperty =
                RegisterProperty<String>(c => c.EndpointRoot);
            /// <summary>
            /// Returns the endpoint root
            /// </summary>
            public String EndpointRoot
            {
                get { return this.ReadProperty<String>(EndpointRootProperty); }
            }
            #endregion

            #region EntityName
            /// <summary>
            /// EntityName property definition
            /// </summary>
            public static readonly PropertyInfo<String> EntityNameProperty =
                RegisterProperty<String>(c => c.EntityName);
            /// <summary>
            /// Returns the gfs entity name
            /// </summary>
            public String EntityName
            {
                get { return this.ReadProperty<String>(EntityNameProperty); }
            }
            #endregion

            #region ProductGroupCode

            public static readonly PropertyInfo<String> ProductGroupCodeProperty =
                RegisterProperty<String>(c => c.ProductGroupCode);
            public String ProductGroupCode
            {
                get { return ReadProperty<String>(ProductGroupCodeProperty); }
            }

            #endregion

            #endregion

            public FetchCategoryTacticByGfsEntityNameProductGroupCodeCriteria(String endpointRoot, String gfsEntityName, String productGroupCode)
            {
                LoadProperty<String>(EndpointRootProperty, endpointRoot);
                LoadProperty<String>(EntityNameProperty, gfsEntityName);
                LoadProperty<String>(ProductGroupCodeProperty, productGroupCode);
            }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
