﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29491 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// Enum of values available for PerformanceSourceDaysPerPeriodType property
    /// Represents a days per period property of the performance source
    public enum PerformanceSourceDaysPerPeriodType
    {
        Unknown = 0,
        Year = 365,
        Quarter = 91,
        Month = 31,
        Period = 28,
        Week = 7
    }

    /// <summary>
    /// DaysPerPeriod Dictionary
    /// </summary>
    public static class PerformanceSourceDaysPerPeriodTypeHelper
    {
        public static readonly Dictionary<PerformanceSourceDaysPerPeriodType, String> FriendlyNames =
            new Dictionary<PerformanceSourceDaysPerPeriodType, String>()
            {
                {PerformanceSourceDaysPerPeriodType.Unknown, Message.Enum_PerformanceSourceDaysPerPeriodType_Unknown},
                {PerformanceSourceDaysPerPeriodType.Year, Message.Enum_PerformanceSourceDaysPerPeriodType_Year},
                {PerformanceSourceDaysPerPeriodType.Quarter, Message.Enum_PerformanceSourceDaysPerPeriodType_Quarter},  
                {PerformanceSourceDaysPerPeriodType.Month, Message.Enum_PerformanceSourceDaysPerPeriodType_Month},
                {PerformanceSourceDaysPerPeriodType.Period, Message.Enum_PerformanceSourceDaysPerPeriodType_Period},  
                {PerformanceSourceDaysPerPeriodType.Week, Message.Enum_PerformanceSourceDaysPerPeriodType_Week}, 
            };

        public static PerformanceSourceDaysPerPeriodType? PerformanceSourceDaysPerPeriodTypeGetEnum(String description)
        {
            foreach (KeyValuePair<PerformanceSourceDaysPerPeriodType, String> keyPair in PerformanceSourceDaysPerPeriodTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}