﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    /// <summary>
    /// A list of category tactic detail locations
    /// </summary>
    [Serializable]
    public partial class CategoryTacticDetailLocationList : ModelReadOnlyList<CategoryTacticDetailLocationList, CategoryTacticDetailLocation>
    {
    }
}
