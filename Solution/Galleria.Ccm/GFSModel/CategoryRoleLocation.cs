﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class CategoryRoleLocation : ModelReadOnlyObject<CategoryRoleLocation>
    {
        #region Properties

        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code);
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> RoleIdProperty =
            RegisterModelProperty<Int32>(c => c.RoleId);
        public Int32 RoleId
        {
            get { return GetProperty<Int32>(RoleIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> RoleNameProperty =
            RegisterModelProperty<String>(c => c.RoleName);
        public String RoleName
        {
            get { return GetProperty<String>(RoleNameProperty); }
        }

        #endregion

        #region Overrides

        public override string ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
