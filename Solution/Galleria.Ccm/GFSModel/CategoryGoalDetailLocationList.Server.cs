﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System.Collections.Generic;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryGoalDetailLocationList
    {
        #region Constructors
        private CategoryGoalDetailLocationList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a category goal detail location list from a list of data contracts
        /// </summary>
        internal static CategoryGoalDetailLocationList GetFromDataContracts(IEnumerable<Services.CategoryGoal.CategoryGoalDetailLocation> categoryGoalDetailLocationDcs)
        {
            return DataPortal.Fetch<CategoryGoalDetailLocationList>(categoryGoalDetailLocationDcs);
        }
        #endregion

        #region Data Access

        private void DataPortal_Fetch(IEnumerable<Services.CategoryGoal.CategoryGoalDetailLocation> categoryGoalDetailLocationDcs)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            foreach (Services.CategoryGoal.CategoryGoalDetailLocation dc in categoryGoalDetailLocationDcs)
            {
                this.Add(CategoryGoalDetailLocation.GetCategoryGoalDetailLocation(dc));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
