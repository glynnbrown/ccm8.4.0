﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class CategoryTacticDetail : ModelReadOnlyObject<CategoryTacticDetail>
    {
        #region Properties

        /// <summary>
        /// The category tactic detail's name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// The category tactic detail's type name
        /// </summary>
        public static readonly ModelPropertyInfo<String> TacticTypeNameProperty =
            RegisterModelProperty<String>(c => c.TacticTypeName);
        public String TacticTypeName
        {
            get { return GetProperty<String>(TacticTypeNameProperty); }
        }

        /// <summary>
        /// The category tactic detail's linked locations
        /// </summary>
        public static readonly ModelPropertyInfo<CategoryTacticDetailLocationList> LocationsProperty =
            RegisterModelProperty<CategoryTacticDetailLocationList>(c => c.Locations);
        public CategoryTacticDetailLocationList Locations
        {
            get { return GetProperty<CategoryTacticDetailLocationList>(LocationsProperty); }
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        #endregion
    }
}
