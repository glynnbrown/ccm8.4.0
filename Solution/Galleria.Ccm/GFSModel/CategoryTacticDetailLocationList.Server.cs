﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System.Collections.Generic;
using Csla;

namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryTacticDetailLocationList
    {
        #region Constructors
        private CategoryTacticDetailLocationList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a category tactic detail location list from a list of data contracts
        /// </summary>
        internal static CategoryTacticDetailLocationList GetFromDataContracts(IEnumerable<Services.CategoryTactic.CategoryTacticDetailLocation> categoryTacticDetailLocationDcs)
        {
            return DataPortal.Fetch<CategoryTacticDetailLocationList>(categoryTacticDetailLocationDcs);
        }
        #endregion

        #region Data Access

        private void DataPortal_Fetch(IEnumerable<Services.CategoryTactic.CategoryTacticDetailLocation> categoryTacticDetailLocationDcs)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;

            foreach (Services.CategoryTactic.CategoryTacticDetailLocation dc in categoryTacticDetailLocationDcs)
            {
                this.Add(CategoryTacticDetailLocation.GetCategoryTacticDetailLocation(dc));
            }

            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        #endregion
    }
}
