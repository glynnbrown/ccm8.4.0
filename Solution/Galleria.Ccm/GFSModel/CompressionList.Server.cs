﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26157 : L.Ineson
//  Copied from SA
// V8-26365 : L.Ineson
//  Updated so that mock services can be used in testing.
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class CompressionList
    {
        #region Constructors
        private CompressionList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all compressions in gfs
        /// </summary>
        public static CompressionList FetchAll(Entity entity)
        {
            return DataPortal.Fetch<CompressionList>(new SingleCriteria<String>(entity.EndpointRoot));
        }

        /// <summary>
        /// Returns all compressions in gfs
        /// </summary>
        public static CompressionList FetchAll(Model.Entity entity)
        {
            return DataPortal.Fetch<CompressionList>(new SingleCriteria<String>(entity.SystemSettings.FoundationServicesEndpoint));
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called when fetching all compression types
        /// </summary>
        private void DataPortal_Fetch(SingleCriteria<String> criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.Value))
            {
                var svc = client.ImageServiceClient;
                var response = svc.GetAllCompressions(new Services.Image.GetAllCompressionsRequest());
                foreach (Services.Image.Compression dc in response.Compressions)
                {
                    this.Add(Compression.GetCompression(dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
