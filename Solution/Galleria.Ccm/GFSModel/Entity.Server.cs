﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
// V8-26365 : L.Ineson
//  Updated so that mock services can be used in testing.
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Services;

namespace Galleria.Ccm.GFSModel
{
    public partial class Entity
    {
        #region Constructor
        private Entity() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the gfs entity for the provided ccm entity
        /// </summary>
        public static Entity FetchByCcmEntity(Model.Entity entity)
        {
            return DataPortal.Fetch<Entity>(new FetchByCcmEntityCriteria(entity));
        }

        /// <summary>
        /// Creates an entity from the given dc
        /// </summary>
        internal static Entity GetEntity(String endpointRoot, Services.Entity.Entity dc)
        {
            return DataPortal.FetchChild<Entity>(endpointRoot, dc);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data contract
        /// </summary>
        private void LoadDataContract(String endpointRoot, Services.Entity.Entity dc)
        {
            this.LoadProperty<Int32>(IdProperty, dc.Id);
            this.LoadProperty<String>(NameProperty, dc.Name);
            this.LoadProperty<String>(DescriptionProperty, dc.Description);
            this.LoadProperty<String>(EndpointRootProperty, endpointRoot);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching by a gfs entity
        /// </summary>
        private void DataPortal_Fetch(FetchByCcmEntityCriteria criteria)
        {
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var svc = client.EntityServiceClient;
                var response = svc.GetAllEntities(new Services.Entity.GetAllEntitiesRequest());
                this.LoadDataContract(criteria.EndpointRoot, response.Entities.FirstOrDefault(c => c.Id == criteria.GfsEntityId));
            }
        }

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        private void Child_Fetch(String endpointRoot, Services.Entity.Entity dc)
        {
            LoadDataContract(endpointRoot, dc);
        }
        #endregion

        #endregion
    }
}