﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26159 : L.Ineson
//  Created
// V8-26365 : L.Ineson
//  Updated so that mock services can be used in testing.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Services;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.GFSModel
{
    public partial class TimelineGroupList
    {
        #region Constructors
        private TimelineGroupList() { }//Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all timeline groups for the given gfs entity name,
        /// performance source name and date range
        /// </summary>
        public static TimelineGroupList FetchByDateRange(
            Entity entity, PerformanceSource performanceSource,
            DateTime startDate, DateTime endDate)
        {
            return DataPortal.Fetch<TimelineGroupList>(new FetchByDateRangeCriteria(entity, performanceSource, startDate, endDate));
        }

        /// <summary>
        /// Returns a list from a list of data contracts
        /// </summary>
        internal static TimelineGroupList GetFromDataContracts(List<Services.Timeline.TimelineGroup> dcList)
        {
            return DataPortal.Fetch<TimelineGroupList>(dcList);
        }
        #endregion

        #region Data Access
        /// <summary>
        /// Called by FetchByEntityNameSourceNameAndDateRange
        /// </summary>
        private void DataPortal_Fetch(FetchByDateRangeCriteria criteria)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var svc = client.TimelineServiceClient;
                var request = new Services.Timeline.GetTimelineGroupByEntityNamePerformanceSourceNameDateRangeRequest(
                    criteria.EntityName, criteria.PerformanceSourceName, criteria.StartDate, criteria.EndDate);
                var response = svc.GetTimelineGroupByEntityNamePerformanceSourceNameDateRange(request);
                foreach (Services.Timeline.TimelineGroup dc in response.TimelineGroups)
                {
                    this.Add(TimelineGroup.GetTimelineGroup(dc));
                }
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called by GetFromDataContracts
        /// </summary>
        private void DataPortal_Fetch(List<Services.Timeline.TimelineGroup> dcList)
        {
            this.RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            foreach (Services.Timeline.TimelineGroup dc in dcList)
            {
                this.Add(TimelineGroup.GetTimelineGroup(dc));
            }
            this.IsReadOnly = true;
            this.RaiseListChangedEvents = true;
        }
        #endregion
    }
}
