﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26157 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Model;

namespace Galleria.Ccm.GFSModel
{
    [Serializable]
    public sealed partial class Compression : ModelReadOnlyObject<Compression>
    {
        #region Properties

        /// <summary>
        /// The compression name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        /// <summary>
        /// Width
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WidthProperty =
            RegisterModelProperty<Int32>(c => c.Width);
        public Int32 Width
        {
            get { return GetProperty<Int32>(WidthProperty); }
        }

        /// <summary>
        /// Height
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> HeightProperty =
            RegisterModelProperty<Int32>(c => c.Height);
        public Int32 Height
        {
            get { return GetProperty<Int32>(HeightProperty); }
        }

        /// <summary>
        /// Maintain Aspect Ratio
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean> MaintainAspectRatioProperty =
            RegisterModelProperty<Boolean>(c => c.MaintainAspectRatio);
        public Boolean MaintainAspectRatio
        {
            get { return GetProperty<Boolean>(MaintainAspectRatioProperty); }
        }

        /// <summary>
        /// Colour Depth
        /// </summary>
        public static readonly ModelPropertyInfo<String> ColourDepthProperty =
            RegisterModelProperty<String>(c => c.ColourDepth);
        public String ColourDepth
        {
            get { return GetProperty<String>(ColourDepthProperty); }
        }

        #endregion

        #region Helper Properties

        /// <summary>
        /// Get the greatest common divider
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private Int32 getGCD(Int32 a, Int32 b)
        {
            return b == 0 ? a : getGCD(b, a % b);
        }

        /// <summary>
        /// Creates a string to show the ratio between two integers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private String CreateRatio(Int32 a, Int32 b)
        {
            Int32 gcd = getGCD(a, b);
            gcd = gcd == 0 ? 1 : gcd;
            return String.Format("{0}:{1}", Width / gcd, Height / gcd);
        }

        public static Dictionary<String, Int16> PixelFormatFriendlyNames = new Dictionary<String, Int16>()
        {
            {"pf8bit", 8}, 
            {"pf16bit", 16},            
            {"pf24bit", 24}, 
            {"pf32bit", 32}
        };

        public String Description
        {
            get
            {
                return String.Format("{0} - ({1} {2}x{3} {4}MP {5}bit Colour)",
                    Name, CreateRatio(Width, Height), Width, Height,
                    Math.Round((Width * Height) / 1000000.0f, 2),
                    PixelFormatFriendlyNames[ColourDepth]);
            }
        }

        #endregion

        #region Overrides
        public override String ToString()
        {
            return Description;
        }
        #endregion
    }
}
