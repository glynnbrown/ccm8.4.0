﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3)
// V8-31834 : L.Ineson
//  Copied from SA
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Services;


namespace Galleria.Ccm.GFSModel
{
    public partial class CategoryGoal
    {
        #region Constructor
        private CategoryGoal() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the category goal for the the given GFS entity name and product group code
        /// </summary>
        /// <returns></returns>
        public static CategoryGoal FetchCategoryGoalByGfsEntityNameProductGroupCode(String endpointRoot, String gfsEntityName, String productGroupCode)
        {
            return DataPortal.Fetch<CategoryGoal>(new FetchCategoryGoalByGfsEntityNameProductGroupCodeCriteria(endpointRoot, gfsEntityName, productGroupCode));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataContract(Services.CategoryGoal.CategoryGoal dc)
        {
            LoadProperty<Int32>(IdProperty, dc.Id);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dc.UniqueContentReference);
            LoadProperty<String>(NameProperty, dc.Name);
            LoadProperty<String>(DescriptionProperty, dc.Description);
            LoadProperty<Int32>(ContentVersionProperty, dc.ContentVersion);
            LoadProperty<String>(UserFullNameProperty, dc.UserFullName);
            LoadProperty<DateTime>(DateLastModifiedProperty, dc.DateLastModified);
            LoadProperty<CategoryGoalDetailList>(DetailsProperty, CategoryGoalDetailList.GetFromDataContracts(dc.Details));
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchCategoryGoalByGfsEntityNameProductGroupCode
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchCategoryGoalByGfsEntityNameProductGroupCodeCriteria criteria)
        {
            using (FoundationServiceClient client = new FoundationServiceClient(criteria.EndpointRoot))
            {
                var svc = client.CategoryGoalServiceClient;
                var request = new Services.CategoryGoal.GetCategoryGoalByEntityNameProductGroupCodeRequest(criteria.EntityName, criteria.ProductGroupCode);
                var response = svc.GetCategoryGoalByEntityNameProductGroupCode(request);
                LoadDataContract(response.CategoryGoal);
            }
        }

        #endregion

        #endregion
    }
   
}
