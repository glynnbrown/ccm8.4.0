﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
// V8-25664 : A.Kuszyk
//  Added User fetch to Authenticate method.
// V8-26124 : L.Ineson
//  Updated authenticate code to consider roles.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Csla;
using Csla.Core;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Dal;
using SmartAssembly.Attributes;

namespace Galleria.Ccm.Security
{
    /// <summary>
    /// Defines a domain identity within the solution
    /// </summary>
    public partial class DomainIdentity
    {
        #region Constructors
        private DomainIdentity() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when returning the current users domain identity
        /// </summary>
        /// <returns>A domain identity object</returns>
        internal static DomainIdentity GetDomainIdentity()
        {
            return DataPortal.Fetch<DomainIdentity>();
        }

        /// <summary>
        /// Called when returning the specified users domain identity
        /// </summary>
        /// <param name="username">The identity to retrieve</param>
        /// <returns>A domain identity object</returns>
        internal static DomainIdentity GetDomainIdentity(String username)
        {
            return DataPortal.Fetch<DomainIdentity>(new SingleCriteria<String>(username));
        }
        #endregion

        #region Methods

        #region Authenticate
        /// <summary>
        /// Performs authentication of this user
        /// </summary>
        /// <param name="username">The user to authenticate</param>
        [ObfuscateControlFlow]
        private void Authenticate(String username)
        {
            // determine if the users authentication details
            // already exist within the database cache
            if (this.LoadFromCache(username)) return;

            // obtain a user-specific lock
            lock (DomainAuthorizationCache.GetUserLock(username))
            {
                // while we were waiting for the lock, another thread may
                // have authenticated the user and updated the cache
                // so perform another check to see if the user is now
                // within the authentication cache
                if (this.LoadFromCache(username)) return;

                // get the default dal factory
                IDalFactory dalFactory = DalContainer.GetDalFactory();

                // ensure that our user is not authenticated
                this.Name = String.Empty;
                this.UserId = 0;
                this.IsAuthenticated = false;
                this.Roles = new MobileList<String>();

                // create a dal context to use for the inserting
                // or updating of all the data in the database
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    // attempt to get the user from the database
                    UserDto user = this.GetUser(dalContext, username);
                    if (user == null || user.DateDeleted != null)
                    {
                        // if the user does not exist or has been deleted
                        // then throw a domain authentication exception
                        throw new DomainAuthenticationException(String.Format(Message.DomainAuthenticationException_Text, username));
                    }

                    // get a list of all the existing roles from the gfs database
                    IEnumerable<RoleInfoDto> roles = this.GetRoles(dalContext);

                    // now we create some lists that will hold which roles a user is a member of
                    // we keep track of those roles that are existing, new, and deleted so that we
                    // can updated all of these in the database after we have worked out all the changes
                    List<RoleMemberDto> existingRoleMembers = this.GetRoleMembers(dalContext, user);

                    // determine if the user is an administrator by seeing if they
                    // are a member of the default administrator role in gfs
                    Boolean isAdministrator = false;
                    RoleInfoDto administratorRole = roles.FirstOrDefault(item => item.IsAdministrator);
                    if (administratorRole != null)
                    {
                        // determine if the user is a member of this administrator role
                        RoleMemberDto roleMember = existingRoleMembers.FirstOrDefault(item => item.RoleId == administratorRole.Id);
                        if (roleMember != null)
                        {
                            isAdministrator = true;
                        }
                    }

                    // set the permissions for this user
                    this.SetPermissions(dalContext, existingRoleMembers, isAdministrator);

                    // set the entity access for this user
                    this.SetEntityAccess(dalContext, existingRoleMembers, isAdministrator);

                    // set the user name id as the user must now exist in the database
                    this.Name = user.UserName;
                    this.UserId = user.Id;

                    // if we got this far the user has successfully been authenticated
                    this.IsAuthenticated = true;

                    // finally, before we release the lock we update the cache
                    // with the information that we authenticated for this user
                    DomainAuthorizationCache.AddUserCache(username, this.UserId, this.IsAuthenticated, this.Roles);
                }
            }
        }
        #endregion

        #region LoadFromCache
        /// <summary>
        /// Attempts to load the users details from the authorization cache
        /// </summary>
        /// <param name="username">The name of the user to check</param>
        /// <returns>True if the users details were loaded from the cache</returns>
        private Boolean LoadFromCache(String username)
        {
            // fetch the details from the cache
            DomainAuthorizationCacheItem cacheItem = DomainAuthorizationCache.GetUserCache(username);

            // if a cache item was not returned, then return false
            if (cacheItem == null) return false;

            // the users details exist within the cache
            // so load this instance with the cached information
            this.Name = username;
            this.UserId = cacheItem.UserId;
            this.IsAuthenticated = cacheItem.IsAuthenticated;

            // copy the cached roles
            this.Roles = new MobileList<String>();
            this.Roles.AddRange(cacheItem.Roles);

            // and return true
            return true;
        }
        #endregion

        #region GetUser
        /// <summary>
        /// Returns the current user from the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="username">The username to retrn</param>
        /// <returns>The current user dto, or null if the user does not exist</returns>
        private UserDto GetUser(IDalContext dalContext, String username)
        {
            UserDto user = null;
            try
            {
                using (IUserDal dal = dalContext.GetDal<IUserDal>())
                {
                    user = dal.FetchByUserName(username);
                }
            }
            catch (DtoDoesNotExistException)
            {
                // we ignore this exception as it is expected
            }
            return user;
        }
        #endregion

        #region GetRoles
        /// <summary>
        /// Returns all roles that have been configured within the gfs database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <returns>All roles within the gfs database</returns>
        private List<RoleInfoDto> GetRoles(IDalContext dalContext)
        {
            List<RoleInfoDto> dtoList;
            using (IRoleInfoDal dal = dalContext.GetDal<IRoleInfoDal>())
            {
                dtoList = dal.FetchAll().ToList();
            }
            return dtoList;
        }
        #endregion

        #region GetRoleMembers
        /// <summary>
        /// Returns all role members for the specified user
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="user">The user to return the data for</param>
        /// <returns>A list of role members</returns>
        private List<RoleMemberDto> GetRoleMembers(IDalContext dalContext, UserDto user)
        {
            List<RoleMemberDto> roleMembers = new List<RoleMemberDto>();
            if (user.Id != 0)
            {
                using (IRoleMemberDal dal = dalContext.GetDal<IRoleMemberDal>())
                {
                    roleMembers = dal.FetchByUserId(user.Id).ToList();
                }
            }
            return roleMembers;
        }
        #endregion

        #region GetPermissions
        /// <summary>
        /// Returns all permissions for all roles within gfs
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <returns>A list of all permissions that have been configured for gfs roles</returns>
        private List<RolePermissionDto> GetPermissions(IDalContext dalContext)
        {
            List<RolePermissionDto> dtoList;
            using (IRolePermissionDal dal = dalContext.GetDal<IRolePermissionDal>())
            {
                dtoList = dal.FetchAll().ToList();
            }
            return dtoList;
        }
        #endregion

        #region SetPermissions
        /// <summary>
        /// Loads the permissions for the current user
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="isAdministrator">Indicates if the user is an administrator</param>
        private void SetPermissions(IDalContext dalContext, List<RoleMemberDto> roleMembers, Boolean isAdministrator)
        {
            // if the user is an administrator
            if (isAdministrator)
            {
                // the user is an administrator, so simply assign
                // all permissions to this users identity
                foreach (DomainPermission permission in Enum.GetValues(typeof(DomainPermission)))
                {
                    this.Roles.Add(permission.ToString());
                }
            }
            else
            {
                // get all the role permissions from the database
                List<RolePermissionDto> permissions = this.GetPermissions(dalContext);
                foreach (RoleMemberDto roleMember in roleMembers)
                {
                    foreach (RolePermissionDto permission in permissions)
                    {
                        if (permission.RoleId == roleMember.RoleId)
                        {
                            String domainPermission = ((DomainPermission)permission.PermissionType).ToString();
                            if (!this.Roles.Contains(domainPermission))
                            {
                                this.Roles.Add(domainPermission);
                            }
                        }
                    }
                }
            }

            // now grant the special authorised permission and also
            // ensure that the restricted permission is not granted
            if (!this.Roles.Contains(DomainPermission.Authenticated.ToString()))
            {
                this.Roles.Add(DomainPermission.Authenticated.ToString());
            }

            // remove access to the restricted permission as nobody should have this
            if (this.Roles.Contains(DomainPermission.Restricted.ToString()))
            {
                this.Roles.Remove(DomainPermission.Restricted.ToString());
            }
        }
        #endregion

        #region SetEntityAccess
        /// <summary>
        /// Sets the users entity access
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="roleMembers">The role members</param>
        /// <param name="isAdministrator">Indicates if the user is an administrator or not</param>
        private void SetEntityAccess(IDalContext dalContext, List<RoleMemberDto> roleMembers, Boolean isAdministrator)
        {
            if (isAdministrator)
            {
                // user is an administrator so add all entities by default
                using (IEntityInfoDal dal = dalContext.GetDal<IEntityInfoDal>())
                {
                    IEnumerable<EntityInfoDto> entities = dal.FetchAll();
                    foreach (EntityInfoDto entity in entities)
                    {
                        this.Roles.Add(String.Format(Constants.EntityAccessPermission, entity.Id));
                    }
                }
            }
            else
            {
                // user is not an administrator so only add those entities
                // this which the user has access through a role
                using (IRoleEntityDal dal = dalContext.GetDal<IRoleEntityDal>())
                {
                    List<RoleEntityDto> roleEntities = dal.FetchAll().ToList();
                    foreach (RoleEntityDto roleEntity in roleEntities)
                    {
                        RoleMemberDto roleMember = roleMembers.FirstOrDefault(item => item.RoleId == roleEntity.RoleId);
                        if (roleMember != null)
                        {
                            String roleName = String.Format(Constants.EntityAccessPermission, roleEntity.EntityId);
                            if (!this.Roles.Contains(roleName))
                            {
                                this.Roles.Add(roleName);
                            }
                        }
                    }
                }
            }
        }
        #endregion

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when authenticating the current users identity
        /// </summary>
        private void DataPortal_Fetch()
        {
            this.Authenticate(WindowsIdentity.GetCurrent().Name);
        }

        /// <summary>
        /// Called when autenticating a specified users identity
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        private void DataPortal_Fetch(SingleCriteria<String> criteria)
        {
            this.Authenticate(criteria.Value);
        }
        #endregion

        #endregion
    }
}
