﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using Csla;
using Csla.Security;

namespace Galleria.Ccm.Security
{
    /// <summary>
    /// Defines a domain identity within the solution
    /// </summary>
    [Serializable]
    public partial class DomainIdentity : CslaIdentity
    {
        #region Properties
        public static readonly PropertyInfo<Int32> UserIdProperty =
            RegisterProperty(typeof(DomainIdentity), new PropertyInfo<Int32>("UserId"));
        /// <summary>
        /// The authenticated user id
        /// </summary>
        public Int32 UserId
        {
            get { return this.ReadProperty<Int32>(UserIdProperty); }
            private set { this.LoadProperty<Int32>(UserIdProperty, value); }
        }
        #endregion
    }
}
