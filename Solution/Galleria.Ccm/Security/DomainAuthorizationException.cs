﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Ccm.Security
{
    /// <summary>
    /// An exception that is thrown if
    /// data transfer object can not be located
    /// </summary>
    [Serializable]
    public class DomainAuthorizationException : Exception
    {
        public DomainAuthorizationException() : base() { }
        public DomainAuthorizationException(string message) : base(message) { }
        public DomainAuthorizationException(string message, Exception innerException) : base(message, innerException) { }
        protected DomainAuthorizationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
