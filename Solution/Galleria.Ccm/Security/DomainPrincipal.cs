﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Security.Principal;
using Csla;
using Csla.Security;

namespace Galleria.Ccm.Security
{
    /// <summary>
    /// The domain security principal class
    /// </summary>
    [Serializable]
    public class DomainPrincipal : CslaPrincipal
    {
        #region Constructors
        public DomainPrincipal() : base() { }
        public DomainPrincipal(IIdentity identity) : base(identity) { }
        #endregion

        #region Properties
        /// <summary>
        /// If authenticated, then returns the current authenticated user id
        /// else returns null
        /// </summary>
        public static Int32? CurrentUserId
        {
            get
            {
                DomainIdentity currentIdentity = ApplicationContext.User.Identity as DomainIdentity;
                if (currentIdentity == null) return null;
                return currentIdentity.UserId;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Logs the current user out
        /// </summary>
        public static void Logout()
        {
            ApplicationContext.User = new UnauthenticatedPrincipal();
        }

#if !SILVERLIGHT
        /// <summary>
        /// Performs a synchornous login process
        /// </summary>
        /// <returns>True if the user was authenticated, else false</returns>
        public static bool Authenticate()
        {
            IIdentity identity = new UnauthenticatedIdentity();
            try
            {
                identity = DomainIdentity.GetDomainIdentity();
            }
            catch (Exception)
            {
            }
            return SetPrincipal(identity);
        }
#endif
        /// <summary>
        /// Performs an asynchronous login process
        /// </summary>
        public static void BeginAuthentication(EventHandler<DomainAuthenticationResultEventArgs> callback)
        {
            // create a data portal to perform an async fetch on the
            // current users identity
            DataPortal<DomainIdentity> dp = new DataPortal<DomainIdentity>();

            // use an anonymous delegate for the fetch callback
            dp.FetchCompleted += ((o, e) =>
            {
                SetPrincipal(e.Object);
                callback(o, new DomainAuthenticationResultEventArgs(e.Error));
            });

            // and execute the process
            dp.BeginFetch();
        }

        /// <summary>
        /// Sets the current application principal
        /// </summary>
        /// <param name="identity">The authenticated users identity</param>
        /// <returns>True if the user was authenticated</returns>
        private static bool SetPrincipal(IIdentity identity)
        {
            // if the identity exists and was successfully authenticated
            if ((identity != null) && (identity.IsAuthenticated))
            {
                DomainPrincipal principal = new DomainPrincipal(identity);
                ApplicationContext.User = principal;
                return true;
            }
            else
            {
                Logout();
                return false;
            }
        }
        #endregion
    }
}
