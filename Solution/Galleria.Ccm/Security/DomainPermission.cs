﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
//  V8-26911 : L.Luong
//    Added EventLogs
// V8-24779 : D.Pleasance
//  Added PlanogramImportTemplate
// V8-28252 : J.Pickup
//  Added Planogram
#endregion
#region Version History: CCM801
// V8-28701 : J.Pickup
//  Added UnlockPlanogram
// V8-28702 : J.Pickup
//  Added SyncWithMerchandisingHierarchy
#endregion
#region Version History: CCM802
// V8-29010 : D.Pleasance
//  Added PlanogramNameTemplate
#endregion
#region Version History : CCM820
// V8-30527 : A.Kuszyk
//  Added PlanogramHierarchyGroupDelete option.
// V8-30738 : L.Ineson
//  Added PrintTemplate permissions.
#endregion
#region Version History : CCM830
// V8-31546 : M.Pettit
//  Added PlanogramExportTemplate option.
// V8-31945 : A.Silva
//  Added PlanogramComparisonTemplate.

#endregion

#endregion

using System.Collections.Generic;
using System;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Security
{
    /// <summary>
    /// This enumeration defines all the available types
    /// of permission throughout the solution. This enum
    /// is internal, as external code such as the client
    /// interacts with permissions through a set of model
    /// objects
    /// </summary>
    public enum DomainPermission
    {
        /////////////////////////////////////////
        // SPECIAL PERMISSIONS

        // This is a special permission that is automatically granted to
        // any user who has successfully been authenticated
        // this permission is used to provide read access to any model
        // object that is required in order to make GFS actually work
        Authenticated = 1,

        // This is a special permission that is NEVER granted to any user,
        // even administrators. It is used within the framework to ensure
        // that every model object always has its four permissions specified
        // but those actions which should not be able to be performed are
        // given this special permission
        Restricted = 2,

        // This is a special permission that is automatically granted to
        // any user who is part of the in-built administrators role
        Administrator = 3,

        /////////////////////////////////////////
        // MODEL OBJECT PERMISSIONS

        // Assortments
        AssortmentGet = 1011,
        AssortmentCreate = 1012,
        AssortmentEdit = 1013,
        AssortmentDelete = 1014,

        // Assortment Minor Revision
        AssortmentMinorRevisionGet = 1021,
        AssortmentMinorRevisionCreate = 1022,
        AssortmentMinorRevisionEdit = 1023,
        AssortmentMinorRevisionDelete = 1024,

        // Blocking
        BlockingGet = 1031,
        BlockingCreate = 1032,
        BlockingEdit = 1033,
        BlockingDelete = 1034,

        // Cluster Scheme
        ClusterSchemeGet = 1041,
        ClusterSchemeCreate = 1042,
        ClusterSchemeEdit = 1043,
        ClusterSchemeDelete = 1044,

        // Consumer Decision Tree
        ConsumerDecisionTreeGet = 1051,
        ConsumerDecisionTreeCreate = 1052,
        ConsumerDecisionTreeEdit = 1053,
        ConsumerDecisionTreeDelete = 1054,

        // Location
        LocationGet = 1061,
        LocationCreate = 1062,
        LocationEdit = 1063,
        LocationDelete = 1064,

        // Location Hierarchy
        //LocationHierarchyGet = 1071,
        //LocationHierarchyCreate = 1072,
        LocationHierarchyEdit = 1073,
        //LocationHierarchyDelete = 1074,

        //Location Plan Assignment
        //LocationPlanAssignmentGet = 1081,
        LocationPlanAssignmentCreate = 1082,
        LocationPlanAssignmentEdit = 1083,
        LocationPlanAssignmentDelete = 1084,

        // Location Product Attributes
        LocationProductAttributeGet = 1091,
        LocationProductAttributeCreate = 1092,
        LocationProductAttributeEdit = 1093,
        LocationProductAttributeDelete = 1094,

        // Location Product Illegal
        LocationProductIllegalGet = 1101,
        LocationProductIllegalCreate = 1102,
        LocationProductIllegalEdit = 1103,
        LocationProductIllegalDelete = 1104,

        // Location Space
        LocationSpaceGet = 1111,
        LocationSpaceCreate = 1112,
        LocationSpaceEdit = 1113,
        LocationSpaceDelete = 1114,

        // Metric
        MetricGet = 1121,
        MetricCreate = 1122,
        MetricEdit = 1123,
        MetricDelete = 1124,

        //Performance Selection
        PerformanceSelectionGet = 1131,
        PerformanceSelectionCreate = 1132,
        PerformanceSelectionEdit = 1133,
        PerformanceSelectionDelete = 1134,

        //PlanogramHierarchy
        //PlanogramHierarchyGet = 1141,
        //PlanogramHierarchyCreate = 1142,
        PlanogramHierarchyEdit = 1143,
        //PlanogramHierarchyDelete = 1144,
        SyncWithMerchandisingHierarchy = 1145,
        PlanogramHierarchyGroupDelete = 1146,

        // Product
        ProductGet = 1151,
        ProductCreate = 1152,
        ProductEdit = 1153,
        ProductDelete = 1154,

        // Product Hierarchy
        //ProductHierarchyGet = 1161,
        //ProductHierarchyCreate = 1162,
        ProductHierarchyEdit = 1163,
        //ProductHierarchyDelete = 1164,

        // Product Universe
        ProductUniverseGet = 1171,
        ProductUniverseCreate = 1172,
        ProductUniverseEdit = 1173,
        ProductUniverseDelete = 1174,

        // Role
        RoleGet = 1181,
        RoleCreate = 1182,
        RoleEdit = 1183,
        RoleDelete = 1184,

        //User
        UserGet = 1191,
        UserCreate = 1192,
        UserEdit = 1193,
        UserDelete = 1194,

        //Workflow
        WorkflowGet = 1201,
        WorkflowCreate = 1202,
        WorkflowEdit= 1203,
        WorkflowDelete = 1204,

        //Workpackage
        //WorkpackageGet = 1211,
        WorkpackageCreate = 1212,
        WorkpackageEdit = 1213,
        WorkpackageDelete = 1214,
        WorkpackageExecute = 1215,

        //MetricProfile
        MetricProfileGet=1221,
        MetricProfileCreate=1222,
        MetricProfileEdit=1223,
        MetricProfileDelete=1224,

        // Event Log
        EventLogGet = 1231,
        //EventLogCreate = 1232,
        //EventLogEdit = 1223,
        EventLogDelete = 1234,

        // Inventory Profile
        InventoryProfileGet = 1241,
        InventoryProfileCreate = 1242,
        InventoryProfileEdit = 1243,
        InventoryProfileDelete = 1244,

        // Renumbering Strategy
        RenumberingStrategyGet = 1261,
        RenumberingStrategyCreate = 1262,
        RenumberingStrategyEdit = 1263,
        RenumberingStrategyDelete = 1264,

        // Sequence
        SequenceGet = 1271,
        SequenceCreate = 1272,
        SequenceEdit = 1273,
        SequenceDelete = 1274,

        // Validation Template
        ValidationTemplateGet = 1281,
        ValidationTemplateCreate = 1282,
        ValidationTemplateEdit = 1283,
        ValidationTemplateDelete = 1284,

        // Content Lookup
        ContentLookupGet = 1291,
        ContentLookupCreate = 1292,
        ContentLookupEdit = 1293,
        ContentLookupDelete = 1294,

        // Entity
        //EntityGet = 1301,
        EntityCreate = 1302,
        EntityEdit = 1303,
        EntityDelete = 1304,

        // Label
        LabelGet = 1311,
        LabelCreate = 1312,
        LabelEdit = 1313,
        LabelDelete = 1314,

        // Highlight
        HighlightGet = 1321,
        HighlightCreate = 1322,
        HighlightEdit = 1323,
        HighlightDelete = 1324,

        // PlanogramImportTemplate
        PlanogramImportTemplateGet = 1331,
        PlanogramImportTemplateCreate = 1332,
        PlanogramImportTemplateEdit = 1333,
        PlanogramImportTemplateDelete = 1334,

        // Planogram (Special permission for where workpackage planogram does not exist). 
        PlanogramGet = 1341,
        PlanogramCreate = 1342,
        PlanogramEdit = 1343,
        PlanogramDelete = 1344,
        UnlockPlanogram = 1345,

        // PlanogramNameTemplate
        PlanogramNameTemplateGet = 1351,
        PlanogramNameTemplateCreate = 1352,
        PlanogramNameTemplateEdit = 1353,
        PlanogramNameTemplateDelete = 1354,   

        // PrintTemplate
        PrintTemplateGet = 1361,
        PrintTemplateCreate = 1362,
        PrintTemplateEdit = 1363,
        PrintTemplateDelete = 1364,

        // PlanogramExportTemplate
        PlanogramExportTemplateGet = 1371,
        PlanogramExportTemplateCreate = 1372,
        PlanogramExportTemplateEdit = 1373,
        PlanogramExportTemplateDelete = 1374,

        // Location Product Legal
        LocationProductLegalGet = 1381,
        LocationProductLegalCreate = 1382,
        LocationProductLegalEdit = 1383,
        LocationProductLegalDelete = 1384,
        
        // Planogram Comparison Template
        PlanogramComparisonTemplateGet = 1391,
        PlanogramComparisonTemplateCreate = 1392,
        PlanogramComparisonTemplateEdit = 1393,
        PlanogramComparisonTemplateDelete = 1394,

        /////////////////////////////////////////
        // IMPORT, EXPORT & DELETE PROCESS PERMISSIONS (2xxx)

        // Imports
        ImportLocationData = 2001,
        ImportLocationHierarchyData = 2002,
        ImportLocationProductAttributeData = 2003,
        ImportLocationProductIllegalData = 2004,
        ImportLocationSpaceData = 2005,
        ImportMerchandisingHierarchyData = 2006,
        ImportProductData = 2007,
        //ImportProductUniverseData = 2008,
        ImportAssortmentData = 2009,
        ImportLocationClusterData = 2010,
        ImportLocationProductLegalData = 201,

        // Exports
        ExportLocationData = 2101,
        ExportLocationHierarchyData = 2102,
        ExportLocationProductAttributeData = 2103,
        ExportLocationProductIllegalData = 2104,
        ExportLocationSpaceData = 2105,
        ExportMerchandisingHierarchyData = 2106,
        ExportProductData = 2107,
        //ExportProductUniverseData = 2108,
        ExportAssortmentData = 2109,
        ExportLocationClusterData = 2110,
        ExportLocationProductLegalData = 2111,

        // Deletes
        DeleteData = 2201,

    }

    /// <summary>
    /// Helper to locate friendly names for the domain permissions
    /// </summary>
    public static class DomainPermissionHelper
    {
        /// <summary>
        /// Friendly names dictionary
        /// </summary>
        public static readonly Dictionary<DomainPermission, String> FriendlyNames = new Dictionary<DomainPermission, String>()
        {
            // Model Object
            {DomainPermission.AssortmentGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Assortment)},
            {DomainPermission.AssortmentCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Assortment)},
            {DomainPermission.AssortmentEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Assortment)},
            {DomainPermission.AssortmentDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Assortment)},

            {DomainPermission.AssortmentMinorRevisionGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_AssortmentMinorRevision)},
            {DomainPermission.AssortmentMinorRevisionCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_AssortmentMinorRevision)},
            {DomainPermission.AssortmentMinorRevisionEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_AssortmentMinorRevision)},
            {DomainPermission.AssortmentMinorRevisionDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_AssortmentMinorRevision)},

            {DomainPermission.ConsumerDecisionTreeGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_ConsumerDecisionTree)},
            {DomainPermission.ConsumerDecisionTreeCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_ConsumerDecisionTree)},
            {DomainPermission.ConsumerDecisionTreeEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ConsumerDecisionTree)},
            {DomainPermission.ConsumerDecisionTreeDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_ConsumerDecisionTree)},

            {DomainPermission.ClusterSchemeGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_ClusterScheme)},
            {DomainPermission.ClusterSchemeCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_ClusterScheme)},
            {DomainPermission.ClusterSchemeEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ClusterScheme)},
            {DomainPermission.ClusterSchemeDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_ClusterScheme)},

            {DomainPermission.EventLogGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_EventLog)},
            //{DomainPermission.EventLogCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_EventLog)},
            //{DomainPermission.EventLogEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_EventLog)},
            {DomainPermission.EventLogDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_DeleteAll, Message.Enum_DomainPermission_EventLog)},

            {DomainPermission.LocationGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Location)},
            {DomainPermission.LocationCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Location)},
            {DomainPermission.LocationEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Location)},
            {DomainPermission.LocationDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Location)},

            //{DomainPermission.LocationHierarchyGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_LocationHierarchy)},
            //{DomainPermission.LocationHierarchyCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_LocationHierarchy)},
            {DomainPermission.LocationHierarchyEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_LocationHierarchy)},
            //{DomainPermission.LocationHierarchyDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_LocationHierarchy)},

            //{DomainPermission.LocationPlanAssignmentGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_LocationPlanAssignment)},
            {DomainPermission.LocationPlanAssignmentCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_LocationPlanAssignment)},
            {DomainPermission.LocationPlanAssignmentEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_LocationPlanAssignment)},
            {DomainPermission.LocationPlanAssignmentDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_LocationPlanAssignment)},
                
            {DomainPermission.LocationProductAttributeGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_LocationProductAttribute)},
            {DomainPermission.LocationProductAttributeCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_LocationProductAttribute)},
            {DomainPermission.LocationProductAttributeEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_LocationProductAttribute)},
            {DomainPermission.LocationProductAttributeDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_LocationProductAttribute)},

            {DomainPermission.LocationProductIllegalGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_LocationProductIllegal)},
            {DomainPermission.LocationProductIllegalCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_LocationProductIllegal)},
            {DomainPermission.LocationProductIllegalEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_LocationProductIllegal)},
            {DomainPermission.LocationProductIllegalDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_LocationProductIllegal)},

            {DomainPermission.LocationSpaceGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_LocationSpace)},
            {DomainPermission.LocationSpaceCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_LocationSpace)},
            {DomainPermission.LocationSpaceEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_LocationSpace)},
            {DomainPermission.LocationSpaceDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_LocationSpace)},

            {DomainPermission.MetricGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Metric)},
            {DomainPermission.MetricCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Metric)},
            {DomainPermission.MetricEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Metric)},
            {DomainPermission.MetricDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Metric)},

            {DomainPermission.PerformanceSelectionGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_PerformanceSelection)},
            {DomainPermission.PerformanceSelectionCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_PerformanceSelection)},
            {DomainPermission.PerformanceSelectionEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_PerformanceSelection)},
            {DomainPermission.PerformanceSelectionDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_PerformanceSelection)},

            //{DomainPermission.PlanogramHierarchyGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_PlanogramHierarchy)},
            //{DomainPermission.PlanogramHierarchyCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_PlanogramHierarchy)},
            {DomainPermission.PlanogramHierarchyEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_PlanogramHierarchy)},
            //{DomainPermission.PlanogramHierarchyDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_PlanogramHierarchy)},
            {DomainPermission.PlanogramHierarchyGroupDelete, String.Format("{0} {1}",Message.Enum_DomainPermission_Delete,Message.Enum_DomainPermission_PlanogramHierarchyGroup)},

            {DomainPermission.ProductGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Product)},
            {DomainPermission.ProductCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Product)},
            {DomainPermission.ProductEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Product)},
            {DomainPermission.ProductDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Product)},

            //{DomainPermission.ProductHierarchyGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ProductHierarchy)},
            //{DomainPermission.ProductHierarchyCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ProductHierarchy)},
            {DomainPermission.ProductHierarchyEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ProductHierarchy)},
            //{DomainPermission.ProductHierarchyDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ProductHierarchy)},
                
            {DomainPermission.ProductUniverseGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_ProductUniverse)},
            {DomainPermission.ProductUniverseCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_ProductUniverse)},
            {DomainPermission.ProductUniverseEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ProductUniverse)},
            {DomainPermission.ProductUniverseDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_ProductUniverse)},

            {DomainPermission.RoleGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Role)},
            {DomainPermission.RoleCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Role)},
            {DomainPermission.RoleEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Role)},
            {DomainPermission.RoleDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Role)},
               
            {DomainPermission.UserGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_User)},
            {DomainPermission.UserCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_User)},
            {DomainPermission.UserEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_User)},
            {DomainPermission.UserDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_User)},

            {DomainPermission.WorkflowGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Workflow)},
            {DomainPermission.WorkflowCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Workflow)},
            {DomainPermission.WorkflowEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Workflow)},
            {DomainPermission.WorkflowDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Workflow)},

            //{DomainPermission.WorkpackageGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Workpackage)},
            {DomainPermission.WorkpackageCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Workpackage)},
            {DomainPermission.WorkpackageEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Workpackage)},
            {DomainPermission.WorkpackageDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Workpackage)},
            {DomainPermission.WorkpackageExecute, String.Format("{0} {1}", Message.Enum_DomainPermission_Execute, Message.Enum_DomainPermission_Workpackage)},

            {DomainPermission.BlockingGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Blocking)},
            {DomainPermission.BlockingCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Blocking)},
            {DomainPermission.BlockingEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Blocking)},
            {DomainPermission.BlockingDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Blocking)},

            {DomainPermission.InventoryProfileGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_InventoryProfile)},
            {DomainPermission.InventoryProfileCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_InventoryProfile)},
            {DomainPermission.InventoryProfileEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_InventoryProfile)},
            {DomainPermission.InventoryProfileDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_InventoryProfile)},

            {DomainPermission.RenumberingStrategyGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_RenumberingStrategy)},
            {DomainPermission.RenumberingStrategyCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_RenumberingStrategy)},
            {DomainPermission.RenumberingStrategyEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_RenumberingStrategy)},
            {DomainPermission.RenumberingStrategyDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_RenumberingStrategy)},

            {DomainPermission.SequenceGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Sequence)},
            {DomainPermission.SequenceCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Sequence)},
            {DomainPermission.SequenceEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Sequence)},
            {DomainPermission.SequenceDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Sequence)},
            {DomainPermission.SyncWithMerchandisingHierarchy, Message.Enum_DomainPermission_SyncWithMerchandisingHierarchy},
            

            {DomainPermission.ValidationTemplateGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_ValidationTemplate)},
            {DomainPermission.ValidationTemplateCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_ValidationTemplate)},
            {DomainPermission.ValidationTemplateEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ValidationTemplate)},
            {DomainPermission.ValidationTemplateDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_ValidationTemplate)},

            {DomainPermission.ContentLookupGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_ContentLookup)},
            {DomainPermission.ContentLookupCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_ContentLookup)},
            {DomainPermission.ContentLookupEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_ContentLookup)},
            {DomainPermission.ContentLookupDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_ContentLookup)},

            {DomainPermission.MetricProfileGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_MetricProfile)},
            {DomainPermission.MetricProfileCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_MetricProfile)},
            {DomainPermission.MetricProfileEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_MetricProfile)},
            {DomainPermission.MetricProfileDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_MetricProfile)},

            //{DomainPermission.EntityGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Entity)},
            {DomainPermission.EntityCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Entity)},
            {DomainPermission.EntityEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Entity)},
            {DomainPermission.EntityDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Entity)},

            {DomainPermission.LabelGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Label)},
            {DomainPermission.LabelCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Label)},
            {DomainPermission.LabelEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Label)},
            {DomainPermission.LabelDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Label)},

            {DomainPermission.HighlightGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Highlight)},
            {DomainPermission.HighlightCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Highlight)},
            {DomainPermission.HighlightEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Highlight)},
            {DomainPermission.HighlightDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Highlight)},

            { DomainPermission.PlanogramImportTemplateGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_PlanogramImportTemplate) },
            { DomainPermission.PlanogramImportTemplateCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_PlanogramImportTemplate) },
            { DomainPermission.PlanogramImportTemplateEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_PlanogramImportTemplate) },
            { DomainPermission.PlanogramImportTemplateDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_PlanogramImportTemplate) },

            { DomainPermission.PlanogramGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_Planogram) },
            { DomainPermission.PlanogramCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_Planogram) },
            { DomainPermission.PlanogramEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_Planogram) },
            { DomainPermission.PlanogramDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_Planogram) },

            { DomainPermission.UnlockPlanogram, Message.Enum_DomainPermission_UnlockPlanogram },

            { DomainPermission.PrintTemplateGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_PrintTemplate) },
            { DomainPermission.PrintTemplateCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_PrintTemplate) },
            { DomainPermission.PrintTemplateEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_PrintTemplate) },
            { DomainPermission.PrintTemplateDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_PrintTemplate) },

#region Planogram Comparison Template
            { DomainPermission.PlanogramComparisonTemplateGet, String.Format("{0} {1}", Message.Enum_DomainPermission_Get, Message.Enum_DomainPermission_PlanogramComparisonTemplate) },
            { DomainPermission.PlanogramComparisonTemplateCreate, String.Format("{0} {1}", Message.Enum_DomainPermission_Create, Message.Enum_DomainPermission_PlanogramComparisonTemplate) },
            { DomainPermission.PlanogramComparisonTemplateEdit, String.Format("{0} {1}", Message.Enum_DomainPermission_Edit, Message.Enum_DomainPermission_PlanogramComparisonTemplate) },
            { DomainPermission.PlanogramComparisonTemplateDelete, String.Format("{0} {1}", Message.Enum_DomainPermission_Delete, Message.Enum_DomainPermission_PlanogramComparisonTemplate) },
#endregion
            
            // Imports
            {DomainPermission.ImportAssortmentData, Message.Enum_DomainPermission_ImportAssortmentData},
            {DomainPermission.ImportLocationData, Message.Enum_DomainPermission_ImportLocationData},
            {DomainPermission.ImportLocationHierarchyData, Message.Enum_DomainPermission_ImportLocationHierarchyData},
            {DomainPermission.ImportLocationProductIllegalData, Message.Enum_DomainPermission_ImportLocationProductIllegalData},
            {DomainPermission.ImportLocationProductAttributeData, Message.Enum_DomainPermission_ImportLocationProductAttributeData},
            {DomainPermission.ImportLocationSpaceData, Message.Enum_DomainPermission_ImportLocationSpaceData},
            {DomainPermission.ImportMerchandisingHierarchyData, Message.Enum_DomainPermission_ImportMerchandisingHierarchyData},
            {DomainPermission.ImportProductData, Message.Enum_DomainPermission_ImportProductData},
            //{DomainPermission.ImportProductUniverseData, Message.Enum_DomainPermission_ImportProductUniverseData},
            {DomainPermission.ImportLocationClusterData, Message.Enum_DomainPermission_ImportLocationClusterData},

            // Exports
            {DomainPermission.ExportAssortmentData, Message.Enum_DomainPermission_ExportAssortmentData},
            {DomainPermission.ExportLocationData, Message.Enum_DomainPermission_ExportLocationData},
            {DomainPermission.ExportLocationHierarchyData, Message.Enum_DomainPermission_ExportLocationHierarchyData},
            {DomainPermission.ExportLocationProductAttributeData, Message.Enum_DomainPermission_ExportLocationProductAttributeData},
            {DomainPermission.ExportLocationProductIllegalData, Message.Enum_DomainPermission_ExportLocationProductIllegalData},
            {DomainPermission.ExportLocationSpaceData, Message.Enum_DomainPermission_ExportLocationSpaceData},
            {DomainPermission.ExportMerchandisingHierarchyData, Message.Enum_DomainPermission_ExportMerchandisingHierarchyData},
            {DomainPermission.ExportProductData, Message.Enum_DomainPermission_ExportProductData},
            //{DomainPermission.ExportProductUniverseData, Message.Enum_DomainPermission_ExportProductUniverseData},
            {DomainPermission.ExportLocationClusterData, Message.Enum_DomainPermission_ExportLocationClusterData},
                
            // Delete
            {DomainPermission.DeleteData, Message.Enum_DomainPermission_DeleteData},
        };
    }
}
