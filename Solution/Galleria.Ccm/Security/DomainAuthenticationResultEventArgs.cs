﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using Csla.Serialization;

namespace Galleria.Ccm.Security
{
    /// <summary>
    /// Defines the event arguments returned from the
    /// authentication and authorization process
    /// </summary>
    [Serializable]
    public class DomainAuthenticationResultEventArgs : EventArgs
    {
        #region Fields
        private Exception _exception;
        #endregion

        #region Properties
        /// <summary>
        /// An exception raised during the authentication process
        /// </summary>
        public Exception Exception
        {
            get { return _exception; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor used to create a new object of this type
        /// </summary>
        /// <param name="exception">An exception thrown during authentication</param>
        internal DomainAuthenticationResultEventArgs(Exception exception)
        {
            _exception = exception;
        }
        #endregion
    }
}
