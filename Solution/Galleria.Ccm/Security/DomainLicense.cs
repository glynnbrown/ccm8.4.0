﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Desaware.MachineLicense40;
using System.Collections.Specialized;
using System.IO;

namespace Galleria.Ccm.Security
{
    public enum InstallType
    {
        Demo,
        Full,
        None
    }

    public enum DialogType
    {
        Error,
        Warning,
        Information
    }

    public class DomainLicense
    {
        #region Fields

        private String _inputKey;
        private String _userName;
        private String _companyName;
        private String _companyAddress;
        private String _companyPhone;
        private String _companyEmail;
        private String _securityDialogHeader;
        private String _securityDialogMessage;
        private InstallType _securityInstallType = InstallType.None;
        private DialogType _securityDialogType = DialogType.Error;

        private ClientLicense _clientLicense = new ClientLicense("License"); //The client license object
        private ValidationStatus _vlresults = default(ValidationStatus); //The client license validation result object
        private InstallErrorResults _inlresults = default(InstallErrorResults); //The client license intallation result object

        #endregion

        #region Properties

        /// <summary>
        /// The input key entered
        /// </summary>
        public string inputKey
        {
            get { return _inputKey; }
            set { _inputKey = value; }
        }

        /// <summary>
        /// The name of the person registering the license
        /// </summary>
        public string userName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        /// <summary>
        /// The name of the company registering the license
        /// </summary>
        public string companyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        /// <summary>
        /// The address of the company registering the license
        /// </summary>
        public string companyAddress
        {
            get { return _companyAddress; }
            set { _companyAddress = value; }
        }

        /// <summary>
        /// The phone number of the company registering the license
        /// </summary>
        public string companyPhone
        {
            get { return _companyPhone; }
            set { _companyPhone = value; }
        }

        /// <summary>
        /// The email address of the company registering the license
        /// </summary>
        public string companyEmail
        {
            get { return _companyEmail; }
            set { _companyEmail = value; }
        }

        /// <summary>
        /// The Security Header Dialog
        /// </summary>
        public string securityDialogHeader
        {
            get { return _securityDialogHeader; }
            set { _securityDialogHeader = value; }
        }

        /// <summary>
        /// The Security Header Dialog
        /// </summary>
        public string securityDialogMessage
        {
            get { return _securityDialogMessage; }
            set { _securityDialogMessage = value; }
        }

        /// <summary>
        /// The Security Install Type
        /// </summary>
        public InstallType securityInstallType
        {
            get { return _securityInstallType; }
            set { _securityInstallType = value; }
        }

        /// <summary>
        /// The Security Dialog Type
        /// </summary>
        public DialogType securityDialogType
        {
            get { return _securityDialogType; }
            set { _securityDialogType = value; }
        }

        /// <summary>
        /// The Validation Status
        /// </summary>
        public ValidationStatus ValidationStatus
        {
            get { return _vlresults; }
            set
            {
                _vlresults = value;
            }
        }

        /// <summary>
        /// The Install Error Results
        /// </summary>
        public InstallErrorResults InstallErrorResults
        {
            get { return _inlresults; }
            set
            {
                _inlresults = value;
            }
        }

        #endregion

        #region License Methods

        /// <summary>
        /// Get the Security Install Type
        /// </summary>
        public InstallType GetSecurityInstallType()
        {
            return _securityInstallType;
        }

        /// <summary>
        /// Verify the license is fully digitaly signed
        /// </summary>
        /// <returns>True if the license is full, else false</returns>
        public bool IsFullLicense()
        {
            Boolean returnVal = true;

            switch (_vlresults)
            {
                case ValidationStatus.DemoExpired:
                case ValidationStatus.DemoNoInternetDate:
                case ValidationStatus.IncorrectSystem:
                case ValidationStatus.InstallKeyDisabled:
                case ValidationStatus.MissingOrIncorrectApplication:
                case ValidationStatus.SignatureVerificationError:
                case ValidationStatus.SubscriptionExpired:
                    returnVal = false;
                    break;

                case ValidationStatus.Success:
                case ValidationStatus.TempSuccess:

                    if (_clientLicense.DemoVersion)
                    {
                        returnVal = true;
                    }
                    else
                    {
                        returnVal = !_clientLicense.Licensed;
                    }
                    break;
            }

            return returnVal;
        }

        /// <summary>
        /// Verify the license is fully digitaly signed
        /// </summary>
        /// <returns>True if the license is full, else false</returns>
        public bool IsValidLicense()
        {
            Boolean returnVal = true;

            if (KeyDisabled() == true)
            {
                _vlresults = ValidationStatus.InstallKeyDisabled;
            }

            switch (_vlresults)
            {
                case ValidationStatus.DemoExpired:
                case ValidationStatus.DemoNoInternetDate:
                case ValidationStatus.IncorrectSystem:
                case ValidationStatus.InstallKeyDisabled:
                case ValidationStatus.InvalidCertificate:
                case ValidationStatus.MissingOrIncorrectApplication:
                case ValidationStatus.SignatureVerificationError:
                case ValidationStatus.SubscriptionExpired:
                    returnVal = false;
                    break;

                case ValidationStatus.Success:
                case ValidationStatus.TempSuccess:

                    int daysRemaining = LicenseDaysRemaining();

                    if (daysRemaining >= 0 && daysRemaining < 11)
                    {
                        returnVal = false;
                    }
                    break;
            }

            return returnVal;
        }

        /// <summary>
        /// Get the remaining number of demo days
        /// </summary>
        /// <returns>Remaining demo period in number of days</returns>
        public int DemoDaysRemaining()
        {
            return (_clientLicense.DemoDaysRemaining + 1);
        }

        /// <summary>
        /// Installs the demo license
        /// </summary>
        /// <returns>Return the result of the demo installation</returns>
        public InstallErrorResults DemoInstall()
        {
            InstallErrorResults installResults = InstallErrorResults.NoLicenseServers;

            if (IsServerConnection())
            {
                //Save the cutom data against the license
                LicenseAddCustomData();

                //Performs a synchronous connection to the server to register the license. Will store a temporary license if the server cannot be reached.
                installResults = _clientLicense.InstallDemo(InstallationModes.SyncActivationRequired);

                //Verify the license
                LicenseVerify();
            }

            return installResults;
        }

        /// <summary>
        /// Returns the expiration date of this particular demo install from the license file. Only valid after calling the VerifyLicense function. If the license file is not a demo, then this date will be the same as the InstallationDate.
        /// </summary>
        /// <returns>The date the demo expires on</returns>
        public DateTime DemoExpirationDate()
        {
            return _clientLicense.ExpirationDate;
        }

        /// <summary>
        /// Set the expiration days for a demo install.
        /// </summary>
        /// <param name="demodays">Number of demo days</param>
        public void DemoExpirationDays(int demodays)
        {
            _clientLicense.DemoExpiration = demodays;
        }

        /// <summary>
        /// Set if there is an internet connection needed for the demo version
        /// </summary>
        /// <param name="connect">Internet Connection Required</param>
        public void DemoRequiresInternet(bool connect)
        {
            _clientLicense.DemoRequireInternet = connect;
        }

        /// <summary>
        // Validate that the demo license is still within its date range. 
        /// </summary>
        public bool DemoValid()
        {
            bool demoValid = true;
            int serverTimeOut = _clientLicense.ServerTimeout;

            DateTime licenseDate = _clientLicense.ExpirationDate;
            DateTime serverDate = _clientLicense.GetActivationServerTime(serverTimeOut);

            if (serverDate != DateTime.MinValue && licenseDate != DateTime.MinValue)
            {
                demoValid = (serverDate <= licenseDate);
            }

            return demoValid;
        }

        /// <summary>
        // Indicates that a valid demo certificate was found 
        // (temporary or signed). 
        // Valid only after calling the VerifyLicense method. 
        /// </summary>
        /// <returns>Return the result of the license type.</returns>
        public bool DemoVersion()
        {
            return _clientLicense.DemoVersion;
        }

        /// <summary>
        // Indicates that a temporary license certificate was found. 
        // Valid only after calling the VerifyLicense method. 
        // Applies to both demo and full licenses.
        /// </summary>
        /// <returns>Return the result of the license type.</returns>
        public bool DeferredLicense()
        {
            return _clientLicense.DeferredLicense;
        }

        /// <summary>
        // Attemps a connection to the license server 
        /// </summary>
        /// <returns>True if there is a connection to the license server else false.</returns>
        public bool IsServerConnection()
        {
            int serverTimeOut = 6000; //Set to 6 Seconds
            int retryCount = 1;

            bool dateMatch = false;

            //Try to establish connection to the license server 2 times and then quit.
            while (dateMatch == false && retryCount < 3)
            {
                dateMatch = (_clientLicense.GetActivationServerTime(serverTimeOut) != DateTime.MinValue);
                retryCount++;
            }

            return dateMatch;
        }

        /// <summary>
        // Indicates that a license certificate was disabled 
        // Valid only after calling the VerifyLicense method.
        /// </summary>
        /// <returns>Return the result of the license type.</returns>
        public bool KeyDisabled()
        {
            return _clientLicense.KeyDisabled;
        }

        /// <summary>
        //Indicates that a valid license certificate was found 
        // (temporary or signed). 
        // Valid only after calling the VerifyLicense method.
        /// </summary>
        /// <returns>Return the result of the license type.</returns>
        public bool Licensed()
        {
            return _clientLicense.Licensed;
        }

        /// <summary>
        /// Settings for the application
        /// </summary>
        /// <param name="name">The application name</param>
        /// <param name="password">The application password</param>
        /// <param name="key">The Public Key of the application</param>
        public void LicenseApplication(string name, string password, string key)
        {
            _clientLicense.ApplicationName = name;
            _clientLicense.ApplicationPassword = password;
            _clientLicense.PublicKey = key;
        }

        /// <summary>
        /// Set any additional data against the license.
        /// </summary>
        private void LicenseAddCustomData()
        {
            //Add the registartion details to the control
            StringDictionary CustomData = new StringDictionary();

            CustomData.Add("Name", this.userName);
            CustomData.Add("Company", this.companyName);
            CustomData.Add("Address", this.companyAddress);
            CustomData.Add("Telephone", this.companyPhone);
            CustomData.Add("Email", this.companyEmail);

            _clientLicense.CustomData = CustomData;
        }

        /// <summary>
        /// Initalize the license component
        /// </summary>
        /// <returns></returns>
        public void LicenseInitalize()
        {
            // 0 = Default, 1 = Loose, 2 = Tight
            //Set the Matching Algorithm to tight
            _clientLicense.DefaultMatchAlgorithm = 2;

            //Set the license file path
            LicenseFilePath(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
                    Constants.AppDataFolderName,
                    "Ccm.dlsc"));

            //Set the timeout server value
            LicenseTimeout();

            //Verify the license is valid
            LicenseVerify();

            //Check for license updates
            LicenseUpdate();
        }

        /// <summary>
        /// Fetches the machine-specific unique installation serial Guid
        /// Note: This is NOT the site licence key
        /// </summary>
        /// <returns>The Install Serial Guid for the current machine</returns>
        public String LicenseInstallationSerial()
        {
            return _clientLicense.InstallationSerial;
        }

        /// <summary>
        /// read the additional data against the license.
        /// </summary>
        public void LicenseLoadCustomData()
        {
            //Add the registartion details to the control
            StringDictionary CustomData = new StringDictionary();

            CustomData = _clientLicense.CustomData;

            if (CustomData != null)
            {
                this.userName = CustomData["Name"];
                this.companyName = CustomData["Company"];
                this.companyAddress = CustomData["Address"];
                this.companyPhone = CustomData["Telephone"];
                this.companyEmail = CustomData["Email"];
            }

        }

        /// <summary>
        /// Get the remaining number of license days
        /// </summary>
        /// <returns>Remaining license period in number of days</returns>
        public int LicenseDaysRemaining()
        {
            return _clientLicense.SubscriptionDaysRemaining;
        }

        /// <summary>
        /// Set the file path of the license file
        /// </summary>
        /// <param name="path">File path of the license file (galleria.dlsc)</param>
        public void LicenseFilePath(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                // ensure that the license folder exists
                Directory.CreateDirectory(Path.GetDirectoryName(path));

                _clientLicense.LicenseFilePath = path;
            }
        }

        /// <summary>
        /// Get the file path of the license file
        /// </summary>
        /// <returns>Where the license file is located on the client machine</returns>
        public string LicenseGetFilePath()
        {
            return _clientLicense.LicenseFilePath;
        }

        /// <summary>
        /// Installs the license using the serial key provided
        /// </summary>
        /// <returns>Return the result of the installation</returns>
        public InstallErrorResults LicenseInstall()
        {
            if (IsServerConnection())
            {
                InstallErrorResults installResults = InstallErrorResults.InvalidCode;

                string serial = this._inputKey;

                //Save the cutom data against the license
                LicenseAddCustomData();

                if (!string.IsNullOrEmpty(serial))
                {
                    //Performs a synchronous connection to the server to register the license. Will store a temporary license if the server cannot be reached.
                    installResults = _clientLicense.InstallLicense(serial, InstallationModes.SyncActivationRequired);

                    LicenseVerify();
                }

                return installResults;
            }
            else
            {
                return InstallErrorResults.NoLicenseServers;
            }
        }

        /// <summary>
        /// Set a list of URL‟s to license servers. The component will try all of them until one successfully licenses the component. You can override the default license server set by the license resource file using this property.
        /// </summary>
        /// <param name="server">URL of the activator web service</param>
        public void LicenseServer(string serverurl)
        {
            _clientLicense.LicenseServers = new string[] { serverurl };
        }

        /// <summary>
        /// Set the timeout value of the server connection
        /// </summary>
        /// <returns></returns>
        public void LicenseTimeout()
        {
            _clientLicense.ServerTimeout = 10000;
        }

        /// <summary>
        /// Updates the license on the user machine with any changes made on the server
        /// </summary>
        /// <returns></returns>
        public void LicenseUpdate()
        {
            DateTime ClientDate = _clientLicense.CertificateUpdateDate;
            DateTime ServerDate = _clientLicense.LastUpdateDate();

            if (ServerDate > DateTime.MinValue)
            {
                if (ServerDate >= ClientDate)
                {
                    _clientLicense.UpdateLicense(false);
                    LicenseVerify();
                }
            }
        }

        /// <summary>
        /// Verifies a license. If a temporary certificate is present, this will try to register it with the licensing server.
        /// </summary>
        /// <returns>Return the result of the validation reults</returns>
        public void LicenseVerify()
        {
            _vlresults = _clientLicense.VerifyLicense(false);
        }

        /// <summary>
        /// Does a first pass verification of an installation code based on the application name and password. Used by the CodeEntryControl to determine if a password is valid. Does not perform verification against the license server.
        /// </summary>
        /// <returns>Return the result of a valid installation code</returns>
        public bool LicenseVerifyInstallationCode()
        {
            string InstallCode = this._inputKey;
            bool IsValid = false;

            if (!string.IsNullOrEmpty(InstallCode))
            {
                IsValid = _clientLicense.VerifyInstallationCode(InstallCode);
            }

            if (IsValid == false)
            {
                InstallErrorResults = InstallErrorResults.InvalidCode;
            }

            return IsValid;
        }

        #endregion
    }
}