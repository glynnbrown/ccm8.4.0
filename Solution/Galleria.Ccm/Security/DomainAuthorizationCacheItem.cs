﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Configuration;
using Csla.Core;

namespace Galleria.Ccm.Security
{
    /// <summary>
    /// Represents an item held within the domain authorization cache
    /// </summary>
    internal class DomainAuthorizationCacheItem
    {
        #region Constants
        private const Int32 _defaultCacheDuration = 0; // the default cache duration in seconds
        private const String _authenticationCacheDurationKey = "AuthenticationCacheDuration"; // the configuration key that holds the cache duration
        #endregion

        #region Fields
        private DateTime _created = DateTime.UtcNow; // holds the date and time the cache item was created
        private Boolean _isAuthenticated; // indicates if the user was authenticated
        private MobileList<String> _roles; // the roles that the user was added too
        private Int32 _userId; // the users id
        #endregion

        #region Properties
        /// <summary>
        /// Returns the cache duration
        /// </summary>
        private Int32 CacheDuration
        {
            get
            {
                Int32 cacheDuration;
                if (Int32.TryParse(ConfigurationManager.AppSettings[_authenticationCacheDurationKey], out cacheDuration))
                {
                    return cacheDuration;
                }
                return _defaultCacheDuration;
            }
        }

        /// <summary>
        /// Indicates if this cache item has expired
        /// </summary>
        public Boolean HasExpired
        {
            get
            {
                if ((this.CacheDuration == 0) || (DateTime.UtcNow > _created.AddSeconds(this.CacheDuration)))
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Indicates if the user was authenticated
        /// </summary>
        public Boolean IsAuthenticated
        {
            get { return _isAuthenticated; }
        }

        /// <summary>
        /// The roles that the user belongs too
        /// </summary>
        public MobileList<String> Roles
        {
            get { return _roles; }
        }

        /// <summary>
        /// The users id
        /// </summary>
        public Int32 UserId
        {
            get { return _userId; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="item">The item to cache</param>
        public DomainAuthorizationCacheItem(Int32 userId, Boolean isAuthenticated, MobileList<String> roles)
        {
            _userId = userId;
            _isAuthenticated = isAuthenticated;
            _roles = roles;
        }
        #endregion
    }
}
