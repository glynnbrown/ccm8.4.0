﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using System.Collections.Concurrent;
using Csla.Core;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Security
{
    /// <summary>
    /// A class used to cache database items used during authentication
    /// </summary>
    internal static class DomainAuthorizationCache
    {
        #region Fields
        private static Guid _dalFactoryId; // holds the dal factory id
        private static ConcurrentDictionary<String, object> _userLocks = new ConcurrentDictionary<string, object>(); // dictionary used to hold user specific locks
        private static ConcurrentDictionary<String, DomainAuthorizationCacheItem> _userCache = new ConcurrentDictionary<string, DomainAuthorizationCacheItem>(); // holds the cached user details
        #endregion

        #region Methods
        /// <summary>
        /// Returns an object that can be used to lock a specific user
        /// </summary>
        /// <param name="username">The username to obtain a lock for</param>
        /// <returns>An object that can be used for locking</returns>
        public static object GetUserLock(String username)
        {
            return _userLocks.GetOrAdd(username, new Object());
        }

        /// <summary>
        /// Returns the cached permission details for the specified user
        /// </summary>
        /// <param name="username">The user to return the details for</param>
        /// <returns>The users cached details, else null</returns>
        public static DomainAuthorizationCacheItem GetUserCache(String username)
        {
            // when attempting to retrieve an item from the cache
            // we must check to see if the database has changed
            // in the background, which would invalidate our cache
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            if (dalFactory.Id != _dalFactoryId)
            {
                // the default dal factory has changed in the background
                // which is a sign that the database may have changed
                // therefore we clear the cache
                _userCache.Clear();

                // and store the dal factory id
                _dalFactoryId = dalFactory.Id;

                // return null as the cache has been cleared
                return null;
            }

            // attempt to read and return a value from the cache
            DomainAuthorizationCacheItem item;
            _userCache.TryGetValue(username, out item);
            if ((item == null) || (item.HasExpired))
            {
                return null;
            }
            return item;
        }

        /// <summary>
        /// Adds a new item to the cache
        /// </summary>
        /// <param name="username">The username</param>
        /// <param name="isAuthenticated">Indicates if the user is </param>
        /// <param name="roles"></param>
        public static void AddUserCache(String username, Int32 userId, Boolean isAuthenticated, MobileList<String> roles)
        {
            DomainAuthorizationCacheItem item = new DomainAuthorizationCacheItem(userId, isAuthenticated, roles);
            _userCache.AddOrUpdate(username, item, (key, oldValue) => item);
        }
        #endregion
    }
}
