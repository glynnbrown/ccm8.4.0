﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27426 : L.Ineson
//  Created
#endregion
#region Version History : CCM830
// V8-31559 : A.Kuszyk
//  Added IsMatch extension method.
// V8-31977 : A.Kuszyk
//  Ensured that Equality comparison is type-specific.
// V8-31977 : A.Kuszyk
//  Added support for enums to Equality comparison.
// V8-31824 : A.Kuszyk
//  Added ShortFriendlyNames.
#endregion
#endregion

using System;
using Galleria.Ccm.Resources.Language;
using System.Collections.Generic;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Engine
{
    public enum TaskParameterFilterOperandType
    {
        Equals = 0,
        Contains = 1,
        NotEquals = 2,
        NotContains = 3,
        GreaterThan = 4,
        GreaterThanOrEqualTo = 5,
        LessThan = 6,
        LessThanOrEqualTo = 7,
    }

    public static class TaskParameterFilterOperandTypeHelper
    {
        /// <summary>
        /// Performs a comparison between <paramref name="matchValue"/> and <paramref name="matchFilter"/> using this
        /// <see cref="TaskParameterFilterOperandType"/> to determine if the values are a match.
        /// </summary>
        /// <param name="operand"></param>
        /// <param name="matchValue">The value to evaluate.</param>
        /// <param name="matchFilter">the value to compare against.</param>
        /// <returns>True if the values match the <see cref="TaskParameterFilterOperandType"/> operator.</returns>
        public static Boolean IsMatch(this TaskParameterFilterOperandType operand, Object matchValue, Object matchFilter)
        {
            if (matchValue == null || matchFilter == null) return false;

            try
            {
                switch (operand)
                {
                    case TaskParameterFilterOperandType.Equals:
                        if (matchValue.GetType().Equals(typeof(Byte)))
                        {
                            return (Byte)matchValue == Convert.ToByte(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int16)))
                        {
                            return (Int16)matchValue == Convert.ToInt16(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int32)))
                        {
                            return (Int32)matchValue == Convert.ToInt32(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Single)))
                        {
                            return ((Single)matchValue).EqualTo(Convert.ToSingle(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(Double)))
                        {
                            return ((Double)matchValue).EqualTo(Convert.ToDouble(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(DateTime)))
                        {
                            return (DateTime)matchValue == Convert.ToDateTime(matchFilter);
                        }
                        else if (matchValue.GetType().IsEnum)
                        {
                            return Enum.Parse(matchValue.GetType(), matchFilter.ToString()).Equals(matchValue);
                        }
                        else
                        {
                            return matchValue.Equals(matchFilter);
                        }

                    case TaskParameterFilterOperandType.Contains:
                        return matchValue.ToString().Contains(matchFilter.ToString());

                    case TaskParameterFilterOperandType.NotEquals:
                        return !matchValue.Equals(matchFilter);

                    case TaskParameterFilterOperandType.NotContains:
                        return !matchValue.ToString().Contains(matchFilter.ToString());

                    case TaskParameterFilterOperandType.GreaterThan:
                        if (matchValue.GetType().Equals(typeof(Byte)))
                        {
                            return (Byte)matchValue > Convert.ToByte(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int16)))
                        {
                            return (Int16)matchValue > Convert.ToInt16(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int32)))
                        {
                            return (Int32)matchValue > Convert.ToInt32(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Single)))
                        {
                            return ((Single)matchValue).GreaterThan(Convert.ToSingle(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(Double)))
                        {
                            return ((Double)matchValue).GreaterThan(Convert.ToDouble(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(DateTime)))
                        {
                            return (DateTime)matchValue > Convert.ToDateTime(matchFilter);
                        }
                        else
                        {
                            return false;
                        }

                    case TaskParameterFilterOperandType.GreaterThanOrEqualTo:
                        if (matchValue.GetType().Equals(typeof(Byte)))
                        {
                            return (Byte)matchValue >= Convert.ToByte(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int16)))
                        {
                            return (Int16)matchValue >= Convert.ToInt16(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int32)))
                        {
                            return (Int32)matchValue >= Convert.ToInt32(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Single)))
                        {
                            return ((Single)matchValue).GreaterOrEqualThan(Convert.ToSingle(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(Double)))
                        {
                            return ((Double)matchValue).GreaterOrEqualThan(Convert.ToDouble(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(DateTime)))
                        {
                            return (DateTime)matchValue >= Convert.ToDateTime(matchFilter);
                        }
                        else
                        {
                            return false;
                        }

                    case TaskParameterFilterOperandType.LessThan:
                        if (matchValue.GetType().Equals(typeof(Byte)))
                        {
                            return (Byte)matchValue < Convert.ToByte(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int16)))
                        {
                            return (Int16)matchValue < Convert.ToInt16(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int32)))
                        {
                            return (Int32)matchValue < Convert.ToInt32(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Single)))
                        {
                            return ((Single)matchValue).LessThan(Convert.ToSingle(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(Double)))
                        {
                            return ((Double)matchValue).LessThan(Convert.ToDouble(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(DateTime)))
                        {
                            return (DateTime)matchValue < Convert.ToDateTime(matchFilter);
                        }
                        else
                        {
                            return false;
                        }

                    case TaskParameterFilterOperandType.LessThanOrEqualTo:
                        if (matchValue.GetType().Equals(typeof(Byte)))
                        {
                            return (Byte)matchValue <= Convert.ToByte(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int16)))
                        {
                            return (Int16)matchValue <= Convert.ToInt16(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Int32)))
                        {
                            return (Int32)matchValue <= Convert.ToInt32(matchFilter);
                        }
                        else if (matchValue.GetType().Equals(typeof(Single)))
                        {
                            return ((Single)matchValue).LessOrEqualThan(Convert.ToSingle(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(Double)))
                        {
                            return ((Double)matchValue).LessOrEqualThan(Convert.ToDouble(matchFilter));
                        }
                        else if (matchValue.GetType().Equals(typeof(DateTime)))
                        {
                            return (DateTime)matchValue <= Convert.ToDateTime(matchFilter);
                        }
                        else
                        {
                            return false;
                        }

                    default:
                        return false;
                }
            }
            catch (FormatException)
            {
                return false;
            }
            catch (InvalidCastException)
            {
                return false;
            }
            catch (OverflowException)
            {
                return false;
            }
        }

        public static readonly Dictionary<TaskParameterFilterOperandType, String> FriendlyNames =
            new Dictionary<TaskParameterFilterOperandType, String>()
            {
                {TaskParameterFilterOperandType.Equals, Message.Enum_TaskParameterFilterOperandType_Equals},
                {TaskParameterFilterOperandType.Contains, Message.Enum_TaskParameterFilterOperandType_Contains}, 
                {TaskParameterFilterOperandType.NotEquals, Message.Enum_TaskParameterFilterOperandType_NotEquals}, 
                {TaskParameterFilterOperandType.NotContains, Message.Enum_TaskParameterFilterOperandType_NotContains},
                {TaskParameterFilterOperandType.GreaterThan, Message.Enum_TaskParameterFilterOperandType_GreaterThan},
                {TaskParameterFilterOperandType.GreaterThanOrEqualTo, Message.Enum_TaskParameterFilterOperandType_GreaterThanOrEqualTo},
                {TaskParameterFilterOperandType.LessThan, Message.Enum_TaskParameterFilterOperandType_LessThan},
                {TaskParameterFilterOperandType.LessThanOrEqualTo, Message.Enum_TaskParameterFilterOperandType_LessThanOrEqualTo},
            };

        public static readonly Dictionary<TaskParameterFilterOperandType, String> ShortFriendlyNames = 
            new Dictionary<TaskParameterFilterOperandType,String>()
            {
                {TaskParameterFilterOperandType.Equals, Message.Enum_TaskParameterFilterOperandType_EqualsSymbol},
                {TaskParameterFilterOperandType.Contains, Message.Enum_TaskParameterFilterOperandType_ContainsSymbol}, 
                {TaskParameterFilterOperandType.NotEquals, Message.Enum_TaskParameterFilterOperandType_NotEqualsSymbol}, 
                {TaskParameterFilterOperandType.NotContains, Message.Enum_TaskParameterFilterOperandType_DoesNotContainSymbol},
                {TaskParameterFilterOperandType.GreaterThan, Message.Enum_TaskParameterFilterOperandType_GreaterThanSymbol},
                {TaskParameterFilterOperandType.GreaterThanOrEqualTo, Message.Enum_TaskParameterFilterOperandType_GreaterThanOrEqualToSymbol},
                {TaskParameterFilterOperandType.LessThan, Message.Enum_TaskParameterFilterOperandType_LessThanSymbol},
                {TaskParameterFilterOperandType.LessThanOrEqualTo, Message.Enum_TaskParameterFilterOperandType_LessThanOrEqualToSymbol},
            };
    }
}
