﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Galleria.Ccm.Engine.Configuration;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// Container class in which tasks may be registered
    /// </summary>
    internal static class TaskContainer
    {
        #region Fields
        private static ConcurrentDictionary<String, Type> _registerTaskTypes = new ConcurrentDictionary<String, Type>();
        #endregion

        #region Properties
        /// <summary>
        /// Returns all the registered task types
        /// </summary>
        public static IEnumerable<String> RegisteredTaskTypes
        {
            get { return _registerTaskTypes.Keys; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Static constructor
        /// </summary>
        static TaskContainer()
        {
            TaskLibraryConfigSection config = TaskLibraryConfigSection.GetConfig();
            if (config != null)
            {
                foreach (TaskLibraryConfigElement taskLibraryConfig in config.TaskLibraries)
                {
                    RegisterAssembly(taskLibraryConfig.AssemblyName);
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates an instance of a registered task
        /// based on the provided task type, returning
        /// null if the task type is not recognised
        /// </summary>
        public static TaskBase CreateTask(String taskType)
        {
            Type type;
            if (!_registerTaskTypes.TryGetValue(taskType, out type))
                type = Type.GetType(taskType);
            if (type == null) return null;
            return (TaskBase)Activator.CreateInstance(type);
        }

        /// <summary>
        /// Resets the contents of the task container
        /// </summary>
        public static void Reset()
        {
            _registerTaskTypes.Clear();
        }

        /// <summary>
        /// Registers a task within this container
        /// </summary>
        public static void RegisterTask(TaskBase task)
        {
            if ((task != null) && (task.IsVisible))
            {
                _registerTaskTypes.AddOrUpdate(task.TaskType, task.GetType(), (k, v) => task.GetType());
            }
        }

        /// <summary>
        /// Registers a task type in this container
        /// </summary>
        public static void RegisterTaskType(Type taskType)
        {
            RegisterTask((TaskBase)Activator.CreateInstance(taskType));
        }

        /// <summary>
        /// Registers all tasks within the assembly
        /// </summary>
        public static void RegisterAssembly(String filename)
        {
            // build the full path to the assembly
            String assemblyPath = filename;
            if (!File.Exists(assemblyPath))
            {
                assemblyPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase), assemblyPath);
            }

            // the code base property returns a uri, so strip the file prefix if required
            if (assemblyPath.Substring(0, 6) == @"file:\")
            {
                assemblyPath = assemblyPath.Substring(6);
            }

            // verify that the assembly exists
            if (!File.Exists(assemblyPath)) throw new InvalidOperationException(Message.TaskContainer_TaskLibraryDoesNotExist);

            // load the assembly
            Assembly assembly = Assembly.LoadFrom(assemblyPath);

            // enumerate all the types within the assembly
            foreach (Type assemblyType in assembly.GetTypes())
            {
                if (assemblyType.IsSubclassOf(typeof(TaskBase)))
                {
                    RegisterTaskType(assemblyType);
                }
            }
        }
        #endregion
    }
}
