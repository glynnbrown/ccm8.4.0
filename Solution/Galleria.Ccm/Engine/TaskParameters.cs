﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion

#region Version History : CCM830

// V8-32787 : A.Silva
//  Added TryGetParameter.

#endregion

#endregion

using System;
using System.Collections.ObjectModel;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// Basic dictionary that will hold a list of parameter values
    /// </summary>
    public class TaskParameters : KeyedCollection<Int32, TaskParameter>
    {
        #region Indexer
        /// <summary>
        /// Indexor property for this list
        /// </summary>
        public TaskParameter this[Object id]
        {
            get { return base[(Int32)id]; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets the key for the specified item
        /// </summary>
        protected override Int32 GetKeyForItem(TaskParameter item)
        {
            return item.Id;
        }

        /// <summary>
        ///     Gets the <paramref name="item"/> associated with the given <paramref name="id"/>.
        /// </summary>
        /// <param name="id">The id whose item to get</param>
        /// <param name="item">When this method returns, the item associated with the specified id, if the id is found; otherwise, <c>null</c>.</param>
        /// <returns><c>True</c> if the <paramref name="item"/> was found for the given <paramref name="id"/>; otherwise, <c>false</c>.</returns>
        public Boolean TryGetParameter(Object id, out TaskParameter item)
        {
            return Dictionary.TryGetValue((Int32)id, out item);
        }

        #endregion
    }
}