﻿#region Header Information

// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800

// V8-25560 : N.Foster
//  Created
// V8-26950 : A.Kuszyk
//  Added ContentLookup parameter types.
// V8-27269 : A.Kuszyk
//  Added ValidationTemplate parameter type.
// V8-27562 : A.Silva
//      Added RenumberingStrategy parameter type.
// V8-24779 : D.Pleasance
//  Added FileNameAndPath \ PlanogramImportTemplateNameSingle
// V8-27783 : M.Brumby
//  Added GfsProjectName
// V8-28066 : A.Kuszyk
//  Added Percentage.
#endregion

#region Version History : CCM801
// V8-27494 : A.Kuszyk
//  Added ProductAttributeMultiple.
#endregion

#region Version History : CCM 802
// V8-29003 : A.Kuszyk
//  Added PlanogramBlockingNameSingle and PlanogramSequenceNameSingle.
// V8-29078 : A.Kuszyk
//  Added CategoryCodeSingle and LocationCodeSingle.
#endregion

#region Version History : CCM 803
// V8-29643 : D.Pleasance
//  Added PlanogramAttributeMultiple.
// V8-29723 : D.Pleasance
//  Added ProductCodeAndReplaceProductCodeMultiple.
#endregion

#region Version History : CCM820
// V8-30728 : A.Kuszyk
//  Added HighlightNameSingle option.
// V8-30733 : L.Luong
//  Added LocationProductAttributeMultiple
// V8-30761 : A.Kuszyk
//  Added Int32.
#endregion

#region Version History : CCM830
// V8-31560 : D.Pleasance
//  Added DecreaseProductMultiple.
// V8-31559 : A.Kuszyk
//  Added ProductFilterSingle.
// V8-31830 : A.Probyn
//  Added PerformanceTimelinesSelection
// V8-31831 : A.Probyn
//  Added PlanogramNameTemplateNameSingle
// V8-31944 : A.Silva
//  Added PlanogramComparisonSettingsNameSingle.
// V8-32359 : A.Silva
//  Added ProductAttributeUpdateValueMultiple.

#endregion

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added a new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion

#endregion

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// Defines the available types of task parameters
    /// </summary>
    /// <remarks>
    /// The enum is split into two groups of parameter types,
    /// basic and advanced. Basic parameters represent simple
    /// data types. Advanced parameters represent more complex
    /// types and allow the UI to present a user friendly mechanism
    /// for populating the parameter
    /// E.g. ProductCode type would allow the UI to present a list
    /// of products to select from.
    /// </remarks>
    public enum TaskParameterType : byte
    {
        Unknown = 0,

        // basic parameter types
        String = 1,
        Boolean = 2,
        Enum = 3,
        Byte = 4,
        Int32 = 5,
        // TODO

        // advanced parameter types
        ProductCodeSingle = 101,
        ProductCodeMultiple = 102,
        AssortmentNameSingle = 103,
        PerformanceSelectionNameSingle = 104,
        ProductUniverseNameSingle = 105,
        BlockingNameSingle = 106,
        SequenceNameSingle = 107,
        MetricProfileNameSingle = 108,
        InventoryProfileNameSingle = 109,
        ConsumerDecisionTreeNameSingle = 110,
        MinorAssortmentNameSingle = 111,
        ClusterSchemeNameSingle = 112,
        ClusterNameSingle = 113,
        ValidationTemplateSingle = 114,
        ProductFilterMultiple = 115,
        RenumberingStrategyNameSingle = 116,
        FileNameAndPath = 117,
        PlanogramImportTemplateNameSingle = 118,
        GfsProjectName = 119,
        Percentage = 120,
        ProductAttributeMultiple = 121,
        PlanogramBlockingNameSingle = 122,
        PlanogramSequenceNameSingle = 123,
        CategoryCodeSingle = 124,
        LocationCodeSingle = 125,
        PlanogramAttributeMultiple = 126,
        ProductCodeAndReplaceProductCodeMultiple = 127,
        HighlightNameSingle = 128,
        LocationProductAttributeMultiple = 129,
        DecreaseProductMultiple = 130,
        ProductFilterSingle = 131,
        PerformanceTimelinesSelection = 132,
        PlanogramNameTemplateNameSingle = 133,
        PlanogramComparisonTemplateNameSingle = 134,
        ProductAttributeUpdateValueMultiple = 135,
        LocationSpaceElementAttributeUpdateValueMultiple = 136
    }
}