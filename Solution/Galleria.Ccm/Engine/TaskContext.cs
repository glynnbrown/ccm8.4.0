﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25787 : N.Foster
//  Created
// V8-27765 : A.Kuszyk
//  Added Logging methods.
// V8-28060 : A.Kuszyk
//  Updated logging methods.
#endregion
#region Version History: CCM803
// V8-29536 : N.Foster
//  Allow task context to be extended
#endregion
#region Version History: CCM810
// V8-29373 : N.Foster
//  Car Park Shelf Changes
// V8-29768 : A.Silva
//  Moved in UpdateSequenceData from InsertProductHelper and refactored into an instance method.
// V8-29990 : N.Foster
//  Fixed issue where creation of car park component on empty planogram throws an exception
// V8-30079 : L.Ineson
//  Added pass through of event score.
// V8-30090 : D.Pleasance
//  Amended GetCarParkMerchandisingGroup to get the highest Sub Component Placement rather than fixture item.
// V8-30123 : N.Foster
//  Added RemerchandisePlanogram method
// V8-30144 : D.Pleasance
//  Amended GetCarParkMerchandisingGroup so that merchandising groups are now passed as a parameter.
#endregion
#region Version History: CCM811
// V8-30313 : L.Ineson
//  Autofill now takes in the list of all plan merch groups.
#endregion
#region Version History: CCM820
// V8-31176 : D.Pleasance
//  Removed UpdateSequenceData
// V8-31237 : J.Pickup
//  Added an order to which the merch groups are auto filled which adds consistency and solves issue with hang groups that have several facings high.
#endregion 
#region Version History: CCM830
// V8-32520 : N.Haywood
//  Added CreateCarparkShelf
// V8-32519 : L.Ineson
//  Added parameter for textbox to CreateCarparkShelf
// V8-32591 : L.Ineson
//  Temporarily increased the textbox font size.
// V8-32591 : L.Ineson
//  Passed through plan settings to CreateCarparkShelf textbox
// V8-32825 : A.Silva
//  Added RestoreSequenceNumbers to help Merchandising tasks that need to tweak the sequence numbers as part of their process.

#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Merchandising;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine
{
    public class TaskContext : ITaskContext
    {
        #region Fields
        private Workpackage _workpackage; // holds the workpackage details
        private WorkpackagePlanogram _workpackagePlanogram; // holds the workpackage planogram details
        private Workflow _workflow; // holds the workflow details
        private WorkflowTask _workflowTask; // holds the workflow task details
        private Package _package; // holds the package details
        private Planogram _planogram; // holds the planogram of interest
        #endregion

        #region Properties
        /// <summary>
        /// Gets the entity id for this task context
        /// </summary>
        public Int32 EntityId
        {
            get { return _workpackage.EntityId; }
        }

        /// <summary>
        /// Gets the workpackage details
        /// </summary>
        public Workpackage Workpackage
        {
            get { return _workpackage; }
        }

        /// <summary>
        /// Gets the workpackage planogram
        /// </summary>
        public WorkpackagePlanogram WorkpackagePlanogram
        {
            get { return _workpackagePlanogram; }
        }

        /// <summary>
        /// Gets the workflow details
        /// </summary>
        public Workflow Workflow
        {
            get { return _workflow; }
        }

        /// <summary>
        /// Gets the workflow task details
        /// </summary>
        public WorkflowTask WorkflowTask
        {
            get { return _workflowTask; }
        }

        /// <summary>
        /// Gets the planogram package details
        /// </summary>
        public Package Package
        {
            get { return _package; }
        }

        /// <summary>
        /// Gets the planogram details
        /// </summary>
        public Planogram Planogram
        {
            get { return _planogram; }
            set
            {
                if ((value != null) && (value != _planogram))
                {
                    _package.Planograms.Remove(_planogram);
                    _planogram = value.Copy();
                    _planogram.Name = value.Name;
                    _package.Planograms.Add(_planogram);
                }
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(
            Workpackage workpackage,
            WorkpackagePlanogram workpackagePlanogram,
            Workflow workflow,
            WorkflowTask workflowTask,
            Package package,
            Planogram planogram)
        {
            _workpackage = workpackage;
            _workpackagePlanogram = workpackagePlanogram;
            _workflow = workflow;
            _workflowTask = workflowTask;
            _package = package;
            _planogram = planogram;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(
            Workpackage workpackage,
            Workflow workflow,
            WorkflowTask workflowTask)
        {
            _workpackage = workpackage;
            _workflow = workflow;
            _workflowTask = workflowTask;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskContext(ITaskContext context)
        {
            _workpackage = context.Workpackage;
            _workpackagePlanogram = context.WorkpackagePlanogram;
            _workflow = context.Workflow;
            _workflowTask = context.WorkflowTask;
            _package = context.Package;
            _planogram = context.Planogram;
        }

        #endregion

        #region Methods

        #region Logging

        /// <summary>
        /// Logs an error to the Planogram event log.
        /// </summary>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        public void LogError(EventLogEvent eventId, params Object[] args)
        {
            Log(PlanogramEventLogEntryType.Error, null, null, eventId, args);
        }

        /// <summary>
        /// Logs a warning to the Planogram event log.
        /// </summary>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        public void LogWarning(EventLogEvent eventId, params Object[] args)
        {
            Log(PlanogramEventLogEntryType.Warning, null, null, eventId, args);
        }

        /// <summary>
        /// Logs information to the Planogram event log.
        /// </summary>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        public void LogInformation(EventLogEvent eventId, params Object[] args)
        {
            Log(PlanogramEventLogEntryType.Information, null, null, eventId, args);
        }

        /// <summary>
        /// Logs information to the Planogram event log.
        /// </summary>
        /// <param name="affectedType">Indicates the type affected by the action being logged.</param>
        /// <param name="affectedId">The Id value of the type affected by the action being logged.</param>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        public void LogInformation(PlanogramEventLogAffectedType affectedType, Object affectedId, EventLogEvent eventId, params Object[] args)
        {
            Log(PlanogramEventLogEntryType.Information, affectedType, affectedId, eventId, args);
        }

        /// <summary>
        /// Logs detailed information to the Planogram event log.
        /// </summary>
        /// <param name="affectedType">Indicates the type affected by the action being logged.</param>
        /// <param name="affectedId">The Id value of the type affected by the action being logged.</param>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        public void LogDebug(PlanogramEventLogAffectedType affectedType, Object affectedId, EventLogEvent eventId, params Object[] args)
        {
            Log(PlanogramEventLogEntryType.Debug, affectedType, affectedId, eventId, args);
        }

        /// <summary>
        /// Creates an entry in the Planogram Event Log using the given parameters.
        /// </summary>
        private void Log(
            PlanogramEventLogEntryType entryType, 
            PlanogramEventLogAffectedType? affectedType, 
            Object affectedId, 
            EventLogEvent eventId, 
            params Object[] args)
        {
            Planogram.EventLogs.Add(PlanogramEventLog.NewPlanogramEventLog(
                entryType,
                EventLogEventHelper.GetPlanogramEventType(eventId),
                affectedType,
                affectedId,
                args.Length > 0 ? String.Format(EventLogEventHelper.FriendlyDescriptions[eventId], args) : EventLogEventHelper.FriendlyDescriptions[eventId],
                args.Length > 0 ? String.Format(EventLogEventHelper.FriendlyContents[eventId], args) : EventLogEventHelper.FriendlyContents[eventId],
                this.Workpackage.Name,
                this.WorkflowTask.Details.GetNameAndVersion(),
                (Int32)eventId, 
                EventLogEventHelper.GetScore(eventId)));
        }

        #endregion

        #region Car Park Component

        /// <summary>
        /// Returns the car park component with the specified name
        /// </summary>
        /// <remarks>
        /// If an existing component already exists within the specified name
        /// then that component is returned, otherwise a new component is created
        /// with the name and positioned above and to the left of the planogram
        /// </remarks>
        /// <param name="carParkComponentName">The car park component name</param>
        /// <param name="merchandisingGroups">The existing merchandisingGroups</param>
        /// <returns>The car park component</returns>
        public PlanogramMerchandisingGroup GetCarParkMerchandisingGroup(String carParkComponentName, PlanogramMerchandisingGroupList merchandisingGroups, Boolean addCarParkTextBox)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // we need to search the planogram for a component
                // that has the car park shelf name, but we need to merchandise
                // this component as part of a merchandising group, so
                // enumerate through the merchandising groups            
                foreach (PlanogramMerchandisingGroup merchandisingGroup in merchandisingGroups)
                {
                    // enumerate throught the sub components
                    // of the merchandising group
                    foreach (PlanogramSubComponentPlacement subComponentPlacement in merchandisingGroup.SubComponentPlacements)
                    {
                        if (subComponentPlacement.SubComponent.Name == carParkComponentName)
                        {
                            return merchandisingGroup;
                        }
                    }
                }

                // if we have not got this far, then the car park component
                // does not yet exist, so we need to create it
                return CreateCarparkShelf(carParkComponentName, addCarParkTextBox);
            }
        }

        /// <summary>
        /// Adds a new carpark shelf to the planogram.
        /// </summary>
        /// <param name="carParkComponentName">The car park component name</param>
        /// <param name="addCarParkTextBox">Flag to indicate if a linked textbox should also be added</param>
        /// <returns>The merchandising group for the new carpark shelf.</returns>
        public PlanogramMerchandisingGroup CreateCarparkShelf(String carParkComponentName, Boolean addCarParkTextBox)
        {
            // start by getting the planogram setting details from the entity
            IPlanogramSettings settings = (IPlanogramSettings)Entity.FetchById(this.EntityId).SystemSettings;

            // if the planogram does not have a fixture item
            // then we will need to create a fixture item
            // using the default settings
            PlanogramFixture fixture = null;
            PlanogramFixtureItem fixtureItem = this.Planogram.FixtureItems.OrderBy(item => item.BaySequenceNumber).FirstOrDefault();
            if (fixtureItem == null)
            {
                // determine if we have a fixture within the planogram
                fixture = this.Planogram.Fixtures.FirstOrDefault();

                // create a fixture if required
                if (fixture == null) fixture = this.Planogram.Fixtures.Add(settings);

                // create the fixture item
                fixtureItem = this.Planogram.FixtureItems.Add(fixture);
            }
            else
            {
                // get the fixture from the fixture item
                fixture = fixtureItem.GetPlanogramFixture();
            }

            // create the car park shelf
            PlanogramFixtureComponent fixtureComponent = fixture.Components.Add(PlanogramComponentType.Shelf, settings);
            PlanogramComponent component = fixtureComponent.GetPlanogramComponent();
            PlanogramSubComponent subComponent = component.SubComponents[0];
            PlanogramSubComponentPlacement carParkPlacement = PlanogramSubComponentPlacement.NewPlanogramSubComponentPlacement(
                subComponent,
                fixtureItem,
                fixtureComponent);

            // we now have our car park sub component placement
            // set the car park component name
            component.Name = carParkComponentName;
            subComponent.Name = carParkComponentName;

            // now position the car park component on the planogram
            // get the highest sub component placement in the planogram
            PlanogramSubComponentPlacement highestSubComponentPlacement = null;
            foreach (PlanogramSubComponentPlacement existingPlanogramSubComponentPlacement in this.Planogram.GetPlanogramSubComponentPlacements())
            {
                // determine if this item is higher
                // than our highest item
                if ((highestSubComponentPlacement == null) ||
                    ((existingPlanogramSubComponentPlacement.GetPlanogramRelativeCoordinates().Y + existingPlanogramSubComponentPlacement.Component.Height) >
                    (highestSubComponentPlacement.GetPlanogramRelativeCoordinates().Y + highestSubComponentPlacement.Component.Height)))
                {
                    highestSubComponentPlacement = existingPlanogramSubComponentPlacement;
                }
            }

            // set the position of the car park shelf based
            // on the highest sub component placement
            carParkPlacement.FixtureComponent.X = 0;
            carParkPlacement.FixtureComponent.Z = 0;
            carParkPlacement.FixtureComponent.Y =
                highestSubComponentPlacement.GetPlanogramRelativeCoordinates().Y +
                highestSubComponentPlacement.Component.Height -
                carParkPlacement.FixtureItem.Y +
                (carParkPlacement.Fixture.Height * 0.25F);


            //add a linked textbox showing the car park name if required:
            if (addCarParkTextBox)
            {
                PlanogramAnnotation carParkTextBox = PlanogramAnnotation.NewPlanogramAnnotation(fixtureComponent, fixtureItem, settings);
                carParkTextBox.Text = carParkComponentName;

                //size the textbox so that it is reasonable relative to the shelf.
                carParkTextBox.Height = carParkPlacement.SubComponent.Height * 10;
                carParkTextBox.Width = carParkPlacement.SubComponent.Width/2;
                carParkTextBox.Depth = 1;

                //position it to the left front of the shelf - coordinates are relative to the component.
                carParkTextBox.X = -carParkTextBox.Width;
                carParkTextBox.Y = 0;
                carParkTextBox.Z = carParkPlacement.SubComponent.Depth;

                this.Planogram.Annotations.Add(carParkTextBox);
            }

            // create a new merchandising group and set for the context
            return PlanogramMerchandisingGroup.NewPlanogramMerchandisingGroup(carParkPlacement);
        }

        #endregion

        #region ReMerchandise
        /// <summary>
        /// Remerchandises the whole planogram
        /// ensuring that autofill is also applied
        /// </summary>
        public void RemerchandisePlanogram()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                if (this.Planogram != null)
                {
                    using (PlanogramMerchandisingGroupList merchandisingGroups = this.Planogram.GetMerchandisingGroups())
                    {
                        merchandisingGroups.RemerchandiseAllGroups();
                    }
                }
            }
        }

        #endregion

        #region Dispose
        /// <summary>
        /// Called when disposing of this instance
        /// </summary>
        public void Dispose()
        {
            this.OnDipose();

            _workpackage = null;
            _workpackagePlanogram = null;
            _workflow = null;
            _workflowTask = null;
            _package = null;
            _planogram = null;
        }

        /// <summary>
        /// Called when this instance is being disposed
        /// </summary>
        protected virtual void OnDipose()
        {
        }
        #endregion

        #endregion

        /// <summary>
        ///     Restore the sequence numbers present in the planogram sequence groups to each position in the planogram, matching by color.
        /// </summary>
        public void RestoreSequenceNumbers()
        {
            Boolean notEnoughData = Planogram == null || Planogram.Sequence == null || Planogram.Sequence.Groups.Count == 0;
            if (notEnoughData) return;

            //  Restore the original sequence numbers in case they were altered during the process.
            foreach (KeyValuePair<Int32, PlanogramSequenceGroup> entry in Planogram.Sequence.Groups.ToDictionary(sequenceGroup => sequenceGroup.Colour))
            {
                Int32 sequenceColour = entry.Key;
                Dictionary<String, PlanogramSequenceGroupProduct> sequenceGroupProductsByGtin = entry.Value.Products.ToDictionary(product => product.Gtin);
                IEnumerable<PlanogramPosition> positionsWithSequenceColor = Planogram.Positions.Where(position => position.SequenceColour == sequenceColour);
                foreach (PlanogramPosition planogramPosition in positionsWithSequenceColor)
                {
                    PlanogramSequenceGroupProduct sequenceProduct;
                    sequenceGroupProductsByGtin.TryGetValue(planogramPosition.GetPlanogramProduct().Gtin, out sequenceProduct);
                    if (sequenceProduct == null) return;

                    planogramPosition.SetSequenceGroup(sequenceProduct);
                }
            }
        }
    }

    public interface ITaskContext : IDisposable
    {
        #region Properties
        /// <summary>
        /// Gets the entity id
        /// </summary>
        Int32 EntityId { get; }

        /// <summary>
        /// Gets the workpackage details
        /// </summary>
        Model.Workpackage Workpackage { get; }

        /// <summary>
        /// Gets the workpackage planogram details
        /// </summary>
        Model.WorkpackagePlanogram WorkpackagePlanogram { get; }

        /// <summary>
        /// Gets the workflow details
        /// </summary>
        Model.Workflow Workflow { get; }

        /// <summary>
        /// Gets the workflow task details
        /// </summary>
        Model.WorkflowTask WorkflowTask { get; }

        /// <summary>
        /// Gets the planogram package details
        /// </summary>
        Package Package { get; }

        /// <summary>
        /// Gets or sets the planogram details
        /// </summary>
        Planogram Planogram { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Logs an error to the Planogram event log.
        /// </summary>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        void LogError(EventLogEvent eventId, params Object[] args);

        /// <summary>
        /// Logs a warning to the Planogram event log.
        /// </summary>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        void LogWarning(EventLogEvent eventId, params Object[] args);

        /// <summary>
        /// Logs information to the Planogram event log.
        /// </summary>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        void LogInformation(EventLogEvent eventId, params Object[] args);

        /// <summary>
        /// Logs information to the Planogram event log.
        /// </summary>
        /// <param name="affectedType">Indicates the type affected by the action being logged.</param>
        /// <param name="affectedId">The Id value of the type affected by the action being logged.</param>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        void LogInformation(PlanogramEventLogAffectedType affectedType, Object affectedId, EventLogEvent eventId, params Object[] args);

        /// <summary>
        /// Logs detailed information to the Planogram event log.
        /// </summary>
        /// <param name="affectedType">Indicates the type affected by the action being logged.</param>
        /// <param name="affectedId">The Id value of the type affected by the action being logged.</param>
        /// <param name="eventId">The EventLogEvent Id for the error being logged.</param>
        /// <param name="args">The parameters that should be formatted into the EventLogEvent content string.</param>
        void LogDebug(PlanogramEventLogAffectedType affectedType, Object affectedId, EventLogEvent eventId, params Object[] args);

        #endregion
    }
}