﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Configuration;

namespace Galleria.Ccm.Engine.Configuration
{
    /// <summary>
    /// Defines a task library configuration element collection
    /// </summary>
    public class TaskLibraryConfigElementCollection : ConfigurationElementCollection
    {
        #region Methods
        /// <summary>
        /// Creates a new configuration collection element
        /// </summary>
        /// <returns>A new configuration collection element</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new TaskLibraryConfigElement();
        }

        /// <summary>
        /// Returns te unique key for a configuration collection element
        /// </summary>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((TaskLibraryConfigElement)element).AssemblyName;
        }
        #endregion
    }
}
