﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Configuration;

namespace Galleria.Ccm.Engine.Configuration
{
    /// <summary>
    /// Defines a task library configuration section
    /// </summary>
    public class TaskLibraryConfigSection : ConfigurationSection
    {
        #region Properties
        /// <summary>
        /// Returns the registered task libraries
        /// </summary>
        [ConfigurationProperty("", IsDefaultCollection=true)]
        [ConfigurationCollection(typeof(TaskLibraryConfigElementCollection), AddItemName="taskLibrary")]
        public TaskLibraryConfigElementCollection TaskLibraries
        {
            get { return this[""] as TaskLibraryConfigElementCollection; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns the task library config section
        /// </summary>
        /// <returns>The task library config section</returns>
        public static TaskLibraryConfigSection GetConfig()
        {
            return ConfigurationManager.GetSection("taskLibraries") as TaskLibraryConfigSection;
        }
        #endregion
    }
}
