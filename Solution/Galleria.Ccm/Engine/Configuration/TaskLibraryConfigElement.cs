﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Configuration;

namespace Galleria.Ccm.Engine.Configuration
{
    /// <summary>
    /// Defines a task library configuration element
    /// </summary>
    public class TaskLibraryConfigElement : ConfigurationElement
    {
        #region Properties
        /// <summary>
        /// Indicates if the configuration is read only
        /// </summary>
        public override bool IsReadOnly()
        {
            return false;
        }

        /// <summary>
        /// The task library assembly name
        /// </summary>
        [ConfigurationProperty("assemblyName", IsRequired = true)]
        public String AssemblyName
        {
            get { return (String)this["assemblyName"]; }
            set { this["assemblyName"] = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskLibraryConfigElement()
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskLibraryConfigElement(String assemblyName)
        {
            this.AssemblyName = assemblyName;
        }
        #endregion
    }
}
