﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27426 : L.Ineson
//  Created
#endregion
#endregion

using System;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// Defines a value of a task parameter
    /// </summary>
    [Serializable]
    public class TaskParameterValue : ITaskParameterValue
    {
        #region Fields
        private Object _value1;
        private Object _value2;
        private Object _value3;
        #endregion

        #region Properties

        /// <summary>
        /// Returns the first value
        /// </summary>
        public Object Value1
        {
            get { return _value1; }
        }

        /// <summary>
        /// Returns the second value
        /// </summary>
        public Object Value2
        {
            get { return _value2; }
        }

        /// <summary>
        /// Returns the third value
        /// </summary>
        public Object Value3
        {
            get { return _value3; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public TaskParameterValue(Object value1)
        {
            _value1 = value1;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public TaskParameterValue(Object value1, Object value2)
        {
            _value1 = value1;
            _value2 = value2;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public TaskParameterValue(Object value1, Object value2, Object value3)
        {
            _value1 = value1;
            _value2 = value2;
            _value3 = value3;
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public TaskParameterValue(ITaskParameterValue taskParameterValue)
        {
            _value1 = taskParameterValue.Value1;
            _value2 = taskParameterValue.Value2;
            _value3 = taskParameterValue.Value3;
        }

        #endregion
    }
}
