﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29731 : Neil Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Ccm.Engine.Exceptions
{
    /// <summary>
    /// Workpackage exception class
    /// </summary>
    [Serializable]
    internal class WorkpackageException : Exception
    {
        #region Fields
        private Int32 _entityId; // holds the entity id
        private Int32 _workpackageId; // holds the workpackage id
        private String _workpackageName; // holds the workpackage name
        #endregion

        #region Property
        /// <summary>
        /// The entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return _entityId; }
        }

        /// <summary>
        /// The workpackage id
        /// </summary>
        public Int32 WorkpackageId
        {
            get { return _workpackageId; }
        }

        /// <summary>
        /// The workpackage name
        /// </summary>
        public String WorkpackageName
        {
            get { return _workpackageName; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        protected WorkpackageException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workpackage.Execution.Message.EntityDc entity,
            Processes.Workpackage.Execution.Message.WorkpackageDc workpackage,
            String message,
            params Object[] args) :
            base(String.Format(message, args))
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workpackage.Execution.Message.EntityDc entity,
            Processes.Workpackage.Execution.Message.WorkpackageDc workpackage,
            Exception innerException) :
            base(innerException.Message, innerException)
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workpackage.Finalization.Message.EntityDc entity,
            Processes.Workpackage.Finalization.Message.WorkpackageDc workpackage,
            String message,
            params Object[] args) :
            base(String.Format(message, args))
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workpackage.Finalization.Message.EntityDc entity,
            Processes.Workpackage.Finalization.Message.WorkpackageDc workpackage,
            Exception innerException) :
            base(innerException.Message, innerException)
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workflow.Initialization.Message.EntityDc entity,
            Processes.Workflow.Initialization.Message.WorkpackageDc workpackage,
            String message,
            params Object[] args) :
            base(String.Format(message, args))
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workflow.Initialization.Message.EntityDc entity,
            Processes.Workflow.Initialization.Message.WorkpackageDc workpackage,
            Exception innerException) :
            base(innerException.Message, innerException)
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workflow.Execution.Message.EntityDc entity,
            Processes.Workflow.Execution.Message.WorkpackageDc workpackage,
            String message,
            params Object[] args) :
            base(String.Format(message, args))
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workflow.Execution.Message.EntityDc entity,
            Processes.Workflow.Execution.Message.WorkpackageDc workpackage,
            Exception innerException) :
            base(innerException.Message, innerException)
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workflow.Sequence.Message.EntityDc entity,
            Processes.Workflow.Sequence.Message.WorkpackageDc workpackage,
            String message,
            params Object[] args) :
            base(String.Format(message, args))
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workflow.Sequence.Message.EntityDc entity,
            Processes.Workflow.Sequence.Message.WorkpackageDc workpackage,
            Exception innerException) :
            base(innerException.Message, innerException)
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workflow.Finalization.Message.EntityDc entity,
            Processes.Workflow.Finalization.Message.WorkpackageDc workpackage,
            String message,
            params Object[] args) :
            base(String.Format(message, args))
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Processes.Workflow.Finalization.Message.EntityDc entity,
            Processes.Workflow.Finalization.Message.WorkpackageDc workpackage,
            Exception innerException) :
            base(innerException.Message, innerException)
        {
            _entityId = entity.Id;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Model.Workpackage workpackage,
            String message,
            params Object[] args) :
            base(String.Format(message, args))
        {
            _entityId = workpackage.EntityId;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageException(
            Int32 entityId,
            Model.WorkpackageInfo workpackage,
            String message,
            params Object[] args) :
            base(String.Format(message, args))
        {
            _entityId = entityId;
            _workpackageId = workpackage.Id;
            _workpackageName = workpackage.Name;
        }

        #endregion
    }
}
