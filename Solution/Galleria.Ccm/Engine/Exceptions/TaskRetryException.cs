﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Ccm.Engine.Exceptions
{
    /// <summary>
    /// An exception that can be thrown by a task
    /// or process that forces the engine to requeue
    /// the message and therefore try again
    /// </summary>
    [Serializable]
    public class TaskRetryException : Exception
    {
        public TaskRetryException() : base() { }
        protected TaskRetryException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
