﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: (GAF 1.0)
//// V8-25787 : Neil Foster
////  Created
//#endregion
//#endregion

//using System;
//using System.Runtime.Serialization;

//namespace Galleria.Ccm.Engine.Exceptions
//{
//    /// <summary>
//    /// Exception raised when a planogram no longer exists
//    /// </summary>
//    [Serializable]
//    internal class WorkflowDeletedException : Exception
//    {
//        #region Fields
//        private Int32 _workflowId; // holds the workflow id
//        private String _workflowName; // holds the workflow name
//        #endregion

//        #region Property
//        /// <summary>
//        /// The workpackage id
//        /// </summary>
//        public Int32 WorkflowId
//        {
//            get { return _workflowId; }
//        }

//        /// <summary>
//        /// The workpackage name
//        /// </summary>
//        public String WorkflowName
//        {
//            get { return _workflowName; }
//        }
//        #endregion

//        #region Constructors
//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        protected WorkflowDeletedException(SerializationInfo info, StreamingContext context)
//            : base(info, context)
//        {
//        }

//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        public WorkflowDeletedException(Int32 workflowId) :
//            base(String.Format(Resources.Language.Message.Engine_WorkflowDeletedException, workflowId))
//        {
//            _workflowId = workflowId;
//        }

//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        public WorkflowDeletedException(Processes.Workflow.Execution.Message.WorkflowDc workflow) :
//            base(String.Format(Resources.Language.Message.Engine_WorkflowDeletedException, workflow.Name))
//        {
//            _workflowId = workflow.Id;
//            _workflowName = workflow.Name;
//        }

//        /// <summary>
//        /// Creates a new instance of this type
//        /// </summary>
//        public WorkflowDeletedException(Processes.Workflow.Sequence.Message.WorkflowDc workflow) :
//            base(String.Format(Resources.Language.Message.Engine_WorkflowDeletedException, workflow.Name))
//        {
//            _workflowId = workflow.Id;
//            _workflowName = workflow.Name;
//        }
//        #endregion
//    }
//}
