﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GAF 1.0)
// V8-25787 : Neil Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Exceptions
{
    /// <summary>
    /// Exception raised when a planogram no longer exists
    /// </summary>
    [Serializable]
    internal class SourcePlanogramInvalidException : Exception
    {
        #region Fields
        private Int32 _planogramId; // holds the planogram id
        private String _planogramName; // holds the planogram name
        #endregion

        #region Property
        /// <summary>
        /// The workpackage id
        /// </summary>
        public Int32 PlanogramId
        {
            get { return _planogramId; }
        }

        /// <summary>
        /// The workpackage name
        /// </summary>
        public String PlanogramName
        {
            get { return _planogramName; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        protected SourcePlanogramInvalidException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public SourcePlanogramInvalidException(Planogram planogram)
            : base(String.Format(Resources.Language.Message.Engine_SourcePlanogramInvalidException, planogram.Name))
        {
            _planogramId = (Int32)planogram.Id;
            _planogramName = planogram.Name;
        }
        #endregion
    }
}
