﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (GAF 1.0)
// V8-25787 : Neil Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Engine.Exceptions
{
    /// <summary>
    /// Exception raised when a planogram no longer exists
    /// </summary>
    [Serializable]
    internal class WorkflowSequenceException : Exception
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        protected WorkflowSequenceException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkflowSequenceException(
            Workpackage workpackage,
            Workflow workflow,
            WorkflowTask workflowTask,
            Exception exception) :
            base(String.Format(Resources.Language.Message.Engine_WorkflowTaskException, workflowTask.Details.Name))
        {
        }
        #endregion
    }
}
