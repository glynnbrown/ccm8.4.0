﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Added IsHiddenByDefault and IsReadOnlyByDefault.
// V8-27426 : L.Ineson
//  Changed values to be a list of TaskParameterValue
#endregion
#region Version History: CCM811
// V8-30429  : J.Pickup
//  _parentId introduced (to hold dependant parameters parent id).
#endregion
#region Version History: CCM820
// V8-30771 : N.Foster
//  Add support for additional task parameter properties
// V8-30907 : A.Kuszyk
//  Ensured that enum values are arrange in ascending friendly name order.
#endregion
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Linq;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// Defines the basic information that is available for
    /// all parameters within a task
    /// </summary>
    [Serializable]
    public class TaskParameter
    {
        #region Fields
        private TaskBase _task; // the parent task
        private Int32 _id; // the parameter id
        private String _name; // the parameter name
        private String _description; // the parameter description
        private String _category; // the parameter category
        private TaskParameterType _parameterType; // the parameter type
        private Object _defaultValue; // holds the default parameter value
        private List<ITaskParameterValue> _values; // holds the actual values for the parameter
        private ReadOnlyCollection<TaskParameterEnumValue> _enumValues; //holds the list of fixed values for the enum type.
        private Boolean _isReadOnlyByDefault;
        private Boolean _isHiddenByDefault;
        private Int32? _parentId; // holds the parent parameter id
        private Func<String> _isRequired = null; // a delegate used to determine if the parameter is required or not
        private Func<Boolean> _isNullAllowed = null; // a delegate used to determine if the parameter allows a null value or not
        private Func<String> _isValid = null; // a delgate used to determine if the parameter is valid of not
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the parent task
        /// </summary>
        internal TaskBase Task
        {
            get { return _task; }
            set { _task = value; }
        }

        /// <summary>
        /// The parameter id
        /// </summary>
        public Int32 Id
        {
            get { return _id; }
        }

        /// <summary>
        /// The parameter name
        /// </summary>
        public String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// The parameter description
        /// </summary>
        public String Description
        {
            get { return _description; }
        }

        /// <summary>
        /// The parameter category
        /// </summary>
        /// <remarks>
        /// The parameter category allows for the UI
        /// to arrange parameters into logical groups
        /// </remarks>
        public String Category
        {
            get { return _category; }
        }

        /// <summary>
        /// Returns the parameter type
        /// </summary>
        public TaskParameterType ParameterType
        {
            get { return _parameterType; }
        }

        /// <summary>
        /// Defines the default paramter value
        /// </summary>
        public Object DefaultValue
        {
            get { return _defaultValue; }
        }

        /// <summary>
        /// Returns the runtime parameter value
        /// </summary>
        public List<ITaskParameterValue> Values
        {
            get { return _values; }
            set { _values = value; }
        }

        /// <summary>
        /// Helper property that returns the first
        /// value in the values list
        /// </summary>
        public ITaskParameterValue Value
        {
            get
            {
                if ((_values == null) || (_values.Count == 0)) return null;
                return _values[0];
            }
        }

        /// <summary>
        /// Returns the collection of fixed values accepted by this parameter.
        /// If null, any value of the correct type may be given.
        /// </summary>
        public ReadOnlyCollection<TaskParameterEnumValue> EnumValues
        {
            get { return _enumValues; }
        }

        /// <summary>
        /// Returns true if the parameter should be
        /// marked readonly by default.
        /// </summary>
        public Boolean IsReadOnlyByDefault
        {
            get { return _isReadOnlyByDefault; }
        }

        /// <summary>
        /// Returns true if the parameter should be
        /// hidden in the bag grid by default.
        /// </summary>
        public Boolean IsHiddenByDefault
        {
            get { return _isHiddenByDefault; }
        }

        /// <summary>
        /// Returns the parent parameter name
        /// </summary>
        public Int32? ParentId
        {
            get { return _parentId; }
        }

        /// <summary>
        /// Indicates if a parameter value is required or not. Return null if required,
        /// else returns the reason why the parameter is not required
        /// </summary>
        public String IsRequired
        {
            get { return _isRequired != null ? _isRequired() : null; }
        }

        /// <summary>
        /// Indicates if the parameter is allowed to have a null value or not
        /// </summary>
        public Boolean IsNullAllowed
        {
            get { return _isNullAllowed != null ? _isNullAllowed() : false; }
        }

        /// <summary>
        /// Indicates if the parameter is valid or not
        /// </summary>
        public String IsValid
        {
            get { return _isValid != null ? _isValid() : null; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="isRequired">
        /// A method that determines if the parameter is required. If the method returns null, then the parameter
        /// is required and a value can be entered. If the method returns a string (containing the reason it is not
        /// required), then the value is not required and the visual control may be disabled.
        /// </param>
        /// <param name="isValid">
        /// A method that determines if the parameter value is required. If the method returns null, then the 
        /// parameter value is considered valid. If the method returns a string (containing the reason it is not
        /// valid), then the value is not validated and the user has to enter a valid value.
        /// </param>
        /// <param name="isNullAllowed">
        /// A method that determines if a null value is allowed, returning true if it is and false if not.
        /// </param>
        public TaskParameter(Object id, String name, String description, String category, TaskParameterType parameterType, Object defaultValue,
            Boolean isHiddenByDefault, Boolean isReadOnlyByDefault, Int32? parentId, Func<String> isRequired, Func<Boolean> isNullAllowed, Func<String> isValid)
        {
            _id = (Int32)id;
            _name = name;
            _description = description;
            _category = category;
            _parameterType = parameterType;
            _defaultValue = defaultValue;
            _isHiddenByDefault = isHiddenByDefault;
            _isReadOnlyByDefault = isReadOnlyByDefault;
            _parentId = parentId;
            _isRequired = isRequired;
            _isNullAllowed = isNullAllowed;
            _isValid = isValid;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskParameter(Object id, String name, String description, String category, TaskParameterType parameterType, Object defaultValue,
            Boolean isHiddenByDefault, Boolean isReadOnlyByDefault)
            : this(id, name, description, category, parameterType, defaultValue,
            isHiddenByDefault, isReadOnlyByDefault, null, null, null, null)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        /// <param name="isRequired">
        /// A method that determines if the parameter is required. If the method returns null, then the parameter
        /// is required and a value can be entered. If the method returns a string (containing the reason it is not
        /// required), then the value is not required and the visual control may be disabled.
        /// </param>
        public TaskParameter(Object id, String name, String description, String category, TaskParameterType parameterType, Object defaultValue,
            Boolean isHiddenByDefault, Boolean isReadOnlyByDefault, Func<String> isRequired)
            : this(id, name, description, category, parameterType, defaultValue,
            isHiddenByDefault, isReadOnlyByDefault, null, isRequired, null, null)
        {
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public TaskParameter(Object id, String name, String description, String category, Type enumType, Object defaultValue,
            Boolean isHiddenByDefault, Boolean isReadOnlyByDefault)
            : this(id, name, description, category, TaskParameterType.Enum, (Int32)defaultValue,
            isHiddenByDefault, isReadOnlyByDefault, null, null, null, null)
        {
            RegisterEnum(enumType);
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="isRequired">
        /// A method that determines if the parameter is required. If the method returns null, then the parameter
        /// is required and a value can be entered. If the method returns a string (containing the reason it is not
        /// required), then the value is not required and the visual control may be disabled.
        /// </param>
        public TaskParameter(Object id, String name, String description, String category, Type enumType, Object defaultValue,
            Boolean isHiddenByDefault, Boolean isReadOnlyByDefault, Func<String> isRequired)
            : this(id, name, description, category, TaskParameterType.Enum, (Int32)defaultValue,
            isHiddenByDefault, isReadOnlyByDefault, null, isRequired, null, null)
        {
            RegisterEnum(enumType);
        }

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        /// <param name="isRequired">
        /// A method that determines if the parameter is required. If the method returns null, then the parameter
        /// is required and a value can be entered. If the method returns a string (containing the reason it is not
        /// required), then the value is not required and the visual control may be disabled.
        /// </param>
        /// <param name="isValid">
        /// A method that determines if the parameter value is required. If the method returns null, then the 
        /// parameter value is considered valid. If the method returns a string (containing the reason it is not
        /// valid), then the value is not validated and the user has to enter a valid value.
        /// </param>
        /// <param name="isNullAllowed">
        /// A method that determines if a null value is allowed, returning true if it is and false if not.
        /// </param>
        public TaskParameter(Object id, String name, String description, String category, Type enumType, Object defaultValue,
            Boolean isHiddenByDefault, Boolean isReadOnlyByDefault, Func<String> isRequired, Func<Boolean> isNullAllowed, Func<String> isValid)
            : this(id, name, description, category, TaskParameterType.Enum, (Int32)defaultValue,
            isHiddenByDefault, isReadOnlyByDefault, null, isRequired, isNullAllowed, isValid)
        {
            RegisterEnum(enumType);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public TaskParameter(Object id, String name, String description, String category, TaskParameterType parameterType, Object defaultValue,
            Boolean isHiddenByDefault, Boolean isReadOnlyByDefault, Int32? parentId) :
            this(id, name, description, category, parameterType, defaultValue,
            isHiddenByDefault, isReadOnlyByDefault, parentId, null, null, null)
        {
            _parentId = parentId;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Registers the given enum type.
        /// </summary>
        /// <param name="enumType"></param>
        private void RegisterEnum(Type enumType)
        {
            // attempt to automatically locate the enum helper type
            String enumHelperTypeName = enumType.AssemblyQualifiedName.Replace(enumType.Name, enumType.Name + "Helper");
            Type enumHelperType = Type.GetType(enumHelperTypeName);

            // get the enum name
            String enumName = String.Format("{0}.{1}", enumType.Namespace, enumType.Name);

            // get the friendly names and descriptions from the helper
            IDictionary friendlyNames = null;
            IDictionary friendlyDescriptions = null;
            if (enumHelperType != null)
            {
                // get the friendly names
                FieldInfo friendlyNamesField = enumHelperType.GetField("FriendlyNames", BindingFlags.Public | BindingFlags.Static);
                if (friendlyNamesField != null)
                {
                    friendlyNames = friendlyNamesField.GetValue(null) as IDictionary;
                }

                // get the friendly descriptions
                FieldInfo friendlyDescriptionsField = enumHelperType.GetField("FriendlyDescriptions", BindingFlags.Public | BindingFlags.Static);
                if (friendlyDescriptionsField != null)
                {
                    friendlyDescriptions = friendlyDescriptionsField.GetValue(null) as IDictionary;
                }
            }

            // enumerate through all the values in the enum
            List<TaskParameterEnumValue> enumValues = new List<TaskParameterEnumValue>();
            foreach (Object enumValue in Enum.GetValues(enumType))
            {
                // generate the enum value name
                String enumValueName = String.Format("{0}.{1}", enumName, enumValue.ToString());

                // add the friendly name
                String friendlyName;
                if (friendlyNames != null)
                {
                    friendlyName = friendlyNames[enumValue].ToString();
                }
                else
                {
                    friendlyName = enumValue.ToString();
                }

                // add the friendly description
                String friendlyDescription;
                if (friendlyDescriptions != null)
                {
                    friendlyDescription = friendlyDescriptions[enumValue].ToString();
                }
                else
                {
                    friendlyDescription = enumValue.ToString();
                }

                // build the resource enum value
                enumValues.Add(new TaskParameterEnumValue((Int32)enumValue, friendlyName, friendlyDescription));
            }
            _enumValues = new ReadOnlyCollection<TaskParameterEnumValue>(enumValues.OrderBy(e => e.Name).ToList());
        }
        #endregion
    }
}
