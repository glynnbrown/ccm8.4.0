﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-258390 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Concurrent;
using System.Collections;
using System.Reflection;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// Defines a possible task parameter value and its friendly name
    /// </summary>
    [Serializable]
    public class TaskParameterEnumValue
    {
        #region Fields
        private Object _value;
        private String _name;
        private  String _description;
        #endregion

        #region Properties

        /// <summary>
        /// The parameter enum value
        /// </summary>
        public Object Value
        {
            get { return _value; }
        }

        /// <summary>
        /// The parameter enum value name
        /// </summary>
        public String Name
        {
            get { return _name; }
        }

        /// <summary>
        /// The parameter enum value description
        /// </summary>
        public String Description
        {
            get { return _description; }
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of this type.
        /// </summary>
        public TaskParameterEnumValue(Object value, String name, String description)
        {
            _value = value;
            _name = name;
            _description = description;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns the instance hash code
        /// </summary>
        public override Int32 GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        /// <summary>
        /// Indicates if two instances are equal
        /// </summary>
        public override Boolean Equals(Object obj)
        {
            TaskParameterEnumValue other = obj as TaskParameterEnumValue;
            if (other != null)
            {
                if (other.Value != this.Value) return false;
            }
            else
            {
                return false;
            }
            return true;
        }

        #endregion
    }
}
