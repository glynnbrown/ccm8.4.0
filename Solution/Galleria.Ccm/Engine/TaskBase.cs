﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25560 : N.Foster
//  Created
// V8-25886 : L.Ineson
//  Added Category
// V8-27426 : L.Ineson
//  Ammended population of parameter values.
#endregion
#region Version History: CCM810
// V8-30123 : N.Foster
//  Removed calls to automatically re-merchandise planogram
// V8-30771 : N.Foster
//  Add support for additional task parameter properties
#endregion

#region Version History: CCM830

// V8-32282 : A.Silva
//  Added IsErrorFromThisTask.

#endregion

#endregion

using System;
using System.Linq;
using Galleria.Ccm.Engine.Exceptions;
using Galleria.Ccm.Model;
using Galleria.Framework.Engine;
using Galleria.Framework.Planograms.Model;
using EngineMetricType = Galleria.Ccm.Model.EngineMetricType;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// A base class from which all workflow tasks inherit
    /// </summary>
    [Serializable]
    public abstract class TaskBase : IDisposable
    {
        #region Enums
        /// <summary>
        /// Enum defining which sequence
        /// step we will execute
        /// </summary>
        internal enum TaskSequenceType
        {
            Pre,
            Post
        }
        #endregion

        #region Fields
        private Object _parametersLock = new Object(); // object used for locking
        private TaskParameters _parameters; // holds the task parameters
        #endregion

        #region Properties
        /// <summary>
        /// Returns the task name
        /// </summary>
        public String TaskType
        {
            get { return this.GetType().FullName; }
        }

        /// <summary>
        /// Returns the task name
        /// </summary>
        public abstract String Name { get; }

        /// <summary>
        /// Returns the task description
        /// </summary>
        public abstract String Description { get; }

        /// <summary>
        /// Returns the task grouping category.
        /// </summary>
        public abstract String Category { get; }

        /// <summary>
        /// Returns the minor version number
        /// </summary>
        public abstract Byte MinorVersion { get; }

        /// <summary>
        /// Returns the major version number
        /// </summary>
        public abstract Byte MajorVersion { get; }

        /// <summary>
        /// Indicates if a debug planogram should be captured for this task
        /// </summary>
        public virtual Boolean CaptureDebug
        {
            get { return true; }
        }

        /// <summary>
        /// Indicates that this task has a common pre-step
        /// </summary>
        public virtual Boolean HasPreStep
        {
            get { return false; }
        }

        /// <summary>
        /// Indicates that this task has a common post-step
        /// </summary>
        public virtual Boolean HasPostStep
        {
            get { return false; }
        }

        /// <summary>
        /// Returns a list of all parameters within the task
        /// </summary>
        public TaskParameters Parameters
        {
            get
            {
                this.RegisterParameters();
                return _parameters;
            }
        }

        /// <summary>
        /// Returns the name to use for this engine metric for this task
        /// </summary>
        private String MetricName
        {
            get { return String.Format("{0} v{1}.{2}", this.Name, this.MajorVersion, this.MinorVersion); }
        }

        /// <summary>
        /// Indicates if this task is visible
        /// </summary>
        public virtual Boolean IsVisible
        {
            get { return true; }
        }
        #endregion

        #region Methods

        #region Parameters
        /// <summary>
        /// Registers the task parameters
        /// </summary>
        private void RegisterParameters()
        {
            if (_parameters == null)
            {
                lock (_parametersLock)
                {
                    if (_parameters == null)
                    {
                        _parameters = new TaskParameters();
                        this.OnRegisterParameters();
                    }
                }
            }
        }

        /// <summary>
        /// Called when parameters are required to be registered
        /// </summary>
        protected abstract void OnRegisterParameters();

        /// <summary>
        /// Registers a parameter within this task
        /// </summary>
        protected void RegisterParameter(TaskParameter parameter)
        {
            parameter.Task = this;
            _parameters.Add(parameter);
        }

        /// <summary>
        /// Initializes the task parameters
        /// </summary>
        internal void InitializeParameters(WorkflowTask workflowTask)
        {
            this.InitializeParameters(workflowTask, null);
        }

        /// <summary>
        /// Initializes the task parameters
        /// </summary>
        internal void InitializeParameters(WorkflowTask workflowTask, WorkpackagePlanogram workpackagePlanogram)
        {
            // ensure that our parameters are registered
            this.RegisterParameters();

            // load this task with the parameters
            foreach (WorkflowTaskParameter workflowTaskParameter in workflowTask.Parameters)
            {
                // get the task parameter id
                Int32 taskParameterId = workflowTaskParameter.ParameterDetails.Id;

                // overlay the workflow task parameter details
                if (this.Parameters.Contains(taskParameterId))
                {
                    this.Parameters[taskParameterId].Values = workflowTaskParameter.Values.Select(p => (ITaskParameterValue)(new TaskParameterValue(p))).ToList();
                }

                // overlay the workpackage task parameter details
                if (workpackagePlanogram != null)
                {
                    var workpackageParameter = workpackagePlanogram.Parameters.FirstOrDefault(p => p.WorkflowTaskParameterId == workflowTaskParameter.Id);
                    if (workpackageParameter != null)
                    {
                        this.Parameters[taskParameterId].Values = workpackageParameter.Values.Select(p => (ITaskParameterValue)(new TaskParameterValue(p))).ToList();
                    }
                }
            }
        }
        #endregion

        #region Execute

        /// <summary>
        /// Executes this task against the specified planogram
        /// </summary>
        /// <exception cref="TaskRetryException">The <c>task</c> to execute was already being executed.</exception>
        internal void ExecuteTask(ITaskContext context)
        {
            // initialize the task parameters
            this.InitializeParameters(context.WorkflowTask, context.WorkpackagePlanogram);

            // now execute the task
            try
            {
                using (new EngineMetric((Byte)EngineMetricType.TaskExecution, this.MetricName)) 
                {
                    this.OnExecute(context);
                }
            }
            catch (TaskRetryException)
            {
                throw;
            }
            catch (Exception ex)
            {
                // an exception occurred when attempting
                // to execute this task, so log the exception
                // details to the planogram event log
                context.LogError(EventLogEvent.UnexpectedException, ex.Message);
            }
        }

        /// <summary>
        /// Executes either a pre or post sequence task step
        /// </summary>
        internal void ExecuteSequence(ITaskContext context, TaskSequenceType taskSequenceType)
        {
            // initialize the task parameters
            this.InitializeParameters(context.WorkflowTask, context.WorkpackagePlanogram);

            // now execute the task
            if (taskSequenceType == TaskSequenceType.Pre)
            {
                using (EngineMetric metric = new EngineMetric((Byte)EngineMetricType.TaskPreExecution, this.MetricName))
                {
                    this.OnPreExecute(context);
                }
            }
            else
            {
                using (EngineMetric metric = new EngineMetric((Byte)EngineMetricType.TaskPostExecution, this.MetricName))
                {
                    this.OnPostExecute(context);
                }
            }
        }

        /// <summary>
        /// Called when this task is to be executed
        /// </summary>
        protected abstract void OnExecute(ITaskContext context);

        /// <summary>
        /// Called when executing the pre sequence task
        /// </summary>
        public virtual void OnPreExecute(ITaskContext context)
        {
            // TODO
        }

        /// <summary>
        /// Called when executing the post sequence task
        /// </summary>
        public virtual void OnPostExecute(ITaskContext context)
        {
            // TODO
        }

        #endregion

        #region Dispose
        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();
        }

        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        protected virtual void OnDispose()
        {
        }
        #endregion

        #endregion

        #region Static Helper Methods

        /// <summary>
        ///     Determines if there are any errors logged in this particular task,
        ///     as oppossed to errors from previous tasks in the workpackage.
        /// </summary>
        /// <returns><c>True</c> if the event log entry is an error from this task, <c>false</c> otherwise.</returns>
        protected static Boolean IsErrorFromThisTask(ITaskContext context, PlanogramEventLog e)
        {
            //  NB some event logs might not have a task source (null, when the event was generated outside a task), 
            //  just ignore them as those are surely not from this one.
            return e.EntryType == PlanogramEventLogEntryType.Error &&
                   !String.IsNullOrWhiteSpace(e.TaskSource) &&
                   e.TaskSource.Contains(context.WorkflowTask.Details.Name);
        }

        #endregion
    }
}
