﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-30079 : L.Ineson
//  Changed workpackage processing status enum type.
#endregion

#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Ccm.Model;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Finalization
{
    /// <summary>
    /// Defines a message passed to the workpackage finalization process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workpackage.Finalization")]
    internal class Message : EngineMessageBase
    {
        #region Nested Classes

        #region Entity
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workpackage.Finalization.Entity")]
        internal class EntityDc
        {
            #region Properties
            /// <summary>
            /// The entity id
            /// </summary>
            [DataMember]
            public Int32 Id;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Workpackage.Execution.Message.EntityDc entity)
            {
                this.Id = entity.Id;
            }

            #endregion
        }
        #endregion

        #region Workpackage
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name="Workpackage.Finalization.Workpackage")]
        internal class WorkpackageDc
        {
            #region Properties
            /// <summary>
            /// The workpackage id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workpackage name
            /// </summary>
            [DataMember]
            public String Name;

            /// <summary>
            /// The workpackage row version
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The workpackage priority
            /// </summary>
            [DataMember]
            public Byte Priority;

            /// <summary>
            /// The workpackage status
            /// </summary>
            [DataMember]
            public WorkpackageProcessingStatusType Status;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Workpackage.Execution.Message.WorkpackageDc workpackage, Byte priority, WorkpackageProcessingStatusType status)
            {
                this.Id = workpackage.Id;
                this.Name = workpackage.Name;
                this.RowVersion = workpackage.RowVersion;
                this.Status = status;
            }

            #endregion
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// The entity details
        /// </summary>
        [DataMember]
        public EntityDc Entity;

        /// <summary>
        /// The workpackage details
        /// </summary>
        [DataMember]
        public WorkpackageDc Workpackage;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workpackage.Execution.Message.EntityDc entity,
            Workpackage.Execution.Message.WorkpackageDc workpackage,
            Byte workpackagePriority,
            WorkpackageProcessingStatusType workpackageStatus) :
            base(EngineMessagePriority.WorkpackageFinalization)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage, workpackagePriority, workpackageStatus);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workpackage.Execution.Message.EntityDc entity,
            Workpackage.Execution.Message.WorkpackageDc workpackage,
            WorkpackageProcessingStatusType workpackageStatus) :
            base(EngineMessagePriority.WorkpackageFinalization)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage, EngineMessagePriority.WorkflowExecution, workpackageStatus);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workpackage.Execution.Message.EntityDc entity,
            Workpackage.Execution.Message.WorkpackageDc workpackage) :
            base(EngineMessagePriority.WorkpackageFinalization)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage, EngineMessagePriority.WorkflowExecution, WorkpackageProcessingStatusType.Complete);
        }

        #endregion
    }
}
