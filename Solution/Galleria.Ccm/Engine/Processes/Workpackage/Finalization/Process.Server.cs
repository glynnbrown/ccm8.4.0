﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Updated Processing status updates to include new fields
// V8-28294 : A.Kuszyk
//  Updated SetWorkpackageProcessingStatus to only fail the workpackage if all the plans failed.
#endregion
#region Version History: CCM810
//V8-30079 : L.Ineson
//  Added addition workpackage processing status warnings.
//  Changed workpackage processing status enum type.
#endregion
#region Version History: CCM820
// GAF-31367 : N.Foster
//  Added scheduling of metadata and validation calculation jobs
#endregion
#region Version History: CCM820CP05
// V8-32572 : N.Foster
//  Requeue the message if a timeout exception occurs
#endregion
#region Version History: CCM830
// V8-32577 : N.Foster
//  Forward integration of V8-32572
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Engine.Exceptions;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Finalization
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!LoadWorkpackagePlanograms(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!SetWorkpackageProcessingStatus(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ProcessWorkpackagePlanograms(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineProcessAbortedException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is WorkpackageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is Galleria.Framework.Dal.TimeoutException)
                    {
                        // a timeout exception occurred when processing this job
                        // so requeue this job in order to try and reprocess it
                        this.RequeueMessage();
                    }
                    else
                    {
                        // throw a workpackage exception
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            ex);
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Returns details for all planograms within the workpackage
        /// </summary>
        private static Boolean LoadWorkpackagePlanograms(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch all workpackage planograms
                context.Planograms = PlanogramInfoList.FetchByWorkpackageId(context.MessageData.Workpackage.Id);

                // return success
                return true;
            }
        }

        /// <summary>
        /// Attempts to set the workpackage status
        /// </summary>
        private static Boolean SetWorkpackageProcessingStatus(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // get the desired processing status
                WorkpackageProcessingStatusType status = context.MessageData.Workpackage.Status;

                // if the workpackage processing status is not
                // failed, then we determine the status by
                // examinging all the planogram within the workpackage
                if (status != WorkpackageProcessingStatusType.Failed)
                {
                    // assume the workpackage was completed successfully
                    status = WorkpackageProcessingStatusType.Complete;

                    // V8-28294
                    // Only consider this workpackage failed if all the planograms failed.
                    if (context.Planograms.All(p => p.AutomationProcessingStatus == ProcessingStatus.Failed))
                    {
                        status = WorkpackageProcessingStatusType.Failed;
                    }
                    //V8-30079 - complete with some failed if any plans failed
                    else if (context.Planograms.Any(p => p.AutomationProcessingStatus == ProcessingStatus.Failed))
                    {
                        status = WorkpackageProcessingStatusType.CompleteWithSomeFailed;
                    }
                    //complete all warnings if all plans have warnings
                    else if (context.Planograms.All(p => p.MetaNoOfWarnings > 0))
                    {
                        status = WorkpackageProcessingStatusType.CompleteWithAllWarnings;
                    }
                    //complete with some warnings if any plan has warnings
                    else if (context.Planograms.Any(p => p.MetaNoOfWarnings > 0))
                    {
                        status = WorkpackageProcessingStatusType.CompleteWithSomeWarnings;
                    }
                }

                // attempt to change the workpackage status
                try
                {
                    using (IWorkpackageProcessingStatusDal dal = context.DalContext.GetDal<IWorkpackageProcessingStatusDal>())
                    {
                        WorkpackageProcessingStatusDto dto = dal.FetchByWorkpackageId(context.MessageData.Workpackage.Id);
                        dto.Status = (Byte)status;
                        dto.StatusDescription = WorkpackageProcessingStatusTypeHelper.FriendlyNames[status];
                        dto.ProgressCurrent = dto.ProgressMax;
                        dto.DateLastUpdated = DateTime.UtcNow;
                        dal.Update(dto);
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    // the workpackage no longer exists
                    // in the database, so there is nothing
                    // else that we can do
                    return false;
                }
                catch (ConcurrencyException)
                {
                    // a concurrency exception has occurred
                    // when trying to update the status of
                    // the workpackage, so requeue the message
                    // in order to try again
                    context.RequeueMessage();

                    // return
                    return false;
                }

                // log workpackage has finished
                Engine.LogWorkpackageFinished(
                    context.MessageData.Entity.Id,
                    context.MessageData.Workpackage.Id,
                    context.MessageData.Workpackage.Name);

                // indicate success
                return true;
            }
        }

        /// <summary>
        /// Enqueues additional messages for each planogram
        /// that was within the workpackage
        /// </summary>
        private static Boolean ProcessWorkpackagePlanograms(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // process each planogram within the original workpackage
                foreach (PlanogramInfo planogram in context.Planograms)
                {
                    // if the original requested workpackage status
                    // was failed, then we need to fail all the planograms
                    // in the workpacakge as well
                    if (context.MessageData.Workpackage.Status == WorkpackageProcessingStatusType.Failed)
                    {
                        Engine.EnqueueMessage(
                            new Workflow.Finalization.Message(
                                context.MessageData.Entity,
                                context.MessageData.Workpackage,
                                planogram,
                                ProcessingStatus.Failed));
                    }
                    else
                    {
                        // enqueue metadata calculation
                        Engine.EnqueueMessage(
                            new MetaData.Calculation.Message(planogram.Id));

                        // enqueue validation calculation
                        Engine.EnqueueMessage(
                            new Validation.Calculation.Message(planogram.Id));
                    }
                }

                // return success
                return true;
            }
        }
        #endregion
    }
}
