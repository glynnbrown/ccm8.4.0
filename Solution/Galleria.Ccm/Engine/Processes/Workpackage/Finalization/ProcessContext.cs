﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Finalization
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Fields
        private PlanogramInfoList _planograms; // holds all planograms that are part of the workpackage
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the planograms within the workpackage
        /// </summary>
        public PlanogramInfoList Planograms
        {
            get { return _planograms; }
            set { _planograms = value; }
        }

        #endregion
    }
}
