﻿#region Header Information

// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800

// GAF-25787 : N.Foster
//  Created

#endregion

#region Version History: CCM803

// V8-29731 : N.Foster
//  Ensure planograms are saved when error occurs

#endregion

#region Version History: CCM810

// V8-29811 : A.Probyn
//  Extended for new PlanogramLocationType
// V8-30079 : L.Ineson
//  Changed workpackage processing status enum type.

#endregion

#region Version History: CCM811

// V8-30369 : A.Silva
//  Amended CreatePlanogramGroup so that case is no longer considered a difference when searching for existing Planogram Groups.

#endregion

#region Version History: CCM820

// V8-31175 : A.Silva
//  Amended AssociatePlanogramsToGroup so that it can handle duplicate planogram names in the same planogram group folder without falling over 
//      (even if ideally there would be no duplicate plan names in the same folder, there are several ways this can happen).

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Engine.Exceptions;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Execution
{
    /// <summary>
    /// Performs the execution of a workpackage
    /// </summary>
    internal partial class Process
    {
        #region Nested Classes
        /// <summary>
        /// A simple structure used to track
        /// pre and post sequence steps
        /// </summary>
        private class SequencedTask
        {
            #region Properties
            public Byte SequenceId;
            public Byte? PreSequenceId;
            public Byte? PostSequenceId;
            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public SequencedTask(Byte sequenceId)
            {
                this.SequenceId = sequenceId;
            }
            #endregion
        }
        #endregion

        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!LoadWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ValidateWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LoadWorkflow(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!SetWorkpackageProcessingStatus(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!CreatePlanogramGroup(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!AssociatePlanogramsToGroup(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ExecuteWorkpackage(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineRequeueMessageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is EngineProcessAbortedException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is WorkpackageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else
                    {
                        // an unexpected exception has occurred
                        // we therefore need to ensure that this
                        // workpackage is failed by enqueing
                        // a workpackage finalization step
                        Engine.EnqueueMessage(
                            new Workpackage.Finalization.Message(
                                this.MessageData.Entity,
                                this.MessageData.Workpackage,
                                WorkpackageProcessingStatusType.Failed),
                            this.MessageData.Id);

                        // throw a workpackage exception
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            ex);
                    }
                }
            }
        }
        #endregion

        #region Methods

        #region LoadWorkpackage
        /// <summary>
        /// Attempt to load the required workpackage details
        /// </summary>
        private static Boolean LoadWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch the workpackage details
                // catching any dto does not exist
                // exceptions which indicate the 
                // workpackage no longer exists
                try
                {
                    context.Workpackage = Model.Workpackage.FetchById(context.MessageData.Workpackage.Id);
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            Resources.Language.Message.Engine_WorkpackageDeletedException);
                    }
                    else
                    {
                        throw;
                    }
                }

                // if the workpackage no longer exists
                // then we return false, and this process
                // simple will end with no further actions
                // being performed
                return context.Workpackage != null;
            }
        }
        #endregion

        #region ValidateWorkpackage
        /// <summary>
        /// Validates the workpackage
        /// </summary>
        private static Boolean ValidateWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // validate that the workpackage has not
                // changed since we started this process
                if (context.Workpackage.RowVersion != context.MessageData.Workpackage.RowVersion)
                    throw new WorkpackageException(
                        context.Workpackage,
                        Resources.Language.Message.Engine_WorkpackageInvalidException);

                // return success
                return true;
            }
        }
        #endregion

        #region LoadWorkflow
        /// <summary>
        /// Loads the workflow details
        /// </summary>
        private static Boolean LoadWorkflow(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Workflow = Model.Workflow.FetchById(context.Workpackage.WorkflowId);
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            Resources.Language.Message.Engine_WorkflowDeletedException,
                            context.Workpackage.WorkflowId);
                    }
                    else
                    {
                        throw;
                    }
                }

                // and return
                return context.Workflow != null;
            }
        }
        #endregion

        #region SetWorkpackageProcessingStatus
        /// <summary>
        /// Sets the workpackage status to processing
        /// </summary>
        private static Boolean SetWorkpackageProcessingStatus(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch the workpackage processing status
                WorkpackageProcessingStatus status = WorkpackageProcessingStatus.FetchByWorkpackageId(context.Workpackage.Id);

                // set the status to processing and update the progress max
                status.Status = WorkpackageProcessingStatusType.Processing;
                status.StatusDescription = WorkpackageProcessingStatusTypeHelper.FriendlyNames[WorkpackageProcessingStatusType.Processing];
                status.ProgressMax = context.Workpackage.Planograms.Count * (context.Workflow.Tasks.Count + 1);
                status.ProgressCurrent = 0;
                status.DateStarted = DateTime.UtcNow;
                status.DateLastUpdated = DateTime.UtcNow;

                // save the workpackage processing status
                status.Save();

                // return successful
                return true;
            }
        }
        #endregion

        #region CreatePlanogramGroup
        /// <summary>
        /// Creates a planogram group in which all the output
        /// planograms will be placed within the repository
        /// </summary>
        private static Boolean CreatePlanogramGroup(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    // if the workpackage type is a modify
                    // then we have no need to create an output planogram group
                    if (context.Workpackage.WorkpackageType == WorkpackageType.ModifyExisting) return true;

                    // fetch the planogram hierarchy
                    PlanogramHierarchy hierarchy = Model.PlanogramHierarchy.FetchByEntityId(context.Workpackage.EntityId);

                    // get all groups in the hierarchy
                    Dictionary<Int32, PlanogramGroup> hierarchyGroupLookup = hierarchy.EnumerateAllGroups().ToDictionary(p => p.Id);

                    // store existing planograms groups to make finding new folders quicker later on
                    IEnumerable<IGrouping<Int32?, PlanogramInfo>> existingPlanogramGroups = null;

                    // fetch all planogram infos for workpackage planogram source ids
                    IEnumerable<WorkpackagePlanogram> workpackageSourcePlans = context.Workpackage.Planograms.Where(p => p.SourcePlanogramId.HasValue);

                    // check for the planogram groups based on the workpackage type
                    if (context.Workpackage.WorkpackageType == WorkpackageType.NewFromExisting ||
                        context.Workpackage.WorkpackageType == WorkpackageType.StoreSpecific)
                    {
                        // fetch the planogram info for the workpackage source planograms
                        PlanogramInfoList workpackageSourcePlanInfos = PlanogramInfoList.FetchByIds(workpackageSourcePlans.Select(p => p.SourcePlanogramId.Value));

                        // get distinct planogram groups that need a new workpackage sub folder creating
                        existingPlanogramGroups = workpackageSourcePlanInfos.GroupBy(p => p.PlanogramGroupId);

                        // check for planogram groups based on the planogram location type
                        if (context.Workpackage.PlanogramLocationType == WorkpackagePlanogramLocationType.InNewSubFolder)
                        {
                            // enumerate through assigned planogram's groups
                            foreach (IGrouping<Int32?, PlanogramInfo> subFolderGroup in existingPlanogramGroups)
                            {
                                // lookup group requiring a sub folder
                                PlanogramGroup existingWorkpackagePlanGroup = null;
                                if (subFolderGroup.Key.HasValue)
                                {
                                    // find group planogram is currently assigned to
                                    hierarchyGroupLookup.TryGetValue(subFolderGroup.Key.Value, out existingWorkpackagePlanGroup);
                                }
                                else
                                {
                                    // use root group
                                    existingWorkpackagePlanGroup = hierarchy.RootGroup;
                                }

                                // ensure group should be found
                                if (existingWorkpackagePlanGroup != null)
                                {
                                    // create a new work package named folder under this (if needed)
                                    PlanogramGroup subFolderWorkpackageGroup = existingWorkpackagePlanGroup.ChildList.FirstOrDefault(group => String.Equals(group.Name, context.Workpackage.Name, StringComparison.InvariantCultureIgnoreCase));
                                    if (subFolderWorkpackageGroup == null)
                                    {
                                        subFolderWorkpackageGroup = PlanogramGroup.NewPlanogramGroup();
                                        subFolderWorkpackageGroup.Name = context.Workpackage.Name;
                                        existingWorkpackagePlanGroup.ChildList.Add(subFolderWorkpackageGroup);
                                    }
                                }
                            }
                        }
                        else
                        {
                            // InSourceFolder means that no new folder needs to be created, and the new planograms
                            // will be placed in the same folder as source planogram. We should already have groups stored in
                            // existingPlanogramGroups.
                        }
                    }
                    else if (context.Workpackage.WorkpackageType == WorkpackageType.NewFromFile)
                    {
                        // create a workpackage group named after the workpackage in the 
                        // root directory (if it doesn't already exist)
                        PlanogramGroup workpackageGroup = hierarchy.RootGroup.ChildList.FirstOrDefault(group => String.Equals(group.Name, context.Workpackage.Name, StringComparison.InvariantCultureIgnoreCase));
                        if (workpackageGroup == null)
                        {
                            workpackageGroup = PlanogramGroup.NewPlanogramGroup();
                            workpackageGroup.Name = context.Workpackage.Name;
                            hierarchy.RootGroup.ChildList.Add(workpackageGroup);
                        }
                    }

                    // save the hierarchy
                    // this could throw an exception is someone else
                    // has modified the hierarchy while this process
                    // has been running
                    if (hierarchy.IsDirty) hierarchy = hierarchy.Save();

                    // create mappings for planograms to groups
                    List<Tuple<Int32, PlanogramGroup>> workpackagePlanogramsGroupLookup = new List<Tuple<Int32, PlanogramGroup>>();
                    if (context.Workpackage.WorkpackageType == WorkpackageType.NewFromExisting ||
                        context.Workpackage.WorkpackageType == WorkpackageType.StoreSpecific)
                    {
                        // refresh hierarchy group lookup to account
                        // for the change in the planogram group ids after save
                        hierarchyGroupLookup = hierarchy.EnumerateAllGroups().ToDictionary(p => p.Id);

                        // find planograms group
                        if (context.Workpackage.PlanogramLocationType == WorkpackagePlanogramLocationType.InNewSubFolder)
                        {
                            foreach (IGrouping<Int32?, PlanogramInfo> subFolderGroup in existingPlanogramGroups)
                            {
                                // get existing group to start from
                                PlanogramGroup existingPlanogramGroup = null;
                                if (subFolderGroup.Key.HasValue)
                                {
                                    existingPlanogramGroup = hierarchyGroupLookup[subFolderGroup.Key.Value];
                                }
                                else
                                {
                                    existingPlanogramGroup = hierarchy.RootGroup;
                                }

                                // find newly created workpackage folder below this existing group
                                PlanogramGroup workpackagePlanogramGroup = existingPlanogramGroup.ChildList.FirstOrDefault(p => String.Equals(p.Name, context.Workpackage.Name, StringComparison.InvariantCultureIgnoreCase));

                                // find all workpackage planograms with this source plan id
                                foreach (PlanogramInfo plan in subFolderGroup)
                                {
                                    IEnumerable<WorkpackagePlanogram> subFolderGroupWorkpackagePlans = workpackageSourcePlans.Where(p => p.SourcePlanogramId == plan.Id);
                                    foreach (WorkpackagePlanogram subFolderGroupWorkpackagePlan in subFolderGroupWorkpackagePlans)
                                    {
                                        Tuple<Int32, PlanogramGroup> item = new Tuple<Int32, PlanogramGroup>(subFolderGroupWorkpackagePlan.DestinationPlanogramId, workpackagePlanogramGroup);
                                        if (!workpackagePlanogramsGroupLookup.Contains(item))
                                        {
                                            workpackagePlanogramsGroupLookup.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            // InSourceFolder means that no new folder needs to be created, and the new planograms
                            // will be placed in the same folder as source planogram.
                            foreach (IGrouping<Int32?, PlanogramInfo> subFolderGroup in existingPlanogramGroups)
                            {
                                // get existing group to start from
                                PlanogramGroup planogramGroup = null;
                                if (subFolderGroup.Key.HasValue)
                                {
                                    planogramGroup = hierarchyGroupLookup[subFolderGroup.Key.Value];
                                }
                                else
                                {
                                    planogramGroup = hierarchy.RootGroup;
                                }

                                // find all workpackage planograms with this source plan id
                                foreach (PlanogramInfo plan in subFolderGroup)
                                {
                                    IEnumerable<WorkpackagePlanogram> subFolderGroupWorkpackagePlans = workpackageSourcePlans.Where(p => p.SourcePlanogramId == plan.Id);
                                    foreach (WorkpackagePlanogram subFolderGroupWorkpackagePlan in subFolderGroupWorkpackagePlans)
                                    {
                                        Tuple<Int32, PlanogramGroup> item = new Tuple<Int32, PlanogramGroup>(subFolderGroupWorkpackagePlan.DestinationPlanogramId, planogramGroup);
                                        if (!workpackagePlanogramsGroupLookup.Contains(item))
                                        {
                                            workpackagePlanogramsGroupLookup.Add(item);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (context.Workpackage.WorkpackageType == WorkpackageType.NewFromFile)
                    {
                        // find workpackage folder in the root
                        PlanogramGroup workpackageGroup = hierarchy.RootGroup.ChildList.FirstOrDefault(group => String.Equals(group.Name, context.Workpackage.Name, StringComparison.InvariantCultureIgnoreCase));

                        // create mapping for each planogram
                        foreach (WorkpackagePlanogram plan in context.Workpackage.Planograms)
                        {
                            workpackagePlanogramsGroupLookup.Add(new Tuple<Int32, PlanogramGroup>(plan.DestinationPlanogramId, workpackageGroup));
                        }
                    }

                    // store the workpackage group within the context
                    context.PlangoramGroups = workpackagePlanogramsGroupLookup;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is ConcurrencyException)
                    {
                        // if a concurrency exception occurred, then
                        // something else has updated the planogram
                        // hierarchy while we have been changing it.
                        // in this instance, we requeue this message
                        // in order to try again
                        context.RequeueMessage();

                        // and return false to prevent further processing
                        return false;
                    }
                    else
                    {
                        throw;
                    }
                }

                // return successful
                return true;
            }
        }
        #endregion

        #region AssociatePlanogramsToGroup
        /// <summary>
        /// Links all the planograms to the planogram group
        /// </summary>
        private static Boolean AssociatePlanogramsToGroup(ProcessContext context)
        {
            using (new CodePerformanceMetric())
            {
                // ensure we have a planogram groups
                if (context.PlangoramGroups == null) return true;

                // create lookup of workpackage plan ids
                Dictionary<Int32, WorkpackagePlanogram> workpackageDestinationPlanLookup =
                    context.Workpackage.Planograms.ToDictionary(p => p.DestinationPlanogramId);

                // group the work package planograms to their new associated planogram groups
                foreach (
                    IGrouping<PlanogramGroup, Tuple<Int32, PlanogramGroup>> workpackagePlanogramsGroup in
                        context.PlangoramGroups.GroupBy(p => p.Item2))
                {
                    // get workpackage plans for this planogram group
                    List<WorkpackagePlanogram> planogramGroupWorkpackagePlans = new List<WorkpackagePlanogram>();
                    foreach (Int32 destinationPlanId in workpackagePlanogramsGroup.Select(p => p.Item1))
                    {
                        planogramGroupWorkpackagePlans.Add(workpackageDestinationPlanLookup[destinationPlanId]);
                    }

                    // fetch all planograms for the planogram group in question
                    PlanogramInfoList planogramGroupPlans = PlanogramInfoList.FetchByPlanogramGroupId(workpackagePlanogramsGroup.Key.Id);

                    // create faster lookup for names
                    Dictionary<String, List<PlanogramInfo>> planogramInfosByPlanogramName =
                        planogramGroupPlans.GroupBy(p => p.Name).ToDictionary(grouping => grouping.Key, grouping => grouping.ToList());

                    // check for any duplicate names
                    foreach (WorkpackagePlanogram workpackagePlan in planogramGroupWorkpackagePlans)
                    {
                        // if a plan with this name already exists
                        List<PlanogramInfo> matchingPlanogramNames;

                        if (!planogramInfosByPlanogramName.TryGetValue(workpackagePlan.Name, out matchingPlanogramNames)) continue;

                        //  Overwite the first match.
                        PlanogramInfo planToOverwrite = matchingPlanogramNames.First();
                        Int32? packageId =
                            PackageHelper.OverwritePackage(planToOverwrite,
                                                           workpackagePlan.DestinationPlanogramId,
                                                           workpackagePlan.Name,
                                                           context.Workpackage.UserId,
                                                           planogramGroupPlans,
                                                           context.Workpackage.EntityId);

                        // if the package has a value, it indicates its been overwritten
                        if (packageId.HasValue)
                        {
                            // log overwrite
                            LoggingHelper.Logger.Write(
                                _engineEventLogName,
                                context.Workpackage.EntityId,
                                EventLogEvent.PlanogramOverwritten,
                                context.Workpackage.Name,
                                context.Workpackage.Id,
                                workpackagePlan.Name,
                                workpackagePlanogramsGroup.Key.Name);
                        }

                        //  Remove the overwritten match from the list in the dictionary.
                        matchingPlanogramNames.Remove(planToOverwrite);
                        //  And the name from the dictionary if the list of matches is now empty.
                        if (!matchingPlanogramNames.Any()) planogramInfosByPlanogramName.Remove(workpackagePlan.Name);
                    }

                    // associate the planograms to the planogram group
                    PlanogramGroup.AssociatePlanograms(
                        workpackagePlanogramsGroup.Key.Id,
                        planogramGroupWorkpackagePlans.Where(p => p != null).Select(p => p.DestinationPlanogramId));
                }

                // return successful
                return true;
            }
        }

        #endregion

        #region ExecuteWorkpackage
        /// <summary>
        /// Executes the planogram
        /// </summary>
        private static Boolean ExecuteWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // we now enqueue a series of messages in order
                // to perform the processing of performance data
                // in parallel with the initialization of all
                // the planograms. We also finally queue up
                // a workpackage finalization step at the end
                // in order to tie everything together

                // next, we need to determine the priority
                // that we are going to execute the rest
                // of the batch at, as we want small batches
                // to take priority over large batches
                // we do this using a simple logarithmic
                // scale based on the number of planograms
                // within the workpackage
                Double calculatedPriority =
                    EngineMessagePriority.WorkflowExecution +
                    Math.Log(context.Workpackage.Planograms.Count, 1.25D);
                Byte priority = calculatedPriority > Byte.MaxValue ? Byte.MaxValue : (Byte)calculatedPriority;

                // we are going to build a planogram lookup
                // dictionary to make later lookups quicker
                Dictionary<Int32, PlanogramInfo> planogramLookup = new Dictionary<Int32, PlanogramInfo>();

                // fetch a list of all the planograms that we
                // are going to process
                IEnumerable<PlanogramInfo> planograms = PlanogramInfoList.FetchByIds(context.Workpackage.Planograms.Select(p => p.DestinationPlanogramId));
                foreach (PlanogramInfo planogram in planograms)
                {
                    if (!planogramLookup.ContainsKey(planogram.Id))
                        planogramLookup.Add(planogram.Id, planogram);
                }

                // fetch a list of the source planogram that
                // we are going to need to process
                IEnumerable<PlanogramInfo> sourcePlanograms = PlanogramInfoList.FetchByIds(context.Workpackage.Planograms.Where(p => p.SourcePlanogramId != null).Select(p => (Int32)p.SourcePlanogramId));
                foreach (PlanogramInfo sourcePlanogram in sourcePlanograms)
                {
                    if (!planogramLookup.ContainsKey(sourcePlanogram.Id))
                        planogramLookup.Add(sourcePlanogram.Id, sourcePlanogram);
                }

                // we now need to determine if any tasks within the worflow
                // has a pre or post step, so that we can orchestrate
                // the execution of the workflow
                Dictionary<Byte, SequencedTask> sequencedTasks = new Dictionary<Byte, SequencedTask>();
                foreach (WorkflowTask task in context.Workflow.Tasks.OrderBy(t => t.SequenceId))
                {
                    // if this task has a pre-step
                    // then add it to the sequenced steps
                    // based on the current sequence id
                    if (task.Details.HasPreStep)
                    {
                        Byte sequenceId = task.SequenceId;
                        if (!sequencedTasks.ContainsKey(sequenceId)) sequencedTasks[sequenceId] = new SequencedTask(sequenceId);
                        sequencedTasks[sequenceId].PreSequenceId = task.SequenceId;
                    }

                    // if this task has a post-step
                    // then add it to the sequenced steps
                    // based on the next sequence id
                    if (task.Details.HasPostStep)
                    {
                        Byte sequenceId = (Byte)(task.SequenceId + 1);
                        if (!sequencedTasks.ContainsKey(sequenceId)) sequencedTasks[sequenceId] = new SequencedTask(sequenceId);
                        sequencedTasks[sequenceId].PostSequenceId = task.SequenceId;
                    }
                }

                // lock the queue
                lock (Engine.QueueLock)
                {
                    // start a transaction
                    context.DalContext.Begin();

                    // enqueue our pre/post step engine tasks
                    Int64 parentMessageId = context.MessageId;
                    List<Tuple<Byte, Int64>> workflowSequencedTasks = new List<Tuple<Byte, Int64>>();
                    foreach (var sequencedTask in sequencedTasks.Values.OrderBy(item => item.SequenceId))
                    {
                        // enqueue our message, making it dependent on
                        // the previous message that was enqueued
                        parentMessageId = Engine.EnqueueMessage(
                            context.DalContext,
                            new Workflow.Sequence.Message(priority, context.MessageData.Entity, context.MessageData.Workpackage, context.Workflow, sequencedTask.PreSequenceId, sequencedTask.PostSequenceId),
                            0,
                            new List<Int64>() { parentMessageId });

                        // build our workflow pre post execution ids
                        workflowSequencedTasks.Add(new Tuple<Byte, Int64>(sequencedTask.SequenceId, parentMessageId));
                    }

                    // enqueue the workpackage finalization message
                    Int64 workpackageFinalizationMessageId = Engine.EnqueueMessage(
                        context.DalContext,
                        new Workpackage.Finalization.Message(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage));

                    // enqueue a message for each planogram that requires processing
                    List<Int64> messageIds = new List<Int64>();
                    Random rnd = new Random();
                    foreach (WorkpackagePlanogram workpackagePlanogram in context.Workpackage.Planograms.OrderBy(i => rnd.NextDouble()))
                    {
                        // locate the output planogram
                        PlanogramInfo planogram = planogramLookup[workpackagePlanogram.DestinationPlanogramId];

                        // locate the source planogram
                        PlanogramInfo sourcePlanogram = workpackagePlanogram.SourcePlanogramId == null ? null : planogramLookup[(Int32)workpackagePlanogram.SourcePlanogramId];

                        // enqueue the message
                        messageIds.Add(Engine.EnqueueMessage(
                            context.DalContext,
                            new Workflow.Initialization.Message(
                                context.MessageData.Entity,
                                context.MessageData.Workpackage,
                                context.Workflow,
                                planogram,
                                sourcePlanogram,
                                priority,
                                workpackageFinalizationMessageId,
                                workflowSequencedTasks)));
                    }

                    // now we have enqueued all our planogram execution
                    // steps, we go back and add dependencies to the first
                    // sequenced task
                    if (workflowSequencedTasks.Count > 0)
                    {
                        Engine.AddDependencies(
                            context.DalContext,
                            workflowSequencedTasks[0].Item2,
                            messageIds);
                    }

                    // finally, we enque the workpackage finalization
                    // step, making it dependent on all the planogram
                    // initialization steps plus the last sequenced task
                    // if sequenced tasks exist. This is easy enough to do
                    // as the parentMessageId variable holds this value
                    messageIds.Add(parentMessageId);

                    // and add dependencies to the workpackage finalization step
                    Engine.AddDependencies(
                        context.DalContext,
                        workpackageFinalizationMessageId,
                        messageIds);

                    // commit the transaction
                    context.DalContext.Commit();
                }

                // return successful
                return true;
            }
        }
        #endregion

        #endregion
    }
}