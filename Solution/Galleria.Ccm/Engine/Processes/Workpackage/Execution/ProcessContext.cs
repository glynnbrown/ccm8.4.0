﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
//  Extended for new PlanogramLocationType
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Execution
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Fields
        Model.Workpackage _workpackage; // holds the workpackage details
        Model.Workflow _workflow; // holds the workflow details
        IEnumerable<Tuple<Int32, Model.PlanogramGroup>> _planogramGroups; // holds the planogram groups
        Dictionary<Byte, TaskBase> _workflowTasks; // holds the workflow tasks
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the workpackge details
        /// </summary>
        public Model.Workpackage Workpackage
        {
            get { return _workpackage; }
            set { _workpackage = value; }
        }

        /// <summary>
        /// Gets or sets the workflow details
        /// </summary>
        public Model.Workflow Workflow
        {
            get { return _workflow; }
            set { _workflow = value; }
        }

        /// <summary>
        /// Gets or sets the planogram group
        /// </summary>
        public IEnumerable<Tuple<Int32, Model.PlanogramGroup>> PlangoramGroups
        {
            get { return _planogramGroups; }
            set { _planogramGroups = value; }
        }

        /// <summary>
        /// Returns the workflow tasks
        /// </summary>
        public Dictionary<Byte, TaskBase> WorkflowTasks
        {
            get
            {
                if (_workflowTasks == null) _workflowTasks = new Dictionary<Byte, TaskBase>();
                return _workflowTasks;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion
    }
}
