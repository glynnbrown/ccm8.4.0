﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-29811 : A.Probyn
//  Added _engineEventLogName
#endregion
#endregion

using Galleria.Framework.Engine;
using Galleria.Framework.Engine.Dal.DataTransferObjects;
using System;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Execution
{
    internal partial class Process : EngineMessageProcessBase<Process, Message>
    {
        #region Constants
        private const String _engineEventLogName = "Engine";
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Process(EngineMessageDto messageDto) : base(messageDto) { }
        #endregion
    }
}
