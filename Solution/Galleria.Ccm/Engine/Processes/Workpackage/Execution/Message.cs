﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Execution
{
    /// <summary>
    /// Defines a message passed to the workpackage finalization process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workpackage.Execution")]
    internal class Message : EngineMessageBase
    {
        #region Nested Classes

        #region Entity
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workpackage.Execution.Entity")]
        internal class EntityDc
        {
            #region Properties
            /// <summary>
            /// The entity id
            /// </summary>
            [DataMember]
            public Int32 Id;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Workpackage.Initialization.Message.EntityDc entity)
            {
                this.Id = entity.Id;
            }
            #endregion
        }
        #endregion

        #region Workpackage
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name="Workpackage.Execution.Workpackage")]
        internal class WorkpackageDc
        {
            #region Properties
            /// <summary>
            /// The workpackage id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workpackage row version
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The workpackage name
            /// </summary>
            [DataMember]
            public String Name;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Workpackage.Initialization.Message.WorkpackageDc workpackage)
            {
                this.Id = workpackage.Id;
                this.RowVersion = workpackage.RowVersion;
                this.Name = workpackage.Name;
            }
            #endregion
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// The entity details
        /// </summary>
        [DataMember]
        public EntityDc Entity;

        /// <summary>
        /// The workpackage details
        /// </summary>
        [DataMember]
        public WorkpackageDc Workpackage;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workpackage.Initialization.Message.EntityDc entity,
            Workpackage.Initialization.Message.WorkpackageDc workpackage) :
            base(EngineMessagePriority.WorkpackageExecution)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage);
        }
        #endregion
    }
}
