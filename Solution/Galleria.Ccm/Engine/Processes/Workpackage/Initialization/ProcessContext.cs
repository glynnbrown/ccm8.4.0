﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Initialization
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Fields
        private WorkpackageProcessingStatusDto _workpackageProcessingStatus; // holds the workpackage processing status details
        private WorkpackageInfoDto _workpackageInfo; // holds the workpackage info
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the workpackage details
        /// </summary>
        public WorkpackageProcessingStatusDto WorkpackageProcessingStatus
        {
            get { return _workpackageProcessingStatus; }
            set { _workpackageProcessingStatus = value; }
        }

        /// <summary>
        /// Gets or sets the workpackage details
        /// </summary>
        public WorkpackageInfoDto Workpackage
        {
            get { return _workpackageInfo; }
            set { _workpackageInfo = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion
    }
}
