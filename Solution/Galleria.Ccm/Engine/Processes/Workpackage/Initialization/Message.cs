﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Initialization
{
    /// <summary>
    /// Defines a message passed to the workpackage initialization process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workpackage.Initialization")]
    internal class Message : EngineMessageBase
    {
        #region Nested Classes

        #region Entity
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workpackage.Initialization.Entity")]
        internal class EntityDc
        {
            #region Properties
            /// <summary>
            /// The entity id
            /// </summary>
            [DataMember]
            public Int32 Id;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Int32 id)
            {
                this.Id = id;
            }

            #endregion
        }
        #endregion

        #region Workpackage
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name="Workpackage.Initialization.Workpackage")]
        internal class WorkpackageDc
        {
            #region Properties
            /// <summary>
            /// The workpackage id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workpackage name
            /// </summary>
            [DataMember]
            public String Name;

            /// <summary>
            /// The workpackage rowversion
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Int32 id, String name, RowVersion rowVersion)
            {
                this.Id = id;
                this.Name = name;
                this.RowVersion = rowVersion;
            }
            #endregion
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// The entity details
        /// </summary>
        [DataMember]
        public EntityDc Entity;

        /// <summary>
        /// The workpackage details
        /// </summary>
        [DataMember]
        public WorkpackageDc Workpackage;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Int32 entityId,
            Int32 workpackageId,
            String workpackageName,
            RowVersion workpackageRowVersion) :
            base(EngineMessagePriority.WorkpackageInitialization)
        {
            this.Entity = new EntityDc(entityId);
            this.Workpackage = new WorkpackageDc(workpackageId, workpackageName, workpackageRowVersion);
        }

        #endregion
    }
}
