﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Updated Processing status updates to include new fields
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.Workpackage.Initialization
{
    /// <summary>
    /// Performs the initialization of a workpackage
    /// </summary>
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!InitializeWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ExecuteWorkpackage(context)) return;
                }
                catch
                {
                    throw;
                }
            }
        }
        #endregion

        #region Methods

        #region InitializeWorkpackage
        /// <summary>
        /// Queue the workpackage
        /// </summary>
        private static Boolean InitializeWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    using (IWorkpackageProcessingStatusDal dal = context.DalContext.GetDal<IWorkpackageProcessingStatusDal>())
                    {
                        // fetch the workpackage processing status details
                        context.WorkpackageProcessingStatus = dal.FetchByWorkpackageId(context.MessageData.Workpackage.Id);

                        // verify that we are not already processing this workpackage
                        if ((context.WorkpackageProcessingStatus.Status == (Byte)WorkpackageProcessingStatusType.Queued) ||
                           (context.WorkpackageProcessingStatus.Status == (Byte)WorkpackageProcessingStatusType.Processing))
                        {
                            return false;
                        }

                        // set the workpackage processing status
                        context.WorkpackageProcessingStatus.Status = (Byte)WorkpackageProcessingStatusType.Queued;
                        context.WorkpackageProcessingStatus.StatusDescription = WorkpackageProcessingStatusTypeHelper.FriendlyNames[WorkpackageProcessingStatusType.Queued];
                        context.WorkpackageProcessingStatus.ProgressMax = 1;
                        context.WorkpackageProcessingStatus.ProgressCurrent = 0;
                        context.WorkpackageProcessingStatus.DateStarted = DateTime.UtcNow;
                        context.WorkpackageProcessingStatus.DateLastUpdated = DateTime.UtcNow;

                        // attempt to update the workpackage processing status
                        dal.Initialize(context.WorkpackageProcessingStatus);
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    // the workpackage no longer exists
                    // so we simple exit this process
                    return false;
                }
                catch (ConcurrencyException)
                {
                    // if a concurrency exception occurred,
                    // then something has modified the workpackage
                    // processing status while we have been processing
                    // this message, so try again
                    context.RequeueMessage();

                    // and return false
                    return false;
                }

                // return success
                return true;
            }
        }

        #endregion

        #region ExecuteWorkpackage
        /// <summary>
        /// Performs the initialization of the workpackage
        /// </summary>
        private static Boolean ExecuteWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch the workpackage info details
                WorkpackageInfoDto workpackage;
                try
                {
                    using (IWorkpackageInfoDal dal = context.DalContext.GetDal<IWorkpackageInfoDal>())
                    {
                        workpackage = dal.FetchById(context.MessageData.Workpackage.Id);
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    return false;
                }

                // enqueue a message to execute the workpackage
                Engine.EnqueueMessage(new Workpackage.Execution.Message(
                    context.MessageData.Entity,
                    context.MessageData.Workpackage));

                // log that the workpackage has been started
                Engine.LogWorkpackageStarted(workpackage.EntityId, workpackage.Id, workpackage.Name);

                // and return successful
                return true;
            }
        }
        #endregion

        #endregion
    }
}
