﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Engine.Processes.Workpackage
{
    /// <summary>
    /// Context for the workpackge process
    /// </summary>
    internal class ProcessContext : IDisposable
    {
        #region Fields
        private IDalContext _dalContext; // the dal context to use for the process
        private WorkpackageDto _workpackageDto; // holds the workpackage dto
        #endregion

        #region Properties
        /// <summary>
        /// Returns the dal context
        /// </summary>
        public IDalContext DalContext
        {
            get { return _dalContext; }
            private set { _dalContext = value; }
        }

        /// <summary>
        /// Returns the workpackage dto
        /// </summary>
        public WorkpackageDto WorkpackageDto
        {
            get { return _workpackageDto; }
            private set { _workpackageDto = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(Int32 workpackageId)
        {
            // create a new dal context
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            this.DalContext = dalFactory.CreateContext();

            // fetch the workpackage information
            using (IWorkpackageDal dal = this.DalContext.GetDal<IWorkpackageDal>())
            {
                this.WorkpackageDto = dal.FetchById(workpackageId);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        public void Dispose()
        {
            if (this.DalContext != null) this.DalContext.Dispose();
        }
        #endregion
    }
}
