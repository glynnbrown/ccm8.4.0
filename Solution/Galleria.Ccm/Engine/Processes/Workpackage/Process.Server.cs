﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Engine.Messages;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Engine.Processes.Workpackage
{
    internal partial class Process
    {
        #region Methods

        #region Process
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (ProcessContext context = new ProcessContext(this.MessageData.WorkpackageId))
            {
                // process the workpackage
                switch (this.MessageData.Action)
                {
                    case WorkpackageMessageAction.Process:
                        ProcessWorkpackage(context);
                        break;
                    case WorkpackageMessageAction.Finalize:
                        FinalizeWorkpackage(context);
                        break;
                }
            }
        }
        #endregion

        #region ProcessWorkpackage
        /// <summary>
        /// Performs the processing of a workpackage
        /// </summary>
        private void ProcessWorkpackage(ProcessContext context)
        {
            // attempt to update the status of the workpackage to processing
            // if a concurrency exception occurs at this point then another process
            // has changed the workpackage so we take no further action
            context.WorkpackageDto.ProcessingStatus = (Byte)WorkpackageProcessingStatus.Processing;
            try
            {
                using (IWorkpackageDal dal = context.DalContext.GetDal<IWorkpackageDal>())
                {
                    dal.Update(context.WorkpackageDto);
                }
            }
            catch (ConcurrencyException)
            {
                return;
            }

            // we are now at the point where we can process our workpackage
            // for now, we are going to simply add a message to the queue
            // for each planogram that requires processing by the workflow
            // so fetch the list of workpackage planograms
            IEnumerable<WorkpackagePlanogramDto> planograms;
            using (IWorkpackagePlanogramDal dal = context.DalContext.GetDal<IWorkpackagePlanogramDal>())
            {
                planograms = dal.FetchByWorkpackageId(context.WorkpackageDto.Id);
            }

            // start a transaction
            context.DalContext.Begin();

            // we will also create a dependant message that finalises the
            // workpackage once all of the planograms have been processed
            // so we will need to keep track of the message ids as they
            // are submitted
            List<Int64> messageIds = new List<Int64>();
            foreach (WorkpackagePlanogramDto planogram in planograms)
            {
                messageIds.Add(Engine.SubmitMessage(
                    context.DalContext,
                    EngineMessageType.Workflow,
                    EngineMessagePriority.Workflow,
                    new WorkflowMessage(planogram.WorkpackageId, planogram.PlanogramId, planogram.SourcePlanogramId)));
            }

            // now submit the dependent message
            Engine.SubmitMessage(
                context.DalContext,
                EngineMessageType.Workpackage,
                EngineMessagePriority.Workpackage,
                new WorkpackageMessage(context.WorkpackageDto.Id, WorkpackageMessageAction.Finalize),
                messageIds);

            // commit the transaction
            context.DalContext.Commit();
        }
        #endregion

        #region FinalizeWorkpackage
        /// <summary>
        /// Performs the finalization of a workpackage
        /// </summary>
        private void FinalizeWorkpackage(ProcessContext context)
        {
            // attempt to update the status of the workpackage to processing
            // if a concurrency exception occurs at this point then another process
            // has changed the workpackage so we take no further action
            context.WorkpackageDto.ProcessingStatus = (Byte)WorkpackageProcessingStatus.Complete;
            try
            {
                using (IWorkpackageDal dal = context.DalContext.GetDal<IWorkpackageDal>())
                {
                    dal.Update(context.WorkpackageDto);
                }
            }
            catch (ConcurrencyException)
            {
                return;
            }
        }
        #endregion

        #endregion
    }
}
