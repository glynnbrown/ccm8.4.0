﻿ #region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
// V8-27964 : A.Silva
//  Added PlanHierarchySync to the message queue.
#endregion
#region Version History: CCM803
// V8-29606 : N.Foster
//  Added the cleanup of orphaned planograms
#endregion
 #endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.Maintenance.Execution
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!EnqueueMessages(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineProcessAbortedException)
                    {
                        // ignore this error as we dont need
                        // the engine to reap this message
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Enqueues a series of new messages that perform the actual maintenance
        /// </summary>
        private static Boolean EnqueueMessages(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // build a list of messages to enqueue
                List<EngineMessageBase> messages = new List<EngineMessageBase>()
            {
                new Processes.Cleanup.EngineMetrics.Message(),
                new Processes.Cleanup.Locks.Message(),
                new Processes.Cleanup.Instances.Message(),
                new Processes.Cleanup.Planograms.Message(),
                new Processes.MetaData.Initialization.Message(),
                new Processes.Validation.Initialization.Message(),
                new Processes.PlanHierarchySync.Execution.Message()
            };

                // enqueue our messages in priority order
                lock (Engine.QueueLock)
                {
                    foreach (EngineMessageBase message in messages.OrderBy(item => item.Priority))
                    {
                        if (!Engine.MessageExists(message.GetMessageType()))
                        {
                            Engine.EnqueueMessage(message);
                        }
                    }
                }

                // and return success
                return true;
            }
        }
        #endregion
    }
}
