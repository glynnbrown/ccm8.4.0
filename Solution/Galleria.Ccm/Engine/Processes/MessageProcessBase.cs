﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Processes;

namespace Galleria.Ccm.Engine.Processes
{
    /// <summary>
    /// Base class for any process executed by
    /// the queue worker
    /// </summary>
    public abstract class MessageProcessBase<TProcess, TMessage> : ProcessBase<TProcess>
        where TProcess : MessageProcessBase<TProcess, TMessage>
        where TMessage : class
    {
        #region Properties

        #region MessageId
        /// <summary>
        /// MessageId property definition
        /// </summary>
        public static readonly PropertyInfo<Int64> MessageIdProperty =
            RegisterProperty<Int64>(c => c.MessageId);
        /// <summary>
        /// Returns the message id
        /// </summary>
        public Int64 MessageId
        {
            get { return this.ReadProperty<Int64>(MessageIdProperty); }
        }
        #endregion

        #region MessagePriority
        /// <summary>
        /// MessagePriority property definition
        /// </summary>
        public static readonly PropertyInfo<Byte> MessagePriorityProperty =
            RegisterProperty<Byte>(c => c.MessagePriority);
        /// <summary>
        /// Returns the message priority
        /// </summary>
        public Byte MessagePriority
        {
            get { return this.ReadProperty<Byte>(MessagePriorityProperty); }
        }
        #endregion

        #region MessageData
        /// <summary>
        /// MessageData property definition
        /// </summary>
        public static readonly PropertyInfo<TMessage> MessageDataProperty =
            RegisterProperty<TMessage>(c => c.MessageData);
        /// <summary>
        /// Returns the message data
        /// </summary>
        public TMessage MessageData
        {
            get { return this.ReadProperty<TMessage>(MessageDataProperty); }
        }
        #endregion

        #endregion

        #region Constructors
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        public MessageProcessBase(MessageDto messageDto)
        {
            this.LoadProperty<Int64>(MessageIdProperty, messageDto.Id);
            this.LoadProperty<Byte>(MessagePriorityProperty, messageDto.Priority);
            this.LoadProperty<TMessage>(MessageDataProperty, Engine.DeserializeMessage<TMessage>(messageDto.Data));
        }
        #endregion
    }
}
