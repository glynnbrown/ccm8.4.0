﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: v8.0

// V8-27964 : A.Silva
//      Created

#endregion

#endregion

using System.Runtime.Serialization;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.PlanHierarchySync.Execution
{
    /// <summary>
    ///     Defines a message passed to the planogram hierarchy synchronization process.
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "PlanHierarchySync.Execution")]
    internal class Message : EngineMessageBase
    {
        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="Message"/>.
        /// </summary>
        public Message() : base(EngineMessagePriority.PlanHierarchySync) { }

        #endregion
    }
}