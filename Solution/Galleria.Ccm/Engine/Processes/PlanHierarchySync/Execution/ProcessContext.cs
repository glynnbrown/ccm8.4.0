﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-27964 : A.Silva
//      Created

#endregion

#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.PlanHierarchySync.Execution
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Properties

        public EntityInfoList Entities { get; set; }
        
        #endregion

        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="ProcessContext"/>.
        /// </summary>
        /// <param name="messageData">The message to use for this process.</param>
        public ProcessContext(IEngineMessageProcess<Message> process) : base(process) { }

        #endregion

        #region Methods

        /// <summary>
        ///     Invoked when this instance is disponsed.
        /// </summary>
        protected override void OnDispose()
        {
            Entities = null;
        }

        #endregion
    }
}
