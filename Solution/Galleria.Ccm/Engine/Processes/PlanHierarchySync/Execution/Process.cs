﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-27964 : A.Silva
//      Created

#endregion

#endregion

using Galleria.Framework.Engine;
using Galleria.Framework.Engine.Dal.DataTransferObjects;

namespace Galleria.Ccm.Engine.Processes.PlanHierarchySync.Execution
{
    /// <summary>
    ///     Implementation of the execution of the planogram hierarchy synchronization process.
    /// </summary>
    internal partial class Process : EngineMessageProcessBase<Process, Message>
    {
        #region Constructor

        /// <summary>
        ///     Initializes a new instance of <see cref="Process"/>.
        /// </summary>
        public Process(EngineMessageDto messageDto) : base(messageDto) { }

        #endregion
    }
}
