﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800

// V8-27964 : A.Silva
//      Created

#endregion

#endregion

using System;
using System.Diagnostics;
using Galleria.Ccm.Model;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.PlanHierarchySync.Execution
{
    /// <summary>
    ///     Server side implementation of PlanHierarchySync.Execution
    /// </summary>
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Invoked when this process is executed.
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!LoadEntities(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!SyncPlanHierarchy(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineProcessAbortedException)
                    {
                        //  Ignore this error as we don't need
                        //  the engine to reap this message.
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Loads the desired Entity in the process context.
        /// </summary>
        /// <param name="context">Context for this method execution.</param>
        /// <returns><c>True</c> if the entity was loaded correctly, <c>false</c> otherwise.</returns>
        /// <remarks>If there is no matching entity in the repository, just returns failure.</remarks>
        private static Boolean LoadEntities(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Entities = EntityInfoList.FetchAllEntityInfos();
                }
                catch (Exception e)
                {
                    Debug.Fail(e.Message);
                    //  Ignore any exceptions in production, the process will be rescheduled.
                    return false;
                }

                //  Return success.
                return true;
            }
        }

        /// <summary>
        ///     Perform the planogram hierarchy sycnhronization.
        /// </summary>
        /// <param name="context">Context for this method execution.</param>
        /// <returns><c>True</c> if the planogram hierarchy was syncronized, <c>false</c> otherwise.</returns>
        private static Boolean SyncPlanHierarchy(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //  Check that there is an entity to synchronize.
                if (context.Entities == null) return false;

                try
                {
                    //  Synchronize the planogram hierarchy for the entity.
                    foreach (EntityInfo entityInfo in context.Entities)
                    {
                        Entity.FetchById(entityInfo.Id).SyncPlanHierarchy();
                    }
                }
                catch (Exception e)
                {
                    Debug.Fail(e.Message);
                    return false;
                }

                //  Return success.
                return true;
            }
        }

        #endregion
    }
}