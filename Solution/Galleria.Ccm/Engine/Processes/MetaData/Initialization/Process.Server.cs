﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM820
// GAF-31367 : N.Foster
//  Added workpackage status checks
// V8-31006 : M.Brumby
//  Allow catchup if we have empty workpackages that are processing/queued
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.MetaData.Initialization
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!CheckWorkpackageStatus(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!MetadataCatchup(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is Galleria.Framework.Dal.TimeoutException)
                    {
                        // ignore this error as we dont
                        // need to worry about a timeout
                    }
                    else if (baseException is EngineProcessAbortedException)
                    {
                        // ignore this error as we dont need
                        // the engine to reap this message
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Checks to see if any workpackages are currently being processed
        /// if so, then no metadata calculations are enqueued at this time
        /// </summary>
        private static Boolean CheckWorkpackageStatus(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // for each entity within the solution
                foreach (EntityInfo entity in EntityInfoList.FetchAllEntityInfos())
                {
                    // for each workpackage within the entity
                    foreach (WorkpackageInfo workpackage in WorkpackageInfoList.FetchByEntityId(entity.Id))
                    {
                        // check the status of the workpackage
                        if ((workpackage.ProcessingStatus == WorkpackageProcessingStatusType.Queued) ||
                            (workpackage.ProcessingStatus == WorkpackageProcessingStatusType.Processing))
                        {
                            //if there are no planograms then the workpackage can never process so ignore it
                            if (workpackage.PlanogramCount == 0) continue;

                            //check individual counts incase planogram count is incorrect
                            //(this can happen if the user deletes plans from the workpackage sometimes)
                            if (workpackage.PlanogramProcessingCount +
                                workpackage.PlanogramQueuedCount == 0) continue;

                            // a workpackage is currently being processed
                            // so do not schedule a metadata update at this time
                            return false;
                        }
                    }
                }

                // return succes
                return true;
            }
        }

        /// <summary>
        /// Enqueues a series of messages to perform
        /// a catch-up of metadata processing
        /// </summary>
        private static Boolean MetadataCatchup(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch a list of all planograms that are
                // waiting to have their metadata calculated
                IEnumerable<Int32> planogramIds;
                using (IEngineMaintenanceDal dal = context.DalContext.GetDal<IEngineMaintenanceDal>())
                {
                    planogramIds = dal.FetchPlanogramsByMetadataCalculationRequired();
                }

                // enqueue a message for each planogram
                // we randomise the order to spread the processing
                // of planograms across the planogram table to reduce
                // contention when processing large amounts of plans
                lock (Engine.QueueLock)
                {
                    Random rnd = new Random();
                    foreach (Int32 planogramId in planogramIds.OrderBy(i => rnd.NextDouble()))
                    {
                        Engine.EnqueueMessage(new MetaData.Calculation.Message(planogramId));
                        if (context.IsAborted) throw new EngineProcessAbortedException();
                    }
                }

                // return that we were successful
                return true;
            }
        }
        #endregion
    }
}
