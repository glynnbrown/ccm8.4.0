﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-29606 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Planograms
{
    /// <summary>
    /// Defines a message passed to the lock cleanup process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Cleanup.Planograms")]
    internal class Message : EngineMessageBase
    {
        #region Properties
        /// <summary>
        /// The planogram id
        /// </summary>
        [DataMember]
        public Int32? PlanogramId;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message() :
            base(EngineMessagePriority.CleanupPlanograms)
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(Int32 planogramId) :
            base(EngineMessagePriority.CleanupPlanograms)
        {
            this.PlanogramId = planogramId;
        }

        #endregion
    }
}
