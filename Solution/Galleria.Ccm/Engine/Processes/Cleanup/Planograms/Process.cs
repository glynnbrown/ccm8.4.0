﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM800
// V8-29606 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Framework.Engine;
using Galleria.Framework.Engine.Dal.DataTransferObjects;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Planograms
{
    /// <summary>
    /// Performs the process of deleting orphaned planograms
    /// </summary>
    internal partial class Process : EngineMessageProcessBase<Process, Message>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Process(EngineMessageDto messageDto) : base(messageDto) { }

        #endregion
    }
}
