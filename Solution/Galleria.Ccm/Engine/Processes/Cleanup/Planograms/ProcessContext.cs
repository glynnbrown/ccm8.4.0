﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29606 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Ccm.Security;
using Galleria.Framework.Engine;
using Planogram = Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Planograms
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDispose()
        {
            // ensure the planogram is unlocked
            if (this.MessageData.PlanogramId != null)
                Planogram.Package.UnlockPackageById(this.MessageData.PlanogramId, DomainPrincipal.CurrentUserId, Planogram.PackageLockType.System);
        }
        #endregion
    }
}
