﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM803
// V8-29606 : N.Foster
//  Created
#endregion
#region Version History: CCM820
// V8-31509 : N.Foster
//  Ensured that cleanup does not run if workpackage is being processed
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;
using Planogram = Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Planograms
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!CheckWorkpackageStatus(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!EnqueueDeletions(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LockPlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!DeletePlanogram(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineProcessAbortedException)
                    {
                        // ingore this error as we dont need
                        // the engine to reap this message
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        #region Methods

        #region CheckWorkpackageStatus
        /// <summary>
        /// Checks to see if any workpackages are currently being processed
        /// if so, then no metadata calculations are enqueued at this time
        /// </summary>
        private static Boolean CheckWorkpackageStatus(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // for each entity within the solution
                foreach (EntityInfo entity in EntityInfoList.FetchAllEntityInfos())
                {
                    // for each workpackage within the entity
                    foreach (WorkpackageInfo workpackage in WorkpackageInfoList.FetchByEntityId(entity.Id))
                    {
                        // check the status of the workpackage
                        if ((workpackage.ProcessingStatus == WorkpackageProcessingStatusType.Queued) ||
                            (workpackage.ProcessingStatus == WorkpackageProcessingStatusType.Processing))
                        {
                            //if there are no planograms then the workpackage can never process so ignore it
                            if (workpackage.PlanogramCount == 0) continue;

                            //check individual counts incase planogram count is incorrect
                            //(this can happen if the user deletes plans from the workpackage sometimes)
                            if (workpackage.PlanogramProcessingCount +
                                workpackage.PlanogramQueuedCount == 0) continue;

                            // a workpackage is currently being processed
                            // so do not schedule a metadata update at this time
                            return false;
                        }
                    }
                }

                // return succes
                return true;
            }
        }
        #endregion

        #region EnqueueDeletions
        /// <summary>
        /// Enqueues a list of planograms for deletion
        /// </summary>
        private static Boolean EnqueueDeletions(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if this process has a planogram id
                // then do not perform this step as this
                // message represents the deletion of a
                // single planogram
                if (context.MessageData.PlanogramId != null) return true;

                // fetch a list of all planograms that are
                // waiting to be deleted
                IEnumerable<Int32> planogramIds;
                using (IEngineMaintenanceDal dal = context.DalContext.GetDal<IEngineMaintenanceDal>())
                {
                    planogramIds = dal.FetchPlanogramsByDeletionRequired();
                }

                // enqueue a message for each planogram
                lock (Engine.QueueLock)
                {
                    Random rnd = new Random();
                    foreach (Int32 planogramId in planogramIds.OrderBy(i => rnd.NextDouble()))
                    {
                        Engine.EnqueueMessage(new Message(planogramId));
                        if (context.IsAborted) throw new EngineProcessAbortedException();
                    }
                }

                // return false so we do not continue in this process
                return false;
            }
        }
        #endregion

        #region LockPlanogram
        /// <summary>
        /// Attempts to lock a planogram
        /// </summary>
        private static Boolean LockPlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // ensure we have a planogram id
                if (context.MessageData.PlanogramId == null) return false;

                // attempt to lock the specified planogram
                if (Planogram.Package.LockPackageById(context.MessageData.PlanogramId, DomainPrincipal.CurrentUserId, Planogram.PackageLockType.System) == Planogram.PackageLockResult.Success)
                    return true;

                // the planogram could not be locked
                // so return false in order to ensure
                // we take no further action.
                // we dont need to worry about requeuing
                // this message as the maintenance worker
                // will eventually catch-up and schedule
                // another deletion for the
                // planogram anyway
                return false;
            }
        }
        #endregion

        #region DeletePlanogram
        /// <summary>
        /// Attempts to delete the specified planogram
        /// </summary>
        private static Boolean DeletePlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // ensure we have a planogram id
                if (context.MessageData.PlanogramId == null) return false;

                // delete the planogram
                Planogram.Package.DeletePackageById(context.MessageData.PlanogramId, DomainPrincipal.CurrentUserId, Planogram.PackageLockType.System);

                // return success
                return true;
            }
        }
        #endregion

        #endregion
    }
}
