﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Instances
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!CleanupInstances(context)) return;

                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineProcessAbortedException)
                    {
                        // ignore this error as we dont need
                        // the engine to reap this message
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the cleanup of old engine instances
        /// </summary>
        private static Boolean CleanupInstances(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // delete inactive instances
                using (IEngineInstanceDal dal = context.DalContext.GetDal<IEngineInstanceDal>())
                {
                    dal.DeleteInactive();
                }

                // and return success
                return true;
            }
        }
        #endregion
    }
}
