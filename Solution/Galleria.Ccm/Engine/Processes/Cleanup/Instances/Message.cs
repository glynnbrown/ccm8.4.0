﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-27411 : N.Foster
//  Created
#endregion
#endregion

using System.Runtime.Serialization;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Instances
{
    /// <summary>
    /// Defines a message passed to the instance cleanup process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Cleanup.Instances")]
    internal class Message : EngineMessageBase
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message() :
            base(EngineMessagePriority.CleanupInstances)
        {
        }
        #endregion
    }
}
