﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Framework.Engine;
using Galleria.Framework.Engine.Dal.DataTransferObjects;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Instances
{
    /// <summary>
    /// Performs the initialization of the cleanup process
    /// </summary>
    internal partial class Process : EngineMessageProcessBase<Process, Message>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Process(EngineMessageDto messageDto) : base(messageDto) { }
        #endregion
    }
}
