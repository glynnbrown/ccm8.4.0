﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Configuration;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.Cleanup.EngineMetrics
{
    internal partial class Process
    {
        #region Constants
        private const String _metricRetentionPeriod = "Cleanup.MetricRetentionPeriod"; // config constant tht defines how long metrics are kept for, measured in days
        private const Int32 _defaultMetricRetentionPeriod = 7; // the retention period for metrics, measured in days
        #endregion

        #region Properties
        /// <summary>
        /// Indicates the number of days that engine metrics are kept for
        /// </summary>
        public static Int32 MetricRetentionPeriod
        {
            get
            {
                Int32 metricRetentionPeriod;
                Int32.TryParse(ConfigurationManager.AppSettings[_metricRetentionPeriod], out metricRetentionPeriod);
                if (metricRetentionPeriod <= 0) return _defaultMetricRetentionPeriod;
                return metricRetentionPeriod;
            }
        }
        #endregion

        #region Execute
        /// <summary>
        /// Called when this process is executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!CleanupMetrics(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineProcessAbortedException)
                    {
                        // ignore this error as we dont need
                        // the engine to reap this message
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the cleanup of old engine metrics
        /// </summary>
        private static Boolean CleanupMetrics(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // delete the old metrics in batches
                using (IEngineMetricDal dal = context.DalContext.GetDal<IEngineMetricDal>())
                {
                    if (!context.IsAborted)
                    {
                        while (dal.DeleteByTimestamp(DateTime.UtcNow.AddDays(-MetricRetentionPeriod)))
                        {
                            if (context.IsAborted) break;
                        }
                    }
                }

                // and return success
                return true;
            }
        }
        #endregion
    }
}
