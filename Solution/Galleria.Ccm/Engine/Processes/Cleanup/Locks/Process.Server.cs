﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27411 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Configuration;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Security;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;
using Planogram = Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Locks
{
    internal partial class Process
    {
        #region Constants
        private const String _userLockRetentionPeriod = "Cleanup.UserLockRetentionPeriod"; // config constant that defines how long user locks remain alive
        private const Int32 _defaultUserLockRetentionPeriod = 12; // the retention period for user locks, measured in days
        #endregion

        #region Properties
        /// <summary>
        /// Indicates the number of days that engine metrics are kept for
        /// </summary>
        public static Int32 UserLockRetentionPeriod
        {
            get
            {
                Int32 userLockRetentionPeriod;
                Int32.TryParse(ConfigurationManager.AppSettings[_userLockRetentionPeriod], out userLockRetentionPeriod);
                if (userLockRetentionPeriod <= 0) return _defaultUserLockRetentionPeriod;
                return userLockRetentionPeriod;
            }
        }
        #endregion

        #region Execute
        /// <summary>
        /// Called when this process is executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!CleanupLocks(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineProcessAbortedException)
                    {
                        // ignore this error as we dont need
                        // the engine to reap this messgae
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Performs the cleanup of old engine metrics
        /// </summary>
        private static Boolean CleanupLocks(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // delete old locks in batches
                using (IPlanogramLockDal dal = context.DalContext.GetDal<IPlanogramLockDal>())
                {
                    // system locks can be cleaned up
                    // based on the engine reaper period
                    // which is measured in minutes
                    if (!context.IsAborted)
                    {
                        while (dal.DeleteByLockTypeDateLocked((Int32)DomainPrincipal.CurrentUserId, (Byte)Planogram.PackageLockType.System, DateTime.UtcNow.AddMinutes(-(Engine.ReaperTimeout + 1))))
                        {
                            if (context.IsAborted) break;
                        }
                    }

                    // user locks are cleaned up based on
                    // a time period measured in hours
                    if (!context.IsAborted)
                    {
                        while (dal.DeleteByLockTypeDateLocked((Int32)DomainPrincipal.CurrentUserId, (Byte)Planogram.PackageLockType.User, DateTime.UtcNow.AddHours(-UserLockRetentionPeriod)))
                        {
                            if (context.IsAborted) break;
                        }
                    }
                }

                // and return success
                return true;
            }
        }
        #endregion
    }
}
