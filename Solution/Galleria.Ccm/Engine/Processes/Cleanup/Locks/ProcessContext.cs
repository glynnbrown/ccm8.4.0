﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25787 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Cleanup.Locks
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion
    }
}
