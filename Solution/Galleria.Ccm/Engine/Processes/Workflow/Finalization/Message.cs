﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Ccm.Model;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workflow.Finalization
{
    /// <summary>
    /// Defines a message passed to the workflow finalization process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Finalization")]
    internal class Message : EngineMessageBase
    {
        #region Nested Classes

        #region Entity
        /// <summary>
        /// Defines the entity data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Finalization.Entity")]
        internal class EntityDc
        {
            #region Properties
            /// <summary>
            /// The workpackage priority
            /// </summary>
            [DataMember]
            public Int32 Id;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Workpackage.Finalization.Message.EntityDc entity)
            {
                this.Id = entity.Id;
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Workflow.Execution.Message.EntityDc entity)
            {
                this.Id = entity.Id;
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Workflow.Initialization.Message.EntityDc entity)
            {
                this.Id = entity.Id;
            }

            #endregion
        }
        #endregion

        #region Workpackage
        /// <summary>
        /// Defines the workpackage data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Finalization.Workpackage")]
        internal class WorkpackageDc
        {
            #region Properties
            /// <summary>
            /// The workpackage id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workpackage name
            /// </summary>
            [DataMember]
            public String Name;

            /// <summary>
            /// The workpackage priority
            /// </summary>
            [DataMember]
            public Byte Priority;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Workpackage.Finalization.Message.WorkpackageDc workpackage)
            {
                this.Id = workpackage.Id;
                this.Name = workpackage.Name;
                this.Priority = workpackage.Priority;
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Workflow.Execution.Message.WorkpackageDc workpackage)
            {
                this.Id = workpackage.Id;
                this.Name = workpackage.Name;
                this.Priority = workpackage.Priority;
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Workflow.Initialization.Message.WorkpackageDc workpackage)
            {
                this.Id = workpackage.Id;
                this.Name = workpackage.Name;
                this.Priority = workpackage.Priority;
            }

            #endregion
        }
        #endregion

        #region Planogram
        /// <summary>
        /// Defines the planogram data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Finalization.Planogram")]
        internal class PlanogramDc
        {
            #region Properties
            /// <summary>
            /// The planogram id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The planogram processing status
            /// </summary>
            [DataMember]
            public Model.ProcessingStatus Status;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public PlanogramDc(PlanogramInfo planogram, ProcessingStatus status)
            {
                this.Id = planogram.Id;
                this.Status = status;
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public PlanogramDc(Workflow.Execution.Message.PlanogramDc planogram, ProcessingStatus status)
            {
                this.Id = planogram.Id;
                this.Status = status;
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public PlanogramDc(Workflow.Initialization.Message.PlanogramDc planogram, ProcessingStatus status)
            {
                this.Id = planogram.Id;
                this.Status = status;
            }

            #endregion
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// The entity details
        /// </summary>
        [DataMember]
        public EntityDc Entity;

        /// <summary>
        /// The workpackage details
        /// </summary>
        [DataMember]
        public WorkpackageDc Workpackage;

        /// <summary>
        /// The planogram details
        /// </summary>
        [DataMember]
        public PlanogramDc Planogram;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workpackage.Finalization.Message.EntityDc entity,
            Workpackage.Finalization.Message.WorkpackageDc workpackage,
            PlanogramInfo planogram,
            ProcessingStatus status) :
            base(EngineMessagePriority.WorkflowFinalization)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage);
            this.Planogram = new PlanogramDc(planogram, status);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workflow.Execution.Message.EntityDc entity,
            Workflow.Execution.Message.WorkpackageDc workpackage,
            Workflow.Execution.Message.PlanogramDc planogram,
            ProcessingStatus status) :
            base(EngineMessagePriority.WorkflowFinalization)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage);
            this.Planogram = new PlanogramDc(planogram, status);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workflow.Initialization.Message.EntityDc entity,
            Workflow.Initialization.Message.WorkpackageDc workpackage,
            Workflow.Initialization.Message.PlanogramDc planogram,
            ProcessingStatus status) :
            base(EngineMessagePriority.WorkflowFinalization)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage);
            this.Planogram = new PlanogramDc(planogram, status);
        }

        #endregion
    }
}
