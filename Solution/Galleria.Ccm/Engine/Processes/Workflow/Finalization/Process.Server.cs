﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Altered names of existing properties in PlanogramProcessingStatusDto
//  Now updates AutomationStatus date and description
#endregion
#region Version History: CCM810
// V8-30079 : L.Ineson
//  Added set of plan status to complete with warnings if required.
// V8-29173 : N.Haywood
//  Added fail task when errors are found in a planogram
#endregion
#region Version History: CCM820
// GAF-31367 : N.Foster
//  Moved scheduling of metadata and validation jobs to workpackage finalization
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Engine.Exceptions;
using Galleria.Ccm.Model;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.Workflow.Finalization
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!FinalizePlanogram(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is EngineProcessAbortedException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is WorkpackageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else
                    {
                        // throw a workpackage exception
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            ex);
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Finalizes this planogram
        /// </summary>
        private static Boolean FinalizePlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we got this far then we
                // have locked the planogram,
                // so we should be safe to update
                // the planogram status to the
                // desired planogram status

                //determine the plan status 
                ProcessingStatus planStatus = context.MessageData.Planogram.Status;
                if (planStatus != ProcessingStatus.Failed)
                {
                    //assume the plan completed successfully
                    planStatus = ProcessingStatus.Complete;

                    //if the plan has any warnings then log this.
                    PlanogramInfo planInfo
                            = PlanogramInfoList.FetchByIds(new List<Int32> { context.MessageData.Planogram.Id }).FirstOrDefault();

                    if (planInfo != null && planInfo.MetaNoOfErrors > 0)
                    {
                        planStatus = ProcessingStatus.Failed;
                    }
                    else if (planInfo != null && planInfo.MetaNoOfWarnings > 0)
                    {
                        planStatus = ProcessingStatus.CompleteWithWarnings;
                    }
                }

                try
                {
                    using (IWorkpackagePlanogramProcessingStatusDal dal = context.DalContext.GetDal<IWorkpackagePlanogramProcessingStatusDal>())
                    {
                        WorkpackagePlanogramProcessingStatusDto dto = dal.FetchByPlanogramIdWorkpackageId(context.MessageData.Planogram.Id, context.MessageData.Workpackage.Id);
                        dto.AutomationStatus = (Byte)planStatus;
                        dto.AutomationStatusDescription = ProcessingStatusTypeHelper.FriendlyNames[context.MessageData.Planogram.Status];
                        dto.AutomationProgressCurrent = dto.AutomationProgressMax;
                        dto.AutomationDateLastUpdated = DateTime.UtcNow;
                        dal.Update(dto);
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    // the planogram no longer exists
                    // so there is nothing left for us to do
                    return false;
                }
                catch (ConcurrencyException)
                {
                    // a concurrency exception occurred so requeue the message
                    context.RequeueMessage();

                    // return
                    return false;
                }

                // indicate we were successful
                return true;
            }
        }

        #endregion
    }
}
