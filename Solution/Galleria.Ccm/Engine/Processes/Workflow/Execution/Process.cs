﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Framework.Engine;
using Galleria.Framework.Engine.Dal.DataTransferObjects;

namespace Galleria.Ccm.Engine.Processes.Workflow.Execution
{
    /// <summary>
    /// Process for executing the of a workpackage
    /// </summary>
    internal partial class Process : EngineMessageProcessBase<Process, Message>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Process(EngineMessageDto messageDto) : base(messageDto) { }
        #endregion
    }
}
