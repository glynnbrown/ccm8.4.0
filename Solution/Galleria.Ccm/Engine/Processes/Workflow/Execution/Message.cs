﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Engine;
using Planograms = Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Workflow.Execution
{
    /// <summary>
    /// Defines a message passed to the workflow process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Execution")]
    internal class Message : EngineMessageBase
    {
        #region Nested Classes

        #region Entity
        /// <summary>
        /// Defines the entity data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Execution.Entity")]
        internal class EntityDc
        {
            #region Properties
            /// <summary>
            /// The entity id
            /// </summary>
            [DataMember]
            public Int32 Id;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Workflow.Initialization.Message.EntityDc entity)
            {
                this.Id = entity.Id;
            }

            #endregion
        }
        #endregion

        #region Workpackage
        /// <summary>
        /// Defines the workpackage data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Execution.Workpackage")]
        internal class WorkpackageDc
        {
            #region Properties
            /// <summary>
            /// The workpackage id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workpackage rowversion
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The workpackage name
            /// </summary>
            [DataMember]
            public String Name;

            /// <summary>
            /// The workpackage priority
            /// </summary>
            [DataMember]
            public Byte Priority;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Workflow.Initialization.Message.WorkpackageDc workpackage)
            {
                this.Id = workpackage.Id;
                this.RowVersion = workpackage.RowVersion;
                this.Name = workpackage.Name;
                this.Priority = workpackage.Priority;
            }

            #endregion
        }
        #endregion

        #region Workflow
        /// <summary>
        /// Defines the workflow data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Execution.Workflow")]
        internal class WorkflowDc
        {
            #region Properties
            /// <summary>
            /// The workflow id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workflow rowversion
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The workflow name
            /// </summary>
            [DataMember]
            public String Name;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkflowDc(Workflow.Initialization.Message.WorkflowDc workflow)
            {
                this.Id = workflow.Id;
                this.RowVersion = workflow.RowVersion;
                this.Name = workflow.Name;
            }

            #endregion
        }
        #endregion

        #region Planogram
        /// <summary>
        /// Defines the planogram data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Execution.Planogram")]
        internal class PlanogramDc
        {
            #region Properties
            /// <summary>
            /// The planogram id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The planogram rowversion
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The planogram name
            /// </summary>
            [DataMember]
            public String Name;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public PlanogramDc(Planograms.Package planogram)
            {
                this.Id = (Int32)planogram.Id;
                this.RowVersion = planogram.RowVersion;
                this.Name = planogram.Name;
            }

            #endregion
        }
        #endregion

        #region SequencedTask
        /// <summary>
        /// Defines the sequenced task information for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Execution.SequencedTask")]
        internal class SequencedTask
        {
            #region Properties
            /// <summary>
            /// The task sequence id
            /// </summary>
            [DataMember]
            public Byte SequenceId;

            /// <summary>
            /// The task message id
            /// </summary>
            [DataMember]
            public Int64 MessageId;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public SequencedTask(Workflow.Initialization.Message.SequencedTask sequencedTask)
            {
                this.SequenceId = sequencedTask.SequenceId;
                this.MessageId = sequencedTask.MessageId;
            }

            #endregion
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// The entity
        /// </summary>
        [DataMember]
        public EntityDc Entity;

        /// <summary>
        /// The workpackage
        /// </summary>
        [DataMember]
        public WorkpackageDc Workpackage;

        /// <summary>
        /// The worflow
        /// </summary>
        [DataMember]
        public WorkflowDc Workflow;

        /// <summary>
        /// The planogram
        /// </summary>
        [DataMember]
        public PlanogramDc Planogram;

        /// <summary>
        /// The workpackage finalization message id
        /// </summary>
        [DataMember]
        public Int64 WorkpackageFinalizationMessageId;

        /// <summary>
        /// The list of sequenced tasks
        /// </summary>
        [DataMember]
        public List<SequencedTask> SequencedTasks;

        /// <summary>
        /// The sequence id of the first task to process
        /// </summary>
        [DataMember]
        public Byte SequenceId;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workflow.Initialization.Message.EntityDc entity,
            Workflow.Initialization.Message.WorkpackageDc workpackage,
            Workflow.Initialization.Message.WorkflowDc workflow,
            Planograms.Package planogram,
            Int64 workpackageFinalizationMessageId,
            IEnumerable<Workflow.Initialization.Message.SequencedTask> sequencedTasks,
            Byte sequenceId) :
            base(workpackage.Priority)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage);
            this.Workflow = new WorkflowDc(workflow);
            this.Planogram = new PlanogramDc(planogram);
            this.SequenceId = sequenceId;
            this.WorkpackageFinalizationMessageId = workpackageFinalizationMessageId;
            this.SequencedTasks = new List<SequencedTask>();
            foreach (var sequencedTask in sequencedTasks)
            {
                this.SequencedTasks.Add(new SequencedTask(sequencedTask));
            }
        }

        #endregion
    }
}
