﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Ccm.Model;
using Galleria.Framework.Engine;
using Planograms = Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Security;
using System;

namespace Galleria.Ccm.Engine.Processes.Workflow.Execution
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Fields
        private Model.Workpackage _workpackage; // holds the workpackage details
        private Model.Workflow _workflow; // holds the workflow details
        private Planograms.Package _package; // the planogram package
        private WorkpackagePlanogramDebugList _debugPlanograms; // holds the debug planogram details
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the workpackage details
        /// </summary>
        public Model.Workpackage Workpackage
        {
            get { return _workpackage; }
            set { _workpackage = value; }
        }

        /// <summary>
        /// Gets or sets the workflow details
        /// </summary>
        public Model.Workflow Workflow
        {
            get { return _workflow; }
            set { _workflow = value; }
        }

        /// <summary>
        /// Gets or sets the planogram package
        /// </summary>
        public Planograms.Package Package
        {
            get { return _package; }
            set { _package = value; }
        }

        /// <summary>
        /// Returns the first planogram in the package
        /// </summary>
        public Planograms.Planogram Planogram
        {
            get { return (_package == null) || (_package.Planograms.Count == 0) ? null : _package.Planograms[0]; }
        }

        /// <summary>
        /// Gets or sets the debug planogram details
        /// </summary>
        public WorkpackagePlanogramDebugList DebugPlanograms
        {
            get { return _debugPlanograms; }
            set { _debugPlanograms = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        protected override void OnDispose()
        {
            // dispose of the source package
            if (_package != null) _package.Dispose();
            _package = null;
        }
        #endregion
    }
}
