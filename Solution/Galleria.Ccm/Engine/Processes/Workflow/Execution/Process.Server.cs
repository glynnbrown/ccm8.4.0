﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Set Package.FetchById() to be system locktype fetch
//  Now updates planogram and workpackage status description and date when incrementing progress
#endregion
#region Version History: CCM803
// V8-29830 : N.Foster
//  Added checks for empty workflows
// V8-29731 : N.Foster
//  Ensure planograms are saved if a task throws an exception
#endregion
#region Version History: CCM811
// V8-30561 : N.Foster
//  Ensure date last modified is not updated when the planogram is saved
#endregion
#region Version History: CCM830
// V8-33027 : D.Pleasance
//  Amended ExecuteTask() so that the task is disposed before creating the debug planogram.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Engine.Exceptions;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;
using Planograms = Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Workflow.Execution
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!LoadWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ValidateWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LoadWorkflow(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ValidateWorkflow(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LockPlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LoadPlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ValidatePlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LoadDebugPlanograms(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ExecuteWorkflow(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DeadlockException)
                    {
                        // a deadlock exception occurred
                        // this is typically due to a deadlock when saving
                        // the planogram so requeue the message to try again
                        this.RequeueMessage();
                    }
                    else if (baseException is TaskRetryException)
                    {
                        // requeue the message to try again
                        this.RequeueMessage();
                    }
                    else if (baseException is EngineRequeueMessageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is EngineProcessAbortedException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is WorkpackageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else
                    {
                        // an unexpected exception occurred when
                        // processing the planogram workflow
                        // so we enqueue a planogram finalization
                        // message in order to fail the plan
                        Engine.EnqueueMessage(
                            new Workflow.Finalization.Message(
                                this.MessageData.Entity,
                                this.MessageData.Workpackage,
                                this.MessageData.Planogram,
                                ProcessingStatus.Failed),
                            this.MessageData.Id);

                        // throw a workpackage exception
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            ex);
                    }
                }
            }
        }
        #endregion

        #region Methods

        #region LoadWorkpackage
        /// <summary>
        /// Loads the desired workpackage
        /// </summary>
        private static Boolean LoadWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    // load the workpackage, but only for the planogram
                    // that we are actually interested in
                    context.Workpackage = Model.Workpackage.FetchByWorkpackageIdDestinationPlanogramId(
                        context.MessageData.Workpackage.Id,
                        context.MessageData.Planogram.Id);
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            Resources.Language.Message.Engine_WorkpackageDeletedException);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ValidateWorkpackage
        /// <summary>
        /// Validates the workpackage
        /// </summary>
        private static Boolean ValidateWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // validate that the workpackage has not
                // changed since we started this process
                if (context.Workpackage.RowVersion != context.MessageData.Workpackage.RowVersion)
                    throw new WorkpackageException(
                        context.Workpackage,
                        Resources.Language.Message.Engine_WorkpackageInvalidException);

                // return success
                return true;
            }
        }
        #endregion

        #region LoadWorkflow
        /// <summary>
        /// Loads the workflow
        /// </summary>
        public static Boolean LoadWorkflow(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Workflow = Model.Workflow.FetchById(context.MessageData.Workflow.Id);
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            Resources.Language.Message.Engine_WorkflowDeletedException,
                            context.MessageData.Workflow.Name);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ValidateWorkflow
        /// <summary>
        /// Validates the workflow
        /// </summary>
        public static Boolean ValidateWorkflow(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // verify that the workflow has not
                // changed since we started this process
                if (context.Workflow.RowVersion != context.MessageData.Workflow.RowVersion)
                    throw new WorkpackageException(
                        context.MessageData.Entity,
                        context.MessageData.Workpackage,
                        Resources.Language.Message.Engine_WorkflowInvalidException,
                        context.MessageData.Workflow.Name);

                // return success
                return true;
            }
        }
        #endregion

        #region LockPlanogram
        /// <summary>
        /// Locks the planogram
        /// </summary>
        private static Boolean LockPlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to lock the specified planogram
                if (Planograms.Package.LockPackageById(context.MessageData.Planogram.Id, DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System) == Planograms.PackageLockResult.Success)
                    return true;

                // the planogram could not be locked
                // we therefore reap this message in
                // order to try again at a later date
                context.RequeueMessage(Engine.MessageRequeueDelay);

                // return that we were unsuccessful
                return false;
            }
        }
        #endregion

        #region LoadPlanogram
        /// <summary>
        /// Loads the planogram
        /// </summary>
        private static Boolean LoadPlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Package = Planograms.Package.FetchById(
                        context.MessageData.Planogram.Id,
                        DomainPrincipal.CurrentUserId,
                        Planograms.PackageLockType.System);
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new PlanogramDeletedException(context.MessageData.Planogram);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ValidatePlanogram
        /// <summary>
        /// Validates the output planogram
        /// </summary>
        private static Boolean ValidatePlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // validate that the output planogram has not changed
                // since we started this process
                if (context.Package.RowVersion != context.MessageData.Planogram.RowVersion)
                    throw new PlanogramInvalidException(context.Planogram);

                // return success
                return true;
            }
        }
        #endregion

        #region LoadDebugPlanograms
        /// <summary>
        /// Loads debug planogram details
        /// into the process context
        /// </summary>
        private static Boolean LoadDebugPlanograms(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch the details of all the existing planograms
                context.DebugPlanograms = WorkpackagePlanogramDebugList.FetchBySourcePlanogramId((Int32)context.Planogram.Id);

                // and return success
                return true;
            }
        }
        #endregion

        #region ExecuteWorkflow
        /// <summary>
        /// Executes the worklfow against the planogram
        /// </summary>
        private static Boolean ExecuteWorkflow(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // first, we need to find the sequence id of 
                // the last task in the workflow
                Byte? finalSequenceId = context.Workflow.Tasks.Count > 0 ? context.Workflow.Tasks.Max(t => t.SequenceId) : (Byte?)null;

                // if the final sequence id is null then
                // we have little to do, as this indicates
                // a workflow that contains no tasks
                List<Message.SequencedTask> sequencedTasks = new List<Message.SequencedTask>();
                if (finalSequenceId != null)
                {
                    // now, we may not execute the full workflow
                    // as we may have sequenced tasks that need
                    // to be performed, so identify the next
                    // sequenced task that we will need to process
                    // we also determine the last sequence in the
                    // workflow that we are going to execute as
                    // part of this execution
                    foreach (var sequencedTask in context.MessageData.SequencedTasks.OrderBy(i => i.SequenceId))
                    {
                        if (sequencedTask.SequenceId > context.MessageData.SequenceId)
                        {
                            sequencedTasks.Add(sequencedTask);
                        }
                    }

                    // determine the last step in the sequence
                    // that we are expecting to process
                    Byte lastSequenceId = (Byte)finalSequenceId;
                    if (sequencedTasks.Count > 0) lastSequenceId = Math.Min(lastSequenceId, (Byte)(sequencedTasks[0].SequenceId - 1));

                    // we have now determined which tasks we
                    // are going to process in this sequence
                    // we have also identified if we have a sequenced
                    // task in the middle of things that needs processing

                    // now we can finally start to process our workflow
                    // processing up to the calcualted last task in the sequenced
                    while (context.MessageData.SequenceId <= lastSequenceId)
                    {
                        // execute the next task in the sequence
                        if (!ExecuteTask(context, context.MessageData.SequenceId, lastSequenceId, (Byte)finalSequenceId)) return false;

                        // verify the process has not been aborted
                        if (context.IsAborted) throw new EngineProcessAbortedException();

                        // increase the sequence number
                        context.MessageData.SequenceId++;
                    }

                    // attempt to save the planogram
                    // note that the planogram id could
                    // change so we need to update
                    // the debug planogram details
                    try
                    {
                        // save the package
                        Int32 oldPlanogramId = (Int32)context.Planogram.Id;
                        context.Package = context.Package.Save(false, false);
                        Int32 newPlanogramId = (Int32)context.Planogram.Id;

                        // resolve the id change
                        context.Workpackage.Planograms.ResolveId(oldPlanogramId, newPlanogramId);
                        context.DebugPlanograms.ResolveId(oldPlanogramId, newPlanogramId);
                    }
                    catch (DataPortalException ex)
                    {
                        Exception baseException = ex.GetBaseException();
                        if (baseException is ConcurrencyException)
                        {
                            throw new PlanogramInvalidException(context.Planogram);
                        }
                        else
                        {
                            throw;
                        }
                    }

                    // save the workpackage debug planogram information
                    context.DebugPlanograms = context.DebugPlanograms.Save();

                    // update the message data with the new row version
                    context.MessageData.Planogram.RowVersion = context.Package.RowVersion;
                }

                // lock the queue
                lock (Engine.QueueLock)
                {
                    // start a transaction
                    context.DalContext.Begin();

                    // We are now at the point that we need to enqueue
                    // the next message in the sequence. The type of message
                    // that we are going to enqueue depends upon whether there
                    // are any further sequenced tasks to process or not
                    if (sequencedTasks.Count == 0)
                    {
                        // there are no further sequenced tasks to process
                        // so that means we have finished our workflow
                        // we therefore enqueue a planogram finalization message
                        // ensuring that the workpackage finalization message
                        // is dependent on this new message
                        Int64 messageId = Engine.EnqueueMessage(
                            context.DalContext,
                            new Workflow.Finalization.Message(
                                context.MessageData.Entity,
                                context.MessageData.Workpackage,
                                context.MessageData.Planogram,
                                Model.ProcessingStatus.Complete));

                        // add a dependency for the workpackage finalization step
                        Engine.AddDependencies(
                            context.DalContext,
                            context.MessageData.WorkpackageFinalizationMessageId,
                            new List<Int64>() { messageId });
                    }
                    else
                    {
                        // addition sequenced tasks exist
                        // so check when these tasks are scheduled
                        // within the workflow
                        if (sequencedTasks[0].SequenceId > finalSequenceId)
                        {
                            // the next sequenced task is after the final
                            // step in the workflow. Enqueue a planogram
                            // finalization step
                            Int64 messageId = Engine.EnqueueMessage(
                                context.DalContext,
                                new Workflow.Finalization.Message(
                                    context.MessageData.Entity,
                                    context.MessageData.Workpackage,
                                    context.MessageData.Planogram,
                                    Model.ProcessingStatus.Complete));

                            // ensure this planogram finalization step
                            // is dependent on the sequenced task
                            Engine.AddDependencies(
                                context.DalContext,
                                messageId,
                                new List<Int64>() { sequencedTasks[0].MessageId });

                            // ensure that workpackage finalization
                            // is dependent on this planogram finalization step
                            Engine.AddDependencies(
                                context.DalContext,
                                context.MessageData.WorkpackageFinalizationMessageId,
                                new List<Int64>() { messageId });
                        }
                        else
                        {
                            // the next sequenced task sits within the middle of
                            // the workflow, so we enqueue another workflow execution
                            // step to continue processing after this sequence step
                            Int64 messageId = Engine.EnqueueMessage(
                                context.DalContext,
                                context.MessageData);

                            // ensure that this workflow execution is dependent
                            // on the workflow sequence step
                            Engine.AddDependencies(
                                context.DalContext,
                                messageId,
                                new List<Int64>() { sequencedTasks[0].MessageId });

                            // if there are no more sequence steps then ensure
                            // that workpackage finalization is dependent on this step
                            if (sequencedTasks.Count == 1)
                            {
                                Engine.AddDependencies(
                                    context.DalContext,
                                    context.MessageData.WorkpackageFinalizationMessageId,
                                    new List<Int64>() { messageId });
                            }
                            else
                            {
                                Engine.AddDependencies(
                                    context.DalContext,
                                    sequencedTasks[1].MessageId,
                                    new List<Int64>() { messageId });
                            }
                        }
                    }

                    // commit the transaction
                    context.DalContext.Commit();
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ExecuteTask
        /// <summary>
        /// Executes a task against the planogram
        /// </summary>
        private static Boolean ExecuteTask(ProcessContext context, Byte sequenceId, Byte lastSequenceId, Byte maxSequenceId)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // get the task that we are currently
                // executing from the workflow
                WorkflowTask workflowTask = context.Workflow.Tasks.First(t => t.SequenceId == sequenceId);
                Boolean captureDebug = false;

                // create an instance of the task
                using (TaskBase task = workflowTask.CreateTask())
                {
                    #region Execute Task
                    captureDebug = task.CaptureDebug;

                    if (task == null)
                    {
                        // the task could not be created as it
                        // is not registered within the container
                        // this could be due to the task assembly
                        // being out-of-date on this server, at
                        // which point we requeue this message
                        // so that it is processed on another node
                        context.RequeueMessage(Engine.MessageRequeueDelay);

                        // and return false to prevent
                        // any further processing
                        return false;
                    }

                    // get the initial planogram id
                    Int32 oldPlanogramId = (Int32)context.Planogram.Id;

                    // locate the planogram within the workpackage
                    WorkpackagePlanogram workpackagePlanogram = context.Workpackage.Planograms.FirstOrDefault(p => p.DestinationPlanogramId == oldPlanogramId);
                    if (workpackagePlanogram == null)
                        throw new WorkpackageException(
                            context.Workpackage,
                            Resources.Language.Message.Engine_WorkpackageInvalidException);

                    // now execute our task
                    using (TaskContext taskContext = new TaskContext(
                        context.Workpackage,
                        workpackagePlanogram,
                        context.Workflow,
                        workflowTask,
                        context.Package,
                        context.Planogram))
                    {
                        task.ExecuteTask(taskContext);
                    }

                    // get the new planogram id
                    Int32 newPlanogramId = (Int32)context.Planogram.Id;

                    // resolve changes to the planogram id made by the task
                    context.Workpackage.Planograms.ResolveId(oldPlanogramId, newPlanogramId);
                    context.DebugPlanograms.ResolveId(oldPlanogramId, newPlanogramId);

                    #endregion
                }

                #region Generate Debug Planogram
                // determine if we already have debug
                // information for this planogram task
                WorkpackagePlanogramDebug debug = context.DebugPlanograms.FirstOrDefault(item => context.Planogram.Id.Equals(item.SourcePlanogramId) && workflowTask.Id.Equals(item.WorkflowTaskId));
                if (debug == null)
                {
                    // no debug planogram exists for this workflow task
                    // so create and link a new debug planogram if required
                    if (sequenceId == maxSequenceId)
                    {
                        // this is the last step in the workflow
                        // so the debug planogram is the output planogram
                        context.DebugPlanograms.Add(context.Planogram, workflowTask, context.Planogram);
                    }
                    else if (captureDebug)
                    {
                        // create and link a new debug planogram
                        Planograms.Package debugPackage = context.Package.Copy();
                        debugPackage.Save(false);
                        debugPackage.Dispose();
                        context.DebugPlanograms.Add(context.Package, workflowTask, debugPackage);
                    }
                }
                else
                {
                    // a debug planogram already exists for this workflow task
                    // try to re-use the debug planogram if possible
                    if (sequenceId == maxSequenceId)
                    {
                        // this is the last step in the workflow
                        // so the debug planogram is the output planogram
                        debug.DebugPlanogramId = (Int32)context.Planogram.Id;
                    }
                    else if (captureDebug)
                    {
                        // a debug planogram already exists
                        // if the current debug planogram is the output planogram
                        // then it means we need to create a new debug planogram
                        Planograms.Package debugPackage = context.Package;
                        if (debug.DebugPlanogramId.Equals(context.Planogram.Id))
                        {
                            debugPackage = context.Package.Copy();
                            debugPackage.Save(false);
                            debugPackage.Dispose();
                        }
                        else
                        {
                            // debug planogram already exists so attempt
                            // to open it and re-use it for this task
                            if (Planograms.Package.LockPackageById(debug.DebugPlanogramId, DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System) == Planograms.PackageLockResult.Success)
                            {
                                using (debugPackage = Planograms.Package.FetchById(debug.DebugPlanogramId, DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System))
                                {
                                    debugPackage.Planograms.Clear();
                                    debugPackage.Planograms.Add(context.Planogram.Copy());
                                    debugPackage.Name = context.Planogram.Name;
                                    debugPackage = debugPackage.Save(false);
                                    debugPackage.Dispose();
                                }
                            }
                        }

                        // assign the debug planogram to the debug list
                        debug.DebugPlanogramId = (Int32)debugPackage.Planograms[0].Id;
                    }
                }
                #endregion

                #region Update Progress
                // find the next task in the sequence
                WorkflowTask nextWorkflowTask = null;
                if (sequenceId < maxSequenceId)
                {
                    nextWorkflowTask = context.Workflow.Tasks.First(t => t.SequenceId == (byte)(sequenceId + 1));
                }
                String nextStatusDescription = nextWorkflowTask != null ? nextWorkflowTask.Details.Name : Ccm.Resources.Language.Message.ProcessingStatus_Automation_FinalizingPlanogram;

                // increment the planogram progress
                WorkpackagePlanogramProcessingStatus.IncrementAutomationProgress( context.MessageData.Planogram.Id, context.MessageData.Workpackage.Id, nextStatusDescription, DateTime.UtcNow);

                // increment the workpackage progress
                WorkpackageProcessingStatus.IncrementProgress(context.MessageData.Workpackage.Id, "TODO", DateTime.UtcNow);

                #endregion

                // return success
                return true;
            }
        }
        #endregion

        #endregion
    }
}
