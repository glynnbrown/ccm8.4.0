﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Engine.Messages;

namespace Galleria.Ccm.Engine.Processes.Workflow
{
    /// <summary>
    /// Process for executing the of a workpackage
    /// </summary>
    internal partial class Process : MessageProcessBase<Process, WorkflowMessage>
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Process(MessageDto processDto)
            : base(processDto)
        {
        }
        #endregion
    }
}
