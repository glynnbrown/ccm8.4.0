﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Threading;

namespace Galleria.Ccm.Engine.Processes.Workflow
{
    internal partial class Process
    {
        #region Methods
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            Thread.Sleep(10000);
            Console.WriteLine("Workflow Processed");
        }
        #endregion
    }
}
