﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
// V8-27411 : M.Pettit
//  Set Package.FetchById() to be system locktype fetch
//  Altered names of existing properties in PlanogramProcessingStatusDto
//  Now updates AutomationStatus and WorkpackageStatus date and description
#endregion
#region Version History: CCM803
// V8-29725 : N.Foster
//  Clear planogram event log when a plan is run through the engine
#endregion
#region Version History: CCM810
// V8-29792 : N.Foster
//  Fixed issue where metadata calculation was changing source planogram row version
// V8-29842 : A.Probyn
//  Added new CopyPlanogramLinks stage to the initilization.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Engine.Exceptions;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;
using Planograms = Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Workflow.Initialization
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!LoadWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ValidateWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LockPlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!SetPlanogramStatus(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LoadPlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LockSourcePlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LoadSourcePlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!PopulatePlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!SavePlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!CopyPlanogramLinks(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ResetDebugPlanograms(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!IncrementProgress(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ExecuteWorkflow(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DeadlockException)
                    {
                        // a deadlock exception occurred
                        // this is typically due to a deadlock when saving
                        // the planogram so requeue the message to try again
                        this.RequeueMessage();
                    }
                    else if (baseException is EngineRequeueMessageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is EngineProcessAbortedException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is WorkpackageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else
                    {
                        // an unexpected exception occurred
                        // when executing this planogrm
                        // so we enqueue a planogram finalization
                        // step in order to fail the plan
                        Engine.EnqueueMessage(
                            new Workflow.Finalization.Message(
                                this.MessageData.Entity,
                                this.MessageData.Workpackage,
                                this.MessageData.Planogram,
                                ProcessingStatus.Failed),
                            this.MessageData.Id);

                        // throw a workpackage exception
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            ex);
                    }
                }
            }
        }
        #endregion

        #region Methods

        #region LoadWorkpackage
        /// <summary>
        /// Loads the desired workpackage
        /// </summary>
        private static Boolean LoadWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Workpackage = Model.WorkpackageInfo.FetchById(context.MessageData.Workpackage.Id);
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            Resources.Language.Message.Engine_WorkpackageDeletedException);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ValidateWorkpackage
        /// <summary>
        /// Validates the workpackage
        /// </summary>
        private static Boolean ValidateWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if the workpackage has changed since
                // we started this process, then throw
                // an exception now
                if (context.Workpackage.RowVersion != context.MessageData.Workpackage.RowVersion)
                    throw new WorkpackageException(
                        context.MessageData.Entity,
                        context.MessageData.Workpackage,
                        Resources.Language.Message.Engine_WorkpackageInvalidException);

                // return successful
                return true;
            }
        }
        #endregion

        #region LockPlanogram
        /// <summary>
        /// Locks the planogram
        /// </summary>
        private static Boolean LockPlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to lock the specified planogram
                if (Planograms.Package.LockPackageById(context.MessageData.Planogram.Id, DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System) == Planograms.PackageLockResult.Success)
                    return true;

                // the planogram could not be locked
                // we therefore requeue this message in
                // order to try again at a later date
                context.RequeueMessage(Engine.MessageRequeueDelay);

                // return that we were unsuccessful
                return false;
            }
        }
        #endregion

        #region SetPlanogramStatus
        /// <summary>
        /// Attempts to set the planogram status
        /// </summary>
        private static Boolean SetPlanogramStatus(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    using (IWorkpackagePlanogramProcessingStatusDal dal = context.DalContext.GetDal<IWorkpackagePlanogramProcessingStatusDal>())
                    {
                        WorkpackagePlanogramProcessingStatusDto dto = dal.FetchByPlanogramIdWorkpackageId(context.MessageData.Planogram.Id, context.MessageData.Workpackage.Id);
                        dto.AutomationStatus = (Byte)ProcessingStatus.Processing;
                        dto.AutomationStatusDescription = ProcessingStatusTypeHelper.FriendlyNames[ProcessingStatus.Processing];
                        dto.AutomationProgressMax = context.MessageData.Workflow.TaskCount + 1;
                        dto.AutomationProgressCurrent = 0;
                        //reset the date started here so that queued jobs, once processing has started, 
                        //have a realistic est. time remaining
                        dto.AutomationDateStarted = DateTime.UtcNow;
                        dto.AutomationDateLastUpdated = DateTime.UtcNow;
                        dal.Update(dto);
                    }
                }
                catch (DtoDoesNotExistException)
                {
                    // the planogram no longer exists
                    // so throw an exception
                    throw new PlanogramDeletedException(context.MessageData.Planogram);
                }
                catch (ConcurrencyException)
                {
                    context.RequeueMessage();
                }

                // return success
                return true;
            }
        }
        #endregion

        #region LoadPlanogram
        /// <summary>
        /// Loads the planogram
        /// </summary>
        private static Boolean LoadPlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Package = Planograms.Package.FetchById(
                        context.MessageData.Planogram.Id,
                        DomainPrincipal.CurrentUserId,
                        Planograms.PackageLockType.System);
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new PlanogramDeletedException(context.MessageData.Planogram);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region LockSourcePlanogram
        /// <summary>
        /// Attempts to lock the source planogram
        /// </summary>
        private static Boolean LockSourcePlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if the source planogram id is null or the source
                // matches the output then there is nothing to do
                if (context.MessageData.SourcePlanogram == null) return true;
                if (context.MessageData.SourcePlanogram.Id == context.MessageData.Planogram.Id) return true;

                // attempt to lock the specified planogram
                context.SourcePlanogramLockResult = Planograms.Package.LockPackageById(context.MessageData.SourcePlanogram.Id, DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System);

                // if we managed to lock the planogram
                // or the planogram was already locked by us
                // then we can continue safely
                if (context.SourcePlanogramLockResult != Planograms.PackageLockResult.Failed)
                    return true;

                // the source planogram could not be locked
                // we therefor reap this message in order
                // to try again at a later date
                context.RequeueMessage(Engine.MessageRequeueDelay);

                // return that we were unsuccessful
                return false;
            }
        }
        #endregion

        #region LoadSourcePlanogram
        /// <summary>
        /// Loads the source planogram
        /// </summary>
        public static Boolean LoadSourcePlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if the source planogram id is null or the source
                // matches the output then there is nothing to do
                if (context.MessageData.SourcePlanogram == null) return true;
                if (context.MessageData.SourcePlanogram.Id == context.MessageData.Planogram.Id) return true;

                // attempt to load the source planogram
                try
                {
                    // depending on the type of lock we obtained
                    // on the source package, then either open
                    // the planogram read-only or fully
                    if (context.SourcePlanogramLockResult == Planograms.PackageLockResult.Success)
                    {
                        // open the package normally
                        context.SourcePackage = Planograms.Package.FetchById(context.MessageData.SourcePlanogram.Id, (Int32)Galleria.Ccm.Security.DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System);
                    }
                    else
                    {
                        // open the package read-only
                        context.SourcePackage = Planograms.Package.FetchById(context.MessageData.SourcePlanogram.Id);
                    }
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new SourcePlanogramDeletedException(context.MessageData.SourcePlanogram);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return successful
                return true;
            }
        }
        #endregion

        #region PopulatePlanogram
        /// <summary>
        /// Populates the initial planogram
        /// </summary>
        private static Boolean PopulatePlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we do not have a source planogram then there is nothing to do
                if (context.SourcePlanogram == null) return true;

                // clear all existing data from our output planogram package
                context.Package.Planograms.Clear();

                // copy the source planogram details into our package
                context.Package.Planograms.Add(context.SourcePlanogram.Copy());

                // ensure the planogram name matches the package name
                context.Planogram.Name = context.Package.Name;

                // and return successful
                return true;
            }
        }
        #endregion

        #region CopyPlanogramLinks
        /// <summary>
        /// Copies any related links from the initial planogram
        /// </summary>
        private static Boolean CopyPlanogramLinks(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // if we do not have a source planogram then there is nothing to do
                if (context.SourcePlanogram == null) return true;

                //If location specific workpackage - need to copy any related content links
                if (context.Workpackage.WorkpackageType == WorkpackageType.StoreSpecific)
                {
                    PackageHelper.CopyPackageContent((Int32)context.SourcePlanogram.Id, (Int32)context.Package.Id);
                }

                // and return successful
                return true;
            }
        }
        #endregion

        #region SavePlanogram
        /// <summary>
        /// Saves the planogram
        /// </summary>
        private static Boolean SavePlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    // clear the planogram event log before saving
                    context.Planogram.EventLogs.Clear();

                    // save the output planogram, ensuring
                    // that metadata is cleared
                    context.Package = context.Package.Save(true);
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is ConcurrencyException)
                    {
                        throw new PlanogramInvalidException(context.Planogram);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ResetDebugPlanograms
        /// <summary>
        /// Clears down any previous planograms
        /// </summary>
        private static Boolean ResetDebugPlanograms(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch all existing debug planograms
                // for the current planogram
                foreach (WorkpackagePlanogramDebug debugPlanogram in WorkpackagePlanogramDebugList.FetchBySourcePlanogramId((Int32)context.Planogram.Id))
                {
                    // get the debug planogram id
                    Int32 planogramId = debugPlanogram.DebugPlanogramId;
                    if (!planogramId.Equals(context.Planogram.Id))
                    {
                        // attempt to lock the debug planogram
                        // dont worry if we cannot lock the planogram
                        // at this time, as we will try it again anyway
                        // when the debug planogram is actually created
                        if (Planograms.Package.LockPackageById(planogramId, (Int32)DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System) == Planograms.PackageLockResult.Success)
                        {
                            // fetch the planogram package
                            using (Planograms.Package debugPackage = Planograms.Package.FetchById(planogramId, (Int32)DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System))
                            {
                                // clear the existing planograms
                                debugPackage.Planograms.Clear();

                                // save the package
                                debugPackage.Save();
                            }
                        }
                    }
                }

                // and return success
                return true;
            }
        }
        #endregion

        #region IncrementProgress
        /// <summary>
        /// Increments the process progress
        /// </summary>
        private static Boolean IncrementProgress(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                //Fetch the current automtion processing status of the planogram
                WorkpackagePlanogramProcessingStatus currentStatus = 
                    WorkpackagePlanogramProcessingStatus.FetchByPlanogramIdWorkpackageId(context.MessageData.Planogram.Id, context.MessageData.Workpackage.Id);
                String statusDescription = currentStatus != null ? currentStatus.AutomationStatusDescription : String.Empty;

                // increment the planogram progress
                WorkpackagePlanogramProcessingStatus.IncrementAutomationProgress(context.MessageData.Planogram.Id, context.MessageData.Workpackage.Id, statusDescription, DateTime.UtcNow);

                // increment the workpackage progress
                WorkpackageProcessingStatus.IncrementProgress(context.MessageData.Workpackage.Id, "TODO", DateTime.UtcNow);

                // return success
                return true;
            }
        }
        #endregion

        #region ExecutePlanogram
        /// <summary>
        /// Schedules the next series of messages
        /// in order to execute a workflow against
        /// the planogam
        /// </summary>
        private static Boolean ExecuteWorkflow(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                lock (Engine.QueueLock)
                {
                    // start a transaction
                    context.DalContext.Begin();

                    // enqueue a workflow execution step
                    Int64 messageId = Engine.EnqueueMessage(
                        context.DalContext,
                        new Workflow.Execution.Message(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            context.MessageData.Workflow,
                            context.Package,
                            context.MessageData.WorkpackageFinalizationMessageId,
                            context.MessageData.SequencedTasks,
                            0));

                    // determine if there are any sequenced tasks
                    if (context.MessageData.SequencedTasks.Count == 0)
                    {
                        // there are no sequenced tasks so ensure
                        // that the workpackage finalization step
                        // is dependant on the workflow execution
                        Engine.AddDependencies(
                            context.DalContext,
                            context.MessageData.WorkpackageFinalizationMessageId,
                            new List<Int64>() { messageId });
                    }
                    else
                    {
                        // get the first sequenced task
                        var sequencedTasks = context.MessageData.SequencedTasks.OrderBy(t => t.SequenceId).ToList();
                        if (sequencedTasks[0].SequenceId == 0)
                        {
                            // the first sequenced task is at step 0 in
                            // the workflow, so ensure the workflow execution
                            // step is dependent on this sequence task
                            Engine.AddDependencies(
                                context.DalContext,
                                messageId,
                                new List<Int64>() { sequencedTasks[0].MessageId });

                            // determine if there are any more sequenced tasks
                            if (context.MessageData.SequencedTasks.Count == 1)
                            {
                                // there are no more sequenced tasks after
                                // the first one which is at step zero, therefore
                                // we ensure that workpackage finalization is
                                // dependent on the workflow execution step
                                Engine.AddDependencies(
                                    context.DalContext,
                                    context.MessageData.WorkpackageFinalizationMessageId,
                                    new List<Int64>() { messageId });
                            }
                            else
                            {
                                // there are addition sequenced tasks
                                // within the workflow, so we ensure
                                // the next of these tasks is dependant
                                // on our workflow execution
                                Engine.AddDependencies(
                                    context.DalContext,
                                    context.MessageData.SequencedTasks[1].MessageId,
                                    new List<Int64>() { messageId });
                            }
                        }
                        else
                        {
                            // there are workflow sequence tasks after step 0
                            // of the workflow, so ensure that the sequence task
                            // is dependent on the workflow execution step
                            Engine.AddDependencies(
                                context.DalContext,
                                sequencedTasks[0].MessageId,
                                new List<Int64>() { messageId });
                        }
                    }

                    // commit the transaction
                    context.DalContext.Commit();
                }

                // return success
                return true;
            }
        }
        #endregion

        #endregion
    }
}
