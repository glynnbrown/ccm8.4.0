﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using Galleria.Framework.Engine;
using Planograms = Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Workflow.Initialization
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Fields
        private Model.WorkpackageInfo _workpackage; // the workpackage details
        private Planograms.Package _package; // the planogram package
        private Int32? _sourcePlanogramId; // the source planogram id
        private Planograms.Package _sourcePackage; // the source package
        private Planograms.PackageLockResult _sourcePackageLockResult; // the source package lock result
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the workpackage
        /// </summary>
        public Model.WorkpackageInfo Workpackage
        {
            get { return _workpackage; }
            set { _workpackage = value; }
        }

        /// <summary>
        /// Gets or sets the planogram package
        /// </summary>
        public Planograms.Package Package
        {
            get { return _package; }
            set { _package = value; }
        }

        /// <summary>
        /// Returns the first planogram in the package
        /// </summary>
        public Planograms.Planogram Planogram
        {
            get { return (_package == null) || (_package.Planograms.Count == 0) ? null : _package.Planograms[0]; }
        }

        /// <summary>
        /// The source planogram package
        /// </summary>
        public Planograms.Package SourcePackage
        {
            get { return _sourcePackage; }
            set { _sourcePackage = value; }
        }

        /// <summary>
        /// Returns the first planogram in the sourcepackage
        /// </summary>
        public Planograms.Planogram SourcePlanogram
        {
            get { return (_sourcePackage == null) || (_sourcePackage.Planograms.Count == 0) ? null : _sourcePackage.Planograms[0]; }
        }

        /// <summary>
        /// Gets or sets the source planogram lock result
        /// </summary>
        public Planograms.PackageLockResult SourcePlanogramLockResult
        {
            get { return _sourcePackageLockResult; }
            set { _sourcePackageLockResult = value; }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being disposed
        /// </summary>
        protected override void OnDispose()
        {
            // unlock the output package
            if (_package != null) _package.Dispose();
            _package = null;

            // unlock the source package
            if (_sourcePackage != null) _sourcePackage.Dispose();
            _sourcePackage = null;
        }
        #endregion
    }
}
