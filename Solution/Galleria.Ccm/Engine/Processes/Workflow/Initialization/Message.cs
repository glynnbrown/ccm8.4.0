﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-29792 : N.Foster
//  Fixed issue where metadata calculation was changing source planogram row version
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workflow.Initialization
{
    /// <summary>
    /// Defines a message passed to the workpackage finalization process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Initialization")]
    internal class Message : EngineMessageBase
    {
        #region Nested Classes

        #region Entity
        /// <summary>
        /// Defines the entity data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Initialization.Entity")]
        internal class EntityDc
        {
            #region Properties
            /// <summary>
            /// The entity id
            /// </summary>
            [DataMember]
            public Int32 Id;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Workpackage.Execution.Message.EntityDc entity)
            {
                this.Id = entity.Id;
            }

            #endregion
        }
        #endregion

        #region Workpackage
        /// <summary>
        /// Defines the workpackage data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Initialization.Workpackage")]
        internal class WorkpackageDc
        {
            #region Properties
            /// <summary>
            /// The workpackage id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workpackage name
            /// </summary>
            [DataMember]
            public String Name;

            /// <summary>
            /// The workpackage rowversion
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The workpackage priority
            /// </summary>
            [DataMember]
            public Byte Priority;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Workpackage.Execution.Message.WorkpackageDc workpackage, Byte priority)
            {
                this.Id = workpackage.Id;
                this.Name = workpackage.Name;
                this.RowVersion = workpackage.RowVersion;
                this.Priority = priority;
            }

            #endregion
        }
        #endregion

        #region Workflow
        /// <summary>
        /// Defines the workflow data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Initialization.Workflow")]
        internal class WorkflowDc
        {
            #region Properties
            /// <summary>
            /// The workflow id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workflow rowversion
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The workflow name
            /// </summary>
            [DataMember]
            public String Name;

            /// <summary>
            /// The workflow task count
            /// </summary>
            [DataMember]
            public Int32 TaskCount;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkflowDc(Model.Workflow workflow)
            {
                this.Id = workflow.Id;
                this.RowVersion = workflow.RowVersion;
                this.Name = workflow.Name;
                this.TaskCount = workflow.Tasks.Count;
            }

            #endregion
        }
        #endregion

        #region Planogram
        /// <summary>
        /// Defines the planogram data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Initialization.Planogram")]
        internal class PlanogramDc
        {
            #region Properties
            /// <summary>
            /// The planogram id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The planogram name
            /// </summary>
            [DataMember]
            public String Name;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public PlanogramDc(Model.PlanogramInfo planogram)
            {
                this.Id = planogram.Id;
                this.Name = planogram.Name;
            }

            #endregion
        }
        #endregion

        #region SequencedTask
        /// <summary>
        /// Defines the sequence task data for this message
        /// </summary>
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Initialization.SequencedTask")]
        internal class SequencedTask
        {
            #region Properties
            /// <summary>
            /// The task sequence id
            /// </summary>
            [DataMember]
            public Byte SequenceId;

            /// <summary>
            /// The task message id
            /// </summary>
            [DataMember]
            public Int64 MessageId;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public SequencedTask(Tuple<Byte, Int64> sequencedTask)
            {
                this.SequenceId = sequencedTask.Item1;
                this.MessageId = sequencedTask.Item2;
            }

            #endregion
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// The entity details
        /// </summary>
        [DataMember]
        public EntityDc Entity;

        /// <summary>
        /// The workpackage details
        /// </summary>
        [DataMember]
        public WorkpackageDc Workpackage;

        /// <summary>
        /// The workflow details
        /// </summary>
        [DataMember]
        public WorkflowDc Workflow;

        /// <summary>
        /// The planogram details
        /// </summary>
        [DataMember]
        public PlanogramDc Planogram;

        /// <summary>
        /// The source planogram details
        /// </summary>
        [DataMember]
        public PlanogramDc SourcePlanogram;

        /// <summary>
        /// The workpackage finalization message id
        /// </summary>
        [DataMember]
        public Int64 WorkpackageFinalizationMessageId;

        /// <summary>
        /// The list of sequenced tasks
        /// </summary>
        [DataMember]
        public List<SequencedTask> SequencedTasks;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Workpackage.Execution.Message.EntityDc entity,
            Workpackage.Execution.Message.WorkpackageDc workpackage,
            Model.Workflow workflow,
            Model.PlanogramInfo planogram,
            Model.PlanogramInfo sourcePlanogram,
            Byte workpackgePriority,
            Int64 workpackageFinalizationMessageId,
            IEnumerable<Tuple<Byte, Int64>> sequencedTasks) :
            base((Byte)Math.Min(workpackgePriority + 1, Byte.MaxValue))
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage, workpackgePriority);
            this.Workflow = new WorkflowDc(workflow);
            this.Planogram = new PlanogramDc(planogram);
            this.SourcePlanogram = sourcePlanogram == null ? null : new PlanogramDc(sourcePlanogram);
            this.WorkpackageFinalizationMessageId = workpackageFinalizationMessageId;
            this.SequencedTasks = new List<SequencedTask>();
            foreach (var sequencedTask in sequencedTasks)
            {
                this.SequencedTasks.Add(new SequencedTask(sequencedTask));
            }
        }

        #endregion
    }
}
