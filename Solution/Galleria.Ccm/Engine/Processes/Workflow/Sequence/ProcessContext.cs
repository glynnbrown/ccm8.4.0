﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Engine;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Workflow.Sequence
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Fields
        private Model.Workpackage _workpackage; // holds the workpackage details
        private Model.Workflow _workflow; // holds the workflow details
        private List<WorkpackagePlanogram> _workpackagePlanograms = new List<WorkpackagePlanogram>(); // holds all the workpackage planograms locked by the process
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the workpackage details
        /// </summary>
        public Model.Workpackage Workpackage
        {
            get { return _workpackage; }
            set { _workpackage = value; }
        }

        /// <summary>
        /// Gets or sets the workflow details
        /// </summary>
        public Model.Workflow Workflow
        {
            get { return _workflow; }
            set { _workflow = value; }
        }

        /// <summary>
        /// Holds all the planograms locked by the process
        /// </summary>
        public List<WorkpackagePlanogram> WorkpackagePlanograms
        {
            get { return _workpackagePlanograms; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is diposed
        /// </summary>
        protected override void OnDispose()
        {
            foreach (WorkpackagePlanogram workpackagePlanogram in this.WorkpackagePlanograms)
            {
                Package.UnlockPackageById(
                    workpackagePlanogram.DestinationPlanogramId,
                    DomainPrincipal.CurrentUserId,
                    PackageLockType.System);
            }
        }
        #endregion
    }
}
