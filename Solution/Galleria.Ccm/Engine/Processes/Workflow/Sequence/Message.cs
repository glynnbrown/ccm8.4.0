﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Workflow.Sequence
{
    /// <summary>
    /// Defines a message passed to the workflow process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Sequence")]
    internal class Message : EngineMessageBase
    {
        #region Nested Classes

        #region Entity
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Sequence.Entity")]
        internal class EntityDc
        {
            #region Properties
            /// <summary>
            /// The entity id
            /// </summary>
            [DataMember]
            public Int32 Id;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public EntityDc(Workpackage.Execution.Message.EntityDc entity)
            {
                this.Id = entity.Id;
            }
            #endregion
        }
        #endregion

        #region Workpackage
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Sequence.Workpackage")]
        internal class WorkpackageDc
        {
            #region Properties
            /// <summary>
            /// The workpackage id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workpackage name
            /// </summary>
            [DataMember]
            public String Name;

            /// <summary>
            /// The workpackage rowversion
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The workpackage priority
            /// </summary>
            [DataMember]
            public Byte Priority;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkpackageDc(Workpackage.Execution.Message.WorkpackageDc workpackage, Byte priority)
            {
                this.Id = workpackage.Id;
                this.Name = workpackage.Name;
                this.RowVersion = workpackage.RowVersion;
                this.Priority = priority;
            }
            #endregion
        }
        #endregion

        #region Workflow
        [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Workflow.Sequence.Workflow")]
        internal class WorkflowDc
        {
            #region Properties
            /// <summary>
            /// The workflow id
            /// </summary>
            [DataMember]
            public Int32 Id;

            /// <summary>
            /// The workflow rowversion
            /// </summary>
            [DataMember]
            public RowVersion RowVersion;

            /// <summary>
            /// The workflow name
            /// </summary>
            [DataMember]
            public String Name;

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public WorkflowDc(Model.Workflow workflow)
            {
                this.Id = workflow.Id;
                this.RowVersion = workflow.RowVersion;
                this.Name = workflow.Name;
            }
            #endregion
        }
        #endregion

        #endregion

        #region Properties
        /// <summary>
        /// The entity details
        /// </summary>
        [DataMember]
        public EntityDc Entity;

        /// <summary>
        /// The workpackage details
        /// </summary>
        [DataMember]
        public WorkpackageDc Workpackage;

        /// <summary>
        /// The worflow details
        /// </summary>
        [DataMember]
        public WorkflowDc Workflow;

        /// <summary>
        /// Sequence id of the pre-task
        /// </summary>
        [DataMember]
        public Byte? PreTaskSequenceId;

        /// <summary>
        /// Sequqnce
        /// </summary>
        [DataMember]
        public Byte? PostTaskSequenceId;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(
            Byte priority,
            Workpackage.Execution.Message.EntityDc entity,
            Workpackage.Execution.Message.WorkpackageDc workpackage,
            Model.Workflow workflow,
            Byte? preTaskSequenceId,
            Byte? postTaskSequenceId) :
            base(priority)
        {
            this.Entity = new EntityDc(entity);
            this.Workpackage = new WorkpackageDc(workpackage, priority);
            this.Workflow = new WorkflowDc(workflow);
            this.PreTaskSequenceId = preTaskSequenceId;
            this.PostTaskSequenceId = postTaskSequenceId;
        }

        #endregion
    }
}
