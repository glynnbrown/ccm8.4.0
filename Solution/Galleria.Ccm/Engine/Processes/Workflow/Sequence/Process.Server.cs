﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Linq;
using Galleria.Ccm.Engine.Exceptions;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Workflow.Sequence
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!LoadWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ValidateWorkpackage(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LoadWorkflow(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ValidateWorkflow(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LockPlanograms(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ExecuteTaskSequence(context, TaskBase.TaskSequenceType.Post)) return;  // yes, we do execute the post step
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ExecuteTaskSequence(context, TaskBase.TaskSequenceType.Pre)) return;   // before the pre step
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DeadlockException)
                    {
                        // a deadlock exception occurred
                        // this is typically due to a deadlock when saving
                        // the planogram so requeue the message to try again
                        this.RequeueMessage();
                    }
                    else if (baseException is TaskRetryException)
                    {
                        // requeue the message to try again
                        this.RequeueMessage();
                    }
                    else if (baseException is EngineRequeueMessageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is EngineProcessAbortedException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else if (baseException is WorkpackageException)
                    {
                        // rethrow this exception to allow the engine to handle
                        throw;
                    }
                    else
                    {
                        // throw a workpackage exception
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            ex);
                    }
                }
            }
        }
        #endregion

        #region Methods

        #region LoadWorkpackage
        /// <summary>
        /// Loads the desired workpackage
        /// </summary>
        private static Boolean LoadWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Workpackage = Model.Workpackage.FetchById(context.MessageData.Workpackage.Id);
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            Resources.Language.Message.Engine_WorkpackageDeletedException);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ValidateWorkpackage
        /// <summary>
        /// Validates the workpackage
        /// </summary>
        private static Boolean ValidateWorkpackage(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // validate that the workpackage has not changed
                // since we started this process
                if (context.Workpackage.RowVersion != context.MessageData.Workpackage.RowVersion)
                    throw new WorkpackageException(
                        context.Workpackage,
                        Resources.Language.Message.Engine_WorkpackageInvalidException);

                // retrn success
                return true;
            }
        }
        #endregion

        #region LoadWorkflow
        /// <summary>
        /// Loads the workflow
        /// </summary>
        public static Boolean LoadWorkflow(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Workflow = Model.Workflow.FetchById(context.MessageData.Workflow.Id);
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        throw new WorkpackageException(
                            context.MessageData.Entity,
                            context.MessageData.Workpackage,
                            Resources.Language.Message.Engine_WorkflowDeletedException,
                            context.MessageData.Workflow.Name);
                    }
                    else
                    {
                        throw;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ValidateWorkflow
        /// <summary>
        /// Validates the workflow
        /// </summary>
        public static Boolean ValidateWorkflow(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // verify that the workflow has not
                // changed since we started this process
                if (context.Workflow.RowVersion != context.MessageData.Workflow.RowVersion)
                    throw new WorkpackageException(
                        context.MessageData.Entity,
                        context.MessageData.Workpackage,
                        Resources.Language.Message.Engine_WorkflowInvalidException,
                        context.MessageData.Workflow.Name);

                // return success
                return true;
            }
        }
        #endregion

        #region LockPlanograms
        /// <summary>
        /// Attempts to lock all planograms within the workpackage
        /// </summary>
        private static Boolean LockPlanograms(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to lock all planograms in the workpackage
                foreach (WorkpackagePlanogram workpackagePlanogram in context.Workpackage.Planograms)
                {
                    if (Package.LockPackageById(workpackagePlanogram.DestinationPlanogramId, DomainPrincipal.CurrentUserId, PackageLockType.System) == PackageLockResult.Success)
                    {
                        // lock successful so add to the process context
                        context.WorkpackagePlanograms.Add(workpackagePlanogram);
                    }
                    else
                    {
                        // lock failed so enqueue a message to retry
                        context.RequeueMessage(Engine.MessageRequeueDelay);

                        // and return false so we do not continue processing
                        return false;
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #region ExecuteTaskSequence
        /// <summary>
        /// Executes the sequence step
        /// </summary>
        private static Boolean ExecuteTaskSequence(ProcessContext context, TaskBase.TaskSequenceType taskSequenceType)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // get the sequence id of the task that we are executing
                Byte sequenceId;
                if (taskSequenceType == TaskBase.TaskSequenceType.Pre)
                {
                    if (context.MessageData.PreTaskSequenceId == null) return true;
                    sequenceId = (Byte)context.MessageData.PreTaskSequenceId;
                }
                else
                {
                    if (context.MessageData.PostTaskSequenceId == null) return true;
                    sequenceId = (Byte)context.MessageData.PostTaskSequenceId;
                }

                // get the task that we are currently
                // executing from the workflow
                WorkflowTask workflowTask = context.Workflow.Tasks.First(t => t.SequenceId == sequenceId);

                // create an instance of the task
                using (TaskBase task = workflowTask.CreateTask())
                {
                    if (task == null)
                    {
                        // the task could not be created as it
                        // is not registered within the container
                        // this could be due to the task assembly
                        // being out-of-date on this server, at
                        // which point we requeue this message
                        // so that it is processed on another node
                        context.RequeueMessage(Engine.MessageRequeueDelay);

                        // and return false to prevent
                        // any further processing
                        return false;
                    }

                    // now execute our task
                    using (TaskContext taskContext = new TaskContext(
                        context.Workpackage,
                        context.Workflow,
                        workflowTask))
                    {
                        task.ExecuteSequence(taskContext, taskSequenceType);
                    }
                }

                // return success
                return true;
            }
        }
        #endregion

        #endregion
    }
}
