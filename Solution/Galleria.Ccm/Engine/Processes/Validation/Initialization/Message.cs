﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System.Runtime.Serialization;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Validation.Initialization
{
    /// <summary>
    /// Defines a message passed to the validation initialization process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Validation.Initialization")]
    internal class Message : EngineMessageBase
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message() :
            base(EngineMessagePriority.ValidationInitialization)
        {
        }
        #endregion
    }
}
