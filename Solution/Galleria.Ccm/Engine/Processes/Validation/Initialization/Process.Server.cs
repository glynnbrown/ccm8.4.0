﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM803
// V8-29606 : N.Foster
//  Refactored engine maintenance
#endregion
#region Version History: CCM820
// GAF-31367 : N.Foster
// Added workpackage status checks
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Model;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;

namespace Galleria.Ccm.Engine.Processes.Validation.Initialization
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!CheckWorkpackageStatus(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!this.ValidationCatchup(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (ex is Galleria.Framework.Dal.TimeoutException)
                    {
                        // ignore this error as we dont
                        // need to worry about a timeout
                    }
                    else if (ex is EngineProcessAbortedException)
                    {
                        // ignore this error as we dont need
                        // the engine to reap this message
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Checks to see if any workpackages are currently being processed
        /// if so, then no validation calculations are enqueued at this time
        /// </summary>
        private static Boolean CheckWorkpackageStatus(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // for each entity within the solution
                foreach (EntityInfo entity in EntityInfoList.FetchAllEntityInfos())
                {
                    // for each workpackage within the entity
                    foreach (WorkpackageInfo workpackage in WorkpackageInfoList.FetchByEntityId(entity.Id))
                    {
                        // check the status of the workpackage
                        if ((workpackage.ProcessingStatus == WorkpackageProcessingStatusType.Queued) ||
                            (workpackage.ProcessingStatus == WorkpackageProcessingStatusType.Processing))
                        {
                            // a workpackage is currently being processed
                            // so do not schedule a validation update at this time
                            return false;
                        }
                    }
                }

                // return succes
                return true;
            }
        }

        /// <summary>
        /// Enqueues a series of messages to perform
        /// a catch-up of validation processing
        /// </summary>
        private Boolean ValidationCatchup(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch a list of all planograms that are
                // waiting to have their validation data calculated
                IEnumerable<Int32> planogramIds;
                using (IEngineMaintenanceDal dal = context.DalContext.GetDal<IEngineMaintenanceDal>())
                {
                    planogramIds = dal.FetchPlanogramsByValidationCalculationRequired();
                }

                // enqueue a message for each planogram
                // we randomise the order to spread the processing
                // of planograms across the planogram table to reduce
                // contention when processing large amounts of plans
                lock (Engine.QueueLock)
                {
                    Random rnd = new Random();
                    foreach (Int32 planogramId in planogramIds.OrderBy(i => rnd.NextDouble()))
                    {
                        Engine.EnqueueMessage(new Validation.Calculation.Message(planogramId));
                        if (this.IsAborted) break;
                    }
                }

                // return that we were successful
                return true;
            }
        }
        #endregion
    }
}
