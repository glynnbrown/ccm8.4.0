﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;
using Galleria.Framework.Engine;

namespace Galleria.Ccm.Engine.Processes.Validation.Calculation
{
    /// <summary>
    /// Defines a message passed to the validation calculation process
    /// </summary>
    [DataContract(Namespace = "http://www.galleria-rts.com/ccm/engine", Name = "Validation.Calculation")]
    internal class Message : EngineMessageBase
    {
        #region Properties
        /// <summary>
        /// The planogram id
        /// </summary>
        [DataMember]
        public Int32 PlanogramId;

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(Int32 planogramId) :
            base(EngineMessagePriority.ValidationCalculation)
        {
            this.PlanogramId = planogramId;
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public Message(Byte priority, Int32 planogramId) :
            base(priority)
        {
            this.PlanogramId = planogramId;
        }
        #endregion
    }
}
