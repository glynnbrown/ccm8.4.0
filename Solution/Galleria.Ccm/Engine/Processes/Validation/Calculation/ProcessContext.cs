﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using Galleria.Framework.Engine;
using Planograms = Galleria.Framework.Planograms.Model;
using Galleria.Ccm.Security;
using System;

namespace Galleria.Ccm.Engine.Processes.Validation.Calculation
{
    internal class ProcessContext : EngineMessageProcessContextBase<Message>
    {
        #region Fields
        private Planograms.Package _package; // holds the planogram we are processing
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the planogram package
        /// </summary>
        public Planograms.Package Package
        {
            get { return _package; }
            set { _package = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public ProcessContext(IEngineMessageProcess<Message> process) :
            base(process)
        {
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is disposed
        /// </summary>
        protected override void OnDispose()
        {
            if (_package != null) _package.Dispose();
            _package = null;
        }
        #endregion
    }
}
