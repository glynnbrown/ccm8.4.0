﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM810
// V8-29980 : M.Brumby
//  Made use of a new CalculateValidationData method to call if we are wanting to update meta data in
//  the same way that CalculateMetadata does. The OnCalculateValidationData nolonger tries to create
//  metadata.
#endregion
#region Version History: CCM811
// V8-30561 : N.Foster
//  Ensure date last modified is not updated when validation data is calculated
#endregion
#endregion

using System;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Logging;
using Galleria.Framework.Planograms.Model;
using Planograms = Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Engine.Processes.Validation.Calculation
{
    internal partial class Process
    {
        #region Execute
        /// <summary>
        /// Called when this process is being executed
        /// </summary>
        protected override void OnExecute()
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            using (ProcessContext context = new ProcessContext(this))
            {
                try
                {
                    if (!ValidatePlanogramStatus(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LockPlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!ValidatePlanogramStatus(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!LoadPlanogram(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!CalculateValidation(context)) return;
                    if (context.IsAborted) throw new EngineProcessAbortedException();
                    if (!SavePlanogram(context)) return;
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DeadlockException)
                    {
                        // a deadlock occurred
                        // this typically happens when saving a planogram
                        // so ignore this error and the engine will try again later
                    }
                    else if (baseException is EngineProcessAbortedException)
                    {
                        // ignore this error as we dont need
                        // the engine to reap this message
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }
        #endregion

        #region Methods

        #region ValidatePlanogramStatus
        /// <summary>
        /// Validates the planogram status
        /// </summary>
        private static Boolean ValidatePlanogramStatus(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // fetch the planogram status
                WorkpackagePlanogramProcessingStatusList statuses = WorkpackagePlanogramProcessingStatusList.FetchByPlanogramId(context.MessageData.PlanogramId);

                // verify the planogram is not currently being processed
                if (statuses.All(status=> (status.AutomationStatus == ProcessingStatus.Complete) ||
                    (status.AutomationStatus == ProcessingStatus.CompleteWithWarnings) ||
                    (status.AutomationStatus == ProcessingStatus.Failed) ||
                    (status.AutomationStatus == ProcessingStatus.Pending)))
                {
                    return true;
                }

                // the planogram is currently being processed,
                // so release the lock on the planogram
                Planograms.Package.UnlockPackageById(context.MessageData.PlanogramId, DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System);

                // and return false to indicate that
                // we do not want to continue processing
                return false;
            }
        }
        #endregion

        #region LockPlanogram
        /// <summary>
        /// Attempts to lock the planogram
        /// </summary>
        private static Boolean LockPlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // attempt to lock the specified planogram
                if (Planograms.Package.LockPackageById(context.MessageData.PlanogramId, DomainPrincipal.CurrentUserId, Planograms.PackageLockType.System) == PackageLockResult.Success)
                    return true;

                // the planogram could not be locked
                // so return false in order to ensure
                // we take no further action.
                // we dont need to worry about requeuing
                // this message as the maintenance worker
                // will eventually catch-up and schedule
                // another validation calculation for the
                // planogram anyway
                return false;
            }
        }
        #endregion

        #region LoadPlanogram
        /// <summary>
        /// Attempts to load the planogram
        /// </summary>
        private static Boolean LoadPlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    context.Package = Planograms.Package.FetchById(
                        context.MessageData.PlanogramId,
                        DomainPrincipal.CurrentUserId,
                        PackageLockType.System);

                    // we have successfully locked and fetched the
                    // planogram and we know that the validation data is out of date, 
                    // so we can now now update the planogram status
                    using (IPlanogramProcessingStatusDal dal = context.DalContext.GetDal<IPlanogramProcessingStatusDal>())
                    {
                        PlanogramProcessingStatusDto dto = dal.FetchByPlanogramId(context.MessageData.PlanogramId);
                        dto.ValidationStatus = (Byte)Model.ProcessingStatus.Processing;
                        dto.ValidationStatusDescription = Ccm.Resources.Language.Message.ProcessingStatus_Validation_CheckingValidationData;
                        dto.ValidationProgressMax = 3;
                        dto.ValidationProgressCurrent = 0;
                        dto.ValidationDateStarted = DateTime.UtcNow;
                        dto.ValidationDateLastUpdated = DateTime.UtcNow;
                        dal.Update(dto);
                    }
                }
                catch (DataPortalException ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is DtoDoesNotExistException)
                    {
                        // if the planogram no longer exists then
                        // there is noting for us to do
                        return false;
                    }
                    else
                    {
                        throw;
                    }
                }

                // return that we were successful
                return true;
            }
        }
        #endregion

        #region CalculateValidation
        /// <summary>
        /// Perform the validation calculation for this planogram
        /// </summary>
        private static Boolean CalculateValidation(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // check that we have a planogra to update
                if (context.Package == null) return false;

                // ensure that the package is not deleted
                if (context.Package.DateDeleted != null) return false;

                // ensure we have validation to calculate
                if (context.Package.DateValidationDataCalculated != null)
                {
                    SetProcessProgressToCompleted(context);
                    return false;
                }

                // update the processing status to indicate 
                // the validation processing has completed the
                // current step
                PlanogramProcessingStatus.IncrementValidationProgress(
                    context.MessageData.PlanogramId,
                    Ccm.Resources.Language.Message.ProcessingStatus_Validation_CalculatingValidationData,
                    DateTime.UtcNow);

                // calculate the planogram metadata
                context.Package.CalculateValidationData(/*processMetaImages*/true,
                    PlanogramMetadataHelper.NewPlanogramMetadataHelper(Convert.ToInt32(context.Package.EntityId), context.Package));

                // return success
                return true;
            }
        }
        #endregion

        #region SavePlanogram
        /// <summary>
        /// Saves the planogram
        /// </summary>
        private static Boolean SavePlanogram(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                try
                {
                    // update the processing status to indicate 
                    // the validation processing has completed the
                    // current step
                    PlanogramProcessingStatus.IncrementValidationProgress(
                        context.MessageData.PlanogramId,
                        Ccm.Resources.Language.Message.ProcessingStatus_Validation_SavingPlanogram,
                        DateTime.UtcNow);

                    context.Package = context.Package.Save(false, false);

                    // update the processing status to indicate 
                    // the validation processing has completed
                    SetProcessProgressToCompleted(context);
                }
                catch (Exception ex)
                {
                    Exception baseException = ex.GetBaseException();
                    if (baseException is ConcurrencyException)
                    {
                        // a concurrency exception occurred
                        // so we ignore this issue as the
                        // maintenance process will reschedule
                        // the processing of this plan again
                    }
                    else
                    {
                        throw;
                    }
                }

                // return successful
                return true;
            }
        }
        #endregion

        #region SetProcessProgressToCompleted
        /// <summary>
        /// Update the metadata to indicate processing is complete
        /// </summary>
        /// <param name="context"></param>
        private static void SetProcessProgressToCompleted(ProcessContext context)
        {
            using (CodePerformanceMetric metric = new CodePerformanceMetric())
            {
                // update the processing status to indicate 
                // the metadata processing has completed
                using (IPlanogramProcessingStatusDal dal = context.DalContext.GetDal<IPlanogramProcessingStatusDal>())
                {
                    PlanogramProcessingStatusDto dto = dal.FetchByPlanogramId(context.MessageData.PlanogramId);
                    dto.ValidationStatus = (Byte)ProcessingStatus.Complete;
                    dto.ValidationStatusDescription = ProcessingStatusTypeHelper.FriendlyNames[ProcessingStatus.Complete];
                    dto.ValidationProgressCurrent = dto.ValidationProgressMax;
                    dto.ValidationDateLastUpdated = DateTime.UtcNow;
                    dal.Update(dto);
                }
            }
        }
        #endregion

        #endregion
    }
}
