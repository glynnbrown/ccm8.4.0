﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-25787 : N.Foster
//	Created
#endregion
#endregion

namespace Galleria.Ccm.Engine
{
    internal enum EngineMessageType : byte
    {
        Unknown = 0,
        Workpackage = 1,
        Workflow = 2
    }
}
