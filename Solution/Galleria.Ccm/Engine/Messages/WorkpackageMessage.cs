﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Ccm.Engine.Messages
{
    /// <summary>
    /// Defines a message passed to the workpackage process
    /// </summary>
    [DataContract]
    internal class WorkpackageMessage
    {
        #region Properties
        /// <summary>
        /// The workpackage id
        /// </summary>
        [DataMember(Name = "WorkpackageId")]
        public Int32 WorkpackageId;

        /// <summary>
        /// The workpackage action
        /// </summary>
        [DataMember(Name = "Action")]
        public WorkpackageMessageAction Action;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageMessage()
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkpackageMessage(Int32 workpackageId, WorkpackageMessageAction action)
        {
            this.WorkpackageId = workpackageId;
            this.Action = action;
        }
        #endregion
    }

    /// <summary>
    /// Defines the action to be performed by the workpackage process
    /// </summary>
    internal enum WorkpackageMessageAction : byte
    {
        Unknown = 0,
        Process = 1,
        Finalize = 2
    }
}
