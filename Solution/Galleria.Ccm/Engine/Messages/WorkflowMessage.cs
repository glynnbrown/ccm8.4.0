﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#endregion

using System;
using System.Runtime.Serialization;

namespace Galleria.Ccm.Engine.Messages
{
    /// <summary>
    /// Defines a message passed to the workflow process
    /// </summary>
    [DataContract]
    internal class WorkflowMessage
    {
        #region Properties
        /// <summary>
        /// The workpackage id
        /// </summary>
        [DataMember(Name = "WorkpackageId")]
        public Int32 WorkpackageId;

        /// <summary>
        /// The planogram id
        /// </summary>
        [DataMember(Name = "PlanogramId")]
        public Int32 PlanogramId;

        /// <summary>
        /// The source planogram id
        /// </summary>
        [DataMember(Name = "SourcePlanogramId")]
        public Int32? SourcePlanogramId;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkflowMessage()
        {
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public WorkflowMessage(Int32 workpackageId, Int32 planogramId, Int32? sourcePlanogramId)
        {
            this.WorkpackageId = workpackageId;
            this.PlanogramId = planogramId;
            this.SourcePlanogramId = sourcePlanogramId;
        }
        #endregion
    }
}
