﻿#region Header Information

// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800

// V8-25787 : N.Foster
//	Created
// V8-27964 : A.Silva
//      Added PlanHierarchySync value.

#endregion

#endregion

using System;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// Defines the priorities of all the message
    /// types within the engine
    /// </summary>
    /// <remarks>
    /// Lower the value, the higher the priority.
    /// </remarks>
    internal class EngineMessagePriority
    {
        public const Byte WorkpackageInitialization = 0; // this is the highest priority as we want the engine to respond asap to indicate that it has started processing a workpackage
        public const Byte WorkflowFinalization = 1; // we want planograms to finish off as soon as they are ready
        public const Byte WorkpackageFinalization = 2; // we want a workpackage to be marked as complete as soon as its ready
        public const Byte WorkpackageExecution = 3; // execute the workpackage
        public const Byte MaintenanceExecution = 4; // maintenance is important to be executed asap in order to perform cleanup actions
        public const Byte CleanupLocks = 5; // we need to ensure locks are cleaned pretty quickly as they can cause a workpackage to be paused if a lock cannot be obtained
        public const Byte PlanHierarchySync = 6; // Synchronizes the planogram hierarchy to the merchandising hierarchy per entity setting.

        public const Byte WorkflowExecution = 10; // base priority for executing a batch of workflows
        
        public const Byte MetaDataCalculation = 200; // calculation of metadata        
        public const Byte MetaDataInitialization = 201; // enqueues all planograms pending metadata calculation
        public const Byte ValidationCalculation = 202; // calculation of validation data
        public const Byte ValidationInitialization = 203; // enqueues all planograms pending validation calculation

        public const Byte CleanupInstances = 253; // cleans up old engine instance details
        public const Byte CleanupEngineMetrics = 254; // cleans up old engine metric details
        public const Byte CleanupPlanograms = 255; // cleans up orphaned planograms
    }
}