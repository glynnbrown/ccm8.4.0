﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// V8-27426 : L.Ineson
//  Created
#endregion
#endregion


using System;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// Denotes the expected memebers of an object 
    /// containing values for a TaskParameterValue
    /// </summary>
    public interface ITaskParameterValue
    {
        Object Value1 { get; }
        Object Value2 { get; }
        Object Value3 { get; }
    }
}
