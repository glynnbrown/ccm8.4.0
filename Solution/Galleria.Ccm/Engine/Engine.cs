﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
// GAF-25787 : N.Foster
//  Created
#endregion
#region Version History: CCM801
// V8-28258 : N.Foster
//  Added version checks to maintenance process
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Dal;
using Galleria.Framework.Engine;
using Galleria.Framework.Engine.Dal.DataTransferObjects;
using Galleria.Framework.Engine.Dal.Interfaces;
using Galleria.Ccm.Engine.Exceptions;

namespace Galleria.Ccm.Engine
{
    /// <summary>
    /// The CCM engine, used for batch processing of tasks
    /// </summary>
    public sealed class Engine : Galleria.Framework.Engine.Engine
    {
        #region Constants
        private const String _engineEventLogName = "Engine";
        #endregion

        #region Logging
        /// <summary>
        /// Logs the engine starting
        /// </summary>
        protected override void OnLogEngineStarting()
        {
            LoggingHelper.Logger.Write(
                _engineEventLogName,
                null,
                EventLogEvent.EngineStarting,
                null,
                null);
        }

        /// <summary>
        /// Logs the engine stopped
        /// </summary>
        protected override void OnLogEngineStopped()
        {
            LoggingHelper.Logger.Write(
                _engineEventLogName,
                null,
                EventLogEvent.EngineStopped,
                null,
                null);
        }

        /// <summary>
        /// Logs an exception
        /// </summary>
        protected override void OnLogException(System.Exception ex)
        {
            if (ex is EngineShutdownException)
            {
                LoggingHelper.Logger.Write(
                    EventLogEntryType.Information,
                    _engineEventLogName,
                    null,
                    EventLogEvent.EngineShutdownException,
                    null,
                    null,
                    ex.Message);
            }
            else if (ex is EngineRequeueMessageException)
            {
                //LoggingHelper.Logger.Write(
                //    EventLogEntryType.Information,
                //    _engineEventLogName,
                //    null,
                //    EventLogEvent.EngineMessageRequeued,
                //    null,
                //    null,
                //    ex.Message);
            }
            else if (ex is WorkpackageException)
            {
                WorkpackageException e = ex as WorkpackageException;
                LoggingHelper.Logger.Write(
                    EventLogEntryType.Error,
                    _engineEventLogName,
                    e.EntityId,
                    EventLogEvent.UnexpectedException,
                    e.WorkpackageName,
                    e.WorkpackageId,
                    ex);
            }
            else
            {
                LoggingHelper.Logger.Write(
                    _engineEventLogName,
                    new Galleria.Ccm.Processes.ProcessException(ex.Message, ex));
            }
        }

        /// <summary>
        /// Logs when a message is reaped
        /// </summary>
        protected override void OnLogMessageReaped()
        {
            LoggingHelper.Logger.Write(_engineEventLogName, null, EventLogEvent.EngineMessageReaped, null, null);
        }

        /// <summary>
        /// Logs that a workpackage has started running
        /// </summary>
        public static void LogWorkpackageStarted(Int32 entityId, Int32 workpackageId, String workpackageName)
        {
            LoggingHelper.Logger.Write(
                _engineEventLogName,
                entityId,
                EventLogEvent.EngineWorkpackageStarted,
                workpackageName,
                workpackageId);
        }

        /// <summary>
        /// Logs that a workpackage has finished running
        /// </summary>
        public static void LogWorkpackageFinished(Int32 entityId, Int32 workpackageId, String workpackageName)
        {
            LoggingHelper.Logger.Write(
                _engineEventLogName,
                entityId,
                EventLogEvent.EngineWorkpackageFinished,
                workpackageName,
                workpackageId);
        }

        #endregion

        #region Maintenance
        /// <summary>
        /// Called when the maintenance thread executes
        /// </summary>
        protected override void OnExecuteMaintenance()
        {
            base.OnExecuteMaintenance();
#if !DEBUG
            this.CheckAssemblyVersions();
#endif
        }

        /// <summary>
        /// Checks the versions of the main assemblies
        /// used by the engine to ensure that they
        /// are up-to-date
        /// </summary>
        private void CheckAssemblyVersions()
        {
            // we only bother check a few of the most imporant assemblies
            this.CheckAssemblyVersion("Galleria.Framework");
            this.CheckAssemblyVersion("Galleria.Ccm");
            this.CheckAssemblyVersion("Galleria.Ccm.Engine.Tasks");
        }

        /// <summary>
        /// Checks the version of the specified assembly
        /// to ensure that it is the most recent
        /// </summary>
        /// <param name="assemblyName">The assembly name</param>
        private void CheckAssemblyVersion(String assemblyName)
        {
            // get the assembly we are interested in
            Assembly assembly = AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.FullName.StartsWith(assemblyName));

            // if the assembly is missing then it
            // may not have been loaded yet, so simple return
            if (assembly == null) return;

            // get the version details
            var assemblyVersion = assembly.GetName().Version;

            // get the parameter details from the database
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IEngineParameterDal dal = dalContext.GetDal<IEngineParameterDal>())
                {
                    // attempt to get the parameter
                    EngineParameterDto parameter = null;
                    Boolean parameterUpdateRequired = false;
                    try
                    {
                        parameter = dal.FetchByName(assemblyName);
                    }
                    catch (DtoDoesNotExistException)
                    {
                        // do nothing, as this just means
                        // the parameter does not yet exist
                    }

                    // if the parameter does not yet exist
                    // then create it now
                    if (parameter == null)
                    {
                        parameter = new EngineParameterDto()
                        {
                            Name = assemblyName,
                            Value = assemblyVersion.ToString()
                        };
                        parameterUpdateRequired = true;
                    }

                    // get the parameter value and extract the version numbers
                    Version databaseVersion = new Version(parameter.Value.ToString());

                    // now compare the assembly version to the database version
                    Int32 compareResult = assemblyVersion.CompareTo(databaseVersion);
                    if (compareResult <0)
                    {
                        // the assembly version is older than the one
                        // within the database, so throw a shutdown exception
                        throw new EngineShutdownException(String.Format(Message.Engine_EngineShutdownException, assemblyName));
                    }
                    else if (compareResult > 0)
                    {
                        // the assembly version is newer than the one
                        // within the database, so perform an update
                        parameterUpdateRequired = true;
                    }

                    // now update the parameter if required
                    if (parameterUpdateRequired)
                    {
                        // ensure the parameter value is set
                        parameter.Value = assemblyVersion.ToString();

                        // attempt an update of the database
                        try
                        {
                            dal.Update(parameter);
                        }
                        catch (ConcurrencyException)
                        {
                            // a concurrency exception means another instance of
                            // the engine has updated the engine parameter for the
                            // assembly. We ignore the exception as we will check
                            // this all again on the next maintenance pass
                        }
                    }
                }
            }
        }
        #endregion
    }
}
