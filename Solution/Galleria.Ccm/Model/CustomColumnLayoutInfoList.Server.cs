#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.
// V8-26351 : A.Silva ~ Some refactoring to tidy up Custom Column Layout classes.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server side implementation of <see cref="CustomColumnLayoutInfoList" />.
    /// </summary>
    public sealed partial class CustomColumnLayoutInfoList
    {
        #region Constructor

        /// <summary>
        ///     Use factory methods to obtain instances of <see cref="CustomColumnLayoutInfoList" />.
        /// </summary>
        private CustomColumnLayoutInfoList()
        {
        }

        #endregion

        #region Factory Methods

        public static CustomColumnLayoutInfoList FetchByIds(List<Object> ids)
        {
            return
                DataPortal.Fetch<CustomColumnLayoutInfoList>(
                    new FetchByIdsCriteria(CustomColumnLayoutDalFactoryType.Unknown, ids));
        }

        #endregion

        #region Data Access

        /// <summary>
        ///     Called by reflection when <see cref="FetchByIds" /> is invoked.
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchByIdsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            String dalFactoryName;
            var dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<ICustomColumnLayoutInfoDal>())
            {
                foreach (var customColumnLayoutDto in dal.FetchByIds(criteria.Ids))
                {
                    Add(criteria.DalType == CustomColumnLayoutDalFactoryType.FileSystem
                        ? CustomColumnLayoutInfo.GetCustomColumnLayoutInfo(dalContext, customColumnLayoutDto,
                            (String) customColumnLayoutDto.Id)
                        : CustomColumnLayoutInfo.GetCustomColumnLayoutInfo(dalContext, customColumnLayoutDto));
                }
            }

            RaiseListChangedEvents = true;
            IsReadOnly = true;
        }

        /// <summary>
        ///     Called by reflection when <see cref="FetchByTypeCriteria" /> is invoked.
        /// </summary>
        /// <param name="criteria"></param>
        private void DataPortal_Fetch(FetchByTypeCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            String dalFactoryName;
            var dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<ICustomColumnLayoutInfoDal>())
            {
                foreach (var customColumnLayoutDto in dal.FetchByType(criteria.Ids, (Byte) criteria.Type))
                {
                    Add(criteria.DalType == CustomColumnLayoutDalFactoryType.FileSystem
                        ? CustomColumnLayoutInfo.GetCustomColumnLayoutInfo(dalContext, customColumnLayoutDto,
                            (String) customColumnLayoutDto.Id)
                        : CustomColumnLayoutInfo.GetCustomColumnLayoutInfo(dalContext, customColumnLayoutDto));
                }
            }

            RaiseListChangedEvents = true;
            IsReadOnly = true;
        }

        #endregion

        #region Mehods

        /// <summary>
        ///     Returns the correct DAL factory based on the given
        ///     <see cref="CustomColumnLayout.CustomColumnLayoutDalFactoryType" /> provided.
        /// </summary>
        /// <param name="dalType">Type of DAL factory being requested.</param>
        /// <param name="dalFactoryName"><c>OUT</c> Name of the retrieved DAL factory.</param>
        /// <returns></returns>
        private static IDalFactory GetDalFactory(CustomColumnLayoutDalFactoryType dalType, out String dalFactoryName)
        {
            switch (dalType)
            {
                case CustomColumnLayoutDalFactoryType.FileSystem:
                    dalFactoryName = Constants.UserDal;
                    return DalContainer.GetDalFactory(dalFactoryName);

                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }
        }

        #endregion
    }
}