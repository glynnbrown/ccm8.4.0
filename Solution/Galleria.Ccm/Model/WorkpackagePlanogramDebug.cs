﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26317 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class WorkpackagePlanogramDebug : ModelObject<WorkpackagePlanogramDebug>
    {
        #region Properties

        #region SourcePlanogramId
        /// <summary>
        /// SourcePlanogramId property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> SourcePlanogramIdProperty =
            RegisterModelProperty<Int32>(c => c.SourcePlanogramId);
        /// <summary>
        /// Returns the source planogram id
        /// </summary>
        public Int32 SourcePlanogramId
        {
            get { return this.ReadProperty<Int32>(SourcePlanogramIdProperty); }
            set { this.SetProperty<Int32>(SourcePlanogramIdProperty, value); }
        }
        #endregion

        #region WorkflowTaskId
        /// <summary>
        /// WorkflowTaskId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> WorkflowTaskIdProperty =
            RegisterModelProperty<Int32>(c => c.WorkflowTaskId);
        /// <summary>
        /// Returns the workflow task Id
        /// </summary>
        public Int32 WorkflowTaskId
        {
            get { return this.ReadProperty<Int32>(WorkflowTaskIdProperty); }
        }
        #endregion

        #region DebugPlanogramId
        /// <summary>
        /// PlanogramId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> DebugPlanogramIdProperty =
            RegisterModelProperty<Int32>(c => c.DebugPlanogramId);
        /// <summary>
        /// Returns the planogram id
        /// </summary>
        public Int32 DebugPlanogramId
        {
            get { return this.ReadProperty<Int32>(DebugPlanogramIdProperty); }
            set { this.SetProperty<Int32>(DebugPlanogramIdProperty, value); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(WorkpackagePlanogramDebug), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramDebug), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.WorkpackageCreate.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramDebug), new IsInRole(AuthorizationActions.EditObject, DomainPermission.WorkpackageEdit.ToString()));
            BusinessRules.AddRule(typeof(WorkpackagePlanogramDebug), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.WorkpackageDelete.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        internal static WorkpackagePlanogramDebug NewWorkpackagePlanogramDebug(Int32 sourcePlanogramId, Int32 workflowTaskId, Int32 debugPlanogramId)
        {
            WorkpackagePlanogramDebug item = new WorkpackagePlanogramDebug();
            item.Create(sourcePlanogramId, workflowTaskId, debugPlanogramId);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(Int32 sourcePlanogramId, Int32 workflowTaskId, Int32 debugPlanogramId)
        {
            this.LoadProperty<Int32>(SourcePlanogramIdProperty, sourcePlanogramId);
            this.LoadProperty<Int32>(WorkflowTaskIdProperty, workflowTaskId);
            this.LoadProperty<Int32>(DebugPlanogramIdProperty, debugPlanogramId);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}
