﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM810
// V8-30079 : L.Ineson
//  Created
#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Enum denoting the available workpackage
    ///     processing status
    /// </summary>
    public enum WorkpackageProcessingStatusType : byte
    {
        Pending = 0,
        Queued = 1,
        Processing = 2,
        Complete = 3,
        Failed = 4,
        CompleteWithSomeFailed = 5,
        CompleteWithSomeWarnings = 6,
        CompleteWithAllWarnings = 7
    }

    /// <summary>
    ///     Processing Status Type Helper Class.
    /// </summary>
    public static class WorkpackageProcessingStatusTypeHelper
    {
        /// <summary>
        ///     Holds the dictionary mapping <see cref="WorkpackageProcessingStatusType" /> type values to friendly names.
        /// </summary>
        public static readonly Dictionary<WorkpackageProcessingStatusType, String> FriendlyNames = new Dictionary
            <WorkpackageProcessingStatusType, String>
        {
            {WorkpackageProcessingStatusType.Complete, Message.Enum_ProcessingStatus_Complete},
            {WorkpackageProcessingStatusType.Failed, Message.Enum_ProcessingStatus_Failed}, 
            {WorkpackageProcessingStatusType.Pending, Message.Enum_ProcessingStatus_Pending},
            {WorkpackageProcessingStatusType.Processing, Message.Enum_ProcessingStatus_Processing},
            {WorkpackageProcessingStatusType.Queued, Message.Enum_ProcessingStatus_Queued},
            {WorkpackageProcessingStatusType.CompleteWithSomeFailed, Message.Enum_ProcessingStatus_CompleteWithSomeFailed},
            {WorkpackageProcessingStatusType.CompleteWithSomeWarnings, Message.Enum_ProcessingStatus_CompleteWithSomeWarnings},
            {WorkpackageProcessingStatusType.CompleteWithAllWarnings, Message.Enum_ProcessingStatus_CompleteWithAllWarnings},
        };
    }


}
