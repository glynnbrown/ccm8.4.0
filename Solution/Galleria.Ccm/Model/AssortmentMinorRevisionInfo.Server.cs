﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25455 : J.Pickup
//  Created (Copied over from GFS).
// V8-26140 : I.George
// Added RowVersion Load Property

#endregion
#region Version History: (CCM 8.0.3)
// V8-29214 : D.Pleasance
//  Removed DateDeleted as items are now deleted outright
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#endregion


using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using System;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentMinorRevisionInfo
    {
        #region Constructor
        private AssortmentMinorRevisionInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns an AssortmentInfo from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        /// <returns>An AssortmentMinorRevisionInfo object</returns>
        internal static AssortmentMinorRevisionInfo GetAssortmentMinorRevisionInfo(IDalContext dalContext, AssortmentMinorRevisionInfoDto dto)
        {
            return DataPortal.FetchChild<AssortmentMinorRevisionInfo>(dalContext, dto);
        }

        /// <summary>
        /// Returns the latest AssortmentMinorRevision with the specified name
        /// </summary>
        /// <param name="name">The AssortmentMinorRevision name</param>
        /// <returns>The latest version of the Assortment</returns>
        public static AssortmentMinorRevisionInfo FetchLatestVersionByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<AssortmentMinorRevisionInfo>(new FetchLatestVersionByEntityIdNameCriteria(entityId, name));
        }
        #endregion

        #region Data Access

        #region Data Transfer Objects
        /// <summary>
        /// Loads this instance from a dto
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentMinorRevisionInfoDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32?>(ConsumerDecisionTreeIdProperty, dto.ConsumerDecisionTreeId);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<String>(ProductGroupNameProperty, dto.ProductGroupName);
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="dto">The dto</param>
        private void Child_Fetch(IDalContext dalContext, AssortmentMinorRevisionInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        /// <summary>
        /// Called when fetching a MacroSpace by name
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchLatestVersionByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IAssortmentMinorRevisionInfoDal dal = dalContext.GetDal<IAssortmentMinorRevisionInfoDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }
        #endregion

        #endregion
    }
}
