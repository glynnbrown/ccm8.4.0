﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26179  : I.George
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Enums;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Metric Model object
    /// </summary>
    [Serializable]
    public sealed partial class Metric : ModelObject<Metric>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// returns unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// Rowversion Property Definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);

        /// <summary>
        /// the rowVersion TimeStamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId
        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// Gets/sets the enityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }
        #endregion

        #region Description
        /// <summary>
        /// Description Property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        /// gets/sets the description
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
            set { SetProperty<String>(DescriptionProperty, value); }
        }
        #endregion

        #region Name
        
        /// <summary>
        /// Name Property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty <String>(c => c.Name);
        /// <summary>
        /// gets/sets the name property
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region DataModelType
        /// <summary>
        /// Date model type Property definition
        /// </summary>
        public static readonly ModelPropertyInfo<MetricElasticityDataModelType> DataModelTypeProperty =
            RegisterModelProperty<MetricElasticityDataModelType>(c => c.DataModelType, Message.MetricElasticityDataModelType_DataModelType);
        /// <summary>
        /// gets/sets the date model type property definition
        /// </summary>
        public MetricElasticityDataModelType DataModelType
        {
            get { return GetProperty<MetricElasticityDataModelType>(DataModelTypeProperty); }
            set { SetProperty<MetricElasticityDataModelType>(DataModelTypeProperty, value); }
        }
        #endregion

        #region SpecialType
        /// <summary>
        /// Defines the special type property
        /// </summary>
        public static readonly ModelPropertyInfo<MetricSpecialType> SpecialTypeProperty =
            RegisterModelProperty<MetricSpecialType>(c => c.SpecialType, Message.MetricSpecialType_SpecialType);
        /// <summary>
        /// gets and sets the Special type property
        /// </summary>
        public MetricSpecialType SpecialType
        {
            get { return GetProperty<MetricSpecialType>(SpecialTypeProperty); }
            set { SetProperty<MetricSpecialType>(SpecialTypeProperty, value); }
        }
        #endregion

        #region Type
        /// <summary>
        /// Defines the type property
        /// </summary>
        public static readonly ModelPropertyInfo<MetricType> TypeProperty =
            RegisterModelProperty<MetricType>(c => c.Type, Message.MetricSpecialType_Type);
        /// <summary>
        /// gets and sets the type property
        /// </summary>
        public MetricType Type
        {
            get { return GetProperty<MetricType>(TypeProperty); }
            set { SetProperty<MetricType>(TypeProperty, value); }
        }
        #endregion

        #region Direction
        /// <summary>
        /// defines the direction Property
        /// </summary>
        public static readonly ModelPropertyInfo<MetricDirectionType> DirectionProperty =
            RegisterModelProperty<MetricDirectionType>(c => c.Direction);
        /// <summary>
        /// gets and sets the Direction Property
        /// </summary>
        public MetricDirectionType Direction
        {
            get { return GetProperty<MetricDirectionType>(DirectionProperty); }
            set { SetProperty<MetricDirectionType>(DirectionProperty, value); }
        }

        #endregion

        #region DateCreated
        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// gets/sets the datecreated
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }
        #endregion

        #region DateLastmodified
        /// <summary>
        /// Date Last modified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// gets/sets date last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted
        /// <summary>
        /// date deleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        /// <summary>
        /// geets/sets the date deleted
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// add business rule to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

            BusinessRules.AddRule(new Required(DescriptionProperty));
            BusinessRules.AddRule(new MaxLength(DescriptionProperty, 1000));

            BusinessRules.AddRule(new Required(DataModelTypeProperty));
            BusinessRules.AddRule(new Required(SpecialTypeProperty));
            BusinessRules.AddRule(new Required(DirectionProperty));
        }
        #endregion

        #region Authorization Rule

        public static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Metric), new IsInRole(AuthorizationActions.GetObject, DomainPermission.MetricGet.ToString()));
            BusinessRules.AddRule(typeof(Metric), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.MetricCreate.ToString()));
            BusinessRules.AddRule(typeof(Metric), new IsInRole(AuthorizationActions.EditObject, DomainPermission.MetricEdit.ToString()));
            BusinessRules.AddRule(typeof(Metric), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.MetricDelete.ToString()));
        }
        #endregion

        #region factoryMethods
        /// <summary>
        /// creates a new metric
        /// </summary>
        /// <returns></returns>
        public static Metric NewMetric(Int32 entityId)
        {
            Metric item = new Metric();
            item.Create(entityId);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// called when creating a new object of this type
        /// </summary>
        public void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty<MetricDirectionType>(DirectionProperty, MetricDirectionType.MaximiseIncrease);
            this.LoadProperty<MetricElasticityDataModelType>(DataModelTypeProperty, MetricElasticityDataModelType.Linear);
            this.LoadProperty<MetricSpecialType>(SpecialTypeProperty, MetricSpecialType.None);
            this.LoadProperty<MetricType>(TypeProperty, MetricType.Integer);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<Metric>(oldId, newId);
        }
        #endregion

    }
}
