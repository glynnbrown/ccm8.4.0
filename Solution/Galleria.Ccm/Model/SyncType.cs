﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion

#region Version History: (CCM 8.1.0)
// V8-29809 : N.Haywood
//  Added communicated dates
#endregion

#region Version History (CCM 8.3.0)
// V8-31835 : N.Haywood
//  Added LocationProductLegal
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum SyncType
    {
        Merchandising = 1,
        Product = 2,
        ProductUnivese = 3,
        Location = 4,
        LocationSpace = 5,
        Cluster = 6,
        Performance = 7,
        Entity = 8,
        LocationHierarchy = 9,
        LocationType = 10,
        Metric = 11,
        CDT = 12,
        Range = 13,
        LocationProductAttribute = 14,
        LocationProductIllegal = 15,
        Assortment = 16,
        CommunicatedDates = 17, 
        LocationProductLegal = 18,
    }

    public static class SyncTypeHelper
    {

        public static readonly Dictionary<SyncType, String> FriendlyDescriptions =
            new Dictionary<SyncType, String>()
            {
                {SyncType.Merchandising, Message.Enum_SyncType_Merchandising},
                {SyncType.Product, Message.Enum_SyncType_Product},          
                {SyncType.ProductUnivese, Message.Enum_SyncType_ProductUnivese},
                {SyncType.Location, Message.Enum_SyncType_Location},
                {SyncType.LocationSpace, Message.Enum_SyncType_LocationSpace},
                {SyncType.Cluster, Message.Enum_SyncType_Cluster},
                {SyncType.Performance, Message.Enum_SyncType_Performance},
                {SyncType.Entity, Message.Enum_SyncType_Entity},
                {SyncType.LocationHierarchy, Message.Enum_SyncType_LocationHierarchy},
                {SyncType.LocationType, Message.Enum_SyncType_LocationType},
                {SyncType.Metric, Message.Enum_SyncType_Metric},
                {SyncType.CDT, Message.Enum_SyncType_CDT},
                {SyncType.Range, Message.Enum_SyncType_Range},
                {SyncType.LocationProductAttribute, Message.Enum_SyncType_LocationProductAttribute},
                {SyncType.LocationProductIllegal, Message.Enum_SyncType_LocationProductIllegal},
                {SyncType.Assortment, Message.Enum_SyncType_Assortment},
                {SyncType.CommunicatedDates, Message.Enum_SyncType_CommunicatedDates},
                {SyncType.LocationProductLegal, Message.Enum_SyncType_LocationProductLegal},
            };
    }
}