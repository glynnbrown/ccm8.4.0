﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-26041 : A.Kuszyk
//  Created (copied from GFS).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class Image
    {
        #region Constructor
        private Image() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the existing object by its id (Fetch)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static Image FetchImageById(Int32 id)
        {
            return DataPortal.Fetch<Image>(id);
        }

        /// <summary>
        /// Returns an existing object (Child Fetch)
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static Image GetImage(IDalContext dalContext, ImageDto dto)
        {
            return DataPortal.FetchChild<Image>(dalContext, dto);
        }

        /// <summary>
        /// Returns the existing object by its id (Child Fetch)
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static Image GetImageById(IDalContext dalContext, Int32 id)
        {
            return DataPortal.FetchChild<Image>(dalContext, id);
        }

        /// <summary>
        /// Returns the existing object by its id (Child Fetch)
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static Image GetImageById(Int32 id)
        {
            return DataPortal.FetchChild<Image>(id);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ImageDto GetDataTransferObject()
        {
            return new ImageDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                OriginalFileId = ReadProperty<Int32>(OriginalFileIdProperty),
                BlobId = ReadProperty<Int32>(BlobIdProperty),
                CompressionId = ReadProperty<Int32>(CompressionIdProperty),
                SizeInBytes = ReadProperty<Int64>(SizeInBytesProperty),
                Width = ReadProperty<Int16>(WidthProperty),
                Height = ReadProperty<Int16>(HeightProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(ImageDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<Int32>(OriginalFileIdProperty, dto.OriginalFileId);
            LoadProperty<Int32>(BlobIdProperty, dto.BlobId);
            LoadProperty<Int32>(CompressionIdProperty, dto.CompressionId);
            LoadProperty<Int64>(SizeInBytesProperty, dto.SizeInBytes);
            LoadProperty<Int16>(HeightProperty, dto.Height);
            LoadProperty<Int16>(WidthProperty, dto.Width);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Int32 id)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                using (IImageDal dal = dalContext.GetDal<IImageDal>())
                {
                    LoadDataTransferObject(dal.FetchById(id));
                }

                dalContext.Commit();
            }

        }

        /// <summary>
        /// Called when returning an existing item
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ImageDto dto)
        {
            LoadDataTransferObject(dto);
        }

        /// <summary>
        /// Called when returning an existing item byid
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 id)
        {
            using (IImageDal dal = dalContext.GetDal<IImageDal>())
            {
                LoadDataTransferObject(dal.FetchById(id));
            }
        }

        /// <summary>
        /// Called when returning an existing item byid
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(Int32 id)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IImageDal dal = dalContext.GetDal<IImageDal>())
                {
                    LoadDataTransferObject(dal.FetchById(id));
                }
                dalContext.Commit();
            }
        }

        #endregion

        #region Insert

        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();

                Insert(dalContext);

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            Insert(dalContext);
        }

        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Insert(IDalContext dalContext)
        {
            // update the data source
            using (IImageDal dal = dalContext.GetDal<IImageDal>())
            {
                //Update the children first as the child ID's are used as parent properties.
                FieldManager.UpdateChildren(dalContext, this);

                LoadProperty<Int32>(OriginalFileIdProperty, OriginalFile.Id);
                LoadProperty<Int32>(BlobIdProperty, ImageBlob.Id);

                // get the dto
                ImageDto dto = GetDataTransferObject();
                dal.Insert(dto);

                //update this.id from the dto
                LoadProperty<Int32>(IdProperty, dto.Id);
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IImageDal dal = dalContext.GetDal<IImageDal>())
                {
                    FieldManager.UpdateChildren(dalContext, this);
                    LoadProperty<Int32>(OriginalFileIdProperty, OriginalFile.Id);
                    LoadProperty<Int32>(BlobIdProperty, ImageBlob.Id);

                    // get the dto
                    ImageDto dto = GetDataTransferObject();
                    dal.Update(dto);

                    //update this from the dto
                    LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                    LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                }

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            using (IImageDal dal = dalContext.GetDal<IImageDal>())
            {
                // get the dto
                ImageDto dto = GetDataTransferObject();
                dal.Update(dto);

                //update this from the dto
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete
        //NOT IMPLEMENTED
        ///// <summary>
        ///// Called when this object is deleting itself
        ///// </summary>
        ///// <param name="dalContext">The current dal context</param>
        //[SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        //private void Child_DeleteSelf(IDalContext dalContext)
        //{
        //    using (IImageDal dal = dalContext.GetDal<IImageDal>())
        //    {
        //        dal.DeleteById(this.Id);
        //    }
        //}
        #endregion

        #endregion
    }
}
