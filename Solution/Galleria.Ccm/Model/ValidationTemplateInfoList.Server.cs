﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.
// V8-24761 : A.Silva ~ Corrected misspelling in DataPortal Fetch methods.

#endregion

#region Version History: (CCM v8.03)
// V8-29219 : L.Ineson
//  Removed FetchByEntityIdIncludingDeleted
#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class ValidationTemplateInfoList
    {
        #region Constructor

        /// <summary>
        ///     Private Constructor. Use factory methods to instantiate this type.
        /// </summary>
        private ValidationTemplateInfoList()
        {

        }

        #endregion

        #region Factory Methods

        public static ValidationTemplateInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ValidationTemplateInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #region Data Access

        /// <summary>
        ///     Called when retrieving a list of all Validation templates for an entity.
        /// </summary>
        /// <param name="criteria">The criteria to select valid items with.</param>
        /// <remarks>This procedure should NOT return <c>deleted</c> items.</remarks>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;
            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IValidationTemplateInfoDal>())
            {
                var dtoList = dal.FetchByEntityId(criteria.EntityId);
                foreach (var dto in dtoList)
                {
                    Add(ValidationTemplateInfo.FetchValidationTemplateInfo(dalContext, dto));
                }
            }
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }


        #endregion
    }
}
