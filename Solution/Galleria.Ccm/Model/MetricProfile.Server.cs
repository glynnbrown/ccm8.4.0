﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26123 : L.Ineson
//		Created (Auto-generated)
// V8-27548 : A.Kuszyk
//  Added FetchByEntityIdName.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class MetricProfile
    {
        #region Constructors
        private MetricProfile() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing MetricProfile with the given id
        /// </summary>
        public static MetricProfile FetchById(Int32 id)
        {
            return DataPortal.Fetch<MetricProfile>(new FetchByIdCriteria(id));
        }

        public static MetricProfile FetchByEntityIdName(Int32 entityId, String name)
        {
            return DataPortal.Fetch<MetricProfile>(new FetchByEntityIdNameCriteria(entityId, name));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, MetricProfileDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            this.LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Int32?>(Metric1MetricIdProperty, dto.Metric1MetricId);
            this.LoadProperty<Single?>(Metric1RatioProperty, dto.Metric1Ratio);
            this.LoadProperty<Int32?>(Metric2MetricIdProperty, dto.Metric2MetricId);
            this.LoadProperty<Single?>(Metric2RatioProperty, dto.Metric2Ratio);
            this.LoadProperty<Int32?>(Metric3MetricIdProperty, dto.Metric3MetricId);
            this.LoadProperty<Single?>(Metric3RatioProperty, dto.Metric3Ratio);
            this.LoadProperty<Int32?>(Metric4MetricIdProperty, dto.Metric4MetricId);
            this.LoadProperty<Single?>(Metric4RatioProperty, dto.Metric4Ratio);
            this.LoadProperty<Int32?>(Metric5MetricIdProperty, dto.Metric5MetricId);
            this.LoadProperty<Single?>(Metric5RatioProperty, dto.Metric5Ratio);
            this.LoadProperty<Int32?>(Metric6MetricIdProperty, dto.Metric6MetricId);
            this.LoadProperty<Single?>(Metric6RatioProperty, dto.Metric6Ratio);
            this.LoadProperty<Int32?>(Metric7MetricIdProperty, dto.Metric7MetricId);
            this.LoadProperty<Single?>(Metric7RatioProperty, dto.Metric7Ratio);
            this.LoadProperty<Int32?>(Metric8MetricIdProperty, dto.Metric8MetricId);
            this.LoadProperty<Single?>(Metric8RatioProperty, dto.Metric8Ratio);
            this.LoadProperty<Int32?>(Metric9MetricIdProperty, dto.Metric9MetricId);
            this.LoadProperty<Single?>(Metric9RatioProperty, dto.Metric9Ratio);
            this.LoadProperty<Int32?>(Metric10MetricIdProperty, dto.Metric10MetricId);
            this.LoadProperty<Single?>(Metric10RatioProperty, dto.Metric10Ratio);
            this.LoadProperty<Int32?>(Metric11MetricIdProperty, dto.Metric11MetricId);
            this.LoadProperty<Single?>(Metric11RatioProperty, dto.Metric11Ratio);
            this.LoadProperty<Int32?>(Metric12MetricIdProperty, dto.Metric12MetricId);
            this.LoadProperty<Single?>(Metric12RatioProperty, dto.Metric12Ratio);
            this.LoadProperty<Int32?>(Metric13MetricIdProperty, dto.Metric13MetricId);
            this.LoadProperty<Single?>(Metric13RatioProperty, dto.Metric13Ratio);
            this.LoadProperty<Int32?>(Metric14MetricIdProperty, dto.Metric14MetricId);
            this.LoadProperty<Single?>(Metric14RatioProperty, dto.Metric14Ratio);
            this.LoadProperty<Int32?>(Metric15MetricIdProperty, dto.Metric15MetricId);
            this.LoadProperty<Single?>(Metric15RatioProperty, dto.Metric15Ratio);
            this.LoadProperty<Int32?>(Metric16MetricIdProperty, dto.Metric16MetricId);
            this.LoadProperty<Single?>(Metric16RatioProperty, dto.Metric16Ratio);
            this.LoadProperty<Int32?>(Metric17MetricIdProperty, dto.Metric17MetricId);
            this.LoadProperty<Single?>(Metric17RatioProperty, dto.Metric17Ratio);
            this.LoadProperty<Int32?>(Metric18MetricIdProperty, dto.Metric18MetricId);
            this.LoadProperty<Single?>(Metric18RatioProperty, dto.Metric18Ratio);
            this.LoadProperty<Int32?>(Metric19MetricIdProperty, dto.Metric19MetricId);
            this.LoadProperty<Single?>(Metric19RatioProperty, dto.Metric19Ratio);
            this.LoadProperty<Int32?>(Metric20MetricIdProperty, dto.Metric20MetricId);
            this.LoadProperty<Single?>(Metric20RatioProperty, dto.Metric20Ratio);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);

        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private MetricProfileDto GetDataTransferObject()
        {
            return new MetricProfileDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                Name = ReadProperty<String>(NameProperty),
                Metric1MetricId = ReadProperty<Int32?>(Metric1MetricIdProperty),
                Metric1Ratio = ReadProperty<Single?>(Metric1RatioProperty),
                Metric2MetricId = ReadProperty<Int32?>(Metric2MetricIdProperty),
                Metric2Ratio = ReadProperty<Single?>(Metric2RatioProperty),
                Metric3MetricId = ReadProperty<Int32?>(Metric3MetricIdProperty),
                Metric3Ratio = ReadProperty<Single?>(Metric3RatioProperty),
                Metric4MetricId = ReadProperty<Int32?>(Metric4MetricIdProperty),
                Metric4Ratio = ReadProperty<Single?>(Metric4RatioProperty),
                Metric5MetricId = ReadProperty<Int32?>(Metric5MetricIdProperty),
                Metric5Ratio = ReadProperty<Single?>(Metric5RatioProperty),
                Metric6MetricId = ReadProperty<Int32?>(Metric6MetricIdProperty),
                Metric6Ratio = ReadProperty<Single?>(Metric6RatioProperty),
                Metric7MetricId = ReadProperty<Int32?>(Metric7MetricIdProperty),
                Metric7Ratio = ReadProperty<Single?>(Metric7RatioProperty),
                Metric8MetricId = ReadProperty<Int32?>(Metric8MetricIdProperty),
                Metric8Ratio = ReadProperty<Single?>(Metric8RatioProperty),
                Metric9MetricId = ReadProperty<Int32?>(Metric9MetricIdProperty),
                Metric9Ratio = ReadProperty<Single?>(Metric9RatioProperty),
                Metric10MetricId = ReadProperty<Int32?>(Metric10MetricIdProperty),
                Metric10Ratio = ReadProperty<Single?>(Metric10RatioProperty),
                Metric11MetricId = ReadProperty<Int32?>(Metric11MetricIdProperty),
                Metric11Ratio = ReadProperty<Single?>(Metric11RatioProperty),
                Metric12MetricId = ReadProperty<Int32?>(Metric12MetricIdProperty),
                Metric12Ratio = ReadProperty<Single?>(Metric12RatioProperty),
                Metric13MetricId = ReadProperty<Int32?>(Metric13MetricIdProperty),
                Metric13Ratio = ReadProperty<Single?>(Metric13RatioProperty),
                Metric14MetricId = ReadProperty<Int32?>(Metric14MetricIdProperty),
                Metric14Ratio = ReadProperty<Single?>(Metric14RatioProperty),
                Metric15MetricId = ReadProperty<Int32?>(Metric15MetricIdProperty),
                Metric15Ratio = ReadProperty<Single?>(Metric15RatioProperty),
                Metric16MetricId = ReadProperty<Int32?>(Metric16MetricIdProperty),
                Metric16Ratio = ReadProperty<Single?>(Metric16RatioProperty),
                Metric17MetricId = ReadProperty<Int32?>(Metric17MetricIdProperty),
                Metric17Ratio = ReadProperty<Single?>(Metric17RatioProperty),
                Metric18MetricId = ReadProperty<Int32?>(Metric18MetricIdProperty),
                Metric18Ratio = ReadProperty<Single?>(Metric18RatioProperty),
                Metric19MetricId = ReadProperty<Int32?>(Metric19MetricIdProperty),
                Metric19Ratio = ReadProperty<Single?>(Metric19RatioProperty),
                Metric20MetricId = ReadProperty<Int32?>(Metric20MetricIdProperty),
                Metric20Ratio = ReadProperty<Single?>(Metric20RatioProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                DateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IMetricProfileDal dal = dalContext.GetDal<IMetricProfileDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }

        /// <summary>
        /// Called when returning FetchByEntityIdName
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdNameCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IMetricProfileDal dal = dalContext.GetDal<IMetricProfileDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchByEntityIdName(criteria.EntityId, criteria.Name));
                }
            }
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                MetricProfileDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IMetricProfileDal dal = dalContext.GetDal<IMetricProfileDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<MetricProfile>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                MetricProfileDto dto = GetDataTransferObject();
                using (IMetricProfileDal dal = dalContext.GetDal<IMetricProfileDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IMetricProfileDal dal = dalContext.GetDal<IMetricProfileDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion

    }
}