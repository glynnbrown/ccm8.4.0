﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25556 : D.Pleasance
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public enum SyncTargetError
    {
        None = 0,
        TargetCouldNotBeLocated = 1,
        CouldNotConnectToFoundationServices = 2,
        MaximumFailedCount = 3
    }

    public static class SyncTargetErrorHelper
    {
        public static readonly Dictionary<SyncTargetError, string> FriendlyNames =
            new Dictionary<SyncTargetError, string>()
            {
                {SyncTargetError.None, Message.Enum_SyncTargetError_None},
                {SyncTargetError.TargetCouldNotBeLocated, Message.Enum_SyncTargetError_TargetCouldNotBeLocated},
                {SyncTargetError.CouldNotConnectToFoundationServices, Message.Enum_SyncTargetError_CouldNotConnectToFoundationServices},
                {SyncTargetError.MaximumFailedCount, Message.Enum_SyncTargetError_MaximumFailedCount}
            };

        public static readonly Dictionary<SyncTargetError, string> FriendlyDescriptions =
            new Dictionary<SyncTargetError, string>()
            {
                {SyncTargetError.None, Message.Enum_SyncTargetError_None_Description},
                {SyncTargetError.TargetCouldNotBeLocated, Message.Enum_SyncTargetError_TargetCouldNotBeLocated_Description},
                {SyncTargetError.CouldNotConnectToFoundationServices, Message.Enum_SyncTargetError_CouldNotConnectToFoundationServices_Description},
                {SyncTargetError.MaximumFailedCount, Message.Enum_SyncTargetError_MaximumFailedCount_Description}
            };

        /// <summary>
        /// Returns the matching enum value from the specifed description
        /// Returns null if no match found.
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static SyncTargetError? SyncTargetErrorGetEnum(string description)
        {
            foreach (KeyValuePair<SyncTargetError, string> keyPair in SyncTargetErrorHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}