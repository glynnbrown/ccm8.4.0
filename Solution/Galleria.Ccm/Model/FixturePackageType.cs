﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// An enumeration that defines a package type
    /// which indicates the source of a package
    /// </summary>
    public enum FixturePackageType
    {
        Unknown = 0,
        FileSystem = 1
        // GfsDatabase
        // GfsWebServices
        // CcmDatabase
        // CcmRepository
        // IkbDatabase
    }

}
