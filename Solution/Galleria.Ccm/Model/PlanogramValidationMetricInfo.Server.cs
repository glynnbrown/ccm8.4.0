﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server-side implementation for <see cref="PlanogramValidationMetricInfo"/>.
    /// </summary>
    public partial class PlanogramValidationMetricInfo
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use factory methods to retrieve new instances.
        /// </summary>
        private PlanogramValidationMetricInfo() {}

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Gets a <see cref="PlanogramValidationMetricInfo"/> from the given <paramref name="dto"/>.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">Source <see cref="PlanogramValidationMetricInfoDto"/> to load in the new instance.</param>
        /// <returns>A new instance of <see cref="PlanogramValidationMetricInfo"/> loaded with the data in the given <paramref name="dto"/>.</returns>
        /// <remarks>CSLA will invoke <see cref="Child_Fetch"/> on the server via reflection.</remarks>
        public static PlanogramValidationMetricInfo GetPlanogramValidationMetricInfo(IDalContext dalContext,
            PlanogramValidationMetricInfoDto dto)
        {
            return DataPortal.FetchChild<PlanogramValidationMetricInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        ///     Loads the data from the given <paramref name="dto"/> into this instance.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">The source <see cref="PlanogramValidationMetricInfoDto"/>.</param>
        private void LoadDataTransferObject(IDalContext dalContext, PlanogramValidationMetricInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Int32>(PlanogramValidationGroupIdProperty, dto.PlanogramValidationGroupId);
            this.LoadProperty<String>(FieldProperty, dto.Field);
            this.LoadProperty<PlanogramValidationTemplateResultType>(ResultTypeProperty, (PlanogramValidationTemplateResultType) dto.ResultType);
        }
        
        #endregion

        #region Fetch

        /// <summary>
        ///     Invoked by CSLA via reflection when calling <see cref="GetPlanogramValidationMetricInfo" />.
        /// </summary>
        /// <param name="dalContext">Not used.</param>
        /// <param name="dto">
        ///     Instance of <see cref="PlanogramValidationMetricInfoDto" /> containing the data to initialize the new
        ///     instance.
        /// </param>
        private void Child_Fetch(IDalContext dalContext, PlanogramValidationMetricInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
