#region Header Information

// Copyright � Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-24700: A.Silva ~ Created.

#endregion

#endregion

using System.Linq;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public sealed partial class CustomColumnFilterList
    {
        #region Constructor

        private CustomColumnFilterList()
        {
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(FetchByParentIdCriteria criteria)
        {
            RaiseListChangedEvents = false;

            using (var dalContext = GetDalFactory(criteria.DalFactoryName).CreateContext())
            using (var dal = dalContext.GetDal<ICustomColumnFilterDal>())
            {
                dal.FetchByCustomColumnLayoutId(criteria.ParentId)
                    .ToList()
                    .ForEach(dto => Add(CustomColumnFilter.FetchCustomColumnFilter(dalContext, dto)));
            }

            MarkAsChild();

            RaiseListChangedEvents = true;
        }

        #endregion

    }
}