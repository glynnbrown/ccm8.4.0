﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class WorkpackagePlanogramProcessingStatusList
    {
        #region Constructor
        private WorkpackagePlanogramProcessingStatusList() { } //force use of factory methods
        #endregion

        #region Factory Mehtods
        /// <summary>
        /// Returns a list of objects for the given plan id for all workpackages.
        /// List is a root object
        /// </summary>
        /// <param name="planogramId"></param>
        /// <returns></returns>
        public static WorkpackagePlanogramProcessingStatusList FetchByPlanogramId(Int32 planogramId)
        {
            return DataPortal.Fetch<WorkpackagePlanogramProcessingStatusList>(new FetchByPlanogramIdCriteria(planogramId));
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when loading this list from entity
        /// </summary>
        /// <param name="criteria">the current entity id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByPlanogramIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                //fetch all metric for the entity
                //IEnumerable<MetricDto> MetricDtoList;
                using (IWorkpackagePlanogramProcessingStatusDal dal = dalContext.GetDal<IWorkpackagePlanogramProcessingStatusDal>())
                {
                    IEnumerable<WorkpackagePlanogramProcessingStatusDto> dtoList = dal.FetchByPlanogramId(criteria.PlanogramId);
                    //load the Metric
                    foreach (WorkpackagePlanogramProcessingStatusDto dto in dtoList)
                    {
                        Add(WorkpackagePlanogramProcessingStatus.FetchChild(dalContext, dto));
                    }

                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #region Update
        /// <summary>
        /// called when this list is being updated
        /// (only when it is a root object)
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }
        #endregion
        #endregion

    }
}
