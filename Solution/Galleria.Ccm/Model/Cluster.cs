﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
// V8-27927 : I.George
//  Added OnCopy method
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed partial class Cluster : ModelObject<Cluster>
    {
        #region Parent

        /// <summary>
        /// Returns a reference to the parent ClusterScheme
        /// </summary>
        public ClusterScheme ParentClusterScheme
        {
            get
            {
                ClusterList parentList = base.Parent as ClusterList;
                if (parentList != null)
                {
                    return parentList.Parent as ClusterScheme;
                }
                return null;
            }
        }

        #endregion

        #region Authorisation Rules
        /// <summary>
        /// Defines the authorisation rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Cluster), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ClusterSchemeCreate.ToString()));
            BusinessRules.AddRule(typeof(Cluster), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ClusterSchemeGet.ToString()));
            BusinessRules.AddRule(typeof(Cluster), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ClusterSchemeEdit.ToString()));
            BusinessRules.AddRule(typeof(Cluster), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ClusterSchemeDelete.ToString()));
        }

        #endregion

        #region Properties

        /// <summary>
        /// The unqiue cluster scheme id
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The cluster scheme name
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Generic_Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        /// <summary>
        /// The locations under this cluster
        /// </summary>
        public static readonly ModelPropertyInfo<ClusterLocationList> LocationsProperty =
            RegisterModelProperty<ClusterLocationList>(c => c.Locations);
        public ClusterLocationList Locations
        {
            get { return GetProperty<ClusterLocationList>(LocationsProperty); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new cluster
        /// </summary>
        /// <returns>The new cluster</returns>
        public static Cluster NewCluster()
        {
            Cluster cluster = new Cluster();
            cluster.Create();
            return cluster;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<ClusterLocationList>(LocationsProperty, ClusterLocationList.NewClusterLocationList());
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<Cluster>(oldId, newId);
        }

        #endregion
    }
}
