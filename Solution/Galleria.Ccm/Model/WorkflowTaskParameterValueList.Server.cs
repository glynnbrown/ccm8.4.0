﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25886 : L.Ineson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkflowTaskParameterValueList
    {
        #region Constructor
        private WorkflowTaskParameterValueList() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all tasks within the specified workflow
        /// </summary>
        internal static WorkflowTaskParameterValueList FetchByWorkflowTaskParameterId(IDalContext dalContext, Int32 workflowTaskParameterId)
        {
            return DataPortal.FetchChild<WorkflowTaskParameterValueList>(dalContext, workflowTaskParameterId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, Int32 workflowTaskParameterId)
        {
            this.RaiseListChangedEvents = false;
            using (IWorkflowTaskParameterValueDal dal = dalContext.GetDal<IWorkflowTaskParameterValueDal>())
            {
                IEnumerable<WorkflowTaskParameterValueDto> dtoList = dal.FetchByWorkflowTaskParameterId(workflowTaskParameterId);
                foreach (WorkflowTaskParameterValueDto dto in dtoList)
                {
                    this.Add(WorkflowTaskParameterValue.GetWorkflowTaskParameterValue(dalContext, dto));
                }
            }
            this.RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
