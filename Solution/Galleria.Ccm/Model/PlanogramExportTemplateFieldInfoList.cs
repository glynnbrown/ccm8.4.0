﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31546 : M.Pettit
//  Created
//  Refactored PlanogramFileType to Framework.Plangoram.Model and renamed PlanogramExportFileType
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.External;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public sealed class PlanogramExportTemplateFieldInfoList : ModelReadOnlyList<PlanogramExportTemplateFieldInfoList, PlanogramExportTemplateFieldInfo>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Constructor
        private PlanogramExportTemplateFieldInfoList() { } //force use of factory methods.
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new list of fields for the given file type.
        /// </summary>
        public static PlanogramExportTemplateFieldInfoList NewPlanogramExportTemplateFieldInfoList(PlanogramExportFileType? fileType, String version)
        {
            PlanogramExportTemplateFieldInfoList item = new PlanogramExportTemplateFieldInfoList();
            item.Create(fileType, version);
            return item;
        }

        #endregion

        #region Data Access

        /// <summary>
        /// Called when creating a new item
        /// </summary>
        private void Create(PlanogramExportFileType? fileType, String version)
        {
            RaiseListChangedEvents = false;

            //Add the fields for the requested type.
            switch (fileType)
            {
                case null:
                    AddCCMFields();
                    break;

                case PlanogramExportFileType.Spaceman:
                    AddSpacemanFields(version);
                    break;

                case PlanogramExportFileType.JDA:
                    AddSpacePlanningFields(version);
                    break;
                case PlanogramExportFileType.Apollo:
                    AddApolloFields(version);
                    break;

                default:
                    Debug.Fail("Unknown file Type when calling Create in PlanogramExportTemplateFieldInfoList.");
                    break;
            }

            RaiseListChangedEvents = true;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds a new item for the given field info.
        /// </summary>
        private void Add(ObjectFieldInfo fieldInfo, PlanogramFieldMappingType type)
        {
            this.Add(PlanogramExportTemplateFieldInfo.NewPlanogramExportTemplateFieldInfo(fieldInfo, type));
        }

        /// <summary>
        /// Adds fields for a ccm file.
        /// </summary>
        private void AddCCMFields()
        {
            Type oType;
            String oName;
            PlanogramFieldMappingType mapType;

            #region planogram fields

            oType = typeof(Planogram);
            oName = Planogram.FriendlyName;
            mapType = PlanogramFieldMappingType.Planogram;

            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, Planogram.NameProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, Planogram.LocationCodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, Planogram.LocationNameProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, Planogram.CategoryCodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, Planogram.CategoryNameProperty), mapType);

            //+ all custom fields
            foreach (var info in CustomAttributeData.EnumerateDisplayablePropertyInfos())
            {
                var field = ObjectFieldInfo.NewObjectFieldInfo(oType, oName, info);
                field.PropertyName = Planogram.CustomAttributesProperty.Name + "." + field.PropertyName;
                Add(field, mapType);
            }

            #endregion

            #region fixture fields

            oType = typeof(PlanogramFixture);
            oName = PlanogramFixture.FriendlyName;
            mapType = PlanogramFieldMappingType.Fixture;

            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramFixture.NameProperty), mapType);

            #endregion

            #region component fields

            oType = typeof(PlanogramComponent);
            oName = PlanogramComponent.FriendlyName;
            mapType = PlanogramFieldMappingType.Component;

            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.NameProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.IsMoveableProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.IsDisplayOnlyProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.CanAttachShelfEdgeLabelProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.RetailerReferenceCodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.BarCodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.ManufacturerProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.ManufacturerPartNameProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.ManufacturerPartNumberProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.SupplierNameProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.SupplierPartNumberProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.SupplierCostPriceProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.SupplierDiscountProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.SupplierLeadTimeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.MinPurchaseQtyProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.WeightLimitProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.WeightProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.VolumeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.DiameterProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramComponent.CapacityProperty), mapType);

            //+ all custom fields
            foreach (var info in CustomAttributeData.EnumerateDisplayablePropertyInfos())
            {
                var field = ObjectFieldInfo.NewObjectFieldInfo(oType, oName, info);
                field.PropertyName = PlanogramComponent.CustomAttributesProperty.Name + "." + field.PropertyName;
                Add(field, mapType);
            }
            #endregion

            #region product fields
            oType = typeof(PlanogramProduct);
            oName = PlanogramProduct.FriendlyName;
            mapType = PlanogramFieldMappingType.Product;

            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.GtinProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.NameProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.BrandProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.PointOfPurchaseHeightProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.PointOfPurchaseWidthProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.PointOfPurchaseDepthProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.StatusTypeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.IsPlaceHolderProductProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.IsActiveProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.ShapeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.PointOfPurchaseDescriptionProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.ShortDescriptionProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.SubcategoryProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.CustomerStatusProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.FillColourProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.FlavourProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.PackagingShapeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.PackagingTypeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.CountryOfOriginProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.CountryOfProcessingProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.ShelfLifeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.DeliveryFrequencyDaysProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.DeliveryMethodProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.VendorCodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.VendorProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.ManufacturerCodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.ManufacturerProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.SizeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.UnitOfMeasureProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.DateIntroducedProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.DateDiscontinuedProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.DateEffectiveProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.HealthProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.CorporateCodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.BarcodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.SellPriceProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.SellPackCountProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.SellPackDescriptionProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.RecommendedRetailPriceProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.ManufacturerRecommendedRetailPriceProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.CostPriceProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.CaseCostProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.TaxRateProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.ConsumerInformationProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.TextureProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.StyleNumberProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.PatternProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.ModelProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.GarmentTypeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.IsPrivateLabelProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.IsNewProductProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.FinancialGroupCodeProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.FinancialGroupNameProperty), mapType);
            Add(ObjectFieldInfo.NewObjectFieldInfo(oType, oName, PlanogramProduct.CasePackUnitsProperty), mapType);

            //+ all custom fields
            foreach (var info in CustomAttributeData.EnumerateDisplayablePropertyInfos())
            {
                var field = ObjectFieldInfo.NewObjectFieldInfo(oType, oName, info);
                field.PropertyName = PlanogramProduct.CustomAttributesProperty.Name + "." + field.PropertyName;
                Add(field, mapType);
            }
            #endregion
        }

        /// <summary>
        /// Adds fields for a spaceman file.
        /// </summary>
        /// <param name="version"></param>
        private void AddSpacemanFields(String version)
        {
            Dictionary<String, ObjectFieldInfo> sectionFields = SpacemanFieldHelper.GetSectionFieldInfos();
            Dictionary<String, ObjectFieldInfo> fixelFields = SpacemanFieldHelper.GetFixelFieldInfos();
            Dictionary<String, ObjectFieldInfo> productFields = SpacemanFieldHelper.GetProductFieldInfos();

            //product fields
            foreach (var productEntry in productFields)
            {
                Add(productEntry.Value, PlanogramFieldMappingType.Product);
                Add(productEntry.Value, PlanogramFieldMappingType.Performance);
            }

            //component fields
            // Just add all fixel fields
            foreach (var fixelEntry in fixelFields)
            {
                Add(fixelEntry.Value, PlanogramFieldMappingType.Component);
            }


            //bay fields
            // Just add all fixel fields
            foreach (var fixelEntry in fixelFields)
            {
                Add(fixelEntry.Value, PlanogramFieldMappingType.Fixture);
            }

            //planogram fields
            foreach (var sectionEntry in sectionFields)
            {
                Add(sectionEntry.Value, PlanogramFieldMappingType.Planogram);
            }

        }

        /// <summary>
        ///     Add to this instance's list all Space Planning fields exportable from a file.
        /// </summary>
        /// <param name="version"></param>
        private void AddSpacePlanningFields(String version)
        {
            foreach (KeyValuePair<String, ObjectFieldInfo> entry in JDAExportHelper.PlanogramFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Planogram);
            }

            foreach (KeyValuePair<String, ObjectFieldInfo> entry in JDAExportHelper.ComponentFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Component);
            }

            foreach (KeyValuePair<String, ObjectFieldInfo> entry in JDAExportHelper.FixtureFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Fixture);
            }

            foreach (KeyValuePair<String, ObjectFieldInfo> entry in JDAExportHelper.ProductFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Product);
            }

            foreach (KeyValuePair<String, ObjectFieldInfo> entry in JDAExportHelper.PerformanceFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Performance);
            }
        }

        /// <summary>
        ///     Add to this instance's list all Apollo fields exportable to a file.
        /// </summary>
        /// <param name="version"></param>
        private void AddApolloFields(String version)
        {
            foreach (KeyValuePair<String, ObjectFieldInfo> entry in ApolloExportHelper.PlanogramFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Planogram);
            }

            foreach (KeyValuePair<String, ObjectFieldInfo> entry in ApolloExportHelper.ComponentFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Component);
            }

            foreach (KeyValuePair<String, ObjectFieldInfo> entry in ApolloExportHelper.FixtureFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Fixture);
            }

            foreach (KeyValuePair<String, ObjectFieldInfo> entry in ApolloExportHelper.ProductFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Product);
            }
            foreach (KeyValuePair<String, ObjectFieldInfo> entry in ApolloExportHelper.PerformanceFieldInfos)
            {
                Add(entry.Value, PlanogramFieldMappingType.Performance);
            }
        }
        #endregion
    }
}
