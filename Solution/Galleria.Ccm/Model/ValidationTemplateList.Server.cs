﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)

// CCM-26474 : A.Silva ~ Created

#endregion

#endregion

using System;
using Csla;

namespace Galleria.Ccm.Model
{
    // Server-side code.
    public sealed partial class ValidationTemplateList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use the public factory methods to instantiate objects of <see cref="ValidationTemplateList" />
        ///     .
        /// </summary>
        private ValidationTemplateList()
        {
        }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Returns a <see cref="ValidationTemplateList"/> for the <see cref="Entity"/> with the given <paramref name="entityId"/>.
        /// </summary>
        /// <param name="entityId">Unique Id value for the <see cref="Entity"/> that contains the <see cref="ValidationTemplate"/> objects.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateList"/>.</returns>
        public static ValidationTemplateList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ValidationTemplateList>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion
    }
}