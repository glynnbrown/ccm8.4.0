﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
// V8-25526 : K.Pickup
//  Use RelationshipTypes.LazyLoad when registering lazy-loaded model properties.
// V8-25873 : A.Probyn
//  Added GetAllChildFolders helper method
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{

    /// <summary>
    /// Folder model object
    /// </summary>
    [Serializable]
    public sealed partial class Folder : ModelObject<Folder>
    {
        #region Nested Classes

        /// <summary>
        /// Denotes the available dalfactories types.
        /// </summary>
        public enum FolderDalFactoryType
        {
            Unknown = 0,
            FileSystem = 1,
        }


        #endregion

        #region Parent

        /// <summary>
        /// Gets the parent folder
        /// </summary>
        public new Folder Parent
        {
            get 
            {
                FolderList parentList = base.Parent as FolderList;
                if (parentList != null)
                {
                    return parentList.Parent as Folder; 
                }
                return null;
            }
        }

        #endregion

        #region Properties

        #region DalFactoryName
        /// <summary>
        /// DalFactoryName property definition
        /// </summary>
        private static readonly ModelPropertyInfo<String> DalFactoryNameProperty =
            RegisterModelProperty<String>(c => c.DalFactoryName);
        /// <summary>
        /// Returns the dal factory name
        /// </summary>
        protected override String DalFactoryName
        {
            get 
            {
                if (this.Parent == null)
                {
                    return this.GetProperty<String>(DalFactoryNameProperty); 
                }
                else
                {
                    return base.DalFactoryName;
                }
            }
        }
        #endregion


        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
            set { SetProperty<Object>(IdProperty, value); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region FileSystemPath

        /// <summary>
        /// FileSystemPath property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FileSystemPathProperty =
            RegisterModelProperty<String>(c => c.FileSystemPath);

        /// <summary>
        /// Gets/Sets the FileSystemPath value
        /// </summary>
        public String FileSystemPath
        {
            get { return GetProperty<String>(FileSystemPathProperty); }
            set { SetProperty<String>(FileSystemPathProperty, value); }
        }

        #endregion

        #region Child Folders

        /// <summary>
        /// ChildFolders property definition
        /// </summary>
        public static readonly ModelPropertyInfo<FolderList> ChildFoldersProperty =
            RegisterModelProperty<FolderList>(c => c.ChildFolders, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Returns the child folders of this one
        /// </summary>
        public FolderList ChildFolders
        {
            get
            {
                return this.GetPropertyLazy<FolderList>(
                    ChildFoldersProperty,
                    new FolderList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    true);
            }
        }

        /// <summary>
        /// Asynchronously returns the ChildFolders list
        /// </summary>
        public FolderList ChildFoldersAsync
        {
            get
            {
                return this.GetPropertyLazy<FolderList>(
                    ChildFoldersProperty,
                    new FolderList.FetchByParentIdCriteria(this.DalFactoryName, this.Id),
                    false);
            }
        }

        #endregion

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

        }

        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region DalType
            /// <summary>
            /// DalType property definition
            /// </summary>
            public static readonly PropertyInfo<FolderDalFactoryType> DalTypeProperty =
                RegisterProperty<FolderDalFactoryType>(c => c.DalType);
            /// <summary>
            /// Returns the dal type
            /// </summary>
            public FolderDalFactoryType DalType
            {
                get { return this.ReadProperty<FolderDalFactoryType>(DalTypeProperty); }
            }
            #endregion

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Object> IdProperty =
                RegisterProperty<Object>(c => c.Id);
            /// <summary>
            /// Returns the package id
            /// </summary>
            public Object Id
            {
                get { return this.ReadProperty<Object>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(FolderDalFactoryType dalType, Object id)
            {
                this.LoadProperty<FolderDalFactoryType>(DalTypeProperty, dalType);
                this.LoadProperty<Object>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new Folder
        /// </summary>
        public static Folder NewFolder()
        {
            Folder item = new Folder();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Object>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<FolderList>(ChildFoldersProperty, FolderList.NewFolderList());
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Object oldId = this.Id;
            Object newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Object>(IdProperty, newId);
            context.RegisterId<Folder>(oldId, newId);
        }

        /// <summary>
        /// Returns a list of this folder and  all child folders below
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Folder> GetAllChildFolders()
        {
            IEnumerable<Folder> returnList = new List<Folder>() { this };

            FolderList children = this.ChildFolders;
            if (children != null)
            {
                foreach (Folder child in children)
                {
                    returnList = returnList.Union(child.GetAllChildFolders());
                }
            }

            return returnList;
        }

        #endregion

    }

}