﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
#endregion

#region Version History: (CCM 8.3.0)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentProduct
    {
        #region Constructor
        private AssortmentProduct() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating an instance from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new instance of this type</returns>
        internal static AssortmentProduct GetAssortmentProduct(IDalContext dalContext, AssortmentProductDto dto)
        {
            return DataPortal.FetchChild<AssortmentProduct>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentProductDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(GtinProperty, dto.GTIN);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            LoadProperty<Boolean>(IsRangedProperty, dto.IsRanged);
            LoadProperty<Int16>(RankProperty, dto.Rank);
            LoadProperty<String>(SegmentationProperty, dto.Segmentation);
            LoadProperty<Byte>(FacingsProperty, dto.Facings);
            LoadProperty<Int16>(UnitsProperty, dto.Units);
            LoadProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty, (PlanogramAssortmentProductTreatmentType)dto.ProductTreatmentType);
            LoadProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty, (PlanogramAssortmentProductLocalizationType)dto.ProductLocalizationType);
            LoadProperty<String>(CommentsProperty, dto.Comments);
            LoadProperty<Byte?>(ExactListFacingsProperty, dto.ExactListFacings);
            LoadProperty<Int16?>(ExactListUnitsProperty, dto.ExactListUnits);
            LoadProperty<Byte?>(PreserveListFacingsProperty, dto.PreserveListFacings);
            LoadProperty<Int16?>(PreserveListUnitsProperty, dto.PreserveListUnits);
            LoadProperty<Byte?>(MaxListFacingsProperty, dto.MaxListFacings);
            LoadProperty<Int16?>(MaxListUnitsProperty, dto.MaxListUnits);
            LoadProperty<Byte?>(MinListFacingsProperty, dto.MinListFacings);
            LoadProperty<Int16?>(MinListUnitsProperty, dto.MinListUnits);
            LoadProperty<String>(FamilyRuleNameProperty, dto.FamilyRuleName);
            LoadProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty, (PlanogramAssortmentProductFamilyRuleType)dto.FamilyRuleType);
            LoadProperty<Byte?>(FamilyRuleValueProperty, dto.FamilyRuleValue);
            LoadProperty<Byte?>(FamilyRuleValueProperty, dto.FamilyRuleValue);
            LoadProperty<Boolean>(IsPrimaryRegionalProductProperty, dto.IsPrimaryRegionalProduct);
            LoadProperty<DateTime?>(DateDeletedProperty, dto.ProductDateDeleted);
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <returns>A data transfer object</returns>
        private AssortmentProductDto GetDataTransferObject(Assortment parent)
        {
            return new AssortmentProductDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                GTIN = ReadProperty<String>(GtinProperty),
                Name = ReadProperty<String>(NameProperty),
                ProductId = ReadProperty<Int32>(ProductIdProperty),
                IsRanged = ReadProperty<Boolean>(IsRangedProperty),
                Rank = ReadProperty<Int16>(RankProperty),
                Segmentation = ReadProperty<String>(SegmentationProperty),
                Facings = ReadProperty<Byte>(FacingsProperty),
                Units = ReadProperty<Int16>(UnitsProperty),
                ProductTreatmentType = (Byte)ReadProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty),
                ProductLocalizationType = (Byte)ReadProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty),
                Comments = ReadProperty<String>(CommentsProperty),
                ExactListFacings = ReadProperty<Byte?>(ExactListFacingsProperty),
                ExactListUnits = ReadProperty<Int16?>(ExactListUnitsProperty),
                PreserveListFacings = ReadProperty<Byte?>(PreserveListFacingsProperty),
                PreserveListUnits = ReadProperty<Int16?>(PreserveListUnitsProperty),
                MaxListFacings = ReadProperty<Byte?>(MaxListFacingsProperty),
                MaxListUnits = ReadProperty<Int16?>(MaxListUnitsProperty),
                MinListFacings = ReadProperty<Byte?>(MinListFacingsProperty),
                MinListUnits = ReadProperty<Int16?>(MinListUnitsProperty),
                FamilyRuleName = ReadProperty<String>(FamilyRuleNameProperty),
                FamilyRuleType = (Byte)ReadProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty),
                FamilyRuleValue = ReadProperty<Byte?>(FamilyRuleValueProperty),
                IsPrimaryRegionalProduct = ReadProperty<Boolean>(IsPrimaryRegionalProductProperty),
                ProductDateDeleted = ReadProperty<DateTime?>(DateDeletedProperty),
                AssortmentId = parent.Id
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentProductDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Assortment parent)
        {
            AssortmentProductDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IAssortmentProductDal dal = dalContext.GetDal<IAssortmentProductDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<AssortmentProduct>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Assortment parent)
        {
            if (this.IsSelfDirty)
            {
                using (IAssortmentProductDal dal = dalContext.GetDal<IAssortmentProductDal>())
                {
                    dal.Update(GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext, Assortment parent)
        {
            using (IAssortmentProductDal dal = dalContext.GetDal<IAssortmentProductDal>())
            {
                dal.DeleteById(this.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}