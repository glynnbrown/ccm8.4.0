﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class FixtureAssembly
    {
        #region Constructor
        private FixtureAssembly() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static FixtureAssembly Fetch(IDalContext dalContext, FixtureAssemblyDto dto)
        {
            return DataPortal.FetchChild<FixtureAssembly>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FixtureAssemblyDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Int16>(NumberOfComponentsProperty, dto.NumberOfComponents);
            this.LoadProperty<Single>(TotalComponentCostProperty, dto.TotalComponentCost);
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private FixtureAssemblyDto GetDataTransferObject(FixturePackage parent)
        {
            return new FixtureAssemblyDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                FixturePackageId = parent.Id,
                Name = ReadProperty<String>(NameProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                Depth = ReadProperty<Single>(DepthProperty),
                NumberOfComponents = ReadProperty<Int16>(NumberOfComponentsProperty),
                TotalComponentCost = ReadProperty<Single>(TotalComponentCostProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, FixtureAssemblyDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, FixturePackage parent)
        {
            FixtureAssemblyDto dto = this.GetDataTransferObject(parent);
            Object oldId = dto.Id;
            using (IFixtureAssemblyDal dal = dalContext.GetDal<IFixtureAssemblyDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<FixtureAssembly>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, FixturePackage parent)
        {
            if (this.IsSelfDirty)
            {
                using (IFixtureAssemblyDal dal = dalContext.GetDal<IFixtureAssemblyDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, FixturePackage parent)
        {
            using (IFixtureAssemblyDal dal = dalContext.GetDal<IFixtureAssemblyDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}