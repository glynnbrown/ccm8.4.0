﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of LocationLevel objects.
    /// </summary>
    [Serializable]
    public sealed partial class LocationLevelList : ModelList<LocationLevelList, LocationLevel>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationLevelList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationLevelList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationLevelList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationLevelList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationHierarchyEdit.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static LocationLevelList NewLocationLevelList()
        {
            LocationLevelList item = new LocationLevelList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}