﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
// V8-27546 : A.Kuszyk
//  Amended Remove method to check dividers for overlap.
// V8-27957 : N.Foster
//  Implement CCM security
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Helpers;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of BlockingDivider objects.
    /// </summary>
    [Serializable]
    public sealed partial class BlockingDividerList : ModelList<BlockingDividerList, BlockingDivider>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new Blocking Parent
        {
            get { return (Blocking)base.Parent; }
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(BlockingDividerList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.BlockingGet.ToString()));
            BusinessRules.AddRule(typeof(BlockingDividerList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.BlockingCreate.ToString()));
            BusinessRules.AddRule(typeof(BlockingDividerList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.BlockingEdit.ToString()));
            BusinessRules.AddRule(typeof(BlockingDividerList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.BlockingDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static BlockingDividerList NewBlockingDividerList()
        {
            BlockingDividerList item = new BlockingDividerList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods

        protected override BlockingDivider AddNewCore()
        {
            BlockingDivider divider = BlockingDivider.NewBlockingDivider();
            this.Add(divider);
            return divider;
        }

        /// <summary>
        /// Adds a new divider to this list at the given position ensuring that groups and
        /// locations are created.
        /// </summary>
        /// <param name="x">The x position percent value between 0 and 1</param>
        /// <param name="y">The yposition percent value between 0 and 1</param>
        /// <param name="type">The type of divider to add</param>
        /// <returns></returns>
        public BlockingDivider Add(Single x, Single y, PlanogramBlockingDividerType type)
        {
            Blocking parentBlocking = this.Parent;
            if (parentBlocking == null) return AddNew();

            //create and add the new divider
            Single dividerX, dividerY, dividerLength;
            Byte dividerLevel;
            BlockingHelper.GetDividerValues(parentBlocking, x, y, type, out dividerX, out dividerY, out dividerLength, out dividerLevel);

            BlockingDivider newDivider = BlockingDivider.NewBlockingDivider(dividerLevel, type, dividerX, dividerY, dividerLength);
            this.Add(newDivider);

            if (parentBlocking.Locations.Count > 0)
            {
                //determine which locations the new divider intersects
                foreach (BlockingLocation loc in parentBlocking.Locations.ToList())
                {
                    if (BlockingHelper.IntersectsWith(newDivider, loc))
                    {
                        //Add a new group
                        BlockingGroup newGroup = parentBlocking.Groups.AddNew();
                        BlockingLocation newLoc = parentBlocking.Locations.Add(newGroup);

                        //update the adjacent dividers of the locations
                        if (type == PlanogramBlockingDividerType.Vertical)
                        {
                            Int32? oldRightId = loc.BlockingDividerRightId;
                            loc.BlockingDividerRightId = newDivider.Id;

                            newLoc.BlockingDividerLeftId = newDivider.Id;
                            newLoc.BlockingDividerRightId = oldRightId;
                            newLoc.BlockingDividerTopId = loc.BlockingDividerTopId;
                            newLoc.BlockingDividerBottomId = loc.BlockingDividerBottomId;
                        }
                        else
                        {
                            Int32? oldTopId = loc.BlockingDividerTopId;
                            loc.BlockingDividerTopId = newDivider.Id;

                            newLoc.BlockingDividerTopId = oldTopId;
                            newLoc.BlockingDividerBottomId = newDivider.Id;
                            newLoc.BlockingDividerLeftId = loc.BlockingDividerLeftId;
                            newLoc.BlockingDividerRightId = loc.BlockingDividerRightId;
                        }
                    }
                }
            }
            else
            {
                //add our first 2 groups
                BlockingGroup group1 = parentBlocking.Groups.AddNew();
                BlockingLocation loc1 = parentBlocking.Locations.Add(group1);

                BlockingGroup group2 = parentBlocking.Groups.AddNew();
                BlockingLocation loc2 = parentBlocking.Locations.Add(group2);

                if (type == PlanogramBlockingDividerType.Vertical)
                {
                    loc1.BlockingDividerRightId = newDivider.Id;
                    loc2.BlockingDividerLeftId = newDivider.Id;
                }
                else
                {
                    loc1.BlockingDividerTopId = newDivider.Id;
                    loc2.BlockingDividerBottomId = newDivider.Id;
                }
            }

            return newDivider;
        }

        /// <summary>
        /// Removes the given divider from the list
        /// </summary>
        /// <param name="divider"></param>
        /// <returns></returns>
        public new Boolean Remove(BlockingDivider divider)
        {
            if (divider == null) return false;
            Blocking parentBlocking = this.Parent;
            if (parentBlocking == null) return base.Remove(divider);
            if (!this.Contains(divider)) return false;

            Boolean result = true;

            List<BlockingDivider> dividersToRemove = new List<BlockingDivider>();
            BlockingDivider nextDividerAlong = BlockingHelper.GetNextDivider(divider, parentBlocking);

            if (divider.Type == PlanogramBlockingDividerType.Vertical)
            {
                Single maxX = (nextDividerAlong != null)?  nextDividerAlong.X : 1;

                //get all dividers in the band
                foreach (BlockingDivider d in parentBlocking.Dividers)
                {
                    //is under the level of the divider to remove
                    if (d.Level > divider.Level)
                    {
                        if (d.Type == PlanogramBlockingDividerType.Horizontal)
                        {
                            //is horizontal and starts and finishes within bounds
                            if (d.X >= divider.X && (d.X + d.Length) <= maxX && divider.GetOverlap(d)>0)
                            {
                                dividersToRemove.Add(d);
                            }
                        }
                        else
                        {
                            //is vertical and starts and finishes within bounds.
                            if (d.X > divider.X && d.X < maxX && divider.GetOverlap(d) > 0)
                            {
                                dividersToRemove.Add(d);
                            }
                        }
                    }
                }

            }
            else
            {
                Single maxY = (nextDividerAlong != null)?  nextDividerAlong.Y : 1;


                //get all dividers in the band
                foreach (BlockingDivider d in parentBlocking.Dividers)
                {
                    //is under the level of the divider to remove
                    if (d.Level > divider.Level)
                    {
                        if (d.Type == PlanogramBlockingDividerType.Vertical)
                        {
                            //is vertical and starts and finishes within bounds
                            if (d.Y >= divider.Y && (d.Y + d.Length) <= maxY && divider.GetOverlap(d) > 0)
                            {
                                dividersToRemove.Add(d);
                            }
                        }
                        else
                        {
                            //is horizontal and starts and finishes within bounds.
                            if (d.Y > divider.Y && d.Y < maxY && divider.GetOverlap(d) > 0)
                            {
                                dividersToRemove.Add(d);
                            }
                        }
                    }
                }
            }

            
            //add this divider to be removed
            dividersToRemove.Add(divider);

            //get a list of locations associated with any of the dividers
            List<Int32> dividerIds = dividersToRemove.Select(d => d.Id).ToList();

            //remove the locations no longer required and update any others that are affected.
            foreach (BlockingLocation l in parentBlocking.Locations.ToList())
            {
                if((l.BlockingDividerLeftId.HasValue && dividerIds.Contains(l.BlockingDividerLeftId.Value))
                        || (l.BlockingDividerBottomId.HasValue && dividerIds.Contains(l.BlockingDividerBottomId.Value)))
                {
                    parentBlocking.Locations.Remove(l);
                }
                else if (divider.Type == PlanogramBlockingDividerType.Vertical &&
                    l.BlockingDividerRightId.HasValue && dividerIds.Contains(l.BlockingDividerRightId.Value))
                {
                    l.BlockingDividerRightId = (nextDividerAlong != null)? (Int32?)nextDividerAlong.Id : null;
                }
                else if (divider.Type == PlanogramBlockingDividerType.Horizontal &&
                    l.BlockingDividerTopId.HasValue && dividerIds.Contains(l.BlockingDividerTopId.Value))
                {
                    l.BlockingDividerTopId = (nextDividerAlong != null) ? (Int32?)nextDividerAlong.Id : null;
                }
            }


            //remove the dividers
            foreach (var div in dividersToRemove)
            {
                base.Remove(div);
            }


            //cycle through tidying up block groups
            foreach (BlockingGroup group in parentBlocking.Groups.ToList())
            {
                if (group.GetBlockingLocations().Count == 0)
                {
                    parentBlocking.Groups.Remove(group);
                }
            }

            //update the lengths of all remaining dividers
            foreach (BlockingDivider div in parentBlocking.Dividers)
            {
                div.UpdateLength();
            }

            return result;
        }

        #endregion
    }
}