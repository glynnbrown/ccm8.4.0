﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26891 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class BlockingGroupList
    {
        #region Constructor
        private BlockingGroupList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches the list by its parent id
        /// </summary>
        internal static BlockingGroupList FetchByBlockingId(IDalContext dalContext, Int32 blockingId)
        {
            return DataPortal.FetchChild<BlockingGroupList>(dalContext, blockingId);
        }

        #endregion

        #region Data Access

        #region Fetch


        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;
            using (IBlockingGroupDal dal = dalContext.GetDal<IBlockingGroupDal>())
            {
                IEnumerable<BlockingGroupDto> dtoList = dal.FetchByBlockingId(parentId);
                foreach (BlockingGroupDto dto in dtoList)
                {
                    this.Add(BlockingGroup.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}