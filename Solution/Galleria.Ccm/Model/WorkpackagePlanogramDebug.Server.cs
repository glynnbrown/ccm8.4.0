﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26317 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class WorkpackagePlanogramDebug
    {
        #region Constructors
        private WorkpackagePlanogramDebug() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Fetches an existing instance of this type
        /// </summary>
        internal static WorkpackagePlanogramDebug GetWorkpackagePlanogramDebug(IDalContext dalContext, WorkpackagePlanogramDebugDto dto)
        {
            return DataPortal.FetchChild<WorkpackagePlanogramDebug>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, WorkpackagePlanogramDebugDto dto)
        {
            this.LoadProperty<Int32>(SourcePlanogramIdProperty, dto.SourcePlanogramId);
            this.LoadProperty<Int32>(WorkflowTaskIdProperty, dto.WorkflowTaskId);
            this.LoadProperty<Int32>(DebugPlanogramIdProperty, dto.DebugPlanogramId);
        }

        /// <summary>
        /// Creates a dto from this instance
        /// </summary>
        private WorkpackagePlanogramDebugDto GetDataTransferObject()
        {
            return new WorkpackagePlanogramDebugDto()
            {
                SourcePlanogramId = this.ReadProperty<Int32>(SourcePlanogramIdProperty),
                WorkflowTaskId = this.ReadProperty<Int32>(WorkflowTaskIdProperty),
                DebugPlanogramId = this.ReadProperty<Int32>(DebugPlanogramIdProperty)
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching an existing instance of this type
        /// </summary>
        private void Child_Fetch(IDalContext dalContext, WorkpackagePlanogramDebugDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting a new item of this type
        /// </summary>
        private void Child_Insert(IDalContext dalContext)
        {
            using (IWorkpackagePlanogramDebugDal dal = dalContext.GetDal<IWorkpackagePlanogramDebugDal>())
            {
                dal.Insert(this.GetDataTransferObject());
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an item of this type
        /// </summary>
        private void Child_Update(IDalContext dalContext)
        {
            using (IWorkpackagePlanogramDebugDal dal = dalContext.GetDal<IWorkpackagePlanogramDebugDal>())
            {
                dal.Update(this.GetDataTransferObject());
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IWorkpackagePlanogramDebugDal dal = dalContext.GetDal<IWorkpackagePlanogramDebugDal>())
            {
                dal.DeleteBySourcePlanogramIdWorkflowTaskId(
                    this.ReadProperty<Int32>(SourcePlanogramIdProperty),
                    this.ReadProperty<Int32>(WorkflowTaskIdProperty));
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}
