﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015
#region Version History: (CCM 8.3.0)
// V8-31831 : A.Probyn
//  Created
#endregion
#endregion

using System.Collections.Generic;
using System;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum PlanogramNameTemplatePlanogramAttributeType
    {
        PlanogramName = 0,
        Text1 = 1,
        Text2 = 2,
        Text3 = 3,
        Text4 = 4,
        Text5 = 5,
        Text6 = 6,
        Text7 = 7,
        Text8 = 8,
        Text9 = 9,
        Text10 = 10,
        Text11 = 11,
        Text12 = 12,
        Text13 = 13,
        Text14 = 14,
        Text15 = 15,
        Text16 = 16,
        Text17 = 17,
        Text18 = 18,
        Text19 = 19,
        Text20 = 20,
        Text21 = 21,
        Text22 = 22,
        Text23 = 23,
        Text24 = 24,
        Text25 = 25,
        Text26 = 26,
        Text27 = 27,
        Text28 = 28,
        Text29 = 29,
        Text30 = 30,
        Text31 = 31,
        Text32 = 32,
        Text33 = 33,
        Text34 = 34,
        Text35 = 35,
        Text36 = 36,
        Text37 = 37,
        Text38 = 38,
        Text39 = 39,
        Text40 = 40,
        Text41 = 41,
        Text42 = 42,
        Text43 = 43,
        Text44 = 44,
        Text45 = 45,
        Text46 = 46,
        Text47 = 47,
        Text48 = 48,
        Text49 = 49,
        Text50 = 50
    }

    public static class PlanogramNameTemplatePlanogramAttributeTypeHelper
    {
        public static readonly Dictionary<PlanogramNameTemplatePlanogramAttributeType, String> FriendlyNames =
            new Dictionary<PlanogramNameTemplatePlanogramAttributeType, String>
            {
                {PlanogramNameTemplatePlanogramAttributeType.PlanogramName, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_PlanogramName},
                {PlanogramNameTemplatePlanogramAttributeType.Text1, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text1},
                {PlanogramNameTemplatePlanogramAttributeType.Text2, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text2},
                {PlanogramNameTemplatePlanogramAttributeType.Text3, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text3},
                {PlanogramNameTemplatePlanogramAttributeType.Text4, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text4},
                {PlanogramNameTemplatePlanogramAttributeType.Text5, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text5},
                {PlanogramNameTemplatePlanogramAttributeType.Text6, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text6},
                {PlanogramNameTemplatePlanogramAttributeType.Text7, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text7},
                {PlanogramNameTemplatePlanogramAttributeType.Text8, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text8},
                {PlanogramNameTemplatePlanogramAttributeType.Text9, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text9},
                {PlanogramNameTemplatePlanogramAttributeType.Text10, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text10},
                {PlanogramNameTemplatePlanogramAttributeType.Text11, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text11},
                {PlanogramNameTemplatePlanogramAttributeType.Text12, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text12},
                {PlanogramNameTemplatePlanogramAttributeType.Text13, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text13},
                {PlanogramNameTemplatePlanogramAttributeType.Text14, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text14},
                {PlanogramNameTemplatePlanogramAttributeType.Text15, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text15},
                {PlanogramNameTemplatePlanogramAttributeType.Text16, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text16},
                {PlanogramNameTemplatePlanogramAttributeType.Text17, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text17},
                {PlanogramNameTemplatePlanogramAttributeType.Text18, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text18},
                {PlanogramNameTemplatePlanogramAttributeType.Text19, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text19},
                {PlanogramNameTemplatePlanogramAttributeType.Text20, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text20},
                {PlanogramNameTemplatePlanogramAttributeType.Text21, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text21},
                {PlanogramNameTemplatePlanogramAttributeType.Text22, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text22},
                {PlanogramNameTemplatePlanogramAttributeType.Text23, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text23},
                {PlanogramNameTemplatePlanogramAttributeType.Text24, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text24},
                {PlanogramNameTemplatePlanogramAttributeType.Text25, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text25},
                {PlanogramNameTemplatePlanogramAttributeType.Text26, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text26},
                {PlanogramNameTemplatePlanogramAttributeType.Text27, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text27},
                {PlanogramNameTemplatePlanogramAttributeType.Text28, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text28},
                {PlanogramNameTemplatePlanogramAttributeType.Text29, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text29},
                {PlanogramNameTemplatePlanogramAttributeType.Text30, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text30},
                {PlanogramNameTemplatePlanogramAttributeType.Text31, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text31},
                {PlanogramNameTemplatePlanogramAttributeType.Text32, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text32},
                {PlanogramNameTemplatePlanogramAttributeType.Text33, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text33},
                {PlanogramNameTemplatePlanogramAttributeType.Text34, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text34},
                {PlanogramNameTemplatePlanogramAttributeType.Text35, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text35},
                {PlanogramNameTemplatePlanogramAttributeType.Text36, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text36},
                {PlanogramNameTemplatePlanogramAttributeType.Text37, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text37},
                {PlanogramNameTemplatePlanogramAttributeType.Text38, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text38},
                {PlanogramNameTemplatePlanogramAttributeType.Text39, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text39},
                {PlanogramNameTemplatePlanogramAttributeType.Text40, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text40},
                {PlanogramNameTemplatePlanogramAttributeType.Text41, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text41},
                {PlanogramNameTemplatePlanogramAttributeType.Text42, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text42},
                {PlanogramNameTemplatePlanogramAttributeType.Text43, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text43},
                {PlanogramNameTemplatePlanogramAttributeType.Text44, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text44},
                {PlanogramNameTemplatePlanogramAttributeType.Text45, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text45},
                {PlanogramNameTemplatePlanogramAttributeType.Text46, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text46},
                {PlanogramNameTemplatePlanogramAttributeType.Text47, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text47},
                {PlanogramNameTemplatePlanogramAttributeType.Text48, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text48},
                {PlanogramNameTemplatePlanogramAttributeType.Text49, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text49},
                {PlanogramNameTemplatePlanogramAttributeType.Text50, Message.Enum_PlanogramNameTemplatePlanogramAttributeType_Text50}

            };
    }
}
