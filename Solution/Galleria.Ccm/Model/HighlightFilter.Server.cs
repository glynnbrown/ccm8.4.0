﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-V8-24801 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class HighlightFilter
    {
        #region Constructor
        private HighlightFilter() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static HighlightFilter Fetch(IDalContext dalContext, HighlightFilterDto dto)
        {
            return DataPortal.FetchChild<HighlightFilter>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, HighlightFilterDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(FieldProperty, dto.Field);
            this.LoadProperty<HighlightFilterType>(TypeProperty, (HighlightFilterType)dto.Type);
            this.LoadProperty<String>(ValueProperty, dto.Value);

        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private HighlightFilterDto GetDataTransferObject(Highlight parent)
        {
            return new HighlightFilterDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                HighlightId = parent.Id,
                Field = ReadProperty<String>(FieldProperty),
                Type = (Byte)ReadProperty<HighlightFilterType>(TypeProperty),
                Value = ReadProperty<String>(ValueProperty),

            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, HighlightFilterDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Highlight parent)
        {
            HighlightFilterDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IHighlightFilterDal dal = dalContext.GetDal<IHighlightFilterDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<HighlightFilter>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Highlight parent)
        {
            if (this.IsSelfDirty)
            {
                using (IHighlightFilterDal dal = dalContext.GetDal<IHighlightFilterDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext, Highlight parent)
        {
            using (IHighlightFilterDal dal = dalContext.GetDal<IHighlightFilterDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}