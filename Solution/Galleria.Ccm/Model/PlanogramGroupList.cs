﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25587 : L.Ineson
//	Created (Auto-generated)
// V8-25787 : N.Foster
//  Added Contains method for searching by name
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of PlanogramGroup objects.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramGroupList : ModelList<PlanogramGroupList, PlanogramGroup>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramGroupList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramGroupList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.PlanogramHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(PlanogramGroupList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.PlanogramHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(PlanogramGroupList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.PlanogramHierarchyEdit.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramGroupList NewPlanogramGroupList()
        {
            PlanogramGroupList item = new PlanogramGroupList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Indicates if the specified name exists within the list
        /// </summary>
        public Boolean Contains(String name)
        {
            foreach (var item in this)
            {
                if (item.Name == name) return true;
            }
            return false;
        }
        #endregion
    }
}