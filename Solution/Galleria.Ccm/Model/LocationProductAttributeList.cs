﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-27630 : I.George
// Removed Unused Factory methods
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing LocationProductAttribute objects
    /// </summary>
    [Serializable]
    public partial class LocationProductAttributeList : ModelList<LocationProductAttributeList, LocationProductAttribute>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationProductAttributeList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationProductAttributeCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationProductAttributeGet.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationProductAttributeEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationProductAttributeList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationProductAttributeDelete.ToString()));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating a new list
        /// </summary>
        /// <returns>A newlist</returns>
        public static LocationProductAttributeList NewList()
        {
            LocationProductAttributeList item = new LocationProductAttributeList();
            item.Create();
            return item;
        }
        #endregion

        #region Criteria
        /// <summary>
        /// Criteria for FetchByEntityId
        /// </summary>
        [Serializable]
        public class FetchByEntityIdCriteria : Csla.CriteriaBase<FetchByEntityIdCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// Entity id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Entity Id
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="entityId">The entity id</param>
            public FetchByEntityIdCriteria(Int32 entityId)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
            }
            #endregion
        }

        /// <summary>
        /// Criteria for FetchByLocationIdProductIdCombinations
        /// </summary>
        [Serializable]
        public class FetchByLocationIdProductIdCombinationsCriteria : Csla.CriteriaBase<FetchByLocationIdProductIdCombinationsCriteria>
        {
            #region Properties

            #region EntityId
            /// <summary>
            /// Entity id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            /// <summary>
            /// Entity id
            /// </summary>
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }
            #endregion

            #region Combinations
            /// <summary>
            /// Combinations property definition
            /// </summary>
            public static readonly PropertyInfo<IEnumerable<Tuple<Int16, Int32>>> CombinationsProperty =
                RegisterProperty<IEnumerable<Tuple<Int16, Int32>>>(c => c.Combinations);
            /// <summary>
            /// Combinations
            /// </summary>
            public IEnumerable<Tuple<Int16, Int32>> Combinations
            {
                get { return ReadProperty<IEnumerable<Tuple<Int16, Int32>>>(CombinationsProperty); }
            }
            #endregion

            #region AllowCreate
            /// <summary>
            /// AllowCreate property definition - 
            /// denotes whether new attributes should be added if none exist for a location/product combination
            /// </summary>
            public static readonly PropertyInfo<Boolean> AllowCreateProperty =
                RegisterProperty<Boolean>(c => c.AllowCreate);
            /// <summary>
            ///AllowCreate
            /// </summary>
            public Boolean AllowCreate
            {
                get { return ReadProperty<Boolean>(AllowCreateProperty); }
            }
            #endregion


            #endregion

            #region Constructors

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="combinationsList">The combination list</param>
            public FetchByLocationIdProductIdCombinationsCriteria(Int32 entityId, IEnumerable<Tuple<Int16, Int32>> combinationsList)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IEnumerable<Tuple<Int16, Int32>>>(CombinationsProperty, combinationsList);
                LoadProperty<Boolean>(AllowCreateProperty, true);
            }

            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="combinationsList">The combination list</param>
            public FetchByLocationIdProductIdCombinationsCriteria(Int32 entityId, IEnumerable<Tuple<Int16, Int32>> combinationsList, Boolean allowCreate)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IEnumerable<Tuple<Int16, Int32>>>(CombinationsProperty, combinationsList);
                LoadProperty<Boolean>(AllowCreateProperty, allowCreate);
            }
            #endregion
        }
       
        #endregion
    }
}
