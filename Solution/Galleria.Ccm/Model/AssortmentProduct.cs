﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
// GFS-26285 : J.Pickup
//  Amended create overrides to Load Gtin property
// V8-26765 : A.Kuszyk
//  Changed the use of AssortmentProduct enums to their Planogram equivilants.
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion

#region Version History: (CCM 8.3.0)
// V8-32396 : A.Probyn ~ Updated GTIN references to Gtin & updated for Delist family rule
// CCM-14014 : M.Pettit
//  Added EnumerateDisplayableFieldInfos method
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using AutoMapper;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Rules;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Helpers;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// A class representing an Assortment Product within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    [Serializable]
    [DefaultNewMethod("NewAssortmentProduct")]
    public partial class AssortmentProduct : ModelObject<AssortmentProduct>, IPlanogramAssortmentProduct
    {
        #region Static Constructor
        static AssortmentProduct()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentProduct id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<String> GtinProperty =
            RegisterModelProperty<String>(c => c.Gtin);
        /// <summary>
        /// The unique product code
        /// </summary>
        public String Gtin
        {
            get { return GetProperty<String>(GtinProperty); }
            set { SetProperty<String>(GtinProperty, value); }
        }


        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The assortment product name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int32> ProductIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductId);
        /// <summary>
        /// The product id
        /// </summary>
        public Int32 ProductId
        {
            get { return GetProperty<Int32>(ProductIdProperty); }
            set { SetProperty<Int32>(ProductIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<Boolean> IsRangedProperty =
            RegisterModelProperty<Boolean>(c => c.IsRanged, Message.AssortmentProduct_IsRanged, Message.AssortmentProduct_IsRanged, true);
        /// <summary>
        /// The AssortmentProduct IsRanged
        /// </summary>
        public Boolean IsRanged
        {
            get { return GetProperty<Boolean>(IsRangedProperty); }
            set { SetProperty<Boolean>(IsRangedProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int16> RankProperty =
            RegisterModelProperty<Int16>(c => c.Rank, Message.AssortmentProduct_Rank);
        /// <summary>
        /// The AssortmentProduct Rank
        /// </summary>
        public Int16 Rank
        {
            get { return GetProperty<Int16>(RankProperty); }
            set { SetProperty<Int16>(RankProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> SegmentationProperty =
            RegisterModelProperty<String>(c => c.Segmentation, Message.AssortmentProduct_Segmentation);
        /// <summary>
        /// The AssortmentProduct Segmentation
        /// </summary>
        public String Segmentation
        {
            get { return GetProperty<String>(SegmentationProperty); }
            set { SetProperty<String>(SegmentationProperty, value); }
        }

        public static readonly ModelPropertyInfo<Byte> FacingsProperty =
            RegisterModelProperty<Byte>(c => c.Facings, Message.AssortmentProduct_Facings);
        /// <summary>
        /// The AssortmentProduct Facings
        /// </summary>
        public Byte Facings
        {
            get { return GetProperty<Byte>(FacingsProperty); }
            set { SetProperty<Byte>(FacingsProperty, value); }
        }

        public static readonly ModelPropertyInfo<Int16> UnitsProperty =
            RegisterModelProperty<Int16>(c => c.Units, Message.AssortmentProduct_Units);
        /// <summary>
        /// The AssortmentProduct Units
        /// </summary>
        public Int16 Units
        {
            get { return GetProperty<Int16>(UnitsProperty); }
            set { SetProperty<Int16>(UnitsProperty, value); }
        }

        public static readonly ModelPropertyInfo<PlanogramAssortmentProductTreatmentType> ProductTreatmentTypeProperty =
            RegisterModelProperty<PlanogramAssortmentProductTreatmentType>(c => c.ProductTreatmentType, Message.AssortmentProduct_ProductTreatmentType,
                null, PlanogramAssortmentProductTreatmentType.Normal);
        /// <summary>
        /// The AssortmentProduct ProductTreatmentType
        /// </summary>
        public PlanogramAssortmentProductTreatmentType ProductTreatmentType
        {
            get { return GetProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductTreatmentType>(ProductTreatmentTypeProperty, value); }
        }

        public static readonly ModelPropertyInfo<PlanogramAssortmentProductLocalizationType> ProductLocalizationTypeProperty =
            RegisterModelProperty<PlanogramAssortmentProductLocalizationType>(c => c.ProductLocalizationType, Message.AssortmentProduct_ProductLocalizationType);
        /// <summary>
        /// The AssortmentProduct ProductLocalizationType (Regional or Local)
        /// </summary>
        public PlanogramAssortmentProductLocalizationType ProductLocalizationType
        {
            get { return GetProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty); }
            set { SetProperty<PlanogramAssortmentProductLocalizationType>(ProductLocalizationTypeProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> CommentsProperty =
            RegisterModelProperty<String>(c => c.Comments, Message.AssortmentProduct_Comments);
        /// <summary>
        /// The AssortmentProduct Comments
        /// </summary>
        public String Comments
        {
            get { return GetProperty<String>(CommentsProperty); }
            set { SetProperty<String>(CommentsProperty, value); }
        }

        public static readonly ModelPropertyInfo<Byte?> ExactListFacingsProperty =
            RegisterModelProperty<Byte?>(c => c.ExactListFacings);
        /// <summary>
        /// The AssortmentProduct Exact List Facings Product Rule
        /// </summary>
        public Byte? ExactListFacings
        {
            get { return GetProperty<Byte?>(ExactListFacingsProperty); }
            set
            {
                if (value != null)
                {
                    SetProperty<Byte?>(ExactListUnitsProperty, null);
                }
                SetProperty<Int16?>(ExactListFacingsProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Int16?> ExactListUnitsProperty =
            RegisterModelProperty<Int16?>(c => c.ExactListUnits);
        /// <summary>
        /// The exact list units product rule value.
        /// </summary>
        public Int16? ExactListUnits
        {
            get { return GetProperty<Int16?>(ExactListUnitsProperty); }
            set
            {
                if (value != null)
                {
                    SetProperty<Byte?>(ExactListFacingsProperty, null);
                }
                SetProperty<Int16?>(ExactListUnitsProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Byte?> PreserveListFacingsProperty =
            RegisterModelProperty<Byte?>(c => c.PreserveListFacings);
        /// <summary>
        /// The AssortmentProduct Preserve List Facings Product Rule
        /// </summary>
        public Byte? PreserveListFacings
        {
            get { return GetProperty<Byte?>(PreserveListFacingsProperty); }
            set
            {
                if (value != null)
                {
                    SetProperty<Int16?>(PreserveListUnitsProperty, null);
                }
                SetProperty<Byte?>(PreserveListFacingsProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Int16?> PreserveListUnitsProperty =
            RegisterModelProperty<Int16?>(c => c.PreserveListUnits);
        /// <summary>
        /// The preserve list units product rule value.
        /// </summary>
        public Int16? PreserveListUnits
        {
            get { return GetProperty<Int16?>(PreserveListUnitsProperty); }
            set
            {
                if (value != null)
                {
                    SetProperty<Byte?>(PreserveListFacingsProperty, null);
                }
                SetProperty<Int16?>(PreserveListUnitsProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Byte?> MaxListFacingsProperty =
            RegisterModelProperty<Byte?>(c => c.MaxListFacings);
        /// <summary>
        /// The AssortmentProduct Max List Facings Product Rule
        /// </summary>
        public Byte? MaxListFacings
        {
            get { return GetProperty<Byte?>(MaxListFacingsProperty); }
            set
            {
                if (value != null)
                {
                    SetProperty<Int16?>(MaxListUnitsProperty, null);
                }
                SetProperty<Byte?>(MaxListFacingsProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Int16?> MaxListUnitsProperty =
            RegisterModelProperty<Int16?>(c => c.MaxListUnits);
        /// <summary>
        /// The max list units product rule value.
        /// </summary>
        public Int16? MaxListUnits
        {
            get { return GetProperty<Int16?>(MaxListUnitsProperty); }
            set
            {
                if (value != null)
                {
                    SetProperty<Byte?>(MaxListFacingsProperty, null);
                }
                SetProperty<Int16?>(MaxListUnitsProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Byte?> MinListFacingsProperty =
            RegisterModelProperty<Byte?>(c => c.MinListFacings);
        /// <summary>
        /// The AssortmentProduct Min List Facings Product Rule
        /// </summary>
        public Byte? MinListFacings
        {
            get { return GetProperty<Byte?>(MinListFacingsProperty); }
            set
            {
                if (value != null)
                {
                    SetProperty<Int16?>(MinListUnitsProperty, null);
                }
                SetProperty<Byte?>(MinListFacingsProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<Int16?> MinListUnitsProperty =
            RegisterModelProperty<Int16?>(c => c.MinListUnits);
        /// <summary>
        /// The min list units product rule value.
        /// </summary>
        public Int16? MinListUnits
        {
            get { return GetProperty<Int16?>(MinListUnitsProperty); }
            set
            {
                if (value != null)
                {
                    SetProperty<Byte?>(MinListFacingsProperty, null);
                }
                SetProperty<Int16?>(MinListUnitsProperty, value);
            }
        }

        public static readonly ModelPropertyInfo<String> FamilyRuleNameProperty =
            RegisterModelProperty<String>(c => c.FamilyRuleName);
        /// <summary>
        /// The AssortmentProduct Family Rule name
        /// </summary>
        public String FamilyRuleName
        {
            get { return GetProperty<String>(FamilyRuleNameProperty); }
            set { SetProperty<String>(FamilyRuleNameProperty, value); }
        }

        public static readonly ModelPropertyInfo<PlanogramAssortmentProductFamilyRuleType> FamilyRuleTypeProperty =
            RegisterModelProperty<PlanogramAssortmentProductFamilyRuleType>(c => c.FamilyRuleType);
        /// <summary>
        /// The AssortmentProduct Family Rule type
        /// </summary>
        public PlanogramAssortmentProductFamilyRuleType FamilyRuleType
        {
            get { return GetProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty); }
            set
            {
                SetProperty<PlanogramAssortmentProductFamilyRuleType>(FamilyRuleTypeProperty, value);

                //Reset the value for the rule if type does not suppport it
                if (value == PlanogramAssortmentProductFamilyRuleType.DependencyList ||
                    value == PlanogramAssortmentProductFamilyRuleType.Delist)
                {
                    SetProperty<Int32?>(FamilyRuleValueProperty, null);
                }
            }
        }

        public static readonly ModelPropertyInfo<Byte?> FamilyRuleValueProperty =
            RegisterModelProperty<Byte?>(c => c.FamilyRuleValue);
        /// <summary>
        /// The AssortmentProduct Family Rule value (only applies if rule type is MaxProductCount or Hurdle)
        /// </summary>
        public Byte? FamilyRuleValue
        {
            get { return GetProperty<Byte?>(FamilyRuleValueProperty); }
            set { SetProperty<Byte?>(FamilyRuleValueProperty, value); }
        }

        public static readonly ModelPropertyInfo<Boolean> IsPrimaryRegionalProductProperty =
            RegisterModelProperty<Boolean>(c => c.IsPrimaryRegionalProduct);
        /// <summary>
        /// Is the Assortment product a primary regional product
        /// </summary>
        public Boolean IsPrimaryRegionalProduct
        {
            get { return GetProperty<Boolean>(IsPrimaryRegionalProductProperty); }
            set { SetProperty<Boolean>(IsPrimaryRegionalProductProperty, value); }
        }

        /// <summary>
        /// The date when the related product record was deleted 
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
        }

        #endregion

        #region Helper Properties

        public const String IsDelistedPropertyName = "IsDelisted";

        /// <summary>
        /// Returns true if the product is delisetd
        /// </summary>
        public Boolean IsDelisted
        {
            get { return ((this.ExactListFacings == 0) || (this.ExactListUnits == 0)); }
        }

        public Boolean HasMaximum
        {
            get { return ((this.MaxListUnits != null) || (this.MaxListFacings != null)); }
        }

        /// <summary>
        /// Returns the rule type that has been applied to the assortment product if any
        /// </summary>
        /// <returns></returns>
        public PlanogramAssortmentProductRuleType RuleType
        {
            get
            {
                if ((this.ExactListFacings != null) || (this.ExactListUnits != null))
                {
                    return PlanogramAssortmentProductRuleType.Force;
                }
                if ((this.PreserveListFacings != null) || (this.PreserveListUnits != null))
                {
                    return PlanogramAssortmentProductRuleType.Preserve;
                }
                if ((this.MinListFacings != null) || (this.MinListUnits != null))
                {
                    return PlanogramAssortmentProductRuleType.MinimumHurdle;
                }
                if (this.ProductTreatmentType == PlanogramAssortmentProductTreatmentType.Core)
                {
                    return PlanogramAssortmentProductRuleType.Core;
                }
                return PlanogramAssortmentProductRuleType.None;
            }
        }

        /// <summary>
        /// Returns the rule value type (Units or Facings) that has been applied to the assortment product if any
        /// </summary>
        /// <returns></returns>
        public PlanogramAssortmentProductRuleValueType RuleValueType
        {
            get
            {
                if ((this.ExactListFacings != null) || (this.PreserveListFacings != null) || (this.MinListFacings != null))
                {
                    return PlanogramAssortmentProductRuleValueType.Facings;
                }

                if ((this.ExactListUnits != null) || (this.PreserveListUnits != null) || (this.MinListUnits != null))
                {
                    return PlanogramAssortmentProductRuleValueType.Units;
                }
                return PlanogramAssortmentProductRuleValueType.Units;
            }
        }

        /// <summary>
        /// Returns the rule value type (Units or Facings) that has been applied to the assortment products max list if any
        /// </summary>
        /// <returns></returns>
        public PlanogramAssortmentProductRuleValueType RuleMaximumValueType
        {
            get
            {
                if (this.MaxListFacings != null)
                {
                    return PlanogramAssortmentProductRuleValueType.Facings;
                }

                if (this.MaxListUnits != null)
                {
                    return PlanogramAssortmentProductRuleValueType.Units;
                }
                return PlanogramAssortmentProductRuleValueType.Units;
            }
        }

        /// <summary>
        /// Returns the rule value that has been applied to the assortment product if any
        /// </summary>
        /// <returns></returns>
        public Int16? RuleValue
        {
            get
            {
                if (this.ExactListFacings != null) return (Int16)this.ExactListFacings;
                if (this.ExactListUnits != null) return (Int16)this.ExactListUnits;
                if (this.PreserveListFacings != null) return (Int16)this.PreserveListFacings;
                if (this.PreserveListUnits != null) return (Int16)this.PreserveListUnits;
                if (this.MinListFacings != null) return (Int16)this.MinListFacings;
                if (this.MinListUnits != null) return (Int16)this.MinListUnits;
                return null;
            }
        }

        /// <summary>
        /// Returns the rule max value that has been applied to the assortment product if any
        /// </summary>
        /// <returns></returns>
        public Int16? RuleMaximumValue
        {
            get
            {
                if (this.MaxListFacings != null) return (Int16)this.MaxListFacings;
                if (this.MaxListUnits != null) return (Int16)this.MaxListUnits;
                return null;
            }
        }

        /// <summary>
        /// Friendly description of the product rules applied to this product
        /// </summary>
        public String RuleSummary
        {
            get
            {
                String value = PlanogramAssortmentProductRuleTypeHelper.FriendlyNames[this.RuleType];

                if (this.RuleType == PlanogramAssortmentProductRuleType.Force && this.RuleValue == 0)
                {
                    value = Message.AssortmentProductRule_SummaryValue_Delisted;
                }
                else if (this.RuleType == PlanogramAssortmentProductRuleType.Core)
                {
                    value = PlanogramAssortmentProductTreatmentTypeHelper.FriendlyNames[PlanogramAssortmentProductTreatmentType.Core];
                }
                else
                {
                    //Get basic rule as friendly text
                    if (this.RuleType != PlanogramAssortmentProductRuleType.None)
                    {
                        value = String.Format(CultureInfo.CurrentCulture, "{0} {1} {2}",
                            value,
                            this.RuleValue,
                            PlanogramAssortmentProductRuleValueTypeHelper.FriendlyNames[this.RuleValueType]);
                    }

                    //add any maximums set
                    if (this.MaxListFacings != null)
                    {
                        value = String.Format(CultureInfo.CurrentCulture, "{0} {1} {2} {3}",
                            value,
                            Message.AssortmentProductRule_SummaryValue_Max,
                            this.MaxListFacings,
                            PlanogramAssortmentProductRuleValueTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleValueType.Facings]);
                    }
                    else if (this.MaxListUnits != null)
                    {
                        value = String.Format(CultureInfo.CurrentCulture, "{0} {1} {2} {3}",
                           value,
                           Message.AssortmentProductRule_SummaryValue_Max,
                           this.MaxListUnits,
                           PlanogramAssortmentProductRuleValueTypeHelper.FriendlyNames[PlanogramAssortmentProductRuleValueType.Units]);
                    }
                }


                return value;
            }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(ProductIdProperty));
            BusinessRules.AddRule(new Required(IsRangedProperty));

            //Facings and Units Rules
            BusinessRules.AddRule(new NullableMinValue<Byte>(PreserveListFacingsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Byte>(MinListFacingsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Byte>(MaxListFacingsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Int16>(PreserveListUnitsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Int16>(MinListUnitsProperty, 1));
            BusinessRules.AddRule(new NullableMinValue<Int16>(MaxListUnitsProperty, 1));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentProduct), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProduct), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProduct), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentProduct), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentProduct NewAssortmentProduct()
        {
            AssortmentProduct item = new AssortmentProduct();
            item.Create();
            return item;
        }


        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentProduct NewAssortmentProduct(Int32 productId)
        {
            AssortmentProduct item = new AssortmentProduct();
            item.Create(productId);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentProduct NewAssortmentProduct(Product product)
        {
            AssortmentProduct item = new AssortmentProduct();
            item.Create(product);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentProduct NewAssortmentProduct(ProductInfo productInfo)
        {
            AssortmentProduct item = new AssortmentProduct();
            item.Create(productInfo);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentProduct NewAssortmentProduct(ProductUniverseProduct productUniverseProduct)
        {
            AssortmentProduct item = new AssortmentProduct();
            item.Create(productUniverseProduct);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentProduct NewAssortmentProduct(ConsumerDecisionTreeNodeProduct consumerDecisionProduct)
        {
            AssortmentProduct item = new AssortmentProduct();
            item.Create(consumerDecisionProduct);
            return item;
        }

        #endregion

        #region Methods

        public void ClearRule()
        {
            this.ExactListFacings = null;
            this.ExactListUnits = null;
            this.PreserveListFacings = null;
            this.PreserveListUnits = null;
            this.MinListFacings = null;
            this.MinListUnits = null;
            this.MaxListFacings = null;
            this.MaxListUnits = null;
            this.ProductTreatmentType = PlanogramAssortmentProductTreatmentType.Normal;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Int32 productId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(ProductIdProperty, productId);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Product product)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(GtinProperty, product.Gtin);
            this.LoadProperty<String>(NameProperty, product.Name);
            this.LoadProperty<Int32>(ProductIdProperty, product.Id);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="productInfo"></param>
        private void Create(ProductInfo productInfo)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(GtinProperty, productInfo.Gtin);
            this.LoadProperty<String>(NameProperty, productInfo.Name);
            this.LoadProperty<Int32>(ProductIdProperty, productInfo.Id);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="productInfo"></param>
        private void Create(ProductUniverseProduct productUniverseProduct)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(GtinProperty, productUniverseProduct.Gtin);
            this.LoadProperty<String>(NameProperty, productUniverseProduct.Name);
            this.LoadProperty<Int32>(ProductIdProperty, productUniverseProduct.ProductId);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="productInfo"></param>
        private void Create(ConsumerDecisionTreeNodeProduct consumerDecisionProduct)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            //this.LoadProperty<String>(GTINProperty, consumerDecisionProduct.GTIN);
            //this.LoadProperty<String>(NameProperty, consumerDecisionProduct.Name);
            this.LoadProperty<Int32>(ProductIdProperty, consumerDecisionProduct.ProductId);

            // Get segmentation detail
            Byte segmentationDetailCount = 0;
            List<String> nodeNames = new List<String>();
            String segmentationDetail = String.Empty;
            //ConsumerDecisionTreeNode nodeDetail = consumerDecisionProduct.ParentNode;

            //while (!nodeDetail.IsRoot && segmentationDetailCount != 3)
            //{
            //    nodeNames.Add(nodeDetail.Name);
            //    nodeDetail = nodeDetail.ParentNode;
            //    segmentationDetailCount++;
            //}

            //root node name is excluded unless there are no child nodes.
            if (nodeNames.Count > 0)
            {
                for (int i = nodeNames.Count; i > 0; i--)
                {
                    segmentationDetail += nodeNames[i - 1] + " - ";
                }

                segmentationDetail = segmentationDetail.Substring(0, segmentationDetail.Length - 3);
            }
            else
            {
                //segmentationDetail = nodeDetail.Name;
            }
            this.LoadProperty<String>(SegmentationProperty, segmentationDetail);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

        #region FieldInfos
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(AssortmentProduct);
            String typeFriendly = AssortmentProduct.FriendlyName;

            String assortmentProductGroup = "Assortment Product"; // Message.AssortmentProduct_AssortmentProduct_Details;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, GtinProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NameProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsRangedProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, RankProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, SegmentationProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, UnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductTreatmentTypeProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ProductLocalizationTypeProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ExactListFacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ExactListUnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PreserveListFacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PreserveListUnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MaxListFacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MaxListUnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MinListFacingsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MinListUnitsProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FamilyRuleNameProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FamilyRuleTypeProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FamilyRuleValueProperty, assortmentProductGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IsPrimaryRegionalProductProperty, assortmentProductGroup);
        }
        #endregion

        #region Overrides

        public override string ToString()
        {
            return String.Format("{0} {1}", this.Gtin, this.Name);
        }

        #endregion
       
    }
}