﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26401 : A.Silva ~ Created.

#endregion

#endregion

using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class ValidationTemplateInfo
    {
        #region Constructors

        private ValidationTemplateInfo() { }

        #endregion

        #region Factory Methods

        /// <summary>
        ///     Request a <see cref="ValidationTemplateInfo"/> instance from its <see cref="dto"/>.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from.</param>
        /// <returns>A new instance of <see cref="ValidationTemplateInfo"/>.</returns>
        internal static ValidationTemplateInfo FetchValidationTemplateInfo(IDalContext dalContext,
            ValidationTemplateInfoDto dto)
        {
            return DataPortal.FetchChild<ValidationTemplateInfo>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object

        private void LoadDataTransferObject(IDalContext dalContext, ValidationTemplateInfoDto dto)
        {
            LoadProperty(IdProperty, dto.Id);
            LoadProperty(EntityIdProperty, dto.EntityId);
            LoadProperty(NameProperty, dto.Name);
        }

        #endregion

        #region Fetch

        /// <summary>
        ///     The data portal invokes this method via reflection when loading this object from a dto.
        /// </summary>
        /// <param name="dalContext"></param>
        /// <param name="dto">The dto to load from.</param>
        private void Child_Fetch(IDalContext dalContext, ValidationTemplateInfoDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion
    }
}
