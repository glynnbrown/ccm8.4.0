﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//		Created (Auto-generated)
// V8-25873 : A.Probyn
//  Added Custom Attribute 1 - 5
// V8-28089 : A.Probyn
//  Added missing (removed) Id load into DataPortal_Update
#endregion
#endregion

using System;
using System.IO;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using Galleria.Framework.Dal.Configuration;

namespace Galleria.Ccm.Model
{
    public partial class FixturePackage
    {
        #region Constants
        private const String _dalAssemblyName = "Galleria.Ccm.Dal.{0}.dll";
        #endregion

        #region Constructors
        private FixturePackage() { } //force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns the specified package
        /// </summary>
        public static FixturePackage FetchByFileName(String fileName)
        {
            return DataPortal.Fetch<FixturePackage>(new FetchCriteria(FixturePackageType.FileSystem, fileName));
        }

        /// <summary>
        /// Returns the package with the specified ID from the default DAL.  Created for the purpose of unit testing
        /// with the Mock DAL--not sure if this is something you'd need in real life or not.
        /// </summary>
        public static FixturePackage FetchById(Object id)
        {
            return DataPortal.Fetch<FixturePackage>(new FetchCriteria(FixturePackageType.Unknown, id));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, FixturePackageDto dto)
        {
            this.LoadProperty<Object>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(DescriptionProperty, dto.Description);
            this.LoadProperty<Single>(HeightProperty, dto.Height);
            this.LoadProperty<Single>(WidthProperty, dto.Width);
            this.LoadProperty<Single>(DepthProperty, dto.Depth);
            this.LoadProperty<Int32>(FixtureCountProperty, dto.FixtureCount);
            this.LoadProperty<Int32>(AssemblyCountProperty, dto.AssemblyCount);
            this.LoadProperty<Int32>(ComponentCountProperty, dto.ComponentCount);
            this.LoadProperty<Byte[]>(ThumbnailImageDataProperty, dto.ThumbnailImageData);
            this.LoadProperty<String>(CustomAttribute1Property, dto.CustomAttribute1);
            this.LoadProperty<String>(CustomAttribute2Property, dto.CustomAttribute2);
            this.LoadProperty<String>(CustomAttribute3Property, dto.CustomAttribute3);
            this.LoadProperty<String>(CustomAttribute4Property, dto.CustomAttribute4);
            this.LoadProperty<String>(CustomAttribute5Property, dto.CustomAttribute5);
            this.LoadProperty<FixtureLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty, (FixtureLengthUnitOfMeasureType)dto.LengthUnitsOfMeasure);
            this.LoadProperty<FixtureAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty, (FixtureAreaUnitOfMeasureType)dto.AreaUnitsOfMeasure);
            this.LoadProperty<FixtureVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty, (FixtureVolumeUnitOfMeasureType)dto.VolumeUnitsOfMeasure);
            this.LoadProperty<FixtureWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty, (FixtureWeightUnitOfMeasureType)dto.WeightUnitsOfMeasure);
            this.LoadProperty<FixtureAngleUnitOfMeasureType>(AngleUnitsOfMeasureProperty, (FixtureAngleUnitOfMeasureType)dto.AngleUnitsOfMeasure);
            this.LoadProperty<FixtureCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty, (FixtureCurrencyUnitOfMeasureType)dto.CurrencyUnitsOfMeasure);
            this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            this.LoadProperty<String>(CreatedByProperty, dto.CreatedBy);
            this.LoadProperty<String>(LastModifiedByProperty, dto.LastModifiedBy);

        }

        /// <summary>
        /// Creates a data transfer object from this instance
        /// </summary>
        private FixturePackageDto GetDataTransferObject()
        {
            return new FixturePackageDto
            {
                Id = ReadProperty<Object>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                Height = ReadProperty<Single>(HeightProperty),
                Width = ReadProperty<Single>(WidthProperty),
                Depth = ReadProperty<Single>(DepthProperty),
                FixtureCount = ReadProperty<Int32>(FixtureCountProperty),
                AssemblyCount = ReadProperty<Int32>(AssemblyCountProperty),
                ComponentCount = ReadProperty<Int32>(ComponentCountProperty),
                ThumbnailImageData = ReadProperty<Byte[]>(ThumbnailImageDataProperty),
                LengthUnitsOfMeasure = (Byte)ReadProperty<FixtureLengthUnitOfMeasureType>(LengthUnitsOfMeasureProperty),
                AreaUnitsOfMeasure = (Byte)ReadProperty<FixtureAreaUnitOfMeasureType>(AreaUnitsOfMeasureProperty),
                VolumeUnitsOfMeasure = (Byte)ReadProperty<FixtureVolumeUnitOfMeasureType>(VolumeUnitsOfMeasureProperty),
                WeightUnitsOfMeasure = (Byte)ReadProperty<FixtureWeightUnitOfMeasureType>(WeightUnitsOfMeasureProperty),
                AngleUnitsOfMeasure = (Byte)ReadProperty<FixtureAngleUnitOfMeasureType>(AngleUnitsOfMeasureProperty),
                CurrencyUnitsOfMeasure = (Byte)ReadProperty<FixtureCurrencyUnitOfMeasureType>(CurrencyUnitsOfMeasureProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),
                CreatedBy = ReadProperty<String>(CreatedByProperty),
                LastModifiedBy = ReadProperty<String>(LastModifiedByProperty),
                CustomAttribute1 = ReadProperty<String>(CustomAttribute1Property),
                CustomAttribute2 = ReadProperty<String>(CustomAttribute2Property),
                CustomAttribute3 = ReadProperty<String>(CustomAttribute3Property),
                CustomAttribute4 = ReadProperty<String>(CustomAttribute4Property),
                CustomAttribute5 = ReadProperty<String>(CustomAttribute5Property),

            };
        }

        #endregion

        #region Fetch
        /// <summary>
        /// Called when fetching a package by its Id
        /// </summary>
        private void DataPortal_Fetch(FetchCriteria criteria)
        {
            if (criteria.PackageType == FixturePackageType.FileSystem)
            {
                LockFixturePackageByFileName((String)criteria.Id);
            }
            IDalFactory dalFactory = this.GetDalFactory(criteria.PackageType, criteria.Id);
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IFixturePackageDal dal = dalContext.GetDal<IFixturePackageDal>())
                {
                    this.LoadDataTransferObject(dalContext, dal.FetchById(criteria.Id));
                }
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting an instance of this type
        /// </summary>
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                FixturePackageDto dto = this.GetDataTransferObject();
                Object oldId = dto.Id;
                using (IFixturePackageDal dal = dalContext.GetDal<IFixturePackageDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Object>(IdProperty, dto.Id);
                this.LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                dalContext.RegisterId<FixturePackage>(oldId, dto.Id);
                DataPortal.UpdateChild(Images, dalContext, this);
                DataPortal.UpdateChild(Components, dalContext, this);
                DataPortal.UpdateChild(Assemblies, dalContext, this);
                DataPortal.UpdateChild(Fixtures, dalContext, this);
                DataPortal.UpdateChild(FixtureItems, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating this instance
        /// </summary>
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                FixturePackageDto dto = this.GetDataTransferObject();
                using (IFixturePackageDal dal = dalContext.GetDal<IFixturePackageDal>())
                {
                    dal.Update(dto);
                }

                this.LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                this.LoadProperty<Object>(IdProperty, dto.Id);

                DataPortal.UpdateChild(Images, dalContext, this);
                DataPortal.UpdateChild(Components, dalContext, this);
                DataPortal.UpdateChild(Assemblies, dalContext, this);
                DataPortal.UpdateChild(Fixtures, dalContext, this);
                DataPortal.UpdateChild(FixtureItems, dalContext, this);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is being deleted
        /// </summary>
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = this.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IFixturePackageDal dal = dalContext.GetDal<IFixturePackageDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }
        #endregion

        #endregion

        #region Commands

        #region LockFixturePackageCommand
        /// <summary>
        /// Performs the locking of a package
        /// </summary>
        private partial class LockFixturePackageCommand : CommandBase<LockFixturePackageCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.FixturePackageType, this.Id, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IFixturePackageDal dal = dalContext.GetDal<IFixturePackageDal>())
                    {
                        dal.LockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #region UnlockFixturePackageCommand
        /// <summary>
        /// Performs the unlocking of a package
        /// </summary>
        private partial class UnlockFixturePackageCommand : CommandBase<UnlockFixturePackageCommand>
        {
            #region Data Access

            #region Execute
            /// <summary>
            /// Called when executing this code on the server side
            /// </summary>
            protected override void DataPortal_Execute()
            {
                String dalFactoryName;
                IDalFactory dalFactory = GetDalFactory(this.FixturePackageType, this.Id, out dalFactoryName);
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IFixturePackageDal dal = dalContext.GetDal<IFixturePackageDal>())
                    {
                        dal.UnlockById(this.Id);
                    }
                }
            }
            #endregion

            #endregion
        }
        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Returns the correct dal factory based on the
        /// package type and id
        /// </summary>
        private IDalFactory GetDalFactory(FixturePackageType packageType, Object id)
        {
            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(packageType, id, out dalFactoryName);
            this.LoadProperty<String>(DalFactoryNameProperty, dalFactoryName);
            return dalFactory;
        }

        /// <summary>
        /// Returns the correct dal factory for this instance
        /// </summary>
        protected override IDalFactory GetDalFactory()
        {
            return DalContainer.GetDalFactory(this.DalFactoryName);
        }

        /// <summary>
        /// Returns the correct dal factory based on the
        /// package type and id
        /// </summary>
        private static IDalFactory GetDalFactory(FixturePackageType packageType, Object id, out String dalFactoryName)
        {
            switch (packageType)
            {
                case FixturePackageType.FileSystem:
                    return GetFileSystemDalFactory(id.ToString(), out dalFactoryName);
                default:
                    dalFactoryName = DalContainer.DalName;
                    return DalContainer.GetDalFactory();
            }
        }

        /// <summary>
        /// Returns a dal factory for a file system object
        /// </summary>
        private static IDalFactory GetFileSystemDalFactory(String fileName, out String dalFactoryName)
        {
            // set the dal factory name
            dalFactoryName = fileName;

            // determine if the dal already exists
            if (DalContainer.Contains(dalFactoryName))
                return DalContainer.GetDalFactory(dalFactoryName);

            // the dal factory does not exist so
            // attempt to register a dal based on
            // the file extension
            String fileExtension = Path.GetExtension(fileName).Trim('.');
            fileExtension =
                fileExtension.Substring(0, 1).ToUpperInvariant() +
                fileExtension.Substring(1).ToLowerInvariant();

            // get the dal assembly name
            String dalAssemblyName = String.Format(
                _dalAssemblyName,
                fileExtension);

            // create the config information for the dal container
            DalFactoryConfigElement dalFactoryConfig = new DalFactoryConfigElement(
                dalFactoryName,
                dalAssemblyName);

            // register the dal inside the dal container
            DalContainer.RegisterFactory(dalFactoryConfig);

            // and return the dal factory
            return DalContainer.GetDalFactory(dalFactoryName);
        }
        #endregion
    }
}