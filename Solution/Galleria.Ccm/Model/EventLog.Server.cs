﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// V8-26911 : L.Luong
//   Created (Copied From GFS)
// V8-27404 : L.Luong
//   Changed LevelId to EntryType
#endregion

#region Version History: (CCM CCM803)
// V8-29590 : A.Probyn
//	Extended for new WorkpackageName, WorkpackageId property
#endregion
#endregion

using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class EventLog
    {
        #region Command Classes
        /// <summary>
        /// The WriteEventLog command enables a single EventLog record to be written to the database.
        /// </summary>
        [Serializable]
        private class CreateEventLogCommand : CommandBase<CreateEventLogCommand>
        {
            #region Fields
            public Int32? EntityId { get; set; }
            public String Source { get; set; }
            public String EventLogName { get; set; }
            public Int32 EventId { get; set; }
            public EventLogEntryType EntryType { get; set; }
            public Int32? UserId { get; set; }
            public String Process { get; set; }
            public String ComputerName { get; set; }
            public String UrlHelperLink { get; set; }
            public String Description { get; set; }
            public String Content { get; set; }
            public String GibraltarSessionId { get; set; }
            public String WorkpackageName { get; set; }
            public Int32? WorkpackageId { get; set; }
            #endregion

            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(CreateEventLogCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(CreateEventLogCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(CreateEventLogCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
                BusinessRules.AddRule(typeof(CreateEventLogCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Methods
            /// <summary>
            /// Called when this command is executed
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                    {
                        dal.InsertWithName(new EventLogDto()
                        {
                            EntityId = this.EntityId,
                            Source = this.Source,
                            EventLogName = this.EventLogName,
                            EventId = this.EventId,
                            EntryType = (Int16)this.EntryType,
                            UserId = this.UserId,
                            Process = this.Process,
                            ComputerName = this.ComputerName,
                            URLHelperLink = this.UrlHelperLink,
                            Description = this.Description,
                            Content = this.Content,
                            GibraltarSessionId = this.GibraltarSessionId,
                            WorkpackageName = this.WorkpackageName,
                            WorkpackageId = this.WorkpackageId
                        });
                    }
                }
            }
            #endregion
        }

        /// <summary>
        /// Marks all event logs as deleted
        /// </summary>
        [Serializable]
        public class DeleteAllEventLogsCommand
            : CommandBase<DeleteAllEventLogsCommand>
        {
            #region Fields

            #endregion

            #region Authorization Rules
            /// <summary>
            /// Defines the authorization rules for this type
            /// </summary>
            private static void AddObjectAuthorizationRules()
            {
                BusinessRules.AddRule(typeof(DeleteAllEventLogsCommand), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(DeleteAllEventLogsCommand), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Restricted.ToString()));
                BusinessRules.AddRule(typeof(DeleteAllEventLogsCommand), new IsInRole(AuthorizationActions.EditObject, DomainPermission.EventLogDelete.ToString()));
                BusinessRules.AddRule(typeof(DeleteAllEventLogsCommand), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
            }
            #endregion

            #region Methods
            /// <summary>
            /// Called when this command is executed
            /// </summary>
            protected override void DataPortal_Execute()
            {
                IDalFactory dalFactory = DalContainer.GetDalFactory();
                using (IDalContext dalContext = dalFactory.CreateContext())
                {
                    dalContext.Begin();

                    using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                    {
                        dal.DeleteAll();
                    }
                    dalContext.Commit();
                }
            }
            #endregion
        }

        #endregion

        #region Constructor
        private EventLog() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates the item from the given dto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static EventLog GetEventLog(IDalContext dalContext, EventLogDto dto)
        {
            return DataPortal.FetchChild<EventLog>(dalContext, dto);
        }

        /// <summary>
        /// Writes a single EventLog record to the database, also creating a corresponding EventLogName record if
        /// required.
        /// </summary>
        public static void CreateEventLog(
            Int32? entityId,
            String source,
            String eventLogName,
            Int32 eventId,
            EventLogEntryType entryType,
            Int32? userId,
            String process,
            String computerName,
            String urlHelperLink,
            String description,
            String content,
            String gibraltarSessionId,
            String workpackageName,
            Int32? workpackageId)
        {
            DataPortal.Execute<CreateEventLogCommand>(
                new CreateEventLogCommand()
                {
                    EntityId = entityId,
                    Source = source,
                    EventLogName = eventLogName,
                    EventId = eventId,
                    EntryType = entryType,
                    UserId = userId,
                    Process = process,
                    ComputerName = computerName,
                    UrlHelperLink = urlHelperLink,
                    Description = description,
                    Content = content,
                    GibraltarSessionId = gibraltarSessionId,
                    WorkpackageName = workpackageName,
                    WorkpackageId = workpackageId
                });
        }

        /// <summary>
        /// Writes a single EventLog record to the database, also creating a corresponding EventLogName record if
        /// required.
        /// </summary>
        public static void CreateEventLog(
            Int32? entityId,
            String source,
            String eventLogName,
            Int32 eventId,
            EventLogEntryType entryType,
            Int32? userId,
            String description,
            String content,
            String gibraltarSessionId,
            String workpackageName,
            Int32? workpackageId)
        {
            DataPortal.Execute<CreateEventLogCommand>(
                new CreateEventLogCommand()
                {
                    EntityId = entityId,
                    Source = source,
                    EventLogName = eventLogName,
                    EventId = eventId,
                    EntryType = entryType,
                    UserId = userId,
                    Process = Path.GetFileName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName),
                    ComputerName = System.Environment.MachineName,
                    // TODO: Automatically generate helper link based on event id
                    UrlHelperLink = "",
                    Description = description,
                    Content = content,
                    GibraltarSessionId = gibraltarSessionId,
                    WorkpackageName = workpackageName,
                    WorkpackageId = workpackageId
                });
        }

        #endregion

        #region Command Factory Methods

        /// <summary>
        /// Marks all event logs as deleted
        /// </summary>
        public static void DeleteAllEventLogs()
        {
            DeleteAllEventLogsCommand cmd = new DeleteAllEventLogsCommand();
            DataPortal.Execute<DeleteAllEventLogsCommand>(cmd);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private EventLogDto GetDataTransferObject()
        {
            return new EventLogDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                EntityId = ReadProperty<Int32?>(EntityIdProperty),
                EntityName = ReadProperty<String>(EntityNameProperty),
                EventLogNameId = ReadProperty<Int32>(EventLogNameIdProperty),
                EventLogName = ReadProperty<String>(EventLogNameProperty),
                Source = ReadProperty<String>(SourceProperty),
                EventId = ReadProperty<Int32>(EventIdProperty),
                EntryType = (Int16)ReadProperty<EventLogEntryType>(EntryTypeProperty),
                UserId = ReadProperty<Int32?>(UserIdProperty),
                UserName = ReadProperty<String>(UserNameProperty),
                DateTime = ReadProperty<DateTime>(DateTimeProperty),
                Process = ReadProperty<String>(ProcessProperty),
                ComputerName = ReadProperty<String>(ComputerNameProperty),
                URLHelperLink = ReadProperty<String>(UrlHelperLinkProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                Content = ReadProperty<String>(ContentProperty),
                GibraltarSessionId = ReadProperty<String>(GibraltarSessionIdProperty),
                WorkpackageName = ReadProperty<String>(WorkpackageNameProperty),
                WorkpackageId = ReadProperty<Int32?>(WorkpackageIdProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, EventLogDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32?>(EntityIdProperty, dto.EntityId);
            LoadProperty<String>(EntityNameProperty, dto.EntityName);
            LoadProperty<Int32>(EventLogNameIdProperty, dto.EventLogNameId);
            LoadProperty<String>(EventLogNameProperty, dto.EventLogName);
            LoadProperty<String>(SourceProperty, dto.Source);
            LoadProperty<Int32>(EventIdProperty, dto.EventId);
            LoadProperty<EventLogEntryType>(EntryTypeProperty, (EventLogEntryType)dto.EntryType);
            LoadProperty<Int32?>(UserIdProperty, dto.UserId);
            LoadProperty<String>(UserNameProperty, dto.UserName);
            LoadProperty<DateTime>(DateTimeProperty, dto.DateTime);
            LoadProperty<String>(ProcessProperty, dto.Process);
            LoadProperty<String>(ComputerNameProperty, dto.ComputerName);
            LoadProperty<String>(UrlHelperLinkProperty, dto.URLHelperLink);
            LoadProperty<String>(DescriptionProperty, dto.Description);
            LoadProperty<String>(ContentProperty, dto.Content);
            LoadProperty<String>(GibraltarSessionIdProperty, dto.GibraltarSessionId);
            LoadProperty<String>(WorkpackageNameProperty, dto.WorkpackageName);
            LoadProperty<Int32?>(WorkpackageIdProperty, dto.WorkpackageId);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(Int32 id)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IEventLogDal dal = dalContext.GetDal<IEventLogDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchById(id));
                }
            }
        }

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, EventLogDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {


            EventLogDto eventLogDto = GetDataTransferObject();

            //insert the Event Log itself

            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();


                //Check for event log name table
                EventLogNameDto dto;
                using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
                {
                    dto = dal.FetchByName(eventLogDto.EventLogName);
                }

                if (dto == null)
                {
                    //Save Event Log Name
                    using (IEventLogNameDal dal = dalContext.GetDal<IEventLogNameDal>())
                    {
                        EventLogNameDto dto1 = new EventLogNameDto()
                        {
                            Name = eventLogDto.EventLogName
                        };
                        dal.Insert(dto1);
                        eventLogDto.EventLogNameId = dto1.Id;
                    }
                }
                else
                {
                    eventLogDto.EventLogNameId = dto.Id;
                }

                //Save Event Log (with Loaded/New EventLogNameId)
                using (IEventLogDal EventLogdal = dalContext.GetDal<IEventLogDal>())
                {
                    EventLogdal.Insert(eventLogDto);
                    LoadProperty<Int32>(IdProperty, eventLogDto.Id);
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
