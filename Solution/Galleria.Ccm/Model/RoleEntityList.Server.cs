﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Defines a list of role entities
    /// </summary>
    public partial class RoleEntityList
    {
        #region Constructor
        private RoleEntityList() { } //Force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a list of all role entities for the specified role
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentId">The parent role id</param>
        /// <returns>A list of role entities</returns>
        internal static RoleEntityList GetListByParentId(IDalContext dalContext, Int32 parentId)
        {
            return DataPortal.FetchChild<RoleEntityList>(dalContext, parentId);
        }
        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when fetching a new list
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parentId">The parent role id</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;
            using (IRoleEntityDal dal = dalContext.GetDal<IRoleEntityDal>())
            {
                IEnumerable<RoleEntityDto> dtoList = dal.FetchByRoleId(parentId);
                foreach (RoleEntityDto childDto in dtoList)
                {
                    this.Add(RoleEntity.GetRoleEntity(dalContext, childDto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
