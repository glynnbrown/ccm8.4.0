﻿//#region Header Information
//// Copyright © Galleria RTS Ltd 2014

//#region Version History: CCM800
//// V8-26179  : I.George
////  Created
//#endregion
//#endregion
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using Galleria.Ccm.Resources.Language;

//namespace Galleria.Ccm.Model
//{
//    public enum MetricType
//    {
//        Currency =0,
//        Integer = 1,
//        Decimal = 2
//    }

//    public static class MetricTypeHelper
//    {
//        public static readonly Dictionary<MetricType, String> FriendlyNames =
//            new Dictionary<MetricType, String>()
//            {
//                {MetricType.Currency, Message.Enum_MetricType_Currency},
//                {MetricType.Decimal, Message.Enum_MetricType_Decimal},
//                {MetricType.Integer, Message.Enum_MetricTypeInteger},
//            };
//    }
//}
