﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629 : A.Kuszyk
//		Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Csla;
using Csla.Data;
using Csla.Security;
using Csla.Serialization;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Cluster server model object
    /// </summary>
    public partial class Cluster
    {
        #region Constructors
        private Cluster() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified cluster object
        /// </summary>
        internal static Cluster FetchCluster(IDalContext dalContext, ClusterDto dto)
        {
            return DataPortal.FetchChild<Cluster>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto for this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ClusterDto GetDataTransferObject(ClusterScheme parent)
        {
            return new ClusterDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                Name = ReadProperty<String>(NameProperty),
                ClusterSchemeId = parent.Id
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, ClusterDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<ClusterLocationList>(LocationsProperty, ClusterLocationList.FetchByClusterId(dalContext, dto.Id));
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance
        /// from a data reader as part of a
        /// cluster list
        /// </summary>
        /// <param name="criteria">an object used to identify the desired review</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ClusterDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when persisting this instance in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, ClusterScheme parent)
        {
            ClusterDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<Cluster>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an existing instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, ClusterScheme parent)
        {
            if (this.IsSelfDirty)
            {
                using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext, ClusterScheme parent)
        {
            using (IClusterDal dal = dalContext.GetDal<IClusterDal>())
            {
                dal.DeleteById(this.Id);
            }
        }

        #endregion

        #endregion
    }
}
