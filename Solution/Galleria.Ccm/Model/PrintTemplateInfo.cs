﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion


using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PrintTemplate Model info object
    /// </summary>
    [Serializable]
    public sealed partial class PrintTemplateInfo : ModelReadOnlyObject<PrintTemplateInfo>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Object> IdProperty =
            RegisterModelProperty<Object>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Object Id
        {
            get { return this.GetProperty<Object>(IdProperty); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #endregion

        #region Description

        /// <summary>
        /// Description property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DescriptionProperty =
            RegisterModelProperty<String>(c => c.Description);

        /// <summary>
        /// Gets the Description value
        /// </summary>
        public String Description
        {
            get { return GetProperty<String>(DescriptionProperty); }
        }

        #endregion


        #endregion

        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Overrides

        public override String ToString()
        {
            return Name;
        }

        #endregion
    }
}