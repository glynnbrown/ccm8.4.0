﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-25608 : N.Foster
//  Created
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents an available status of a planogram within the repository
    /// </summary>
    [Serializable]
    public partial class PlanogramStatus : ModelObject<PlanogramStatus>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique worflow id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        private static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// Returns the row version
        /// </summary>
        private RowVersion RowVersion
        {
            get { return this.GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId
        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// Returns the entity id
        /// </summary>
        public Int32 EntityId
        {
            get { return this.GetProperty<Int32>(EntityIdProperty); }
        }
        #endregion

        #region Name
        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// Gets or sets the workflow name
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
            set { this.SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region DateCreated
        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);
        /// <summary>
        /// Returns the date this workflow was created
        /// </summary>
        public DateTime DateCreated
        {
            get { return this.GetProperty<DateTime>(DateCreatedProperty); }
        }
        #endregion

        #region DateLastModified
        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);
        /// <summary>
        /// Returns the date this workflow was last modified
        /// </summary>
        public DateTime DateLastModified
        {
            get { return this.GetProperty<DateTime>(DateLastModifiedProperty); }
        }
        #endregion

        #region DateDeleted
        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);
        /// <summary>
        /// Returns the date this workflow was deleted
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return this.GetProperty<DateTime?>(DateDeletedProperty); }
        }
        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramStatus), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramStatus), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramStatus), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramStatus), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramStatus NewPlanogramStatus(EntityInfo entity)
        {
            return NewPlanogramStatus(entity.Id);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramStatus NewPlanogramStatus(Entity entity)
        {
            return NewPlanogramStatus(entity.Id);
        }

        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static PlanogramStatus NewPlanogramStatus(Int32 entityId)
        {
            PlanogramStatus item = new PlanogramStatus();
            item.Create(entityId);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<String>(NameProperty, Message.PlanogramStatus_DefaultName);
            base.Create();
        }
        #endregion

        #endregion
    }
}
