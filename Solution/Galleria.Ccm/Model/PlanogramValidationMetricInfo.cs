﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     PlanogramValidationMetric Info object.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramValidationMetricInfo : ModelReadOnlyObject<PlanogramValidationMetricInfo>
    {
        #region Propertiesplanogramvalidationmetricinfodal

        #region Id

        /// <summary>
        ///		Metadata for the <see cref="Id"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(o => o.Id);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Id"/> property.
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }

        #endregion

        #region PlanogramValidationGroupId

        /// <summary>
        ///		Metadata for the <see cref="PlanogramValidationGroupId"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> PlanogramValidationGroupIdProperty =
            RegisterModelProperty<Int32>(o => o.PlanogramValidationGroupId);

        /// <summary>
        ///		Gets or sets the value for the <see cref="PlanogramValidationGroupId"/> property.
        /// </summary>
        public Int32 PlanogramValidationGroupId
        {
            get { return this.GetProperty<Int32>(PlanogramValidationGroupIdProperty); }
        }

        #endregion

        #region Field

        /// <summary>
        ///		Metadata for the <see cref="Field"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> FieldProperty =
            RegisterModelProperty<String>(o => o.Field);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Field"/> property.
        /// </summary>
        public String Field
        {
            get { return this.GetProperty<String>(FieldProperty); }
        }

        #endregion

        #region ResultType

        /// <summary>
        ///		Metadata for the <see cref="ResultType"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationTemplateResultType> ResultTypeProperty =
            RegisterModelProperty<PlanogramValidationTemplateResultType>(o => o.ResultType);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ResultType"/> property.
        /// </summary>
        public PlanogramValidationTemplateResultType ResultType
        {
            get { return this.GetProperty<PlanogramValidationTemplateResultType>(ResultTypeProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for the <see cref="PlanogramValidationMetricInfo"/> type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramValidationMetricInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationMetricInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationMetricInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationMetricInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override String ToString()
        {
            return String.Format("{0} ({1})", this.Field, this.ResultType);
        }

        #endregion
    }
}
