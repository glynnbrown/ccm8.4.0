﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.3.0)
// V8-31550 : A.Probyn
//  Created
#endregion

#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class AssortmentProductBuddy
    {
        #region Constructor
        private AssortmentProductBuddy() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating an instance from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new instance of this type</returns>
        internal static AssortmentProductBuddy GetAssortmentProductBuddy(IDalContext dalContext, AssortmentProductBuddyDto dto)
        {
            return DataPortal.FetchChild<AssortmentProductBuddy>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, AssortmentProductBuddyDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            LoadProperty<String>(ProductGtinProperty, dto.ProductGtin);
            LoadProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty, (PlanogramAssortmentProductBuddySourceType)dto.SourceType);
            LoadProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty, (PlanogramAssortmentProductBuddyTreatmentType)dto.TreatmentType);
            LoadProperty<Single>(TreatmentTypePercentageProperty, dto.TreatmentTypePercentage);
            LoadProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty, (PlanogramAssortmentProductBuddyProductAttributeType)dto.ProductAttributeType);
            LoadProperty<Int32?>(S1ProductIdProperty, dto.S1ProductId);
            LoadProperty<Single?>(S1PercentageProperty, dto.S1Percentage);
            LoadProperty<String>(S1ProductGtinProperty, dto.S1ProductGtin);
            LoadProperty<Int32?>(S2ProductIdProperty, dto.S2ProductId);
            LoadProperty<Single?>(S2PercentageProperty, dto.S2Percentage);
            LoadProperty<String>(S2ProductGtinProperty, dto.S2ProductGtin);
            LoadProperty<Int32?>(S3ProductIdProperty, dto.S3ProductId);
            LoadProperty<Single?>(S3PercentageProperty, dto.S3Percentage);
            LoadProperty<String>(S3ProductGtinProperty, dto.S3ProductGtin);
            LoadProperty<Int32?>(S4ProductIdProperty, dto.S4ProductId);
            LoadProperty<Single?>(S4PercentageProperty, dto.S4Percentage);
            LoadProperty<String>(S4ProductGtinProperty, dto.S4ProductGtin);
            LoadProperty<Int32?>(S5ProductIdProperty, dto.S5ProductId);
            LoadProperty<Single?>(S5PercentageProperty, dto.S5Percentage);
            LoadProperty<String>(S5ProductGtinProperty, dto.S5ProductGtin);
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <param name="parent">The Assortment</param>
        /// <returns>A data transfer object</returns>
        private AssortmentProductBuddyDto GetDataTransferObject(Assortment parent)
        {
            return new AssortmentProductBuddyDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                AssortmentId = parent.Id,
                SourceType = (Byte)ReadProperty<PlanogramAssortmentProductBuddySourceType>(SourceTypeProperty),
                TreatmentType = (Byte)ReadProperty<PlanogramAssortmentProductBuddyTreatmentType>(TreatmentTypeProperty),
                ProductAttributeType = (Byte)ReadProperty<PlanogramAssortmentProductBuddyProductAttributeType>(ProductAttributeTypeProperty),
                TreatmentTypePercentage = ReadProperty<Single>(TreatmentTypePercentageProperty),
                ProductId = ReadProperty<Int32>(ProductIdProperty),
                ProductGtin = ReadProperty<String>(ProductGtinProperty),
                S1ProductId = ReadProperty<Int32?>(S1ProductIdProperty),
                S1ProductGtin = ReadProperty<String>(S1ProductGtinProperty),
                S1Percentage = ReadProperty<Single?>(S1PercentageProperty),
                S2ProductId = ReadProperty<Int32?>(S2ProductIdProperty),
                S2ProductGtin = ReadProperty<String>(S2ProductGtinProperty),
                S2Percentage = ReadProperty<Single?>(S2PercentageProperty),
                S3ProductId = ReadProperty<Int32?>(S3ProductIdProperty),
                S3ProductGtin = ReadProperty<String>(S3ProductGtinProperty),
                S3Percentage = ReadProperty<Single?>(S3PercentageProperty),
                S4ProductId = ReadProperty<Int32?>(S4ProductIdProperty),
                S4ProductGtin = ReadProperty<String>(S4ProductGtinProperty),
                S4Percentage = ReadProperty<Single?>(S4PercentageProperty),
                S5ProductId = ReadProperty<Int32?>(S5ProductIdProperty),
                S5ProductGtin = ReadProperty<String>(S5ProductGtinProperty),
                S5Percentage = ReadProperty<Single?>(S5PercentageProperty),
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, AssortmentProductBuddyDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, Assortment parent)
        {
            AssortmentProductBuddyDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IAssortmentProductBuddyDal dal = dalContext.GetDal<IAssortmentProductBuddyDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<AssortmentProductBuddy>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent assortment</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, Assortment parent)
        {
            if (this.IsSelfDirty)
            {
                using (IAssortmentProductBuddyDal dal = dalContext.GetDal<IAssortmentProductBuddyDal>())
                {
                    dal.Update(GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext, Assortment parent)
        {
            using (IAssortmentProductBuddyDal dal = dalContext.GetDal<IAssortmentProductBuddyDal>())
            {
                dal.DeleteById(this.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}
