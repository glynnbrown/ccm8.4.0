﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26234 : L.Ineson
//	Copied from GFS
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// List of role members
    /// </summary>
    public partial class RoleMemberList
    {
        #region Constructor
        private RoleMemberList() { } //Force use of factory methods
        #endregion

        #region Criteria

        #region FetchByRoleIdCriteria
        /// <summary>
        /// Criteria for RoleMember.FetchByRoleId
        /// </summary>
        [Serializable]
        internal class FetchByRoleIdCriteria : CriteriaBase<FetchByRoleIdCriteria>
        {
            #region Properties

            #region Dal Context
            /// <summary>
            /// DalContext property definition
            /// </summary>
            public static PropertyInfo<IDalContext> DalContextProperty =
                RegisterProperty<IDalContext>(c => c.DalContext);
            /// <summary>
            /// The current dal context
            /// </summary>
            public IDalContext DalContext
            {
                get { return ReadProperty<IDalContext>(DalContextProperty); }
            }
            #endregion

            #region Role Id
            /// <summary>
            /// RoleId property definition
            /// </summary>
            public static PropertyInfo<Int32> RoleIdProperty =
                RegisterProperty<Int32>(c => c.RoleId);
            /// <summary>
            /// The parent role id
            /// </summary>
            public Int32 RoleId
            {
                get { return ReadProperty<Int32>(RoleIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Called when creating a new instance of this type
            /// </summary>
            /// <param name="roleId">The role id</param>
            public FetchByRoleIdCriteria(IDalContext dalContext, Int32 roleId)
            {
                LoadProperty<IDalContext>(DalContextProperty, dalContext);
                LoadProperty<Int32>(RoleIdProperty, roleId);
            }
            #endregion
        }
        #endregion

        #region FetchByUserIdCriteria
        /// <summary>
        /// Criteria for RoleMember.FetchByUserId
        /// </summary>
        [Serializable]
        internal class FetchByUserIdCriteria : CriteriaBase<FetchByUserIdCriteria>
        {
            #region Properties

            #region Dal Context
            /// <summary>
            /// DalContext property definition
            /// </summary>
            public static PropertyInfo<IDalContext> DalContextProperty =
                RegisterProperty<IDalContext>(c => c.DalContext);
            /// <summary>
            /// The current dal context
            /// </summary>
            public IDalContext DalContext
            {
                get { return ReadProperty<IDalContext>(DalContextProperty); }
            }
            #endregion

            #region UserId
            /// <summary>
            /// UserId property definition
            /// </summary>
            public static PropertyInfo<Int32> UserIdProperty =
                RegisterProperty<Int32>(c => c.UserId);
            /// <summary>
            /// The user id
            /// </summary>
            public Int32 UserId
            {
                get { return ReadProperty<Int32>(UserIdProperty); }
            }
            #endregion

            #endregion

            #region Constructors
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            /// <param name="userId">The user id</param>
            public FetchByUserIdCriteria(IDalContext dalContext, Int32 userId)
            {
                LoadProperty<IDalContext>(DalContextProperty, dalContext);
                LoadProperty<Int32>(UserIdProperty, userId);
            }
            #endregion
        }
        #endregion

        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all role members for the specified role
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="roleId">The role id</param>
        /// <returns>All reole members for the specified role</returns>
        internal static RoleMemberList FetchByRoleId(IDalContext dalContext, Int32 roleId)
        {
            return DataPortal.FetchChild<RoleMemberList>(new FetchByRoleIdCriteria(dalContext, roleId));
        }

        /// <summary>
        /// Returns all role members for the specified user
        /// </summary>
        /// <param name="dalContext">The dal context</param>
        /// <param name="userId">The user id</param>
        /// <returns>All role members for the specified user</returns>
        internal static RoleMemberList FetchByUserId(IDalContext dalContext, Int32 userId)
        {
            return DataPortal.FetchChild<RoleMemberList>(new FetchByUserIdCriteria(dalContext, userId));
        }

        #endregion

        #region Data Access

        #region Fetch
        /// <summary>
        /// Called when returning a list of role members
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(FetchByRoleIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IRoleMemberDal dal = criteria.DalContext.GetDal<IRoleMemberDal>())
            {
                foreach (RoleMemberDto dto in dal.FetchByRoleId(criteria.RoleId))
                {
                    this.Add(RoleMember.GetRoleMember(criteria.DalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        /// <summary>
        /// Called when returning a list of role members
        /// </summary>
        /// <param name="criteria">The fetch criteria</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(FetchByUserIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            using (IRoleMemberDal dal = criteria.DalContext.GetDal<IRoleMemberDal>())
            {
                foreach (RoleMemberDto dto in dal.FetchByUserId(criteria.UserId))
                {
                    this.Add(RoleMember.GetRoleMember(criteria.DalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }
        #endregion

        #endregion
    }
}
