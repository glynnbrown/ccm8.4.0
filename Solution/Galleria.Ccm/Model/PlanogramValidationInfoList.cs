﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     This class holds a list of <see cref="PlanogramValidationInfo" /> instances.
    /// </summary>
    /// <remarks>This class is a root object.</remarks>
    [Serializable]
    public sealed partial class PlanogramValidationInfoList :
        ModelReadOnlyList<PlanogramValidationInfoList, PlanogramValidationInfo>
    {
        #region Criteria

        #region FetchByPlanogramIdsCriteria

        /// <summary>
        ///     Holds the criteria to fetch <see cref="PlanogramValidationInfo" /> using a given collection of Ids.
        /// </summary>
        [Serializable]
        public class FetchByPlanogramIdsCriteria : CriteriaBase<FetchByPlanogramIdsCriteria>
        {
            #region Properties

            #region PlanogramIds

            /// <summary>
            ///     PlanogramIds property definition
            /// </summary>
            private static readonly PropertyInfo<List<Int32>> PlanogramIdsProperty =
                RegisterProperty<List<Int32>>(c => c.PlanogramIds);

            /// <summary>
            ///     Returns the list of ids to fetch for
            /// </summary>
            public List<Int32> PlanogramIds
            {
                get { return this.ReadProperty<List<Int32>>(PlanogramIdsProperty); }
            }

            #endregion

            #endregion

            #region Constructors

            /// <summary>
            ///     Creates a new instance of this type
            /// </summary>
            public FetchByPlanogramIdsCriteria(IEnumerable<Int32> planogramIds)
            {
                this.LoadProperty(PlanogramIdsProperty, planogramIds.ToList());
            }

            #endregion
        }

        #endregion

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for <see cref="PlanogramValidationInfoList" />.
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramValidationInfoList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationInfoList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationInfoList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationInfoList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }

        #endregion
    }
}