﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{       
    /// <summary>
    /// SearchColumnType is used to differentiate between objects from two different observable collections 
    /// on the Search Advanced screen 
    /// </summary>
    public enum SearchColumnType
    {
        Product = 0,
        Component = 1
    }
}
