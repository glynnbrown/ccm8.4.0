﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25447 : N.Haywood
//  Copied over from GFS
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Csla;

namespace Galleria.Ccm.Model
{
    public partial class LocationProductIllegalList
    {
        #region Constructor
        private LocationProductIllegalList() { }//Force use of factory methods
        #endregion

        #region Factory Methods

        public static LocationProductIllegalList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<LocationProductIllegalList>(new FetchByEntityIdCriteria(entityId));
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when fetching all objects with specific entityId
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationProductIllegalDal dal = dalContext.GetDal<ILocationProductIllegalDal>())
                {
                    IEnumerable<LocationProductIllegalDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (LocationProductIllegalDto dto in dtoList)
                    {
                        this.Add(LocationProductIllegal.GetLocationProductIllegal(dalContext, dto));
                    }
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when the list is updated
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                Child_Update(dalContext);
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
