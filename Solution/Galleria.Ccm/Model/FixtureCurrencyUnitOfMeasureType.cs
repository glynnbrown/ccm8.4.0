﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion

#region Version History: CCM801
// V8-28668 : L.Luong
//  Added HKD.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available currency unit of measure types
    /// </summary>
    public enum FixtureCurrencyUnitOfMeasureType
    {
        Unknown = 0,
        GBP = 1,
        USD = 2,
        HKD = 3,
        EUR = 4
    }

    /// <summary>
    /// FixtureLengthUnitOfMeasureType Helper Class
    /// </summary>
    public static class FixtureCurrencyUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureCurrencyUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<FixtureCurrencyUnitOfMeasureType, String>()
            {
                {FixtureCurrencyUnitOfMeasureType.Unknown, String.Empty},
                {FixtureCurrencyUnitOfMeasureType.GBP, Message.Enum_FixtureCurrencyUnitOfMeasureType_GBP},
                {FixtureCurrencyUnitOfMeasureType.USD, Message.Enum_FixtureCurrencyUnitOfMeasureType_USD},
                {FixtureCurrencyUnitOfMeasureType.HKD, Message.Enum_FixtureCurrencyUnitOfMeasureType_HKD},
                {FixtureCurrencyUnitOfMeasureType.EUR, Framework.Planograms.Resources.Language.Message.Enum_PlanogramCurrencyUnitOfMeasureType_EUR},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureCurrencyUnitOfMeasureType, String> Abbreviations =
            new Dictionary<FixtureCurrencyUnitOfMeasureType, String>()
            {
                {FixtureCurrencyUnitOfMeasureType.Unknown, String.Empty},
                {FixtureCurrencyUnitOfMeasureType.GBP, Message.Enum_FixtureCurrencyUnitOfMeasureType_GBP_Abbrev},
                {FixtureCurrencyUnitOfMeasureType.USD, Message.Enum_FixtureCurrencyUnitOfMeasureType_USD_Abbrev},
                {FixtureCurrencyUnitOfMeasureType.HKD, Message.Enum_FixtureCurrencyUnitOfMeasureType_HKD_Abbrev},
                {FixtureCurrencyUnitOfMeasureType.EUR, Framework.Planograms.Resources.Language.Message.Enum_PlanogramCurrencyUnitOfMeasureType_EUR_Abbrev},
            };

        public static FixtureCurrencyUnitOfMeasureType Parse(String value)
        {
            return EnumHelper.Parse<FixtureCurrencyUnitOfMeasureType>(value, FixtureCurrencyUnitOfMeasureType.Unknown);
        }
    }
}
