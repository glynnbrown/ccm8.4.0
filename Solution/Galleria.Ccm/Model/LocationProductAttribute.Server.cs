﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// CCM-25446 : N.Haywood
//  Copied over from GFS
// V8-26041 : A.Kuszyk
//      Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
// V8-27630 : I.George
// Removed Unused Factory methods
#endregion
#region Version History: (CCM 8.03)
//V8-29267 : L.Ineson
//  Removed date deleted.
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    public partial class LocationProductAttribute
    {
        #region Constructor
        private LocationProductAttribute() { } // force the use of factory methods
        #endregion

        #region Factory Methods


        public static LocationProductAttribute FetchByLocationIdProductId(Int16 locationId, Int32 productId)
        {
            return DataPortal.Fetch<LocationProductAttribute>(new FetchByLocationIdProductIdCriteria(locationId, productId));

        }

        /// <summary>
        /// Creates the item from the given dto
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static LocationProductAttribute GetLocationProductAttribute(IDalContext dalContext, LocationProductAttributeDto dto)
        {
            return DataPortal.FetchChild<LocationProductAttribute>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private LocationProductAttributeDto GetDataTransferObject()
        {
            return new LocationProductAttributeDto
            {
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                ProductId = ReadProperty<Int32>(ProductIdProperty),
                LocationId = ReadProperty<Int16>(LocationIdProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                GTIN = ReadProperty<String>(GTINProperty),
                Description = ReadProperty<String>(DescriptionProperty),
                Height = ReadProperty<Single?>(HeightProperty),
                Width = ReadProperty<Single?>(WidthProperty),
                Depth = ReadProperty<Single?>(DepthProperty),
                CasePackUnits = ReadProperty<Int16?>(CasePackUnitsProperty),
                CaseHigh = ReadProperty<Byte?>(CaseHighProperty),
                CaseWide = ReadProperty<Byte?>(CaseWideProperty),
                CaseDeep = ReadProperty<Byte?>(CaseDeepProperty),
                CaseHeight = ReadProperty<Single?>(CaseHeightProperty),
                CaseWidth = ReadProperty<Single?>(CaseWidthProperty),
                CaseDepth = ReadProperty<Single?>(CaseDepthProperty),
                DaysOfSupply = ReadProperty<Byte?>(DaysOfSupplyProperty),
                MinDeep = ReadProperty<Byte?>(MinDeepProperty),
                MaxDeep = ReadProperty<Byte?>(MaxDeepProperty),
                TrayPackUnits = ReadProperty<Int16?>(TrayPackUnitsProperty),
                TrayHigh = ReadProperty<Byte?>(TrayHighProperty),
                TrayWide = ReadProperty<Byte?>(TrayWideProperty),
                TrayDeep = ReadProperty<Byte?>(TrayDeepProperty),
                TrayHeight = ReadProperty<Single?>(TrayHeightProperty),
                TrayWidth = ReadProperty<Single?>(TrayWidthProperty),
                TrayDepth = ReadProperty<Single?>(TrayDepthProperty),
                TrayThickHeight = ReadProperty<Single?>(TrayThickHeightProperty),
                TrayThickDepth = ReadProperty<Single?>(TrayThickDepthProperty),
                TrayThickWidth = ReadProperty<Single?>(TrayThickWidthProperty),
                StatusType = (Byte?)ReadProperty<PlanogramProductStatusType?>(StatusTypeProperty),
                ShelfLife = ReadProperty<Int16?>(ShelfLifeProperty),
                DeliveryFrequencyDays = ReadProperty<Single?>(DeliveryFrequencyDaysProperty),
                DeliveryMethod = ReadProperty<String>(DeliveryMethodProperty),
                VendorCode = ReadProperty<String>(VendorCodeProperty),
                Vendor = ReadProperty<String>(VendorProperty),
                ManufacturerCode = ReadProperty<String>(ManufacturerCodeProperty),
                Manufacturer = ReadProperty<String>(ManufacturerProperty),
                Size = ReadProperty<String>(SizeProperty),
                UnitOfMeasure = ReadProperty<String>(UnitOfMeasureProperty),
                SellPrice = ReadProperty<Single?>(SellPriceProperty),
                SellPackCount = ReadProperty<Int16?>(SellPackCountProperty),
                SellPackDescription = ReadProperty<String>(SellPackDescriptionProperty),
                RecommendedRetailPrice = ReadProperty<Single?>(RecommendedRetailPriceProperty),
                CostPrice = ReadProperty<Single?>(CostPriceProperty),
                CaseCost = ReadProperty<Single?>(CaseCostProperty),
                ConsumerInformation = ReadProperty<String>(ConsumerInformationProperty),
                Pattern = ReadProperty<String>(PatternProperty),
                Model = ReadProperty<String>(ModelProperty),
                CorporateCode = ReadProperty<String>(CorporateCodeProperty),
                DateCreated = ReadProperty<DateTime>(DateCreatedProperty),
                DateLastModified = ReadProperty<DateTime>(DateLastModifiedProperty),

                //#region Custom Attributes
                //CustomAttribute01 = ReadProperty<Object>(CustomAttribute01Property),
                //CustomAttribute02 = ReadProperty<Object>(CustomAttribute02Property),
                //CustomAttribute03 = ReadProperty<Object>(CustomAttribute03Property),
                //CustomAttribute04 = ReadProperty<Object>(CustomAttribute04Property),
                //CustomAttribute05 = ReadProperty<Object>(CustomAttribute05Property),
                //CustomAttribute06 = ReadProperty<Object>(CustomAttribute06Property),
                //CustomAttribute07 = ReadProperty<Object>(CustomAttribute07Property),
                //CustomAttribute08 = ReadProperty<Object>(CustomAttribute08Property),
                //CustomAttribute09 = ReadProperty<Object>(CustomAttribute09Property),
                //CustomAttribute10 = ReadProperty<Object>(CustomAttribute10Property),
                //CustomAttribute11 = ReadProperty<Object>(CustomAttribute11Property),
                //CustomAttribute12 = ReadProperty<Object>(CustomAttribute12Property),
                //CustomAttribute13 = ReadProperty<Object>(CustomAttribute13Property),
                //CustomAttribute14 = ReadProperty<Object>(CustomAttribute14Property),
                //CustomAttribute15 = ReadProperty<Object>(CustomAttribute15Property),
                //CustomAttribute16 = ReadProperty<Object>(CustomAttribute16Property),
                //CustomAttribute17 = ReadProperty<Object>(CustomAttribute17Property),
                //CustomAttribute18 = ReadProperty<Object>(CustomAttribute18Property),
                //CustomAttribute19 = ReadProperty<Object>(CustomAttribute19Property),
                //CustomAttribute20 = ReadProperty<Object>(CustomAttribute20Property),
                //CustomAttribute21 = ReadProperty<Object>(CustomAttribute21Property),
                //CustomAttribute22 = ReadProperty<Object>(CustomAttribute22Property),
                //CustomAttribute23 = ReadProperty<Object>(CustomAttribute23Property),
                //CustomAttribute24 = ReadProperty<Object>(CustomAttribute24Property),
                //CustomAttribute25 = ReadProperty<Object>(CustomAttribute25Property),
                //CustomAttribute26 = ReadProperty<Object>(CustomAttribute26Property),
                //CustomAttribute27 = ReadProperty<Object>(CustomAttribute27Property),
                //CustomAttribute28 = ReadProperty<Object>(CustomAttribute28Property),
                //CustomAttribute29 = ReadProperty<Object>(CustomAttribute29Property),
                //CustomAttribute30 = ReadProperty<Object>(CustomAttribute30Property),
                //CustomAttribute31 = ReadProperty<Object>(CustomAttribute31Property),
                //CustomAttribute32 = ReadProperty<Object>(CustomAttribute32Property),
                //CustomAttribute33 = ReadProperty<Object>(CustomAttribute33Property),
                //CustomAttribute34 = ReadProperty<Object>(CustomAttribute34Property),
                //CustomAttribute35 = ReadProperty<Object>(CustomAttribute35Property),
                //CustomAttribute36 = ReadProperty<Object>(CustomAttribute36Property),
                //CustomAttribute37 = ReadProperty<Object>(CustomAttribute37Property),
                //CustomAttribute38 = ReadProperty<Object>(CustomAttribute38Property),
                //CustomAttribute39 = ReadProperty<Object>(CustomAttribute39Property),
                //CustomAttribute40 = ReadProperty<Object>(CustomAttribute40Property),
                //CustomAttribute41 = ReadProperty<Object>(CustomAttribute41Property),
                //CustomAttribute42 = ReadProperty<Object>(CustomAttribute42Property),
                //CustomAttribute43 = ReadProperty<Object>(CustomAttribute43Property),
                //CustomAttribute44 = ReadProperty<Object>(CustomAttribute44Property),
                //CustomAttribute45 = ReadProperty<Object>(CustomAttribute45Property),
                //CustomAttribute46 = ReadProperty<Object>(CustomAttribute46Property),
                //CustomAttribute47 = ReadProperty<Object>(CustomAttribute47Property),
                //CustomAttribute48 = ReadProperty<Object>(CustomAttribute48Property),
                //CustomAttribute49 = ReadProperty<Object>(CustomAttribute49Property),
                //CustomAttribute50 = ReadProperty<Object>(CustomAttribute50Property)
                //#endregion
            };
        }


        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationProductAttributeDto dto)
        {
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            LoadProperty<Int16>(LocationIdProperty, dto.LocationId);
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<String>(GTINProperty, dto.GTIN);
            LoadProperty<String>(DescriptionProperty, dto.Description);
            LoadProperty<Single?>(HeightProperty, dto.Height);
            LoadProperty<Single?>(WidthProperty, dto.Width);
            LoadProperty<Single?>(DepthProperty, dto.Depth);
            LoadProperty<Int16?>(CasePackUnitsProperty, dto.CasePackUnits);
            LoadProperty<Byte?>(CaseHighProperty, dto.CaseHigh);
            LoadProperty<Byte?>(CaseWideProperty, dto.CaseWide);
            LoadProperty<Byte?>(CaseDeepProperty, dto.CaseDeep);
            LoadProperty<Single?>(CaseHeightProperty, dto.CaseHeight);
            LoadProperty<Single?>(CaseWidthProperty, dto.CaseWidth);
            LoadProperty<Single?>(CaseDepthProperty, dto.CaseDepth);
            LoadProperty<Byte?>(DaysOfSupplyProperty, dto.DaysOfSupply);
            LoadProperty<Byte?>(MinDeepProperty, dto.MinDeep);
            LoadProperty<Byte?>(MaxDeepProperty, dto.MaxDeep);
            LoadProperty<Int16?>(TrayPackUnitsProperty, dto.TrayPackUnits);
            LoadProperty<Byte?>(TrayHighProperty, dto.TrayHigh);
            LoadProperty<Byte?>(TrayWideProperty, dto.TrayWide);
            LoadProperty<Byte?>(TrayDeepProperty, dto.TrayDeep);
            LoadProperty<Single?>(TrayHeightProperty, dto.TrayHeight);
            LoadProperty<Single?>(TrayWidthProperty, dto.TrayWidth);
            LoadProperty<Single?>(TrayDepthProperty, dto.TrayDepth);
            LoadProperty<Single?>(TrayThickHeightProperty, dto.TrayThickHeight);
            LoadProperty<Single?>(TrayThickDepthProperty, dto.TrayThickDepth);
            LoadProperty<Single?>(TrayThickWidthProperty, dto.TrayThickWidth);
            LoadProperty<PlanogramProductStatusType?>(StatusTypeProperty, (PlanogramProductStatusType?)dto.StatusType);
            LoadProperty<Int16?>(ShelfLifeProperty, dto.ShelfLife);
            LoadProperty<Single?>(DeliveryFrequencyDaysProperty, dto.DeliveryFrequencyDays);
            LoadProperty<String>(DeliveryMethodProperty, dto.DeliveryMethod);
            LoadProperty<String>(VendorCodeProperty, dto.VendorCode);
            LoadProperty<String>(VendorProperty, dto.Vendor);
            LoadProperty<String>(ManufacturerCodeProperty, dto.ManufacturerCode);
            LoadProperty<String>(ManufacturerProperty, dto.Manufacturer);
            LoadProperty<String>(SizeProperty, dto.Size);
            LoadProperty<String>(UnitOfMeasureProperty, dto.UnitOfMeasure);
            LoadProperty<Single?>(SellPriceProperty, dto.SellPrice);
            LoadProperty<Int16?>(SellPackCountProperty, dto.SellPackCount);
            LoadProperty<String>(SellPackDescriptionProperty, dto.SellPackDescription);
            LoadProperty<Single?>(RecommendedRetailPriceProperty, dto.RecommendedRetailPrice);
            LoadProperty<Single?>(CostPriceProperty, dto.CostPrice);
            LoadProperty<Single?>(CaseCostProperty, dto.CaseCost);
            LoadProperty<String>(ConsumerInformationProperty, dto.ConsumerInformation);
            LoadProperty<String>(PatternProperty, dto.Pattern);
            LoadProperty<String>(ModelProperty, dto.Model);
            LoadProperty<String>(CorporateCodeProperty, dto.CorporateCode);
            LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
            LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);

            //#region Custom Attributes
            //LoadProperty<Object>(CustomAttribute01Property, dto.CustomAttribute01);
            //LoadProperty<Object>(CustomAttribute02Property, dto.CustomAttribute02);
            //LoadProperty<Object>(CustomAttribute03Property, dto.CustomAttribute03);
            //LoadProperty<Object>(CustomAttribute04Property, dto.CustomAttribute04);
            //LoadProperty<Object>(CustomAttribute05Property, dto.CustomAttribute05);
            //LoadProperty<Object>(CustomAttribute06Property, dto.CustomAttribute06);
            //LoadProperty<Object>(CustomAttribute07Property, dto.CustomAttribute07);
            //LoadProperty<Object>(CustomAttribute08Property, dto.CustomAttribute08);
            //LoadProperty<Object>(CustomAttribute09Property, dto.CustomAttribute09);
            //LoadProperty<Object>(CustomAttribute10Property, dto.CustomAttribute10);
            //LoadProperty<Object>(CustomAttribute11Property, dto.CustomAttribute11);
            //LoadProperty<Object>(CustomAttribute12Property, dto.CustomAttribute12);
            //LoadProperty<Object>(CustomAttribute13Property, dto.CustomAttribute13);
            //LoadProperty<Object>(CustomAttribute14Property, dto.CustomAttribute14);
            //LoadProperty<Object>(CustomAttribute15Property, dto.CustomAttribute15);
            //LoadProperty<Object>(CustomAttribute16Property, dto.CustomAttribute16);
            //LoadProperty<Object>(CustomAttribute17Property, dto.CustomAttribute17);
            //LoadProperty<Object>(CustomAttribute18Property, dto.CustomAttribute18);
            //LoadProperty<Object>(CustomAttribute19Property, dto.CustomAttribute19);
            //LoadProperty<Object>(CustomAttribute20Property, dto.CustomAttribute20);
            //LoadProperty<Object>(CustomAttribute21Property, dto.CustomAttribute21);
            //LoadProperty<Object>(CustomAttribute22Property, dto.CustomAttribute22);
            //LoadProperty<Object>(CustomAttribute23Property, dto.CustomAttribute23);
            //LoadProperty<Object>(CustomAttribute24Property, dto.CustomAttribute24);
            //LoadProperty<Object>(CustomAttribute25Property, dto.CustomAttribute25);
            //LoadProperty<Object>(CustomAttribute26Property, dto.CustomAttribute26);
            //LoadProperty<Object>(CustomAttribute27Property, dto.CustomAttribute27);
            //LoadProperty<Object>(CustomAttribute28Property, dto.CustomAttribute28);
            //LoadProperty<Object>(CustomAttribute29Property, dto.CustomAttribute29);
            //LoadProperty<Object>(CustomAttribute30Property, dto.CustomAttribute30);
            //LoadProperty<Object>(CustomAttribute31Property, dto.CustomAttribute31);
            //LoadProperty<Object>(CustomAttribute32Property, dto.CustomAttribute32);
            //LoadProperty<Object>(CustomAttribute33Property, dto.CustomAttribute33);
            //LoadProperty<Object>(CustomAttribute34Property, dto.CustomAttribute34);
            //LoadProperty<Object>(CustomAttribute35Property, dto.CustomAttribute35);
            //LoadProperty<Object>(CustomAttribute36Property, dto.CustomAttribute36);
            //LoadProperty<Object>(CustomAttribute37Property, dto.CustomAttribute37);
            //LoadProperty<Object>(CustomAttribute38Property, dto.CustomAttribute38);
            //LoadProperty<Object>(CustomAttribute39Property, dto.CustomAttribute39);
            //LoadProperty<Object>(CustomAttribute40Property, dto.CustomAttribute40);
            //LoadProperty<Object>(CustomAttribute41Property, dto.CustomAttribute41);
            //LoadProperty<Object>(CustomAttribute42Property, dto.CustomAttribute42);
            //LoadProperty<Object>(CustomAttribute43Property, dto.CustomAttribute43);
            //LoadProperty<Object>(CustomAttribute44Property, dto.CustomAttribute44);
            //LoadProperty<Object>(CustomAttribute45Property, dto.CustomAttribute45);
            //LoadProperty<Object>(CustomAttribute46Property, dto.CustomAttribute46);
            //LoadProperty<Object>(CustomAttribute47Property, dto.CustomAttribute47);
            //LoadProperty<Object>(CustomAttribute48Property, dto.CustomAttribute48);
            //LoadProperty<Object>(CustomAttribute49Property, dto.CustomAttribute49);
            //LoadProperty<Object>(CustomAttribute50Property, dto.CustomAttribute50);
            //#endregion
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when return FetchById
        /// </summary>
        /// <param name="criteria">The criteria used to load the item</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByLocationIdProductIdCriteria criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    LoadDataTransferObject(dalContext, dal.FetchByLocationIdProductId(criteria.LocationId, criteria.ProductId));
                }
            }
        }

        /// <summary>
        /// Called when returning an existing object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationProductAttributeDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert

        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            //insert the LocationProductAttribute
            LocationProductAttributeDto dto = GetDataTransferObject();
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    dal.Insert(dto);

                    //what about inserted item id?
                    //LoadProperty<Int32>(IdProperty, dto.Id);
                    LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                    LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                    LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                }

                FieldManager.UpdateChildren(dalContext, this);

                dalContext.Commit();
            }
        }

        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="parent">The parent model node</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext)
        {
            using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
            {
                // get the dto
                LocationProductAttributeDto dto = GetDataTransferObject();
                dal.Insert(dto);

                //update this.RowVersion from the dto RowVersion
                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                LoadProperty<DateTime>(DateCreatedProperty, dto.DateCreated);
                LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Update

        /// <summary>
        /// Called when updating this object in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    LocationProductAttributeDto dto = GetDataTransferObject();
                    dal.Update(dto);

                    LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                    LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
                }
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext)
        {
            using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
            {
                // get the dto
                LocationProductAttributeDto dto = GetDataTransferObject();
                dal.Update(dto);

                LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                LoadProperty<DateTime>(DateLastModifiedProperty, dto.DateLastModified);
            }

            //update children first for ids if any inserts are required
            FieldManager.UpdateChildren(dalContext, this);
        }

        #endregion

        #region Delete

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
                {
                    dal.DeleteById(this.LocationId, this.ProductId);
                }
                dalContext.Commit();
            }
        }

        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (ILocationProductAttributeDal dal = dalContext.GetDal<ILocationProductAttributeDal>())
            {
                dal.DeleteById(this.LocationId, this.ProductId);
            }
        }
        #endregion


        #endregion
    }
}
