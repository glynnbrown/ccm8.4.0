﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Created
// V8-25669 : A.Kuszyk
//      Added Entity Id property.
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramProductInfo
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing an info for a product.
    /// (child of ProductInfoList)
    /// </summary>
    [Serializable]
    public partial class ProductInfo : ModelReadOnlyObject<ProductInfo>, IPlanogramProductInfo
    {
        #region Static Constructor
        /// <summary>
        /// Static Constructor
        /// </summary>
        static ProductInfo()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
         RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
         RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// The Entity Id.
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
        }

        public static readonly ModelPropertyInfo<String> GtinProperty =
            RegisterModelProperty<String>(c => c.Gtin);
        /// <summary>
        /// The product GTIN
        /// </summary>
        public String Gtin
        {
            get { return GetProperty<String>(GtinProperty); }
        }

        public static readonly ModelPropertyInfo<String> NameProperty =
           RegisterModelProperty<String>(c => c.Name);
        /// <summary>
        /// The product name
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
        }

        #region Helper Properties

        /// <summary>
        /// Helper property to return product's info full name
        /// </summary>
        public String FullName
        {
            get { return this.ToString(); }
        }

        #endregion

        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ProductInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(ProductInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(ProductInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new instance for the given product
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        public static ProductInfo NewProductInfo(Product product)
        {
            ProductInfo item = new ProductInfo();
            item.Create(product);
            return item;
        }

        #endregion

        #region Data Access

        private void Create(Product product)
        {
            this.LoadProperty<Int32>(IdProperty, product.Id);
            this.LoadProperty<Int32>(EntityIdProperty, product.EntityId);
            this.LoadProperty<String>(GtinProperty, product.Gtin);
            this.LoadProperty<String>(NameProperty, product.Name);
        }

        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Gtin + ' ' + this.Name;
        }

        #endregion

        #region Mapping
        /// <summary>
        /// Creates all mappings for this type
        /// </summary>        
        internal static void CreateMappings()
        {
            //Mapper.CreateMap<ProductInfo, ProductInfoDc>();
        }
        #endregion
    }
}
