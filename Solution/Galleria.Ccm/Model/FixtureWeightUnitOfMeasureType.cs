﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM CCM800)
// CCM-24979 : L.Hodson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available weight unit of measure types
    /// </summary>
    public enum FixtureWeightUnitOfMeasureType
    {
        Unknown = 0,
        Kilograms = 1,
        Pounds = 2
    }

    /// <summary>
    /// PlanogramWeightUnitOfMeasureType Helper Class
    /// </summary>
    public static class FixtureWeightUnitOfMeasureTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureWeightUnitOfMeasureType, String> FriendlyNames =
            new Dictionary<FixtureWeightUnitOfMeasureType, String>()
            {
                {FixtureWeightUnitOfMeasureType.Unknown, String.Empty},
                {FixtureWeightUnitOfMeasureType.Kilograms, Message.Enum_FixtureWeightUnitOfMeasureType_Kilograms},
                {FixtureWeightUnitOfMeasureType.Pounds, Message.Enum_FixtureWeightUnitOfMeasureType_Pounds},
            };

        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureWeightUnitOfMeasureType, String> Abbreviations =
            new Dictionary<FixtureWeightUnitOfMeasureType, String>()
            {
                {FixtureWeightUnitOfMeasureType.Unknown, String.Empty},
                {FixtureWeightUnitOfMeasureType.Kilograms, Message.Enum_FixtureWeightUnitOfMeasureType_Kilograms_Abbrev},
                {FixtureWeightUnitOfMeasureType.Pounds, Message.Enum_FixtureWeightUnitOfMeasureType_Pounds_Abbrev},
            };

        public static FixtureWeightUnitOfMeasureType Parse(String value)
        {
            return EnumHelper.Parse<FixtureWeightUnitOfMeasureType>(value, FixtureWeightUnitOfMeasureType.Unknown);
        }
    }
}
