﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// V8-26179  : I.George
//  Created
// V8-28149 : M.Pettit
//  Ensured all types have friendly names for reporting
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Denotes the available area of unit measure type
    /// </summary>
    public enum UnitOfMeasureAreaType
    {
        Unknown = 0,
        SquareCentimeter = 1,
        SquareInches = 2
    }
    /// <summary>
    /// UnitOfMeasureAreaType helper class
    /// </summary>
    public static class UnitOfMeasureAreaTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<UnitOfMeasureAreaType, String> FriendlyNames =
            new Dictionary<UnitOfMeasureAreaType, String>()
           {
               {UnitOfMeasureAreaType.Unknown, String.Empty},
               {UnitOfMeasureAreaType.SquareCentimeter, Message.Enum_UnitOfMeasureAreaType_SquareCentimeters},
               {UnitOfMeasureAreaType.SquareInches, Message.Enum_UnitOfMeasureAreaType_SquareInches},
           };

        public static readonly Dictionary<UnitOfMeasureAreaType, String> Abbreviations =
             new Dictionary<UnitOfMeasureAreaType, String>()
            {
                {UnitOfMeasureAreaType.Unknown, String.Empty},
                {UnitOfMeasureAreaType.SquareCentimeter, Message.Enum_UnitOfMeasureAreaType_SquareCentimeters_Abbrev},
                {UnitOfMeasureAreaType.SquareInches, Message.Enum_UnitOfMeasureAreaTypeSquareInches_Abbrev},
            };
    }
}
