﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// An enumeration that defines the label dal factory type
    /// which indicates the source of a label
    /// </summary>
    public enum LabelDalFactoryType
    {
        Unknown = 0,
        FileSystem = 1
        // GfsDatabase
        // GfsWebServices
        // CcmDatabase
        // CcmRepository
        // IkbDatabase
    }
}
