#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25879 : Martin Shelley
//		Created (Auto-generated)
//      Added BayTotalWidth property
#endregion

#region Version History: (CCM 8.0.2)
// V8-28986 : M.Shelley
//	Added friendly name definitions, the EnumerateDisplayableFieldInfos method and 
//  the ModelPropertyDisplayType.Length property for the bay width
#endregion

#region Version History: (CCM 8.1.0)
// V8-29598 : L.Luong
//	Added Height
#endregion

#region Version History : CCM830

// V8-31917 : A.Silva
//  Added some methods to help Plan Assignment Auto Match.
// CCM-13612 : Greg Richards
//  Changed the matches by location to only compare location code as location name can contain other content
//  whereas location code should only contain a valid relationship.

#endregion

#region Version History: CCM840

// CCM-13408 : J.Mendes
//  Added LocationSpaceProductGroup fields to the Planogram Assignment

#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using System.Collections.Generic;
using System.Linq;
using Galleria.Framework.ViewModel;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PlanAssignmentStoreSpaceInfo model object
    /// </summary>
    [Serializable]
    public sealed partial class PlanAssignmentStoreSpaceInfo : ModelReadOnlyObject<PlanAssignmentStoreSpaceInfo>
    {
		#region Properties

		#region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region LocationId
        /// <summary>
        /// LocationId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16> LocationIdProperty =
            RegisterModelProperty<Int16>(c => c.LocationId);
        /// <summary>
        /// Gets/Sets the LocationId value
        /// </summary>
        public Int16 LocationId
        {
            get { return GetProperty<Int16>(LocationIdProperty); }
        }
        #endregion

        #region LocationCode
        /// <summary>
        /// LocationCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationCodeProperty =
            RegisterModelProperty<String>(c => c.LocationCode);
        /// <summary>
        /// Gets/Sets the LocationCode value
        /// </summary>
        public String LocationCode
        {
            get { return GetProperty<String>(LocationCodeProperty); }
        }

        #endregion

        #region LocationName
        /// <summary>
        /// LocationName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationNameProperty =
            RegisterModelProperty<String>(c => c.LocationName);
        /// <summary>
        /// Gets/Sets the LocationName value
        /// </summary>
        public String LocationName
        {
            get { return GetProperty<String>(LocationNameProperty); }
        }
        #endregion

        #region LocationAddress
        /// <summary>
        /// LocationAddress property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationAddressProperty =
            RegisterModelProperty<String>(c => c.LocationAddress);
        /// <summary>
        /// Gets/Sets the LocationAddress value
        /// </summary>
        public String LocationAddress
        {
            get { return GetProperty<String>(LocationAddressProperty); }
        }
        #endregion

        #region ProductGroupId property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.ProductGroupId);
        /// <summary>
        /// 
        /// </summary>
        public Int32 ProductGroupId
        {
            get { return this.GetProperty<Int32>(ProductGroupIdProperty); }
        }

        #endregion

        #region ProductGroupCode
        /// <summary>
        /// ProductGroupCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductGroupCodeProperty =
            RegisterModelProperty<String>(c => c.ProductGroupCode);
        /// <summary>
        /// Returns the product group code
        /// </summary>
        public String ProductGroupCode
        {
            get { return this.GetProperty<String>(ProductGroupCodeProperty); }
        }
        #endregion
				
        #region ProductGroupName
        /// <summary>
        /// ProductGroupName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ProductGroupNameProperty =
            RegisterModelProperty<String>(c => c.ProductGroupName);
        /// <summary>
        /// Gets/Sets the ProductGroupName value
        /// </summary>
        public String ProductGroupName
        {
            get { return GetProperty<String>(ProductGroupNameProperty); }
        }
        #endregion

        #region BayDimensions
        /// <summary>
        /// BayDimensions property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> BayDimensionsProperty =
            RegisterModelProperty<String>(c => c.BayDimensions);
        /// <summary>
        /// Gets/Sets the BayDimensions value
        /// </summary>
        public String BayDimensions
        {
            get { return GetProperty<String>(BayDimensionsProperty); }
        }
        #endregion

        #region BayTotalWidth property
        /// <summary>
        /// BayTotalWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BayTotalWidthProperty =
            RegisterModelProperty<Double>(c => c.BayTotalWidth, Message.PlanAssignmentStoreSpaceInfo_BayTotalWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Returns the bay total width
        /// </summary>
        public Double BayTotalWidth
        {
            get { return this.GetProperty<Double>(BayTotalWidthProperty); }
        }
        #endregion

        #region BayHeight property
        /// <summary>
        /// BayHeight property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> BayHeightProperty =
            RegisterModelProperty<Double>(c => c.BayHeight, Message.PlanAssignmentStoreSpaceInfo_BayHeight, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public Double BayHeight
        {
            get { return this.GetProperty<Double>(BayHeightProperty); }
        }
        #endregion
				
        #region BayCount
        /// <summary>
        /// BayCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> BayCountProperty =
            RegisterModelProperty<Int32>(c => c.BayCount, Message.PlanAssignmentStoreSpaceInfo_BayCount);
        /// <summary>
        /// Return the bay count
        /// </summary>
        public Int32 BayCount
        {
            get { return this.GetProperty<Int32>(BayCountProperty); }
        }
        #endregion

        #region ClusterSchemeName property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<String> ClusterSchemeNameProperty =
            RegisterModelProperty<String>(c => c.ClusterSchemeName, Message.PlanAssignmentStoreSpaceInfo_ClusterSchemeName);
        /// <summary>
        /// 
        /// </summary>
        public String ClusterSchemeName
        {
            get { return this.GetProperty<String>(ClusterSchemeNameProperty); }
        }

        #endregion

        #region ClusterName property

        /// <summary>
        /// 
        /// </summary>
        public static readonly ModelPropertyInfo<String> ClusterNameProperty =
            RegisterModelProperty<String>(c => c.ClusterName, Message.PlanAssignmentStoreSpaceInfo_ClusterName);
        /// <summary>
        /// 
        /// </summary>
        public String ClusterName
        {
            get { return this.GetProperty<String>(ClusterNameProperty); }
        }

        #endregion

        #region ProductCount property
        /// <summary>
        /// ProductCount property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> ProductCountProperty =
            RegisterModelProperty<Int32>(c => c.ProductCount, Message.PlanAssignmentStoreSpaceInfo_ProductCount);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public Int32 ProductCount
        {
            get { return this.GetProperty<Int32>(ProductCountProperty); }
        }
        #endregion

        #region AverageBayWidth property
        /// <summary>
        /// AverageBayWidth property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Double> AverageBayWidthProperty =
            RegisterModelProperty<Double>(c => c.AverageBayWidth, Message.PlanAssignmentStoreSpaceInfo_AverageBayWidth, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public Double AverageBayWidth
        {
            get { return this.GetProperty<Double>(AverageBayWidthProperty); }
        }
        #endregion

        #region AisleName property
        /// <summary>
        /// AisleName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> AisleNameProperty =
            RegisterModelProperty<String>(c => c.AisleName, Message.PlanAssignmentStoreSpaceInfo_AisleName);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public String AisleName
        {
            get { return this.GetProperty<String>(AisleNameProperty); }
        }
        #endregion

        #region ValleyName property
        /// <summary>
        /// ValleyName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ValleyNameProperty =
            RegisterModelProperty<String>(c => c.ValleyName, Message.PlanAssignmentStoreSpaceInfo_ValleyName);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public String ValleyName
        {
            get { return this.GetProperty<String>(ValleyNameProperty); }
        }
        #endregion

        #region ZoneName property
        /// <summary>
        /// ZoneName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ZoneNameProperty =
            RegisterModelProperty<String>(c => c.ZoneName, Message.PlanAssignmentStoreSpaceInfo_ZoneName);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public String ZoneName
        {
            get { return this.GetProperty<String>(ZoneNameProperty); }
        }
        #endregion

        #region CustomAttribute01 property
        /// <summary>
        /// CustomAttribute01 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute01Property =
            RegisterModelProperty<String>(c => c.CustomAttribute01, Message.PlanAssignmentStoreSpaceInfo_CustomAttribute01);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public String CustomAttribute01
        {
            get { return this.GetProperty<String>(CustomAttribute01Property); }
        }
        #endregion

        #region CustomAttribute02 property
        /// <summary>
        /// CustomAttribute02 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute02Property =
            RegisterModelProperty<String>(c => c.CustomAttribute02, Message.PlanAssignmentStoreSpaceInfo_CustomAttribute02);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public String CustomAttribute02
        {
            get { return this.GetProperty<String>(CustomAttribute02Property); }
        }
        #endregion

        #region CustomAttribute03 property
        /// <summary>
        /// CustomAttribute03 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute03Property =
            RegisterModelProperty<String>(c => c.CustomAttribute03, Message.PlanAssignmentStoreSpaceInfo_CustomAttribute03);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public String CustomAttribute03
        {
            get { return this.GetProperty<String>(CustomAttribute03Property); }
        }
        #endregion

        #region CustomAttribute04 property
        /// <summary>
        /// CustomAttribute04 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute04Property =
            RegisterModelProperty<String>(c => c.CustomAttribute04, Message.PlanAssignmentStoreSpaceInfo_CustomAttribute04);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public String CustomAttribute04
        {
            get { return this.GetProperty<String>(CustomAttribute04Property); }
        }
        #endregion

        #region CustomAttribute05 property
        /// <summary>
        /// CustomAttribute05 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CustomAttribute05Property =
            RegisterModelProperty<String>(c => c.CustomAttribute05, Message.PlanAssignmentStoreSpaceInfo_CustomAttribute05);
        /// <summary>
        /// Returns the bay height
        /// </summary>
        public String CustomAttribute05
        {
            get { return this.GetProperty<String>(CustomAttribute05Property); }
        }
        #endregion
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanAssignmentStoreSpaceInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanAssignmentStoreSpaceInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanAssignmentStoreSpaceInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanAssignmentStoreSpaceInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }
        #endregion

		#region Criteria

		#region FetchByIdCriteria

		// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

		#endregion

		#endregion

        #region Methods

        /// <summary>
        /// Enumerates through infos for all properties on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(PlanAssignmentStoreSpaceInfo);

            // This would normally be the model class friendly name, but read-only model objects cannot set this,
            // so just set it to a localised string
            String typeFriendly = Message.PlanAssignmentStoreSpaceInfo_ClassFriendlyName;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.BayCountProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.BayHeightProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.BayTotalWidthProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.ProductCountProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.AverageBayWidthProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.AisleNameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.ValleyNameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.ZoneNameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.CustomAttribute01Property);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.CustomAttribute02Property);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.CustomAttribute03Property);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.CustomAttribute04Property);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.CustomAttribute05Property);
        }

        /// <summary>
        /// Enumerates through infos for all properties on this model that can be displayed to the user that are related to the Location.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfosLocation()
        {
            // This would normally be the model class friendly name, but read-only model objects cannot set this,
            // so just set it to a localised string
            Type type = typeof(PlanAssignmentStoreSpaceInfo);
            String typeFriendly = Location.FriendlyName;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.ClusterSchemeNameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, PlanAssignmentStoreSpaceInfo.ClusterNameProperty);
        }

        /// <summary>
        ///     Find the first <see cref="LocationPlanAssignment"/>, or null, in the given <paramref name="assignments"/> that matches the values in this instance.
        /// </summary>
        /// <param name="assignments">The enumeration of assigments to find a match in.</param>
        /// <returns>The matching element OR <c>null</c> if there are no matches.</returns>
        /// <remarks>Matches are determined by comparing <see cref="ProductGroupId"/> and <see cref="LocationId"/>.</remarks>
        public LocationPlanAssignment FirstMatchByCategoryAndLocationOrDefault(IEnumerable<LocationPlanAssignment> assignments)
        {
            return assignments.FirstOrDefault(x => x.ProductGroupId == ProductGroupId && x.LocationId == LocationId);
        }

        /// <summary>
        ///     Find the last <see cref="PlanogramInfo"/>, or null, in the given <paramref name="planInfos"/> that matches the location information in this instance.
        /// </summary>
        /// <param name="planInfos">The enumeration of planogram infos to find a match in.</param>
        /// <param name="match">Out parameter containing the matching <see cref="PlanogramInfo"/> if found. Null otherwise.</param>
        /// <returns><c>True</c> if the match was found, <c>false</c> otherwise.</returns>
        /// <remarks>Matches are determined by comparing <see cref="LocationCode"/>.</remarks>
        public Boolean TryMatchByLocation(IEnumerable<PlanogramInfo> planInfos, out PlanogramInfo match)
        {
            match = planInfos.LastOrDefault(info =>                                                    
                                                    String.Equals(info.LocationCode, LocationCode));
            return match != null;
        }

        /// <summary>
        ///     Filter all <see cref="PlanogramInfo"/> in the given <paramref name="planInfos"/> that matches the location information in this instance.
        /// </summary>
        /// <param name="planInfos">The enumeration of planogram infos to find matches in.</param>
        /// <returns>A list of <see cref="PlanogramInfo"/> with all the instances from the source <see cref="planInfos"/> enumeration that are matches.</returns>
        /// <remarks>Matches are determined by comparing <see cref="LocationCode"/>.</remarks>
        public List<PlanogramInfo> MatchesByLocation(IEnumerable<PlanogramInfo> planInfos)
        {
            return planInfos.Where(plan =>
                                   String.Equals(plan.LocationCode, LocationCode))
                            .ToList();
        }

        #endregion
    }
}
