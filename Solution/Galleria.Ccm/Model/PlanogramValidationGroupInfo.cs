﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)

// V8-26944 : A.Silva ~ Created.
// V8-27004 : A.Silva ~ Properly implemented.

#endregion

#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     PlanogramValidationGroup Info object.
    /// </summary>
    [Serializable]
    public sealed partial class PlanogramValidationGroupInfo : ModelReadOnlyObject<PlanogramValidationGroupInfo>
    {
        #region Properties

        #region Id

        /// <summary>
        ///		Metadata for the <see cref="Id"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(o => o.Id);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Id"/> property.
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }

        #endregion

        #region PlanogramValidationId

        /// <summary>
        ///		Metadata for the <see cref="PlanogramValidationId"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> PlanogramValidationIdProperty =
            RegisterModelProperty<Int32>(o => o.PlanogramValidationId);

        /// <summary>
        ///		Gets or sets the value for the <see cref="PlanogramValidationId"/> property.
        /// </summary>
        public Int32 PlanogramValidationId
        {
            get { return this.GetProperty<Int32>(PlanogramValidationIdProperty); }
        }

        #endregion

        #region Name

        /// <summary>
        ///		Metadata for the <see cref="Name"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(o => o.Name);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Name"/> property.
        /// </summary>
        public String Name
        {
            get { return this.GetProperty<String>(NameProperty); }
        }

        #endregion

        #region ResultType

        /// <summary>
        ///		Metadata for the <see cref="ResultType"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationTemplateResultType> ResultTypeProperty =
            RegisterModelProperty<PlanogramValidationTemplateResultType>(o => o.ResultType);

        /// <summary>
        ///		Gets or sets the value for the <see cref="ResultType"/> property.
        /// </summary>
        public PlanogramValidationTemplateResultType ResultType
        {
            get { return this.GetProperty<PlanogramValidationTemplateResultType>(ResultTypeProperty); }
        }

        #endregion

        #region Metrics

        /// <summary>
        ///		Metadata for the <see cref="Metrics"/> property.
        /// </summary>
        private static readonly ModelPropertyInfo<PlanogramValidationMetricInfoList> MetricsProperty =
            RegisterModelProperty<PlanogramValidationMetricInfoList>(o => o.Metrics);

        /// <summary>
        ///		Gets or sets the value for the <see cref="Metrics"/> property.
        /// </summary>
        public PlanogramValidationMetricInfoList Metrics
        {
            get { return this.GetProperty<PlanogramValidationMetricInfoList>(MetricsProperty); }
        }

        #endregion

        #endregion

        #region Authorization Rules

        /// <summary>
        ///     Defines the authorization rules for the <see cref="PlanogramValidationGroupInfo"/> type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PlanogramValidationGroupInfo), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationGroupInfo), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationGroupInfo), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Restricted.ToString()));
            BusinessRules.AddRule(typeof(PlanogramValidationGroupInfo), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Restricted.ToString()));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override String ToString()
        {
            return String.Format("{0} ({1})", this.Name, this.ResultType);
        }

        #endregion
    }
}
