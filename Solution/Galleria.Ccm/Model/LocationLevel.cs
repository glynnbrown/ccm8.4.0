﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// LocationLevel Model object
    /// </summary>
    [Serializable]
    public sealed partial class LocationLevel : ModelObject<LocationLevel>
    {
        #region Static Constructor
        static LocationLevel()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent

        /// <summary>
        /// Returns the parent level of this level
        /// </summary>
        public LocationLevel ParentLevel
        {
            get
            {
                LocationLevelList parentList = base.Parent as LocationLevelList;
                if (parentList != null)
                {
                    return parentList.Parent as LocationLevel;
                }
                return null;
            }
        }

        /// <summary>
        /// Returns the parent hierarchy of this level
        /// </summary>
        public LocationHierarchy ParentHierarchy
        {
            get
            {
                LocationHierarchy parentHierarchy = base.Parent as LocationHierarchy;
                if (parentHierarchy == null)
                {
                    LocationLevel currentParent = this.ParentLevel;
                    while (currentParent != null)
                    {
                        if (currentParent.ParentLevel != null)
                        {
                            currentParent = currentParent.ParentLevel;
                        }
                        else
                        {
                            return currentParent.Parent as LocationHierarchy;
                        }
                    }
                }
                else
                {
                    return parentHierarchy;
                }

                return null;
            }
        }

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set
            {
                String newValue = value;

                if (this.ParentHierarchy != null)
                {
                    Int32 levelNo = this.ParentHierarchy.EnumerateAllLevels().ToList().IndexOf(this);

                    IEnumerable<LocationLevel> otherLevels = this.ParentHierarchy.EnumerateAllLevels().Where(l => l != this);
                    while (otherLevels.Select(l => l.Name).Contains(newValue))
                    {
                        String levelDefaultName = String.Format(Message.LocationLevel_DefaultName, levelNo);
                        newValue = String.Format("{0} ({1})", newValue, levelDefaultName);

                        levelNo++;
                    }
                }
                SetProperty<String>(NameProperty, newValue);
            }
        }

        #endregion

        #region ShapeNo

        /// <summary>
        /// ShapeNo property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> ShapeNoProperty =
            RegisterModelProperty<Byte>(c => c.ShapeNo);

        /// <summary>
        /// Gets/Sets the ShapeNo value
        /// </summary>
        public Byte ShapeNo
        {
            get { return GetProperty<Byte>(ShapeNoProperty); }
            set { SetProperty<Byte>(ShapeNoProperty, value); }
        }

        #endregion

        #region Colour

        internal static readonly ModelPropertyInfo<Int32> ColourProperty =
          RegisterModelProperty<Int32>(c => c.Colour);
        /// <summary>
        /// The colour used to display this level
        /// </summary>
        /// <remarks>Hide from ui - it will use resolved colour instead</remarks>
        internal Int32 Colour
        {
            get { return GetProperty<Int32>(ColourProperty); }
            set { SetProperty<Int32>(ColourProperty, value); }
        }

        /// <summary>
        /// Gets/Sets the level colour
        /// This exists as a helper as media.colour is not serializable.
        /// </summary>
        public Color ResolvedColour
        {
            get
            {
                Byte[] colourBytes = BitConverter.GetBytes(GetProperty<Int32>(ColourProperty));
                if (colourBytes.Length == 4)
                {
                    return
                        Color.FromArgb(colourBytes[0], colourBytes[1], colourBytes[2], colourBytes[3]);

                }
                else
                {
                    return Colors.White;
                }
            }
            set
            {
                this.Colour = BitConverter.ToInt32(
                    new Byte[] { value.A, value.R, value.G, value.B }, 0);
                OnPropertyChanged("ResolvedColour");
            }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region Child Level

        private static readonly ModelPropertyInfo<LocationLevelList> ChildLevelListProperty =
            RegisterModelProperty<LocationLevelList>(c => c.ChildLevelList);
        /// <summary>
        /// The Child level list: see GFS-13403
        /// </summary>
        private LocationLevelList ChildLevelList
        {
            get
            {
                LocationLevelList returnList = GetProperty<LocationLevelList>(ChildLevelListProperty);
                if (returnList == null)
                {
                    returnList = LocationLevelList.NewLocationLevelList();
                    LoadProperty<LocationLevelList>(ChildLevelListProperty, returnList);
                }
                return returnList;
            }
        }
        public static String ChildLevelProperty = "ChildLevel";
        /// <summary>
        /// The child level of this, may be null
        /// To move an item to here first set it here then set the old parent property value to null
        /// </summary>
        public LocationLevel ChildLevel
        {
            get { return this.ChildLevelList.FirstOrDefault(); }
            set
            {
                //remove the existing level 
                //- if it was being moved anywhere it should have already gone.
                LocationLevel existingLevel = this.ChildLevelList.FirstOrDefault();
                if (existingLevel != null)
                {
                    this.ChildLevelList.Remove(existingLevel);
                }

                if (value != null)
                {
                    //add the level to this list
                    this.ChildLevelList.Add(value);
                }

                //fire the on property changed for this
                OnPropertyChanged("ChildLevel");
            }
        }

        #endregion

        #endregion

        #region Helper Properties

        /// <summary>
        /// Returns true if this is the root level
        /// </summary>
        public Boolean IsRoot
        {
            get { return this.ParentLevel == null; }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));

        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationLevel), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(LocationLevel), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationLevel), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationHierarchyEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationLevel), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationHierarchyEdit.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static LocationLevel NewLocationLevel()
        {
            LocationLevel item = new LocationLevel();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// ToString override
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return this.Name;
        }

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<LocationLevel>(oldId, newId);
        }
        #endregion
    }
}