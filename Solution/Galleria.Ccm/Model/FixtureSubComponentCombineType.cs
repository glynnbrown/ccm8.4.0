﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// L.Hodson
//  Initial version.
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Helpers;

namespace Galleria.Ccm.Model
{
    public enum FixtureSubComponentCombineType
    {
        None = 0,
        Both = 1,
        LeftOnly = 2,
        RightOnly = 3
    }

    /// <summary>
    /// FixtureSubComponentCombineType Helper Class
    /// </summary>
    public static class FixtureSubComponentCombineTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<FixtureSubComponentCombineType, String> FriendlyNames =
            new Dictionary<FixtureSubComponentCombineType, String>()
            {
                {FixtureSubComponentCombineType.None, Message.Enum_FixtureSubComponentCombineType_None},
                {FixtureSubComponentCombineType.Both, Message.Enum_FixtureSubComponentCombineType_Both},
                {FixtureSubComponentCombineType.LeftOnly, Message.Enum_FixtureSubComponentCombineType_LeftOnly},
                {FixtureSubComponentCombineType.RightOnly, Message.Enum_FixtureSubComponentCombineType_RightOnly},
            };

        public static FixtureSubComponentCombineType Parse(String enumName)
        {
            return EnumHelper.Parse<FixtureSubComponentCombineType>(enumName, FixtureSubComponentCombineType.None);
        }
    }
}
