﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 8.0)
// V8-24265 : J.Pickup
//  Created
#endregion
#region Version History: (CCM 8.03)
// V8-28662 : L.Ineson
//  Added ToPlanogramLabelHorizontalAlignment
#endregion

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum LabelHorizontalPlacementType
    {
        Left = 0,
        Center = 1,
        Right = 2,
    }

    public static class LabelHorizontalPlacementTypeHelper
    {
        public static readonly Dictionary<LabelHorizontalPlacementType, String> FriendlyNames =
            new Dictionary<LabelHorizontalPlacementType, String>()
            {
                {LabelHorizontalPlacementType.Left, Message.Enum_LabelHorizontalPlacementType_Left}, 
                {LabelHorizontalPlacementType.Center, Message.Enum_LabelHorizontalPlacementType_Center},
                {LabelHorizontalPlacementType.Right, Message.Enum_LabelHorizontalPlacementType_Right},
            };

        public static PlanogramLabelHorizontalAlignment ToPlanogramLabelHorizontalAlignment(this LabelHorizontalPlacementType value)
        {
            switch (value)
            {
                default:
                case LabelHorizontalPlacementType.Center: return PlanogramLabelHorizontalAlignment.Center;
                case LabelHorizontalPlacementType.Left: return PlanogramLabelHorizontalAlignment.Left;
                case LabelHorizontalPlacementType.Right: return PlanogramLabelHorizontalAlignment.Right;
            }
        }
    }
}
