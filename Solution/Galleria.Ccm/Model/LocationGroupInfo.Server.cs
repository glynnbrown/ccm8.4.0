﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25445 : L.Ineson
//	Created (Auto-generated)
// CCM-25444 : N.Haywood
//      Added FetchByLocationGroupIds
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using System.Collections.Generic;


namespace Galleria.Ccm.Model
{
    public partial class LocationGroupInfo
    {
        #region Constructors
        private LocationGroupInfo() { } // force the use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static LocationGroupInfo Fetch(IDalContext dalContext, LocationGroupInfoDto dto)
        {
            return DataPortal.FetchChild<LocationGroupInfo>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, LocationGroupInfoDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<String>(NameProperty, dto.Name);
            this.LoadProperty<String>(CodeProperty, dto.Code);
            this.LoadProperty<DateTime?>(DateDeletedProperty, dto.DateDeleted);
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationGroupInfoDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Returns a single productgroup info by id.
        /// Uses the LocationGroupInfoList FetchByLocationGroupIds.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static LocationGroupInfo FetchById(Int32 id)
        {
            LocationGroupInfoList list = LocationGroupInfoList.FetchByLocationGroupIds(new List<Int32> { id });

            if (list.Count > 0)
            {
                return list[0];
            }

            return null;
        }

        #endregion
    }
}
