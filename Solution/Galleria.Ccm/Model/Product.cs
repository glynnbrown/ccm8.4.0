﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: CCM800
// L.Hodson
//  Created
// V8-25669 : A.Kuszyk
//  Added EntityId and ReplacementProductId properties. Updated business rules.
// V8-25452 : N.Haywood
//	Readded product group id and missing fetch methods required for imports
// V8-25787 : N.Foster
//  Added support for IPlanogramProduct
// V8-26041 : A.Kuszyk
//  Changed use of CCM ProductStatusType to Framework PlanogramProductStatusType.
//  Changed use of CCM ProductOrientationType to Framework PlanogramProductOrientationType.
//  Introduced use of PlanogramProductMerchandisingStyle type for MerchandisingStyle property.
//  Renamed FillPattern property to FillPatternType.
//  Added ImageList property.
//  Implemented IPlanogramProductImageHelper
//  Added ShapeType and changed FillColour to Int32.
//  Removed IPlanogramProduct Members region - all properties are now implemented.
// CCM-26150 : L.Ineson
//	Added CustomAttributes property
// CCM-26516 : L.Ineson
//  Corrected squeeze height width and depth.
// V8-26704 : A.Kuszyk
//  Implemented IPlanogramProductInfo.
// V8-26560 : A.Probyn
//  Added ModelPropertyDisplayTypes
// V8-27424 : L.Luong
//  Added FinancialGroupCode and FinancialGroupName
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
// V8-26385 : L.Luong
//  Added RowVersion
// V8-26777 : L.Ineson
//  Removed CanMerch properties
// V8-27961 : A.Probyn
//  Added default values to enum properties
// V8-28274 : A.Probyn
//  Removed authorization rules
// V8-28348 : A.Kuszyk
//  Protected Squeeze properties from divisions by zero.
#endregion
#region Version History: CCM801
// V8-28722 : N.Foster
//  Added support for batch dal operations
#endregion
#region Version History: CCM810
// V8-29593 : L.Ineson
//  Fill colour now defaults to white.
#endregion
#region Version History: CCM820
// V8-30865 : N.Haywood
//  Increased length of some product attributes
// V8-31456 : N.Foster
//  Removed asynchronous custom attributes
#endregion
#region Version History: CCM830
// V8-31531 : A.Heathcote
//  Removed IsTrayProducts
// V8-31564 : M.Shelley
//  Renamed PlanogramProductPegProngOffset to PlanogramProductPegProngOffsetX
//  Added new field PlanogramProductPegProngOffsetY
// V8-31866 : D.Pleasance
//  Amended business rules so that Master.Product match Content.PlanogramProduct
// V8-30697 : N.Haywood
//  Removed NestMaxHigh and NestMaxDeep
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Planograms.Interfaces;
using Galleria.Framework.Planograms.Model;
using Galleria.Framework.Rules;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Product model Object
    /// </summary>
    [Serializable]
    public sealed partial class Product :
        ModelObject<Product>,
        IPlanogramProduct,
        IPlanogramProductImageHelper,
        IPlanogramProductInfo,
        ICustomAttributeDataParent
    {
        #region Static Constructor
        /// <summary>
        /// Static constructor for this type
        /// </summary>
        static Product()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Fields

        private object _attributeDataLock = new object(); //object used to synchronise access to attribute data
        private Object _imageListLock = new Object(); //object used to synchronise access to image data

        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        internal static readonly ModelPropertyInfo<Int32> IdProperty =
               RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The unique Object id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        /// <summary>
        /// The unique Object id
        /// </summary>
        Object IPlanogramProduct.Id
        {
            get { return this.Id; }
        }

        /// <summary>
        /// The unique object id
        /// </summary>
        Object ICustomAttributeDataParent.Id
        {
            get { return this.Id; }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId
        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);
        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }
        #endregion

        #region ProductGroupId
        public static readonly ModelPropertyInfo<Int32?> ProductGroupIdProperty =
            RegisterModelProperty<Int32?>(c => c.ProductGroupId);
        /// <summary>
        /// The ID of the Product Group to which this product belongs, if the info is available.
        /// </summary>
        public Int32? ProductGroupId
        {
            get { return GetProperty<Int32?>(ProductGroupIdProperty); }
            set { SetProperty<Int32?>(ProductGroupIdProperty, value); }
        }
        #endregion

        #region ReplacementProductId
        /// <summary>
        /// ReplacementProductId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> ReplacementProductIdProperty =
            RegisterModelProperty<Int32?>(c => c.ReplacementProductId);
        /// <summary>
        /// Gets/Sets the ReplacementProductId value
        /// </summary>
        public Int32? ReplacementProductId
        {
            get { return GetProperty<Int32?>(ReplacementProductIdProperty); }
            set { SetProperty<Int32?>(ReplacementProductIdProperty, value); }
        }
        #endregion

        #region Gtin
        public static readonly ModelPropertyInfo<String> GtinProperty =
            RegisterModelProperty<String>(c => c.Gtin, Message.Product_Gtin);
        public String Gtin
        {
            get { return GetProperty<String>(GtinProperty); }
            set { SetProperty<String>(GtinProperty, value); }
        }
        #endregion

        #region Name
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Product_Name);
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }
        #endregion

        #region Height
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.Product_Height, ModelPropertyDisplayType.Length);
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }
        #endregion

        #region Width
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.Product_Width, ModelPropertyDisplayType.Length);
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }
        #endregion

        #region Depth
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.Product_Depth, ModelPropertyDisplayType.Length);
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }
        #endregion

        #region DisplayHeight
        public static readonly ModelPropertyInfo<Single> DisplayHeightProperty =
            RegisterModelProperty<Single>(c => c.DisplayHeight, Message.Product_DisplayHeight, ModelPropertyDisplayType.Length);
        public Single DisplayHeight
        {
            get { return GetProperty<Single>(DisplayHeightProperty); }
            set { SetProperty<Single>(DisplayHeightProperty, value); }
        }
        #endregion

        #region DisplayWidth
        public static readonly ModelPropertyInfo<Single> DisplayWidthProperty =
            RegisterModelProperty<Single>(c => c.DisplayWidth, Message.Product_DisplayWidth, ModelPropertyDisplayType.Length);
        public Single DisplayWidth
        {
            get { return GetProperty<Single>(DisplayWidthProperty); }
            set { SetProperty<Single>(DisplayWidthProperty, value); }
        }
        #endregion

        #region DisplayDepth
        public static readonly ModelPropertyInfo<Single> DisplayDepthProperty =
            RegisterModelProperty<Single>(c => c.DisplayDepth, Message.Product_DisplayDepth, ModelPropertyDisplayType.Length);
        public Single DisplayDepth
        {
            get { return GetProperty<Single>(DisplayDepthProperty); }
            set { SetProperty<Single>(DisplayDepthProperty, value); }
        }
        #endregion

        #region AlternateHeight
        public static readonly ModelPropertyInfo<Single> AlternateHeightProperty =
            RegisterModelProperty<Single>(c => c.AlternateHeight, Message.Product_AlternateHeight, ModelPropertyDisplayType.Length);
        public Single AlternateHeight
        {
            get { return GetProperty<Single>(AlternateHeightProperty); }
            set { SetProperty<Single>(AlternateHeightProperty, value); }
        }
        #endregion

        #region AlternateWidth
        public static readonly ModelPropertyInfo<Single> AlternateWidthProperty =
            RegisterModelProperty<Single>(c => c.AlternateWidth, Message.Product_AlternateWidth, ModelPropertyDisplayType.Length);
        public Single AlternateWidth
        {
            get { return GetProperty<Single>(AlternateWidthProperty); }
            set { SetProperty<Single>(AlternateWidthProperty, value); }
        }
        #endregion

        #region AlternateDepth
        public static readonly ModelPropertyInfo<Single> AlternateDepthProperty =
            RegisterModelProperty<Single>(c => c.AlternateDepth, Message.Product_AlternateDepth, ModelPropertyDisplayType.Length);
        public Single AlternateDepth
        {
            get { return GetProperty<Single>(AlternateDepthProperty); }
            set { SetProperty<Single>(AlternateDepthProperty, value); }
        }
        #endregion

        #region PointOfPurchaseHeight
        public static readonly ModelPropertyInfo<Single> PointOfPurchaseHeightProperty =
            RegisterModelProperty<Single>(c => c.PointOfPurchaseHeight, Message.Product_PointOfPurchaseHeight, ModelPropertyDisplayType.Length);
        public Single PointOfPurchaseHeight
        {
            get { return GetProperty<Single>(PointOfPurchaseHeightProperty); }
            set { SetProperty<Single>(PointOfPurchaseHeightProperty, value); }
        }
        #endregion

        #region PointOfPurchaseWidth
        public static readonly ModelPropertyInfo<Single> PointOfPurchaseWidthProperty =
            RegisterModelProperty<Single>(c => c.PointOfPurchaseWidth, Message.Product_PointOfPurchaseWidth, ModelPropertyDisplayType.Length);
        public Single PointOfPurchaseWidth
        {
            get { return GetProperty<Single>(PointOfPurchaseWidthProperty); }
            set { SetProperty<Single>(PointOfPurchaseWidthProperty, value); }
        }
        #endregion

        #region PointOfPurchaseDepth
        public static readonly ModelPropertyInfo<Single> PointOfPurchaseDepthProperty =
            RegisterModelProperty<Single>(c => c.PointOfPurchaseDepth, Message.Product_PointOfPurchaseDepth, ModelPropertyDisplayType.Length);
        public Single PointOfPurchaseDepth
        {
            get { return GetProperty<Single>(PointOfPurchaseDepthProperty); }
            set { SetProperty<Single>(PointOfPurchaseDepthProperty, value); }
        }
        #endregion

        #region NumberOfPegHoles
        public static readonly ModelPropertyInfo<Byte> NumberOfPegHolesProperty =
            RegisterModelProperty<Byte>(c => c.NumberOfPegHoles, Message.Product_NumberOfPegHoles);
        public Byte NumberOfPegHoles
        {
            get { return GetProperty<Byte>(NumberOfPegHolesProperty); }
            set { SetProperty<Byte>(NumberOfPegHolesProperty, value); }
        }
        #endregion

        #region PegX
        public static readonly ModelPropertyInfo<Single> PegXProperty =
            RegisterModelProperty<Single>(c => c.PegX, Message.Product_PegX, ModelPropertyDisplayType.Length);
        public Single PegX
        {
            get { return GetProperty<Single>(PegXProperty); }
            set { SetProperty<Single>(PegXProperty, value); }
        }
        #endregion

        #region PegX2
        public static readonly ModelPropertyInfo<Single> PegX2Property =
            RegisterModelProperty<Single>(c => c.PegX2, Message.Product_PegX2, ModelPropertyDisplayType.Length);
        public Single PegX2
        {
            get { return GetProperty<Single>(PegX2Property); }
            set { SetProperty<Single>(PegX2Property, value); }
        }
        #endregion

        #region PegX3
        public static readonly ModelPropertyInfo<Single> PegX3Property =
            RegisterModelProperty<Single>(c => c.PegX3, Message.Product_PegX3, ModelPropertyDisplayType.Length);
        public Single PegX3
        {
            get { return GetProperty<Single>(PegX3Property); }
            set { SetProperty<Single>(PegX3Property, value); }
        }
        #endregion

        #region PegY
        public static readonly ModelPropertyInfo<Single> PegYProperty =
            RegisterModelProperty<Single>(c => c.PegY, Message.Product_PegY, ModelPropertyDisplayType.Length);
        public Single PegY
        {
            get { return GetProperty<Single>(PegYProperty); }
            set { SetProperty<Single>(PegYProperty, value); }
        }
        #endregion

        #region PegY2
        public static readonly ModelPropertyInfo<Single> PegY2Property =
            RegisterModelProperty<Single>(c => c.PegY2, Message.Product_PegY2, ModelPropertyDisplayType.Length);
        public Single PegY2
        {
            get { return GetProperty<Single>(PegY2Property); }
            set { SetProperty<Single>(PegY2Property, value); }
        }
        #endregion

        #region PegY3
        public static readonly ModelPropertyInfo<Single> PegY3Property =
            RegisterModelProperty<Single>(c => c.PegY3, Message.Product_PegY3, ModelPropertyDisplayType.Length);
        public Single PegY3
        {
            get { return GetProperty<Single>(PegY3Property); }
            set { SetProperty<Single>(PegY3Property, value); }
        }
        #endregion

        #region PegProngOffsetX

        public static readonly ModelPropertyInfo<Single> PegProngOffsetXProperty =
            RegisterModelProperty<Single>(c => c.PegProngOffsetX, Message.Product_PegProngOffsetX, ModelPropertyDisplayType.Length);
        public Single PegProngOffsetX
        {
            get { return GetProperty<Single>(PegProngOffsetXProperty); }
            set { SetProperty<Single>(PegProngOffsetXProperty, value); }
        }

        #endregion

        #region PegProngOffsetY

        public static readonly ModelPropertyInfo<Single> PegProngOffsetYProperty =
            RegisterModelProperty<Single>(c => c.PegProngOffsetY, Message.Product_PegProngOffsetY, ModelPropertyDisplayType.Length);
        public Single PegProngOffsetY
        {
            get { return GetProperty<Single>(PegProngOffsetYProperty); }
            set { SetProperty<Single>(PegProngOffsetYProperty, value); }
        }

        #endregion

        #region PegDepth
        public static readonly ModelPropertyInfo<Single> PegDepthProperty =
            RegisterModelProperty<Single>(c => c.PegDepth, Message.Product_PegDepth, ModelPropertyDisplayType.Length);
        public Single PegDepth
        {
            get { return GetProperty<Single>(PegDepthProperty); }
            set { SetProperty<Single>(PegDepthProperty, value); }
        }
        #endregion

        #region SqueezeHeight

        public static readonly ModelPropertyInfo<Single> SqueezeHeightProperty =
            RegisterModelProperty<Single>(c => c.SqueezeHeight, Message.Product_SqueezeHeight, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// The product SqueezeHeight - this is the percentage squeeze to
        /// e.g. 100% = no squeeze
        /// </summary>
        public Single SqueezeHeight
        {
            get { return GetProperty<Single>(SqueezeHeightProperty); }
            set
            {
                SetProperty<Single>(SqueezeHeightProperty, value);
                SetProperty<Single>(SqueezeHeightActualProperty, this.Height * (value / (Single)1.00));
            }
        }

        public static readonly ModelPropertyInfo<Single> SqueezeHeightActualProperty =
        RegisterModelProperty<Single>(c => c.SqueezeHeightActual, Message.Product_SqueezeHeightActual, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the actual height to which the product may be squeezed
        /// </summary>
        public Single SqueezeHeightActual
        {
            get { return this.Height * (this.SqueezeHeight / (Single)1.00); }
            set
            {
                this.SqueezeHeight = (this.Height.EqualTo(0) ? 1F : (value / this.Height)) * (Single)1.00;
                SetProperty<Single>(SqueezeHeightActualProperty, value);
            }
        }

        #endregion

        #region SqueezeWidth

        public static readonly ModelPropertyInfo<Single> SqueezeWidthProperty =
         RegisterModelProperty<Single>(c => c.SqueezeWidth, Message.Product_SqueezeWidth, String.Empty, (Single)1.00, RelationshipTypes.None, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// The product SqueezeWidth
        /// </summary>
        public Single SqueezeWidth
        {
            get { return GetProperty<Single>(SqueezeWidthProperty); }
            set
            {
                SetProperty<Single>(SqueezeWidthProperty, value);
                SetProperty<Single>(SqueezeWidthActualProperty, this.Width * (value / (Single)1.00));
            }
        }

        public static readonly ModelPropertyInfo<Single> SqueezeWidthActualProperty =
        RegisterModelProperty<Single>(c => c.SqueezeWidthActual, Message.Product_SqueezeWidthActual, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the actual height to which the product may be squeezed
        /// </summary>
        public Single SqueezeWidthActual
        {
            get { return this.Width * (this.SqueezeWidth / (Single)1.00); }
            set
            {
                this.SqueezeWidth = (this.Width.EqualTo(0) ? 1F : (value / this.Width)) * (Single)1.00;
                SetProperty<Single>(SqueezeWidthActualProperty, value);
            }
        }
        #endregion

        #region SqueezeDepth
        public static readonly ModelPropertyInfo<Single> SqueezeDepthProperty =
        RegisterModelProperty<Single>(c => c.SqueezeDepth, Message.Product_SqueezeDepth, String.Empty, (Single)1.00, RelationshipTypes.None, ModelPropertyDisplayType.Percentage);
        /// <summary>
        /// The product SqueezeDepth
        /// </summary>
        public Single SqueezeDepth
        {
            get { return GetProperty<Single>(SqueezeDepthProperty); }
            set
            {
                SetProperty<Single>(SqueezeDepthProperty, value);
                SetProperty<Single>(SqueezeDepthActualProperty, this.Depth * (value / (Single)1.00));
            }
        }

        public static readonly ModelPropertyInfo<Single> SqueezeDepthActualProperty =
        RegisterModelProperty<Single>(c => c.SqueezeDepthActual, Message.Product_SqueezeDepthActual, ModelPropertyDisplayType.Length);
        /// <summary>
        /// Gets/Sets the actual height to which the product may be squeezed
        /// </summary>
        public Single SqueezeDepthActual
        {
            get { return this.Depth * (this.SqueezeDepth / (Single)1.00); }
            set
            {
                this.SqueezeDepth = (this.Depth.EqualTo(0) ? 1F : (value / this.Depth)) * (Single)1.00;
                SetProperty<Single>(SqueezeDepthActualProperty, value);
            }
        }
        #endregion

        #region NestingHeight
        public static readonly ModelPropertyInfo<Single> NestingHeightProperty =
            RegisterModelProperty<Single>(c => c.NestingHeight, Message.Product_NestingHeight, ModelPropertyDisplayType.Length);
        public Single NestingHeight
        {
            get { return GetProperty<Single>(NestingHeightProperty); }
            set { SetProperty<Single>(NestingHeightProperty, value); }
        }
        #endregion

        #region NestingWidth
        public static readonly ModelPropertyInfo<Single> NestingWidthProperty =
            RegisterModelProperty<Single>(c => c.NestingWidth, Message.Product_NestingWidth, ModelPropertyDisplayType.Length);
        public Single NestingWidth
        {
            get { return GetProperty<Single>(NestingWidthProperty); }
            set { SetProperty<Single>(NestingWidthProperty, value); }
        }
        #endregion

        #region NestingDepth
        public static readonly ModelPropertyInfo<Single> NestingDepthProperty =
            RegisterModelProperty<Single>(c => c.NestingDepth, Message.Product_NestingDepth, ModelPropertyDisplayType.Length);
        public Single NestingDepth
        {
            get { return GetProperty<Single>(NestingDepthProperty); }
            set { SetProperty<Single>(NestingDepthProperty, value); }
        }
        #endregion

        #region CasePackUnits
        public static readonly ModelPropertyInfo<Int16> CasePackUnitsProperty =
            RegisterModelProperty<Int16>(c => c.CasePackUnits, Message.Product_CasePackUnits);
        public Int16 CasePackUnits
        {
            get { return GetProperty<Int16>(CasePackUnitsProperty); }
            set { SetProperty<Int16>(CasePackUnitsProperty, value); }
        }
        #endregion

        #region CaseHigh
        public static readonly ModelPropertyInfo<Byte> CaseHighProperty =
            RegisterModelProperty<Byte>(c => c.CaseHigh, Message.Product_CaseHigh);
        public Byte CaseHigh
        {
            get { return GetProperty<Byte>(CaseHighProperty); }
            set { SetProperty<Byte>(CaseHighProperty, value); }
        }
        #endregion

        #region CaseWide
        public static readonly ModelPropertyInfo<Byte> CaseWideProperty =
            RegisterModelProperty<Byte>(c => c.CaseWide, Message.Product_CaseWide);
        public Byte CaseWide
        {
            get { return GetProperty<Byte>(CaseWideProperty); }
            set { SetProperty<Byte>(CaseWideProperty, value); }
        }
        #endregion

        #region CaseDeep
        public static readonly ModelPropertyInfo<Byte> CaseDeepProperty =
            RegisterModelProperty<Byte>(c => c.CaseDeep, Message.Product_CaseDeep);
        public Byte CaseDeep
        {
            get { return GetProperty<Byte>(CaseDeepProperty); }
            set { SetProperty<Byte>(CaseDeepProperty, value); }
        }
        #endregion

        #region CaseHeight
        public static readonly ModelPropertyInfo<Single> CaseHeightProperty =
            RegisterModelProperty<Single>(c => c.CaseHeight, Message.Product_CaseHeight, ModelPropertyDisplayType.Length);
        public Single CaseHeight
        {
            get { return GetProperty<Single>(CaseHeightProperty); }
            set { SetProperty<Single>(CaseHeightProperty, value); }
        }
        #endregion

        #region CaseWidth
        public static readonly ModelPropertyInfo<Single> CaseWidthProperty =
            RegisterModelProperty<Single>(c => c.CaseWidth, Message.Product_CaseWidth, ModelPropertyDisplayType.Length);
        public Single CaseWidth
        {
            get { return GetProperty<Single>(CaseWidthProperty); }
            set { SetProperty<Single>(CaseWidthProperty, value); }
        }
        #endregion

        #region CaseDepth
        public static readonly ModelPropertyInfo<Single> CaseDepthProperty =
            RegisterModelProperty<Single>(c => c.CaseDepth, Message.Product_CaseDepth, ModelPropertyDisplayType.Length);
        public Single CaseDepth
        {
            get { return GetProperty<Single>(CaseDepthProperty); }
            set { SetProperty<Single>(CaseDepthProperty, value); }
        }
        #endregion

        #region MaxStack
        public static readonly ModelPropertyInfo<Byte> MaxStackProperty =
            RegisterModelProperty<Byte>(c => c.MaxStack, Message.Product_MaxStack);
        public Byte MaxStack
        {
            get { return GetProperty<Byte>(MaxStackProperty); }
            set { SetProperty<Byte>(MaxStackProperty, value); }
        }
        #endregion

        #region MaxTopCap
        public static readonly ModelPropertyInfo<Byte> MaxTopCapProperty =
            RegisterModelProperty<Byte>(c => c.MaxTopCap, Message.Product_MaxTopCap);
        public Byte MaxTopCap
        {
            get { return GetProperty<Byte>(MaxTopCapProperty); }
            set { SetProperty<Byte>(MaxTopCapProperty, value); }
        }
        #endregion

        #region MaxRightCap
        public static readonly ModelPropertyInfo<Byte> MaxRightCapProperty =
            RegisterModelProperty<Byte>(c => c.MaxRightCap, Message.Product_MaxRightCap);
        public Byte MaxRightCap
        {
            get { return GetProperty<Byte>(MaxRightCapProperty); }
            set { SetProperty<Byte>(MaxRightCapProperty, value); }
        }
        #endregion

        #region MinDeep
        public static readonly ModelPropertyInfo<Byte> MinDeepProperty =
            RegisterModelProperty<Byte>(c => c.MinDeep, Message.Product_MinDeep);
        public Byte MinDeep
        {
            get { return GetProperty<Byte>(MinDeepProperty); }
            set { SetProperty<Byte>(MinDeepProperty, value); }
        }
        #endregion

        #region MaxDeep
        public static readonly ModelPropertyInfo<Byte> MaxDeepProperty =
            RegisterModelProperty<Byte>(c => c.MaxDeep, Message.Product_MaxDeep);
        public Byte MaxDeep
        {
            get { return GetProperty<Byte>(MaxDeepProperty); }
            set { SetProperty<Byte>(MaxDeepProperty, value); }
        }
        #endregion

        #region TrayPackUnits
        public static readonly ModelPropertyInfo<Int16> TrayPackUnitsProperty =
            RegisterModelProperty<Int16>(c => c.TrayPackUnits, Message.Product_TrayPackUnits);
        public Int16 TrayPackUnits
        {
            get { return GetProperty<Int16>(TrayPackUnitsProperty); }
            set { SetProperty<Int16>(TrayPackUnitsProperty, value); }
        }
        #endregion

        #region TrayHigh
        public static readonly ModelPropertyInfo<Byte> TrayHighProperty =
            RegisterModelProperty<Byte>(c => c.TrayHigh, Message.Product_TrayHigh);
        public Byte TrayHigh
        {
            get { return GetProperty<Byte>(TrayHighProperty); }
            set { SetProperty<Byte>(TrayHighProperty, value); }
        }
        #endregion

        #region TrayWide
        public static readonly ModelPropertyInfo<Byte> TrayWideProperty =
            RegisterModelProperty<Byte>(c => c.TrayWide, Message.Product_TrayWide);
        public Byte TrayWide
        {
            get { return GetProperty<Byte>(TrayWideProperty); }
            set { SetProperty<Byte>(TrayWideProperty, value); }
        }
        #endregion

        #region TrayDeep
        public static readonly ModelPropertyInfo<Byte> TrayDeepProperty =
            RegisterModelProperty<Byte>(c => c.TrayDeep, Message.Product_TrayDeep);
        public Byte TrayDeep
        {
            get { return GetProperty<Byte>(TrayDeepProperty); }
            set { SetProperty<Byte>(TrayDeepProperty, value); }
        }
        #endregion

        #region TrayHeight
        public static readonly ModelPropertyInfo<Single> TrayHeightProperty =
            RegisterModelProperty<Single>(c => c.TrayHeight, Message.Product_TrayHeight, ModelPropertyDisplayType.Length);
        public Single TrayHeight
        {
            get { return GetProperty<Single>(TrayHeightProperty); }
            set { SetProperty<Single>(TrayHeightProperty, value); }
        }
        #endregion

        #region TrayWidth
        public static readonly ModelPropertyInfo<Single> TrayWidthProperty =
            RegisterModelProperty<Single>(c => c.TrayWidth, Message.Product_TrayWidth, ModelPropertyDisplayType.Length);
        public Single TrayWidth
        {
            get { return GetProperty<Single>(TrayWidthProperty); }
            set { SetProperty<Single>(TrayWidthProperty, value); }
        }
        #endregion

        #region TrayDepth
        public static readonly ModelPropertyInfo<Single> TrayDepthProperty =
            RegisterModelProperty<Single>(c => c.TrayDepth, Message.Product_TrayDepth, ModelPropertyDisplayType.Length);
        public Single TrayDepth
        {
            get { return GetProperty<Single>(TrayDepthProperty); }
            set { SetProperty<Single>(TrayDepthProperty, value); }
        }
        #endregion

        #region TrayThickHeight
        public static readonly ModelPropertyInfo<Single> TrayThickHeightProperty =
            RegisterModelProperty<Single>(c => c.TrayThickHeight, Message.Product_TrayThickHeight, ModelPropertyDisplayType.Length);
        public Single TrayThickHeight
        {
            get { return GetProperty<Single>(TrayThickHeightProperty); }
            set { SetProperty<Single>(TrayThickHeightProperty, value); }
        }
        #endregion

        #region TrayThickWidth
        public static readonly ModelPropertyInfo<Single> TrayThickWidthProperty =
            RegisterModelProperty<Single>(c => c.TrayThickWidth, Message.Product_TrayThickWidth, ModelPropertyDisplayType.Length);
        public Single TrayThickWidth
        {
            get { return GetProperty<Single>(TrayThickWidthProperty); }
            set { SetProperty<Single>(TrayThickWidthProperty, value); }
        }
        #endregion

        #region TrayThickDepth
        public static readonly ModelPropertyInfo<Single> TrayThickDepthProperty =
            RegisterModelProperty<Single>(c => c.TrayThickDepth, Message.Product_TrayThickDepth, ModelPropertyDisplayType.Length);
        public Single TrayThickDepth
        {
            get { return GetProperty<Single>(TrayThickDepthProperty); }
            set { SetProperty<Single>(TrayThickDepthProperty, value); }
        }
        #endregion

        #region FrontOverhang
        public static readonly ModelPropertyInfo<Single> FrontOverhangProperty =
            RegisterModelProperty<Single>(c => c.FrontOverhang, Message.Product_FrontOverhang, ModelPropertyDisplayType.Length);
        public Single FrontOverhang
        {
            get { return GetProperty<Single>(FrontOverhangProperty); }
            set { SetProperty<Single>(FrontOverhangProperty, value); }
        }
        #endregion

        #region FingerSpaceAbove
        public static readonly ModelPropertyInfo<Single> FingerSpaceAboveProperty =
            RegisterModelProperty<Single>(c => c.FingerSpaceAbove, Message.Product_FingerSpaceAbove, ModelPropertyDisplayType.Length);
        public Single FingerSpaceAbove
        {
            get { return GetProperty<Single>(FingerSpaceAboveProperty); }
            set { SetProperty<Single>(FingerSpaceAboveProperty, value); }
        }
        #endregion

        #region FingerSpaceToTheSide
        public static readonly ModelPropertyInfo<Single> FingerSpaceToTheSideProperty =
            RegisterModelProperty<Single>(c => c.FingerSpaceToTheSide, Message.Product_FingerSpaceToTheSide, ModelPropertyDisplayType.Length);
        public Single FingerSpaceToTheSide
        {
            get { return GetProperty<Single>(FingerSpaceToTheSideProperty); }
            set { SetProperty<Single>(FingerSpaceToTheSideProperty, value); }
        }
        #endregion

        #region StatusType
        public static readonly ModelPropertyInfo<PlanogramProductStatusType> StatusTypeProperty =
            RegisterModelProperty<PlanogramProductStatusType>(c => c.StatusType, Message.Product_StatusType,
            null, PlanogramProductStatusType.Active);
        public PlanogramProductStatusType StatusType
        {
            get { return GetProperty<PlanogramProductStatusType>(StatusTypeProperty); }
            set { SetProperty<PlanogramProductStatusType>(StatusTypeProperty, value); }
        }
        #endregion

        #region OrientationType
        public static readonly ModelPropertyInfo<PlanogramProductOrientationType> OrientationTypeProperty =
            RegisterModelProperty<PlanogramProductOrientationType>(c => c.OrientationType, Message.Product_OrientationType,
            null, PlanogramProductOrientationType.Front0);
        public PlanogramProductOrientationType OrientationType
        {
            get { return GetProperty<PlanogramProductOrientationType>(OrientationTypeProperty); }
            set { SetProperty<PlanogramProductOrientationType>(OrientationTypeProperty, value); }
        }
        #endregion

        #region MerchandisingStyle
        public static readonly ModelPropertyInfo<PlanogramProductMerchandisingStyle> MerchandisingStyleProperty =
            RegisterModelProperty<PlanogramProductMerchandisingStyle>(c => c.MerchandisingStyle, Message.Product_MerchandisingStyle,
            null, PlanogramProductMerchandisingStyle.Unit);
        public PlanogramProductMerchandisingStyle MerchandisingStyle
        {
            get { return GetProperty<PlanogramProductMerchandisingStyle>(MerchandisingStyleProperty); }
            set { SetProperty<PlanogramProductMerchandisingStyle>(MerchandisingStyleProperty, value); }
        }
        #endregion

        #region IsFrontOnly
        public static readonly ModelPropertyInfo<Boolean> IsFrontOnlyProperty =
            RegisterModelProperty<Boolean>(c => c.IsFrontOnly, Message.Product_IsFrontOnly);
        public Boolean IsFrontOnly
        {
            get { return GetProperty<Boolean>(IsFrontOnlyProperty); }
            set { SetProperty<Boolean>(IsFrontOnlyProperty, value); }
        }
        #endregion

        #region IsPlaceHolderProduct
        public static readonly ModelPropertyInfo<Boolean> IsPlaceHolderProductProperty =
            RegisterModelProperty<Boolean>(c => c.IsPlaceHolderProduct, Message.Product_IsPlaceholderProduct);
        public Boolean IsPlaceHolderProduct
        {
            get { return GetProperty<Boolean>(IsPlaceHolderProductProperty); }
            set { SetProperty<Boolean>(IsPlaceHolderProductProperty, value); }
        }
        #endregion

        #region IsActive
        public static readonly ModelPropertyInfo<Boolean> IsActiveProperty =
            RegisterModelProperty<Boolean>(c => c.IsActive, Message.Product_IsActive);
        public Boolean IsActive
        {
            get { return GetProperty<Boolean>(IsActiveProperty); }
            set { SetProperty<Boolean>(IsActiveProperty, value); }
        }
        #endregion

        #region CanBreakTrayUp
        public static readonly ModelPropertyInfo<Boolean> CanBreakTrayUpProperty =
            RegisterModelProperty<Boolean>(c => c.CanBreakTrayUp, Message.Product_CanBreakTrayUp);
        public Boolean CanBreakTrayUp
        {
            get { return GetProperty<Boolean>(CanBreakTrayUpProperty); }
            set { SetProperty<Boolean>(CanBreakTrayUpProperty, value); }
        }
        #endregion

        #region CanBreakTrayDown
        public static readonly ModelPropertyInfo<Boolean> CanBreakTrayDownProperty =
            RegisterModelProperty<Boolean>(c => c.CanBreakTrayDown, Message.Product_CanBreakTrayDown);
        public Boolean CanBreakTrayDown
        {
            get { return GetProperty<Boolean>(CanBreakTrayDownProperty); }
            set { SetProperty<Boolean>(CanBreakTrayDownProperty, value); }
        }
        #endregion

        #region CanBreakTrayBack
        public static readonly ModelPropertyInfo<Boolean> CanBreakTrayBackProperty =
            RegisterModelProperty<Boolean>(c => c.CanBreakTrayBack, Message.Product_CanBreakTrayBack);
        public Boolean CanBreakTrayBack
        {
            get { return GetProperty<Boolean>(CanBreakTrayBackProperty); }
            set { SetProperty<Boolean>(CanBreakTrayBackProperty, value); }
        }
        #endregion

        #region CanBreakTrayTop
        public static readonly ModelPropertyInfo<Boolean> CanBreakTrayTopProperty =
            RegisterModelProperty<Boolean>(c => c.CanBreakTrayTop, Message.Product_CanBreakTrayTop);
        public Boolean CanBreakTrayTop
        {
            get { return GetProperty<Boolean>(CanBreakTrayTopProperty); }
            set { SetProperty<Boolean>(CanBreakTrayTopProperty, value); }
        }
        #endregion

        #region ForceMiddleCap
        public static readonly ModelPropertyInfo<Boolean> ForceMiddleCapProperty =
            RegisterModelProperty<Boolean>(c => c.ForceMiddleCap, Message.Product_ForceMiddleCap);
        public Boolean ForceMiddleCap
        {
            get { return GetProperty<Boolean>(ForceMiddleCapProperty); }
            set { SetProperty<Boolean>(ForceMiddleCapProperty, value); }
        }
        #endregion

        #region ForceBottomCap
        public static readonly ModelPropertyInfo<Boolean> ForceBottomCapProperty =
            RegisterModelProperty<Boolean>(c => c.ForceBottomCap, Message.Product_ForceBottomCap);
        public Boolean ForceBottomCap
        {
            get { return GetProperty<Boolean>(ForceBottomCapProperty); }
            set { SetProperty<Boolean>(ForceBottomCapProperty, value); }
        }
        #endregion

        #region PointOfPurchaseDescription
        public static readonly ModelPropertyInfo<String> PointOfPurchaseDescriptionProperty =
            RegisterModelProperty<String>(c => c.PointOfPurchaseDescription, Message.Product_PointOfPurchaseDescription);
        public String PointOfPurchaseDescription
        {
            get { return GetProperty<String>(PointOfPurchaseDescriptionProperty); }
            set { SetProperty<String>(PointOfPurchaseDescriptionProperty, value); }
        }
        #endregion

        #region ShortDescription
        public static readonly ModelPropertyInfo<String> ShortDescriptionProperty =
            RegisterModelProperty<String>(c => c.ShortDescription, Message.Product_ShortDescription);
        public String ShortDescription
        {
            get { return GetProperty<String>(ShortDescriptionProperty); }
            set { SetProperty<String>(ShortDescriptionProperty, value); }
        }
        #endregion

        #region Subcategory
        public static readonly ModelPropertyInfo<String> SubcategoryProperty =
            RegisterModelProperty<String>(c => c.Subcategory, Message.Product_SubCategory);
        public String Subcategory
        {
            get { return GetProperty<String>(SubcategoryProperty); }
            set { SetProperty<String>(SubcategoryProperty, value); }
        }
        #endregion

        #region CustomerStatus
        public static readonly ModelPropertyInfo<String> CustomerStatusProperty =
            RegisterModelProperty<String>(c => c.CustomerStatus, Message.Product_CustomerStatus);
        public String CustomerStatus
        {
            get { return GetProperty<String>(CustomerStatusProperty); }
            set { SetProperty<String>(CustomerStatusProperty, value); }
        }
        #endregion

        #region Colour
        public static readonly ModelPropertyInfo<String> ColourProperty =
           RegisterModelProperty<String>(c => c.Colour, Message.Product_Color);
        public String Colour
        {
            get { return GetProperty<String>(ColourProperty); }
            set { SetProperty<String>(ColourProperty, value); }
        }
        #endregion

        #region Flavour
        public static readonly ModelPropertyInfo<String> FlavourProperty =
           RegisterModelProperty<String>(c => c.Flavour, Message.Product_Flavor);
        public String Flavour
        {
            get { return GetProperty<String>(FlavourProperty); }
            set { SetProperty<String>(FlavourProperty, value); }
        }
        #endregion

        #region PackagingShape
        public static readonly ModelPropertyInfo<String> PackagingShapeProperty =
           RegisterModelProperty<String>(c => c.PackagingShape, Message.Product_PackagingShape);
        public String PackagingShape
        {
            get { return GetProperty<String>(PackagingShapeProperty); }
            set { SetProperty<String>(PackagingShapeProperty, value); }
        }
        #endregion

        #region PackingType
        public static readonly ModelPropertyInfo<String> PackagingTypeProperty =
          RegisterModelProperty<String>(c => c.PackagingType, Message.Product_PackagingType);
        public String PackagingType
        {
            get { return GetProperty<String>(PackagingTypeProperty); }
            set { SetProperty<String>(PackagingTypeProperty, value); }
        }
        #endregion

        #region CountryOfOrigin
        public static readonly ModelPropertyInfo<String> CountryOfOriginProperty =
          RegisterModelProperty<String>(c => c.CountryOfOrigin, Message.Product_CountryOfOrigin);
        public String CountryOfOrigin
        {
            get { return GetProperty<String>(CountryOfOriginProperty); }
            set { SetProperty<String>(CountryOfOriginProperty, value); }
        }
        #endregion

        #region CountryOfProcessing
        public static readonly ModelPropertyInfo<String> CountryOfProcessingProperty =
          RegisterModelProperty<String>(c => c.CountryOfProcessing, Message.Product_CountryOfProcessing);
        public String CountryOfProcessing
        {
            get { return GetProperty<String>(CountryOfProcessingProperty); }
            set { SetProperty<String>(CountryOfProcessingProperty, value); }
        }
        #endregion

        #region ShelfLife
        public static readonly ModelPropertyInfo<Int16> ShelfLifeProperty =
          RegisterModelProperty<Int16>(c => c.ShelfLife, Message.Product_ShelfLife);
        public Int16 ShelfLife
        {
            get { return GetProperty<Int16>(ShelfLifeProperty); }
            set { SetProperty<Int16>(ShelfLifeProperty, value); }
        }
        #endregion

        #region DeliveryFrequency
        public static readonly ModelPropertyInfo<Single?> DeliveryFrequencyDaysProperty =
          RegisterModelProperty<Single?>(c => c.DeliveryFrequencyDays, Message.Product_DeliveryFrequencyDays);
        public Single? DeliveryFrequencyDays
        {
            get { return GetProperty<Single?>(DeliveryFrequencyDaysProperty); }
            set { SetProperty<Single?>(DeliveryFrequencyDaysProperty, value); }
        }
        #endregion

        #region DeliveryMethod
        public static readonly ModelPropertyInfo<String> DeliveryMethodProperty =
            RegisterModelProperty<String>(c => c.DeliveryMethod, Message.Product_DeliveryMethod);
        public String DeliveryMethod
        {
            get { return GetProperty<String>(DeliveryMethodProperty); }
            set { SetProperty<String>(DeliveryMethodProperty, value); }
        }
        #endregion

        #region Brand
        public static readonly ModelPropertyInfo<String> BrandProperty =
            RegisterModelProperty<String>(c => c.Brand, Message.Product_Brand);
        public String Brand
        {
            get { return GetProperty<String>(BrandProperty); }
            set { SetProperty<String>(BrandProperty, value); }
        }
        #endregion

        #region VendorCode
        public static readonly ModelPropertyInfo<String> VendorCodeProperty =
            RegisterModelProperty<String>(c => c.VendorCode, Message.Product_VendorCode);
        public String VendorCode
        {
            get { return GetProperty<String>(VendorCodeProperty); }
            set { SetProperty<String>(VendorCodeProperty, value); }
        }
        #endregion

        #region Vendor
        public static readonly ModelPropertyInfo<String> VendorProperty =
            RegisterModelProperty<String>(c => c.Vendor, Message.Product_Vendor);
        public String Vendor
        {
            get { return GetProperty<String>(VendorProperty); }
            set { SetProperty<String>(VendorProperty, value); }
        }
        #endregion

        #region ManufacturerCode
        public static readonly ModelPropertyInfo<String> ManufacturerCodeProperty =
            RegisterModelProperty<String>(c => c.ManufacturerCode, Message.Product_ManufacturerCode);
        public String ManufacturerCode
        {
            get { return GetProperty<String>(ManufacturerCodeProperty); }
            set { SetProperty<String>(ManufacturerCodeProperty, value); }
        }
        #endregion

        #region Manufacturer
        public static readonly ModelPropertyInfo<String> ManufacturerProperty =
            RegisterModelProperty<String>(c => c.Manufacturer, Message.Product_Manufacturer);
        public String Manufacturer
        {
            get { return GetProperty<String>(ManufacturerProperty); }
            set { SetProperty<String>(ManufacturerProperty, value); }
        }
        #endregion

        #region Size
        public static readonly ModelPropertyInfo<String> SizeProperty =
            RegisterModelProperty<String>(c => c.Size, Message.Product_Size);
        public String Size
        {
            get { return GetProperty<String>(SizeProperty); }
            set { SetProperty<String>(SizeProperty, value); }
        }
        #endregion

        #region UnitsOfMeasure
        public static readonly ModelPropertyInfo<String> UnitOfMeasureProperty =
            RegisterModelProperty<String>(c => c.UnitOfMeasure, Message.Product_UnitOfMeasure);
        public String UnitOfMeasure
        {
            get { return GetProperty<String>(UnitOfMeasureProperty); }
            set { SetProperty<String>(UnitOfMeasureProperty, value); }
        }
        #endregion

        #region DateIntroduced
        public static readonly ModelPropertyInfo<DateTime?> DateIntroducedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateIntroduced, Message.Product_DateIntroduced);
        public DateTime? DateIntroduced
        {
            get { return GetProperty<DateTime?>(DateIntroducedProperty); }
            set { SetProperty<DateTime?>(DateIntroducedProperty, value); }
        }
        #endregion

        #region DateDiscontinued
        public static readonly ModelPropertyInfo<DateTime?> DateDiscontinuedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDiscontinued, Message.Product_DateDiscontinued);
        public DateTime? DateDiscontinued
        {
            get { return GetProperty<DateTime?>(DateDiscontinuedProperty); }
            set { SetProperty<DateTime?>(DateDiscontinuedProperty, value); }
        }
        #endregion

        #region DateEffective
        public static readonly ModelPropertyInfo<DateTime?> DateEffectiveProperty =
            RegisterModelProperty<DateTime?>(c => c.DateEffective, Message.Product_DateEffective);
        public DateTime? DateEffective
        {
            get { return GetProperty<DateTime?>(DateEffectiveProperty); }
            set { SetProperty<DateTime?>(DateEffectiveProperty, value); }
        }
        #endregion

        #region Health
        public static readonly ModelPropertyInfo<String> HealthProperty =
            RegisterModelProperty<String>(c => c.Health, Message.Product_Health);
        public String Health
        {
            get { return GetProperty<String>(HealthProperty); }
            set { SetProperty<String>(HealthProperty, value); }
        }
        #endregion

        #region CorporateCode
        public static readonly ModelPropertyInfo<String> CorporateCodeProperty =
            RegisterModelProperty<String>(c => c.CorporateCode, Message.Product_CorporateCode);
        public String CorporateCode
        {
            get { return GetProperty<String>(CorporateCodeProperty); }
            set { SetProperty<String>(CorporateCodeProperty, value); }
        }
        #endregion

        #region Barcode
        public static readonly ModelPropertyInfo<String> BarcodeProperty =
            RegisterModelProperty<String>(c => c.Barcode, Message.Product_Barcode);
        public String Barcode
        {
            get { return GetProperty<String>(BarcodeProperty); }
            set { SetProperty<String>(BarcodeProperty, value); }
        }
        #endregion

        #region SellPrice
        public static readonly ModelPropertyInfo<Single?> SellPriceProperty =
            RegisterModelProperty<Single?>(c => c.SellPrice, Message.Product_SellPrice, ModelPropertyDisplayType.Currency);
        public Single? SellPrice
        {
            get { return GetProperty<Single?>(SellPriceProperty); }
            set { SetProperty<Single?>(SellPriceProperty, value); }
        }
        #endregion

        #region SellPackCount
        public static readonly ModelPropertyInfo<Int16?> SellPackCountProperty =
            RegisterModelProperty<Int16?>(c => c.SellPackCount, Message.Product_SellPackCount);
        public Int16? SellPackCount
        {
            get { return GetProperty<Int16?>(SellPackCountProperty); }
            set { SetProperty<Int16?>(SellPackCountProperty, value); }
        }
        #endregion

        #region SellPackDescription
        public static readonly ModelPropertyInfo<String> SellPackDescriptionProperty =
            RegisterModelProperty<String>(c => c.SellPackDescription, Message.Product_SellPackDescription);
        public String SellPackDescription
        {
            get { return GetProperty<String>(SellPackDescriptionProperty); }
            set { SetProperty<String>(SellPackDescriptionProperty, value); }
        }
        #endregion

        #region RecommendedRetailPrice
        public static readonly ModelPropertyInfo<Single?> RecommendedRetailPriceProperty =
            RegisterModelProperty<Single?>(c => c.RecommendedRetailPrice, Message.Product_RecommendedRetailPrice, ModelPropertyDisplayType.Currency);
        public Single? RecommendedRetailPrice
        {
            get { return GetProperty<Single?>(RecommendedRetailPriceProperty); }
            set { SetProperty<Single?>(RecommendedRetailPriceProperty, value); }
        }
        #endregion

        #region ManufacterRecommendedRetailPrice
        public static readonly ModelPropertyInfo<Single?> ManufacturerRecommendedRetailPriceProperty =
            RegisterModelProperty<Single?>(c => c.ManufacturerRecommendedRetailPrice, Message.Product_ManufacturerRecommendedRetailPrice, ModelPropertyDisplayType.Currency);
        public Single? ManufacturerRecommendedRetailPrice
        {
            get { return GetProperty<Single?>(ManufacturerRecommendedRetailPriceProperty); }
            set { SetProperty<Single?>(ManufacturerRecommendedRetailPriceProperty, value); }
        }
        #endregion

        #region CostPrice
        public static readonly ModelPropertyInfo<Single?> CostPriceProperty =
            RegisterModelProperty<Single?>(c => c.CostPrice, Message.Product_CostPrice, ModelPropertyDisplayType.Currency);
        public Single? CostPrice
        {
            get { return GetProperty<Single?>(CostPriceProperty); }
            set { SetProperty<Single?>(CostPriceProperty, value); }
        }
        #endregion

        #region CaseCost
        public static readonly ModelPropertyInfo<Single?> CaseCostProperty =
            RegisterModelProperty<Single?>(c => c.CaseCost, Message.Product_CaseCost, ModelPropertyDisplayType.Currency);
        public Single? CaseCost
        {
            get { return GetProperty<Single?>(CaseCostProperty); }
            set { SetProperty<Single?>(CaseCostProperty, value); }
        }
        #endregion

        #region TaxRate
        public static readonly ModelPropertyInfo<Single?> TaxRateProperty =
            RegisterModelProperty<Single?>(c => c.TaxRate, Message.Product_TaxRate);
        public Single? TaxRate
        {
            get { return GetProperty<Single?>(TaxRateProperty); }
            set { SetProperty<Single?>(TaxRateProperty, value); }
        }
        #endregion

        #region ConsumerInformation
        public static readonly ModelPropertyInfo<String> ConsumerInformationProperty =
            RegisterModelProperty<String>(c => c.ConsumerInformation, Message.Product_ConsumerInformation);
        public String ConsumerInformation
        {
            get { return GetProperty<String>(ConsumerInformationProperty); }
            set { SetProperty<String>(ConsumerInformationProperty, value); }
        }
        #endregion

        #region Texture
        public static readonly ModelPropertyInfo<String> TextureProperty =
            RegisterModelProperty<String>(c => c.Texture, Message.Product_Texture);
        public String Texture
        {
            get { return GetProperty<String>(TextureProperty); }
            set { SetProperty<String>(TextureProperty, value); }
        }
        #endregion

        #region StyleNumber
        public static readonly ModelPropertyInfo<Int16?> StyleNumberProperty =
            RegisterModelProperty<Int16?>(c => c.StyleNumber, Message.Product_StyleNumber);
        public Int16? StyleNumber
        {
            get { return GetProperty<Int16?>(StyleNumberProperty); }
            set { SetProperty<Int16?>(StyleNumberProperty, value); }
        }
        #endregion

        #region Pattern
        public static readonly ModelPropertyInfo<String> PatternProperty =
            RegisterModelProperty<String>(c => c.Pattern, Message.Product_Pattern);
        public String Pattern
        {
            get { return GetProperty<String>(PatternProperty); }
            set { SetProperty<String>(PatternProperty, value); }
        }
        #endregion

        #region Model
        public static readonly ModelPropertyInfo<String> ModelProperty =
            RegisterModelProperty<String>(c => c.Model, Message.Product_Model);
        public String Model
        {
            get { return GetProperty<String>(ModelProperty); }
            set { SetProperty<String>(ModelProperty, value); }
        }
        #endregion

        #region GarmentType
        public static readonly ModelPropertyInfo<String> GarmentTypeProperty =
            RegisterModelProperty<String>(c => c.GarmentType, Message.Product_GarmentType);
        public String GarmentType
        {
            get { return GetProperty<String>(GarmentTypeProperty); }
            set { SetProperty<String>(GarmentTypeProperty, value); }
        }
        #endregion

        #region IsPrivateLabel
        public static readonly ModelPropertyInfo<Boolean> IsPrivateLabelProperty =
            RegisterModelProperty<Boolean>(c => c.IsPrivateLabel, Message.Product_IsPrivateLabel);
        public Boolean IsPrivateLabel
        {
            get { return GetProperty<Boolean>(IsPrivateLabelProperty); }
            set { SetProperty<Boolean>(IsPrivateLabelProperty, value); }
        }
        #endregion

        #region IsNewProduct
        public static readonly ModelPropertyInfo<Boolean> IsNewProductProperty =
            RegisterModelProperty<Boolean>(c => c.IsNewProduct, Message.Product_IsNewProduct);
        public Boolean IsNewProduct
        {
            get { return GetProperty<Boolean>(IsNewProductProperty); }
            set { SetProperty<Boolean>(IsNewProductProperty, value); }
        }
        #endregion

      

        #region Shape
        public static readonly ModelPropertyInfo<String> ShapeProperty =
         RegisterModelProperty<String>(c => c.Shape, Message.Product_Shape);
        public String Shape
        {
            get { return GetProperty<String>(ShapeProperty); }
            set { SetProperty<String>(ShapeProperty, value); }
        }
        #endregion

        #region FillColour
        public static readonly ModelPropertyInfo<Int32> FillColourProperty =
            RegisterModelProperty<Int32>(c => c.FillColour, Message.Product_FillColor);
        public Int32 FillColour
        {
            get { return GetProperty<Int32>(FillColourProperty); }
            set { SetProperty<Int32>(FillColourProperty, value); }
        }
        #endregion

        #region FillPattern
        public static readonly ModelPropertyInfo<PlanogramProductFillPatternType> FillPatternTypeProperty =
            RegisterModelProperty<PlanogramProductFillPatternType>(c => c.FillPatternType, Message.Product_FillPattern);
        public PlanogramProductFillPatternType FillPatternType
        {
            get { return GetProperty<PlanogramProductFillPatternType>(FillPatternTypeProperty); }
            set { SetProperty<PlanogramProductFillPatternType>(FillPatternTypeProperty, value); }
        }
        #endregion

        #region FinancialGroupCode
        public static readonly ModelPropertyInfo<String> FinancialGroupCodeProperty =
         RegisterModelProperty<String>(c => c.FinancialGroupCode, Message.Product_FinancialGroupCode);
        public String FinancialGroupCode
        {
            get { return GetProperty<String>(FinancialGroupCodeProperty); }
            set { SetProperty<String>(FinancialGroupCodeProperty, value); }
        }
        #endregion

        #region FinancialGroupName
        public static readonly ModelPropertyInfo<String> FinancialGroupNameProperty =
         RegisterModelProperty<String>(c => c.FinancialGroupName, Message.Product_FinancialGroupName);
        public String FinancialGroupName
        {
            get { return GetProperty<String>(FinancialGroupNameProperty); }
            set { SetProperty<String>(FinancialGroupNameProperty, value); }
        }
        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion

        #region ImageList

        public static readonly ModelPropertyInfo<ProductImageList> ImageListProperty =
            RegisterModelProperty<ProductImageList>(c => c.ImageList);
        /// <summary>
        /// The attribute data for this product
        /// </summary>
        /// <remarks>If the object is not new this will be lazy loaded.</remarks>
        public ProductImageList ImageList
        {
            get
            {
#if !SILVERLIGHT

                if (ReadProperty<ProductImageList>(ImageListProperty) == null)
                {
                    //need this to stop lock dropping in nunit
                    if (_attributeDataLock == null)
                    {
                        _attributeDataLock = new object();
                    }

                    lock (_imageListLock)
                    {
                        if (ReadProperty<ProductImageList>(ImageListProperty) == null)
                        {
                            LoadProperty<ProductImageList>(ImageListProperty, ProductImageList.GetListByProductId(this.Id));
                            OnPropertyChanged(ImageListProperty);
                        }
                    }

                }
                return GetProperty<ProductImageList>(ImageListProperty);

#else

                if (ReadProperty<ProductImageList>(ImageListProperty) == null)
                {
                    this.MarkBusy();
                    DataPortal.BeginFetch<ProductImageList>((o,e)=>
                    {
                        if(e.Error != null)
                        {
                            throw e.Error;
                        }
                        LoadProperty<ProductImageList>(ImageListProperty, e.Object);
                        OnPropertyChanged(ImageListProperty);
                        MarkIdle();
                    });
                }
                return GetProperty<ProductImageList>(ImageListProperty);

#endif
            }
        }
        #endregion

        #region CustomAttributes

        /// <summary>
        /// CustomAttributes property definition
        /// </summary>
        public static readonly ModelPropertyInfo<CustomAttributeData> CustomAttributesProperty =
            RegisterModelProperty<CustomAttributeData>(c => c.CustomAttributes, RelationshipTypes.LazyLoad);

        /// <summary>
        /// Gets the child custom attributes object for this product.
        /// </summary>
        public CustomAttributeData CustomAttributes
        {
            get { return this.GetProperty<CustomAttributeData>(CustomAttributesProperty); }
        }

        /// <summary>
        /// Gets the child custom attributes object for this plan.
        /// Explicit implementation of IPlanogramProduct member
        /// </summary>
        ICustomAttributeData IPlanogramProduct.CustomAttributes
        {
            get { return CustomAttributes; }
        }

        #endregion

        #region ShapeType
        /// <summary>
        /// ShapeType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<PlanogramProductShapeType> ShapeTypeProperty =
            RegisterModelProperty<PlanogramProductShapeType>(c => c.ShapeType, Message.Product_ShapeType,
            null, PlanogramProductShapeType.Box);
        /// <summary>
        /// Specifies the shape of the product, default is box.
        /// </summary>
        /// <remarks>
        /// Not to be confused with the Shape property, which is simply a string description
        /// of the product's shape.
        /// </remarks>
        public PlanogramProductShapeType ShapeType
        {
            get { return GetProperty<PlanogramProductShapeType>(ShapeTypeProperty); }
            set { SetProperty<PlanogramProductShapeType>(ShapeTypeProperty, value); }
        }
        #endregion

        #region ParentType
        /// <summary>
        /// Returns the custom attribute data parent type
        /// </summary>
        Byte ICustomAttributeDataParent.ParentType
        {
            get { return (Byte)CustomAttributeDataParentType.Product; }
        }
        #endregion

        #endregion

        #region Authorization Rules

        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            //V8-28274
            // no authentication rules required as this object
            // is accessed by the editor when not connected to
            // a repository
        }

        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));

            BusinessRules.AddRule(new Required(GtinProperty));
            BusinessRules.AddRule(new MaxLength(GtinProperty, 14));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 100));

            BusinessRules.AddRule(new MaxLength(ShapeProperty, 20));
            BusinessRules.AddRule(new MaxLength(FillColourProperty, 20));
            BusinessRules.AddRule(new MaxLength(FillPatternTypeProperty, 20));
            BusinessRules.AddRule(new MaxLength(PointOfPurchaseDescriptionProperty, 50));
            BusinessRules.AddRule(new MaxLength(ShortDescriptionProperty, 50));
            BusinessRules.AddRule(new MaxLength(SubcategoryProperty, 50));
            BusinessRules.AddRule(new MaxLength(CustomerStatusProperty, 50));
            BusinessRules.AddRule(new MaxLength(ColourProperty, 50));
            BusinessRules.AddRule(new MaxLength(FlavourProperty, 100));
            BusinessRules.AddRule(new MaxLength(PackagingShapeProperty, 50));
            BusinessRules.AddRule(new MaxLength(PackagingTypeProperty, 50));
            BusinessRules.AddRule(new MaxLength(CountryOfOriginProperty, 50));
            BusinessRules.AddRule(new MaxLength(CountryOfProcessingProperty, 50));
            BusinessRules.AddRule(new MaxLength(DeliveryMethodProperty, 20));
            BusinessRules.AddRule(new MaxLength(BrandProperty, 100));
            BusinessRules.AddRule(new MaxLength(VendorCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(VendorProperty, 100));
            BusinessRules.AddRule(new MaxLength(ManufacturerCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManufacturerProperty, 100));
            BusinessRules.AddRule(new MaxLength(SizeProperty, 50));
            BusinessRules.AddRule(new MaxLength(UnitOfMeasureProperty, 50));
            BusinessRules.AddRule(new MaxLength(HealthProperty, 50));
            BusinessRules.AddRule(new MaxLength(CorporateCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(BarcodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(SellPackDescriptionProperty, 100));
            BusinessRules.AddRule(new MaxLength(ConsumerInformationProperty, 50));
            BusinessRules.AddRule(new MaxLength(TextureProperty, 100));
            BusinessRules.AddRule(new MaxLength(PatternProperty, 100));
            BusinessRules.AddRule(new MaxLength(ModelProperty, 100));
            BusinessRules.AddRule(new MaxLength(GarmentTypeProperty, 100));
            BusinessRules.AddRule(new MaxLength(FinancialGroupCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(FinancialGroupNameProperty, 100));

            BusinessRules.AddRule(new NullableMinValue<Single>(SellPriceProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(SellPriceProperty, 999999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(CostPriceProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(CostPriceProperty, 999999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(RecommendedRetailPriceProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(RecommendedRetailPriceProperty, 999999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(CaseCostProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(CaseCostProperty, 999999.99f));
            BusinessRules.AddRule(new NullableMinValue<Single>(TaxRateProperty, 0.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(TaxRateProperty, 999.99f));
            BusinessRules.AddRule(new NullableMinValue<Int16>(ShelfLifeProperty, 0));

            BusinessRules.AddRule(new NullableMaxValue<Int16>(SellPackCountProperty, 32767));
            BusinessRules.AddRule(new NullableMaxValue<Int16>(StyleNumberProperty, 32767));

            // V8-28455
            // Ensure that the Nesting size does not exceed the product size.
            BusinessRules.AddRule(new Dependency(HeightProperty, NestingHeightProperty));
            BusinessRules.AddRule(new Dependency(WidthProperty, NestingWidthProperty));
            BusinessRules.AddRule(new Dependency(DepthProperty, NestingDepthProperty));
            BusinessRules.AddRule(new MaxValueProperty<Single, Single>(NestingHeightProperty, HeightProperty));
            BusinessRules.AddRule(new MaxValueProperty<Single, Single>(NestingWidthProperty, WidthProperty));
            BusinessRules.AddRule(new MaxValueProperty<Single, Single>(NestingDepthProperty, DepthProperty));
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int32> IdProperty =
                RegisterProperty<Int32>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int32 Id
            {
                get { return this.ReadProperty<Int32>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int32 id)
            {
                this.LoadProperty<Int32>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new Product
        /// </summary>
        public static Product NewProduct()
        {
            Product item = new Product();
            item.Create();
            return item;
        }

        /// <summary>
        /// Creates a new Product with the given Entity Id.
        /// </summary>
        public static Product NewProduct(Int32 entityId)
        {
            Product item = new Product();
            item.Create(entityId);
            return item;
        }

        /// <summary>
        /// Creates a new product from the given source
        /// </summary>
        public static Product NewProduct(IPlanogramProduct product, Boolean markAsChild)
        {
            Product item = new Product();
            item.Create(product, markAsChild);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new Object of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.NewCustomAttributeData());
            this.LoadProperty<Single>(SqueezeHeightProperty, 1);
            this.LoadProperty<Single>(SqueezeWidthProperty, 1);
            this.LoadProperty<Single>(SqueezeDepthProperty, 1);
            this.LoadProperty<Int32>(FillColourProperty, /*white*/-1);
            base.Create();
        }

        /// <summary>
        /// Called when creating a new Object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<Int16>(CasePackUnitsProperty, 1);
            this.LoadProperty<Byte>(CaseHighProperty, 1);
            this.LoadProperty<Byte>(CaseWideProperty, 1);
            this.LoadProperty<Byte>(CaseDeepProperty, 1);
            this.LoadProperty<Single>(SqueezeHeightProperty, 1);
            this.LoadProperty<Single>(SqueezeWidthProperty, 1);
            this.LoadProperty<Single>(SqueezeDepthProperty, 1);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            this.LoadProperty<Boolean>(IsNewProductProperty, true);
            this.LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.NewCustomAttributeData());
            this.LoadProperty<Int32>(FillColourProperty, /*white*/-1);
            base.Create();
        }

        /// <summary>
        /// Called when creating an instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(IPlanogramProduct product, Boolean markAsChild)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<CustomAttributeData>(CustomAttributesProperty, CustomAttributeData.NewCustomAttributeData());
            this.LoadProperty<Single>(SqueezeHeightProperty, 1);
            this.LoadProperty<Single>(SqueezeWidthProperty, 1);
            this.LoadProperty<Single>(SqueezeDepthProperty, 1);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);

            Mapper.Map<IPlanogramProduct, Product>(product, this);

            if (markAsChild) this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Searches the Product's ImageList for an image that matches the given types and returns
        /// the image data if found.
        /// </summary>
        /// <param name="imageType">The type of image to find.</param>
        /// <param name="facingType">The facing type to find.</param>
        /// <returns>A byte array containing the image data. If no image is found, null is returned.</returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when the given types do not match an equivilant Product enum.
        /// </exception>
        public Byte[] GetImageData(PlanogramImageType imageType, PlanogramImageFacingType facingType)
        {
            // Try to convert the given enums to their Product equivilants.
            ProductImageType productImageType = EnumHelper.Parse<ProductImageType>(imageType.ToString(), ProductImageType.None);
            ProductImageFacing productImageFacingType = EnumHelper.Parse<ProductImageFacing>(facingType.ToString(), ProductImageFacing.None);

            // Find matching image.
            var matchingImage = ImageList.FirstOrDefault(img =>
                img.FacingType == productImageFacingType && img.ImageType == productImageType);

            if (matchingImage == null) return null;

            // Return image data.
            try
            {
                return matchingImage.ProductImageData.OriginalFile.FileBlob.Data;
            }
            catch (NullReferenceException)
            {
                return null;
            }
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(Product);
            String typeFriendly = Product.FriendlyName;

            String detailsGroup = Message.Product_PropertyGroup_Details;
            String attributesGroup = Message.Product_PropertyGroup_Attributes;
            String sizeGroup = Message.Product_PropertyGroup_Size;
            String placementGroup = Message.Product_PropertyGroup_Placement;
            String customGroup = Message.Product_PropertyGroup_Custom;

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.GtinProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.NameProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.StatusTypeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.SellPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.RecommendedRetailPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ManufacturerRecommendedRetailPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CostPriceProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CaseCostProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TaxRateProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.IsPlaceHolderProductProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.IsActiveProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ForceMiddleCapProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ForceBottomCapProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ShapeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.FillColourProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.FillPatternTypeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.SizeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DateIntroducedProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DateDiscontinuedProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DateEffectiveProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CorporateCodeProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.IsPrivateLabelProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.IsNewProductProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ShortDescriptionProperty, detailsGroup);

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.OrientationTypeProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.IsFrontOnlyProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.NumberOfPegHolesProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegXProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegX2Property, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegX3Property, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegYProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegY2Property, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegY3Property, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegProngOffsetXProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegProngOffsetYProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PegDepthProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CaseHighProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CaseWideProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CaseDeepProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayPackUnitsProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.MaxStackProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.MaxTopCapProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.MaxRightCapProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.MinDeepProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.MaxDeepProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.FrontOverhangProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.FingerSpaceAboveProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.FingerSpaceToTheSideProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.MerchandisingStyleProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CanBreakTrayUpProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CanBreakTrayDownProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CanBreakTrayBackProperty, placementGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CanBreakTrayTopProperty, placementGroup);

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CasePackUnitsProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.HeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.WidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DisplayHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DisplayWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DisplayDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.AlternateHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.AlternateWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.AlternateDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PointOfPurchaseHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PointOfPurchaseWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PointOfPurchaseDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.SqueezeHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.SqueezeWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.SqueezeDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.NestingHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.NestingWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.NestingDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CaseHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CaseWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CaseDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayThickHeightProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayThickWidthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayThickDepthProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayHighProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayWideProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TrayDeepProperty, sizeGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PointOfPurchaseDescriptionProperty, sizeGroup);

            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.SubcategoryProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CustomerStatusProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.FlavourProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PackagingShapeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PackagingTypeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CountryOfOriginProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.CountryOfProcessingProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ShelfLifeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DeliveryFrequencyDaysProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.DeliveryMethodProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.BrandProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.VendorCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.VendorProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ManufacturerCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ManufacturerProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ColourProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.UnitOfMeasureProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.HealthProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.BarcodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.SellPackCountProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.SellPackDescriptionProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ConsumerInformationProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.TextureProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.StyleNumberProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.PatternProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.ModelProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.GarmentTypeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.FinancialGroupCodeProperty, attributesGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Product.FinancialGroupNameProperty, attributesGroup);


            //return properties from custom attribute data.
            foreach (var property in CustomAttributeData.EnumerateDisplayablePropertyInfos())
            {
                var fieldInfo = ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, property, customGroup);
                fieldInfo.PropertyName = Product.CustomAttributesProperty.Name + "." + fieldInfo.PropertyName;
                yield return fieldInfo;
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Returns a string representation of this Object
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return String.Format("{0} {1}", this.Gtin, this.Name);
        }
        #endregion

        #region Mapping
        /// <summary>
        /// Creates all mappings for this type
        /// </summary>        
        private static void CreateMappings()
        {
            // interface to model
            Mapper.CreateMap<IPlanogramProduct, Product>()
                .ForMember(dest => dest.Id, map => map.Ignore())
                .ForMember(dest => dest.CustomAttributes, map => map.MapFrom(s => s.CustomAttributes))
                .ForMember(dest => dest.CustomAttributes, map => map.UseDestinationValue());
        }
        #endregion
    }
}
