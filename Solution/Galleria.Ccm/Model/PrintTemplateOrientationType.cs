﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//		Created
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Dal;
using System.IO;


namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum denoting the different page orientations
    /// </summary>
    public enum PrintTemplateOrientationType
    {
        Portrait = 0,
        Landscape = 1
    }

}
