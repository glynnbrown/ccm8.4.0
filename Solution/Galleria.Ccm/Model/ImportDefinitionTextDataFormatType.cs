﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (V8 1.0)
// V8-25395 : A.Probyn
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Enum of values available for ImportDefinition DataFormat property
    /// Represents a type of the data format for txt files
    /// </summary>
    public enum ImportDefinitionTextDataFormatType
    {
        Delimited = 1,
        Fixed = 2
    }

    public static class ImportDefinitionTextDataFormatTypeHelper
    {
        public static readonly Dictionary<ImportDefinitionTextDataFormatType, String> FriendlyNames =
            new Dictionary<ImportDefinitionTextDataFormatType, String>()
            {
                {ImportDefinitionTextDataFormatType.Delimited, Message.Enum_ImportDefinitionTextDataFormatType_Delimited},
                {ImportDefinitionTextDataFormatType.Fixed, Message.Enum_ImportDefinitionTextDataFormatType_Fixed}
            };

        public static ImportDefinitionTextDataFormatType? ImportDefinitionTextDataFormatTypeGetEnum(String description)
        {
            foreach (KeyValuePair<ImportDefinitionTextDataFormatType, String> keyPair in ImportDefinitionTextDataFormatTypeHelper.FriendlyNames)
            {
                if (keyPair.Value.Equals(description))
                {
                    return keyPair.Key;
                }
            }
            return null;
        }
    }
}
