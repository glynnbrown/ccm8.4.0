﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PrintTemplateSection
    {
        #region Constructor
        private PrintTemplateSection() { }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing item from the given dto
        /// </summary>
        internal static PrintTemplateSection Fetch(IDalContext dalContext, PrintTemplateSectionDto dto)
        {
            return DataPortal.FetchChild<PrintTemplateSection>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        private void LoadDataTransferObject(IDalContext dalContext, PrintTemplateSectionDto dto)
        {
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            this.LoadProperty<Byte>(NumberProperty, dto.Number);
            this.LoadProperty<PrintTemplateOrientationType>(OrientationProperty, (PrintTemplateOrientationType)dto.Orientation);

            this.LoadProperty<PrintTemplateComponentList>(ComponentsProperty,
                PrintTemplateComponentList.FetchByParentId(dalContext, dto.Id));
        }

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        private PrintTemplateSectionDto GetDataTransferObject(PrintTemplateSectionGroup parent)
        {
            return new PrintTemplateSectionDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                PrintTemplateSectionGroupId = parent.Id,
                Number = ReadProperty<Byte>(NumberProperty),
                Orientation = (Byte)ReadProperty<PrintTemplateOrientationType>(OrientationProperty),
            };
        }

        #endregion

        #region Fetch

        /// <summary>
        /// Called when fetching an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, PrintTemplateSectionDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Called when inserting this item
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, PrintTemplateSectionGroup parent)
        {
            PrintTemplateSectionDto dto = this.GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IPrintTemplateSectionDal dal = dalContext.GetDal<IPrintTemplateSectionDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<PrintTemplateSection>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an instance of this type
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, PrintTemplateSectionGroup parent)
        {
            if (this.IsSelfDirty)
            {
                using (IPrintTemplateSectionDal dal = dalContext.GetDal<IPrintTemplateSectionDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete

        /// <summary>
        /// Called when deleting an instance of this type
        /// </summary>
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IPrintTemplateSectionDal dal = dalContext.GetDal<IPrintTemplateSectionDal>())
            {
                dal.DeleteById(this.Id);
            }
        }
        #endregion

        #endregion

    }
}