﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: CCM800
//  Created : N.Foster
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model object representing a repository sync operation
    /// </summary>
    public partial class RepositorySync : ModelObject<RepositorySync>
    {
        #region Static Constructor
        static RepositorySync()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique repository sync id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region Source
        /// <summary>
        /// Source property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RepositorySyncTarget> SourceProperty =
            RegisterModelProperty<RepositorySyncTarget>(c => c.Source);
        /// <summary>
        /// Returns the source repository target
        /// </summary>
        public RepositorySyncTarget Source
        {
            get { return this.GetProperty<RepositorySyncTarget>(SourceProperty); }
        }
        #endregion

        #region Destination
        /// <summary>
        /// Destination property defintion
        /// </summary>
        public static readonly ModelPropertyInfo<RepositorySyncTarget> DestinationProperty =
            RegisterModelProperty<RepositorySyncTarget>(c => c.Destination);
        /// <summary>
        /// Returns the destination repository target
        /// </summary>
        public RepositorySyncTarget Destination
        {
            get { return this.GetProperty<RepositorySyncTarget>(DestinationProperty); }
        }
        #endregion

        #region Direction
        /// <summary>
        /// Direction property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RepositorySyncDirection> DirectionProperty =
            RegisterModelProperty<RepositorySyncDirection>(c => c.Direction);
        /// <summary>
        /// Returns or sets the repository sync direction
        /// </summary>
        public RepositorySyncDirection Direction
        {
            get { return this.GetProperty<RepositorySyncDirection>(DirectionProperty); }
            set { this.SetProperty<RepositorySyncDirection>(DirectionProperty, value); }
        }
        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(RepositorySync), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySync), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySync), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Temporary.ToString()));
            BusinessRules.AddRule(typeof(RepositorySync), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Temporary.ToString()));
        }
        #endregion
    }
}
