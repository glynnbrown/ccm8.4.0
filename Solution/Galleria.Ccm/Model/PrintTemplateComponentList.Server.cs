﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History: (CCM 820)
// CCM-30738 : L.Ineson
//	Created (Auto-generated)
#endregion
#endregion

using System;
using System.Collections.Generic;

using Csla;

using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;
using System.Diagnostics.CodeAnalysis;

namespace Galleria.Ccm.Model
{
    public sealed partial class PrintTemplateComponentList
    {
        #region Constructor
        private PrintTemplateComponentList() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Fetches the list by its parent id
        /// </summary>
        internal static PrintTemplateComponentList FetchByParentId(IDalContext dalContext, Int32 parentId)
        {
            return DataPortal.FetchChild<PrintTemplateComponentList>(dalContext, parentId);
        }

        #endregion

        #region Data Access

        #region Fetch

        /// <summary>
        /// Called when returing all items for a given parent id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, Int32 parentId)
        {
            RaiseListChangedEvents = false;
            using (IPrintTemplateComponentDal dal = dalContext.GetDal<IPrintTemplateComponentDal>())
            {
                IEnumerable<PrintTemplateComponentDto> dtoList = dal.FetchByPrintTemplateSectionId(parentId);
                foreach (PrintTemplateComponentDto dto in dtoList)
                {
                    this.Add(PrintTemplateComponent.Fetch(dalContext, dto));
                }
            }
            RaiseListChangedEvents = true;
        }

        #endregion

        #endregion
    }
}