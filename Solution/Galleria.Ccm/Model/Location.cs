﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM v8.0)
// CCM-25628 : A.Kuszyk
//		Created (Auto-generated)
// V8-25748 : K.Pickup
//      Changed data type of LocationId to Int16.
// V8-24264 : A.Probyn
//      Added missing ModelPropertyType
#endregion
#region Version History: (CCM v8.0.2)
// V8-29010 : D.Pleasance
//  Added EnumerateDisplayableFieldInfos
#endregion
#region Version History: (CCM 8.3.0)
// V8-32180 : N.Haywood
//  Reduced length of telCountryCode, telAreaCode, faxCountryCode and faxAreaCode to 5
#endregion
#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Rules;
using Galleria.Ccm.Resources.Language;
using System.Collections.Generic;
using Galleria.Framework.ViewModel;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a location - usually a store
    /// (root object or child of LocationList)
    /// </summary>
    [Serializable]
    public sealed partial class Location : ModelObject<Location>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int16> IdProperty =
            RegisterModelProperty<Int16>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int16 Id
        {
            get { return this.GetProperty<Int16>(IdProperty); }
            set { SetProperty<Int16>(IdProperty, value); }
        }
        #endregion

        #region RowVersion
        /// <summary>
        /// RowVersion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<RowVersion> RowVersionProperty =
            RegisterModelProperty<RowVersion>(c => c.RowVersion);
        /// <summary>
        /// The row version timestamp
        /// </summary>
        public RowVersion RowVersion
        {
            get { return GetProperty<RowVersion>(RowVersionProperty); }
        }
        #endregion

        #region EntityId

        /// <summary>
        /// EntityId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> EntityIdProperty =
            RegisterModelProperty<Int32>(c => c.EntityId);

        /// <summary>
        /// Gets/Sets the EntityId value
        /// </summary>
        public Int32 EntityId
        {
            get { return GetProperty<Int32>(EntityIdProperty); }
            set { SetProperty<Int32>(EntityIdProperty, value); }
        }

        #endregion

        #region LocationGroupId

        /// <summary>
        /// LocationGroupId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> LocationGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.LocationGroupId);

        /// <summary>
        /// Gets/Sets the LocationGroupId value
        /// </summary>
        public Int32 LocationGroupId
        {
            get { return GetProperty<Int32>(LocationGroupIdProperty); }
            set { SetProperty<Int32>(LocationGroupIdProperty, value); }
        }

        #endregion

        #region Code

        /// <summary>
        /// Code property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CodeProperty =
            RegisterModelProperty<String>(c => c.Code, Message.Location_Code);

        /// <summary>
        /// Gets/Sets the Code value
        /// </summary>
        public String Code
        {
            get { return GetProperty<String>(CodeProperty); }
            set { SetProperty<String>(CodeProperty, value); }
        }

        #endregion

        #region Name

        /// <summary>
        /// Name property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> NameProperty =
            RegisterModelProperty<String>(c => c.Name, Message.Location_Name);

        /// <summary>
        /// Gets/Sets the Name value
        /// </summary>
        public String Name
        {
            get { return GetProperty<String>(NameProperty); }
            set { SetProperty<String>(NameProperty, value); }
        }

        #endregion

        #region Region

        /// <summary>
        /// Region property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> RegionProperty =
            RegisterModelProperty<String>(c => c.Region, Message.Location_Region);

        /// <summary>
        /// Gets/Sets the Region value
        /// </summary>
        public String Region
        {
            get { return GetProperty<String>(RegionProperty); }
            set { SetProperty<String>(RegionProperty, value); }
        }

        #endregion

        #region TVRegion

        /// <summary>
        /// TVRegion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TVRegionProperty =
            RegisterModelProperty<String>(c => c.TVRegion, Message.Location_TVRegion);

        /// <summary>
        /// Gets/Sets the TVRegion value
        /// </summary>
        public String TVRegion
        {
            get { return GetProperty<String>(TVRegionProperty); }
            set { SetProperty<String>(TVRegionProperty, value); }
        }

        #endregion

        #region Location

        /// <summary>
        /// Location property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationAttributeProperty =
            RegisterModelProperty<String>(c => c.LocationAttribute, Message.Location_LocationAttribute);

        /// <summary>
        /// Gets/Sets the Location value
        /// </summary>
        public String LocationAttribute
        {
            get { return GetProperty<String>(LocationAttributeProperty); }
            set { SetProperty<String>(LocationAttributeProperty, value); }
        }

        #endregion

        #region DefaultClusterAttribute

        /// <summary>
        /// DefaultClusterAttribute property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DefaultClusterAttributeProperty =
            RegisterModelProperty<String>(c => c.DefaultClusterAttribute,Message.Location_DefaultClusterAttribute);

        /// <summary>
        /// Gets/Sets the DefaultClusterAttribute value
        /// </summary>
        public String DefaultClusterAttribute
        {
            get { return GetProperty<String>(DefaultClusterAttributeProperty); }
            set { SetProperty<String>(DefaultClusterAttributeProperty, value); }
        }

        #endregion

        #region Address1

        /// <summary>
        /// Address1 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Address1Property =
            RegisterModelProperty<String>(c => c.Address1,Message.Location_Address1);

        /// <summary>
        /// Gets/Sets the Address1 value
        /// </summary>
        public String Address1
        {
            get { return GetProperty<String>(Address1Property); }
            set { SetProperty<String>(Address1Property, value); }
        }

        #endregion

        #region Address2

        /// <summary>
        /// Address2 property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> Address2Property =
            RegisterModelProperty<String>(c => c.Address2, Message.Location_Address2);

        /// <summary>
        /// Gets/Sets the Address2 value
        /// </summary>
        public String Address2
        {
            get { return GetProperty<String>(Address2Property); }
            set { SetProperty<String>(Address2Property, value); }
        }

        #endregion

        #region City

        /// <summary>
        /// City property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CityProperty =
            RegisterModelProperty<String>(c => c.City, Message.Location_City);

        /// <summary>
        /// Gets/Sets the City value
        /// </summary>
        public String City
        {
            get { return GetProperty<String>(CityProperty); }
            set { SetProperty<String>(CityProperty, value); }
        }

        #endregion

        #region County

        /// <summary>
        /// County property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CountyProperty =
            RegisterModelProperty<String>(c => c.County, Message.Location_County);

        /// <summary>
        /// Gets/Sets the County value
        /// </summary>
        public String County
        {
            get { return GetProperty<String>(CountyProperty); }
            set { SetProperty<String>(CountyProperty, value); }
        }

        #endregion

        #region State

        /// <summary>
        /// State property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> StateProperty =
            RegisterModelProperty<String>(c => c.State, Message.Location_State);

        /// <summary>
        /// Gets/Sets the State value
        /// </summary>
        public String State
        {
            get { return GetProperty<String>(StateProperty); }
            set { SetProperty<String>(StateProperty, value); }
        }

        #endregion

        #region PostalCode

        /// <summary>
        /// PostalCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PostalCodeProperty =
            RegisterModelProperty<String>(c => c.PostalCode, Message.Location_PostalCode);

        /// <summary>
        /// Gets/Sets the PostalCode value
        /// </summary>
        public String PostalCode
        {
            get { return GetProperty<String>(PostalCodeProperty); }
            set { SetProperty<String>(PostalCodeProperty, value); }
        }

        #endregion

        #region Country

        /// <summary>
        /// Country property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CountryProperty =
            RegisterModelProperty<String>(c => c.Country, Message.Location_Country);

        /// <summary>
        /// Gets/Sets the Country value
        /// </summary>
        public String Country
        {
            get { return GetProperty<String>(CountryProperty); }
            set { SetProperty<String>(CountryProperty, value); }
        }

        #endregion

        #region Longitude

        /// <summary>
        /// Longitude property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> LongitudeProperty =
            RegisterModelProperty<Single?>(c => c.Longitude, Message.Location_Longitude);

        /// <summary>
        /// Gets/Sets the Longitude value
        /// </summary>
        public Single? Longitude
        {
            get { return GetProperty<Single?>(LongitudeProperty); }
            set { SetProperty<Single?>(LongitudeProperty, value); }
        }

        #endregion

        #region Latitude

        /// <summary>
        /// Latitude property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> LatitudeProperty =
            RegisterModelProperty<Single?>(c => c.Latitude, Message.Location_Latitude);

        /// <summary>
        /// Gets/Sets the Latitude value
        /// </summary>
        public Single? Latitude
        {
            get { return GetProperty<Single?>(LatitudeProperty); }
            set { SetProperty<Single?>(LatitudeProperty, value); }
        }

        #endregion

        #region DateOpen

        /// <summary>
        /// DateOpen property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateOpenProperty =
            RegisterModelProperty<DateTime?>(c => c.DateOpen, Message.Location_DateOpen);

        /// <summary>
        /// Gets/Sets the DateOpen value
        /// </summary>
        public DateTime? DateOpen
        {
            get { return GetProperty<DateTime?>(DateOpenProperty); }
            set { SetProperty<DateTime?>(DateOpenProperty, value); }
        }

        #endregion

        #region DateLastRefitted

        /// <summary>
        /// DateLastRefitted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateLastRefittedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateLastRefitted, Message.Location_DateLastReffited);

        /// <summary>
        /// Gets/Sets the DateLastRefitted value
        /// </summary>
        public DateTime? DateLastRefitted
        {
            get { return GetProperty<DateTime?>(DateLastRefittedProperty); }
            set { SetProperty<DateTime?>(DateLastRefittedProperty, value); }
        }

        #endregion

        #region DateClosed

        /// <summary>
        /// DateClosed property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateClosedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateClosed, Message.Location_DateClosed);

        /// <summary>
        /// Gets/Sets the DateClosed value
        /// </summary>
        public DateTime? DateClosed
        {
            get { return GetProperty<DateTime?>(DateClosedProperty); }
            set { SetProperty<DateTime?>(DateClosedProperty, value); }
        }

        #endregion

        #region CarParkSpaces

        /// <summary>
        /// CarParkSpaces property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int16?> CarParkSpacesProperty =
            RegisterModelProperty<Int16?>(c => c.CarParkSpaces, Message.Location_CarParkSpaces);

        /// <summary>
        /// Gets/Sets the CarParkSpaces value
        /// </summary>
        public Int16? CarParkSpaces
        {
            get { return GetProperty<Int16?>(CarParkSpacesProperty); }
            set { SetProperty<Int16?>(CarParkSpacesProperty, value); }
        }

        #endregion

        #region CarParkManagement

        /// <summary>
        /// CarParkManagement property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> CarParkManagementProperty =
            RegisterModelProperty<String>(c => c.CarParkManagement, Message.Location_CarParkManagement);

        /// <summary>
        /// Gets/Sets the CarParkManagement value
        /// </summary>
        public String CarParkManagement
        {
            get { return GetProperty<String>(CarParkManagementProperty); }
            set { SetProperty<String>(CarParkManagementProperty, value); }
        }

        #endregion

        #region PetrolForecourtType

        /// <summary>
        /// PetrolForecourtType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> PetrolForecourtTypeProperty =
            RegisterModelProperty<String>(c => c.PetrolForecourtType, Message.Location_PetrolForeCourt);

        /// <summary>
        /// Gets/Sets the PetrolForecourtType value
        /// </summary>
        public String PetrolForecourtType
        {
            get { return GetProperty<String>(PetrolForecourtTypeProperty); }
            set { SetProperty<String>(PetrolForecourtTypeProperty, value); }
        }

        #endregion

        #region Restaurant

        /// <summary>
        /// Restaurant property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> RestaurantProperty =
            RegisterModelProperty<String>(c => c.Restaurant, Message.Location_Restaurant);

        /// <summary>
        /// Gets/Sets the Restaurant value
        /// </summary>
        public String Restaurant
        {
            get { return GetProperty<String>(RestaurantProperty); }
            set { SetProperty<String>(RestaurantProperty, value); }
        }

        #endregion

        #region SizeGrossFloorArea

        /// <summary>
        /// SizeGrossFloorArea property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> SizeGrossFloorAreaProperty =
            RegisterModelProperty<Int32?>(c => c.SizeGrossFloorArea, Message.Location_SizeGrossFloorArea, ModelPropertyDisplayType.Area);

        /// <summary>
        /// Gets/Sets the SizeGrossFloorArea value
        /// </summary>
        public Int32? SizeGrossFloorArea
        {
            get { return GetProperty<Int32?>(SizeGrossFloorAreaProperty); }
            set { SetProperty<Int32?>(SizeGrossFloorAreaProperty, value); }
        }

        #endregion

        #region SizeNetSalesArea

        /// <summary>
        /// SizeNetSalesArea property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> SizeNetSalesAreaProperty =
            RegisterModelProperty<Int32?>(c => c.SizeNetSalesArea, Message.Location_SizeNetSalesArea, ModelPropertyDisplayType.Area);

        /// <summary>
        /// Gets/Sets the SizeNetSalesArea value
        /// </summary>
        public Int32? SizeNetSalesArea
        {
            get { return GetProperty<Int32?>(SizeNetSalesAreaProperty); }
            set { SetProperty<Int32?>(SizeNetSalesAreaProperty, value); }
        }

        #endregion

        #region SizeMezzSalesArea

        /// <summary>
        /// SizeMezzSalesArea property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32?> SizeMezzSalesAreaProperty =
            RegisterModelProperty<Int32?>(c => c.SizeMezzSalesArea, Message.Location_SizeMessSalesArea, ModelPropertyDisplayType.Area);

        /// <summary>
        /// Gets/Sets the SizeMezzSalesArea value
        /// </summary>
        public Int32? SizeMezzSalesArea
        {
            get { return GetProperty<Int32?>(SizeMezzSalesAreaProperty); }
            set { SetProperty<Int32?>(SizeMezzSalesAreaProperty, value); }
        }

        #endregion

        #region TelephoneCountryCode

        /// <summary>
        /// TelephoneCountryCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TelephoneCountryCodeProperty =
            RegisterModelProperty<String>(c => c.TelephoneCountryCode,Message.Location_TelephoneCountryCode);

        /// <summary>
        /// Gets/Sets the TelephoneCountryCode value
        /// </summary>
        public String TelephoneCountryCode
        {
            get { return GetProperty<String>(TelephoneCountryCodeProperty); }
            set { SetProperty<String>(TelephoneCountryCodeProperty, value); }
        }

        #endregion

        #region TelephoneAreaCode

        /// <summary>
        /// TelephoneAreaCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TelephoneAreaCodeProperty =
            RegisterModelProperty<String>(c => c.TelephoneAreaCode, Message.Location_TelephoneAreaCode);

        /// <summary>
        /// Gets/Sets the TelephoneAreaCode value
        /// </summary>
        public String TelephoneAreaCode
        {
            get { return GetProperty<String>(TelephoneAreaCodeProperty); }
            set { SetProperty<String>(TelephoneAreaCodeProperty, value); }
        }

        #endregion

        #region TelephoneNumber

        /// <summary>
        /// TelephoneNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> TelephoneNumberProperty =
            RegisterModelProperty<String>(c => c.TelephoneNumber, Message.Location_TelephoneNumber);

        /// <summary>
        /// Gets/Sets the TelephoneNumber value
        /// </summary>
        public String TelephoneNumber
        {
            get { return GetProperty<String>(TelephoneNumberProperty); }
            set { SetProperty<String>(TelephoneNumberProperty, value); }
        }

        #endregion

        #region FaxCountryCode

        /// <summary>
        /// FaxCountryCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FaxCountryCodeProperty =
            RegisterModelProperty<String>(c => c.FaxCountryCode, Message.Location_FaxCountryCode);

        /// <summary>
        /// Gets/Sets the FaxCountryCode value
        /// </summary>
        public String FaxCountryCode
        {
            get { return GetProperty<String>(FaxCountryCodeProperty); }
            set { SetProperty<String>(FaxCountryCodeProperty, value); }
        }

        #endregion

        #region FaxAreaCode

        /// <summary>
        /// FaxAreaCode property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FaxAreaCodeProperty =
            RegisterModelProperty<String>(c => c.FaxAreaCode, Message.Location_FaxAreaCode);

        /// <summary>
        /// Gets/Sets the FaxAreaCode value
        /// </summary>
        public String FaxAreaCode
        {
            get { return GetProperty<String>(FaxAreaCodeProperty); }
            set { SetProperty<String>(FaxAreaCodeProperty, value); }
        }

        #endregion

        #region FaxNumber

        /// <summary>
        /// FaxNumber property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> FaxNumberProperty =
            RegisterModelProperty<String>(c => c.FaxNumber, Message.Location_FaxNumber);

        /// <summary>
        /// Gets/Sets the FaxNumber value
        /// </summary>
        public String FaxNumber
        {
            get { return GetProperty<String>(FaxNumberProperty); }
            set { SetProperty<String>(FaxNumberProperty, value); }
        }

        #endregion

        #region OpeningHours

        /// <summary>
        /// OpeningHours property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> OpeningHoursProperty =
            RegisterModelProperty<String>(c => c.OpeningHours, Message.Location_OpeningHours);

        /// <summary>
        /// Gets/Sets the OpeningHours value
        /// </summary>
        public String OpeningHours
        {
            get { return GetProperty<String>(OpeningHoursProperty); }
            set { SetProperty<String>(OpeningHoursProperty, value); }
        }

        #endregion

        #region AverageOpeningHours

        /// <summary>
        /// AverageOpeningHours property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Single?> AverageOpeningHoursProperty =
            RegisterModelProperty<Single?>(c => c.AverageOpeningHours, Message.Location_AverageOpeningHours);

        /// <summary>
        /// Gets/Sets the AverageOpeningHours value
        /// </summary>
        public Single? AverageOpeningHours
        {
            get { return GetProperty<Single?>(AverageOpeningHoursProperty); }
            set { SetProperty<Single?>(AverageOpeningHoursProperty, value); }
        }

        #endregion

        #region ManagerName

        /// <summary>
        /// ManagerName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManagerNameProperty =
            RegisterModelProperty<String>(c => c.ManagerName, Message.Location_ManagerName);

        /// <summary>
        /// Gets/Sets the ManagerName value
        /// </summary>
        public String ManagerName
        {
            get { return GetProperty<String>(ManagerNameProperty); }
            set { SetProperty<String>(ManagerNameProperty, value); }
        }

        #endregion

        #region ManagerEmail

        /// <summary>
        /// ManagerEmail property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> ManagerEmailProperty =
            RegisterModelProperty<String>(c => c.ManagerEmail, Message.Location_ManagerEmail);

        /// <summary>
        /// Gets/Sets the ManagerEmail value
        /// </summary>
        public String ManagerEmail
        {
            get { return GetProperty<String>(ManagerEmailProperty); }
            set { SetProperty<String>(ManagerEmailProperty, value); }
        }

        #endregion

        #region RegionalManagerName

        /// <summary>
        /// RegionalManagerName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> RegionalManagerNameProperty =
            RegisterModelProperty<String>(c => c.RegionalManagerName, Message.Location_RegionalManagerName);

        /// <summary>
        /// Gets/Sets the RegionalManagerName value
        /// </summary>
        public String RegionalManagerName
        {
            get { return GetProperty<String>(RegionalManagerNameProperty); }
            set { SetProperty<String>(RegionalManagerNameProperty, value); }
        }

        #endregion

        #region RegionalManagerEmail

        /// <summary>
        /// RegionalManagerEmail property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> RegionalManagerEmailProperty =
            RegisterModelProperty<String>(c => c.RegionalManagerEmail, Message.Location_RegionalManagerEmail);

        /// <summary>
        /// Gets/Sets the RegionalManagerEmail value
        /// </summary>
        public String RegionalManagerEmail
        {
            get { return GetProperty<String>(RegionalManagerEmailProperty); }
            set { SetProperty<String>(RegionalManagerEmailProperty, value); }
        }

        #endregion

        #region DivisionalManagerName

        /// <summary>
        /// DivisionalManagerName property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DivisionalManagerNameProperty =
            RegisterModelProperty<String>(c => c.DivisionalManagerName, Message.Location_DivisionalManagerName);

        /// <summary>
        /// Gets/Sets the DivisionalManagerName value
        /// </summary>
        public String DivisionalManagerName
        {
            get { return GetProperty<String>(DivisionalManagerNameProperty); }
            set { SetProperty<String>(DivisionalManagerNameProperty, value); }
        }

        #endregion

        #region DivisionalManagerEmail

        /// <summary>
        /// DivisionalManagerEmail property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DivisionalManagerEmailProperty =
            RegisterModelProperty<String>(c => c.DivisionalManagerEmail, Message.Location_DivisionalManagerEmail);

        /// <summary>
        /// Gets/Sets the DivisionalManagerEmail value
        /// </summary>
        public String DivisionalManagerEmail
        {
            get { return GetProperty<String>(DivisionalManagerEmailProperty); }
            set { SetProperty<String>(DivisionalManagerEmailProperty, value); }
        }

        #endregion

        #region AdvertisingZone

        /// <summary>
        /// AdvertisingZone property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> AdvertisingZoneProperty =
            RegisterModelProperty<String>(c => c.AdvertisingZone, Message.Location_AdvertisingZone);

        /// <summary>
        /// Gets/Sets the AdvertisingZone value
        /// </summary>
        public String AdvertisingZone
        {
            get { return GetProperty<String>(AdvertisingZoneProperty); }
            set { SetProperty<String>(AdvertisingZoneProperty, value); }
        }

        #endregion

        #region DistributionCentre

        /// <summary>
        /// DistributionCentre property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> DistributionCentreProperty =
            RegisterModelProperty<String>(c => c.DistributionCentre, Message.Location_DistributionCenter);

        /// <summary>
        /// Gets/Sets the DistributionCentre value
        /// </summary>
        public String DistributionCentre
        {
            get { return GetProperty<String>(DistributionCentreProperty); }
            set { SetProperty<String>(DistributionCentreProperty, value); }
        }

        #endregion

        #region NoOfCheckouts

        /// <summary>
        /// NoOfCheckouts property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Byte?> NoOfCheckoutsProperty =
            RegisterModelProperty<Byte?>(c => c.NoOfCheckouts, Message.Location_NoOfCheckouts);

        /// <summary>
        /// Gets/Sets the NoOfCheckouts value
        /// </summary>
        public Byte? NoOfCheckouts
        {
            get { return GetProperty<Byte?>(NoOfCheckoutsProperty); }
            set { SetProperty<Byte?>(NoOfCheckoutsProperty, value); }
        }

        #endregion

        #region IsMezzFitted

        /// <summary>
        /// IsMezzFitted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsMezzFittedProperty =
            RegisterModelProperty<Boolean?>(c => c.IsMezzFitted, Message.Location_IsMezzFitted);

        /// <summary>
        /// Gets/Sets the IsMezzFitted value
        /// </summary>
        public Boolean? IsMezzFitted
        {
            get { return GetProperty<Boolean?>(IsMezzFittedProperty); }
            set { SetProperty<Boolean?>(IsMezzFittedProperty, value); }
        }

        #endregion

        #region IsFreehold

        /// <summary>
        /// IsFreehold property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsFreeholdProperty =
            RegisterModelProperty<Boolean?>(c => c.IsFreehold, Message.Location_IsFreeHold);

        /// <summary>
        /// Gets/Sets the IsFreehold value
        /// </summary>
        public Boolean? IsFreehold
        {
            get { return GetProperty<Boolean?>(IsFreeholdProperty); }
            set { SetProperty<Boolean?>(IsFreeholdProperty, value); }
        }

        #endregion

        #region Is24Hours

        /// <summary>
        /// Is24Hours property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> Is24HoursProperty =
            RegisterModelProperty<Boolean?>(c => c.Is24Hours, Message.Location_Is24Hours);

        /// <summary>
        /// Gets/Sets the Is24Hours value
        /// </summary>
        public Boolean? Is24Hours
        {
            get { return GetProperty<Boolean?>(Is24HoursProperty); }
            set { SetProperty<Boolean?>(Is24HoursProperty, value); }
        }

        #endregion

        #region IsOpenMonday

        /// <summary>
        /// IsOpenMonday property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsOpenMondayProperty =
            RegisterModelProperty<Boolean?>(c => c.IsOpenMonday, Message.Location_IsOpenMonday);

        /// <summary>
        /// Gets/Sets the IsOpenMonday value
        /// </summary>
        public Boolean? IsOpenMonday
        {
            get { return GetProperty<Boolean?>(IsOpenMondayProperty); }
            set { SetProperty<Boolean?>(IsOpenMondayProperty, value); }
        }

        #endregion

        #region IsOpenTuesday

        /// <summary>
        /// IsOpenTuesday property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsOpenTuesdayProperty =
            RegisterModelProperty<Boolean?>(c => c.IsOpenTuesday, Message.Location_IsOpenTuesday);

        /// <summary>
        /// Gets/Sets the IsOpenTuesday value
        /// </summary>
        public Boolean? IsOpenTuesday
        {
            get { return GetProperty<Boolean?>(IsOpenTuesdayProperty); }
            set { SetProperty<Boolean?>(IsOpenTuesdayProperty, value); }
        }

        #endregion

        #region IsOpenWednesday

        /// <summary>
        /// IsOpenWednesday property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsOpenWednesdayProperty =
            RegisterModelProperty<Boolean?>(c => c.IsOpenWednesday, Message.Location_IsOpenWednesday);

        /// <summary>
        /// Gets/Sets the IsOpenWednesday value
        /// </summary>
        public Boolean? IsOpenWednesday
        {
            get { return GetProperty<Boolean?>(IsOpenWednesdayProperty); }
            set { SetProperty<Boolean?>(IsOpenWednesdayProperty, value); }
        }

        #endregion

        #region IsOpenThursday

        /// <summary>
        /// IsOpenThursday property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsOpenThursdayProperty =
            RegisterModelProperty<Boolean?>(c => c.IsOpenThursday, Message.Location_IsOpenThursday);

        /// <summary>
        /// Gets/Sets the IsOpenThursday value
        /// </summary>
        public Boolean? IsOpenThursday
        {
            get { return GetProperty<Boolean?>(IsOpenThursdayProperty); }
            set { SetProperty<Boolean?>(IsOpenThursdayProperty, value); }
        }

        #endregion

        #region IsOpenFriday

        /// <summary>
        /// IsOpenFriday property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsOpenFridayProperty =
            RegisterModelProperty<Boolean?>(c => c.IsOpenFriday, Message.Location_IsOpenFriday);

        /// <summary>
        /// Gets/Sets the IsOpenFriday value
        /// </summary>
        public Boolean? IsOpenFriday
        {
            get { return GetProperty<Boolean?>(IsOpenFridayProperty); }
            set { SetProperty<Boolean?>(IsOpenFridayProperty, value); }
        }

        #endregion

        #region IsOpenSaturday

        /// <summary>
        /// IsOpenSaturday property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsOpenSaturdayProperty =
            RegisterModelProperty<Boolean?>(c => c.IsOpenSaturday, Message.Location_IsOpenSaturday);

        /// <summary>
        /// Gets/Sets the IsOpenSaturday value
        /// </summary>
        public Boolean? IsOpenSaturday
        {
            get { return GetProperty<Boolean?>(IsOpenSaturdayProperty); }
            set { SetProperty<Boolean?>(IsOpenSaturdayProperty, value); }
        }

        #endregion

        #region IsOpenSunday

        /// <summary>
        /// IsOpenSunday property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> IsOpenSundayProperty =
            RegisterModelProperty<Boolean?>(c => c.IsOpenSunday, Message.Location_IsOpenSunday);

        /// <summary>
        /// Gets/Sets the IsOpenSunday value
        /// </summary>
        public Boolean? IsOpenSunday
        {
            get { return GetProperty<Boolean?>(IsOpenSundayProperty); }
            set { SetProperty<Boolean?>(IsOpenSundayProperty, value); }
        }

        #endregion

        #region HasPetrolForecourt

        /// <summary>
        /// HasPetrolForecourt property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasPetrolForecourtProperty =
            RegisterModelProperty<Boolean?>(c => c.HasPetrolForecourt, Message.Location_HasPetrolForeCourt);

        /// <summary>
        /// Gets/Sets the HasPetrolForecourt value
        /// </summary>
        public Boolean? HasPetrolForecourt
        {
            get { return GetProperty<Boolean?>(HasPetrolForecourtProperty); }
            set { SetProperty<Boolean?>(HasPetrolForecourtProperty, value); }
        }

        #endregion

        #region HasNewsCube

        /// <summary>
        /// HasNewsCube property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasNewsCubeProperty =
            RegisterModelProperty<Boolean?>(c => c.HasNewsCube, Message.Location_HasNewsCube);

        /// <summary>
        /// Gets/Sets the HasNewsCube value
        /// </summary>
        public Boolean? HasNewsCube
        {
            get { return GetProperty<Boolean?>(HasNewsCubeProperty); }
            set { SetProperty<Boolean?>(HasNewsCubeProperty, value); }
        }

        #endregion

        #region HasAtmMachines

        /// <summary>
        /// HasAtmMachines property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasAtmMachinesProperty =
            RegisterModelProperty<Boolean?>(c => c.HasAtmMachines, Message.Location_HasATMMachine);

        /// <summary>
        /// Gets/Sets the HasAtmMachines value
        /// </summary>
        public Boolean? HasAtmMachines
        {
            get { return GetProperty<Boolean?>(HasAtmMachinesProperty); }
            set { SetProperty<Boolean?>(HasAtmMachinesProperty, value); }
        }

        #endregion

        #region HasCustomerWC

        /// <summary>
        /// HasCustomerWC property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasCustomerWCProperty =
            RegisterModelProperty<Boolean?>(c => c.HasCustomerWC, Message.Location_HasCustomerWC);

        /// <summary>
        /// Gets/Sets the HasCustomerWC value
        /// </summary>
        public Boolean? HasCustomerWC
        {
            get { return GetProperty<Boolean?>(HasCustomerWCProperty); }
            set { SetProperty<Boolean?>(HasCustomerWCProperty, value); }
        }

        #endregion

        #region HasBabyChanging

        /// <summary>
        /// HasBabyChanging property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasBabyChangingProperty =
            RegisterModelProperty<Boolean?>(c => c.HasBabyChanging,Message.Location_HasBabyChanging);

        /// <summary>
        /// Gets/Sets the HasBabyChanging value
        /// </summary>
        public Boolean? HasBabyChanging
        {
            get { return GetProperty<Boolean?>(HasBabyChangingProperty); }
            set { SetProperty<Boolean?>(HasBabyChangingProperty, value); }
        }

        #endregion

        #region HasInStoreBakery

        /// <summary>
        /// HasInStoreBakery property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasInStoreBakeryProperty =
            RegisterModelProperty<Boolean?>(c => c.HasInStoreBakery, Message.Location_HasInStoreBakery);

        /// <summary>
        /// Gets/Sets the HasInStoreBakery value
        /// </summary>
        public Boolean? HasInStoreBakery
        {
            get { return GetProperty<Boolean?>(HasInStoreBakeryProperty); }
            set { SetProperty<Boolean?>(HasInStoreBakeryProperty, value); }
        }

        #endregion

        #region HasHotFoodToGo

        /// <summary>
        /// HasHotFoodToGo property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasHotFoodToGoProperty =
            RegisterModelProperty<Boolean?>(c => c.HasHotFoodToGo, Message.Location_HasHotFoodToGo);

        /// <summary>
        /// Gets/Sets the HasHotFoodToGo value
        /// </summary>
        public Boolean? HasHotFoodToGo
        {
            get { return GetProperty<Boolean?>(HasHotFoodToGoProperty); }
            set { SetProperty<Boolean?>(HasHotFoodToGoProperty, value); }
        }

        #endregion

        #region HasRotisserie

        /// <summary>
        /// HasRotisserie property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasRotisserieProperty =
            RegisterModelProperty<Boolean?>(c => c.HasRotisserie, Message.Location_HasRotisserie);

        /// <summary>
        /// Gets/Sets the HasRotisserie value
        /// </summary>
        public Boolean? HasRotisserie
        {
            get { return GetProperty<Boolean?>(HasRotisserieProperty); }
            set { SetProperty<Boolean?>(HasRotisserieProperty, value); }
        }

        #endregion

        #region HasFishmonger

        /// <summary>
        /// HasFishmonger property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasFishmongerProperty =
            RegisterModelProperty<Boolean?>(c => c.HasFishmonger, Message.Location_HasFishMonger);

        /// <summary>
        /// Gets/Sets the HasFishmonger value
        /// </summary>
        public Boolean? HasFishmonger
        {
            get { return GetProperty<Boolean?>(HasFishmongerProperty); }
            set { SetProperty<Boolean?>(HasFishmongerProperty, value); }
        }

        #endregion

        #region HasButcher

        /// <summary>
        /// HasButcher property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasButcherProperty =
            RegisterModelProperty<Boolean?>(c => c.HasButcher, Message.Location_HasButcher);

        /// <summary>
        /// Gets/Sets the HasButcher value
        /// </summary>
        public Boolean? HasButcher
        {
            get { return GetProperty<Boolean?>(HasButcherProperty); }
            set { SetProperty<Boolean?>(HasButcherProperty, value); }
        }

        #endregion

        #region HasPizza

        /// <summary>
        /// HasPizza property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasPizzaProperty =
            RegisterModelProperty<Boolean?>(c => c.HasPizza, Message.Location_HasPizza);

        /// <summary>
        /// Gets/Sets the HasPizza value
        /// </summary>
        public Boolean? HasPizza
        {
            get { return GetProperty<Boolean?>(HasPizzaProperty); }
            set { SetProperty<Boolean?>(HasPizzaProperty, value); }
        }

        #endregion

        #region HasDeli

        /// <summary>
        /// HasDeli property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasDeliProperty =
            RegisterModelProperty<Boolean?>(c => c.HasDeli, Message.Location_HasDeli);

        /// <summary>
        /// Gets/Sets the HasDeli value
        /// </summary>
        public Boolean? HasDeli
        {
            get { return GetProperty<Boolean?>(HasDeliProperty); }
            set { SetProperty<Boolean?>(HasDeliProperty, value); }
        }

        #endregion

        #region HasSaladBar

        /// <summary>
        /// HasSaladBar property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasSaladBarProperty =
            RegisterModelProperty<Boolean?>(c => c.HasSaladBar, Message.Location_HasSaladBar);

        /// <summary>
        /// Gets/Sets the HasSaladBar value
        /// </summary>
        public Boolean? HasSaladBar
        {
            get { return GetProperty<Boolean?>(HasSaladBarProperty); }
            set { SetProperty<Boolean?>(HasSaladBarProperty, value); }
        }

        #endregion

        #region HasOrganic

        /// <summary>
        /// HasOrganic property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasOrganicProperty =
            RegisterModelProperty<Boolean?>(c => c.HasOrganic, Message.Location_HasOrganic);

        /// <summary>
        /// Gets/Sets the HasOrganic value
        /// </summary>
        public Boolean? HasOrganic
        {
            get { return GetProperty<Boolean?>(HasOrganicProperty); }
            set { SetProperty<Boolean?>(HasOrganicProperty, value); }
        }

        #endregion

        #region HasGrocery

        /// <summary>
        /// HasGrocery property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasGroceryProperty =
            RegisterModelProperty<Boolean?>(c => c.HasGrocery, Message.Location_HasGrocery);

        /// <summary>
        /// Gets/Sets the HasGrocery value
        /// </summary>
        public Boolean? HasGrocery
        {
            get { return GetProperty<Boolean?>(HasGroceryProperty); }
            set { SetProperty<Boolean?>(HasGroceryProperty, value); }
        }

        #endregion

        #region HasMobilePhones

        /// <summary>
        /// HasMobilePhones property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasMobilePhonesProperty =
            RegisterModelProperty<Boolean?>(c => c.HasMobilePhones, Message.Location_HasMobilePhones);

        /// <summary>
        /// Gets/Sets the HasMobilePhones value
        /// </summary>
        public Boolean? HasMobilePhones
        {
            get { return GetProperty<Boolean?>(HasMobilePhonesProperty); }
            set { SetProperty<Boolean?>(HasMobilePhonesProperty, value); }
        }

        #endregion

        #region HasDryCleaning

        /// <summary>
        /// HasDryCleaning property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasDryCleaningProperty =
            RegisterModelProperty<Boolean?>(c => c.HasDryCleaning, Message.Location_HasDryCleaning);

        /// <summary>
        /// Gets/Sets the HasDryCleaning value
        /// </summary>
        public Boolean? HasDryCleaning
        {
            get { return GetProperty<Boolean?>(HasDryCleaningProperty); }
            set { SetProperty<Boolean?>(HasDryCleaningProperty, value); }
        }

        #endregion

        #region HasHomeShoppingAvailable

        /// <summary>
        /// HasHomeShoppingAvailable property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasHomeShoppingAvailableProperty =
            RegisterModelProperty<Boolean?>(c => c.HasHomeShoppingAvailable, Message.Location_HasHomeShoppingAvailable);

        /// <summary>
        /// Gets/Sets the HasHomeShoppingAvailable value
        /// </summary>
        public Boolean? HasHomeShoppingAvailable
        {
            get { return GetProperty<Boolean?>(HasHomeShoppingAvailableProperty); }
            set { SetProperty<Boolean?>(HasHomeShoppingAvailableProperty, value); }
        }

        #endregion

        #region HasOptician

        /// <summary>
        /// HasOptician property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasOpticianProperty =
            RegisterModelProperty<Boolean?>(c => c.HasOptician, Message.Location_HasOptician);

        /// <summary>
        /// Gets/Sets the HasOptician value
        /// </summary>
        public Boolean? HasOptician
        {
            get { return GetProperty<Boolean?>(HasOpticianProperty); }
            set { SetProperty<Boolean?>(HasOpticianProperty, value); }
        }

        #endregion

        #region HasPharmacy

        /// <summary>
        /// HasPharmacy property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasPharmacyProperty =
            RegisterModelProperty<Boolean?>(c => c.HasPharmacy, Message.Location_HasPharmacy);

        /// <summary>
        /// Gets/Sets the HasPharmacy value
        /// </summary>
        public Boolean? HasPharmacy
        {
            get { return GetProperty<Boolean?>(HasPharmacyProperty); }
            set { SetProperty<Boolean?>(HasPharmacyProperty, value); }
        }

        #endregion

        #region HasTravel

        /// <summary>
        /// HasTravel property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasTravelProperty =
            RegisterModelProperty<Boolean?>(c => c.HasTravel, Message.Location_HasTravel);

        /// <summary>
        /// Gets/Sets the HasTravel value
        /// </summary>
        public Boolean? HasTravel
        {
            get { return GetProperty<Boolean?>(HasTravelProperty); }
            set { SetProperty<Boolean?>(HasTravelProperty, value); }
        }

        #endregion

        #region HasPhotoDepartment

        /// <summary>
        /// HasPhotoDepartment property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasPhotoDepartmentProperty =
            RegisterModelProperty<Boolean?>(c => c.HasPhotoDepartment, Message.Location_HasPhotoDepartment);

        /// <summary>
        /// Gets/Sets the HasPhotoDepartment value
        /// </summary>
        public Boolean? HasPhotoDepartment
        {
            get { return GetProperty<Boolean?>(HasPhotoDepartmentProperty); }
            set { SetProperty<Boolean?>(HasPhotoDepartmentProperty, value); }
        }

        #endregion

        #region HasCarServiceArea

        /// <summary>
        /// HasCarServiceArea property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasCarServiceAreaProperty =
            RegisterModelProperty<Boolean?>(c => c.HasCarServiceArea, Message.Location_HasCarServiceArea);

        /// <summary>
        /// Gets/Sets the HasCarServiceArea value
        /// </summary>
        public Boolean? HasCarServiceArea
        {
            get { return GetProperty<Boolean?>(HasCarServiceAreaProperty); }
            set { SetProperty<Boolean?>(HasCarServiceAreaProperty, value); }
        }

        #endregion

        #region HasGardenCentre

        /// <summary>
        /// HasGardenCentre property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasGardenCentreProperty =
            RegisterModelProperty<Boolean?>(c => c.HasGardenCentre, Message.Location_HasGardenCenter);

        /// <summary>
        /// Gets/Sets the HasGardenCentre value
        /// </summary>
        public Boolean? HasGardenCentre
        {
            get { return GetProperty<Boolean?>(HasGardenCentreProperty); }
            set { SetProperty<Boolean?>(HasGardenCentreProperty, value); }
        }

        #endregion

        #region HasClinic

        /// <summary>
        /// HasClinic property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasClinicProperty =
            RegisterModelProperty<Boolean?>(c => c.HasClinic, Message.Location_HasClinic);

        /// <summary>
        /// Gets/Sets the HasClinic value
        /// </summary>
        public Boolean? HasClinic
        {
            get { return GetProperty<Boolean?>(HasClinicProperty); }
            set { SetProperty<Boolean?>(HasClinicProperty, value); }
        }

        #endregion

        #region HasAlcohol

        /// <summary>
        /// HasAlcohol property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasAlcoholProperty =
            RegisterModelProperty<Boolean?>(c => c.HasAlcohol, Message.Location_HasAlcohol);

        /// <summary>
        /// Gets/Sets the HasAlcohol value
        /// </summary>
        public Boolean? HasAlcohol
        {
            get { return GetProperty<Boolean?>(HasAlcoholProperty); }
            set { SetProperty<Boolean?>(HasAlcoholProperty, value); }
        }

        #endregion

        #region HasFashion

        /// <summary>
        /// HasFashion property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasFashionProperty =
            RegisterModelProperty<Boolean?>(c => c.HasFashion, Message.Location_HasFashion);

        /// <summary>
        /// Gets/Sets the HasFashion value
        /// </summary>
        public Boolean? HasFashion
        {
            get { return GetProperty<Boolean?>(HasFashionProperty); }
            set { SetProperty<Boolean?>(HasFashionProperty, value); }
        }

        #endregion

        #region HasCafe

        /// <summary>
        /// HasCafe property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasCafeProperty =
            RegisterModelProperty<Boolean?>(c => c.HasCafe, Message.Location_HasCafe);

        /// <summary>
        /// Gets/Sets the HasCafe value
        /// </summary>
        public Boolean? HasCafe
        {
            get { return GetProperty<Boolean?>(HasCafeProperty); }
            set { SetProperty<Boolean?>(HasCafeProperty, value); }
        }

        #endregion

        #region HasRecycling

        /// <summary>
        /// HasRecycling property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasRecyclingProperty =
            RegisterModelProperty<Boolean?>(c => c.HasRecycling, Message.Location_HasRecycling);

        /// <summary>
        /// Gets/Sets the HasRecycling value
        /// </summary>
        public Boolean? HasRecycling
        {
            get { return GetProperty<Boolean?>(HasRecyclingProperty); }
            set { SetProperty<Boolean?>(HasRecyclingProperty, value); }
        }

        #endregion

        #region HasPhotocopier

        /// <summary>
        /// HasPhotocopier property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasPhotocopierProperty =
            RegisterModelProperty<Boolean?>(c => c.HasPhotocopier, Message.Location_HasPhotocopier);

        /// <summary>
        /// Gets/Sets the HasPhotocopier value
        /// </summary>
        public Boolean? HasPhotocopier
        {
            get { return GetProperty<Boolean?>(HasPhotocopierProperty); }
            set { SetProperty<Boolean?>(HasPhotocopierProperty, value); }
        }

        #endregion

        #region HasLottery

        /// <summary>
        /// HasLottery property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasLotteryProperty =
            RegisterModelProperty<Boolean?>(c => c.HasLottery,Message.Location_HasLottery);

        /// <summary>
        /// Gets/Sets the HasLottery value
        /// </summary>
        public Boolean? HasLottery
        {
            get { return GetProperty<Boolean?>(HasLotteryProperty); }
            set { SetProperty<Boolean?>(HasLotteryProperty, value); }
        }

        #endregion

        #region HasPostOffice

        /// <summary>
        /// HasPostOffice property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasPostOfficeProperty =
            RegisterModelProperty<Boolean?>(c => c.HasPostOffice, Message.Location_HasPostOffice);

        /// <summary>
        /// Gets/Sets the HasPostOffice value
        /// </summary>
        public Boolean? HasPostOffice
        {
            get { return GetProperty<Boolean?>(HasPostOfficeProperty); }
            set { SetProperty<Boolean?>(HasPostOfficeProperty, value); }
        }

        #endregion

        #region HasMovieRental

        /// <summary>
        /// HasMovieRental property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasMovieRentalProperty =
            RegisterModelProperty<Boolean?>(c => c.HasMovieRental, Message.Location_HasMovieRental);

        /// <summary>
        /// Gets/Sets the HasMovieRental value
        /// </summary>
        public Boolean? HasMovieRental
        {
            get { return GetProperty<Boolean?>(HasMovieRentalProperty); }
            set { SetProperty<Boolean?>(HasMovieRentalProperty, value); }
        }

        #endregion

        #region HasJewellery

        /// <summary>
        /// HasJewellery property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Boolean?> HasJewelleryProperty =
            RegisterModelProperty<Boolean?>(c => c.HasJewellery, Message.Location_HasJewelry);

        /// <summary>
        /// Gets/Sets the HasJewellery value
        /// </summary>
        public Boolean? HasJewellery
        {
            get { return GetProperty<Boolean?>(HasJewelleryProperty); }
            set { SetProperty<Boolean?>(HasJewelleryProperty, value); }
        }

        #endregion

        #region LocationType
        /// <summary>
        /// Location Type property definition
        /// </summary>
        public static readonly ModelPropertyInfo<String> LocationTypeProperty =
            RegisterModelProperty<String>(c => c.LocationType, Message.Location_LocationType);

        /// <summary>
        /// Gets/Sets the Location Type Property
        /// </summary>
        public String LocationType
        {
            get { return GetProperty<String>(LocationTypeProperty); }
            set { SetProperty<String>(LocationTypeProperty, value); }
        }

        #endregion

        #region DateCreated

        /// <summary>
        /// DateCreated property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateCreatedProperty =
            RegisterModelProperty<DateTime>(c => c.DateCreated);

        /// <summary>
        /// Gets/Sets the DateCreated value
        /// </summary>
        public DateTime DateCreated
        {
            get { return GetProperty<DateTime>(DateCreatedProperty); }
            set { SetProperty<DateTime>(DateCreatedProperty, value); }
        }

        #endregion

        #region DateLastModified

        /// <summary>
        /// DateLastModified property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime> DateLastModifiedProperty =
            RegisterModelProperty<DateTime>(c => c.DateLastModified);

        /// <summary>
        /// Gets/Sets the DateLastModified value
        /// </summary>
        public DateTime DateLastModified
        {
            get { return GetProperty<DateTime>(DateLastModifiedProperty); }
            set { SetProperty<DateTime>(DateLastModifiedProperty, value); }
        }

        #endregion

        #region DateDeleted

        /// <summary>
        /// DateDeleted property definition
        /// </summary>
        public static readonly ModelPropertyInfo<DateTime?> DateDeletedProperty =
            RegisterModelProperty<DateTime?>(c => c.DateDeleted);

        /// <summary>
        /// Gets/Sets the DateDeleted value
        /// </summary>
        public DateTime? DateDeleted
        {
            get { return GetProperty<DateTime?>(DateDeletedProperty); }
            set { SetProperty<DateTime?>(DateDeletedProperty, value); }
        }

        #endregion


        #endregion

        #region Business Rules

        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(EntityIdProperty, 1));
            BusinessRules.AddRule(new MinValue<Int32>(LocationGroupIdProperty, 1));
            BusinessRules.AddRule(new Required(CodeProperty));
            BusinessRules.AddRule(new MaxLength(CodeProperty, 50));
            BusinessRules.AddRule(new Required(NameProperty));
            BusinessRules.AddRule(new MaxLength(NameProperty, 50));
            BusinessRules.AddRule(new MaxLength(RegionProperty, 50));
            BusinessRules.AddRule(new MaxLength(TVRegionProperty, 50));
            BusinessRules.AddRule(new MaxLength(LocationAttributeProperty, 50));
            BusinessRules.AddRule(new MaxLength(DefaultClusterAttributeProperty, 50));
            BusinessRules.AddRule(new MaxLength(Address1Property, 50));
            BusinessRules.AddRule(new MaxLength(Address2Property, 50));
            BusinessRules.AddRule(new MaxLength(CityProperty, 50));
            BusinessRules.AddRule(new MaxLength(CountyProperty, 50));
            BusinessRules.AddRule(new MaxLength(StateProperty, 50));
            BusinessRules.AddRule(new MaxLength(PostalCodeProperty, 50));
            BusinessRules.AddRule(new MaxLength(CountryProperty, 50));
            BusinessRules.AddRule(new MaxLength(CarParkManagementProperty, 50));
            BusinessRules.AddRule(new MaxLength(PetrolForecourtTypeProperty, 50));
            BusinessRules.AddRule(new MaxLength(RestaurantProperty, 50));
            BusinessRules.AddRule(new MaxLength(TelephoneCountryCodeProperty, 5));
            BusinessRules.AddRule(new MaxLength(TelephoneAreaCodeProperty, 5));
            BusinessRules.AddRule(new MaxLength(TelephoneNumberProperty, 50));
            BusinessRules.AddRule(new MaxLength(FaxCountryCodeProperty, 5));
            BusinessRules.AddRule(new MaxLength(FaxAreaCodeProperty, 5));
            BusinessRules.AddRule(new MaxLength(FaxNumberProperty, 50));
            BusinessRules.AddRule(new MaxLength(OpeningHoursProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManagerNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(ManagerEmailProperty, 256));
            BusinessRules.AddRule(new MaxLength(RegionalManagerNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(RegionalManagerEmailProperty, 256));
            BusinessRules.AddRule(new MaxLength(DivisionalManagerNameProperty, 50));
            BusinessRules.AddRule(new MaxLength(DivisionalManagerEmailProperty, 256));
            BusinessRules.AddRule(new MaxLength(AdvertisingZoneProperty, 50));
            BusinessRules.AddRule(new MaxLength(DistributionCentreProperty, 50));
            BusinessRules.AddRule(new NullableMinValue<Single>(LatitudeProperty, -90.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(LatitudeProperty, 90.0f));
            BusinessRules.AddRule(new NullableMinValue<Single>(LongitudeProperty, -180.0f));
            BusinessRules.AddRule(new NullableMaxValue<Single>(LongitudeProperty, 180.0f));
            BusinessRules.AddRule(new MaxLength(LocationTypeProperty, 100));
        }

        #endregion

        #region Authorization Rules

        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(Location), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationGet.ToString()));
            BusinessRules.AddRule(typeof(Location), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationCreate.ToString()));
            BusinessRules.AddRule(typeof(Location), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationEdit.ToString()));
            BusinessRules.AddRule(typeof(Location), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationDelete.ToString()));
        }

        #endregion

        #region Criteria

        #region FetchByIdCriteria

        /// <summary>
        /// Criteria for the FetchById factory method
        /// </summary>
        [Serializable]
        public class FetchByIdCriteria : CriteriaBase<FetchByIdCriteria>
        {
            #region Properties

            #region Id
            /// <summary>
            /// Id property definition
            /// </summary>
            public static readonly PropertyInfo<Int16> IdProperty =
                RegisterProperty<Int16>(c => c.Id);
            /// <summary>
            /// Returns the id
            /// </summary>
            public Int16 Id
            {
                get { return this.ReadProperty<Int16>(IdProperty); }
            }
            #endregion

            #endregion

            #region Constructor
            /// <summary>
            /// Creates a new instance of this type
            /// </summary>
            public FetchByIdCriteria(Int16 id)
            {
                this.LoadProperty<Int16>(IdProperty, id);
            }
            #endregion
        }

        #endregion

        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new location
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static Location NewLocation(Int32 entityId)
        {
            Location item = new Location();
            item.Create(entityId);
            return item;
        }

        /// <summary>
        /// Creates a new Location
        /// </summary>
        public static Location NewLocation(Int32 locationGroupId, Int32 entityId)
        {
            Location item = new Location();
            item.Create(locationGroupId, entityId);
            return item;
        }

        #endregion

        #region Data Access

        #region Create

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 locationGroupId, Int32 entityId)
        {
            this.LoadProperty<Int32>(LocationGroupIdProperty, locationGroupId);
            this.Create(entityId);
        }

        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create(Int32 entityId)
        {
            this.LoadProperty<Int16>(IdProperty, IdentityHelper.GetNextInt16());
            this.LoadProperty<Int32>(EntityIdProperty, entityId);
            this.LoadProperty<DateTime>(DateCreatedProperty, DateTime.UtcNow);
            this.LoadProperty<DateTime>(DateLastModifiedProperty, DateTime.UtcNow);
            base.Create();
        }

        #endregion

        #endregion

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int16 oldId = this.Id;
            Int16 newId = IdentityHelper.GetNextInt16();
            this.LoadProperty<Int16>(IdProperty, newId);
            context.RegisterId<Location>(oldId, newId);
        }

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos()
        {
            Type type = typeof(Location);
            String typeFriendly = Location.FriendlyName;
                        
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.CodeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.NameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.RegionProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.TVRegionProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.LocationAttributeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.DefaultClusterAttributeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.Address1Property);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.Address2Property);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.CityProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.CountyProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.StateProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.PostalCodeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.CountryProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.LongitudeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.LatitudeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.DateOpenProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.DateLastRefittedProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.DateClosedProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.CarParkSpacesProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.CarParkManagementProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.PetrolForecourtTypeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.RestaurantProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.SizeGrossFloorAreaProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.SizeNetSalesAreaProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.SizeMezzSalesAreaProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.TelephoneCountryCodeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.TelephoneAreaCodeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.TelephoneNumberProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.FaxCountryCodeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.FaxAreaCodeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.FaxNumberProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.OpeningHoursProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.AverageOpeningHoursProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.ManagerNameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.ManagerEmailProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.RegionalManagerNameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.RegionalManagerEmailProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.DivisionalManagerNameProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.DivisionalManagerEmailProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.AdvertisingZoneProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.DistributionCentreProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.NoOfCheckoutsProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsMezzFittedProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsFreeholdProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.Is24HoursProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsOpenMondayProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsOpenTuesdayProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsOpenWednesdayProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsOpenThursdayProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsOpenFridayProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsOpenSaturdayProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.IsOpenSundayProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasPetrolForecourtProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasNewsCubeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasAtmMachinesProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasCustomerWCProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasBabyChangingProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasInStoreBakeryProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasHotFoodToGoProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasRotisserieProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasFishmongerProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasButcherProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasPizzaProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasDeliProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasSaladBarProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasOrganicProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasGroceryProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasMobilePhonesProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasDryCleaningProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasHomeShoppingAvailableProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasOpticianProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasPharmacyProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasTravelProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasPhotoDepartmentProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasCarServiceAreaProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasGardenCentreProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasClinicProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasAlcoholProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasFashionProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasCafeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasRecyclingProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasPhotocopierProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasLotteryProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasPostOfficeProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasMovieRentalProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.HasJewelleryProperty);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, Location.LocationTypeProperty);            
        }
        #endregion

        #region Overrides

        public override String ToString()
        {
            return this.Code + ' ' + this.Name;
        }

        #endregion
    }
}