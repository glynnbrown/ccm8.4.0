﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25453 : A.Kuszyk
//  Created (copied from SA).
#endregion
#endregion

using System;
using System.Collections.Generic;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a list containing ConsumerDecisionTree objects
    /// </summary>
    [Serializable]
    public sealed partial class ConsumerDecisionTreeList : ModelList<ConsumerDecisionTreeList, ConsumerDecisionTree>
    {
        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeList), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.ConsumerDecisionTreeCreate.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeList), new IsInRole(AuthorizationActions.GetObject, DomainPermission.ConsumerDecisionTreeGet.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeList), new IsInRole(AuthorizationActions.EditObject, DomainPermission.ConsumerDecisionTreeEdit.ToString()));
            BusinessRules.AddRule(typeof(ConsumerDecisionTreeList), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.ConsumerDecisionTreeDelete.ToString()));
        }
        #endregion

        #region Criteria

        /// <summary>
        /// Criteria for ConsumerDecisionTreeList.FetchByEntityIdConsumerDecisionTreeIds
        /// </summary>
        [Serializable]
        internal class FetchByEntityIdConsumerDecisionTreeIdsCriteria : CriteriaBase<FetchByEntityIdConsumerDecisionTreeIdsCriteria>
        {
            #region Properties

            public static PropertyInfo<Int32> EntityIdProperty =
                RegisterProperty<Int32>(c => c.EntityId);
            public Int32 EntityId
            {
                get { return ReadProperty<Int32>(EntityIdProperty); }
            }

            public static PropertyInfo<IEnumerable<Int32>> ConsumerDecisionTreeIdsProperty =
                RegisterProperty<IEnumerable<Int32>>(c => c.ConsumerDecisionTreeIds);
            public IEnumerable<Int32> ConsumerDecisionTreeIds
            {
                get { return ReadProperty<IEnumerable<Int32>>(ConsumerDecisionTreeIdsProperty); }
            }

            #endregion

            public FetchByEntityIdConsumerDecisionTreeIdsCriteria(Int32 entityId, IEnumerable<Int32> consumerDecisionTreeIds)
            {
                LoadProperty<Int32>(EntityIdProperty, entityId);
                LoadProperty<IEnumerable<Int32>>(ConsumerDecisionTreeIdsProperty, consumerDecisionTreeIds);
            }
        }

        #endregion
    }
}
