﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM V8)
// CCM-25395 : A.Probyn
//	Created (Auto-generated)
#endregion
#endregion

using System;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Model representing a list of ProductLibraryColumnMapping objects.
    /// </summary>
    [Serializable]
    public sealed partial class ProductLibraryColumnMappingList : ModelList<ProductLibraryColumnMappingList, ProductLibraryColumnMapping>
    {
        #region Authorization Rules
        // no authentication rules required as this object
        // is accessed by the editor when not connected to
        // a repository
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new ProductLibrary Parent
        {
            get { return (ProductLibrary)base.Parent; }
        }
        #endregion

        #region Factory Methods
        /// <summary>
        /// Creates a new instance of this type
        /// </summary>
        public static ProductLibraryColumnMappingList NewProductLibraryColumnMappingList()
        {
            ProductLibraryColumnMappingList item = new ProductLibraryColumnMappingList();
            item.Create();
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        protected override void Create()
        {
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion
    }
}