﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// GFS-25454 : J.Pickup
//  Created (Copied over from GFS).
#endregion

#endregion

using System;
using Csla;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents an Assortment File within CCM
    /// (Child of Assortment)
    /// This is a child Content object and so it has the following properties/actions:
    /// - This object does NOT have a DateDeleted property or DateDeleted field in its supporting database table
    /// - if its parent is deleted, it is not removed or marked as deleted (this allows for parent 'undeletes')
    /// - if it is deleted directly, it's associated record is removed from the database. 
    [Serializable]
    [DefaultNewMethod("NewAssortmentFile", "FileId")]
    public partial class AssortmentFile : ModelObject<AssortmentFile>
    {
        #region Static Constructor
        static AssortmentFile()
        {
        }
        #endregion

        #region Properties

        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// The AssortmentFile id
        /// </summary>
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }

        public static readonly ModelPropertyInfo<Int32> FileIdProperty =
            RegisterModelProperty<Int32>(c => c.FileId);
        /// <summary>
        /// The File Id
        /// </summary>
        public Int32 FileId
        {
            get { return GetProperty<Int32>(FileIdProperty); }
            set { SetProperty<Int32>(FileIdProperty, value); }
        }

        public static readonly ModelPropertyInfo<String> FileNameProperty =
            RegisterModelProperty<String>(c => c.FileName);
        /// <summary>
        /// The assortment file's name and path
        /// </summary>
        public String FileName
        {
            get { return GetProperty<String>(FileNameProperty); }
        }

        public static readonly ModelPropertyInfo<String> FileTypeProperty =
            RegisterModelProperty<String>(c => c.FileType);
        /// <summary>
        /// The assortment file's type (PDF,Word Doc etc)
        /// </summary>
        public String FileType
        {
            get { return GetProperty<String>(FileTypeProperty); }
        }

        public static readonly ModelPropertyInfo<Int64> SizeInBytesProperty =
            RegisterModelProperty<Int64>(c => c.SizeInBytes);
        /// <summary>
        /// The assortment file's Size In Bytes
        /// </summary>
        public Int64 SizeInBytes
        {
            get { return GetProperty<Int64>(SizeInBytesProperty); }
        }

        public static readonly ModelPropertyInfo<String> SourceFilePathProperty =
            RegisterModelProperty<String>(c => c.SourceFilePath);
        /// <summary>
        /// The assortment file's file path
        /// </summary>
        public String SourceFilePath
        {
            get { return GetProperty<String>(SourceFilePathProperty); }
        }

        public static readonly ModelPropertyInfo<String> UserNameProperty =
            RegisterModelProperty<String>(c => c.UserName);
        /// <summary>
        /// The assortment file's author
        /// </summary>
        public String UserName
        {
            get { return GetProperty<String>(UserNameProperty); }
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            BusinessRules.AddRule(new Required(FileIdProperty));
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(AssortmentFile), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.AssortmentCreate.ToString()));
            BusinessRules.AddRule(typeof(AssortmentFile), new IsInRole(AuthorizationActions.GetObject, DomainPermission.AssortmentGet.ToString()));
            BusinessRules.AddRule(typeof(AssortmentFile), new IsInRole(AuthorizationActions.EditObject, DomainPermission.AssortmentEdit.ToString()));
            BusinessRules.AddRule(typeof(AssortmentFile), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.AssortmentDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentFile NewAssortmentFile(Int32 fileId)
        {
            AssortmentFile item = new AssortmentFile();
            item.Create(fileId);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentFile NewAssortmentFile(Galleria.Ccm.Model.File file)
        {
            AssortmentFile item = new AssortmentFile();
            item.Create(file);
            return item;
        }

        /// <summary>
        /// Creates a new object
        /// </summary>
        /// <returns>A new object</returns>
        public static AssortmentFile NewAssortmentFile(FileInfo fileInfo)
        {
            AssortmentFile item = new AssortmentFile();
            item.Create(fileInfo);
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Int32 fileId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(FileIdProperty, fileId);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(Galleria.Ccm.Model.File file)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(FileIdProperty, file.Id);
            this.LoadProperty<String>(FileNameProperty, file.Name);
            this.LoadProperty<String>(FileTypeProperty, Galleria.Ccm.Model.File.GetFileType(file.Name));
            this.LoadProperty<Int64>(SizeInBytesProperty, file.SizeInBytes);
            this.LoadProperty<String>(SourceFilePathProperty, file.SourceFilePath);
            this.LoadProperty<String>(UserNameProperty, ApplicationContext.User.Identity.Name);
            this.MarkAsChild();
            base.Create();
        }

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        /// <param name="product"></param>
        private void Create(FileInfo fileInfo)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(FileIdProperty, fileInfo.Id);
            this.LoadProperty<String>(FileNameProperty, fileInfo.Name);
            this.LoadProperty<String>(FileTypeProperty, Galleria.Ccm.Model.File.GetFileType(fileInfo.Name));
            this.LoadProperty<Int64>(SizeInBytesProperty, fileInfo.SizeInBytes);
            this.LoadProperty<String>(SourceFilePathProperty, fileInfo.SourceFilePath);
            this.LoadProperty<String>(UserNameProperty, ApplicationContext.User.Identity.Name);
            this.MarkAsChild();
            base.Create();
        }

        #endregion

        #endregion

    }
}


