﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25664: A.Kuszyk
//		Created.
// V8-26822 : L.Ineson
//  Amended new factory method to take values.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// UserPlanogramGroup Model object
    /// </summary>
    [Serializable]
    public partial class UserPlanogramGroup : ModelObject<UserPlanogramGroup>
    {
        #region Properties

        #region Id
        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion

        #region UserId
        /// <summary>
        /// UserId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> UserIdProperty =
            RegisterModelProperty<Int32>(c => c.UserId);
        /// <summary>
        /// Gets/Sets the UserId value
        /// </summary>
        public Int32 UserId
        {
            get { return GetProperty<Int32>(UserIdProperty); }
            set { SetProperty<Int32>(UserIdProperty, value); }
        }
        #endregion

        #region PlanogramGroupId
        /// <summary>
        /// PlanogramGroupId property definition
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> PlanogramGroupIdProperty =
            RegisterModelProperty<Int32>(c => c.PlanogramGroupId);
        /// <summary>
        /// Gets/Sets the PlanogramGroupId value
        /// </summary>
        public Int32 PlanogramGroupId
        {
            get { return GetProperty<Int32>(PlanogramGroupIdProperty); }
            set { SetProperty<Int32>(PlanogramGroupIdProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();

            BusinessRules.AddRule(new MinValue<Int32>(UserIdProperty, 0));
            BusinessRules.AddRule(new MinValue<Int32>(PlanogramGroupIdProperty, 0));
            BusinessRules.AddRule(new Required(UserIdProperty));
            BusinessRules.AddRule(new Required(PlanogramGroupIdProperty));
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(UserPlanogramGroup), new IsInRole(AuthorizationActions.GetObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(UserPlanogramGroup), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(UserPlanogramGroup), new IsInRole(AuthorizationActions.EditObject, DomainPermission.Authenticated.ToString()));
            BusinessRules.AddRule(typeof(UserPlanogramGroup), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.Authenticated.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static UserPlanogramGroup NewUserPlanogramGroup(Int32 userId, Int32 planogramGroupId)
        {
            UserPlanogramGroup item = new UserPlanogramGroup();
            item.Create(userId, planogramGroupId);
            return item;
        }

        #endregion

        #region DataAccess

        #region Create

        /// <summary>
        /// Called when creating a new item of this type.
        /// </summary>
        private new void Create(Int32 userId, Int32 planogramGroupId)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Int32>(UserIdProperty, userId);
            this.LoadProperty<Int32>(PlanogramGroupIdProperty, planogramGroupId);
            this.MarkAsChild();
            base.Create();
        }

        #endregion


        #endregion

        #region Overrides
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<UserPlanogramGroup>(oldId, newId);
        }
        #endregion
    }
}
