﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-27132 : A.Kuszyk
//  Added Product Gtin property.
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class ConsumerDecisionTreeNodeProduct
    {
        #region Constructors
        private ConsumerDecisionTreeNodeProduct() { } // force use of factory methods
        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns an existing object
        /// </summary>
        /// <returns>An existing unit</returns>
        internal static ConsumerDecisionTreeNodeProduct FetchConsumerDecisionTreeNodeProduct
            (IDalContext dalContext, ConsumerDecisionTreeNodeProductDto dto)
        {
            return DataPortal.FetchChild<ConsumerDecisionTreeNodeProduct>(dalContext, dto);
        }

        #endregion

        #region Data Access

        #region Data Transfer Object

        /// <summary>
        /// Creates a dto from this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ConsumerDecisionTreeNodeProductDto GetDataTransferObject(ConsumerDecisionTreeNode parent)
        {
            return new ConsumerDecisionTreeNodeProductDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                ProductId = ReadProperty<Int32>(ProductIdProperty),
                ProductGtin = ReadProperty<String>(ProductGtinProperty),
                ConsumerDecisionTreeNodeId = parent.Id
            };
        }


        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, ConsumerDecisionTreeNodeProductDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(ProductIdProperty, dto.ProductId);
            LoadProperty<String>(ProductGtinProperty, dto.ProductGtin);
        }
        #endregion

        #region Fetch

        /// <summary>
        /// Called when returning an existing item
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, ConsumerDecisionTreeNodeProductDto dto)
        {
            LoadDataTransferObject(dalContext, dto);
        }

        #endregion

        #region Insert
        /// <summary>
        /// Inserts a new object into the solution
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, ConsumerDecisionTreeNode parent)
        {
            ConsumerDecisionTreeNodeProductDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (IConsumerDecisionTreeNodeProductDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<ConsumerDecisionTreeNodeProduct>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an object of this type
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent model</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, ConsumerDecisionTreeNode parent)
        {
            if (this.IsSelfDirty)
            {
                using (IConsumerDecisionTreeNodeProductDal dal = dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_DeleteSelf(IDalContext dalContext)
        {
            using (IConsumerDecisionTreeNodeProductDal dal =
                dalContext.GetDal<IConsumerDecisionTreeNodeProductDal>())
            {
                dal.DeleteById(this.Id);
            }

        }
        #endregion

        #endregion
    }
}
