﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History : (CCM CCM800)

// CCM800-26953 : I.George
//   Initial Version
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Helpers;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.Enums;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// PerformanceSelectionMetric Model
    /// </summary>
    [Serializable]
    public sealed partial class PerformanceSelectionMetric : ModelObject<PerformanceSelectionMetric>
    {
        #region Static Constructor
        static PerformanceSelectionMetric()
        {
            MappingHelper.InitializeMappings();
        }
        #endregion

        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new PerformanceSelection Parent
        {
            get 
            {
                PerformanceSelectionMetricList parentList = base.Parent as PerformanceSelectionMetricList;
                if (parentList != null)
                {
                    return parentList.Parent as PerformanceSelection;
                }
                return null;
            }
        }
        #endregion

        #region Properties

        #region Id

        /// <summary>
        /// Id property definition
        /// </summary>
        private static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        /// <summary>
        /// Returns the unique id
        /// </summary>
        public Int32 Id
        {
            get { return this.GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }
        #endregion
       
        #region GFSPerformanceSourceMetricId
            /// <summary>
            /// GFSPerformanceSelectionId Property definition
            /// </summary>
            public static readonly ModelPropertyInfo<Int32> GFSPerformanceSourceMetricIdProperty =
            RegisterModelProperty<Int32>(c => c.GFSPerformanceSourceMetricId);

        /// <summary>
            /// Gets/Sts GFSPerformanceSelectionId value
        /// </summary>
        public Int32 GFSPerformanceSourceMetricId
        {
            get { return GetProperty<Int32>(GFSPerformanceSourceMetricIdProperty); }
            set { SetProperty<Int32>(GFSPerformanceSourceMetricIdProperty, value); }
        }
        #endregion

        #region AggregationType
        /// <summary>
        /// AggregationType property definition
        /// </summary>
        public static readonly ModelPropertyInfo<AggregationType> AggregationTypeProperty =
            RegisterModelProperty<AggregationType>(c => c.AggregationType);
        /// <summary>
        /// 
        /// </summary>
        public AggregationType AggregationType
        {
            get { return GetProperty<AggregationType>(AggregationTypeProperty); }
            set { SetProperty<AggregationType>(AggregationTypeProperty, value); }
        }

        #endregion

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
        }
        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(PerformanceSelectionMetric), new IsInRole(AuthorizationActions.GetObject, DomainPermission.PerformanceSelectionGet.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionMetric), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.PerformanceSelectionCreate.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionMetric), new IsInRole(AuthorizationActions.EditObject, DomainPermission.PerformanceSelectionEdit.ToString()));
            BusinessRules.AddRule(typeof(PerformanceSelectionMetric), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.PerformanceSelectionDelete.ToString()));
        }
        #endregion

        #region Factory Methods

        /// <summary>
        /// Creates a new object of this type
        /// </summary>
        /// <returns>A new object</returns>
        public static PerformanceSelectionMetric NewPerformanceSelectionMetric()
        {
            PerformanceSelectionMetric item = new PerformanceSelectionMetric();
            item.Create();
            return item;
        }
        #endregion

        #region DataAccess

        #region Create 

        /// <summary>
        /// Called when creating a new instance of this type
        /// </summary>
        private new void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<AggregationType>(AggregationTypeProperty, AggregationType.Sum);
            this.MarkAsChild();
            base.Create();
        }

        #region Methods
        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            this.LoadProperty<Int32>(IdProperty, newId);
            context.RegisterId<PerformanceSelectionMetric>(oldId, newId);
        }
        #endregion

        #endregion

        #endregion
    }
}

