﻿#region Header Information

// Copyright © Galleria RTS Ltd 2015

#region Version History: CCM830

// V8-31945 : A.Silva
//  Created.

#endregion

#endregion

using System;
using System.Collections.Generic;
using Csla;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    public partial class PlanogramComparisonTemplateInfoList
    {
        #region Constructors

        private PlanogramComparisonTemplateInfoList() { } // force the use of factory methods

        #endregion

        #region Factory Methods

        /// <summary>
        /// Returns a list of all items from ids
        /// </summary>
        public static PlanogramComparisonTemplateInfoList FetchByFileNames(List<Object> ids)
        {
            return DataPortal.Fetch<PlanogramComparisonTemplateInfoList>(new FetchByIdsCriteria(PlanogramComparisonTemplateDalType.FileSystem, ids));
        }

        /// <summary>
        /// Returns a list of all items from ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public static PlanogramComparisonTemplateInfoList FetchByIds(List<Object> ids)
        {
            return DataPortal.Fetch<PlanogramComparisonTemplateInfoList>(new FetchByIdsCriteria(PlanogramComparisonTemplateDalType.Unknown, ids));
        }

        /// <summary>
        /// Returns a list of all items by entity id not including deleted. 
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static PlanogramComparisonTemplateInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramComparisonTemplateInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns a list of all items by entity id including deleted. 
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public static PlanogramComparisonTemplateInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<PlanogramComparisonTemplateInfoList>(new FetchByEntityIdIncludingDeletedCriteria(PlanogramComparisonTemplateDalType.Unknown, entityId));
        }

        #endregion

        #region Data Access

        private void DataPortal_Fetch(FetchByIdsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateInfoDal>())
                {
                    foreach (PlanogramComparisonTemplateInfoDto dto in dal.FetchByIds(criteria.Ids))
                    {
                        if (criteria.DalType == PlanogramComparisonTemplateDalType.FileSystem)
                        {
                            Add(PlanogramComparisonTemplateInfo.GetPlanogramComparisonTemplateInfo(dalContext, dto, (String)dto.Id));
                        }
                        else
                        {
                            Add(PlanogramComparisonTemplateInfo.GetPlanogramComparisonTemplateInfo(dalContext, dto));
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
            IsReadOnly = true;
        }

        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateInfoDal>())
                {
                    foreach (PlanogramComparisonTemplateInfoDto dto in dal.FetchByEntityId(criteria.EntityId))
                    {
                        if (criteria.DalType == PlanogramComparisonTemplateDalType.FileSystem)
                        {
                            Add(PlanogramComparisonTemplateInfo.GetPlanogramComparisonTemplateInfo(dalContext, dto, (String)dto.Id));
                        }
                        else
                        {
                            Add(PlanogramComparisonTemplateInfo.GetPlanogramComparisonTemplateInfo(dalContext, dto));
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
            IsReadOnly = true;
        }

        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            String dalFactoryName;
            IDalFactory dalFactory = GetDalFactory(criteria.DalType, out dalFactoryName);

            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (var dal = dalContext.GetDal<IPlanogramComparisonTemplateInfoDal>())
                {
                    foreach (PlanogramComparisonTemplateInfoDto dto in dal.FetchByEntityIdIncludingDeleted(criteria.EntityId))
                    {
                        if (criteria.DalType == PlanogramComparisonTemplateDalType.FileSystem)
                        {
                            Add(PlanogramComparisonTemplateInfo.GetPlanogramComparisonTemplateInfo(dalContext, dto, (String)dto.Id));
                        }
                        else
                        {
                            Add(PlanogramComparisonTemplateInfo.GetPlanogramComparisonTemplateInfo(dalContext, dto));
                        }
                    }
                }
            }

            RaiseListChangedEvents = true;
            IsReadOnly = true;
        }
        
        #endregion

        #region Methods

        /// <summary>
        ///     Returns the correct dal factory based on the given type and args.
        /// </summary>
        private static IDalFactory GetDalFactory(PlanogramComparisonTemplateDalType dalType, out String dalFactoryName)
        {
            if (dalType == PlanogramComparisonTemplateDalType.FileSystem)
            {
                dalFactoryName = Constants.UserDal;
                return DalContainer.GetDalFactory(dalFactoryName);
            }
            
            dalFactoryName = DalContainer.DalName;
            return DalContainer.GetDalFactory();
        }

        #endregion
    }
}