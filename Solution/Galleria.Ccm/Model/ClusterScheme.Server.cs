﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25629: A.Kuszyk
//		Created (copied from SA).
// CCM-27078 : I.George
//  Added ProductGroupIdProperty
#endregion
#region Version History: (CCM 8.0.3)
// V8-29498 : N.Haywood
//  Added ParentUniqueContentReference
#endregion
#region Version History: (CCM 8.1.0)
// V8-29936 : N.Haywood
//  Update will set a new ucr
#endregion
#endregion

using System;
using System.Diagnostics.CodeAnalysis;
using Csla;
using Galleria.Framework.Dal;
using Galleria.Framework.DataStructures;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Cluster Scheme server model object
    /// </summary>
    public partial class ClusterScheme
    {
        #region Constructors
        private ClusterScheme() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns the specified review object
        /// </summary>
        /// <param name="id">the unique cluster scheme id</param>
        public static ClusterScheme FetchById(Int32 id)
        {
            return DataPortal.Fetch<ClusterScheme>(new SingleCriteria<ClusterScheme, Int32>(id));
        }

        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Returns a dto for this object
        /// </summary>
        /// <returns>A new dto</returns>
        private ClusterSchemeDto GetDataTransferObject()
        {
            return new ClusterSchemeDto
            {
                Id = ReadProperty<Int32>(IdProperty),
                RowVersion = ReadProperty<RowVersion>(RowVersionProperty),
                UniqueContentReference = ReadProperty<Guid>(UniqueContentReferenceProperty),
                Name = ReadProperty<String>(NameProperty),
                IsDefault = ReadProperty<Boolean>(IsDefaultProperty),
                ProductGroupId = ReadProperty<Int32?>(ProductGroupIdProperty),
                EntityId = ReadProperty<Int32>(EntityIdProperty),
                ParentUniqueContentReference = ReadProperty<Guid?>(ParentUniqueContentReferenceProperty)
            };
        }

        /// <summary>
        /// Loads this object from a dto
        /// </summary>
        /// <param name="dto">The dto to load</param>
        private void LoadDataTransferObject(IDalContext dalContext, ClusterSchemeDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
            LoadProperty<Guid>(UniqueContentReferenceProperty, dto.UniqueContentReference);
            LoadProperty<String>(NameProperty, dto.Name);
            LoadProperty<Boolean>(IsDefaultProperty, dto.IsDefault);
            LoadProperty<ClusterList>(ClustersProperty, ClusterList.FetchByClusterSchemeId(dalContext, dto.Id));
            LoadProperty<Int32>(EntityIdProperty, dto.EntityId);
            LoadProperty<Int32?>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<Guid?>(ParentUniqueContentReferenceProperty, dto.ParentUniqueContentReference);
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when loading this instance
        /// from a data reader as part of a
        /// cluster scheme list
        /// </summary>
        /// <param name="criteria">an object used to identify the desired review</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(SingleCriteria<ClusterScheme, Int32> criteria)
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                ClusterSchemeDto dto;
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dto = dal.FetchById(criteria.Value);
                }

                LoadDataTransferObject(dalContext, dto);
            }
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when persisting this instance in the data store
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Insert()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ClusterSchemeDto dto = GetDataTransferObject();
                Int32 oldId = dto.Id;
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dal.Insert(dto);
                }
                this.LoadProperty<Int32>(IdProperty, dto.Id);
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                dalContext.RegisterId<ClusterScheme>(oldId, dto.Id);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when updating an existing instance
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_Update()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                ClusterSchemeDto dto = GetDataTransferObject();
                dto.UniqueContentReference = Guid.NewGuid();
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dal.Update(dto);
                }
                this.LoadProperty<RowVersion>(RowVersionProperty, dto.RowVersion);
                FieldManager.UpdateChildren(dalContext, this);
                dalContext.Commit();
            }
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this object is deleting itself
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        protected override void DataPortal_DeleteSelf()
        {
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                dalContext.Begin();
                using (IClusterSchemeDal dal = dalContext.GetDal<IClusterSchemeDal>())
                {
                    dal.DeleteById(this.Id);
                }
                dalContext.Commit();
            }
        }

        #endregion

        #endregion
    }
}
