﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-26159 : L.Ineson
//		Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;

namespace Galleria.Ccm.Model
{
    public enum PerformanceSelectionType
    {
        Fixed = 0,
        Dynamic = 1
    }

    public static class PerformanceSelectionTypeHelper
    {
        public static readonly Dictionary<PerformanceSelectionType, String> FriendlyNames =
            new Dictionary<PerformanceSelectionType, String>()
            {
                {PerformanceSelectionType.Fixed, Message.Enum_PerformanceSelectionType_Fixed},
                {PerformanceSelectionType.Dynamic, Message.Enum_PerformanceSelectionType_Dynamic},
            };

    }
}
