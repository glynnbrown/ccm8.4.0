﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
//V8-24801 L.Hodson
//  Created
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Fill Pattern Type Enum
    /// </summary>
    public enum HighlightFillPatternType
    {
        Solid = 0,
        Border = 1,
        DiagonalUp = 2,
        DiagonalDown = 3,
        Crosshatch = 4,
        Horizontal = 5,
        Vertical = 6,
        Dotted = 7
    }

    /// <summary>
    /// HighlightFillPatternType Helper Class
    /// </summary>
    public static class HighlightFillPatternTypeHelper
    {
        /// <summary>
        /// Dictionary with enum value as key and friendly name as value.
        /// </summary>
        public static readonly Dictionary<HighlightFillPatternType, String> FriendlyNames =
            new Dictionary<HighlightFillPatternType, String>()
            {
                {HighlightFillPatternType.Solid, Message.Enum_HighlightFillPatternType_Solid},
                {HighlightFillPatternType.Border, Message.Enum_HighlightFillPatternType_Border},
                {HighlightFillPatternType.DiagonalUp, Message.Enum_HighlightFillPatternType_DiagonalUp},
                {HighlightFillPatternType.DiagonalDown, Message.Enum_HighlightFillPatternType_DiagonalDown},
                {HighlightFillPatternType.Crosshatch, Message.Enum_HighlightFillPatternType_Crosshatch},
                {HighlightFillPatternType.Horizontal, Message.Enum_HighlightFillPatternType_Horizontal},
                {HighlightFillPatternType.Vertical, Message.Enum_HighlightFillPatternType_Vertical},
                {HighlightFillPatternType.Dotted, Message.Enum_HighlightFillPatternType_Dotted}
            };

        public static PlanogramHighlightFillPatternType ToPlanogramHighlightFillPatternType(this HighlightFillPatternType value)
        {
            switch (value)
            {
                default:
                case HighlightFillPatternType.Solid: return PlanogramHighlightFillPatternType.Solid;
                case HighlightFillPatternType.Border:return PlanogramHighlightFillPatternType.Border;
                case HighlightFillPatternType.Crosshatch: return PlanogramHighlightFillPatternType.Crosshatch;
                case HighlightFillPatternType.DiagonalDown: return PlanogramHighlightFillPatternType.DiagonalDown;
                case HighlightFillPatternType.DiagonalUp: return PlanogramHighlightFillPatternType.DiagonalUp;
                case HighlightFillPatternType.Dotted: return PlanogramHighlightFillPatternType.Dotted;
                case HighlightFillPatternType.Horizontal: return PlanogramHighlightFillPatternType.Horizontal;
                case HighlightFillPatternType.Vertical: return PlanogramHighlightFillPatternType.Vertical;
            }
        }

    }
}
