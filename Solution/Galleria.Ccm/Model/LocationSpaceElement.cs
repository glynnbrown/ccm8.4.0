﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
// V8-24264 : A.Probyn
//      Added missing ModelPropertyType
// V8-27710 : A.Kuszyk
//  Added Id value assignment to Create() method.
// V8-28482 : A.Kuszyk
//  Added OnCopy override.
#endregion

#region Version History: CCM833
// CCM-19130 : J.Mendes
//  Added EnumerateDisplayableFieldInfos for the new paramenter to the UpdatePlanogramLocationSpace which allows the user to ignore Location Space Element Attributes 
//    If the user does not ignore any attributtes the shelf will be updated with the Location Space Element attributtes
//    If the user ignores all attributes(with exception of the width attribute), the values of each attribute will remain the same as the original shelf.
//    If the user ignores the width, the shelf width will be same as the bay width
#endregion
#endregion

using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Resources.Language;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;
using Galleria.Framework.ViewModel;
using System.Collections.Generic;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// Represents a description of a type of location space Element
    /// (child of location space Element list)
    /// </summary>
    [Serializable]
    public partial class LocationSpaceElement : ModelObject<LocationSpaceElement>
    {
        #region Parent
        /// <summary>
        /// Returns a reference to the parent object
        /// </summary>
        public new LocationSpaceBay Parent
        {
            get { return ((LocationSpaceElementList)base.Parent).Parent; }
        }
        #endregion

        #region Properties

        /// <summary>
        /// The Location Space Element Id.
        /// </summary>
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);
        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
            set { SetProperty<Int32>(IdProperty, value); }
        }

        /// <summary>
        /// The Order.
        /// </summary>
        public static readonly ModelPropertyInfo<Byte> OrderProperty =
            RegisterModelProperty<Byte>(c => c.Order, Message.LocationSpaceElement_Order);
        public Byte Order
        {
            get { return GetProperty<Byte>(OrderProperty); }
            set { SetProperty<Byte>(OrderProperty, value); }
        }

        /// <summary>
        /// The X.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> XProperty =
            RegisterModelProperty<Single>(c => c.X, ModelPropertyDisplayType.Length);
        public Single X
        {
            get { return GetProperty<Single>(XProperty); }
            set { SetProperty<Single>(XProperty, value); }
        }

        /// <summary>
        /// The Y.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> YProperty =
            RegisterModelProperty<Single>(c => c.Y, Message.LocationSpaceElement_Y, ModelPropertyDisplayType.Length);
        public Single Y
        {
            get { return GetProperty<Single>(YProperty); }
            set { SetProperty<Single>(YProperty, value); }
        }

        /// <summary>
        /// The Z.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> ZProperty =
            RegisterModelProperty<Single>(c => c.Z, ModelPropertyDisplayType.Length);
        public Single Z
        {
            get { return GetProperty<Single>(ZProperty); }
            set { SetProperty<Single>(ZProperty, value); }
        }

        /// <summary>
        /// The MerchandisableHeight.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> MerchandisableHeightProperty =
            RegisterModelProperty<Single>(c => c.MerchandisableHeight, Message.LocationSpaceElement_MerchandisableHeight, ModelPropertyDisplayType.Length);
        public Single MerchandisableHeight
        {
            get { return GetProperty<Single>(MerchandisableHeightProperty); }
            set { SetProperty<Single>(MerchandisableHeightProperty, value); }
        }

        /// <summary>
        /// The Height.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> HeightProperty =
            RegisterModelProperty<Single>(c => c.Height, Message.LocationSpaceElement_Height, ModelPropertyDisplayType.Length);
        public Single Height
        {
            get { return GetProperty<Single>(HeightProperty); }
            set { SetProperty<Single>(HeightProperty, value); }
        }

        /// <summary>
        /// The Width.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> WidthProperty =
            RegisterModelProperty<Single>(c => c.Width, Message.LocationSpaceElement_Width, ModelPropertyDisplayType.Length);
        public Single Width
        {
            get { return GetProperty<Single>(WidthProperty); }
            set { SetProperty<Single>(WidthProperty, value); }
        }

        /// <summary>
        /// The Depth.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> DepthProperty =
            RegisterModelProperty<Single>(c => c.Depth, Message.LocationSpaceElement_Depth, ModelPropertyDisplayType.Length);
        public Single Depth
        {
            get { return GetProperty<Single>(DepthProperty); }
            set { SetProperty<Single>(DepthProperty, value); }
        }

        /// <summary>
        /// The Face Thickness.
        /// </summary>
        public static readonly ModelPropertyInfo<Single> FaceThicknessProperty =
            RegisterModelProperty<Single>(c => c.FaceThickness, Message.LocationSpaceElement_FaceThickness, ModelPropertyDisplayType.Length);
        public Single FaceThickness
        {
            get { return GetProperty<Single>(FaceThicknessProperty); }
            set { SetProperty<Single>(FaceThicknessProperty, value); }
        }


        /// <summary>
        /// The Notch Number
        /// </summary>
        public static readonly ModelPropertyInfo<Nullable<Single>> NotchNumberProperty =
            RegisterModelProperty<Nullable<Single>>(c => c.NotchNumber, Message.LocationSpaceElement_NotchNumber);
        public Nullable<Single> NotchNumber
        {
            get { return GetProperty<Nullable<Single>>(NotchNumberProperty); }
            set { SetProperty<Nullable<Single>>(NotchNumberProperty, value); }
        }


        #endregion

        #region Authorization Rules
        /// <summary>
        /// Defines the authorization rules for this type
        /// </summary>
        private static void AddObjectAuthorizationRules()
        {
            BusinessRules.AddRule(typeof(LocationSpaceElement), new IsInRole(AuthorizationActions.CreateObject, DomainPermission.LocationSpaceCreate.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceElement), new IsInRole(AuthorizationActions.GetObject, DomainPermission.LocationSpaceGet.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceElement), new IsInRole(AuthorizationActions.EditObject, DomainPermission.LocationSpaceEdit.ToString()));
            BusinessRules.AddRule(typeof(LocationSpaceElement), new IsInRole(AuthorizationActions.DeleteObject, DomainPermission.LocationSpaceDelete.ToString()));
        }

        #endregion

        #region Business Rules
        /// <summary>
        /// Adds business rules to this instance
        /// </summary>
        protected override void AddBusinessRules()
        {
            base.AddBusinessRules();
            BusinessRules.AddRule(new Required(OrderProperty));
            BusinessRules.AddRule(new MinValue<Byte>(OrderProperty, 1));

            BusinessRules.AddRule(new Required(MerchandisableHeightProperty));
            BusinessRules.AddRule(new MinValue<Single>(MerchandisableHeightProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(MerchandisableHeightProperty, 999.99f));

            BusinessRules.AddRule(new Required(DepthProperty));
            BusinessRules.AddRule(new MinValue<Single>(DepthProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(DepthProperty, 999.99f));

            BusinessRules.AddRule(new Required(HeightProperty));
            BusinessRules.AddRule(new MinValue<Single>(HeightProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(HeightProperty, 999.99f));

            BusinessRules.AddRule(new Required(WidthProperty));
            BusinessRules.AddRule(new MinValue<Single>(WidthProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(WidthProperty, 999.99f));

            BusinessRules.AddRule(new Required(FaceThicknessProperty));
            BusinessRules.AddRule(new MinValue<Single>(FaceThicknessProperty, 0));
            BusinessRules.AddRule(new MaxValue<Single>(FaceThicknessProperty, 999.99f));

            BusinessRules.AddRule(new Required(XProperty));
            BusinessRules.AddRule(new MinValue<Single>(XProperty, -99999.0f));
            BusinessRules.AddRule(new MaxValue<Single>(XProperty, 99999.0f));

            BusinessRules.AddRule(new Required(YProperty));
            BusinessRules.AddRule(new MinValue<Single>(YProperty, -99999.0f));
            BusinessRules.AddRule(new MaxValue<Single>(YProperty, 99999.0f));

            BusinessRules.AddRule(new Required(ZProperty));
            BusinessRules.AddRule(new MinValue<Single>(ZProperty, -99999.0f));
            BusinessRules.AddRule(new MaxValue<Single>(ZProperty, 99999.0f));
        }

        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns a new LocationSpaceElement object
        /// </summary>
        /// <returns></returns>
        public static LocationSpaceElement NewLocationSpaceElement()
        {
            LocationSpaceElement item = new LocationSpaceElement();
            item.Create();
            return item;
        }

        #endregion

        #region Data Access

        #region Create
        /// <summary>
        /// Called when creating a new object of this type
        /// </summary>
        private void Create()
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<Byte>(OrderProperty, 1);
            this.MarkAsChild();
            this.MarkGraphAsInitialized();
        }
        #endregion

        #endregion

        #region Methods
        
        #region Field Infos

        /// <summary>
        /// Enumerates through infos for all properties
        /// on this model that can be displayed to the user.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ObjectFieldInfo> EnumerateDisplayableFieldInfos(Boolean ExcludeNonShelfUpdateAttributes)
        {
            Type type = typeof(LocationSpaceElement);
            String typeFriendly = LocationSpaceElement.FriendlyName;

            String detailsGroup = LocationSpaceElement.FriendlyName;

            if (!ExcludeNonShelfUpdateAttributes) yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, IdProperty, detailsGroup);
            if (!ExcludeNonShelfUpdateAttributes) yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, OrderProperty, detailsGroup);
            if (!ExcludeNonShelfUpdateAttributes) yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, XProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, YProperty, detailsGroup);
            if (!ExcludeNonShelfUpdateAttributes) yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, ZProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, MerchandisableHeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, HeightProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, WidthProperty, detailsGroup);
            yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, DepthProperty, detailsGroup);
            if (!ExcludeNonShelfUpdateAttributes) yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, FaceThicknessProperty, detailsGroup);
            if (!ExcludeNonShelfUpdateAttributes) yield return ObjectFieldInfo.NewObjectFieldInfo(type, typeFriendly, NotchNumberProperty, detailsGroup);

        }

        #endregion

        #endregion

        #region Override

        /// <summary>
        /// Called when this instance is being copied
        /// </summary>
        protected override void OnCopy(CopyContext context)
        {
            Int32 oldId = this.Id;
            Int32 newId = IdentityHelper.GetNextInt32();
            SetProperty<Int32>(IdProperty, newId);
            context.RegisterId<LocationSpaceElement>(oldId, newId);
        }

        public override string ToString()
        {
            return this.Id.ToString();
        }
        #endregion
    }
}
