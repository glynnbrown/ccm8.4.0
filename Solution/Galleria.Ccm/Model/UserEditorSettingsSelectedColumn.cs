﻿#region Header Information
// Copyright © Galleria RTS Ltd 2015

#region Version History CCM830
// V8-31934 : A.Heathcote
//      Created.
#endregion
#endregion
using System;
using Csla.Rules;
using Csla.Rules.CommonRules;
using Galleria.Ccm.Security;
using Galleria.Framework.Helpers;
using Galleria.Framework.Model;

namespace Galleria.Ccm.Model
{
    [Serializable]
    public partial class UserEditorSettingsSelectedColumn : ModelObject<UserEditorSettingsSelectedColumn>
    {
        #region Properties

        #region Id
        public static readonly ModelPropertyInfo<Int32> IdProperty =
            RegisterModelProperty<Int32>(c => c.Id);

        public Int32 Id
        {
            get { return GetProperty<Int32>(IdProperty); }
        }
        #endregion

        #region FieldPlaceHolder
        public static readonly ModelPropertyInfo<String> FieldPlaceHolderProperty =
           RegisterModelProperty<String>(c => c.FieldPlaceHolder);

        public String FieldPlaceHolder
        {
            get { return GetProperty<String>(FieldPlaceHolderProperty); }
        }
        #endregion

        #region ColumnType
        public static readonly ModelPropertyInfo<SearchColumnType> ColumnTypeProperty =
           RegisterModelProperty<SearchColumnType>(c => c.ColumnType);

        public SearchColumnType ColumnType
        {
            get { return GetProperty<SearchColumnType>(ColumnTypeProperty); }
        }
        #endregion

        #endregion

        #region Authorisation Rules
        private static void AddObjectAuthorisationRules()
        {
        }
        #endregion

        #region Factory Methods
        public static UserEditorSettingsSelectedColumn NewUserEditorSettingsSelectedColumn(String FieldPlaceHolder, SearchColumnType ColumnType)
        {
            UserEditorSettingsSelectedColumn item = new UserEditorSettingsSelectedColumn();
            item.Create(FieldPlaceHolder, ColumnType);
            return item;
        }
        #endregion

        #region Data Access

        #region Create
        private void Create(String FieldPlaceHolder, SearchColumnType ColumnType)
        {
            this.LoadProperty<Int32>(IdProperty, IdentityHelper.GetNextInt32());
            this.LoadProperty<String>(FieldPlaceHolderProperty, FieldPlaceHolder);
            this.LoadProperty<SearchColumnType>(ColumnTypeProperty, ColumnType);
            this.MarkAsChild();
            base.Create();
        }
        #endregion

        #endregion

    }
}
