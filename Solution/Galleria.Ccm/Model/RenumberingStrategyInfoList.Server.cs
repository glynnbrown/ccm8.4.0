﻿#region Header Information

// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)

// V8-27153 : A.Silva   ~ Created.

#endregion

#endregion

using System;
using Csla;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Framework.Dal;

namespace Galleria.Ccm.Model
{
    /// <summary>
    ///     Server side implementation for <see cref="RenumberingStrategyInfoList"/>.
    /// </summary>
    public partial class RenumberingStrategyInfoList
    {
        #region Constructor

        /// <summary>
        ///     Private constructor. Use factory methods to instantiate this type.
        /// </summary>
        private RenumberingStrategyInfoList(){}

        #endregion

        #region Factory Methods

        public static RenumberingStrategyInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<RenumberingStrategyInfoList>(new RenumberingStrategyInfoList.FetchByEntityIdCriteria(entityId));
        }

        public static RenumberingStrategyInfoList FetchByEntityIdIncludingDeleted(Int32 entityId)
        {
            return DataPortal.Fetch<RenumberingStrategyInfoList>(new RenumberingStrategyInfoList.FetchByEntityIdIncludingDeletedCriteria(entityId));
        }

        #endregion

        #region Data Access

        /// <summary>
        ///     Called when retrieving a list of all Validation templates for an entity.
        /// </summary>
        /// <param name="criteria">The criteria to select valid items with.</param>
        /// <remarks>This procedure should NOT return <c>deleted</c> items.</remarks>
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;
            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IRenumberingStrategyInfoDal>())
            {
                var dtoList = dal.FetchByEntityId(criteria.EntityId);
                foreach (var dto in dtoList)
                {
                    Add(RenumberingStrategyInfo.GetRenumberingStrategyInfo(dalContext, dto));
                }
            }
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        /// <summary>
        ///     Called when retrieving a list of all Validation templates for an entity.
        /// </summary>
        /// <param name="criteria">The criteria to select valid items with.</param>
        private void DataPortal_Fetch(FetchByEntityIdIncludingDeletedCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;
            var dalFactory = DalContainer.GetDalFactory();
            using (var dalContext = dalFactory.CreateContext())
            using (var dal = dalContext.GetDal<IRenumberingStrategyInfoDal>())
            {
                var dtoList = dal.FetchByEntityIdIncludingDeleted(criteria.EntityId);
                foreach (var dto in dtoList)
                {
                    Add(RenumberingStrategyInfo.GetRenumberingStrategyInfo(dalContext, dto));
                }
            }
            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        #endregion
    }
}
