﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 800)
// CCM-25631 : N.Haywood
//      Copied from SA
#endregion
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.DataTransferObjects;
using Csla;
using System.Diagnostics.CodeAnalysis;
using Galleria.Ccm.Dal.Interfaces;

namespace Galleria.Ccm.Model
{
    public partial class LocationSpaceProductGroup
    {
        #region Constructor
        private LocationSpaceProductGroup() { } // force use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Called when creating an instance from a dto
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        /// <returns>A new instance of this type</returns>
        internal static LocationSpaceProductGroup GetLocationSpaceProductGroup(IDalContext dalContext, LocationSpaceProductGroupDto dto)
        {
            return DataPortal.FetchChild<LocationSpaceProductGroup>(dalContext, dto);
        }
        #endregion

        #region Data Access

        #region Data Transfer Object
        /// <summary>
        /// Loads this instance from a data transfer object
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        private void LoadDataTransferObject(IDalContext dalContext, LocationSpaceProductGroupDto dto)
        {
            LoadProperty<Int32>(IdProperty, dto.Id);
            LoadProperty<Int32>(ProductGroupIdProperty, dto.ProductGroupId);
            LoadProperty<Single>(BayCountProperty, dto.BayCount);
            LoadProperty<Int32?>(ProductCountProperty, dto.ProductCount);
            LoadProperty<Single>(AverageBayWidthProperty, dto.AverageBayWidth);
            LoadProperty<String>(AisleNameProperty, dto.AisleName);
            LoadProperty<String>(ValleyNameProperty, dto.ValleyName);
            LoadProperty<String>(ZoneNameProperty, dto.ZoneName);
            LoadProperty<String>(CustomAttribute01Property, dto.CustomAttribute01);
            LoadProperty<String>(CustomAttribute02Property, dto.CustomAttribute02);
            LoadProperty<String>(CustomAttribute03Property, dto.CustomAttribute03);
            LoadProperty<String>(CustomAttribute04Property, dto.CustomAttribute04);
            LoadProperty<String>(CustomAttribute05Property, dto.CustomAttribute05);
            LoadProperty<LocationSpaceBayList>(BaysProperty, LocationSpaceBayList.FetchByLocationSpaceProductGroupId(dalContext, dto.Id));
        }

        /// <summary>
        /// Returns a dto created from this instance
        /// </summary>
        /// <returns>A data transfer object</returns>
        private LocationSpaceProductGroupDto GetDataTransferObject(LocationSpace parent)
        {
            return new LocationSpaceProductGroupDto()
            {
                Id = ReadProperty<Int32>(IdProperty),
                ProductGroupId = ReadProperty<Int32>(ProductGroupIdProperty),
                BayCount = ReadProperty<Single>(BayCountProperty),
                ProductCount = ReadProperty<Int32?>(ProductCountProperty),
                AverageBayWidth = ReadProperty<Single>(AverageBayWidthProperty),
                AisleName = ReadProperty<String>(AisleNameProperty),
                ValleyName = ReadProperty<String>(ValleyNameProperty),
                ZoneName = ReadProperty<String>(ZoneNameProperty),
                CustomAttribute01 = ReadProperty<String>(CustomAttribute01Property),
                CustomAttribute02 = ReadProperty<String>(CustomAttribute02Property),
                CustomAttribute03 = ReadProperty<String>(CustomAttribute03Property),
                CustomAttribute04 = ReadProperty<String>(CustomAttribute04Property),
                CustomAttribute05 = ReadProperty<String>(CustomAttribute05Property),
                LocationSpaceId = parent.Id
            };
        }
        #endregion

        #region Fetch
        /// <summary>
        /// Called when this instance is being loaded
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="dto">The dto to load from</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Fetch(IDalContext dalContext, LocationSpaceProductGroupDto dto)
        {
            this.LoadDataTransferObject(dalContext, dto);
        }
        #endregion

        #region Insert
        /// <summary>
        /// Called when this instance is being inserted into the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Insert(IDalContext dalContext, LocationSpace parent)
        {
            LocationSpaceProductGroupDto dto = GetDataTransferObject(parent);
            Int32 oldId = dto.Id;
            using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
            {
                dal.Insert(dto);
            }
            this.LoadProperty<Int32>(IdProperty, dto.Id);
            dalContext.RegisterId<LocationSpaceProductGroup>(oldId, dto.Id);
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Update
        /// <summary>
        /// Called when this instance is being updated in the database
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        /// <param name="parent">The parent product universe</param>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void Child_Update(IDalContext dalContext, LocationSpace parent)
        {
            if (this.IsSelfDirty)
            {
                using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
                {
                    dal.Update(this.GetDataTransferObject(parent));
                }
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #region Delete
        /// <summary>
        /// Called when this instance is deleting itself
        /// </summary>
        /// <param name="dalContext">The current dal context</param>
        private void Child_DeleteSelf(IDalContext dalContext, LocationSpace parent)
        {
            using (ILocationSpaceProductGroupDal dal = dalContext.GetDal<ILocationSpaceProductGroupDal>())
            {
                dal.DeleteById(this.Id);
            }
            FieldManager.UpdateChildren(dalContext, this);
        }
        #endregion

        #endregion
    }
}
