﻿#region Header Information
// Copyright © Galleria RTS Ltd 2014

#region Version History: (CCM 8.0)
// V8-25632 : A.Kuszyk
//		Created (copied from SA).
// V8-26520 : J.Pickup
//		Added fetch by product group id criteria
#endregion
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

using Csla;
using Csla.Data;
using Csla.Security;
using Csla.Serialization;
using System.Linq;

using Galleria.Framework.Dal;
using Galleria.Ccm.Dal.Interfaces;
using Galleria.Ccm.Dal.DataTransferObjects;
using Galleria.Ccm.Security;
using Galleria.Framework.DataStructures;

namespace Galleria.Ccm.Model
{
    /// <summary>
    /// ConsumerDecisionTree info list
    /// </summary>
    public partial class ConsumerDecisionTreeInfoList
    {
        #region Constructors
        private ConsumerDecisionTreeInfoList() { } // force the use of factory methods
        #endregion

        #region Factory Methods
        /// <summary>
        /// Returns all entity infos
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static ConsumerDecisionTreeInfoList FetchByEntityId(Int32 entityId)
        {
            return DataPortal.Fetch<ConsumerDecisionTreeInfoList>(new FetchByEntityIdCriteria(entityId));
        }

        /// <summary>
        /// Returns all entity infos
        /// </summary>
        /// <returns>A list of entity infos</returns>
        public static ConsumerDecisionTreeInfoList FetchByProductGroupId(Int32 productGroupId)
        {
            return DataPortal.Fetch<ConsumerDecisionTreeInfoList>(new FetchByProductGroupIdCriteria(productGroupId));
        }

        /// <summary>
        /// Returns all infos for the given ids including deleted.
        /// </summary>
        /// <param name="consumerDecisionTreeIds"></param>
        /// <returns></returns>
        public static ConsumerDecisionTreeInfoList FetchByIds(IEnumerable<Int32> consumerDecisionTreeIds)
        {
            return DataPortal.Fetch<ConsumerDecisionTreeInfoList>(new FetchByIdsCriteria(consumerDecisionTreeIds.ToList()));
        }
        #endregion

        #region Data Access

        /// <summary>
        /// Called when returning all reviews
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByEntityIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeInfoDal dal = dalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                {
                    IEnumerable<ConsumerDecisionTreeInfoDto> dtoList = dal.FetchByEntityId(criteria.EntityId);
                    foreach (ConsumerDecisionTreeInfoDto dto in dtoList)
                    {
                        this.Add(ConsumerDecisionTreeInfo.FetchConsumerDecisionTreeInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            RaiseListChangedEvents = true;
        }


        /// <summary>
        /// Called when returning by product group id
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void DataPortal_Fetch(FetchByProductGroupIdCriteria criteria)
        {
            RaiseListChangedEvents = false;
            this.IsReadOnly = false;
            IDalFactory dalFactory = DalContainer.GetDalFactory();
            using (IDalContext dalContext = dalFactory.CreateContext())
            {
                using (IConsumerDecisionTreeInfoDal dal = dalContext.GetDal<IConsumerDecisionTreeInfoDal>())
                {
                    IEnumerable<ConsumerDecisionTreeInfoDto> dtoList = dal.FetchByProductGroupId(criteria.ProductGroupId);
                    foreach (ConsumerDecisionTreeInfoDto dto in dtoList)
                    {
                        this.Add(ConsumerDecisionTreeInfo.FetchConsumerDecisionTreeInfo(dalContext, dto));
                    }
                }
            }
            this.IsReadOnly = true;
            RaiseListChangedEvents = true;
        }


        #endregion
    }
}
