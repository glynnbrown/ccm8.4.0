﻿#region Header Information
// Copyright © Galleria RTS Ltd 2013

#region Version History: (CCM 8.0)
// V8-24265 : J.Pickup
//  Created
#endregion
#region Version History: (CCM 8.03)
// V8-28662 : L.Ineson
//  Added ToPlanogramLabelTextWrapping
#endregion
#endregion

using System;
using System.Collections.Generic;
using Galleria.Ccm.Resources.Language;
using Galleria.Framework.Planograms.Interfaces;

namespace Galleria.Ccm.Model
{
    public enum LabelTextWrapping
    {
        Letter = 0,
        Word = 1,
        None = 2,
    }

    public static class LabelTextWrappingHelper
    {
        public static readonly Dictionary<LabelTextWrapping, String> FriendlyNames =
            new Dictionary<LabelTextWrapping, String>()
            {
                {LabelTextWrapping.Letter, Message.Enum_TextWrappingType_Letter},
                {LabelTextWrapping.Word, Message.Enum_TextWrappingType_Word},
                {LabelTextWrapping.None, Message.Enum_TextWrappingType_None},
            };

        public static PlanogramLabelTextWrapping ToPlanogramLabelTextWrapping(this LabelTextWrapping value)
        {
            switch (value)
            {
                default:
                case LabelTextWrapping.Letter: return PlanogramLabelTextWrapping.Letter;
                case LabelTextWrapping.None: return PlanogramLabelTextWrapping.None;
                case LabelTextWrapping.Word: return PlanogramLabelTextWrapping.Word;
            }
        }
    }
}
